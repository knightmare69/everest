<?php 
namespace App\Notifications\Models;

use illuminate\Database\Eloquent\Model;

Class Inbox extends Model {

	public $table='ES_GS_Inbox';
	protected $primaryKey ='ID';

	protected $fillable  = array(
			'InboxTypeID',
			'UserIDFrom',
			'Icon',
			'Subject',
			'Content',
			'InboxDate',
			'InboxBy',
	);

	public $timestamps = false;
}