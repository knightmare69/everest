<?php 
namespace App\Notifications\Models;

use illuminate\Database\Eloquent\Model;

Class To extends Model {

	public $table='ES_GS_InboxUserTo';
	protected $primaryKey ='ToID';

	protected $fillable  = array(
			'InboxID',
			'UserIDTo',
			'IsRead',
			'ReadDate'
	);

	public $timestamps = false;
}