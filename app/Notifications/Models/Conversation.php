<?php 
namespace App\Notifications\Models;

use illuminate\Database\Eloquent\Model;

Class Conversation extends Model {

	public $table='ES_GS_InboxConversations';
	protected $primaryKey ='ConvoID';

	protected $fillable  = array(
			'InboxID',
			'UserID',
			'IsSender',
			'Content',
			'IsRead',
			'ReadDate',
			'Content',
			'Content',
			'CreatedBy',
	);

	public $timestamps = false;
}