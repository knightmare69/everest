<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
    'auth' => 'Auth\AuthController',
]);

Route::group(['prefix'=>'general'], function() {
	Route::get('getThumbnailPhoto','General@getThumbnailPhoto');
	Route::get('defaultThumnail','General@defaultThumnail');
	Route::get('getGuardianPhotoThumbnail','General@getGuardianPhotoThumbnail');
	Route::get('getStudentPhoto','General@getStudentPhoto');
	Route::get('getGuardianPhoto','General@getGuardianPhoto');
	Route::get('downloadAdmissionDoc','General@downloadAdmissionDoc');
	Route::get('getUserPhoto','General@getUserPhoto');
	Route::get('getDocs','General@getDocs');
	Route::get('test','General@test');
    Route::get('getPupilPhoto','General@getPupilPhoto');
	Route::get('getStudentPhotoByStudentNo','General@getStudentPhotoByStudentNo');
});

Route::get('job','Job@index');


Route::group(['prefix'=>'notification'], function() {
	//pending notifications
	Route::post('getNofifications','Notification@getNofifications');
	Route::post('getNofificationsCount','Notification@getNofificationsCount');

	//unread messages 
	Route::post('getUnreadMessages','Notification@getUnreadMessages');
	Route::post('getUnreadMessagesCount','Notification@getUnreadMessagesCount');

	Route::post('event','Notification@event');
});
