<?php

namespace App\Http\Controllers;
use Request;
use Image;
use DB;

class General extends Controller {

    public function getThumbnailPhoto()
    {
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		echo Request::file('photo')->getRealPath();

    }

    public function defaultThumnail()
    {
    	ob_clean();
    	header("Content-Type: image/png");

		return
		Image::make(public_path('assets/system/media/images/no-image.png'))
		->fit(200)
		->fit(200, 200, function ($constraint) {
		    $constraint->upsize();
		})
		->response();
    }

    public function getGuardianPhotoThumbnail()
    {
    	if (Request::get('PhotoID')) {

			$FamilyID = decode(Request::get('PhotoID'));
			$FileName = DB::table('ESv2_Admission_FamilyBackground')->select('Guardian_Photo')->where('FamilyID',$FamilyID)->pluck('Guardian_Photo');
			$FileNamePath = env('STORAGE_ADMISSION').'Family\\'.$FamilyID.'\\'.$FileName;
			if ($FileName) {
				if(file_exists(base_path('storage\app\\'.$FileNamePath))) {
					$FileNamePath = storage_path('app/'.$FileNamePath);
					header("Content-Type: ".image_type_to_mime_type(exif_imagetype($FileNamePath)));
					readfile(
							Image::make($FileNamePath)
							->fit(50)
							->fit(50, 50, function ($constraint) {
							    $constraint->upsize();
							})
							->encode('data-url')
					);
					die();
				}
			}
		}
		header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->fit(50)
			->fit(50, 50, function ($constraint) {
			    $constraint->upsize();
			})
			->response();
    }

    public function getPupilPhoto(){
        ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "350K");

        $data = DB::table('ES_Students')->select(['StudentPicture','StudentNo'])->where('StudentNo',decode(Request::get('Idno')))->first();

		if ($data) {
			if (trim($data->StudentPicture)){
				header("Content-Type: image/png ");
				readfile(
						Image::make(hex2bin($data->StudentPicture))
						->fit(200, 200, function ($constraint) {
						    $constraint->upsize();
						})
						->encode('data-url')
				);
				die();
			}
		}

		header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
    }

    public function getStudentPhotoByStudentNo()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		$data = DB::table('ES_Students');

		if ($data->where('StudentNo',Request::get('StudentNo'))->count() > 0) {
			$data = $data->where('StudentNo',Request::get('StudentNo'))->first();
			if (!empty($data->StudentPicture)){
				header("Content-Type: image/jpg");
				readfile(
						Image::make(hex2bin($data->StudentPicture))
						->fit(200, 200, function ($constraint) {
						    $constraint->upsize();
						})
						->encode('data-url')
				);
				die();
			}
		}

		header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
	}


    public function getStudentPhoto()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		$data = DB::connection('sqlsrvAttachment')->table('AdmissionPhoto');

		if ($data->where('AppNo',Request::get('AppNo'))->count() > 0) {
			$data = $data->where('AppNo',Request::get('AppNo'))->get();
			$data = $data[0];
			if (trim($data->Attachment)){
				header("Content-Type: ".$data->FileType);
				readfile(
						Image::make(hex2bin($data->Attachment))
						->fit(200, 200, function ($constraint) {
						    $constraint->upsize();
						})
						->encode('data-url')
				);
				die();
			}
		}

		header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
	}

	public function getGuardianPhoto()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		if (Request::get('FamilyID')) {

			$data = DB::connection('sqlsrvAttachment')->table('AdmissionGuardianPhoto');

			if ($data->where('FamilyID',decode(Request::get('FamilyID')))->count() > 0) {
				$data = $data->where('FamilyID',decode(Request::get('FamilyID')))->get();
				$data = $data[0];
				header("Content-Type: ".$data->FileType);
				readfile(
						Image::make(hex2bin($data->Attachment))
						->fit(200, 200, function ($constraint) {
						    $constraint->upsize();
						})
						->encode('data-url')
				);
				die();
			}
		}
		header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
	}

	public function getDocs()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		$FamilyID = decode(Request::get('FamilyID'));
		$docs = DB::table('ESv2_Admission_RequiredDocs')->select(['Filename','AppNo'])->where('EntryID',decode(Request::get('key')))->get();

		$FileName = $docs[0]->Filename;
		$FileNamePath = env('STORAGE_ADMISSION').'Family/'.$FamilyID.'/students/'.$docs[0]->AppNo.'\\'.$FileName;
		if ($FileName) {
			if(file_exists(base_path('storage/app/'.$FileNamePath))) {
				$FileNamePath = storage_path('app/'.$FileNamePath);
				header("Content-Type: ".image_type_to_mime_type(exif_imagetype($FileNamePath)));
				readfile(
						Image::make($FileNamePath)
						// ->resize(500, 500, function($constraint) {
						// 	$constraint->aspectRatio();
						// })
						// ->resizeCanvas(510, 510, 'center', false, 'ccc')
						->encode('data-url')
				);
				die();
			}
		}
		header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
	}

	public function downloadAdmissionDoc()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		$docs = DB::table('ESv2_Admission_RequiredDocs')->select(['Filename','AppNo'])->where('EntryID',decode(Request::get('key')))->get();
		if (count($docs) > 0) {
			$FamilyID = getFamilyIDByAppNo($docs[0]->AppNo);
			$FileName = $docs[0]->Filename;
			$FileNamePath = env('STORAGE_ADMISSION').'Family/'.$FamilyID.'/students/'.$docs[0]->AppNo.'\\'.$FileName;
			if ($FileName) {
				if(file_exists(base_path('storage/app/'.$FileNamePath))) {
					$FileNamePath = storage_path('app/'.$FileNamePath);
					ini_set("odbc.defaultlrl", "100000K");
					header("Content-Type: ".image_type_to_mime_type(exif_imagetype($FileNamePath)));
					header('Content-Description: File Transfer');
			        header('Content-Disposition: attachment; filename="'.$FileName.'"');
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				    header('Pragma: public');
				    header('Content-Length: '.filesize($FileNamePath));

				    echo file_get_contents($FileNamePath);

				}
			}
		} else {
			echo "No Document Found!";
		}
	}

	public function getUserPhoto()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");

		$data = DB::table('ESV2_Users');

		$UserID = getUserID();

		if (Request::has('UserID')) {
			$UserID = decode(Request::get('UserID'));
		}

		if ($data->where('UserIDX',$UserID)->count() > 0) {
			$data = $data->where('UserIDX',$UserID)->get();
			$data = $data[0];
			if ($data->Photo) {
				header("Content-Type: ".$data->PhotoExt);
				readfile(
						Image::make(hex2bin($data->Photo))
						->fit(200, 200, function ($constraint) {
						    $constraint->upsize();
						})
						->encode('data-url')
				);
				die();
			}
		}

	header("Content-Type: image/png");
		return
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
	}

	public function test()
	{
		// DB::raw('0x'.bin2hex((zlib_encode(json_encode($value), 15))))
		$file = 'C:\Users\victor\Pictures\10169327_986737274675353_6505018572816968485_n.jpg';

		DB::connection('sqlsrvAttachment')->table('AdmissionPhoto')
			->insert([
				'AppNo' => 'test1',
				'FileName' => '',
				'Attachment' => DB::raw('0x'.bin2hex(file_get_contents($file))),
				'FileType' => 'type',
				'FileSize' => FileSize($file),
				'FileExtension' => 'jpg',
				'CreatedDate' => systemDate(),
				'CreatedBy' => '1',
			]);

		$data = DB::connection('sqlsrvAttachment')->table('AdmissionPhoto')
			->select('Attachment')
			->where('AppNo','test1')
			->pluck('Attachment');

			header("Content-Type: image/jpg");

			echo hex2bin($data);
	}
    
    
    
}
