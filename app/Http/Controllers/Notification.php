<?php namespace App\Http\Controllers;

use App\Notifications\Services;
use Request;
use Image;
use DB;

class Notification extends Controller {
	 
	//for reference see App\Notifications\Services

	//use this function to create message  notification()->createMessage($UserFrom, $UserTo, $Subject = '', $Content = '');

	//use this function to create notification  notification()->createNotification($UserFrom, $UserTo, $Subject = '', $Content = '');

	public function __construct() {
		$this->initializer();
	}

	public function getNofifications() {
		return
		view('Content.Notifications.PendingNoti',[
				'data' => $this->services->getUserNofifications()
							->where('InboxTypeID',1)
							->where('To.IsRead','<>','1')
							->where('to.UserIDTo',getUserID())
							->get()
			])->render();
	}

	public function getNofificationsCount() {
		return ['total' => 
					$this->services->getUserNofifications()
					->where('InboxTypeID',1)
					->where('To.IsRead','<>','1')
					->where('to.UserIDTo',getUserID())
					->count()
				];
	}

	public function getUnreadMessages() {
		return
		view('Content.Notifications.UnreadMessages',[
				'data' => $this->services->getUserNofifications()
							->where('InboxTypeID',2)
							->where('To.IsRead','<>','1')
							->where('to.UserIDTo',getUserID())
							->get()
			])->render();
	}

	public function getUnreadMessagesCount() {
		return ['total' => 
					$this->services->getUserNofifications()
					->where('InboxTypeID',2)
					->where('To.IsRead','<>','1')
					->where('to.UserIDTo',getUserID())
					->count()
				];
	}

	public function event()
	{	
		$response = noEvent();
		if (Request::ajax())
		{
			switch(Request::get('event'))
			{
				case 'showMessageForm':
					//set notifaction to read
					$this->services->setRead(decode(Request::get('InboxID')),decode(Request::get('ToID')));
					//return message
					$response = view('Content.Notifications.ViewMessage')->render();
					break;
				case 'MessageCompose':
					$response = view('Content.Notifications.Reply')->render();
					break;
				
			}
		}
		return $response;
	}

	private function initializer()
	{
		$this->services = new Services;
	}
}
