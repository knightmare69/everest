<?php

namespace App\Http\Controllers\Auth;

use App\Modules\Security\Controllers\VCLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

trait AuthenticatesUsers
{
    use RedirectsUsers;

    private $username = 'Username';

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOverride(){
		putSessionData('currentStep',1);
        return redirect('/admission/apply');
	}
	
	public function getLogin(){
        putSessionData('inquired',false);
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function postLogin(Request $request)
    {

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        //updated for confirmation code login
        if (VCLogin::isVC()) {
            if (VCLogin::loginUsingVC()) {
                if(Auth::loginUsingId(VCLogin::getUser())) {
                    return $this->handleUserWasAuthenticated($request, $throttles);    
                }
            }
        } else {
            $credentials = $this->getCredentials($request);
            if (Auth::attempt($credentials, $request->has('remember'))) {
                if (!VCLogin::isConfirmed()) {
                    Auth::logout();
                    return redirect($this->loginPath())
                        ->withInput($request->only($this->loginUsername(), 'remember'))
                        ->withErrors([
                            $this->loginUsername() => $this->unConfirmedMessage($request),
                        ]);
                }
                $valid = new \App\Modules\Security\Services\Password\Validation;
                if ($valid->isPswdExpired(getUserExpiryDate())) {
                     Auth::logout();
                     return redirect($this->loginPath())
                    ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => $this->getFailedPswdExpiryMessage(),
                        'InvalidPswd' => true,
                        'token' => encodeToken($request->get($this->loginUsername()))
                    ]);
                }
                VCLogin::setLastDateLogged();                
                SystemLog('Authentication','','Log In','User log in','Authentication','' );
                return $this->handleUserWasAuthenticated($request, $throttles);
            }
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
                'InvalidPswd' => false,
                'token' => ''
            ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect($this->redirectPath());
    }

    protected function unConfirmedMessage(Request $request)
    {
        return  'These credentials should be confirmed first. Please check your email.'.
                ' <a href="javascript:;" data-c="'.encodeToken($request->get('Username')).'" id="ResendConfirmCode">Click here</a> to resend your confirmation code.';
    }

    /**
     * Get the password expiration message.
     *
     * @return string
     */
    protected function getFailedPswdExpiryMessage()
    {
        return 
            'For security purposes your account password must be change.
            Your Password is either already expired or a default password.';
    }


    protected function redirectPath() 
    {
        return '/security/validation';   
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
                ? Lang::get('auth.failed')
                : 'These credentials do not match our records.';
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }
}
