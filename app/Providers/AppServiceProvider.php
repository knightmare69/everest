<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->creator('Home', 'App\Modules\Home\Views');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->creator('Home', 'App\Modules\Home\Views');
    }
}
