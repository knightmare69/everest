<?php
		
	function permissionDenied()
	{
		return  ['error'=>true,'message'=>'Permission Denied!'];
	}

	function noEvent()
	{
		return ['error'=>true,'message'=>'No Event Selected'];
	}

	function successSave() {
		return ['error'=>false,'message'=>'<b>Success!</b>. Saved!'];
	}

	function errorSave() {
		return ['error'=>true,'message'=>'There was an error while saving.'];
	}

	function errorDelete() {
		return ['error'=>true,'message'=>'There was an error while deleting.'];
	}

	function successUpdate() {
		return ['error'=>false,'message'=>'<b>Success!</b>. Update!'];
	}

	function errorUpdate() {
		return ['error'=>true,'message'=>'There was an error while updating.'];
	}

	function successConnect() {
		return ['error'=>false,'message'=>'Successfully connected.'];
	}

	function errorConnect() {
		return ['error'=>true,'message'=>'There was an error while connecting.'];
	}

	function successDelete() {
		return ['error'=>false,'message'=>'Successfully deleted record.'];
	}

	function successConfirm() {
		return ['error'=>false,'message'=>'<b>Success!</b> Confirmed account!. <br> You can now proceed to login.'];
	}

	function alreadyConfirmCode() {
		return ['error'=>true,'message'=>'This code is already confirmed!'];
	}

	function errorConfirm() {
		return ['error'=>true,'message'=>'Invalid code.'];
	}

	function successSaveAdmission() {
		return ['error'=>false,'message'=>'<b>Success!</b>. Saved!'];
	}

	function errorSaveAdmission() {
		return ['error'=>true,'message'=>'There was an error while saving.'];
	}

	function errorAdmittedAlready() {
		return ['error'=>true,'message'=>'This Application is already admitted.'];
	}

    function system_message() {
		return ['error'=>true,'message'=>'This Application is already admitted.'];
	}
    
    


?>