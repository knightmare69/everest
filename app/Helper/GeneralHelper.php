<?php

	function getErrorMessages($errors)
	{
		$li="";
		$message='';
		if ( count($errors) > 1 )
		{
			foreach($errors as $row)
			{
				foreach($row as $key => $message)
				{
					$li.="<li>{$message}</li>";
				}

			}
			$message="<ul>{$li}</ul>";
		}
		else
		{
			foreach($errors as $key => $msg) { $message=$msg[0]; }
		}

		return $message;
	}

	function getModelMessage($error,$message=null)
	{
		$message=$message ? $message : '';

		if ( !$error )
		{
			return [ 'error' => $error,'message' => $message ];
		}

		return [ 'error' => $error,'message' => $message ];
	}

	function getActionMessage($error,$message='',$action='save')
	{
		switch (strtolower($action) ) {
			case 'delete':
				$action="Deleted!";
				break;
			case 'update':
				$action="Update!";
				break;
			default:
				$action="Save!";
				break;
		}
		if ( $error == false || $error == 0 || $error == '' )
		{
			$message = "Successfully ".$action;
		}
		else
		{
			$message =  ( $message == '' ) ? 'Could not '.($action == 'Deleted!' ? 'Delete' : $action) :  $message;
		}
		return $message;
	}

	function InObject($field,$key,$objects)
	{
		foreach($objects as $row)
		{

			if ( $row->$field == $key )
			{
				return true;
			}
		}
		return false;
	}

	function IsEqualTo($data,$field,$value,$loop = true)
	{
		if ( $loop )
		{
			foreach($data as $row)
			{
				if ( (is_object($data)) && (isset($row->$field) && ($row->$field == $value)) )
				{
					return true;
				}
				elseif ( (is_array($data)) && (isset($row[$field]) && $row[$field] == $value) )
				{
					return true;
				}
			}
		}
		else
		{
			if ( (is_object($data)) && (isset($data->$field) && ($data->$field == $value)) )
			{
				return true;
			}
			elseif ( (is_array($data)) && (isset($data[$field]) && $data[$field] == $value) )
			{
				return true;
			}
		}

		return false;
	}

	function trimmed($value, $type='')
	{
        $output = str_replace(array("'"),"",trim($value));
        switch($type){

            case 'i':
                $output = str_replace(",","",$output);
                if( is_numeric($output) )   $output = intval($output);

            break;

            case 'd':

                $output = str_replace(",","",$output);
                if( is_numeric($output) )   $output = floatval($output);

            break;
						case 'utf':
							$output = mb_convert_encoding($output,'UTF-8');
						break;


        }

		return $output;
	}

	function trimLength($value,$length = 500,$more = '')
	{
		$ret = substr($value,0,$length);
		return strlen($value) <= $length ? $ret : $ret.$more;
	}

	function decimal($value)
	{
		return str_replace(array(","),"",trim($value));
	}

	function getObjectValue($source,$field,$isTrimmed = false)
	{
		$value = '';
		if ( is_object($source) ) {
			// if (count($source) > 0) {
			// 	$value =  isset($source[0]->$field) ? $source[0]->$field : '';
			// } else {
				$value =  isset($source->$field) ? $source->$field : '';
			// }
		}
		elseif ( is_array($source) ) {
			// if (count($source) > 0) {
			// 	$value =  isset($source[0][$field]) ? $source[0][$field] : '';
			// } else {
				$value =  isset($source[$field]) ? $source[$field] : '';
			// }
		}
		else {
			$value =  isset($source->$field) ? $source->$field : '';
		}
		
		$value = str_replace("'","`",$value);
		return $isTrimmed ? trimmed($value) : $value;
	}

	function getObjectValueWithReturn($source,$field,$return)
	{
		if ( is_object($source) )
		{
			if( isset($source->$field) && $source->$field != '')
			{
				return $source->$field;
			}
		}
		elseif ( is_array($source) )
		{
			if( isset($source[$field]) && $source[$field] != '')
			{
				return $source[$field];
			}
		}

		return $return;
	}

	function getFirstLetter($words)
	{
		$words=explode(' ',$words);
		$letters='';
		foreach($words as $word)
		{
			$letters.=strtoupper(substr($word,0,1));
		}

		return $letters;
	}

	function getLetterEach($words,$l = 5)
	{
		$words=explode(' ',$words);
		$letters='';
		foreach($words as $word)
		{
			$letters.=strtoupper(substr($word,0,1));
		}

		return $letters;
	}

	function getGlobalForm()
	{
		$global  = "<button class='btn bg-purple-studio btn-sm' id='global-form'><i class='fa fa-folder'></i> Form</button>";
		$refresh = "<button class='btn bg-purple-plum btn-sm' id='global-refresh'><i class='fa fa-refresh'></i></button>";
		return $global.''.$refresh;
	}

	function convertArrayTo($data,$formatTo = '-',$fieldFormat = '_')
	{
		$ret = array();
		$cformat = '';
		foreach($data as $field)
		{
			switch (strtoupper($formatTo)) {
				case 'UCFIRST':
					$format   = explode($fieldFormat,$field);
					$cformat  = $field;
					if ( count($format) > 1 )
					{
						$cformat = '';
						foreach($format as $f){
							$cformat.=ucfirst(strtolower($f));
						}
					}
					$ret[] = $cformat;
					break;
				default:
					$ret[] = str_replace($fieldFormat, $formatTo, $field);
					break;
			}
		}
		return $ret;
	}

	function convertStrTo($string,$formatTo = '-',$fieldFormat = '_')
	{
		$ret = '';
		$cformat = '';

		switch (strtoupper($formatTo)) {
			case 'UCFIRST':
				$format   = explode($fieldFormat,$string);
				$cformat  = $string;
				if ( count($format) > 1 )
				{
					$cformat = '';
					foreach($format as $f){
						$cformat.=ucfirst(strtolower($f));
					}
				}
				$ret = $cformat;
				break;
			default:
				$ret = str_replace($fieldFormat, $formatTo, $string);
				break;
		}
		return $ret;
	}

	function getControllerName()
	{
		$url = config('app.host_type').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$split = explode('/',str_replace(url().'/', '', $url));
		return isset($split[count($split)-1]) ? $split[count($split)-1] : '';
	}

	function getUrlSegment()
	{
		$url = config('app.host_type').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$split = explode('/',str_replace(url().'/', '', $url));
		$segment = '';
		foreach($split as $key => $value){
			$segment.= $value.'/';
		}
		return substr($segment,0,-1);
	}

	function encode($value)
	{
		return base64_encode($value);
	}

	function decode($value)
	{
		return base64_decode($value);
	}

	function getSessionData($key)
	{
		return Session::get($key);
	}

	function putSessionData($key,$data)
	{
		return Session::put($key,$data);
	}

	function uploadTo($file,$destination)
	{
		move_uploaded_file($file, $destination);
	}

	function filesize_formatted($bytes)
	{
	    if ($bytes >= 1073741824) {
	        return number_format($bytes / 1073741824, 2) . ' GB';
	    } elseif ($bytes >= 1048576) {
	        return number_format($bytes / 1048576, 2) . ' MB';
	    } elseif ($bytes >= 1024) {
	        return number_format($bytes / 1024, 2) . ' KB';
	    } elseif ($bytes > 1) {
	        return $bytes . ' bytes';
	    } elseif ($bytes == 1) {
	        return '1 byte';
	    } else {
	        return '0 bytes';
	    }
	}

  	function error_print($input)
  	{
     	echo '<pre>';
      	print_r($input);
     	echo "</pre>";
  	}

  	function numberonly($value) {
  		return preg_replace("/['\"\' '\aA-zZ]/", '', $value);
  	}

  	function stringonly($value) {
  		return preg_replace("/(['\"\])([0-9])/", '', $value);
  	}

  	function ucFirstWord($words) {
  		$w = explode(' ',$words);$string = '';
  		for($i = 0;$i<count($w);$i++) {
  			$string .= ucfirst(strtolower($w[$i]));
		}
		return $string;
  	}

  	function getFileInfo($file,$isArray = false,$index = 0)
  	{
  		set_time_limit(0);
        ini_set('memory_limit', '-1');

  		if ($isArray) {
  			$attachment = $file['tmp_name'][$index];
			$file = $file['name'][$index];
  		} else {
  			$attachment = $file['tmp_name'];
			$file = $file['name'];
  		}

  		$data = [
  			'FileName' => $file,
  			'FileExtension' => isset(explode('.',$file)[1]) ? explode('.',$file)[1] : '',
  			'Attachment' => $attachment,
  			'FileType' => image_type_to_mime_type(exif_imagetype($attachment)),
  			'FileSize' => filesize($attachment)
  		];

  		set_time_limit(0);
        ini_set('memory_limit', '128M');

        return $data;
  	}

  	function setDateFormat($date,$givenFormatDate,$toDateFormat)
  	{
  		switch (strtolower($givenFormatDate)) {
  			case 'mm/dd/yyyy':
  					$date = explode(' ',$date)[0];
  					switch ($toDateFormat) {
  						case 'mm/dd/yyyy':
  							$date = $date;
  							break;
                        case 'yyyy-mm-dd':
  							$date = explode('/',$date);
  							$date = getObjectValue($date,2).'-'.getObjectValue($date,0).'-'.getObjectValue($date,1);
  							break;

  						default:
  							break;
  					}
  				break;
  			case 'dd/mm/yyyy':
  					$date = explode(' ',$date)[0];
  					switch ($toDateFormat) {
  						case 'yyyy-mm-dd':
  							$date = explode('/',$date);
  							$date = getObjectValue($date,2).'-'.getObjectValue($date,1).'-'.getObjectValue($date,0);
  							break;
  						default:
  							break;
  					}
  				break;
  			case 'yyyy-mm-dd':
  					$date = explode(' ',$date)[0];
  					switch (strtolower($toDateFormat)) {
  						case 'mm/dd/yyyy':
  							$date = explode('-',$date);
  							$date = getObjectValue($date,1).'/'.getObjectValue($date,2).'/'.getObjectValue($date,0);
  							break;
  						default:
  							break;
  					}
  				break;
  			default:
  				break;
  		}
  		if ($date == '//' || $date == '01/01/1900') {
  			return '';
  		}
  		return $date;
  	}

  	function assertModified($data)
	{
		$data['ModifiedDate'] = systemDate();
		$data['ModifiedBy'] = getUserID();

		return $data;
	}

	function assertDeleted($data)
	{
		$data['DeletedDate'] = systemDate();
		$data['DeletedBy'] = getUserID();

		return $data;
	}

	function assertCreated($data)
	{
		$data['CreatedDate'] = systemDate();
		$data['CreatedBy'] = getUserID();

		return $data;
	}

	function hasRef(&$ref,$data)
	{
		if (getObjectValue($data,$ref)) {
			$ref = getObjectValue($data,$ref);
			return true;
		}
		return false;
	}

	function getRawData($data)
	{
		if (isset($data[0])) {
			return $data[0];
		}
		return $data;
	}
    function setDate($date,$format)
  	{
        if(@strtotime($date)){
  		 $now = new DateTime($date);
  		 return $now->format($format);
  	    }else{
  	     return date($format,strtotime('01-01-1900'));	
  	    }
  	}
    function setDateTime($date, $format){
  		return date($format,strtotime($date));
    }

  	function DateDiff($dateFrom,$dateTo)
  	{
  		return
  		date_diff(
            date_create($dateFrom),
            date_create($dateTo)
        )
        ->format('%R%a');
  	}

  	function jsonToArray(&$data)
	{
		return $data = json_decode('['.$data.']',true);
	}

	function arrayToStr(&$data)
	{
		$str = '';
		foreach($data as $row) {
			$str.= $row.',';
		}

		return substr($str,0,-1);
	}

	function strToArray(&$data)
	{
		return explode(',',$data);
	}

    function DBresultToArr(&$data)
	{
		return json_decode(json_encode($data), true);
	}

    
    function convertNumberToWord($num = false)
	{
	    $num = str_replace(array(',', ' '), '' , trim($num));
	    if(! $num) {
	        return false;
	    }
	    $num = (int) $num;
	    $words = array();
	    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
	        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
	    );
	    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
	    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
	        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
	        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
	    );
	    $num_length = strlen($num);
	    $levels = (int) (($num_length + 2) / 3);
	    $max_length = $levels * 3;
	    $num = substr('00' . $num, -$max_length);
	    $num_levels = str_split($num, 3);
	    for ($i = 0; $i < count($num_levels); $i++) {
	        $levels--;
	        $hundreds = (int) ($num_levels[$i] / 100);
	        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
	        $tens = (int) ($num_levels[$i] % 100);
	        $singles = '';
	        if ( $tens < 20 ) {
	            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
	        } else {
	            $tens = (int)($tens / 10);
	            $tens = ' ' . $list2[$tens] . ' ';
	            $singles = (int) ($num_levels[$i] % 10);
	            $singles = ' ' . $list1[$singles] . ' ';
	        }
	        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
	    } //end for loop
	    $commas = count($words);
	    if ($commas > 1) {
	        $commas = $commas - 1;
	    }
	    return implode(' ', $words);
	}

	function getAge($birthDate) {
		//date in mm/dd/yyyy format; or it can be in other formats as well
	 	$birthDate = "04/24/2012";
	  	//explode the date to get month, day and year
	  	$birthDate = explode("/", $birthDate);
	  	//get age from date or birthdate
	  	$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
	    	? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
	    
	  	return $age;
	}
    

?>
