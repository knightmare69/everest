<?php

    function notification() {
        return new App\Notifications\Services;
    }

    function getSetting($setup)
    {
        $setting = DB::table('ES_GS_Setup')->select('Value')->where('SetupID',$setup)->first();
        return  isset($setting->Value) ? $setting->Value : '';
    }

    function getPeriodName($period) {
		return
			DB::table('ES_GS_GradingPeriods')
			->select('Description1')
			->where('PeriodID',$period)
			->pluck('Description1');

    }

    function systemDate($format = 'Y-m-d H:i:s')
    {
        return date($format);
    }

    function getAdminCode()
    {
        return 'ADMIN';
    }

    function pagePermission($page)
    {
       return new Permission($page);
    }

    function isPermissionHas($page,$action) 
    {
        $permission = new Permission($page); 
        return property_exists($permission, $action) ? $permission->$action : false;
        //return true;
    }
    /**
     * module permission
     *
     * @param string $module
     * @return class
     */
    function modulePermission($module_id) {

        $link = getUserGroupID();

        if($link == -1){
            $link = getUserID();
        }

        $output = true;

        $value = DB::table('access as a')
                    ->select('m.name')
                    ->leftJoin('pages as p','a.page_id','=','p.page_id')
                    ->leftJoin('modules as m','m.module_id','=','p.module_id')
                    ->where(['link_id' => $link , 'p.module_id' => $module_id ])
                    ->groupBy(['m.name'])
                    ->pluck('m.name');

        if( trimmed($value) == '' || $value == null || empty( $value ) ){
            $output = false;
        }

       return $output;
    }
    


 function getBrowser()
    {
        $ub='';
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Edge/i',$u_agent))
        {
            $bname = 'Edge';
            $ub = "Edge";
        }
        elseif(preg_match('/Firefox/i',$u_agent) && preg_match('/Gecko/i',$u_agent) )
        {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= isset($matches['version'][0]) ? $matches['version'][0] : '';
            }
            else {
                $version= isset($matches['version'][1]) ? $matches['version'][1] : '';
            }
        }
        else {
           $version= isset($matches['version'][0]) ? $matches['version'][0] : '';
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }



    define('token_length','20');

    function encodeToken($token) {
        if (!$token) return;
        $key = sha1('ab$6*1hVmkLd^0.');
        $code1 = substr($key,0,strlen($key)-constant('token_length'));
        $code2 = substr($key,strlen($key)-constant('token_length'),constant('token_length'));
        $key = $code1.$token.$code2.'|'.base64_encode(strlen($code2.$token));

        return base64_encode($key);
    }


    function decodeToken($token) {
        if (!$token) return;
        $token_ = base64_decode($token);
        $token_ = explode('|',$token_);
        if (count($token_ ) <= 1) {
            return '';
        }
        $tokenLeftLength = base64_decode($token_[1]);
        $token_ = $token_[0];

        $key = substr($token_, -$tokenLeftLength);

        return substr($key,0,strlen($key)-constant('token_length'));
    }

    function it($desc,$it) {
        $it();
    }

    function SystemLog($module, $controller, $method, $action = '', $parameter = '', $message = '')
    {
        $module = trimmed($module);
        $controller = trimmed($controller);
        $method = trimmed($method);
        $action = trimmed($action);

        $message = is_array($message) ? json_encode($message) : $message;


        if(is_array($parameter)){
            $parameter = json_encode($parameter) ;

            $parameter =  str_replace('"',' ',$parameter);
            $parameter =  str_replace('{','',$parameter);
            $parameter =  str_replace('}','',$parameter);
            $parameter =  str_replace(':','=',$parameter);
            $parameter =  str_replace('TermID','Term',$parameter);
            $parameter =  str_replace('CampusID','Campus',$parameter);
        }
        try {
          DB::table('auditrails')
            ->insert([
                'module' => $module,
                'controller' => $controller ,
                'method' => $method,
                'action' => $action,
                'parameter' => $parameter,
                'message' => $message,
                'browser' => json_encode(getBrowser()),
                'device' => gethostname(),
                'ip_address' => Request::ip(),
                'created_date' => systemDate(),
                'created_by' => getUserID()
            ]);

        } catch (Exception $e) {
           return $e;

        }


    }

    // Added by lloyd
    function getPic($type, $id)
    {
        $name = encode($id);
        $file_dir = public_path().'/images/'.$name.'.jpeg';

        switch ($type) {
            case 'student':
                $get = DB::table('ESv2_Admission')->select('Photo')->where('AppNo', $id)->first();
                $return = $get->Photo;
                break;

            default:
                $return = false;
                break;
        }

        if(empty($return)){
            $return = asset('assets/system/media/images/avatar.png');
        } else {
            $return = 'data:image/jpeg;base64,'.base64_encode(hex2bin($return));
        }
        echo $return;
    }

    function getLatestActiveTerm()
    {
        $active_term = DB::table('vw_K12_AcademicYears')
                        ->select('TermID')
                        ->where('Active_OnlineEnrolment', '1')
                        ->orderBy('TermID', 'DESC')->first();
        return $active_term->TermID;
    }

    function getActiveTermList()
    {
        $active_term = DB::table('vw_K12_AcademicYears')
                        ->select('TermID')
                        ->where('Active_OnlineEnrolment', '1')
                        ->get();
        return array_pluck($active_term, 'TermID');
    }
    // end

    function err_log($exception){
        Log::error($exception);
    }


     function getSystemlogo(){

        $path = './public/assets/system/media/images/logo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    function getExcelColbyInt($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return getExcelColbyInt($num2) . $letter;
        } else {
            return $letter;
        }
    }

    function getAYTerm($term = 0) {
        $rs = DB::table('ES_AYTerm')
			->selectRaw("TermID, AcademicYear + ' ' + SchoolTerm AS AYTerm, AcademicYear ")
			->where('Hidden','0');
         if($term != 0){
           return $rs->where('TermID', $term)->first();
         }else{
            return $rs->orderBy('AYTerm','DESC')->get();
        }
    }

     function getPaymentOptions($option = 0) {
        $rs = DB::table('ES_PaymentOptions')
			->selectRaw("PaymentOptionID, PaymentOptionName AS PayOption")
			->where('Inactive','0');
         if($option != 0){
           return $rs->where('PaymentOptionID', $option)->first();
         }else{
            return $rs->orderBy('SeqNo','ASC')->get();
        }
    }
  