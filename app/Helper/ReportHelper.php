<?php
	function jasperReport($filename, $params, $format){
		$jasper = new PHPJasper\PHPJasper;

		// $input=  base_path() . '/public/assets/system/jasper/'.$filename.'.jrxml';   
		// $jasper->compile($input)->execute();

		$input = base_path() . '/public/assets/system/jasper/'.$filename.'.jasper';
		$output = base_path() . '/public/assets/system/jasper/output';
		// dd($term);
		$options = [
		'format' => [$format],
		'locale' => 'en',
		'resources' => base_path() . '/public/assets/system/jasper', //place of resources
		'params' => $params,
		'db_connection' => [
		'driver' => 'mysql', //mysql, ....
		'username' => env('DB_USERNAME', 'root'),
		'password' => env('DB_PASSWORD', ''),
		'host' => env('DB_HOST', 'localhost'),
		'database' => env('DB_DATABASE', 'db_name'),
		'port' => 3306
		]

		];

		
		$jasper->process(
		$input,
		$output,
		$options
		)->execute();
		// dd($jasper);
		// error_log($jasper);
		$file ='/public/assets/system/jasper/output/'.$filename.'.'.$format;
		$file2 ='/assets/system/jasper/output/'.$filename.'.'.$format;
		$path= base_path() . $file;
		
		if($format == 'pdf' || $format == 'html'){
			header('Content-type: application/'.$format);
			header('Content-Disposition: inline; filename='.$filename.'.'.$format);
			header('Content-Transfer-Encoding: utf8');
			header('Accept-Ranges: bytes');
		}else{		
			header('Location: '.url($file2));
			die();

	
			
		}

	
		readfile($path);
		die();
	}
    
    function getInstInfo(){
		 $rs =  DB::table('es_institution')->first();
		 return $rs;
	}
    
 function computeMeritDemerit_2($termid, $studentno, $period, $section ){

	$att = new App\Modules\ClassRecords\Models\Attendance\Attendance;
	$deport = new App\Modules\ClassRecords\Models\Deportment;
        $letter = 'A';
                      
                    switch($period){
                        case 1: case 11: $letter="A";  break;
                        case 2: case 12: $letter="B"; break;
                        case 3:  $letter="C"; break;
                        case 4:  $letter="D"; break;
                    }
        $where_md = [
            'StudentNo' => $studentno,
            'PeriodID' => $period,                          
            'SectionID' => $section,
        ];

        $where_jayz =[
            'PeriodID' => $period,
            'StudentNo' => $studentno,
            'TermID' => $termid,
        ];

        $total =[
            'au'=> 0,
            'tu' => 0,
            'ae' => 0,
            'te' =>0,
        ];
        $total['au'] = $att->where($where_jayz)->where('IsExcused', 0)->where('Type',1)->count();
        $total['tu'] = $att->where($where_jayz)->where('IsExcused', 0)->where('Type',2)->count();
        $total['te'] = $att->where($where_jayz)->where('IsExcused', 1)->where('Type',2)->count();
        $total['ae'] = $att->where($where_jayz)->where('IsExcused', 1)->where('Type',1)->count();
        
        $total['au'] = $total['au'] /2;
        $total['tu'] = $total['tu'] /2;
        $data_md = array(
            'StudentNo' => $studentno,
            'TermID' => $termid,
            'PeriodID' =>$period,
            'SectionID' => $section
        );
        $where_md['DeportmentID'] = 5;
        $data_md['DeportmentID'] = 5;
        $au = $deport->where($where_md)->first();
        if($au){
            $deport->where($where_md)->update(['Score' => $total['au'] ]);
        }else{
            $data_md['FacultyID'] = getUserFacultyID();
            $data_md['Createdby'] = getUserID();
            $data_md['DateCreated'] = date('Y-m-d H:i:s');
            $deport->create($data_md);
        }

        $where_md['DeportmentID'] = 6;
        $data_md['DeportmentID'] = 6;
        $au = $deport->where($where_md)->first();
        if($au){
            $deport->where($where_md)->update(['Score' => $total['tu'] ]);
        }else{
            $data_md['FacultyID'] = getUserFacultyID();
            $data_md['Createdby'] = getUserID();
            $data_md['DateCreated'] = date('Y-m-d H:i:s');
            $deport->create($data_md);
        }
       
        
        DB::table('es_permanentrecord_master')
        ->where('TermID', $termid )
        ->where('StudentNo', $studentno )
        ->update([
            'Tardiness'.$letter => $total['te']  ,
            'Absent'.$letter => $total['ae'] ,
            ]);  
	} 
	
function getAcademicHonors($term, $period, $yearlevel, $progid, $strand, $campus){

	if(isSHS($progid)){
		$wHighestHonors = [
			'min_ga' => 98,
			'min_subj' => 92,
			'min_con' => 90
		];
	
		$wHighHonors = [
			'min_ga' => 95,
			'min_subj' => 90,
			'min_con' => 90
		];
		$wHonors = [
			'min_ga' => 90,
			'min_subj' => 88,
			'min_con' => 88
		];
		$acadDis = [
			'min_ga' => 89,
			'min_subj' => 85,
			'min_con' => 85
		];

	}else if ($progid == 2 || $progid == 5){

		$wHighestHonors = [
			'min_ga' => 97.5,
			'min_subj' => 80,
			'min_con' => 80
		];
	
		$wHighHonors = [
			'min_ga' => 93.5,
			'min_subj' => 80,
			'min_con' => 80
		];
		$wHonors = [
			'min_ga' => 89.5,
			'min_subj' => 80,
			'min_con' => 80
		];
		$acadDis = [
			'min_ga' => 100,
			'min_subj' => 100,
			'min_con' => 100
		];

	}else{
		$wHighestHonors = [
			'min_ga' => 98,
			'min_subj' => 85,
			'min_con' => 88
		];
	
		$wHighHonors = [
			'min_ga' => 95,
			'min_subj' => 85,
			'min_con' => 85
		];
		$wHonors = [
			'min_ga' => 90,
			'min_subj' => 83,
			'min_con' => 85
		];
		$acadDis = [
			'min_ga' => 86,
			'min_subj' => 83,
			'min_con' => 82
		];

	}

	

	$per = 'A';	
	switch($period){
			   case 1:case 11: $per = 'A';break;
			   case 2:case 12: $per =  'B';break;
			   case 3:  $per =  'C';break;
			   case 4: $per =  'D';break;
		   }

	$whereRaw = "pm.CampusID = '{$campus}' and  pm.StudentNo in (SELECT reg.StudentNo FROM es_registrations as reg where reg.TermID = ".$term." and reg.ValidationDate is not null and reg.ClassSectionID = pm.ClassSectionID) ";
	if(isSHS($progid)){
		$whereRaw .= "AND ClassSectionName like '%".$strand."%'";
	}
	$get  = DB::table('es_permanentrecord_master as pm')
	->selectRaw("
	   SummaryID,
	   fn_StudentName(pm.StudentNo) as `Name`,
	   pm.StudentNo,
	   pm.TermID,
	   fn_AcademicYear(pm.TermID) as SchoolYear,
	   fn_SectionAdviser(pm.ClassSectionID) as Adviser,
	   fn_K12_YearLevel(pm.YearLevelID) as `GradeLevel`,
	   fn_SectionName(pm.ClassSectionID) as `Section`,
	   fn_K12_YearLevel(YearLevelID) as Level,
	   pm.ClassSectionID as SectionID,
	   pm.YearLevelID,
	   fn_ProgramName2(pm.ProgramID) as Program,
	   case when pm.Academic".$per."_Average >= ".$wHighestHonors['min_ga']."
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where pd2.Average".$per." < ".$wHighestHonors['min_subj']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Average".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where  pd2.Conduct".$per." < ".$wHighestHonors['min_con']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Conduct".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   then 'Yes' else '' end as `wHighestHonors`,
	   

	   case when pm.Academic".$per."_Average >= ".$wHighHonors['min_ga']."
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where pd2.Average".$per." < ".$wHighHonors['min_subj']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Average".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where  pd2.Conduct".$per." < ".$wHighHonors['min_con']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Conduct".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   then 'Yes' else '' end as `wHighHonors`,



	   case when pm.Academic".$per."_Average >= ".$wHonors['min_ga']."
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where pd2.Average".$per." < ".$wHonors['min_subj']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Average".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where  pd2.Conduct".$per." < ".$wHonors['min_con']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Conduct".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   then 'Yes' else '' end as `wHonors`,


	   case when pm.Academic".$per."_Average >= ".$acadDis['min_ga']."
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where pd2.Average".$per." < ".$acadDis['min_subj']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Average".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   and (select count(pd2.StudentNo) from es_permanentrecord_details as pd2 where  pd2.Conduct".$per." < ".$acadDis['min_con']." and pd2.StudentNo = pm.StudentNo and pd2.SummaryID = pm.SummaryID and pd2.Conduct".$per." > 0 and pd2.ScheduleID in (select rd.ScheduleID from es_registrationdetails as rd where rd.RegID = pd2.RegID)   ) = 0
	   then 'Yes' else '' end as `acadDis`,

	   RANK() OVER (PARTITION BY
                   ClassSectionID
                 ORDER BY
				 	pm.Academic".$per."_Average DESC
				) `SectionRank`,

				RANK() OVER (
					ORDER BY
					pm.Academic".$per."_Average DESC
				)  `GradeRank`,
			

	   pm.Academic".$per."_Average as `QtrAve`,
	   pm.Conduct".$per."_Average as `QtrConAve`

	")
	->where('pm.TermID', $term )
	->where('pm.YearLevelID', $yearlevel)
	->whereRaw($whereRaw)

	->orderBy('Section', 'ASC')
	->orderBy('Name', 'ASC')
	->get();

 return $get;
}


function array_group_by(array $array, $key)
{
	if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
		trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
		return null;
	}

	$func = (!is_string($key) && is_callable($key) ? $key : null);
	$_key = $key;

	// Load the new array, splitting by the target key
	$grouped = [];
	foreach ($array as $value) {
		$key = null;

		if (is_callable($func)) {
			$key = call_user_func($func, $value);
		} elseif (is_object($value) && isset($value->{$_key})) {
			$key = $value->{$_key};
		} elseif (isset($value[$_key])) {
			$key = $value[$_key];
		}

		if ($key === null) {
			continue;
		}

		$grouped[$key][] = $value;
	}

	// Recursively build a nested grouping if more parameters are supplied
	// Each grouped array value is grouped according to the next sequential key
	if (func_num_args() > 2) {
		$args = func_get_args();

		foreach ($grouped as $key => $value) {
			$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
			$grouped[$key] = call_user_func_array('array_group_by', $params);
		}
	}

	return $grouped;
}

function reportcard_details_hchs($termid, $yearlevelid, $section_id, $studentno, $is_sec)
{
	$whereRaw = '';
	$whereRaw = "sj.IsChinese = {$is_sec} and reg.TermID = '{$termid}'
							AND reg.StudentNo = CASE WHEN {$studentno} = 0
																			 THEN reg.StudentNo
																			 ELSE {$studentno}
																	END";
	if($is_sec  == 0){
		$whereRaw .= " AND  reg.YearLevelID = CASE WHEN {$yearlevelid} = 0
																					 THEN reg.YearLevelID
																					 ELSE {$yearlevelid}
																			END
 AND reg.ClassSectionID = CASE WHEN {$section_id} = 0
																		THEN reg.ClassSectionID
																		ELSE {$section_id}
															 END";
	}else{
		$whereRaw .= "  AND reg.ClassSectionID_2 = CASE WHEN {$section_id} = 0
																		THEN reg.ClassSectionID_2
																		ELSE {$section_id}
															 END";
	}

	$rs = DB::table("es_registrations as reg")
									->selectRaw(" fn_AcademicYear(reg.TermID) AS SchoolYear , reg.ProgID ,
									reg.TermID ,
									pd.GradeIDX,
									reg.YearLevelID ,
									s.LRN ,
									reg.StudentNo ,
									fn_Age(s.DateofBirth,now()) as Age,
									s.Fullname AS StudentName ,
									s.ChineseName,
									c.SectionID,
									reg.ClassSectionID_2 as SectionID2,
									c.Is2ndSection,
									sj.IsChinese,
									sj.ChineseSubject,
									sj.LetterGradeOnly,
									sj.Weight,
									c.SectionName AS SectionName,
									(select SectionName_2 from es_classsections where SectionID = reg.ClassSectionID_2) as SectionName2,
									fn_EmployeeName3(c.AdviserID) AS Adviser ,
									pl.Principal AS Principal ,
									sj.SubjectID,
									sj.SubjectTitle ,
									(select count(studentno) from es_registrations where ClassSectionID = c.SectionID) as totalstudents,
									cs.SectionID AS SubjectSectionID,
									reg.ClassSectionID AS RegSectionID,
									fn_SubjectSortOrder2(cs.SubjectID, c.CurriculumID) AS SeqNo,
									CASE WHEN IFNULL(pd.DatePosted1, '') = '' or sj.IsSemestral=1 THEN ''
									ELSE pd.AverageA
									END AS AverageA,
									fn_gs_gradescriptor(1, pd.AverageA) AS LetterA,
									CASE WHEN IfNULL(pd.DatePosted2, '') = '' THEN ''
									ELSE pd.AverageB
									END AS AverageB ,
									fn_gs_gradescriptor(1, pd.AverageB) AS LetterB,
									CASE WHEN IfNULL(pd.DatePosted3, '') = ''  or sj.IsSemestral=1 THEN ''
									ELSE pd.AverageC
									END AS AverageC ,
									fn_gs_gradescriptor(1, pd.AverageC) AS LetterC,
									CASE WHEN IfNULL(pd.DatePosted4, '') = '' THEN ''
									ELSE pd.AverageD
									END AS AverageD ,
									fn_gs_gradescriptor(1, pd.AverageD) AS LetterD,
									pd.TotalAverageAB ,
									pd.TotalAverageCD ,
									pd.Final_Average ,
									pd.Final_Remarks ,
									pd.ConductA ,
									pd.ConductB ,
									pd.ConductC ,
									pd.ConductD ,
									pd.Conduct_FinalAverage ,
									pd.Conduct_FinalRemarks ,
									sd.JUN ,
									sd.JUL ,
									sd.AUG ,
									sd.SEP ,
									sd.OCT ,
									sd.NOV ,
									sd.DEC ,
									sd.JAN,
									sd.FEB ,
									sd.MAR ,
									sd.APR ,
									sd.MAY ,
									sd.TOTAL AS DOS ,
									fn_countOffenses(reg.TermID, reg.StudentNo, 1, 6) AS Absent_JUN,
									fn_countOffenses(reg.TermID, reg.StudentNo, 1, 6) AS Absent_JUN ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 7) AS Absent_JUL ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 8) AS Absent_AUG ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 9) AS Absent_SEP ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 10) AS Absent_OCT ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 11) AS Absent_NOV ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 12) AS Absent_DEC ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 1) AS Absent_JAN ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 2) AS Absent_FEB ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 3) AS Absent_MAR ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 1, 4) AS Absent_APR ,
							fn_countOffenses(reg.TermID, reg.StudentNo, 1, 5) AS Absent_MAY ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 6) AS Tardy_JUN ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 7) AS Tardy_JUL ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 8) AS Tardy_AUG ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 9) AS Tardy_SEP ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 10) AS Tardy_OCT ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 11) AS Tardy_NOV ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 12) AS Tardy_DEC ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 1) AS Tardy_JAN ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 2) AS Tardy_FEB ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 3) AS Tardy_MAR ,
						 fn_countOffenses(reg.TermID, reg.StudentNo, 2, 4) AS Tardy_APR ,
								fn_countOffenses(reg.TermID, reg.StudentNo, 2, 5) AS Tardy_MAY ,

									IfNULL(sj.SubjParentID, 0) AS ParentSubjectID ,
									IfNULL(sp.SubjParentID,0) AS IsParentSubjectID,
									fn_ProgramClassCode(reg.ProgID) as ProgClass,
									sj.LetterGradeOnly,
									fn_IsTotalDroppedOut(reg.RegID) AS IsDroppedOut,
									(SELECT FilingDate FROM es_withdrawaldropout WHERE RegID = reg.RegID  limit 1) AS DateWithdrawn,
									(Select Final_Average from es_permanentrecord_master where RegID= reg.RegID and TermID = reg.TermID AND StudentNo=reg.StudentNo) As FinalAverage,
									fn_gs_gradescriptor(1, pd.Final_Average)  AS FinalLetter
										")
									->leftJoin('es_registrationdetails as rd','rd.RegID','=','reg.RegID')
									->leftJoin('es_classschedules as cs','rd.ScheduleID','=','cs.ScheduleID')
									->leftJoin('es_classsections as c','reg.ClassSectionID','=','c.SectionID')
									->leftJoin('es_subjects as sj','sj.SubjectID','=','cs.SubjectID')
									->leftJoin('es_students as s','reg.StudentNo','=','s.StudentNo')
									->leftJoin('es_principal as pl','pl.TermID','=','reg.TermID')
									->leftJoin('es_permanentrecord_schooldays as sd',function($join)
									{
										$join->on('reg.TermID','=','sd.TermID');
										$join->where('sd.SchoolID', '=',0);
									})
									->leftJoin('es_permanentrecord_details as pd',function($join)
									{
										$join->on('reg.StudentNo','=','pd.StudentNo');
										$join->on('pd.TermID', '=', 'reg.TermID');
										$join->on('rd.ScheduleID', '=', 'pd.ScheduleID');
									})
									->leftJoin(DB::raw("(SELECT SubjParentID FROM es_subjects WHERE IfNULL(SubjParentID,0) > 0 GROUP BY SubjParentID )  as sp ") , function($join)
									{
										$join->on('sp.SubjParentID', '=', 'sj.SubjectID');
									})
									->whereRaw($whereRaw)
									->orderBy('SeqNo')
									// ->limit(20)
									->get();
		return $rs;
}

function reportcard_details($termid, $yearlevelid, $section_id, $studentno, $period, $is_sec)
{

	// dd($period);
	$eval =  new App\Modules\Registrar\Services\EvaluationServices;
					$whereRaw = "sj.IsChinese = 0 and reg.TermID = '{$termid}' AND  reg.YearLevelID = CASE WHEN {$yearlevelid} = 0
					THEN reg.YearLevelID
					ELSE {$yearlevelid}
				END
				AND reg.ClassSectionID = CASE WHEN {$section_id} = 0
					THEN reg.ClassSectionID
					ELSE {$section_id}
				END
				AND reg.StudentNo = CASE WHEN '{$studentno}' = '0'
				THEN reg.StudentNo
				ELSE '{$studentno}'
				END";
				$whereRaw2 = "reg.TermID = '{$termid}' AND  reg.YearLevelID = CASE WHEN {$yearlevelid} = 0
					THEN reg.YearLevelID
					ELSE {$yearlevelid}
				END
				AND reg.ClassSectionID = CASE WHEN {$section_id} = 0
					THEN reg.ClassSectionID
					ELSE {$section_id}
				END
				AND reg.StudentNo = CASE WHEN '{$studentno}' = 0
				THEN reg.StudentNo
				ELSE '{$studentno}'
				END";

				$data_jay = DB::table("es_registrations as reg")->whereRaw($whereRaw2)->get();    

				foreach ($data_jay as $v) {
				$eval->computeQuarterAve( $v->TermID, $v->StudentNo, $period );
				// if($period == 2){
				// $eval->computeQuarterAve( $v->TermID, $v->StudentNo, 1);
				// }
				}         
				$rs = DB::table("es_registrations as reg")
				->selectRaw(" fn_AcademicYear(reg.TermID) AS SchoolYear , reg.ProgID ,
				reg.TermID ,
				reg.YearLevelID ,
				fn_AcademicSchoolTerm(reg.TermID) as SchoolTerm,
				s.LRN ,
				fn_ProgramName2(reg.ProgID) as ProgramName,
				s.Gender,
				s.LastName,
				s.FirstName,
				s.MiddleInitial,
				reg.StudentNo ,
				fn_K12_YearLevel(reg.YearLevelID) as Level,
				fn_K12_YearLevel(reg.YearLevelID+1) as NextLevel,
				fn_Age(s.DateofBirth,now()) as Age,
				fn_StudentName3(s.StudentNo) AS StudentName ,
				s.ChineseName,
				c.SectionID,
				reg.ClassSectionID_2 as SectionID2,
				c.Is2ndSection,
				pd.ShowOnReport,
				sj.IsChinese,
				sj.ChineseSubject,
				sj.Weight,
				sj.LetterGradeOnly,
				c.SectionName AS SectionName,
				fn_EmployeeName4(c.AdviserID) AS Adviser ,
				(select SectionName_2 from es_classsections where SectionID = reg.ClassSectionID_2) as SectionName2,
				pl.Principal AS Principal ,
				sj.SubjectID,
				sj.SubjectTitle ,
				sj.SubjectCode,
				(select count(studentno) from es_registrations where ClassSectionID = c.SectionID) as totalstudents,
				cs.SectionID AS SubjectSectionID,
				reg.ClassSectionID AS SectionID,
				fn_SubjectSortOrder2(sj.SubjectID, c.CurriculumID) AS SeqNo,
				CASE WHEN IFNULL(pd.DatePosted1, '') = '' or sj.IsSemestral=1 THEN ''
				ELSE pd.AverageA
				END AS AverageA,
				fn_gs_gradescriptor(1, pd.AverageA) AS LetterA,
				CASE WHEN IfNULL(pd.DatePosted2, '') = '' THEN ''
				ELSE pd.AverageB
				END AS AverageB ,
				fn_gs_gradescriptor(1, pd.AverageB) AS LetterB,
				CASE WHEN IfNULL(pd.DatePosted3, '') = ''  or sj.IsSemestral=1 THEN ''
				ELSE pd.AverageC
				END AS AverageC ,
				fn_gs_gradescriptor(1, pd.AverageC) AS LetterC,
				CASE WHEN IfNULL(pd.DatePosted4, '') = '' THEN ''
				ELSE pd.AverageD
				END AS AverageD ,
				fn_gs_gradescriptor(1, pd.AverageD) AS LetterD,
				pd.TotalAverageAB ,
				pd.TotalAverageCD ,
				pd.Final_Average as Subj_Average,
				pd.Final_Remarks as Subj_Remarks,
				pd.ConductA ,
				pd.ConductB ,
				pd.ConductC ,
				pd.ConductD ,
				pd.Conduct_FinalAverage ,
				pd.Conduct_FinalRemarks ,
				sd.JUN ,
				sd.JUL ,
				sd.AUG ,
				sd.SEP ,
				sd.OCT ,
				sd.NOV ,
				sd.DEC ,
				sd.JAN,
				sd.FEB ,
				sd.MAR ,
				sd.APR ,
				sd.MAY ,
				sd.TOTAL AS DOS ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 6) AS Absent_JUN,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 6) AS Absent_JUN ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 7) AS Absent_JUL ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 8) AS Absent_AUG ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 9) AS Absent_SEP ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 10) AS Absent_OCT ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 11) AS Absent_NOV ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 12) AS Absent_DEC ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 1) AS Absent_JAN ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 2) AS Absent_FEB ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 3) AS Absent_MAR ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 4) AS Absent_APR ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 1, 5) AS Absent_MAY ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 6) AS Tardy_JUN ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 7) AS Tardy_JUL ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 8) AS Tardy_AUG ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 9) AS Tardy_SEP ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 10) AS Tardy_OCT ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 11) AS Tardy_NOV ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 12) AS Tardy_DEC ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 1) AS Tardy_JAN ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 2) AS Tardy_FEB ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 3) AS Tardy_MAR ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 4) AS Tardy_APR ,
				fn_countOffenses(reg.TermID, reg.StudentNo, 2, 5) AS Tardy_MAY ,
				IfNULL(sj.SubjParentID, 0) AS ParentSubjectID ,
				IfNULL(sp.SubjParentID,0) AS IsParentSubjectID,
				IfNULL(sj.SubjGroupID, 0) as SubjGroupID,
				fn_ProgramClassCode(reg.ProgID) as ProgClass,
				sj.LetterGradeOnly,
				fn_IsTotalDroppedOut(reg.RegID) AS IsDroppedOut,
				(SELECT FilingDate FROM es_withdrawaldropout WHERE RegID = reg.RegID  limit 1) AS DateWithdrawn,
				pm.Final_Average,
				CASE WHEN IFNULL(pd.DatePosted1, '') = '' or sj.IsSemestral=1 THEN ''
				ELSE  pm.AcademicA_Average
				END AS AcademicA_Average,
				CASE WHEN IfNULL(pd.DatePosted2, '') = '' THEN ''
				ELSE  pm.AcademicB_Average
				END AS AcademicB_Average,



				pm.AcademicC_Average,
				pm.AcademicD_Average,


				CASE WHEN IFNULL(pd.DatePosted1, '') = '' or sj.IsSemestral=1 THEN ''
				ELSE  pm.ConductA_Average
				END AS ConductA_Average,

				pm.ConductB_Average,
				pm.ConductC_Average,
				pm.ConductD_Average,
				pm.AbsentA,
				pm.AbsentB,
				pm.AbsentC,
				pm.AbsentD,
				pm.TardinessA,
				pm.TardinessB,
				pm.TardinessC,
				pm.TardinessD,
				pm.Final_Remarks,
				pm.Final_Conduct,

				fn_gs_gradescriptor(1, pd.Final_Average)  AS FinalLetter
				")
				->leftJoin('es_registrationdetails as rd','rd.RegID','=','reg.RegID')
				->leftJoin('es_classschedules as cs','rd.ScheduleID','=','cs.ScheduleID')
				->leftJoin('es_classsections as c','reg.ClassSectionID','=','c.SectionID')
				->leftJoin('es_subjects as sj','sj.SubjectID','=','cs.SubjectID')
				->leftJoin('es_students as s','reg.StudentNo','=','s.StudentNo')
				->leftJoin('es_principal as pl','pl.TermID','=','reg.TermID')

				->leftJoin('es_permanentrecord_schooldays as sd',function($join)
				{
				$join->on('reg.TermID','=','sd.TermID');
				$join->where('sd.SchoolID', '=',0);
				})
				->leftJoin('es_permanentrecord_details as pd',function($join)
				{
				$join->on('reg.StudentNo','=','pd.StudentNo');
				$join->on('pd.TermID', '=', 'reg.TermID');
				$join->on('rd.ScheduleID', '=', 'pd.ScheduleID');
				})
				->leftJoin('es_permanentrecord_master as pm', 'pm.SummaryID', '=','pd.SummaryID')
				->leftJoin(DB::raw("(SELECT SubjParentID FROM es_subjects WHERE IfNULL(SubjParentID,0) > 0 GROUP BY SubjParentID )  as sp ") , function($join)
				{
				$join->on('sp.SubjParentID', '=', 'sj.SubjectID');
				})
				->whereRaw($whereRaw)
				->orderBy("StudentName")
				->orderBy('SubjGroupID')
				->orderBy('SeqNo')
				->get();
		return $rs;
}


function getEquivalentGrade($grade, $lettergrade)
{
	$average= '';
	if($lettergrade == 1){
		$grade  = round($grade);
		if($grade >=95){
			$average = 'A+';
		}else if ($grade >= 92 && $grade <= 94){
			$average = 'A';
		}else if ($grade >= 89.00 && $grade <= 91.00){
			$average = 'A-';
		}else if ($grade >= 86.00 && $grade <= 88.00){
			$average = 'B+';
		}else if ($grade >= 83 && $grade <= 85.00){
			$average = 'B';
		}else if ($grade >= 80 && $grade <= 82){
			$average = 'B-';
		}else if ($grade >= 77 && $grade <= 79){
			$average = 'C+';
		}else if ($grade >= 75 && $grade <= 76){
			$average = 'C';
		}else if ( $grade >= 2 && $grade <= 74){
			$average = 'C-';
		}else if($grade <= 1){
			$average= '';
		}
	}else{
	$average = 	$grade > 0 ? round($grade) : $grade; ;
	}
	return $average;

}

function getReportCardRemarks($grade){
	if ($grade >= 75){
		return 'PASSED';
	}else if ($grade >= 0 && $grade < 75){
		return 'FAILED';
	}else{
		return '';
	}
}

function get_dayth($day){
    $suffix = '';
    if($day !=''){
        $d = substr($day,-1);
        
        switch($d){
            case '1':
            
                if($day != '11')
                    $suffix = 'th';
                else
                    $suffix = 'st';                    
                 
            break;
            case '2': 

                if($day != '12')
                    $suffix = 'th';
                else
                    $suffix = 'nd';                    
            
            break;
            case '3': 
                if($day != '13')
                    $suffix = 'th';
                else
                    $suffix = 'rd';              
            break;
            default: $suffix = 'th'; break;
        }
            
    }
    return $day.$suffix;    
}

function getPeriodDesc($term, $per){
	$desc = '';
	$schoolterm = DB::table('es_ayterm')->where('TermID', $term)->value('SchoolTerm');

	$period = DB::table('es_gs_gradingperiods')->where('PeriodID', $per)->first();
	
	if($schoolterm == '2nd Semester'){
		$desc = getObjectvalue($period, 'Description2');
	}else{
		$desc = getObjectvalue($period, 'Description1');
	}

	if($term == 0){
		$desc =  getObjectvalue($period, 'Description1');
	}
	return $desc;
}

	function getInstitutionInfo() {
		return DB::table('es_institution as inst')
			->selectRaw("inst.*, t.pri_font_color,t.pri_bg_color,t.sec_font_color,t.sec_bg_color, email_bg_color")
			->leftJoin('theme as t','t.institution_id', '=', 'inst.InstID')
		->first();
	}