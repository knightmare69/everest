<?php

    function hasAuthenticated() {
        $check = Auth::check();
		if(!$check) {
    		redirect('auth/login');
            die();
		}
	}

    function isGuest() {
        return Auth::guest();
    }

	function getUserName() {
		return ((Auth::user())? (Auth::user()->Username):'');
	}

	function getUserID() {
	    hasAuthenticated();
		return Auth::user()->UserIDX;
	}
	
	function isFamilyUpdated(){
	   hasAuthenticated();
	   $uid = Auth::user()->UserIDX;
	   $qry = "SELECT (CASE WHEN ISNULL(Guardian_Name,'')=''
					  OR  ISNULL(Guardian_ParentMaritalID,0)=0
					  OR  ISNULL(Guardian_Address,'')=''
					  OR  (CASE WHEN Guardian_IsOtherCity=1 THEN ISNULL(Guardian_OtherCity,'') ELSE ISNULL(Guardian_TownCity,'') END)=''
					  OR  ISNULL(Guardian_Email,'')=''
					  OR  ISNULL(Guardian_Mobile,'')=''
					  
					  OR  ISNULL(Father_Name,'')=''
					  OR  ISNULL(Father_Email,'')=''
					  OR  ISNULL(Father_Mobile,'')=''
					  
					  OR  ISNULL(Mother_Name,'')=''
					  OR  ISNULL(Mother_Email,'')=''
					  OR  ISNULL(Mother_Mobile,'')=''
					  
					  OR  ISNULL(EmergencyContact_Name,'')=''
					  OR  ISNULL(EmergencyContact_Relationship,'')=''
					  OR  ISNULL(EmergencyContact_Mobile,'')=''
					  
					  OR  ISNULL(DataPrivacy,'')='' THEN 0 ELSE 1 END) as IxSet
					 ,fb.FamilyID 
				 FROM ESv2_Admission_FamilyBackground as fb
		   INNER JOIN ESv2_Users as u ON fb.FamilyID=u.FamilyID WHERE UserIDX='".$uid."'";
	   $exec = DB::SELECT($qry);
       if($exec && count($exec)>0){
	     return $exec[0]->IxSet;
	   }else{
	     return 0;
	   }
	}
	
	function getFamilyID() {
	    hasAuthenticated();
		return Auth::user()->FamilyID;
	}

    /*Added : Allan Robert B. Sanchez Date: Sept. 16, 2016 12:44H */
    function getUserID2() {
	    hasAuthenticated();
		return Auth::user()->UserID;
	}

	function getUserFullName() {
        if(isStudents()){
            return strtoupper(DB::table('ESv2_Users as u')
                	->selectRaw('dbo.fn_StudentName(UserID) AS StudentName')
                	->leftJoin('ESv2_UsersGroup as g','g.GroupID','=','u.UsersGroupID')
                	->where('u.UserIDX',getUserID())
                	->pluck('StudentName'));        
        }else{
            return Auth::user()->FullName;    
        }
		
	}

    function getUserFacultyID() {
		/*Added : Allan Robert B. Sanchez Date: June 9, 2016 15:38H */
		$fac_id = Auth::user()->FacultyID;
		return trim($fac_id);
	}

	function getUserPosition() {
	   
        if(isStudents()){
            return 'Student';
        }else{
            return App\Modules\setup\Models\Position::select('PositionDesc')
    			->where('PosnTitleID',Auth::user()->PositionID)
    			->pluck('PositionDesc');    
        }       		
	}

	function getProfilePic() {
		return Auth::user()->Photo;
	}

	function getHashUserID() {
		$id = md5(getUserID());
		return $id;
	}

	function getUserEmail() {
		return Auth::user()->Email;
	}

	function getUserDepartment() {
		return Auth::user()->DepartmentID;
	}

	function getUserDepartmentID() {
		return Auth::user()->DepartmentID;
	}

	function updateLastLogin() {
        // DB::statement("update ESv2_Users set LastDateLogin='".systemDate()."' where UserIDX=".getUserID());
    }

    function getParentGroupID() {
    	return DB::table('ESv2_UsersGroup')->select('GroupID')->where('GroupName','Parent')->pluck('GroupID');
    }

    function getStudentsGroupID() {
    	return DB::table('ESv2_UsersGroup')->select('GroupID')->where('GroupCode','students')->pluck('GroupID');
    }


    function getUserGroup() {
    	return
    	strtolower(DB::table('ESv2_Users as u')
    	->select('g.GroupName')
    	->leftJoin('ESv2_UsersGroup as g','g.GroupID','=','u.UsersGroupID')
    	->where('u.UserIDX',getUserID())
    	->pluck('GroupName'));
    }
    
    function getFamilyIDByAppNo($AppNo) {
    	return DB::table('ESv2_Admission')->select('FamilyID')->where('AppNo',$AppNo)->pluck('FamilyID');
    }

	function chkAppEntry(){
	  $qry = "SELECT i.id as InquiryID, ISNULL(a.AppNo,'') as AppNo,ISNULL(a.StatusID,0) as StatusID,u.FamilyID 
	                 FROM es_inquiry as i
			   INNER JOIN es_inquiry_parent as ip ON i.InquiryParentID=ip.id
			   INNER JOIN ESv2_Users as u ON ip.email=u.Email
			   INNER JOIN ESv2_Admission as a ON ip.FamilyID=a.FamilyID AND i.AppNo=a.AppNo AND a.StatusID NOT IN (2,3)
					WHERE u.UserIDX='".getUserID()."'";
	  $exec = DB::select($qry);
      if($exec && count($exec)>0){
	    return $exec;
	  }else{
	    return false;
	  }	  
	}
	
    function chkInquiryEntry(){
	  $qry = "SELECT i.id as InquiryID, ISNULL(a.AppNo,'') as AppNo,ISNULL(a.StatusID,0) as StatusID,u.FamilyID 
	                 FROM es_inquiry as i
			   INNER JOIN es_inquiry_parent as ip ON i.InquiryParentID=ip.id
			   INNER JOIN ESv2_Users as u ON ip.email=u.Email
				LEFT JOIN ESv2_Admission as a ON ip.FamilyID=a.FamilyID AND a.StatusID NOT IN (2,3)
					WHERE u.UserIDX='".getUserID()."'";
	  $exec = DB::select($qry);
      if($exec && count($exec)>0){
	    return $exec;
	  }else{
	    return false;
	  }	  
	}	
	
    function isParent() {
        $parentgroup =  DB::table('ESv2_UsersGroup')->select(array('GroupID','GroupName'))->where('GroupName','Parent')->get();
		if($parentgroup && count($parentgroup)>0){
		$parentgroup = $parentgroup[0]->GroupName;
		}else{
		$parentgroup = 'xparent';
		}
    	return (strtolower(getUserGroup()) == strtolower($parentgroup));
    }

    function isStudents() {

    	return ( getUserGroup() == 'students' ? true : false );
    }

    function isAccounting() {

        return ( (getUserGroup() == 'administrator' || getUserGroup() == 'accounting'  || getUserGroup() == 'cashier') ? true : false );
    }

	function IsAdmin() {
    	return ( getUserGroup() == 'administrator' ? true : false );
    }

    function isfaculty() {

        return ( getUserGroup() == 'faculty' ? true : false );
    }
	
	function IsPanel() {
        return ( getUserGroup() == 'panel' ? true : false );
    }

    function isParentOrStudents() {

    	return ( getUserGroup() == 'students' ||  getUserGroup() == 'parent' ? true : false );
    }

    function getGroupCodeByUserID($key) {
    	return DB::table('ESv2_Users')->select(DB::raw('(select GroupName from ESv2_UsersGroup where GroupID=UsersGroupID) as GroupName'))->where('UserIDX',$key)->pluck('GroupName');
    }

    function getUserGroupID($key ='') {
        $key = ($key == '') ? getUserID() : $key ;
    	return DB::table('ESv2_Users')->select('UsersGroupID')->where('UserIDX',$key)->pluck('UsersGroupID');
    }

    function getTotalUser($group = 0) {
        
        $sWhere = array();
        
        if($group != '0'){
            $sWhere = array(
                'UsersGroupID' => $group
            );
        }
    	return DB::table('ESv2_Users')->where($sWhere)->count();
    }
    
    

    function getLoginPhotoID() {
    	if (isParent()) {
    		return getGuardianFamilyID();
    	}
    	return getUserID();
    }

    function getUserIDByVC($vc) {
    	return DB::table('ESv2_Users')->select('UserIDX')->where('ConfirmNo',$vc)->pluck('UserIDX');
    }

    function getUserNameByID($key) {
    	return DB::table('ESv2_Users')->select('Username')->where('UserIDX',$key)->pluck('Username');
    }
    function getUserNameByFID($key) {
    	return DB::table('ESv2_Users')->select('Username')->where('FacultyID',$key)->pluck('Username');
    }
    function getUserNameByEID($key) {
    	return DB::table('ESv2_Users')->select('Username')->where('FacultyID',$key)->pluck('Username');
    }

    function getUserIDByEmail($email) {
    	return DB::table('ESv2_Users')->select('UserIDX')->where('Email',$email)->pluck('UserIDX');
    }

    function getUserEmailByID($id) {
    	return DB::table('ESv2_Users')->select('Email')->where('UserIDX',$id)->pluck('Email');
    }

    function getGuardianFamilyID($AppNo = '') {
		return
			!$AppNo ? Auth::user()->FamilyID :
			DB::table('ESv2_Admission')
			->select('FamilyID')
			->where('AppNo',$AppNo)
			->pluck('FamilyID');
	}

	function getGuardianName() {
		return
		DB::table('ESv2_ParentAccounts as a')
		->select('Guardian_Name')
		->leftJoin('ESv2_Admission_FamilyBackground as b'
			,'a.FamilyID','=','b.FamilyID')
		->where('a.FamilyID',getGuardianFamilyID())
		->pluck('Guardian_Name');
	}

	function getGuardianFamilyIDByUserIDX($UserIDX) {
		return
			DB::table('ESv2_Users')
			->select('FamilyID')
			->where('UserIDX',$UserIDX)
			->pluck('FamilyID');
	}

	function getUserExpiryDate() {
		return Auth::user()->PwdExpiryDate;
	}

	function updateLoginDate() {
		DB::statement("update ESv2_Users set LastLoginDate='".systemDate()."' where UserID='".getUserName()."'");
	}

    function getUserProgramAccess(){

        // $key = getUserGroupID();
        $key = getUserID(); // UserGroupID == UserIDX on ESV2_UserGroup_ProgramAccess
        $group = getUserGroupID();

        $result =  DB::table('ESv2_UserGroup_ProgramAccess')
            ->select('ProgID')
            ->where('UserGroupID',$key)
            ->where('IsGroup',1)
            ->get();

        if(empty($result)) {
            $result =  DB::table('ESv2_UserGroup_ProgramAccess')
            ->select('ProgID')
            ->where('UserGroupID',$group)
            ->where('IsGroup',1)
            ->get();
        }    

        $array = [];
        $i=0;
        foreach($result As $r){
            $array[$i] = $r->ProgID;
            ++$i;
        }

        return $array ;
                                                         
    }

    // function getUserProgramAccess(){

    //     // $key = getUserGroupID();
    //     $key = getUserID(); // UserGroupID == UserIDX on ESV2_UserGroup_ProgramAccess

    //     $result =  DB::table('ESv2_UserGroup_ProgramAccess')
    //         ->select('ProgID')
    //         ->where('UserGroupID',$key)
    //         ->where('IsGroup',1)
    //         ->get();

    //     $array = [];
    //     $i=0;
    //     foreach($result As $r){
    //         $array[$i] = $r->ProgID;
    //         ++$i;
    //     }

    //     return $array ;
                                                         
    // }

    function getOutstandingBalance($type,$idno, $asof ='', $not_inclOnwards =0 ){

        $asof = $asof == ''? systemDate() : $asof ;

        $query = "SELECT TOP 100 PERCENT J.TermID, dbo.fn_AcademicYearTerm(J.TermID) AS AcademicYearTerm, J.TransDate AS [Date], J.TransID, FT.TransCode, J.ReferenceNo, J.DMCMRefNo, SUM(J.Debit) AS Debit, SUM(J.Credit) AS Credit, SUM(J.[Assess Fee]) AS AssessFee, SUM(J.Discount) AS FinancialAid, CASE WHEN J.TransID = 0 THEN SUM(J.ActualPayment) ELSE 0 END as Payment,
                       CASE WHEN J.TransID = 20 THEN dbo.fn_GetPaymentDiscount(J.ReferenceNo) ELSE 0 END AS Discount, CASE WHEN J.TransID = 20 THEN dbo.fn_GetPaymentNonLedger_r2(J.ReferenceNo, 1, J.IDNo) ELSE 0 END AS NonLedger, J.IDType, J.IDNo, CASE WHEN J.TransID = 60 THEN J.Description ELSE FT.Remarks END as Remarks, CASE J.TransID WHEN 60 THEN ( SELECT TOP 1 Posted FROM    ES_DebitCreditMemo WHERE   RefNo = J.ReferenceNo ) WHEN 20 THEN ( SELECT TOP 1 (CASE WHEN ForPosting = '1' THEN 0 ELSE 1 END) FROM dbo.ES_OfficialReceipts WHERE     ORNo = J.ReferenceNo) ELSE 1 END AS Posted,
                       CASE WHEN J.TransID = 60 THEN (SELECT TOP 1 PostedDate FROM ES_DebitCreditMemo WHERE RefNo = J.ReferenceNo) ELSE J.TransDate END AS DatePosted, CASE WHEN J.TransID = 20 THEN (select top 1 RefNo from es_officialreceipts where orno = J.ReferenceNo) else '' END as [OR RefNo], CASE WHEN J.TransID = 20 THEN (select top 1 dbo.fn_TransactionName(TransType) from es_officialreceipts where orno = J.ReferenceNo) else '' END as [OR PaymentFor], IsNULL(CASE WHEN J.TransID = 20 THEN (SELECT TOP 1 DiscountoffsetBackAccount FrOM ES_OfficialReceipts WHERE ORNo = J.ReferenceNo) ELSE 0 END, 0) as OffsetBackaccount, (CASE WHEN J.TransID = 20 THEN dbo.fn_GetOverPayment(J.ReferenceNo) ELSE 0 END) as Overpayment, (CASE WHEN J.TransID = 60 THEN dbo.fn_GetWithdrawnRefund(J.ReferenceNo) ELSE 0 END) as WithdrawnRefund
             FROM      dbo.ES_Journals J LEFT OUTER JOIN dbo.ES_FinancialTransactions FT ON J.TransID = FT.TransID
             WHERE     ((J.TransID NOT IN (0, 1, 2, 3, 4, 5, 6)) OR (J.DMCMRefNo = '')) AND NOT (J.TransID = 20 AND J.Debit > 0 AND J.Credit = 0)
             GROUP     BY J.ReferenceNo, FT.TransCode, FT.Remarks, J.IDType, J.IDNo, J.TermID, J.TransID, J.DMCMRefNo, J.TransDate, J.Description, FT.SortNumber
             HAVING    (J.IDType = '{$type}') AND (J.IDNo = '{$idno}') AND (J.TransDate <= '{$asof}') ".
                        ($not_inclOnwards != 0 ? " AND dbo.fn_Academicyearterm(J.TermID) < dbo.fn_Academicyearterm('{$not_inclOnwards}') " : "") .
             "ORDER BY dbo.fn_AcademicYearTerm(J.TermID), J.TransDate, FT.SortNumber";

        $rs = DB::select($query);

        $runningBalance = 0;
        $debit= 0;
        $credit= 0;
        $discount = 0;

        foreach($rs as $r){
            if ( $r->Posted == '1' ){
                            
                $debit = floatval($r->Debit)  - floatval($r->WithdrawnRefund);
                $credit =  floatval($r->Credit) -  floatval($r->NonLedger) ; // + Trimmed(rs![Overpayment], VALUE_CURRENCY)
    
                If($r->OffsetBackaccount == '1') {
                    $discount = floatval($r->Discount);
                }else{
                    $discount = 0;
                }
    
                $runningBalance += ( floatval($debit) - ( floatval($credit) + floatval($discount)));

            }
        }

        return number_format($runningBalance,2,'.','') ;
    }

    function getTotalSiblings($studnum) {
        $FamID =  DB::table('ES_students')
            ->select('FamilyID')
            ->where('StudentNo',$studnum)
            ->pluck('FamilyID');
			
		$term =  DB::table('ES_AYTerm')
            ->select('TermID')
            ->where('AcademicYear','like',('%-'.date('Y')))
            ->pluck('TermID');	
      //SELECT TOP 1 * FROM ES_AYTerm WHERE AcademicYear LIKE CONCAT('%-',YEAR(GETDATE())) ORDER BY AcademicYear DESC
        return DB::table('ES_Students')
		       ->leftjoin('ES_Registrations','ES_Students.StudentNo','=','ES_Registrations.StudentNo')
			   ->where('ES_Students.StatusID','<',7)
		       ->where('FamilyID',$FamID)
			   ->where('ES_Registrations.TermID',$term)->count();
    }
	
    function getTotalSiblingsX($studnum){
        $FamID =  DB::table('ES_students')
            ->select('FamilyID')
            ->where('StudentNo',$studnum)
            ->pluck('FamilyID');
			
		$term =  DB::table('ES_AYTerm')
            ->select('TermID')
            ->where('AcademicYear','like',('%-'.date('Y')))
            ->pluck('TermID');	
        
		return DB::SELECT("SELECT COUNT(s.StudentNo) as items 
		                     FROM ES_Students as s 
							WHERE StudentNo IN (SELECT StudentNo FROM ES_Registrations) AND FamilyID='".$FamID."' AND StatusID<7
							  AND StudentNo NOT IN (SELECT StudentNo FROM ES_Registrations WHERE ProgID=7 AND YearLevelID=4 AND YEAR(RegDate)<YEAR(GETDATE()))")[0]->items;
    }


?>
