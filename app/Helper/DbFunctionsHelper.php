<?php

	function getAdmissionStatus()
	{
		return [1,2,3,4, 10, 11, 12];
	}

    function getSubjectInfo($pk){
        return DB::table('ES_Subjects')->where('subjectID',$pk)->first();
    }

    function isSHS($prog_id){
        $rs = DB::table('ES_GS_Setup')->where('SetupID','7')->first();
        return ( trimmed($rs->Value) == $prog_id ? '1':'0');
    }
    
	function getActiveTerm(){
	  $rs     = DB::table('ES_AYTerm')->where('Active_OnlineEnrolment',1)->first();
	  return  (($rs && count($rs)>0)?($rs->TermID):1);	
	}
	
	function getCurrentTerm(){
	  $rs     = DB::table('ES_AYTerm')->where('IsCurrentTerm',1)->first();
	  return  (($rs && count($rs)>0)?($rs->TermID):1);	
	}
	
    function shsterm(){
        $rs = DB::table('ES_GS_Setup')->where('SetupID','10')->first();
        return $rs->Value;
    }

    function grade_remarks($setup, $grade){
        $rs = DB::table('ES_GS_GradingSystem_Details')
		              ->select(['Remark'])
                	  ->whereRaw(" ('". $grade ."' BETWEEN CAST([Min] AS FLOAT) AND CAST([Max] AS FLOAT) ) ")
                      ->where('TemplateID' , $setup )
                      ->first();

        return trimmed($rs->Remark);
    }

    function conduct_gradingsystem($deportment){

        $rs = DB::table('ES_DeportmentTemplates')
		              ->select(['GradingSystem'])
                      ->where('TemplateID' , $deportment )
                      ->first();

        return isset($rs->GradingSystem) ? trimmed($rs->GradingSystem) : getSetting('2') ;
    }

    function get_componentId($component){
        $rs = DB::table('ES_GS_GradeComponents')
		              ->select(['fldPK'])
                      ->where('CompDescription' , trimmed($component) )
                      ->first();

        return trimmed($rs->fldPK);
    }

    function get_subjpolicy ($sched, $period){

         $policy = DB::table('ES_GS_PolicySetup as s')
                        ->leftJoin('ES_GS_GradingPolicy as c', 's.PolicyID','=','c.PolicyID')
		              ->selectRaw(" c.* ")
                	  ->whereRaw("s.ScheduleID = {$sched} AND s.PeriodID = '{$period}'")
                      ->first();

        return $policy;
    }

    function IsTFAccountID($account_opt){
        
        $result = false;
        #Account Options for Tuition Fee
        $options = array(
            1 ,2 ,22 ,23 ,24 ,25 ,26 ,27
        );
        
        if($account_opt == 0){
            return $result;
        }else{
            
            foreach( $options as $o){
                if($o == $account_opt){
                    $result = true;
                    break;
                }
            }
            
            return $result;

        }


    }

    function get_ProgClassCode($level){
        $rs = DB::table('ES_Program_Class')
		              ->select(['ClassDesc'])
                      ->where('ClassCode' , trimmed($level) )
                      ->first();
        if (!is_null($rs)){
            return trimmed($rs->ClassDesc);
        }
        return '';
    }

   	function get_template_status($key = '')
	{
	    $status= array(
            0 => 'All',
            1 => 'New',
            2 => 'Old'
        );
        if($key == -1){
            return $status['0'];
        }
        if($key != ''){
            return $status[$key];
        }else{
            return $status ;
        }

	}

    function get_template_session($key = '')
	{
	   $session = array(
            0 => 'Any',
            1 => 'Day',
            2 => 'Evening'
        );

        if($key == -1){
            return $status['0'];
        }

        if($key != ''){
            return $session[$key];
        }else{
            return $session ;
        }

	}

    function get_Yearlevel($level = 0, $prog = 0){

        $rs = DB::table('ESv2_YearLevel')->selectRaw("YearLevelID, YLID_OldValue, YearLevelCode, YearLevelName");

        if($prog != 0){
            $rs->where('ProgID', $prog);
        }

        if($level != 0 ){
            return $rs->where('YearLevelID', $level)->first();
        }  else{
            return $rs->get();
        }
    }

    function get_programs($prog = 0)
    {
        $progIds = getUserProgramAccess();
        
        $get = DB::table('es_programs')->select('ProgID', 'ProgName', 'ProgShortName', 'ProgClass')
                ->whereIn('ProgID', $progIds )
                // ->whereNotIn('ProgClass', [50, 80, 60])
                ->orderBy('ProgClass', 'ASC')->get();

        return $get;
    }

    function get_majors($prog)
    {

        $get = DB::table('ES_ProgramMajors')
                ->selectRaw("MajorDiscID As MajorID, ProgID, dbo.fn_MajorName(MajorDiscID) As MajorName ")
                ->whereRaw("progid = '{$prog}' and Inactive = 0")
                ->get();

        return $get;
    }

    function get_PayerType($key = '')
	{
	     $payer = array(
            '0' => 'Other'
           ,'1' => 'Student'
           ,'2' => 'Employee'
           ,'3' => 'Scho. Provider'
           ,'4' => 'Tenant'
           ,'5' => 'Other Payer'
           ,'6' => 'Applicant'
           ,'' => 'Other Payer'
        );

        if($key != ''){
            return $payer[$key];
        }else{
            return $payer ;
        }

	}

    function get_TransactionType($key = '')
	{
	    $rs = DB::table('ES_FinancialTransactions')
		              ->select('*');

		if($key != ''){
            return $rs->where('TransID',$key)->first();
        }else{
            return $rs->get();
        }

	}

    function get_campus($id='')
    {

        $rs = DB::table('ES_Campus')
		              ->select('*');

		if($id != ''){
            return $rs->where('CampusUD',$id)->first();
        }else{
            return $rs->get();
        }
    }

    function getAccountInfo($pk){
        return DB::table('ES_Accounts')->where('AcctID',$pk)->first();
    }

    function getPeriod($class = '1')
    {
        $get = DB::table('ES_GS_GradingPeriods')->select('PeriodID', 'Description1', 'Description2', 'GroupClass')
				->where('GroupClass', $class)
				->get();
        return $get;
    }
    
    function get_deptlevel($level = 0){                
        
        $rs = DB::table('esv2_yearlevel')->selectRaw("YearLevelID, YearLevelCode, YearLevelName, ProgID, YLID_OldValue");
        
        $allowed_ids = getUserProgramAccess();
        
        
        if($level != 0 ){
            return $rs->where('YearLevelID', $level)->first();
        }  else{ 
            return $rs->whereIn('ProgID', $allowed_ids )->get();   
        }               
    }
    
    
    
   
?>