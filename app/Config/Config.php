<?php

Namespace App\Config;

Class Config extends Set {
	/**
	* Admission condfiguration
	*/
	public static function isHigherLevel() {
		return self::$isHigherLevel;
	}

	public static function numberOfcampusesAndCourse() {
		return self::$numberOfcampusesAndCourse;
	}

	public static function campusAndCourse1() {
		return self::$campusAndCourse1;
	}

	public static function campusAndCourse2() {
		return self::$campusAndCourse2;
	}

	public static function campusAndCourse3() {
		return self::$campusAndCourse3;
	}

	public static function campusAndCourse4() {
		return self::$campusAndCourse4;
	}

	public static function campusAndCourse1Required() {
		return self::$campusAndCourse1Required;
	}

	public static function campusAndCourse2Required() {
		return self::$campusAndCourse2Required;
	}

	public static function campusAndCourse3Required() {
		return self::$campusAndCourse3Required;
	}

	public static function campusAndCourse4Required() {
		return self::$campusAndCourse4Required;
	}

	public static function defaultCampus() {
		return self::$defaultCampus;
	}

	public static function AdmittedStatusID() {
		return self::$AdmittedStatusID;
	}

	public static function parentAppTypeIDs() {
		return self::$parentAppTypeIDs;
	}

	public static function appTypeIDDefault() {
		return self::$appTypeIDDefault;
	}

	public static function currentTermiID() {
		return self::$currentTermiID;
	}

	public static function getContentTemplate($code) {
		return \App\Modules\Setup\Models\Content::getTemplate($code);
	}
}

