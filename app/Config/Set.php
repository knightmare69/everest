<?php

Namespace App\Config;
Class Set {
	use Admission;
}

trait Admission {

	public static $isHigherLevel = true;

	public static $defaultCampus = 1;

	public static $numberOfcampusesAndCourse = 4;

	public static $campusAndCourse1 = true;

	public static $campusAndCourse2 = false;

	public static $campusAndCourse3 = false;

	public static $campusAndCourse4 = false;

	public static $campusAndCourse1Required = false;

	public static $campusAndCourse2Required = false;

	public static $campusAndCourse3Required = false;

	public static $campusAndCourse4Required = false;

	public static $AdmittedStatusID = 2;

	public static $appTypeIDDefault = 1;

	public static $parentAppTypeIDs = [1,2,3,4];

	public static $currentTermiID = 103;

}

