<?php namespace App\Libraries;
use App\Libraries\pdf\TCPDF;
class PDF extends TCPDF{
    
   public function outputHtml($html,$title = 'Report',$resolution = 'L',$properties = ''){
        
        $this->SetFont('helvetica','',8);

        $this->setPageUnit('pt');

        if (!empty($properties)){
        
            $this->AddPage($resolution,$properties);
        }
        else {
            $this->AddPage($resolution);
        }

        $this->setTitle($title);
        $this->writeHTML($html);
        $this->output($title, 'I');
   }

    public function outputData($title = 'Report',$resolution = ''){
        $this->SetFont('helvetica','',9);
        if (!empty($resolution)){
            $pdf->setPageUnit('pt');
            $pdf->SetMargins(0,8);
            $pdf->AddPage('L',$resolution);
        }
        else {
            $this->AddPage();
        }
        $this->setTitle($title);
        $this->output();
    }

    private $tds = '';
    private $ths = '';
    private $trs = '';
    private $table = '';


    public function setTd($data,$width = '',$style = '',$align = 'left',$colspan = '1') 
    {
        $this->tds .='<td '.($style ? 'style="'.$style.'"' : '').' '.($width ? 'width="'.$width.'"' : '').' align="'.$align.'" colspan="'.$colspan.'">'.$data.'</td>';
        return $this;
    }

    public function setRow() 
    {
        $this->trs .= "<tr>".$this->tds."</tr>";
        $this->tds = '';
        return $this;
    }

    public function setTh($data,$width = '',$align = 'left') 
    {
        $this->ths .='<th '.($width ? 'width="'.$width .'"': '').'  align="'.$align.'"><b>'.$data.'</b></th>';
        return $this;
    }

    public function setHeader()
    {
        return "<thead><tr>".$this->ths."</tr></thead>";
    }

    public function setTable($width)
    {
        $data = '<table cellspacing="0" cellpadding="1" border="1" width="'.$width.'">'.
            $this->setHeader().
            "<tbody>".
                $this->trs.
            "</tbody>".
            "</table>";
        $this->clearTable();
        return $data;
    }

    public function clearTable()
    {
        $this->trs = '';
        $this->tds = '';
        $this->ths = '';
    }
    
    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Date Printed : ' . systemDate() , 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }


}

?>