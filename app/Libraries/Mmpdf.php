<?php namespace App\Libraries;
use App\Libraries\pdf\TCPDF;
class Mmpdf {
    
   public function output($html){
        $pdf = new TCPDF;
        $pdf->setPrintHeader(false);
		$pdf->SetFont('dejavusans','',9);
        if (!empty($resolution)){
            $pdf->setPageUnit('pt');
            $pdf->SetMargins(0,8);
            $pdf->AddPage('L',$resolution);
        }
        else {
            $pdf->AddPage();
        }
        $pdf->writeHTML($html);
        $pdf->output();
   }
  public function xserialize($arr=array()){
	$pdf = new TCPDF;
    return $pdf->serializeTCPDFtagParameters($arr);    
  }
}
?>