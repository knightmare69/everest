<?php

Route::group(['prefix' => 'reports', 'middleware'=>'auth'], function () {
    Route::get('/', 'Reports@index');
    Route::post('event', 'Reports@event');
    Route::get('print', 'Reports@print_report');
    Route::get('export', 'Reports@export_report');
    Route::get('preview', 'Reports@print_preview');
});
