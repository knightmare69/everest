<?php

namespace App\Modules\Reports\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Reports extends Model
{
    protected $table = 'es_reports';
    protected $primaryKey = 'ReportID';

    protected $fillable  = ['ReportNo', 'Module', 'ReportTitle', 'Description', 'ReportPath', 'ReportFileName', 'StoredProcedure', 'ForOnlineApp', 'CreatedBy', 'DateCreated', 'ModifiedBy', 'DateModified', 'SubStoredProcedure1', 'SubStoredProcedure2', 'SubStoredProcedure3', 'SubStoredProcedure4', 'SubStoredProcedure5', 'SubRep1_File', 'SubRep2_File', 'SubRep3_File', 'SubRep4_File', 'SubRep5_File'];

    public $timestamps = false;

    public function getReports()
    {
        $rep_module_id = 2011;

        $reports = DB::table('pages as p')->select('p.page_id', 'pa.action_id as RepID', 'p.name as RepParent', 'p.slug as RepParentSlug', 'pa.name as RepName', 'pa.slug as RepSlug', 'r.StoredProcedure as SP')
                    ->join('access as a', 'p.page_id', '=', 'a.page_id')
                    ->join('page_actions as pa', 'pa.action_id', '=', 'a.action_id')
                    ->leftJoin('es_reports as r', 'r.ReportNo', '=', 'pa.action_id')
                    ->where([
                        'a.link_id' => getUserGroupID(),
                        'p.module_id'=> $rep_module_id,
                        'pa.is_default' => 0
                    ])
                    ->orderBy('p.sort', 'asc')
                    ->get();

        $reports = collect($reports)->groupBy('RepParent')->toArray();

        return $reports;
    }

    public function AcademicTerm()
    {
        $get = DB::table('es_ayterm')->select('TermID', 'AcademicYear', 'SchoolTerm')
        // ->where('SchoolTerm', 'School Year')
        ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function Programs()
    {
        $get = DB::table('es_programs')->select('ProgID', 'ProgName', 'ProgShortName', 'ProgClass')
                // ->whereIn('ProgShortName', ['Grade School', 'High School', 'Senior High School'])
                // ->whereNotIn('ProgClass', [50, 80, 60])
                ->orderBy('ProgClass', 'ASC')->get();

        return $get;
    }

    public function Campus()
    {
        $get = DB::table('es_campus')->select('CampusID', 'Acronym', 'ShortName')->get();
        return $get;
    }
    
    public function getReportdetails($report_id)
    {
        $reports = DB::table('pages as p')->select('p.page_id', 'pa.action_id as RepID', 'p.name as RepParent', 'p.slug as RepParentSlug', 'pa.name as RepName', 'pa.slug as RepSlug')
                    ->join('access as a', 'p.page_id', '=', 'a.page_id')
                    ->join('page_actions as pa', 'pa.action_id', '=', 'a.action_id')                    
                    ->where([
   
                        'pa.action_id' => $report_id
                    ])                    
                    ->first();

        return $reports;
    }
    
    public function getYearLevel($prog_class=0)
    {
        
        $at = DB::table('esv2_yearlevel')->select('YLID_OldValue as YearLevelID', 'YearLevelCode as Code','YearLevelName as YearLevel', 'ProgClass');
        
        if($prog_class!=0){
            $at->where('ProgClass', $prog_class);
        }                       
         
        return $at->orderBy('SeqNo', 'ASC')->get();
    }
    
    public function getSections($term_id, $prog_id, $yl_id)
    {

        $sec = DB::table('es_classsections')->select('SectionID', 'SectionName')
                ->where(['TermID' => $term_id, 'ProgramID' => $prog_id])
                ->where('YearLevelID', $yl_id)
                ->orderBy('SectionName', 'ASC')->get();

        return $sec;
    }

    public function getSubjects($term_id, $sections)
    {
        $subjlist = explode(",", $sections);

        $sec = DB::table('es_classschedules')
                ->select(['SubjectID', DB::raw(" dbo.fn_SubjectCode(SubjectID) As SubjectCode ")])
                ->where(['TermID' => $term_id])
                ->whereIn('SectionID',$subjlist)
                ->groupBy("SubjectID")
                ->orderBy('SubjectCode', 'ASC')->get();

        return $sec;
    }
    
    public function getStudent($find_str, $term, $program, $year_level, $section_id)
    {
        $student = DB::table('es_registrations as r')
                    ->select('s.StudentNo', 's.FirstName', 's.LastName', 's.MiddleName', 's.MiddleInitial')
                    ->join('es_students as s', 'r.StudentNo', '=', 's.StudentNo')
                    ->where(['r.ProgID' => $program, 'r.YearLevelID' => $year_level, 'r.TermID' => $term])
                    ->where(function($query) use ($find_str){
                        $query->where('r.StudentNo', '=', $find_str)
                        ->orWhere('s.LastName', 'like', '%'.$find_str.'%')
                        ->orWhere('s.FirstName', 'like', '%'.$find_str.'%');
                    });

        if($section_id != 0){
            $student->where(['r.ClassSectionID' => $section_id]);
        }

        return $student->get();
    }

    public function getStudentSectionID($student_no, $term_id, $prog_id, $year_level_id)
    {
        $get = DB::table('es_registrations')->select('ClassSectionID')
                ->where(['StudentNo' => $student_no, 'TermID' => $term_id, 'ProgID' => $prog_id, 'YearLevelID' => $year_level_id])->first();

        $ret = !empty($get->ClassSectionID) ? $get->ClassSectionID : 0;
        return $ret;
    }

    public function getProjections($term, $prog){

        $get = DB::select("SELECT IFNULL(p.IndexID,0) AS IndexID, ProgClass, {$term} AS TermID , 1 AS CampusID, y.ProgID, y.YearLevelID, y.YearLevelName AS YearLevel,
                        dbo.fn_ProgramCollegeID(y.ProgID) AS CollegeID, m.MajorDiscID AS MajorID, dbo.fn_MajorName(m.MajorDiscID) AS Major,
                        IFNULL(p.NoSections,0) As NoSections,
                        IFNULL(p.NoStudents,0) As NoStudents
                        FROM es_yearlevel y
                        LEFT JOIN es_programmajors m ON y.ProgID=m.ProgID
                        LEFT JOIN es_enrollmentprojections p ON p.ProgID = y.ProgID AND p.MajorID = m.MajorDiscID AND p.YearLevelID=y.YearLevelID
                        WHERE y.ProgID = {$prog}");

        return $get;

    }

    public function setProjections($term, $post){

        $prog = decode(getObjectValue($post,'prog'));

        $whre = array(
            'TermID' => $term,
            'ProgID' => $prog,
            'MajorID' => getObjectValue($post,'major'),
            'YearLevelID' => getObjectValue($post,'level'),

        );

        $data = array(
            'TermID' => $term,
            'CampusID' => 1,
            'ProgID' => $prog,
            'MajorID' => getObjectValue($post,'major'),
            'YearLevelID' => getObjectValue($post,'level'),
            'NoSections' => getObjectValue($post,'sec'),
            'NoStudents' => getObjectValue($post,'stu'),
        );

        $ret = DB::table('es_enrollmentprojections')->where($whre)->count();

        if($ret > 0 ){
            $ret = DB::table('es_enrollmentprojections')->where($whre)->update($data);
        }else{
            $ret = DB::table('es_enrollmentprojections')->insert($data);
        }

        return $ret;
    }
    



        
}
