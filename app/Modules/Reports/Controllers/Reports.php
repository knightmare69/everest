<?php

namespace App\Modules\Reports\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Academics\Models\EmpFaculty as FacultyModel;
use App\Modules\Reports\Models\Reports as Model;
use App\Modules\Reports\Services\ReportsSP as Services;

use App\Libraries\PDF;
use Spipu\Html2Pdf\Html2Pdf;

use Response;
use Request;
use Input;
use Permission;
use File;

use DB;
use App\Libraries\CrystalReport as crystal;

Class Reports extends Controller
{
	private $typeid   = 27;
	private $extfile  = 'pdf';
	private $xreports = array(
	                     'studdetail'                      				      => array('name'=>'Student Details','sp'=>'rpt_StudentDetails','file'=>'Accounting/StudentDetails.rpt','logo'=>1),
	                     'payledger'                       				      => array('name'=>'Student Payment Ledger','sp'=>'rpt_StudentDetails','file'=>'Accounting/StudentPaymentLedger.rpt','logo'=>1),
	                     'unpaid'                          				      => array('name'=>'Student Unpaid Fees','sp'=>'rpt_StudentDetails','file'=>'Accounting/StudentUnpaidFees.rpt','logo'=>1),
						 'account-receivable---enrollment' 				      => array('name'=>'Account Receivable - Enrollment1','sp'=>'ES_rptAccountReceivables','file'=>'Accounting/account_receivables_enrollment1.rpt','logo'=>1),
						 'account-receivable---enrollment-[layout-2]' 	      => array('name'=>'Account Receivable - Enrollment2','sp'=>'ES_rptAccountReceivables','file'=>'Accounting/account_receivables_enrollment2.rpt','logo'=>1),
						 'account-receivable---enrollment-tuition-fee-1'      => array('name'=>'Account Receivable - Tuition 1','sp'=>'Exec rpt_AccountReceivables_Tuition','file'=>'Accounting/account_receivables_tuition 1.rpt','logo'=>1),
						 'account-receivable---enrollment-tuition-2'          => array('name'=>'Account Receivable - Tuition 2','sp'=>'Exec rpt_AccountReceivables_Tuition','file'=>'Accounting/account_receivables_tuition 2.rpt','logo'=>1),
						 'account-receivable---enrollment-[selected-account]' => array('name'=>'Account Receivable - Selected Account','sp'=>'Exec rpt_AccountReceivables_Tuition','file'=>'Accounting/account_receivables_selecteditems.rpt','logo'=>1),
						 'summary-of-billing-and-collection'                  => array('name'=>'Summary of Billing and Collection','sp'=>'ES_rptSummaryofBillingandCollection','file'=>'Accounting/summary of billing and collection.rpt','logo'=>1),
						 'sum.-of-billing-and-collection-with-financial-aid'  => array('name'=>'Summary of Billing and Collection With Financial Aid','sp'=>'ES_rptSummaryofBillingFinancialAidAndCollection','file'=>'Accounting/summary of billing and collection with financial aid.rpt','logo'=>1),
						 'summary-of-assessment'                              => array('name'=>'Summary of Assessment','sp'=>'ES_RptSummaryofAssessment','file'=>'Accounting/summary of assessment.rpt','logo'=>1),
						 'list-of-fully-paid---enrollment'                    => array('name'=>'List of Fully Paid - Enrollment','sp'=>'ES_rptListofFullyPaidStudents','file'=>'Accounting/list_of_fully_paid_students 1.rpt','logo'=>1),
						 'list-of-fully-paid---enrollment-2'                  => array('name'=>'List of Fully Paid - Enrollment','sp'=>'ES_rptListofFullyPaidStudents','file'=>'Accounting/list_of_fully_paid_students 2.rpt','logo'=>1),
						 
						 
						 'collection-report-by-payor-1'                       => array('name'=>'Collection Report By Payor','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/collection report by payor 1.rpt','logo'=>0),
	                     'collection-report-by-payor-2'                       => array('name'=>'Collection Report By Payor','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/collection report by payor 2.rpt','logo'=>0),
	                     'collection-report-by-payor-3'                       => array('name'=>'Collection Report By Payor','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/collection report by payor 3.rpt','logo'=>0),
	                     'collection-report-by-payor-4'                       => array('name'=>'Collection Report By Payor','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/collection report by payor 4.rpt','logo'=>0),
	                     'collection-report-by-department-1'                  => array('name'=>'Collection Report By Department 1','sp'=>'ES_rptCollectionReportByDepartment','file'=>'Cashier/collection report by department 1.rpt','logo'=>0),
	                     'collection-report-by-department-2'                  => array('name'=>'Collection Report By Department 2','sp'=>'ES_rptCollectionReportByDepartment','file'=>'Cashier/collection report by department 2.rpt','logo'=>0),
	                     'collection-report-by-academic-dept-1'               => array('name'=>'Collection Report By Academic Program 1','sp'=>'ES_rptCollectionReportByAcademicProgram','file'=>'Cashier/collection report by academic program 1.rpt','logo'=>0),
	                     'collection-report-by-academic-dept-2'               => array('name'=>'Collection Report By Academic Program 1','sp'=>'ES_rptCollectionReportByAcademicProgram','file'=>'Cashier/collection report by academic program 2.rpt','logo'=>0),
	                     'collection-report-by-yearlevel-1'                   => array('name'=>'Collection Report By Academic Program 1','sp'=>'ES_rptCollectionReportByYearLevel','file'=>'Cashier/collection report by year level 1.rpt','logo'=>0),
	                     'collection-report-by-yearlevel-2'                   => array('name'=>'Collection Report By Academic Program 1','sp'=>'ES_rptCollectionReportByYearLevel','file'=>'Cashier/collection report by year level 2.rpt','logo'=>0),
						 'detailedcollect1'                                   => array('name'=>'Detailed Collection Report','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/detailed collection report 1.rpt','logo'=>0),
						 'detailedcollect2'                                   => array('name'=>'Detailed Collection Report','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/detailed collection report 2.rpt','logo'=>0),
						 'detailedcollectcampus'                              => array('name'=>'Detailed Collection Report','sp'=>'ES_rptDetailedCollectionReport','file'=>'Cashier/detailed collection report 2.rpt','logo'=>0),
						 'summarybysubaccounts'                               => array('name'=>'Summary of Collection By SubAccounts','sp'=>'ES_rptSummaryCollectionBySubAccounts','file'=>'Cashier/summary of collections by sub-accounts.rpt','logo'=>0),
						 'summarybyaccounts'                                  => array('name'=>'Summary of Collection By Accounts','sp'=>'ES_rptSummaryCollectionByAccounts','file'=>'Cashier/summary of collections by accounts.rpt','logo'=>0),
						 'summarybyfunds'                                     => array('name'=>'Summary of Collection By Funds','sp'=>'ES_rptSummaryCollectionByFunds','file'=>'Cashier/summary of collections by funds.rpt','logo'=>0),
						 'summarybycollege'                                   => array('name'=>'Summary of Collection By College','sp'=>'ES_rptSummaryCollectionByColleges','file'=>'Cashier/summary of collections by colleges.rpt','logo'=>0),
						 'annualsummary'                                      => array('name'=>'Annual Summary of Collection','sp'=>'ES_rptAnnualSummaryofCollection','file'=>'Cashier/annual summary of collection.rpt','logo'=>1),
						 'annualsummarydetailed'                              => array('name'=>'Annual Summary of Collection','sp'=>'ES_rptSummaryofCollectionPerAccount_Annual','file'=>'Cashier/annual summary of collection by accounts.rpt','logo'=>1),
						 'listofpayorbyaccount'                               => array('name'=>'List of Payor by Account','sp'=>'ES_rptListofPayorsByAccount','file'=>'Cashier/list of payors by account.rpt','logo'=>0),
						 'listofpayorpersemester'                             => array('name'=>'List of Payor per Semester','sp'=>'ES_rptSemestralCollectionperAccount','file'=>'Cashier/list of payors by account by semester.rpt','logo'=>1),	                     
						 'listofpayorclassification'                          => array('name'=>'List of Payor by Classification','sp'=>'ES_rptListofPayorsByClassification','file'=>'Cashier/list of payors by classification.rpt','logo'=>0),	                     
						 'listofpayorsubsidiary'                              => array('name'=>'List of Payor by Subsidiary','sp'=>'ES_RptListofPayorsBySubsidiaryCode','file'=>'Cashier/list of payors by subsidiary codes.rpt','logo'=>1),	                     
						 'dailycashierreport'                                 => array('name'=>'Daily Cashier Report','sp'=>'ES_rptDailyCashiersReport','file'=>'Cashier/daily cashiers report.rpt','logo'=>0),
						 'dailycashierreportrefund'                           => array('name'=>'Daily Cashier Report','sp'=>'ES_rptDailyCashiersReportWithRefund','file'=>'Cashier/daily cashiers report with refund.rpt','logo'=>1),
						 'paymentproof'                                       => array('name'=>'Payment Proof','sp'=>'ES_rptPaymentProoflist','file'=>'Cashier/payment prooflist.rpt','logo'=>1),
						 'deferredtuition'                                    => array('name'=>'Deferred Tuition','sp'=>'ES_rptDeferred_Payments','file'=>'Cashier/deferred payments.rpt','logo'=>1),
						 'payorbycash'                                        => array('name'=>'List of Payor By Cash','sp'=>'ES_rptListofPayorsPaidByCash','file'=>'Cashier/list of payors paid by cash 1.rpt','logo'=>0),
						 'payorbychecks'                                      => array('name'=>'List of Payor By Checks','sp'=>'ES_rptListofPayorsPaidByChecks','file'=>'Cashier/list of payors paid by checks 1.rpt','logo'=>0),
						 'payorbycredit'                                      => array('name'=>'List of Payor By Credit Card','sp'=>'ES_rptListofPayorsPaidByCard','file'=>'Cashier/list of payors paid by card 1.rpt','logo'=>0),
						 'cancelreceipts'                                     => array('name'=>'List of Void Receipts','sp'=>'ES_rptVoidOfficialReceipts_r2','file'=>'Cashier/list of cancelled official receipts 1.rpt','logo'=>0),
						 'summarylist1'                                       => array('name'=>'Transaction Summary List 1','sp'=>'ES_rptSummaryTransactionList','file'=>'Cashier/transaction summary list 1.rpt','logo'=>0),
						 'summarylist2'                                       => array('name'=>'Transaction Summary List 2','sp'=>'ES_rptSummaryTransactionList','file'=>'Cashier/transaction summary list 2.rpt','logo'=>0),
						 'summarydetailed'                                    => array('name'=>'Transaction Summary Detailed','sp'=>'ES_rptSummaryTransactionDetailed','file'=>'Cashier/transaction summary detailed 1.rpt','logo'=>0),
						 'cashreceiptbook'                                    => array('name'=>'Cash Receipt Book','sp'=>'ES_rptCashReceiptBook','file'=>'Cashier/cash receipt book 1.rpt','logo'=>0),
	                );
	
	
    private $module_name = 'reports-all';

    private $media = [
        'Title' => 'Reports',
        'Description' => 'Welcome To Reports',
        'js' => ['Reports/report-setup', 'Registrar/report-settings'],
        'init' => ['Report2.run()'],
        'plugin_js' => [
            'bootbox/bootbox.min',
            'jstree/dist/jstree.min',
            'datatables/media/js/jquery.dataTables.min',
            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
            'datatables/extensions/Scroller/js/dataTables.scroller.min',
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2.min',
            'bootstrap-datepicker/js/bootstrap-datepicker'
        ],
        'plugin_css' => [
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2',
            'jstree/dist/themes/default/style.min',
            'bootstrap-datepicker/css/datepicker'
        ],
    ];

    private $url = ['page' => 'reports/'];

    private $views = 'Reports.Views.';
    
    public function __construct()
    {
        $this->initializer();
    }

    public function index(){
        if (isParent()) {
            return redirect('/security/validation');
        }

        $rep_types = $this->model->getReports();

        //if (!empty($rep_types)) {
            $at = $this->model->AcademicTerm();
            $camps = $this->model->Campus();


            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'reports' => $rep_types, 'at' => $at, 'campus' => $camps]), 'url' => $this->url, 'media' => $this->media));
        //} else {
        //    return view(config('app.403'));
        //}
    }

    public function rptPrint2($req){

         $filename = '';
         $rep_id = $req->get('report');
         $rep_type = $this->model->getReportdetails($rep_id);

         if (!empty($rep_type)) {

           $filename = 'reports.'.str_replace('-report','',$rep_type->RepParentSlug).'.'.$rep_type->RepSlug;
          // die($filename);
           //if( file_exists( base_path('resources/views/'. str_replace('.','/',$filename).'.blade.php' ) ) ) {
            // ob_clean();
            // $width_in_mm = 279;
            // $height_in_mm= 210;
            // $html2pdf = new HTML2PDF('P', array($width_in_mm,$height_in_mm), 'en', true, 'UTF-8', array(6, 6, 6, 6));
            // // $html2pdf->setDefaultFont("Courier");
            //   // $html2pdf = new HTML2PDF();
            //
            //   $html2pdf->pdf->SetTitle('PrintPOCWithCarePlan');
            //   $content = view($filename)->render();
            //     $html2pdf->setTestTdInOnePage(false);
            //     // $html2pdf->pdf->SetProtection(array('print','copy'));
            //   $html2pdf->writeHTML($content);
            //   $html2pdf->output();
            echo view($filename)->render();
         //  }{
         //   ob_clean();
         //   echo 'Sorry, no report to view '. base_path('resources/views/'. str_replace('.','/',$filename).'.blade.php' );
         //  }

         }
    }

    public function rptPrint(Request $req)
	{     
        $this->rptPrint2($req);
        die();

        $rep_type = $this->model->getReports();

        if (!empty($rep_type)) {
            $new_parms = [];
            $subrpt = [];
            $sp_count = 5;
            $rep_id = $req->get('report-type');
            $rep_details = $this->model->where('ReportNo', $rep_id)->first();
            $dl_name = $rep_details->ReportTitle;

            // This will map params based on storedprocedure linked on $rep_params.
            // Array keys on this will reffered to the params on storedprocedure (e.g. @StudentNo = 'StudentNo')
            $vals = [
                '@StudentNo' => "'".$req->get('snum')."'",
                '@TermID' => decode($req->get('academic-term')),
                '@YearLevelID' => decode($req->get('year-level')),
                '@ProgramID' => decode($req->get('programs')),
                '@PeriodID' => decode($req->get('period')),
                '@SectionID' => decode($req->get('section')),
               // '@ExcludeEmptySubj' => !empty($req->get('print-empty-subj')) ? 1 : 0,
                '@WithLogo' => $req->get('logo'),
                '@CampusID' => 1,
            ];

            $new_sp = str_replace(array_keys($vals), array_values($vals), $rep_details->StoredProcedure);

            for ($c = 1; $c <= $sp_count; $c++) {
                $sp = ['SubStoredProcedure'.$c, 'SubRep'.$c.'_File'];

                if (!empty($rep_details->$sp[0]) && !empty($rep_details->$sp[1])) {
                    $subrpt['subreport'.$c] = [
                        'file' => $rep_details->$sp[1],
                        'query' => 'EXEC '.str_replace(array_keys($vals), array_values($vals), $rep_details->$sp[0]),
                    ];
                }
            }

            //$xpdf = new xpdf;
            $xpdf->filename = $rep_details->ReportPath.$rep_details->ReportFileName;
            // $xpdf->query = 'EXEC '.$params[0].implode(', ', $new_parms);
            $xpdf->query = 'EXEC '.$new_sp;

            $xpdf->SubReport = $subrpt;
            $data = $xpdf->generate();

            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename='.$dl_name.'.pdf');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($data['file']);
            die();

        } else {
            return view(config('app.403'));
        }
    }

    public function print_preview(Request $p)
    {

        $pdf = new PDF();

        switch($p['report']){
            case 'soa':
                $is_html =1;

                $data = array(
                    'term' => $p['term'],
                    'reg' => $p['regid'],
                );

                ob_clean();

                if($is_html == 0){

                    $pdf->setPrintHeader(false);
                    $pdf->setPrintFooter(true);
                    $pdf->SetMargins(25, 25, 25, true);
                    $pdf->setPageUnit('pt');
                    $pdf->AddPage('P','LETTER');
                    $pdf->setTitle('Print Preview');
                    $pdf->writeHTML(view('reports.finance.soa',$data)->render());

                    $pdf->output();

                } else {
                    echo view('reports.finance.soa',$data)->render();
                }

                set_time_limit(30);
                ini_set('memory_limit', '128M');

            break;
        }

    }
    
	public function export_report(){
	  $this->typeid = 27;
	  $this->extfile= 'xls';
	  $this->print_report();
	}
	
	public function print_report(){
		$this->initializer();
		$get     = Request::all();
		$qry     = '';
		$report  = getObjectValue($get,'report');
		$printby = getUserFacultyID();
		$printby = (($printby=='')?'admin':$printby);
			
		$filename= '';
		
		switch($report){
		 //Registrar
		 case 'student-profile':
		    return view('reports.registrar.student-profile');
			break;
		 case 'guardian-directory':
		    return view('reports.registrar.guardian');
			break;
		 case 'father-directory':
		    return view('reports.registrar.father');
			break;
		 case 'mother-directory':
		    return view('reports.registrar.mother');
			break;
		 case 'emergency-contact':
		    return view('reports.registrar.emergency');
			break;
		 //Accounting	
		 case 'reserved-students':
		    return view('reports.accounting.reserved-students');
			break;
		 case 'parent-monitoring':
		    return view('reports.enrollment.online-access');
			break;
		 case 'online-enrollment':
		    return view('reports.enrollment.online-enrollment');
			break;
		 case 'clinic-visit-per-department':
		   $year     = ((getObjectValue($get,'year')=='')?date('Y'):$get['year']);
		   $month    = ((getObjectValue($get,'month')=='')?date('m'):$get['month']);
		   $from     = date('Y-m-01',strtotime($year.'-'.$month.'-01'));
		   $to       = date('Y-m-t',strtotime($year.'-'.$month.'-30'));
		   
		   $filename = 'Clinic/clinic-visits-per-department.rpt';
		   $qry = "SELECT *,(SELECT COUNT(m.StudentNo) FROM ES_StudentsMedicalRecords as m 
                         INNER JOIN ES_Registrations as r ON m.TermID=r.TermID AND m.StudentNo=r.StudentNo 
					          WHERE (m.Date BETWEEN '".$from."' AND '".$to."') AND r.ProgID=y.ProgID AND r.YearLevelID=y.YLID_OldValue) as Visits 
				     FROM ESv2_YearLevel as y";
		 break;	
         case 'hourly-census':
		   $program  = ((getObjectValue($get,'programs')=='')?0:decode($get['programs']));
		   $year     = ((getObjectValue($get,'year')=='')?date('Y'):$get['year']);
		   $month    = ((getObjectValue($get,'month')=='')?date('m'):$get['month']);
		   $from     = date('Y-m-01',strtotime($year.'-'.$month.'-01'));
		   $to       = date('Y-m-t',strtotime($year.'-'.$month.'-30'));
		   
		   $filename = 'Clinic/hourly-census.rpt';
		   $qry = "SELECT TimeRange,COUNT(StudentNo) as Visits, '".$program."' as ProgID FROM (
					SELECT (CASE WHEN TimeIn BETWEEN '00:00' AND '01:00' THEN '12:00 AM - 01:00 AM' 
								 WHEN TimeIn BETWEEN '01:00' AND '02:00' THEN '01:00 AM - 02:00 AM' 
								 WHEN TimeIn BETWEEN '02:00' AND '03:00' THEN '02:00 AM - 03:00 AM' 
								 WHEN TimeIn BETWEEN '03:00' AND '04:00' THEN '03:00 AM - 04:00 AM' 
								 WHEN TimeIn BETWEEN '04:00' AND '05:00' THEN '04:00 AM - 05:00 AM' 
								 WHEN TimeIn BETWEEN '05:00' AND '06:00' THEN '05:00 AM - 06:00 AM' 
								 WHEN TimeIn BETWEEN '06:00' AND '07:00' THEN '06:00 AM - 07:00 AM' 
								 WHEN TimeIn BETWEEN '07:00' AND '08:00' THEN '07:00 AM - 08:00 AM' 
								 WHEN TimeIn BETWEEN '08:00' AND '09:00' THEN '08:00 AM - 09:00 AM' 
								 WHEN TimeIn BETWEEN '09:00' AND '10:00' THEN '09:00 AM - 10:00 AM' 
								 WHEN TimeIn BETWEEN '10:00' AND '11:00' THEN '10:00 AM - 11:00 AM' 
								 WHEN TimeIn BETWEEN '11:00' AND '12:00' THEN '11:00 AM - 12:00 PM' 
								 WHEN TimeIn BETWEEN '12:00' AND '13:00' THEN '12:00 AM - 13:00 PM' 
								 WHEN TimeIn BETWEEN '13:00' AND '14:00' THEN '13:00 PM - 14:00 PM' 
								 WHEN TimeIn BETWEEN '14:00' AND '15:00' THEN '14:00 PM - 15:00 PM' 
								 WHEN TimeIn BETWEEN '15:00' AND '16:00' THEN '15:00 PM - 16:00 PM' 
								 WHEN TimeIn BETWEEN '16:00' AND '17:00' THEN '16:00 PM - 17:00 PM' 
								 WHEN TimeIn BETWEEN '17:00' AND '18:00' THEN '17:00 PM - 18:00 PM' 
								 WHEN TimeIn BETWEEN '18:00' AND '19:00' THEN '18:00 PM - 19:00 PM' END) as TimeRange, 
						  TimeIn,
						  r.ProgID,
						  r.YearLevelID,
						  r.StudentNo 
					  FROM ES_StudentsMedicalRecords as m
					  INNER JOIN ES_Registrations as r ON m.StudentNo=r.StudentNo AND m.TermID=r.TermID
					  WHERE (m.Date BETWEEN '".$from."' AND '".$to."') ".(($program>0)?(" AND r.ProgID='".$program."'"):'').") as p GROUP BY TimeRange ORDER BY TimeRange";
		   			  
         break;
         case 'top-10-illness':
		   $program  = ((getObjectValue($get,'programs')=='')?0:decode($get['programs']));
		   if($program==''){
		    $dept    = 'All Department';
		   }else if($program=='1'){
		    $dept    = 'LOWER SCHOOL';
		   }elseif($program=='7'){
		    $dept    = 'UPPER SCHOOL';
		   }elseif($program=='8'){
		    $dept    = 'MIDDLE SCHOOL';
		   }
		   
		   $year     = ((getObjectValue($get,'year')=='')?date('Y'):$get['year']);
		   $month    = ((getObjectValue($get,'month')=='')?date('m'):$get['month']);
		   $from     = date('Y-m-01',strtotime($year.'-'.$month.'-01'));
		   $to       = date('Y-m-t',strtotime($year.'-'.$month.'-30'));
		   
		   $filename = 'Clinic/top-10-illness.rpt';
		   $qry = "SELECT Category,COUNT(StudentNo) as Visits,'".date('F',strtotime($from))."' as Month,'".$dept."' as Programs FROM (
					SELECT m.Category,
						   r.ProgID,
						   r.YearLevelID,
						   r.StudentNo 
					  FROM ES_StudentsMedicalRecords as m
					  INNER JOIN ES_Registrations as r ON m.StudentNo=r.StudentNo AND m.TermID=r.TermID
					  WHERE (m.Date BETWEEN '".$from."' AND '".$to."') ".(($program>0)?(" AND r.ProgID='".$program."'"):'').") as p GROUP BY Category ORDER BY Visits DESC";
		 break;		 
		 case 'top-5-most-utilized-supplies-used':
		   $program  = ((getObjectValue($get,'programs')=='')?0:decode($get['programs']));
		   if($program==''){
		    $dept    = 'All Department';
		   }else if($program=='1'){
		    $dept    = 'LOWER SCHOOL';
		   }elseif($program=='7'){
		    $dept    = 'UPPER SCHOOL';
		   }elseif($program=='8'){
		    $dept    = 'MIDDLE SCHOOL';
		   }
		   
		   $year     = ((getObjectValue($get,'year')=='')?date('Y'):$get['year']);
		   $month    = ((getObjectValue($get,'month')=='')?date('m'):$get['month']);
		   $from     = date('Y-m-01',strtotime($year.'-'.$month.'-01'));
		   $to       = date('Y-m-t',strtotime($year.'-'.$month.'-30'));
		   
		   $filename = "Clinic/top-5-most-utilized-supplies-used.rpt";
		   $qry = "SELECT Supplies,COUNT(StudentNo) as Visits,'".date('F',strtotime($from))."' as Month,'".$dept."' as Programs FROM (
					SELECT m.Supplies,
						   r.ProgID,
						   r.YearLevelID,
						   r.StudentNo 
					  FROM ES_StudentsMedicalRecords as m
					  INNER JOIN ES_Registrations as r ON m.StudentNo=r.StudentNo AND m.TermID=r.TermID
					  WHERE (m.Date BETWEEN '".$from."' AND '".$to."') ".(($program>0)?(" AND r.ProgID='".$program."'"):'')." AND m.Supplies NOT IN ('','N/A','none')) as p GROUP BY Supplies ORDER BY Visits DESC";
		 
		  //die($qry);
		 break;
		 case 'top-5-student-that-visits-the-clinic-in-a-month': 
		   $program  = ((getObjectValue($get,'programs')=='')?0:decode($get['programs']));
		   if($program==''){
		    $dept    = 'All Department';
		   }else if($program=='1'){
		    $dept    = 'LOWER SCHOOL';
		   }elseif($program=='7'){
		    $dept    = 'UPPER SCHOOL';
		   }elseif($program=='8'){
		    $dept    = 'MIDDLE SCHOOL';
		   }
		   
		   $year     = ((getObjectValue($get,'year')=='')?date('Y'):$get['year']);
		   $month    = ((getObjectValue($get,'month')=='')?date('m'):$get['month']);
		   $from     = date('Y-m-01',strtotime($year.'-'.$month.'-01'));
		   $to       = date('Y-m-t',strtotime($year.'-'.$month.'-30'));
		   
		   $filename = "Clinic/top-5-students.rpt";
		   $qry = "SELECT x.*,s.Fullname,
						   y.YearLevelCode,y.YearLevelName,
						   m.Date,CONVERT(VARCHAR(250),m.TimeIn,0) as TimeIn,CONVERT(VARCHAR(250),m.TimeOut,0) as TimeOut,
						   m.Complaint,m.Category,m.Diagnosis,
						   '".$dept."' as Program,
						   '".date('F',strtotime($from))."' as xMonth,
						   '".$year."' as xYear
					  FROM (SELECT m.StudentNo,r.TermID,r.ProgID,r.YearLevelID,COUNT(m.Date) as Visits FROM ES_StudentsMedicalRecords as m
						INNER JOIN ES_Registrations as r ON m.StudentNo=r.StudentNo AND m.TermID=r.TermID
							 WHERE (m.Date BETWEEN '".$from."' AND '".$to."') ".(($program>0)?(" AND r.ProgID='".$program."'"):'')."
						  GROUP BY m.StudentNo,r.TermID,r.ProgID,r.YearLevelID) as x 
					INNER JOIN ES_Students as s ON x.StudentNo=s.StudentNo
					INNER JOIN ES_StudentsMedicalRecords as m ON x.StudentNo=m.StudentNo AND x.TermID=m.TermID
					INNER JOIN ESv2_YearLevel as y ON x.ProgID=y.ProgID AND x.YearLevelID=y.YLID_OldValue
						 WHERE (m.Date BETWEEN '".$from."' AND '".$to."') ".(($program>0)?(" AND r.ProgID='".$program."'"):'')." AND x.Visits>1  
					  ORDER BY x.Visits DESC,m.Date ASC";
		 
		 break;
		 case 'all-subjects-of--student-grades':
		 case 'all-subjects-of-student-grades':
			return view('reports\academics\all-subjects-of-student-grades');
			die();
		 break;
		 case 'comparative-class-average-of-students':
			return view('reports\academics\comparative-class-average-of-students');
			die();
		 break;
		 case 'distribution-of-grades-per-class':
			return view('reports\academics\distribution-of-grades-per-class');
			die();
		 break;
		 default:
		  $subrpt = array();
		  if (@array_key_exists($report,$this->xreports)){
		    $rpdata = $this->xreports[$report];
		    if($this->xreports[$report]['logo']==1){
			 $subrpt['subreport1'] = array('file' =>'Company_Logo','query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'"); 
			}
			
		    $term     = ((getObjectValue($get,'term')=='')?1:decode($get['term']));
		    $campus   = ((getObjectValue($get,'campus')=='')?1:decode($get['campus']));
		    $asof     = ((getObjectValue($get,'date')=='')?date('Y-m-d'):$get['date']);
			$filename = $this->xreports[$report]['file'];
		    switch($report){
			  //Accounting Reports
			  case 'account-receivable---enrollment':
			  case 'account-receivable---enrollment-[layout-2]':
			   $qry      = "EXEC ES_rptAccountReceivables '".$term."','".$campus."','','','0','".$asof."','0','0','1','0','".$printby."'";
			   break;
			  case 'account-receivable---enrollment-tuition-fee-1':
			  case 'account-receivable---enrollment-tuition-2':
			   $qry      = "EXEC rpt_AccountReceivables_Tuition '62CFAB983ACB39E7B46BD34ACC0C4ABB','".$asof."','".$printby."'";  
			   break;
			  case 'account-receivable---enrollment-[selected-account]':
			   $qry      = "EXEC ES_RptAccountReceivable_Enrollment_SelectedItems 'E1C3E75C602BCA6B6DC069A955119496','".$term."','".$campus."','','','0','".$printby."'";
			   break;
			  case 'summary-of-billing-and-collection':
			  case 'sum.-of-billing-and-collection-with-financial-aid':
			  case 'list-of-fully-paid---enrollment':
			  case 'list-of-fully-paid---enrollment-2':
			   $progid   = ((getObjectValue($get,'programs')=='')?0:decode($get['programs']));
			   $byprogid = (($progid>0)?1:0);
			   $qry      = "EXEC ".$rpdata['sp']." '".$term."','0','0','".$byprogid."','".$progid."','".$asof."','".$printby."'";
			   break;
			  case 'summary-of-assessment':
			   $progid   = ((getObjectValue($get,'programs')=='')?0:decode($get['programs']));
			   $byprogid = (($progid>0)?1:0);
			   $qry      = "EXEC ".$rpdata['sp']." '".$term."','".$campus."', '0', '', '0', '0','13', '14', '75','".$printby."'";
			   break;
			  //Cashier Reports 
			  case 'collection-report-by-payor-1':
			  case 'collection-report-by-payor-2':
			  case 'collection-report-by-payor-3':
			  case 'collection-report-by-payor-4':
			   $option   = getObjectValue($get,'optionbox');
			   $option   = (($option==1)?0:1);
			   $dtfrom   = getObjectValue($get,'dtfrom');
			   $dtto     = getObjectValue($get,'dtto');
			   $orfrom   = getObjectValue($get,'or_from');
			   $orto     = getObjectValue($get,'or_to');
			   $cashier  = getObjectValue($get,'cashier_id');
			   $cashopt  = (($cashier=='')?1:0);
			   
			   if($option==1){
				$src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				$src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
			   }else{
			    $src_fr = $orfrom;
				$src_to = $orto;
			   }
			   
			   $qry      = "EXEC ES_rptCollectionReportByPayor 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
			   $subrpt['subreport1'] = array('file' =>'checks','query' => "EXEC ES_rptCollectionReportByPayor_BankChecks '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."'");
			   $subrpt['subreport2'] = array('file' =>'VoidORs','query' => "EXEC ES_rptCollectionReport_VoidORs '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."'");
               break;			
			  case 'collection-report-by-department-1':
			  case 'collection-report-by-department-2':
			  case 'collection-report-by-academic-dept-1':
			  case 'collection-report-by-academic-dept-2':
			  case 'collection-report-by-yearlevel-1':
			  case 'collection-report-by-yearlevel-2':
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
			    }
			   
			    $qry = "EXEC ".$rpdata['sp']." 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
				break;
			  case 'detailedcollect1':
			  case 'detailedcollect2':
			  case 'detailedcollectcampus':
			    if($report=='detailedcollectcampus'){
					die('Report Not Available.');
				}
				
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
			    }
				
				$qry = "EXEC ES_rptDetailedCollectionReport 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
				break;
			  case 'summarybysubaccounts':
			  case 'summarybyaccounts':
			  case 'summarybyfunds':
			  case 'summarybycollege':	
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
			    }
				
                $qry = "EXEC  ".$rpdata['sp']." 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
                $subrpt['subreport1'] = array('file' =>'header','query' => "EXEC ES_rptSummaryCollectionHeader 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$orfrom."', '".$orto."', '".$dtfrom."', '".$dtto."', '".$cashopt."'");
                break;				
			  case 'annualsummary':
                $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			    $xyear    = date('Y');
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $xyear  = date('Y',strtotime($src_fr));
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
			    }
				
                $qry = "EXEC  ".$rpdata['sp']." '".$xyear."', '".$printby."'";
                break;			  
			  case 'annualsummarydetailed':
                $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			    $xyear    = date('Y');
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $xyear  = date('Y',strtotime($src_fr));
			    }else{
				 die('Invalid Parameter');
			    }
				
                $qry = "EXEC ES_rptSummaryofCollectionPerAccount_Annual '".$src_fr."','".$src_to."',''";
                break;			  
			  case 'listofpayorbyaccount':
                $account  = getObjectValue($get,'accountid');
                $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			    $xyear    = date('Y');
			    if($option==1){
				 $option = 0;
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $xyear  = date('Y',strtotime($src_fr));
			    }else{
				 die('Invalid Parameter');
			    }
				
				if($account=='' || $account==false){
				 die('Select a specific account');
				}else{
				 $account = decode($account);
				}
				
                $qry = "EXEC ES_rptListofPayorsByAccount 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$account."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$cashier."', '".$printby."'";
                break;			  
			  case 'listofpayorpersemester':
			  case 'listofpayorclassification':
			  case 'listofpayorsubsidiary':
                $term     = getObjectValue($get,'term');
                $account  = getObjectValue($get,'accountid');
                $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			    $xyear    = date('Y');
			    if($option==1){
				 $option = 0;
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $xyear  = date('Y',strtotime($src_fr));
			    }else{
				 die('Invalid Parameter');
			    }
				
				if($account=='' || $account==false){
				 die('Error: Select a specific account');
				}else{
				 $account = decode($account);
				}
				
				if($report=='listofpayorpersemester'){
				$qry = "EXEC ".$rpdata['sp']." '".$term."', '".$account."', '".$src_fr."', '".$src_to."', '".$cashier."', '".$printby."'";
				}elseif($report=='listofpayorsubsidiary'){
				$qry = "EXEC ".$rpdata['sp']." '1', '1','".$option."', '".$src_fr."', '".$src_to."'";
				}elseif($report=='listofpayorclassification'){
				$qry = "EXEC ".$rpdata['sp']." 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$account."','".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$cashier."', '".$printby."'";
				}
                break;	 
			  case 'dailycashierreport':
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $orstart= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date ASC");
				 if($orstart && count($orstart)>0){
					$orfrom = $orstart[0]->ORNo;
				 }
				 $orfinal= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date DESC");
				 if($orfinal && count($orfinal)>0){
					$orto = $orfinal[0]->ORNo;
				 }
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
				 $dtstart= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_fr."'");
				 if($dtstart && count($dtstart)>0){
					$dtfrom = date('m/d/Y H:i A',strtotime($dtstart[0]->Date));
				 }
				 
				 $dtfinal= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_to."'");
				 if($dtfinal && count($dtfinal)>0){
					$dtto = date('m/d/Y H:i A',strtotime($dtfinal[0]->Date));
				 }
			    }
				
                $qry = "EXEC  ".$rpdata['sp']." 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
                $subrpt['subreport1'] = array('file' =>'header','query' => "EXEC ES_rptDailyCashiersReportHeader 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."', '".$option."', '".$orfrom."', '".$orto."', '".$dtfrom."', '".$dtto."', '1'"); 
			    break;	 
			  case 'dailycashierreportrefund':
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $orstart= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date ASC");
				 if($orstart && count($orstart)>0){
					$orfrom = $orstart[0]->ORNo;
				 }
				 $orfinal= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date DESC");
				 if($orfinal && count($orfinal)>0){
					$orto = $orfinal[0]->ORNo;
				 }
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
				 $dtstart= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_fr."'");
				 if($dtstart && count($dtstart)>0){
					$dtfrom = date('m/d/Y H:i A',strtotime($dtstart[0]->Date));
				 }
				 
				 $dtfinal= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_to."'");
				 if($dtfinal && count($dtfinal)>0){
					$dtto = date('m/d/Y H:i A',strtotime($dtfinal[0]->Date));
				 }
			    }
				
                $qry = "EXEC  ES_rptDailyCashiersReportWithRefund '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
                break;				
			  case 'paymentproof':
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $orstart= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date ASC");
				 if($orstart && count($orstart)>0){
					$orfrom = $orstart[0]->ORNo;
				 }
				 $orfinal= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date DESC");
				 if($orfinal && count($orfinal)>0){
					$orto = $orfinal[0]->ORNo;
				 }
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
				 $dtstart= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_fr."'");
				 if($dtstart && count($dtstart)>0){
					$dtfrom = date('m/d/Y H:i A',strtotime($dtstart[0]->Date));
				 }
				 
				 $dtfinal= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_to."'");
				 if($dtfinal && count($dtfinal)>0){
					$dtto = date('m/d/Y H:i A',strtotime($dtfinal[0]->Date));
				 }
			    }
				
                $qry = "EXEC ES_rptPaymentProoflist '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '".$printby."'";
                break;
			  case 'deferredtuition':
			    $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			   
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $orstart= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date ASC");
				 if($orstart && count($orstart)>0){
					$orfrom = $orstart[0]->ORNo;
				 }
				 $orfinal= DB::select("SELECT TOP 1 ORNo FROM ES_OfficialReceipts WHERE [Date] BETWEEN '".$src_fr."' AND '".$src_to."' ORDER BY Date DESC");
				 if($orfinal && count($orfinal)>0){
					$orto = $orfinal[0]->ORNo;
				 }
			    }else{
			     $src_fr = $orfrom;
				 $src_to = $orto;
				 $dtstart= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_fr."'");
				 if($dtstart && count($dtstart)>0){
					$dtfrom = date('m/d/Y H:i A',strtotime($dtstart[0]->Date));
				 }
				 
				 $dtfinal= DB::select("SELECT TOP 1 * FROM ES_OfficialReceipts WHERE ORNo='".$src_to."'");
				 if($dtfinal && count($dtfinal)>0){
					$dtto = date('m/d/Y H:i A',strtotime($dtfinal[0]->Date));
				 }
			    }
				
                $qry = "EXEC ES_rptDeferred_Payments '".$option."', '".$src_fr."', '".$src_to."', '".$cashier."', '".$printby."'";
                break; 
			  case 'payorbycash':
			  case 'payorbycredit':
			  case 'cancelreceipts':
			  case 'summarylist1':
			  case 'summarylist2':
			  case 'summarydetailed':
			  case 'cashreceiptbook':
                $account  = getObjectValue($get,'accountid');
                $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			    $xyear    = date('Y');
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $xyear  = date('Y',strtotime($src_fr));
			    }else{
				 $src_fr = $orfrom;
				 $src_to = $orto;
				}
				
			    if($report=='cashreceiptbook'){
					$subrpt['subreport1'] = array('file' =>'summary','query' => "EXEC ES_rptCashReceiptBook_Summary 'Prince technologies', 'Republic', 'Campusaddress', 'campusname', '".$cashier."','".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'");
				}
				
                $qry = "EXEC ".$rpdata['sp']." 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."','".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
                break;
			  case 'payorbycheck':
			  case 'payorbychecks':
                $account  = getObjectValue($get,'accountid');
                $option   = getObjectValue($get,'optionbox');
			    $option   = (($option==1)?0:1);
			    $dtfrom   = getObjectValue($get,'dtfrom');
			    $dtto     = getObjectValue($get,'dtto');
			    $orfrom   = getObjectValue($get,'or_from');
			    $orto     = getObjectValue($get,'or_to');
			    $cashier  = getObjectValue($get,'cashier_id');
			    $cashopt  = (($cashier=='')?1:0);
			    $xyear    = date('Y');
			    if($option==1){
				 $src_fr = ((strtotime($dtfrom))?date('Y-m-d 00:00',strtotime($dtfrom)):date('Y-m-d 00:00'));
				 $src_to = ((strtotime($dtto))?date('Y-m-d 23:59',strtotime($dtto)):date('Y-m-d 23:59'));
				 $xyear  = date('Y',strtotime($src_fr));
			    }else{
				 $src_fr = $orfrom;
				 $src_to = $orto;
				}
				
                $qry = "EXEC ES_rptListofPayorsPaidByChecks 'Everest Academy', 'Republic of the Philippines', '3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines', 'Alabang Campus', '".$cashier."','".$option."', '".$src_fr."', '".$src_to."', '".$cashopt."', '".$printby."'";
				$subrpt['subreport1'] = array('file' =>'checks','query' => "EXEC ES_rptListofPayorsPaidByChecks_BankChecks '".$cashier."', '".$option."', '".$src_fr."', '".$src_to."', '1'"); 			    
                break;			 
			  default:
			  $report   = '';
			  $filename = '';
			  break;
			}
			
			if(count($subrpt)>0){
			 $this->crystal->SubReport = $subrpt;				   
		    }
		  }else{
			$report   = '';
			$filename = '';
		  }
		 break;
		}
		
		if($report=='' || $filename==''){
			echo 'No Report Selected';
		}else{
		    
			if($this->extfile=='xls'){
			 $this->crystal->formatType = $this->typeid;
			 $this->crystal->extension  = $this->extfile;
			}
			
			$this->crystal->filename = $filename;
			$this->crystal->query    = $qry;
			$data = $this->crystal->generate();
			
			if($this->extfile=='pdf'){
				$filename  = str_replace('rpt','pdf',$filename);
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="' .$filename. '"');
				header('Content-Transfer-Encoding: binary');
				header('Accept-Ranges: bytes');
				@readfile($data['file']);
			}else{
				$filename  = str_replace('rpt','xls',$filename);
				$xls = url('assets/system/reports/'.$filename);
				header("Location:".$xls);
				exit();
			}
		}
	}
	
	public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax())
        {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch ($post['event']) {

                case 'get-form':

                    $mode  = getObjectValue($post,'mod');
                    $slug  = getObjectValue($post,'slug');
                    $table = '';
                    $data= array(
                        'progs' => $this->model->Programs(),
                        'level' => $this->model->getYearLevel()
                    );

                    switch($mode){
                        case 'cashier-report': $table = view($this->views.'cashier' )->render();break;
                        case 'accounting-report':
                        case 'finance-report': 
						    if($slug=='account-receivable---enrollment-[selected-account]'){
								$data['account'] = DB::SELECT("SELECT * FROM ES_Accounts WHERE Inactive=0");
							}
							$table = view($this->views.'finance',$data )->render(); 
							break;
                        case 'enrollment-report': $table = view($this->views.'enrollment', $data )->render(); break;
                        case 'admission-report': $table = view($this->views.'admission' )->render(); break;
                        case 'academic-report': $table = view($this->views.'academic', $data )->render(); break;
                        case 'clinic-reports':
							$data['monthly'] = true;
							$table           = view($this->views.'clinic', $data)->render();
							break;	
                        case 'guidance-reports':
                        case 'registrar-reports': $table = view($this->views.'registrar', $data)->render(); break;
                        default: break;
                    }

                    $response = ['error'=>false,'message'=>'Successfully Save!', 'view' => $table ];

                break;

                case 'get-year-level':

                    $cls = decode(Request::get('pclass'));

                    $data = $this->model->getYearLevel($cls);

                    if(!empty($data)){
                        $vw = view($this->views.'sub.year-level', ['yl' => $data])->render();
                        $response = ['error' => false, 'html' => $vw, 'period'  ];
                    } else {
                        $response = ['error' => true, 'message' => 'No year level found.'];
                    }

                    break;

                case 'get-sections':

                    $yl_id = decode(Request::get('yl'));
                    $term_id = decode(Request::get('t'));
                    $prog_id = decode(Request::get('p'));

                    $data = $this->model->getSections($term_id, $prog_id, $yl_id);

                    if(!empty($data)){

                        $subjectlist = "";

                        foreach($data AS $r){
                            $subjectlist .= $r->SectionID .',';
                        }

                        $subjects = $this->model->getSubjects($term_id, $subjectlist );

                        $vw = view($this->views.'sub.section', ['sec' => $data])->render();
                        $subj = view($this->views.'sub.subjects', ['subjects' => $subjects])->render();

                        $response = ['error' => false, 'html' => $vw , 'subjects' => $subj ];
                    } else {
                        $response = ['error' => true, 'message' => 'No section(s) found.'];
                    }

                    break;
                case 'get-students':

                        $find = Request::get('student-name');

                        // if(!empty($find)){
                            $term_id = decode(Request::get('academic-term'));
                            $prog_id = decode(Request::get('programs'));
                            $yl_id = decode(Request::get('year-level'));
                            $section_id = decode(Request::get('section'));
                            $data = $this->model->getStudent($find, $term_id, $prog_id, $yl_id, $section_id);

                            if(!empty($data)){
                                $vw  = view($this->views.'sub.students', ['data' => $data])->render();
                                $response = ['error' => false, 'html' => $vw];

                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }

                    break;

                    case 'get-report':
                    $rep_id = Request::get('rep_id');
                    $rep_type = $this->model->getReportdetails($rep_id);
                    $response = ['error'=>false,'message'=>'Successfully Save!', 'rep' => $rep_type->RepName ];

                    break;


                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }




    private function initializer(){
        $this->services = new Services();
        $this->model = new Model();
		$this->crystal = new crystal();
        // $this->permission = new Permission($this->module_name);
    }
}
