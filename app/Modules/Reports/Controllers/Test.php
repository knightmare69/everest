<?php

namespace App\Modules\Reports\Controllers;

use App\Http\Controllers\Controller;

use Mail;
use Request;
use Response;
use Permission;
use DB;

class Test extends Controller{
  public function index(){
    $report = Request::get('rid');
	if($report=='2')
      return view('reports.transcripts.senior-high-school-student-permanent-record-2',array());
	else
	  return view('reports.transcripts.senior-high-school-student-permanent-record-1',array());
	
  }
}
?>