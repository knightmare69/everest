<?php
    $at = getAYTerm();
    $campus = get_campus();
    $i=0;
	$export = ((Request::get('slug')=='reserved-students')?'hide':'');
?>
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Academic Term</label>
                <select class="select2 form-control" name="term" id="term">
                    @if(!empty($at))
                        @foreach($at as $d)
                        <option value="{{ encode($d->TermID) }}">{{ $d->AYTerm }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
             <div class="form-group">
                <label class="control-label">Campus</label>
                <select class="form-control" name="campus" id="campus">
                    @if(!empty($campus))
                    @foreach($campus as $_this)
                    <option value="{{ encode($_this->CampusID) }}">
                        {{ $_this->ShortName }}
                    </option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="row">


        <div class="col-md-12" >
            <div class="form-group">
                <label class="control-label">Department</label>
                <select class="form-control" name="programs" id="programs">
                    <option value="MA==" selected >All Department</option>
                    @if(!empty($progs))
                        @foreach($progs as $_this)
                        <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                            {{ $_this->ProgName }}
                        </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>


    </div>

    {{-- <div class="row">
        <div class="col-md-12 rep-inp-hold" id="yl-div">
            <div class="form-group">
                <label class="control-label">Year Level</label>
                <select class="select2 form-control" name="year-level" id="year-level" disabled>

                </select>
            </div>
        </div>
    </div> --}}
    <div class="row">
        <div class="col-md-12 rep-inp-hold">
            <div class="form-group">
                <label class="control-label">Date as of:</label>
                <div class="input-group input-xlarge date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                  <input type="text" id="date"  name="date" value="{{date("m/d/Y")}}" class="form-control date-picker " />
                  </div>

            </div>
        </div>
    </div>
	<?php
	if(isset($account) && count($account)>0){
	?>
    <div class="row">
        <div class="col-md-12 rep-inp-hold">
            <div class="form-group">
                <label class="control-label">Account:</label>
                <div class="input-group input-xlarge">
                  <select id="acct"  name="acct" class="form-control required">
				    <?php 
					  foreach($account as $acct){
						echo '<option value="'.$acct->AcctID.'">'.$acct->AcctName.' ('.$acct->AcctCode.')</option>';
					  }
					?>
				  </select>
                </div>
            </div>
        </div>
    </div>
    <?php 
	}
	?>

</div>
<div class="form-actions right">
    <button class="btn pull-left hide default" style="margin-left: 10px;" id="btnsetup" type="button"><i class="fa fa-cog"></i> Setup </button>
    <!--<button class="btn default" id="export" type="button">Export</button>-->
	<button class="btn green <?php echo $export;?>" type="button" id="btn_export"><i class="fa fa-download"></i> Export Report</button>
    <button class="btn blue " type="button" id="btn_print"><i class="fa fa-print"></i> Print Report</button>
</div>

<script>
    $('.date-picker').datepicker();
</script>
