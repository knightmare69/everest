<div class="form-body">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group">

                <label class="control-label">Academic Year and Term:</label>

                <div class="input-group input-xlarge">
					<select class="form-control" id="term" name="term">
						<?php
							$term = DB::select("SELECT * FROM ES_AYTerm");
							foreach($term as $rs){
							 echo '<option value="'.$rs->TermID.'" '.(($rs->Active_OnlineEnrolment==1)?'selected':'').'>'.$rs->AcademicYear.' '.$rs->SchoolTerm.'</option>';
							}
						?>
					</select>
				</div>
				<!-- /input-group -->
				<span class="help-block">Select term </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="form-group">

                <label class="control-label"><input type="checkbox" class="optbox" name="optionbox" value="0"  checked="" /> Date Covered</label>

                <div class="input-group input-xlarge date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
					<input type="text" id="dtfrom" name="dtfrom" value="{{date('m/d/Y')}}" class="form-control date-picker " />
					<span class="input-group-addon">
					to </span>
                    <input type="text" id="dtto" name="dtto" value="{{date('m/d/Y')}}" class="form-control date-picker" />
				</div>
				<!-- /input-group -->
				<span class="help-block">Select date range </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">

                <label class="control-label"><input type="checkbox" class="optbox" name="optionbox" value="1" /> Official Receipts</label>

                <div class="input-group input-xlarge ">
					<input type="text" id="or_from" name="or_from" value="" class="form-control " />
					<span class="input-group-addon">
					to </span>
                    <input type="text" id="or_to" name="or_to" value="" class="form-control" />
				</div>
				<!-- /input-group -->
				<span class="help-block">Select date range </span>
            </div>
        </div>
    </div>
    <div class="row">
         <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Cashier</label>
                <select class="select2 form-control" name="cashier_id" id="cashier_id">
                    <?php
					$cashier = DB::select('SELECT DISTINCT CreatedBy FROM es_officialreceipts');
					$cashier = json_decode(json_encode($cashier), true);
				  //$user = DB::table('esv2_users')->whereIn('UserIDX', $cashier)->get();
				    $user = DB::table('esv2_users')->whereIn('FacultyID', $cashier)->get();
                   ?>
                     <option value="" >All Cashier</option>
                    @foreach($user as $key => $_this)
                    <option value="{{($_this->UserIDX) }}" >
                        {{ ($_this->FullName )}}
                    </option>
                    @endforeach
                </select>
                {{-- <input type="text" value="" class="form-control" name="cashier_id" /> --}}
               	{{-- <span class="help-block">Leave it blank for all cashier option</span> --}}
            </div>
        </div>
        @if(1==1)
        <div class="col-md-6" id="acctdiv">
            <div class="form-group">
                <label class="control-label">Account:</label>
                <select class="select2 form-control" name="accountid" id="accountid">
					<option value="" selected>All Account</option>
                    <?php 
					$slug = getObjectValue(Request::all(),'slug');
					$user = App\Modules\Accounting\Models\ChartOfAccounts::get(); 
					if($slug=='listofpayorclassification'){
					$user = DB::SELECT("SELECT ClassID as AcctID,ClassName As AcctName FROM ES_AccountsClass");
					}elseif($slug=='listofpayorsubsidiary'){
					$user = DB::SELECT("SELECT IndexID as AcctID,SubCode As AcctName FROM ES_SubsidiaryCode");
					}
					?>
                    @foreach($user as $key => $_this)
                    <option value="{{ encode($_this->AcctID) }}">
                        {{  $_this->AcctName }}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
      @endif
    </div>
</div>
<div class="form-actions right">
    <button class="btn green " type="button" id="btn_export"><i class="fa fa-download"></i> Export Report</button>
    <button class="btn blue " type="button" id="btn_print"><i class="fa fa-print"></i> Print Report</button>
</div>
<script>
    $('.date-picker').datepicker();
</script>
