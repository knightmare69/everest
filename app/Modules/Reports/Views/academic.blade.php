<?php
    $at = getAYTerm();
    $campus = get_campus();
    $i=0;
?>
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Academic Term</label>
                <select class="select2 form-control" name="academic-term" id="academic-term">
                    @if(!empty($at))
                        @foreach($at as $d)
                        <option value="{{ encode($d->TermID) }}">{{ $d->AYTerm }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
             <div class="form-group">
                <label class="control-label">Campus</label>
                <select class="form-control" name="campus" id="campus">
                    @if(!empty($campus))
                        @foreach($campus as $_this)
                        <option value="{{ encode($_this->CampusID) }}">
                            {{ $_this->ShortName }}
                        </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 " >
            <div class="form-group">
                <label class="control-label">Department</label>
                <select class="form-control" name="programs" id="programs" required>
                    <option value="" selected >- Select One -</option>
                    @if(!empty($progs))
                        @foreach($progs as $_this)
                        <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                            {{ $_this->ProgName }}
                        </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="form-group">
                <label class="control-label">Grade Level</label>
                <select class="form-control" name="year-level" id="year-level" disabled required>
                    <option value="" selected disabled> - Select one - </option>
                    @if(!empty($level))
                        @foreach($level as $d)
                        <option value="{{ encode($d->YearLevelID) }}">
                            {{ $d->YearLevel }}
                        </option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>
    </div>
     <div class="row" data-mode="schedules">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Section</label>
                <select class="select2 form-control" name="section" id="section" disabled>
                    <option value="" selected >All Section/s</option>
                </select>
            </div>
        </div>
		
        <div class="col-md-6  rep-inp-hold" id="period-div">
          <div class="form-group">
              <label class="control-label">Period</label>
              <select class="select2 form-control" name="period" id="period">
                  <option value="" selected> - All Period - </option>
				  <?php $qtr = App\Modules\Registrar\Services\RegistrarServiceProvider::Period(); ?>
                  @foreach($qtr as $key => $_this)
                  <option value="{{ encode($_this->PeriodID) }}" data-group="{{ $_this->GroupClass }}" class="{{ ($_this->GroupClass == 21) ? 'hide' : '' }}">
                      {{ ($_this->GroupClass == 21) ? $_this->Description1 : $_this->Description2 }}
                  </option>
                  @endforeach
              </select>
          </div>
		</div>
        
		<div class="col-md-6 hidden">
            <div class="form-group">
                <label class="control-label">Subject</label>
                <select class="select2 form-control" name="subject" id="subject" disabled>
                    <option value="" selected >All Subject/s</option>
                </select>
            </div>
        </div>
    </div>

       <div class="col-md-6 hidden">
           <div class="form-group">
               <label class="control-label">Output</label>
               <select class="select2 form-control" name="output" id="output">
                   <option  value="print" selected >PDF/Print</option>
                   <option value="excel"  >EXCEL</option>
               </select>
           </div>
       </div>


   </div>
   <!--
   <div class="row" data-mode="month">     
         <div class="col-md-6">
             <div class="form-group">
                 <label class="control-label">Month</label>
                 <select class="form-control input-small " id="month" name="month" >
                        @foreach(getMonths() as $m => $k)
                        <option value="{{$m}}" <?= (date('m')) == $m ? 'selected':''  ?> > <?= $k ?> </option>
                        @endforeach
                    </select>
             </div>
         </div>
  
  
   </div>
   -->

</div>
<div class="form-actions right">
    <button class="btn pull-left hide default" style="margin-left: 10px;" id="btnsetup" type="button"><i class="fa fa-cog"></i> Setup </button>
    <!--<button class="btn default" id="export" type="button">Export</button>-->
    <button class="btn blue " type="button" id="btn_print"><i class="fa fa-print"></i> Print Report</button>
</div>
