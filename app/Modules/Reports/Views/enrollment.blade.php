<?php
    $at = getAYTerm();
    $campus = get_campus();
    $i=0;
?>
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Academic Term</label>
                <select class="select2 form-control" name="term" id="term">
                    @if(!empty($at))
                        @foreach($at as $d)
                        <option value="{{ encode($d->TermID) }}">{{ $d->AYTerm }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
             <div class="form-group">
                <label class="control-label">Campus</label>
                <select class="form-control" name="campus" id="campus">
                    @if(!empty($campus))
                    @foreach($campus as $_this)
                    <option value="{{ encode($_this->CampusID) }}">
                        {{ $_this->ShortName }}
                    </option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 " id="prog-div">
            <div class="form-group">
                <label class="control-label">Department</label>
                <select class="form-control" name="programs" id="programs">
                    <option value=""  selected >All Department</option>
                    @if(!empty($progs))
                    @foreach($progs as $_this)
                    <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                        {{ $_this->ProgName }}
                    </option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-sm-6">
			<div class="form-group">
                <label class="control-label">As Of</label>
				<input class="form-control datepicker" type="date" name="asof" id="asof" data-dateformat="yyyy-mm-dd"/>
			</div>
		</div>
	</div>
</div>
<div class="form-actions right">
    <button class="btn pull-left hide default" style="margin-left: 10px;" id="btnsetup" type="button"><i class="fa fa-cog"></i> Setup </button>
    <!--<button class="btn default" id="export" type="button">Export</button>-->
    <button class="btn blue " type="button" id="btn_print"><i class="fa fa-print"></i> Print Report</button>
</div>
