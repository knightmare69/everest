<?php
 // error_print($reports);
// die();
?>
<div class="row">

    <div class="col-md-4">

        <div class="portlet bordered light ">
            <div class="portlet-title">
                <div class="caption">List of Report</div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body" id="tvreports">
                <div class="scroller" style="height:450px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd" >
    	            <div id="tree_report">
                    <ul>
                        @foreach($reports as $key => $v)
                        <li data-jstree='{ "opened" : true }' >
                        {{ $key }}
                            <ul>

                                @foreach($v as $d)
                                <?php
                                if($d->SP != ''){

                                    $find = explode('@', str_replace(', ', '', $d->SP));
                                    unset($find[0]);
                                    $find =  implode(',', $find);
                                }else{
                                    $find='';
                                }
                                ?>

        						<li data-parent='{{$d->RepParentSlug}}' data-slug="{{$d->RepSlug}}" data-value="{{ $d->RepID }}" data-find='{{$find}}'  data-jstree='{ "icon" : "fa fa-file icon-state-success ",  }'>
        							 {{ $d->RepName }}
        						</li>

                                @endforeach
                            </ul>
                        </li>
                        @endforeach
					</ul>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="col-md-8">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i>Report Settings
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                <form class="horizontal-form" id="report-form" action="#" method="POST">
                    <div class="form-body">
                    <p>
                        Please select a report.
                    </p>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@include('Registrar.Views.Reports.sub.modal')
