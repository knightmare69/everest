<?php
    $at       = getAYTerm();
    $campus   = get_campus();
    $i        = 0;
	$rdata    = Request::all();
	$mod      = getObjectValue($rdata,'mod');
	$slug     = getObjectValue($rdata,'slug');
	$ishidden = (($slug=='student-profile' || $slug=='guardian-directory' || $slug=='father-directory' || $slug=='mother-directory' || $slug=='emergency-contact')?'hidden':'');
	$isenable = (($slug=='student-profile' || $slug=='guardian-directory' || $slug=='father-directory' || $slug=='mother-directory' || $slug=='emergency-contact')?'':'disabled');
?>
<div class="form-body">
    <div class="row">
        <div class="col-md-12 rep-inp-hold" id="campus-div">
            <div class="form-group">
                <label class="control-label">Campus</label>
                <select class="select2 form-control" name="campus" id="campus">
                    <option value="" disabled selected></option>
                    @if(!empty($campus))
					$i=0;
                    @foreach($campus as $_this)
                    <option value="{{ encode($_this->CampusID) }}" {{ (($i==0)?'selected':'') }}>
                        {{ $_this->ShortName }}
                    </option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 rep-inp-hold">
          <div class="form-group">
              <label class="control-label">Academic Term</label>
              <select class="select2 form-control" name="academic-term" id="academic-term">
                  @if(!empty($at))
                      @foreach($at as $d)
                      <option value="{{ encode($d->TermID) }}">{{ $d->AYTerm }}</option>
                      @endforeach
                  @endif
              </select>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-6" id="prog-div">
            <div class="form-group">
                <label class="control-label">Programs</label>
                <select class="select2 form-control" name="programs" id="programs">
                    <option value="" disabled selected></option>
                    <option value="" selected="">All Department</option>
                    @if(!empty($progs))
                    @foreach($progs as $_this)
                    <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                        {{ $_this->ProgName }}
                    </option>
                    @endforeach
                    @endif
                </select>
            </div>
      </div>
        <div class="col-md-6  rep-inp-hold {{ $ishidden }}" id="period-div">
            <div class="form-group">
                <label class="control-label">Quarter / Period</label>
                <select class="select2 form-control" name="period" id="period">
                    <?php $qtr = App\Modules\Registrar\Services\RegistrarServiceProvider::Period(); ?>
                    @foreach($qtr as $key => $_this)
                    <option value="{{ encode($_this->PeriodID) }}" data-group="{{ $_this->GroupClass }}" class="{{ ($_this->GroupClass == 21) ? 'hide' : '' }}">
                        {{ ($_this->GroupClass == 21) ? $_this->Description1 : $_this->Description2 }}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12  rep-inp-hold" id="yl-div">
            <div class="form-group">
                <label class="control-label">Grade Level</label>
                <select class="select2 form-control" name="year-level" id="year-level" {{ $isenable }}>
					<?php
					  if($isenable==''){
					    echo '<option value="">All Grade Level</option>';
						$exec = DB::SELECT("SELECT * FROM ESv2_YearLevel");
						if($exec && count($exec)>0){
						   foreach($exec as $y){
							echo '<option value="'.encode($y->YearLevelID).'">'.$y->YearLevelName.'</option>';
						   }
						}
					  }
					?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 rep-inp-hold {{ $ishidden }}" id="sec-div">
            <div class="form-group">
                <label class="control-label">Section</label>
                <select class="select2 form-control" name="section" id="section" disabled>
                </select>
            </div>
        </div>
    </div>
    <!--/row-->
    <div class="row">
        <div class="col-md-12  rep-inp-hold  {{ $ishidden }}" id="student-div">
            <label class="control-label">Student(s)</label>
            <div class="input-group">
                <input type="text" class="form-control stud-find" data-snum="" name="student-name" id="student-name" {{ $isenable }}>
                <span class="input-group-btn">
                    <button type="button" class="faculty-search-btn btn btn-default stud-find" id="student-find" {{ $isenable }}>
                        <i class="fa fa-search"></i>
                    </button>
                    <button type="button" class="faculty-search-btn btn btn-default stud-remove" id="student-remove">
                        <i class="fa fa-times"></i>
                    </button>
                </span>
            </div>
        </div>
        <!--/span-->
    </div>
    <br>
     <div class="row  {{ $ishidden }}">
        <div class="col-md-12">
            <label id="exclude-chk" class=" rep-inp-hold">
                <input type="checkbox" name="print-empty-subj"></input> Exclude subjects with empty grades?
            </label>
            <br>
            <label id="w-logo" class=" rep-inp-hold">
                <input type="checkbox" name="report-card-logo" id="report-card-logo" class="report-card-logo"> With Logo?
            </label>
        </div>
    </div>
    <!--/row-->
</div>
<div class="form-actions right">
    <button class="btn pull-left default hide" style="margin-left: 10px;" id="btnsetup" type="button"><i class="fa fa-cog"></i> Setup </button>
    <!--<button class="btn default" id="export" type="button">Export</button>-->
    <button class="btn blue btn_action btn_save" type="button" id="print-report2"><i class="fa fa-print"></i> Print Report</button>
</div>
