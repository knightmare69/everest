<?php

namespace App\Modules\Reports\Services;

/**
 *
 */
class ReportsSP
{
    public $def_path = '';

    // Use this for the parameters of stored procedure
    public $rep_params = ['StudentNo' => 'Student No.', 'TermID' => 'Term', 'YearLevelID' => 'Year Level', 'ProgramID' => 'Program', 'PeriodID' => 'Quarter / Period', 'SectionID' => 'Section', 'ExcludeEmptySubj' => 'Exclude Empty Subject', 'WithLogo' => 'With Logo'];

    public function __construct()
    {
        $this->def_path = public_path('assets/system/reports/*');
    }

    public function dirListing($type = 'folders')
    {
        $l = [];

        switch ($type) {
            case 'folders':
                $fs = glob($this->def_path, GLOB_ONLYDIR);

                foreach($fs as $f) {
                    $l[] = basename($f);
                }

                break;

            default:
                return false;
                break;
        }

        return $l;

    }

    public function IsValidDir($path)
    {
        $p = rtrim($this->def_path, '*').$path;

        $r = (file_exists($p)) ? true : false;

        return $r;
    }

    public function checkReportParams($par, $sort)
    {

    }

}
