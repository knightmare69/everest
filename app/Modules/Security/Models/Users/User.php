<?php namespace App\Modules\Security\Models\Users;
use illuminate\Database\Eloquent\Model;
use Validator;
use View;
use DB;
Class User extends Model {

	public $table='ESv2_Users';
	public $primaryKey ='UserIDX';

	protected $fillable  = array(
			'UserID',
			'Username',
			'FullName',
			'password',
			'Remember_Token',
			'Email',
			'DepartmentID',
			'PositionID',
			'Photo',
			'UsersGroupID',
			'ProgAccessID',
			'ReportTo_UserIDX',
			'IsLogin',
			'IsBlock',
			'Inactive',
			'LastDateLogin',
			'LastDateChangedPassword',
			'CreatedBy',
			'CreatedDate',
			'LastModifiedBy',
			'LastModifiedDate',
			'IsConfirmed',
			'ConfirmNo',
			'FamilyID',
			'Mobile',
			'PhotoExt',
            'FacultyID',
            'PwdExpiryDate'
		);

	public $timestamps = false;

	public function getAll($limit = 20)
	{
		return
		DB::table('ESv2_Users as u')
		->select([
			'UserIDX',
			'Username',
			'FullName',
			'Email',
			'LastDateLogin',
			'UsersGroupID',
			'CreatedDate'
		])
		->limit($limit)
		->get();
	}

	public function get($where,$limit = 20)
	{
		return
		DB::table('ESv2_Users as u')
		->select([
			'UserIDX',
			'Username',
			'FullName',
			'Email',
			'LastDateLogin',
			'UsersGroupID',
			'PositionID',
            'FacultyID',
            'CreatedDate',
			'DepartmentID',
			'FamilyID'
		])
		->where($where)
		->limit($limit)
		->get();
	}

	public static function data()
	{
		return
			DB::table('ESv2_Users as u')
                // ->selectRaw("*, dbo.fn_StudentName(UserID) As StudentName ")
				->leftJoin('ESv2_UsersGroup as g','g.GroupID','=','u.UsersGroupID')
				->leftJoin('HR_PositionTitles as p','p.PosnTitleID','=','u.PositionID')
				->leftJoin('ES_Departments as d','d.DeptID','=','u.DepartmentID');

	}
}