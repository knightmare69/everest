<div class="row inbox">
    <div class="col-md-2">
    	<ul class="inbox-nav margin-bottom-10">
    		<li class="compose-btn">
    			<a href="javascript:;" data-title="Compose" class="btn green">
    			<i class="fa fa-edit"></i> Compose </a>
    		</li>
    		<li class="inbox active">
    			<a href="javascript:;" class="btn" data-title="Inbox">
    			Inbox(3) </a>
    			<b></b>
    		</li>
    		<li class="sent">
    			<a class="btn" href="javascript:;" data-title="Sent">
    			Sent </a>
    			<b></b>
    		</li>
    		<li class="draft">
    			<a class="btn" href="javascript:;" data-title="Draft">
    			Draft </a>
    			<b></b>
    		</li>
    		<li class="trash">
    			<a class="btn" href="javascript:;" data-title="Trash">
    			Trash </a>
    			<b></b>
    		</li>
    	</ul>
    </div>
    <div class="col-md-10">
    	<div class="inbox-header">
    		<h1 class="pull-left">Inbox</h1>
    		<form class="form-inline pull-right" action="index.html">
    			<div class="input-group input-medium">
    				<input class="form-control" placeholder="Password" type="text">
    				<span class="input-group-btn">
    				<button type="submit" class="btn green"><i class="fa fa-search"></i></button>
    				</span>
    			</div>
    		</form>
    	</div>
    	<div class="inbox-loading" style="display: none;">
    		 Loading...
    	</div>
        <div class="inbox-content">
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                	<th colspan="3">
                		<div class="checker"><span><input class="mail-checkbox mail-group-checkbox" type="checkbox"></span></div>
                		<div class="btn-group">
                			<a class="btn btn-sm blue dropdown-toggle" href="#" data-toggle="dropdown">
                			More <i class="fa fa-angle-down"></i>
                			</a>
                			<ul class="dropdown-menu">
                				<li>
                					<a href="#">
                					<i class="fa fa-pencil"></i> Mark as Read </a>
                				</li>
                				<li>
                					<a href="#">
                					<i class="fa fa-ban"></i> Spam </a>
                				</li>
                				<li class="divider">
                				</li>
                				<li>
                					<a href="#">
                					<i class="fa fa-trash-o"></i> Delete </a>
                				</li>
                			</ul>
                		</div>
                	</th>
                	<th class="pagination-control" colspan="3">
                		<span class="pagination-info">
                		1-30 of 789 </span>
                		<a class="btn btn-sm blue">
                		<i class="fa fa-angle-left"></i>
                		</a>
                		<a class="btn btn-sm blue">
                		<i class="fa fa-angle-right"></i>
                		</a>
                	</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>