<style >
    #TableUsers_filter, #TableUsers_paginate {
        text-align: right;
    }
</style>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"> <i class="fa fa-globe"></i> User list</div>
        <div class="tools"></div>
        <div class="actions">
            <a href="javascript:void(0)" class="btn btn-sm btn-default BtnProgramAccess"><i class="fa fa-file  font-pink"></i> Data Access </a>
            <div class="btn-group">
                <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#" class="btn bg bg-purple btn-sm dropdown-toggle" aria-expanded="false">
                Actions <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                	<li>
                        <a href="javascript:void(0)" class="a_addnew">
                        <i class="fa fa-plus font-green"></i> Add new</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="a_block">
                        <i class="fa fa-power-off  font-pink"></i> Block</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="a_status">
                        <i class="fa fa-check  font-blue"></i> Active/Deactivate</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class='a_remove'>
                        <i class="fa fa-trash font-red"></i> Remove</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class='reset-password'>
                        <i class="fa fa-history"></i> Reset Password </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body">
    	<div class="row">
	       	<div class="col-md-12 table_wrapper">
	           @include('Security.Views.users.sub.center')
			</div>
		</div>
    </div>
</div>

<div class="modal fade" id="modal_program_access" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header modal-header-bg">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"> Data Access </h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th colspan="2" id="FormGroupWrapper">
										<div class="input-group SearchTemplateWrapper">
											<select class="form-control" name="ProgramTemplate" id="ProgramTemplate">
												<option value="" selected="">Select...</option>
    											@foreach( App\Modules\Setup\Models\ProgramAccess_Templates::get() as $row)
    												<option  value="{{ encode($row->PATemplateID) }}">{{ $row->PATemplateName }}</option>
    											@endforeach
											</select>
											<span class="input-group-addon cursor-pointer CreateTemplate">
												<i class="fa fa-plus"></i>
											</span>
										</div>
										<div class="input-group hide CreateTemplateWrapper">
											<input type="text" class="form-control" name="TemplateName" id="TemplateName" placeholder="Template Name">
											<span class="input-group-addon cursor-pointer BtnCancelTemplateName">
												<i class="fa fa-arrow-left"></i>
											</span>
										</div>
									</th>
								</tr>
							</thead>
						</table>
					</div>
                    </div>
                <div class="row">
					<div class="col-md-12" >
                        <div style="height: 400px !important; overflow-y: auto !important;" id="TableProgramWrapper" >
                        </div>
					</div>
				</div>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn default btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn blue btn_modal btnSaveProgramAccess"><i class="fa fa-check"></i> Save Access</button>
				<button type="button" class="btn blue btn_modal BtnSaveTemplateName hide">Save Template</button>
			</div>
		</div>
	</div>
</div>