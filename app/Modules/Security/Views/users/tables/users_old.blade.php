<?php $users = isset($users) ? $users : array(); ?>
<table id="TableUsers" class="table table-striped table-hover table_scroll" style="cursor: pointer;">
    <thead>
        <tr>
            
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit" >#</th>
            <th>Pic</th>
            <th>Full Name</th>
            <th>Username</th>
            <th>Role</th>
            <th>Email</th>
            <th>Position</th>
            <th>Department</th>            
            <th class="autofit">Last Login</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($users as $row)
        <tr data-id="{{ encode($row->UserIDX) }}"  class="without-bg">                                    
            <td class="autofit"><input type="checkbox" class="chk-child"></td>
            <td class="autofit">{{$i}}.</td>
            <td class="autofit text-center">
                <img src="{{ url() }}/general/getUserPhoto?UserID={{encode($row->UserIDX)}}" width="30" alt=""> 
            </td>
            <td class="autofit"><a href="javascript:void(0)" class="a_select">{{ trimmed($row->FullName) }}</a></td>
            <td>{{ $row->Username }}</td>
            <td>{{ $row->GroupName }}</td>
            <td class="email">{{ $row->Email }}</td>
            <td>{{ $row->PositionDesc }}</td>
            <td>
                @if($row->GroupID == '7')
                    {{ $row->Major }}
                @else
                {{ $row->DeptName }}
                @endif
            </td>            
            <td class="autofit">{{ $row->LastDateLogin }}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
    </tbody>
</table>