<?php $data = isset($data[0]) ? $data[0] : array(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_user" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <!--/row-->
        <h4><b>Account Login</b></h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Username <small><i class="text-danger">*</i></small> </label>
                    <div class="input-group">
                        <input type="text" placeholder="Username" class="form-control input-xs" name="username" id="username" value="{{ getObjectValue($data,'Username') }}">
                        <span class="input-group-addon span_username_checker">
                            <i class="fa fa-user font-grey-steel"></i>
                        </span>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-3 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Password <small><i class="text-danger">*</i></small></label>
                    <input type="password" placeholder="Password" class="form-control input-xs {{ count($data) > 0 ? 'not-required' : '' }}"  name="upassword" id="upassword">
                    <span class="help-block hide password"></span>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-3 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Confirm Password <small><i class="text-danger">*</i></small></label>
                    <input type="password" placeholder="Confirm Password" class="form-control input-xs {{ count($data) > 0 ? 'not-required' : '' }}"  name="cpassword" id="cpassword">
                </div>
            </div>
            <!--/span-->
            @if(!empty($data))
            <div class="col-md-3">
                <div class="form-group" style="margin-top:24px;">
                    <button type="button" class="btn btn-primary" id="reset-passw" data-id="{{ !empty($data->UserIDX) ? encode($data->UserIDX) : 0 }}">Reset Password</button>
                </div>
            </div>
            @endif
            <!--/span-->
        </div>
        <!--/row-->

        <h4><b>Account Information</b></h4>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Full Name <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="First Name" class="form-control input-xs" name="FullName" id="FullName" value="{{ getObjectValue($data,'FullName') }}">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
             <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Email Address <small><i class="text-danger">*</i></small></label>
                    <input type="email" placeholder="Email address" class="form-control input-xs" name="email" id="email" value="{{ getObjectValue($data,'Email') }}" data-orig-email="{{ getObjectValue($data,'Email') }}">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Position <small><i class="text-danger">*</i></small></label>
                   <select class="form-control input-xs select2 not-required" name="position" id="position">
                        @foreach(App\Modules\Setup\Models\Position::get() as $row)
                            <option {{ $row->PosnTitleID == getObjectValue($data,'PositionID') ? 'selected' : '' }} value="{{ $row->PosnTitleID }}">{{ $row->PositionDesc }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Group</label>
                    <select class="form-control input-xs select2" name="group" id="group">
                    @foreach (App\Modules\Setup\Models\Group::get() as $row )
                        <option {{  getObjectValue($data,'UsersGroupID') == $row->GroupID ? 'selected' : '' }} value="{{ $row->GroupID }}">{{ $row->GroupName }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">Department/Office</label>
                    <select class="form-control input-xs select2 not-required" name="department" id="department">
                        @foreach(App\Modules\Setup\Models\Department::get() as $row)
                            <option <?=  getObjectValue($data,'DepartmentID')  == $row->DeptID ? 'selected' : '' ?> value="{{$row->DeptID}}">{{ $row->DeptName }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">PRISMS ID</label>
                    <input type="text" placeholder="Input PRISMS ID" class="form-control input-xs" name="facultyid" id="facultyid" value="{{ getObjectValue($data,'FacultyID') }}" data-orig-data="{{ getObjectValue($data,'FacultyID') }}">
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="control-label">FamilyID <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="FamilyID" class="form-control input-xs" name="FamilyID" id="FamilyID" value="{{ getObjectValue($data,'FamilyID') }}">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->

    </div>
</form>