<div class="portlet light">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-globe"></i> System Logs </div>
        <div class="tools"></div>
        <div class="actions">
            <div class="btn-group">
                <a class="btn btn-default btn-circle" href="javascript:;" data-toggle="dropdown"><i class="fa fa-share"></i> Tools <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:;" data-export="excel" class="export"> Export to Excel </a></li>
                    <li><a href="javascript:;" data-export="pdf" class="export hidden"> Export to PDF </a></li>
                </ul>
            </div>
        </div>
        <br/>
        
    </div>
    <div class="row">
        <div class="table-group-action-input">
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label">Date Covered</label>
                <div class="input-group date date-picker margin-bottom-5 " data-date-format="dd/mm/yyyy">
                    <input id="log_date_from"  type="text" class="form-control form-filter input-sm" readonly name="log_date_from" placeholder="From">
                    <span class="input-group-btn">
                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
                <div class="input-group date date-picker " data-date-format="dd/mm/yyyy">
                    <input id="log_date_to" type="text" class="form-control form-filter input-sm" readonly name="log_date_to" placeholder="To">
                    <span class="input-group-btn">
                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label">User Name or Email</label>
                    <input id="log_by" type="text" class="form-control form-filter input-sm" name="log_by">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label">Action Taken</label>
                    <input id="action" type="text" class="form-control form-filter input-sm" name="action">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label">Parameters</label>
                    <div class="input-group margin-bottom-5">
                        <input id="parameter" type="text" class="form-control form-filter input-sm" name="parameter">
                    </div>
                    {{-- <div class="input-group">
                        <input id="browser" type="text" class="form-control form-filter input-sm" placeholder="Browser" name="browser">
                    </div> --}}

                </div>


            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label">Devices</label>
                    <div class="input-group margin-bottom-5">
                    <input id="device" type="text" class="form-control form-filter input-sm" placeholder="Device" name="device">
                    </div>
                    {{-- <div class="input-group">
                    <input id="ip_address" type="text" class="form-control form-filter input-sm" placeholder="IP Address" name="ip_address">
                    </div> --}}

                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label">&nbsp;</label>
                    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                    <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <hr />
    	<div class="row">
	       	<div class="col-md-12 table_wrapper">
		      @include($views.'tables.logs')
			</div>
		</div>
    </div>
</div>
