<div class="portlet bordered light ">
    <div class="portlet-title">
        <div class="caption">List of Users</div>
        <div class="tools">
        <a href="javascript:void(0)" class="BtnProgramAccess" ><i class="fa fa-unlock "></i> Data Access </a>
        </div>
    </div>
    <div class="portlet-body" id="tvaccounts">
    	<div class="row">
    		<div class="col-md-12">
                <div class="scroller" style="height:500px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd" >
       			  @include('Security.Views.access.content.sub.accounts.accounts')
                </div>
       		</div>
   		</div>
    </div>
</div>
