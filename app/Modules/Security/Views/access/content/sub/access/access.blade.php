<div id="tree_access">
</div>
<div class="modal fade" id="modal_program_access" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header modal-header-bg">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Progam Privileges</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th colspan="2" id="FormGroupWrapper">
										<div class="input-group SearchTemplateWrapper">
											<select class="form-control select2" name="ProgramTemplate" id="ProgramTemplate">
												<option value="">Select...</option>
											@foreach( App\Modules\Setup\Models\ProgramAccess_Templates::get() as $row)
												<option  value="{{ encode($row->PATemplateID) }}">{{ $row->PATemplateName }}</option>
											@endforeach
											</select>
											<span class="input-group-addon cursor-pointer CreateTemplate">
												<i class="fa fa-plus"></i>
											</span>
										</div>
										<div class="input-group hide CreateTemplateWrapper">
											<input type="text" class="form-control" name="TemplateName" id="TemplateName" placeholder="Template Name">
											<span class="input-group-addon cursor-pointer BtnCancelTemplateName">
												<i class="fa fa-arrow-left"></i>
											</span>
										</div>
									</th>
								</tr>
							</thead>
						</table>
					</div>
                </div>
                <div class="row">                
					<div class="col-md-12" >
                        <div style="height: 400px !important; overflow-y: auto !important;" id="TableProgramWrapper" >
                        </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="button" class="btn blue btn_modal btnSaveProgramAccess">Save User Program Access</button>
				<button type="button" class="btn blue btn_modal BtnSaveTemplateName">Save Template</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->