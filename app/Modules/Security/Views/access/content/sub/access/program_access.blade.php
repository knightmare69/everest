<table class="table table-hover table-condensed " id="TablePrograms">
	<thead>
		<tr>
			<th class="autofit">
				<input type="checkbox" class="chk-header">
			</th>
			<th width="95%">Academic Program</th>
		</tr>
	</thead>
	<tbody>
	<?php

        $user = Request::get('ProgramTemplate');

        $model = App\Modules\Setup\Models\Programs::from('ES_Programs AS p')
        ->select(['ProgID','ProgCode','ProgName'])
        ->selectRaw(" (SELECT IndexID FROM [ESv2_UserGroup_ProgramAccess] WHERE ProgID = p.ProgID AND [UserGroupID] = '".decode($user)."' AND IsGroup = '". Request::get('isgroup') ."' ) AS [Exists]     ")
        ->orderBy('ProgName','asc');

	?>
	@foreach($model->get() as $row)
		<tr class="without-bg" data-id="{{ encode($row->ProgID) }}">
			<td class="autofit">
				<input {{ $row->Exists != '' ? 'checked' : '' }} type="checkbox" class="chk-child" name="program_{{ $row->ProgCode }}" id="program_{{ encode($row->ProgID) }}">
			</td>
			<td><label for="program_{{ encode($row->ProgID) }}" class="cursor-pointer">{{ $row->ProgName }}</label></td>
		</tr>
	@endforeach
	</tbody>
</table>