<?php

//SECURITY
 	Route::get('logout', 'Users@logout');	
	Route::group(['prefix' => 'security','middleware'=>'auth'], function () {
	    Route::group(['prefix' => 'access'], function () {
	    	Route::get('/', 'Security@index');
		    Route::post('access', 'Security@access');
            Route::post('event', 'Security@access');      
		    Route::post('accounts', 'Security@accounts');
		    Route::post('actions', 'Security@actions');
		});

		Route::group(['prefix' => 'users'], function () {
	    	Route::get('/', 'Users@index');
		    Route::post('event', 'Users@event');
		    Route::post('access', 'Users@access');
		    Route::post('accounts', 'Users@accounts');
		    Route::post('actions', 'Users@actions');
		});

		Route::group(['prefix' => 'logs'], function () {
		    Route::get('/', 'AuditTrails@index');
			Route::post('search', 'AuditTrails@search');
		});

		Route::group(['prefix' => 'password'], function () {
	    	Route::get('/', 'Password@index');
		    Route::post('event', 'Password@event');
		});

		Route::get('validation','Security@validation');
	});

	Route::group(['prefix' => 'reset'], function () {
	    Route::get('/', 'Reset@index');
		Route::post('sendEmailReset','Reset@sendEmailReset');
		Route::post('password','Reset@reset');
	});

	Route::group(['prefix' => 'profile'], function () {
	    Route::get('/', 'Profile@index');
		Route::post('event', 'Profile@event');

		Route::group(['prefix' => 'account'], function () {
		    Route::get('/', 'Profile@account');
		});

		Route::group(['prefix' => 'help'], function () {
		    Route::get('/', 'Profile@help');
		});
        
        Route::group(['prefix' => 'inbox'], function () {
		    Route::get('/', 'Profile@inbox');
		});
        
	});

	Route::get('changePswd', 'Reset@changePswd');
	Route::post('pchangePswd', 'Reset@pchangePswd');
?>