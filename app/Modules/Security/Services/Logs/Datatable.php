<?php
namespace App\Modules\Security\Services\Logs;
use App\Modules\Security\Models\Auditrails as AuditrailsModel;
use DB;
use Request;
use Response;

Class Datatable {

	public function __construct()
	{
		$this->model = new AuditrailsModel;
	}

	public function filter($permission)
	{
		/*  Modified by Jays: update v1.1
			Added Filter Function - 02-28-19
		*/
		$model = DB::table($this->model->table.' as t');
        $where = "";
		$hasParam = false;
        /*
		$columns = array(
			't.module',
			't.controller',
			't.action',
			't.parameter',
			't.message',
			't.browser',
			't.device',
			't.ip_address',
			't.created_date',
			'u.UserName as name',
		);
        */
        // $where = " t.id > 0 ";

        $model = $model->selectRaw(' t.module , t.controller , t.action , t.parameter , t.message , t.browser , t.device , t.ip_address , t.created_date , u.UserName as name ')
                       // ->leftJoin('esv2_users as u',DB::raw('convert(varchar,u.UserIDX)'),'=','t.created_by');
    					->leftJoin('esv2_users as u','u.UserIDX','=','t.created_by');

		if (Request::get('module')) {
			$model->where('module','like','%'.trim(Request::get('module')).'%');
			$hasParam = true;
		}

		if (Request::get('page')) {
			$model->where('controller','like','%'.trim(Request::get('page')).'%');
			$hasParam = true;
		}

		if (Request::get('action') && !is_array(Request::get('action'))) {
			$model->where('action','like','%'.trim(Request::get('action')).'%');
			$hasParam = true;
		}

		if (Request::get('parameter')) {
						$model->where('Parameter','like','%'.trim(Request::get('parameter')).'%');
			//$model->where('parameter','like','%'.trim(Request::get('parameter')).'%');
            // $where .= " AND (Parameter Like '%". trim(Request::get('Parameter')) ."%')";

			$hasParam = true;
		}
        /*
		if (Request::get('message')) {
			$model->where('message','like','%'.trim(Request::get('message')).'%');
			$hasParam = true;
		}
        */
		if (Request::get('browser')) {
			$model->where('browser','like','%'.trim(Request::get('browser')).'%');
			$hasParam = true;
		}

		if (Request::get('device')) {
			$model->where('device','like','%'.trim(Request::get('device')).'%');
			$hasParam = true;
		}

		if (Request::get('ip_address')) {
			$model->where('ip_address','like','%'.trim(Request::get('ip_address')).'%');
			$hasParam = true;
		}

		if (Request::get('log_date_from') && Request::get('log_date_to')) {
			$model->where('t.created_date','>=',setDateFormat(Request::get('log_date_from'),'dd/mm/yyyy','yyyy-mm-dd').' 1:00 AM');
			$model->where('t.created_date','<=',setDateFormat(Request::get('log_date_to'),'dd/mm/yyyy','yyyy-mm-dd').' 23:59 PM');
			$hasParam = true;
		}

		if (Request::get('log_by')) {
            $model->whereRaw(" ( u.Username like '%".Request::get('log_by')."%' OR u.FullName like '%". Request::get('log_by') ."%' ) ");
			$hasParam = true;
		}

		//if (isParent()) {
			// $model->where('created_by',getUserID());
		//}

        if($where !=''){
            $model->whereRaw($where);
        }

		if($permission->has('view-all') == false){
			$model->where('created_by',getUserID());
		}

		$iTotalRecords = $this->model->count();
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$records = array();
		$records["data"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = !$hasParam ? $end > $iTotalRecords ? $iTotalRecords : ($end == $iTotalRecords) ? 0 : $end : 0;
		// Changed skip($end)
		$data = $model->skip($iDisplayStart)->take($iDisplayLength)->orderBy('created_date', 'desc')->get();

		foreach($data as $row) {
			$browser = json_decode($row->browser,true);
			$records['data'][] = [
				setDate( $row->created_date,'m/d/Y H:i:s a'),
                $row->name,
                // $row->module,
				// $row->controller,
				$row->action,
				$row->parameter,
				//$row->message,
				$browser['name'],
				$row->device,
				$row->ip_address,


				''
			];
		}

		if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
		    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
		    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
		 }
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		return $records;
	}
}
?>
