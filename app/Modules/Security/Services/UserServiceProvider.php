<?php
namespace App\Modules\Security\Services;
use App\Modules\Security\Services\Users\Validation;
use sysAuth;
use DB;

Class UserServiceProvider {

	public function __construct()
	{
		$this->validation = new Validation;
		$this->pswd = new sysAuth;
	}

	public function isValid($post,$action = 'save')
	{	
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		
		return ['error'=>false,'message'=> ''];
	}

	public function postSave($post)
	{
		return [
				'Username' => getObjectValue($post,'username'),
				'password' =>  bcrypt(getObjectValue($post,'upassword')),
				// 'level_id'		=>  getObjectValue($post,'level'),
				'FullName'	=>  getObjectValue($post,'FullName'),
				'Email' =>  getObjectValue($post,'email'),
				'PositionID'	=>  getObjectValue($post,'position',''),
				'DepartmentID'	=>  getObjectValue($post,'department',''),
				'FacultyID' => 	getObjectValue($post,'facultyid'),
				'UsersGroupID'		=>  getObjectValue($post,'group'),		
				'PwdExpiryDate' => $this->pswd->getExpiryDate(),
				'CreatedBy'		=> getUserID(),
				'CreatedDate'	=>  systemDate(),
				'IsConfirmed'     => 1,
			];			
	}

	public function postUpdate($post)
	{
		return [
				'Username' =>  getObjectValue($post,'username'),
				// 'password'		=>  bcrypt(getObjectValue($post,'upassword')),
				// 'level_id'		=>  getObjectValue($post,'level'),
				'FullName'	=>  getObjectValue($post,'FullName'),
				'Email'			=>  getObjectValue($post,'email'),
				'PositionID'	=>  getObjectValue($post,'position'),
				'DepartmentID'	=>  getObjectValue($post,'department'),
				'UsersGroupID'	 =>  getObjectValue($post,'group'),	
                'FacultyID' => 	getObjectValue($post,'facultyid'),
                'FamilyID' => 	getObjectValue($post,'FamilyID'),
				'LastModifiedBy' =>  getUserID(),
				'LastModifiedDate'	=>  systemDate()
			];			
	}

	public function postProfile($post)
	{
		return [
				'FullName' => getObjectValue($post,'FullName'),
				'Email'	=> getObjectValue($post,'Email'),
				'Mobile' => getObjectValue($post,'Mobile'),
			];	
	}

	public function postPhoto()
	{
		$data = getFileInfo($_FILES['photo']);
		return [
			'Photo' => DB::raw('0x'.bin2hex(file_get_contents(getObjectValue($data,'Attachment')))),
			'PhotoExt' => getObjectValue($data,'FileType')
		];
	}

	public function postPassword($post)
	{
		return [
				'password' => bcrypt(getObjectValue($post,'NewPassword')),
				'PwdExpiryDate' => $this->pswd->getExpiryDate(),
			];	
	}
}	
?>