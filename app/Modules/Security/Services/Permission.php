<?php
namespace App\Modules\Security\Services;
use DB;
use App\Modules\Security\Models\Access\Access;
class Permission
{
    public $page;

    private $actions;

    public function __construct($page)
    {
        $this->page = $page;
        $this->setPermission();
    }

    public function has($action)
    {
        // return true;
        return (property_exists($this,$action)) ? $this->$action : false;
    }

    private function setPermission()
    {
        foreach($this->getAccess() as $action)
        {
            $this->setActions($action);
        }
    }

    private function getAccess()
    {
        return $this->actions = DB::select("exec spPermission '".getUserName()."','".$this->page."'");
    }

    private function setActions($actions)
    {
        $action = $actions->slug;
        $this->$action = $actions->slug;
    }

    private function initializer()
    {
        $this->access = new Access;
    }
}
