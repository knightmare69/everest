<?php
namespace App\Modules\Security\Services\Access;

use App\Modules\Setup\Models\Department;
use App\Modules\Security\Models\Users\User;
use DB;
class Accounts
{
    public function __construct()
    {
        $this->department = new Department;
        $this->user = new User;
    }

    public function treeData()
    {
        $tree[] =
        [
            'text' => 'Groups',
            'children' =>  $this->loadGroups(),
        ];
        $tree[] =
        [
            'text' => 'Users',
            'children' =>  $this->LoadAccounts(),
        ];
        return $tree;
    }
    
    public function LoadAccounts()
    {
        // $data = App\Modules\Security\Models\Users::where('IsBlock',0)->get();
        $data = DB::table($this->user->table.' as u')
        ->select([
            'UserIDX',
            'FullName as name',
            'DepartmentID',
            'd.DeptName as department'
        ])
        ->leftJoin($this->department->table. ' as d','d.DeptID','=','u.DepartmentID')
        ->where('IsBlock',0)
        ->get();
        
        $tree = array();
        $children = array();
        $container = array();
        $parent = array();
        $i = 0;
        foreach ($data as $row) {
            $children[] =
            [
                'text' => $row->UserIDX.' - '."<sysaccount>".$row->name."</sysaccount><id class='account hide'>".encode($row->UserIDX)."</id>",
                'icon' => 'fa fa-user font-green',
            ];

        }
       
        return $children;
    }

    public function loadGroups()
    {   
        // $data = App\Modules\Setup\Models\Group::where('Inactive',0)->get();
        $data = DB::select("select GroupID,GroupName from ESv2_UsersGroup where Inactive=0");
        $tree = array();
        $children = array();
        $container = array();
        $i = 0;
        foreach ($data as $row) {
            $parent[] =
            [
                'text' => "<sysaccount>".$row->GroupName."</sysaccount><id class='group hide'>".encode($row->GroupID)."</id>",
                'icon' => 'fa fa-file font-green',
            ];
        }
        return $parent;
    }
}
