<?php
namespace App\Modules\Security\Services\Access;

use App\Modules\Setup\Models\ProgramAccess_Templates as ProgramTemplate;
use App\Modules\Setup\Models\UserGroup_ProgramAccess as UserProgramAccess;
use App\Modules\Security\Models\Users\User as UserModel;
use DB;
use Request;

class ProgramAccess
{

    protected $TemplateID;
    public function __construct() {
        $this->UserProgramAccess = new UserProgramAccess;
        $this->ProgramTemplate = new ProgramTemplate;
        $this->user = new UserModel;
        $this->TemplateID = Request::get('TemplateID') ? decode(Request::get('TemplateID')) : '';
        $this->UserID = decode(Request::get('account'));
    }

    public function save()
    {
        if (Request::get('isTemplate') == '0') {
            $template = $this->ProgramTemplate->create(['PATemplateName'=>Request::get('TemplateName')]);
            $data = $this->getTemplateData(getObjectValue($template,'PATemplateID'));
            $this->TemplateID = getObjectValue($template,'PATemplateID');
        } else {
           $data = $this->getTemplateData($this->TemplateID);
        }
        $this->UserProgramAccess
                ->where('PATemplateID',$this->TemplateID)
                ->insert($data);

        $this->bindTemplateData($this->TemplateID);
        return true;
   }

   public function saveUserTemplate()
   {
      return $this->user->where('UserIDX',$this->UserID)->update(['ProgAccessID'=>$this->TemplateID]);
   }

   public function setDataAccess(){

        $data = array(
            'ProgID' => decode(Request::get('prog')),
            'UserGroupID' => decode(Request::get('user')),
            'IsGroup'=>Request::get('isgroup'),
        );
        return $this->UserProgramAccess->updateOrCreate($data,$data);
   }

   public function unsetDataAccess(){
        return  $this->UserProgramAccess
            ->whereRaw("UserGroupID = '". decode(Request::get('user')) ."'")

            ->where('ProgID',decode(Request::get('prog')))
            ->where('IsGroup',Request::get('isgroup'))
            ->delete();
   }

   public function getUserProgramTemplate()
   {
        return encode($this->user->select('ProgAccessID')->where('UserIDX',$this->UserID)->pluck('ProgAccessID'));
   }

   public function getTempates()
   {
        $data = [];
        $result = $this->ProgramTemplate->select('*')->get();
        foreach($result as $row) {
            $data[] = [
                'id' => encode($row->PATemplateID),
                'name' => $row->PATemplateName
            ];
        }
        return $data;
   }

   protected function getTemplateData($TemplateID)
   {
        $data = json_decode('['.Request::get('data').']',true);
        $dataRaw = [];
        foreach($data[0] as $row) {
            $where = [
                'UserGroupID' => $TemplateID,
                'IsGroup' => 0,
                'ProgID' => decode(getObjectValue($row,'id')),
            ];
            if ($this->UserProgramAccess->where($where)->count() <= 0) {
                $dataRaw[] = $where;
            }
        }

        return $dataRaw;
   }

   protected function bindTemplateData($TemplateID)
   {
        $data = json_decode('['.Request::get('data').']',true);
        $whereProgData = [];
        foreach($data[0] as $row) {
            $whereProgData[] = decode(getObjectValue($row,'id'));
        }

        $this->UserProgramAccess
            ->where('UserGroupID',$TemplateID)
            ->whereNotIn('ProgID',$whereProgData)
            ->where('IsGroup','0')
            ->delete();
   }
}
