<?php namespace App\Modules\Security\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Security\Models\Users\User as UserModel;
use App\Modules\Security\Models\Password\password as PwdModel;
use Auth;
use Request;
use Response;
use Mail;
use sysAuth;

class Reset extends Controller {


	public function index()
	{
		if (!$this->isValidToken()) {
			echo "Invalid Reset!";die();
		}		
		return  view('auth.reset');
	}

	public function reset()
	{
		$this->initializer();
		$response = ['error' => true,'message' => 'Could not reset password.'];
		
		if ($this->isValidToken()) {
			$key = $this->decodeToken(Request::get('token'));
			$this->model
				->where('UserIDX',$key)
				->update([
					'Password' => bcrypt(Request::get('password')),
					'PwdExpiryDate' => $this->pswd->getExpiryDate(),
				]);
                
                //SystemLog('Password Reset','Reset','reset','password reset','UserIDx :' . $key,'Password was successfully reset!');
                
				$this->sendEmailAccount($key,Request::all());
			$response = ['error' => false,'message' => 'Successfully Reset Password. This will automatically redirect you to login page.'];
		}
		return $response;
	}

	public function sendEmailReset()
	{
		$this->initializer();
		$email = trimmed(Request::get('email'));
		$post['email'] = $email;
		$post['token'] = $this->encodeToken(getUserIDByEmail($email));
		$response = ['error' => true,'message' => 'Could not find email address.'];                
        
		if ($this->model->where('Email',$email)->count() > 0) {
			ini_set('max_execution_time', 3600);
			$data = $this->model->where('UserIDX',getUserIDByEmail($email))->get();
			$data = isset($data[0]) ? $data[0] : [];
			Mail::send('email.reset', ['data'=>$data,'token'=>$post['token']], function ($message) use ($post) {
	        	$message->from('k12@princetech.com.ph', 'K to 12 System');
	        	$message->to(getObjectValue($post,'email'))->subject('K to 12 Password Reset');
                $message->bcc('k12@princetech.com.ph');
			});
			$response = ['error' => false,'message' => 'Please check your email for the reset details.'];
		}
		return $response;
	}

	public function sendEmailAccount($key,$post)
	{
		$this->initializer();
		$post['email'] = getUserEmailByID($key);
		$post['username'] = getUserNameByID($key);
		ini_set('max_execution_time', 3600);
		Mail::send('email.account_details', ['post'=>$post], function ($message) use ($post) {
			$message->from('k12@princetech.com.ph', 'K to 12 System');
        	$message->to(getObjectValue($post,'email'))->subject('K to 12 User Credentials');
            $message->bcc('k12@princetech.com.ph');
		});
	}

	public function pchangePswd() {
		return $this->changePswd();
	}

	public function changePswd()
	{	
		$url = 'changePswd?token='.Request::get('token');
		if (!$this->isValidToken()) {
			return 'Invalid token!';
		}
		if (strtolower(Request::get('p')) == 'a') {
			return view('auth.changePswdExpiration');
		}
		$this->initializer();
		$response = ['error' => true,'message' => 'New Password did not matched!'];
		$userID = decodeToken(Request::get('token'));
		if (Request::get('cpswd') == Request::get('npswd')) {
			if ($this->model->where('UserName',$userID)->count() > 0) {
				$credentials = ['UserName'=>$userID,'password'=>Request::get('curpswd')];
				if (Auth::attempt($credentials,false)) {
					if (Request::get('curpswd') != Request::get('cpswd')) {
						$sysAuth = $this->sysAuth->isValid(Request::get('npswd'));
						if (!$sysAuth['error']) {
							updateLoginDate();
							$this->model->where('UserName',$userID)
							->update([
								'password' => bcrypt(Request::get('npswd')),
								'PwdExpiryDate' => $this->sysAuth->getExpiryDate()
							]);
							SystemLog(
								'Security','Reset','pswdChangeForExpiration',
								'changePswd',Request::all(),
								'Change password due to pswd expiration'
							);
							return 
								redirect($url.'&p=a')
									->withErrors([
										'error'=>false,
										'message'=>'Successfully change password. Wait for the page to redirect you to home page.'
									]);
						}
						$response = $sysAuth;
					} else {
						$response = ['error' => true,'message' => 'Please do not repeat your current password.'];
					}
				} else {
					$response = ['error' => true,'message' => 'Invalid Old Password.'];
				}
			}
		}
		return redirect($url.'&p=a')->withErrors($response);
	}

	private $tokenLen = 20;

	protected function encodeToken($id)
	{
		$key = sha1('ab$6*1hVmkLd^0.');
 		$code1 = substr($key,0,strlen($key)-$this->tokenLen); 		
 		$code2 = substr($key,strlen($key)-$this->tokenLen,$this->tokenLen);
 		$key = $code1.$id.$code2.'|'.base64_encode(strlen($code2.$id));

 		return base64_encode($key);
	}

	protected function decodeToken($token)
	{
		$token_ = base64_decode($token);
		$token_ = explode('|',$token_);

		$tokenLeftLength = base64_decode($token_[1]);
		$token_ = $token_[0];

		$key = substr($token_, -$tokenLeftLength);

		return substr($key,0,strlen($key)-$this->tokenLen);
	}

	protected function isValidToken()
	{
		if (Request::get('token')) {
			if (count(explode('|',base64_decode(Request::get('token')))) > 1) {
				return true;
			}
		}
		return false;
	}

	private function initializer()
	{
		$this->model = new UserModel;
		$this->sysAuth = new sysAuth;
        $this->pswd = new PwdModel;
	}

}