<?php
namespace App\Modules\Security\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Security\Services\Access\Access;
use App\Modules\Security\Services\Access\Accounts;
use App\Modules\Security\Models\Access\Access as UserAccess;
use App\Modules\Security\Models\Access\Actions;
use App\Modules\Security\Services\Access\ProgramAccess as ProgramAccessServices;
use Response;
use Request;
use Permission;
use DB;

class Security extends Controller
{
    private $media =
    [
        'Title'         => 'Privileges',
        'Description'   => 'General Settings',
        'js'            => ['Security/access','Security/tree','Security/security','Security/program_access'],
        'init'         => ['SECURITY.init()'],
        'plugin_js'        => [
                            'jstree/dist/jstree.min','bootbox/bootbox.min',
                            'select2/select2.min',
                            'jquery-multi-select/js/jquery.multi-select',
                            'bootstrap-select/bootstrap-select.min',
                       ],
        'plugin_css'       => [
            'jstree/dist/themes/default/style.min','bootstrap-select/bootstrap-select.min',
            'bootstrap-select/bootstrap-select.min','select2/select2','jquery-multi-select/css/multi-select'
        ],    
    ];

    private $url = 
    [
        'form' => 'form',
        'page'  => 'security/access/'
    ];

    private $views = 'Security.Views.';

    public function index()
    {
        $this->initializer();
       // if ($this->permission->has('read')) {
            return view('layout',array('content'=>view('Security.Views.access.index')->render(),'url'=>$this->url,'media'=>$this->media));
        // }
       // return view(config('app.404'));
    }

    public function actions()
    {
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event'))
            {
                case 'save':
                    if ($this->permission->has('add')) {
                        
                        $data['page_id'] = Request::get('page');
                        $data['name'] = Request::get('action');
                        $data['slug'] = str_replace([' ','_'],'-',strtolower(str_replace("'","",Request::get('action'))));
                        $data['sort'] = $this->actions->where('page_id',Request::get('page'))->count()+1;

                        SystemLog('Security','Security','actions','save',Request::all(),successSave());
                        
                        $response = Response::json(['error'=>false,'message'=>'Succesfully Save','action_id'=> $this->actions->create($data)->action_id]);
                    }
                    break;
                case 'update':
                    if ($this->permission->has('edit')) {
                        it("logs Module,Controller,Method,Action,Param,Msg", function() {
                            $data['name'] = Request::get('action');
                            SystemLog('Security','Security','actions','update',Request::all(),'Succesfully Update');
                        });
                        $response = Response::json(['error'=>false,'message'=>'Succesfully Update','action_id'=>$this->actions->where('action_id',Request::get('action_id'))->update($data)]);
                    }
                    break;
                case 'autoSaveAction':
                     $data = [
                        'link_id' => decode(Request::get('account')),
                        'page_id' => Request::get('page'),
                        'action_id' => Request::get('action')
                    ];
                    if ($this->isAccountGroup()) {
                        $data['link_type'] = 'group';
                        if ($this->UserAccess->where($data)->count() <= 0) {
                            $response = $this->UserAccess->insert($data);
                        }
                    } else {
                        $data['link_type'] = 'account';
                        if ($this->UserAccess->where($data)->count() <= 0) {
                            $response = $this->UserAccess->insert($data);
                        }
                    }
                    SystemLog('Security','Security','actions','autoSaveAction',Request::all(),'Succesfully Save');
                    $response = $response ? successSave() : errorSave();
                break;
                case 'autoUncheckAction':
                    if ($this->isAccountGroup()) {
                        $response = $this->UserAccess->where([
                            'link_type' => 'group',
                            'link_id' => decode(Request::get('account')),
                            'page_id' => Request::get('page'),
                            'action_id' => Request::get('action')
                        ])->delete();
                    } else {
                        $response = $this->UserAccess->where([
                            'link_type' => 'account',
                            'link_id' => decode(Request::get('account')),
                            'page_id' => Request::get('page'),
                            'action_id' => Request::get('action')
                        ])->delete();
                    }
                    SystemLog('Security','Security','actions','autoUncheckAction',Request::all(),successSave());
                    $response = $response ? successSave() : errorSave();
                break;
            }
            return $response;
        }   
        return $response;
    }

    public function access()
    {
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event'))
            {
                case 'getGroupAccess':
                    $this->access->AccountAccess('getGroupAccess',decode(Request::get('id'),true));
                    $response =  Response::json($this->access->treeData());
                    break;
                case 'getUserAccess':
                    $this->access->AccountAccess('getUserAccess',decode(Request::get('id'),true));
                    $response = Response::json($this->access->treeData());
                    break;
                case 'save':
                    $response = ['error'=>true,'message'=>'Permission Denied!'];
                    if ($this->permission->has('add')) {
                        it("logs Module,Controller,Method,Action,Param,Msg", function() {
                            $ids = [];
                            $access = json_decode('['.Request::get('access').']',true);
                            
                            foreach($access as $action)
                            {
                                $data = array(
                                    'link_type' => Request::get('account_type'),
                                    'link_id' => decode(Request::get('account')),
                                    'page_id' => $action['page'],
                                    'action_id' => $action['action']
                                );

                                $id = $this->UserAccess->select('index_id')->where($data)->get();
                                $ids[]  = (count($id)<=0) ? $this->UserAccess->create($data)->index_id : $id[0]->index_id;
                            }
                            
                            $this->UserAccess->whereNotIn('index_id',$ids)->where(['link_id'=>Request::get('account'),'link_type'=>Request::get('account_type')])->delete();
                            SystemLog('Security','Security','access','save',Request::all(),successSave());
                        });
                        $response = Response::json(['error'=>false,'message'=>'Succesfully save!']);
                    }
                    break;
                    
                    case 'showModalProgramAccess':                                            
                        $response = ['error'=>false,'content'=>view($this->views.'access.content.sub.access.program_access')->render()];
                    break;
                    
                    case 'saveTemplate':
                        if ($this->permission->has('insert-template')) {
                            $result = $this->ProgramAccessServices->save();
                            $response = $result ? successSave() : errorSave();
                            SystemLog('Security','Security','access','saveTemplate',Request::all(),$response);
                        }
                    break;
                    case 'saveUserProgramAccess':
                        if ($this->permission->has('save-user-template')) {
                            $result = $this->ProgramAccessServices->saveUserTemplate();
                            $response = $result ? successSave() : errorSave();
                            SystemLog('Security','Security','access','saveUserProgramAccess',Request::all(),$response);;
                        }
                    break;
                    case 'setDataAccess':
                    if( Request::get('val') == 'true' ){
                        $result = $this->ProgramAccessServices->setDataAccess();
                        $response = $result ? successSave() : errorSave();
                        SystemLog('Security','Security','Data Access','set Data Access','UserID : ' .  decode(Request::get('user')) . ' ProgID : ' . decode(Request::get('prog'))  ,$response);;
                    }else{
                        $result = $this->ProgramAccessServices->unsetDataAccess();
                        $response = $result ? successDelete() : errorDelete();
                        SystemLog('Security','Security','Data Access','unset Data Access','UserID : ' .  decode(Request::get('user')) . ' ProgID : ' . decode(Request::get('prog'))  ,$response);;                        
                    }                                            
                break;
                
                
                    case 'getUserProgramTemplate':
                        $response = $this->ProgramAccessServices->getUserProgramTemplate();
                    break;
                    
                    case 'getTemplates':
                        return $this->ProgramAccessServices->getTempates();
                    break;
            }
        }   
        return $response;
    }

    public function accounts()
    {   
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            switch(Request::get('event'))
            {
                case 'loadAccounts':
                    $response = Response::json($this->accounts->treeData());;
                    break;
                case 'loadAccess':
                    $response = Response::json($this->access->treeData());
                    break;
            }
        }   
        return $response;
    }

    public function validation(){   
        $redirect = '/';
		switch (strtolower(getUserGroup())){
            case 'parent':
			    /**/
			    $inquiry  = DB::select("SELECT TOP 1 ISNULL(a.AppNo,'') as AppNo,ISNULL(a.StatusID,0) as StatusID FROM ES_inquiry as i
											   INNER JOIN ES_inquiry_parent as ip ON i.InquiryParentID=ip.id
	                                           INNER JOIN ESv2_Users as u ON CONVERT(VARCHAR(255),ip.Email)=CONVERT(VARCHAR(255),u.Email)
											    LEFT JOIN ESv2_Admission as a ON ip.FamilyID=a.FamilyID
										            WHERE u.UserIDX='".getUserID()."' AND ISNULL(a.StatusID,0) NOT IN (2,3)
										    ORDER BY AppNo DESC");
				if(count($inquiry)>0){
				  putSessionData('inquired',1);
                  if($inquiry[0]->AppNo=='' || $inquiry[0]->StatusID<=0)	
                     $redirect = '/admission/apply';
				  else
					 $redirect = '/admission/edit?AppNo='.encode($inquiry[0]->AppNo); 
			    }else{
				  putSessionData('inquired',0);								
                  $redirect = '/guardian';
				}
                break;
            default:
                # code...
                break;
        }
        updateLastLogin();
        putSessionData('currentStep',1);
        return redirect($redirect);
    }

    private function isAccountGroup() 
    {
        if (strtolower(Request::get('account_type')) == 'group') {
            return true;
        }
        return false;
    }

    private function initializer()
    {
        $this->access = new Access;
        $this->UserAccess = new UserAccess;
        $this->accounts = new Accounts;
        $this->actions = new Actions;
        $this->ProgramAccessServices = new ProgramAccessServices;
        $this->permission = new Permission('access');
    }
}
