<?php

namespace App\Modules\Security\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Security\Models\Users\User as UserModel;
use App\Modules\Security\Services\UserServiceProvider;
use App\Modules\Security\Services\Access\ProgramAccess as ProgramAccessServices;
use Auth;
use Request;
use Response;
use Permission;
use Mail;

class Users extends Controller
{
    private $media = [
        'Title' => 'Users',
        'Description' => 'Manage',
        'js' => ['Security/user/user', 'Security/user/user_index', 'Security/program_access'],
        'init' => ['FN.multipleSelect()', 'FN.dataTable("#TableUsers")'],
        'plugin_js' => ['bootbox/bootbox.min',
                        'select2/select2.min',
                        'bootstrap-datepicker/js/bootstrap-datepicker',
                        'select2/select2.min',
                        'jquery-multi-select/js/jquery.multi-select',
                        'bootstrap-select/bootstrap-select.min',
                        'datatables/media/js/jquery.dataTables.min',
                        'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                        'datatables/extensions/Scroller/js/dataTables.scroller.min',
                        'datatables/plugins/bootstrap/dataTables.bootstrap',
                        ],
        'plugin_css' => ['bootstrap-select/bootstrap-select.min', 'select2/select2', 'jquery-multi-select/css/multi-select', 'datatables/plugins/bootstrap/dataTables.bootstrap'],
    ];

    private $url =
    [
        'form' => '.form_user',
        'page' => 'security/users/',
    ];

    public $views = 'Security.Views.';

    public function index()
    {   ini_set('max_execution_time', 300);
        $this->initializer();
        if ($this->permission->has('read')) {
            // SystemLog("Setup","User","logout","view");
            return view('layout', array('content' => view($this->views.'users.index', $this->init())->render(), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.404'));
    }

    public function init($key = null)
    {
		return [];
    //     return array('users' => $this->model
    //         ->data()
    //         ->selectRaw(' UserIDX, g.GroupName , p.PositionDesc , FullName , GroupID , dbo.fn_StudentMajorDiscName(UserID) As Major , LastDateLogin , ConfirmNo,Username, Email,DeptName')
    //         ->get(),
    //    );
    }

    public function event()
    {
        $response = 'No Event Selected';
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'getForm':
                    $response = view($this->views.'users.forms.user');
                    break;
                case 'save':
                    if ($this->permission->has('add')) {
                        $validation = $this->services->isValid(Request::all());
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            it('logs Module,Controller,Method,Action,Param,Msg', function () {
                                $this->model->create($this->services->postSave(Request::all()));
                                SystemLog('Security', 'Users', 'event', 'save', Request::all(), successSave());
                            });
                            $response = ['error' => false, 'message' => 'Successfully Save!'];
                        }
                    }
                    break;
                case 'update':
                    if ($this->permission->has('edit')) {
                        $validation = $this->services->isValid(Request::all(), 'update');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            it('logs Module,Controller,Method,Action,Param,Msg', function () {
                                $p = Request::all();
                                $data = $this->services->postUpdate(Request::all());

                                unset($data['username']);

                                if ($p['upassword'] != '') {
                                    if ($p['upassword'] === $p['cpassword']) {
                                        $data['password'] = bcrypt(getObjectValue($p, 'upassword'));
                                    }
                                } else {
                                    unset($data['upassword'], $data['cpassword']);
                                }

                                $this->model->where('UserIDX', decode(Request::get('id')))->update($data);

                                SystemLog('Security', 'Users', 'event', 'update', Request::all(), successSave());
                            });

                            $response = ['error' => false, 'message' => 'Successfully Update!'];
                        }
                    }
                    break;
                case 'delete':
                        if ($this->permission->has('delete')) {
                            it('logs Module,Controller,Method,Action,Param,Msg', function () {
                                $ids = json_decode('['.Request::get('ids').']', true);
                                foreach ($ids as $id) {
                                    $res = $this->model->destroy(decode($id['id']));
                                    err_log($res);
                                }
                                SystemLog('Security', 'Users', 'event', 'delete', Request::all(), successDelete());
                            });
                            $response = successDelete();
                        }
                    break;
                case 'block':
                        if ($this->permission->has('block')) {
                            it('logs Module,Controller,Method,Action,Param,Msg', function () {
                                $ids = json_decode('['.Request::get('ids').']', true);
                                foreach ($ids as $id) {
                                    $status = $this->model->where('UserIDX', decode($id['id']))->update(['IsBlock' => 1]);
                                }

                                SystemLog('Security', 'Users', 'event', 'block', Request::all(), 'Successfully Block User/s');
                            });
                            $response = ['error' => false, 'message' => 'Successfully Block User/s'];
                        }
                    break;
                case 'edit':
                        // print_r($this->model->get([
                        //     'UserIDX' => decode(Request::get('id')),
                        //     ]));
                        //die();
                        $response = view($this->views.'users.forms.user', [
                            'data' => $this->model->get([
                            'UserIDX' => decode(Request::get('id')),
                            ]),
                        ]);
                    break;
                case 'checkUsername':
                        $response = ['isExist' => $this->model->where('Username', Request::get('id'))->count()];
                    break;
                case 'showModalProgramAccess':
                        $response = ['error' => false, 'content' => view($this->views.'access.content.sub.access.program_access')->render()];
                    break;
                case 'saveTemplate':
                    if ($this->permission->has('insert-template')) {
                        $result = $this->ProgramAccessServices->save();
                        $response = $result ? successSave() : errorSave();
                        SystemLog('Security', 'Security', 'access', 'saveTemplate', Request::all(), $response);
                    }
                break;
                case 'saveUserProgramAccess':
                    if ($this->permission->has('save-user-template')) {
                        if (Request::get('TemplateID') != 'undefined') {
                            $result = $this->ProgramAccessServices->saveUserTemplate();
                        } else {
                            $result = false;
                        }

                        $response = $result ? successSave() : errorSave();
                        SystemLog('Security', 'Security', 'access', 'saveUserProgramAccess', Request::all(), $response);
                    }
                   break;
                case 'setDataAccess':
                    if (Request::get('val') == 'true') {
                        $result = $this->ProgramAccessServices->setDataAccess();
                        $response = $result ? successSave() : errorSave();
                        SystemLog('Security', 'Security', 'Data Access', 'set Data Access', 'UserID : '.decode(Request::get('user')).' ProgID : '.decode(Request::get('prog')), $response);
                    } else {
                        $result = $this->ProgramAccessServices->unsetDataAccess();
                        $response = $result ? successDelete() : errorDelete();
                        SystemLog('Security', 'Security', 'Data Access', 'unset Data Access', 'UserID : '.decode(Request::get('user')).' ProgID : '.decode(Request::get('prog')), $response);
                    }
                break;

                case 'getUserProgramTemplate':
                    $response = $this->ProgramAccessServices->getUserProgramTemplate();
                       break;
                case 'getTemplates':
                    return $this->ProgramAccessServices->getTempates();
                    break;

                case 'getEmail':
                    $id = Request::get('user');
                    if (!empty($id)) {
                        $get = $this->model->select('Email')->where('UserIDX', decode($id))->first();
                        $response = ['error' => false, 'email' => $get->Email];
                    } else {
                        $response = ['error' => true, 'message' => 'Unable to find email.'];
                    }
                    break;

				case 'get':
					if (getUserGroup() == 'administrator' || getUserGroup()=='executive_sec') {
						$r = Request::all();
						$sort_col_index = $r['order'][0]['column'];
						$sort = $r['order'][0]['dir'];

						// Column Index => DB Fielname
						$colum_sorts = [2 => 'u.Fullname', 3 => 'Username', 4 => 'g.GroupName', 5 => 'Email', 6 => 'p.PositionDesc', 7 => ['Major', 'DeptName'], 8 => 'LastDateLogin', 9 => 'u.LastActivity'];

						if (is_array($colum_sorts[$sort_col_index])) {
							$arr = array_map(function ($a) use ($sort) { return $a.' '.$sort; } , $colum_sorts[$sort_col_index]);
							$sorting = implode(', ', $arr);

						} else {
							$sorting = $colum_sorts[$sort_col_index].' '.$sort;
						}

						if (!empty($r['search']['value'])) {
							$v = $r['search']['value'];

							$data = $this->model->data()->selectRaw('total_count = COUNT(*) OVER(), dbo.fn_StudentName(UserID) As StudentName, UserIDX, g.GroupName, p.PositionDesc, u.FullName, GroupID, dbo.fn_StudentMajorDiscName(UserID) As Major, LastDateLogin , ConfirmNo, Username, Email, DeptName, u.LastActivity')
							->where('Fullname', 'like', '%'.$v.'%')
							->orWhere('Username', 'like', '%'.$v.'%')
							->orderByRaw($sorting)
							->skip($r['start'])
							->take($r['length'])
							->get();

						} else {
							$data = $this->model->data()->selectRaw('total_count = COUNT(*) OVER(), dbo.fn_StudentName(UserID) As StudentName, UserIDX, g.GroupName, p.PositionDesc, u.FullName, GroupID, dbo.fn_StudentMajorDiscName(UserID) As Major, LastDateLogin , ConfirmNo, Username, Email, DeptName, u.LastActivity')
							->orderByRaw($sorting)
							->skip($r['start'])
							->take($r['length'])
							->get();

						}

	                    $response = $this->formatDataToDisplay($data, $r);
					}
					break;
            }
        }

        return $response;
    }

    public function logout()
    {
        try {
            echo redirect()->intended('auth/login');
            SystemLog('Users', 'Users', 'Log-Out', 'Log-out', 'UserID : '.getUserID());

            Auth::logout();

            return redirect()->intended('auth/login');
        } catch (Exception $e) {
            //return $e;
        }
    }

	private function formatDataToDisplay($data, $settings)
    {
        $return_arr = [];

        if(!empty($data)){
            $total_count = $data[0]->total_count;
            $return_arr = array(
                'recordsTotal' => $total_count,
                'recordsFiltered' => $total_count,
            );

            for($a = 0; $a < count($data); $a++){

                $_this = $data[$a];
                $cur_count = $a + 1;
				$group = ($_this->GroupID == 7) ? $_this->Major : $_this->DeptName;

                $return_arr['data'][] = array(
					'DT_RowClass' => 'without-bg',
					'DT_RowData' => ['id' => encode($_this->UserIDX)],
                    '<input type="checkbox" class="chk-child">',
					// $cur_count,
                    '<img src="'.url().'/general/getUserPhoto?UserID='.encode($_this->UserIDX).'" width="30" alt="">',
					'<a href="javascript:void(0)" class="a_select">'.trimmed($_this->FullName).'</a>',
					$_this->Username,
					$_this->GroupName,
					$_this->Email,
					$_this->PositionDesc,
					$group,
					$_this->LastDateLogin,
					$_this->LastActivity,
                );
            }

        } else {
            $return_arr = array(
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => []
            );
        }

        return $return_arr;
    }

    private function initializer()
    {
        $this->services = new UserServiceProvider();
        $this->model = new UserModel();
        $this->ProgramAccessServices = new ProgramAccessServices();
        $this->permission = new Permission('users');
    }
}