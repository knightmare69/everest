<?php namespace App\Modules\Security\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Security\Models\Password\Password as PasswordModel;
use App\Modules\Security\Services\UserServiceProvider;
use Auth;
use Request;
use Response;
use Permission;

class Password extends Controller {
	private $media = [
    	'Title'     => 'Password',
        'Description' => 'Config',
        'js'		=> ['Security/password'],
		'init'		=> ['Password.init()','FN.multipleSelect()'],
		'plugin_js'	=> ['bootbox/bootbox.min', 'select2/select2.min',
                        'bootstrap-datepicker/js/bootstrap-datepicker',
                        'select2/select2.min',
                        'jquery-multi-select/js/jquery.multi-select',
                        'bootstrap-select/bootstrap-select.min',
                        'datatables/media/js/jquery.dataTables.min',
			            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
			            'datatables/extensions/Scroller/js/dataTables.scroller.min',
			            'datatables/plugins/bootstrap/dataTables.bootstrap',
			            'bootstrap-switch/js/bootstrap-switch.min'
                        ],
        'plugin_css'   => [
        	'bootstrap-select/bootstrap-select.min','select2/select2','jquery-multi-select/css/multi-select',
        	'bootstrap-switch/css/bootstrap-switch.min'
        ],
    ];

    private $url = [
        'form' => '.form_user',
        'page'  => 'security/password/'
    ];

    public $views = 'Security.Views.password.';

	function index()
	{
		$this->initializer();
		if ($this->permission->has('read')) {
			SystemLog("Setup","User","logout","view");
			return view('layout',array('content'=>view($this->views.'index',$this->init())->render(),'url'=>$this->url,'media'=>$this->media));
		}
		return view(config('app.403'));
	}

	function init($key=null)
	{
		return array(
			'views' => $this->views,
			'data' => $this->model->get()
		);
	}

	function event()
	{
		$response = 'No Event Selected';
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'save':
					if ($this->permission->has('edit')) {
						$this->model->where('index_id',decode(Request::get('key')))
							->update(['value'=>Request::get('value')]);
					} 
					$response = successSave();
					break;
			}
		}
		return $response;
	}

	function logout()
	{
		SystemLog("Setup","User","logout","logout",getUserName());
		Auth::logout();
		return redirect()->intended('auth/login');
	}

	private function initializer()
	{
		$this->services = new UserServiceProvider;
		$this->model = new PasswordModel;
		$this->permission = new Permission('pswd-config');
	}

}

/*
insert into pages values('Password Config','pswd-config','','','',3,0,0,0,0,0)
alter table ESv2_Users
add PwdExpiryDate datetime

alter table ESV2_Users
add LastLoginDate datetime

INSERT INTO ESV2_password_config VALUES ( 'Has Special Character', '0', 'boolean');
INSERT INTO ESV2_password_config VALUES ( 'Special Characters', '#$*^&!@', '');
INSERT INTO ESV2_password_config VALUES ( 'Is Character Repetition', '0', 'boolean');
INSERT INTO ESV2_password_config VALUES ( 'Maximum Character repetition', '3', '');
INSERT INTO ESV2_password_config VALUES ( 'Is Digit Mandatory', '0', 'boolean');
INSERT INTO ESV2_password_config VALUES ( 'Is Upper Case Mandatory', '0', 'boolean');
INSERT INTO ESV2_password_config VALUES ( 'Is Lower Case Mandatory', '0', 'boolean');
INSERT INTO ESV2_password_config VALUES ( 'Maximum Length', '30', '');
INSERT INTO ESV2_password_config VALUES ( 'Minimum Length', '6', '');
INSERT INTO ESV2_password_config VALUES ( 'Password expirey Days', '12', '');
INSERT INTO ESV2_password_config VALUES ( 'Password History', '5', '');
INSERT INTO ESV2_password_config VALUES ( 'Default Password', 'pswd123', '');
INSERT INTO ESV2_password_config VALUES ( 'Maximum Invalid Entry', '10', '');
INSERT INTO ESV2_password_config VALUES ('Has Password expiration', '1', 'boolean');
*/