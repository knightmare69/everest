<?php

namespace App\Modules\Enrollment\Models;

use illuminate\Database\Eloquent\Model;

class mAddrop extends Model
{
    protected $table = 'ES_AddChangeDropSubjects';
    protected $primaryKey = 'RefID';
    protected $fillable = ['Date'
      ,'RegID'
      ,'Reason'
      ,'OfficerID'
      ,'ORNo'
      ,'AssessedDate'
      ,'AssessedBy'
      ,'ValidationDate'
      ,'ValidationOfficerID'
      ,'TotalNetAssessed'
      ,'TotalPayment'
      ,'TotalDiscount'
      ,'TotalDebitMemo'
      ,'TotalCreditMemo'
      ,'RemoveValidationBy'
      ,'RemoveValidationDate'
      ,'Explanation'
      ,'SummaryInfo'
      ,'ChargeToFinancialAid'
      ,'TotalFinancialExternal'
      ,'SpecialTransNo'
      ,'SchoProviderType'
      ,'SchoProviderID'
      ,'SchoAllAccount'
      ,'SchoPercentage'
      ,'GrantTemplateID'
      ,'ValidationType'
      ,'SchoOptionID'
      ,'SpecialClass_ChargeAddtleFee'
      ,'Remarks'
      ,'BillingNotes'
    ];
      
    public $timestamps = false;
}
?>