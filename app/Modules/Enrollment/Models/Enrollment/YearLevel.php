<?php

namespace App\Modules\Enrollment\Models\Enrollment\YearLevel;

use illuminate\Database\Eloquent\Model;

class YearLevel extends Model
{
    protected $table = 'ESv2_YearLeavel';
    protected $primaryKey = 'YearLevelID';

    public $timestamps = false;
}
?>