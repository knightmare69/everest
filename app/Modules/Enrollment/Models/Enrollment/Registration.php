<?php

namespace App\Modules\Enrollment\Models\Registration;

use illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = 'ES_Registrations';
    protected $primaryKey = 'RegID';

    protected $fillable = ['link_id', 'link_type','page_id','action_id'];

    public $timestamps = false;
}
