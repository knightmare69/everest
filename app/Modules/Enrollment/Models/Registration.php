<?php

namespace App\Modules\Enrollment\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Registration extends Model
{
    protected $table = 'ES_Registrations';
    protected $primaryKey = 'RegID';
    protected $fillable = ['StudentNo',
						   'RegDate',
						   'ExpireDate',
						   'TermID',
						   'CampusID',
						   'AdvisingOfficerID',
						   'ValidatingOfficerID',
						   'ValidationDate',
						   'ORNo',
						   'AssessedFees',
						   'Discount',
						   'TableofFeeID',
						   'SchoProviderType',
						   'SchoProviderID',
						   'SchoAllAccount',
                           'SchoPercentage',
                           'GrantTemplateID',
                           'LastPrintoutDate',
						   'LastModifiedBy',
						   'LastModifiedDate',
						   'AssessedBy',
						   'AssessedDate',
						   'FirstPaymentDueDate',
						   'SecondPaymentDueDate',
						   'ThirdPaymentDueDate',
						   'FourthPaymentDueDate',
						   'FifthPaymentDueDate',
						   'ClassSectionID',
						   'IsWithdrawal',
						   'YearLevelID',
						   'TotalAssessment',
						   'TotalFinancialAid',
						   'TotalNetAssessed',
						   'TotalPayment',
						   'TotalDiscount',
						   'TotalNonLedger',
						   'TotalRefund',
						   'TotalBalance',
						   'CollegeID',
						   'ProgID',
						   'MajorID',
						   'DateGradeCardGenerated',
						   'RetentionStatus',
						   'SubjectsEnrolled',
						   'CreditUnitsEnrolled',
						   'CreditUnitsEarned',
						   'GWA',
						   'AcademicScholarship',
						   'RedRemarks',
						   'NumberofPayments',
						   'TotalDebitMemo',
						   'TotalCreditMemo',
						   'ReservationORNo',
						   'ReservationAmount',
						   'ReservationMemoID',
						   'RemovedValidationBy',
						   'RemovedValidationDate',
						   'SchoDelinquencyRemarks',
						   'DateWithdrawn',
						   'Remarks',
						   'TotalLecUnits',
						   'TotalLabUnits',
						   'ValidationType',
						   'SchoOptionID',
						   'TotalFinancialAidExternal',
						   'SessionID',
						   'TotalPreviousBalance',
						   'MaxUnitsAllowed',
						   'SchoStatusID',
						   'AwardNo',
						   'SixthPaymentDueDate',
						   'SeventhPaymentDueDate',
						   'EightPaymentDueDate',
						   'NinethPaymentDueDate',
						   'TenthPaymentDueDate',
						   'ConfigDateID',
						   'NonZeroBase',
						   'IsResidencyOnly',
						   'IsOnlineEnrolment'];
    public $timestamps = false;


    public function getClassSectionName($sec_id){

        $data = DB::select(DB::raw(" SELECT dbo.fn_SectionName('".$sec_id."') AS SectionName "));
		return $data;

    }

    public function getProgClassID($prog_id){
        $data = DB::select(DB::raw(" SELECT dbo.fn_ProgramClassCode('".$prog_id."') AS ClassID "));
		return $data;
    }

    public function getYearLevelName($yearlevel, $progcls){
        $data = DB::select(DB::raw(" SELECT dbo.fn_YearLevel2('".$yearlevel."','".$progcls."') AS YearLevel "));
		return $data;
    }

    public function scopeValidated($query,$term){        
		return $query->whereNotNull('ValidationDate')->where('termid',$term);
    }
    
}