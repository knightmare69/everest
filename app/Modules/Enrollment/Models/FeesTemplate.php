<?php

namespace App\Modules\Enrollment\Models;

use illuminate\Database\Eloquent\Model;

class FeesTemplate extends Model
{
    protected $table = 'ES_TableofFees';
    protected $primaryKey = 'TemplateID';
    protected $fillable = ['TemplateCode'
      ,'TemplateDesc'
      ,'TemplateRemarks'
      ,'TermID'
      ,'CampusID'
      ,'LastModified'
      ,'ModifiedBy'
      ,'ForForeign'
      ,'TransType'
      ,'InActive'
      ,'CurrencyID'
      ,'StudentStatus'
      ,'StudentGender'
      ,'PaymentScheme'
      ,'PaymentOption'
      ,'LockedBy'
      ,'LockedDate'
      ,'PracticumOnly'
      ,'Total1stPayment'
      ,'Total2ndPayment'
      ,'Total3rdPayment'
      ,'Total4thPayment'
      ,'Total5thPayment'
      ,'Total6thPayment'
      ,'Total7thPayment'
      ,'Total8thPayment'
      ,'Total9thPayment'
      ,'Total10thPayment'
      ,'TotalNumPayment'
      ,'TotalAmount'
      ,'TotalNumMonth'
      ];
    
    public $timestamps = false;
    
    
    public function scopeDistribution($query, $prog, $major){
        return $query->leftJoin("ES_TableofFee_DistributionList as p","p.TemplateID","=","ES_TableofFees.TemplateID")
                ->where(["ProgID"=>$prog, "MajorID"=>$major]);
    }
    
}
?>