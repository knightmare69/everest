<?php

namespace App\Modules\Enrollment\Models;

use illuminate\Database\Eloquent\Model;

class YearLevel extends Model
{
    protected $table = 'ESv2_YearLevel';
    protected $primaryKey = 'YearLevelID';

    public $timestamps = false;
}
?>