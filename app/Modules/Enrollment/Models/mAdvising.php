<?php

namespace App\Modules\Enrollment\Models;

use illuminate\Database\Eloquent\Model;

class mAdvising extends Model
{
    protected $table = 'ES_Advising';
    protected $primaryKey = 'AdvisedID';
    protected $fillable = ['TermID'
      ,'CampusID'
      ,'CollegeID'
      ,'ProgID'
      ,'MajorID'
      ,'StudentNo'
      ,'YearLevel'
      ,'MinLoad'
      ,'MaxLoad'
      ,'AdvisedSubject'
      ,'AdvisedUnits'
      ,'AdviserID'
      ,'DateAdvised'
      ,'ModifiedBy'
      ,'DateModified'
      ,'AccessCode'
      ,'FeesID'
      ,'Accountabilities'
      ,'OutBalance'
      ,'RegID'
      ,'IsOnlineAdvising'
      ,'IsGraduating'
      ,'IsRegularStudent'
      ,'IsNewStudent'
      ,'EnrolmentStatus'
    ];
      
    public $timestamps = false;
}
?>