<?php
namespace App\Modules\Enrollment\Controllers;
use App\Http\Controllers\Controller;

use App\Modules\Setup\Models\GradingSystemSetup As mSetup;
use App\Modules\Enrollment\Models\Registration as mRegistration;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Registrar\Models\TermConfig as term_model;
use App\Modules\Admission\Models\GuardianBackround;
use Request;
use Response;
use Permission;
use DB;
use CrystalReport;

class Promotions extends Controller{

	private $media =
		[
			'Title'=> 'Student Promotions',
			'Description'=> 'Student Promotions',
			'js'		=> ['Enrollment/promotions'],
			'init'		=> ['MOD.init()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap',

                'icheck/icheck',
                'SmartNotification/SmartNotification.min'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all',
                'SmartNotification/SmartNotification',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
			]
		];

	private $url = [ 'page' => 'enrollment/promotions' ];

	private $views = 'Enrollment.Views.promotions.';

	function __construct()
	{
		$this->initializer();
	}

 	public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $mterm = new term_model;

            $term = decode(Request::get('t'));
            $l = decode(Request::get('y'));
            $p = Request::get('p');
            $f = Request::get('f');
            $v = Request::get('v');

            if( $p == '0' || $p == 'undefined' ) {
                $p = 0;
            } else{
                $p = decode(Request::get('p'));
            }

            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),
                'data' => $this->getlist($term,$p,$l,$f,($v == 'undefined' ? 1 : $v)),
                'total' => $this->getTotal($term,$p,$l,$f),
                'term' => $term,
                'level' => $l,
                'prog' => $p,
                'filter'=>$f,
                'page'=> ($v == 'undefined' ? 1 : $v)
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getlist($term, $prog, $level, $filter='', $page=1){
        $model = new mRegistration();
        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        //$skip = ($page == 1 ? 1 : ($take * $page) + 1); updated : due to some records are skipped
        $skip = ($page == 1 ? 0 : ($take * ($page == 0 ? 0 : $page-1)));
        $rs = null;
        if($term != ''){

        $whereRaw = "TermID = {$term} AND ValidationDate IS NOT NULL AND  IsWithdrawal = 0 ";

        if( $prog != '0' && $prog != '' ) $whereRaw .= " AND ProgID = '{$prog}'";
        if( $level != '0' && $level != '') $whereRaw .= " AND YearLevelID = '{$level}'";


        if($filter == '0'){
            $whereRaw .= " AND dbo.fn_Promoted(RegID) = '0'";
        }elseif($filter == '1'){
            $whereRaw .= " AND dbo.fn_Promoted(RegID) = '1'";
        }

        $query = "SELECT *, dbo.fn_StudentName(StudentNo) As StudentName,
                        dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel,
                        dbo.fn_majorName(MajorID) As Major,
                        dbo.fn_Promoted(RegID) As Promoted,
                        IsNull((SELECT TOP 1 SA.Reason FROM ES_StudentAccountabilities SA WHERE SA.StudentNo = ES_Registrations.StudentNo AND Cleared = 0), '') as Accountabilities,
                        IsNull((SELECT Top 1 TrackID FROM dbo.ES_Admission_Reservation WHERE IDNo = ES_Registrations.StudentNo),0) As TrackID
                  FROM  ES_Registrations WHERE " . $whereRaw . " Order By StudentName ASC
                  OFFSET     {$skip} ROWS
					 FETCH NEXT {$take}  ROWS ONLY
                  " ;
        //err_log($query);
        $rs = DB::select($query);

        }

        return $rs;
        /*
        return $model->Validated($term)
                     ->whereRaw($whereRaw)
                     ->selectRaw("*, dbo.fn_StudentName(StudentNo) As StudentName,
                                     dbo.fn_StudentYearLevel_k12(StudentNo) As YearLevel,
                                     dbo.fn_majorName(MajorID) As Major,
                                     dbo.fn_Promoted(RegID) As Promoted,
                                     IsNull((SELECT TOP 1 SA.Reason FROM ES_StudentAccountabilities SA WHERE SA.StudentNo = ES_Registrations.StudentNo AND Cleared = 0), '') as Accountabilities,
                                     IsNull((SELECT Top 1 TrackID FROM dbo.ES_Admission_Reservation WHERE IDNo = ES_Registrations.StudentNo),0) As TrackID
                       ")
                     ->orderBy('StudentName','asc')
                     ->limit($take)
                     ->get();
          */
    }

    private function getTotal($term, $prog, $level, $filter=''){
        $model = new mRegistration();

        $whereRaw = "IsWithdrawal = 0 AND ProgID = '{$prog}' AND YearLevelID = '{$level}'";

        if($filter == '0'){
            $whereRaw .= " AND dbo.fn_Promoted(RegID) = '0'";
        }elseif($filter == '1'){
            $whereRaw .= " AND dbo.fn_Promoted(RegID) = '1'";
        }

        return $model->Validated($term)
                    ->whereRaw($whereRaw)
                     ->count();
    }

 	function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
			    case 'set':

                    if(Request::get('name') == 'track')
                    {
                        $data = ['TrackID' =>  Request::get('val')];
                        $where = ['EntryID' => decode(Request::get('id'))];
                        $res = $this->model->where($where)->update($data);
                    }

                    $response = successUpdate();

                break;
				 case 'get-students':
                    if($this->permission->has('read')){
                        $model = new mStudents();

                        $find = Request::get('find');

                        if( $find == ''){
                            $find = date('Y').'%';
                        }

                        $data = $model->studentsSearch($find,'50');

                        if(!empty($data)){
                            $vw  = view($this->views.'students', ['data' => $data])->render();
                            $response = ['error' => false, 'data' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }
                    }
                    break;

				case 'save':

                    $term = decode(Request::get('term'));
                    $mprog = decode(Request::get('p'));
                    $mlevel = decode(Request::get('y'));
                    $mfilter = (Request::get('f'));
                    $page = (Request::get('v'));
                    $nos = (Request::get('nos'));

					$no_promoted=0;

                    foreach($nos as $i){

                        $ref = decode($i['ref']);
                        $idno = decode($i['idno']);
                        $prog = ($i['prog']);
                        $major = getObjectValue($i,'major');
                        $level = ($i['level']);

                        $review = $this->promoteThis($ref, $idno,$prog,$major,$level );
                        if($review){
                            SystemLog('Promotions', '', 'Promote','Confirm Promotion','RegID = '. $ref ,'Student Promoted!');
                            $no_promoted++;
                        }
                    }
                	if($no_promoted>0){
                        $data = $this->getlist($term,$mprog,$mlevel,$mfilter, $page );
                        $param = ['data' => $data, 'term' => $term,
                                    'level' => $mlevel,
                                    'prog' => $mprog,
                                    'filter' =>$mfilter,
                                    'page' => $page,
                                    'total' => $this->getTotal($term,$mprog,$mlevel, $mfilter ),
                                  ];

                        $vw  = view($this->views.'list',$param)->render();
                        $response = [ 'error'=>false, 'message'=> "Record successfully saved!", 'data'=> $vw];
                	}else{
                        $response = [ 'error'=>true, 'message'=> "no student promoted "];
                	}


                break;

			}
		}
		return $response;
	}

    private function promoteThis($reg,$idno,$prog, $major , $level ){
        $term = decode(Request::get('term'));
        
        DB::update("UPDATE dbo.ES_PermanentRecord_Master SET IsPromoted = '1' WHERE TermID = '{$term}' AND RegID = '{$reg}' AND StudentNo = '{$idno}'");
        
          DB::update("UPDATE ES_Students SET ProgID = '{$prog}', MajorDiscID = '{$major}' , YearLevelID='{$level}' 
                    CurriculumID = CASE WHEN ProgID <> $prog THEN ISNULL((SELECT TOP 1 IndexID FROM dbo.ES_Curriculums WHERE ProgramID = '{$prog}' AND MajorID='{$major}' ORDER BY CurriculumCode DESC),0) ELSE CurriculumID END  
                    WHERE StudentNo = '{$idno}' ");
                
        return true;        
        
    }

	public function printPDF()
	{
		$CrystalReport = new CrystalReport;

		$CrystalReport->filename = 'admission_exampermit.rpt';

		$CrystalReport->query = "EXEC dbo.ES_rptAdmissionExaminationPermit '400454', 'San Beda College Alabang', '', 'Benavidez St., Sta. Cruz, Manila', 'Alabang Campus', 'admin'";

		$file = $CrystalReport->generate();

		header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $file['filename'] . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file['file']);
        unlink($file['file']);
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-reservation');
 		$this->model = new mRegistration;
 	}

}
