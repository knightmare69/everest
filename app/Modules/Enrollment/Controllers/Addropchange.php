<?php

namespace App\Modules\Enrollment\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as tModel;
use App\Modules\Enrollment\Models\mAddrop;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Enrollment\Services\AddDropChangeServices as services;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF;

use Permission;
use Request;
use Response;
use DB;

class Addropchange extends Controller
{
    protected $ModuleName = 'addropchange';

    private $media = [
            'Title' => 'Add/Drop/Change of Subject',
            'Description' => 'Welcome To Add/Drop/Change of Subject',
            'js' => ['enrollment/addrop.js?v=1.1'],
            'css' => ['profile'],
            'init' => ['MOD.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'SmartNotification/SmartNotification.min'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'SmartNotification/SmartNotification',
            ],
        ];

    private $url = ['page' => 'add-drop-change'];

    private $views = 'Enrollment.Views.addrop.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{	
 		$this->initializer();
        if ($this->permission->has('read')) {
                
            $mterm = new tModel;
            
            $term = decode(Request::get('t'));
            $f = Request::get('f');
            $v = Request::get('v');           
                             
            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),   
                'data' => $this->getData('list',$term,$f,($v == 'undefined' ? 1 : $v)),
                'total' => $this->getData('total',$term,$f),      
                'term' => $term,   
                'filter'=>$f,  
                'page'=> ($v == 'undefined' ? 1 : $v)
            ];        
            
            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
            
 	}
    
    private function getData($mode,$term, $filter='', $page=1){
        
        ini_set('max_execution_time', 120);
        
        $whereRaw = "";
        
        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;
        if($term != ''){                    
        
            $whereRaw = "r.TermID = {$term} ";                        
            $whereRaw .= " AND r.ProgID IN (".implode(getUserProgramAccess(),',') .")";            
            
            $query = "SELECT a.*, r.StudentNo, dbo.fn_StudentName(r.StudentNo) As StudentName, 
                            dbo.fn_K12_YearLevel3(r.YearLevelID, r.ProgID) As YearLevel, r.MajorID, 
                            dbo.fn_majorName(MajorID) As Major                             
                      FROM  ES_AddChangeDropSubjects a
                        Left Join ES_Registrations r on a.RegID=r.RegID 
                      WHERE " . $whereRaw . " Order By StudentName ASC 
                      OFFSET     {$skip} ROWS 
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('ES_AddChangeDropSubjects As a')
                        ->leftJoin('ES_Registrations As r','r.RegID','=','a.RegID')
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }                            
        }
        
        return $rs;                        
    }
    
    public function advise(){
        $this->initializer();
        if ($this->permission->has('read')) {
            
            $model = new mStudents();
            $mterm = new tModel;
            $lock = 0;
            
            $term = decode(Request::get('t'));
            $id = decode(Request::get('idno'));                       
            $ref = decode(Request::get('ref'));
            
            $balance = number_format( getOutstandingBalance('1',$id, systemDate(), $term ) , 4 , ".", ",") ;
            $reg_id = $this->getRegistration($term,$id);
            
            $stud = $model->where('StudentNo', $id)->selectRaw("StudentNo, LastName, FirstName, ProgID, YearLevelID, dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel,  CurriculumID, dbo.fn_CurriculumCode(CurriculumID) AS CurriculumCode , dbo.fn_MajorName(MajorDiscID) As Major ")->first();
            
            
            if($ref != '0' && $ref != '' ){
                $advised = $this->getRegSubjectWithADC($reg_id,$ref );    
            }else{
                $advised = $this->getSubjectRegistered($reg_id);                    
            }                                                            
                        
            $_incl = [
                'views' => $this->views,
                'term' => $term,
                'academic_year' => $mterm->AcademicTerm(),
                'idno' => $id,
                'data' => $stud,
                'advised' => $advised,
                'balance' => $balance,
                'regid' => $reg_id
                ,'locked' => $this->IsLocked($ref)
            ];        
            
            return view('layout',array('content'=>view($this->views.'registered')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
                case 'drop-subject':
                                        
                    $response = ['error' => false, 'message' => 'record successfully', 'grade'=> 'CRD', 'remarks'=>'Credited' ];
                    
                break;
                
                case 'save':
                
                    if($this->permission->has('read')){
                        
                        $model = new mStudents();
                        
                        
                        $idno = decode(Request::get('idno'));
                        $term = (Request::get('term'));
                        $ref = decode(Request::get('ref'));
                        
                        $rs = DB::table("ES_Registrations")->selectRaw("RegID, MajorID, YearLevelID,TableofFeeID ")->where("TermID",$term)->where('StudentNo',$idno)->first();
                                                                       
                        $fees_id = $rs->TableofFeeID;
                        $campus=1;
                        $regid = 0;                                                                                                
                        $method = 0 ;
                        $appno = '';
                        $student = $model->where('StudentNo',$idno)->first();
                        
                        $major = $rs->MajorID;
                        $appno = '';
                        $amt = 0;                                                
                        $yrlvl = $rs->YearLevelID;
                        
                                                                                                                                                                                                                                                                       
                        $subj = (Request::get('subj'));
                        $unit = 0;
                        
                        if($ref == ''){
                            $ref = $this->service->saveNewADC($rs->RegID,$subj);                                                        
                        }                                                                                                                                                                                                              
                        
                        $subs = $this->service->saveADCdetails($ref,$subj);
                        $journal = $this->service->processAssessment($term, $campus, $idno, $ref,  $fees_id, $subs['totalunit'] );
                                                
                        $response = ['error' => false, 'message' => 'record successfully', 'content'=> $rs, 'reg'=> encode($rs->RegID)];
                                                        
                    }
                    
                
                break;
                    
                case 'get-students':
                    
                    if($this->permission->has('read')){
                        
                        $where = "ValidationDate IS NOT NULL AND IsWithdrawal = 0 ";
                                            
                        $term = decode(Request::get('term'));
                        $find = Request::get('find');                        
                        if( $find != ''){ 
                            $where .= " AND (StudentNo Like '%".$find."%' OR dbo.fn_StudentName(StudentNo) LIKE '%".$find."%')";
                        }                                     
                        
                        $data = DB::table("ES_Registrations")
                                ->selectRaw("StudentNo, dbo.fn_StudentName(StudentNo) As FullName, YearLevelID,  dbo.fn_YearLevel_k12(YearLevelID, dbo.fn_ProgramClassCode(ProgID)) As YearLevel ")
                                ->where("TermID",$term)
                                ->whereRaw($where)
                                ->wherein('ProgID', getUserProgramAccess() )
                                ->limit(30)
                                ->get();

                        if(!empty($data)){
                            $vw  = view($this->views.'students', ['data' => $data])->render();
                            $response = ['error' => false, 'data' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }
                    }                                        
                
                break;
                    
            
                 case 'remove':
                    
                    $list = Request::get('idno');
                    
                    foreach($list as $r){
                        $ref = decode($r['ref']);
                        $ret = $this->model->where(['RefID' => $ref ])->delete();
                        $idno = decode($r['idno']);
                        
                        DB::statement("DELETE ES_Journals WHERE TransID = 2 AND ReferenceNo = '{$ref}' AND IDType =1 AND IDNo ='{$idno}'");
                        SystemLog('Add/Drop/Change', 'ADC', 'Purge ADC', 'Purge Add/Drop/Change', 'RefID:='. $ref . ' IDNo:='.$idno , 'Purge adding and dropping of transaction.');                                                   
                    }                                                            
                    
                    $response = successDelete();
                    
                break;
                 case 'schedule':
			        $termid   = (Request::get('termid'));	
					$progid   = Request::get('progid');	
					$regid   = (Request::get('regid'));	                    
					$scheds   = DB::select("SELECT cs.ScheduleID
							     ,s.SectionID ,s.SectionName
                                 ,dbo.fn_SubjectCode(cs.SubjectID) As Code
                                 ,dbo.fn_SubjectTitle(cs.SubjectID) as Name
                                 ,dbo.fn_SubjectLectureHours(SubjectID) As Credit
							     ,(CASE WHEN cs.Limit=0 THEN s.Limit ELSE cs.Limit END) as Limit
							     ,(SELECT COUNT(*) FROM ES_RegistrationDetails WHERE ScheduleID=cs.ScheduleID) as Registered
							     ,(SELECT COUNT(*) FROM ES_Advising_Details WHERE ScheduleID=cs.ScheduleID) as Advised
							     ,Time1_Start ,Time1_End ,Days1 ,Sched_1 as Sched1 ,Room1_ID ,dbo.fn_RoomName(Room1_ID) AS Room1, dbo.fn_EmployeeName(FacultyID) As Faculty1
							     ,Time2_Start ,Time2_End ,Days2 ,Sched_2 as Sched2
							     ,Time3_Start ,Time3_End ,Days3 ,Sched_3 as Sched3
							     ,Time4_Start ,Time4_End ,Days4 ,Sched_4 as Sched4                                                                  
							FROM ES_ClassSchedules as cs
					  INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
						   WHERE cs.TermID=".$termid." AND s.ProgramID=".$progid." AND ScheduleID NOT IN (SELECT ScheduleID FROM ES_RegistrationDetails WHERE RegID = '{$regid}')");
                    
                    $vw  = view($this->views.'offered', ['data' => $scheds])->render();
         
				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'content' => $vw
					                           ,'error'   => false
											   ,'message' => ''
				   ]);
                    break;	
                case 'get-grades': 
                    
                    $idno   = decode(Request::get('idno'));
                
               	    $grades = DB::table('ES_CurriculumDetails as cd')
                        ->leftJoin("ES_Students as x",'x.curriculumid','=','cd.curriculumid')
                        ->leftJoin('ES_Subjects as s','s.SubjectID','=','cd.SubjectID' )
                        ->selectRaw("cd.IndexID, cd.CurriculumID, cd.SubjectID, s.SubjectCode, s.SubjectTitle, s.LectHrs As Credit, s.IsPassFail , YearTermID, dbo.fn_YearTerm(YearTermID) AS YearTerm ")
                        ->where('x.studentno', $idno )
                        ->whereRaw("YearTermID <> 22")
                        ->orderBy('cd.YearTermID','asc')
                        ->orderBy('cd.SortOrder','asc')
                        ->get();
                                            
                    $vw  = view($this->views.'evaluation.list', ['data' => $grades, 'idno'=> $idno ])->render();
         
				    $response = Response::json([
												'success' => true
					                           ,'data' => $vw
					                           ,'error'   => false
											   ,'message' => ''
                    ]);
                    
                break;
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }
    
    private function getRegistration($term,$idno){
        
        $ret = 0;
        $reg = DB::table('es_registrations')
                ->where('StudentNo',$idno )
                ->where('TermID', $term)
                ->get();
    
        if(count($reg)>0){
            $ret = $reg[0]->RegID;
        }
        return $ret;
    
    }
    
     private function IsLocked($ref){
        
        $ret = 0;
        $reg = DB::table('ES_AddChangeDropSubjects')
                ->where('RefID', $ref)
                ->get();
    
        foreach($reg as $r){
            if( $r->ValidationDate != ''){
                $ret = 1;
            } 
        }
        
        return $ret;
    
    }
        
    private function getSubjectRegistered($regid){
        return DB::select("SELECT s.SubjectID, s.SubjectCode, s.SubjectTitle, cs.SectionID, dbo.fn_SectionName(cs.SectionID) AS SectionName, cs.Sched_1, cs.Room1_ID, dbo.fn_RoomName(cs.Room1_ID) AS RoomName,
                        s.LectHrs As Credit, dbo.fn_EmployeeName(FacultyID) As Faculty,
                        d.RegTagID, d.ScheduleID, 0 As CurrentTxn 
                        FROM es_registrationdetails d 
                        	LEFT JOIN dbo.ES_ClassSchedules cs ON d.ScheduleID=cs.ScheduleID
                        	LEFT JOIN dbo.ES_Subjects s ON cs.SubjectID=s.SubjectID
                        WHERE RegID = {$regid}");
        
    }
    
    private function getRegSubjectWithADC($regId, $refId){
        /*
        SELECT s.SubjectID, s.SubjectCode, s.SubjectTitle, cs.SectionID, dbo.fn_SectionName(cs.SectionID) AS SectionName, cs.Sched_1, cs.Room1_ID, dbo.fn_RoomName(cs.Room1_ID) AS RoomName,
                        s.LectHrs As Credit, dbo.fn_EmployeeName(FacultyID) As Faculty,
                        d.RegTagID, d.ScheduleID, 0 As CurrentTxn 
                        FROM es_registrationdetails d 
                        	LEFT JOIN dbo.ES_ClassSchedules cs ON d.ScheduleID=cs.ScheduleID
                        	LEFT JOIN dbo.ES_Subjects s ON cs.SubjectID=s.SubjectID
                        WHERE RegID = '{$regId}' AND isnull(RefNo,0) = 0
                    UNION
                    
        */
        return DB::select("
                    SELECT  s.SubjectID ,
                            s.SubjectCode ,
                            s.SubjectTitle ,
                            cs.SectionID ,
                            dbo.fn_SectionName(cs.SectionID) AS SectionName ,
                            cs.Sched_1 ,
                            cs.Room1_ID ,
                            dbo.fn_RoomName(cs.Room1_ID) AS RoomName ,
                            s.LectHrs AS Credit ,
                            dbo.fn_EmployeeName(FacultyID) AS Faculty ,
                            a.TransType As RegTagID ,
                            a.Target_ScheduleID AS ScheduleID
                            , 1 As CurrentTxn 
                    FROM dbo.ES_AddChangeDropDetails AS a
                    	LEFT JOIN dbo.ES_ClassSchedules cs ON a.Target_ScheduleID=cs.ScheduleID
                    	LEFT JOIN dbo.ES_Subjects s ON cs.SubjectID = s.SubjectID
                    WHERE a.TransID = '{$refId}' ");
    }        
    
    #Date:20170504.1319H ARS
    public function eprint(){

        $this->pdf = new PDF;
		set_time_limit(0);
        ini_set('memory_limit', '-1');
		$this->pdf->setPrintHeader(false);
		$this->pdf->setPrintFooter(true);
		$this->font = 11;
		//$this->customFont = 'Libraries/pdf/fonts/Calibri.ttf';
        //$this->pdf->SetFont($this->pdf->addTTFfont(app_path($this->customFont)),'',$this->font);

        $this->pdf->SetMargins(50, 50, 50, true);
        $this->pdf->setPageUnit('pt');
    
        $data = array(
            'key' => decode(Request::get('key')),
        );

        $this->pdf->AddPage('P','LETTER');
        $this->pdf->setTitle('Print Preview');
        $this->pdf->writeHTML(view('reports.adc-assessment',$data)->render());

        $this->pdf->output();
		set_time_limit(30);
        ini_set('memory_limit', '128M');
    }
    
    private function initializer()
    {
        
        $this->model = new mAddrop();
        $this->permission = new Permission($this->ModuleName);
        $this->service = new services;
        $this->xpdf = new xpdf;
    }
}
