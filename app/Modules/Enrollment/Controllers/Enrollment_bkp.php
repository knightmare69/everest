<?php
namespace App\Modules\Enrollment\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Enrollment\Models\Registration as registered;
use App\Modules\Enrollment\Models\YearLevel as yrlvl;
use App\Modules\Enrollment\Services\Registration\Registration as register;
use App\Modules\Enrollment\Services\Registration\Advising as advise;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;
use DB;

class Enrollment extends Controller
{
	private $media = [ 'Title' => 'Enrollment',
        'Description'   => 'Process',
        'js'            => ['Enrollment/register',],
        'plugin_js'     => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
							'jquery-validation/js/jquery.validate.min',
							'jquery-validation/js/additional-methods.min',
							'bootstrap-wizard/jquery.bootstrap.wizard.min',
                            'SmartNotification/SmartNotification.min'
                           ],
        
		/*
		'init'         => ['SECURITY.init()'],
        */
        'plugin_css'       => ['bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification',],
		
    ];
    
    private $url =  [
        'page'  => 'enrollment/registration/',
        'form' => 'form',
    ];
    
	public $r_view = 'Enrollment.Views.registration.';
	
    public function index()
    {
		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->r_view.'index',$this->init('student')),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }
	
	public function registered()
	{
		//$this->media['js']=[];
	    $this->initializer();
		$this->permission = new Permission('enrollment-registered');
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->r_view.'index',$this->init('registered')),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
	}
	
	public function children()
	{
        $this->initializer();
		$this->permission = new Permission('enrollment-child');
		$appno = Request::get('appno');
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->r_view.'index',$this->init('children'))->with(array('appno'=>$appno)),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
	}
	
    public function custom()
    {
		$this->initializer();
		$this->media['js'] = ['Enrollment/advising'];
		if ($this->permission->has('read')) {
           return view('layout',array('content'=>view($this->r_view.'index',$this->init('custom')),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
		//die('Trial');
	}	
    
    public function actions()
    {
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event'))
            {
                case 'validate':
                
                    $reg = Request::get('reg');
                    
                    $data=array(
                        'ValidationDate' => systemDate(),
                        'ValidatingOfficerID' => getUserID()
                    );
                    
                    DB::table('es_registrations')->where('RegID',$reg)->update($data);
                
                    SystemLog('Enrollment','','Registered Page','validation of registration','StudentNo: '. Request::get('idno')  ,'' );
                    $response = Response::json(array('message'=>'enrollment successfully validated','error'=>false));
                                                    
                break;
				case 'list':
					$tbl_data = array(0=>'ES_Students',1=>'ES_Registrations',2=>'ES_Admission',3 =>'Reserve Students');
					$type     = Request::get('type');
					$yrlvl    = Request::get('level');
					$progid   = Request::get('progid');
					$term     = Request::get('term');
					$campus   = Request::get('campus');
					$search   = Request::get('search');
					$where    = (($search && array_key_exists('value',$search))?$search['value']:false);	
					$order    = Request::get('order');
					$offset   = Request::get('start');
					$limit    = Request::get('length');
					$rawData  = $this->register->get_xdetail($term,$campus,$yrlvl,$type,$where,$order,$offset,$limit);
					$count    = $this->register->get_xcount($term,$campus,$yrlvl,$type,$where);
					$display  = $this->register->generate_arr($rawData['output'],$tbl_data[$type]);
					$draw     = 0;
				    $response = Response::json(array('draw'=>0,
					                                 'recordsTotal'=>$count,
													 'recordsFiltered'=>$count,
													 'data'=>$display,
													 //'query'=>$rawData['query'],
													 'error'=>''
													));
					break;
				case 'registered':
					$yrlvl    = Request::get('level');
					$progid   = Request::get('progid');
					$term     = Request::get('term');
					$campus   = Request::get('campus');
					$where    = array('yrlvl'=>$yrlvl,'termid'=>$term,'campus'=>$campus);  							
					$search   = Request::get('search');
					if($search && array_key_exists('value',$search) && trim($search['value'])!='')
					{
					 $where['search'] = $search['value'];	
					}
					
					$order    = Request::get('order');
					$offset   = Request::get('start');
					$limit    = Request::get('length');
					$rawData  = $this->register->get_registered($where,$order,$offset,$limit);
					$display  = $this->register->generate_regarr($rawData['output'],'ES_Registrations');
					$count    = $rawData['count'];
					$draw     = 0;
				    $response = Response::json(array('draw'=>$draw,
					                                 'recordsTotal'=>$count,
													 'recordsFiltered'=>$count,
													 'data'=>$display,
													 'query'=>$rawData['query'],
													 'error'=>''
													));
					break;
				case 'listchild':
					$response = Response::json([
												'success' => true
					                           ,'content' => (string)view($this->r_view.'sub.table.children',$this->init('children'))
						                       ,'error'   => false
											   ,'message' => ''
											   ]);
					break;
				case 'seltemplate':
				    $campus   = Request::get('campus');
				    $term     = Request::get('term');
				    $studno   = Request::get('studno');
				    $yrlvl    = Request::get('yrlvl');
				    $major    = Request::get('major');
				    $scheme   = Request::get('scheme');
					$rs       = $this->register->pay_template(1,0,$campus,$term,$studno,$yrlvl,$major,$scheme,'studtemplate','form-control','');
				    $response = Response::json([
									'success' => true,
									'content' => (string)view($this->r_view.'sub.assess',array('template'=>$rs)),
								    'error'   => false,
								    'message' => '',
								]);
					break;
				case 'ledger':
				    $term    = Request::get('term');
				    $studno  = Request::get('studno');
				    $rs      = $this->register->get_studentledger($studno,$term);
					$response = Response::json([
									'success' => true,
									'content' => (string)view($this->r_view.'sub.table.ledger',array('ledger'=>$rs)),
								    'error'   => false,
								    'message' => '',
								]);
				    break;
				case 'tempinfo':
				    $campus   = Request::get('campus');
				    $term     = Request::get('term');
				    $studno   = Request::get('studno');
				    $yrlvl    = Request::get('yrlvl');
				    $major    = Request::get('major');
				    $scheme   = Request::get('scheme');
				    $tempid = Request::get('tempid');
					$info   = $this->register->pay_template(2,$tempid,$campus,$term,$studno,$yrlvl,$major,$scheme);
					if($info)
					{	
				     $response = Response::json([
									'success' => true,
									'content' => (string)view($this->r_view.'sub.table.breakdown',array('info'=>$info)),
								    'error'   => false,
								    'message' => '',
								]);
					}
					break;
				case 'reginfo':
				    $regid    = Request::get('regid');
				    $rs = $this->register->get_reginfo($regid);
					if($rs && count($rs)>0)
					{
					 $breakdown = ((@array_key_exists('breakdown',$rs))?(string)view($this->r_view.'sub.table.breakdown',array('info'=>$rs['breakdown'])):'');
					 $response = Response::json([
									'success' => true,
									'content' => (string)view($this->r_view.'sub.update',array('regdata'=>$rs,'breakdown'=>$breakdown)),
								    'error'   => false,
								    'message' => '',
								 ]);	
					}
					break;
				case 'register':
                    //if ($this->permission->has('add')) {
                        $studs = Input::get('register');
						$result = true; 
						if(is_array($studs) && count($studs)>0)
						{	
                         foreach($studs as $rs)
						 {
                          $termid = ((array_key_exists('termid',$rs))?$rs['termid']:0);
                          $termid = ((array_key_exists('term',$rs))?$rs['term']:$termid);
	                      $regid  = ((array_key_exists('regid',$rs))?$rs['regid']:'');
						  $appno  = ((array_key_exists('appno',$rs))?$rs['appno']:'');						  
						  $studno = ((array_key_exists('studno',$rs))?$rs['studno']:'');
						  $yrlvl  = ((array_key_exists('yrlvl',$rs))?$rs['yrlvl']:0);
						  $major  = ((array_key_exists('major',$rs))?$rs['major']:0);
						  $tempid = ((array_key_exists('tempid',$rs))?$rs['tempid']:0);                                                                              
						  $method = ((array_key_exists('method',$rs))?$rs['method']:0);
						  $famid  = ((array_key_exists('famid',$rs))?$rs['famid']:false);
						  $guard  = ((array_key_exists('guard',$rs))?$rs['guard']:false);
						  $amt    = ((array_key_exists('amt',$rs))?$rs['amt']:0);
                          $assess = ((array_key_exists('assess',$rs))?$rs['assess']:array());
						  
                          $status    = ((array_key_exists('status',$rs))?$rs['status']:0);
                          $foreign   = ((array_key_exists('foreign',$rs))?$rs['foreign']:0);
                          $prog      = ((array_key_exists('prog',$rs))?$rs['prog']:0);
						  
                          if(array_key_exists('guard',$rs)){parse_str($rs['guard'],$guard); }
						  
                          //$tempid = $this->register->getFeesTemplateID($termid,$status,$foreign,$prog,$major,$yrlvl);
                          
						  if(($appno!='' || $studno!='') && $yrlvl!=0)
						  {
    					   $parentinfo = (($guard && $famid)?($this->register->update_familyinfo($famid,$guard)):false);
						   $query      = $this->register->register_student($termid,$regid,$studno,$appno,$yrlvl,$major,$tempid,$method,$amt);
						  }
						  else
						   $query = false;
						
						  $result = ((!$query)?false:$result);    
						 }
						 
						 if($result)
						 {
                          $exec                 = $this->register->save_journals($termid,1,$studno,$regid,$assess);
						  $response['success']  = $result;						  
						  $response['message']  = 'Successfully Registered!';	
                          $response['result']   = $query;				
                          $response['guardian'] = $guard;						  
						 }
                         else
                          $response['message']='Failed to Register';	 							 
						}
						else
						 $response['message']='No Data To Register!';	
					   
					    $response = Response::json($response); 
                    //}
                    break;
				case 'delete':
                    //if ($this->permission->has('delete')) {
                        $result   = false;
						$regid    = Request::get('regid');
						if($regid!='')
						{	
                         $result   = $this->register->purge_registration($regid);
						}
						$response = Response::json(['success' => (($result)?true:false)
						                           ,'error'   => (($result)?false:true)
												   ,'message' => (($result)?'Successfully Deleted':'Failed to Execute!')
												   ]);
                    //}
                    break;
				case 'stats':
					 $termid = Request::get('term');	
				     $stats  = $this->register->generate_stats($termid);
				     $response = Response::json([
												'success' => true
					                           ,'content' => (string)view($this->r_view.'sub.table.stats',array('stats'=>$stats))
						                       ,'error'   => false
											   ,'message' => ''
											   ]);
				    break;
				case 'studinfo':
					 $studno = Request::get('studno');	
				     $sinfo  = $this->register->get_studinfo($studno);
				     $response = Response::json([
												'success' => (($sinfo)?true:false)
					                           ,'content' => (($sinfo)?$sinfo[0]:false)
						                       ,'error'   => false
											   ,'message' => ''
											   ]);
				    break;
				case 'studadv':
					 $termid   = Request::get('termid');	
					 $studno   = Request::get('studno');
					 $major    = Request::get('strand');	
					 $curr     = Request::get('curr');	
					 //$this->advise->curriculumid = (($curr=='')?0:$curr);
					 //$this->advise->strand       = (($major=='')?0:$major);
				     $studinfo = $this->advise->get_StudentDetails($termid,$studno);
                     
                     $this->advise->strand = $studinfo['content']['MajorDiscID'];
                     $this->advise->curriculumid = $studinfo['content']['CurriculumID'];
                     
		             $advised  = $this->advise->exec_AutoAdvise($studno);
				     $response = Response::json([
												'success' => (($studinfo)?true:false)
					                           ,'content' => (($studinfo)?$studinfo['content']:false)
					                           ,'advise'  => (string)view($this->r_view.'sub.table.advise',array('advise'=>$advised))
						                       ,'offered' => $advised
											   ,'error'   => false
											   ,'message' => ''
											   ]);
				    break;
			   case 'schedule':
			        $termid   = Request::get('termid');	
					$progid   = Request::get('progid');	
					$subjid   = Request::get('subjid');	
					$scheds   = $this->advise->get_AvailableSchedule($termid,$progid,$subjid);
				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'content' => $scheds
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                    break;			
			   case 'setschedule':
			        $termid   = Request::get('termid');	
					$studno   = Request::get('studno');
					$progid   = Request::get('progid');	
					$major    = Request::get('strand');	
					$yrlvl    = Request::get('yrlvl');	
					$curr     = Request::get('curr');	
					$subjid   = Request::get('subjid');	
					$schedid  = Request::get('sched');	
					$section  = Request::get('sect');	
					$isblock  = Request::get('block');	
					
					$scheds   = $this->advise->set_AdvisedSchedule($termid,$studno,$progid,$major,$yrlvl,$curr,$subjid,$schedid,$section,$isblock);
				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'content' => $scheds
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                    break;			
			   case 'advregister':
			        $termid   = Request::get('termid');	
					$studno   = Request::get('studno');
					$progid   = Request::get('progid');	
					$strand   = Request::get('strand');	
					$yrlvl    = Request::get('yrlvl');	
					$curr     = Request::get('curr');	
					
			        $reg = $this->advise->exec_Register($termid,$studno,$strand,$curr,$yrlvl);
			        $response = Response::json([
												'success' => (($reg)?true:false)
					                           ,'content' => $reg
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                    break;		
			   case 'search':
			        $param   = Request::get('param');
					$param   = trim($param);
					$where   = '';
                    $content =  '';
					$arr_val =  explode(' ',$param);
					foreach($arr_val as $a){
						if($where=='')
						 $where.= "(StudentNo LIKE '%".$a."%' OR LastName LIKE '%".$a."%' OR FirstName LIKE '%".$a."%')";
						else
						 $where.= "AND (StudentNo LIKE '%".$a."%' OR LastName LIKE '%".$a."%' OR FirstName LIKE '%".$a."%')";
					}
					$exec    = DB::select("SELECT TOP 100 StudentNo,LastName,FirstName,MiddleName,YearLevelName,y.YearLevelID,s.ProgID,ProgName 
					                         FROM ES_Students as s 
								       INNER JOIN ES_Programs as p ON s.ProgID=p.ProgID
								       INNER JOIN ESv2_YearLevel as y ON s.YearLevelID=y.YLID_OldValue AND s.ProgID=y.ProgID
								            WHERE ".$where);  		
					if($exec){
		               foreach($exec as $r){
						   $content.= '<tr class="tddata" data-value="'.$r->StudentNo.'">
						                  <td>'.$r->StudentNo.'</td>
						                  <td>'.trimmed($r->LastName).', '.trimmed($r->FirstName).'</td>
						                  <td>'.$r->ProgName.'</td>
						                  <td>'.$r->YearLevelName.'</td>
						               </tr>';
					   }				
					}	
					
					$response = Response::json([
												'success' => (($exec)?true:false)
					                           ,'content' => (($exec)?($content):false)
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                    break;		
                case 'assessment-reg':
                    
                    $key = (rand());
                    $index = Request::get('regid');
                    
                    foreach($index as $i){
                        $result = DB::insert("INSERT INTO ES_SelectedTemp(KeyID,Temp_Int) values('{$key}','{$i}') ",[]);    
                    }
                     $response = Response::json([
												'success' => true
					                           ,'content' => $key
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                                        
                break;	   
            }
            return $response;
        }   
        return $response;
    }

	public function print_report_old()
    {
	  $response = 'No Event Selected';
      $this->initializer();
      $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
      switch(Request::get('event'))
      {
	   case 'assessment':
	      $arrdata = Request::get('arrdata');
		  $term    = Request::get('term');
		  $campus  = Request::get('campus');
		  $regid   = Request::get('regid');
		  $studno  = Request::get('studno');
		  $yrlvl   = Request::get('yrlvl');
		  if(is_array($arrdata))
		  {
		   $result = array();
		   $count  = 0;
           foreach($arrdata as $k=>$arr)
           {
			 $regid   = $arr['regid'];
		     $studno  = $arr['studno'];
		     $yrlvl   = $arr['yrlvl'];
		     $result[$count] = $this->register->print_registration($term,$campus,$regid,$yrlvl)[0];
		     $count++;
		   }		   
		  }	  
		  else
		   $result  = $this->register->print_registration($term,$campus,$regid,$yrlvl);
	   
		  $html = (string)view($this->r_view.'report.prereg',array('xpdf'=>$this->pdf,'rptdata'=>$result));
		  $this->pdf->output($html);
		break;
	   default:
	      redirect('enrollment','refresh');
	    break;
	  }
	}
    
    private function assign_section(){
        
        $ret = '';                
        return $ret;
    }
    
    public function print_report()
    {
	  $response = 'No Event Selected';
      $this->initializer();
      $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
      switch(Request::get('event'))
      {
        case 'cor':
          
          $arrdata = Request::get('arrdata');
		  $term    = Request::get('term');
		  $campus  = Request::get('campus');
		  $regid   = Request::get('regid');
		  $studno  = Request::get('studno');
		  $yrlvl   = Request::get('yrlvl');
		  $major   = Request::get('major');
		  $progid  = Request::get('progid');
		  
          $result = DB::statement("exec sp_AutoAssignSection '{$regid}'",[]);
                    
		  //$result = DB::insert("INSERT INTO ES_RegistrationCopy(StudentNo,RegID,CopyID,CopyName) SELECT StudentNo,RegID,1,'Student Copy' FROM ES_Registrations WHERE RegID = '".$regid."' AND RegID NOT IN (SELECT RegID FROM ES_RegistrationCopy)",[]);
		  if($progid==29){
			$this->xpdf->filename='SHS\COR.rpt';
		    $this->xpdf->query="EXEC dbo.K12_rptRegAssessment '".$regid."', 'San Beda College Alabang', '', 'Alabang Hills Village, Alabang, Muntinlupa City', 'Alabang Campus', 'admin',  'STUDENT/CASHIER'";
		    $subrpt = array(
                           'subreport1' => array('file' =>'Company_Logo','query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'")
		                   ,'subreport2' => array('file'=>'dsr_subjects','query'=>"EXEC dbo.ES_rptRegAssessment_EnrolledSubjects '".$regid."'")	
						   //,'subreport3' => array('file'=>'AssessedFees','query'=>"EXEC K12_rptRegAssessment_Accounts '".$regid."',0 ")
                           ,'subreport3' => array('file'=>'AssessedFees','query'=>"EXEC ES_GetStudentAccounts '".$regid."'")
                           	
		                  ); 
		  }else{
		    $this->xpdf->filename='CORSHS.rpt';
		    $this->xpdf->query="EXEC dbo.ES_GetStudentRegistration '".$regid."'";
		    $subrpt = array(
                           'subreport1' => array('file' => 'Company_Logo','query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'"),
		                   'subreport2' => array('file'=>'dsr_subjects','query'=>"EXEC ES_GetStudentRegistration_Subjects '".$regid."'")
						  ,'subreport3' => array('file'=>'dsr_assesedfees','query'=>"EXEC ES_GetStudentAccounts '".$regid."'")	
		                  );
		  }
		  
		  $this->xpdf->SubReport=$subrpt;				   
		  $data = $this->xpdf->generate();
		  
		  header('Content-type: application/pdf');
          header('Content-Disposition: inline; filename="' . $data['filename'] . '"');
          header('Content-Transfer-Encoding: binary');
          header('Accept-Ranges: bytes');
          @readfile($data['file']);
        break;
	   case 'assessment':
	      $arrdata = Request::get('arrdata');
		  $term    = Request::get('term');
		  $campus  = Request::get('campus');
		  $regid   = Request::get('regid');
		  $studno  = Request::get('studno');
		  $yrlvl   = Request::get('yrlvl');
		  $major   = Request::get('major');
		  $progid  = Request::get('progid');
		  
          $group  = Request::get('group');
          $option = '2';
          
          if($group == '1'){
            $option = '3';  
            $rs = DB::select("SELECT * FROM dbo.ES_SelectedTemp WHERE KeyID = '{$regid}'");
            
            foreach($rs as $r){
                $result = DB::insert("INSERT INTO ES_RegistrationCopy(StudentNo,RegID,CopyID,CopyName)
                                      SELECT StudentNo,RegID,1,'Student Copy' FROM ES_Registrations WHERE RegID = '".$r->Temp_Int."' AND RegID NOT IN (SELECT RegID FROM ES_RegistrationCopy)",[]);    
            }
                                   
          }else{
            $result = DB::insert("INSERT INTO ES_RegistrationCopy(StudentNo,RegID,CopyID,CopyName) SELECT StudentNo,RegID,1,'Student Copy' FROM ES_Registrations WHERE RegID = '".$regid."' AND RegID NOT IN (SELECT RegID FROM ES_RegistrationCopy)",[]);  
          }          
		            
		  if($yrlvl!=5){
		   $this->xpdf->filename='registration_form2.rpt';
		   //$this->xpdf->query="EXEC dbo.K12_rptRegAssessment '".$regid."', 'San Beda College Alabang', '', 'Alabang Hills Village, Alabang, Muntinlupa City', 'Alabang Campus', 'admin',  'STUDENT/CASHIER'";
           $this->xpdf->query="EXEC ES_GetStudentRegistrationGSHS 'San Beda College Alabang', 'Alabang Hills Village, Alabang, Muntinlupa City', '',  '".$campus."', '".$term."', '".$progid."', '{$option}','".$regid."', 'admin', '1', '0', '0', '0'";
		   $subrpt = array(		                   
						  'subreport1' => array('file'=>'dsr_subjects','query'=>"EXEC dbo.ES_rptRegAssessment_EnrolledSubjects '".$regid."'")	
						  ,'subreport2' => array('file'=>'AssessedFees','query'=>"EXEC ES_GetScheduleOfFees_r3 ".$campus.",".$term.",".$progid.",2,".$regid)	
		                  );
		  }else{
		   $this->xpdf->filename='registration_form.rpt';
		   $this->xpdf->query="EXEC ES_GetStudentRegistrationGSHS 'San Beda College Alabang', 'Alabang Hills Village, Alabang, Muntinlupa City', '',  '".$campus."', '".$term."', '".$progid."', '{$option}','".$regid."', 'admin', '1', '0', '0', '0'";
		   $subrpt = array(
		                   'subreport1' => array('file'=>'schedules_of_fees_new','query'=>"EXEC ES_GetScheduleOfFees_r3 '".$campus."', '".$term."', '".$progid."', '{$option}','".$regid."'")
						  ,'subreport2' => array('file'=>'schedules_of_fees_old','query'=>"EXEC ES_GetScheduleOfFees_r3 '".$campus."', '".$term."', '".$progid."', '{$option}','".$regid."'")	
		                  );
		  }				  
		  $this->xpdf->SubReport=$subrpt;				   
		  $data = $this->xpdf->generate();
		  
		  header('Content-type: application/pdf');
          header('Content-Disposition: inline; filename="' . $data['filename'] . '"');
          header('Content-Transfer-Encoding: binary');
          header('Accept-Ranges: bytes');
          @readfile($data['file']);
	    break;
	   case 'assessmentx':
	      $arrdata = Request::get('arrdata');
		  $term    = Request::get('term');
		  $campus  = Request::get('campus');
		  $regid   = Request::get('regid');
		  $studno  = Request::get('studno');
		  $yrlvl   = Request::get('yrlvl');
		  $major   = Request::get('major');
		  $progid  = Request::get('progid');
		  
		  $major   = (($major=='' || $major==NULL)?0:$major);
		  
		  $result = DB::insert("INSERT INTO ES_RegistrationCopy(StudentNo,RegID,CopyID,CopyName) SELECT StudentNo,RegID,1,'Student Copy' FROM ES_Registrations WHERE RegID = '".$regid."' AND RegID NOT IN (SELECT RegID FROM ES_RegistrationCopy)",[]);
		  $this->xpdf->filename='registration_form_specific.rpt';
		  $this->xpdf->query="EXEC dbo.ES_GetStudentRegistrationGSHS 'San Beda College Alabang', 'Alabang Hills Village, Alabang, Muntinlupa City', '',  '".$campus."', '".$term."', '".$progid."', '2','".$regid."', 'admin', '1', '0', '0', '0'";
		  $subrpt = array(
		                   'subreport1' => array('file'=>'schedules_of_fees_new','query'=>"EXEC ESv2_GetK12Fees ".$campus.",".$term.",'".$studno."',".$yrlvl.",".$major.",0,0,1"),
						  );
						  
		  $this->xpdf->SubReport=$subrpt;				   
		  $data = $this->xpdf->generate();
		  
		  header('Content-type: application/pdf');
          header('Content-Disposition: inline; filename="' . $data['filename'] . '"');
          header('Content-Transfer-Encoding: binary');
          header('Accept-Ranges: bytes');
          @readfile($data['file']);
	    break;
	   default:
	      echo view(config('app.403'));
	    break;
	  }
	}	
	
	public function init($apage='student')
	{
		$this->initializer();
		$data =array(
					'data'      => 'trial',
					'yrlvl'     => $this->register->get_yearlvl_list(),
					'current'   => $this->register->get_SY(1,'TermID'),
					'currentSY' => $this->register->get_SY(1,'SYTerm'),
					'previous'  => $this->register->get_SY(2,'SYTerm'),
					'r_view'    => $this->r_view,
					'apage'     => $apage,
		            );
		
        $xyrlvl_opt = array('table'    =>'ESv2_YearLevel',
							'col_id'   =>'YearLevelID',
							'col_desc' =>'YearLevelName',
							'name'     =>'yrlvl',
							'cls'      =>'form-control input-inline',
							'condition'=>"Inactive=0",
							'order'    =>"YearLevelID ASC",
						   );		
		if($apage=='registered')
		{	
		 $xyrlvl_opt['int']      = '-1';
		 $xyrlvl_opt['init_lbl'] = 'All Year Level';
		}
		
		$data['xyrlvl'] = $this->register->dropdown($xyrlvl_opt);
		$data['campus'] = $this->register->dropdown(array('table'  =>'ES_Campus',
														  'col_id'  =>'CampusID',
														  'col_desc'=>'Acronym',
														  'name'    =>'campus',
														  'cls'     =>'form-control input-small input-inline',
														 )
												     );
        
        err_log($data['xyrlvl']);
        
		if($apage=='registered' || $apage=='student')
        {
		 $data['ayterms']  = $this->register->dropdown(array('table'  =>'ES_AYTerm',
														     'col_id'   =>'TermID',
														     'col_desc' =>array('AcademicYear','SchoolTerm'),
														     'name'     =>'ayterm',
														     'cls'      =>'form-control input-medium input-inline',
														     'condition'=>"Hidden='0'",
														     'order'    =>"AcademicYear + ' ' + SchoolTerm DESC ",
														     'selected' =>$this->register->get_SY(1,'TermID'),
														    )
												       );
		}	
		if($apage=='children')
        {
		 $data['termid']     = $this->register->get_SY(1,'TermID');
		 $data['familyinfo'] = $this->register->get_familyinfo();
		 $data['tblist']     = $this->register->generate_children();
		 $data['major_data'] = $this->register->get_acadtrack();
		 $data['major']      = $this->register->dropdown(array('table'   =>'vw_K12_AcademicTrack',
														   'col_id'  =>'MajorDiscID',
														   'col_desc'=>'MajorName',
														   'name'    =>'studmajor',
														   'cls'     =>'form-control',
														   'init'    =>'-1'
														  )
													);	  
		 $data['livingwith'] = $this->register->dropdown(array('table' =>'ESv2_Child_LivingWith',
														     'col_id'  =>'PKID',
														     'col_desc'=>'LivingWithName',
														     'name'    =>'guardwith',
														     'cls'     =>'form-control guarddata',
														     'init'    =>'-1'
														    )
												    );
		 $data['civilstatus'] = $this->register->dropdown(array('table'  =>'CivilStatus',
														       'col_id'  =>'StatusID',
														       'col_desc'=>'CivilDesc',
														       'name'    =>'guardmarital',
														       'cls'     =>'form-control guarddata',
															   'attr'    =>'required',
														       'init'    =>'-1'
														       )
												    );		
		 $data['paymethod'] = $this->register->dropdown(array('table'   =>'ESv2_PaymentMethods',
														     'col_id'  =>'MethodIDX',
														     'col_desc'=>'Name',
														     'name'    =>'paymethod',
														     'cls'     =>'form-control paymethod',
															 'attr'    =>'required',
														     'init'    =>'-1',
														     'condition'=>"Inactive=0",
														    )
												    );											
 		}
		if($apage=='custom'){
			$data['ayterms'] = $this->register->dropdown(array('table'  =>'ES_AYTerm',
														       'col_id'   =>'TermID',
														       'col_desc' =>array('AcademicYear','SchoolTerm'),
														       'name'     =>'ayterm',
														       'cls'      =>'form-control input-medium input-inline',
														       'condition'=>"Hidden='0' AND SchoolTerm<>'School Year'",
														       'order'    =>"AcademicYear + ' ' + SchoolTerm DESC ",
														       'selected' =>$this->register->get_SY(3,'TermID'),
														      )
												         );
			$data['strand'] = $this->register->dropdown(array('table'     =>'vw_ProgramMajors',
															  'col_id'    =>'MajorDiscID',
															  'col_desc'  =>'Major',
															  'name'      =>'strand',
															  'cls'       =>'form-control',
															  'attr'      =>'style="width=100%"',
															  'init'      => '-1',
															  'selected'  => '-1',
															  'limit'     => '0',
															  'condition' =>"ProgID=29",
															 )
														 );
			$data['curr']   = $this->register->dropdown(array('table'     =>'ES_Curriculums',
															  'col_id'    =>'IndexID',
															  'col_desc'  =>'CurriculumCode',
															  'name'      =>'curriculum',
															  'cls'       =>'form-control',
															  'attr'      =>'style="width=100%"',
															  'init'      => '-1',
															  'selected'  => '-1',
															  'limit'     => '0',
															  'condition' =>"ProgramID=29",
															 )
														 );
			}
	
     return $data; 
	}
	
	public function trial()
	{
	 $this->initializer();
	 $rs = DB::select('SELECT * FROM ESv2_YearLevel WHERE YearLevelID=2');
	 echo '<pre>';
	 print_r($rs);
	}

    private function initializer()
    {
	   ini_set('max_execution_time', 300);
	   $this->register   = new register;
	   $this->advise     = new advise;
	   $this->registered = new registered;
	   $this->yrlvl      = new yrlvl;	   
	   $this->xpdf       = new xpdf;
	   $this->permission = new Permission('enrollment');
    }
}