<?php

namespace App\Modules\Enrollment\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as tModel;
use App\Modules\Enrollment\Models\mAdvising;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Enrollment\Services\Registration\Advising as advise;
use App\Modules\Registrar\Services\EvaluationServices As evaluation;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Libraries\CrystalReport as xpdf;

use Permission;
use Request;
use Response;
use DB;

class Advising extends Controller
{
    protected $ModuleName = 'student-advising';

    private $media = [ 'Title' => 'Student Advising',
            'Description' => 'Welcome To Student Advising',
            'js' => ['enrollment/student-advising'],
            'css' => ['profile'],
            'init' => ['MOD.init()'],
            'plugin_js' => ['bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap','select2/select2.min',
                            'SmartNotification/SmartNotification.min' ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'SmartNotification/SmartNotification',
            ],
        ];

    private $url = ['page' => 'enrollment/advising'];

    private $views = 'Enrollment.Views.advising.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $mterm = new tModel;

            $term = decode(Request::get('t'));
            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),
                'data' => $this->getData('list',$term,$f,($v == 'undefined' ? 1 : $v)),
                'total' => $this->getData('total',$term,$f),
                'term' => $term,
                'filter'=>$f,
                'page'=> ($v == 'undefined' ? 1 : $v)
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode,$term, $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * ($page == 0 ? 0 : $page-1)));
        $rs = null;
        if($term != ''){

            $whereRaw = "TermID = {$term} ";
            $whereRaw .= " AND ProgID IN (".implode(getUserProgramAccess(),',') .")";

            if($filter == '0'){
                $whereRaw .= " AND dbo.fn_Promoted(RegID) = '0'";
            }elseif($filter == '1'){
                $whereRaw .= " AND dbo.fn_Promoted(RegID) = '1'";
            }

            $query = "SELECT *, dbo.fn_StudentName(StudentNo) As StudentName,
                            dbo.fn_StudentYearLevel_k12(StudentNo) As YearLevel,
                            dbo.fn_majorName(MajorID) As Major,
                            dbo.fn_Promoted(RegID) As Promoted,
                            IsNull((SELECT TOP 1 SA.Reason FROM ES_StudentAccountabilities SA WHERE SA.StudentNo = ES_Advising.StudentNo AND Cleared = 0), '') as Accountabilities,
                            IsNull((SELECT Top 1 TrackID FROM dbo.ES_Admission_Reservation WHERE IDNo = ES_Advising.StudentNo),0) As TrackID
                      FROM  ES_Advising WHERE " . $whereRaw . " Order By StudentName ASC
                      OFFSET     {$skip} ROWS
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('ES_Advising')
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }
        }

        return $rs;
    }

    public function advise(){
        $this->initializer();
        if ($this->permission->has('read')) {

            $model = new mStudents();
            $mterm = new tModel;

            $term = decode(Request::get('t'));
            $id = decode(Request::get('idno'));

            $balance = number_format( getOutstandingBalance('1',$id, systemDate(), $term ) , 4 , ".", ",") ;
            $reg_id = $this->getAdvisedRegistration($term,$id);

            $stud = $model->where('StudentNo', $id)->selectRaw("StudentNo, AppNo,  LastName, FirstName, ProgID, YearLevelID, dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel,  CurriculumID, dbo.fn_CurriculumCode(CurriculumID) AS CurriculumCode , dbo.fn_MajorName(MajorDiscID) As Major ")->first();

            if($reg_id != 0){
                $advised = $this->getSubjectRegistered($reg_id);
            }else{
                $advised = $this->getAdvisedSubject($term,$id,$stud->CurriculumID );
            }

            if($stud->AppNo != '' && ( $stud->YearLevelID == '1' || $stud->YearLevelID == '2' )){
                 $rs = DB::select("SELECT Top 1 ApplyTypeID FROM es_admission WHERE AppNo = '{$stud->AppNo}'");
                 foreach($rs as $r){
                     if($r->ApplyTypeID == '20'){
                        $yearlevel = '5';
                     }else{
                        $yearlevel = '6';
                     }
                 }

                 DB::statement("Update ES_Students SET YearLevelID = '{$yearlevel}' WHERE StudentNo = '{$id}'");
                 $stud = $model->where('StudentNo', $id)->selectRaw("StudentNo, AppNo,  LastName, FirstName, ProgID, YearLevelID, dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel,  CurriculumID, dbo.fn_CurriculumCode(CurriculumID) AS CurriculumCode , dbo.fn_MajorName(MajorDiscID) As Major ")->first();
            }

            $_incl = [
                'views' => $this->views,
                'term' => $term,
                'academic_year' => $mterm->AcademicTerm(),
                'idno' => $id,
                'data' => $stud,
                'advised' => $advised,
                'balance' => $balance,
                'regid' => $reg_id
            ];

            return view('layout',array('content'=>view($this->views.'advising')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {

                case 'credit':
                    $idno = decode(Request::get('idno'));
                    $subj = (Request::get('subj'));
                    $credit = (Request::get('credit'));

                    if($credit == 'true'){
                        $data = $this->service->creditSubject($idno, $subj);
                        $response = ['error' => false, 'message' => 'record successfully', 'grade'=> 'CRD', 'remarks'=>'Credited' ];
                    }else{
                        $data = $this->service->remove_creditSubject($idno, $subj);
                        $response = ['error' => false, 'message' => 'record successfully', 'grade'=> '', 'remarks'=>'' ];
                    }

                break;

                case 'register':

                    if($this->permission->has('read')){

                        $model = new mStudents();


                        $idno = decode(Request::get('idno'));
                        $term = (Request::get('term'));

                        $campus=1;
                        $regid = 0;
                        $method = 0 ;
                        $appno = '';
                        $student = $model->getStudentInfo($idno, $term);

                        $major = $student->MajorDiscID;
                        $appno = '';
                        $amt = 0;
                        $useoldlvl = 1;
                        $yrlvl = $student->YearLevelID;
                        //$yrlvl = 15;

                        $section = (Request::get('section'));
                        $book = (Request::get('option'));

                        $option = '';
                        $status = '1';
                        
                        switch($book){
                            case '0': $option = " AND t.TemplateCode like '%TEXTBOOK%'";
                            break;
                            case '1': $option = " AND t.TemplateCode like '%EBOOK%'";
                            break;
                            default: $option = "";
                            break;
                        }
                        

                        if($student->IsEldest == '1'){
                            $option .= " AND t.TemplateCode like '%POF%'";
                        }

                        if( strtolower($student->Status) == 'old'){
                            $status = '2';
                        }


                        $fee_id = $this->enrolment->getFeesTemplate($term, $student->ProgID, $major, $student->YearLevelID, $student->IsForeign, $status , $option);;

                        $rs = DB::table("ES_Registrations")->selectRaw("RegID As Idx")->where("TermID",$term)->where('StudentNo',$idno)->get();

                        if(count($rs) > 0){
                            $reg = $rs[0]->Idx;

                            DB::statement("UPDATE ES_Registrations SET TableofFeeID = '{$fee_id}', ClassSectionID ='{$section}' WHERE RegID = '{$reg}'");

                        }else{
                            //$reg = DB::select("EXEC K12_RegisterStudent @StudentNo='{$idno}' , @TermID='".$term."',@GradeLevelID='".$yrlvl."',@StrandID='".$major."',@FeesID='".$fees_id."',@PayMethodID='".$method."',@Amount='".$amt."',@AppNo='".$appno."',@UserID='".getUserID()."',@UseOldLvl=1");

                            $data = array(
                                'StudentNo' => $idno,
                                'TermID' =>  $term,
                                'CampusID' => '1',
                                'ClassSectionID' => $section ,
                                'RegDate' => systemDate(),
                                'CollegeID' => $student->CollegeID,
                                'ProgID' => $student->ProgID,
                                'MajorID' => $student->MajorDiscID,
                                'YearLevelID' => $student->YearLevelID,
                                'IsOnlineEnrolment' => 1,
                                'IsRegularStudent' => 1,
                                'TableofFeeID' => $fee_id

                            );

                            $reg = DB::table('ES_Registrations')->insertGetId($data);


                        }

                        $subj = (Request::get('subj'));
                        $unit = 0;

                        /* Clear Subjects */
                        DB::statement("DELETE ES_RegistrationDetails WHERE RegID = '{$reg}'");

                        foreach($subj as $r){
                            $unit = $unit + $this->getUnit(($r));
                            DB::statement("INSERT INTO dbo.ES_RegistrationDetails ( RegID , ScheduleID , RegTagID ) VALUES  ( {$reg} , ".($r)." , 0  )");
                        }

                        //err_log($unit);

                        $dues = DB::table("ES_TableofFee_Details AS d")
                                            ->leftJoin("dbo.ES_Accounts AS a",'d.AccountID','=','a.AcctID')
                                            ->selectRaw("d.*, a.AcctOption")
                                            ->where('TemplateID',$fee_id)
                                            ->get();

                        /* Clear Journals */
                        DB::statement("DELETE ES_Journals WHERE TransID = 1 AND TermID = {$term} AND IDType = 1 AND idno = '{$idno}' AND ReferenceNo = '{$reg}'");

                        foreach($dues as $d){
                            if($d->AcctOption != '0'){
                                $amt = round( $d->Amount * $unit );
                            }else{
                                $amt = $d->Amount;
                            }
                            $acct=$d->AccountID;

                            DB::statement("INSERT INTO dbo.ES_Journals ( ServerDate , TransDate , TermID , CampusID , TransID , ReferenceNo , AccountID ,
                            		          IDType ,
                            		          IDNo ,
                                              Debit ,
                                              [Assess Fee] ,
                                              [1st Payment]
                                              ) VALUES  ( Getdate(), Getdate(),{$term},1,1,'{$reg}',{$acct},1,'". $idno ."', ".($amt)." , {$amt},{$amt}  )");
                        }

                        $whre = array(
                            'StudentNo' => $idno,
                            'TermID' => $term
                        );

                        $data = array(
                            'TermID'  => $term,
                            'CampusID' => 1 ,
                            'CollegeID' => $student->CollegeID ,
                            'ProgID' => $student->ProgID ,
                            'StudentNo' => $idno,
                            'YearLevel' => $student->YearLevelID,
                            'MinLoad' => 3,
                            'MaxLoad' => 6,
                            'AdviserID' => getUserID() ,
                            'DateAdvised' => systemDate() ,
                            'FeesID' => $fee_id ,
                            'RegID' =>  $reg,
                            'IsOnlineAdvising'=> 1,
                            'MajorID' => $major,
                        );


                        $this->model->updateOrCreate($whre,$data);

                        DB::statement("UPDATE ES_Registrations SET TotalLecUnits = '{$unit}' WHERE RegID = '{$reg}'");

                        $response = ['error' => false, 'message' => 'record successfully', 'content'=> $reg, 'reg'=> encode($reg)];

                    }


                break;

                case 'get-students':

                    if($this->permission->has('read')){
                        $model = new mStudents();

                        $find = Request::get('find');

                        if( $find == ''){ $find = date('Y').'%'; }
                        $data = $model->studentsSearch($find,'50');

                        if(!empty($data)){
                            $vw  = view($this->views.'students', ['data' => $data])->render();
                            $response = ['error' => false, 'data' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }
                    }

                break;


                 case 'remove':

                    $list = Request::get('idno');

                    foreach($list as $r){
                        $ret = $this->model->where(['AdvisedID' => decode($r['ref']) ])->delete();
                    }

                    $response = successDelete();

                break;
                 case 'schedule':
			        $termid   = (Request::get('termid'));
					$progid   = Request::get('progid');
					$subjid   = (Request::get('subjid'));
					$scheds   = DB::select("SELECT cs.ScheduleID
							     ,s.SectionID ,s.SectionName
							     ,(CASE WHEN cs.Limit=0 THEN s.Limit ELSE cs.Limit END) as Limit
							     ,(SELECT COUNT(*) FROM ES_RegistrationDetails WHERE ScheduleID=cs.ScheduleID) as Registered
							     ,(SELECT COUNT(*) FROM ES_Advising_Details WHERE ScheduleID=cs.ScheduleID) as Advised
							     ,Time1_Start
							     ,Time1_End
							     ,Days1
							     ,Sched_1 as Sched1
							     ,Time2_Start
							     ,Time2_End
							     ,Days2
							     ,Sched_2 as Sched2
							     ,Time3_Start
							     ,Time3_End
							     ,Days3
							     ,Sched_3 as Sched3
							     ,Time4_Start
							     ,Time4_End
							     ,Days4
							     ,Sched_4 as Sched4
                                 ,Room1_ID
                                 ,dbo.fn_RoomName(Room1_ID) AS Room1
                            FROM ES_ClassSchedules as cs
                            INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
                            WHERE cs.SubjectID='".$subjid."' AND cs.TermID=".$termid." AND s.ProgramID=".$progid);

                    $vw  = view($this->views.'offered', ['data' => $scheds])->render();

				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'content' => $vw
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                    break;
                case 'schedule-by-section':

                    $termid   = (Request::get('termid'));
					$progid   = Request::get('progid');
					$section   = (Request::get('section'));
                    $scheds   = DB::select("SELECT cs.ScheduleID
							     ,s.SectionID ,s.SectionName, cs.SubjectID
							     ,(CASE WHEN cs.Limit=0 THEN s.Limit ELSE cs.Limit END) as Limit
							     ,(SELECT COUNT(*) FROM ES_RegistrationDetails WHERE ScheduleID=cs.ScheduleID) as Registered
							     ,(SELECT COUNT(*) FROM ES_Advising_Details WHERE ScheduleID=cs.ScheduleID) as Advised
							     ,Time1_Start
							     ,Time1_End
							     ,Days1
							     ,Sched_1 as Sched1
							     ,Time2_Start
							     ,Time2_End
							     ,Days2
							     ,Sched_2 as Sched2
							     ,Time3_Start
							     ,Time3_End
							     ,Days3
							     ,Sched_3 as Sched3
							     ,Time4_Start
							     ,Time4_End
							     ,Days4
							     ,Sched_4 as Sched4
                                 ,Room1_ID
                                 ,dbo.fn_RoomName(Room1_ID) AS Room1
							FROM ES_ClassSchedules as cs
					  INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
						   WHERE s.SectionID ='".$section."' AND cs.TermID=".$termid." AND s.ProgramID=".$progid);


				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'list' => $scheds
					                           ,'error'   => false
											   ,'message' => ''
											   ]);

                break;
                case 'get-grades':

                    $idno   = decode(Request::get('idno'));

               	    $grades = DB::table('ES_CurriculumDetails as cd')
                        ->leftJoin("ES_Students as x",'x.curriculumid','=','cd.curriculumid')
                        ->leftJoin('ES_Subjects as s','s.SubjectID','=','cd.SubjectID' )
                        ->selectRaw("cd.IndexID, cd.CurriculumID, cd.SubjectID, s.SubjectCode, s.SubjectTitle, s.LectHrs As Credit, s.IsPassFail , YearTermID, dbo.fn_YearTerm(YearTermID) AS YearTerm ")
                        ->where('x.studentno', $idno )
                        ->whereRaw("YearTermID <> 22")
                        ->orderBy('cd.YearTermID','asc')
                        ->orderBy('cd.SortOrder','asc')
                        ->get();


                    $vw  = view($this->views.'evaluation.list', ['data' => $grades, 'idno'=> $idno ])->render();

				    $response = Response::json(['success' => true
					                           ,'data' => $vw
					                           ,'error'   => false
											   ,'message' => ''
                    ]);

                break;

                case 'check-conflict':
                    $test_scheds = Request::get('test_scheds');
                    $sched_id = Request::get('sched');
                    $exclude_id = Request::get('exclude_sched');

                    if(!empty($test_scheds)){
                        foreach ($test_scheds as $ts) {
                            if($exclude_id != $ts){
                                $chk = DB::select('exec dbo.sp_CheckSchedConflicts ?, ?', [$sched_id, $ts]);

                                if(!empty($chk[0]->Conflict)){
                                    return ['error' => true, 'message' => 'Conflict subject. Please select another.', 'sched' => $ts];
                                }
                            }
                        }
                    }

                    $response = ['error' => false];

                    break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    private function getAdvisedRegistration($term,$idno){

        $ret = 0;
        $reg = DB::table('es_registrations')
                ->where('StudentNo',$idno )
                ->where('TermID', $term)
                ->get();

        if(count($reg)>0){
            $ret = $reg[0]->RegID;
        }
        return $ret;

    }

    private function getSubjectRegistered($regid){
        return DB::select("SELECT s.SubjectID, s.SubjectCode, s.SubjectTitle, cs.SectionID, dbo.fn_SectionName(cs.SectionID) AS SectionName, cs.Sched_1, cs.Room1_ID, dbo.fn_RoomName(cs.Room1_ID) AS RoomName
                        FROM es_registrationdetails d
                        	LEFT JOIN dbo.ES_ClassSchedules cs ON d.ScheduleID=cs.ScheduleID
                        	LEFT JOIN dbo.ES_Subjects s ON cs.SubjectID=s.SubjectID
                        WHERE RegID = {$regid}");

    }

    private function getAdvisedSubject($term, $idno, $curriculum){
        return DB::table('ES_CurriculumDetails as cd')
                        ->leftJoin('ES_Subjects as s','s.SubjectID','=','cd.SubjectID' )
                        ->selectRaw("cd.IndexID, cd.SubjectID, s.SubjectCode, s.SubjectTitle , s.LectHrs As Credit, '' AS SectionName ,'' As Sched_1, '' As RoomName, YearTermID, dbo.fn_YearTerm(YearTermID) AS YearTerm ")
                        ->where(['CurriculumID' => $curriculum ])
                        ->whereRaw("cd.SubjectID IN (Select SubjectID From ES_ClassSchedules WHERE TermID = '". $term ."') AND
                            ISNULL((SELECT TOP 1 LOWER(Final_Remarks) FROM dbo.ES_PermanentRecord_Details WHERE StudentNo = '".$idno."' AND SubjectID=cd.subjectid ORDER BY RegID desc),'') not in ('passed','credited')
                            AND YearTermID <> 22
                        ")
                        ->orderBy('cd.YearTermID','asc')
                        ->orderBy('cd.SortOrder','asc')
                        ->get();
    }

    private function getUnit($subj){

        $this->SectionID = 0;
        $rs = DB::table("ES_Subjects as s")
                ->leftJoin('es_classSchedules as cs ','cs.subjectid','=','s.subjectid')
                ->selectRaw("s.LectHrs, cs.SectionID")
                ->where('ScheduleID',$subj)
                ->get();

        if(count($rs) > 0){
            return $rs[0]->LectHrs;
            $this->SectionID = $rs[0]->SectionID;
        }else{
            return 0;
        }

    }

    public function print_report()
    {

        $idno  = decode(Request::get('idno'));
        $this->xpdf->filename='SHS\crediting_form.rpt';

        $this->xpdf->query="EXEC sp_k12_credit_form '{$idno}',0";
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="Credit Form"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
	}

    private function initializer()
    {
        $this->model = new mAdvising();
        $this->permission = new Permission($this->ModuleName);
        $this->advise     = new advise;
        $this->service = new evaluation;
        $this->enrolment = new Services;
        $this->xpdf = new xpdf;
    }
}
