<div class="row">
	<div class="col-md-12">
	   <!--
	   <p>Current{{$current}}</p>
	   -->
	   @if($apage=='student')
            @include($r_view.'sub.student')
	   @elseif($apage=='children')
            @include($r_view.'sub.children')
	   @elseif($apage=='registered')
            @include($r_view.'sub.registered')
	   @elseif($apage=='custom')
            @include($r_view.'sub.custom')
	   @endif
	</div>
</div>