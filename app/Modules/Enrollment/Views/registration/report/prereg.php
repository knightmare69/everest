<style>
.barcode,.pull-right
{
 text-align: right;
 margin:-5px;
}
.text-center
{
 text-align:center;	
}
.assessment
{
 font-size:small;
}
.info
{
  border: 2px solid black;
  border-collapse: collapse;
  padding:3px;
  background-color:rgb(230, 234, 241);
  font-size:small;
}
.payinfo
{
  border: 1px solid black;
  border-collapse: collapse;
  padding:3px;
  font-size:medium;
}
.bordered
{
 border:1px solid black;		
}
.border-bottom
{
 border-bottom:1px solid black;	
}
.border-top
{
 border-top:1px solid black;	
}
.space
{
 padding:0px;	
}
</style>
<?php
if(isset($rptdata) && $rptdata)
{
 $display=0;	
 foreach($rptdata as $rs)
 {	 
  $republic    = ((property_exists($rs,'Republic'))?($rs->Republic):'');	 
  $schoolname  = ((property_exists($rs,'InstitutionName'))?($rs->InstitutionName):'');	 
  $schooladdr  = ((property_exists($rs,'CampusAddress'))?($rs->CampusAddress):'');	 
  $schoolyear  = ((property_exists($rs,'SchoolYear'))?($rs->SchoolYear):'');	 
  $regid       = ((property_exists($rs,'RegID'))?($rs->RegID):'');	 
  $studno      = ((property_exists($rs,'StudentNo'))?($rs->StudentNo):'');	 
  $regdate     = ((property_exists($rs,'RegDate'))?($rs->RegDate):'');	 
  $fullname    = ((property_exists($rs,'StudentName'))?($rs->StudentName):'');	 
  $yrlvl       = ((property_exists($rs,'YearLevel'))?($rs->YearLevel):'');	 
  $gender      = ((property_exists($rs,'Gender'))?($rs->Gender):'');
  $birthdate   = ((property_exists($rs,'DateOfBirth'))?($rs->DateOfBirth):'');
  $age         = ((property_exists($rs,'Age'))?($rs->Age):'');
  $nation      = ((property_exists($rs,'Nationality'))?($rs->Nationality):'');
  $religion    = ((property_exists($rs,'Religion'))?($rs->Religion):'');
  
  $address     = ((property_exists($rs,'HomeAddress'))?($rs->HomeAddress):'');
  $telno       = ((property_exists($rs,'TelNo'))?($rs->TelNo):'n/a');
  $mobile      = ((property_exists($rs,'MobileNo'))?($rs->MobileNo):'n/a');
  $email       = ((property_exists($rs,'Email'))?($rs->Email):'n/a');
  
  $father      = ((property_exists($rs,'Father'))?($rs->Father):'');
  $mother      = ((property_exists($rs,'Mother'))?($rs->Mother):'');
  $guardian    = ((property_exists($rs,'Guardian'))?($rs->Guardian):'');
  $relation    = ((property_exists($rs,'Guardian_Relationship'))?($rs->Guardian_Relationship):'');
  
  $status      = ((property_exists($rs,'Status'))?($rs->Status):'');	 
  
  $noofpay     = ((property_exists($rs,'NoOfPayments'))?($rs->NoOfPayments):0);	 
  $payscheme   = ((property_exists($rs,'PaymentScheme'))?($rs->PaymentScheme):0);	 
  $tuition     = ((property_exists($rs,'TuitionFee'))?($rs->TuitionFee):0);	 
  $misc        = ((property_exists($rs,'MiscFee'))?($rs->MiscFee):0);	  
  $otheracct   = ((property_exists($rs,'OtherAccounts'))?($rs->OtherAccounts):0);	 
  $nonresident = ((property_exists($rs,'NonResidentFee'))?($rs->NonResidentFee):0);	 
  $newstudent  = ((property_exists($rs,'AdditionalFeesForNewStudent'))?($rs->AdditionalFeesForNewStudent):0);	 
  $book        = ((property_exists($rs,'Book'))?($rs->Book):0);	 
  $GALA        = ((property_exists($rs,'GALA'))?($rs->GALA):0);	 
  $HISFACO     = ((property_exists($rs,'HISFACO'))?($rs->HISFACO):0);	 
  $PTC         = ((property_exists($rs,'PTC'))?($rs->PTC):0);	 
  $publica     = ((property_exists($rs,'Publication'))?($rs->Publication):0);	 
  
  $firstpay    = ((property_exists($rs,'FirstPaymentDueDate'))?($rs->FirstPaymentDueDate):'');	 
  $secondpay   = ((property_exists($rs,'SecondPaymentDueDate'))?($rs->SecondPaymentDueDate):'');	 
  $thirdpay    = ((property_exists($rs,'ThirdPaymentDueDate'))?($rs->ThirdPaymentDueDate):'');	 
  $fourpay     = ((property_exists($rs,'FourthPaymentDueDate'))?($rs->FourthPaymentDueDate):'');	 
  $fivepay     = ((property_exists($rs,'FifthPaymentDueDate'))?($rs->FifthPaymentDueDate):'');	 
  $sixpay      = ((property_exists($rs,'SixthPaymentDueDate'))?($rs->SixthPaymentDueDate):'');	 
  $sevenpay    = ((property_exists($rs,'SeventhPaymentDueDate'))?($rs->SeventhPaymentDueDate):'');	 
  $eightpay    = ((property_exists($rs,'EighthPaymentDueDate'))?($rs->EighthPaymentDueDate):'');	 
  $ninepay     = ((property_exists($rs,'NinthPaymentDueDate'))?($rs->NinthPaymentDueDate):'');	 
  $tenpay      = ((property_exists($rs,'TenthPaymentDueDate'))?($rs->TenthPaymentDueDate):'');	 
  $barcode     = $xpdf->xserialize(array($studno, 'C128C', '', '', 50, 8, 95, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>1), 'N'));
		  
  if($regdate!='')
  {
	$tmpdate = new DateTime($regdate);
    $regdate = $tmpdate->format('m/d/Y h:i a');	
  }	  
  if($birthdate!='')
  {
	$tmpdate = new DateTime($birthdate);
    $birthdate = $tmpdate->format('m/d/Y');	
  }	  
  
  $total = floatval($tuition)+
	       floatval($misc)+
		   floatval($otheracct)+
		   floatval($newstudent)+
		   floatval($nonresident)+
		   floatval($book)+
		   floatval($GALA)+
		   floatval($HISFACO)+
		   floatval($publica)+
		   floatval($PTC);
	
  $div  = 1;
  $part = 1;
  if($payscheme == 1)
  {	
    $div  = 0.45;
    $part = 1;
  }
  else if($payscheme == 2)
  {	
    $div = 0.55;
	$part = 3; 
  }
  else if($payscheme == 2)
  {	
    $div = 0.55;
	$part = 9; 
  }
	
  $main     = ($tuition+$misc+$otheracct+$newstudent);
  $mainpart = ((($tuition+$misc+$otheracct+$newstudent)* $div)/$part);
  $oneamt   = (($payscheme==0)? $total : (($main -($mainpart * $part))+ floatval($nonresident)+ floatval($book)+ floatval($GALA)+ floatval($HISFACO)+ floatval($publica)+ floatval($PTC))); 
  $twoamt   = $mainpart;
	
?>
<table>
 <tbody>
  <tr><td colspan="3" class="text-center"><?php echo $schoolname;?></td></tr>
  <tr><td colspan="3" class="text-center"><?php echo $schooladdr;?><br/></td></tr>
  <tr>
  <td width="20%"></td>
  <td width="60%" class="text-center"><h4>INTEGRATED BASIC EDUCATION DEPARTMENT</h4></td>
  <td width="20%" rowspan="2">REG NO.<?php echo((isset($regid))?str_pad($regid,9,0,STR_PAD_LEFT):'000000000');?><br/><?php echo((isset($regdate))?$regdate:'');?></td>
  </tr>
  <tr>
  <td width="20%"></td>
  <td width="60%" class="text-center"><h3>REGISTRATION FORM</h3></td>
  </tr>
  <tr>
   <td width="35%"></td>
   <td width="30%" class="text-center">Academic Year:<?php echo $schoolyear;?></td>
   <td width="35%" class="barcode"><tcpdf method="write1DBarcode" params="<?php echo $barcode;?>"/></td>
  </tr>
 </tbody>	
</table>
<table class="info" width="538pt">
 <tbody>
  <tr>
   <td>StudentNo:</td><td><?php echo $studno;?></td>
   <td></td>
   <td></td>
   <td>Status:<?php echo $status;?></td>
   <td></td>
   <td></td>
   <td colspan="2">Level:<?php echo $yrlvl;?></td>
  </tr>
  <tr>
   <td>Fullname</td><td colspan="8"><strong><?php echo ((isset($fullname))?$fullname:'');?></strong></td>
  </tr>
  <tr>
   <td>Gender:</td><td><?php echo $gender;?></td>
   <td colspan="2">DateofBirth: <?php echo $birthdate;?></td>
   <td>Age:<?php echo number_format($age,2,'.',',');?></td>
   <td colspan="2">Citizenship: <?php echo $nation;?></td>
   <td colspan="2">Religion: <?php echo $religion;?></td>
  </tr>
  </tbody>
</table>
<table class="info" width="538pt">
 <tbody>  
  <tr>
   <td width="10%">Address:</td>
   <td width="90%" colspan="6"><?php echo $address;?></td>
  </tr>
  <tr>
   <td colspan="2">TelNo: <?php echo $telno;?></td>
   <td colspan="2">CellNo: <?php echo $mobile;?></td>
   <td colspan="2">Email: <?php echo $email;?></td>
   <td></td>
  </tr>
  </tbody>
</table>
<table class="info" width="538pt">
 <tbody>  
  <tr>
   <td>Father's Name:</td>
   <td colspan="2"><?php echo $father;?></td>
   <td colspan="3">No Of Siblings:1(Elder)</td>
  </tr>
  <tr>
   <td>Mother's Name:</td>
   <td colspan="5"><?php echo $mother;?></td>
  </tr>
  <tr>
   <td>Guardian's Name:</td>
   <td colspan="2"><?php echo $guardian;?></td>
   <td colspan="3">Relationship:<?php echo $relation;?></td>
  </tr>
 </tbody>	
</table>
<table>
<tr>
<td width="75%">
<h3>SCHEDULE OF FEES:</h3>
<table class="assessment">
 <tbody>
  <tr><td width="25%">I.Tuition Fee</td><td class="pull-right" width="20%"><?php echo number_format($tuition,2,'.',',');?></td></tr>
  <tr><td width="25%">II.Miscellaneous</td><td class="pull-right" width="20%"><?php echo number_format($misc,2,'.',',');?></td></tr>
  <tr><td width="25%">III.Other Accounts</td><td class="pull-right" width="20%"><?php echo number_format($otheracct,2,'.',',');?></td></tr>
  <?php 
  if(strtoupper($status)=='NEW'){
  ?>
  <tr><td width="25%">IV.For New Students</td><td class="pull-right" width="20%"><?php echo number_format($newstudent,2,'.',',');?></td></tr>
  <?php 
  }
  ?>
  <tr><td width="25%"> TOTAL</td><td class="pull-right border-top" width="20%"><strong><?php echo number_format($main,2,'.',',');?></strong></td></tr>
  <tr><td colspan="2"></td></tr>
 </tbody>
</table>
<table class="assessment" <?php echo(($book<=0 && $publica<=0 && $PTC<=0 && $HISFACO<=0 && $GALA<=0)?'style="display:none;"':'');?>>
 <tbody>
  <tr><td colspan="3"><i>Additional fees if applicable(to be collected per family)</i></td></tr>
  <?php
  if($book>0){
  ?>
  <tr><td width="5%"></td><td width="20%">Book</td><td class="pull-right" width="20%"><?php echo number_format($book,2,'.',',');?></td></tr>
  <?php
  }
  if($publica>0){
  ?>
  <tr><td width="5%"></td><td width="20%">Publication</td><td class="pull-right" width="20%"><?php echo number_format($publica,2,'.',',');?></td></tr>
  <?php
  }
  if($PTC>0){
  ?>
  <tr><td width="5%"></td><td width="20%">PTC</td><td class="pull-right" width="20%"><?php echo number_format($PTC,2,'.',',');?></td></tr>
  <?php 
  }
  if($HISFACO>0){	  
  ?>
  <tr><td width="5%"></td><td width="20%">HISFACO</td><td class="pull-right" width="20%"><?php echo number_format($HISFACO,2,'.',',');?></td></tr>
  <?php
  }
  if($GALA>0){
  ?>
  <tr><td width="5%"></td><td width="20%">GALA</td><td class="pull-right" width="20%"><?php echo number_format($GALA,2,'.',',');?></td></tr>
  <?php
  }
  ?>
  <tr><td colspan="3"></td></tr>
 </tbody>
</table>
<table class="assessment" style="display:none;">
 <tbody>
  <tr><td colspan="3"><i>Extra Payment</i></td></tr>
  <tr><td width="5%"></td><td width="20%">Bank Charge</td><td class="pull-right" width="20%">0.00</td></tr>
  <tr><td colspan="3"></td></tr>
  <tr><td colspan="3"></td></tr>
 </tbody>
</table>
<table class="assessment">
 <tbody>
  <tr><td width="25%"><strong>UPON ENROLLMENT</strong></td><td width="20%"></td><td class="pull-right" width="20%"><?php echo number_format($oneamt,2,'.',',');?></td></tr>
  <?php
  if($noofpay>0)
  {
  ?>
  <tr><td width="25%"><strong>2ND PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $secondpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <?php
  }
  if($noofpay>2)
  {	  
  ?>
  <tr><td width="25%"><strong>3RD PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $thirdpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <tr><td width="25%"><strong>4TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $fourpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <?php
  }
  if($noofpay>4)
  {	  
  ?>
  <tr><td width="25%"><strong>5TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $fivepay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <tr><td width="25%"><strong>6TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $sixpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <tr><td width="25%"><strong>7TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $sevenpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <tr><td width="25%"><strong>8TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $eightpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <tr><td width="25%"><strong>9TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $ninepay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <tr><td width="25%"><strong>10TH PAYMENT</strong></td><td class="text-center" width="20%"><?php echo $tenpay;?></td><td class="pull-right" width="20%"><?php echo number_format($twoamt,2,'.',',');?></td></tr>
  <?php
  }
  ?>
 </tbody>
</table>
</td>
<td style="width:25%">
 <br/>
 <br/>
 <table class="payinfo">
  <tbody>
   <tr><td class="text-center" colspan="3">Payment Information</td></tr>
   <tr><td width="5%"></td><td width="10%" class="bordered"></td><td width="85%">Cash</td></tr>
   <tr><td colspan="3" class="space"></td></tr>
   <tr><td width="5%"></td><td width="10%" class="bordered"></td><td>Check</td></tr>
   <tr><td colspan="3" class="space"></td></tr>
   <tr><td width="5%"></td><td width="10%" class="bordered"></td><td>Credit Card</td></tr>
   <tr><td colspan="3" class="space"></td></tr>
   <tr><td colspan="3">Date: _____________________</td></tr>
   <tr><td colspan="3">OR #: _____________________</td></tr>
   <tr><td colspan="3">Cashier: __________________</td></tr>
   <tr><td colspan="3" class="space"></td></tr>
   <tr><td width="5%"></td><td width="10%" class="bordered"></td><td width="85%">Officially Enrolled</td></tr>
   <tr><td colspan="3" class="space"></td></tr>
   <tr><td width="5%"></td><td width="10%" class="bordered"></td><td rowspan="2">Enrollment Subject To Check Clearing</td></tr>
   <tr><td width="5%"></td><td width="10%"></td></tr>
  </tbody>
 </table>
</td>
</tr>
</table>
<br/>
<br/>
<table>
 <tbody>
 <tr><td width="5%">Note:</td><td colspan="2" width="90%">I/we have completely read and fully understood the stipulations indicated in the Finance Policy Agreement and hereby promise to abide by the terms and conditions stated therein.<br/></td></tr>
 <tr><td></td><td width="60%"></td><td class="border-bottom" width="35%"></td></tr>
 <tr><td></td><td width="60%"></td><td class="text-center">Signatrure Over Printed Name</td></tr>
 <tr><td></td><td width="60%"></td><td class="text-center">(Parent/Guardian)</td></tr>
 </tbody>
</table>
<?php
$display++;
echo ((count($rptdata)>1 && count($rptdata)>$display)?'<tcpdf method="AddPage" params=""/>':'');
 }
}
?>