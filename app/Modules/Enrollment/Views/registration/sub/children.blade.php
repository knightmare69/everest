	<div class="portlet blue box dvchild">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i> List of Student/s <?php echo ((isset($currentSY))?(' - '.$currentSY):'');?>
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body" id="list" data-term="{{$termid}}" data-sy="{{$currentSY}}">
		<div class="row" style="margin-bottom:2px;">
		 <div class="col-sm-12 hidden">
		  Color Legend: 
		  <small>
		   <span class="badge badge-default badge-roundless" style="background-color:#FFFFFF;border:1px solid black;margin-left:10px;"><i class="fa fa-fw"></i></span> Allowed To Enroll
		   <span class="badge badge-danger badge-roundless" style="background-color:#F2DEDE;margin-left:10px;"><i class="fa fa-fw"></i></span> With Balance/Accountability
		   <span class="badge badge-info badge-roundless" style="background-color:#D9EDF7;margin-left:10px;"><i class="fa fa-fw"></i></span> Registered
		   <span class="badge badge-success badge-roundless" style="background-color:#DFF0D8;margin-left:10px;"><i class="fa fa-fw"></i></span> Validated
		  </small>
		 </div>
		</div>
		<div class="row">
			<div class="col-sm-12">
			<?php
			  $isupdated = isFamilyUpdated();
			  if($isupdated==0){
				echo '<div class="alert alert-danger"><i class="fa fa-warning"></i> You need to update your family information in order to proceed. Kindly click <a href="'.url('guardian').'">My Profile</a>.</div>';
			  }
			?>
			</div>
		</div>
		<div class="row" style="margin-bottom:1px;">
		 <div class="col-sm-12" data-trgtno="<?php echo ((isset($appno))?$appno:'');?>">
		   <div class="table-responsive">
		   @include($r_view.'sub.table.children')
		   </div>
		 </div>
		</div>
	</div>
	<div class="hidden">
	 <?php $frs = ((isset($familyinfo) && is_object($familyinfo))?$familyinfo:false);?>
	 <input type="hidden" class="guardinfo" id="xoption" value="<?php echo (($frs && property_exists($frs,'Guardian_Name'))?'old':'new');?>">
	 <input type="hidden" class="guardinfo" id="xguardname" value="<?php echo (($frs && property_exists($frs,'Guardian_Name'))?$frs->Guardian_Name:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardmarital" value="<?php echo (($frs && property_exists($frs,'Guardian_ParentMaritalID'))?$frs->Guardian_ParentMaritalID:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardwith" value="<?php echo (($frs && property_exists($frs,'Guardian_LivingWith'))?$frs->Guardian_LivingWith:-1);?>">
	 <input type="hidden" class="guardinfo" id="xguardother" value="<?php echo (($frs && property_exists($frs,'Guardian_RelationshipOthers'))?$frs->Guardian_RelationshipOthers:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardaddr" value="<?php echo (($frs && property_exists($frs,'Guardian_Address'))?$frs->Guardian_Address:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardstreet" value="<?php echo (($frs && property_exists($frs,'Guardian_Street'))?$frs->Guardian_Street:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardbrgy" value="<?php echo (($frs && property_exists($frs,'Guardian_Barangay'))?$frs->Guardian_Barangay:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardcity" value="<?php echo (($frs && property_exists($frs,'Guardian_TownCity'))?$frs->Guardian_TownCity:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardprov" value="<?php echo (($frs && property_exists($frs,'Guardian_Province'))?$frs->Guardian_Province:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardzip" value="<?php echo (($frs && property_exists($frs,'Guardian_ZipCode'))?$frs->Guardian_ZipCode:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardtelno" value="<?php echo (($frs && property_exists($frs,'Guardian_TelNo'))?$frs->Guardian_TelNo:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardmobile" value="<?php echo (($frs && property_exists($frs,'Guardian_Mobile'))?$frs->Guardian_Mobile:'');?>">
	 <input type="hidden" class="guardinfo" id="xguardemail" value="<?php echo (($frs && property_exists($frs,'Guardian_Email'))?$frs->Guardian_Email:'');?>">
	</div>
	</div>
	<div class="portlet yellow box dvenroll hidden">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-file"></i> Child Enrollment <?php echo ((isset($currentSY))?(' - '.$currentSY):'');?>
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body" id="modal_register" data-term="{{$termid}}" data-sy="{{$currentSY}}">
		<div class="row" style="margin-bottom:2px;">
		  <div class="">
	      @include($r_view.'sub.modal')
		  </div>
		</div>
	</div>
	</div>
	
	<div id="modal_agreement" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
		<div class="modal-backdrop fade in"></div>
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
				<h4 class="modal-title">Terms And Conditions</h4>
				</div>
				<div class="modal-body" style="padding-top:1px !important;">
				   <div class="col-sm-12" style="overflow-y:auto;max-height:400px !important;">
					 <p>
					 SCHOOL FEES POLICIES:<br/>	 	 	 	 	 	 
						1. Once a student is admitted, it is understood that he/she is staying for the entire school year and all required fees are paid.</br> 
						2. Application Fee and Reservation fees are non-refundable. Only the Reservation fee is deductible upon enrollment.</br>
						3. Only the Basic Tuition may be paid by quarter or semester. All other fees must be paid upon enrollment.</br>
						<br/> 
						In case of failure to pay on the due date, the amount due shall be charged 3% per month. The school reserves the right to withdraw services to a student whose fees equivalent to one semester remain unpaid.  <br/>
						4. Only the annual and semestral payment options are available for company-sponsored enrollments. A letter of guarantee is also required from the company's senior executive confirming the arrangement. The letter must be in the official company letterhead with a seal. The timely payment of fees is the obligation of the parent / guardian. <br/>
						5. Students who have accounts in arrears at the end of the quarter will not be issued their report cards.  The school reserves the right to withdraw services to a student whose fees equivalent to one semester remain unpaid. Parents or guardians are expected to monitor their accounts and ensure these are up-to-date.  <br/>
						6. Payments can be made through credit card, wire transfer, direct deposit to the school�s bank accounts, or through check.  Checks must be paid out to Everest Academy, Inc.  Cash payments will not be accepted.<br/>
						<br/>
						REFUNDS<br/>
						7. When a child withdraws before the school year starts, the amount of the refund is based on total amount paid net of reservation fee and applicable bank charges. <br/>
						8. Basic tuition fees will not be pro-rated for part of a quarter regardless of the number of days attended.<br/>
						9. The capital development fee, miscellaneous and matriculation fees are not refundable and will not be pro-rated.<br/>
						10. The refund will be made through a check that will be released within thirty (30) calendar days after the completion of the student's clearance.   <br/>
						For inquiries, please contact the Business Office at accounting@everestmanila.edu.ph.  <br/>
					 </p>
				   </div>
				   <div class="modal-footer">
						<button class="btn btn-sm btn-success btn-aggree" class="close" data-dismiss="modal" aria-hidden="true">I Agree</button>
				   </div>
				</div>
			</div>
        </div>
    </div>		