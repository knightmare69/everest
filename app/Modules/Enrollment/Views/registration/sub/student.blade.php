<div class="portlet bordered light ">
	<div class="portlet-title">
		<div class="caption caption-md font-blue-madison">
			<i class="fa fa-user font-blue-madison"></i> List of Students
		</div>
		<div class="tools">
		    <button class="btn btn-info btn-sm btn-list"><i class="fa fa-list"></i> List of Registered</button>			
            <button class="btn btn-success btn-sm btn-register"><i class="fa fa-plus-circle"></i> Register Selected</button>
			<button class="btn btn-warning btn-sm btn-stats"><i class="fa fa-bar-chart-o"></i> Stats.</button>
			<!-- <button class="btn btn-default btn-xs"><i class="fa fa-gear"></i> Config</button> -->
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">
            <div class="portlet-input input-inline input-small">	
                <label class="control-label"> Campus : </label><br/>								
                <?php echo ((isset($campus))?$campus:'');?>			
            </div>
            <div class="portlet-input input-inline input-medium">	
                <label class="control-label"> AY Term : </label><br/>									
                <?php echo ((isset($ayterms))?$ayterms:'');?>			
            </div>
            <div class="portlet-input input-inline">	
                <label class="control-label"> Level : </label><br/>								
                <?= $xyrlvl ?>			
            </div>
            <div class="portlet-input input-inline">	
                <label class="control-label"> Option : </label><br/>								
                <select class="form-control input-medium input-inline" id="type" name="type">    				
    				<option value="1" selected="" >Officially Enrolled Previous SY</option>
    				<option value="2">Admitted Applicant Only</option>
    				<option value="3">Reserved Students</option>
                    <option value="0">All Students</option>
			     </select>			
            </div>
          
            <div class="portlet-input input-inline pull-right">	
                <label class="control-label"> Filter : </label>								
                <input type="text" class="tmp_search form-control input-small input-inline"/>
                <select class="tmp_length form-control input-xsmall input-inline">
    			   <option value="10">10</option>
    			   <option value="15">15</option>
    			   <option value="20">20</option>
    			   <option value="25">25</option>
    			   <option value="50">50</option>
    			   <option value="-1">All</option>
                  </select>              			
            </div>                       
        </div>        	
	    @include($r_view.'sub.table.student')
    </div>
</div>