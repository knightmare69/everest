	<style>
		.form-control-static{
			font-size:14px !important;
		}
		.bg-inverse{
			background-color:white !important;
		}
	</style>
	<div class="form">
		<form action="#" class="form-horizontal" id="registration_form" method="POST" onsubmit="return false;">
			<div class="form-wizard">
				<div class="form-body">
					<ul class="nav nav-pills nav-justified steps" style="margin-bottom:2px; padding:1px;">
						<li class="active">
							<a href="#tab1" data-toggle="tab" class="step">
							<span class="number">
							1 </span>
							<span class="desc">
							<i class="fa fa-check"></i> Registration</span>
							</a>
						</li>
						<!--
						<li>
							<a href="#tab2" data-toggle="tab" class="step">
							<span class="number">
							2 </span>
							<span class="desc">
							<i class="fa fa-check"></i> Family Info.</span>
							</a>
						</li>
						-->
						<li>
							<a href="#tab3" data-toggle="tab" class="step">
							<span class="number">
							2 </span>
							<span class="desc">
							<i class="fa fa-check"></i> Assessment</span>
							</a>
						</li>
						<li>
							<a href="#tab4" data-toggle="tab" class="step active">
							<span class="number">
							3 </span>
							<span class="desc">
							<i class="fa fa-check"></i> Verification</span>
							</a>
						</li>
					</ul>
					<div id="bar" class="progress progress-striped" role="progressbar">
						<div class="progress-bar progress-bar-success register-progress" style="width:10%;"></div>
					</div>
					<div class="tab-content">
						<div class="alert alert-danger display-none">
							<button class="close" data-dismiss="alert"></button>
							You have some form errors. Please check below.
						</div>
						<div class="alert alert-success display-none">
							<button class="close" data-dismiss="alert"></button>
							Your form validation is successful!
						</div>
						<div class="tab-pane active" id="tab1">
							<h4 class="block"><strong>Confirm student information:</strong></h4>
							<input type="hidden" id="regid" name="regid"/>
							<div class="form-group">
								<label class="control-label col-md-3">School Year: <span class="required">*</span></label>
								<div class="col-md-4">
									<label class="form-control" id="term" name="term" data-id="" required></label>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">StudentNo: <span class="required">*</span></label>
								<div class="col-md-4">
									<label class="form-control" id="studno" name="studno" required>0000000000</label>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Name: <span class="required">*</span></label>
								<div class="col-md-6">
									<label class="form-control" id="studname" name="studname" required>Dela Cruz, Juan</label>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Level to Enroll:<span class="required">*</span></label>
								<div class="col-md-4">
									<label class="form-control" id="studlvl" name="studlvl" required>Grade 1</label>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group major-group hidden">
								<label class="control-label col-md-3">Strand/Track:<span class="required">*</span></label>
								<div class="col-md-4">
								    <label class="form-control" id="studmajor" name="studmajor" data-major="0"></label>
									<?php //echo ((isset($major))?$major:'');?>
									<span class="help-block"></span>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab2">
						 <div class="row" style="padding-left:20px;margin-bottom:5px; padding-right:20px!important;">
							<div class="col-sm-12" style="padding-bottom:5px !important;border-bottom:1px solid #eee !important;">
							<label class="col-sm-8">Student Name: <span class="lbstdname"></span></label>
							<label class="col-sm-4">Level: <span class="lbstdlvl"></span></label>
							</div>
						 </div>
						 <div class="row" style="padding-left:20px; padding-right:20px!important;">
						    <h4 class="form-section" style="margin-bottom:5px;"><strong>Verify and update parent/guardian information:</strong></h4>
						    <div class="col-sm-12" style="padding-left:1px !important;padding-right:1px !important;">
								<ul class="nav nav-tabs">
								  <li class="active">
									<a href="#tab_guardian" data-toggle="tab">Family Information</a>
								  </li>
								  <li>
									<a href="#tab_father" data-toggle="tab">Father</a>
								  </li>
								  <li>
									<a href="#tab_mother" data-toggle="tab">Mother</a>
								  </li>
								  <li>
									<a href="#tab_emergency" data-toggle="tab">Emergency Contacts</a>
								  </li>
							    </ul>
							</div>
							<div class="tab-content">
							<?php
							$faminfo= array();
							$tmpfam = DB::SELECT("SELECT * FROM ESv2_Admission_FamilyBackground WHERE FamilyID='".getFamilyID()."'");
							if($tmpfam && count($tmpfam)>0){
							  $faminfo = $tmpfam[0];
							}
							?>
							<div class="tab-pane fade active in" id="tab_guardian">
							<div class="col-sm-12">
							<div class="form-group col-sm-6">
							    <input type="hidden" class="guarddata" id="famid" name="famid" value="<?php echo getFamilyID();?>"/>
								<label class="control-label col-md-5">Primary Contact Person: <span class="required">*</span></label>
								<div class="col-md-7">
									<input type="text" class="form-control guarddata" id="guardname" name="guardname" value="<?php echo getObjectValue($faminfo,'Guardian_Name');?>" required/>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group col-sm-6">
							    <label class="control-label col-md-4">Marital Status: <span class="required">*</span></label>
								<div class="col-md-8">
								    <select class="form-control guarddata" id="guardmarital" name="guardmarital" required/>
									  <option value="0">Single</option>
									  <option value="2">Married</option>
									  <option value="3">Divorced/Separated</option>
									  <option value="4">Widow/er</option>
									</select>
									<span class="help-block"></span>
								</div>
							</div>
							</div>
						    <div class="form-group hidden">
							<div class="row">
							    <div class="col-sm-6">
								<label class="control-label col-md-5">Living With: <span class="required">*</span></label>
								<div class="col-md-7">
									<?php echo ((isset($livingwith))?$livingwith:'<select class="form-control" id="guardwith" name="guardwith"></select>');?>
									<span class="help-block"></span>
								</div>
								</div>
								<div class="col-sm-5">
								<label class="control-label col-md-3" style="padding:1px;">If Other: <span class=""></span></label>
								<div class="col-md-8">
									<input type="text" class="form-control guarddata" id="guardother" name="guardother"/>
									<span class="help-block"></span>
								</div>
								</div>
							</div>
							</div>
							<h5 class="form-section" style="margin-top:10px;margin-bottom:5px;">Home Address</h5>
							<br/>
							<div class="col-sm-12">
						    <div class="form-group col-sm-6">
								<label class="control-label col-md-4">Address Line 1: <span class="required">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control guarddata" id="guardaddr" name="guardaddr" value="<?php echo getObjectValue($faminfo,'Guardian_Address');?>" required/>
									<span class="help-block"></span>
								</div>
							</div>
                            <div class="form-group col-sm-6">  							
								<label class="control-label col-md-4">Address Line 2: <span class="required">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control guarddata" id="guardstreet" name="guardstreet" value="<?php echo getObjectValue($faminfo,'Guardian_Street');?>" required/>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="control-label col-md-4">Barangay: <span class=""></span></label>
								<div class="col-md-8">
									<input type="text" class="form-control guarddata" id="guardbrgy" name="guardbrgy"  value="<?php echo getObjectValue($faminfo,'Guardian_Barangay');?>"/>
									<span class="help-block"></span>
								</div>
							</div>	
							<div class="form-group col-sm-6">
								<label class="control-label col-md-4">City: <span class="required">*</span></label>
								<div class="col-md-8">
									<select class="form-control select2 guarddata" id="guardcity" name="guardcity" required>										
									@foreach(App\Modules\Setup\Models\City::where('CountryCode','PH')->get() as $city)
										<option data-id={{ $city->CityID }} value="{{ $city->City }}">{{ $city->City }}</option>
									@endforeach	
									</select>
									<span class="help-block"></span>
								</div>
							</div>
							<!--
						    <div class="form-group col-sm-6">
								<label class="control-label col-md-4">Province: <span class="required">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control guarddata" id="guardprov" name="guardprov" value="<?php echo getObjectValue($faminfo,'Guardian_Province');?>" required/>
									<span class="help-block"></span>
								</div>
							</div>
						    <div class="form-group col-sm-6">	
								<label class="control-label col-md-4">Zip Code: <span class="required">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control guarddata" id="guardzip" name="guardzip"  value="<?php echo getObjectValue($faminfo,'Guardian_ZipCode');?>" required/>
									<span class="help-block"></span>
								</div>
							</div>
							--->
							</div>
							<h5 class="form-section" style="margin-top:10px;margin-bottom:5px;">Contact Information:</h5>
							<div class="form-group">
							<div class="col-sm-12">
						    <div class="col-sm-6">
								<label class="control-label col-md-3">Tel No: <span class=""></span></label>
								<div class="col-md-9">
									<input type="text" class="form-control guarddata" id="guardtelno" name="guardtelno"  value="<?php echo getObjectValue($faminfo,'Guardian_TelNo');?>"/>
									<span class="help-block"></span>
								</div>
							</div>
						    <div class="col-sm-6">	
								<label class="control-label col-md-3">Mobile: <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" class="form-control guarddata" id="guardmobile" name="guardmobile" value="<?php echo getObjectValue($faminfo,'Guardian_Mobile');?>" required/>
									<span class="help-block"></span>
								</div>
							</div>
							</div>
							<div class="col-sm-12">
						    <div class="col-sm-6">
								<label class="control-label col-md-3">Email: <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" class="form-control guarddata" id="guardemail" name="guardemail" value="<?php echo getObjectValue($faminfo,'Guardian_Email');?>" required/>
									<i>*This is the email where the payment links and other communication will be sent.</i>
									<span class="help-block"></span>
								</div>
							</div>
						    </div>
							</div>
							</div> <!-- guardian -->
							<div class="tab-pane fade" id="tab_father">
							    <!-- @include('Students.Views.student-profile.family-info.father-form') -->
								<h5 class="form-section">Father Information</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-12">
									<label class="control-label">Full Name: <span class="required">*</span></label>
									<input type="text" class="form-control guarddata" id="fathername" name="fathername" value="<?php echo getObjectValue($faminfo,'Father_Name');?>" required/>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Date of Birth: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="icon-calendar"></i>
										<input type="date" class="form-control datepicker guarddata" name="fatherdob" id="fatherdob"  data-date-format="yyyy-mm-dd" value="<?php echo ((getObjectValue($faminfo,'Father_BirthDate'))?date('Y-m-d',strtotime(getObjectValue($faminfo,'Father_BirthDate'))):'');?>"/>
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Occupation:</label>
									<div class="input-icon">
										<i class="icon-briefcase"></i>
										<input type="text" class="form-control guarddata" name="fatherocptn" id="fatherocptn" value="<?php echo getObjectValue($faminfo,'Father_Occupation');?>"/>
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Company:</label>
									<div class="input-icon">
										<i class="fa fa-building"></i>
										<input type="text" class="form-control guarddata" name="fathercompany" id="fathercompany" value="<?php echo getObjectValue($faminfo,'Father_Company');?>">
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								<div class="col-sm-12">
								<div class="form-group col-sm-8">
									<label class="control-label">Business Address:</label>
									<div class="input-icon">
										<i class="icon-pointer"></i>
										<input type="text" class="form-control guarddata" name="fatherbaddr" id="fatherbaddr" value="<?php echo getObjectValue($faminfo,'Father_CompanyAddress');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Business Contact No.:</label>
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<input type="text" class="form-control guarddata" name="fatherbmobile" id="fatherbmobile" value="<?php echo getObjectValue($faminfo,'Father_CompanyPhone');?>">
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								<h5 class="form-section">Contact Information</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-6">
									<label class="control-label">Email: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-envelope"></i>
										<input type="text" class="form-control guarddata" name="fatheremail" id="fatheremail" value="<?php echo getObjectValue($faminfo,'Father_Email');?>" required>
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">Contact No.: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<input type="text" class="form-control guarddata" name="fathermobile" id="fathermobile" value="<?php echo getObjectValue($faminfo,'Father_Mobile');?>" required>
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								
								<h5 class="form-section">Other Information</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-6">
									<label class="control-label">Educational Attainment:</label>
									<div class="input-icon">
										<i class="icon-graduation"></i>
										<input type="text" class="form-control guarddata" name="fathereduc" id="fathereduc" value="<?php echo getObjectValue($faminfo,'Father_EducAttainment');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">School Attended:</label>
									<div class="input-icon">
										<i class="fa fa-bank"></i>
										<input type="text" class="form-control guarddata" name="fatherschool" id="fatherschool" value="<?php echo getObjectValue($faminfo,'Father_SchoolAttended');?>">
									</div>
									<span class="help-block"></span>
								</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tab_mother">
							    <!-- @include('Students.Views.student-profile.family-info.mother-form') -->
								<h5 class="form-section">Mother Information</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-12">
									<label class="control-label">Full Name: <span class="required">*</span></label>
									<input type="text" class="form-control guarddata" id="mothername" name="mothername" value="<?php echo getObjectValue($faminfo,'Mother_Name');?>" required/>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Date of Birth: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="icon-calendar"></i>
										<input type="date" class="form-control datepicker guarddata" name="motherdob" id="motherdob" data-date-format="yyyy-mm-dd" value="<?php echo ((getObjectValue($faminfo,'Mother_BirthDate'))?date('Y-m-d',strtotime(getObjectValue($faminfo,'Mother_BirthDate'))):'');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Occupation:</label>
									<div class="input-icon">
										<i class="icon-briefcase"></i>
										<input type="text" class="form-control guarddata" name="motherocptn" id="motherocptn" value="<?php echo getObjectValue($faminfo,'Mother_Occupation');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Company:</label>
									<div class="input-icon">
										<i class="fa fa-building"></i>
										<input type="text" class="form-control guarddata" name="mothercompany" id="mothercompany" value="<?php echo getObjectValue($faminfo,'Mother_Company');?>">
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								<div class="col-sm-12">
								<div class="form-group col-sm-8">
									<label class="control-label">Business Address:</label>
									<div class="input-icon">
										<i class="icon-pointer"></i>
										<input type="text" class="form-control guarddata" name="motherbaddr" id="motherbaddr" value="<?php echo getObjectValue($faminfo,'Mother_CompanyAddress');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-4">
									<label class="control-label">Business Contact No.:</label>
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<input type="text" class="form-control guarddata" name="motherbmobile" id="motherbmobile" value="<?php echo getObjectValue($faminfo,'Mother_CompanyPhone');?>"/>
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								<h5 class="form-section">Contact Information</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-6">
									<label class="control-label">Email: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-envelope"></i>
										<input type="text" class="form-control guarddata" name="motheremail" id="motheremail" value="<?php echo getObjectValue($faminfo,'Mother_Email');?>" required/>
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">Contact No.: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<input type="text" class="form-control guarddata" name="mothermobile" id="mothermobile" value="<?php echo getObjectValue($faminfo,'Mother_Mobile');?>" required/>
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								
								<h5 class="form-section">Other Information</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-6">
									<label class="control-label">Educational Attainment:</label>
									<div class="input-icon">
										<i class="icon-graduation"></i>
										<input type="text" class="form-control guarddata" name="mothereduc" id="mothereduc" value="<?php echo getObjectValue($faminfo,'Mother_EducAttainment');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">School Attended:</label>
									<div class="input-icon">
										<i class="fa fa-bank"></i>
										<input type="text" class="form-control guarddata" name="motherschool" id="motherschool" value="<?php echo getObjectValue($faminfo,'Mother_SchoolAttended');?>">
									</div>
									<span class="help-block"></span>
								</div>
								</div>
							</div>
							<div class="tab-pane fade" id="tab_emergency">
							    <!-- @include('Students.Views.student-profile.family-info.emergency-form') -->
								<h5 class="form-section">List all persons to be contacted in the event of any emergency when none of the parents are able to be reached.  Note:  Please use only local contacts.</h5>
								<div class="col-sm-12">
								<div class="form-group col-sm-12">
									<label class="control-label">NAME OF EMERGENCY CONTACT 1: <span class="required">*</span></label>
									<input type="text" class="form-control guarddata" id="emerganame" name="emerganame" value="<?php echo getObjectValue($faminfo,'EmergencyContact_Name');?>" required/>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">Relationship to student: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-users"></i>
										<input type="text" class="form-control guarddata" name="emergarelate" id="emergarelate" value="<?php echo getObjectValue($faminfo,'EmergencyContact_Relationship');?>" required/>
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">Contact No.: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<input type="text" class="form-control guarddata" name="emergacontact" id="emergacontact" value="<?php echo getObjectValue($faminfo,'EmergencyContact_Mobile');?>" required/>
									</div>
									<span class="help-block"></span>
								</div>
								</div>
								
								<div class="col-sm-12">
								<div class="form-group col-sm-12">
									<label class="control-label">NAME OF EMERGENCY CONTACT 2: <span class="required">*</span></label>
									<input type="text" class="form-control guarddata" id="emergbname" name="emergbname" value="<?php echo getObjectValue($faminfo,'EmergencyContactB_Name');?>"/>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">Relationship to student: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-users"></i>
										<input type="text" class="form-control guarddata" name="emergbrelate" id="emergbrelate" value="<?php echo getObjectValue($faminfo,'EmergencyContactB_Relationship');?>">
									</div>
									<span class="help-block"></span>
								</div>
								<div class="form-group col-sm-6">
									<label class="control-label">Contact No.: <span class="required">*</span></label>
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<input type="text" class="form-control guarddata" name="emergbcontact" id="emergbcontact" value="<?php echo getObjectValue($faminfo,'EmergencyContactB_Mobile');?>">
									</div>
									<span class="help-block"></span>
								</div>
								</div>
							</div>
							<div class="form-group">
							  <div class="col-sm-12" style="margin-top:20px !important;">
								<label class="checkbox"><input type="checkbox" id="datapriv" name="datapriv"/> &nbsp;I/We hereby affirm that all information supplied is complete and accurate. I/We also hereby allow Everest Academy Manila to collect, use and process the above mentioned information for legitimate purposes specifically for enrolment and allow authorized personnel to process such information as contained in the Data Privacy Policy of Everest Academy Manila.</label>
							  </div>
							</div>
							</div> <!-- tab-content -->
						 </div>
						</div>
						<div class="tab-pane" id="tab3">
						 <div class="row" style="padding-left:20px;margin-bottom:5px;">
							<div class="col-sm-12" style="padding-bottom:5px !important;border-bottom:1px solid #eee !important;">
							<label class="col-sm-8">Student Name: <span class="lbstdname"></span></label>
							<label class="col-sm-4">Level: <span class="lbstdlvl"></span></label>
							</div>
						 </div>
						 <div class="row" style="padding-left:20px;">
							<h4 class="block"><strong>Select payment scheme:</strong></h4>
							<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label col-md-4">Payment Sheme: <span class="required">*</span></label>
								<div class="col-md-6">
									<select class="form-control studpaymode" id="studpaymode" name="studpaymode" required>
									 <option value="-1" selected disabled>- Select One -</option>
									 <option value="0">Full Payment</option>
									 <option value="1">Semestral Payment</option>
									 <option value="2">Quarterly Payment</option>
								<!-- <option value="3">Monthly</option> -->
									</select>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group hidden">
								<label class="control-label col-md-4">Template: <span class="required">*</span></label>
								<div class="col-md-6">
									<select class="form-control" id="studtemplate" name="studtemplate" required>
									 <option value="-1" selected disabled>- Select One -</option>
									</select>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group hidden">
								<label class="control-label col-md-4">Payment Method: <span class="required">*</span></label>
								<div class="col-md-6">
									<?php echo ((isset($paymethod))?$paymethod:"<select class='form-control' id='paymethod' name='paymethod'></select>");?>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group hidden">
								<label class="control-label col-md-4">Instructions: <span class=""></span></label>
								<div class="col-md-6">
									<p><small class="xinstruction"></small></p>
								</div>
							</div>
							</div>
							<div class="col-sm-8 col-sm-offset-2">
							<div class="form-group" style="padding-right:20px;">
							 <div class="breakdown">
							 </div>
							</div>
							</div>
						 </div>
						</div>
						<div class="tab-pane" id="tab4" style="font-size:x-small;">
						 <h4 class="block"><strong>Confirm your registration details</strong></h4>
						 <div class="row">
						 <div class="col-sm-12 col-md-6 col-lg-5"> 
						 <div class="form-group" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">School Year:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#term"></p>
								</div>
						 </div>
						 <h4 class="form-section" style="margin-bottom:2px;">Student Info.</h4>
						    <div class="form-group" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Student:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#studno"></p>
								</div>
							</div>
						    <div class="form-group" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Name:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#studname"></p>
								</div>
							</div>
						    <div class="form-group" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Level:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#studlvl"></p>
								</div>
							</div>
						    <div class="form-group major-group hidden" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Major:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#major"></p>
								</div>
							</div>
						    <h4 class="form-section" style="margin-bottom:2px;">Assessment Info.</h4>
						    <div class="form-group" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Payment Scheme:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#studpaymode"></p>
								</div>
							</div>
						    <div class="form-group hidden" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Template:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#studtemplate"></p>
								</div>
							</div>	
						    <div class="form-group hidden" style="margin-bottom:2px;">
								<label class="control-label col-sm-5 col-md-4">Payment Method:</label>
								<div class="col-sm-7 col-md-8">
								 <p class="form-control-static" data-display="#paymethod"></p>
								</div>
							</div>
                            <div class="form-group hidden" style="margin-bottom:2px;">
							 <label class="control-label col-sm-5 col-md-4">Instructions:</label>
							 <div class="col-sm-7 col-sm-8"><p class="form-control-static" data-display=".xinstruction"></p></div>
							</div>								
						  </div>
                          <div class="col-sm-12 col-md-6 col-lg-7">						  
                            <div class="form-group" style="margin-bottom:2px;">
                             <div class="col-sm-11 form-control-static" data-display=".breakdown" style="overflow:auto;max-height:400px !important;"></div>
							</div>							  
                            <div class="form-group" style="margin-bottom:2px;">
							<div class="col-sm-11" style="overflow-y:auto;max-height:400px !important;">
							 <p>
							 <strong>SCHOOL FEES POLICY:</strong>
									<p>1. Once a student is admitted, it is understood that he/she is staying for the entire school year and all required fees are paid.</p>
                                    <p>2. Application and Reservation Fees are non-refundable. However, the Reservation Fee is deductible upon enrollment.</p>
									<p>3. Basic Tuition and CDF may be paid by semester or quarter. All other fees must be paid in full upon enrollment.</p>
									<p>In case of failure to pay on due date, the amount due shall be charged a 3%-interest per month. The school
									   reserves the right to withdraw services to a student whose fees equivalent to one semester remain unpaid.</p>
									<p>4. Only the annual and semestral payment options are available for company-sponsored enrollments. A letter of guarantee is also required from the company's senior executive confirming the arrangement. The letter must be in the official company letterhead with a seal. The timely payment of fees is the obligation of the parent/guardian.</p>
									<p>5. Students who have accounts in arrears at the end of the quarter will not be issued their report cards. The school reserves the right to withdraw services to a student whose fees equivalent to one semester remain unpaid. Parents or guardians are expected to monitor their accounts and ensure these are up-to-date.</p>
									<p>6. Payments can be made through the school online system that will accept credit cards, wire transfer, and direct deposit to Paynamics Technologies Inc. (the school's accredited partner). Checks paid over the counter must be paid out to Paynamics Technologies, Inc., with the reference number indicated in the online system. Cash payments must be made directly to the accredited banks. The school cashier will no longer accept payments for tuition fees.</p><br/>
                                    <strong>REFUNDS</strong><br/><br/>
									<p>7. When a child withdraws before the school year starts, the amount of refund is based on total amount paid net of reservation fee and applicable bank charges.</p>
									<p>8. Basic tuition fee will not be pro-rated for part of a quarter regardless of the number of days attended.</p>
									<p>9. While the capital development fee, miscellaneous and matriculation fees are now paid over a period instead of upon enrollment, it is understood that these fees are non-refundable and will not be prorated in case a student leaves.</p>
									<p>10. The refund will be made through a check that will be released within thirty (30) calendar days after the completion of the student's clearance.</p>
									<p>For inquiries, please contact the Business Office at accounting@everestmanila.edu.ph.</p>
							 </p>
						    </div>
							</div>	
							<div class="form-group" style="margin-left:5px !important;">
							 <label>
							  <div class="checker">
								<span>
								<input type="checkbox" id="agreed" name="agreed"/>
								</span>
								</div>
								I agree with the <a href="javascript:void();" class="vwagreed">Terms and Conditions</a>
							  </label>
							</div>
						  </div>	
						</div>	
						</div>
                    </div>
                </div>					
			</div>	
		</form>
	</div>
	<div class="modal-footer">
	   <button class="btn button-cancel pull-left">Cancel</button>
	   <button class="btn blue button-previous"><i class="fa fa-arrow-circle-o-left"></i> Previous</button>
	   <button class="btn blue button-next">Next <i class="fa fa-arrow-circle-o-right"></i></button>
	   <button class="btn green button-submit" data-payment=0>Submit</button>
	   <button class="btn yellow button-submit hidden" data-payment=1>Submit & Pay</button>
	</div>