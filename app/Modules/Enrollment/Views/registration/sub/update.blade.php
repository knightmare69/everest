<?php 
 $r_data = ((isset($regdata))?$regdata:false);
 $scheme = (($r_data && @array_key_exists('scheme',$r_data))?$r_data['scheme']:'-1');
?>
<form action="#" class="form-horizontal" id="payment_form" method="POST" onsubmit="return false;">
   <div class="row" style="padding-left:20px;">
		<input type="hidden" id="termid" name="termid" value="<?php echo (($r_data && array_key_exists('termid',$r_data))?$r_data['termid']:'');?>" required/>
		<input type="hidden" id="campus" name="campus" value="<?php echo (($r_data && array_key_exists('campusid',$r_data))?$r_data['campusid']:'');?>" required/>
		<input type="hidden" id="regid" name="regid" value="<?php echo (($r_data && array_key_exists('regid',$r_data))?$r_data['regid']:'');?>" required/>
		<input type="hidden" id="studno" name="studno" value="<?php echo (($r_data && array_key_exists('studno',$r_data))?$r_data['studno']:'');?>" required/>
		<input type="hidden" id="studlvl" name="studlvl" value="<?php echo (($r_data && array_key_exists('yrlvl',$r_data))?$r_data['yrlvl']:'');?>" required/>
		<input type="hidden" id="studmajor" name="studmajor" value="<?php echo (($r_data && array_key_exists('major',$r_data))?$r_data['major']:0);?>" required/>
		<h5 class="block">Select the scheme of payment and the template of your fees</h5>
		<div class="col-sm-7">
		<div class="form-group">
			<label class="control-label col-md-4">Payment Sheme: <span class="required">*</span></label>
			<div class="col-md-6">
				<select class="form-control studpaymode" id="studpaymode" name="studpaymode" required>
				 <option value="-1" <?php echo (($scheme=='-1')?'selected':'');?>>- Select One -</option>
				 <option value="0" <?php echo (($scheme=='0')?'selected':'');?>>Cash</option>
				 <option value="1" <?php echo (($scheme=='1')?'selected':'');?>>Semestral</option>
				 <option value="2" <?php echo (($scheme=='2')?'selected':'');?>>Quarterly</option>
				 <option value="3" <?php echo (($scheme=='3')?'selected':'');?>>Monthly</option>
				</select>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Template: <span class="required">*</span></label>
			<div class="col-md-6">
			    <?php 
				if($r_data && array_key_exists('sel_temp',$r_data))
				 echo $r_data['sel_temp'];
                else
                {					
				?>
				<select class="form-control" id="studtemplate" name="studtemplate" required>
				  <option value="-1" selected disabled>- Select One -</option>
				</select>
				<?php
				}
				?>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4">Payment Method: <span class="required">*</span></label>
			<div class="col-md-6">
				<?php echo (($r_data && array_key_exists('sel_method',$r_data))?$r_data['sel_method']:"<select class='form-control paymethod' id='paymethod' name='paymethod'></select>");?>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group hidden">
			<label class="control-label col-md-4">Instructions: <span class=""></span></label>
			<div class="col-md-6">
				<p><small class="xinstruction"></small></p>
			</div>
		</div>
		</div>
		<div class="col-sm-5">
		<div class="form-group" style="padding-right:20px;">
		 <div class="breakdown">
		  <?php echo ((isset($breakdown) && $breakdown)?$breakdown:'');?>
		 </div>
		</div>
		</div>
	</div>
</form>