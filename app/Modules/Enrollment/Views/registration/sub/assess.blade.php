<div class="col-sm-12">
   <div class="table-responsive xinstruction">
	<table id="tblassess" class="table table-bordered table-condense">
	  <thead>
		<th>Description</th>
		<th>Fees</th>
		<th>Discount</th>
		<th>Amount Due</th>
	  </thead>
	  <tbody>
	  <?php
	   $scheme   = 0;
	   $paymentb = 0;
	   $paymentc = 0;
	   $paymentd = 0;
	   foreach($template as $t){
		  $scheme   = $t->PaymentScheme;
	      $paymentb += $t->{'2ndPayment'};
	      $paymentc += $t->{'2ndPayment'};
	      $paymentd += $t->{'2ndPayment'};
		  echo '<tr data-acctid="'.$t->AcctID.'" data-assess="'.$t->Amount.'" data-discount="0.00" data-netamt="'.$t->Amount.'" 
		            data-amta="'.$t->{'1stPayment'}.'"  data-amtb="'.$t->{'2ndPayment'}.'"  data-amtc="'.$t->{'3rdPayment'}.'" data-amtd="'.$t->{'4thPayment'}.'">
				  <td>'.$t->AcctName.'</td>
				  <td class="text-right">'.$t->Amount.'</td>
				  <td class="text-right">0.0000</td>
	                <td class="text-right tblcomponent">'.(($t->{'1stPayment'}=='0.00')?$t->Amount:$t->{'1stPayment'}).'</td>
				</tr>'; 
	   }
	  ?>
	   <tr data-type="filler"><td colspan="4"><br/></td></tr>
	   <tr data-acctid="0001" data-assess="0.00" data-discount="0.00" data-netamt="0.00">
		 <td>Car Sticker</td>
		 <td class="text-right tbtotal" data-source="sticker">0.00</td>
		 <td class="text-right">0.0000</td>
		 <td class="text-right tbtotal tblcomponent" data-source="sticker">0.00</td>
	   </tr>
	   <tr data-acctid="0002" data-assess="0.00" data-discount="0.00" data-netamt="0.00">
		 <td>Uniform</td>
		 <td class="text-right tbtotal" data-source="uniform">0.0000</td>
		 <td class="text-right">0.0000</td>
		 <td class="text-right tbtotal tblcomponent" data-source="uniform">0.0000</td>
	   </tr>
	   <tr class="hidden" data-acctid="0003"  data-assess="0.00" data-discount="0.00" data-netamt="0.00">
		 <td>Gr8 Package</td>
		 <td class="text-right tbtotal" data-source="grade8">0.0000</td>
		 <td class="text-right">0.0000</td>
		 <td class="text-right tbtotal tblcomponent" data-source="grade8">0.0000</td>
	   </tr>
	   <tr class="hidden" data-acctid="0004"  data-assess="0.00" data-discount="0.00" data-netamt="0.00">
		 <td>Summer Package</td>
		 <td class="text-right tbtotal" data-source="summer">0.0000</td>
		 <td class="text-right">0.0000</td>
		 <td class="text-right tbtotal tblcomponent" data-source="summer">0.0000</td>
	   </tr>
	  </tbody>
	  <tfoot>
	   <tr>
	     <td colspan="3" style="text-align:right">TOTAL AMOUNT DUE:</td>
	     <td class="text-right text-bold tbltotal">0.0000</td>
	   </tr>
	   <?php 
	   if($scheme>0){
		echo '<tr>
			   <td colspan="3" style="text-align:right">2nd Payment:</td>
			   <td class="text-right text-bold tbltotalb">'.number_format($paymentb,4,'','.').'</td>
			  </tr>';   
	   }
	   if($scheme>1){
		echo '<tr>
			   <td colspan="3" style="text-align:right">3rd Payment:</td>
			   <td class="text-right text-bold tbltotalc">'.number_format($paymentc,4,'','.').'</td>
			  </tr>
			  <tr>
			   <td colspan="3" style="text-align:right">4th Payment:</td>
			   <td class="text-right text-bold tbltotald">'.number_format($paymentd,4,'','.').'</td>
			  </tr>';   
	   }
	   ?>
	  </tfoot>
	</table>
   </div>
</div>
<div class="col-sm-12">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab_1_1" data-toggle="tab" aria-expanded="true">
			Car Sticker </a>
		</li>
		<li class="">
			<a href="#tab_1_2" data-toggle="tab" aria-expanded="false">
			Uniform </a>
		</li>
		<li class="hidden">
			<a href="#tab_1_4" data-toggle="tab" aria-expanded="false">
			Summer Program </a>
		</li>
		<li class="hidden">
			<a href="#tab_1_3" data-toggle="tab" aria-expanded="false">
			Grad.Package </a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in" id="tab_1_1"  data-target="sticker">
		  <div class="col-sm-12 formula-row" data-operate="multiply">
			<div class="col-sm-4 col-md-3" style="margin-bottom:2px;">
				<label class="control-label col-md-4">Price:</label>
				<div class="col-md-8">
				 <label class="form-control component numberonly">150.00</label>
				</div>
			</div>
			<div class="col-sm-4 col-md-4" style="margin-bottom:2px;">
				<label class="control-label col-md-4">Quantity:</label>
				<div class="col-md-8">
				 <input type="text" class="form-control component numberonly" value="0.00"/>
				</div>
			</div>
			<div class="col-sm-4 col-md-5" style="margin-bottom:2px;">
				<label class="control-label col-md-4">Amount Due:</label>
				<div class="col-md-8">
				 <label class="form-control tb-total formula-total numberonly">0.00</label>
				</div>
			</div>
		  </div>
		</div>
		<div class="tab-pane fade" id="tab_1_2" data-target="uniform">
		  <div class="col-sm-12">
			   <table class="table table-bordered table-condense">
			      <thead>
				    <th colspan="2">Description</th>
				    <th>Price</th>
				    <th>Quantity</th>
				    <th>Amount</th>
				  </thead>
				  <tbody>
				     <tr><td colspan="5">Boy's Regular Uniform</td></tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">White Shirt</td>
					     <td class="component text-right">315.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">Gray Pants(with garter)</td>
					     <td class="component text-right">700.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">Gray Shorts</td>
					     <td class="component text-right">600.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr><td colspan="5">Girl's Regular Uniform</td></tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">LS PeterPan White Shirt(Kinder to Gr5 only)</td>
					     <td class="component text-right">295.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">MS White Blouse(Gr6 to Gr8 only)</td>
					     <td class="component text-right">350.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">Plaid Jumper(Kinder to Gr5 only)</td>
					     <td class="component text-right">1500.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">Plaid Skirt(Gr6 to Gr8 only)</td>
					     <td class="component text-right">1300.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr><td colspan="5">PE Uniform</td></tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">PE Shirt</td>
					     <td class="component text-right">285.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">PE Pants</td>
					     <td class="component text-right">315.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">PE Shorts</td>
					     <td class="component text-right">205.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">PE Vest</td>
					     <td class="component text-right">180.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr><td colspan="5">Outerwear</td></tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">Zip-up Sweater(Boys)</td>
					     <td class="component text-right">385.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				     <tr class="formula-row" data-operate="multiply">
					     <td width="5%"></td>
					     <td width="60%">Button-down Cardigan(Girls)</td>
					     <td class="component text-right">360.00</td>
					     <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" value="0.00"></td>
					     <td class="formula-total text-right">0.00</td>
					 </tr>
				  </tbody>
			   </table>
		  </div>
	   </div>
	  <div class="tab-pane fade" id="tab_1_3">
		  <div class="col-sm-12 formula-row">
			<div class="col-sm-4 col-md-3" style="margin-bottom:2px;">
				<label class="control-label col-md-4">Price:</label>
				<div class="col-md-8">
				 <label class="form-control component numberonly">0.00</label>
				</div>
			</div>
			<div class="col-sm-4 col-md-4" style="margin-bottom:2px;">
				<label class="control-label col-md-4">Quantity:</label>
				<div class="col-md-8">
				 <input type="text" class="form-control component numberonly" value="0.00"/>
				</div>
			</div>
			<div class="col-sm-4 col-md-5" style="margin-bottom:2px;">
				<label class="control-label col-md-4">Amount Due:</label>
				<div class="col-md-8">
				 <label class="form-control formula-total numberonly">0.00</label>
				</div>
			</div>
		  </div>
	  </div>	
	</div>
</div>