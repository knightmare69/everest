<div class="modal fade bs-modal-lg" id="modal_ledger" tabindex="-1" role="dialog" aria-hidden="false">
<div class="modal-backdrop fade in"></div>
<div class="modal-dialog modal-lg">
<div class="modal-content">
 <div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
   <h4 class="modal-title">Student Ledger</h4>
 </div>
 <div class="modal-body">
  <div class="row">
   <div class="col-sm-12 dv_ledger">
    @include($r_view.'sub.table.ledger')
   </div>
  </div> 
 </div>
</div>
</div>
</div>