<div class="portlet bordered light ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i> Student Advising 
			<?php echo $campus.$ayterms;?>
		</div>
		<div class="actions">
		  <div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn btn-sm blue btnprintg" aria-expanded="true">
				<i class="fa fa-print"></i> Print <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li><a href="javascript:void(0);" class="btn-print"><i class="fa fa-print btnprinta"></i> Assessment </a></li>
					<li class="divider"></li>
					<li><a href="#"> <i class="fa fa-times"></i> Cancel </a></li>
				</ul>
		  </div>
        </div>
	</div>
	<div class="portlet-body">
	 <div class="row">
		 <form role="form" onsubmit="return false;">
		   <div class="form-body">
			 <div class="form-group">	
			   <div class="input-group col-sm-4" style="padding-left:15px;padding-right:15px;">  
				 <span class="input-group-addon"><i class="fa fa-user"></i></span>
				 <input class="form-control" id="studno" name="studno" placeholder="StudentNo" type="text">
				 <input type="hidden" id="regid" name="regid"/>
				 <span class="input-group-addon btnsearch"><i class="fa fa-search"></i></span>
			   </div>	
			 </div>
			 <div class="form-group" style="padding-bottom:10px;">
               <label class="col-sm-12"> Student Name:</label> 			 
			   <div class="col-sm-4">  
				 <label class="form-control" id="lname"></label>
			   </div>
			   <div class="col-sm-5">  
				 <label class="form-control" id="fname"></label>
			   </div>	
			   <div class="col-sm-3">  
				 <label class="form-control" id="mname"></label>
			   </div>	
			 </div>
			 <div class="form-group">
               <div class="col-sm-4">  
				 <label> Year Level:</label> 		
                 <select id="yrlvlid" name="yrlvlid" class="form-control">   				 
			       <option value="5">Grade 11</option>
			       <option value="6">Grade 12</option>
				 </select>
			   </div>
			   <div class="col-sm-4">  
				 <label> Strand:</label> 			 
			     <?php echo $strand;?>
			   </div>	
			   <div class="col-sm-4">  
				 <label> Curriculum:</label> 			 
			     <?php echo $curr;?>
			   </div>
			   <div class="tmpcurr hidden">
			     <?php echo $curr;?>
			   </div>
               <div class="col-sm-12"></div>  			   
			 </div> 
			 <div class="form-group">
               <div class="col-sm-4">  
				 <label> Min:</label> 		
                 <label class="form-control" id="min">0.00</label>
			   </div>
               <div class="col-sm-4">  
				 <label> Max:</label> 		
                 <label class="form-control" id="max">0.00</label>
			   </div>
               <div class="col-sm-4">  
				 <label> Balance:</label> 		
                 <label class="form-control">0.00</label>
			   </div>
			 </div> 
			 <div class="form-group hidden">
               <div class="col-sm-4">  
				 <label> Fees Template:</label> 		
                 <select class="form-control">
				   <option value="-1" selected disabled>-Select one-</option>
				 </select>
			   </div>
			 </div>
		   </div>   
		 </form>
	 </div>
	 <div class="row"><hr/></div>
	 <div class="row">
		<div class="portlet purple box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i> Subjects/Schedules <strong class="SY"></strong>
				</div>
				<div class="actions">
				  <button class="btn green btn-sm btnregister"><i class="fa fa-save"></i> Register</button>
				</div>
			</div>
			<div class="portlet-body">
			  <div class="table-responsive">
			     <table id="adv_details" class="table table-bordered table-condensed">
				   <thead>
				    <tr>
					 <th width="20px;"><button class="btn blue btn-sm btn-refresh"><i class="fa fa-refresh"></i> Advise</button></th>
					 <th>Code</th>
					 <th>Subject</th>
					 <th>Section</th>
					 <th>Schedule</th>
					</tr> 
				   </thead>
				   <tbody>
				     <tr>
					   <td colspan="5" class="text-center"><i class="fa fa-warning text-warning"></i> Nothing to Advise</td>
					 </tr>
				   </tbody>
				   <tfoot>
				     <tr class="text-right">
				     <td colspan="2" class="text-right"></td>
					 <td>Items:<strong class="totali">0</strong></td>
					 <td>Units:<strong class="totalu">0</strong></td>
					 </tr>
				   </tfoot>
				 </table>
			  </div>
			</div>
		</div>
	 </div>
	</div>
</div>	
<div class="modal fade bs-modal-lg" id="modal_schedule" tabindex="-1" role="dialog" aria-hidden="false">
<div class="modal-backdrop fade in"></div>
<div class="modal-dialog modal-md">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
<h4 class="modal-title">Select a schedule</h4>
</div>
<div class="modal-body">
	<div class="portlet-body form">
	  <input type="hidden" id="selsubj" name="selsubj"/>
	  <table id="sched_details" class="table table-bordered table-condensed">
	    <thead>
		  <tr>
		    <th width="10px;"></th>
		    <th>Section</th>
		    <th>Schedule</th>
		  </tr>
		</thead>
		<tbody>
		 <tr><td class="text-center" colspan="3"><i class="fa fa-warning"></i> No Data to load.</td><tr>
		</tbody>
	  </table>
	</div>
    <div class="modal-footer">
	 <label class="checkbox-inline pull-left">
	    <input id="block" name="block" type="checkbox"/> 
		Block Section? 
	 </label>
	 <button class="btn btn-primary btnsetsched">Select</button>
	 <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
</div>
</div>
</div>
</div>	
<div class="modal fade bs-modal-lg" id="modal_search" tabindex="-1" role="dialog" aria-hidden="false">
<div class="modal-backdrop fade in"></div>
<div class="modal-dialog modal-md">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
<h4 class="modal-title">Search Student</h4>
</div>
<div class="modal-body">
	<div class="portlet-body form">
	  <div class="form-group">	
	   <div class="input-group">  
		 <input class="form-control" id="search" name="search" placeholder="StudentNo,LastName,FirstName" type="text">
		 <span class="input-group-addon btnfind"><i class="fa fa-search"></i></span>
	   </div>	
	  </div>
	  <div class="table-responsive" style='max-height:360px;overflow:auto;'>
	  <table id="tblsearch" class="table table-bordered table-condensed" style="whitespace:nowrap;">
	    <thead>
		  <tr>
		    <th>StudentNo</th>
		    <th>Fullname</th>
		    <th>Program</th>
		    <th>YearLevel</th>
		  </tr>
		</thead>
		<tbody>
		 <tr><td class="text-center" colspan="4"><i class="fa fa-warning"></i> No Data to load.</td><tr>
		</tbody>
		<tfoot class="template hidden">
		 <tr><td class="text-center" colspan="4"><i class="fa fa-warning"></i> No Data to load.</td><tr>
		</tfoot>
	  </table>
	  </div>
	</div>
    <div class="modal-footer">
	 <button class="btn btn-primary btnselect">Select</button>
	 <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
</div>
</div>
</div>
</div>	