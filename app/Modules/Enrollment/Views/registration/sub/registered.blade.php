<div class="portlet bordered light ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i> List of Registered Student :  <strong class="SY"></strong>
		</div>
		<div class="actions">
		  <button class="btn red btn-sm btn-purge"><i class="fa fa-times"></i> Purge</button>
          <button class="btn btn-success btn-sm btn-validate hidden"><i class="fa fa-check"></i> Validate</button>
          <div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn btn-sm blue " aria-expanded="true">
				<i class="fa fa-print"></i> Print <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li><a href="#" class="btn-print"><i class="fa fa-print"></i> Assessment </a></li>
					<li><a href="#" class="btn-cor" ><i class="fa fa-print"></i> COR </a></li>
					<li><a href="javascript:void(0)" class="btn-sal" ><i class="fa fa-print"></i> Student Actual Load </a></li>
					<li class="divider"></li>
					<li><a href="#"> <i class="fa fa-times"></i> Cancel </a></li>
				</ul>
			</div>
            
		</div>
	</div>
	<div class="portlet-body">
	 <div class="well well-sm well-light" style="margin-bottom: 5px;">
        <div class="portlet-input input-inline input-small ">	
            <label class="control-label font-xs bold"> Campus : </label><br/>								
            <?= ((isset($campus))?$campus:'');?>			
        </div>
        <div class="portlet-input input-inline input-medium">
            <label class="control-label font-xs bold"> Term : </label><br/>
            <?php echo ((isset($ayterms))?$ayterms:'');?>
        </div>
        <div class="portlet-input input-inline">
            <label class="control-label font-xs bold"> Year level : </label><br/>
            <?php echo $xyrlvl;?>
        </div>
        <div class="portlet-input input-inline">
            <label class="control-label font-xs bold"> &nbsp; </label><br/>
            <button type="button" class="btn btn-default" onclick="get_list();"><i class="fa fa-refresh"></i> Refresh </button>
        </div>
        
        <div class="portlet-input input-inline pull-right ">	
            <label class="control-label font-xs bold"> Filter : </label>								
            <input type="text" class="tmp_search form-control input-small input-inline"/>
            <select class="tmp_length form-control input-xsmall input-inline">
    		   <option value="10">10</option>
    		   <option value="15">15</option>
    		   <option value="20">20</option>
    		   <option value="25">25</option>
    		   <option value="50">50</option>
    		   <option value="-1">All</option>
              </select>              			
        </div>              
	 </div>
     <div id="ctable">
        @include($r_view.'sub.table.registered')
     </div>     	 		   
	</div>
</div>