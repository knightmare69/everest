<table class="table table-bordered table-condensed" id="tbchildren">
	<thead>
		<tr>
			<th></th>
			<th>Student No</th>
			<th>Full Name</th>
			<th class="hidden">Current Level</th>
			<th>Incoming Level</th>
			<th class="hidden">Strand/Track</th>
			<th>Status</th>
			<th class="hidden">Accountability</th>
			<th class="hidden">Prev. Balance</th>
			<th>Current Balance</th>
			<th class="hidden">RegID</th>
			<th>Tuition Fee Table</th>
		</tr>
	</thead>
	<tbody>
	<?php
	 $useroldlvl = env('USEOLDLVL',false);
	 $arr_yrlvl  = array();
	 $arr_major  = array();
	 if(isset($yrlvl))
	 {
	  //echo '<pre>';
	  //print_r($yrlvl);
	  $dt_count=0;
	  foreach($yrlvl as $yr)
	  {
		if($yr->Inactive==0)
		{
		 $arr_yrlvl[$dt_count]=array();
		 $arr_yrlvl[$dt_count]['id']        = $yr->YearLevelID;
		 $arr_yrlvl[$dt_count]['old_id']    = $yr->Old_YearLevelID;
		 $arr_yrlvl[$dt_count]['name']      = $yr->YearLevelName;
		 $arr_yrlvl[$dt_count]['code']      = $yr->YearLevelCode;
		 $arr_yrlvl[$dt_count]['progid']    = $yr->ProgID;
		 $arr_yrlvl[$dt_count]['progclass'] = $yr->ProgClass;
		 $dt_count++;
		}
	  }
	 }	 
	 
	 if(isset($major_data))
	 {
	  foreach($major_data as $mj)
	  {
		$id                     = $mj->MajorDiscID;
		$arr_major[$id]         = array();
		$arr_major[$id]['id']   = $mj->MajorDiscID;
		$arr_major[$id]['code'] = $mj->MajorCode;
		$arr_major[$id]['name'] = $mj->MajorName;
	  }
	 }	 
	 
	 if(isset($tblist))
	 {
		foreach($tblist as $rs)
		{
		  $term       = ((property_exists($rs,'TermID'))?($rs->TermID):0);
		  $campus     = ((property_exists($rs,'CampusID'))?($rs->CampusID):1);
		  $studno     = ((property_exists($rs,'StudentNo'))?($rs->StudentNo):''); 
		  $regid      = ((property_exists($rs,'RegID'))?($rs->RegID):''); 
		  $feeid      = ((property_exists($rs,'TableOfFeeID'))?($rs->TableOfFeeID):''); 
		  $name       = ((property_exists($rs,'xFullName'))?($rs->xFullName):''); 
		  $birthdate  = ((property_exists($rs,'DateOfBirth'))?($rs->DateOfBirth):''); 
		  $yrlvlid    = ((property_exists($rs,'YearLevelID'))?($rs->YearLevelID):'-1'); 
		  $majorid    = ((property_exists($rs,'MajorID'))?($rs->MajorID):'-1'); 
		  $progid     = ((property_exists($rs,'ProgID'))?($rs->ProgID):'0'); 
		  $regyrlvlid = ((property_exists($rs,'RegYearLevelID'))?($rs->RegYearLevelID):'-1'); 
		  $status     = ((property_exists($rs,'Status'))?($rs->Status):''); 
		  $balance    = ((property_exists($rs,'Balance'))?($rs->Balance):0); 
		  $current    = ((property_exists($rs,'Current'))?($rs->Current):0); 
		  $accounts   = ((property_exists($rs,'Accounts'))?($rs->Accounts):'');
		  $familyid   = ((property_exists($rs,'FamilyID'))?($rs->FamilyID):'');
		  $billno     = ((property_exists($rs,'BillingRefNo'))?($rs->BillingRefNo):'');
		  $fees       = ((property_exists($rs,'TemplateCode'))?($rs->TemplateCode):'');
		  $ispaid     = ((property_exists($rs,'IsPaid'))?($rs->IsPaid):'');
		  $validdate  = ((property_exists($rs,'ValidationDate'))?($rs->ValidationDate):'');
		  $validator  = ((property_exists($rs,'ValidatingOfficerID'))?($rs->ValidatingOfficerID):'');
		  $appno      = ((property_exists($rs,'AppNo'))?($rs->AppNo):'');
          $progclass  = '';
		  $xprogid    = '0';
          $xprogclass = '';
		  $yrlvlname  = ''; 
		  $xyrlvlid   = '';		
		  $xold_id    = '';				  
		  $xyrlvlcode = '';				  
		  $xyrlvlname = '';
		  $btn        = '';
		  $sel_major  = '';
		  $class      = '';
		  $xtarget    = '';
		  
		  $feeid      = (($feeid==0 || $feeid=='0')?'':$feeid);
		  
		  if($feeid!=0 && $feeid!=''){
			$feedetail = DB::SELECT("SELECT TOP 1 * FROM ES_TableofFees WHERE TemplateID='".$feeid."'");
			if($feedetail && count($feedetail)>0){
				$tmpscheme = $feedetail[0]->PaymentScheme;
				switch($tmpscheme){
					case 1: $fees ='Semestral'; break;
					case 2: $fees ='Quarterly'; break;
					case 3: $fees ='Monthly'; break;
					default:
						$fees = 'Full';
					break;
				}
			}
		  }
		  
		  foreach($arr_yrlvl as $k=>$v)
		  {
			if(($useroldlvl==false && $v['id']==$yrlvlid) || ($v['old_id']==$yrlvlid && $v['progid']==$progid))
            {
			 $yrlvlname = $v['name'];
		     $progclass = $v['progclass'];
			 if($regyrlvlid=='' || $regyrlvlid=='-1')
			 {	 
			  $xtarget   = ((strtoupper($status)=='OLD')?($k+1):$k);
			 }
			}
			
			if(($useroldlvl==false && $v['id']==$regyrlvlid) || ($v['old_id']==$regyrlvlid && $v['progid']==$progid))
			{
			 $xtarget = $k;	
			}	
			
			if($k==$xtarget)
			{
			 $xyrlvlid   = $v['id'];
			 $xold_id    = $v['old_id'];
			 $xyrlvlcode = $v['code'];
			 $xyrlvlname = $v['name'];
			 $xyrlvlname = $v['name'];
			 $xprogid    = $v['progid'];
		     $xprogclass = $v['progclass'];	
			}	
		  }
		  
		  //$xyrlvlcode = 'G11';
		  //$xprogclass = 21;	
		  //$majorid    = 609;	
		  
		  if($xyrlvlcode=='G11' || $xyrlvlcode=='G12'){
			$majorid = intval($majorid);
			$sel_major = ((array_key_exists($majorid,$arr_major))?($arr_major[$majorid]['name']):''); 
		  }	  
		  
		  $tmpacct = DB::SELECT("SELECT * FROM ES_StudentAccountabilities WHERE StudentNo='".$studno."' AND Cleared=0");
		  if($tmpacct && count($tmpacct)>0){
		    foreach($tmpacct as $ra){
				$accounts = $ra->Reason;
			}
		  }
		  
		  $opay   = 0;
		  $pcheck = DB::SELECT("SELECT StatusID,StudentNo,RegID FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID WHERE pd.StudentNo='".$studno."' ORDER BY DetailID DESC,EntryID DESC"); 
		  if($pcheck && count($pcheck)>0){
			$tmpdata = $pcheck[0];
			$tmpstat = getObjectValue($tmpdata,'StatusID');
			if($tmpstat==1){
			   $opay = 1;
               $class= 'info';			   
			}elseif($tmpstat=='-2'){
			   $opay = 1;
               $class= 'warning';			   
			}else{
			   $opay = 0;
               $class= '';			   
			}
		  }
		  
		  $btn='<button class="btn btn-sm btn-default btn-prompt" data-account="'.trim($accounts).'"><i class="fa fa-lock"></i> Not Ready</button>';
		  if($regid!=''  && ($feeid=='' || $validdate=='' || $validator=='')){
		   if($opay==0)
			 $btn = '<button class="btn btn-sm btn-success btn-child-xregister"><i class="fa fa-file"></i> Register</button>';
		   else
			 $btn = '<button class="btn btn-sm btn-info btn-child-soa"><i class="fa fa-refresh fa-spin"></i> Pending Payment</button>';
		  }else if($regid!='' && $feeid!='' && $validdate!=''){
		   $class   = 'success';
		   $btn     = '<button class="btn btn-sm green btn-child-soa"><i class="fa fa-lock"></i> Enrolled</button>';	  
		   $tmpdate   = new DateTime($validdate);
           $validdate = $tmpdate->format('m/d/Y');	   
		  }else if($regid=='' && ($balance>0 || trim($accounts)!='')){
	       $class = 'danger';
		   $btn='<button class="btn btn-sm btn-default btn-prompt" data-account="'.trim($accounts).'"><i class="fa fa-lock"></i> Not Ready</button>';
		  }
		  
		  
		  /*
		  if($balance<0){$balance = 0;}
		  if($yrlvlname=='Grade 12' && $xyrlvlname=='Kinder'){
			continue;
		  }
		  */
		  $link = '<a href="'.(($fees!='')?url('enrollment/print_report?event=assessment&term='.$term.'&campus=1&group=0&regid='.$regid.'&studno='.$studno.'&yrlvl='.$xyrlvlid.'&major=0&progid='.$progid):'').'" '.(($fees!='')?'target="_blank"':'').'>'.$fees.'</a>';
		  if($validdate!=''){
		  $link = '<a href="'.(($fees!='')?url('enrollment/print_report?event=soa&term='.$term.'&campus=1&group=0&regid='.$regid.'&studno='.$studno.'&yrlvl='.$xyrlvlid.'&major=0&progid='.$progid):'').'" '.(($fees!='')?'target="_blank"':'').'>'.$fees.'</a>';
		  }
		  
		  $isupdated = isFamilyUpdated();
		  if($isupdated==0){
			$btn = '<a class="btn btn-sm btn-default btn-disabled" href="'.url('guardian').'"><i class="fa fa-lock"></i> Locked</a>';
		  }
	?>
	<tr data-id="{{$studno}}" data-appno="{{$appno}}" data-campus="{{$campus}}" data-family="{{$familyid}}" data-previous="{{$yrlvlid}}" data-target="{{$xyrlvlid}}" data-oldid="{{$xold_id}}" data-track="{{$majorid}}" data-trgtcls="{{$xprogclass}}" data-prog="{{$xprogid}}" data-regid="{{$regid}}" class="{{$class}}">
	    <td width="10px;"><?php echo $btn;?></td>
		<td width="100px;">{{$studno}}</td>
		<td width="280px;">{{$name}}</td>
		<td class="hidden">{{$yrlvlname}}</td>
		<td>{{$xyrlvlname}}</td>
		<td class="hidden"><?php echo $sel_major;?></td>
		<td>{{((strtoupper($status)=='OLD')?'RETURNING':$status)}}</td>
		<td class="hidden">{{$accounts}}</td>
		<td class="hidden text-right">{{number_format($balance,2,'.',',')}}</td>
		<td class="{{(($feeid>0)?'text-right td-current':'')}}"><?php echo (($feeid>0)?number_format($current,2,'.',','):'No Assessment');?></td>
		<td class="hidden">{{(($regid!='' && $feeid!='')?$regid:'')}}</td>
		<td><small><?php echo $link;?></small></td>
	 </tr>
	<?php			
		}
	 }	 
	?>
	</tbody>
</table>