<div class="portlet blue box">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i> Enrollment Statistics
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body">
		<div class="row" style="margin-bottom:1px;">
		 <div class="col-sm-12">
		   <table class="table table-bordered table-condensed">
		    <thead>
			 <tr>
			   <th rowspan="2"><small>Level</small></th>
			   <th colspan="3" class="text-center"><small>Registered</small></th>
			   <th colspan="3" class="text-center"><small>Enrolled</small></th>
			 </tr>
			 <tr>
			  <th class="text-center"><small>Male</small></th>
			  <th class="text-center"><small>Female</small></th>
			  <th class="text-center"><small>Total</small></th>
			  <th class="text-center"><small>Male</small></th>
			  <th class="text-center"><small>Female</small></th>
			  <th class="text-center"><small>Total</small></th>
			 </tr>
			</thead>
			<tbody>
		   <?php
            if(isset($stats))
            {
			 foreach($stats as $rs)
             {
		   ?>
		      <tr>
			   <td><small>{{$rs->YearLevelName}}</small></td>
			   <td class="text-center">{{$rs->RegisteredMale}}</td>
			   <td class="text-center">{{$rs->RegisteredFemale}}</td>
			   <td class="text-center"><strong>{{$rs->Registered}}</strong></td>
			   <td class="text-center">{{$rs->ValidatedMale}}</td>
			   <td class="text-center">{{$rs->ValidatedFemale}}</td>
			   <td class="text-center"><strong>{{$rs->Validated}}</strong></td>
			  </tr> 
           <?php		   
			 }			 
			}				
		   ?>
		   </tbody>
		   </table>
		 </div>
		</div>
	</div>
</div>