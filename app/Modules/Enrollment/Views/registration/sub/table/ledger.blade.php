<table class="table table-bordered table-condensed">
 <thead>
  <th>School Year</th>
  <th>Trans Date</th>
  <th>Trans ID</th>
  <th>Ref No.</th>
  <th>Debit</th>
  <th>Credit</th>
  <th>Balance</th>
  <th>Remarks</th>
  <th>DatePosted</th>
 </thead>
 <tbody>
  <?php
   if(isset($ledger) && count($ledger)>0)
   {
	 $total = 0;
	 foreach($ledger as $rs)
     {
	  $xayterm  = property_exists($rs,'AcademicYearTerm')? $rs->AcademicYearTerm : '';
	  $xtransdate = property_exists($rs,'TransDate')? $rs->TransDate : '';
	  $xtransid     = property_exists($rs,'TransID')? $rs->TransID : '';
	  $xrefno    = property_exists($rs,'Referenceno')? $rs->Referenceno : '';
	  $xdebit   = property_exists($rs,'Debit')? $rs->Debit : '0.00';
	  if($xdebit==0.00){$xdebit='0.00';}
	  $xcredit   = property_exists($rs,'Credit')? $rs->Credit : '0.00';
	  if($xcredit==0.00){$xcredit='0.00';}
	  $xbalance  = property_exists($rs,'Balance')? $rs->Balance : '0.00';
	  if($xbalance==0.00){$xbalance='0.00';}
	  $xremarks  = property_exists($rs,'Remarks')? $rs->Remarks : '';
	  $xposted  = property_exists($rs,'Posted')? $rs->Posted : 0;
	  $xchkposted  = ($xposted==1)? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>';
	  $xdatepost  = property_exists($rs,'DatePosted')? $rs->DatePosted : '*Non-Ledger';
	  $total = $total+($xdebit-$xcredit);
	  if($xtransid!='')
	  {	  
   ?>
     <tr>
	   <td><?php echo $xayterm;?></td>
	   <td><?php echo $xtransdate;?></td>
	   <td><?php echo $xtransid;?></td>
	   <td><?php echo $xrefno;?></td>
	   <td class="text-right"><?php echo number_format($xdebit,2,'.',',');?></td>
	   <td class="text-right"><?php echo number_format($xcredit,2,'.',',');?></td>
	   <td class="text-right"><?php echo number_format($xbalance,2,'.',',');?></td>
	   <td><?php echo $xremarks;?></td>
	   <td><?php echo $xdatepost;?></td>
	 </tr>
   <?php
      }   
	 }
     
	 if(isset($ledger))
	 {
      $total = (($total<0)?0:$total);		 
    ?>
       <tr>
	    <td colspan="2" class="text-right"><strong>Balance:</strong></td>
	    <td colspan="5" class="text-right"><strong><?php echo number_format($total,2,'.',',');?></strong></td>
	    <td></td>
	    <td></td>
       </tr>
   <?php   
	 }
   }
   else
   {	   
  ?>
  <tr>
   <td colspan="9" class="text-center"><i class="fa fa-warning text-warning"></i> No Data Available</td>
  </tr>
  <?php
   }
  ?>
 </tbody>
</table>