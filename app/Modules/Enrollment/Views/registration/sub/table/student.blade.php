<table class="table table-bordered table-condensed" id="tblapplicant" style="margin-bottom:5px;">
 <thead>
  <tr>
    <th class="table-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label=" " style="width: 24px;">
	 <div class="checker hover">
		<span class="">
			<input type="checkbox" class="chk-header"/>
		</span>
	 </div>
	</th>
	<th width="5%">StudentNo</th>
	<th width="20%">Name</th>
	<th class="text-center autofit">Next Level</th>
	<th class="text-center">Strands</th>
	<th class="text-center">Nationality</th>
	<th class="text-center">Gender</th>
	<th class="text-center autofit">Birth Date</th>
	<th class="text-center">Age</th>
	<th width="10%" class="text-center">Religion</th>
	<th class="text-center autofit">Status</th>
	<th class="text-center">Balance</th>
	<th class="text-center">Accountabilities</th>
  </tr>
 </thead>
 <tbody>
   <tr>
	 <td colspan="14" class="text-center"><i class="fa fa-warning text-warning"></i> No Data Available!</td>
   </tr>
 </tbody>
</table>