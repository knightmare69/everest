<table class="table table-bordered table-hover table-condensed" id="tblregistered" style="margin-bottom:5px; cursor: pointer;">
    <thead>
        <th class="table-checkbox sorting_disabled" rowspan="1" colspan="1" aria-label=" " style="width: 24px;">
        <div class="checker hover">
    		<span class="">
    			<input type="checkbox" class="chk-header"/>
    		</span>
        </div>
        </th>
        <th class="text-center">RegID</th>
        <th class="text-center">RegDate</th>
        <th class="text-center">ID #</th>
        <th class="text-center" width="16%"> Student Name</th>
        <th class="text-center" width="1%" > Gender </th>
        <th class="text-center">Age</th>
        <th class="text-center">Strands</th>
        <th class="text-center">Year Level</th>
        <th class="text-center">Nationality</th>
        <th class="text-center">Status</th>
        <th class="text-center">Table of Fee</th>
        <th class="text-center">Billing No.</th>
        <th class="text-center">Validation Date</th>
        </tr>
    </thead>
    <tbody>
        <tr>
         <td colspan="14" class="text-center"><i class="fa fa-warning text-warning"></i> No Data Available!</td>
        </tr>
    </tbody>
</table>