<table class="table table-bordered" style="font-size:x-small;margin-bottom:15px;">
<tbody>
 <?php 
   $rs = ((isset($info))? $info[0] : false);
   if($rs)
   {
    $tuition  = ((property_exists($rs,'TuitionFee'))?floatval($rs->TuitionFee):0);
    $misc     = ((property_exists($rs,'MiscFee'))?floatval($rs->MiscFee):0);
    $other    = ((property_exists($rs,'OtherAccounts'))?floatval($rs->OtherAccounts):0);
    $addition = ((property_exists($rs,'AdditionalFeesForNewStudent'))?floatval($rs->AdditionalFeesForNewStudent):0);
    $nonresid = ((property_exists($rs,'NonResidentFee'))?floatval($rs->NonResidentFee):0);
	$book     = ((property_exists($rs,'Book'))?floatval($rs->Book):0);
    $gala     = ((property_exists($rs,'GALA'))?floatval($rs->GALA):0);
    $hisfaco  = ((property_exists($rs,'HISFACO'))?floatval($rs->HISFACO):0);
    $ptc      = ((property_exists($rs,'PTC'))?floatval($rs->PTC):0);
    $publi    = ((property_exists($rs,'Publication'))?floatval($rs->Publication):0);
    $pof      = ((property_exists($rs,'POF'))?floatval($rs->POF):0);
    $ict      = ((property_exists($rs,'ICT'))?floatval($rs->ICT):0);
    $scheme   = ((property_exists($rs,'PaymentScheme'))?floatval($rs->PaymentScheme):0);
	$noofpay  = ((property_exists($rs,'NoOfPayment'))?intval($rs->NoOfPayment):1);
	$onepay   = ((property_exists($rs,'FirstPaymentDueDate'))?($rs->FirstPaymentDueDate) : '');
	$twopay   = ((property_exists($rs,'SecondPaymentDueDate'))?($rs->SecondPaymentDueDate) : '');
	$threepay = ((property_exists($rs,'ThirdPaymentDueDate'))?($rs->ThirdPaymentDueDate) : '');
	$fourpay  = ((property_exists($rs,'FourthPaymentDueDate'))?($rs->FourthPaymentDueDate) : '');
	$fivepay  = ((property_exists($rs,'FifthPaymentDueDate'))?$rs->FifthPaymentDueDate:'');
	$sixpay   = ((property_exists($rs,'SixthPaymentDueDate'))?$rs->SixthPaymentDueDate:'');
	$sevenpay = ((property_exists($rs,'SeventhPaymentDueDate'))?$rs->SeventhPaymentDueDate:'');
	$eightpay = ((property_exists($rs,'EighthPaymentDueDate'))?$rs->EighthPaymentDueDate:'');
	$ninepay  = ((property_exists($rs,'NinthPaymentDueDate'))?$rs->NinthPaymentDueDate:'');
	$tenpay   = ((property_exists($rs,'TenthPaymentDueDate'))?$rs->TenthPaymentDueDate:'');
	$total    = floatval($tuition)+
	            floatval($misc)+
				floatval($other)+
				floatval($addition)+
				floatval($nonresid)+
				floatval($book)+
				floatval($gala)+
				floatval($hisfaco)+
				floatval($publi)+
				floatval($ptc);
	
	$div  = 1;
	$part = 1;
	if($scheme == 1)
	{	
	 $div  = 0.45;
     $part = 1;
	}
	else if($scheme == 2)
	{	
     $div = 0.55;
	 $part = 3; 
	}
	else if($scheme == 3)
	{	
     $div = 0.55;
	 $part = 9; 
	}
	
	if($onepay!='')
	{	
	 $tmpdate = new DateTime($onepay);
	 $onepay  = $tmpdate->format('m/d/Y');
	}
	
	if($twopay!='')
	{	
	 $tmpdate = new DateTime($twopay);
	 $twopay  = $tmpdate->format('m/d/Y');
	}
	
	if($threepay!='')
	{	
	 $tmpdate = new DateTime($threepay);
	 $threepay  = $tmpdate->format('m/d/Y');
	}
	
	if($fourpay!='')
	{	
	 $tmpdate = new DateTime($fourpay);
	 $fourpay  = $tmpdate->format('m/d/Y');
	}
	
	$uponamt  = 0;
	$divamt   = 0;
    $main     = ($tuition+$misc+$other+$addition);
	$mainpart = round(((($tuition+$misc+$other+$addition)* $div)/$part),-2);
	$uponamt  = (($scheme==0)? $total : (($main -($mainpart * $part))+ floatval($nonresid)+ floatval($book)+ floatval($gala)+ floatval($hisfaco)+ floatval($publi)+ floatval($ptc)+ floatval($pof)+ floatval($ict))); 
	$divamt   = $mainpart;
   }
 ?>
 <tr><td><strong>Tuition Fee:</strong></td><td class="text-right"><?php echo number_format(floatval($tuition),2,'.',',');?></td></tr>
 <tr><td><strong>Miscellaneous:</strong></td><td class="text-right"><?php echo number_format(floatval($misc),2,'.',',');?></td></tr>
 <tr><td><strong>Other Accounts:</strong></td><td class="text-right"><?php echo number_format(floatval($other),2,'.',',');?></td></tr>
 <tr class="<?php echo ((floatval($addition)<=0)?'hidden':'');?>">
  <td><strong>For New Student:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($addition),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($nonresid)<=0)?'hidden':'');?>">
  <td><strong>NonResidentFee:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($nonresid),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($book)<=0 && floatval($gala)<=0 && floatval($hisfaco)<=0 && floatval($publi)<=0 && floatval($ptc)<=0)?'hidden':'');?>">
   <td colspan="2"></td>
 </tr>
 <tr class="<?php echo ((floatval($book)<=0)?'hidden':'');?>">
  <td><strong>Book:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($book),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($gala)<=0 && floatval($hisfaco)<=0 && floatval($publi)<=0 && floatval($ptc)<=0)?'hidden':'');?>">
   <td colspan="2"><i>Additional fees if applicable (to be collected per family)</i></td>
 </tr>
 <tr class="<?php echo ((floatval($publi)<=0)?'hidden':'');?>">
  <td><strong>Publication:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($publi),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($ptc)<=0)?'hidden':'');?>">
  <td><strong>PTC:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($ptc),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($hisfaco)<=0)?'hidden':'');?>">
  <td><strong>HISFACO:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($hisfaco),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($pof)<=0)?'hidden':'');?>">
  <td><strong>Parent Organization Fee:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($pof),2,'.',',');?></td>
 </tr>
 <tr class="<?php echo ((floatval($gala)<=0)?'hidden':'');?>">
  <td><strong>GALA:</strong></td>
  <td class="text-right"><?php echo number_format(floatval($gala),2,'.',',');?></td>
 </tr>
 <tr><td colspan="2"></td></tr>
 <tr>
  <td><strong>TOTAL:</strong></td>
  <td class="text-right studtotalamt" data-value="<?php echo $total;?>"><?php echo number_format($total,2,'.',',');?></td>
 </tr>
 
</tbody>
</table>
<div class="col-sm-12">
<h5 class="block text-black" style="border-bottom:1px solid black;margin-top:2px;margin-bottom:5px;">Schedule of Payment</h5>
<table class="table table-condensed" style="border:none;font-size:x-small;">
<tbody>
 <tr><td></td><td>Upon Enrollment</td><td><?php echo $onepay;?></td><td class="text-right"><?php echo number_format($uponamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<2 && $scheme<1)?'hidden':'');?>"><td></td><td>2nd Payment</td><td><?php echo $twopay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<3 && $scheme<2)?'hidden':'');?>"><td></td><td>3rd Payment</td><td><?php echo $threepay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<4 && $scheme<2)?'hidden':'');?>"><td></td><td>4th Payment</td><td><?php echo $fourpay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<5 && $scheme<3)?'hidden':'');?>"><td></td><td>5th Payment</td><td><?php echo $fivepay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<6 && $scheme<3)?'hidden':'');?>"><td></td><td>6th Payment</td><td><?php echo $sixpay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<7 && $scheme<3)?'hidden':'');?>"><td></td><td>7th Payment</td><td><?php echo $sevenpay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<8 && $scheme<3)?'hidden':'');?>"><td></td><td>8th Payment</td><td><?php echo $eightpay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<9 && $scheme<3)?'hidden':'');?>"><td></td><td>9th Payment</td><td><?php echo $ninepay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
 <tr class="<?php echo (($noofpay<10 && $scheme<3)?'hidden':'');?>"><td></td><td>10th Payment</td><td><?php echo $tenpay;?></td><td class="text-right"><?php echo number_format($divamt,2,'.',',');?></td></tr>
</tbody>
</table>
</div>