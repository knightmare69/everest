<?php
if(isset($advise) && $advise){
 foreach($advise as $k=>$v){	
?>
  <tr id="<?php echo $v['SubjectID'];?>" 
        data-subj="<?php echo $v['SubjectID'];?>" 
        data-sched="<?php echo $v['ScheduleID'];?>" 
        data-acad="<?php echo $v['AcadUnits'];?>" 
        data-labu="<?php echo $v['LabUnits'];?>"
        data-yearterm="<?php echo $v['YearTermID'];?>"
  >
    <td><a href="javascript:void(0);" class="btn btn-sm btn-danger btnremove"><i class="fa fa-times"></i></a>
	    <a href="javascript:void(0);" class="btn btn-sm btn-primary btnsched"><i class="fa fa-calendar"></i></a></td>
    <td><?php echo $v['SubjectCode'];?></td> 
    <td><?php echo $v['SubjectTitle'];?></td> 
    <td class="tdsection"><?php echo $v['SectionName'];?></td> 
    <td class="tdschedule"><?php echo $v['Sched1'];?></td> 
  </tr>
<?php
 }
}else{
  echo "<tr><td colspan='5' class='text-center'><i class='fa fa-warning text-warning'></i> No Subject/Schedule Available.</td></tr>";	
}
?>