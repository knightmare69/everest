<div class="modal fade bs-modal-lg" id="modal_payment" tabindex="-1" role="dialog" aria-hidden="false">
<div class="modal-backdrop fade in"></div>
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
<h4 class="modal-title">Change Payment Options</h4>
</div>
<div class="modal-body">
	<div class="portlet-body form" style="max-height:320px;overflow-y:auto;overflow-x:hidden;">
		<form action="#" class="form-horizontal" id="payment_form" method="POST" onsubmit="return false;">
           <div class="row" style="padding-left:10px;">
		        <input type="hidden" id="studno" name="studno" required/>
		        <input type="hidden" id="studlvl" name="studlvl" required/>
		        <input type="hidden" id="studmajor" name="studmajor" required/>
				<h5 class="block">Select the scheme of payment and the template of your fees</h5>
				<div class="col-sm-7">
				<div class="form-group">
					<label class="control-label col-md-4">Payment Sheme: <span class="required">*</span></label>
					<div class="col-md-6">
						<select class="form-control studpaymode" id="studpaymode" name="studpaymode" required>
						 <option value="-1" selected disabled>- Select One -</option>
						 <option value="0">Cash</option>
						 <option value="1">Semestral</option>
						 <option value="2">Quarterly</option>
						 <option value="3">Monthly</option>
						</select>
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Template: <span class="required">*</span></label>
					<div class="col-md-6">
						<select class="form-control" id="studtemplate" name="studtemplate" required>
						 <option value="-1" selected disabled>- Select One -</option>
						</select>
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Payment Method: <span class="required">*</span></label>
					<div class="col-md-6">
						<?php echo ((isset($paymethod))?$paymethod:"<select class='form-control paymethod' id='paymethod' name='paymethod'></select>");?>
						<span class="help-block"></span>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-4">Instructions: <span class=""></span></label>
					<div class="col-md-6">
						<p><small class="xinstruction"></small></p>
					</div>
				</div>
				</div>
				<div class="col-sm-5">
				<div class="form-group" style="padding-right:20px;">
				 <div class="breakdown">
				 </div>
				</div>
				</div>
		    </div>
	   </form>
	</div>
</div>
<div class="modal-footer">
   <button class="btn green button-update">Update</button>
</div>
</div>
</div>
</div>			