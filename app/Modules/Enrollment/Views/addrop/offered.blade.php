<?php //error_print($data); die();
$i = 1;
?>
@if(!empty($data))
<table class="table table-striped table-bordered table-hover table_scroll dataTable table-condensed no-footer" id="tbloffered" style="cursor: pointer; margin-top: 0px !important;">    
    <tbody>
        @foreach($data as $r)
        @if($r->Registered < $r->Limit)
        <tr data-id="<?= encode($r->ScheduleID) ?>" data-code="{{$r->Code}}" data-name="{{$r->Name}}" data-unit="{{$r->Credit}}"
            data-section="{{$r->SectionName}}" data-sched="{{$r->Sched1}}" data-room="{{$r->Room1}}" data-faculty="{{$r->Faculty1}}"
            >
            <td width="5%"><?php echo $i; ++$i; ?>.</td>
            <td width="20%">{{$r->Code}}</td>
            <td width="20%">{{$r->SectionName}}</td>
            <td width="10%" class="text-center">{{$r->Registered.'/'.$r->Limit}}</td>
            <td width="25%">{{$r->Sched1}}</td>            
            <td width="20%">{{$r->Room1}}</td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
@endif