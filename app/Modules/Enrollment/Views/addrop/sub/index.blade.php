<div class="portlet light ">
    <div class="portlet-title">
    	<div class="caption caption-md">
    		<i class="icon-bar-chart theme-font font-blue-madison"></i>
    		<span class="caption-subject theme-font font-blue-madison bold uppercase">Add/Drop/Change History</span>
    		<span class="caption-helper">This module is intent for crediting of subject</span>
    	</div>
    	<div class="actions">
    		 <div class="portlet-input input-inline input-medium">
                <select class="form-control input-sm select2" disabled="" name="term" id="term">
                    <option value="-1"> - Select Academic Year - </option>
                    @if(!empty($academic_year))
                        @foreach($academic_year as $ayt)
                            <option <?= $ayt->TermID == $term ? 'selected':'' ?> value="{{($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>
                        @endforeach
                    @endif
                </select>
            </div>
            <button type="button" class="btn btn-default" id="reload" ><i class="fa fa-refresh"></i> Refresh </button>
            <a class="btn btn-default" href="{{url('enrollment/Advising?t='.encode($term))}}"><i class="fa fa-list"></i> List </a>
    	</div>
    </div>
	<div class="portlet-body" >
		<div class="table-scrollable " oncontextmenu="return false;">
			<table id="tblhistory" oncontextmenu="return false;" class="table table-bordered table-condensed table-hover " style="cursor: pointer;">
                <thead>
        			<tr class="uppercase">
                        <th>#</th>
                        <th>Year Term</th>
        				<th > Subject Code </th>
        				<th> Descriptive Title </th>
                        <th class="text-center"> Credit </th>
        				<th class="text-center"> Grade </th>
        				<th class="text-center"> Remarks </th>
                        <th class="text-center" title="Pre-Requisite/s">Pre-Req.</th>
        			</tr>
                </thead>
    			<tbody>

                </tbody>
            </table>
		</div>
	</div>
</div>
<ul id="mouse_menu_right" class="dropdown-menu" style="display: none; " oncontextmenu="return false;">
    <li><a href="javascript:void(0);" class="options" data-menu="credit" >Credit This</a></li>
    <li><a href="javascript:void(0);" class="options" data-menu="remcrd" >Remove Credit</a></li>
    <li class="divider"></li>
    <li><a href="javascript:void(0);" class="options" data-menu="cancel-menu" ><i class="fa fa-times"></i> Close</a></li>
</ul>