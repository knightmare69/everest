<?php //error_print($data); die();?>
@if(!empty($data))
<table class="table table-striped table-bordered table-hover table_scroll dataTable table-condensed no-footer" id="table-students-res" style="cursor: pointer; margin-top: 0px !important;">    
    <tbody>
        @foreach($data as $r)
        <tr data-id="<?= encode($r->StudentNo) ?>">
            <td width="20%">{{$r->StudentNo}}</td>
            <td width="60%">
                {{ ucwords(strtolower($r->FullName)) }}
            </td>
            <td width="20%">
                {{$r->YearLevel}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif