<?php 
    $service = new \App\Modules\Registrar\Services\EvaluationServices;
    $i=1;    
    $regTag = array(
        0 => 'Regular Subject',
        1 => 'Added Subject',
        2 => 'Change Subject',
        3 => 'Dropped Subject',
    );
?>
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img class="img-responsive" alt="{{asset('assets/admin/layout/img/logo.png')}}" src="{{ url() .'/general/getPupilPhoto?Idno='. encode($data->StudentNo) .'&t='. rand()  }}">
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name" id="student-name" data-prog="{{$data->ProgID}}">
					 <?= trimmed($data->LastName) . ', '.trimmed($data->FirstName) ?>
				</div>
				<div class="profile-usertitle-job" id="student-num">
					 <?= ($idno) ?>
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<div class="profile-userbuttons">
            
                <span class="bold font-purple">{{$data->Major}}</span>
                <br />
                <span class="font-xs font-grey">Strand / Track</span>
				<br />
                <span class="bold">{{$data->YearLevel}}</span>
                <br />
                <span class="font-xs font-grey">Level</span>
                <br />
                <span class="bold">{{$data->CurriculumCode}}</span>
                <br />
                <span class="font-xs font-grey">Curriculum</span>
				
			</div>
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active"><a href="javascript:void(0);" class="views" data-menu="addrop"><i class="icon-home"></i> Add/Drop/Change </a></li>
					<li><a href="javascript:void(0);" class="views" data-menu="history"><i class="icon-user"></i> History </a></li>
					<li><a class="views" href="javascript:void(0);"><i class="icon-info"></i>Help </a></li>
				</ul>
			</div>
			<!-- END MENU -->
		</div>														
	</div>
	<!-- END BEGIN PROFILE SIDEBAR -->
	<!-- BEGIN PROFILE CONTENT -->
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET -->
                <div id="Advising">                                                                        
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md"><i class="icon-bar-chart theme-font font-blue-madison"></i>
							<span class="caption-subject theme-font font-blue-madison bold uppercase">Registered Subjects</span>
							<span class="caption-helper">This module is intent for Advising of subject</span>
						</div>
						<div class="actions">
							 <div class="portlet-input input-inline input-medium">
                                <select class="form-control input-sm select2" disabled="" name="term" id="term">
                                    <option value="-1"> - Select Academic Year - </option>
                                    @if(!empty($academic_year))
                                        @foreach($academic_year as $ayt)                                                                                    
                                            <option <?= $ayt->TermID == $term ? 'selected':'' ?> value="{{($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>                                                            
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <button type="button" class="btn btn-default" data-menu="reload"><i class="fa fa-refresh"></i> Refresh </button>                            
                            <a class="btn btn-default" href="{{url('add-drop-change?t='.encode($term))}}"><i class="fa fa-list"></i> List </a>  
						</div>
					</div>
					<div class="portlet-body" >	 
                        @if($locked == '0')                      
                        <button type="button" class="btn btn-primary btn-sm" data-menu="addsubj"><i class="fa fa-plus"></i> Add Subject </button>                                                
                        <button type="button" class="btn btn-danger btn-sm" data-menu="dropsubj"><i class="fa fa-times"></i> Drop Subject </button>
                        @endif
                        
                        <div class="pull-right">
                            @if($locked == '0')
                            <button type="button" class="btn btn-success btn-sm" data-menu="save"><i class="fa fa-save"></i> Save </button>
                            @endif
                            <button type="button" class="btn btn-default btn-sm" id="btnprint" data-reg="{{encode($regid)}}" data-menu="print"><i class="fa fa-print"></i> Print </button>
                        </div>                                                                                                                                                    									
						<div class="table-scrollable ">
							<table id="tblregistered" class="table table-bordered table-condensed table-hover " style="cursor: pointer;">
							<thead>
							<tr class="uppercase text-center">
                                <th>#</th>
                                @if($locked == '0')
                                <th>Change</th>
                                @endif
								<th>Status</th>
                                <th> Code </th>
								<th> Descriptive Title </th>
                                <th class="text-center">Credit</th>
								<th>Section</th>
								<th>Schedule</th>
								<th>Room</th>
                                <th>Faculty</th>
                                
							</tr>
							</thead>
							<tbody>
                            @foreach($advised as $r)
                                <?php
                                    $index = getObjectValue($r,'IndexID');
                                    $subjid = getObjectValue($r,'SubjectID');
                                    
                                    $credited = $service->prereqPassed($index, $subjid, $idno);
                                    $prereq = $service->getPrereqSubCode($index, $subjid);
                                ?>
                                
                                @if($credited == '1')
                                <tr data-id="{{$r->SubjectID}}" data-sched="{{encode($r->ScheduleID)}}" data-dirty="{{$r->CurrentTxn}}" data-tag="{{$r->RegTagID}}" >
                                    <td class="autofit font-xs"><?php echo $i; $i++; ?></td>
                                    @if($locked == '0')
                                    <td class="autofit action">
                                        @if($r->CurrentTxn == 1)
                                            <a href="javascript:void(0);" class="options" data-menu="cancel-txn" ><i class="fa fa-times"></i> Cancel</a>
                                        @else
                                            <a href="javascript:void(0);" class="options" data-menu="change" ><i class="fa fa-retweet"></i> Change</a>
                                        @endif
                                    </td>
                                    @endif
                                    <td class="autofit status">{{$regTag[getObjectValue($r,'RegTagID')]}}</td>
                                    <td class="autofit">{{$r->SubjectCode}}</td>
                                    <td>{{$r->SubjectTitle}}</td>
                                    <td class="autofit text-center">{{getObjectValue($r,'Credit')}}</td>
                                    <td class="">                                        
                                        <?= $r->SectionName != '' ? $r->SectionName : '- select -' ?>
                                    </td>
                                    <td class="sched autofit">{{$r->Sched_1}}</td>
                                    <td class="room autofit">{{$r->RoomName}}</td>
                                    <td class="autofit faculty">{{getObjectValue($r,'Faculty')}}</td>
                                    
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                            <tfoot style="display: none;">
                                <tr>
                                    <td class="autofit font-xs"></td>
                                    <td class="change autofit">
                                        <a href="javascript:void(0);" class="options" data-menu="cancel-txn" ><i class="fa fa-times"></i> Cancel</a>
                                    </td>
                                    <td class="stat autofit"></td>
                                    <td class="code autofit"></td>
                                    <td class="name"></td>
                                    <td class="unit autofit text-center"></td>
                                    <td class="sect"></td>
                                    <td class="sched autofit"></td>
                                    <td class="room autofit"></td>
                                    <td class="faculty autofit"></td>                                    
                                </tr>
                            </tfoot>
                            </table>
						</div>                                                                   
					</div>
				</div>	
                </div>
                <div id="evaluation" style="display: none;">
                    @include($views.'sub.index')
                </div>   			
			</div>			
		</div>		
	</div>	
</div>
</div>
@include($views.'schedule')