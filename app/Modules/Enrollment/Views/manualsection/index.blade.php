<div class="row">
<div class="col-sm-12">
	<div class="portlet light">
		<div class="tab-content">
			<div class="tab-pane active" id="tblist">
			   <div class="row">
				<div class="col-sm-12">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user fa-2x"></i> Student Info
							</div>
							<div class="tools">
								<select class="form-control" id="term" name="term">
									<option value="-1"> - Select AYTerm - </option>
									<?php
								     $aqry = DB::select("SELECT * FROM ES_AYTerm ORDER BY AcademicYear DESC,SchoolTerm DESC");
									 if($aqry && count($aqry)>0){
									    foreach($aqry as $rs){
										 echo '<option value="'.$rs->TermID.'" '.(($rs->IsCurrentTerm==1)?'selected':'').'>'.$rs->AcademicYear.' '.$rs->SchoolTerm.'</option>';
										}
									 }
									?>
								</select>
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row" style="margin-bottom:10px;">
							   <div class="col-sm-12" style="margin-bottom:5px;">
								<label class="col-sm-4" style="padding-left:1px !important;">Student:<b class="studno"></b></label>
								<label class="col-sm-4">Level:<b class="yrlvl"></b></label>
								<label class="col-sm-4">Acad Prog:<b class="program"></b></label>
								<div class="input-group">
									<input type="text" class="form-control" id="studname" name="studname"/>
									<span class="input-group-addon btnfilter" data-source="student" data-parameter="#studname" data-id=".studno"><i class="fa fa-search"></i></span>
								</div>
							   </div>
							   <div class="col-sm-4">
								<label>RegID:</label>
								<input type="text" class="form-control" id="regid" name="regid" readonly/>
							   </div>
							   <div class="col-sm-4">
								<label>RegDate:</label>
								<input type="text" class="form-control" id="regdate" name="regdate" readonly/>
							   </div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-table fa-2x"></i> Reg. Details
							</div>
							<div class="tools">
								<button class="btn btn-sm btn-warning btnsave"><i class="fa fa-save"></i> Save</button>
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row" style="margin-bottom:10px;">
							   <div class="col-sm-12">
							   <div class="table-responsive">
									<table class="table table-bordered table-condensed" id="regdetail">
										<thead>
											<th width="90px;"><button class="btn btn-sm btn-info btnrefresh"><i class="fa fa-refresh"></i></button>
											                  <button class="btn btn-sm btn-danger btnadd"><i class="fa fa-plus"></i></button></th>
											<th class="text-center">Section</th>
											<th class="text-center">Code</th>
											<th class="text-center">Title</th>
											<th class="text-center">Lec</th>
											<th class="text-center">Lab</th>
											<th class="text-center">Target Section</th>
											<th class="text-center">Target Code</th>
											<th class="text-center">Target Title</th>
										</thead>
										<tbody></tbody>
										<tfoot class="hidden">
										  <tr data-regid="tmp" data-sched="tmp" data-subj="tmp" data-targetsubj="">
				                           <td><button class="btn btn-sm btn-danger btnsubjdel"><i class="fa fa-trash-o"></i></button>
										       <button class="btn btn-sm btn-warning btnsubjedt"><i class="fa fa-edit"></i></button></td>
				                           <td class="text-center"></td>
				                           <td class="text-center"></td>
				                           <td></td>
				                           <td class="text-center"></td>
				                           <td class="text-center"></td>
				                           <td class="targetsect" data-sched="0"></td>
				                           <td class="targetsubj"></td>
				                           <td></td>
										  </tr>
										</tfoot>
									</table>
							   </div>
							   </div>
							</div>
						</div>
					</div>
				</div>
			   </div>
			</div>
		</div>
	</div>
</div>

	
</div>
<div id="modal_filter" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select from the list</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="input-group">
				    <input class="form-control" id="txt-modal-search" data-button=".btn-modal-search" placeholder="StudentNo, LastName, Firstname" type="text">
					<span class="input-group-addon btn-modal-search" data-source="#txt-modal-search"><i class="fa fa-search"></i></span>
				  </div>
				  <div class="table-responsive modal_list" style="max-height:400px; overflow:scroll;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>Last Name</th>
						    <th>First Name</th>
						    <th>Middle Name</th>
						    <th>Program</th>
						    <th>Level</th>
						</thead>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-modal-select">Select</button>
				</div>
			</div>
		</div>
	</div>	
</div>