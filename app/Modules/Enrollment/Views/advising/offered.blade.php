<?php //error_print($data); die();
$i = 1;
?>
@if(!empty($data))
<table class="table table-striped table-bordered table-hover table_scroll dataTable table-condensed no-footer" id="tbloffered" style="cursor: pointer; margin-top: 0px !important;">    
    <tbody>
        @foreach($data as $r)
            @if($r->Registered < $r->Limit )
        <tr data-id="<?= ($r->ScheduleID) ?>" data-secId="{{($r->SectionID)}}" data-section="{{$r->SectionName}}" data-sched="{{$r->Sched1}}" data-room="{{$r->Room1}}" >
            <td width="5%"><?php echo $i; ++$i; ?></td>
            <td width="20%">{{$r->SectionName}}</td>
            <td width="10%">{{$r->Limit}}</td>
            <td width="45%">{{$r->Sched1}}</td>            
            <td width="20%">{{$r->Room1}}</td>
        </tr>
            @endif
        @endforeach
    </tbody>
</table>
@endif