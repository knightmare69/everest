<?php
    $service = new \App\Modules\Registrar\Services\EvaluationServices;
    $i=1;
?>
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img class="img-responsive" alt="{{asset('assets/admin/layout/img/logo.png')}}" src="{{ url() .'/general/getPupilPhoto?Idno='. encode($data->StudentNo) .'&t='. rand()  }}">
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name" id="student-name">
					 <?= trimmed($data->LastName) . ', ' . trimmed($data->FirstName) ?>
				</div>
				<div class="profile-usertitle-job" id="student-num">
					 <?= ($idno) ?>
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<div class="profile-userbuttons">

                <span class="bold font-purple">{{$data->Major}}</span>
                <br />
                <span class="font-xs font-grey">Strand / Track</span>
				<br />
                <span class="bold">{{$data->YearLevel}}</span>
                <br />
                <span class="font-xs font-grey">Level</span>
                <br />
                <span class="bold">{{$data->CurriculumCode}}</span>
                <br />
                <span class="font-xs font-grey">Curriculum</span>

			</div>
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active">
						<a href="javascript:void(0);" class="views" data-menu="advising"><i class="icon-home"></i> Advising </a>
					</li>
					<li>
						<a href="javascript:void(0);" class="views" data-menu="evaluation"><i class="icon-user"></i> Evaluation </a>
					</li>
					<li><a class="views" href="javascript:void(0);"><i class="icon-check"></i>Conduct </a></li>
					<li><a class="views" href="javascript:void(0);"><i class="icon-info"></i>Help </a></li>
				</ul>
			</div>
			<!-- END MENU -->
		</div>
	</div>
	<!-- END BEGIN PROFILE SIDEBAR -->
	<!-- BEGIN PROFILE CONTENT -->
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET -->
                <div id="advising">
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font font-blue-madison"></i>
							<span class="caption-subject theme-font font-blue-madison bold uppercase">Advised Subjects</span>
							<span class="caption-helper">This module is intent for advising of subject</span>
						</div>
						<div class="actions">
							 <div class="portlet-input input-inline input-medium">
                                <select class="form-control input-sm select2" disabled="" name="term" id="term">
                                    <option value="-1"> - Select Academic Year - </option>
                                    @if(!empty($academic_year))
                                        @foreach($academic_year as $ayt)
                                            <option <?= $ayt->TermID == $term ? 'selected':'' ?> value="{{($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <button type="button" class="btn btn-default" data-menu="reload"><i class="fa fa-refresh"></i> Refresh </button>
                            <a class="btn btn-default" href="{{url('enrollment/advising?t='.encode($term))}}"><i class="fa fa-list"></i> List </a>
						</div>
					</div>
					<div class="portlet-body" >
                        @if($balance > 0 )
                            <div class="alert alert-danger">
                                <i class="fa fa-waning"></i> Sorry, unable to advised student. Please settle your balance first before proceed to advising.
                            </div>
                        @else
                            <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> Re-Advised </button>
                            <button type="button" class="btn btn-success btn-sm" data-menu="register"><i class="fa fa-save"></i> Register </button>
                            <div class="portlet-input input-inline  ">
                                <label class="control-label font-xs bold"> Option : </label>
                                <select id="optionbk" class=" form-control input-small input-inline">                        		   
                                   <option value="1">E-Book</option>
                        		   <option value="0">Textbook</option>
                                   <option value="-1">N/A</option>
                                  </select>
                            </div>
                            <button type="button" class="btn btn-default pull-right btn-sm" id="btnprint" data-reg="{{encode($regid)}}" data-menu="print"><i class="fa fa-print"></i> Print </button>
                        @endif
						<div class="table-scrollable ">
							<table id="tbladvised" class="table table-bordered table-condensed table-hover " style="cursor: pointer;">
							<thead>
							<tr class="uppercase text-center">
                                <th>#</th>
								<th> Code </th>
								<th> Descriptive Title </th>
                                <th class="text-center">Credit</th>
								<th>Section</th>
								<th>Schedule</th>
								<th>Room</th>
                                <th>Year Term</th>
							</tr>
							</thead>
							<tbody>
                            @foreach($advised as $r)
                                <?php
                                    $index = getObjectValue($r,'IndexID');
                                    $subjid = getObjectValue($r,'SubjectID');

                                    $credited = $service->prereqPassed($index, $subjid, $idno);
                                    $prereq = $service->getPrereqSubCode($index, $subjid);
                                ?>

                                @if($credited == '1')
                                <tr data-id="{{$r->SubjectID}}" data-sched="0" >
                                    <td class="autofit font-xs"><?php echo $i; $i++; ?></td>
                                    <td class="autofit">{{$r->SubjectCode}}</td>
                                    <td>{{$r->SubjectTitle}}</td>
                                    <td class="autofit text-center">{{getObjectValue($r,'Credit')}}</td>
                                    <td class="">
                                        <a href="javascript:void(0);" class="options section" data-menu="sched" ><?= $r->SectionName != '' ? $r->SectionName : '- select -' ?></a>
                                    </td>
                                    <td class="sched autofit">{{$r->Sched_1}}</td>
                                    <td class="room autofit">{{$r->RoomName}}</td>
                                    <td class="autofit">{{getObjectValue($r,'YearTerm')}}</td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                            </table>
						</div>
					</div>
				</div>
                </div>
                  <div id="evaluation" style="display: none;">
                        @include($views.'evaluation.index')
                  </div>
			</div>
		</div>
	</div>
</div>
</div>
@include($views.'schedule')