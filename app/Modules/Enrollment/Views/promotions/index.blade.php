<?php 
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;     
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-graduation-cap"></i>
			<span class="caption-subject bold uppercase"> Student Promotions </span>
			<span class="caption-helper">Use this module to promote student to higher level...</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline  input-medium">									
	           <input type="text" class="form-control input-circle  filter-table input-sm " data-table="records-table" />
			</div>                                			                        
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">            
            <div class="portlet-input input-inline input-medium">									
                <select class="select2 form-control" name="academic-term" id="academic-term">
                    <option value="0" >- Select AY Term -</option>
                    @if(!empty($at))                        
                        @foreach($at as $r)
                        <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
                        @endforeach
                    @endif
                </select>				
			</div> 
            <div class="portlet-input input-inline input-medium">									
                <select class="form-control" name="level" id="level">                    	
					<option value="0">- select - </option>
					@foreach(App\Modules\Enrollment\Models\YearLevel::get() as $r)
					<option <?= $level == $r->YLID_OldValue && $r->ProgID == $prog ? 'selected' : '' ?> data-prog="{{encode($r->ProgID)}}" value="{{ encode($r->YLID_OldValue) }}">{{ $r->YearLevelName }}</option>
					@endforeach
				</select>                				
			</div>
             <div class="portlet-input input-inline input-small">									
                <select class="form-control" name="filter" id="filter">                    	
					<option value="0" <?= $filter == '0' ? 'selected' : '' ?> > for Promotion </option>
					<option value="1" <?= $filter == '1' ? 'selected' : '' ?> > Promoted </option>
                    <option value="2" <?= $filter == '2' ? 'selected' : '' ?> > View All </option>
				</select>                				
			</div>
            
            <button type="button" class="btn btn-sm btn-default " data-menu="refresh"><i class="fa fa-refresh"></i> Refresh </button>                 
            <button type="button" class="btn btn-sm btn-primary " data-menu="add"><i class="fa fa-plus"></i> Promote Selected </button>            
        </div>
        <div class="scroller" id="grdList">
            @include($views.'list')            
        </div>        	
	</div>
</div>