<?php

    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;

    $reg_with_bal = $service->registerWithBalance(['TermID' => $term]);
    $regs_studnum = array_pluck($reg_with_bal, 'StudentNo');

    $j = 1;

    global $yrlvl;
    $x = 0;

    $yrlvl = App\Modules\Enrollment\Models\YearLevel::where('Inactive',0)->get();

    function selectedYear( $yr, $progid ){
        global $yrlvl;
        $return = '';
        $selected = "";
        $disable = "display:none;";

        foreach($yrlvl as $r){

            if($disable != '' &&  $yr == $r->YLID_OldValue && $r->ProgID == $progid ){
                $disable = "";
            }

            if($yr + 1 == $r->YLID_OldValue){
                $selected = "selected";
            }

            $return .= '<option style="'.$disable.'" '. $selected.' data-prog="'.$r->ProgID.'" value="'. ($r->YLID_OldValue).'">'.$r->YearLevelName.'</option>';
            $selected = "";
        }


        return $return ;
    }

    $balance = 0;


?>
<div class="" style="padding-bottom: 5px;">
<div class="btn-group btn-group-circle">
    <?php
        $add = 0;
        $max =  $total / 50;
        // if( $total % 50 <= 4) $add = 1;
        $max = (floor($max) + 1);

        $url = url('enrollment/promotions?t='.encode($term).'&y='.encode($level).'&p='.encode($prog).'&f='.($filter));
    ?>
    <a class="btn btn-default" href="{{$url.'&v=1'}}">&LessLess; First</a>
    @for($i =1 ; $i<= $max; $i++ )
        <a class="btn btn-default <?= $page == $i ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a>
	@endfor
    <a class="btn btn-default" href="{{$url.'&v='.$max}}">Last &GreaterGreater;</a>
</div>
</div>

<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Status</th>
            <th>Reg #</th>
            <th>Student Name</th>
            <th>Current Level</th>
            <th>Promotion Level</th>
            @if($level == 4 && $prog == '1' )
            <th>Track/Strands</th>
            @endif
            <th>Outstanding Balance</th>
            <th>Accountabilities</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <?php
            $balance = number_format( getOutstandingBalance('1',$r->StudentNo) , 2 , ".", ",") ;
            $majorid = $r->MajorID == 0 ? $r->TrackID : $r->MajorID ;
        ?>
        <tr data-id="{{ encode($r->RegID) }}" data-idno="{{ encode($r->StudentNo) }}" data-major="{{$majorid}}" data-bal="<?= floatval( $balance )  ?>" data-acct="{{$r->Accountabilities}}" >
            <td class="autofit">
                @if($r->Promoted == '0' && $balance <= 0 && trimmed($r->Accountabilities) == '' || in_array($r->StudentNo, $regs_studnum))
                    <input type="checkbox" class="chk-child" >
                @endif
            </td>
            <td class="font-xs autofit bold">{{$j}}.</td>
            <td class="status">
                @if( (floatval($balance) > 0 || trimmed($r->Accountabilities) != '') && !in_array($r->StudentNo, $regs_studnum) )
                <span class="label label-lg label-danger "> Unable to promote </span>
                @elseif($r->Promoted == '1')
                <span class="label label-lg label-success "> Promoted </span>
				@elseif(in_array($r->StudentNo, $regs_studnum))
                    Approve to Promote
                @else
                 Ready to Promote
                @endif
            </td>
            <td>{{ $r->RegID }}</a></td>
            <td class="">{{ $r->StudentName }} <br /> <small class="font-xs bold">{{$r->StudentNo}}</small></td>
            <td>{{$r->YearLevel}} </td>

            <td class="autofit">
                @if($r->Promoted == '0')
                    <select class="form-control input-small input-sm level" >
    					<option value="">Select...</option>
    					<?= selectedYear($r->YearLevelID, $r->ProgID) ?>
    				</select>
                @endif
            </td>
            @if( ($level == 4 && $prog == '1')  )

            <td class="autofit">
               	<select name="track" class="input-borderless input-sm track input-medium form-control ">
                    <option value="0">- Select -</option>
                    @foreach($service->GetAcademicTrack() as $at)
                    <option value="{{ $at->IndexID }}" <?= $majorid == $at->IndexID ? 'selected' : ''  ?> >{{ $at->MajorDiscDesc }}</option>
                    @endforeach
                </select>
            </td>

            @endif
            <td class="autofit balance text-right ">{{ $balance }}</td>
            <td class="acct">{{$r->Accountabilities}}</td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>
<div class="">
<div class="btn-group btn-group-circle">
    <a class="btn btn-default" href="{{$url.'&v=1'}}">&LessLess; First</a>
    @for($i =1 ; $i<= $max; $i++ )
        <a class="btn btn-default <?= $page == $i ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a>
	@endfor
    <a class="btn btn-default" href="{{$url.'&v='.$max}}">Last &GreaterGreater;</a>
</div>
</div>
