<?php
namespace App\Modules\Enrollment\Services\Registration;
use Validator;
Class Validation {
	public function validateSave($post)
	{
		$validator = validator::make(
			[
				'Username'		=> getObjectValue($post,'username'),
				'FullName'	=> getObjectValue($post,'FullName'),
				// 'position_id'	=> getObjectValue($post,'position'),
				// 'department_id'	=> getObjectValue($post,'department'),
				'group_id'		=> getObjectValue($post,'group'),
				'email'		=> getObjectValue($post,'email'),
				'password'		=> getObjectValue($post,'upassword'),
				'password_confirmation'	=> getObjectValue($post,'cpassword'),

			],			
			[
				'Username'		=> 'required|unique:ESv2_Users',
				'password'		=> 'required|confirmed',
				'FullName'	=> 'required',
				'email'			=> 'required|email|unique:ESv2_Users',
				// 'position_id'	=> 'required',
				// 'department_id'	=> 'required',
				'group_id'		=> 'required',
			]
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			[
				'Username'		=> getObjectValue($post,'username'),
				'FullName'	=> getObjectValue($post,'FullName'),
				// 'position_id'	=> getObjectValue($post,'position'),
				// 'department_id'	=> getObjectValue($post,'department'),
				'group_id'		=> getObjectValue($post,'group'),
				'email'		=> getObjectValue($post,'email'),
			],			
			[
				'Username'		=> 'required',
				'FullName'	=> 'required',
				'email'			=> 'required',
				// 'position_id'	=> 'required',
				// 'department_id'	=> 'required',
				'group_id'		=> 'required',
			]
		);
		return $validator;
	}
}	
?>