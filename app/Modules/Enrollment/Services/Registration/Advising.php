<?php
namespace App\Modules\Enrollment\Services\Registration;
use DateTime;
use DB;
Class Advising
{
  public $info             = false;
  public $termid           = 105;
  public $campusid         = 1;
  public $ayterm           = '';
  public $schoolterm       = '';
  public $progid           = 29;
  public $strand           = 610;
  public $curriculumid     = 164;
  public $regid            = 0;
  public $yrlvlid          = 0;
  public $minload          = 15;
  public $maxload          = 26;
  public $countenr         = 0;
  public $readvised        = 0;
  public $isadvised        = 0;
  public $studinfo         = array();
  public $subj_curr        = array();
  public $subj_sched       = array();
  public $subj_taken       = array();
  public $subj_offer       = array();
  public $subj_prereq      = array();
  public $subj_equiva      = array();

  function check_IsAdvise($termid=105,$studno=''){
	 $data = DB::select("SELECT COUNT(*) as Items FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$termid);
	 return $data[0]->Items;
  }

  function get_CurrentYearLvl($termid=105, $studno=''){
	 $yrlvl    = 5;
	 $progid   = 29;
	 $data     = DB::select("SELECT TOP 1 ProgID,YearLevelID FROM ES_Students WHERE StudentNo='".$studno."'")[0];
	 $regs     = DB::select("SELECT COUNT(RegID) as RegCount FROM ES_Registrations WHERE StudentNo='".$studno."' AND ProgID=29 AND YearLevelID=5 AND TermID<".$termid)[0];
	 if($data->ProgID==1 && $data->YearLevelID==4){
		$this->progid  = 29;
		$this->yrlvlid = 5;
		$yrlvl         = 5;
	 }else{
		if($regs->RegCount>=2)
         $yrlvl = 6;
        else
         $yrlvl = 5;
	 }

	 return $yrlvl;
  }

  function get_ApplicantInfo($termid,$studno){
	 $data = DB::select("SELECT ISNULL(s.StudentNo,a.AppNo) as StudentNo
							   ,ISNULL(s.LastName,a.LastName) as LastName
							   ,ISNULL(s.FirstName,a.FirstName) as FirstName
							   ,ISNULL(s.MiddleName,a.MiddleName) as MiddleName
							   ,".$termid." as TermID
							   ,ISNULL(r.CampusID,a.CampusID) as CampusID
							   ,ISNULL(r.ProgID,29) as ProgID
							   ,ISNULL(r.YearLevelID,(CASE WHEN AppTypeID=20 THEN 5 ELSE 6 END)) as YearLevelID
							   ,ISNULL(r.MajorID,a.MajorID) as MajorID
							   ,".$this->curriculumid." as CurriculumID
							   ,ISNULL(r.RegID,'') as RegID
							   ,1 as ReAdvise
							   ,rd.RegTagID
							   ,rd.ScheduleID
							   ,cs.SubjectID
							   ,pd.GradeIDX
							   ,pd.SubjectCode
							   ,pd.SubjectTitle
							   ,pd.SummaryID
							   ,pd.Final_Average
							   ,(CASE WHEN pd.Final_Remarks='' OR pd.Final_Remarks IS NULL THEN
									(SELECT TOP 1 gr.Remarks
										FROM ES_GradingSystem as gs
								  INNER JOIN ES_GradeFinalRemark as gr ON gs.FinalRemarkID=gr.RemarkID
									   WHERE gs.ProgClassCode=pd.ProgClassID AND ((pd.Final_Average BETWEEN gs.MinGrade AND gs.MaxGrade) OR pd.Final_Average = Grade))
								 ELSE
								     pd.Final_Remarks
								 END) as Final_Remarks
						FROM (
						SELECT AppNo,CampusID,TermID,AppTypeID,GradeLevelID,ProgramID,MajorID,LastName,FirstName,MiddleName,MiddleInitial,ExtName,Gender
						  FROM ESv2_Admission
						UNION ALL
						SELECT AppNo,Choice1_CampusID,TermID,ApplyTypeID,0 as GradeLevelID,Choice1_Course as ProgramID,Choice1_CourseMajor as MajorID,LastName,FirstName,MiddleName,MiddleInitial,ExtName,Gender
						  FROM ES_Admission
						) as a
					  LEFT JOIN ES_Students as s ON a.AppNo=s.AppNo
					  LEFT JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo AND r.ProgID=29
					  LEFT JOIN ES_RegistrationDetails as rd ON r.RegID=rd.RegID
					  LEFT JOIN ES_ClassSchedules as cs ON rd.ScheduleID = cs.ScheduleID
					  LEFT JOIN ES_PermanentRecord_Details as pd ON s.StudentNo=pd.StudentNo AND r.RegID=pd.RegID AND rd.ScheduleID=pd.ScheduleID
						  WHERE a.AppNo='".$studno."' AND a.TermID=".$termid."
					   ORDER BY a.TermID DESC");
     return $data;
  }

  function get_StudentInfo($termid,$studno){
	 $data = DB::select("SELECT s.StudentNo
	                           ,s.LastName
							   ,s.FirstName
							   ,s.MiddleName
							   ,'".$termid."' as TermID
							   ,r.CampusID
							   ,ISNULL(r.ProgID,29) as ProgID
							   ,ISNULL(r.YearLevelID,s.YearLevelID) as YearLevelID
							   ,(CASE WHEN (r.TermID IS NULL OR r.TermID<>".$termid.") AND ISNULL(r.MajorID,0)<>".$this->strand." THEN ".(($this->strand<>-1)?$this->strand:'r.MajorID')." ELSE r.MajorID END) as MajorID
							   ,".$this->curriculumid." as CurriculumID
							   ,(SELECT TOP 1 ISNULL(RegID,'') as RegID FROM ES_Registrations WHERE StudentNo='".$studno."' AND TermID='".$termid."') as RegID
							   ,(CASE WHEN s.CurriculumID<>".$this->curriculumid." THEN 1 ELSE 0 END) as ReAdvise
							   ,rd.RegTagID
							   ,rd.ScheduleID
							   ,cs.SubjectID
							   ,pd.GradeIDX
							   ,pd.SubjectCode
							   ,pd.SubjectTitle
							   ,pd.SummaryID
							   ,pd.Final_Average
							   ,(CASE WHEN pd.Final_Remarks='' OR pd.Final_Remarks IS NULL THEN
								   (SELECT TOP 1 gr.Remarks
									  FROM ES_GradingSystem as gs
								INNER JOIN ES_GradeFinalRemark as gr ON gs.FinalRemarkID=gr.RemarkID
									 WHERE gs.ProgClassCode=pd.ProgClassID AND ((pd.Final_Average BETWEEN gs.MinGrade AND gs.MaxGrade) OR pd.Final_Average = Grade))
								ELSE
								  pd.Final_Remarks
								END) as Final_Remarks
						   FROM ES_Students as s
					  LEFT JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo AND r.ProgID=29
					  LEFT JOIN ES_RegistrationDetails as rd ON r.RegID=rd.RegID
					  LEFT JOIN ES_ClassSchedules as cs ON rd.ScheduleID = cs.ScheduleID
					  LEFT JOIN ES_PermanentRecord_Details as pd ON s.StudentNo=pd.StudentNo AND r.RegID=pd.RegID AND rd.ScheduleID=pd.ScheduleID
						  WHERE s.StudentNo='".$studno."'
					   ORDER BY r.TermID DESC");
     return $data;
  }

  function get_StudentDetails($termid,$studno=''){
	 $this->termid  = $termid;
	 if(substr($studno,4,3)=='200' || substr($studno,4,3)=='100' || substr($studno,4,3)=='300'){
       $data          = $this->get_StudentInfo($termid,$studno);

       if( count($data) > 0 ){
        $this->yrlvlid = $data[0]->YearLevelID;
       }else{
        $this->yrlvlid = $this->get_CurrentYearLvl($termid,$studno);
       }

	 }else{
	   $data          = $this->get_ApplicantInfo($termid,$studno);
	   $this->yrlvlid = 0;
	 }

     if(!$data){return array('success'=>false,'content'=>'','error'=>DB::getQueryLog()); }
	 foreach($data as $r){
		    $this->studinfo['StudentNo']   = $r->StudentNo;
		    $this->studinfo['LastName']    = $r->LastName;
		    $this->studinfo['FirstName']   = $r->FirstName;
		    $this->studinfo['MiddleName']  = $r->MiddleName;
		    $this->studinfo['RegID']       = $r->RegID;
		    $this->studinfo['ReAdvise']    = $r->ReAdvise;
			$this->readvised               = $r->ReAdvise;
			if($termid==$r->TermID){
		      $this->studinfo['TermID']        = $r->TermID;
		      $this->regid                     = $r->RegID;
		      $this->studinfo['RegID']         = $this->regid;
			  $this->progid                    = $r->ProgID;
			  $this->studinfo['ProgID']        = $r->ProgID;
			  $this->strand                    = $r->MajorID;
			  $this->studinfo['MajorDiscID']   = $r->MajorID;
			  $this->curriculumid              = $r->CurriculumID;
			  $this->studinfo['CurriculumID']  = $this->curriculumid;
			  $this->studinfo['YearLevelID']   = (($this->yrlvlid==0)?5:($this->yrlvlid));
			  $this->isadvised                 = (($r->ReAdvise==1)? 0 : 1);
			  $this->studinfo['IsAdvised']     = (($r->ReAdvise==1)? 0 : 1);
			}else{
			  $this->studinfo['TermID']        = $termid;
			  $this->studinfo['RegID']         = '';
			  $this->studinfo['ProgID']        = $this->progid;
			  $this->studinfo['MajorDiscID']   = $this->strand;
			  $this->studinfo['CurriculumID']  = $this->curriculumid;
			  $this->studinfo['YearLevelID']   = (($this->yrlvlid==0)?5:($this->yrlvlid));
			  $this->studinfo['IsAdvised']     = $this->check_IsAdvise($termid,$studno);
			  $this->isadvised                 = $this->check_IsAdvise($termid,$studno);
			}

			$this->get_Curriculum();
			$this->get_Prereq_Equiv();
			$this->info = true;

		 if($r->SubjectID!='' && $r->GradeIDX!=''){
		    $tmp_subj = $r->SubjectID;
		    $arr_data = array();
		    $arr_data['SubjectID'] = $r->SubjectID;
		    $arr_data['GradeIDX']  = $r->GradeIDX;
		    $arr_data['Grade']     = $r->Final_Average;
		    $arr_data['Remarks']   = $r->Final_Remarks;
			if(!array_key_exists($tmp_subj,$this->subj_taken) || ($this->subj_taken[$tmp_subj]['Remarks']!='Passed' || $this->subj_taken[$tmp_subj]['Remarks']!='Credited')){
			  $this->subj_taken[$tmp_subj]=$arr_data;
			}
		 }
	 }

	 return array('success'=>true,'content'=>$this->studinfo,'error'=>false);
  }

  function get_Curriculum(){
	$col = "c.IndexID
		   ,c.CurriculumCode
		   ,c.ProgramID
		   ,c.MajorID
		   ,RegularMinLoad
		   ,SummerMinLoad
		   ,RegularMaxLoad
		   ,SummerMaxLoad
		   ,cd.IndexID as CurriculumIndexID
		   ,cd.YearTermID
		   ,y.YearLevelID
		   ,cd.SubjectID
		   ,s.SubjectCode
		   ,s.SubjectTitle
		   ,cd.SortOrder
		   ,cd.SubjectElective
		   ,cd.SubjectMajor
		   ,s.AcadUnits
		   ,s.LabUnits
		   ,sec.SectionID
		   ,sec.SectionName
		   ,cs.ScheduleID
		   ,Time1_Start
		   ,Time1_End
		   ,Days1
		   ,Time2_Start
		   ,Time2_End
		   ,Days2
		   ,Time3_Start
		   ,Time3_End
		   ,Days3
		   ,Time4_Start
		   ,Time4_End
		   ,Days4
		   ,Sched_1
		   ,Sched_2
		   ,Sched_3
		   ,Sched_4";
	$qry = "SELECT ".$col."
				   ,'01' as xSortOrder
			   FROM ES_Curriculums as c
		 INNER JOIN ES_CurriculumDetails as cd ON c.IndexID=cd.CurriculumID
		 INNER JOIN ES_Subjects as s ON cd.SubjectID=s.SubjectID
		 INNER JOIN ES_YearTerms as y ON cd.YearTermID=y.IndexID
		  LEFT JOIN ES_ClassSections as sec ON c.IndexID=sec.CurriculumID  AND sec.TermID=".$this->termid." AND sec.YearLevelID=".(($this->yrlvlid==5)?1:'sec.YearLevelID')."
		  LEFT JOIN ES_ClassSchedules as cs ON cd.SubjectID=cs.SubjectID AND sec.SectionID=cs.SectionID AND cs.TermID=".$this->termid." AND cs.IsDissolved=0
			  WHERE c.IndexID = ".$this->curriculumid." AND YearTermID<>22
		 UNION ALL
			 SELECT ".$col."
				   ,'02' as xSortOrder
			   FROM ES_Curriculums as c
		 INNER JOIN ES_CurriculumDetails as cd ON c.IndexID=cd.CurriculumID
		 INNER JOIN ES_Subjects as s ON cd.SubjectID=s.SubjectID
		 INNER JOIN ES_YearTerms as y ON cd.YearTermID=y.IndexID
		  LEFT JOIN ES_ClassSections as sec ON c.IndexID=sec.CurriculumID  AND sec.TermID=".$this->termid."
		  LEFT JOIN ES_ClassSchedules as cs ON cd.SubjectID=cs.SubjectID AND sec.SectionID=cs.SectionID AND cs.TermID=".$this->termid." AND cs.IsDissolved=0
			  WHERE c.IndexID = ".$this->curriculumid." AND YearTermID=22
		   ORDER BY xSortOrder,YearTermID";
    $data = DB::select($qry);
    if(!$data){return array('success'=>false,'content'=>'','error'=>DB::getQueryLog()); }
	$i = 1;
	foreach($data as $c){
	   $tmpindx   = $c->CurriculumIndexID;
	   $tmpschd   = $c->ScheduleID;
	   $tmpsubj   = $c->SubjectID;
	   $arr_data  = array();
	   $arr_sched = array();
	   $arr_offer = array();
	   $arr_data['CurriculumIndxID'] = $c->CurriculumIndexID;
	   $arr_data['ProgID']           = $c->ProgramID;
	   $arr_data['MajorID']          = $c->MajorID;
	   $arr_data['YearTermID']       = $c->YearTermID;
	   $arr_data['YearLevelID']      = $c->YearLevelID;
	   $arr_data['SubjectID']        = $c->SubjectID;
	   $arr_data['SubjectCode']      = $c->SubjectCode;
	   $arr_data['SubjectTitle']     = $c->SubjectTitle;
	   $arr_data['SubjectElective']  = $c->SubjectElective;
	   $arr_data['SubjectMajor']     = $c->SubjectMajor;
	   $arr_data['AcadUnits']        = $c->AcadUnits;
	   $arr_data['LabUnits']         = $c->LabUnits;
	   $arr_data['SortOrder']        = $i;
	   $this->subj_curr[$tmpindx]    = $arr_data;
	   if($tmpschd!=''){
		 $arr_schd['ScheduleID']     = $c->ScheduleID;
         $arr_schd['SubjectID']      = $c->SubjectID;
         $arr_schd['SectionID']      = $c->SectionID;
         $arr_schd['SectionName']    = $c->SectionName;
         $arr_schd['Sched1']         = $c->Sched_1;
         $arr_schd['Sched2']         = $c->Sched_2;
         $arr_schd['Sched3']         = $c->Sched_3;
         $arr_schd['Sched4']         = $c->Sched_4;
		 $this->subj_sched[$tmpschd] = $arr_schd;
		 $this->subj_offer[$tmpsubj] = array_merge($arr_data,$arr_schd);
       }
	   $i++;
    }
    return array('success'=>true,'content'=>$this->subj_sched,'error'=>false);
  }

  function get_Prereq_Equiv(){
	 $data   = DB::select("SELECT IndexID as RefID
	                             ,CurriculumIndexID as IndexID
			                     ,SubjectID_Curriculum as SubjectID
			                     ,pre.SubjectID as EquivalentID
			                     ,SubjectCode as EquivalentCode
			                     ,SubjectTitle as EquivalentTitle
			                     ,Options
                             FROM ES_PreRequisites as pre
                       INNER JOIN ES_Subjects as s ON pre.SubjectID=s.SubjectID
                            WHERE CurriculumIndexID IN (SELECT IndexID FROM ES_CurriculumDetails WHERE CurriculumID=".$this->curriculumid.")");
     foreach($data as $e){
		 $tmpsubj  = $e->SubjectID;
		 $tmpequiv = $e->EquivalentID;
         if($e->Options=='Pre-Requisite')
           $this->subj_prereq[$tmpsubj][$tmpequiv] = ((in_array($tmpequiv,$this->subj_taken) && ($this->subj_taken[$tmpequiv]['Remarks']=='Passed' || $this->subj_taken[$tmpequiv]['Remarks']=='Credited'))?1:0);
         else if($e->Options=='Equivalent'){
		   $this->subj_equiva[$tmpsubj][$tmpequiv] = $e->Options;
		 }
     }

     return $this->subj_prereq;
  }

  function check_PreRequisite($subjid){
	  $result = true;
	  if(array_key_exists($subjid,$this->subj_prereq)){
		foreach($this->subj_prereq[$subjid] as $k=>$v){
		  if(array_key_exists($k,$this->subj_taken) && ($this->subj_taken[$k]['Remarks']=='Passed' || $this->subj_taken[$k]['Remarks']=='Credited'))
			$result = (($result==false)?false:true);
		  else
			$result = false;
		}
	  }
	  return $result;
  }

  function exec_AutoAdvise($studno=''){
	  if($this->isadvised==0)
		return $this->get_AutoAdvise($studno);
	  else{
		//if($this->readvised==1)
		  return $this->get_AutoAdvise($studno);
	    //else
	    //   return $this->get_AdviseSubj($studno);
	  }
  }

  function get_AutoAdvise($studno){
	$result   = array();
	$advdata  = array();
	$tmpunits = 0;
	$count    = 0;

	foreach($this->subj_curr as $k=>$v){
	  $subjid = $v['SubjectID'];
	  if(@array_key_exists($subjid,$this->subj_offer) && $this->check_PreRequisite($subjid)){
		  if(!array_key_exists($subjid,$this->subj_taken)){
		   $result[$k]                = $v;
		   $result[$k]['ScheduleID']  = 0;
		   $result[$k]['SectionID']   = 0;
		   $result[$k]['SectionName'] = '';
		   $result[$k]['Time1_Start'] = '';
		   $result[$k]['Time1_End']   = '';
		   $result[$k]['Days1']       = '';
		   $result[$k]['Sched1']      = '';
		   $result[$k]['Time2_Start'] = '';
		   $result[$k]['Time2_End']   = '';
		   $result[$k]['Days2']       = '';
		   $result[$k]['Sched2']      = '';
		   $result[$k]['Time3_Start'] = '';
		   $result[$k]['Time3_End']   = '';
		   $result[$k]['Days3']       = '';
		   $result[$k]['Sched3']      = '';
		   $result[$k]['Time4_Start'] = '';
		   $result[$k]['Time4_End']   = '';
		   $result[$k]['Days4']       = '';
		   $result[$k]['Sched4']      = '';
		   $advdata[$count]           = array('SubjectID'          => $subjid
		                                      ,'SeqNo'             => $count
											  ,'ElectiveSubjectID' => 0
											  ,'ScheduleID'        => 0);
		   $tmpunits   = $tmpunits + floatval($v['AcadUnits']);
           $count++;
		 }else if(@array_key_exists($subjid,$this->subj_taken) &&($this->subj_taken[$subjid]['Remarks']!='' && ($this->subj_taken[$subjid]['Remarks']!='Passed' || $this->subj_taken[$subjid]['Remarks']!='Credited'))){
		   $result[$k]                = $v;
		   $result[$k]['ScheduleID']  = 0;
		   $result[$k]['SectionID']   = 0;
		   $result[$k]['SectionName'] = '';
		   $result[$k]['Time1_Start'] = '';
		   $result[$k]['Time1_End']   = '';
		   $result[$k]['Days1']       = '';
		   $result[$k]['Sched1']      = '';
		   $result[$k]['Time2_Start'] = '';
		   $result[$k]['Time2_End']   = '';
		   $result[$k]['Days2']       = '';
		   $result[$k]['Sched2']      = '';
		   $result[$k]['Time3_Start'] = '';
		   $result[$k]['Time3_End']   = '';
		   $result[$k]['Days3']       = '';
		   $result[$k]['Sched3']      = '';
		   $result[$k]['Time4_Start'] = '';
		   $result[$k]['Time4_End']   = '';
		   $result[$k]['Days4']       = '';
		   $result[$k]['Sched4']      = '';
		   $advdata[$count]           = array('SubjectID'          => $subjid
		                                      ,'SeqNo'             => $count
											  ,'ElectiveSubjectID' => 0
											  ,'ScheduleID'        => 0);
		   $tmpunits   = $tmpunits + floatval($v['AcadUnits']);
           $count++;
		 }else{
		   $result[$k]                = $v;
		 }
	  }
    }

	if(count($result)>0){
	 $advise = $this->make_Advised($studno,$advdata);
	 $this->studinfo['AdvisedID'] = $advise;
	}
	return $result;
  }

  function make_Advised($studno,$advdata=array()){
	$advfields = array(
           'TermID'          => $this->termid
          ,'CampusID'        => 1
          ,'CollegeID'       => 6
          ,'ProgID'          => $this->progid
          ,'StudentNo'       => $studno
          ,'YearLevel'       => $this->yrlvlid
          ,'MinLoad'         => $this->minload
          ,'MaxLoad'         => $this->maxload
          ,'AdvisedSubject'  => count($advdata)
          ,'AdvisedUnits'    => 0
          ,'AdviserID'       => 0
          ,'DateAdvised'     => date('Y-m-d')
          ,'AccessCode'      => ''
          ,'FeesID'          => ''
    );

	$clean = DB::statement("DELETE FROM ES_Advising_Details WHERE AdvisedID=(SELECT TOP 1 AdvisedID FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$this->termid.")
                            DELETE FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$this->termid);
	$advid = DB::select("SELECT TOP 1 AdvisedID FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$this->termid);
	if($advid){
	  $advid = $advid[0]->AdvisedID;
	}else{
	  $advid = DB::table('ES_Advising')->insertGetId($advfields,'AdvisedID');
	  foreach($advdata as $k=>$v){
		$v['AdvisedID'] = $advid;
		$advdtl         = DB::table('ES_Advising_Details')->insert($v);
	  }
	}

	return $advid;
  }

  function get_AdviseSubj($studno){
	 $result = array();
	 $query  = '';
	 if($this->regid=='' || $this->regid=='0'){
	  $query = "SELECT r.StudentNo
			          ,r.RegID
				      ,r.TermID
				      ,r.CampusID
				      ,r.ProgID
				      ,'".$this->strand."' as MajorID
				      ,rd.EntryID as ReferenceID
				      ,rd.ScheduleID
				      ,s.SubjectID
				      ,s.SubjectCode
				      ,s.SubjectTitle
				      ,s.AcadUnits
				      ,s.LabUnits
				      ,sec.SectionID
				      ,sec.SectionName
				      ,cs.Time1_Start
				      ,cs.Time1_End
				      ,cs.Days1
				      ,cs.Time2_Start
				      ,cs.Time2_End
				      ,cs.Days2
				      ,cs.Time3_Start
				      ,cs.Time3_End
				      ,cs.Days3
				      ,cs.Time4_Start
				      ,cs.Time4_End
				      ,cs.Days4
				      ,cs.Sched_1
				      ,cs.Sched_2
				      ,cs.Sched_3
				      ,cs.Sched_4
			      FROM ES_Advising as r
			INNER JOIN ES_Advising_Details as rd ON r.AdvisedID  = rd.AdvisedID
			INNER JOIN ES_ClassSchedules as cs ON rd.ScheduleID = cs.ScheduleID
			INNER JOIN ES_ClassSections as sec ON cs.SectionID  = sec.SectionID
			INNER JOIN ES_Subjects as s ON cs.SubjectID = s.SubjectID
			WHERE r.StudentNo='".$studno."' AND r.TermID=".$this->termid;
	 }else{
	  $query = "SELECT  r.StudentNo
					   ,r.RegID
					   ,r.TermID
					   ,r.CampusID
					   ,r.ProgID
					   ,r.MajorID
					   ,rd.ReferenceID
					   ,rd.ScheduleID
					   ,s.SubjectID
					   ,s.SubjectCode
					   ,s.SubjectTitle
					   ,s.AcadUnits
					   ,s.LabUnits
					   ,sec.SectionID
					   ,sec.SectionName
					   ,cs.Time1_Start
					   ,cs.Time1_End
					   ,cs.Days1
					   ,cs.Time2_Start
					   ,cs.Time2_End
					   ,cs.Days2
					   ,cs.Time3_Start
					   ,cs.Time3_End
					   ,cs.Days3
					   ,cs.Time4_Start
					   ,cs.Time4_End
					   ,cs.Days4
					   ,cs.Sched_1
					   ,cs.Sched_2
					   ,cs.Sched_3
					   ,cs.Sched_4
				   FROM ES_Registrations as r
			 INNER JOIN ES_RegistrationDetails as rd ON r.RegID  = rd.RegID
			 INNER JOIN ES_ClassSchedules as cs ON rd.ScheduleID = cs.ScheduleID
			 INNER JOIN ES_ClassSections as sec ON cs.SectionID  = sec.SectionID
			 INNER JOIN ES_Subjects as s ON cs.SubjectID = s.SubjectID
				  WHERE r.StudentNo='".$studno."' AND r.TermID=".$this->termid;
	}
	$data   = DB::select($query);
	foreach($data as $a){
		$tmpid                    = $a->ReferenceID;
		$arr_data                 = array();
		$arr_data['SubjectID']    = $a->SubjectID;
		$arr_data['SubjectCode']  = $a->SubjectCode;
		$arr_data['SubjectTitle'] = $a->SubjectTitle;
		$arr_data['AcadUnits']    = $a->AcadUnits;
		$arr_data['LabUnits']     = $a->LabUnits;
		$arr_data['ScheduleID']   = $a->ScheduleID;
		$arr_data['SectionID']    = $a->SectionID;
		$arr_data['SectionName']  = $a->SectionName;
		$arr_data['Sched1']       = $a->Sched_1;
		$arr_data['Sched2']       = $a->Sched_2;
		$arr_data['Sched3']       = $a->Sched_3;
		$arr_data['Sched4']       = $a->Sched_4;
		$result[$tmpid]           = $arr_data;
	}
	return (($data)?$result:array());
  }

  function get_AvailableSchedule($termid,$progid,$subjid){
	 $result = false;
	 $data   = DB::select("SELECT cs.ScheduleID
							     ,s.SectionID
							     ,s.SectionName
							     ,(CASE WHEN cs.Limit=0 THEN s.Limit ELSE cs.Limit END) as Limit
							     ,(SELECT COUNT(*) FROM ES_RegistrationDetails WHERE ScheduleID=cs.ScheduleID) as Registered
							     ,(SELECT COUNT(*) FROM ES_Advising_Details WHERE ScheduleID=cs.ScheduleID) as Advised
							     ,Time1_Start
							     ,Time1_End
							     ,Days1
							     ,Sched_1 as Sched1
							     ,Time2_Start
							     ,Time2_End
							     ,Days2
							     ,Sched_2 as Sched2
							     ,Time3_Start
							     ,Time3_End
							     ,Days3
							     ,Sched_3 as Sched3
							     ,Time4_Start
							     ,Time4_End
							     ,Days4
							     ,Sched_4 as Sched4
							FROM ES_ClassSchedules as cs
					  INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
						   WHERE cs.SubjectID='".$subjid."' AND cs.TermID=".$termid." AND s.ProgramID=".$progid);
	$result  = (($data)?"":false);
	foreach($data as $s){
	  if($s->Limit>$s->Registered){
		  $result .= '<tr data-id="'.$s->ScheduleID.'" data-section="'.$s->SectionID.'">
						<td width="10px;"><label><input type="checkbox" value="'.$s->ScheduleID.'"/></label></td>
						<td>'.$s->SectionName.'</td>
						<td>'.$s->Sched1.'</td>
					 </tr>';
	  }
	}
	return $result;
  }

  function set_AdvisedSchedule($termid,$studno,$progid,$strand,$yrlvl,$curr,$subjid,$schedid,$section,$isblock){
	$advdtl = DB::select("SELECT TOP 1 AdvisedID FROM ES_Advising WHERE TermID=".$termid." AND StudentNo='".$studno."'");
	$result = false;
	if($advdtl){
	  $advid = $advdtl[0]->AdvisedID;
	  if($isblock==1){
		$saved = DB::statement("UPDATE d SET d.ScheduleID=cs.ScheduleID FROM ES_Advising_Details as d INNER JOIN ES_ClassSchedules as cs ON d.SubjectID=cs.SubjectID AND cs.SectionID=".$section." WHERE d.AdvisedID=(SELECT TOP 1 AdvisedID FROM ES_Advising WHERE TermID=".$termid." AND StudentNo='".$studno."')");
	  }else{
	    $saved = DB::statement("UPDATE ES_Advising_Details SET ScheduleID=".$schedid." WHERE AdvisedID=(SELECT TOP 1 AdvisedID FROM ES_Advising WHERE TermID=".$termid." AND StudentNo='".$studno."') AND SubjectID=".$subjid);
	  }

	  $result = DB::select("SELECT d.SubjectID
								  ,d.ScheduleID
								  ,cs.SectionID
								  ,s.SectionName
								  ,Time1_Start
								  ,Time1_End
								  ,Days1
								  ,Sched_1 as Sched1
								  ,Time2_Start
								  ,Time2_End
								  ,Days2
								  ,Sched_2 as Sched2
								  ,Time3_Start
								  ,Time3_End
								  ,Days3
								  ,Sched_3 as Sched3
								  ,Time4_Start
								  ,Time4_End
								  ,Days4
								  ,Sched_4 as Sched4
							  FROM ES_Advising_Details as d
						INNER JOIN ES_ClassSchedules as cs ON d.ScheduleID=cs.ScheduleID
						INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
		                     WHERE d.AdvisedID=".$advid);
	}
	return $result;
  }

  function gen_StudentNo($appno,$yrlvl=5,$curr=0){
	  $qry = "INSERT INTO ES_Students
                         (StudentNo,AppNo,
                          CampusID,TermID,
                          ProgID,YearLevelID,MajorDiscID,CurriculumID,
                          LastName,FirstName,Middlename,MiddleInitial,ExtName,
                          Fullname ,
                          DateOfBirth,PlaceOfBirth,Gender,
                          CivilStatusID,ReligionID,NationalityID ,
                          TelNo,MobileNo,Email ,
                          Res_Address,Res_Street,Res_Barangay,Res_TownCity,Res_Province,Res_ZipCode,
                          Perm_Address,Perm_Street,Perm_Barangay,Perm_TownCity,Perm_Province,Perm_ZipCode ,
                          Father,Father_Occupation,Father_Company,Father_CompanyAddress,Father_TelNo,Father_Email,
                          Mother,Mother_Occupation,Mother_Company,Mother_CompanyAddress,Mother_TelNo,Mother_Email,
                          Guardian,Guardian_Relationship,Guardian_Address,Guardian_Street,Guardian_Barangay,Guardian_TownCity,Guardian_Province,Guardian_ZipCode ,
                          Guardian_Occupation ,Guardian_Company ,
                          Guardian_TelNo,Guardian_Email,
                          Elem_School,Elem_Address,Elem_InclDates,
                          HS_School,HS_Address,HS_InclDates,
                          StudentPicture,FamilyID)
	          SELECT TOP 1
		             StudentNo,AppNo,
		             CampusID,TermID ,
		             ProgramID,".$yrlvl." as GradeLevelID,MajorID,".$curr." AS CurriculumID,
		             LastName,FirstName,MiddleName,MiddleInitial,ExtName,
		             Fullname ,
		             DateOfBirth,PlaceOfBirth,Gender,
		             CivilStatusID,ReligionID,NationalityID,
		             Guardian_TelNo,Guardian_Mobile,Guardian_Email ,
		             Guardian_Address,
		             Guardian_Street,Guardian_Barangay,Guardian_TownCity,Guardian_Province,Guardian_ZipCode,
		             Guardian_Address ,
		             Guardian_Street,Guardian_Barangay,Guardian_TownCity,Guardian_Province,Guardian_ZipCode,
		             Father_Name,Father_Occupation,Father_Company,Father_CompanyAddress,Father_TelNo,Father_Email ,
		             Mother_Name,Mother_Occupation,Mother_Company,Mother_CompanyAddress,Mother_TelNo,Mother_Email ,
		             Guardian_Name,'' as Guardian_RelationshipOthers ,
		             Guardian_Address,Guardian_Street,Guardian_Barangay,Guardian_TownCity,Guardian_Province,Guardian_ZipCode ,
		             '' AS Guardian_Occupation,'' AS Guardian_Company,Guardian_TelNo,Guardian_Email,
		             Elem_School,Elem_Address,Elem_InclDates ,
		             HS_School ,HS_Address ,HS_InclDates ,
		             Photo ,FamilyID
                FROM
                (SELECT dbo.fn_K12_GenerateSN(1,TermID,15) as StudentNo,AppNo ,
                        CampusID,TermID ,
		                ProgramID,".$yrlvl." as GradeLevelID,MajorID,".$curr." AS CurriculumID,
		                LastName,FirstName,MiddleName,MiddleInitial,ExtName,
                        CONCAT(UPPER(LastName),',',FirstName,' ',MiddleInitial) AS Fullname ,
		                DateOfBirth,PlaceOfBirth,Gender,
		                CivilStatusID,ReligionID,NationalityID,
		                fb.Guardian_Name,fb.Guardian_TelNo ,fb.Guardian_Mobile,fb.Guardian_Email,
						fb.Guardian_Address,fb.Guardian_Street,fb.Guardian_Barangay,fb.Guardian_TownCity,fb.Guardian_Province,fb.Guardian_ZipCode,
                        '' AS Guardian_Occupation ,'' AS Guardian_Company ,
                        fb.Father_Name,fb.Father_Occupation,fb.Father_Company,fb.Father_CompanyAddress,fb.Father_TelNo,fb.Father_Email ,
                        fb.Mother_Name,fb.Mother_Occupation,fb.Mother_Company,fb.Mother_CompanyAddress,fb.Mother_TelNo,fb.Mother_Email ,
						Elem_School,Elem_Address,Elem_InclDates,
						HS_School,HS_Address,HS_InclDates ,
						Photo,adm.FamilyID
				   FROM ESv2_Admission AS adm
				  INNER JOIN ESv2_Admission_FamilyBackground AS fb ON adm.FamilyID = fb.FamilyID
                  UNION ALL
                 SELECT dbo.fn_K12_GenerateSN(1,TermID,15) as StudentNo,AppNo,
	                    Choice1_CampusID,TermID,
	                    Choice1_Course as ProgramID,5 as GradeLevelID,Choice1_CourseMajor as MajorID,0 AS CurriculumID,
	                    LastName,FirstName,MiddleName,MiddleInitial,ExtName,
                        CONCAT(UPPER(LastName),',',FirstName,' ',MiddleInitial) AS Fullname ,
                        DateOfBirth,PlaceOfBirth,Gender,
		                CivilStatusID,ReligionID,NationalityID,
		                Guardian as Guardian_Name,Guardian_TelNo,'' as Guardian_Mobile,Guardian_Email,
						Guardian_Address,'' as Guardian_Street,'' as Guardian_Barangay,'' as Guardian_TownCity,'' as Guardian_Province,'' as Guardian_ZipCode,
                        '' AS Guardian_Occupation ,'' AS Guardian_Company ,
                        Father as Father_Name,Father_Occupation,Father_Company,Father_CompanyAddress,Father_TelNo,Father_Email,
                        Mother as Mother_Name,Mother_Occupation,Mother_Company,Mother_CompanyAddress,Mother_TelNo,Mother_Email,
                        Elem_School,Elem_Address,Elem_InclDates,
                        HS_School,HS_Address,HS_InclDates ,
                        Photo,'' as FamilyID
                   FROM ES_Admission
                ) as a
            WHERE AppNo = '".$appno."' AND a.AppNo NOT IN (SELECT AppNo FROM ES_Students)";
	$exec  = DB::statement($qry);
    return $exec;
  }

  function exec_Register($termid,$studno,$strand,$curr,$yrlvl){
	 $regid = 0;

     if(strlen($studno)==10 && substr($studno,4,3)==200){

       $totalunit = 25;

       $exec    = DB::statement("INSERT INTO ES_Registrations(StudentNo,RegDate,TermID,CampusID,AdvisingOfficerID,AssessedFees,TableofFeeID,ClassSectionID,YearLevelID,CollegeID,ProgID,MajorID,IsOnlineEnrolment,TotalLecUnits)
	                                SELECT TOP 1 StudentNo,'".date('Y-m-d')."',TermID,CampusID,'admin','0.00',0,0,".$yrlvl.",CollegeID,ProgID,'".$strand."',1,'".$totalunit."' FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$termid);
	   $xstudno = $studno;
	 }else{
	   $xstudno = $this->gen_StudentNo($studno,$yrlvl,$curr);
	   $xstudno = DB::select("SELECT TOP 1 StudentNo FROM ES_Students WHERE AppNo='".$studno."'")[0]->StudentNo;
	   $exec    = DB::statement("INSERT INTO ES_Registrations(StudentNo,RegDate,TermID,CampusID,AdvisingOfficerID,AssessedFees,TableofFeeID,ClassSectionID,YearLevelID,CollegeID,ProgID,MajorID,IsOnlineEnrolment)
	                                SELECT TOP 1 '".$xstudno."','".date('Y-m-d')."',TermID,CampusID,'admin','0.00',0,0,".$yrlvl.",CollegeID,ProgID,'".$strand."',1 FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$termid);
	 }

	 if($exec){
		$getreg = DB::select("SELECT TOP 1 RegID FROM ES_Registrations WHERE StudentNo='".$xstudno."' AND TermID=".$termid);
		if($getreg){
		   $regid  = $getreg[0]->RegID;
           $update = DB::statement("UPDATE s SET s.ProgID = r.ProgID,s.MajorDiscID = r.MajorID,s.YearLevelID = r.YearLevelID,s.CurriculumID=".$curr." FROM ES_Students as s INNER JOIN ES_Registrations as r ON s.StudentNo = r.StudentNo AND r.TermID=".$termid." WHERE s.StudentNo='".$studno."'");
		   $exec   = DB::statement("INSERT INTO ES_RegistrationDetails(RegID,ScheduleID,RegTagID,SeqNo)
                                   SELECT '".$regid."',ScheduleID,0,SeqNo FROM ES_Advising_Details WHERE ScheduleID<>0 AND AdvisedID IN (SELECT TOP 1 AdvisedID FROM ES_Advising WHERE StudentNo='".$studno."' AND TermID=".$termid.")");
	  	   return $exec;
		}
	 }
     return false;
  }

}
?>