<?php
namespace App\Modules\Enrollment\Services\Registration;
use DateTime;
use App\Modules\Setup\Models\GradingSystemSetup As mSetup;
use App\Modules\Students\Services\StudentProfileServiceProvider as studService;

use DB;
Class Registration 
{
	public function admission_to_student($appno=false){
	   if($appno){
		   $qry   = "INSERT  INTO ES_Students ( StudentNo,AppNo,CampusID,TermID,ProgID,YearLevelID,MajorDiscID,CurriculumID,LastName,FirstName,Middlename,MiddleInitial,ExtName,
								                Fullname,DateOfBirth,PlaceOfBirth,Gender,CivilStatusID,ReligionID,NationalityID,TelNo,MobileNo,Email,
								                Res_Address,Res_Street,Res_Barangay,Res_TownCity,Res_Province,Res_ZipCode,
												Perm_Address,Perm_Street,Perm_Barangay ,Perm_TownCity ,Perm_Province,Perm_ZipCode ,
								  Father ,
								  Father_Occupation ,
								  Father_Company ,
								  Father_CompanyAddress ,
								  Father_TelNo ,
								  Father_Email ,
								  Mother ,
								  Mother_Occupation ,
								  Mother_Company ,
								  Mother_CompanyAddress ,
								  Mother_TelNo ,
								  Mother_Email ,
								  Guardian ,
								  Guardian_Relationship ,
								  Guardian_Address ,
								  Guardian_Street ,
								  Guardian_Barangay ,
								  Guardian_TownCity ,
								  Guardian_Province ,
								  Guardian_ZipCode ,
								  Guardian_Occupation ,
								  Guardian_Company ,
								  Guardian_TelNo ,
								  Guardian_Email ,
								  Elem_School ,
								  Elem_Address ,
								  Elem_InclDates ,
								  HS_School ,
								  HS_Address ,
								  HS_InclDates ,
								  StudentPicture ,
								  FamilyID
								)
								SELECT TOP 1
										dbo.fn_K12_GenerateSN(CampusID, TermID, GradeLevelID),
										AppNo ,
										CampusID ,
										TermID ,
										ProgramID ,
										GradeLevelID,
										MajorID ,
										0 AS CurriculumID ,
										LastName ,
										FirstName ,
										MiddleName ,
										MiddleInitial ,
										ExtName ,
										CONCAT(UPPER(LastName), ', ', FirstName, ' ',MiddleInitial) AS Fullname ,
										DateOfBirth ,
										PlaceOfBirth ,
										Gender ,
										CivilStatusID ,
										ReligionID ,
										NationalityID ,
										fb.Guardian_TelNo ,
										fb.Guardian_Mobile ,
										fb.Guardian_Email ,
										fb.Guardian_Address ,
										fb.Guardian_Street ,
										fb.Guardian_Barangay ,
										fb.Guardian_TownCity ,
										fb.Guardian_Province ,
										fb.Guardian_ZipCode ,
										fb.Guardian_Address ,
										fb.Guardian_Street ,
										fb.Guardian_Barangay ,
										fb.Guardian_TownCity ,
										fb.Guardian_Province ,
										fb.Guardian_ZipCode ,
										fb.Father_Name ,
										fb.Father_Occupation ,
										fb.Father_Company ,
										fb.Father_CompanyAddress ,
										fb.Father_TelNo ,
										fb.Father_Email ,
										fb.Mother_Name ,
										fb.Mother_Occupation ,
										fb.Mother_Company ,
										fb.Mother_CompanyAddress ,
										fb.Mother_TelNo ,
										fb.Mother_Email ,
										fb.Guardian_Name ,
										fb.Guardian_RelationshipOthers ,
										fb.Guardian_Address ,
										fb.Guardian_Street ,
										fb.Guardian_Barangay ,
										fb.Guardian_TownCity ,
										fb.Guardian_Province ,
										fb.Guardian_ZipCode ,
										'' AS Guardian_Occupation ,
										'' AS Guardian_Company ,
										fb.Guardian_TelNo ,
										fb.Guardian_Email ,
										Elem_School ,
										Elem_Address ,
										Elem_InclDates ,
										HS_School ,
										HS_Address ,
										HS_InclDates ,
										Photo ,
										a.FamilyID
								FROM    ESv2_Admission AS a
						   INNER JOIN   ESv2_Admission_FamilyBackground AS fb ON a.FamilyID = fb.FamilyID
								WHERE   AppNo = '".$appno."' AND StatusID=2
										AND a.AppNo NOT IN ( SELECT AppNo FROM   ES_Students )";
		 $exec = DB::statement($qry);
	   }
       return true;	   
	}
	
	public function get_xdetail($term=0,$campus=0,$yrlvl=0,$option=0,$where=false,$order=false,$offset=0,$limit=500)
	{
	 $useoldlvl = 1; 
	 $col_order = 'StudentName';
     $col_acdc  = 'ASC';	 
	 if(is_array($order))
	 {
	   $column = array(0=>'StudentNo', 1=>'StudentNo',2=>'StudentName',6=>'Gender',7=>'BirthDate',8=>'BirthDate',10=>'Status',11=>'Balance',12=>'Accounts');
	   foreach($order as $arr)
	   {
		 $indx      = ((array_key_exists('column',$arr))?$arr['column']:0);
		 $col_order = ((array_key_exists($indx,$column))?$column[$indx]:$column[0]);
		 $col_acdc  = ((array_key_exists('dir',$arr))?$arr['dir']:'asc');
		 $col_acdc  = strtoupper($col_acdc);
	   }	   
	 }
	 
	 DB::enableQueryLog();
	 $data = DB::select("EXEC sp_K12_Enrollment @TermID='".$term."',@CampusID='".$campus."',@LevelID='".$yrlvl."',@Option='".$option."'".(($where)?",@Where='".$where."'":"").",@Order='".$col_order."',@ACDC='".$col_acdc."',@Limit='".$limit."',@Offset='".$offset."',@UseOldLvl=".(($useoldlvl)?1:0));
	
	 //var_dump("EXEC sp_K12_Enrollment @TermID='".$term."',@CampusID='".$campus."',@LevelID='".$yrlvl."',@Option='".$option."'".(($where)?",@Where='".$where."'":"").",@Order='".$col_order."',@ACDC='".$col_acdc."',@Limit='".$limit."',@Offset='".$offset."',@UseOldLvl=".(($useoldlvl)?1:0));
	 //();

     return array('output'=>(($data)?$data:false),'query'=>DB::getQueryLog()); 
	}
    
	public function get_xcount($term=0,$campus=0,$yrlvl=0,$option=0,$where=false)
	{
	 DB::enableQueryLog();
	 $count = DB::select("EXEC sp_K12_Enrollment @TermID='".$term."',@CampusID='".$campus."',@LevelID='".$yrlvl."',@Option='".$option."',@Count=1".(($where)?",@Where='".$where."'":""));
	 if($count)
     {
	  foreach($count as $rs)
      {
	    return ((property_exists($rs,'Count'))? $rs->Count : 0);  
	  }	  
	 }
     else	 
      return 0; 
	}
	
	public function get_registered($data='',$order=false,$offset=0,$limit=500)
	{
	 DB::enableQueryLog();
     $result    = false;
	 $count     = 0;
	 $where     = '';
	 $str_order = '';
	 $useoldlvl = env('USEOLDLVL',true);
	 $campus = ((is_array($data) && array_key_exists('campus',$data))?$data['campus']:1);
	 $SYTerm = ((is_array($data) && array_key_exists('termid',$data))?$data['termid']:$this->get_SY(1,'TermID'));
	 $default_field = "dbo.fn_StudentName(s.StudentNo) as xFullName
					  ,dbo.fn_Nationality(NationalityID) as Nationality
					  ,dbo.fn_RegStudentStatus(s.TermID,'".$SYTerm."') as Status
	                  ,r.RegID as RegID
					  ,r.RegDate as RegDate
					  ,s.StudentNo as StudentNo
					  ,s.Gender as Gender
					  ,dbo.fn_Age(s.DateOfBirth,GETDATE()) as Age
					  ,r.YearLevelID as YearLevelID
					  ,r.MajorID
                      ,dbo.fn_MajorName(r.MajorID) As MajorName
					  ,r.ProgID
					  ,dbo.fn_YearLevel_k12(r.YearLevelID, dbo.fn_ProgramClassCode(r.ProgID)) as YearLevel
					  ,r.TableofFeeID as TemplateID
					  ,dbo.fn_TemplateCode(r.TableofFeeID) as TemplateCode
					  ,r.BillingRefNo as BillingRefNo
					  ,r.ValidationDate as ValidationDate
					  ,r.ValidatingOfficerID as ValidatingOfficerID";
	 
	 if(is_array($order))
	 {
	   $column = array(0=>'RegID',1=>'RegID',2=>'RegDate',3=>'StudentNo',4=>'xFullName',5=>'Gender',6=>'Age',7=>'YearLevel',9=>'Status',10=>'TemplateCode',12=>'ValidationDate');
	   foreach($order as $arr)
       {
		$indx = ((array_key_exists('column',$arr))?$arr['column']:0);
		$col  = ((array_key_exists($indx,$column))?$column[$indx]:$column[0]);
		$dir  = ((array_key_exists('dir',$arr))?$arr['dir']:'asc');
		$str_order .= (($str_order!='')?',':' ').$col.' '.strtoupper($dir);
	   }	   
	 }
	 
	 if(!is_array($data))
	  $where = 'r.TermID='.$SYTerm;
     else
	 {	 
	  if(array_key_exists('search',$data) &&  trim($data['search'])!='')
	  {
	   $where .= "(s.StudentNo='".$data['search']."' OR r.RegID='".intval($data['search'])."' OR dbo.fn_StudentName(s.StudentNo) LIKE '%".$data['search']."%')"; 
	  }	 
	  if(array_key_exists('yrlvl',$data) && $data['yrlvl']!='-1')
	  {
	   /*
	   $yrlvlinfo = $this->get_yearlvl_info($data['yrlvl']);
	   $progid = 0;
	   $yrlvl  = 0;
	   
	   foreach($yrlvlinfo as $yrdata)
	   {
		$yrlvl     = (($useoldlvl)? $yrdata->Old_YearLevelID : $yrdata->YearLevelID); 
		$yrlvlname = $yrdata->YearLevelName; 
		$progid    = $yrdata->ProgID; 
	   }
	   
	   //$default_field = str_replace("(SELECT TOP 1 YearLevelName FROM ESv2_YearLevel WHERE YearLevelID=r.YearLevelID)","'".$yrlvlname."'",$default_field);
       */
	   //$where .= (($where!='')?' AND ':'')."(r.YearLevelID='".$yrlvl."' AND r.ProgID = ".$progid.")"; 
       $progams = getUserProgramAccess();
       $xprogid = ((array_key_exists('progid', $data))?($data['progid']):0);
       if($xprogid>0){       
        $where .= (($where!='')?' AND ':'')."(r.YearLevelID='".$data['yrlvl']."' AND r.ProgID='".$xprogid."' AND r.ProgID In (". implode(",",$progams) .")) ";
       }else{
       	$where .= (($where!='')?' AND ':'')."(r.YearLevelID='".$data['yrlvl']."' AND r.ProgID In (". implode(",",$progams) .")) ";
       }
	  }
	 
	  $where .= (($where!='')?' AND ':'')."(s.Inactive=0 AND r.TermID='".$SYTerm."' AND r.IsWithdrawal=0)"; 
	 }
	 
	 $rs = DB::select(" SELECT ".(($limit>0 && $offset==0)? " TOP ".$limit:"")." ".$default_field." FROM ES_Students as s 
	                                                                                        INNER JOIN ES_Registrations as r ON s.StudentNo = r.StudentNo".
																							" ".(($where!='')?" WHERE ".$where:" ").
																							" ".(($str_order!='')?" ORDER BY ".$str_order:" ").
																							" ".(($limit>0 && $offset>0)?" OFFSET ".$offset." ROWS FETCH NEXT ".$limit." ROWS ONLY":" "));
	 $rs_count = DB::select("SELECT COUNT(*) as Count FROM ES_Students as s 
											    INNER JOIN ES_Registrations as r ON s.StudentNo = r.StudentNo".
											    " ".(($where!='')?" WHERE ".$where:" ")); 			
     
	 $count=0;
	 foreach($rs_count as $xcount)
     {
	  $count = ((property_exists($xcount,'Count'))?($xcount->Count):0); 
	 }	 
	 
	 return array('count'=>$count,'output'=>(($rs)?$rs:false),'query'=>DB::getQueryLog()); 		
	}
	
	public function generate_select($table)
	{
	 $rs = DB::table($table);
     return (($rs)?($rs->get()):false);
    }
	
	public function generate_children()
	{
	 $SYTerm   = $this->get_SY(1,'TermID');	
	 $PrevTerm = $this->get_SY(2,'TermID');	
	 $rs = DB::select("SELECT DISTINCT 
					          s.StudentNo as StudentNo
						 	 ,r.RegID
							 ,TableOfFeeID
							 ,s.DateOfBirth
							 ,dbo.fn_StudentName(s.StudentNo) as xFullName
							 ,ISNULL(r.TermID,83) as TermID
							 ,ISNULL(r.CampusID,s.CampusID) as CampusID
							 ,s.YearLevelID as YearLevelID
							 ,r.YearLevelID as RegYearLevelID
							 ,r.MajorID  as MajorID
							 ,ISNULL(r.ProgID,s.ProgID) as ProgID
							 ,dbo.fn_Nationality(s.NationalityID) as Nationality
							 ,dbo.fn_RegStudentStatus(s.TermID,".$SYTerm.") as Status
							 ,dbo.ESv2_fn_PrevBalancePerTerm(s.StudentNo,".$SYTerm.") as Balance
							 ,dbo.ESv2_fn_BalancePerTerm(s.StudentNo,83) as [Current]
							 ,(SELECT TOP 1 Reason FROM ES_StudentAccountabilities WHERE Cleared=0 AND TermID=".$SYTerm." AND StudentNo=s.StudentNo) as Accounts
							 ,dbo.fn_TemplateCode(r.TableofFeeID) as TemplateCode		
  							 ,r.BillingRefNo
						     ,ISNULL(j.TransRefNo,0) as IsPaid
						     ,r.ValidationDate as ValidationDate
					         ,r.ValidatingOfficerID as ValidatingOfficerID
							 ,s.FamilyID	
                             ,a.AppNo							 
						 FROM ES_Students as s
					LEFT JOIN ESv2_Users as u ON s.FamilyID = u.FamilyID
					LEFT JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo AND r.TermID=".$SYTerm."
					LEFT JOIN ESv2_Admission as a ON s.AppNo = a.AppNo
					LEFT JOIN ES_Journals as j ON s.StudentNo=j.IDNo AND CONVERT(VARCHAR(50),r.RegID)=CONVERT(VARCHAR(50),j.TransRefNo)
					WHERE s.ProgID<30 AND u.UserIDX='".getUserID()."'
					  AND (CASE WHEN s.ProgID=7 AND s.YearLevelID=4 AND ISNULL(r.RegID,0)=0 THEN 'FALSE' ELSE 'TRUE' END)='TRUE'
					  AND s.StatusID<6
					ORDER BY s.DateOfBirth DESC"
					);	
	 				
	 return $rs;
	}
	
	public function generate_regarr($data,$tbl='')
	{
	 $result = array();
	 $i=0;
	 if($data)
     {
	  foreach($data as $rs)
      {
	   $regid     = ((property_exists($rs,'RegID'))?($rs->RegID):'');	  
	   $studno    = ((property_exists($rs,'StudentNo'))?($rs->StudentNo):'');	  
	   $name      = ((property_exists($rs,'xFullName'))?($rs->xFullName):'');	  
	   $regdate   = ((property_exists($rs,'RegDate'))?($rs->RegDate):'');
	   $gender    = ((property_exists($rs,'Gender'))?($rs->Gender):'');
	   $age       = ((property_exists($rs,'Age'))?($rs->Age):'');
	   $yrlvlid   = ((property_exists($rs,'YearLevelID'))?($rs->YearLevelID):'');
	   $major     = ((property_exists($rs,'MajorID'))?($rs->MajorID):0);
	   $progid    = ((property_exists($rs,'ProgID'))?($rs->ProgID):0);
	   $yrlvl     = ((property_exists($rs,'YearLevel'))?($rs->YearLevel):'');
	   $nation    = ((property_exists($rs,'Nationality'))?($rs->Nationality):'');
	   $status    = ((property_exists($rs,'Status'))?($rs->Status):'');
	   $feeid     = ((property_exists($rs,'TemplateID'))?($rs->TemplateID):'');
	   $fees      = ((property_exists($rs,'TemplateCode'))?($rs->TemplateCode):'');
	   $bill      = ((property_exists($rs,'BillingRefNo'))?($rs->BillingRefNo):'');
	   $validdate = ((property_exists($rs,'ValidationDate'))?($rs->ValidationDate):'');
	   $validoff  = ((property_exists($rs,'ValidatingOfficerID'))?($rs->ValidatingOfficerID):'');
	    
       $track     = ((property_exists($rs,'MajorName'))?($rs->MajorName):0);
        
	   $class     = '';
	   $isvalid   = 0;
	   
	   if($regdate!='')
	   {
		 $tmpdate = new DateTime($regdate);
         $regdate = $tmpdate->format('Y-m-d');		 
	   }	   
	   if($validdate!='')
	   {
		 $tmpdate = new DateTime($validdate);
         $validdate = $tmpdate->format('Y-m-d');		 
		 $class   = 'info';
		 $isvalid   = 1;
	   }	   
	   
	   if($age!='' || $age!=0)
	    $age = number_format(floatval($age),2,'.',',');  
	   else
        $age ='';		   
	   
	   $btn       = '<div class="checker hover">
					  <span class="" data-id="'.$regid.'" data-stdno="'.$studno.'" data-yrlvl="'.$yrlvlid.'" data-track="'.$major.'" data-prog="'.$progid.'"  data-feeid="'.$feeid.'" data-isvalid="'.$isvalid.'">
						<input type="checkbox" class="chk-child chk-registered"/>
					  </span>
					 </div>';
	   
	   if($studno!='' && $regid!='')
	   {	   
	    $result[$i]=array(
	                    'DT_RowClass'=>$class
						,0  => $btn
						,1  => $regid
						,2  => $regdate
						,3  => '<span class="autofit">'. $studno.'</span>' 
						,4  => '<span class="autofit">'. $name.'</span>' 
						,5  => $gender
						,6  => $age
                        ,7  => '<span class="autofit">'. $track.'</span>'
						,8  => '<span class="autofit">'. $yrlvl.'</span>' 
						,9  => $nation
						,10  => $status
						,11 => '<span class="autofit">'. $fees.'</span>' 
						,12 => $bill
						,13 => $validdate
		               );
	    $i++;		
       }
	   
	  }
	 } 
	 return $result;
	}
	
	public function generate_arr($data,$tbl='')
	{
	   $service = new studService();
	 $result = array();
	 $i=0;
	 if($data)
     {
	  foreach($data as $rs)
      {
	   $studno    = ((property_exists($rs,'StudentNo'))?($rs->StudentNo):'');	  
	   $appno     = ((property_exists($rs,'AppNo'))?($rs->AppNo):'');	  
	   $name      = ((property_exists($rs,'StudentName'))?($rs->StudentName):'');
	   $yrlvl     = ((property_exists($rs,'YearLevelID'))?($rs->YearLevelID):'');
	   $xyrlvl    = ((property_exists($rs,'NextYearLevelID'))?($rs->NextYearLevelID):'');
	   $prgcls    = ((property_exists($rs,'ProgClass'))?($rs->ProgClass):'');
	   $xprgcls   = ((property_exists($rs,'NextProgClass'))?($rs->NextProgClass):'');
	   $nation    = ((property_exists($rs,'Nationality'))?($rs->Nationality):'');
	   $gender    = ((property_exists($rs,'Gender'))?($rs->Gender):'');
	   $age       = ((property_exists($rs,'Age'))?($rs->Age):'');
	   $birthdate = ((property_exists($rs,'DateOfBirth'))?($rs->DateOfBirth):'');
	   $religion  = ((property_exists($rs,'Religion'))?($rs->Religion):'');
	   $status    = ((property_exists($rs,'Status'))?($rs->Status):'');
	   
       //$balance   = ((property_exists($rs,'Balance'))?($rs->Balance):0);
       $balance = ((property_exists($rs,'Balance'))?($rs->Balance):'');
       
	   $account   = ((property_exists($rs,'Accountabilities'))?($rs->Accountabilities):'');
       $foreign   = ((property_exists($rs,'IsForeign'))?($rs->IsForeign):'');
       $track     = ((property_exists($rs,'StrandTrackID'))?($rs->StrandTrackID):'');
       $prog      = ((property_exists($rs,'NextProgID'))?($rs->NextProgID):'');
       $term      = ((property_exists($rs,'TermID'))?($rs->TermID):'');
	   $btn       = '';
       $class     = ''; 
	   $sel_major = '';
	   $sel_yrlvl = $yrlvl;
	   $allow = '0';
       
       $allow = count($service->registerWithBalance(['TermID' => $term, 'StudentNo' => $studno ]));
       
       
	   if($age!='' && $age>0)
	    $age = number_format($age,2);
       else
        $age = '';

       if($birthdate!='')
       {
		 $tmpdate   = new DateTime($birthdate);
		 $birthdate = $tmpdate->format('m/d/Y'); 
	   }		   
	   
	   if( ($balance<1 && $account=='') || $allow != '0'  ) 
       {
		$btn = '<div class="checker hover">
		          <span class="" data-id="'.$studno.'" data-appno="'.$appno.'" data-status="'.$status.'" data-foreign="'. $foreign .'" data-prog="'.$prog.'">
				    <input type="checkbox" class="chk-child chk-student"/>
				  </span>
				</div>';   
	   }	   
	   else
	    $class='danger';   
	   
	   $sel_yrlvl = $this->dropdown(array('table'    =>'ESv2_YearLevel',
	                                      'col_id'   =>'YearLevelID',
	                                      'col_desc' =>'YearLevelName',
	                                      'name'     =>'nxtlvl',
	                                      'cls'      =>'input-borderless input-small',
	                                      'attr'     =>'data-id="'.$studno.'" data-appno="'.$appno.'" '.(($btn=='')?"disabled":''),
										  'selected' =>$yrlvl,
										  'limit'    =>4,
										  'limit_sel'=>(($xyrlvl==$yrlvl)?0:1),
										  'condition'=>"Inactive=0",
										 )
            );
	   
           
        
            $setup = new mSetup();
        
	 	   $sel_major = $this->dropdown(array('table'     =>'vw_ProgramMajors',
	                                          'col_id'    =>'MajorDiscID',
	                                          'col_desc'  =>'Major',
	                                          'name'      =>'major',
	                                          'cls'       =>'input-borderless input-medium track',
	                                          'attr'      =>'style="width=100%" data-id="'.$studno.'" data-appno="'.$appno.'" '.(($btn=='')?"disabled":''),
										      'selected'  => $track,
										      'limit'     => 0,
										      'condition' =>"ProgID=".$setup->IsSHS(),
										 )
								    );
                               
                                    
	   
	   if($studno!='' || $appno!='')
	   {	   
	    $result[$i]=array(
	                    'DT_RowClass'=>$class
						,0 => $btn
						,1 => $studno
						,2 => '<a href="javascript:void(0);" class="autofit">'.$name.'</a>'
						,3 => $sel_yrlvl
						,4 => $sel_major
						,5 => $nation
						,6 => '<div class="text-center">'.$gender.'</div>'
						,7 => $birthdate
						,8 => $age
						,9 => '<span class="autofit">'.$religion.'</span>'
						,10 => $status
						,11 => '<div class="text-right">'.number_format($balance,2,'.',',').'</div>'
						,12 => ((trim($account)!='')?$account:'')
		               );
	    $i++;		
       }
	  }	  
	 }
	 
	 return $result;
	}
	
	public function get_acadtrack()
	{
     $rs = DB::select('SELECT IndexID,MajorDiscID,MajorCode,MajorName FROM vw_K12_AcademicTrack');
     return $rs;	 
	}
	
	public function get_studentledger($stdno='',$term='')
	{
	 $rs = DB::select("EXEC K12_studentLedger @StudentNo='".$stdno."', @TermID='".$term."'");
     return $rs;	 
	}
	
	public function dropdown($param = array())
	{
	  $table     = ((array_key_exists('table',$param))?$param['table']:'');
	  $col_id    = ((array_key_exists('col_id',$param))?$param['col_id']:'');
	  $col_desc  = ((array_key_exists('col_desc',$param))?$param['col_desc']:'');
	  $name      = ((array_key_exists('name',$param))?$param['name']:'');
	  $cls       = ((array_key_exists('cls',$param))?$param['cls']:'');
	  $attr      = ((array_key_exists('attr',$param))?$param['attr']:'');
	  $selected  = ((array_key_exists('selected',$param))?$param['selected']:'');
	  $init      = ((array_key_exists('init',$param))?$param['init']:'');
	  $init_lbl  = ((array_key_exists('init_lbl',$param))?$param['init_lbl']:'');
	  $limit     = ((array_key_exists('limit',$param))?$param['limit']:0);
	  $limit_sel = ((array_key_exists('limit_sel',$param))?$param['limit_sel']:0);
	  $condition = ((array_key_exists('condition',$param))?$param['condition']:'');
	  $order     = ((array_key_exists('order',$param))?$param['order']:'');
	  $spc_attr  = '';
	  $count     = 0;
	  $pointer   = '';
	  
	  $output='';
	  $result = DB::table($table);
	  if($condition!='')
	  {
	   $result = $result->whereRaw($condition);	  
	  }
	  
      if($order!='')
      {
		//$split_ord = explode(' ',$order);
      	$result = $result->orderByRaw($order);	
	  }		  
	  
	  $result = (($table=='ESv2_YearLevel')? $this->get_yearlvl_list() : $result->get());  
	  
	  if($result)
	  {
		$output='<select id="'.$name.'" name="'.$name.'" class="'.$cls.'" '.$attr.'> <option value="0">- Select -</option>';
		if($init!=''){$output .= '<option value="'.$init.'" '.(($selected=='')?'selected':'').'>'.(($init_lbl!='')?$init_lbl:'-Select One-').'</option>'; }
		foreach($result as $rs)
        {
		 $desc  = '';
		 $value = ((property_exists($rs,$col_id))?$rs->$col_id:'');
         	
		 if(is_array($col_desc))
		 {	 
		  foreach($col_desc as $arr_desc)
		  {
			$desc .= (($desc!='')?' ':'').((property_exists($rs,$arr_desc))?$rs->$arr_desc:'');	  
		  }
	     }
		 else	 
		  $desc  = ((property_exists($rs,$col_desc))?$rs->$col_desc:'');	
	     
		 if($table=='ESv2_YearLevel')
		  $spc_attr ='data-class="'.((property_exists($rs,'ProgClass'))?$rs->ProgClass:'').'" data-prog="'.((property_exists($rs,'ProgID'))?$rs->ProgID:'').'" data-old-id="'.((property_exists($rs,'Old_YearLevelID'))?$rs->Old_YearLevelID:'').'"  data-code="'.((property_exists($rs,'YearLevelCode'))?$rs->YearLevelCode:'').'" data-withmajor="'.((property_exists($rs,'YearLevelCode') && ($rs->YearLevelCode=='G10' || $rs->YearLevelCode=='G11' || $rs->YearLevelCode=='G12'))?1:0).'"';	  
		 else if($table=='ESv2_PaymentMethods')	 
		  $spc_attr ='data-instruct="'.((property_exists($rs,'Instructions'))?$rs->Instructions:'').'"';
	     else if($table=='ES_Curriculums')
		  $spc_attr ='data-progid="'.((property_exists($rs,'ProgramID'))?$rs->ProgramID:'').'" data-majorid="'.((property_exists($rs,'MajorID'))?$rs->MajorID:'').'"  data-min="'.((property_exists($rs,'RegularMinLoad'))?$rs->RegularMinLoad:'0').'"  data-max="'.((property_exists($rs,'RegularMaxLoad'))?$rs->RegularMaxLoad:'0').'"';	  
		 
		 if($limit>0)
		 {
		   if($pointer=='' && $value==$selected && $count<$limit)
		   {
			 $pointer = $value;
			 $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($count==$limit_sel)?'selected':'').'>'.$desc.'</option>'; 
			 $count++;
	       }
           else if($pointer!='' && $count<$limit)
		   {
			 $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($count==$limit_sel)?'selected':'').'>'.$desc.'</option>'; 
			 $count++;
	       }		   
		 }
		 else if($table=='ESv2_PaymentMethods')
		  $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($rs->SystemDefault==1 || $value==$selected)?'selected':'').'>'.$desc.'</option>';	 
		 else	 
		  $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($value==$selected)?'selected':'').'>'.$desc.'</option>';
		}		
		$output.='</select>';
	  }	  	              
	  return $output;	
	}
	
	public function pay_template($opt=1,$tempid=0,$campus=1,$term=83,$studno='',$yrlvl=1,$major=0,$scheme=0,$name='',$cls='',$attr='',$selected='-1')
	{
	  $output='';
	  $useoldlvl = env('USEOLDLVL',true); 
	  $rs = DB::select("EXEC ESv2_GetK12Fees @TemplateID  = '".$tempid."', 
	                                         @CampusID    = '".$campus."', 
	                                         @TermID      = '".$term."', 
	                                         @StudentNo   = '".$studno."', 
	                                         @YearLevelID = '".$yrlvl."',  
	                                         @MajorID     = '".$major."', 
	                                         @SchemeID    = '".$scheme."',
											 @UseOldLvl   = ".(($useoldlvl)?1:0));
											 
      switch($opt){
	    case 1:
		    $chk   = DB::select("SELECT (CASE WHEN TermID=".$term." THEN 1 ELSE 0 END) as SStatus FROM ES_Students WHERE StudentNo='".$studno."'");
			$stats = (($chk && count($chk)>0)? ($chk[0]->SStatus):0);
		    $output='<select class="form-control input-sm '.$cls.' '.$selected.'" id="'.$name.'" name="'.$name.'" '.$attr.'>';
		  //$output .= '<option value="-1" '.(($selected=='-1')?'selected disabled':'').'>-Select One-</option>';
			$rcount = count($rs);
			$i      = 0;
		    foreach($rs as $d){
			 $value    = ((property_exists($d,'TemplateID'))?$d->TemplateID:'');	
		     $desc     = ((property_exists($d,'TemplateCode'))?$d->TemplateCode:'');
             $iselder  = ((property_exists($d,'IsElder'))?($d->IsElder):0);
             $gala     = ((property_exists($d,'GALA'))?floatval($d->GALA):0);
             $hisfaco  = ((property_exists($d,'HISFACO'))?floatval($d->HISFACO):0);
             $ptc      = ((property_exists($d,'PTC'))?floatval($d->PTC):0);
             $publi    = ((property_exists($d,'Publication'))?floatval($d->Publication):0);
             $pof      = ((property_exists($d,'POF'))?floatval($d->POF):0);
             $ict      = ((property_exists($d,'ICT'))?floatval($d->ICT):0);	
			 
		   //if($iselder==1 && $gala==0 && $hisfaco==0 && $ptc==0 && $publi==0 && $pof==0) 
		   //	continue;
		   //else if($iselder==0 && ($gala>0 || $hisfaco>0 || $ptc>0 || $publi>0 || $pof>0)) 
		   //	continue;
		     if($stats==1 && strpos(strtolower($d->TemplateCode),'old')!==false){
			    continue;
			 }
			 
		     if($stats==0 && strpos(strtolower($d->TemplateCode),'new')!==false){
			    continue;
			 }
		     
			 
			 if($i==0){
			  $output.= '<option value="'.$value.'" '.((intval($selected)==intval($value))?'selected':'').'>'.$desc.'</option>';
			  $i++;
			 }
			 else
			 continue;
			}
			
		    $output.='</select>';
          break;
        case 2:
		  $output = $rs;
          break;		
	  }
	  
      return $output;	  
	}
	
	public function get_reginfo($regid=0)
	{
	 $output    = array();
	 $termid    = 0;
	 $campusid  = 0;
	 $studno    = 0;
	 $yrlvl     = 0;
	 $tempid    = 0;
	 $scheme    = 0;
	 $method = 0;
	 $rs = DB::select("SELECT TOP 1 r.TermID
                                   ,r.CampusID
			                       ,RegID
			                       ,StudentNo
			                       ,YearLevelID
								   ,MajorID
								   ,ProgID
			                       ,t.PaymentScheme as SchemeID
			                       ,TableofFeeID
			                       ,PaymentMethod
                              FROM ES_Registrations r 
                         LEFT JOIN ES_TableofFees t ON r.TableofFeeID= t.TemplateID
	                         WHERE RegID='".$regid."'");
	 foreach($rs as $d)
	 {
	  $termid   = ((property_exists($d,'TermID'))?$d->TermID:0);	 
	  $campusid = ((property_exists($d,'CampusID'))?$d->CampusID:1);	 
	  $studno   = ((property_exists($d,'StudentNo'))?$d->StudentNo:'');	 
	  $yrlvl    = ((property_exists($d,'YearLevelID'))?$d->YearLevelID:0);	
	  $major    = ((property_exists($d,'MajorID'))?$d->MajorID:0);	
	  $progid   = ((property_exists($d,'ProgID'))?$d->ProgID:0);	 
	  $tempid   = ((property_exists($d,'TableofFeeID'))?$d->TableofFeeID:0);	 
	  $scheme   = ((property_exists($d,'SchemeID'))?$d->SchemeID:0);	 
	  $method   = ((property_exists($d,'PaymentMethod'))?$d->PaymentMethod:0);	 
	 }
	 
	 $useoldlvl = env('USEOLDLVL',true);
	 if($useoldlvl)
	 {	 
	  $yrdata = $this->get_yearlvl_info($yrlvl,$progid);
	  foreach($yrdata as $yrs)
	  {
	   $yrlvl     = $yrs->YearLevelID; 
	   $yrlvlname = $yrs->YearLevelName; 
	   $progid    = $yrs->ProgID;
	  }
	 }
	 
	 if($termid>0 && $campusid>0 && $studno!='')
	 {
	  $output['termid']     = $termid;	 
	  $output['campusid']   = $campusid;	 
	  $output['regid']      = $regid;	 
	  $output['studno']     = $studno;	 
	  $output['yrlvl']      = $yrlvl;	 
	  $output['major']      = $major;	 
	  $output['scheme']     = $scheme;	 
	  $output['tempid']     = $tempid;	 
	  $output['method']     = $method;	 
	  $output['sel_temp']   = $this->pay_template(1,0,$campusid,$termid,$studno,$yrlvl,$major,$scheme,'studtemplate','form-control','',$tempid);
	  $output['breakdown']  = $this->pay_template(2,$tempid,$campusid,$termid,$studno,$yrlvl,$major,$scheme,'studtemplate','form-control','',$tempid);
	  $output['sel_method'] = $this->dropdown(array('table'     =>'ESv2_PaymentMethods',
													'col_id'    =>'MethodIDX',
													'col_desc'  =>'Name',
													'name'      =>'paymethod',
													'cls'       =>'form-control paymethod',
													'attr'      =>'required',
													'selected'  =>$method,
													'condition' =>"Inactive=0",
													));
	 }	 
	 
	 return $output;
	}
	
	public function get_SY($opt=0,$data='TermID',$current=0)
	{
	 if($opt==1) //current
     {
	  $result = DB::table('ES_AYTerm')
	                    ->select(DB::raw('dbo.fn_AcademicYearTerm(TermID) as SYTerm, TermID'))
						->whereRaw("Active_OnlineEnrolment=1"); 	  
	 }
	 else if($opt==2)
	 {
	  $current = (($current==0)?$this->get_SY(1,'TermID'):$current);
	  $result  = DB::table('ES_AYTerm')
	                    ->select(DB::raw('dbo.fn_AcademicYearTerm(TermID) as SYTerm, TermID as TermID'))
						->whereRaw("SchoolTerm='School Year' AND AcademicYear LIKE (SELECT TOP 1 '%'+LEFT(AcademicYear,4) FROM ES_AYTerm WHERE TERMID='".$current."')"); 	  
	 	 
	 }	 
	 else if($opt==3)
	 {
	  $result  = DB::table('ES_Configuration')
	                    ->select(DB::raw('dbo.fn_AcademicYearTerm(Value) as SYTerm, Value as TermID'))
						->whereRaw("IndexID = 2"); 	  
	 	 
	 }	 
	 
     
	 if($result)
	 {
      foreach($result->get() as $rs)
      {
		return ((property_exists($rs,$data))?($rs->$data):false);  
	  } 	  
	 }	 
	 else
	  return false;
    }
	
	public function get_familyinfo($userID=''){
	 $rs = DB::select("SELECT f.* FROM ESv2_Users u INNER JOIN ESv2_Admission_FamilyBackground f ON u.FamilyID=f.FamilyID WHERE UserIDX='".(($userID=='')?getUserID():$userID)."'");
	 if($rs){
	   foreach($rs as $result)
       {
		return $result;   
	   }	   
	 }else
       return false;		 
	}
	
	public function clear_arr($arr=array())
	{
	 if(!is_array($arr)){return false; }
	 foreach($arr as $k=>$v)
     {
	  if($v=='' || $v==NULL || $v==false)
      {
		unset($arr[$k]);  
	  }		  
	 }
     return $arr;	 
	}
	
	public function update_familyinfo($familyid='',$data=array())
	{
	 $new      = ((array_key_exists('option',$data) && $data['option']=='new')?true:false);
	 $tmpcheck =  DB::SELECT("SELECT * FROM ESv2_Admission_FamilyBackground WHERE FamilyID='".$familyid."'");
	 if($tmpcheck && count($tmpcheck)>0){
		$new   = false;
	 }else{
	    $new   = true;
	 }
	 
	 $tmpfields=array();
	 $tmpfields['Guardian_Name']               = ((array_key_exists('guardname',$data) && $data['guardname']!='')?$data['guardname']:'');
	 $tmpfields['Guardian_ParentMaritalID']    = ((array_key_exists('guardmarital',$data) && $data['guardmarital']!='')?$data['guardmarital']:'');
	 $tmpfields['Guardian_LivingWith']         = ((array_key_exists('guardwith',$data) && $data['guardwith']!='')?$data['guardwith']:'');
	 $tmpfields['Guardian_RelationshipOthers'] = ((array_key_exists('guardother',$data) && $data['guardother']!='')?$data['guardother']:'');
	 $tmpfields['Guardian_Address']            = ((array_key_exists('guardaddr',$data) && $data['guardaddr']!='')?$data['guardaddr']:'');
	 $tmpfields['Guardian_Street']             = ((array_key_exists('guardstreet',$data) && $data['guardstreet']!='')?$data['guardstreet']:'');
	 $tmpfields['Guardian_Barangay']           = ((array_key_exists('guardbrgy',$data) && $data['guardbrgy']!='')?$data['guardbrgy']:'');
	 $tmpfields['Guardian_TownCity']           = ((array_key_exists('guardcity',$data) && $data['guardcity']!='')?$data['guardcity']:'');
	 
	 if($tmpfields['Guardian_TownCity']!=''){
		$tmpcity = DB::SELECT("SELECT TOP 1 * FROM ESv2_City WHERE City='".$tmpfields['Guardian_TownCity']."'");
		if($tmpcity && count($tmpcity)>0){
		  $tmpfields['Guardian_CityID'] = $tmpcity[0]->CityID;
		}
	 }
	 
	 $tmpfields['Guardian_Province']           = ((array_key_exists('guardprov',$data) && $data['guardprov']!='')?$data['guardprov']:'');
	 $tmpfields['Guardian_ZipCode']            = ((array_key_exists('guardprov',$data) && $data['guardzip']!='')?$data['guardzip']:'');
	 $tmpfields['Guardian_TelNo']              = ((array_key_exists('guardprov',$data) && $data['guardtelno']!='')?$data['guardtelno']:'');
	 $tmpfields['Guardian_Mobile']             = ((array_key_exists('guardprov',$data) && $data['guardzip']!='')?$data['guardmobile']:'');
	 $tmpfields['Guardian_Email']              = ((array_key_exists('guardemail',$data) && $data['guardemail']!='')?$data['guardemail']:'');
	 
	 $tmpfields['Father_Name']                 = ((array_key_exists('fathername',$data) && $data['fathername']!='')?$data['fathername']:'');
	 $tmpfields['Father_BirthDate']            = ((array_key_exists('fatherdob',$data) && $data['fatherdob']!='')?(date('Y-m-d',strtotime($data['fatherdob']))):'');
	 $tmpfields['Father_Occupation']           = ((array_key_exists('fatherocptn',$data) && $data['fatherocptn']!='')?$data['fatherocptn']:'');
	 $tmpfields['Father_Company']              = ((array_key_exists('fathercompany',$data) && $data['fathercompany']!='')?$data['fathercompany']:'');
	 $tmpfields['Father_CompanyAddress']       = ((array_key_exists('fatherbaddr',$data) && $data['fatherbaddr']!='')?$data['fatherbaddr']:'');
	 $tmpfields['Father_CompanyPhone']         = ((array_key_exists('fatherbmobile',$data) && $data['fatherbmobile']!='')?$data['fatherbmobile']:'');
	 $tmpfields['Father_Email']                = ((array_key_exists('fatheremail',$data) && $data['fatheremail']!='')?$data['fatheremail']:'');
	 $tmpfields['Father_Mobile']               = ((array_key_exists('fathermobile',$data) && $data['fathermobile']!='')?$data['fathermobile']:'');
	 $tmpfields['Father_EducAttainment']       = ((array_key_exists('fathereduc',$data) && $data['fathereduc']!='')?$data['fathereduc']:'');
	 $tmpfields['Father_SchoolAttended']       = ((array_key_exists('fatherschool',$data) && $data['fatherschool']!='')?$data['fatherschool']:'');
	 
	 $tmpfields['Mother_Name']                 = ((array_key_exists('mothername',$data) && $data['mothername']!='')?$data['mothername']:'');
	 $tmpfields['Mother_BirthDate']            = ((array_key_exists('motherdob',$data) && $data['motherdob']!='')?(date('Y-m-d',strtotime($data['motherdob']))):'');
	 $tmpfields['Mother_Occupation']           = ((array_key_exists('motherocptn',$data) && $data['motherocptn']!='')?$data['motherocptn']:'');
	 $tmpfields['Mother_Company']              = ((array_key_exists('mothercompany',$data) && $data['mothercompany']!='')?$data['mothercompany']:'');
	 $tmpfields['Mother_CompanyAddress']       = ((array_key_exists('motherbaddr',$data) && $data['motherbaddr']!='')?$data['motherbaddr']:'');
	 $tmpfields['Mother_CompanyPhone']         = ((array_key_exists('motherbmobile',$data) && $data['motherbmobile']!='')?$data['motherbmobile']:'');
	 $tmpfields['Mother_Email']                = ((array_key_exists('motheremail',$data) && $data['motheremail']!='')?$data['motheremail']:'');
	 $tmpfields['Mother_Mobile']               = ((array_key_exists('mothermobile',$data) && $data['mothermobile']!='')?$data['mothermobile']:'');
	 $tmpfields['Mother_EducAttainment']       = ((array_key_exists('mothereduc',$data) && $data['mothereduc']!='')?$data['mothereduc']:'');
	 $tmpfields['Mother_SchoolAttended']       = ((array_key_exists('motherschool',$data) && $data['motherschool']!='')?$data['motherschool']:'');
	 
	 $tmpfields['EmergencyContact_Name']         = ((array_key_exists('emerganame',$data) && $data['emerganame']!='')?$data['emerganame']:'');
	 $tmpfields['EmergencyContact_Relationship'] = ((array_key_exists('emergarelate',$data) && $data['emergarelate']!='')?$data['emergarelate']:'');
	 $tmpfields['EmergencyContact_Mobile']       = ((array_key_exists('emergacontact',$data) && $data['emergacontact']!='')?$data['emergacontact']:'');
	 
	 $tmpfields['EmergencyContactB_Name']        = ((array_key_exists('emergbname',$data) && $data['emergbname']!='')?$data['emergbname']:'');
	 $tmpfields['EmergencyContactB_Relationship']= ((array_key_exists('emergbrelate',$data) && $data['emergbrelate']!='')?$data['emergbrelate']:'');
	 $tmpfields['EmergencyContactB_Mobile']      = ((array_key_exists('emergbcontact',$data) && $data['emergbcontact']!='')?$data['emergbcontact']:'');
	 
	 $tmpfields=$this->clear_arr($tmpfields);
	 if($new){
	  $tmpfields['FamilyID']=$familyid;
	  $result = DB::table('ESv2_Admission_FamilyBackground')
                         ->insert($tmpfields);
	 }else{	 
      $result = DB::table('ESv2_Admission_FamilyBackground')
                         ->where('FamilyID', $familyid)
                         ->update($tmpfields);
	 }
	 
	 if($result)
	  return true;
     else
	  return false;
	}

	public function save_journals($termid,$campus=1,$studno='',$regid='',$assess=array()){
		$exec = false;
	    foreach($assess as $k=>$ass){
		 //die($termid.'-'.$campus.'-'.$studno.'-'.$ass['acctid']);
		   $exec = DB::statement("INSERT INTO ES_Journals(ServerDate
		                                                 ,TransDate
														 ,TermID
														 ,CampusID
														 ,TransID
														 ,ReferenceNo
														 ,[Description]
														 ,AccountID
														 ,IDType
														 ,IDNo
														 ,Debit
														 ,[Assess Fee]
														 ,Discount
														 ,[1st Payment]
														 ,[2nd Payment]
														 ,[3rd Payment]
														 ,[4th Payment]
														 ,ActualPayment)
								                   SELECT GETDATE() as ServerDate
														 ,RegDate
														 ,TermID
														 ,CampusID
														 ,1 as TransID
														 ,RegID
														 ,'' as [Description]
														 ,'".$ass['acctid']."' as AccountID
														 ,1 as IDType
														 ,StudentNo
														 ,'".$ass['assess']."' as Debit
														 ,'".$ass['assess']."' as [Assess Fee]
														 ,'".$ass['discount']."' as Discount
														 ,'".$ass['amta']."' as [1st Payment]
														 ,'".$ass['amtb']."' as [2nd Payment]
														 ,'".$ass['amtc']."' as [3rd Payment]
														 ,'".$ass['amtd']."' as [4th Payment]
														 ,'0.00' as [ActualPayment] 
													 FROM ES_Registrations 
													WHERE TermID='".$termid."' AND StudentNo='".$studno."'");	 
		    
		   /**/		  
	  }
	  return $exec;	
	}
	
	public function register_student($termid=0,$regid='',$studno='',$appno='',$yrlvl='',$major=0,$tempid=0,$method=0,$amt=0)
    {
	 DB::enableQueryLog();	
	 $campus    = false;
	 $SYTerm    = (($termid==0)? $this->get_SY(1,'TermID') : $termid);	
	 $useoldlvl = env('USEOLDLVL',true); 
	 if($regid==''){	       
           $result = DB::select("EXEC K12_RegisterStudent @StudentNo=".(($studno!='')?"'".$studno."'":"NULL").",@TermID='".$SYTerm."',@GradeLevelID='".$yrlvl."',@StrandID='".$major."',@FeesID='".$tempid."',@PayMethodID='".$method."',@Amount='".$amt."',@AppNo='".$appno."',@UserID='".getUserID()."',@UseOldLvl=".(($useoldlvl)?1:0));
           $update = DB::statement("UPDATE s SET s.YearLevelID=r.YearLevelID,s.ProgID=r.ProgID
                                      FROM ES_Students as s
                                INNER JOIN ES_Registrations as r ON r.TermID='".$termid."' AND r.StudentNo=s.StudentNo
                                     WHERE r.StudentNo='".$studno."'");      
     }else{
	  $tfields=array();
	  $tfields['RegDate']         = date('Y-m-d H:i:s');
	  $tfields['TableOfFeeID']    = $tempid;
	  $tfields['PaymentMethod']   = $method;
	  $tfields['TotalAssessment'] = $amt;
	  $tfields['LastModifiedBy']  = getUserID();
	  $tfields['LastModifiedDate']= date('Y-m-d H:i:s');
	  $query   = DB::table('ES_Registrations')->where('RegID', $regid)->update($tfields); 
	  $result  = array($regid,$tempid,$method);					 
	 }
	 
	 $queries = DB::getQueryLog();
	 if($result){
	   if($tempid!=0){
	    $pdates = DB::SELECT("SELECT Date1,Date2,Date3,Date4 FROM ES_TableOfFees_Dates WHERE TemplateID='".$tempid."'
                              UNION ALL
                              SELECT FirstPaymentDate,SecondPaymentDate,ThirdPaymentDate,FourthPaymentDate FROM ES_RegistrationConfig_Dates as cd 
							        INNER JOIN ES_TableofFees as t ON t.TermID=cd.TermID AND t.PaymentScheme=cd.PaymentSchemeID WHERE t.TemplateID='".$tempid."'");
		if($pdates && count($pdates)>0){
		$tfields=array();
	    $tfields['FirstPaymentDueDate'] =$pdates[0]->Date1;
	    $tfields['SecondPaymentDueDate']=$pdates[0]->Date2;
	    $tfields['ThirdPaymentDueDate'] =$pdates[0]->Date3;
	    $tfields['FourthPaymentDueDate']=$pdates[0]->Date4;
	    $query   = DB::table('ES_Registrations')->where('RegID', $regid)->update($tfields);
	    }							
	   }
	   
	   $result = DB::table('ES_Registrations')
	                  ->whereRaw("StudentNo='".$studno."' AND TermID='".$SYTerm."'")
                      ->get();					 
       if($result){
		$regid  = ((property_exists($result[0],'RegID'))?($result[0]->RegID):false);  
		$campus = ((property_exists($result[0],'CampusID'))?($result[0]->CampusID):false);  
		$result = (($regid && $campus)?true:false);
		if($regid && $campus){
	   //$delete  = DB::table('ES_Journals')->whereRaw("ReferenceNo='".$regid."'")->delete();
	   //$result  = DB::statement("EXEC sp_K12_RegAssessment @StudentNo='".$studno."',@TemplateID=".$tempid.",@TermID=".$SYTerm.",@CampusID=".$campus.",@RegID='".$regid."'");	
	   //$result  = end($queries);
		}	
	   } 		   
	 }
     
	 return end($queries);
	}
	
	public function register_section($regid){
		
	}
	
	public function purge_registration($regid='')
	{
	 $result2=false;
	 $result1 = DB::table('ES_Registrations')
	                   ->whereRaw("RegID ='".$regid."' AND ValidationDate IS NULL")
					   ->delete();
	 if($result1)
	 {	 
	  $result2 = DB::table('ES_RegistrationDetails')
	                   ->whereRaw("RegID ='".$regid."'")
					   ->delete();
      
      $result2 = DB::table('ES_Journals')
	                   ->whereRaw("TransID = 1 AND ReferenceNo ='".$regid."'")
					   ->delete();
                       
	 }
	 return (($result1)? true : false);
	}
	
	public function print_registration($termid=0,$campus=1,$regid='',$yrlvl='')
	{
	 $result = DB::select("EXEC K12_rptRegistrationForm @InstitutionName='SchoolName'
                                                       ,@CampusAddress='School Address'
                                                       ,@Republic='Republic of the Philippines'
                                                       ,@CampusID='".$campus."'
                                                       ,@TermID='".$termid."'
                                                       ,@RegID='".$regid."'
                                                       ,@YearLevelID='".$yrlvl."'
                                                       ,@ExecuteBy='".getUserID()."'
                                                       ,@Copy='".'StudentCopy'."'");	
     return $result;													   
	}
	
	public function generate_stats($SYTerm=0)
	{
	  $SYTerm = (($SYTerm>0)?$SYTerm:$this->get_SY(1,'TermID'));	
	  $rs = DB::select("SELECT y.*
                      ,(SELECT COUNT(*) FROM ES_Registrations r WHERE r.YearLevelID=y.YearLevelID AND r.ProgID=p.ProgID AND TermID=".$SYTerm.") as Registered
	                  ,(SELECT COUNT(*) FROM ES_Registrations r INNER JOIN ES_Students s ON r.StudentNo=s.StudentNo AND s.Gender='M' WHERE         r.YearLevelID=y.YearLevelID AND r.ProgID=p.ProgID AND r.TermID=".$SYTerm.") as RegisteredMale
	                  ,(SELECT COUNT(*) FROM ES_Registrations r INNER JOIN ES_Students s ON r.StudentNo=s.StudentNo AND s.Gender='F' WHERE r.YearLevelID=y.YearLevelID AND r.ProgID=p.ProgID AND r.TermID=".$SYTerm.") as RegisteredFemale
                      ,(SELECT COUNT(*) FROM ES_Registrations r WHERE r.YearLevelID=y.YearLevelID AND r.ProgID=p.ProgID AND TermID=".$SYTerm." AND ValidationDate IS NOT NULL) as Validated 
                      ,(SELECT COUNT(*) FROM ES_Registrations r INNER JOIN ES_Students s ON r.StudentNo=s.StudentNo AND s.Gender='F' WHERE r.YearLevelID=y.YearLevelID AND r.ProgID=p.ProgID AND r.TermID=".$SYTerm." AND ValidationDate IS NOT NULL) as ValidatedFemale 
	                  ,(SELECT COUNT(*) FROM ES_Registrations r INNER JOIN ES_Students s ON r.StudentNo=s.StudentNo AND s.Gender='M' WHERE r.YearLevelID=y.YearLevelID AND r.ProgID=p.ProgID AND r.TermID=".$SYTerm." AND ValidationDate IS NOT NULL) as ValidatedMale 
                  FROM ESv2_YearLevel as y
            INNER JOIN ES_Programs p ON y.ProgClass=p.ProgClass
			WHERE y.Inactive=0");
	 return $rs;
    }
	
	public function get_yearlvl_list()
    {
      /*2017-04-03 - Update ung mga putang inang hardcode na pahamak sa system(Mamatay na ung sumisira ng module) */
	  /* $rs = DB::select("SELECT Y.YearLevelID ,Y.ProgClass
		                      ,Y.YearLevelCode
		                      ,Y.YearLevelName
		                      ,Y.SeqNo
		                      ,Y.Inactive
		                      ,p.ProgID
		                      ,p.CollegeID
		                     ,xY.YearLevelID as Old_YearLevelID
	                     FROM ESv2_YearLevel AS Y 
	               INNER JOIN ES_Programs AS p ON p.ProgClass = Y.ProgClass AND p.ProgID IN ('1','21','29')
	                LEFT JOIN (SELECT SeqNo, YearLevelID, 
					                  (CASE WHEN Description3='Nursery' THEN Description3 ELSE Description2 END) as YearLevelName 
								 FROM ES_YearLevel WHERE Description2 <> ''
		                        UNION ALL 
		                       SELECT (SeqNo + 9) as SeqNo, YearLevelID, 
							          HSDescription as YearLevelName 
							     FROM ES_YearLevel 
								WHERE HSDescription <> '') AS xY ON Y.YearLevelName=xY.YearLevelName
	                    WHERE Y.Inactive=0
	                 ORDER BY Y.YearLevelID");
       */
       /*Update : ARS 20170407.1046H - Simplified */
       $rs = DB::select("SELECT  Y.YearLevelID ,
                                Y.ProgClass ,
                                Y.YearLevelCode ,
                                Y.YearLevelName ,
                                Y.SeqNo ,
                                Y.Inactive ,
                                Y.ProgID,
                                dbo.fn_ProgramCollegeID(Y.ProgID) AS CollegeID ,
                                Y.YLID_OldValue AS Old_YearLevelID
                        FROM    ESv2_YearLevel AS Y
                        WHERE   Y.Inactive = 0
                        ORDER BY Y.YearLevelID");
                            	
	 return $rs;
   }	
   
    public function get_yearlvl_info($yrlvlid=0,$progid=0)
    {
	  $rs = DB::select("SELECT Y.YearLevelID
                              ,Y.ProgClass
	                          ,Y.YearLevelCode
	                          ,Y.YearLevelName
	                          ,Y.SeqNo
	                          ,Y.Inactive
	                          ,p.ProgID
	                          ,p.CollegeID
	                          ,xY.YearLevelID as Old_YearLevelID
                         FROM ESv2_YearLevel AS Y 
                   INNER JOIN ES_Programs AS p ON p.ProgClass = Y.ProgClass
                    LEFT JOIN (SELECT SeqNo, YearLevelID, (CASE WHEN Description3='Nursery' THEN Description3 ELSE Description2 END) as YearLevelName FROM ES_YearLevel WHERE Description2 <> ''
                               UNION ALL 
                               SELECT (SeqNo + 9) as SeqNo, YearLevelID, HSDescription as YearLevelName FROM ES_YearLevel WHERE HSDescription <> '') AS xY ON Y.YearLevelName=xY.YearLevelName
						WHERE ".(($progid==0)? "Y.YearLevelID=".$yrlvlid : "xY.YearLevelID=".$yrlvlid." AND p.ProgID=".$progid));	
	 return $rs;
   }	
   
   public function get_studinfo($studno=''){
	  if($studno=='' || $studno==false){return false;} 
      
      $setup = new mSetup();
      
	  $rs = DB::select("SELECT TOP 1 StudentNo
                            ,LastName
			                ,FirstName
                            ,MiddleName
							,MiddleInitial
                            ,MajorDiscIDb
                            ,YearLevelID
                    				,CurriculumID
									,0 as Min
                                    ,0 as Max
                                    ,0.00 as Balance									
	                     FROM ES_Students 
						 WHERE StudentNo='".$studno."' AND (ProgID='". $setup->IsSHS() ."' OR (ProgID=1 AND (YearLevelID=4 AND ProgID=1)))");	
	 return $rs;
   }
   
   public function getFeesTemplateID($term, $status, $foreign, $prog = 0, $major , $level){
        $template = 0 ;
        $model = new \App\Modules\Enrollment\Models\FeesTemplate;
        
        $status = $status == 'New' ? '1':'2';
        
        $where = [
            'TermID' => $term,
            'ForForeign' => $foreign,
            'Inactive' => 0,
            'StudentStatus'=>$status,
        ];
        
        $rs = $model->Distribution($prog, $major);
        $rs = $rs->where($where);
        
        if($rs->count() > 0){            
            $template = $rs->first()->TemplateID;             
        }
                        
        return $template;
   }
   
}
?>