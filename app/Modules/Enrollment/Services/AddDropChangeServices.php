<?php

namespace App\Modules\Enrollment\Services;

// use App\Modules\GradingSystem\Services\Areas\Validation;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Enrollment\Models\mAddrop As mADC;
use App\Modules\Registrar\Models\Subjects\Subjects as mSubjects;
use DB;
use Request;

class AddDropChangeServices
{
    
    public static function saveNewADC($regId, $subjID){
        
        $model = new mADC();
        $return = 0;
        
        $data = array(
            'Date' => systemDate()
          ,'RegID' => $regId          
          ,'OfficerID' => getUserID()
          ,'AssessedDate' => systemDate()
          ,'AssessedBy' => getUserID()
        );
        
        $return = DB::table('ES_AddChangeDropSubjects')->insertGetId($data);
        
        return $return;
        
    }
    
    public static function saveADCdetails($ref, $subjs){
        
        /* Clear Subjects */
        DB::statement("DELETE ES_AddChangeDropDetails WHERE TransID = '{$ref}'");
        $cnt = 0;
        $unit = 0;
        
        foreach($subjs as $r){
            $exist = 0;
            
            $data = array(
                'TransID' => $ref
                ,'TransType' => $r['tag']
                ,'Target_ScheduleID' => decode($r['id'])
                ,'ChangeTo_ScheduleID' => $r['change']
            );
            
            if($r['tag'] == '1'){
                $unit += floatval($r['unit']);    
            }
                                    
            $exist = DB::table("ES_AddChangeDropDetails")->insert($data); 
            if($exist != '0'){
                $cnt++;
            }
        }
        
        return array('total' => $cnt, 'totalunit'=> $unit );

    }

    public static function processAssessment($term, $campus, $idno, $ref,  $feeId, $unit)
    {
        $dues = DB::table("ES_TableofFee_Details AS d")
                    ->leftJoin("dbo.ES_Accounts AS a",'d.AccountID','=','a.AcctID')
                    ->selectRaw("d.*, a.AcctOption")
                    ->where('TemplateID',$feeId)
                    ->get();
        
        /* Clear ADC Journals */
        DB::statement("DELETE ES_Journals WHERE TransID = 2 AND TermID = {$term} AND IDType = 1 AND idno = '{$idno}' AND ReferenceNo = '{$ref}'");
        
        foreach($dues as $d){
            
            if($d->AcctOption == '24'){
                $amt = $d->Amount * $unit;                                                   
                                                                                         
                $acct=$d->AccountID;
                $serverDate = systemDate();
                
                $data = array(
                     'ServerDate' => $serverDate
                    ,'TransDate' => $serverDate
                    ,'TermID' => $term
                    ,'CampusID' => $campus
                    ,'TransID' => 2
                    ,'ReferenceNo' => $ref
                    ,'AccountID' => $acct
                    ,'IDType' => 1
                    ,'IDNo' => $idno
                    ,'Debit' => $amt
                    ,'Assess Fee' => $amt
                    ,'1st Payment' => $amt
                );
                
                $exist = DB::table("ES_Journals")->insertGetId($data);         
            
            }
                      		          		          
        }
                                
    }
                   
}