<?php
    Route::group(['prefix' => 'enrollment','middleware'=>'auth'], function () {
	    Route::get('/','Enrollment@index');
		Route::get('/children','Enrollment@children');
		Route::get('/registered','Enrollment@registered');
		Route::get('/custom','Enrollment@custom');
		//Route::get('/Advising','Enrollment@custom');
		Route::get('/actions','Enrollment@actions');
		Route::post('/actions','Enrollment@actions');
		Route::get('/studentlist','Enrollment@studentlist');
		Route::get('/print_report','Enrollment@print_report');
		Route::get('/print_report_old','Enrollment@print_report_old');
		Route::get('/trial','Enrollment@trial');
        
        
        Route::group(['prefix' => 'promotions', 'middleware' => 'auth'], function () {
	        Route::get('/', 'Promotions@index');
	        Route::post('event', 'Promotions@event');
	    });
        
        Route::group(['prefix' => 'advising', 'middleware' => 'auth'], function () {
	        Route::get('/', 'Advising@index');
            Route::get('/new', 'Advising@advise');
            Route::get('/edit', 'Advising@advise');
	        Route::post('event', 'Advising@event');
            Route::get('/printEval', 'Advising@print_report');
	    });                
	});
    Route::group(['prefix' => 'add-drop-change', 'middleware' => 'auth'], function () {
        Route::get('/', 'Addropchange@index');
        Route::get('/new', 'Addropchange@advise');
        Route::get('/edit', 'Addropchange@advise');
        Route::post('event', 'Addropchange@event');
        Route::get('/print', 'Addropchange@eprint');
        
    });
    
	Route::group(['prefix'=>'manual-class','middleware'=>'auth'],function(){
        Route::get('/', 'Enrollment@ManualClassSection');
        Route::post('/txn', 'Enrollment@ManualClassSection');
	});    
?>