<?php
    $j=1;

    $data = isset($balance) ? $balance : null;
	$summary = isset($summary) ? $summary : null;
    $obalance = isset($obalance) ? $obalance : 0;
    $outstandingbal = isset($obalance) ? $obalance : 0;

    $regid = isset($regid) ? $regid : 0;
    $all = isset($all) ? $all : 0;

    $balance = 0;
    $total = 0;
    $due = 0;
    $totaldue = 0;
    $discount=0;

    $stop_process = 0;
    $proceed = 0;

?>
<div class="table-scrollable" style="height: 270px; overflow-y: scroll;" >
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="records-table" data-reg="{{$all}}" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit">#</th>
            <th>Code</th>
            <th>Account Name</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Discount</th>
            <th class="danger text-center">Total</th>
            <th>Remarks</th>
            <th class="autofit text-center"><span class="font-xs">NON<br />LEDGER</span></th>
            <th>Group</th>
            <th>Type</th>
            <th>Reference</th>
        </tr>
    </thead>
    <tbody>

		@foreach($data as $r)
			<?php

				//err_log('summary : '. $s->ReferenceNo. ", details : ".$r->ReferenceNo );
				if ($r->ReferenceNo != '')
				{

					$balance = $r->Balance;

					if($balance != 0 && $stop_process == 0)
					{
						$total += $r->Balance;
						$due = $r->Balance - $discount;
						$totaldue += $due;
						
						if ($balance > 0){

			?>

		<tr data-id="{{$r->EntryID}}" data-due="{{$due}}"  data-ac="{{$r->AccountID}}" data-code="{{$r->AcctCode}}" data-discount="{{$discount}}" data-first="{{ $r->{'1st Payment'} }}" data-curr="1" data-txn="{{$r->TransID}}" data-ref="{{$r->ReferenceNo}}" data-trn="{{$r->ReferenceNo.';'.$r->EntryID.';1'}}" data-remarks="{{$r->Remarks}}"
		data-nonledger="0" data-deptid="{{$r->CostCenterID}}" data-dept="{{$r->CostCenterCode}}" data-yr="{{$r->YearLevelID}}" data-lvl="{{$r->YearLevel}}" >
			<td class="font-xs autofit bold">{{$j}}.</td>
			<td class="autofit">{{$r->AcctCode}}</td>
			<td>{{$r->AcctName}}</td>
			<td class="autofit text-right balance bold ">
				<input type="text" class="numberonly text-right balance-amt input-no-border input-xsmall " value="{{number_format($balance,2)}}" />
			</td>
			<td class="autofit text-right discount bold "><input type="text" class="numberonly text-right discount-amt input-no-border input-xsmall " value="{{number_format($discount,2)}}" /></td>
			<td class="autofit danger bold text-right due">{{number_format($due,2)}}</td>
			<td>{{$r->Remarks}}</td>
			<td class="autofit text-center"><input type="checkbox" class="non-ledger" /> </td>
			<td class="autofit">{{$r->ClassShort}}</td>
			<td class="autofit text-center">{{$r->TransID}}</td>
			<td class="autofit">{{$r->ReferenceNo.';'.$r->EntryID.';1'}}</td>
		</tr>

		<?php 
			
			} 
			
			$j++;

			If ( $obalance > $balance ){
				$obalance = $obalance - $balance;
			}else{
				$balance = $obalance;
				$stop_process = 1;
			}

		} } ?>

		@endforeach

        @if(!empty($data))
        <tfoot class="hide">

          <tr data-id="" 
              data-due="" 
              data-ac="" 
              data-code="" 
              data-discount="0" 
              data-first="0" 
              data-curr="1" 
              data-txn="" 
              data-ref="" 
              data-trn="" 
              data-remarks=""
              data-nonledger="0" 
              data-deptid="" 
              data-dept="" 
              data-yr="" 
              data-lvl="" >
                        <td class="font-xs autofit seqno bold">
                        </td>
                        <td class="autofit code"></td>
                        <td class="name"></td>
                        <td class="autofit text-right balance bold ">
                            <input type="text" class="numberonly text-right balance-amt input-no-border input-xsmall " value="0.00" />
                        </td>
                        <td class="autofit text-right discount bold ">
                            <input type="text" class="numberonly text-right discount-amt input-no-border input-xsmall " value="0.00" />
                        </td>
                        <td class="autofit danger bold text-right due">0.00</td>
                        <td class="remarks"> </td>
                        <td class="autofit text-center"><input type="checkbox" class="non-ledger" /> </td>
                        <td class="autofit class-short" > </td>
                        <td class="autofit text-center txn_id"> </td>
                        <td class="autofit ref_no"> </td>
                    </tr>

        </tfoot>
        @endif
    </tbody>
</table>
</div>
<div class="row">
    <div class="col-xs-3 col-md-4">
        <button class="btn btn-default btn-sm" data-menu="clear-table" type="button">
            Clear All
        </button>
        <button class="btn btn-primary btn-sm" data-menu="add-row" type="button">
            Add Row
        </button>
        <button class="btn btn-danger btn-sm" data-menu="rem-row" type="button">
            Remove Selected
        </button>

    </div>
    <div class="col-xs-3 col-md-3 text-right">
        <label>Outstanding Balance : <span class="bold font-lg" id="obalance">{{number_format($outstandingbal,2)}}</span> </label>
    </div>
    <div class="col-xs-6 col-md-5">
        <div class="row">
        <div class="col-sm-6 text-right">
        <h3 class="" style="margin-bottom: 0px; margin-top: 0px;" >
            Discount : <span class="bold" id="total-discount">{{number_format($discount,2)}}</span>
        </h3>
        </div>
        <div class="col-sm-6 text-right">
        <h3 class="" style="margin-bottom: 0px; margin-top: 0px;"  >
            Amount Due : <span class="bold text-danger" id="total-due">{{number_format($totaldue,2)}}</span>
        </h3>
        </div>
        </div>
    </div>
</div>
