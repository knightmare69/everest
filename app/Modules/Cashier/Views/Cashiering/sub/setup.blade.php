<div id="setup_modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" ><div class="modal-content">
    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Official Receipt Setup</h4></div>
    <div class="modal-body">
        
        <form role="form" class="form-horizontal" action="?" id="frm_programs" >
            <fieldset>
                <div class="form-group setup-or-group" style="margin-bottom: 8px;">
                    <div class="col-md-12">
                        <label class="control-label">Official Receipt Number</label>
                        <div class="input-group">
                            <input type="text" class="form-control" value="" id="setup_orno" name="setup_orno" placeholder="O.R. Number" />
                            <span class="input-group-btn">
			                 <button class="btn blue" type="button" data-menu="verify"><i class="fa fa-check"></i> Verify</button>
                            </span>            
                        </div>
                        <span class="help-block"></span>                                                                                    
                    </div>
                </div>
                <div class="form-group datetime-setup hidden" style="margin-bottom: 8px;">
                    <div class="col-md-12">
                        <label class="control-label">Date / Time </label>
                        <select id="setup_datetime" name="setup_datetime" class="form-control" >
                            <option value="0" selected> Server Date/Time </option>
                            <option value="1"> User Defined Date/Time </option>
                        </select>
                    </div>
                </div>
                <div class="form-group datetime-setup hidden" style="margin-bottom: 8px;">
                    
                    <div class="col-md-6">                        
                        <label class="control-label">Input Date/Time </label>
                        <input type="text" class="form-control date-picker" value="" id="setup_date" name="setup_date" placeholder="Date/Time" value="<?php echo date('Y-m-d');?>"/>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">&nbsp;</label>                        
                        <input type="text" class="form-control timepicker timepicker-default" value="" id="setup_time" name="setup_time" placeholder="Date/Time" />
                    </div>
                    
                </div>   
            </fieldset>
        </form>                
    </div>
    <div class="modal-footer">
        <button class="btn btn-success" data-menu="save-setup" type="button"> Save </button>
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i> Close</button>
    </div></div></div>
</div>