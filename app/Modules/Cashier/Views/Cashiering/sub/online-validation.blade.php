<div id="online-validation" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Validate Online Transaction</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal" action="?" id="form_online" autocomplete="off" >
                    <fieldset>
                        <div class="form-group setup-or-group" style="margin-bottom: 8px;">
                            <div class="col-md-12">
                                <label class="control-label">Payment Portals</label>
                                <div class="input-group" style="width: 100%;">
                                    <select id="portal-lists" name="portal-lists" class="form-control">
                                        <option value="-1" data-info="-1"> - Select Payment Portals - </option>
                                        <option value="paynamics" data-info="1" selected>Paynamics</option>
                                    </select>
                                </div>
                                <label class="control-label">Transaction ID/Number</label>
                                <div class="input-group" style="width: 100%">
                                    <input type="text" class="form-control" value="" id="tx_number" name="tx_number" placeholder="TX ID/Number" />
                                </div>
                                <div class="input-group">
                                    <strong id="status-message" class="status-message font-purple-plum"></strong>
                                </div>
                                <label class="control-label">Online Debit Information</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6>
                                            <strong>Balance : &#8369;<span id="online-debit-amount" class="font-black"></span></strong>
                                        </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>
                                            <strong>Payed by : <span class="online-debit-payor" id="online-debit-payor"></span></strong>
                                        </h6>                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6>
                                            School Term : <span class="online-school-term" id="online-school-term"></span>
                                        </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>
                                            Payed to : <span class="online-payed-to" id="online-payed-to"></span>
                                        </h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="offset-md-6 col-md-6">
                                        <h6>
                                            School ID # : <span class="online-payed-id" id="online-payed-id"></span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="set-online" data-online="set-online" type="button"><i class="fa fa-check"></i> Select </button>
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>