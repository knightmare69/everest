<div class="row">    
<form role="form" class="form-horizontal" id="form_payment" action="?" autocomplete="false"  >

    <div class="col-sm-7">        
        <fieldset>
        <div class="form-group" style="margin-bottom: 8px;">
            <div class="col-sm-12">                        
            <div class="panel panel-info">
        		<div class="panel-heading"><h3 class="panel-title bold ">Check Payment</h3></div>
        		<div class="panel-body" style="padding: 0px;">                                                    
        			<table id="tblchecks" class="table table-striped table-bordered table-condensed" style="margin: 0px;">
    					<thead>
    						<tr>
    							<th width="27" >#</th>
    							<th width="43" class="text-center">Type</th>
    							<th width="70" class="text-center" >Check #</th>
    							<th width="81">Date</th>
                                <th >Bank - Branch</th>
                                <th width="70">Amount</th>
    						</tr>
    					</thead>					
    
                    	<tbody>
                            @for($i=1; $i<=5; $i++)
                              <tr>
                                <td class="font-xs" width="27">{{$i}}.</td>
                                <td width="43" class="text-center" style="padding: 0px !important;"><input type="text" id="chktype{{$i}}" maxlength="1" class="numberonly text-center input-no-border" value="0" /> </td>
                                <td width="70" class="" style="padding: 0px !important;"><input type="text" id="chkno{{$i}}" class="numberonly input-no-border" /> </td>
                                <td width="80" class="" style="padding: 0px !important;"><input type="text" class="input-no-border" id="chkdate{{$i}}" /> </td>
                                <td class="" style="padding: 0px !important;"><input type="text" class="input-no-border" id="chkbank{{$i}}" /> </td>
                                <td width="70" class="" style="padding: 0px !important;"><input type="text" id="chkamt{{$i}}" class="numberonly bold text-right input-no-border txt-check-amount " /> </td>
                              </tr>  
                            @endfor						  
    					</tbody>                        
                    </table>                    
        		</div>
        	</div>
            </div>            
        </div>
         <div class="form-group" style="margin-bottom: 8px;">
            <div class="col-sm-12">                        
            <div class="panel panel-warning" style="margin-bottom: 0px;">
        		<div class="panel-heading">
        			<h3 class="panel-title bold ">Credit Card / EPS System </h3>
        		</div>
        		<div class="panel-body" style="padding: 0px;">                                                    
        			 <table id="tblcard" class="table table-striped table-bordered table-condensed" style="margin: 0px; width: 100%;">
						<thead>
						<tr>
							<th >#</th>
							<th  class="text-center">Card Type</th>
							<th class="text-center" >Card No. / Account No. </th>
							<th width="70">Approval Code</th>                            
                            <th width="70">Amount</th>
						</tr>
						</thead>					
						
                        <tbody>
                            @for($i=1; $i<=2; $i++)
                              <tr>
                                <td class="font-xs autofit">{{$i}}.</td>
                                <td class="text-center" style="padding: 0px !important;">
                                    <select class="input-no-border input-small" name="cardtype{{$i}}">
                                        <option value="0">- select -</option>
									<?php
									    $chktype = DB::select("SELECT * FROM ES_PaymentTypes WHERE Options=6");
										if($chktype && count($chktype)>0){
										  foreach($chktype as $ct){
										    echo '<option value="'.$ct->PaymentID.'">'.$ct->ShortName.'('.$ct->Payment.')'.'</option>';
										  }
										}
									?>
                                    </select>
                                </td>
                                <td class="" style="padding: 0px !important;"><input name="cardno{{$i}}" type="text" class="input-no-border" /> </td>
                                <td width="70" class="" style="padding: 0px !important;"><input name="cardapproval{{$i}}" type="text" class="input-no-border" /> </td>
                                
                                <td class="" width="70" style="padding: 0px !important;"><input type="text" name="card{{$i}}" class="numberonly txt-card-amount text-right bold input-no-border" value="0.00" /> </td>
                              </tr>  
                            @endfor						  
						</tbody>                        
                    </table>                        
        		</div>
        	</div>
            </div>            
        </div>   
    </fieldset>

</div>
<div class="col-sm-5">
    
    <div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-money"></i> Payment Details
			</div>
			<div class="tools">
			
			</div>
		</div>
		<div class="portlet-body form">
			
				<div class="form-body">
					<div class="form-group has-error">
						<label class="col-md-6 bold control-label">Total Cash Received</label>
						<div class="col-md-6">
							<div class="input-icon right">
								<i class="fa fa-exclamation tooltips" data-original-title="Email address" data-container="body"></i>
								<input id="total-cash" name="cash" type="text" class="form-control numberonly text-right bold total-cash" value="0.00">
							</div>
						</div>
					</div>
					<div class="form-group has-info">
						<label class="col-md-6 bold control-label">Online Payment</label>
						<div class="col-md-6">
							<div class="input-icon right">
								<i class="fa fa-exclamation tooltips" data-original-title="Email address" data-container="body"></i>
								<input id="total-online-debit" name="total-online-debit" type="text" class="form-control numberonly text-right bold total-cash" value="0.00">
							</div>
						</div>
					</div>
					<div class="form-group has-warning">
						<label class="col-md-6 control-label bold">Total Check Received</label>
						<div class="col-md-6">
							<div class="input-icon right">
								<i class="fa fa-exclamation tooltips" data-original-title="You look OK!" data-container="body"></i>								
                                <span class="form-control text-right bold" id="total-check">0.00</span>
							</div>
						</div>
					</div>
				    <div class="form-group has-warning">
						<label class="col-md-6 control-label bold">Total Card Received</label>
						<div class="col-md-6">
							<div class="input-icon right">
								<i class="fa fa-exclamation tooltips" data-original-title="You look OK!" data-container="body"></i>								
                                <span class="form-control text-right bold" id="total-card" >0.00</span>
							</div>
						</div>
					</div>
                    
                    <div class="form-group has-error">
						<label class="col-md-6 control-label bold">Amount to be Paid...</label>
						<div class="col-md-6">
							<div class="input-icon right">
								<i class="fa fa-exclamation tooltips" data-original-title="You look OK!" data-container="body"></i>								
                                <span class="form-control text-right bold text-danger " id="total-amountdue" >{{$due}}</span>
							</div>
						</div>
					</div>
                    
                    <hr style="margin-bottom: 10px; margin-top: 4px;" />
                
					<div class="form-group has-info">
						<label class="col-md-6 control-label bold">Change</label>
						<div class="col-md-6">
							<div class="input-icon right">
								<i class="fa fa-times tooltips" data-original-title="please write a valid email" data-container="body"></i>
								<span class="form-control text-right bold" id="total-change" >0.00</span>
							</div>
						</div>
					</div>
				</div>			
                <input type="hidden" id="paytype" name="type" value="{{encode($type)}}" />
                <input type="hidden" id="payidno" name="idno" value="{{($idno)}}" />
                <input type="hidden" id="paytxn" name="txn" value="{{encode($txn)}}" />
                <input type="hidden" id="payref" name="ref" value="{{$ref}}" />                			
		</div>	                
    </div>
        <input type="checkbox" id="validate" name="validate" checked=""  /> Validate Current Transaction. <br />
        <input type="checkbox" id="post" name="post" checked="" /> Post Payment After Saved. <br />
        <input type="checkbox" id="print" name="print" checked="" /> Print Official Receipt After Saved.
        <input type="hidden" id="amtinwrd" name="amtinwrd" value="" /> 
    
</div>
</form>
</div>