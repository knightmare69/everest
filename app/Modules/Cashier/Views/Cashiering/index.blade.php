<style>
.profile{
    float: left;
    width: 150px;
    margin-right: 20px;
}
.content{
    overflow: hidden;
}
</style>
<script type="text/javascript">
 var php_yrs = <?php echo date('Y');?>;
 var php_mon = <?php echo date('m');?>;
 var php_day = <?php echo date('d');?>;
 var php_hrs = <?php echo date('H');?>;
 var php_min = <?php echo date('i');?>;
 var php_sec = <?php echo date('s');?>;
</script>
<div class="portlet light hidden-print" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-money"></i>
			<span class="caption-subject bold uppercase"> Cashiering </span>
			<span class="caption-helper hidden-xs">Use this module to accept payment/s.</span>
        </div>
        
		<div class="actions hidden-xs ">
            <div class="portlet-input hide input-inline input-medium ">
                <div class="input-group  ">
	             <input type="text" class="form-control input-sm " id="orsearch" placeholder="Search O.R"  />
                 <span class="input-group-btn ">
			     <button class="btn btn-default btn-sm" style="height: 28px;" data-menu="orsearch" type="button">Go!</button>
			     </span>
               </div>               
            </div>            
            <?php
			$this->permission = new Permission('cashiering');
			$get = Request::all();
			$orno = '';
			$stdn = '';
			$payor= '';
			$xref = '';
			$xtxn = '';
			$xorn = '';
			$xdt  = '';
			$prtcl= '';
			
			
			if(getObjectValue($get,'edit')!='' && getObjectValue($get,'edit')!=false){
			  $orno = getObjectValue($get,'edit');
			  $edit = DB::SELECT("SELECT * FROM ES_OfficialReceipts WHERE ORNo='".$orno."'");
			  if($edit && count($edit)>0){
				$xorn = $edit[0]->ORNo;
				$xdt  = $edit[0]->Date;
				$stdn = $edit[0]->PayorID;
				$payor= $edit[0]->PayorName;
			    $xref = $edit[0]->RefNo;
			    $xref = (($xref=='')?'':encode($xref));
				$ptype= $edit[0]->PayorType;
			    $xtxn = $edit[0]->TransType;
			    $prtcl= $edit[0]->Particular;
			    $xtxn = (($xtxn=='')?1:$xtxn);
				echo '<span data-orno="'.$xorn.'" data-txn="'.$xtxn.'" data-ptype="'.$ptype.'" data-date="'.date('Y-m-d H:i:s',strtotime($xdt)).'" 
				            data-xdate="'.date('m/d/Y',strtotime($xdt)).'" data-xtime="'.date('h:i:s A',strtotime($xdt)).'"
				            data-id="'.$stdn.'" data-payor="'.$payor.'" data-ref="'.$xref.'" data-particular="'.$prtcl.'"></span>';
				
				echo '<a href="'.url('cashier/cashiering').'" class="btn btn-sm btn-success" data-id="'.$stdn.'" data-ref="'.$xref.'" data-txn="'.$xtxn.'" data-menu="new" data-original-title="New" title="New"><i class="fa fa-file"></i> New</a>&nbsp;&nbsp;';        
				
				if($this->permission->has('edit')){
			    echo '<a href="javascript:void();" class="btn btn-sm btn-info" data-id="'.$stdn.'" data-ref="'.$xref.'" data-txn="'.$xtxn.'" data-menu="edit" data-original-title="Edit" title="Edit"><i class="fa fa-edit"></i> Edit</a>&nbsp;&nbsp;';        
				}
				
				if($this->permission->has('delete')){
                echo '<a href="javascript:void();" class="btn btn-sm btn-danger" data-id="'.$orno.'" data-menu="void" data-original-title="" title="Delete"><i class="fa fa-times"></i> Void</a>        
                      <a href="javascript:void();" class="btn btn-sm btn-danger" data-id="'.$orno.'" data-menu="delete" data-original-title="" title="Void"><i class="fa fa-trash-o"></i> Delete</a>';
				}	  
			  }
			}
			?>
            <a href="{{url('cashier/official-receipts')}}" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Open"><i class="fa fa-folder"></i></a>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row">
            <div class="col-sm-9">
                <div class="profile">
                    <img id="stud_photo" class="entryphoto pull-left" width="150" height="180" src="<?php echo url('assets/system/media/images/no-image.png').'?'.rand(0,9999);?>"/>
                </div>
                <div class="content">
                    @include($views.'profile')
                </div>
            </div>
            <div class="col-sm-3 util-btn-margin-bottom-5">
                    <button class="btn btn-sm pull-right btn-info" type="button" data-menu="setup" ><i class="fa fa-cog"></i> Setup</button>
                    <h3 class="bold" style="margin-bottom: 5px; margin-top: 0px;">
                        O.R. No : <span id="or_num" class="font-red">{{$orno}}</span>
                    </h3>
                    <h5 class="bold" >
                        O.R. Date : <span id="or_date"  data-date="{{systemDate()}}" class="font-red">{{systemDate()}}</span>
                    </h5>
                    <h5 class="bold" >
                        Currency : 
                        <select id="or_curr" name="or_curr" class="input-small">
                            <option value="1">PHP</option>
                            <option value="2">USD</option>
                        </select>
                    </h5>                                        
                    <hr />
                    <div class="clearfix">
                        <button class="btn btn-sm btn-success" type="button" data-menu="full"><i class="fa fa-money"></i> Full Payment</button>
                        <button class="btn btn-sm btn-info" type="button" data-menu="auto"><i class="fa fa-calculator"></i> Auto Distribute</button>
                        <button class="btn btn-sm bg-yellow-crusta" type="button" data-menu="schedule"><i class="fa fa-calendar"></i> Payment Schedule</button>
                        <button class="btn btn-sm bg-info purple" type="button" data-menu="online"><i class="fa fa-table"></i> Online Payment</button>
                    </div>
              </div>
        </div>
		<div class="alert alert-danger alert-posted hidden"><i class="fa fa-warning"></i> Transaction is already posted!</div>
        <div id="transaction_details" style="margin-bottom: 5px;">
		  @include($views.'list')
        </div>
        <div class="row">
            <div class="col-xs-5">
                <a data-menu="soa" class="btn btn-md green hidden-print margin-bottom-5">S.O.A</a>
                <a id="ledger" href="{{url('accounting/studentledger' )}}" class="btn btn-md blue hidden-print margin-bottom-5">Ledger</a>
                <a id="balance_summary" class="btn btn-md purple hidden-print margin-bottom-5">Summary of Balance</a> |
                <a data-menu="template" class="btn btn-md red-pink hidden-print margin-bottom-5">Fees Template</a>
            </div>
            <div class="col-xs-4">
                <div class="input-group">
                    <span class="input-group-addon">
    				    Particular
    				</span>
                    <input type="text" id="particular" name="particular" class="form-control bold" value="" placeholder="Particular" />

                </div>
            </div>
            <div class="col-xs-3 text-right invoice-block">

                <a class="btn btn-lg blue hidden-print margin-bottom-5" data-menu="print-receipt" disabled>Print <i class="fa fa-print"></i></a>
                <a class="btn btn-lg green hidden-print margin-bottom-5" data-menu="accept">Accept Payment <i class="fa fa-check"></i></a>
            </div>
        </div>
	</div>
</div>
@include($views.'sub.setup')
@include($views.'sub.add')
@include($views.'sub.online-validation')
<div class="for_printing">
</div>