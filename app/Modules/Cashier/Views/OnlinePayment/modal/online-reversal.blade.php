<?php
$_mid       = "00000009052092F0B682"; 
$cert       = "F22D593D5402073A22CE0AB361D5AA3A"; //<-- your merchant key
$purl       = "https://testpti.payserv.net/pnxquery/queryservice.asmx";
$_ipaddress = "122.2.22.163";

$payment    = ((isset($rspayment) && $rspayment)?$rspayment:array());
$payinfo    = (($payment && count($payment)>0)?($payment[0]):array());
$userid     = getObjectValue($payinfo,'UserID');

$_requestid = getObjectValue($payinfo,'RequestID');
$_reference = getObjectValue($payinfo,'ReferenceNo');
$regid      = getObjectValue($payinfo,'EntryID');
$_amount    = getObjectValue($payinfo,'TotalAmount');

if($userid && $userid!=''){
  $tmpusr     = DB::SELECT("SELECT * FROM ESv2_Users WHERE UserIDX='".$userid."'");	
  $usrinfo    = (($tmpusr && count($tmpusr)>0)?($tmpusr[0]):array());
}else{
  $usrinfo = array();
}   
?>
<div class="row">
	<div class="col-sm-12">
		<p>Are you sure you want to reverse this transaction?</p>
	</div>
	<div class="col-sm-6">
		<label class="control-label">Transaction Code:<b><?php echo getObjectValue($payinfo,'RequestID');?></b></label>
	</div>
	<div class="col-sm-6">
		<label class="control-label">Paygate RefNo:<b><?php echo getObjectValue($payinfo,'ReferenceNo');?></b></label>
	</div>
	<div class="col-sm-6">
		<label class="control-label">Transaction By:<b><?php echo getObjectValue($usrinfo,'FullName');?></b></label>
	</div>
	<div class="col-sm-6">
		<label class="control-label">Date:<b><?php echo date('m/d/Y',strtotime(getObjectValue($payinfo,'DateCreated')));?></b></label>
	</div>
	<div class="col-sm-12">
		<label class="control-label">Amount:<strong><?php echo number_format(floatval(getObjectValue($payinfo,'TotalAmount')),2);?></strong></label>
	</div>
	<div class="col-sm-12"><hr/></div>
	<div class="col-sm-6">
		<label class="control-label">Status Code:<strong><?php echo getObjectValue($payinfo,'ResponseCode');?></strong></label>
	</div>
	<div class="col-sm-6">
		<label class="control-label">Message:<strong><?php echo getObjectValue($payinfo,'ResponseMessage');?></strong></label>
	</div>
	<div class="col-sm-12"><hr/></div>
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table table-bordered table-condense" id="paydetail" data-id="<?php echo getObjectValue($payinfo,'EntryID');?>"  data-request="<?php echo getObjectValue($payinfo,'RequestID');?>">
				<thead>
					<th>Student No</th>
					<th>Name</th>
					<th>Amount</th>
					<th>ORNo</th>
					<th>Date</th>
				</thead>
				<tbody>
					<?php
					  foreach($payment as $r){
					    $class = (($r->ORNo!='')?'danger':'info');	
						echo '<tr class="'.$class.'" data-studno="'.$r->StudentNo.'" data-orno="'.$r->ORNo.'">
								<td>'.$r->StudentNo.'</td>
								<td>'.$r->FullName.'</td>
								<td class="text-right">'.number_format($r->TotalAmount,2).'</td>
								<td>'.$r->ORNo.'</td>
								<td>'.$r->ORDate.'</td>
						      </tr>';
					  }
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-12 hidden">
	  <?php
        $_noturl    = url('epayment/notify/'.$regid); // url where response is posted
		$_resurl    = url('epayment/process/'.$regid); //url of merchant landing page
		
		$_cancelurl = url('epayment/cancel');
		$_amount    = number_format($_amount,2, '.', '');
		
		$forSign    = $_mid . $_requestid . $_reference . $_ipaddress . $_noturl . $_resurl .  $_amount;
		$_sign      = hash("sha512", $forSign.$cert);
		
		$strxml     = "";	    
			
		$strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
		$strxml = $strxml . "<Request>";
		$strxml = $strxml . "<merchantid>" . $_mid . "</mid>";
		$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
		$strxml = $strxml . "<org_trxid>" . $_reference . "</<org_trxid>";
		$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
		$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
		$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
		$strxml = $strxml . "<amount>" . $_amount . "</amount>";
		$strxml = $strxml . "<signature>" . $_sign . "</signature>";
		$strxml = $strxml . "</Request>";
		$b64string =  base64_encode($strxml);
	   ?>
	   <form name="form1" method="post" action="<?php echo $purl;?>" target="_blank">
		<input type="hidden" name="reversalrequest" id="reversalrequest" value="<?php echo $b64string ?>"/>
		<button class="btn btn-success btnsubmit">Testing</button>
	   </form> 	 
	</div>
</div>