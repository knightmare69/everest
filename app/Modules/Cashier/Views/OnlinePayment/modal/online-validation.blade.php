<div id="online-validation" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Validate Online Transaction</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-12 col-xs-12 col-sm-12 notice-bar">
                            <h4>Notice : <span id="notice-message"></span></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-7">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="student-information">
                                        <h4>Student Name : <span id="student-name"></span></h4>
                                    </div>
                                    <div class="student-school-id">
                                        <h4>School ID # : <span id="school-id"></span></h4>
                                    </div>
                                    <div class="student-school-term">
                                        <h4>School Term : <span id="school-term"></span></h4>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="amount-payed">
                                        <h4>Amount : &#8369;<span id="amount-payed"></span></h4>
                                    </div>
                                    <div class="payor">
                                        <h4>Payor : <span id="amount-payor"></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>

{{-- <div id="online-validation" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body text-center">
            <div class="sa">
              <div class="sa-success">
              <div class="sa-success-tip"></div>
              <div class="sa-success-long"></div>
              <div class="sa-success-placeholder"></div>
              <div class="sa-success-fix"></div>
              </div>
            </div>
          
            <h1 class="heading-message" >Congratulations!</h1>
            <p class="content-message">
              Your application was successfully submitted
              <br>
              Kindly check your email for verification and status of your application.
            </p>
          
            <h3 class="priority-message">
              Your Pooling number is <strong>202004004</strong>
           </h3>
          
          <div class="info-message">
            Please take note of your pooling number.
          </div>
          
          <div class="footer-message">
              <a href="" class="btn btn-primary">Return home</a>
          </div>
        </div>
      </div>
    </div>
</div> --}}