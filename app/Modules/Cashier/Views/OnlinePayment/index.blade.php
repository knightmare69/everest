<style>
.dataTables_filter{
    float: right!important;
}

.trinfo td{
 background-color:white !important;
}

.trinfo .active-row td{
 background-color:white !important;
}
</style>
<div class="portlet light">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase">Online Payment Account</span>
        </div>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="javascript:void(0)" data-target="#transaction-records" data-toggle="tab">Transaction Records</a>
            </li>
            
            <li class="hidden">
                <a href="javascript:void(0)" data-target="#reports-collected" data-toggle="tab">List of Reports Collected</a>
            </li>
            <li class="hidden">
                <a href="javascript:void(0)" data-target="#bills" data-toggle="tab">Print Bills</a>
            </li>
        </ul>
    </div>
    <div class="portlet-body">
        <div class="tab-content">
            <div class="tab-pane active" id="transaction-records">
                <div class="row">
				   <form class="" id="frm_search">
                    <div class="col-sm-4 col-md-3">
                        <label class="control-label">Payment Portals</label>
                        <div class="input-group" style="width: 100%;">
                            <select id="portal-lists" name="portal-lists" class="form-control">
                                <option value="" data-info=""> - Select Payment Portals - </option>
                                <option value="paynamics" data-info="paynamics" selected>Paynamics</option>
                            </select>
                        </div>
                        <div id="portal-lists-error" class="invalid-feedback hidden">
                            Please choose payment portal.
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <label class="control-label">Date from :</label>
                        <input class="form-control" type="date" id="date-from" name="date-from" value="<?php echo date('Y-m-01');?>">
                        <div id="date-from-error" class="invalid-feedback hidden">
                            Please choose date from.
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <label class="control-label">Date to :</label>
                        <input class="form-control" type="date" id="date-to" name="date-to" value="<?php echo date('Y-m-t');?>">
                        <div id="date-to-error" class="invalid-feedback hidden">
                            Please choose date to.
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <label class="control-label" >Payment Status</label>
                        <div class="input-group" style="width: 100%;">
                            <select class="form-control" id="payment-status" name="payment-status">
                                <option value=""> - Select Status - </option>
                                <option value="0">To Transact</option>
                                <option value="-2">Pending</option>
                                <option value="1">Verified</option>
                                <option value="2">Re-verify</option>
                                <option value="3">Error</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="col-sm-9">
						<label class="control-label">Transaction Status</label>
                        <div class="input-group" style="width: 100%;">
                            <select class="form-control" id="transaction-status" name="transaction-status">
                                <option selected="selected" value="all">All</option>
                                <option value="used">Used</option>
                                <option value="unused">Unused</option>
                            </select>
                        </div>
						</div>
						<div class="col-sm-3">
						    <label class="control-label">&nbsp;&nbsp;</label>
							<a class="btn btn-warning btnfilter" href="javascript:void(0);"><i class="fa fa-search"></i></a>
						</div>
                    </div>
				  </form>
                </div>
                <div class="row hidden">
                    <div class="col-md-push-9 col-md-3" style="margin: 17px 0px;">
                        <label class="control-label">Search Transaction Code</label>
                        <div class="input-group" style="width: 100%;">
                            <input class="form-control" type="text" id="search-tx-code">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><br/></div>
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover table_scroll dataTable" id="onlinepayment_rectable">
                            <thead>
                                <tr>
                                    <th class="text-center">Action</th>
                                    <th class="text-center">OR Number</th>
                                    <th class="text-center">Cashier Name</th>
                                    <th class="text-center">Transaction Code</th>
                                    <th class="text-center">Student Name</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Payment Status</th>
                                    <th class="text-center">Date Transact</th>
                                </tr>
                            </thead>
							<tbody>
								<tr><td colspan="8" class="text-center">Loading..</td></tr>
							</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="reports-collected">
                @include($views.'tabs.list')
            </div>
            <div class="tab-pane" id="bills">
                @include($views.'tabs.print')
            </div>
            
        </div>
    </div>
</div>
@include($views.'modal.online-validation')