<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
</HEAD>
<BODY>
<style>
    a {text-decoration:none}
    a img {border-style:none; border-width:0}
    .fcibqn52s90ps-0 {font-size:34pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fcibqn52s90ps-1 {font-size:7pt;color:#000000;font-family:Arial;font-weight:normal;}
    .Report-Title {font-size:14pt;color:#000000;font-family:Old English Text MT;font-weight:normal;}
    .fcibqn52s90ps-3 {font-size:9pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fcibqn52s90ps-4 {font-size:11pt;color:#000000;font-family:Swis721 Cn BT;font-weight:normal;}
    .fcibqn52s90ps-5 {font-size:8pt;color:#000000;font-family:Arial;font-weight:bold; text-indent: 2px;}
    .fcibqn52s90ps-6 {font-size:8pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fcibqn52s90ps-7 {font-size:8pt;color:#000000;font-family:Arial Narrow;font-weight:normal; text-indent: 15px;}
    .fcibqn52s90ps-8 {font-size:5pt;color:#000000;font-family:Arial;font-weight:normal; text-indent: 2px;}
    .fcibqn52s90ps-9 {font-size:6pt;color:#000000;font-family:Arial;font-weight:normal; text-indent: 2px;}
    .fcibqn52s90ps-10 {font-size:8pt;color:#000000;font-family:Arial Narrow;font-weight:normal; text-indent: 2px;}
    .fcibqn52s90ps-11 {font-size:8pt;color:#000000;font-family:Arial Narrow;font-weight:bold; text-indent: 2px;}
    .adibqn52s90ps-0 {border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0; text-indent:3px;}
    .adibqn52s90ps-1 {border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0;}
    .table{ border-spacing: 0px; border-collapse: collapse ; }
    .table th,.table td{ border: 1px solid black; display: table-cell; box-sizing: content-box; }
    .table tbody > tr {height: 18px !important;}
    .bold { font-weight: bold;}
    @media  print {
        .page-break{    page-break-after: always; }
    }
    .report-header{ width: 768px !important; }
</style>

<title>Summary of Billing and Collection by Department</title>   
    <center class="report-header" style="margin-top: 15px;">
        <div style="position: absolute; left: 100px;">
            <img src="http://localhost/cirrusk12/public/assets/admin/layout/img/report-logo.png" border="0" style="float: left;" width="75px" height="75px" />
        </div>
        <span align="center" class="Report-Title">PRINCE TECHNOLOGIES CORPORATION</span>
    </center>
    
    <center class="report-header" >
        <span align="center"  class="fcibqn52s90ps-1">B2 L17 Royal South Townhomes, Talon 5 Las Pi&ntilde;as City, Philippines</span>
    </center>
    
    <center class="report-header">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <td align="center">
                <span class="fcibqn52s90ps-1">&nbsp;</span>
            </td>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <td align="center">
                <span class="fcibqn52s90ps-1">&nbsp;</span>
            </td>
        </table>
    </center>
    <center class="report-header" style="margin-bottom: 7px; margin-top: 5px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center"><span class="fcibqn52s90ps-4">SUMMARY OF BILLING AND COLLECTION BY DEPARTMENT</span></td>
            </tr>
            <tr>
                <td align="center"><span  class="fcibqn52s90ps-3">School Year 2019-2020</span></td>
            </tr>
            <tr>
                <td align="center"><span  class="fcibqn52s90ps-3">Campus : {{ getInstitutionName() }}</span></td>
            </tr>
            <tr>
                <td align="center"><span  class="fcibqn52s90ps-3">Date from : {{ date('d/F/Y', strtotime($date_from)) }} &nbsp;Date to : {{ date('d/F/Y', strtotime($date_to)) }}</span></td>
            </tr>
            <tr>
                <td align="center"><span  class="fcibqn52s90ps-3">Date issued : {{ date('d/F/Y', strtotime($date_today)) }}</span></td>
            </tr>
                
        </table>
    </center>
        
            
    <center class="report-header">
        <table class="table" width="751px" cellpadding=0 cellspacing=0 >
            <thead>
                <tr>
                    <th width="20"  class="fcibqn52s90ps-6" style="width: 20px;"> # </th>
                    <th width="311"  class="fcibqn52s90ps-6" style="width: 311px;">TRANSACTION CODES</th>
                    <th width="120"  class="fcibqn52s90ps-6" style="width: 120px;">RECEIVED BY</th>
                    <th width="120"  class="fcibqn52s90ps-6" style="width: 120px;">AMOUNT COLLECTED</th>
                    <th width="120"  class="fcibqn52s90ps-6" style="width: 120px;">PAYMENT RECEIVED</th>            
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $info)
                    <tr nobr=true >
                        <td width="20" style="width: 20px;" class="seq fcibqn52s90ps-8" align="center">{{ $index++ }}</td>
                        <td width="311" style="width: 311px;" class="sur fcibqn52s90ps-7">{{ $info->transaction_code }}</td>
                        <td width="120" style="width: 120px;" align="right" class="sex fcibqn52s90ps-10">{{ $info->cashier_name }}&nbsp;&nbsp;</td>
                        <td width="120" style="width: 120px;" align="right" class="sex fcibqn52s90ps-10">{{ $info->date_payed }}&nbsp;&nbsp;</td>
                        <td width="120" style="width: 120px;" align="right" class="sex fcibqn52s90ps-10">{{ $info->collection_amount }}&nbsp;&nbsp;</td>
                    </tr>    
                @endforeach
                <tr nobr=true class="bold" >                
                    <td width="331" colspan="2" style="width: 331px;" align="right" class="sur fcibqn52s90ps-11">Total : </td>
                    <td width="120" style="width: 120px;" align="right" class="sex  fcibqn52s90ps-11">&nbsp;&nbsp;</td>
                    <td width="120" style="width: 120px;" align="right" class="sex  fcibqn52s90ps-11">&nbsp;&nbsp;</td>
                    <td width="120" style="width: 120px;" align="right" class="sex  fcibqn52s90ps-11">{{ $total }}&nbsp;</td>
                </tr
            </tbody>
        </table>
    
    </center>
    
    
    <center class="report-header">
    <div  style="text-align: center; margin-top: 40px; width:751px ">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="50%"><span class="fcibqn52s90ps-1">Date&nbsp;Printed : {{ $date_today }}</span></td>
                <td width="50%" align="right"><span align="center" class="fcibqn52s90ps-1">Page 1 of 1</span></td>
            </tr>
        </table>
    </div>
    </center>
    <div class="page-break"></div>
    
    </BODY>
    </HTML>