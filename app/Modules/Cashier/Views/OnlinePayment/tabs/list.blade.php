<div class="row">
    <div class="col-md-3">
        <label class="control-label">Payment Portals</label>
        <div class="input-group" style="width: 100%;">
            <select id="report-portal-lists" name="report-portal-lists" class="form-control">
                <option value="" data-info=""> - Select Payment Portals - </option>
            </select>
        </div>
        <div id="report-portal-lists-error" class="invalid-feedback hidden">
            Please choose payment portal.
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Date Created from :</label>
        <div class="input-group" style="width: 100%">
            <input class="form-control" type="date" id="report-date-from">
        </div>
        <div id="date-from-error" class="invalid-feedback hidden">
            Please choose date from.
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label">Date Created to :</label>
        <div class="input-group" style="width: 100%">
            <input class="form-control" type="date" id="report-date-to">
        </div>
        <div id="date-to-error" class="invalid-feedback hidden">
            Please choose date to.
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover table_scroll dataTable" id="onlinepayment_reporttable">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Payment From</th>
                    <th>Payment To</th>
                    <th>Reports Name</th>
                    <th>Collection For</th>
                    <th>Payment Portal</th>
                    <th>Date Created</th>
                </tr>
            </thead>
        </table>
    </div>
</div>