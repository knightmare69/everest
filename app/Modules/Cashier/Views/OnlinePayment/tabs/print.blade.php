<div class="row">
    <div class="col-md-4">
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Generate Bill
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Payment Portals</label>
                            <div class="input-group" style="width: 100%;">
                                <select id="bill-portal-lists" name="bill-portal-lists" class="form-control">
                                    <option value="" data-info=""> - Select Payment Portals - </option>
                                </select>
                            </div>
                            <div id="bill-portal-lists-error" class="invalid-feedback hidden">
                                Please choose payment portal.
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Date Payment from :</label>
                            <div class="input-group" style="width: 100%">
                                <input class="form-control" type="date" id="bill-date-from">
                            </div>
                            <div id="bill-date-from-error" class="invalid-feedback hidden">
                                Please choose date from.
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Date Payment to :</label>
                            <div class="input-group" style="width: 100%">
                                <input class="form-control" type="date" id="bill-date-to">
                            </div>
                            <div id="bill-date-to-error" class="invalid-feedback hidden">
                                Please choose date to.
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Bill for</label>
                            <div class="input-group" style="width: 100%;">
                                <select id="bill-persona" name="bill-persona" class="form-control">
                                    <option value="" data-info=""> - Select Bill for - </option>
                                    <option value="ptc-bill">PTC</option>
                                    <option value="school-bill">School</option>
                                </select>
                            </div>
                            <div id="bill-persona-error" class="invalid-feedback hidden">
                                Please select collection for.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions right" style="margin-top: 20px;">
                    <button type="submit" id="preview-bill" class="btn green">Preview</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Review Bill Report
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <button type="submit" id="print-btn-bill" class="btn green" style="display: none"><i class="fa fa-print"></i>&nbsp;Print</button>
                    </div>
                </div>
            </div>
            <div class="portlet-body" id="print-preview">
                
            </div>
        </div>
    </div>
</div>