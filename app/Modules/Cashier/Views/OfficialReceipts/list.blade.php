<?php
    $j=1;

    $data = isset($data) ? $data : null;

    $cash = 0;
    $check = 0;
    $card = 0;
    $change = 0;
    $totaldue = 0;
    $discount=0;

    $descripancy = 0;
    $status = "";
    $payer_type = "";
    $txn='';

?>
<div class="table-scrollable"  >
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="">#</th>
            <th class="text-center autofit">Action</th>
            <th class="text-center autofit">Posted</th>
            <th class="text-center">O.R. Details</th>
            <th>Payer Info</th>
            <th class="">Transaction</th>
            <th class="text-center info ">Amount<br />Due</th>
            <th class="text-center">Cash<br />Received</th>
            <th class="text-center">Check<br />Received</th>
            <th class="text-center">Card<br />Received</th>
            <th class="text-center">Change</th>
            <th class="text-center">Descripancy</th>
            <th class="text-center">Cashier</th>
            <th class="text-center">Date Created</th>
            <th class="text-center">Terminal</th>
            <th class="text-center">Status</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
            @foreach($data as $r)
                <?php

                    $cash = $r->CashReceive;
                    $check = $r->CheckReceive;
                    $card = $r->CardReceive;
                    $change = $r->Change;
                    $descripancy = 0;

                    $descripancy = ($cash + $check + $card) - $r->Descripancy;

                    if( $descripancy > 0){
                        $descripancy = '('.number_format($descripancy,2).')';
                    }

                    if($r->ForPosting == '1'){
                        $status="Unposted";
                    }else{
                        $status="Posted";
                    }
                    if($r->PayorType != ''){
                        $payer_type = get_PayerType($r->PayorType);
                        //$payer_type = $r->PayorType;
                    }else{
                        $payer_type = "";
                    }

                    $txn = get_TransactionType($r->TransType)->TransName;

                ?>

            <tr data-receipt="{{$r->ORNo}}" data-void="{{$r->IsVoid}}" data-type="{{$r->PayorType}}" data-txn="{{$r->TransType}}" >
                <td class="font-xs autofit bold">{{$j}}.</td>
                <td class="font-xs center bold"><a  class="fa fa-print" href="{{ url()}}/cashier/cashiering/print?or={{$r->ORNo}}"  target="_blank"> Print</a></td>
                <td class="text-center autofit">
                    @if($status=='Posted')
                    <input type="checkbox" class="box" checked="" />
                    @else
                        <input type="checkbox" class="box" />
                    @endif
                </td>
                <td class="autofit">
                OR #<span class="bold"><a href="{{url('cashier/cashiering?edit='.$r->ORNo)}}" target="_blank">{{$r->ORNo}}</a></span><br />
                {{$r->Date}}</td>
                <td class="autofit">{{$r->PayorName}}<br /> <span class="font-xs">{{$payer_type}} : {{$r->PayorID}}</span></td>
                <td class="autofit" >{{$txn}} : {{$r->RefNo}}</td>
                <td class="autofit text-right bold info ">{{number_format($r->AmountDue,2)}}</td>
                <td class="autofit text-right  ">{{number_format($cash,2)}}</td>
                <td class="autofit text-right  ">{{number_format($check,2)}}</td>
                <td class="autofit  bold text-right">{{number_format($card,2)}}</td>
                <td class="autofit  bold text-right">{{number_format($change,2)}}</td>
                <td class="autofit  bold text-right">{{number_format(floatval($descripancy),2)}}</td>
                <td>{{$r->CashierID}}</td>
                <td>{{$r->CreatedDate}}</td>
                <td>{{$r->CreatedTerminal}}</td>
                <td>{{$status}}</td>
            </tr>
            <?php $j++; ?>


            @endforeach
        @else
        <tr>
            <td colspan="14">There are no data at this moment...</td>
        </tr>
        @endif

    </tbody>
</table>
</div>
