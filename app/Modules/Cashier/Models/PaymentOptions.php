<?php

namespace App\Modules\Cashier\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class PaymentOptions extends Model
{
    protected $table = 'es_paymentoptions';
    protected $primaryKey = 'PaymentOptionID';
    
   	protected $fillable  = array(
	   'PaymentOptionName'
      ,'PaymentOptionName'
      ,'PaymentDescription'
      ,'SeqNo'
      ,'Inactive'
      ,'1stPayment'
      ,'2ndPayment'
      ,'3rdPayment'
      ,'4thPayment'
      ,'5thPayment'
      ,'6thPayment'
      ,'7thPayment'
      ,'8thPayment'
      ,'9thPayment'
      ,'10thPayment'
      ,'NumPayment'
      ,'IsDefault'
      ,'PaymentSchemeID'       
	);

	public $timestamps = false;        
}