<?php

namespace App\Modules\Cashier\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class OtherTransactions extends Model
{
    protected $table = 'es_othertransactions';
    protected $primaryKey = 'RefNo';
    
   	protected $fillable  = array(
	   'TransType'
      ,'TermID'
      ,'CampusID'
      ,'Date'
      ,'TblFeeID'
      ,'Remarks'
      ,'IDType'
      ,'IDNo'
      ,'ORNo'
      ,'AssessedBy'
      ,'AssessedDate'      
      ,'ModifiedBy'
      ,'ModifiedDate'
      ,'ValidatingOfficerID'
      ,'ValidationDate'
      ,'IsVoid'
      ,'VoidDate'
      ,'TotalNetAssessed'
      ,'TotalPayment'
      ,'TotalDiscount'
      ,'TotalNonLedger'
      ,'TotalRefund'
      ,'SchoProviderType'
      ,'SchoProviderID'
      ,'TotalDebitMemo'
      ,'RemovedValidationBy'
      ,'RemovedValidationDate'
      ,'RegID'
      ,'BillingNotes'
      ,'ProgID'
       ,'YrLvlID'
       ,'Percentage'
       ,'SchoOptionID'
       ,'SchoAccounts'
       ,'GrantTemplateID'
       ,'SchoFundType'      
       ,'ApplyOnNet'       
       ,'ApplyOnTotal'       
	);

	public $timestamps = false;
    
    
}