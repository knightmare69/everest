<?php
namespace App\Modules\Cashier\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\OnlinePayment\Models\OnlinePaymentRecords as TransactionRecords;
use App\Modules\OnlinePayment\Models\PaymentCollected as PaymentCollected;
use App\Modules\OnlinePayment\Models\ChargeRecords as ChargeRecords;
use App\Modules\OnlinePayment\Models\Reports as PaymentReports;

use Carbon\Carbon as Carbon;
use Request;
use DB;

use SoapClient;

class OnlineTransactions extends Controller
{
    private $media =
		[
			'Title'=> 'Online Transactions',
			'Description'=> 'Welcome to Online Transaction Pages!',
		    'closeSidebar'  => true,
            'js'		=> ['Cashier/online-txn.js'],
            'init'		=> [],
			'plugin_js'	=> [
                'bootbox/bootbox.min',
                'bootstrap-select/bootstrap-select.min',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-datepicker/js/bootstrap-datepicker',
                'select2/select2.min',
			],
			'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'smartnotification/smartnotification',
                'online-payment/styles'
			]
        ];
    
    private $url = [ 'page' => 'cashier/online-transactions/' ];
        
    private $views = 'Cashier.Views.OnlinePayment.';
    
    public function index(){
		return view('layout',array(
			'content'=>view($this->views.'index')->with(['views'=>$this->views]),
			'url'=>$this->url,
			'media'=>$this->media,
		));
	}

	private function getDataTable($mode='', $filter='', $page=1, $take=10,$skip=0){
        ini_set('max_execution_time', 120);
		$whereRaw = "";
        $rs = null;
		$post = Request::all();
		
		if(getObjectValue($post,'date-from')!='' && getObjectValue($post,'date-to')!=''){
		   $dfrom = date('Y-m-d',strtotime(getObjectValue($post,'date-from')));
		   $dend  = date('Y-m-d',strtotime(getObjectValue($post,'date-to')));
		   $whereRaw .= "(p.DateCreated BETWEEN '".$dfrom."' AND '".$dend."')";
		}else{
		   $dfrom = date('Y-m-01');
		   $dend  = date('Y-m-t');
		   $whereRaw .= "(p.DateCreated BETWEEN '".$dfrom."' AND '".$dend."')";
		}
		
	    if(getObjectValue($post,'payment-status')!=''){
		 $whereRaw .= " AND (p.StatusID = '".getObjectValue($post,'payment-status')."')";
	    }
		
		if(getObjectValue($post,'transaction-status')!=''){
		 if(getObjectValue($post,'transaction-status')=='used'){
		 $whereRaw .= " AND (ISNULL(pd.TransactionNo,'')<>'')";
		 }else{
		 $whereRaw .= " AND (ISNULL(pd.TransactionNo,'')='')";
		 }
	    }
		
		if($filter!=''){
		    $whereRaw .= " AND (ORNo LIKE '%".$filter."%' OR PayorID LIKE '%".$filter."%' OR o.PayorName LIKE '%".$filter."%' OR s.FullName LIKE '%".$filter."%')";
		}
		
		if($whereRaw!=''){
			$whereRaw =' WHERE '.$whereRaw;
		}
		

       if($mode == 'total'){
            $rs =  DB::SELECT("SELECT COUNT(pd.StudentNo) as Items 
                                 FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID
                           INNER JOIN ES_Students as s ON pd.StudentNo=s.StudentNo
                            LEFT JOIN ES_OfficialReceipts as o ON pd.TransactionNo=o.ORNo AND pd.StudentNo=o.PayorID ".$whereRaw);
			if($rs && count($rs)>0){
				$rs = $rs[0]->Items;
			}else{
				$rs = 0; 
			}				
        }else{
              $rs = DB::SELECT("SELECT p.EntryID,p.TermID,p.UserID,p.StatusID,p.RequestID,p.ReferenceNo,p.ResponseCode,p.ResponseMessage,p.DateCreated
									  ,pd.DetailID,pd.StudentNo,pd.RegID,pd.TotalAmount,pd.IsTransacted,pd.TransactionNo
									  ,s.FamilyID,s.LastName,s.FirstName,s.MiddleInitial,s.FullName
									  ,o.ORNo,o.Date as ORDate,o.CreatedBy as CashierID  
								  FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID
							INNER JOIN ES_Students as s ON pd.StudentNo=s.StudentNo
							 LEFT JOIN ES_OfficialReceipts as o ON pd.TransactionNo=o.ORNo AND pd.StudentNo=o.PayorID ".$whereRaw." ORDER BY p.DateCreated DESC
								OFFSET ".$skip." ROWS
								 FETCH NEXT ".$take." ROWS ONLY;");
        }
		
		return $rs;
	}
	
	private function soapFunction($func='query'){
		$post       = Request::all();
        $payid      = getObjectValue($post,'entryid');
		$_mid       = "00000009052092F0B682"; 
		$cert       = "F22D593D5402073A22CE0AB361D5AA3A"; //<-- your merchant key
		$purl       = "https://testpti.payserv.net/pnxquery/queryservice.asmx";
		$_ipaddress = "122.2.22.163";
        
		try{
			$client     = new SoapClient("https://testpti.payserv.net/Paygate/ccservice.asmx?wsdl");
			$response   = array();
			
			switch($func){
				case 'refund':
				case 'reversal':
				    
					$payment    = DB::SELECT("SELECT p.EntryID,p.TermID,p.UserID,p.StatusID,p.RequestID,p.ReferenceNo,p.ResponseCode,p.ResponseMessage,p.DateCreated
													,pd.DetailID,pd.StudentNo,pd.RegID,pd.TotalAmount,pd.IsTransacted,pd.TransactionNo
													,s.FamilyID,s.LastName,s.FirstName,s.MiddleInitial,s.FullName
													,o.ORNo,o.Date as ORDate,o.CreatedBy as CashierID  
											   FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID
										 INNER JOIN ES_Students as s ON pd.StudentNo=s.StudentNo
										  LEFT JOIN ES_OfficialReceipts as o ON pd.TransactionNo=o.ORNo AND pd.StudentNo=o.PayorID WHERE p.EntryID='".$payid."'");
					$payinfo    = (($payment && count($payment)>0)?($payment[0]):array());
					$userid     = getObjectValue($payinfo,'UserID');

				  //$_requestid = '5ed4aced2027c';
					$_requestid = substr(uniqid(), 0, 13);
					$_reference = getObjectValue($payinfo,'ReferenceNo');
					$regid      = getObjectValue($payinfo,'EntryID');
					$_amount    = 0;
					
					foreach($payment as $r){
					  $_amount  += floatval(getObjectValue($payinfo,'TotalAmount'));	
					}
					
					$_noturl    = url('epayment/notify/'.$regid); // url where response is posted
					$_resurl    = url('epayment/process/'.$regid); //url of merchant landing page
					
					$_cancelurl = url('epayment/cancel');
					
					$_amount    = (($_amount>0)?number_format($_amount,2, '.', ''):'0.00');
					
					$forSign    = $_mid . $_requestid . $_reference . $_ipaddress . $_noturl . $_resurl .  $_amount;
					$_sign      = hash("sha512", $forSign.$cert);
		
					if($func=='refund' && $_reference!=''){
						$response = $client->refund(array('merchantid'      =>$_mid,
														   'request_id'      =>$_requestid,
														   'org_trxid'        =>$_reference,
														   'ip_address'      =>$_ipaddress,
														   'notification_url'=>$_noturl,
														   'response_url'    =>$_resurl,
														   'amount'          =>$_amount,
														   'signature'       =>$_sign
														));
						$response = ((@property_exists($response,'refundResult'))?($response->refundResult):array());								
					}elseif($func=='reversal' && $_reference!=''){
						$response = $client->reversal(array('merchantid'      =>$_mid,
														   'request_id'      =>$_requestid,
														   'org_trxid'        =>$_reference,
														   'ip_address'      =>$_ipaddress,
														   'notification_url'=>$_noturl,
														   'response_url'    =>$_resurl,
														   'amount'          =>$_amount,
														   'signature'       =>$_sign
														));
						$response = ((@property_exists($response,'refundResult'))?($response->reversalResult):array());
					}
					
					if($response && count($response)>0){
						$xml  = $response;
						$xapp = ((@property_exists($xml,'application'))?($xml->application):array());
						$xres = ((@property_exists($xml,'responseStatus'))?($xml->responseStatus):array());
					
						$mid  = getObjectValue($xapp,'merchantid');
						$rid  = getObjectValue($xapp,'request_id');
						$resp = getObjectValue($xapp,'response_id');
						$tstmp= getObjectValue($xapp,'timestamp');
						
						$rcode= getObjectValue($xres,'response_code');
						$rmsg = getObjectValue($xres,'response_message');
												
						if($_requestid && $_requestid!='' && $rcode && $rcode!=''){
						   $stats = '4';
						   switch($rcode){
							case 'GR033':
							  $stats = '-2';
							  break;
							case 'GR001':
							case 'GR002':
							  $stats = '4';
							  break;
							default:  
							  $stats = '5'; //FAILED
							  break;	
						   }
						   $exec = DB::table('ESv2_EPayment')->where(array('EntryID'=> $payid))
															 ->update(array('StatusID'=> $stats,
																			'RequestID'=>$rid,
																			'ReferenceNo'=>$resp,
																			'ResponseCode'=>$rcode,
																			'ResponseMessage'=>$rmsg,																	 
																		)); 				   
						   return ($exec);												
						}else{
						   return false;
						}
						
					}else{
						return false;
					}
					
					
					break;							
				default:
					return false;
					break;
			}
		}catch(Exception $e){
		    return false;
		}
	}
	
	private function statusLabel($statsid=0){
		$txstatus = 'To Transact';
		switch($statsid){
		  case '-1':
			$txstatus = 'Transaction Cancelled';
		  break;
		  case '-2':
			$txstatus = 'Pending';
		  break;
		  case '1':
			$txstatus = 'Successful';
		  break;
		  case '3':
			$txstatus = 'Failed';
		  break;
		  case '4':
			$txstatus = 'Reverted';
		  break;
		  case '5':
			$txstatus = 'Failed Revert';
		  break;
		}
		return $txstatus;
	}
	
	public function event(){
        $response = ['error' => true, 'message' => 'No Event Selected'];
        $post     = Request::all();
		$content  = '';
        if (Request::ajax()) {
            $response = ['error' => true, 'message' => 'Permission Denied!'];
    
            $start = Request::has('start')?Request::get('start'):0;
            $length = Request::has('length')?Request::get('length'):10;
            $search = Request::has('search')?Request::get('search'):array();
            $order = Request::has('order')?Request::get('order'):array();
            $page = Request::has('page')?Request::get('page'):1;
            $type = Request::has('type')?Request::get('type'):'';

            $data = array();
            $total = 0;

            switch (Request::get('event')) {
			    case 'soap-function':					
					$client = new SoapClient("https://testpti.payserv.net/Paygate/ccservice.asmx?wsdl");
					var_dump($client->__getFunctions());
					die();
					break;
			    case 'revert-entry':
					$payid = getObjectValue($post,'entryid');
					$exec  = false;
					if($payid && $payid!=false){
					  $exec = DB::table('ESv2_EPayment')->where('EntryID',$payid)->update(array('StatusID'=>'4','ResponseMessage'=>'Manually Reverted!'));
					}
					$response = ['error'=>(!$exec),'message'=>(($exec)?'Success':'Failed to execute'),'content'=>$content];
					break;
			    case 'reverse-entry':
					$payid  = getObjectValue($post,'entryid');
					$content= '<p><i class="fa fa-warning text-warning"></i> Nothing to load</p>';
					$exec   = DB::SELECT("SELECT p.EntryID,p.TermID,p.UserID,p.StatusID,p.RequestID,p.ReferenceNo,p.ResponseCode,p.ResponseMessage,p.DateCreated
											    ,pd.DetailID,pd.StudentNo,pd.RegID,pd.TotalAmount,pd.IsTransacted,pd.TransactionNo
											    ,s.FamilyID,s.LastName,s.FirstName,s.MiddleInitial,s.FullName
											    ,o.ORNo,o.Date as ORDate,o.CreatedBy as CashierID  
										   FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID
									 INNER JOIN ES_Students as s ON pd.StudentNo=s.StudentNo
									  LEFT JOIN ES_OfficialReceipts as o ON pd.TransactionNo=o.ORNo AND pd.StudentNo=o.PayorID WHERE p.EntryID='".$payid."'");
					$content  =	(string)view($this->views.'modal.online-reversal',array('rspayment'=>$exec));			  
					$response = ['error'=>(!$exec),'message'=>(($exec)?'Success':'Failed to execute'),'content'=>$content];
				    break;
			    case 'cancel-entry':
					$payid = getObjectValue($post,'entryid');
					$exec  = false;
					if($payid && $payid!=''){
					 $exec = DB::table('ESv2_EPayment')->where('EntryID',$payid)->update(array('StatusID'=>'-1')); 	
					}
					$response = ['error'=>(!$exec),'message'=>(($exec)?'Success':'Failed to execute')];
				    break;
                case 'load-list':
					$draw = 1;
					$tmpflt=Request::get('search');
					$filter='';
					if($tmpflt && @array_key_exists('value',$tmpflt)){
					  $filter = trim($tmpflt['value']);
					}
					
					$exec  = $this->getDataTable('data',$filter,Request::get('draw'),Request::get('length'),Request::get('start'));
					$arr   = array();
					if($exec){
					    $length= Request::get('length');
						$start = Request::get('start');
						$draw  = ((intval($start)<=0)?1:($start/$length));
						$draw  = Request::get('draw');
						$j     = (($start=='')?1:($start+1));
						foreach($exec as $r){
						$btn = '<a  class="btn btn-sm btn-info btninfo" href="javascript:void(0);"><i class="fa fa-plus"></i></a>';
						if($r->StatusID==0){
						$btn .= '<a  class="btn btn-sm btn-danger btncancel" href="javascript:void(0);"><i class="fa fa-times"></i></a>';
						}else if($r->StatusID==1 && $r->ORNo==''){
						$btn .= '<a  class="btn btn-sm btn-danger btnreverse" href="javascript:void(0);"><i class="fa fa-reply"></i></a>';
						}else if($r->StatusID==1 && $r->ORNo!=''){
						$btn .= '<a  class="btn btn-sm btn-warning" href="'.url('cashier/cashiering/print?or='.$r->ORNo).'"  target="_blank"><i class="fa fa-print"></i></a>';
						}
						
						$txstats = $this->statusLabel($r->StatusID);
						$tmparr = array(
										 'DT_RowId'=>$r->EntryID
										,'DT_RowData'=>array()
										,'DT_RowAttr'=>array()
										,'0'=>'<span class="data-info" style="width:100px !important;" data-payment="'.$r->EntryID.'" data-detail="'.$r->DetailID.'" data-requestid="'.$r->RequestID.'">'.$btn.'</span>'
										,'1'=>$r->ORNo
										,'2'=>$r->CashierID
										,'3'=>$r->RequestID
										,'4'=>$r->FullName
										,'5'=>number_format(floatval($r->TotalAmount),2)
										,'6'=>$txstats
										,'7'=>date('Y-m-d',strtotime($r->DateCreated))
									 );
						$arr[] = $tmparr;
						$j++;
						}
					
					}
					if(count($arr)<=0){
					  $draw=0;
					}
					
				    return ['draw'=>$draw,'recordsTotal'=>$this->getDataTable('total'),'recordsFiltered'=>$this->getDataTable('total',$filter),'data'=>$arr,'error'=>false];
				    break;
                case 'sort-transaction-list':
                    $dateFrom = Request::has('dateFrom')?Request::get('dateFrom'):NULL;
                    $dateTo = Request::has('dateTo')?Request::get('dateTo'):NULL;
                    $payment_status = Request::has('payment_status')?Request::get('payment_status'):NULL;
                    $payment_portal = Request::has('payment_portal')?Request::get('payment_portal'):NULL;
                    $transaction_status = Request::has('transaction_status')?Request::get('transaction_status'):NULL;

                    $select = array(
                        '_id',
                        DB::raw('online_payment_tx_records.date_added as transaction_created'),
                        DB::raw('online_payment_tx_used.date_added as transaction_used'),
                        'or_number',
                        'cashier_name',
                        'transaction_code',
                        'student_name',
                        'amount',
                        'student_id',
                        'user_fullname',
                        'request_status',
                        'payment_status',
                        'payment_gateway',
                        'is_validated'
                    );

                    $model = TransactionRecords::select($select)
                        ->leftJoin('online_payment_tx_used','online_payment_tx_records._id','=','online_payment_tx_used.id')
                        ->orderBy('online_payment_tx_records.date_added', 'desc');

                    if (isset($search) && isset($search['value']) && !empty($search['value'])){
                        $new_search = $search['value'];
                        $model = $model->where(function($q) use ($new_search,$select){
                            $q->where($select[5],'like','%'.$new_search.'%')
                                ->orWhere($select[6],'like','%'.$new_search.'%');
                        });
                    }
                    
                    $model = $model->skip($start)->take($length+1);
                    if (count($order) > 0){
                        foreach($order as $sort){
                            $model = $model->orderBy($select[$sort['column']],$sort['dir']);
                        }
                    }
                    if(isset($payment_portal) && $payment_portal != '')
                    {
                        $model = $model->where('payment_gateway','like',$payment_portal.'%');
                    }
                    
                    if(isset($payment_status) && $payment_status != '')
                    {
                        $model = $model->where('is_validated',$payment_status);
                    }

                    if(isset($dateTo) && $dateTo != '')
                    {
                        $dateFrom = new Carbon($dateFrom);
                        $dateTo = new Carbon($dateTo);
                        $dateTo->setTime(23,59, 59); // set date-to to its last minute before the next day

                        $model = $model->whereBetween('online_payment_tx_records.date_added',[$dateFrom,$dateTo]);
                    }

                    if(isset($transaction_status) && $transaction_status != '')
                    {
                        switch($transaction_status)
                        {
                            case "used":
                                $model = $model->where('is_code_used',true);
                            break;

                            case "unused":
                                $model = $model->where('is_code_used',false);
                            break;
                        }
                    }

                    $data = $model->get();

                    $total = $length+$start;
                    if (count($data) > $length){
                        $total += 1;
                        unset($data[count($data)-1]);
                    }

                    $response = $this->formatDataToDisplay($total, $data, $start, 'transactions');

                    break;

                case 'report-collected':
                    $dateFrom = Request::has('dateFrom')?Request::get('dateFrom'):NULL;
                    $dateTo = Request::has('dateTo')?Request::get('dateTo'):NULL;
                    $payment_portal = Request::has('payment_portal')?Request::get('payment_portal'):NULL;

                    $model = PaymentReports::leftJoin('online_payment_collected','online_payment_reports.id','=','online_payment_collected.report_id')
                        ->orderBy('online_payment_reports.date_added', 'desc');

                    if(isset($payment_portal) && $payment_portal != '')
                    {
                        $model = $model->where('payment_portal','like',$payment_portal.'%');
                    }
                        
                    if(isset($dateTo) && $dateTo != '')
                    {
                        $dateFrom = new Carbon($dateFrom);
                        $dateTo = new Carbon($dateTo);
                        $dateTo->setTime(23,59, 59); // set date-to to its last minute before the next day

                        $model = $model->whereBetween('online_payment_reports.date_added',[$dateFrom,$dateTo]);
                    }

                    $data = $model->get();

                    $total = $length+$start;
                    if (count($data) > $length){
                        $total += 1;
                        unset($data[count($data)-1]);
                    }

                    $response = $this->formatDataToDisplay($total, $data, $start, 'reports');
                    break;
            }
        }else{
            switch (Request::get('event')) {
			    case 'soap-function':					
				    $exec = $this->soapFunction('refund');
					var_dump($exec);
					die();
					break;
	        }	
		}
        return $response;
    }
    
    private function formatDataToDisplay($total, $data, $start, $table)
    {
        $return_arr = [];
        $count = $start + 1;
        if(count($data) > 0){
            $return_arr = array(
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
            );

            for($a = 0; $a < count($data); $a++){

                $_this = $data[$a];


                switch($table)
                {
                    case 'transactions':
                        $status_codes = array('Pending' => 'P','Refund' => 'R','Chargeback' => 'K','Void' => 'V','Authorized' => 'A','Failure' => 'F','Unknown' => 'U','Success' => 'S');
                        $status_key = array_search($_this->request_status,$status_codes,true);
                        switch($_this->is_validated)
                        {
                            case '0':
                                $btn = '<input id="transaction-btn" class="btn btn-primary" type="button" data-btn="pay-and-verify" data-portal="'.$_this->payment_gateway. '" data-id="'.encode($_this->student_id).'" value="Verify" class="include">';
                            break;

                            case '1':
                                $btn = '<input id="transaction-btn" class="btn btn-success" data-btn="show-validted" type="button" value="Validated" class="include">';
                            break;

                            case '2':
                                $btn = '<input id="transaction-btn" class="btn btn-warning" type="button" data-btn="pay-and-verify" data-portal="'.$_this->payment_gateway. '" data-id="'.encode($_this->student_id).'" value="Re-Verify" class="include">';
                            break;

                            case '-1':
                                $btn = '<input id="transaction-btn" class="btn btn-danger" data-btn="show-error" type="button" value="Error" class="include">';
                            break;

                            default:
                                $btn = '';
                            break;
                        }

                        $return_arr['data'][] = array(
                            isset($_this->transaction_used) ? '<td class="font-xs center bold"><a  class="fa fa-print" href="'.url('/'). '/cashier/cashiering/print?or='.$_this->or_number. '"  target="_blank"> Print</a></td>' : '',
                            $_this->transaction_used,
                            $_this->or_number,
                            $_this->cashier_name,
                            $_this->transaction_code,
                            $_this->student_name,
                            $_this->amount,
                            $status_key ? $status_key : 'Transaction Aborted',
                            $_this->transaction_created,
                            $btn,
                        );
                    break;

                    case 'reports':
                        // dd(preg_match('(00:00:00|-0|-1)', $_this->reports_name));
                        $return_arr['data'][] = array(
                            '<td class="font-xs center bold"><a href="#" class="fa fa-eye" id="collected-btn-list" data-bill-for='.$_this->collection_for. ' data-collections='. $_this->charge_id.'> View</a></td>',
                            date('F d,Y h:i A', strtotime($_this->date_from)),
                            date('F d,Y h:i A', strtotime($_this->date_to)),
                            preg_match('(00:00:00|-0|-1)', $_this->reports_name) ? date('F d,Y h:i A', strtotime($_this->reports_name)): $_this->reports_name,
                            $_this->collection_for ? 'On Premise' : 'PTC',
                            $_this->payment_portal,
                            $_this->date_added
                        );
                    break;
                }
            }

        } else {
            $return_arr = array(
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => []
            );
        }
        return $return_arr;
    }

    public function xprint()
    {
        $bill = Request::has('bill')?Request::get('bill'):NULL;
        $portal = Request::has('portal')?Request::get('portal'):NULL;
        $reportName = Request::has('report-name')?Request::get('report-name'):NULL;
        $dateFrom = Request::has('dateFrom')?Request::get('dateFrom'):NULL;
        $dateTo = Request::has('dateTo')?Request::get('dateTo'):NULL;
        $charge_ids = Request::has('charge_id')?Request::get('charge_id'):NULL;
        $reportName = isset($reportName) ? $reportName : Carbon::now()->format('Y-m-d H:i:s');

        $select = array(
            'online_payment_charge_records.ptc_amount',
            'online_payment_charge_records.school_amount',
            'online_payment_charge_records._id',
            'online_payment_tx_records.transaction_code',
            'online_payment_tx_records.student_name',
            'online_payment_tx_records.student_id',
            'online_payment_tx_records.student_term',
            DB::raw('online_payment_tx_records.date_added as date_requested'),
            DB::raw('online_payment_tx_used.date_added as date_payed'),
            'online_payment_tx_used.or_number',
            'online_payment_tx_used.cashier_name'
        );

        $dateFrom = new Carbon($dateFrom);
        $dateTo = new Carbon($dateTo);
        $now = Carbon::now();
        $dateTo->setTime(23,59); // set date-to to its last minute before the next day
        $total_collection = 0;
        $response = array();

        $records = TransactionRecords::select($select)
                ->join('online_payment_tx_used','online_payment_tx_records._id','=','online_payment_tx_used.id')
                ->join('online_payment_charge_records','online_payment_tx_records.charge_id', '=', 'online_payment_charge_records._id')
                ->where('online_payment_tx_records.payment_gateway', 'like', '%' .$portal. '%')
                ->whereBetween('online_payment_tx_used.date_added',[$dateFrom,$dateTo]);
        
        switch($bill)
        {
            case 'ptc-bill':
                
                $charge_ids = array();
                $ptc_records = $records->where('online_payment_charge_records.is_ptc_collected',false)->get();
                if(count($ptc_records) < 1)
                {
                    return array('error' => true , 'message'=> 'No data found, check your selected information');
                }
                else
                {
                    foreach($ptc_records as $info)
                    {
                        $total_collection += $info->ptc_amount;
                        $info->collection_amount = $info->ptc_amount;
                        unset($info->ptc_amount);

                        DB::table('online_payment_charge_records')
                            ->where('_id',$info->_id)
                            ->update(['is_ptc_collected' => true]);

                        array_push($charge_ids, $info->_id);
                    }
                    $report_model = PaymentReports::create(
                        array(
                            'reports_name' => $reportName,
                            'payment_portal' => $portal,
                            'date_from' => $dateFrom,
                            'date_to' => $dateTo,
                            'collection_for' => false
                        )
                    );

                    if($report_model->save())
                    {
                        $payment_model = PaymentCollected::create(
                            array(
                                'report_id' => $report_model->id,
                                'charge_id' => json_encode($charge_ids)
                            )
                        )->save();
                    }

                    $html = view($this->views.'reports.bill', array(
                        'data' => $ptc_records,
                        'total' => $total_collection,
                        'index' => 1,
                        'date_today' => $now,
                        'date_from' => $dateFrom,
                        'date_to' => $dateTo->toDateTimeString()
                    ))->render();
                    
                    $response = array('error' => false , 'html'=>$html);
                }
            break;

            case 'school-bill':

                $charge_ids = array();
                $school_records = $records->where('online_payment_charge_records.is_school_collected',false)->get();
                if(count($ptc_records) < 1)
                {
                    return array('error' => true , 'message'=> 'No data found, check your selected information');
                }
                else
                {
                    foreach($school_records as $info)
                    {
                        $total_collection += $info->school_amount;
                        $info->collection_amount = $info->school_amount;
                        unset($info->school_amount);

                        DB::table('online_payment_charge_records')
                            ->where('_id',$info->_id)
                            ->update(['is_school_collected' => true]);

                        array_push($charge_ids, $info->_id);
                    }

                    $report_model = PaymentReports::create(
                        array(
                            'reports_name' => $reportName,
                            'payment_portal' => $portal,
                            'date_from' => $dateFrom,
                            'date_to' => $dateTo,
                            'collection_for' => true
                        )
                    );

                    if($report_model->save())
                    {
                        $payment_model = PaymentCollected::create(
                            array(
                                'report_id' => $report_model->id,
                                'charge_id' => json_encode($charge_ids)
                            )
                        )->save();
                    }

                    $html = view($this->views.'reports.bill', array(
                        'data' => $ptc_records,
                        'total' => $total_collection,
                        'index' => 1,
                        'date_today' => $now,
                        'date_from' => $dateFrom,
                        'date_to' => $dateTo
                    ))->render();
                    
                    $response = array('error' => false , 'html'=>$html);
                }
            break;

            case 'view-report':
                $records = array();
                $select = array(
                    'online_payment_tx_records.transaction_code',
                    'online_payment_tx_used.cashier_name',
                    'online_payment_tx_used.date_added'
                );

                $model = ChargeRecords::select($select)
                        ->join('online_payment_tx_records','online_payment_tx_records.charge_id','=','online_payment_charge_records._id')
                        ->join('online_payment_tx_used','online_payment_tx_used.id','=','online_payment_tx_records._id');

                foreach($charge_ids as $id)
                {
                    // $data = ChargeRecords
                }
            break;
        }
        
        return $response;
    }
}