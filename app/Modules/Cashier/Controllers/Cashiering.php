<?php
/*
    created by   : ARS
    Description  : This module is for cash collection of the system.
    Release Date : TBA
    Version      : 1.0
    Update Notes :

            2018.02.14 13:31H  -  Activate Fees Selection Templates
            2018.02.16 12:23H  -  Activate Schedule of payment

*/

namespace App\Modules\Cashier\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Cashier\Services\CashierServices as services;
use App\Modules\Accounting\Services\AccountingServiceProvider as sFinance;
use App\Modules\Accounting\Services\Assessment\assessment as assess;

use App\Libraries\PDF;

use Permission;
use Request;
use Response;
use DB;

class Cashiering extends Controller
{
    protected $ModuleName = 'cashiering';

    private $media = [
            'Title' => 'Cashiering',
            'Description' => 'Welcome to cashiering module.',
		    'closeSidebar'  => true,
            'js' => ['Cashier/cashier.js?v=1.26'],
            'css' => ['profile'],
            'init' => ['Cashiering.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                           ,'bootstrap-datepicker/js/bootstrap-datepicker'
                           ,'bootstrap-timepicker/js/bootstrap-timepicker.min'
                        ],
             'plugin_css' =>
             [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification'
                ,'bootstrap-datepicker/css/datepicker3'
                ,'bootstrap-timepicker/css/bootstrap-timepicker.min'
             ],
        ];

    private $url = ['page' => 'cashier/cashiering'];

    private $views = 'Cashier.Views.Cashiering.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));
            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = ['views' => $this->views];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode,$term, $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;
        if($term != ''){

            $whereRaw = "a.TermID = {$term} ";
           // $whereRaw .= " AND r.ProgID IN (".implode(getUserProgramAccess(),',') .")";

            $query = "SELECT a.*, fn_CurrencyCode(a.CurrencyID) As Curr
                      FROM  es_tableoffees a
                      WHERE " . $whereRaw . " Order By TemplateCode ASC
                      OFFSET {$skip} ROWS
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('es_tableoffees As a')
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }
        }

        return $rs;
    }

    public function view(){
        $this->initializer();
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));
            $ref = decode(Request::get('ref'));

            $_incl = [
                'views' => $this->views
                ,'term' => $term
                ,'ref' => ($ref)
            ];

            SystemLog( $this->media['Title'] ,'','Page View','page-view','','' );
            return view('layout',array('content'=>view($this->views.'view')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];

        if (Request::ajax()) {

            $this->initializer();

            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
			    case 'get-online-payment':
					$idno = decode(getObjectValue($post,'idno'));
					$exec = DB::SELECT("SELECT p.EntryID,p.UserID,p.RequestID
                                              ,dbo.fn_AcademicYearTerm(p.TermID) as AYTerm
											  ,p.TermID,pd.StudentNo,pd.RegID,pd.ScheduleID
	                                          ,pd.TotalAmount,p.StatusID
	                                          ,(SELECT TOP 1 Fullname FROM ESv2_Users WHERE UserIDX=p.UserID) as Fullname
										  FROM ESv2_EPayment as p
									INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID
										 WHERE pd.StudentNo='".$idno."' AND StatusID=1 AND ISNULL(IsTransacted,0)=0
									  ORDER BY EntryID DESC");
					if($exec && count($exec)>0){
					   $tmpoption  = '';
					   $tmpcontent = '<select class="form-control" id="tx_number" name="tx_number"><option value="" disabled selected>- Select one -</option></select>';
					   foreach($exec as $r){
					     $tmpoption .= '<option value="'.$r->EntryID.'" data-term="'.$r->TermID.'"
                                                                        data-ayterm="'.$r->AYTerm.'"						 
						                                                data-name="'.$r->Fullname.'" 
						                                                data-std="'.$r->StudentNo.'" 
																		data-amt="'.number_format(floatval($r->TotalAmount),2,'.','').'">'.$r->RequestID.'</option>';
					   }
					   if($tmpoption!=''){
					     $tmpcontent = '<select class="form-control" value="" id="tx_number" name="tx_number"><option value="" disabled selected>- Select one -</option>'.$tmpoption.'</select>';
					   }
					   $response = Response::json(['error' => false, 'message' => 'Success!', 'content'=>$tmpcontent]);
					}else{
					   $response = Response::json(['error' => true, 'message' => 'Nothing to load!']);
					}
					break;
                case 'get-employees':
						$exec = DB::SELECT("SELECT * 
						                      FROM HR_Employees as e
										 LEFT JOIN ES_Departments as d ON e.DeptID=d.DeptID
											 WHERE Inactive=0");
						$content = '';
                        if($exec && count($exec)>0){
						   foreach($exec as $e){
						    $fname    = ucfirst($e->LastName).', '.ucfirst($e->FirstName).' '.$e->MiddleName;
							$content .= '<tr data-id="'.$e->EmployeeID.'">
											<td>'.$e->EmployeeID.'</td>
											<td><a href="javascript:void(0);" class="employee-select" data-id="'.$e->EmployeeID.'" data-name="'.$fname.'" data-dept="'.$e->DeptName.'">'.$fname.'</a></td>
											<td>'.$e->DeptName.'</td>
										 </tr>';
						   }		
						}
						
						$list = '<table class="table table-striped table-bordered table-hover table_scroll no-footer dataTable" id="employee-table-res">
									<thead>
										<th>IDNo.</th>
										<th>Name</th>
										<th>Department</th>
									</thead>
									<tbody>'.$content.'</tbody>
								 </table>';					 
                        $response = Response::json(['error' => false, 'message' => 'Success!','list'=>$list]);
					break;
                case 'get-scholarship':
						$exec = DB::SELECT("SELECT * 
						                      FROM HR_Employees as e 
										 LEFT JOIN ES_Departments as d ON e.DeptID=d.DeptID
											 WHERE Inactive=0");
					break;
                case 'get-fees':

                    $idno = decode($post['idno']);
                    $term = decode($post['term']);
                    $fee = decode($post['fee']);
                    $refno = decode($post['refno']);
                    $txn = ($post['txn']);
                    $type = '1';

                    $data = sFinance::createAssessment($term, $idno,$txn, $refno, $fee);

                    if ($data['err']){
                        $response = ['error'=>true,'message'=>$data['msg']];
                    }else{
                        $response = ['error'=>false,'message'=>'success'];
                    }


                break;

                case 'fees':

                    $idno = decode($post['idno']);
                    $term = decode($post['term']);

                    $data = sFinance::getFeesTemplatebyStudent($term, $idno);

                    $view =  view($this->views.'sub.fees',  ['data' => $data ] )->render();

                    $response = ['error'=>false,'message'=>'success', 'list' => $view ];

                break;

                case 'orsearch':

                    $date = systemDate('Y-m-d');

                    $where = "";

                    if ($this->permission->has('search-view-all')) {
                        $where = "";
                    }

                    $search = trimmed($post['search']);

                    $qry = "SELECT *, fn_TotalReceiptAmount('20',ORNo) As Descripancy FROM es_officialreceipts
                            WHERE (Date Between '{$date} 00:00:00' AND '{$date} 23:59:59')
                                AND  ORNo LIKE '%{$search}%' LIMIT 50 ";

                    $data = DB::select($qry);

                    $view =  view($this->views.'sub.orlist',  ['data' => $data ] )->render();

                    $response = ['error'=>false,'message'=>'success', 'list' => $view ];

                break;

                case 'get-schedule':
                    $idno = $post['idno'];
                    $type = $post['type'];
                    $txn = $post['txn'];
                    $ref = decode($post['ref']);

                    if($idno != '' ){

                        $qry = "SELECT j.*, a.AcctName, a.AcctCode
                                FROM es_journals j
                                LEFT JOIN es_accounts a on j.AccountID=a.AcctID
                                WHERE j.TransID = '{$txn}' AND j.ReferenceNo = '{$ref}'
								ORDER BY j.AccountID";
                        $data = DB::select($qry);
                        $dues = DB::table('es_registrations')->where('RegID', $ref)->first();

                        $schedule = view($this->views.'sub.schedule',  ['data' => $data,'dues'=> $dues  ] )->render();;


                        $response = ['error'=>false,'message'=>'success', 'schedule' => $schedule];

                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }


                break;

                case 'get-payment-schedule':

                    $idno = decode($post['idno']);
                    $type = $post['type'];
                    $txn = $post['txn'];
                    $col = $post['col'];
                    $ref = ($post['refno']);

                    if($idno != '' ){

                       $scheme = array(
                            1 => '1st Payment',
                            2 => '2nd Payment',
                            3 => '3rd Payment',
                            4 => '4th Payment',
                            5 => '5th Payment',
                            6 => '6th Payment',
                            7 => '7th Payment',
                            8 => '8th Payment',
                            9 => '9th Payment',
                            10 => '10th Payment',
                        );
                        if($col == 0){
                            $qry = "SELECT J.*, a.AcctName, a.AcctCode , Debit-ActualPayment As Due,'".$col."' as Col
                                    FROM es_journals j
                                    LEFT JOIN es_accounts a on j.AccountID=a.AcctID
                                    WHERE TransID = '{$txn}' AND ReferenceNo = '{$ref}' AND (Debit-ActualPayment) > 0  
								    ORDER BY j.AccountID";

                        } else{
                            $qry = "SELECT j.*, a.AcctName, a.AcctCode , [".$scheme[$col]."] As Due,'".$col."' as Col
                                    FROM es_journals j
                                    LEFT JOIN es_accounts a on j.AccountID=a.AcctID
                                    WHERE TransID = '{$txn}' AND ReferenceNo = '{$ref}' AND [".$scheme[$col]."] > 0 
								    ORDER BY j.AccountID";
                        }


                        $data = DB::select($qry);

                        $view =  view($this->views.'schedassess', ['data' => $data]  )->render();
                        $response = ['error'=>false,'message'=>'success', 'view' =>  $view ];
						SystemLog('Cashier','Cashiering','view schedule','', ('STN:'.$idno.',REF:'.$ref), '');
                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }


                break;

                case  'get-refs':
                    $orno = getObjectValue($post,'orno');
                    $idno = decode($post['idno']);
                    $type = $post['type'];
                    $txn  = $post['txn'];
                    $all  = $post['all'];

                    if($idno != '' ){
					    if($type==1){
                        $recalc = DB::select("SELECT TOP 1 TermID,RegID,StudentNo,TableofFeeID FROM ES_Registrations WHERE StudentNo='".$idno."' ORDER BY RegID DESC");
                        if($recalc && count($recalc)>0){
						   $xterm  = $recalc[0]->TermID;
						   $xregid = $recalc[0]->RegID;
						   $xstdno = $recalc[0]->StudentNo;
						   $xfeeid = $recalc[0]->TableofFeeID;
				           $rexec  = $this->assess->get_studentjournal($xterm,$xstdno,$xregid,2);
						}
						
						$obalance = getOutstandingBalance($type , $idno, systemDate() );
                        }else{
						$obalance = 0;
						$idno     = (($idno==0 && $type>1)?'':$idno);
						}
						
						$data =  $this->services->setPayer($type, $idno, $txn);
                        $refs =  view($this->views.'sub.refs',['data' =>  $data['refs']])->render();

                        
                        $data['details']['obalance'] = $obalance;
                        if(!empty($data['refs'])){
                            $data['details']['regid'] = $data['refs'][0]->RegID ;
                        }
						
                        $data['details']['all'] = $all ;

                        
                        if($orno!=''){
						  $data['details']['balance'] = DB::SELECT("SELECT  J.EntryID, J.AccountID, J.AccountCode AS AcctCode, J.AccountName AS AcctName, J.AcctShortName AS ShortName ,
																			J.[AssessFee] AS Assessment ,
																			J.Discount AS FinancialAid ,
																			J.Debit AS NetAssessed ,
																			J.ActualPayment ,
																			J.CashierDiscount AS PaymentDiscount ,
																			J.CreditMemo ,
																			J.Refund ,
																			J.FinancialAidExternal ,
																			J.ADCRefund,
																			J.Remarks ,
																			J.TransID,J.ReferenceNo,J.DMCMRefNo ,
																			J.Credit as Balance,
																			J.[1stPayment] AS [1st Payment],
																			J.[2ndPayment] AS [2nd Payment] ,
																			J.[3rdPayment] AS [3rd Payment] ,
																			J.[4thPayment] AS [4th Payment] ,
																			J.[5thPayment] AS [5th Payment] ,
																			J.[6thPayment] AS [6th Payment] ,
																			J.[7thPayment] AS [7th Payment] ,
																			J.[8thPayment] AS [8th Payment] ,
																			J.[9thPayment] AS [9th Payment] ,
																			J.[10thPayment] AS [10th Payment] ,
																			J.AcctOption,J.PaymentOption,
																			J.GroupID,J.GroupCode,J.GroupName,J.GroupShort,J.GroupSort,
																			J.ClassCode,J.ClassName,J.ClassShort,J.ClassSort,
																			J.TermID ,
																			dbo.fn_AcademicYearTerm(J.TermID) AS AYTerm ,
																			dbo.fn_TransactionCode(J.TransID) AS TransactionCode ,
																			dbo.fn_TransactionName(J.TransID) AS TransactionName ,
																			J.SpecialTransNo,
																			J.ProgID AS CostCenterID,
																			J.ProgCode AS CostCenterCode,
																			J.ProgName AS CostCenterName,
																			J.YearLevelID,
																			J.YearLevel
																	FROM vw_Journals AS J
																 WHERE TransID=20 AND IDNo='".$idno."' AND ReferenceNo='".$orno."' 
																 ORDER BY EntryID ASC");
																 
						$assessment =  view($this->views.'elist',  $data['details'] )->render();
                        }else{
                        $assessment =  view($this->views.'list',  $data['details'] )->render();
                        }
						
                        unset($data['details']);
                        unset($data['refs']);

                        $response = ['error'=>false,'message'=>'success', 'data' => $refs,'assessment' => $assessment , 'ref' => $data, 'obalance' => number_format($obalance,2) ];

                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }

                break;

                case 'verify':

                    $or_num = $post['or_no'];
                    $data =  $this->services->verifyORNo($or_num);
                    if($data =='1'){
                        $response = ['error'=>false,'message'=>'success', 'data' => $data ];
                    }else{
                        $response = ['error'=>true,'message'=>'error', 'data' => $data ];
                    }

                break;

                case 'assessment':

                    $idno = decode($post['idno']);
                    $type = $post['type'];

                    $data = $this->services->getAssessmentDetails($type , $idno);

                    $data['regid'] = $post['refno'];
                    $data['all'] = $post['all'];
                    $data['obalance'] = getOutstandingBalance($type , $idno, systemDate() );

                    $view = view($this->views.'list',  $data )->render();

                    $response = ['error'=>false,'message'=>'success', 'view' => $view];

                break;

                case 'get-payment':

                    $validation = $this->services->validate(Request::all(), 'payment-form' );

                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $view =  view($this->views.'sub.payment', $post )->render();
                        $response = ['error'=>false,'message'=>'success', 'form' => $view];
                    }

                break;

                case 'save-payment':
                  //print_r(Request::all());
			      //die();
                    $validation = $this->services->validate(Request::all(), 'checkout' );
                     if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                      // die;

                        $xr_num = getObjectValue($post,'xrno');
                        $or_num = $post['orno'];
						
						if($xr_num!=''){
						  $ordetail = DB::statement("DELETE FROM ES_Journals WHERE ReferenceNo='".$xr_num."'");
						  $ormain   = DB::statement("DELETE FROM ES_OfficialReceipts WHERE ORNo='".$xr_num."'");
						}
						
						
                        ob_clean();
                        if (  $this->services->verifyORNo($or_num) == '1' ){
                            $r = $this->services->saveOR($post);
                            
							$txn  = decode(getObjectValue($post,'txn'));
							$type = decode(getObjectValue($post,'type'));
							$idno = decode(getObjectValue($post,'idno'));
							$orno = (getObjectValue($post,'orno'));
							if(getObjectValue($post,'ref')=='' ||getObjectValue($post,'ref')==false)
							  $ref = trim(getObjectValue($post,'orno'));
							else
							  $ref = decode(getObjectValue($post,'ref'));

                            if( isset( $post['validate'])  ){
                                $txn = decode(getObjectValue($post,'txn'));
                                if(getObjectValue($post,'ref')=='' ||getObjectValue($post,'ref')==false)
								  $ref = trim(getObjectValue($post,'orno'));
								else
								  $ref = decode(getObjectValue($post,'ref'));
                                
								$type = decode(getObjectValue($post,'type'));
                                $idno = decode(getObjectValue($post,'idno'));
                                $orno = (getObjectValue($post,'orno'));

                                SystemLog('Cashier','Cashiering','validate','', ('STN:'.$idno.',ORN:'.$orno), '');
                                sFinance::validateTxn($txn, $ref, $type, $idno, $orno);	
                            }else{
								SystemLog('Cashier','Cashiering','validation skip','', ('STN:'.$idno.',ORN:'.$orno), '');
							}
							
							//For Online Payment saving
							if(getObjectValue($post,'online')!='' && intval(getObjectValue($post,'online'))>0){
							    $pid  = intval(getObjectValue($post,'online'));
								$exec = DB::table('ESv2_EPayment_Details')
								                     ->where(array('PaymentID'=> $pid,'StudentNo'=>$idno))
				                                     ->update(array('IsTransacted'=> 1,
													                'TransactionNo'=>$orno,																	 
				                                                )); 
							}
							
							#Recompute payment
							if($idno!='' && $type==1 && getObjectValue($post,'ref')!=''){
							  sFinance::RecomputeTotalPayment($txn, $ref, $type, $idno, systemDate());
							  $regid  = ((getObjectValue($post,'ref')=='')?0:(getObjectValue($post,'ref')));
							  $recalc = $this->assess->recalc_assessment($idno,$regid);
							}
                            

                            #$view = view('reports.official_receipts.acknowledgement_receipt', array('orno' => $or_num) )->render();
                            $view='';
                            $response = ['error'=>false,'message'=>'official receipt successfully saved!', 'view' => $view];
					        SystemLog('Cashier','Cashiering','save','', ('STN:'.$idno.',ORN:'.$orno), '');
                        }else{
                            $response = ['error'=>true,'message'=>'official receipt already used!'];
                        }
                    }

                break;

                default: return response('Unauthorized.', 401); break;
            }
        }
        ob_clean();
        return $response;
    }


        public function eprint(){

            $this->pdf = new PDF;
        set_time_limit(0);
            ini_set('memory_limit', '-1');

            $pdf = 0;

            $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(true);
        $this->font = 11;
        //$this->customFont = 'Libraries/pdf/fonts/Calibri.ttf';
            //$this->pdf->SetFont($this->pdf->addTTFfont(app_path($this->customFont)),'',$this->font);

            $this->pdf->SetMargins(50, 50, 50, true);
            $this->pdf->setPageUnit('pt');

            $data = array(
                'orno' => (Request::get('or')),
            );


            if($pdf == 1){

                $this->pdf->AddPage('P','LETTER');
                $this->pdf->setTitle('Print Preview');
                $this->pdf->writeHTML(view('reports.official_receipts.acknowledgement_receipt',$data)->render());

                $this->pdf->output();

            } else {

                echo view('reports.official_receipts.or_1_15',$data)->render();
                // echo view('reports.official_receipts.acknowledgement_receipt',$data)->render();
            }


		set_time_limit(30);
        ini_set('memory_limit', '128M');
    }

    private function initializer()
    {
	    $this->assess     = new assess;
        $this->permission = new Permission($this->ModuleName);
        $this->services = new services;
    }
}
