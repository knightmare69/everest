<?php

namespace App\Modules\Cashier\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Cashier\Services\CashierServices as services;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF;

use Permission;
use Request;
use Response;
use DB;

class OfficialReceipts extends Controller
{
    protected $ModuleName = 'cashiering';

    private $media = [
            'Title' => 'Official Receipts',
            'Description' => 'Welcome to official receipt module',
            'js' => ['Cashier/receipt.js?v=1'],
            'css' => ['profile'],
            'init' => ['Receipts.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                           ,'bootstrap-datepicker/js/bootstrap-datepicker'
                           ,'bootstrap-timepicker/js/bootstrap-timepicker.min'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification'
                ,'bootstrap-datepicker/css/datepicker3'
                ,'bootstrap-timepicker/css/bootstrap-timepicker.min'
            ],
        ];

    private $url = ['page' => 'cashier/official-receipts'];

    private $views = 'Cashier.Views.OfficialReceipts.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = [
                'views' => $this->views
               ,'data' => $this->getData()
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode='', $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";
        $current_year = systemDate('Y');
        $current_month = systemDate('m');

        $start = Request::get('start');
        $end = Request::get('end');
        $payer = Request::get('payer');
        $or_start = Request::get('orbegin');
        $or_end = Request::get('orend');
        $payer_type = Request::get('payer_type');
        $status = Request::get('status');
        $inclvoid = Request::get('Inclvoid');

        $take = 2000;
        $skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;

        if($start == ''){
            $whereRaw = "Month(date) = '{$current_month}' AND Year(Date) = '{$current_year}' ";
        }else{
            $whereRaw = " (date between '{$start}' AND '{$end} 23:59:59') ";
        }

        if($payer != ''){
            $whereRaw .= " AND (PayorName like '%". trimmed($payer) ."%') ";
        }

        if($or_start != '' && $or_end != '' ){
            $whereRaw .= " AND (ORNo between '{$or_start}' and '{$or_end}') ";
        }

        if ($payer_type != '0' && $payer_type != ''){
            $whereRaw .= " AND (payortype = '".$payer_type."') ";
        }

        if ($status == '3'){
            $whereRaw .= " AND (dbo.fn_TotalReceiptAmount('20',ORNo) = 0 ) ";
        }

        if ($inclvoid == ''){
            $whereRaw .= " AND IsVoid = 0 ";
        }


       if($mode == 'total'){
            $rs =  DB::table('es_officialreceipts As a')
                    ->whereRaw($whereRaw)
                    ->count();
        }else{
              $rs = DB::table('es_officialreceipts')
                 ->selectRaw("*, dbo.fn_CurrencyCode(CurrencyID) As Curr, dbo.fn_TotalReceiptAmount('20',ORNo) As Descripancy ")
                 ->whereRaw($whereRaw)
                 ->orderBy('ORNo','ASC')
                 ->skip($skip)
                 ->take(100)
                 ->get();
        }

        return $rs;
    }

    private function getDataTable($mode='', $filter='', $page=1, $take=10,$skip=0){

        ini_set('max_execution_time', 120);

        $whereRaw = "";
        $current_year = systemDate('Y');
        $current_month = systemDate('m');

        $start = Request::get('dtstart');
        $end = Request::get('dtend');
        $payer = Request::get('payer');
        $or_start = Request::get('orbegin');
        $or_end = Request::get('orend');
        $payer_type = Request::get('payer_type');
        $status = Request::get('status');
        $inclvoid = Request::get('Inclvoid');

      //$skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;

        if($start == ''){
            $whereRaw = "Month(date) = '{$current_month}' AND Year(Date) = '{$current_year}' ";
        }else{
            $whereRaw = " (date between '{$start}' AND '{$end} 23:59:59') ";
        }

        if($payer != ''){
            $whereRaw .= " AND (PayorName like '%". trimmed($payer) ."%') ";
        }

        if($or_start != '' && $or_end != '' ){
            $whereRaw .= " AND (ORNo between '{$or_start}' and '{$or_end}') ";
        }

        if ($payer_type != '0' && $payer_type != ''){
            $whereRaw .= " AND (payortype = '".$payer_type."') ";
        }

        if ($status == '3'){
            $whereRaw .= " AND (dbo.fn_TotalReceiptAmount('20',ORNo) = 0 ) ";
        }

        if ($inclvoid == ''){
            $whereRaw .= " AND IsVoid = 0 ";
        }
        
		if($filter!=''){
		    $whereRaw .= " AND (ORNo LIKE '%".$filter."%' OR PayorID LIKE '%".$filter."%' OR PayorName LIKE '%".$filter."%')";
		}
		

       if($mode == 'total'){
            $rs =  DB::table('es_officialreceipts As a')
                    ->whereRaw($whereRaw)
                    ->count();
        }else{
              $rs = DB::table('es_officialreceipts')
                 ->selectRaw("*, dbo.fn_CurrencyCode(CurrencyID) As Curr, dbo.fn_TotalReceiptAmount('20',ORNo) As Descripancy ")
                 ->whereRaw($whereRaw)
                 ->orderBy('ORNo','ASC')
                 ->skip($skip)
                 ->take($take)
                 ->get();
        }

        return $rs;
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        
        if (Request::ajax()) {
        
            $this->initializer();
        
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) 
            {
			    case  'o-post':
				case  'o-upost':
                    $or_num = $post['ornum'];
					$topost = (($post['event']=='o-post')?0:1);
					$exec   = DB::STATEMENT("UPDATE ES_Journals SET NonLedger=".$topost." WHERE ReferenceNo='".$or_num."' AND TransID=20
											 UPDATE ES_OfficialReceipts SET ForPosting=".$topost." WHERE ORNo='".$or_num."'");
					if($exec){
					   $response = ['error'=>false,'message'=>'Success', 'data'=>$or_num ];    
					   SystemLog('Cashier','Official Receipt',(($post['event']=='o-post')?'post':'unpost'),'', ('ORN:'.$or_num), '');
					}else{
					   $response = ['error'=>false,'message'=>'Failed to execute', 'data'=>$or_num ];    
					}						 
				break;
                case  'delete':                
                
                    $or_num = $post['ornum'];
                    $data =  $this->services->deleteReceipt($or_num);
                    if($data =='1'){
                        $response = ['error'=>false,'message'=>'success', 'data' => $data ];    
					    SystemLog('Cashier','Official Receipt','delete','', ('ORN:'.$or_num), '');
                    }else{
                        $response = ['error'=>true,'message'=>'error', 'data' => $data ];
                    }
                                        
                break;
                
                case  'void':                
                
                    $or_num = $post['ornum'];
                    $data =  $this->services->voidReceipt($or_num);
                    if($data =='1'){
                        $response = ['error'=>false,'message'=>'success', 'data' => $data ];    
					    SystemLog('Cashier','Official Receipt','void','', ('ORN:'.$or_num), '');
                    }else{
                        $response = ['error'=>true,'message'=>'error', 'data' => $data ];
                    }
                                        
                break;
                
                case 'orsearch':
                    $lst = view($this->views.'list',  ['data' =>  $this->getData() ] )->render(); 
                    $response = ['error'=>false,'message'=>'success', 'view' => $lst ];   
                break;     
                case 'dtsearch':
				    $draw  = 1;
					$tmpflt=Request::get('search');
					$filter='';
					if($tmpflt && @array_key_exists('value',$tmpflt)){
					  $filter = trim($tmpflt['value']);
					}
					
				    $exec  = $this->getDataTable('data',$filter,Request::get('draw'),Request::get('length'),Request::get('start'));
					$arr   = array();
					if($exec){
					    $length= Request::get('length');
						$start = Request::get('start');
						$draw  = ((intval($start)<=0)?1:($start/$length));
						$draw  = Request::get('draw');
						$j     = (($start=='')?1:($start+1));

						$cash = 0;
						$check = 0;
						$card = 0;
						$change = 0;
						$totaldue = 0;
						$discount=0;

						$descripancy = 0;
						$status = "";
						$payer_type = "";
						$txn='';
					
						foreach($exec as $r){
                            $tmparr = array();
							$cash = $r->CashReceive;
							$check = $r->CheckReceive;
							$card = $r->CardReceive;
							$change = $r->Change;
							$descripancy = 0;

							$descripancy = ($cash + $check + $card) - $r->Descripancy;

							if( $descripancy > 0){
								$descripancy = '('.number_format($descripancy,2).')';
							}

							if($r->ForPosting == '1'){
								$status="Unposted";
							}else{
								$status="Posted";
							}
							if($r->PayorType != ''){
								$payer_type = get_PayerType($r->PayorType);
								//$payer_type = $r->PayorType;
							}else{
								$payer_type = "";
							}

							$txn = get_TransactionType($r->TransType)->TransName;
                            $tmparr = array(
											 'DT_RowId'=>$r->ORNo
											,'DT_RowData'=>array('data-receipt'=>$r->ORNo,'data-void'=>$r->IsVoid,'data-type'=>$r->PayorType,'data-txn'=>$r->TransType)
											,'DT_RowAttr'=>array('data-receipt'=>$r->ORNo,'data-void'=>$r->IsVoid,'data-type'=>$r->PayorType,'data-txn'=>$r->TransType)
											,'0'=>'<span class="rinfo" data-receipt="'.$r->ORNo.'" data-void="'.$r->IsVoid.'" data-type="'.$r->PayorType.'" data-txn="'.$r->TransType.'">'.$j.'</span>'
											,'1'=>'<a  class="fa fa-print" href="'.url('cashier/cashiering/print?or='.$r->ORNo).'"  target="_blank"> Print</a>'
											,'2'=>(($status=='Posted')?'<input type="checkbox" class="box" checked="" />':'<input type="checkbox" class="box" />')
											,'3'=>('OR #<span class="bold"><a href="'.url('cashier/cashiering?edit='.$r->ORNo).'" target="_blank">'.$r->ORNo.'</a></span><br />'.$r->Date)
											,'4'=>($r->PayorName.'<br /> <span class="font-xs">'.$payer_type.' : '.$r->PayorID.'</span>')
											,'5'=>($txn.' : '.$r->RefNo)
											,'6'=>(number_format($r->AmountDue,2))
											,'7'=>(number_format($cash,2))
											,'8'=>(number_format($check,2))
											,'9'=>(number_format($card,2))
											,'10'=>(number_format($change,2))
											,'11'=>(number_format(floatval($descripancy),2))
											,'12'=>($r->CashierID)
											,'13'=>($r->CreatedDate)
											,'14'=>($r->CreatedTerminal)
											,'15'=>$status
							             );
							$arr[] = $tmparr;
							$j++;
							
						}
					}
					
					if(count($arr)<=0){
					  $draw=0;
					}
					
				    return ['draw'=>$draw,'recordsTotal'=>$this->getDataTable('total'),'recordsFiltered'=>$this->getDataTable('total',$filter),'data'=>$arr,'error'=>false];
                break;				
                
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }
    
    private function initializer()
    {
        $this->permission = new Permission($this->ModuleName);
        $this->services = new services;        
    }
}