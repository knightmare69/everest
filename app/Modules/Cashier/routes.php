<?php

Route::group(['prefix' => 'cashier','middleware'=>'auth'], function () {

    Route::group(['prefix' => 'cashiering', 'middleware' => 'auth'], function () {
        Route::get('/', 'Cashiering@index');
        Route::post('event', 'Cashiering@event');
        Route::get('/print', 'Cashiering@eprint');
    });
    
    Route::group(['prefix' => 'official-receipts', 'middleware' => 'auth'], function () {
        Route::get('/', 'OfficialReceipts@index');
        Route::get('event', 'OfficialReceipts@event');
        Route::post('event', 'OfficialReceipts@event');
        
    });

    
    Route::group(['prefix' => 'online-transactions', 'middleware' => 'auth'], function () {
        Route::get('/', 'OnlineTransactions@index');        
        Route::get('event', 'OnlineTransactions@event');   
        Route::post('event', 'OnlineTransactions@event');
        Route::post('print', 'OnlineTransactions@print');
    });
});

?>