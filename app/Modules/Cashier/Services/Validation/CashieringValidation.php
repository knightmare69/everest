<?php

namespace App\Modules\Cashier\Services\Validation;

use Validator;

class CashieringValidation
{
    public function payment_form($post)
    {
        $txn = getObjectValue($post, 'txn');
        if(trim(getObjectValue($post, 'ref'))=='' || getObjectValue($post, 'txn')==false){
		  $post['ref']=encode(0);
		}		
		
        $validator = validator::make(
            [
                'ref' => decode(getObjectValue($post, 'ref')),
                'due' => trimmed( getObjectValue($post, 'due') , 'd')
            ],
            [
                'ref' => 'required|string',                
                'due' => 'required|numeric'
            ]
        );

        return $validator;
    }

    public function checkout($post)
    {   
	    $txn = decode(getObjectValue($post, 'txn'));
        if(trim(getObjectValue($post, 'ref'))=='' || getObjectValue($post, 'ref')==false){
		  $post['ref']=encode(getObjectValue($post, 'orno'));
		}
		
        $validator = validator::make(
            [
                 'ref' => decode(getObjectValue($post, 'ref'))
                ,'due' => trimmed( getObjectValue($post, 'due') , 'd')
                ,'idno' => trimmed( getObjectValue($post, 'idno'))
                ,'txn' => decode(getObjectValue($post, 'txn')) 
                ,'type' => decode(getObjectValue($post, 'type'))
                ,'orno' => decode(getObjectValue($post, 'orno'))
            ],
            [
                'ref' => 'required|string'    
               ,'due' => 'required|numeric'
               ,'idno' => 'required|string'
               ,'txn' => 'required|integer'
               ,'type' => 'required|integer'
               ,'orno' => 'required|string'
            ]
        );

        return $validator;
    }
    
   
    
}
?>