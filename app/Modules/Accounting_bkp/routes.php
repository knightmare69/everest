<?php
Route::group(['prefix' => 'accounting','middleware'=>'auth'], function () {
		    Route::get('/','Accounting@index');
			Route::get('/txn','Accounting@txn');
		    Route::post('/txn','Accounting@txn');
		    Route::get('/pdf','Accounting@pdf');
			Route::get('/dmcm','DMCM@index');
			Route::get('/reports','Reports@index');
			Route::get('/print','Reports@print_report');		
			Route::get('/export','Reports@export_report');		
});

Route::group(['prefix' => 'epayment'], function () {
		    Route::get('/','Epayment@index');
		    Route::post('/txn','Epayment@txn');
});

Route::get('recalc','Accounting@botfunc');
?>