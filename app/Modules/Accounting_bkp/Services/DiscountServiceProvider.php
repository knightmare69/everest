<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\City\Validation;
use DB;
use Request;

Trait DiscountServiceProvider {

	public function saveDiscount() {

		DB::table('ES_OtherTransactions')
			->insert([
					'TransType' => Request::get('transactiion'),
					'CampusID' => Request::get('campus'),
					'TermID' => Request::get('acadyear'),
					'Date' => setDate(Request::get('date_entry'),'Y-m-d H:i:s'),
					'TblFeeID' => 0,
					'SchoProviderType' => Request::get('provider_type'),
					'SchoProviderID' => Request::get('provider'),
					// 'Remarks' => Request::get(''),
					'IDType' => Request::get('provider_type'),
					'IDNo' => $this->getStudentNoByRegId(Request::get('reg')),
					// 'ORNo' => Request::get(''),
					'TotalNetAssessed' => Request::get('applied_net_assessed') ? Request::get('applied_amount') : 0,
					'TotalDiscount' => Request::get('applied_net_assessed') ? Request::get('applied_amount') : 0,
					'RegID' => Request::get('reg'),
					'AssessedBy' => getUserName(),
					'AssessedDate' => date('Y-m-d H:i:s')
				]);

		return successSave();
	}

	public function removeDiscount($id) {
		if (DB::table('ES_OtherTransactions')->where('RefNo',$id)->delete()) {
			return successDelete();
		}
		return errorDelete();
	}

	public function getDiscountsByRegId($RegId) {
		$join = DB::table('ES_OtherTransactions as t')
			->join('ES_SchoProviders as p',
					'p.SchoProviderID','=','t.SchoProviderID'
				)
			// ->where('t.IDType',1)
			->where('t.RegID',$RegId)
			->get();

		return $join;
	}

	public function SearchStudent() {
		$term = Request::get('term');
		$data = DB::table('ES_Registrations as r')
				->select(
						'r.RegID',
						DB::raw("LastName+','+FirstName+' '+MiddleName as name")
					)
				->join('ES_Students as s',
						's.StudentNo','=','r.StudentNo'
					)
				->limit(100)
				->whereRaw("(s.FirstName like '%$term%' OR s.LastName like '%$term%')")
				->get();
		$records = [];
		foreach($data as $row) {
			$records[] = array(
					'name' => $row->name,
					'id' => $row->RegID
				);
		}

		return $records;
	}

	public function getDiscountProviders() {
		return 
			DB::table('ES_SchoProviders')
				->select([
						'SchoProviderID',
						'ProvCode',
						'ProvName',
						'ProvShort'
					])
				->get();
	}

	public function showDiscounts()
	{
		return view($this->views.'tables.discounts',['data' => $this->getDiscountsByRegId(Request::get('regid'))]);
	}

	public function showProviderModal() {
		return view($this->views.'tables.providers',['data' => $this->getDiscountProviders()]);
	}

	public function getCampus() {
		return DB::table('ES_Campus')
						->select(['CampusID','ShortName'])
						->get();
	}

	public function getTerms() {
		return DB::table('ES_AYTerm')
					->select(['TermID','AcademicYear','SchoolTerm','Active_OnlineEnrolment'])
					->orderBy('TermID','desc')
					->get();
	}

	private function getStudentNoByRegId($regId) {
		return DB::table('ES_Registrations')->where('RegID',$regId)->select('StudentNo')->pluck('StudentNo');
	}

}	
?>