<?php
namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Services\Assessment\assessment as assess;

use DB;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;

class Epayment extends Controller
{
   private $media = [ 'Title' => 'Online Payment',
        'Description'   => 'Online Payment for Student/Parent',
        'js'            => ['Accounting/epayment'],
        'plugin_js'     => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
							'jquery-validation/js/jquery.validate.min',
							'jquery-validation/js/additional-methods.min',
							'bootstrap-wizard/jquery.bootstrap.wizard.min',
                            'SmartNotification/SmartNotification.min'
                           ],
	 /* 'init'          => ['SECURITY.init()'], */
        'plugin_css'    => ['bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification',],	
    ];
    
    private $url =  [
        'page'  => 'accounting/',
        'form' => 'form',
    ];
    
	public $r_view = 'Accounting.Views.epayment.';
	
    public function index(){
	    $this->initializer();
		return view('layout',array('content'=>view($this->r_view.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
        //return view(config('app.403'));
    }
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event'))
            {
			  case 'get_paynamics':
			            $stdno       = Request::get('studno');
						$exec        = DB::SELECT("SELECT TOP 1 * FROM ES_Students WHERE StudentNo='".$stdno."'");
						$studentinfo = $this->getstudentinfo($stdno);
			            $_mid        = "000000110118836A18E8"; //<-- your merchant id
						$cert        = "B850B12E400080A23AC282ABACE77C60"; //<-- your merchant key
                        
						$_requestid = substr(uniqid(), 0, 13);
						$_ipaddress = "192.168.10.1";
						$_noturl    = ""; // url where response is posted
						$_resurl    = url(); //url of merchant landing page
						$_cancelurl = url(); //url of merchant landing page
						$_fname     = $studentinfo->FirstName; // kindly set this to first name of the cutomer
						$_fname     = (($_fname=='')?'Not Applicable':$_fname);
						$_mname     = $studentinfo->MiddleName; // kindly set this to middle name of the cutomer
						$_mname     = (($_mname=='')?'Not Specified':$_mname);
						$_lname     = $studentinfo->LastName; // kindly set this to last name of the cutomer
						$_lname     = (($_lname=='')?'Not Specified':$_lname);
						$_addr1     = $studentinfo->Res_Address; // kindly set this to address1 of the cutomer
						$_addr1     = (($_addr1=='')?'638 Mendiola St':$_addr1);
						$_addr2     = trim($studentinfo->Res_Address2);// kindly set this to address2 of the cutomer
						$_addr2     = ((trim($_addr2)=='')?'San Miguel':$_addr2);
						$_city      = $studentinfo->Res_TownCity; // kindly set this to city of the cutomer
						$_city      = (($_city=='')?'Manila':$_city);
						$_state     = $studentinfo->Res_Province; // kindly set this to state of the cutomer
						$_state     = (($_state=='')?'Metro Manila':$_state);
						$_country   = "PH"; // kindly set this to country of the cutomer ex. PH
						$_zip       = $studentinfo->Res_ZipCode; // kindly set this to zip/postal of the cutomer
						$_zip       = (($_zip=='')?'1005':$_zip);
						$_sec3d     = "try3d"; // 
						$_email     = $studentinfo->StudentEmail; // kindly set this to email of the cutomer
						$_phone     = $studentinfo->StudentNo; // set to this ID number
						$_mobile    = $studentinfo->StudentContact; // kindly set this to mobile number of the cutomer
						if(($_phone=='' || $_phone=='-') && ($_mobile!='' && $_mobile!='-')){
						    $_phone = $_mobile;
						}
						if($_phone=='' || $_phone=='-'){
						    $_phone = '027356011';
						}
						$_clientip = $_SERVER['REMOTE_ADDR'];
						$t_amount  = 1000; // value of payment
						$_amount   = number_format($t_amount,2, '.', ''); 
						$_currency = "PHP"; //PHP or USD
						$forSign   = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;
						
						$_sign     = hash("sha512", $forSign.$cert);
						$strxml    = "";
						
						$strxml    = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
						$strxml    = $strxml . "<Request>";
						$strxml = $strxml . "<orders>";
						$strxml = $strxml . "<items>";
						$strxml = $strxml . "<Items>";
						$strxml = $strxml . "<itemname>item 1</itemname><quantity>1</quantity><amount>".$_amount."</amount>"; // pls change this value to the preferred item to be seen by customer. (eg. Room Detail (itemname - Beach Villa, 1 Room, 2 Adults       quantity - 0       amount - 10)) NOTE : total amount of item/s should be equal to the amount passed in amount xml node below. 
						$strxml = $strxml . "</Items>";
						$strxml = $strxml . "</items>";
						$strxml = $strxml . "</orders>";
						$strxml = $strxml . "<mid>" . $_mid . "</mid>";
						$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
						$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
						$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
						$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
						$strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
						$strxml = $strxml . "<mtac_url></mtac_url>"; // pls set this to the url where your terms and conditions are hosted
						$strxml = $strxml . "<descriptor_note>''</descriptor_note>"; // pls set this to the descriptor of the merchant ""
						$strxml = $strxml . "<fname>" . $_fname . "</fname>";
						$strxml = $strxml . "<lname>" . $_lname . "</lname>";
						$strxml = $strxml . "<mname>" . $_mname . "</mname>";
						$strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
						$strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
						$strxml = $strxml . "<city>" . $_city . "</city>";
						$strxml = $strxml . "<state>" . $_state . "</state>";
						$strxml = $strxml . "<country>" . $_country . "</country>";
						$strxml = $strxml . "<zip>" . $_zip . "</zip>";
						$strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
						$strxml = $strxml . "<trxtype>sale</trxtype>";
						$strxml = $strxml . "<email>" . $_email . "</email>";
						$strxml = $strxml . "<phone>" . $_phone . "</phone>";
						$strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
						$strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
						$strxml = $strxml . "<amount>" . $_amount . "</amount>";
						$strxml = $strxml . "<currency>" . $_currency . "</currency>";
						$strxml = $strxml . "<mlogo_url>http://www.sanbeda.edu.ph/img/preloadlogo.png</mlogo_url>";// pls set this to the url where your logo is hosted
						$strxml    = $strxml . "<pmethod></pmethod>";
						$strxml    = $strxml . "<signature>" . $_sign . "</signature>";
						$strxml    = $strxml . "</Request>";
						$b64string =  base64_encode($strxml);
						$content   = '<form name="form1" method="post" action="https://testpti.payserv.net/webpaymentv2/default.aspx">
										<input type="hidden" name="paymentrequest" id="paymentrequest" value="'.$b64string.'" style="width:800px; padding: 10px;">
										<input type="submit" class="btn btn-primary btn-lg btn-block" value="Pay Now" style="margin-top: 50px;">
									  </form>';
						$response = ['success'=>true,'content'=>$content,'error'=>false,'message'=>'Success!'];
			  break;
			  case 'get_dragonpay':
			            $b64string =  '';
						$content   = '<form name="form1" method="post" action="'.url().'/epayment/txn?event=dragonpay'.'">
										<input type="hidden" name="paymentrequest" id="paymentrequest" value="'.$b64string.'" style="width:800px; padding: 10px;">
										<input type="submit" class="btn btn-primary btn-lg btn-block" value="Pay Now" style="margin-top: 50px;">
									  </form>';
						$response = ['success'=>true,'content'=>$content,'error'=>false,'message'=>'Success!'];
			  break;
			  case 'dragonpay':
			    echo 'testing';
				die();
				define('MERCHANT_ID', 'SANBEDA');
				define('MERCHANT_PASSWORD', 'n3i97U9R3Kqrr4N');

				define('ENV_TEST', 0);
				define('ENV_LIVE', 1);

				$environment = ENV_TEST;
				$studno      = Request::get('stdno');
				$errors      = array();
				$is_link     = false;

				$parameters = array(
					  'merchantid' => MERCHANT_ID,
					  'txnid' => '000001',
					  'amount' => 100,
					  'ccy' => 'PHP',
					  'description' => 'My order description.',
					  'email' => 'sample@merchant.ph',
				);

				$fields = array(
					  'txnid' => array(
						  'label' => 'Transaction ID',
						  'type' => 'text',
						  'attributes' => array(),
						  'filter' => FILTER_SANITIZE_STRING,
						  'filter_flags' => array(FILTER_FLAG_STRIP_LOW),
					  ),
					  'amount' => array(
						  'label' => 'Amount',
						  'type' => 'number',
						  'attributes' => array('step="0.01"'),
						  'filter' => FILTER_SANITIZE_NUMBER_FLOAT,
						  'filter_flags' => array(FILTER_FLAG_ALLOW_THOUSAND, FILTER_FLAG_ALLOW_FRACTION),
					  ),
					  'description' => array(
						  'label' => 'Description',
						  'type' => 'text',
						  'attributes' => array(),
						  'filter' => FILTER_SANITIZE_STRING,
						  'filter_flags' => array(FILTER_FLAG_STRIP_LOW),
					  ),
					  'email' => array(
						  'label' => 'Email',
						  'type' => 'email',
						  'attributes' => array(),
						  'filter' => FILTER_SANITIZE_EMAIL,
						  'filter_flags' => array(),
					  ),
				);
				  
				foreach ($fields as $key => $value) {
				  // Sanitize user input. However:
				  // NOTE: this is a sample, user's SHOULD NOT be inputting these values.
				  if (isset($_POST[$key])) {
					  $parameters[$key] = filter_input(INPUT_POST, $key, $value['filter'],
						array_reduce($value['filter_flags'], function ($a, $b) { return $a | $b; }, 0));
				  }
				}

				// Validate values.
				// Example, amount validation.
				// Do not rely on browser validation as the client can manually send
				// invalid values, or be using old browsers.
				if (!is_numeric($parameters['amount'])) {
				  $errors[] = 'Amount should be a number.';
				}
				else if ($parameters['amount'] <= 0) {
				  $errors[] = 'Amount should be greater than 0.';
				}

				if (empty($errors)) {
				  // Transform amount to correct format. (2 decimal places,
				  // decimal separated by period, no thousands separator)
				  $parameters['amount'] = number_format($parameters['amount'], 2, '.', '');
				  // Unset later from parameter after digest.
				  $parameters['key'] = MERCHANT_PASSWORD;
				  $digest_string = implode(':', $parameters);
				  unset($parameters['key']);
				  // NOTE: To check for invalid digest errors,
				  // uncomment this to see the digest string generated for computation.
				  // var_dump($digest_string); $is_link = true;
				  $parameters['digest'] = sha1($digest_string);
				  $url = 'https://gw.dragonpay.ph/Pay.aspx?';
				  if ($environment == ENV_TEST) {
					$url = 'http://test.dragonpay.ph/Pay.aspx?';
					//$url = 'https://test.dragonpay.ph/GenPay.aspx?';
				  }

				  $url .= http_build_query($parameters, '', '&');

				  if ($is_link) {
					echo '<br><a href="' . $url . '">' . $url . '</a>';
				  }
				  else {
					header("Location: $url");
				  }
				}
				
			  break;
			  default:
              $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
			  break;
			}
		}
        return $response;		
	}
	
	private function initializer()
    {
	   ini_set('max_execution_time', 300);
		$this->assess = new assess;
    }
	
	private function getstudentinfo($stdno=''){
		$studentinfo = DB::select("SELECT TOP 1 r.RegID,
												r.RegDate,
												r.BillingRefNo,
												r.FirstPaymentDueDate,
												r.SecondPaymentDueDate,
												r.ThirdPaymentDueDate,
												r.FourthPaymentDueDate,
												s.StudentNo,
												s.LastName,
												s.FirstName,
												s.MiddleName,
												s.MiddleInitial,
												ISNULL(s.Email,ISNULL(s.Father_Email,s.Mother_Email)) as StudentEmail,
												ISNULL(s.TelNo,s.MobileNo) as StudentContact,
												ISNULL(s.Father_TelNo,s.Father_Mobile) as FatherContact,
												ISNULL(s.Mother_TelNo,s.Mother_Mobile) as MotherContact,
												ISNULL(f.Guardian_Address,s.Res_Address) as Res_Address,
												CONCAT(s.Res_Barangay,' ',s.Res_Street) as Res_Address2,
												ISNULL(f.Guardian_Billing_TownCity,s.Res_TownCity) as Res_TownCity,
												ISNULL(f.Guardian_Province,s.Res_Province) as Res_Province, 
												ISNULL(f.Guardian_ZipCode,s.Res_ZipCode) as Res_ZipCode
										   FROM ES_Students as s 
									  LEFT JOIN ESv2_Admission_FamilyBackground as f ON s.FamilyID=f.FamilyID
									 INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo
										  WHERE s.StudentNo='".$stdno."'
									   ORDER BY r.RegID DESC");
									   
	    return $studentinfo[0];
	}
	
	public function init($apage='assess'){
		$this->initializer();
		$studentinfo = DB::select("SELECT TOP 1 r.RegID,
												r.RegDate,
												r.BillingRefNo,
												r.FirstPaymentDueDate,
												r.SecondPaymentDueDate,
												r.ThirdPaymentDueDate,
												r.FourthPaymentDueDate,
												s.StudentNo,
												s.LastName,
												s.FirstName,
												s.MiddleName,
												s.MiddleInitial,
												ISNULL(s.Email,ISNULL(s.Father_Email,s.Mother_Email)) as StudentEmail,
												ISNULL(s.TelNo,s.MobileNo) as StudentContact,
												ISNULL(s.Father_TelNo,s.Father_Mobile) as FatherContact,
												ISNULL(s.Mother_TelNo,s.Mother_Mobile) as MotherContact,
												ISNULL(f.Guardian_Address,s.Res_Address) as Res_Address,
												CONCAT(s.Res_Barangay,' ',s.Res_Street) as Res_Address2,
												ISNULL(f.Guardian_Billing_TownCity,s.Res_TownCity) as Res_TownCity,
												ISNULL(f.Guardian_Province,s.Res_Province) as Res_Province, 
												ISNULL(f.Guardian_ZipCode,s.Res_ZipCode) as Res_ZipCode
										   FROM ES_Students as s 
									  LEFT JOIN ESv2_Admission_FamilyBackground as f ON s.FamilyID=f.FamilyID
									 INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo
										  WHERE s.StudentNo='2013349'
									   ORDER BY r.RegID DESC");
		$data = array(
		         'stdno'       => $studentinfo[0]->StudentNo,
				 'studentinfo' => $studentinfo[0],
		        );
		return $data;
	}		
	
}
?>