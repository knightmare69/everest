<?php 
 $studentinfo = ((isset($studentinfo))?$studentinfo:array());
?>
<div id="content" style="overflow:auto;">
 <div class="col-sm-12 col-md-12">
 <link rel="stylesheet" href="<?php echo url('assets/resources/style.css');?>">
 <link rel="stylesheet" href="<?php echo url('assets/resources/css?family=Montserrat');?>">
 <section class="shopping-cart dark">
	 		<div class="container">
		        <div class="block-heading text-center hidden">
		          <p><img src="<?php echo url('assets/resources/logo-large.png');?>" style="width: 25%;"/></p>
		        </div>
		        <div class="content">
					<?php
						$_mid       = "000000110118836A18E8"; //<-- your merchant id
						$_requestid = substr(uniqid(), 0, 13);
						$_ipaddress = "192.168.10.1";
						$_noturl    = ""; // url where response is posted
						$_resurl    = url(); //url of merchant landing page
						$_cancelurl = url(); //url of merchant landing page
						$_fname     = $studentinfo->FirstName; // kindly set this to first name of the cutomer
						$_fname     = (($_fname=='')?'Not Applicable':$_fname);
						$_mname     = $studentinfo->MiddleName; // kindly set this to middle name of the cutomer
						$_mname     = (($_mname=='')?'Not Specified':$_mname);
						$_lname     = $studentinfo->LastName; // kindly set this to last name of the cutomer
						$_lname     = (($_lname=='')?'Not Specified':$_lname);
						$_addr1     = $studentinfo->Res_Address; // kindly set this to address1 of the cutomer
						$_addr1     = (($_addr1=='')?'638 Mendiola St':$_addr1);
						$_addr2     = trim($studentinfo->Res_Address2);// kindly set this to address2 of the cutomer
						$_addr2     = ((trim($_addr2)=='')?'San Miguel':$_addr2);
						$_city      = $studentinfo->Res_TownCity; // kindly set this to city of the cutomer
						$_city      = (($_city=='')?'Manila':$_city);
						$_state     = $studentinfo->Res_Province; // kindly set this to state of the cutomer
						$_state     = (($_state=='')?'Metro Manila':$_state);
						$_country   = "PH"; // kindly set this to country of the cutomer ex. PH
						$_zip       = $studentinfo->Res_ZipCode; // kindly set this to zip/postal of the cutomer
						$_zip       = (($_zip=='')?'1005':$_zip);
						$_sec3d     = "try3d"; // 
						$_email     = $studentinfo->StudentEmail; // kindly set this to email of the cutomer
						$_email     = ((trim($_email)=='')?'jhe69samson@gmail.com':$_email);
						$_phone     = $studentinfo->StudentContact; // set to this ID number
						$_mobile    = $studentinfo->StudentContact; // kindly set this to mobile number of the cutomer
						if(($_phone=='' || $_phone=='-') && ($_mobile!='' && $_mobile!='-')){
						    $_phone = $_mobile;
						}
						if($_phone=='' || $_phone=='-'){
						    $_phone = '027356011';
						}
						$_clientip  = $_SERVER['REMOTE_ADDR'];
						$t_amount   = 1000; // value of payment
						$_amount    = number_format($t_amount,2, '.', ''); 
						$_currency  = "PHP"; //PHP or USD
						$forSign    = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;
						
						$cert       = "B850B12E400080A23AC282ABACE77C60"; //<-- your merchant key
                        $_sign      = hash("sha512", $forSign.$cert);
						$xmlstr     = "";
						$strxml     = "";
					?>
	 				<div class="row">
	 					<div class="col-md-12 col-lg-8">
	 						<div class="info-details">
								<h3 class="title">Payment Information</h3>
								<div class="row">
								  <div class="form-group col-sm-4">
									<label for="name-holder">First Name</label>
									<input id="fname" name="txtfname" type="text" class="form-control" placeholder="First Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_fname;?>" disabled>
								  </div>

								  <div class="form-group col-sm-4">
									<label for="name-holder">Middle Name</label>
									<input id="mname" name="txtmname" type="text" class="form-control" placeholder="Middle Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_mname;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-4">
									<label for="name-holder">Last Name</label>
									<input id="lname" name="txtlname" type="text" class="form-control" placeholder="Last Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_lname;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Address 1</label>
									<input id="address1" name="txtaddress1" type="text" class="form-control" placeholder="Address 1" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_addr1;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Address 2</label>
									<input id="address2" name="txtaddress2" type="text" class="form-control" placeholder="Address 2" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_addr2;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">City</label>
									<input id="city" name="txtcity" type="text" class="form-control" placeholder="City" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_city;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Country</label>
									<input id="country" name="txtcountry" type="text" class="form-control" placeholder="country" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_country;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Zip Code</label>
									<input id="zipcode" name="txtzip" type="text" class="form-control" placeholder="country" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_zip;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Email</label>
									<input id="email" name="txtemail" type="text" class="form-control" placeholder="Email" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_email;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Mobile Number</label>
									<input id="number" name="txtnumber" type="text" class="form-control" placeholder="Mobile Number" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_mobile;?>" disabled>
								  </div>
								  
								</div>
							 </div>
			 			</div>
			 			<div class="col-md-12 col-lg-4" style="height:100%;">
			 				<div class="summary">
			 					<h3>Current Balance</h3>
								<div class="summary-item"><span class="price">PHP : <?php echo number_format($t_amount,2); ?></span></div>
			 					<div class="summary-item">
									<h4>Payment Solution</h4>
									<div class="radio-list">
										<label>
										<div class="radio"><input type="radio" name="optpayment" id="optPaynamics" data-event="get_paynamics" value="option1" checked=""></div> Paynamics</label>
										<label>
										<div class="radio"><input type="radio" name="optpayment" id="optDragonPay" data-event="get_dragonpay" value="option2"></div> DragonPay </label>
									</div>
								</div>
			 					<div class="summary-item"><span class="text"></span><span class="price"></span></div>
								<?php
									$strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
									$strxml = $strxml . "<Request>";
									$strxml = $strxml . "<orders>";
									$strxml = $strxml . "<items>";
									$strxml = $strxml . "<Items>";
									$strxml = $strxml . "<itemname>item 1</itemname><quantity>1</quantity><amount>".$_amount."</amount>"; // pls change this value to the preferred item to be seen by customer. (eg. Room Detail (itemname - Beach Villa, 1 Room, 2 Adults       quantity - 0       amount - 10)) NOTE : total amount of item/s should be equal to the amount passed in amount xml node below. 
									$strxml = $strxml . "</Items>";
									$strxml = $strxml . "</items>";
									$strxml = $strxml . "</orders>";
									$strxml = $strxml . "<mid>" . $_mid . "</mid>";
									$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
									$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
									$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
									$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
									$strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
									$strxml = $strxml . "<mtac_url></mtac_url>"; // pls set this to the url where your terms and conditions are hosted
									$strxml = $strxml . "<descriptor_note>''</descriptor_note>"; // pls set this to the descriptor of the merchant ""
									$strxml = $strxml . "<fname>" . $_fname . "</fname>";
									$strxml = $strxml . "<lname>" . $_lname . "</lname>";
									$strxml = $strxml . "<mname>" . $_mname . "</mname>";
									$strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
									$strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
									$strxml = $strxml . "<city>" . $_city . "</city>";
									$strxml = $strxml . "<state>" . $_state . "</state>";
									$strxml = $strxml . "<country>" . $_country . "</country>";
									$strxml = $strxml . "<zip>" . $_zip . "</zip>";
									$strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
									$strxml = $strxml . "<trxtype>sale</trxtype>";
									$strxml = $strxml . "<email>" . $_email . "</email>";
									$strxml = $strxml . "<phone>" . $_phone . "</phone>";
									$strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
									$strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
									$strxml = $strxml . "<amount>" . $_amount . "</amount>";
									$strxml = $strxml . "<currency>" . $_currency . "</currency>";
									$strxml = $strxml . "<mlogo_url>http://www.sanbeda.edu.ph/img/preloadlogo.png</mlogo_url>";// pls set this to the url where your logo is hosted
									$strxml = $strxml . "<pmethod></pmethod>";
									$strxml = $strxml . "<signature>" . $_sign . "</signature>";
									$strxml = $strxml . "</Request>";
									$b64string =  base64_encode($strxml);
								?>
								<div class="submit_form">
								<form name="form1" method="post" action="https://testpti.payserv.net/webpaymentv2/default.aspx">
									<input type="hidden" name="paymentrequest" id="paymentrequest" value="<?php echo $b64string ?>" style="width:800px; padding: 10px;">
									<input type="submit" class="btn btn-primary btn-lg btn-block" value="Pay Now" style="margin-top: 50px;">
								</form>
								</div>
				 			</div>
			 			</div>
		 			</div> 
		 		</div>
	 		</div>
		</section>
 </div>
	 
</div>

<script src="<?php echo url('assets/resources/jquery-3.2.1.min.js');?>"></script>
<script src="<?php echo url('assets/resources/bootstrap.min.js');?>""></script>