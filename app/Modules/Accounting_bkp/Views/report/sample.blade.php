<style>
.barcode,.pull-right
{
 text-align: right;
 margin:-5px;
}
.text-center
{
 text-align:center;	
}
.assessment
{
 font-size:small;
}
.info
{
  border: 2px solid black;
  border-collapse: collapse;
  padding:3px;
  background-color:rgb(230, 234, 241);
  font-size:small;
}
.payinfo
{
  border: 1px solid black;
  border-collapse: collapse;
  padding:3px;
  font-size:medium;
}
.bordered
{
 border:1px solid black;		
}
.border-bottom
{
 border-bottom:1px solid black;	
}
.border-top
{
 border-top:1px solid black;	
}
.space
{
 padding:0px;	
}
</style>
<table>
 <thead>
   <tr>
     <th class="bordered">StudentNo</th>
     <th class="bordered">Fullname</th>
   </tr>
 </thead>
 <tbody>
   <?php
   foreach($rs as $r){
     echo '<tr>
			 <td class="bordered">'.$r->StudentNo.'</td>
			 <td class="bordered">'.$r->Fullname.'</td>
		   </tr>';	   
   }
   ?>
 </tbody>
</table>