<div class="col-sm-12"> 
  <div class="table-responsive">
	<table class="table table-bordered table-condense">
	  <thead>
		<th>Description</th>
		<th>Price</th>
		<th>Quantity</th>
		<th>Amount</th>
	  </thead>
	  <tbody>
		 <?php 
		  $test = DB::select("SELECT * FROM ES_Accounts WHERE AcctID IN ('1021','1022','1023','1024')");
		  $sum  = array();
		  if($test && count($test)>0){
		   foreach($test as $s){
		    $sum[$s->AcctID]=$s->DefaultAmount;
		   }
		  }
		 ?>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="1021"  data-grdmin="1" data-grdmax="1">
		  <td><b>Getting Ready for Kindergarten</b></td>
		  <td class="component text-right" data-price="html"><?php echo number_format($sum[1021],2,".","");?></td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
		<tr data-grdmin="1" data-grdmax="1"><td colspan="4" class=""><br/></td></tr>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="1022" data-grdmin="2" data-grdmax="2">
		  <td><b>Getting Ready for Grade 1</b></td>
		  <td class="component text-right" data-price="html"><?php echo number_format($sum[1022],2,".","");?></td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
		<tr data-grdmin="2" data-grdmax="2"><td colspan="4" class=""><br/></td></tr>
		<tr><td colspan="4" class=""><b>Summer Remedial Program</b></td></tr>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="1023">
		  <td>Subject 1</td>
		  <td class="component text-right" data-price="html"><?php echo number_format($sum[1024],2,".","");?></td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="1023">
		  <td>Subject 2</td>
		  <td class="component text-right" data-price="html">9000.00</td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
		<tr><td colspan="4" class=""><br/></td></tr>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="1024">
		  <td><b>Basketball Summer Clinic</b></td>
		  <td class="component text-right" data-price="html"><?php echo number_format($sum[1024],2,".","");?></td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
	  </tbody>
	</table>
  </div>
</div>