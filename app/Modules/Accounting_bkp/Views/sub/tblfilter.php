<?php
$type = ((isset($type))?$type:1);
switch($type){	
 case '1':
 case 'student':
	if($list){
		 echo '<table class="table table-bordered table-condense" style="white-space:nowrap;">
				<thead>
				  <th>StudentNo</th>
				  <th>Last Name</th>
				  <th>First Name</th>
				  <th>Middle Name</th>
				  <th>Program</th>
				  <th>Level</th>
				  <th>Siblings</th>
				  <th>FamilyID</th>
			   </thead>
			   <tbody>';
		foreach($list as $r){
		 echo  '<tr data-list="'.$r->StudentNo.'" data-name="'.$r->LastName.', '.$r->FirstName.' '.$r->Middlename.'" 
					data-progid="'.$r->ProgID.'" data-progid="'.$r->ProgID.'" data-progname="'.$r->ProgName.'"
					data-yrlvlid="'.$r->YearLevelID.'" data-yrlvl="'.$r->YearLevelName.'">
				  <td>'.$r->StudentNo.'</td>
				  <td>'.$r->LastName.'</td>
				  <td>'.$r->FirstName.'</td>
				  <td>'.$r->Middlename.'</td>
				  <td>'.$r->ProgName.'</td>
				  <td>'.$r->YearLevelName.'</td>
				  <td>'.getTotalSiblings($r->StudentNo).'</td>
				  <td>'.$r->FamilyID.'</td>
				</tr>';
		}
		 echo '</tbody></table>';
	}else{
	  echo '<table class="table table-bordered table-condense">
				<thead>
					<th>Last Name</th>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Program</th>
					<th>Level</th>
				</thead>
			</table>';	
	} 
 break; 
 case 'scholarship':
    if($list){
		 echo '<table class="table table-bordered table-condense" style="white-space:nowrap;">
				<thead>
				  <th>Code</th>
				  <th>Provider</th>
				  <th>Schort Name</th>
			   </thead>
			   <tbody>';
		foreach($list as $r){
		 echo  '<tr data-list="'.$r->SchoProviderID.'" data-code="'.$r->ProvCode.'" data-name="'.$r->ProvName.'">
				  <td>'.$r->ProvCode.'</td>
				  <td>'.$r->ProvName.'</td>
				  <td>'.$r->ProvShort.'</td>
				</tr>';
		}
		 echo '</tbody></table>';
	}else{
	  echo '<table class="table table-bordered table-condense">
				<thead>
					<th>Code</th>
					<th>Provider</th>
					<th>Short Name</th>
				</thead>
			</table>';	
	} 
 break;
}
?>