<?php
$payment =  new App\Modules\Accounting\Services\Assessment\Assessment;
$detail  = ((isset($detail))?$detail:array());  
foreach($assess as $k=>$r){
//$netass  = $r['AssessFee'] - ($r['Discount']+$r['FinancialAid']);
  $idno               = $r['IDNo'];
  $reserve            = $payment->get_reservation($r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  $r['ActualPayment'] = $payment->get_actualpayment($r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  $r['CreditMemo']    = $payment->get_debitcredit($r['IDNo'],$r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  
  if($r['10thPayment']==0){
	  $netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']) - ($r['Discount']+$r['FinancialAid']);
	  $balance = $netass - $r['ActualPayment'];	
	  if($r['AccountID']!='111' && $r['AccountID']!='110'){
		$r['1stPayment'] = $netass;  
	  }else{
		$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);  
		$balance = $netass - $r['ActualPayment'];	
	  }
  }else{
	$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']+$r['5thPayment']+$r['6thPayment']+$r['7thPayment']+$r['8thPayment']+$r['9thPayment']+$r['10thPayment']);
	$balance = $netass - $r['ActualPayment'];	
  }
  
  if($r['AccountID']=='110' || $r['AccountID']=='111'){
    $r['1stPayment']    = $r['1stPayment'];  
	$r['ActualPayment'] = $r['ActualPayment']+$reserve;
	$balance            = $balance - $reserve;
  }
  
  if($r['CreditMemo']>0){
	$balance            = $balance - $r['CreditMemo'];   
  }
  
  echo '<tr data-entryid="'.$r['EntryID'].'" data-refno="'.$r['ReferenceNo'].'" data-acctid="'.$r['AccountID'].'" data-idno="'.$r['IDNo'].'">
		   <td style="font-size:9px !important;" data-acctcode="'.$r['AcctCode'].'">'.$r['AcctCode'].'</td>
		   <td style="font-size:9px !important;">'.$r['AcctName'].'</td>
           <td class="text-right" style="font-size:9px !important;" data-assess="'.$r['AssessFee'].'">'.number_format($r['AssessFee'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-discount="'.$r['Discount'].'">'.number_format($r['Discount'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-financial="'.$r['FinancialAid'].'">'.number_format($r['FinancialAid'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-netassess="'.number_format($netass,2,'.','').'">'.number_format($netass,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenta="'.number_format($r['1stPayment'],2,'.','').'">'.number_format($r['1stPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentb="'.number_format($r['2ndPayment'],2,'.','').'">'.number_format($r['2ndPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentc="'.number_format($r['3rdPayment'],2,'.','').'">'.number_format($r['3rdPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentd="'.number_format($r['4thPayment'],2,'.','').'">'.number_format($r['4thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paydiscount="'.number_format($r['PaymentDiscount'],2,'.','').'">'.number_format($r['PaymentDiscount'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-payment="'.number_format($r['ActualPayment'],2,'.','').'">'.number_format($r['ActualPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-creditmemo="'.number_format($r['CreditMemo'],2,'.','').'">'.number_format($r['CreditMemo'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-refund="'.number_format($r['Refund'],2,'.','').'">'.number_format($r['Refund'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-balance="'.number_format($balance,2,'.','').'">'.number_format($balance,2,'.',',').'</td>
		   <td style="font-size:9px !important;">'.$r['Remarks'].'</td>
		   <td style="font-size:9px !important;">'.$r['DMCMRefNo'].'</td>
		   <td style="font-size:9px !important;">'.$r['EntryID'].'</td>
		   <td style="font-size:9px !important;"><i class="fa '.(($r['InstallmentExcluded']>0)?'fa-check-square-o':'fa-square-o').'"></i></td>
		   <td style="font-size:9px !important;"><i class="fa '.(($r['SpecialFee']>0)?'fa-check-square-o':'fa-square-o').'"></i></td>
		   <td style="font-size:9px !important;">'.$r['SeqNo'].'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymente="'.number_format($r['5thPayment'],2,'.','').'">'.number_format($r['5thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentf="'.number_format($r['6thPayment'],2,'.','').'">'.number_format($r['6thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentg="'.number_format($r['7thPayment'],2,'.','').'">'.number_format($r['7thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenth="'.number_format($r['8thPayment'],2,'.','').'">'.number_format($r['8thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenti="'.number_format($r['9thPayment'],2,'.','').'">'.number_format($r['9thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentj="'.number_format($r['10thPayment'],2,'.','').'">'.number_format($r['10thPayment'],2,'.',',').'</td>
		</tr>'; 	
}
?>
