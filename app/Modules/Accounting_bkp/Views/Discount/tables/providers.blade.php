<table class="table table-striped table-bordered table-condensed table-hover font-xs " id="table-discount-provider">
	<thead>
		<tr>
			<th>CODE</th>
			<th>SCHOLARSHIP PROVIDER</th>
			<th>SHORT NAME</th>
			<th>SELECT</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr data-id="{{ $row->SchoProviderID }}">
			<td>{{ $row->ProvCode }}</td>
			<td>{{ $row->ProvName }}</td>
			<td>{{ $row->ProvShort }}</td>
			<td><a href="javascript:;" class="a-provider-sel btn btn-sm btn-primary">SELECT</a></td>
		</tr>
		@endforeach
	</tbody>
</table>