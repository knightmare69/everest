<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-table"></i> List of Discounts
        </div>
        <div class="tools">
        </div>
        <div class="actions">
           
        </div>
    </div>
    <div class="portlet-body">
    	<div class="row">
	       	<div class="col-md-12 table_wrapper">
                @include($views.'tables.table')
			</div>
		</div>
    </div>
</div>
