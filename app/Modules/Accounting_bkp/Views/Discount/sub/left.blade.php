<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users"></i> Discount Form
        </div>
        <div class="tools">
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
    	<div class="row">
	       	<div class="col-md-12 form_wrapper">
	       		@include($views.'forms.form')
			</div>
		</div>
    </div>
</div>

