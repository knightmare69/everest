<?php
	Route::group(['prefix' => 'setup','middleware'=>'auth'], function () {
		Route::group(['prefix' => 'department'], function () {
			Route::get('/','Department@index');
			Route::post('event','Department@event');
		});
		Route::group(['prefix' => 'group'], function () {
			Route::get('/','Group@index');
			Route::post('event','Group@event');
		});
		Route::group(['prefix' => 'position'], function () {
			Route::get('/','Position@index');
			Route::post('event','Position@event');
		});

		Route::group(['prefix' => 'city'], function () {
			Route::get('/','City@index');
			Route::post('event','City@event');
		});

		Route::group(['prefix' => 'medical'], function(){
			Route::get('/', 'Medical@index');
			Route::post('event','Medical@event');
			Route::get('print', 'Medical@print_data');
		});

		Route::group(['prefix' => 'testing'], function(){
			Route::get('/', 'Testing@index');
			Route::post('event','Testing@event');
			Route::get('print','Testing@print_data');
		});
        
        Route::group(['prefix' => 'academic-yearterm'], function () {
			Route::get('/','ayterm@index');
			Route::post('event','ayterm@event');
		});
         Route::group(['prefix' => 'discount'], function () {
			Route::get('/','Discount@index');
			Route::get('/search-student','Discount@SearchStudent');
			Route::post('/provider-modal','Discount@showProviderModal');
			Route::post('/save-discount','Discount@saveDiscount');
			Route::post('/show-discounts','Discount@showDiscounts');
			Route::post('/remove-discount/{id}','Discount@removeDiscount');
		});
	});
?>
