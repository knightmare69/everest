<?php 
namespace App\Modules\Webmail\Models;

use illuminate\Database\Eloquent\Model;

Class Config extends Model {

	protected $table='webmail_config';
	protected $primaryKey ='id';

	protected $fillable  = array(
			'user_id',
			'name',
			'email',
			'host',
			'server',
			'port',
			'encryption',
			'validate_cert',
			'remember_token',
			'created_at',
			'updated_at'
	);

	public function has($user_id) {
		return $this->where('user_id',$user_id)->limit(1)->count();
	}

	public function get($user_id) {
		return $this->where('user_id',$user_id)->first();	
	}

	public function add($data, $password) {
		if($this->create($data)) {
			$this->setSession($data, $password);
		}
	}

	public function _update($data, $password) {
		$this->where('user_id',$data['user_id'])->update($data);
		$this->setSession($data,$password);
		
	}

	public function setSession($data, $password) {
		session(['host' => $data['host']]);
        session(['port' => $data['port']]);
        session(['encryption' => $data['encryption']]);
        session(['validate_cert' => $data['validate_cert']]);
        session(['user_id' => $data['user_id']]);
        session(['username' => $data['email']]);
        session(['password' => base64_encode($password)]);
	}
}