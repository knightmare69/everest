<?php 
namespace App\Modules\Webmail\Models;

use illuminate\Database\Eloquent\Model;

Class Messages extends Model {

	protected $table='webmail_messages';
	protected $primaryKey ='id';

	protected $fillable  = array(
			'user_id',
			'msgno',
			'refid',
			'created_at',
			'updated_at'
	);

	public function isExist($user_id, $msgno) {
		if ($this->where(['msgno' => $msgno,'user_id' => $user_id])->limit(1)->count() > 0) {
			return true;
		}
		return false;
	}

	public function add($data) {
		return $this->create($data);
	}

	public function getAll($user_id) {
		return $this->where('user_id',$user_id)->get();
	}

	public function getMessage($user_id, $msgno) {
		return $this->where(['msgno' => $msgno,'user_id' => $user_id])->first();
	}

	public function has($user_id) {
		return $this->where('user_id',$user_id)->limit(1)->count();
	}
}