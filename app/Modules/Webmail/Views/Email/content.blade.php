<?php
$is_conn = (($conn)?1:0);
$page    = ((isset($page) && is_numeric($page))?$page:1);
?>
<table class="table table-striped table-advance table-hover">
<thead>
<tr>
	<th colspan="6">
		<input type="checkbox" class="mail-checkbox mail-group-checkbox">
		<div class="btn-group">
			<a class="btn btn-sm blue dropdown-toggle" href="javascript:;" data-toggle="dropdown">
			Option <i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu">
				<li>
					<a href="javascript:;">
					<i class="fa fa-pencil"></i> Mark as Read </a>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-ban"></i> Spam </a>
				</li>
				<li class="divider">
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-trash-o"></i> Delete </a>
				</li>
			</ul>
		</div>
	    <span class="pagination-control pull-right">
		<span class="pagination-info"><b class="initial"><?php echo (($page-1)*10)+1;?></b> to <b class="lastrec">1</b></span>
		<a class="btn btn-sm blue btn-prev"><i class="fa fa-angle-left"></i></a>
		<a class="btn btn-sm blue btn-next"><i class="fa fa-angle-right"></i></a>
		</span>
	</th>
</tr>
</thead>
<tbody>
<?php
$page = (($page-1)*10);
for($i=$page;$i<($page+10) && $i<count($cont);$i++){
  $msgid    = $cont[$i];
  $overview = imap_fetch_overview($conn,$msgid,0);
  if(count($overview)>0){ 
   $r       = $overview[0];  
?>
<tr class="<?php echo (($r->seen==0)?'unread':'');?>" data-messageid="<?php echo $msgid;?>" data-itemno="<?php echo ($i+1);?>" data-total="<?php echo count($cont);?>">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star hidden"></i>
	</td>
	<td class="view-message hidden-xs">
		 <?php echo imap_utf8($r->from);?>
	</td>
	<td class="view-message ">
		 <?php echo imap_utf8($r->subject);?>
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip hidden"></i>
	</td>
	<td class="view-message text-right">
		 <?php 
           $msgdate = $r->{'date'};
		   echo date('Y-m-d H:i A',strtotime($msgdate));
		 ?>
	</td>
</tr>
<?php
  }
}
?>
</tbody>
</table>