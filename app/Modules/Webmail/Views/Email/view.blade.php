<?php
 $detail = ((isset($detail))?$detail[0]:array());
?>
<div class="inbox-view-info">
	<div class="row">
		<div class="col-md-7">
			<span class="bold"><?php echo $detail->from;?></span>
			<span></span>
			to <span class="bold">
			me </span>
			on 
			<?php 
			  $msgdate = $detail->{'date'};
			  echo date('Y-m-d H:i A',strtotime($msgdate));
			?>
		</div>
		<div class="col-md-5 inbox-info-btn">
			<div class="btn-group">
				<button data-messageid="<?php echo $msgid;?>" class="btn blue btn-reply"><i class="fa fa-reply"></i> Reply </button>
				<button class="btn blue dropdown-toggle hidden" data-toggle="dropdown">
				<i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="javascript:;" data-messageid="<?php echo $msgid;?>" class="reply-btn">
						<i class="fa fa-reply"></i> Reply </a>
					</li>
					<li>
						<a href="javascript:;">
						<i class="fa fa-arrow-right"></i> Forward </a>
					</li>
					<li>
						<a href="javascript:;">
						<i class="fa fa-print"></i> Print </a>
					</li>
					<li class="divider">
					</li>
					<li>
						<a href="javascript:;">
						<i class="fa fa-ban"></i> Spam </a>
					</li>
					<li>
						<a href="javascript:;">
						<i class="fa fa-trash-o"></i> Delete </a>
					</li>
					<li>
					</div>
				</div>
			</div>
		</div>
		<div class="inbox-view">
		   <input type="hidden" id="reply" name="reply"/>
		   <?php echo $xcontent; ?>
		</div>
		<hr>
		<div class="inbox-attached hidden">
			<div class="margin-bottom-15">
				<span>
				3 attachments — </span>
				<a href="javascript:;">
				Download all attachments </a>
				<a href="javascript:;">
				View all images </a>
			</div>
			<div class="margin-bottom-25">
				<img src="">
				<div>
					<strong>image4.jpg</strong>
					<span>
					173K </span>
					<a href="javascript:;">
					View </a>
					<a href="javascript:;">
					Download </a>
				</div>
			</div>
		</div>
</div>