<div id="modal_linker" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Linking Your Email Account To K-12 System</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <form role="form" id="frmlinker" onsubmit="return false;">
					<div class="form-wizard">
						<div class="form-body">
							<ul class="nav nav-pills nav-justified steps" style="margin-bottom:2px; padding:1px;">
								<li class="active">
									<a href="#tab1" data-toggle="tab" class="step">
									<span class="number"> 1 </span><br/>
									<span class="desc"><i class="fa fa-check"></i> Provider</span>
									</a>
								</li>
								<li class="">
									<a href="#tab2" data-toggle="tab" class="step">
									<span class="number"> 2 </span><br/>
									<span class="desc"><i class="fa fa-check"></i> Instructions</span>
									</a>
								</li>
								<li class="">
									<a href="#tab3" data-toggle="tab" class="step">
									<span class="number"> 3 </span><br/>
									<span class="desc"><i class="fa fa-check"></i> Credentials</span>
									</a>
								</li>
								<li class="">
									<a href="#tab4" data-toggle="tab" class="step">
									<span class="number"> 4 </span><br/>
									<span class="desc"><i class="fa fa-check"></i> Validation</span>
									</a>
								</li>
							</ul>								
							<div id="bar" class="progress progress-striped" role="progressbar">
								<div class="progress-bar progress-bar-success register-progress" style="width:10%;"></div>
							</div>
							<div class="tab-content" style="max-height:320px;overflow-y:auto;overflow-x:hidden;">
								<div class="alert alert-danger display-none">
									<button class="close" data-dismiss="alert"></button>
									You have some form errors. Please check below.
								</div>
								<div class="alert alert-success display-none">
									<button class="close" data-dismiss="alert"></button>
									Your form validation is successful!
								</div>
								<div class="tab-pane active" id="tab1">		
                                  <p>Please select one of the email provider to connect/link your account:</p>								
								  <div class="form-group">
									<label class="">Provider:</label>
									<select class="form-control" id="provider" name="provider">
									   <option value="1">Google</option>
									</select>
								  </div>
								</div>
								<div class="tab-pane" id="tab2">
								   <h3 class="">Instructions:</h3>
								   <div class="col-sm-12 dvinstruct"> 
								     <div id="kb_article_text">
										<h3>Enabling IMAP for Google@UH Gmail</h3>
										<p>To setup IMAP for Google@UH Gmail, you must first enable IMAP by:</p>
										<ul>
											<li>Log into <a href="http://gmail.hawaii.edu">Google@UH Gmail</a>.</li>
											<li>Click on the gear icon&nbsp;<img src="/help/chasek/images/Image/gmail gear icon(1).png" alt="" width="81" height="35"> in the upper right, then select&nbsp;<strong>Settings</strong>.</li>
											<li>Click on the <strong>Forwarding and POP/IMAP</strong> tab.</li>
											<li>Under IMAP Access, select the radio button for <strong>Enable IMAP</strong> and click on <strong>Save Changes</strong>. You should be back viewing your Inbox.</li>
											<li>Click on <strong>Settings</strong> again.</li>
											<li>Click on the <strong>Labels</strong> tab.</li>
											<li>Find the <strong>All Mail</strong> label and make sure <strong>Show in IMAP</strong> is unchecked.</li>
											<li>You may now open and <a href="http://hawaii.edu/askus/1077">configure</a>&nbsp;your email client.</li>
										</ul>
										<p><a href="http://www.hawaii.edu/askus/1077">Click here</a>&nbsp;for additional IMAP/POP and SMTP settings.</p>
									</div>
								   </div>
								</div>
								<div class="tab-pane" id="tab3">
								  <div class="form-group">
									<label class="">Username/Email:</label>
									<input type="text" class="form-control" id="email" name="email">
								  </div>
								  <div class="form-group">
									<label class="">Password:</label>
									<input type="password" class="form-control" id="pass" name="pass">
								  </div>
								</div>
								<div class="tab-pane" id="tab4">
								   <h2 class="text-center validation-msg">Successfully Connected!!</h2>
								</div>
							</div>						
						</div>
                    </div>			
				  </form>
				</div>
				<div class="modal-footer">
                   <button class="btn btn-sm blue button-previous"><i class="fa fa-arrow-circle-o-left"></i> Previous</button>
                   <button class="btn btn-sm blue button-next">Next <i class="fa fa-arrow-circle-o-right"></i></button>
				   <button class="btn btn-sm green button-submit btn-alink"><i class="fa fa-check"></i> Link</button>
				</div>
			</div>
		</div>
	</div>	
</div>