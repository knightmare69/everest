<form class="inbox-compose form-horizontal" id="compose" action="#" method="POST" enctype="multipart/form-data" onsubmit="return false;">
	<div class="inbox-compose-btn">
		<button class="btn blue btn-send"><i class="fa fa-check"></i>Send</button>
		<button class="btn yellow btn-save"><i class="fa fa-save"></i>Save</button>
		<button class="btn btn-cancel inbox-discard-btn">Discard</button>
		<button class="btn btn-draft">Draft</button>
		<span class="btn btn-xs green fileinput-btn">
		 <label for="fileinput-file"><i class="fa fa-paperclip"></i> Attach</label>
		 <input id="fileinput-file" class="btn btn-sm green hidden" type="file" name="files[]" multiple/>
		</span>
		<span class="pull-right">
		  <button class=" btn btn-warning btn-link"><i class="fa fa-link"></i></button>
		  <button class=" btn btn-warning btn-unlink hidden"><i class="fa fa-unlink"></i></button>
		</span>
	</div>
	<input type="hidden" id="xid" class="hidden"/>
	<div class="inbox-form-group mail-to">
	   <label class="control-label">To:</label>
		<div class="controls">
		    <select id="grpid" name="grpid" class="form-control"><?php echo $group;?></select>
			<input type="text" id="multi-append" name="to" class="form-control select2" multiple="multiple" tabindex="-1" aria-hidden="true"/>                   
		</div>
	</div>
	<div class="inbox-form-group input-cc display-hide">
		<a href="javascript:;" class="close">
		</a>
		<label class="control-label">Cc:</label>
		<div class="controls controls-cc">
			<input type="text" name="cc" class="form-control">
		</div>
	</div>
	<div class="inbox-form-group input-bcc display-hide">
		<a href="javascript:;" class="close">
		</a>
		<label class="control-label">Bcc:</label>
		<div class="controls controls-bcc">
			<input type="text" name="bcc" class="form-control">
		</div>
	</div>
	<div class="inbox-form-group">
		<label class="control-label">Subject:</label>
		<div class="controls">
			<input type="text" class="form-control" name="subject">
		</div>
	</div>
	<div class="inbox-form-group">
		<textarea class="summernote form-control" id="message" name="message" rows="12"></textarea>
	</div>
	<div class="inbox-compose-attachment">
		<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
		<!-- The table listing the files available for upload/download -->
		<table id="tblattachment" role="presentation" class="table table-condensed table-bordered" style="margin-top:5px;">
		  <tbody class="files">
		  </tbody>
		</table>
	</div>
</form>	