<?php 
 $is_conn = (($conn)?1:0);
?>
<div class="row inbox">
	<div class="col-sm-4 col-md-3 email-menu">
		<ul class="inbox-nav margin-bottom-10 xmenu">
			<li class="compose-btn btn-compose">
				<a href="javascript:;" data-title="Compose" class="btn yellow">
				<i class="fa fa-edit"></i> Compose </a>
			</li>
		</ul>	
		<div class="alert alert-warning xalert <?php echo (($is_conn==0)?'':'hidden');?> ">
		   <p>Your account is not yet connected to your email. <button class=" btn btn-sm btn-danger btn-link"><i class="fa fa-link"></i> Connect Your Email</button></p>
		</div>
		<ul class="inbox-nav margin-bottom-10 xmenu <?php echo (($is_conn==0)?'hidden':'');?>">
			<li class="compose-btn" style="background-color:#4b8df8 !important;">
				<button class="btn btn-sm yellow btn-unlink pull-right <?php echo (($is_conn==0)?'hidden':'');?>" style="margin-top:5px;margin-right:5px;"><i class="fa fa-unlink"></i></button>
				<a href="javascript:;" data-title="Online Folder" class="btn yellow btn-toggle">
				<i class="fa fa-folder"></i> Online Folder
				</a>
			</li>
			<?php
			 $i = 1;
			 foreach($folder as $k=>$v){
				echo '<li class="'.strtolower($v['name']).' '.(($i==1)?'active':'').'" data-link="'.$v['link'].'" data-table="">
						<a href="javascript:;" class="btn" data-title="'.$v['name'].'">'.$v['name'].'</a><b></b>
					  </li>'; 
				$i++;	  
			 }
			?>
		</ul>
		<ul class="inbox-nav margin-bottom-10 xmenu">
			<li class="compose-btn" style="background-color:#4b8df8 !important;">
				<a href="javascript:;" data-title="Local Folder" class="btn yellow btn-toggle">
				<i class="fa fa-folder"></i> Local Folder </a>
			</li>
			<li class="draft" data-link="" data-table="draft">
				<a class="btn" href="javascript:;" data-title="Draft">Draft</a><b></b>
			</li>
			<li class="sent" data-link="" data-table="sent">
				<a class="btn" href="javascript:;" data-title="Sent">Sent</a><b></b>
			</li>
		</ul>	
	</div>
	<div class="col-sm-8 col-md-9 email-content">
       <span class="xloader hidden"><i class="fa fa-refresh fa-spin"></i> Loading..</span>
	   <span class="xmain">
	   @include($view.'content')
	   </span>
	</div>
</div>
@include($view.'linker')