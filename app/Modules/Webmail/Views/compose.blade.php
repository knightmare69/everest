<?php $data = isset($data) ? $data : array(); $to = ''; ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <h4><b>Information</b></h4>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">From <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control" name="from" id="from" placeholder="From" value="{{ $MyEmail }}">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">To <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control" name="to" id="to" placeholder="To" value="">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Subject <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" value="">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Message <small><i class="text-danger">*</i></small></label>
                    <textarea class="form-control" rows="20" name="mymessage" id="mymessage" placeholder="Enter message here"></textarea>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Cancel</button>
        <button class="btn blue btn_action btn-send" type="button"><i class="fa fa-check"></i> Send</button>
    </div>
</form>
<!-- END FORM-->