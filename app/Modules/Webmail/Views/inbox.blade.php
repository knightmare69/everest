<?php $inbox = isset($inbox) ? $inbox : []; ?>
<table class="table table-striped table-bordered table-hover" id="table-inbox">
	<thead>
		<tr role="row" class="heading hide">
			<th width="3%" class="hide">
				S#
			</th>
			<th width="10%">
				Subject
			</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 0; $message = ''; ?>
		@foreach($inbox as $row)
		<tr role="row" class="filter">
			<td class="hide">{{ count($inbox) - $i }}</td>
			<td><a href="javascript:;" data-msgno="{{ $row['msgno'] }}" class="btn btn-small btn-preview {{ $row['read_status'] }}"><small>{{ $row['subject'] }}</small></a></td>
		</tr>
		<?php $i++;?>
		@endforeach
	</tbody>
</table>
