<style>
.read {
    color: #ccc;
}
.unread {
    color: #000;
    font-weight: bold;
}
</style>
<div class="row">
    <div class="col-md-4">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> Inbox
                </div>
                <div class="tools">
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn btn-default btn-circle btn-refresh">
                        <i class="fa fa-share"></i> Refresh
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-default btn-circle btn-compose" data-url="{{ url('webmail/compose') }}">
                        <i class="fa fa-share"></i> Compose
                        </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
            	<div class="row">
        	       	<div class="col-md-12 table_wrapper">
        		      @include($views.'inbox')
        			</div>
        		</div>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> Inbox
                </div>
                <div class="tools">
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn btn-default btn-circle btn-replyto hide" data-url="{{ url('webmail/reply') }}">
                        <i class="fa fa-share"></i> Reply
                        </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12 view_wrapper">
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

