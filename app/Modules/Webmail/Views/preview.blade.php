<?php $message = isset($message) ? $message : ''; ?>

<div class="message_wrapper">
{!! $message !!}
</div>
<div class="reply_wrapper hide">
	@include($views.'reply_form')
</div>