<?php $data = isset($login) ? $login : array();?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <h4><b>User Account</b></h4>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Username <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="username" value="{{ getObjectValue($login,'email') }}">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Password <small><i class="text-danger">*</i></small></label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Mail Server <small><i class="text-danger">*</i></small></label>
                    <select class="form-control" required name="server" id="server">
                        <option value="">-- SELECT --</option>
                        <option {{ getObjectValue($login,'$login->server') == 'gmail' ? 'selected' : '' }} value="gmail">Gmail</option>
                        <option {{ getObjectValue($login,'$login->server') == 'yahoo' ? 'selected' : '' }} value="yahoo">Yahoo</option>
                        <option {{ getObjectValue($login,'$login->server') == 'hotmail' ? 'selected' : '' }} value="hotmail">Hotmail </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn blue btn_action btn_login" type="button"><i class="fa fa-check"></i> Login</button>
    </div>
</form>
<!-- END FORM-->