<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list"></i> Reply
        </div>
        <div class="tools">
        </div>
        <div class="actions">
            
        </div>
    </div>
    <div class="portlet-body">
    	<div class="row">
	       	<div class="col-md-12 table_wrapper">
		      @include($views.'reply_form')
			</div>
		</div>
    </div>
</div>

