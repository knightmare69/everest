<?php namespace App\Modules\Webmail\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Webmail\Services\Services;
use App\Modules\Webmail\Models\Config as ConfigModel;
use Permission;
use Response;
use Request;
use Sys;
use DB;
use Session;

class Webmail extends Controller{

	use Services;

	public $global_user_id = 1;

	private $media =
		[
			'Title'=> 'Webmail',
			'Description'=> 'Welcome To Webmail!',
			// 'BClass' => 'page-container-bg-solid',
			'js'		=> ['Webmail/index','datatable'],
			'init'		=> ['Webmail.init()','FN.datePicker()'],
			'plugin_js'	=> [
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'bootbox/bootbox.min',
				'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
				'fancybox/source/jquery.fancybox.pack',
				'datatables/media/js/jquery.dataTables.min',
	            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
	            'datatables/extensions/Scroller/js/dataTables.scroller.min',
	            'datatables/plugins/bootstrap/dataTables.bootstrap',
				'fancybox/source/jquery.fancybox',
				'bootstrap-wysihtml5/bootstrap-wysihtml5',
				'inbox/inbox',
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
                'smartnotification/SmartNotification',
				'fancybox/source/jquery.fancybox',
				'bootstrap-wysihtml5/bootstrap-wysihtml5',
				'inbox/inbox',
			]
		];

	private $url = [ 'page' => 'webmail/' ];
	private $views = 'Webmail.Views.';

	function __construct()
	{
		$this->initializer();
		$this->initMail();
	}


 	public function index()
 	{	
 		// Session::flush();
 		if (!$this->config->has($this->global_user_id)) {
 			return $this->login();
 		}

 		$this->media['js'] = ['Webmail/index','datatable'];
 		$this->media['init'] = ['Webmail.init()','FN.datePicker()'];

 		if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->initData()),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}	

 	public function login()
 	{	
 		$this->media['js'] = ['Webmail/login'];
 		$this->media['init'] = ['Login.init()'];

        return view('layout',array('content'=>view($this->views.'login',$this->initLogin()),'url'=>$this->url,'media'=>$this->media));
 	}	

 	public function reply($msgno) {
 	
		$data = array_merge(
			['views' => $this->views],
			['MyEmail' => $this->email],
			$this->getMessageContent(base64_decode($msgno),false)
		);

        return view($this->views.'compose',$data);
 	}

 	public function compose() {
		$data = array_merge(
			['views' => $this->views],
			['MyEmail' => $this->getMyEmail()]
		);

       	return view($this->views.'compose',$data);
 	}

 	public function previewMessage($msgno) {
        $data = array_merge(
            ['views' => $this->views],
            ['MyEmail' => $this->email],
            $this->getMessageContent(base64_decode($msgno),true)
        );
        return view($this->views.'preview',$data);
    }


 	public function getMailBox() {
 		return view($this->views.'inbox',['inbox' => $this->getInbox()]);
 	}

 	public function initLogin()
 	{
 		return array(
 			'views' => $this->views,
 			'login' => $this->config->get($this->global_user_id)
 		);
 	}

 	public function initData()
 	{
 		return array(
 			'views' => $this->views
 		);
 	}

 	private function initializer()
 	{
	  //ini_set('max_execution_time', 512);
		ini_set('memory_limit','512M');
 		$this->permission = new Permission('dashboard');
 		$this->config = new ConfigModel;
		$this->global_user_id = getUserID();
 	}
}