<?php 
namespace App\Modules\Webmail\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Webmail\Services\Exmail as email;
use App\Modules\Webmail\Services\Emailer as mailer;
//use App\Modules\Webmail\Models\Config as ConfigModel;
use Permission;
use Response;
use Request;
use Sys;
use DB;
use Session;
use Mail;

class EXmail extends Controller{
    private $media = ['Title'         => 'Email',
					  'Description'   => 'Email',
					  'js'            => ['Webmail/email'],
					  'init'          => [],
					  'plugin_js'     => ['bootbox/bootbox.min',
										  'bootstrap-select/bootstrap-select.min',
										  'datatables/media/js/jquery.dataTables.min',
										  'datatables/extensions/TableTools/js/dataTables.tableTools.min',
										  'datatables/extensions/Scroller/js/dataTables.scroller.min',
										  'datatables/plugins/bootstrap/dataTables.bootstrap',
										  'jquery-validation/js/jquery.validate.min',
										  'jquery-validation/js/additional-methods.min',
										  'bootstrap-wizard/jquery.bootstrap.wizard.min',
										  'SmartNotification/SmartNotification.min',
						                  'select2/select2',
										  'fancybox/source/jquery.fancybox',
										  'summernote/summernote',
										],
						'plugin_css'    => ['bootstrap-select/bootstrap-select.min',
						                    'SmartNotification/SmartNotification',
						                    'select2/select2',
											'fancybox/source/jquery.fancybox',
										    'summernote/summernote',
											'inbox/inbox'
											],
					];
    
    private $url =  [
        'page'  => 'email/',
        'form' => 'form',
    ];
    
	public $r_view = 'Webmail.Views.Email.';
	
    public function index(){
		$this->initializer();
		return view('layout',array('content'=>view($this->r_view.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
	}
	
	public function trial(){
		$dir   = "\\\\192.168.1.254";
        $files = scandir($dir);
		echo '<pre>';
		print_r($files);
		die();
	}
	
	public function txn(){
		ini_set('max_execution_time', 60);
		$response = 'No Event Selected';
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'contacts': 
				   $items    = $this->email->getContact();
				   $response = Response::json(array('results'=>$items));					
				break;
				case 'compose':
				   $content  = view($this->r_view.'compose',$this->init());	
				   $response = Response::json(array('success'=>true,'content'=>(string)$content));	
				break;
				case 'content';
				   $key      = Request::get('key');
				   $page     = Request::get('page');
				   $conn     = $this->email->AccountLogin($key);
				   $cont     = $this->email->ContentList($key);
				   $content  = view($this->r_view.'content',array('conn'=>$conn['conn'],'host'=>$conn['host'],'cont'=>$cont,'page'=>$page));	
				 //imap_close($conn['conn']);
				   $response = Response::json(array('success'=>true,'content'=>(string)$content));		
                break;
				case 'tbrow';
				   $response = Response::json(array('success'=>true,'content'=>''));		
                break;
				case 'view':
				   $key      = Request::get('key');
				   $mid      = Request::get('mid');
				   $page     = Request::get('page');
				   $conn     = $this->email->AccountLogin($key);
				   $header   = $this->email->getReplyAddress($conn['conn'],$mid);
				   $format   = imap_fetchstructure($conn['conn'], $mid);
                   $details  = imap_fetch_overview($conn['conn'],$mid,0);
				 //$message  = $this->email->getBody($conn['conn'],$mid);
				   $message  = $this->email->fetchBody($conn['conn'],$mid);
				   $content  = (string)view($this->r_view.'view',array('conn'=>$conn['conn'],'host'=>$conn['host'],'msgid'=>$mid,'header'=>$header,'detail'=>$details,'xcontent'=>$message));	
				 //imap_close($conn['conn']);
				   $response = Response::json(array('success'=>true,'content'=>$content,'msgid'=>$mid,'header'=>$header,'structure'=>$format));	
				break;
                case 'delmsg':
                   $response = Response::json(array('success'=>true));		
                break;				
				case 'save':
				   $from      = getUserID();
				   $xid       = Request::get('xid');
				   $recipient = Request::get('to');
				   $groupid   = Request::get('grpid');
				   $subject   = Request::get('subject');
				   $message   = Request::get('message');
				   if($recipient=='' && $groupid>-1){
					 $recipient = $groupid;
				   }
				   if(!$xid){
				    $exec      = DB::table('ESv2_Email')->insertGetId(array('UserID'=>$from,'Recipient'=>$recipient,'ESubject'=>$subject,'EContent'=>$message,'DateCreated'=>date('Y-m-d H:i')));
				   }else{
					$exec      = DB::table('ESv2_Email')->where('EntryID',$xid)->update(array('UserID'=>$from,'Recipient'=>$recipient,'ESubject'=>$subject,'EContent'=>$message,'DateModified'=>date('Y-m-d H:i')));	
					$exec      = $xid;
				   }
				   $response  = Response::json(array('success'=>true,'content'=>'Email Successfully Saved','xid'=>$exec));			
				break;
				case 'attachment':
				   $xid         = Request::get('xid');
				   $pathname    = str_replace('index.php','assets/upload/email/',$_SERVER['SCRIPT_FILENAME']).str_pad($xid,8,'0',STR_PAD_LEFT);
				   $linkname    = url().'assets/upload/email/'.str_pad($xid,8,'0',STR_PAD_LEFT);
				   if(!file_exists($pathname)){mkdir($pathname);}
				   $fileName    = $_FILES['file']['name'];
                   $fileType    = $_FILES['file']['type'];
                   $fileError   = $_FILES['file']['error'];
                   $fileContent = file_get_contents($_FILES['file']['tmp_name']);
				   file_put_contents($pathname.'/'.$fileName,$fileContent);
				   $exec     = DB::table('ESv2_Email_Attachment')->insert(array('UserID'=>getUserID(),'EmailID'=>$xid,'Filename'=>$fileName,'Extname'=>$pathname.'/'.$fileName,'Data'=>$linkname.'/'.$fileName));
				   $response = Response::json(array('success'=>true,'content'=>'Attachment Successfully Uploaded'));			
				break;
				case 'delattach':
				    $aid      = Request::get('aid');
					$exec     = DB::table('ESv2_Email_Attachment')->where('EntryID',$aid)->delete();
					$response = Response::json(array('success'=>true,'content'=>'Attachment Successfully Deleted'));			
				break;
				case 'send': 
				   ini_set('MAX_EXECUTION_TIME', -1);
				   $arrto          = array();
				   $xid            = Request::get('xid');
				   $data           = Request::all();
				   if($data['grpid']>-1){
					 $arrto   = $this->email->getContactByGroup($data['grpid']);   
				   }
				   if(count($arrto)>0){
					  foreach($arrto as $k=>$a){
					     $data['to']     = $a;  
				         $data['attach'] = $this->email->getAttactment($xid);
				         $exec           = $this->mailer->sendEmail($data);
					  } 
				   }else{
					$data['attach'] = $this->email->getAttactment($xid);
				    $exec           = $this->mailer->sendEmail($data);
				   }
				   
				   if($exec){
					 $exec     = DB::statement("UPDATE ESv2_Email SET IsSent=1 WHERE EntryID='".$xid."'");  
				     $response = Response::json(array('success'=>true,'content'=>'Message Successfully Sent'));					
				   }else{
					 $response = Response::json(array('success'=>false,'content'=>'Message Failed'));					
				   }
				break;
				case 'acctlink':
				   $data       = Request::all();
				   $exec       = $this->email->AccountLink($data);
				   if($exec){
				     $response = Response::json(array('success'=>true,'content'=>'Successfully Linking Email Account!'));					
				   }else{
					 $response = Response::json(array('success'=>false,'content'=>'Failed!, Please make sure that you follow the instructions and check your email,and password.'));					
				   }
				break;
				case 'unlink':
				   $exec       = DB::table('ESv2_Email_Link')->where('UserID',getUserID())->delete();
				   if($exec){
				     $response = Response::json(array('success'=>true,'content'=>'Successfully Unlinking Email Account!'));					
				   }else{
					 $response = Response::json(array('success'=>false,'content'=>'Failed to unlink!'));					
				   }
				break;
			}
            return $response;
        }
        return $response;	
    }		
	
	private function init(){
		$conn = $this->email->AccountLogin();
		$list = ((is_array($conn))?$this->email->FolderList($conn['conn'],$conn['host']):array());
	    $cont =  array();
	  //$overview = imap_fetchbody($conn['conn'],3918,'');
		$data = array('view'   => $this->r_view
		             ,'conn'   => ((is_array($conn))?$conn['conn']:false)
		             ,'host'   => ((is_array($conn))?$conn['host']:'')
					 ,'cont'   => $cont
		             ,'folder' => $list
					 ,'group'  => $this->email->getGroups()
					 );
	  //echo '<pre>';
	  //die(imap_utf8($overview));
		return $data;
	}
	
	private function initializer(){
	   ini_set('max_execution_time', 300);
	   $this->email = new email;	
	   $this->mailer = new mailer;	
	}
}
?>