<?php
	Route::group(['prefix' => '/webmail','middleware'=>'auth'], function () {
	    Route::get('/','Webmail@index');
	    Route::get('/getInbox','Webmail@getMailBox');
	    Route::get('/reply/{msgno}','Webmail@reply');
	    Route::post('/reply','Webmail@sendMessage');
	    Route::get('/compose','Webmail@compose');
	    Route::post('/send','Webmail@sendMessage');
	    Route::post('/login','Webmail@loggingIn');
	    Route::get('/preview/{msgno}','Webmail@previewMessage');
	});
	Route::group(['prefix' => '/email','middleware'=>'auth'], function () {
	    Route::get('/','EXmail@index');
	    Route::get('/txn','EXmail@txn');
	    Route::post('/txn','EXmail@txn');
	});
?>	