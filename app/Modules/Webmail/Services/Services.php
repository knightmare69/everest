<?php
namespace App\Modules\Webmail\Services;

use DB;
use Request;
use Sys;
use Webklex\IMAP\Client;
use Mail;
use Session;

Trait Services {

	use Messages;

    public function loggingIn() {

        $response = ['error' => true,'message' => 'Could not connect to mail server.'];

        $credentials  = $this->getMailCredetials(Request::get('server'));

        if ($this->validateLogin()['error']) {
            return $this->validateLogin();
        }

        if (empty($credentials)) {
            return ['error' => true, 'message' => 'Please select mail server.'];
        }

        $oClient = new Client([
            'host'          => $credentials['host'],
            'port'          => $credentials['port'],
            'encryption'    => $credentials['encryption'],
            'validate_cert' => $credentials['validate_cert'],
            'username'      => Request::get('username'),
            'password'      => Request::get('password'),
        ]);
        try {
            //Connect to the IMAP Server
            if($oClient->connect()) {
                if (!$this->config->has($this->global_user_id)) {
                    $this->config->add([
                        'host'          => $credentials['host'],
                        'port'          => $credentials['port'],
                        'encryption'    => $credentials['encryption'],
                        'validate_cert' => $credentials['validate_cert'],
                        'email'         => Request::get('username'),
                        'user_id'      =>  $this->global_user_id,
                        'server'        => Request::get('server')
                    ],Request::get('password'));
                } else {
                    $this->config->_update([
                        'host'          => $credentials['host'],
                        'port'          => $credentials['port'],
                        'encryption'    => $credentials['encryption'],
                        'validate_cert' => $credentials['validate_cert'],
                        'email'         => Request::get('username'),
                        'user_id'      =>  $this->global_user_id,
                        'server'        => Request::get('server')
                    ],Request::get('password'));
                }

                //initialize account

                $this->initMail();

                return ['error' => false,' message' => 'Successfully Login!'];
            }
        } catch (Exception  $e) {
            $response = ['error' => 'false', 'message' => 'Invalid Credentials!'];
        }
        return $response;
    }

    private function validateLogin() {
        if (empty(Request::get('username')) || empty(Request::get('password'))) {
            return ['error' => true, 'message' => 'Please complete login credentials!'];
        }

        return ['error' => false];
    }

    private function getMailCredetials($server = 'gmail') {
        $data = [
            'gmail' => [
                'host' => 'imap.gmail.com',
                'port' => 993,
                'encryption' => 'ssl',
                'validate_cert' => false
            ],
            'yahoo' => [
                'host' => ' imap.mail.yahoo.com',
                'port' => 993,
                'encryption' => 'ssl',
                'validate_cert' => false
            ],
            'hotmail' => [
                'host' => 'pop3.live.com',
                'port' => 995,
                'encryption' => 'ssl',
                'validate_cert' => false
            ],
        ];

        return isset($data[$server]) ? $data[$server] : '';
    }

    public function getInbox() {
        
        $this->connect();

        //Get all Mailboxes

        $aMailboxes = $this->mail->getFolders();

        $messages_old = [];
        $messages_new = [];

        $subjects = [];
        $subject = '';

        $criteria = $this->messages->has($this->user_id) ? 'ALL' : 'ALL';

        foreach($aMailboxes[0]->getMessages($criteria) as $msgno => $msg) {

            $read_status = 'read';
            $subject = trim(preg_replace("/Re\:|re\:|RE\:|Fwd\:|fwd\:|FWD\:/i", '', $msg->subject)); 
            $body = substr(trim(str_replace(["\n","\r"],'',$msg->getTextBody())),0,100) ;

            $msgno = explode('_x_',$msgno)[1];
            $refid = explode('_x_',$msgno)[0];

            if (!$this->messages->isExist($this->user_id, $msgno)) {
                $read_status = 'unread';
                /*$messages_old[$subject] = [
                    'osubject' => $msg->subject,
                    'msgno' => base64_encode($msgno),
                    'subject' => $msg->subject,
                    'message' => $body,
                    'read_status' => $read_status
                ];*/
            }  
            
            $messages_new[$subject] = [
                'osubject' => $msg->subject,
                'msgno' => base64_encode($msgno),
                'subject' => $msg->subject,
                'message' => $body,
                'read_status' => $read_status
            ];
        }
       
        return $messages_new;
    }

    public function sendMessage() {
		ini_set('max_execution_time', 600);
        $post['message'] = Request::get('message');
        Mail::send(['html' => $this->views.'message_content','message' => $post['message']],$post, function ($message) use ($post) {
            $message->from(Request::get('from'),Request::get('subject'));
            $message->sender(Request::get('from'));
            $message->to(Request::get('to'));
            $message->replyTo(Request::get('from'));
            $message->subject(Request::get('subject'));
        });
        return ['error' => false];
    }	
}	
?>