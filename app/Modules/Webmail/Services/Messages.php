<?php
namespace App\Modules\Webmail\Services;

use DB;
use Request;
use Sys;
use Webklex\IMAP\Client;
use App\Modules\Webmail\Models\Messages as MessagesModel;
use Webklex\IMAP\Message;
use Webklex\IMAP\Exceptions\GetMessagesFailedException;

Trait Messages {

	public $mail;

    protected $email;

    protected $host;

    protected $port;

    protected $encryption;

    protected $validate_cert;

    protected $password;

    protected $user_id;

    public function initMail() {

        $this->email = session('username');

        $this->password = base64_decode(session('password'));

        $this->user_id = session('user_id');

        $this->host = session('host');

        $this->port = session('port');

        $this->encryption = session('encryption');

        $this->validate_cert = session('validate_cert');

        $this->messages = new MessagesModel;
    }

	public function connect() {
        $this->initMail();
		$oClient = new Client([
            'host'          => $this->host,
            'port'          => $this->port,
            'encryption'    => $this->encryption,
            'validate_cert' => $this->validate_cert,
            'username'      => $this->email,
            'password'      => $this->password,
        ]);

        //Connect to the IMAP Server
        $oClient->connect();

        return $this->mail = $oClient;
	}


  
    public function getMessageContent($msgno, $isHtml = true) {
        $this->connect();
        
        $message = $this->getSpecificMessage($msgno);

        if (!$this->messages->isExist($this->user_id, $msgno)) {
            $this->messages->add([
                'user_id' => 1,
                'msgno' => $msgno,
            ]);
        }
         
        if ($isHtml) {
            return [
                'message' => $message->getHTMLBody(),
                'plain_message' => $message->getTextBody(),
                'reply_to' => $message->reply_to,
                'subject' => $message->subject,
            ];  
        }
        return [
            'message' => $message->getTextBody(),
            'reply_to' => $message->reply_to,
            'subject' => $message->subject,
        ]; 
    }

    /**
     * Get messages from folder.
     *
     * @param Folder $folder
     * @param string $criteria
     * @param integer $fetch_options
     *
     * @return array
     * @throws GetMessagesFailedException
     */
    public function getSpecificMessage($msgno, $fetch_options = null)
    {
        $this->mail->checkConnection();

        try {
            $message = new Message($msgno, 1, $this->mail, $fetch_options);

            return $message;

        } catch (\Exception $e) {
            $message = $e->getMessage();

            throw new GetMessagesFailedException($message);
        }
    }

    public function getMyEmail() {
        return $this->email;
    }

    public function getOldMessages() {
        return $this->messages->getAll($this->user_id);
    }

	
}	
?>