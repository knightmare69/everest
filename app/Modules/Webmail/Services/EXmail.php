<?php
namespace App\Modules\Webmail\Services;

use DB;
use Request;
use Sys;
use Mail;
use Session;

class Exmail {
	public function AccountLink($data){
		$hostname  = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
		$hostport  = '446';
		$hostemail = 'smtp.googlemail.com';
		$emailport = '465';
		$username  = $data['email'];
		$password  = $data['pass'];
		$conn      = false; 
		try{
		 $conn = @imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
		 $exec = DB::statement("INSERT INTO ESv2_Email_Link(UserID,Email,PassKey,AcctTypeID,Host,Port,EHost,EPort)
		                        SELECT '".getUserID()."' as UserID
								      ,'".$data['email']."' as Email
								      ,'".base64_encode($data['pass'])."' as PassKey
								      ,'".$data['provider']."' as TypeID
								      ,'".$hostname."' as Host
									  ,'".$hostport."' as Port 
								      ,'".$hostemail."' as EHost
									  ,'".$emailport."' as EPort 
							    WHERE '".getUserID()."' NOT IN (SELECT UserID FROM ESv2_Email_Link)
								UPDATE ESv2_Email_Link SET Email      = '".$data['email']."'
                                                          ,PassKey    = '".base64_encode($data['pass'])."' 
								                          ,AcctTypeID = '".$data['provider']."'
								                          ,Host       = '".$hostname."'
									                      ,Port       = '".$hostport."'  
								                          ,EHost      = '".$hostemail."' 
									                      ,EPort      = '".$emailport."'
													 WHERE UserID='".getUserID()."'");
		}catch(Exception $e){
		 $conn = false;	
		} 
		return $conn;
	}
	
	public function AccountLogin($host=''){
		$conn     = false; 
		$exec     = DB::select("SELECT * FROM ESv2_Email_Link WHERE UserID='".getUserID()."'");
		foreach($exec as $r){
			$hostname = (($host=='')?'{imap.gmail.com:993/imap/ssl/novalidate-cert}':$host);
			$username = $r->Email;
			$password = $r->PassKey;
			$password = base64_decode($password);
			try{
			   $conn  = @imap_open($hostname,$username,$password);
			   if(imap_errors()){
				  return array('conn'=>false,'host'=>false); 
			   }else{
			      return array('conn'=>$conn,'host'=>$hostname);
			   }
			}catch(Exception $e){
			   $conn  = false;	
			} 
		}
		return $conn;
	}
	
	public function FolderList($conn,$host){
		$arr_list  = array(0=>'Inbox',1=>'Drafts',2=>'Sent',3=>'Trash');
		$list      = array();
		$mailboxes = @imap_list($conn, $host, '*');
		if($mailboxes){
			foreach($mailboxes as $k=>$v){
				foreach($arr_list as $i=>$a){
					 if(strpos($v,$a)!==false || strpos($v,strtoupper($a))!==false){
						$list[] = array('link'=>$v,'name'=>$a);
						unset($arr_list[$i]);
					 }else{
						continue; 
					 }	
				}				
			}
			
		}
		return $list;
	}
	
	public function ContentList($dir){
		$conn     = $this->AccountLogin($dir);
        if(is_array($conn)){
		  try{	
		    $reconn = imap_reopen($conn['conn'],$dir) or die('Cannot connect to Gmail: ' . imap_last_error());
		    $emails = imap_search($conn['conn'],'ALL');	
		    rsort($emails);
		    return $emails;
		  }catch(Exception $e){
			return array();  
		  }	
		} 		
		return array();
	}
	
	public function getReplyAddress($conn,$mid){
	    $header   = imap_header($conn,$mid);
		$return   = array('reply'=>array(),'cc'=>array(),'bcc'=>array());
		if(property_exists($header,'reply_to')){
			foreach($header->from as $r){
			   $address           = $r->mailbox.'@'.$r->host;
			   $return['reply'][] = array('name',$r->personal,'address'=>$address);	
			}
		}
		if(property_exists($header,'reply_to')){
			foreach($header->reply_to as $r){
			   $address           = $r->mailbox.'@'.$r->host;
			   $return['reply'][] = array('name',$r->personal,'address'=>$address);	
			}
		}
		if(property_exists($header,'cc')){
			foreach($header->reply_to as $r){
			   $address        = $r->mailbox.'@'.$r->host;
			   $return['cc'][] = array('name',$r->personal,'address'=>$address);	
			}
		}
		if(property_exists($header,'bcc')){
			foreach($header->reply_to as $r){
			   $address         = $r->mailbox.'@'.$r->host;
			   $return['bcc'][] = array('name',$r->personal,'address'=>$address);	
			}
		}
		return $return;	
	}
	
	public function getAttactment($xid){
		$attachment = array();
		$exec = DB::select("SELECT * FROM ESv2_Email_Attachment WHERE EmailID='".$xid."'");
		foreach($exec as $r){
		   $attachment[] = array(
		                     'path' => $r->Extname,
							 'as'   => $r->Filename,
							 'mime' => mime_content_type($r->Extname),
		                   );
		}
		return $attachment;
	}
	
	public function getGroups(){
		$content   = "<option value='-1' selected>Custom</option>";
		if(isfaculty()==false){
		 $content .= "<option value='0'>All</option>";
		}
		$content  .= "<option value='1'>All Faculty</option>";
		
		if(isfaculty()==false){
		 $content .= "<option value='2'>All Parents</option>";
		}else{
		 $content .= "<option value='3'>All Parents</option>";	
		}
		return $content;
	}
	
	public function getContactByGroup($grpid=0){
		$contact = array();
		$qry     = '';
		switch($grpid){
		  case 0:	
			$qry = "SELECT Guardian_Name as FullName
						  ,Guardian_Email as Email 
						  ,'Parent' as UserGroup
					  FROM ESv2_Admission_FamilyBackground WHERE Guardian_Name<>'' AND Guardian_Email<>''
					UNION ALL
					SELECT Father_Name
						  ,Father_Email 
						  ,'Parent' as UserGroup
					  FROM ESv2_Admission_FamilyBackground WHERE Father_Name<>'' AND Father_Email<>''
					UNION ALL
					SELECT Mother_Name
						  ,Mother_Email 
						  ,'Parent' as UserGroup
					  FROM ESv2_Admission_FamilyBackground WHERE Mother_Name<>'' AND Mother_Email<>''
					UNION ALL
					SELECT FullName
						  ,Email
						  ,'Employee' as UserGroup 
					  FROM ESv2_Users WHERE FacultyID<>'' AND Email<>''";
		  break;
		  case 1:
		    $qry = "SELECT FullName
						  ,Email
						  ,'Employee' as UserGroup 
					  FROM ESv2_Users WHERE FacultyID<>'' AND Email<>''";
		  break;
          case 2:
		  case 3:
		    $qry = "SELECT Guardian_Name as FullName
						  ,Guardian_Email as Email 
						  ,'Parent' as UserGroup
					  FROM ESv2_Admission_FamilyBackground WHERE Guardian_Name<>'' AND Guardian_Email<>''
					UNION ALL
					SELECT Father_Name
						  ,Father_Email 
						  ,'Parent' as UserGroup
					  FROM ESv2_Admission_FamilyBackground WHERE Father_Name<>'' AND Father_Email<>''
					UNION ALL
					SELECT Mother_Name
						  ,Mother_Email 
						  ,'Parent' as UserGroup
					  FROM ESv2_Admission_FamilyBackground WHERE Mother_Name<>'' AND Mother_Email<>''";
          break;		  
		}
		if($qry!=''){
			$exec  = DB::select($qry);
			$tmpid = count($contact);
			$count = 1;
			foreach($exec as $r){
				if($count<=10){
				  $contact[$tmpid][]  = trim($r->Email); 	 
				}else{
				  $tmpid              = count($contact);
				  $contact[$tmpid][]  = trim($r->Email);
				  $count=1;
				}
				$count++;
			}
		}
		
		return $contact;
	}
	
	public function getContact(){
		$contacts   = array();
		
		$contacts[] = array('text'=>'Parents','children'=>$this->getAllParent());   
		$contacts[] = array('text'=>'Faculty','children'=>$this->getAllFaculty());   
		$contacts[] = array('text'=>'Staff','children'=>$this->getAllStaff());   
		return $contacts;
	}
	
	public function getAllFaculty(){
		$contacts = array();
		$exec     = DB::select("SELECT Username,FullName,Email FROM ESv2_Users WHERE FacultyID<>''");
		foreach($exec as $r){
			$contacts[] = array('id'=>$r->Email,'text'=>$r->FullName);   
		}
		return $contacts;
	}
	
	public function getAllStaff(){
	    $contacts = array();
		$exec     = DB::select("SELECT Username,FullName,Email FROM ESv2_Users WHERE FacultyID='' AND FamilyID=''");
		foreach($exec as $r){
			$contacts[] = array('id'=>$r->Email,'text'=>$r->FullName);   
		}
		return $contacts;
	}
	
	public function getAllParentFaculty(){
	   $qry  = "SELECT fb.Guardian_Name
					 ,fb.Guardian_Email
					 ,fb.Father_Name
					 ,fb.Father_Email
					 ,fb.Mother_Name
					 ,fb.Mother_Email 
				 FROM ESv2_Users as u
		   INNER JOIN ES_ClassSchedules as cs ON u.FacultyID=cs.FacultyID
		   INNER JOIN ES_RegistrationDetails as rd ON cs.ScheduleID=rd.ScheduleID
		   INNER JOIN ES_Registrations as r ON rd.RegID = r.RegID
		   INNER JOIN ES_Students as s ON r.StudentNo=s.StudentNo
		   INNER JOIN ESv2_Admission_FamilyBackground as fb ON s.FamilyID=fb.FamilyID
			    WHERE u.UserIDX='".getUserID()."'";
       $exec = DB::select($qry);
       return $exec;	   
	}
	
	public function getAllParent(){
		$contacts = array();
		$exec     = DB::select("SELECT Username,FullName,Email FROM ESv2_Users WHERE FamilyID<>''");
		foreach($exec as $r){
			$contacts[] = array('id'=>$r->Email,'text'=>$r->FullName);   
		}
		return $contacts;
	}
	
	function fetchBody($conn,$mid){
	   $message  = imap_fetchbody($conn,$mid,2);
	   if($message==''){
		$message = imap_fetchbody($conn,$mid,'');
		$message = explode('quoted-printable',$message);
		$message = imap_qprint($message[1]);
	   }else{
		$message = imap_qprint($message);   
	   }
	   
	   return $message;		   
	}
	
	//===================================================================================================================================================================================================
	function getBody($imap,$uid)
	{
		$body = $this->get_part($imap, $uid, "TEXT/HTML");
		// if HTML body is empty, try getting text body
		if ($body == "") {
			$body = $this->get_part($imap, $uid, "TEXT/PLAIN");
		}
		return $body;
	}

	function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false)
	{
		if (!$structure) {
			$structure = imap_fetchstructure($imap, $uid);
		}
		if ($structure) {
			if ($mimetype == $this->get_mime_type($structure)) {
				if (!$partNumber) {
					$partNumber = 1;
				}
				$text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);
				switch ($structure->encoding) {
					case 3:
						return imap_base64($text);
					case 4:
						return imap_qprint($text);
					default:
						return $text;
				}
			}

			// multipart
			if ($structure->type == 1) {
				foreach ($structure->parts as $index => $subStruct) {
					$prefix = "";
					if ($partNumber) {
						$prefix = $partNumber . ".";
					}
					$data = $this->get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
					if ($data) {
						return $data;
					}
				}
			}
		}
		return false;
	}

	function get_mime_type($structure)
	{
		$primaryMimetype = ["TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER"];

		if ($structure->subtype) {
			return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
		}
		return "TEXT/PLAIN";
	}
	
}
?>