<?php
	Route::group(['prefix' => 'class-record','middleware'=>'auth'], function () {
	    Route::group(['prefix' => 'schedules','middleware'=>'auth'], function () {
		    Route::get('/','FacultySchedules@index');
		    Route::post('event','FacultySchedules@event_service');
		});
        Route::group(['prefix' => 'eclass-records','middleware'=>'auth'], function () {
		    Route::get('/','eClassRecords@index');
		    Route::post('thisRequest','eClassRecords@iservice');
            Route::get('layout','eClassRecords@generate_layout');
            Route::get('print','eClassRecords@print_data');
		});
        Route::group(['prefix' => 'enarrative','middleware'=>'auth'], function () {
		    Route::get('/','eNarrative@index');
		    Route::get('thisRequest','eNarrative@txn');
		    Route::post('thisRequest','eNarrative@txn');
		});
        Route::group(['prefix' => 'subject-formations','middleware'=>'auth'], function () {
		    Route::get('/','Formations@index');
		    Route::post('thisRequest','Formations@myservice');
		});  
        Route::group(['prefix' => 'attendance','middleware'=>'auth'], function () {
		    Route::get('/','Attendance@index');
		    Route::post('/event','Attendance@event');
		});     
        Route::group(['prefix' => 'discipline','middleware'=>'auth'], function () {
		    Route::get('/','Attendance@index');
		    Route::post('/event','Attendance@event');
		});     
        
        Route::group(['prefix' => 'student-conduct','middleware'=>'auth'], function () {
		    Route::get('/','StudentConduct@index');
		    Route::post('event','StudentConduct@event_service');
		});  
        
        Route::group(['prefix' => 'checklist','middleware'=>'auth'], function () {
		    Route::get('/','Checklist@index');
		    Route::post('event','Checklist@event_service');
            Route::get('print','Checklist@print_data');
		});
		
        Route::group(['prefix' => 'remedial','middleware'=>'auth'], function () {
		    Route::get('/','Remedial@index');
		    Route::post('event','Remedial@event');
		});
        
        Route::group(['prefix' => 'review','middleware'=>'auth'], function () {
		    Route::get('/','ClassRecReview@index');
		    Route::post('event','ClassRecReview@event');
		});
        
        
        Route::group(['prefix' => 'student-clubs','middleware'=>'auth'], function () {
		    Route::get('/','StudentClubs@index');
		    Route::post('event','StudentClubs@event_service');
            Route::get('print','StudentClubs@print_data');
		});

		Route::group(['prefix' => 'mentoring','middleware'=>'auth'], function () {
		    Route::get('/','Mentoring@index');
            Route::get('/new','Mentoring@sheet');
            Route::get('/edit','Mentoring@sheet');
		    Route::post('event','Mentoring@event');
            Route::get('print','Mentoring@print_data');
		});
		 Route::group(['prefix' => 'student-attendance','middleware'=>'auth'], function () {
		    Route::get('/','HomeRoom@index');
		    Route::post('event','HomeRoom@event_service');
		});
		 
                     
	});
?>