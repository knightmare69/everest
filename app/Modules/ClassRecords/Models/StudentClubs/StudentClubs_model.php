<?php 

namespace App\Modules\ClassRecords\Models\StudentClubs;
use illuminate\Database\Eloquent\Model;
use DB;

Class StudentClubs_model extends Model {

	protected $table='ES_StudentsClubbing';
	public $primaryKey ='IndexID';

	protected $fillable  = array(    	   
          'RegID'
          ,'StudentNo'
          ,'ClubID'
          ,'DateCreated'
          ,'Createdby'
	);

	public $timestamps = false;

    public function scopeScore($query, $sched, $conduct, $period, $idno ){        
        $data = $query                   
            ->where('ScheduleID', $sched)
            ->where('ConductID', $conduct)
            ->where('PeriodID', $period)  
            ->where('StudentNo', $idno);            
		return $data;                
    }             
    
    public static function ConductGrade($contbl, $grade  ){            
        $components = DB::table('ES_GS_GradingSystem_Details')
		              ->select([ 'LetterGrade','Description','Remark'])                		                      
                	  ->whereRaw(" ('". $grade ."' BETWEEN CAST([Min] AS FLOAT) AND CAST([Max] AS FLOAT) ) ")
                      ->where('TemplateID' , $contbl )                      
                      ->first();
        
        return $components;               
    }
    
    public function getClassSections($term, $faculty)
	{
	   // TEMP : Filter by IBED & SHS Only
	    $data = DB::table('ES_ClassSections')
                    ->select(["SectionID",'SectionName','Limit','IsBlock','YearLevelID'])
                    ->where('TermID', $term)
                    ->where('AdviserID', $faculty )
                    ->where('IsDissolved','0')
                    ->whereIn('ProgramID',['1','21','29'])
                    ->get();
                    
		return $data; 
	}
    
    public function getClassSchedules($term, $faculty )
	{
	   
        // TEMP : Filter by IBED & SHS Only                
	    $data = DB::table('ES_ClassSchedules as cs ')
                    ->leftJoin('ES_ClassSections AS se ','se.SectionID','=','cs.SectionID')
                    ->select(["ScheduleID","se.SectionName", DB::raw(" dbo.fn_SubjectCode(SubjectID) As SubjectCode ") ])
                    ->where('cs.TermID', $term)
                    ->where('cs.FacultyID', $faculty )
                    ->whereIn('se.ProgramID',['1','21','29'])
                    
                    ->get();
                    
		return $data; 
	}
    
       
    public static function getStudents($term, $club, $period  ){    
           
           $colGrade = '';           
           $colDatePosted = '';
           $colPostedby = '';
           
           switch($period){
             case '1':
             case '11': 
                $colGrade ='pd.AverageA';
                $colDatePosted = 'DatePosted1';
                $colPostedby = 'PostedBy1';
             break;
             case '2':
             case '12': 
                $colGrade ='pd.AverageB';
                $colDatePosted = 'DatePosted2';
                $colPostedby = 'PostedBy2';
             break;
             case '3': 
                $colGrade ='pd.AverageC';
                $colDatePosted = 'DatePosted3';
                $colPostedby = 'PostedBy3';
             break;
             case '4': 
                $colGrade ='pd.AverageD';
                $colDatePosted = 'DatePosted4';
                $colPostedby = 'PostedBy4';
             break;
             
           }
           
            /* Provide a list of students whose currently enrolled. */
            $data = DB::table('ES_Registrations as r')          
                        ->leftJoin('ES_StudentsClubbing as c', 'c.RegID','=','r.RegID')          
                        ->leftJoin('ES_RegistrationDetails as d', 'd.RegID','=','r.RegID')
                        ->leftJoin('ES_ClassSchedules as cs', 'cs.ScheduleID','=','d.ScheduleID')
                        ->leftJoin('ES_Subjects as sub', 'cs.SubjectID','=','sub.SubjectID')
                        ->leftJoin('ES_PermanentRecord_Details as pd', function($join){
                            $join->on('pd.RegID', '=', 'r.RegID');
                            $join->on('pd.ScheduleID','=','cs.ScheduleID');
                        })
                        ->select(['r.RegID','r.StudentNo', DB::raw("dbo.fn_StudentName(r.StudentNo) AS FullName, 
                            dbo.fn_StudentGender(r.StudentNo) AS Gender,
                            dbo.fn_YearLevel_k12(r.YearLevelID, dbo.fn_ProgramClassCode(r.ProgID)) As YearLevel,
                            cs.ScheduleID, ". $colGrade ." As FinalGrade 
                            
                            "), $colDatePosted.' AS DatePosted ', $colPostedby . ' As Postedby' ])
                        ->where(['r.TermID' => $term , 'r.IsWithdrawal' => '0' , 'c.ClubID' => $club , 'sub.IsClubOrganization' => '1'  ])                                                                   
                        ->whereRaw("r.ValidationDate IS NOT NULL AND d.RegTagID < 3  ")
                        ->orderBy('FullName','asc')
                        ->get();        
       
		return $data;
                   
    }           
}