<?php

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class eClassRecord_model extends Model {

	protected $table='ES_GS_RawScores';
	public $primaryKey ='fldPK';

	protected $fillable  = array(
	     'EventID'
        ,'StudentNo'
        ,'TermID'
        ,'GradingPeriod'
        ,'InputDate'
        ,'RawScore'
        ,'TransmutedGrade'
        ,'LetterGrade'
        ,'IsAbsent'
        ,'LastModifiedBy'
        ,'LastModifiedDate'
	);

	public $timestamps = false;


	public static function get_academicyearterm($facultyid)
	{
        $data = DB::table('ES_ClassSchedules as p')
		              ->select([ DB::raw("DISTINCT p.TermID,p.ScheduleID, dbo.fn_AcademicYearTerm(p.TermID) AS 'AYTerm',
                          s.SectionName , s.YearLevelID AS 'YearLevel', p.SubjectID, dbo.fn_SubjectCode(p.SubjectID) AS 'SubjectCode', dbo.fn_SubjectTitle(p.SubjectID) AS 'SubjectTitle' , dbo.fn_SubjectAcademicUnits(p.SubjectID) AS 'Units',
                          o.PolicyID AS 'PolicyID', g.Policy AS 'PolicyName', s.ProgramID,
                          g.IsConduct, g.IsZeroBased,
                          ISNULL(dbo.fn_K12_YearLevel3(s.YearLevelID, s.ProgramID),'') as 'YearLevelName'
                            
                        " )])
                      ->leftJoin('ES_ClassSections AS s','s.SectionID','=','p.SectionID')
                      ->leftJoin('ES_GS_PolicySetup As o','o.ScheduleID','=','p.ScheduleID')
                      ->leftJoin('ES_GS_GradingPolicy As g','g.PolicyID','=','o.PolicyID')
                      ->whereRaw("( p.FacultyID = '" . $facultyid. "' OR p.FacultyID_2 = '". $facultyid ."')" )
                      ->orderBy('AYTerm','DESC')
                      ->orderBy('SectionName','ASC')
                      ->get();

	    //$data = DB::select("SELECT TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm', ScheduleID, dbo.fn_SectionName(SectionID) AS 'SectionName', dbo.fn_SubjectCode(SubjectID) AS 'SubjectCode'  FROM dbo.ES_ClassSchedules WHERE FacultyID = '".$facultyid."' ORDER BY AYTerm DESC");
		return $data;
	}

    public static function get_students($schedule_id, $gender , $shs, $period ){         
        if($shs != 0 ){
            $data = DB::select(DB::raw("EXEC dbo.sp_shsofficial_classlist '".$schedule_id."','".$gender."','". $period ."'"));
        }else{
            $data = DB::select(DB::raw("EXEC dbo.sp_official_classlist '".$schedule_id."','".$gender."','".$period."'"));
        }

		return $data;
    }

    public static function get_components($sched_id, $period){

        $components = DB::table('ES_GS_PolicySetup as p')
		              ->select([ 'p.ScheduleID','p.PeriodID','p.PolicyID', 'c.ComponentID', 'c.CompCode', 'c.Percentage', 'Caption', 'g.IsHomeRoom','c.ParentID','c.HasChild','g.ProgID', 'c.Cols','g.IsConduct'])
                  ->selectRaw("(SELECT COUNT(EventID) FROM dbo.ES_GS_GradeEvents WHERE ScheduleID=".$sched_id." AND GradingPeriod=".$period." AND ComponentID=c.ComponentID) as count")
                      ->leftJoin('ES_GS_PolicyComponents AS c','p.PolicyID','=','c.PolicyID')
                      ->leftJoin('ES_GS_GradingPolicy AS g','p.PolicyID','=','g.PolicyID')
                	  ->where([ 'p.ScheduleID' => $sched_id, 'p.PeriodID' => $period,])
                      ->orderBy('c.ParentID','asc')
                      ->orderBy('c.SortOrder','asc')
                      ->get();

        return $components;
    }

    public static function scopeScore($query, $event, $idno){
        $data = $query
            ->select(['*'])
            ->where('EventID', $event)
            ->whereRaw('EventID <> 0 ')
            ->where('StudentNo',$idno);

		return $data;
    }

     public function IsPosted($period, $termid , $sched ,  $idno){
        $col = '';
        $date = '';
        $grade = '';
        $final = '';

        switch($period){
            case '1':
            case '11':
                $col = 'PostedBy1';
                $grade = 'PeriodGrade1';
                $final = 'AverageA';
                $date = 'DatePosted1';
            break;
            case '2':
            case '12':
                $col = 'PostedBy2';
                $grade = 'PeriodGrade2';
                $final = 'AverageB';
                $date = 'DatePosted2';
            break;
            case '3':
                $col = 'PostedBy3';
                $grade = 'PeriodGrade3';
                $final = 'AverageC';
                $date = 'DatePosted3';
            break;
            case '4':
                $col = 'PostedBy4';
                $grade = 'PeriodGrade4';
                $final = 'AverageD';
                $date = 'DatePosted4';
            break;

        }

        $res = DB::table('ES_PermanentRecord_Details')
                      ->select([ $col . ' AS IsPostedby ', $final . ' AS Average', $grade . ' AS Grade ' , $date .' AS DatePosted ' ])
                      ->where('StudentNo' , $idno )
                      ->where('TermID' , $termid )
                      ->where('ScheduleID' , $sched )
                      ->first();

         if ( empty($res) ){
            $posted = ['Posted' => 0 , 'Grade' => '0.00', 'Final' => '60', 'DatePosted' => '' ];
         }else{
            $grade = ($res->Grade != '' ?  number_format($res->Grade,2) : '0.00' );
            $final = ($res->Average != '' ?  $res->Average : '60' );
            $posted = $posted = ['Posted' => $res->IsPostedby != '' ? 1 : 0  , 'Grade' => $grade , 'Final' => $final , 'DatePosted' => $res->DatePosted ];
         }
        return $posted;
    }

    public static function transmuteGrade($setup, $grade){

         $components = DB::table('ES_GS_Transmutation_Table')
		              ->select([ 'TransmutedGrade'])
                	   ->whereRaw(" ('". number_format($grade,2) ."' BETWEEN Min AND Max ) ")
                        ->where('TemplateID' , $setup )
                        ->first();
        return $components;
    }

    public static function gradeRemarks ($setup, $grade){

         $components = DB::table('ES_GS_GradingSystem_Details')
		              ->select([ '*'])
                	  ->whereRaw(" ('". $grade ."' BETWEEN CAST([Min] AS FLOAT) AND CAST([Max] AS FLOAT) ) ")
                      ->where('TemplateID' , $setup )
                      ->first();

        return $components;
    }

    public static function get_parentScheduleID($regid, $subjid ){

        $output = DB::table('ES_RegistrationDetails as rd')
		              ->select(['cs.ScheduleID'])
                      ->leftJoin('ES_ClassSchedules AS cs','rd.ScheduleID','=','cs.ScheduleID')
                	  ->where([ 'rd.RegID' => $regid, 'cs.SubjectID' => $subjid,])
                      ->pluck('cs.ScheduleID');
        return $output;
    }

    
   	public static function get_classRecord($term, $idno )
	{
        $data = DB::table('ES_ClassSchedules as p')
		              ->selectRaw("DISTINCT p.TermID,p.ScheduleID, dbo.fn_AcademicYearTerm(p.TermID) AS 'AYTerm',
                          s.SectionName , s.YearLevelID AS 'YearLevel', p.SubjectID, dbo.fn_SubjectCode(p.SubjectID) AS 'SubjectCode', dbo.fn_SubjectTitle(p.SubjectID) AS 'SubjectTitle' , dbo.fn_SubjectAcademicUnits(p.SubjectID) AS 'Units',
                          o.PolicyID AS 'PolicyID', g.Policy AS 'PolicyName', s.ProgramID,
                          g.IsConduct, g.IsZeroBased,
                          ISNULL(dbo.fn_K12_YearLevel3(s.YearLevelID, s.ProgramID),'') as 'YearLevelName'
                            
                        ")
                      ->leftJoin('ES_ClassSections AS s','s.SectionID','=','p.SectionID')
                      ->leftJoin('ES_GS_PolicySetup As o','o.ScheduleID','=','p.ScheduleID')
                      ->leftJoin('ES_GS_GradingPolicy As g','g.PolicyID','=','o.PolicyID')
                      ->whereRaw("p.TermID = '{$term}' AND ( p.FacultyID = '" . $idno. "' OR p.FacultyID_2 = '". $idno ."')" )
                      ->orderBy('AYTerm','DESC')
                      ->orderBy('SectionName','ASC')
                      ->get();

	    //$data = DB::select("SELECT TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm', ScheduleID, dbo.fn_SectionName(SectionID) AS 'SectionName', dbo.fn_SubjectCode(SubjectID) AS 'SubjectCode'  FROM dbo.ES_ClassSchedules WHERE FacultyID = '".$facultyid."' ORDER BY AYTerm DESC");
		return $data;
	}
    
	public static function getConductNarrative($term,$idno){
	    $data = DB::table('ES_Registrations as reg')
		                ->selectRaw("dbo.fn_K12_GetAverageConduct(reg.TermID,2,reg.StudentNo,1) AS ConductA1,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,3,reg.StudentNo,1) AS ConductB1,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,4,reg.StudentNo,1) AS ConductC1,
									 
									 dbo.fn_K12_GetAverageConduct(reg.TermID,2,reg.StudentNo,2) AS ConductA2,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,3,reg.StudentNo,2) AS ConductB2,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,4,reg.StudentNo,2) AS ConductC2,
									 
									 dbo.fn_K12_GetAverageConduct(reg.TermID,2,reg.StudentNo,3) AS ConductA3,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,3,reg.StudentNo,3) AS ConductB3,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,4,reg.StudentNo,3) AS ConductC3,
									 
									 dbo.fn_K12_GetAverageConduct(reg.TermID,2,reg.StudentNo,4) AS ConductA4,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,3,reg.StudentNo,4) AS ConductB4,
									 dbo.fn_K12_GetAverageConduct(reg.TermID,4,reg.StudentNo,4) AS ConductC4,
								   
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,1,1) AS AbsentQ1,
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,1,2) AS AbsentQ2,
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,1,3) AS AbsentQ3,
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,1,4) AS AbsentQ4,

									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,2,1) AS TardyQ1,
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,2,2) AS TardyQ2,
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,2,3) AS TardyQ3,
									 dbo.fn_countOffensesv2(reg.TermID,reg.studentno,2,4) AS TardyQ4,
									
									 (SELECT NarrativeA1 FROM dbo.ESv2_Narrative WHERE TermID=reg.TermID AND StudentNo=reg.StudentNo) AS Narrative1,
									 (SELECT NarrativeA2 FROM dbo.ESv2_Narrative WHERE TermID=reg.TermID AND StudentNo=reg.StudentNo) AS Narrative2,
									 (SELECT NarrativeA3 FROM dbo.ESv2_Narrative WHERE TermID=reg.TermID AND StudentNo=reg.StudentNo) AS Narrative3,
									 (SELECT NarrativeA4 FROM dbo.ESv2_Narrative WHERE TermID=reg.TermID AND StudentNo=reg.StudentNo) AS Narrative4")
						->whereRaw("reg.TermID='".$term."' AND reg.StudentNo='".$idno."'")
                        ->get();
		return $data;
	}
    
}
