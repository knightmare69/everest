<?php

namespace App\Modules\ClassRecords\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class ClassRecReview_model extends Model
{
    private $tbl_empl = 'HR_Employees';
    private $tbl_sched = 'ES_ClassSchedules';

    public function getFaculty($term_id, $find = '', $not_include = array())
    {
        $_arr = [];

        if(!empty($not_include)){
            $not_include = array_map('decode', $not_include);
        }

        if(getUserPosition() == 'Principal' || getUserPosition() == 'Vice Principal' || 
		   getUserPosition() == 'Dean' || getUserPosition() == 'Vice Dean' || 
		   getUserPosition() == 'Coordinator' || getUserPosition() == 'Psychometrician'){

            switch (getUserPosition()) {
                case 'Principal':
                    $hadled_dept = DB::table('ES_Departments')->select('DeptID')->where('DeptHead_EmployeeID', getUserFacultyID())->get();
                    break;
                case 'Vice Principal':
                    $hadled_dept = DB::table('ES_Departments')->select('DeptID')->where('DeptVice_EmployeeID', getUserFacultyID())->get();
                    break;
                case 'Dean':
                case 'Vice Dean':
                case 'Coordinator':
                case 'Psychometrician':
                    $hadled_dept = DB::table('ES_Departments')->select('DeptID')->where('DeptID', getUserDepartment())->get();
                    break;
            }

            if(!empty($hadled_dept)){
                foreach ($hadled_dept as $key => $hd) {
                    $dept_id[] = $hd->DeptID;
                }
            } else {
                $dept_id[0] = '';
            }

            if(!empty($hadled_dept)){
                foreach ($hadled_dept as $key => $hd) {
                    $dept_id[] = $hd->DeptID;
                }
            } else {
                $dept_id[0] = '';
            }

            // $dept_id = !empty($hadled_dept->DeptID) ? $hadled_dept->DeptID : 0;

            $faculties = DB::table($this->tbl_sched.' as cs')
                        ->select(DB::raw('distinct cs.FacultyID, cs.FacultyID_2'))
                        ->join('ESv2_Users as u', 'u.FacultyID', '=', 'cs.FacultyID')
                        // ->join('ES_ClassSections as csec', 'csec.SectionID', '=', 'cs.SectionID')
                        ->where('cs.TermID', $term_id)
                        ->where('cs.FacultyID', '!=', '')
                        ->whereIn('u.DepartmentID', $dept_id)
                        // ->whereIn('csec.ProgramID', [1, 21, 29])
                        ->whereNotIn('cs.FacultyID', $not_include)->get();


        } else {
			$fac_id    = getUserFacultyID();
			$faculties = DB::table($this->tbl_sched.' as cs')
                        ->select(DB::raw('distinct cs.FacultyID, cs.FacultyID_2'))
                        ->join('ES_Subjects as s', 's.SubjectID', '=', 'cs.SubjectID')
                        ->leftjoin('es_subjectAreas as sa', 'sa.SubjectAreaID', '=', 's.SubjectAreaID')
                        ->where('cs.TermID', $term_id)
                        ->where('cs.FacultyID', '!=', '')
                        ->where('sa.CoordinatorID', '=' , trim($fac_id))
                        ->whereNotIn('cs.FacultyID', $not_include)->get();	
        }
		
        for($a = 0; $a < count($faculties); $a++){
            $_arr[] = $faculties[$a]->FacultyID;

            if(!empty($faculties[$a]->FacultyID_2)){
                $_arr[] = $faculties[$a]->FacultyID_2;
            }
        }

        $tbl = DB::table($this->tbl_empl)
                ->select('EmployeeID', 'LastName', 'FirstName', 'MiddleInitial', 'Gender', DB::raw('dbo.fn_DepartmentName(DeptID) as DeptName'))
                ->whereIn('EmployeeID', $_arr);

        if(!empty($find)){
            $tbl->where(function($query) use ($find){
                $query->where('LastName', 'like', "%$find%")
                ->orWhere('FirstName', 'like', "%$find%");
            });
        }

        return $tbl->take(10)->orderBy('LastName', 'ASC')->get();
    }

    public function getFacultyPhoto($faculty_id)
    {
        $find = DB::table($this->tbl_empl)->select('Photo')->where('EmployeeID', $faculty_id)->first();
        return $find;
    }

    public function getFacultySectionWithSubjects($term_id, $e_id)
    {
        $get = DB::table($this->tbl_sched.' as sc')
                ->select('ScheduleID', 'sc.SectionID', 'SubjectID',
                    DB::raw('dbo.fn_SectionName(sc.SectionID) as SectionName, dbo.fn_SubjectCode(SubjectID) as SubjectCode, dbo.fn_SubjectTitle(SubjectID) as SubjectTitle, dbo.fn_YearLevel_k12(cs.YearLevelID, dbo.fn_ProgramClassCode(cs.ProgramID)) as YearLevelName'), 'cs.ProgramID'
                )
                ->join('ES_ClassSections as cs', 'cs.SectionID', '=', 'sc.SectionID')
                // ->join('ESv2_YearLevel as yl', 'yl.ProgID', '=', 'cs.ProgramID')
                ->where(['sc.TermID' => $term_id, 'sc.FacultyID' => $e_id])
                ->orWhere(['sc.TermID' => $term_id, 'FacultyID_2' => $e_id])
                ->whereIn('cs.ProgramID', [1, 21, 29])
                ->orderBy('SectionID')
                ->get();

        return $get;
    }

    public function getStudents($period, $term_id, $faculty_id, $sched_id)
    {
       $get = DB::select('dbo.sp_K12_viewGSHSgrades ?, ?, ?, ?', [$period, $term_id, $faculty_id, $sched_id]);
       return $get;
    }
}
