<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Mentoring_model extends Model {

	protected $table='ES_Mentoring';
	public $primaryKey ='SessionID';

	protected $fillable  = array(
    	   
          'TransDate'          
          ,'StudentNo'
          ,'YearLevelID'
          ,'ProgID'
          ,'TermID'
          ,'Topic'
          ,'Issue'
          ,'Goals'
          ,'MentorFollowUp'
          ,'MentorID'
          ,'DateCreated'
          ,'UserID'
          ,'DateModified'
          ,'Modifiedby'	
	);

	public $timestamps = false;

   
    
    
}