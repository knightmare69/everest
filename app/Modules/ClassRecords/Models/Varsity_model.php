<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Varsity_model extends Model {

	protected $table='es_gs_VarsityPlayer';
	public $primaryKey ='IndexID';

	protected $fillable  = array(
		'ScheduleID'
      ,'PeriodID'
      ,'StudentNo'
      ,'Grade'
      ,'Createdby'
      ,'DateCreated'
		);

	public $timestamps = false;
    
    public static function scopeIsMember($query, $stud, $sched, $period ){        
        $data = $query            
            ->where('ScheduleID', $sched)
            ->where('PeriodID', $period)
            ->where('StudentNo', $stud)            
            ->count();
                                        
		return $data;                
    }          
}