<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class ReportCardDetails_model extends Model {

	protected $table='ES_PermanentRecord_Details';
	public $primaryKey ='GradeIDX';

	protected $fillable  = array(
	   'SummaryID'
      ,'RegID'
      ,'CampusID'
      ,'TermID'
      ,'ProgClassID'
      ,'ProgramID'
      ,'YearLevelID'
      ,'StudentNo'
      ,'ScheduleID'
      ,'SubjectID'
      ,'SubjectCode'
      ,'SubjectTitle'
      ,'SubjectCreditUnits'
      ,'CreditedUnits'
      ,'IsClubbing'
      ,'IsNonAcademic'
      ,'ParentSubjectID'
      ,'AverageA'
      ,'AverageB'
      ,'TotalAverageAB'
      ,'AverageC'
      ,'AverageD'
      ,'TotalAverageCD'
      ,'Final_Average'
      ,'Final_Remarks'
      ,'Final_Equivalent'
      ,'IsConduct'
      ,'ConductA'
      ,'ConductB'
      ,'ConductC'
      ,'ConductD'
      ,'Conduct_FinalAverage'
      ,'Conduct_FinalRemarks'
      ,'LastSchoolID'
      ,'LastSchoolName'
      ,'DatePosted'
      ,'PostedBy'
      ,'LastModifiedDate'
      ,'LastModifiedBy'
      ,'CorrectionDate'
      ,'CorrectionBy'
      ,'SeqNo'
      ,'SubjectGroupID'
      ,'DatePosted1'
      ,'PostedBy1'
      ,'DatePosted2'
      ,'PostedBy2'
      ,'DatePosted3'
      ,'PostedBy3'
      ,'DatePosted4'
      ,'PostedBy4'	
      ,'PeriodGrade1'
      ,'PeriodGrade2'
      ,'PeriodGrade3'
      ,'PeriodGrade4'
      ,'GPA1'
      ,'GPA2'
      ,'GPA3'
      ,'GPA4'	  
      ,'IsSemestral'
      ,'IsNonConduct'
      ,'SuppressSubject'
      ,'SuppressAcademicGrade'
      ,'SuppressConductGrade'
	);

	public $timestamps = false;
    
    // public function gradeView($termID, $student){        
    //      $get = DB::table('ES_Registrations as r')
    //             ->leftJoin('ES_RegistrationDetails as d', 'd.RegID','=','r.regid') 
    //             ->leftJoin('ES_ClassSchedules as cs', 'cs.ScheduleID','=','d.ScheduleID')                                            
    //             ->leftJoin('ES_Subjects as s ', 's.SubjectID','=','cs.SubjectID')                                            
    //             ->leftJoin('ES_PermanentRecord_Details as pd', function($join){
    //                 $join->on('pd.RegID', '=',  'd.RegID');
    //                 $join->on('pd.ScheduleID', '=', 'd.ScheduleID');   
    //             })
    //             ->select(DB::raw("pd.GradeIDX, d.ScheduleID, cs.SubjectID, dbo.fn_SubjectCode(cs.SubjectID) AS SubjectCode, dbo.fn_SubjectTitle(cs.SubjectID) AS SubjectTitle, pd.SubjectCreditUnits, pd.ParentSubjectID,
    //                 dbo.fn_SubjectAcademicUnits(cs.SubjectID) As [CreditUnit], 
    //                 dbo.fn_SectionName(cs.SectionID) As SectionName,  TotalAverageAB, s.IsNonAcademic,
    //                 CASE WHEN pd.PostedBy1 <>'' THEN  pd.AverageA ELSE '' END AS AverageA , 
    //                 CASE WHEN pd.PostedBy2 <>'' THEN  pd.AverageB ELSE '' END AS AverageB , 
    //                 CASE WHEN pd.PostedBy3 <>'' THEN  pd.AverageC ELSE '' END AS AverageC , 
    //                 CASE WHEN pd.PostedBy4 <>'' THEN  pd.AverageD ELSE '' END AS AverageD ,
    //                 Final_Average,
    //                 Final_Remarks,                    
    //                 dbo.fn_gs_graderemarks(1,Final_Average) As Remarks,
    //                 round(dbo.fn_getGeneralAverageSHSk12 (r.TermID, r.StudentNo),2) as general_average
    //             "))
    //             ->where('r.StudentNo', $student)
    //             ->where('r.TermID', $termID);

    //      return $get;                     
    // }
    public function gradeView($termID, $student){        
         $get = DB::table('ES_Registrations as r')
                ->leftJoin('ES_RegistrationDetails as d', 'd.RegID','=','r.regid') 
                ->leftJoin('ES_ClassSchedules as cs', 'cs.ScheduleID','=','d.ScheduleID')                                            
                ->leftJoin('ES_Subjects as s ', 's.SubjectID','=','cs.SubjectID')                                            
                ->leftJoin('ES_PermanentRecord_Master as pm ', 'pm.regid','=','r.regid')
                ->leftJoin('ES_PermanentRecord_Details as pd', function($join){
                    $join->on('pd.RegID', '=',  'd.RegID');
                    $join->on('pd.ScheduleID', '=', 'd.ScheduleID');   
                })
                ->select(DB::raw("pd.GradeIDX, d.ScheduleID, cs.SubjectID, dbo.fn_SubjectCode(cs.SubjectID) AS SubjectCode, dbo.fn_SubjectTitle(cs.SubjectID) AS SubjectTitle, pd.SubjectCreditUnits, pd.ParentSubjectID,
                    dbo.fn_SubjectAcademicUnits(cs.SubjectID) As [CreditUnit]
                    ,s.Weight					
                    ,dbo.fn_SectionName(cs.SectionID) As SectionName,   s.IsNonAcademic
                    ,CASE WHEN pd.PostedBy1 <>'' THEN  pd.AverageA ELSE '' END AS AverageA
                    ,CASE WHEN pd.PostedBy2 <>'' THEN  pd.AverageB ELSE '' END AS AverageB
                    ,CASE WHEN pd.PostedBy3 <>'' THEN  pd.AverageC ELSE '' END AS AverageC
                    ,CASE WHEN pd.PostedBy4 <>'' THEN  pd.AverageD ELSE '' END AS AverageD
                    ,TotalAverageAB 
                    ,pd.Final_Average
                    ,pd.Final_Remarks
                    ,CASE WHEN ISNUMERIC(pd.Final_Average) = 1 THEN dbo.fn_gs_graderemarks(1, pd.Final_Average) 
                          ELSE pd.Final_Average END AS Remarks                     
                    ,pm.Final_Average AS general_average
                    ,ConductA
                    ,ConductB
                    ,ConductC
                    ,ConductD
                    ,Conduct_FinalAverage
                    ,Conduct_FinalRemarks
                    ,dbo.fn_SubjectSortOrder2(cs.SubjectID, (SELECT CurriculumID FROM dbo.ES_Students where studentno=r.StudentNo)) As SeqNo
					,pd.GPA1
				    ,pd.GPA2
				    ,pd.GPA3
				    ,pd.GPA4
                "))
                ->where('r.StudentNo', $student)
                ->where('r.TermID', $termID)
                ->whereRaw('s.SubjectCode IS NOT NULL')
                ->orderBy('SeqNo','asc')
                ;
                
                //round(dbo.fn_getGeneralAverageSHSk12 (r.TermID, r.StudentNo),2) as general_average
                
         return $get;                       
    }
    
    
    public static function scopeStudent($query, $idno){        
		return $query->where('StudentNo', $idno);                
    }
    
    public static function scopeSubject($query, $subj){                                            
		return $query->where('SubjectID', $subj);
    }
    
    public function gradeView3($termID, $student){
         $get = DB::table('es_permanentrecord_master as pm')                
                ->leftJoin('es_permanentrecord_details as pd', function($join){
                    $join->on('pm.TermID', '=',  'pd.TermID');                    
					$join->on('pm.StudentNo', '=', 'pd.StudentNo');					
                })
                ->leftJoin('es_subjects as s ', 'pd.SubjectID','=','s.SubjectID')               
                ->selectRaw("pd.GradeIDX, pd.ScheduleID, pd.SubjectID, s.SubjectCode AS SubjectCode, s.SubjectTitle AS SubjectTitle, 
                    pd.SubjectCreditUnits, pd.ParentSubjectID,
                    s.CreditUnits As CreditUnit,
                    s.Weight,					
                    pd.DatePosted,
                    pd.PostedBy,
                    pm.ClassSectionName As SectionName,  s.IsNonAcademic,
                    CASE WHEN pd.PostedBy1 <>'' THEN  pd.AverageA ELSE '' END AS AverageA ,
                    CASE WHEN pd.PostedBy2 <>'' THEN  pd.AverageB ELSE '' END AS AverageB ,
                    CASE WHEN pd.PostedBy3 <>'' THEN  pd.AverageC ELSE '' END AS AverageC ,
                    CASE WHEN pd.PostedBy4 <>'' THEN  pd.AverageD ELSE '' END AS AverageD ,
                    TotalAverageAB, TotalAverageCD
                    ,pd.Final_Average
                    ,pd.Final_Remarks
                    ,pd.Final_Equivalent
                    ,'' AS Remarks
                    ,pm.Final_Average AS general_average
                    ,(SELECT ISNULL(score,0) FROM dbo.ES_GS_Conduct WHERE StudentNo=pm.StudentNo AND ConductID=1 AND ScheduleID=pd.ScheduleID AND PeriodID=1) as ConductA
                    ,(SELECT ISNULL(score,0) FROM dbo.ES_GS_Conduct WHERE StudentNo=pm.StudentNo AND ConductID=1 AND ScheduleID=pd.ScheduleID AND PeriodID=2) as ConductB
                    ,(SELECT ISNULL(score,0) FROM dbo.ES_GS_Conduct WHERE StudentNo=pm.StudentNo AND ConductID=1 AND ScheduleID=pd.ScheduleID AND PeriodID=3) as ConductC
                    ,(SELECT ISNULL(score,0) FROM dbo.ES_GS_Conduct WHERE StudentNo=pm.StudentNo AND ConductID=1 AND ScheduleID=pd.ScheduleID AND PeriodID=4) as ConductD
                    ,Conduct_FinalAverage
                    ,Conduct_FinalRemarks
                    ,dbo.fn_SubjectSortOrder2(s.SubjectID, (SELECT CurriculumID FROM es_students where studentno=pm.StudentNo)) As SeqNo
                    ,s.IsPassFail
					,pd.GPA1
				    ,pd.GPA2
				    ,pd.GPA3
				    ,pd.GPA4
                ")
                ->where('pm.StudentNo', $student)
                ->where('pm.TermID', $termID)
                ->whereRaw('s.SubjectCode IS NOT NULL')
                ->orderBy('SeqNo','asc')
                ;

                //round(fn_getGeneralAverageSHSk12 (r.TermID, r.StudentNo),2) as general_average

         return $get;
    }
    
    
    
    
}