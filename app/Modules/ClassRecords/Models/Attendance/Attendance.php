<?php 
namespace App\Modules\ClassRecords\Models\Attendance;

use illuminate\Database\Eloquent\Model;
use DB;


Class Attendance extends Model {

	public $table='ES_GS_MeritDemerits';
	protected $primaryKey ='fldPK';

	protected $fillable  = array(
			'TermID',
			'PeriodID',
			'StudentNo',
			'Date',
			'Particulars',
			'IsExcused',
			'Merit',
			'Demerit',
			'Remarks',
			'DateEnd',
			'Days',
			'SectionID',
			'Type'
	);

	public $timestamps = false;
	public static function get_advisersection($facultyid){   
		$data = DB::table('ES_ClassSections as s')
		              ->join('ES_AYTerm as a', 'a.TermID', '=', 's.TermID')
		              ->select([ 's.SectionID','s.SectionName','s.TermID','a.AcademicYear','a.SchoolTerm'])
		              ->where('s.AdviserID' , $facultyid )
		               ->where('s.TermID' , getLatestActiveTerm() )
                      ->get();
		return $data;	
    }
    
	public static function getStudents($term, $sec )
    {
        /* Provide a list of students whose currently enrolled per section. */
        return DB::table('ES_Registrations as r')                    
                    ->selectRaw(" 
                            r.StudentNo, 
                            dbo.fn_StudentName(r.StudentNo) AS FullName, 
                            dbo.fn_StudentGender(r.StudentNo) AS Gender ")
                    ->where('r.TermID', $term)                    
                    ->where('r.IsWithdrawal','0')
                    ->where('r.ClassSectionID',$sec)
                    ->whereRaw("r.ValidationDate IS NOT NULL ")
                    ->orderBy('FullName','asc')
                    ->get();
                        
    }
}

