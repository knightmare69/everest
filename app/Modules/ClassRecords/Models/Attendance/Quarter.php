<?php 
namespace App\Modules\ClassRecords\Models\Attendance;

use illuminate\Database\Eloquent\Model;

Class Quarter extends Model {

	public $table='ES_GS_GradingPeriods';
	protected $primaryKey ='PeriodID';

	protected $fillable  = array(
			'PeriodCode',
			'Description1',
			'Description2',
	);

	public $timestamps = false;
}