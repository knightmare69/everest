<?php 

namespace App\Modules\ClassRecords\Models\Conduct;
use illuminate\Database\Eloquent\Model;
use DB;

Class ConductComponent_model extends Model {

	protected $table='ES_GS_ConductComponent';
	public $primaryKey ='IndexID';

	protected $fillable  = array(
    	   
          'CoreValues'
          ,'BehaviorStatement1'
          ,'BehaviorStatement2'
	);

	public $timestamps = false;

          
}