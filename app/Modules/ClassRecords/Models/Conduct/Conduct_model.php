<?php 

namespace App\Modules\ClassRecords\Models\Conduct;
use illuminate\Database\Eloquent\Model;
use DB;

Class Conduct_model extends Model {

	protected $table='ES_GS_Conduct';
	public $primaryKey ='IndexID';

	protected $fillable  = array(    	   
          'ScheduleID'
          ,'PeriodID'
          ,'StudentNo'
          ,'ConductID'
          ,'Score'
          ,'DateCreated'
          ,'Createdby'
          ,'DateModified'
          ,'Modifiedby'
          ,'TermID'
          ,'SectionID'
          ,'IsAdviser'		
	);

	public $timestamps = false;

    public function scopeScore($query, $sched, $conduct, $period, $idno ){        
        $data = $query                   
            ->where('ScheduleID', $sched)
            ->where('ConductID', $conduct)
            ->where('PeriodID', $period)  
            ->where('StudentNo', $idno);            
		return $data;                
    }             
    
    public static function ConductGrade($contbl, $grade  ){            
        $components = DB::table('ES_GS_GradingSystem_Details')
		              ->select([ 'LetterGrade','Description','Remark'])                		                      
                	  ->whereRaw(" ('". $grade ."' BETWEEN CAST([Min] AS FLOAT) AND CAST([Max] AS FLOAT) ) ")
                      ->where('TemplateID' , $contbl )                      
                      ->first();
        
        return $components;               
    }
    
    public function getClassSections($term, $faculty)
	{
	   // TEMP : Filter by IBED & SHS Only
	    $data = DB::table('ES_ClassSections')
                    ->select(["SectionID",'SectionName','Limit','IsBlock','YearLevelID'])
                    ->where('TermID', $term)
                    ->where('AdviserID', $faculty )
                    ->where('IsDissolved','0')
                    ->whereIn('ProgramID',['1','21','29'])
                    ->get();
                    
		return $data; 
	}
    
    public function getClassSchedules($term, $faculty )
	{
	   
        // TEMP : Filter by IBED & SHS Only
        
        
	    $data = DB::table('ES_ClassSchedules as cs ')
                    ->leftJoin('ES_ClassSections AS se ','se.SectionID','=','cs.SectionID')
                    ->select(["ScheduleID","se.SectionName", DB::raw(" dbo.fn_SubjectCode(SubjectID) As SubjectCode ") ])
                    ->where('cs.TermID', $term)
                    ->where('cs.FacultyID', $faculty )
                    ->whereIn('se.ProgramID',['1','21','29'])
                    
                    ->get();
                    
		return $data; 
	}
    
    public static function getStudents($term, $sec, $period , $type ){    
        
        if ($type == 0 ){                    
        
            /* Provide a list of students whose currently enrolled per section. */
            $data = DB::table('ES_Registrations as r')                    
                        ->select(['r.StudentNo', DB::raw("dbo.fn_StudentName(r.StudentNo) AS FullName, 
                            dbo.fn_StudentGender(r.StudentNo) AS Gender,
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 1 AND PeriodID = '". $period . "') AS 'Conduct1',
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 2 AND PeriodID = '". $period . "') AS 'Conduct2',
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 3 AND PeriodID = '". $period . "') AS 'Conduct3',
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 4 AND PeriodID = '". $period . "') AS 'Conduct4',
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 5 AND PeriodID = '". $period . "') AS 'Conduct5',
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 6 AND PeriodID = '". $period . "') AS 'Conduct6',
                            (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND ConductID = 7 AND PeriodID = '". $period . "') AS 'Conduct7',

                            (SELECT TOP 1 Postedby FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND PeriodID = '". $period . "' ) AS 'Postedby',
                            (SELECT TOP 1 DatePosted FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND sectionid = r.ClassSectionID AND StudentNo=r.StudentNo AND PeriodID = '". $period . "' ) AS 'DatePosted' ")])
                        ->where('r.TermID', $term)                    
                        ->where('r.IsWithdrawal','0')
                        ->where('ClassSectionID',$sec)
                        ->whereRaw("r.ValidationDate IS NOT NULL ")
                        ->orderBy('FullName','asc')
                        ->get();
        
        } else{
            $data = DB::table('ES_Registrations as r')       
                    ->leftJoin('ES_RegistrationDetails AS d','r.RegID','=','d.RegID')             
                    ->select(['r.StudentNo', DB::raw("dbo.fn_StudentName(r.StudentNo) AS FullName, 
                        dbo.fn_StudentGender(r.StudentNo) AS Gender,
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 1 AND PeriodID = '". $period . "') AS 'Conduct1',
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 2 AND PeriodID = '". $period . "') AS 'Conduct2',
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 3 AND PeriodID = '". $period . "') AS 'Conduct3',
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 4 AND PeriodID = '". $period . "') AS 'Conduct4',
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 5 AND PeriodID = '". $period . "') AS 'Conduct5',
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 6 AND PeriodID = '". $period . "') AS 'Conduct6',
                        (SELECT TOP 1 Score FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND ConductID = 7 AND PeriodID = '". $period . "') AS 'Conduct7',
                        (SELECT TOP 1 Postedby FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND PeriodID = '". $period . "' ) AS 'Postedby',
                        (SELECT TOP 1 DatePosted FROM dbo.ES_GS_Conduct WHERE TermID = r.TermID AND ScheduleID = d.ScheduleID AND StudentNo=r.StudentNo AND PeriodID = '". $period . "' ) AS 'DatePosted' ")])
                    ->where('r.TermID', $term)                    
                    ->where('r.IsWithdrawal','0')
                    ->where('d.ScheduleID',$sec)
                    ->where('d.RegTagID','<','3')
                    ->whereRaw("r.ValidationDate IS NOT NULL ")
                    ->orderBy('FullName','asc')
                    ->get();
                        
        }        
		return $data;
                   
    }           
}