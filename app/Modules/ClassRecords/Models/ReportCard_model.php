<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class ReportCard_model extends Model {

	protected $table='ES_PermanentRecord_Master';
	public $primaryKey ='SummaryID';

	protected $fillable  = array(
	   'RegID'
      ,'CampusID'
      ,'TermID'
      ,'ProgClassID'
      ,'ProgramID'
      ,'ClassSectionID'
      ,'ClassSectionName'
      ,'YearLevelID'
      ,'YearLevelName'
      ,'LastSchoolID'
      ,'LastSchoolName'
      ,'StudentNo'
      ,'AcademicA_Average'
      ,'AcademicB_Average'
      ,'AcademicC_Average'
      ,'AcademicD_Average'
      ,'Final_Average'
      ,'Final_Remarks'
      ,'ConductA_Average'
      ,'ConductB_Average'
      ,'ConductC_Average'
      ,'ConductD_Average'
      ,'Final_Conduct'
      ,'TotalSubjects'
      ,'TotalUnitsEnrolled'
      ,'TotalUnitsEarned'
      ,'SchoolDays'
      ,'PresentDays'
      ,'Status'
      ,'StatusName'
      ,'LockedDate'
      ,'LockedBy'
      ,'LeftDate'
      ,'LeftPeriod'
      ,'RemarksA'
      ,'RemarksB'
      ,'RemarksC'
      ,'RemarksD'
      ,'LastPeriod'
      ,'TotalYearsInSchool'
      ,'AdvancedUnitsIn'
      ,'LacksUnitsIn'			
	);

	public $timestamps = false;
    
    
}