<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class ClassSection_model extends Model {

	protected $table='ES_ClassSections';
	public $primaryKey ='SectionID';

	protected $fillable  = array(
		'SectionName'
      ,'TermID'
      ,'CampusID'
      ,'CollegeID'
      ,'CurriculumID'
      ,'ProgramID'
      ,'AdviserID'
      ,'IsBlock'
      ,'RoomID'
      ,'Limit'
      ,'CreationDate'
      ,'YearLevelID'
      ,'CreatedBy'
      ,'IsEvening'
      ,'IsDissolved'
      ,'ShortName'
      ,'ClassTypeID'
      ,'SectionName_2'
      ,'Is2ndSection'
      ,'Elective'
		);

	public $timestamps = false;

    public static function scopeView($query, $term, $faculty){        
        $data = $query
            ->select(['*',DB::raw("dbo.fn_YearLevel_k12(YearLevelID, dbo.fn_ProgramClassCode(ProgramID)) AS YearLevel, dbo.fn_EmployeeName(AdviserID) As 'AdviserName' , dbo.fn_TotalStudentPerSection(SectionID, TermID)  AS 'TotalStudents' ")])
            ->where('TermID', $term)
            ->where('AdviserID',$faculty);
                            
		return $data;                
    }
    
    public static function scopeSectionName($query, $sec_id){        
        $data = $query            
            ->where('SectionID', $sec_id)
            ->pluck('SectionName');                            
		return $data;                
    }  
    
    public static function scopeYearLevelID($query, $sec_id){        
        $data = $query            
            ->where('SectionID', $sec_id)
            ->pluck('YearLevelID');                            
		return $data;                
    } 
    
	public static function scopeSubjectTeacher($query,$faculty){
	   $data = DB::select("SELECT  sec.*, 
								   dbo.fn_AcademicYearTerm(cs.TermID) AS 'AYTerm', 
								   dbo.fn_YearLevel_k12(YearLevelID, 
								   dbo.fn_ProgramClassCode(ProgramID)) AS YearLevel, 
								   dbo.fn_EmployeeName(AdviserID) As 'AdviserName' , 
								   dbo.fn_TotalStudentPerSection(cs.SectionID, cs.TermID)  AS 'TotalStudents'
							  FROM ES_ClassSchedules as cs
						INNER JOIN ES_ClassSections as sec ON cs.SectionID=sec.SectionID 
							 WHERE cs.FacultyID='".$faculty."'
							 ORDER BY AYTerm DESC");
		return $data;					 
	}
	
    public static function scopeSubjectView($query, $term, $faculty){        
        $data = DB::SELECT("SELECT  sec.*, 
									dbo.fn_YearLevel_k12(YearLevelID,
									dbo.fn_ProgramClassCode(ProgramID)) AS YearLevel,
									dbo.fn_EmployeeName(AdviserID) As 'AdviserName' , 
									dbo.fn_TotalStudentPerSection(cs.SectionID, cs.TermID)  AS 'TotalStudents'
							   FROM ES_ClassSchedules as cs
							  INNER JOIN ES_ClassSections as sec ON cs.SectionID=sec.SectionID 
							  WHERE cs.FacultyID='".$faculty."'
								AND cs.TermID='".$term."'");
                            
		return $data;                
    }
	
    public static function scopeClassAdviser($query, $faculty){        
        $data = $query
            ->selectRaw(" *, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm', dbo.fn_YearLevel_k12(YearLevelID, dbo.fn_ProgramClassCode(ProgramID)) AS YearLevel, dbo.fn_EmployeeName(AdviserID) As 'AdviserName' , dbo.fn_TotalStudentPerSection(SectionID, TermID)  AS 'TotalStudents' ")            
            ->where('AdviserID',$faculty)
            ->orderby('AYTerm','DESC');
            //->toSql();
                      
                      //var_dump($data);
                      //die();      
    return $data;                
    }

      

}