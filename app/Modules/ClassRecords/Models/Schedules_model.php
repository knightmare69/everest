<?php

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Schedules_model extends Model {

	protected $table='ES_ClassSchedules';
	public $primaryKey ='ScheduleID';

	protected $fillable  = array(
	       'ScheduleID'
          ,'TermID'
          ,'SectionID'
          ,'SubjectID'
          ,'FacultyID'
          ,'Limit'
          ,'IsSpecialClass'
          ,'Time1_Start'
          ,'Time1_End'
          ,'Days1'
          ,'Room1_ID'
          ,'Time2_Start'
          ,'Time2_End'
          ,'Days2'
          ,'Room2_ID'
          ,'Time3_Start'
          ,'Time3_End'
          ,'Days3'
          ,'Room3_ID'
          ,'Time4_Start'
          ,'Time4_End'
          ,'Days4'
          ,'Room4_ID'
          ,'Time5_Start'
          ,'Time5_End'
          ,'Days5'
          ,'Room5_ID'
          ,'Days1_EventID'
          ,'Days2_EventID'
          ,'Days3_EventID'
          ,'Days4_EventID'
          ,'Days5_EventID'
          ,'Sched_1'
          ,'Sched_2'
          ,'Sched_3'
          ,'Sched_4'
          ,'Sched_5'
          ,'LastModifiedBy'
          ,'LastModifiedDate'
          ,'Cntr'
          ,'GradesPostingDate'
          ,'OverideConflict'
          ,'GradesPostingDate2'
          ,'IsDissolved'
          ,'MidtermGradesPostingDate'
          ,'MidtermGradesPostingBy'
          ,'GradesPostingBy'
          ,'IsHonorarium'
          ,'FacultyID_2'
          ,'FacultyID_3'
          ,'FacultyID_4'
          ,'FacultyID_5'
          ,'DatePosted'
          ,'PostedBy'
          ,'RoomDatePosted'
          ,'RoomPostedBy'
          ,'FacultyDatePosted'
          ,'FacultyPostedBy'
          ,'LoadTypeID'
          ,'EffectivityDate'
          ,'MergeWith'
          ,'FacultyLoad'
          ,'ActualHrPerWeek'
          ,'ScheduleCode'
          ,'TransferedSubjectID'
          ,'SubmittedGradeDate'
          ,'SubmittedGradeTo'
          ,'IsCustomSched'
          ,'GradingSystemSettingsID'
          ,'FacultybyGender'
		);

	public $timestamps = false;

    public static function scopeView($query, $term, $faculty){

        $progams = getUserProgramAccess();         
        
        if( count($progams) == 0 ) {
            $progams =  "0";
        } else{
            $progams =  implode($progams,',');
        }
        
        //var_dump($progams);
        $data = $query
            ->select(['*', DB::raw("
                  dbo.fn_SectionName(SectionID) AS 'SectionName', dbo.fn_ClassSectionYearLevel(SectionID) AS 'YearLevel', dbo.fn_SubjectCode(SubjectID) AS 'SubjectCode', dbo.fn_SubjectTitle(SubjectID) AS 'SubjectTitle' , dbo.fn_SubjectAcademicUnits(SubjectID) AS 'Units',
                  dbo.fn_SubjectPolicy(ScheduleID) AS 'PolicyID', dbo.fn_SubjectPolicyName(ScheduleID) AS 'PolicyName', dbo.fn_TotalStudentPerSchedule(ScheduleID,TermID) AS 'TotalStudents' , dbo.fn_TotalStudentPerSchedulebySex(TermID, ScheduleID, 'M') AS 'TotalMale' , dbo.fn_TotalStudentPerSchedulebySex(TermID, ScheduleID, 'F') AS 'TotalFemale',
                  dbo.fn_SectionAdviser(SectionID) As TeacherName
            " )])
            ->where('TermID', $term);
          //->whereRaw("SectionID In (Select SectionID FROM ES_ClassSections WHERE ProgramID IN (". $progams ."))");

            if($faculty !=''){
                $data = $data->whereRaw("FacultyID = '".$faculty."' OR FacultyID_2 = '".$faculty."' ");
            }

		return $data;
    }

    public function Section($schedid){
		$exec = DB::select("SELECT s.* FROM ES_ClassSchedules as cs INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID WHERE cs.ScheduleID='".$schedid."'")[0];
		return $exec;
	}
	
    public static function Students($sched_id){
        $data = DB::select(DB::raw("EXEC dbo.ES_GetOfficialClassList @ScheduleID = '".$sched_id."' "));
		return $data;
    }

    public static function StudentsWithGrade($sched_id){
        $data = DB::select(DB::raw("SELECT *, dbo.fn_StudentName(StudentNo) AS StudentName, dbo.fn_YearLevel_k12(YearLevelID, ProgClassID) AS YearLevel FROM es_permanentrecord_details WHERE ScheduleID = '".$sched_id."' "));
		return $data;
    }

	public static function get_academicyearterm($facultyid)
	{
	    $data = DB::select("SELECT TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm'  FROM dbo.ES_ClassSchedules WHERE FacultyID = '".$facultyid."' GROUP BY TermID ORDER BY AYTerm DESC");
        //DB::table('ES_ClassSchedules')->select(" TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm' ")->where('FacultyID',$facultyid)->groupBy('TermID')->orderBy('AYTerm','DESC')->get();
		return $data;
	}

    public static function scopeInfo($query, $sched_id){
        $data = $query
            ->select(['*', DB::raw("
                  dbo.fn_SectionName(SectionID) AS 'SectionName', dbo.fn_ClassSectionYearLevel(SectionID) AS 'YearLevel', dbo.fn_SubjectCode(SubjectID) AS 'SubjectCode', dbo.fn_SubjectTitle(SubjectID) AS 'SubjectTitle' , dbo.fn_SubjectAcademicUnits(SubjectID) AS 'Units',
                  dbo.fn_SubjectPolicy(ScheduleID) AS 'PolicyID', dbo.fn_SubjectPolicyName(ScheduleID) AS 'PolicyName', dbo.fn_TotalStudentPerSchedule(ScheduleID,TermID) AS 'TotalStudents' , dbo.fn_TotalStudentPerSchedulebySex(TermID, ScheduleID, 'M') AS 'TotalMale' , dbo.fn_TotalStudentPerSchedulebySex(TermID, ScheduleID, 'F') AS 'TotalFemale'
            " )])
            ->where('ScheduleID', $sched_id)
            ->first();

		return $data;
    }
}
