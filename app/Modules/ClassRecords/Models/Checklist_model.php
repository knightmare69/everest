<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Checklist_model extends Model {

	protected $table='ES_GS_Checklist_Grades';
	public $primaryKey ='IndexID';

	protected $fillable  = array(
	   'InputDate'
      ,'SectionID'
      ,'PeriodID'
      ,'StudentNo'
      ,'ChecklistID'
      ,'Grade'
      ,'TeacherID'
      ,'Modifiedby'
      ,'DateModified'
      ,'Correction'
      ,'Correctionby' 
      ,'DateCorrection'
      ,'DatePosted'
      ,'Postedby'
      ,'NoteID' 
	);

	public $timestamps = false;

    public static function scopeView($query, $term, $faculty){        
        $data = $query
            ->select(['*',DB::raw("dbo.fn_YearLevel_with_K12(YearLevelID, dbo.fn_ProgramClassCode(ProgramID)) AS YearLevel, dbo.fn_EmployeeName(AdviserID) As 'AdviserName' , dbo.fn_TotalStudentPerSection(SectionID, TermID)  AS 'TotalStudents' ")])
            ->where('TermID', $term)
            ->where('AdviserID',$faculty);
                            
		return $data;                
    }
    
    public static function scopeLastDateEncoded($query, $secid){        
        return $query
            ->select(['InputDate'])          
            ->where('SectionID', $secid)
            ->orderBy('InputDate','desc');
            
    }
    
    public static function scopeGrade($query, $secid, $stud_id, $period, $chkid ){
                        
        return $query
            ->select(['Grade'])          
            ->where('SectionID', $secid)
            ->where('StudentNo', $stud_id)
            ->where('PeriodID', $period)
            ->where('ChecklistID', $chkid);
    }
    

    public static function Students($sec_id, $period ){                        
        $data = DB::table('ES_Registrations as r')
		              ->select(['*',DB::raw(" dbo.fn_StudentLName(StudentNo) AS LName, dbo.fn_StudentGender(StudentNo) As Sex , 
                       dbo.fn_StudentName(r.StudentNo) As StudentName 
                       ")])
                      ->where('ClassSectionID',$sec_id)
                      ->where('IsWithdrawal',0)
                      ->whereRaw('ValidationDate is not null ')
                      ->orderBy('Sex','desc')          
                      ->orderBy('LName','asc')      	                      
                      ->get();                
		return $data;               
    }
    
    public static function DomainChecklist($template, $period, $sec_id ){
        $facultyid=getUserFacultyID();
        $whr = [];
        
        switch($period){
            case '1': $whr['P1'] = 1; break;
            case '2': $whr['P2'] = 1; break;
            case '3': $whr['P3'] = 1; break;
            case '4': $whr['P4'] = 1; break;                        
            default:
                $whr['P1'] = 1; 
            break;
        }
                
        $data = DB::table('es_gs_checklist as r')
                      ->leftJoin('ES_GS_Checklist_Class as c','c.ChkClassID','=','r.ClassID')
                      ->leftJoin('es_gs_checklist_subclass as s','s.SubID','=','r.SubClassID')
                      ->leftJoin('es_gs_checklist_criteria as t','t.ChkCriteriaID','=','r.CriteriaID')
		              ->select(['r.*','c.ClassName','s.SubClass','t.Criteria'])
                      ->where('TemplateID',$template)
                      ->whereraw("c.subjectid in (SELECT SubjectID FROM dbo.ES_ClassSchedules WHERE SectionID='".$sec_id."' AND FacultyID='".$facultyid."')")    
                      ->where($whr)                                        
                      ->orderBy('ClassID','asc')     
                      ->orderBy('SubClassID','asc')      
                      ->orderBy('Seq','asc')                                                    	                                                                       	                      
                      ->get();                
		return $data;               
    }
    
	public static function get_academicyearterm($facultyid)
  {
      //$data = DB::select("SELECT TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm'  FROM dbo.ES_ClassSections WHERE AdviserID = '".$facultyid."' GROUP BY TermID ORDER BY AYTerm DESC");
        $data= DB::select("SELECT TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm'  FROM dbo.ES_ClassSchedules WHERE FacultyID = '".$facultyid."' GROUP BY TermID ORDER BY AYTerm DESC");
    return $data; 
  }

  public static function loadschedules($term, $faculty){

        $progams = getUserProgramAccess();         
        
        if( count($progams) == 0 ) {
            $progams =  "0";
        } else{
            $progams =  implode($progams,',');
        }
        
        //var_dump($progams);
        $data = DB::table('ES_ClassSchedules as cs')
            ->select([DB::raw("
                  DISTINCT cs.SectionID,dbo.fn_SectionName(SectionID) AS 'SectionName', dbo.fn_ClassSectionYearLevel(SectionID) AS 'YearLevel',
                  dbo.fn_SectionAdviser(SectionID) As AdviserName
            " )])
            ->where('TermID', $term)
            ->whereRaw("5=(SELECT ProgramID FROM dbo.ES_ClassSections WHERE SectionID=cs.sectionid)")
            ->whereRaw("(FacultyID = '".$faculty."' OR FacultyID_2 = '".$faculty."' )");
            //->toSql();
          //->whereRaw("SectionID In (Select SectionID FROM ES_ClassSections WHERE ProgramID IN (". $progams ."))");

            //die($data);

    return $data;
    }

}