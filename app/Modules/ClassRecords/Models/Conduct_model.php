<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Conduct_model extends Model {

	protected $table='ES_GS_Conduct';
	public $primaryKey ='IndexID';

	protected $fillable  = array(
    	   
          'ScheduleID'
          ,'PeriodID'
          ,'StudentNo'
          ,'ConductID'
          ,'Score'
          ,'DateCreated'
          ,'Createdby'
          ,'DateModified'
          ,'Modifiedby'
          ,'TermID'		
	);

	public $timestamps = false;

    public function scopeScore($query, $sched, $conduct, $period, $idno ){        
        $data = $query                   
            ->where('ScheduleID', $sched)
            ->where('ConductID', $conduct)
            ->where('PeriodID', $period)  
            ->where('StudentNo', $idno);            
		return $data;                
    }
    
    public static function ConductGrade($contbl, $grade  ){    
        
        $components = DB::table('ES_GS_GradingSystem_Details')
		              ->select([ 'LetterGrade','Description','Remark'])                		                      
                	  ->whereRaw(" ('". $grade ."' BETWEEN CAST([Min] AS FLOAT) AND CAST([Max] AS FLOAT) ) ")
                      ->where('TemplateID' , $contbl )                      
                      ->first();
        
        return $components;               
    }
    
    
}