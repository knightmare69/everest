<?php

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class GradeEvent_model extends Model {

	protected $table='ES_GS_GradeEvents';
	public $primaryKey ='EventID';

	protected $fillable  = array(
	   'ScheduleID'
      ,'ClubID'
      ,'ComponentID'
      ,'GradeCompCode'
      ,'TermID'
      ,'GradingPeriod'
      ,'TeacherID'
      ,'SortOrder'
      ,'TotalItems'
      ,'EventName'
      ,'EventDate'
      ,'EventDescription'
      ,'Gender'
      ,'LastModifiedBy'
      ,'LastModifiedDate'
	);

	public $timestamps = false;

    public static function scopeView($query, $sched){
        $data = $query
            ->select(['*',DB::raw("dbo.fn_PolicyName(ComponentID) As Policy, dbo.fn_PeriodName(PeriodID) AS 'Period' " )])
            ->where('ScheduleID', $sched)
            ->where('PeriodID','>','0')
            ->whereRaw('PeriodID IS NOT NULL');
		return $data;
    }
    
    public static function scopeGetInfo($query, $event){
        return $query->where('EventID', $event);		
    }
}