<?php 

namespace App\Modules\ClassRecords\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Formation_model extends Model {

	protected $table='ES_GS_PolicySetup';
	public $primaryKey ='IndexID';

	protected $fillable  = array(
    	'ScheduleID',
    	'SubjectID',
    	'PolicyID',
    	'PeriodID',
        'DateCreated',
        'Createdby'			
	);

	public $timestamps = false;

	public static function get_academicyearterm($facultyid)
	{
	    $data = DB::select("SELECT TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm' FROM dbo.ES_ClassSchedules WHERE FacultyID = '".$facultyid."' GROUP By TermID ORDER BY AYTerm DESC");
        //DB::table('ES_ClassSchedules')->select(" TermID, dbo.fn_AcademicYearTerm(TermID) AS 'AYTerm' ")->where('FacultyID',$facultyid)->groupBy('TermID')->orderBy('AYTerm','DESC')->get();
		return $data; 
	}
    
    public static function scopeView($query, $sched){        
        $data = $query
            ->select(['*',DB::raw("dbo.fn_PolicyName(PolicyID) As Policy, dbo.fn_PeriodName(PeriodID) AS 'Period' " )])
            ->where('ScheduleID', $sched)
            ->where('PeriodID','>','0')
            ->whereRaw('PeriodID IS NOT NULL');      
		return $data;                
    }
    
    public static function get_schedules($term){        
        $progIds = getUserProgramAccess();
        $data = DB::select("SELECT *, dbo.fn_classpolicy(ScheduleID) As Total FROM vw_MainClassSchedules WHERE TermID = '".$term."' AND ProgramID IN (0,". implode($progIds,',') .")");                
		return $data;                
    }

}