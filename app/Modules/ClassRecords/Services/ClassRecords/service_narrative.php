<?php

namespace App\Modules\ClassRecords\Services\ClassRecords;
use Validator;
use DB;

class service_narrative
{
   function getDropdown($empid){
	 $out     = array('term'=>array(),'section'=>array());  
	 $term    = array();
	 $section = array();
	 $qry     = "SELECT DISTINCT 
						cs.AdviserID
					   ,cs.TermID
					   ,t.AcademicYear
					   ,t.SchoolTerm
					   ,cs.CampusID
					   ,(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN sc.ScheduleID ELSE '' END) as ScheduleID
					   ,sub.SubjectID
					   ,sub.SubjectCode
					   ,sub.SubjectTitle
					   ,cs.SectionID
					   ,cs.SectionName
					   ,cs.YearLevelID			
					   ,cs.ProgramID
					   ,y.YearLevelCode
					   ,y.YearLevelName	
					   ,t.Active_OnlineEnrolment
				   FROM ESv2_Users as u 
			 INNER JOIN ES_ClassSections as cs ON u.FacultyID=(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN u.FacultyID ELSE cs.AdviserID END)
			 INNER JOIN ES_Registrations as r ON cs.SectionID=r.ClassSectionID
			 INNER JOIN ES_AYTerm as t ON r.TermID=t.TermID AND cs.TermID=t.TermID
			 INNER JOIN ESv2_YearLevel as y ON cs.ProgramID=y.ProgID AND cs.YearLevelID=y.YLID_OldValue
			  LEFT JOIN ES_ClassSchedules as sc ON cs.SectionID=(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN sc.SectionID ELSE 0 END) AND sc.FacultyID=(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN u.FacultyID ELSE sc.FacultyID END)
			  LEFT JOIN ES_Subjects as sub ON sc.SubjectID=sub.SubjectID
                  WHERE u.UserIDX='".$empid."' AND (cs.AdviserID=u.FacultyID OR sc.FacultyID=u.FacultyID)
			   ORDER BY t.AcademicYear DESC";   
	 $exec    = DB::select($qry);	   
	 foreach($exec as $r){
		if(!array_key_exists($r->TermID,$term)){
			$term[$r->TermID] = array(
			                           'name'     => $r->AcademicYear.' '.$r->SchoolTerm
			                          ,'termid'   => $r->TermID
									  ,'isactive' => $r->Active_OnlineEnrolment
					                 );
		}
		
		if(!array_key_exists($r->TermID,$section) && ($r->ScheduleID=='' || $r->ScheduleID=='0')){
			$section[$r->SectionID] = array(
			                           'name'       => $r->SectionName
			                          ,'sectionid'  => $r->SectionID
									  ,'scheduleid' => ''
			                          ,'termid'     => $r->TermID
					                 );
		}else if(!array_key_exists((($r->ScheduleID!='' && $r->ScheduleID!='0')?('s'.$r->ScheduleID):''),$section)){
			$sec_id  = (($r->ScheduleID!='' && $r->ScheduleID!='0')?('s'.$r->ScheduleID):'');
			$section[$sec_id] = array(
			                           'name'       => $r->SectionName.' - '. $r->SubjectCode
			                          ,'sectionid'  => $r->SectionID
									  ,'scheduleid' => (($r->ScheduleID!='' && $r->ScheduleID!='0')?('s'.$r->ScheduleID):'')
			                          ,'termid'     => $r->TermID
					                 );
		}
		$out = array('term'=>$term,'section'=>$section);
	 }
	 return $out;
   }
   
   function LoadList($empid,$sectionid,$period=1){
	  $issched   = ((strpos($sectionid,'s')!==false)?'sc.ScheduleID':'cs.SectionID');
	  $column    = ((strpos($sectionid,'s')!==false)?'n.ScheduleID':'n.SectionID');
	  $sectionid = str_replace('s','',$sectionid);
	  $qry  = "SELECT cs.AdviserID
					,cs.TermID
					,cs.CampusID
					,cs.SectionID
					,cs.SectionName
					,(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN sc.ScheduleID ELSE '' END) as ScheduleID
					,sub.SubjectID
					,sub.SubjectCode
					,sub.SubjectTitle
					,r.YearLevelID
					,r.ProgID
					,r.RegID
					,s.StudentNo
					,s.Fullname
					,s.LastName
					,s.FirstName
					,s.Middlename
					,s.MiddleInitial
					,s.Gender
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=2  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct1
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=3  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct2
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=4  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct3
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=5  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct4
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=6  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct5
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=7  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct6
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=8  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct7
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=9  AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct8
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=10 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct9
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=11 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct10
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=12 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct11
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=13 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct12
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=14 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct13
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=15 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct14
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=16 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct15
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=17 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct16
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=18 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct17
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=19 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct18
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=20 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct19
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=21 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct20
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=22 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct21
                    ,(SELECT TOP 1 Score FROM ES_GS_Conduct WHERE ConductID=23 AND TermID=cs.TermID AND PeriodID=".$period." AND StudentNo=s.StudentNo AND (ScheduleID=ISNULL(sc.ScheduleID,0) OR SectionID=cs.SectionID)) as Conduct22
                    ,n.NarrativeA".$period."
                    ,n.NarrativeB".$period."
                    ,n.NarrativeC".$period."
                    ,n.NarrativeD".$period."
                    ,n.DatePosted".$period."
                    ,n.PostedBy".$period."					
				FROM ESv2_Users as u 
		  INNER JOIN ES_ClassSections as cs ON u.FacultyID=(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN u.FacultyID ELSE cs.AdviserID END)
		  INNER JOIN ES_Registrations as r ON cs.SectionID=r.ClassSectionID AND r.ValidationDate IS NOT NULL
		  INNER JOIN ES_Students as s ON r.StudentNo=s.StudentNo
		   LEFT JOIN ES_ClassSchedules as sc ON cs.SectionID=(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN sc.SectionID ELSE 0 END) AND sc.FacultyID=(CASE WHEN cs.ProgramID=5 AND cs.YearLevelID=1 THEN u.FacultyID ELSE sc.FacultyID END)
		   LEFT JOIN ES_Subjects as sub ON sc.SubjectID=sub.SubjectID
		   LEFT JOIN ESv2_Narrative as n ON cs.TermID=n.TermID AND r.RegID=n.RegID AND s.StudentNo=n.StudentNo AND ".$column."='".$sectionid."'
			   WHERE u.UserIDX='".$empid."' AND ".$issched."='".$sectionid."'
			ORDER BY s.Fullname"; 
	  
	  $exec = DB::select($qry);
      return $exec;	  
   }
   
   function UpdateOrSave($data=false){
	  $narrate  = array('0'=>'A','1'=>'B','2'=>'C','3'=>'D');
	  $result   = false;
	  $period   = $data['period'];
	  $section  = decode($data['section']);
	  $issched  = (strpos($section,'s')!==false);
	  $adviser  = (($issched)?0:1);
	  $schedule = (($issched)?str_replace('s','',$section):0);
	  $section  = (($issched)?0:$section);
	  $update   = array();
	  $con_data = array();
	  if(array_key_exists('narrate',$data) && is_array($data['narrate'])){
	    foreach($data['narrate'] as $k=>$v){
		  $update['Narrative'.$narrate[$k].$period] = $v;
		}	
	  }
	  
	  if(array_key_exists('conduct',$data) && is_array($data['conduct'])){
		foreach($data['conduct'] as $k=>$v){
			$condid   = $k+2; 
		    $qry      = "DELETE FROM ES_GS_Conduct WHERE TermID='".$data['term']."' AND StudentNo='".$data['studno']."' AND SectionID='".$section."' AND ConductID='".$condid."' AND PeriodID='".$period."'
			             INSERT INTO ES_GS_Conduct(TermID,ScheduleID,SectionID,PeriodID,ConductID,StudentNo,Score,IsAdviser) VALUES ('".$data['term']."','".$schedule."','".$section."','".$period."','".$condid."','".$data['studno']."','".$v."','".$adviser."')";
		  //die($qry);
			$exec_con = DB::statement($qry);
		}	
	  }
	  
	  $create = DB::statement("INSERT INTO ESv2_Narrative(TermID,ScheduleID,SectionID,RegID,StudentNo) 
	                           SELECT TermID,'".$schedule."','".$section."',RegID,StudentNo FROM ES_Registrations WHERE TermID='".$data['term']."' AND StudentNo='".$data['studno']."' AND RegID NOT IN (SELECT RegID FROM ESv2_Narrative)");
	  if(count($update)>0){
	    $result = DB::table('ESv2_Narrative')->where(array('TermID'=>$data['term'],'StudentNo'=>$data['studno'],'SectionID'=>$section,'ScheduleID'=>$schedule))->update($update);
	  }
	  
	  if(array_key_exists('option',$data) && $data['option']==0){
	    $result = DB::statement("UPDATE ESv2_Narrative SET DatePosted".$period."=NULL,PostedBy".$period."='' WHERE TermID='".$data['term']."' AND ScheduleID='".$schedule."' AND SectionID='".$section."' AND StudentNo='".$data['studno']."'");
	  }else if(array_key_exists('option',$data) && $data['option']==1){
	    $result = DB::statement("UPDATE ESv2_Narrative SET DatePosted".$period."=GETDATE(),PostedBy".$period."='".getUserID()."' WHERE TermID='".$data['term']."' AND ScheduleID='".$schedule."' AND SectionID='".$section."' AND StudentNo='".$data['studno']."'");
	  }
	  return $result;
   }
}
?>