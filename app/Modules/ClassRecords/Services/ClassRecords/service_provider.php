<?php

namespace App\Modules\ClassRecords\Services\ClassRecords;
use Validator;

class service_provider
{
    public function __construct(){

    }

    public function validateDefault($post)
    {

        $validator = validator::make(
			['period'		=>  getObjectValue($post,'period')],
			['period'		=> 'required']
		);

        return $validator;
    }

    public function validate_grade($p){

        $validator = validator::make(
			['idno' =>  getObjectValue($p,'idno'), 'activity' =>  getObjectValue($p,'activity')],
			['idno' => 'required', 'activity' => 'required']
		);

        return $validator;
    }

    public function isValid($post, $action = 'save')
    {
        $result = ['error' => false, 'message' => ''];
        switch($action){
            case 'save':

                $validate = $this->validateDefault($post);
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }
            break;
            case 'save-grade':

                $validate = $this->validate_grade($post);
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }

            break;

            case 'erase-grade':
                $validate = $this->validate_grade($post);
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }
            break;
            
            case 'save-conduct':
                
                $validate = validator::make(
        			['conduct' =>  getObjectValue($post,'conduct'), 
                     'idno' =>  getObjectValue($post,'idno')
                    ],
        			['idno' => 'required', 
                    'conduct' => 'required']
        		);
        
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }
                
            break;
            
            case 'post': 
            break;

        }

        return $result;
    }

    public function postEvents($post)
    {

        $edate = getObjectValue($post, 'date');

        $return = array(
            'ScheduleID' => (getObjectValue($post, 'sched')),
    	    'ComponentID' => (getObjectValue($post, 'component')),
 	        'TermID' => decode(getObjectValue($post, 'term')),
            'GradingPeriod' => getObjectValue($post, 'period'),
            'TotalItems' => getObjectValue($post, 'items'),
            'EventName' => getObjectValue($post, 'name'),
            'EventDate' => ( $edate == "" ? NULL : $edate),
            'EventDescription' => getObjectValue($post, 'desc'),
            'GradeCompCode' => '',
            'SortOrder' => 0,
            'Gender' => getObjectValue($post, 'gender'),
            'TeacherID' => getUserID(),
            'LastModifiedBy' => getUserID(),
            'LastModifiedDate' => date('Y-m-d H:i:s')
        );

        return $return;
    }

    public function postScore($post)
    {

        $return = array(
            'EventID' => getObjectValue($post, 'activity'),
            'StudentNo' => decode(getObjectValue($post, 'idno')),
 	        'TermID' => decode(getObjectValue($post, 'term')),
            'GradingPeriod' => getObjectValue($post, 'period'),
            'TransmutedGrade' => '',
            'LetterGrade'=> '',
            'InputDate' => date('Y-m-d H:i:s'),
            'RawScore' => getObjectValue($post, 'score'),
            'IsAbsent' => getObjectValue($post, 'absent'),
            'LastModifiedBy' => getUserID(),
            'LastModifiedDate' => date('Y-m-d H:i:s')
        );

        return $return;
    }
    
    public function postConduct($post)
    {

        $return = array(            
            'ScheduleID' => getObjectValue($post, 'sched'),
            'StudentNo' => decode(getObjectValue($post, 'idno')),
 	        'TermID' => decode(getObjectValue($post, 'term')),
            'PeriodID' => getObjectValue($post, 'period'),
            'ConductID' => getObjectValue($post, 'conduct'),
            'Score' => getObjectValue($post, 'score'),            
            'ModifiedBy' => getUserID(),
            'DateModified' => date('Y-m-d H:i:s')
        );

        return $return;
    }
    
     public function postReportCard($post)
    {

        $return = array(            
        
            'TermID' => decode(getObjectValue($post, 'term')),
            'StudentNo' => decode(getObjectValue($post, 'idno')),
            'ScheduleID' => getObjectValue($post, 'sched'),             	        
            'PeriodID' => getObjectValue($post, 'period'),
            'FinalGrade' => getObjectValue($post, 'finalgrade'),
            'Remarks' => getObjectValue($post, 'remarks'),
            'TeacherID' => getUserID(),            
            'DatePosted' => date('Y-m-d H:i:s'),
            'ModifiedBy' => getUserID(),
            'DateModified' => date('Y-m-d H:i:s')
        );

        return $return;
    }
    

}
