<?php

namespace App\Modules\ClassRecords\Services;

use App\Modules\ClassRecords\Models\ReportCard_model As reportcard_model;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As reportcardetails_model;
use App\Modules\Registrar\Models\Subjects\Subjects as subject_model;

use Validator;
use DB;

class StudentClubServices
{
    public function __construct()
    {
        // $this->validation = new Validation();
    }
    
    public function register_grade($regid, $sched, $period, $grade, $final){
         
         $reg = New \App\Modules\Enrollment\Models\Registration; 
         $schedules = New \App\Modules\ClassRecords\Models\Schedules_model;
         
         $model = new reportcard_model();
         $model2 = new reportcardetails_model();
         $subjmodel = new subject_model();                             
                             
         $stud_reg = $reg->where('RegID',$regid)->first();
         $stud_sched = $schedules->where('ScheduleID',$sched)->first();
         $subj = $subjmodel->where('SubjectID',$stud_sched->SubjectID)->first();
         
         $idx = 0;         
                
         $whre = array(
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'RegID' => $regid,     
            'CampusID' => $stud_reg->CampusID,                            
         );
         
         $clsID = $reg->getProgClassID($stud_reg->ProgID)[0]->ClassID;
         
         $data = array(
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'RegID' => $regid,     
            'YearLevelID' => $stud_reg->YearLevelID,
            'YearLevelName'=> $reg->getYearLevelName($stud_reg->YearLevelID, $clsID)[0]->YearLevel,
            'ProgramID' => $stud_reg->ProgID,
            'ClassSectionID' => $stud_reg->ClassSectionID,
            'ClassSectionName'=> $reg->getClassSectionName($stud_reg->ClassSectionID)[0]->SectionName,
            'ProgClassID'=> $clsID,                         
         );
                
         $idx = $model->updateOrCreate($whre,$data);                        
         $idx = $idx['SummaryID'];                                                             
         
         $whre = array(
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'RegID' => $regid,     
            'CampusID' => $stud_reg->CampusID,
            //'SummaryID' => $idx,   
            'ScheduleID' => $sched,                
            'YearLevelID' => $stud_reg->YearLevelID         
         );
                                                                                                                                             
         $data = array(       
            'SummaryID' => $idx,
            'RegID' => $regid,
            'YearLevelID' => $stud_reg->YearLevelID,
            'ProgramID'=> $stud_reg->ProgID,                                            
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,            
            'ScheduleID' => $sched,                                         	                                                                                                                                                    
            'SubjectID' => $subj->SubjectID,
            'SubjectCode' => $subj->SubjectCode,
            'SubjectTitle' => $subj->SubjectTitle,
            'SubjectCreditUnits' => $subj->CreditUnits,
            'IsClubbing' => $subj->IsClubOrganization,
            'IsNonAcademic' => $subj->IsNonAcademic,
            'ParentSubjectID' => ( $subj->SubjParentID == NULL ? 0 : $subj->SubjParentID ),
         );
                
        switch( $period ){
            case '1': 
            case '11':
                $data['AverageA'] =  trimmed($final) ;
                $data['PeriodGrade1'] =  trimmed($grade) ;
            break;
            case '2': 
            case '12':
                $data['AverageB'] =  trimmed($final) ;
                $data['PeriodGrade2'] =  trimmed($grade) ;
            break;
            case '3': 
            
                $data['AverageC'] =  trimmed($final) ;
                $data['PeriodGrade3'] =  trimmed($grade) ;
            break;
            
            case '4': 
            
                $data['AverageD'] =  trimmed($final) ;
                $data['PeriodGrade4'] =  trimmed($grade) ;
            break;                        
        }
                                
        return $model2->updateOrCreate($whre,$data);                                        
                                                                           
    }
    
    public function post_grade($post){
        
         $model = new reportcardetails_model();                                                                         
         
         $period = decode(getObjectValue($post, 'period'));
         $term = decode(getObjectValue($post, 'term'));
                                 
         foreach($post['stud'] As $s){
            
            $reg_id = decode($s['reg']);
            $stud = decode($s['idno']);
            $sched = decode($s['sched']);
                           
            $whre = array(
                'TermID' => $term,
                'StudentNo' => $stud,
                'RegID' => $reg_id,     
                'ScheduleID' => $sched,                                    	                                    
            );
            
            $data = array( );
            
            switch( $period  ){
                case '1':
                case '11': 
                    $data['PostedBy1'] =  getUserID();
                    $data['DatePosted1'] =  date('Y-m-d H:i:s');
                break;
                case '2':
                case '12':
                    $data['PostedBy2'] =  getUserID();
                    $data['DatePosted2'] =  date('Y-m-d H:i:s');
                break;
                case '3':
                    $data['PostedBy3'] =  getUserID();
                    $data['DatePosted3'] =  date('Y-m-d H:i:s');
                break;
                case '4':
                    $data['PostedBy4'] =  getUserID();
                    $data['DatePosted4'] =  date('Y-m-d H:i:s');
                break;
                case '14':
                    $data['PostedBy'] =  getUserID();
                    $data['DatePosted'] =  date('Y-m-d H:i:s');
                break;
                
            }
                                    
            $model->where($whre)->update($data);  
            SystemLog('Student Clubs','','Post Grade','Post Grades', $whre ,'record successfully posted!' );
			
                                                                                                                                                                                                           
            $parent = $model->where($whre)->first();
            
            if ($parent->ParentSubjectID > 0 ){
                /*Create Parent Entry*/
                $final = '';
                $posted_child = true;
                
                $child = $model->where([
                    'TermID' => $term ,
                    'StudentNo' => $stud ,
                    'RegID' => $reg_id,     
                    'ParentSubjectID' => $parent->ParentSubjectID ,  
                ])->get();
                
                foreach($child as $r){
                    
                    switch( $period ){
                        case '1':
                        case '11': 
                            $final +=  ($r->SubjectCreditUnits * $r->AverageA);
                            if($r->PostedBy1 == ''){
                                $posted_child = false;
                            } 
                        break;
                        case '2':
                        case '12':
                            $final += ($r->SubjectCreditUnits * $r->AverageB);
                            if($r->PostedBy2 == ''){
                                $posted_child = false;
                            }
                            
                        break;
                        case '3':
                            $final += ($r->SubjectCreditUnits * $r->AverageC);
                            if($r->PostedBy3 == ''){
                                $posted_child = false;
                            }
                        break;
                        case '4':
                            $final += ($r->SubjectCreditUnits * $r->AverageD);
                            if($r->PostedBy4 == ''){
                                $posted_child = false;
                            }
                        break;
                        
                    }                                                        
                }
                
                $parentSchedID = $this->dbcon->get_parentScheduleID($reg_id, $parent->ParentSubjectID );
                
                $this->save_grade($reg_id, $parentSchedID , $period , '', $final);
                
                if($posted_child){
                
                    $data = array( );
                
                    switch( $period  ){
                        case '1':
                        case '11': 
                            $data['PostedBy1'] =  getUserID();
                            $data['DatePosted1'] =  date('Y-m-d H:i:s');
                        break;
                        case '2':
                        case '12':
                            $data['PostedBy2'] =  getUserID();
                            $data['DatePosted2'] =  date('Y-m-d H:i:s');
                        break;
                        case '3':
                            $data['PostedBy3'] =  getUserID();
                            $data['DatePosted3'] =  date('Y-m-d H:i:s');
                        break;
                        case '4':
                            $data['PostedBy4'] =  getUserID();
                            $data['DatePosted4'] =  date('Y-m-d H:i:s');
                        break;
                        
                    }
                                            
                    $model->where([
                    'TermID' => $term ,
                    'StudentNo' => $stud ,
                    'RegID' => $reg_id,     
                    'ScheduleID' => $parentSchedID ,  
                ])->update($data);  
                }
                                            
            }                                                                                                                       
        }                    
                    
                    
    }
    
    public function isValid($post, $action = 'update')
    {
        if ($action == 'update') {
            $validate = $this->validation->validateFaculty($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else if($action == 'search-faculty') {
            $validate = $this->validation->validateSearchFaculty($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }
        return ['error' => false, 'message' => ''];
    }

    public function academic_year()
    {
        
        $get = DB::table('ES_AYTerm')->selectRaw("TermID, AcademicYear + ' ' + SchoolTerm AS AYTerm ")
            ->where('SchoolTerm','School Year')
            ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function getEventID($term, $period, $clubid, $comp )
    {
        $eventID = '';
        
        if($clubid !=0 ){
                    
            $eventID = DB::table('ES_GS_GradeEvents')
                ->select('EventID')
                ->where(['TermID'=> $term, 'ClubID' => $clubid, 'GradingPeriod' => $period, 'ComponentID' => $comp  ])
                ->pluck('EventID');
            
            if($eventID ==''){
                
                $data = array(
                    'TermID'=> $term, 
                    'ClubID' => $clubid, 
                    'GradingPeriod' => $period, 
                    'ComponentID' => $comp,
                    'TeacherID' => getUserID(),
                    'TotalItems' => '100',
                    'LastModifiedBy' => getUserID(),
                    'LastModifiedDate' => systemDate(),
                    'GradeCompCode' =>'',
                    'SortOrder'=>0,
                );
                
                $eventID = DB::table('ES_GS_GradeEvents')->insertGetId($data);                               
            }        
        }    
                            
        return $eventID;
    }
    
     public function getEventScore($term, $period, $event, $student )
    {
                    
        $score = DB::table('ES_GS_RawScores')
            ->select('RawScore')
            ->where(['TermID'=> $term, 'EventID' => decode($event), 'GradingPeriod' => $period, 'StudentNo' => $student  ])
            ->pluck('RawScore');
                            
        return $score;
    }
    
    
}
