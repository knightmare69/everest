<?php

namespace App\Modules\ClassRecords\Services;

use Validator;
use DB;

class ClassRecReviewServiceProvider
{
    public function __construct()
    {
        // $this->validation = new Validation();
    }

    public function isValid($post, $action = 'update')
    {
        if ($action == 'update') {
            $validate = $this->validation->validateFaculty($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else if($action == 'search-faculty') {
            $validate = $this->validation->validateSearchFaculty($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }
        return ['error' => false, 'message' => ''];
    }

    public function academic_year()
    {
        /*
        $sched = DB::table('ES_AYTerm')->select('TermID')->where('Hidden', 0)->get();
        $terms = array();

        foreach ($sched as $_this) {
            $terms[] = $_this->TermID;
        }
        */
        
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')->where('Hidden', 0)->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function campus()
    {
        $get = DB::table('ES_Campus')->select('CampusID', 'Acronym', 'ShortName')->get();

        return $get;
    }
}
