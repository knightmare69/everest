<?php

namespace App\Modules\ClassRecords\Services\Formations;
use Validator;

class service_provider
{
    public function __construct(){
 
    }

    public function validateDefault($post)
    {
        
        $validator = validator::make(
			['policy'		=>  getObjectValue($post,'policy')],			
			['policy'		=> 'required']
		);
		
        return $validator;                
    }
    
    public function isValid($post, $action = 'save')
    {
        $result = ['error' => false, 'message' => ''];
        switch($action){
            case 'save': 
            
                $validate = $this->validateDefault($post);
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }
            
            break;                    
        }
        
        return $result;
    }

    public function postdetails($post)
    {
                
        $return = array(
            'ScheduleID' => decode(getObjectValue($post, 'sched')),
            'SubjectID' => decode(getObjectValue($post, 'subject')),
    	    'PolicyID' => decode(getObjectValue($post, 'policy')),
 	        'PeriodID' => getObjectValue($post, 'quarter'),
            'Createdby' => getUserID(),
            'DateCreated' => date('Y-m-d H:i:s')
        );
                
        return $return;
    }
}
