<?php $table = isset($table) ? $table : array() ; ?>
<div class="table-scrollable">
	<table class="table table-striped table-hover" style="cursor: pointer;">
    	<thead>
        	<tr>
        		<th class="autofit"> # </th>
        		<th> 
                Section Name &amp; <br />        
        		Subject Code / Title
                </th>		
        		<th> Units </th>
                <th> Schedule/Room </th>        
                <th> Class Adviser </th>
                <th> # Students </th>
                <th> Policy </th>
                <th> Last Date Encoded </th>
                <th> Date Grade Posted </th>
                <th> Options </th>
        	</tr>
    	</thead>
    	<tbody>		
            @for($i=0; $i < count($table) ; $i++ )
        	<tr data-id="<?= $table[$i]->ScheduleID ?>" data-section="<?= encode($table[$i]->SectionID) ?>" data-polid="<?= encode($table[$i]->PolicyID) ?>" >
        		<td class="autofit">{{ $i + 1 }}.</td>
        		<td class="autofit">
                    Subject Title : <span class="bold"><?= $table[$i]->SubjectTitle ?></span><br />
                    Subject Code : <span class="bold"><?= $table[$i]->SubjectCode ?></span><br />
                    Section : <span class="bold"><?= $table[$i]->SectionName ?></span> 
                </td>                    		    		
                <td class="autofit"> <?= number_format( $table[$i]->Units,2) ?>  </td>
                <td> <?= $table[$i]->Sched_1 ?>  </td>            
                <td> <?= $table[$i]->TeacherName?>  </td>
                <td> <?= $table[$i]->TotalStudents ?></td>
                <td> <?= $table[$i]->PolicyName ?>  </td>
                <td> <?= '0'?>  </td>
                <td> <?= $table[$i]->GradesPostingDate ?>  </td>            
        		<td> <button type="button" class="btn btn-default btn-xs "> Grade Sheet </button></td>
        	</tr>
            @endfor
    	</tbody>
	</table>
</div>