<div class="row">
<div class="col-sm-12">
    @if($facultyId == '')
        <div class="note note-danger">
    		<h4 class="block">Warning! You have no IEncode ID</h4>
    		<p>
            Please contact your system administrator for your faculty id.
    		</p>
    	</div>                            
    @endif
    
	<div class="portlet blue box">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-calendar"></i> Faculty Schedules </div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>				
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="portlet-body">                  
            <select class="bs-select form-control input-medium " id="term" name="term" >
                <option> - Select - </option>
                
                @if(!empty($academic_year))
                    @foreach($academic_year as $ayt)
                        <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AYTerm }} </option>                                                                 
                    @endforeach
                @endif                                           
            </select>        
            <button class="btn btn-default" type="button" data-menu="refresh"><i class="fa fa-refresh"></i> </button>
            <hr style="margin-top: 10px; margin-bottom: 5px;" />
            <div id="myschedules">
                @include($views.'table')
            </div>                                        
		</div>
	</div>
</div>
</div>