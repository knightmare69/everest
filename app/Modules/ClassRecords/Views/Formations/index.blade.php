<style>
.components{
    min-height: 200px !important;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div id="formations" class="portlet light" style="margin-bottom: 0px;">
        	<div class="portlet-title">
        		<div class="caption"><i class="fa fa-sliders"></i>
        			<span class="caption-subject bold uppercase"> Subject Formation</span> <br />
        			<span class="caption-helper">subject formation for grading policy</span>
        		</div>
        		<div class="actions">
                    <div class="portlet-input input-inline input-medium">
                        <select class="bs-select form-control " id="term" name="term" >
                            <option value="-1"> - Select - </option>
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                    <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm }} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="portlet-input input-inline input-medium">
        				<div class="input-icon right"><i class="icon-magnifier"></i>
        					<input type="text" placeholder="search..." class="form-control  filter-table" data-table="tblschedule">
        				</div>
        			</div>
                    <button type="button" class="btn btn-default" data-menu="refresh"><i class="fa fa-refresh"></i> Refresh </button>
        		</div>
        	</div>
        	<div class="portlet-body clearfix" style="padding-top: 0px;">
                <div class="col-sm-9">
                    <div class="scroller" style="height: 550px;">
                        @include($views.'table')
                    </div>
                </div>
                <div class="col-sm-3">
                    <div id="policyinfo" class="well well-sm well-light">
                        <table class="table" id="tblsummary">
                            <thead>
                                <tr>
                                    <th>Period</th>
                                    <th>Policy</th>
                                </tr>
                            </thead>
                           <tr>
                           </tr>
                        </table>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div> @include($views.'modal')