<?php
    $setup = new \App\Modules\Setup\Models\GradingSystemSetup;
    $table = isset($table) ? $table : array();
?>
<table class="table table-striped table-hover table_scroll table-selection " id="tblschedule" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Policy</th>
            <th class="autofit">Sched.ID</th>
            <th>Subject Code</th>
            <th>Subject Title</th>
            <th class="autofit">Units</th>
            <th class="">Section</th>
            <th class="">Schedules</th>
            <th class="">Faculty</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->ScheduleID) }}" data-subid="{{encode($row->SubjectID)}}"
            data-name="{{ $row->SubjectTitle }}"  data-code="{{ $row->SubjectCode }}" data-unit="{{$row->AcadUnits}}" data-status=""
            data-shs="<?php echo $setup->IsSHS() == $row->ProgramID ? 1 : 0 ; ?>"            
            class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td class="autofit font-xs">{{$i}}.</td>
            <td class="autofit text-center">
                @if($row->Total ==0)
                    <i class="fa fa-times text-danger"></i>
                @else
                    <i class="fa fa-check text-success"></i>
                @endif
            </td>
            <td class="autofit">{{ $row->ScheduleID }}</td>
            <td class="autofit"><a href="javascript:void(0)" class="view-subject">{{ $row->SubjectCode }}</a></td>
            <td>{{ $row->SubjectTitle }}</td>
            <td>{{ number_format($row->AcadUnits,2) }}</td>
            <td class="autofit">{{ $row->SectionName }}</td>
            <td class="autofit">{{ $row->Sched_1 . ' '. $row->Room1 }}</td>
            <td class="autofit">{{ $row->Faculty1 }}</td>
        </tr>
        <?php ++$i; ?>
    @endforeach
    </tbody>
</table>