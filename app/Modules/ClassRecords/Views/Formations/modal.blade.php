<?php
    $model = new \App\Modules\GradingSystem\Models\Policy_model;
    
    $data =  $model->where('Inactive','0')->orderby('Policy','ASC')->get();     
    
        
    
    $options = '';
    if(!empty($data)):
        foreach($data as $r){
            $options .= "<option value='".encode($r->PolicyID)."'>".$r->Policy."</option>";
        }
    endif;
?>
<div id="form_modal1" class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog" style="width: 70% !important;">
		<div class="modal-content">
			<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Subject Policy Manager</h4></div>
			<div id="policybody" class="modal-body">
                <input type="hidden" id="idx" name="idx" value="0" />
                <input type="hidden" id="subid" name="subid" value="0" />
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well well-sm well-light">
                            <h4 class="bold"> Setup : <span id="form-title" class=" text-primary"></span> </h4>
                            <div id="polcontainer">
                                @include($views.'sub.periods')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label> Period :  </label>
                        <hr style="margin-top: 10px; margin-bottom: 5px;" />
                        <select id="cbopolicy" class="form-control  bs-select input-sm periods" data-live-search="true">
                            <option value="-1"> - Select Policy - </option>
                            <?php echo $options ;?>
                        </select>
                        <p class="components"></p>
                        <div class="text-right">
                            <button type="button" class="btn btn-sm btn-default" data-menu="use-policy" data-mode='one' > Use this policy</button>
                            <button type="button" class="btn btn-sm btn-default" data-menu="apply-to-all" data-mode='all' > Applies to All</button>
                        </div>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
				<button class="btn green btn-primary" data-menu="save-policy" type="button">Confirm Setup</button>
			</div>
		</div>
	</div>
</div>