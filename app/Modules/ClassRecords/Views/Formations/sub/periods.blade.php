<?php    
    $class = (isset($class) ?  $class : 1);
    $period_model = new \App\Modules\Setup\Models\Grading_Periods;    
    $mterm = new \App\Modules\Setup\Models\AcademicYearTerm;

    $IsSummer = $mterm->IsSummer($term)->count();
    
    if($IsSummer > 0 ){
        $class = 9;
    }
    
    $period = $period_model->where('GroupClass',$class )->get();
    $i=0;
    //error_print(!empty($data));   
?>
<table class="table table-bordered table-striped table-hover " data-class="<?php echo $class;?>" id="tblsummary" style="cursor: pointer;">
    <thead>
        <tr>
            <th>Select</th>
            <th>Period</th>
            <th>Policy</th>
            <th>Remove</th>
        </tr>
    </thead>
    <tbody>
    
    @forelse($data AS $p )        
        <tr data-period="{{$p->PeriodID}}" >
            <td>
                <a href="javascript:void(0);"> Select </a>
            </td>
            <td class="bold" >{{ $p->Period }} </td>
            <td class="policy" data-policy="{{encode($p->PolicyID)}}" >
                {{$p->Policy}}           
            </td>   
            <td>
                <a class="text-danger" href="javascript:void(0);"> <i class="fa fa-times"></i> Remove Policy </a>
            </td>
                                    
        </tr>
        
    @empty
        @foreach($period as $p)
        <tr data-period="{{$p->PeriodID}}" >
            <td>
                <a href="javascript:void(0);"> Select </a>
            </td>
            <td class="bold" >{{ $p->Description1 }} </td>
            <td class="policy" data-policy="" >
                [No Policy Yet]           
            </td>   
            <td>
                <a class="text-danger" href="javascript:void(0);"> <i class="fa fa-times"></i> Remove Policy </a>
            </td>
                                    
        </tr>
        @endforeach
    @endforelse                
    </tbody>
</table>