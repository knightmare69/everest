<table class="table table-striped table-hover table_scroll" id="tblpolicycomponent" style="cursor: pointer;">
    <thead>
        <tr>            
            <th class="autofit">#</th>        
            <th>Period</th>
            <th>Policy</th>
                                    
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($data as $row)
        <tr data-id="{{ encode($row->fldPK)}}" 
            data-period="{{$row->PeriodID}}"
            data-sched="{{ encode($row->ScheduleID) }}" 
            data-policy="{{ encode($row->PolicyID) }}" class="without-bg">            
            <td class="autofit font-xs">{{$i}}.</td>            
            <td><a href="javascript:void(0)" class="a_select">{{ $row->Period }}</a></td>
            <td class="policy">{{ $row->Policy }}</td>                        
        </tr>
        <?php ++$i; ?>
    @endforeach
    </tbody>
</table>
