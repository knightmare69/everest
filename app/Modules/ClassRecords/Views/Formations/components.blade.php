<?php
    $model = new \App\Modules\GradingSystem\Models\Policy_model;
    $data = array();
    $total = 0;
    if(!empty($policy)){
        if(is_numeric($policy) ) $data =  $model->get_components($policy);    
    }
?>
<table class="table table-striped table-hover table_scroll" id="tblpolicycomponent" style="cursor: pointer;">
    <thead>
        <tr>            
            <th class="autofit">#</th>        
            <th>Code</th>
            <th>Component</th>
            <th>Percentage</th>                        
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($data as $row)
        <tr data-id="{{ encode($row->fldPK) }}" 
            data-component="{{ encode($row->ComponentID) }}" 
            data-policy="{{ encode($row->PolicyID) }}"  data-code="{{ $row->CompCode }}" data-name="{{ $row->Caption }}" class="without-bg">            
            <td class="autofit font-xs">{{$i}}.</td>            
            <td><a href="javascript:void(0)" class="a_select">{{ $row->CompCode }}</a></td>
            <td>{{ $row->Caption }}</td>
            <td class="text-center">{{ $row->Percentage }}%</td>            
        </tr>
        <?php $total+= $row->Percentage;  ++$i; ?>
    @endforeach
    </tbody>
    @if($total>0)
    <tfoot>
        <tr>
            <td colspan="3" class="bold"> Total </td>
            <td class="text-center">{{$total}}%</td>
            
            
        </tr>
    </tfoot>
    @endif
</table>
