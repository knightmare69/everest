<?php 
    $data = isset($data) ? $data : array() ;
    $mSetup = new \App\Modules\Setup\Models\GradingSystemSetup ;
    $mComp = new \App\Modules\GradingSystem\Models\Policy_model;
    $sClub = new App\Modules\ClassRecords\Services\StudentClubServices;
     
    $comp = $mComp->get_components($mSetup->ClubPolicy());
    
    $eventids = [];
    
    foreach($comp As $r){
        $eventids[$r->ComponentID] = encode( $sClub->getEventID($term,$period, $club, $r->ComponentID) ) ;    
    }
    
             
?>
<table id="tblstudents" class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important;">
    <thead>
        <tr>            
            <th rowspan="3" class="autofit" ><input type="checkbox" class="chk-header"></th>
            <th rowspan="3"  >#</th>
            <th rowspan="3" >Pic</th>
            <th rowspan="3" >ID No.</th>
            <th rowspan="3" >Complete Name</th>
            <th rowspan="3" class="text-center">Sex</th>  
            <th rowspan="3" class="text-center">Year Level</th>
            
            @foreach($comp AS $r)
                <th colspan="2" data-id="{{$r->fldPK}}" data-comp="{{$r->ComponentID}}" class="autofit text-center " >{{ $r->Caption }}</th>
            @endforeach                                
            
            <th rowspan="3" class="text-center">Final Grade</th>
            <th rowspan="3" class="text-center">Letter</th>
            <th rowspan="3" class="text-center">Remarks</th>
            <th rowspan="3" class="autofit text-center" >Date Posted</th>            
            <th rowspan="3" class="autofit text-center" >Posted by</th>
        </tr>  
        <tr>
            @foreach($comp AS $r)
                <th>Grade</th>
                <th>EQ</th>    
            @endforeach                        
        </tr>
        <tr>
            @foreach($comp AS $r)
                <th class="text-center totalScore "  data-event="{{ $eventids[$r->ComponentID] }}" >100</th>
                <th>{{ $r->Percentage }}%</th>    
            @endforeach                        
        </tr>
              
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        
        <tr class="<?= ($data[$i]->Postedby == '' ? '' : 'success' ) ?>" 
            data-name="<?= $data[$i]->FullName  ?>" 
            data-reg="{{encode($data[$i]->RegID)}}"
            data-idno="{{ encode($data[$i]->StudentNo)}}"
            data-sched="{{encode($data[$i]->ScheduleID)}}"
            data-posted="<?= ($data[$i]->Postedby == '' ? '0' : '1' ) ?>"
             >
            <td class="autofit" >
                @if($data[$i]->Postedby == '')
                <input type="checkbox" class="chk-child">
                @else
                <i class="fa fa-lock"></i>
                @endif
            </td>
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="pic autofit"  > 
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($data[$i]->StudentNo) . '&t='. rand() }}" class="img"  height="25" width="25" alt="">
            </td>

            <td class="autofit studno"><?= $data[$i]->StudentNo ?></td>
		    <td class="bold autofit text-primary"><?= $data[$i]->FullName  ?> </a></td>
            <td class="autofit text-center "><?= $data[$i]->Gender ?></td>                
            <td class="autofit text-center "><?= $data[$i]->YearLevel ?></td>
            <?php $total = 0; ?>
            @foreach($comp AS $r)
                <?php
                    $score = $sClub->getEventScore($term,$period, $eventids[$r->ComponentID] , $data[$i]->StudentNo ) ;
                    $percentage = $score * ($r->Percentage / 100);
                    $total += $percentage; 

                    if($score > 0){
                        $score = number_format($score,2) ;
                    }
                    
                ?>
                <td data-event="{{ $eventids[$r->ComponentID] }}" data-group="{{$r->fldPK}}" data-percent="{{ $r->Percentage / 100 }}" data-total="100" tabindex="{{$i}}1" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> clubScore text-center "  >
                    {{ $score }}
                </td>
                <td data-group="{{$r->fldPK}}" tabindex="{{$i}}2" class="percent text-center bold info" data-percent="{{ $r->Percentage }}">
                {{$percentage}}
                </td>    
            @endforeach
                                    
            <td class="final color-red font-lg warning text-center bold" >
            <?= $data[$i]->FinalGrade ?>
            </td>
            <td class="letter bold text-center" > </td>
            <td class="remarks bold autofit">    
                   
            </td>            
            <td class="dtposted autofit text-center">
            <?= $data[$i]->DatePosted ?>            
            </td>
            <td class="byposted autofit text-center">
            <?= $data[$i]->Postedby ?>
            </td>                        	 
        </tr> 
    @endfor        
    </tbody>
</table>	
<script>
if (window['ME'] != undefined) ME.initremarks();
</script>
           	