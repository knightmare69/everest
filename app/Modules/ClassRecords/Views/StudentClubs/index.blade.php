<div class="row"><div class="col-sm-12">
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"> 
            <i class="fa fa-users font-blue-steel "></i>
            <span class="caption-subject bold font-blue-madison "> Student Clubs </span><br />
            <span class="caption-helper"> Use this module to grade your pupil based on their club behavior...</span>
        </div>
		<div class="tools">				                
            <div class="portlet-input input-inline input-medium">
                <select class="bs-select form-control input-medium " id="term" name="term" >
                    <option value="-1"> - Select - </option>                
                    @if(!empty($academic_year))
                        @foreach($academic_year as $ayt)
                            <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AYTerm }} </option>                                                                 
                        @endforeach
                    @endif                                           
                </select> 
            </div>
            
            <div class="portlet-input input-inline input-medium">
                <select class="form-control input-medium " id="club" name="club" >
                    <option value="-1"> - Select - </option>                
                                                               
                </select> 
            </div>
            
            <div class="portlet-input input-inline input-small">
                <select class="form-control input-small " id="period" name="period" >
                    <option value="-1"> - Select - </option>                                                                               
                </select>
            </div>
            <button class="btn btn-default" type="button" data-menu="refresh"><i class="fa fa-refresh"></i> Reload </button>
			 <div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn btn-default" aria-expanded="false">
				<i class="fa fa-list"></i> Menu <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li><a href="#" class="clink" data-menu="export" ><i class="fa fa-download"></i> Download Students </a></li>
					<li><a href="#" class="clink" data-menu="print" data-mode="homeroom"><i class="fa fa-print"></i> Print </a></li>
					<li><a href="#" class="clink" data-menu="post" ><i class="fa fa-lock"></i> Post </a></li>
					<li class="divider"></li>
					<li><a href="#" > <i class="fa fa-times"></i> Cancel </a></li>
				</ul>
			</div>
            
		</div>
	</div>
	<div class="portlet-body clearfix">
        <div class="row">
            <div class="col-sm-12" >        
                <div class="profile-sidebar hidden-sm hidden-xs hidden-md">
                    <div class="portlet light bordered  profile-sidebar-portlet ">
            			<!-- SIDEBAR USERPIC -->
            			<div class="profile-userpic">
            				<img alt="" class="img-responsive" src="{{ asset('assets/admin/layout/img/logo.png') }}">
            			</div>
            			<br />
    					<!-- SIDEBAR USER TITLE -->
    					<div class="profile-usertitle text-center">
    						<div class="profile-usertitle-name">[Student Name]</div>
    						<div class="profile-usertitle-job">[Student No.]</div>
    					</div>
                        <div class="profile-userbuttons ">
    						<button class="btn btn-circle green-haze btn-sm" type="button">View Profile</button>								
    					</div>	
                        <div class="profile-usermenu">
    						<ul class="nav">
    							<li class="active"><a href="javascript:void(0);" data-menu="conduct" ><i class="icon-home"></i> Conduct Record </a></li>
    							<li><a  href="javascript:void(0);" data-menu="summary" ><i class="icon-check"></i> Summary </a></li>
    							<li><a href="javascript:void(0);" data-menu="help" ><i class="icon-list"></i> Grading System </a></li>
    						</ul>
    					</div>                                						    							
                    </div>
                </div>                                              
                <div id="gradingsystem-container" class="well well-light well-sm" style=" display: none; overflow: auto; height: 550px; margin-bottom: 0px;">
                    @include($views.'gradingsystem')
                </div>            
                <div id="studentlist-container" class="well well-light well-sm" style="overflow: auto; height: 550px; margin-bottom: 0px;">
                    @include($views.'students')
                </div>
                <label id='tbleditor' style='display: none;' class=' input table-editor' data-table="tblstudents">
                    <input value="0" maxlength="6" class="form-control input-sm text-center  " style="border: none;" />
                </label>    
            </div>
        </div>            
	</div>
</div></div></div>