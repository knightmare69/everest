<div class="row"><div class="col-sm-12">
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"> 
            <i class="fa fa-meh-o"></i>
            <span class="caption-subject bold "> Student Attendance </span><br />
            <span class="caption-helper"> Use this module to grade your pupil based on their attendance...</span>
        </div>
		<div class="tools">				                
            <div class="portlet-input input-inline input-medium">
                <select class="bs-select form-control input-medium " id="term" name="term" >
                    <option value="-1"> - Select - </option>                
                    @if(!empty($academic_year))
                        @foreach($academic_year as $ayt)
                            <option value="{{encode($ayt->TermID)}}" <?= $ayt->TermID == $term ? 'selected' : '' ?> > {{ $ayt->AYTerm }} </option>                                                                 
                        @endforeach
                    @endif                                           
                </select> 
            </div>
            
            <div class="portlet-input input-inline input-small">
                <select class="form-control input-small " id="section" name="section" >
                    <?= $sections; ?> 
                             
                                                               
                </select> 
            </div>
            
            <div class="portlet-input input-inline input-small">
                <select class="form-control input-small " id="period" name="period" >
                    <?= $period; ?>                                                                           
                </select>
            </div>
            
            <div class="portlet-input input-inline input-small">
                <select class="form-control input-small " id="month" name="month" >
                    @foreach(getMonths() as $m => $k)
                    <option value="{{$m}}" <?= (date('m')) == $m ? 'selected':''  ?> > <?= $k ?> </option>
                    @endforeach
                </select>
            </div>
            
            <button class="btn btn-default" type="button" data-menu="refresh"><i class="fa fa-refresh"></i> Reload </button>
			
            <div class="btn-group hide">
				<a data-toggle="dropdown" href="#" class="btn btn-default" aria-expanded="false">
				<i class="fa fa-list"></i> Menu <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li><a href="#" class="clink" data-menu="export" ><i class="fa fa-download"></i> Download Students </a></li>
					<li><a href="#" class="clink" data-menu="print" data-mode="homeroom"><i class="fa fa-print"></i> Print </a></li>
					<li><a href="#" class="clink" data-menu="post" ><i class="fa fa-lock"></i> Post </a></li>
					<li class="divider"></li>
					<li><a href="#" > <i class="fa fa-times"></i> Cancel </a></li>
				</ul>
			</div>
            
		</div>
	</div>
	<div class="portlet-body clearfix">
        <div class="row">
            <div class="col-sm-12" >        
                
                <span class="font-xs bold">Note: AE = Absent Excused, AU = Absent Unexcused, TE = Tardiness Excused, TU = Tardiness Unexcused </span>                         
                <div id="gradingsystem-container" class="well well-light well-sm" style=" display: none; overflow: auto; height: 550px; margin-bottom: 0px;">
                    @include($views.'gradingsystem')
                </div>            
                <div id="studentlist-container" class="well well-light well-sm" style=" margin-bottom: 0px;">
                    
                </div>
                <label id='tbleditor' style='display: none;' class=' input table-editor' data-table="tblstudents">
                    <input value="0" maxlength="4" class="form-control input-sm text-center  " style="border: none;" />
                </label>    
            </div>
        </div>            
	</div>
</div></div></div>