<?php 
    $data = isset($data) ? $data : array() ;
    $model = new \App\Modules\ClassRecords\Models\Attendance\Attendance;   
    
    #$year= date('Y');
    $d_date = $pyear.'-'.$mnth.'-01';
    $days_code= array(
        0 => 'Su'
       ,1 => 'M'
       ,2 => 'T'
       ,3 => 'W'
       ,4 => 'Th'
       ,5 => 'F'
       ,6 => 'S' 
    );
    
    
    $dtstart =  new DateTime($d_date);
    $dtend = new DateTime($d_date);
    $dtend->modify('+1 month');
?>
<style>
    .name_width{ width: 250px !important; }
    .day_hwidth{ width: 28px !important; }
    .day_width{ width: 28px !important; height: 25px !important; }

div.name_width {
    white-space: nowrap; 
    width: 250px; 
    overflow: hidden;
    text-overflow:ellipsis;
}

</style>

<div style="float: left; width: 405px !important;">
<table class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important; margin-bottom: 0px;">
<thead>
        <tr>            
            <th class="autofit" ><input type="checkbox" class="chk-header"></th>
            <th width="25" ><div style="width: 25px !important; height: 20px !important; ">
            </div> #</th>
            <th width="25" ><div style="width: 25px !important;">PIC</div></th>            
            <th width="250" > <div class="name_width"> Complete Name </div></th>
                          
           
            
        </tr>
        
    </thead>
</table>
</div>


<div id="mytblheader" style=" margin-left: 405px !important;  overflow-y: scroll; overflow-x: hidden; overflow: -moz-scrollbars-vertical;">
<table class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important; margin-bottom: 0px;">
<thead>
        <tr>            
                        
            <?php
                $i=1;
                while ($dtstart != $dtend){
                    $css = '';
                    $code = '';
                    if( $dtstart->format('w') == '0'){
                        $css = 'danger';
                    }
                    
                    if( $dtstart->format('w') == '6'){
                        $css = 'warning';
                    }
                    
                    $code = $days_code[$dtstart->format('w')];
                    echo '<th width="30" class="text-center '.$css.'" ><div class="day_hwidth" >'.$i.'<br/>'. $code .'</div></th>';
                   
                                
                    $dtstart->modify('+1 day');
                  
                    $i++;
                    if($i == 32){
                        break;
                    }
            
                }
            ?>
            
        </tr>
        
    </thead>
</table>
</div>

<div id="studentcols" style="height: 500px; float: left; width: 405px !important; overflow-x: scroll; overflow-y: hidden; overflow: -moz-scrollbars-horizontal;">

<table id="tblnames" class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important; margin-bottom: 0px;">    
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        
        <tr class="" data-name="<?= $data[$i]->FullName  ?>" data-idno="{{$data[$i]->StudentNo}}" >
            <td class="autofit" >
                <input type="checkbox" class="chk-child">
            </td>
            <td width="25" style="width: 25px !important;" class="font-xs">
                <div style="width: 25px !important;">
                {{$i + 1}}.
                </div>                
            </td>
            <td width="39"  class="pic"  > 
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($data[$i]->StudentNo) }}" class="img"  height="25" width="25" alt="">
            </td>
		    <td width="250"  class="bold text-primary">
                <div class="name_width" style="text-overflow: ellipsis;" title="<?= $data[$i]->FullName  ?>"> <?= $data[$i]->FullName  ?> </div> 
            </td>                                                                                                                             	 
        </tr> 
    @endfor        
    </tbody>
</table>	
</div>
<div style="overflow: auto; height: 500px; margin-left: 405px !important; " onscroll="scrollme(this)">

<table id="tblstudents" class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important; margin-bottom: 0px;">
    <thead class="hide">
        <tr>            
            <th class="autofit" ><input type="checkbox" class="chk-header"></th>
            <th >#</th>
            <th >Pic</th>
            
            <th >Complete Name</th>
            <th class="text-center">Sex</th>                
            <?php
                $i=1;
                while ($dtstart != $dtend){
                    
                    echo '<th width="30" class="text-center" >'.$i.'</th>';
                    if( $dtstart->format('w') != '0'){
                        //$days++;
                    }
                                
                    $dtstart->modify('+1 day');
                  
                    $i++;
                    if($i == 32){
                        break;
                    }
            
                }
            ?>
            
        </tr>
        
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        
        <tr class="" data-name="<?= $data[$i]->FullName  ?>" data-idno="{{$data[$i]->StudentNo}}" >

            
            <?php
                $j=1;
                $dtstart =  new DateTime($d_date);
                $dtend = new DateTime($d_date);
                $dtend->modify('+1 month');
                
                while ($dtstart != $dtend)
                {                    
                    $code = '';
                    $css = '';
                    $note = '';
                    
                    if( $dtstart->format('w') == '0'){
                        $css = 'danger';
                    }
                    
                    if( $dtstart->format('w') == '6'){
                        $css = 'warning';
                    }
                    
                    
                    $rs = $model
                            ->where('StudentNo', $data[$i]->StudentNo)
                            ->where('Date', $dtstart->format('Y-m-d'))
                            ->first();
                            
                    if($rs){    
                        if($rs->Type == '1' && $rs->IsExcused == '1' ){
                            $code = 'AE';
                        }elseif($rs->Type == '1' && $rs->IsExcused == '0'){
                            $code = 'AU';
                        }elseif($rs->Type == '2' && $rs->IsExcused == '1'){
                            $code = 'TE';
                        }elseif($rs->Type == '2' && $rs->IsExcused == '0'){
                            $code = 'TU';
                        }  
                        $note = $rs->Remarks;                                 
                    }
                    
                    echo '<td width="30" tabindex="'. $i.$j .'" title="'.$note.'" data-date="'.$dtstart->format('Y-m-d').'" class="text-center uppercase '. $css .' bold editable" ><div class="day_width" >'. $code .'</div></td>';
                  
                                
                    $dtstart->modify('+1 day');
                  
                    $j++;
                    if($j == 32){
                        break;
                    }
            
                }
            ?>                                                                                      	 
        </tr> 
    @endfor        
    </tbody>
</table>	     
</div>
            	