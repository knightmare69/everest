<?php $table = isset($table) ? $table : array() ; ?>
@if(!empty($table))
    @foreach( $table AS $r )           
        <option value="<?= encode($r->SectionID) ?>">
            <?= $r->SectionName ?>
        </option>
    @endforeach
@endif    