<div style="width: 500px; background-color: #8e44ad !important; overflow: auto; height: 250px;">
<table id="tblgradesystem" class="table table-condensed" style="cursor: pointer !important; color: white;">
    <thead>
        <th>#</th>
        <th>Min</th>
        <th>Max</th>
        <th>Letter</th>
        <th>Descriptors</th>
        <th>Symbols</th>
    </thead>
    <tbody>
    
        <tr data-id ="" >
            <td class="autofit font-xs">1.</td>
            <td class="text-center autofit min" >AE</td>
		    <td class="text-center autofit max" >AE</td>
            <td class="letter" >AE</td>
            <td class="desc" >ABSENT EXCUSED </td>
            <td class="remarks text-center">ABSENT EXCUSED</td>
        </tr>
        <tr data-id ="" >
            <td class="autofit font-xs">2.</td>
            <td class="text-center autofit min" >AU</td>
		    <td class="text-center autofit max" >AU</td>
            <td class="letter" >AU</td>
            <td class="desc" >ABSENT UN-EXCUSED </td>
            <td class="remarks text-center">ABSENT UN-EXCUSED</td>
        </tr>

        <tr data-id ="" >
            <td class="autofit font-xs">3.</td>
            <td class="text-center autofit min" >TE</td>
		    <td class="text-center autofit max" >TE</td>
            <td class="letter" >TE</td>
            <td class="desc" >TARDINESS EXCUSED </td>
            <td class="remarks text-center">TARDINESS EXCUSED</td>
        </tr>

        <tr data-id ="" >
            <td class="autofit font-xs">4.</td>
            <td class="text-center autofit min" >TU</td>
		    <td class="text-center autofit max" >TU</td>
            <td class="letter" >TU</td>
            <td class="desc" >TARDINESS UN-EXCUSED </td>
            <td class="remarks text-center">TARDINESS UN-EXCUSED</td>
        </tr>
    
    </tbody>
</table>
</div>
