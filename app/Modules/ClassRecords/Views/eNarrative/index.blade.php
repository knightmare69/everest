<div class="row">
  <div class="col-sm-12">
    <div class="portlet clearfix light" id="main-portlet" style="margin-bottom: 0px;">
		<div class="portlet-title">
			<div class="caption">
				<span class="caption-subject bold text-primary"><i class="fa fa-star"></i> eNarrative</span> <br />
				<span class="caption-helper text-info hidden-xs hidden-sm hidden-md ">Use this module for your narrative management...</span>                                    		
			</div>                
			<div class="tools">
				<div class="portlet-input input-inline input-medium">
					<select class="form-control input-sm" name="term" id="term">
						<option value="-1"> - Select Academic Year - </option>
						<?php foreach($ayterm as $k=>$a){
						  echo '<option value="'.encode($k).'" '.(($a['isactive']==1)?'selected':'').'>'.$a['name'].'</option>';	
						}?>
					</select>
				</div>                
				<div class="portlet-input input-inline input-medium">
					<select class="form-control input-sm" name="section" id="section">
						<option value="-1"> - Select Section - </option>
						<?php foreach($section as $k=>$s){
						  $k = (($s['scheduleid']=='')?$k:$s['scheduleid']);
						  echo '<option value="'.encode($k).'" data-term="'.encode($s['termid']).'" data-schedule="'.$s['scheduleid'].'">'.$s['name'].'</option>';	
						}?>
					</select>
				</div> 
				<div class="portlet-input input-inline input-small">
					<select class="form-control input-sm" name="period" id="period">
						<option value="-1"> - Select Period - </option>
						<option value="1"> 1ST QUARTER</option>
						<option value="2"> 2ND QUARTER</option>
						<option value="3"> 3RD QUARTER</option>
						<option value="4"> 4TH QUARTER</option>
					</select>
				</div>                       
				<button type="button" class="btn btn-sm btn-default" data-menu="refresh" ><i class="fa fa-refresh "></i></button>
				<button type="button" class="btn btn-sm btn-default fullscreen " ><i class="fa fa-expand "></i></button>                
			</div>
		</div>
		<div class="portlet-body">
		    <div class="row">
			    <div class="pull-right">
				    <button class="btn btn-warning btn-sm btn-save"><i class="fa fa-save"></i> Save</button>
				    <button class="btn btn-danger btn-sm btn-delete"><i class="fa fa-times"></i> Delete</button>
				    <button class="btn btn-default btn-sm btn-post"><i class="fa fa-lock"></i> Submit</button>
					<?php
						// if($permission->has('export')){
						   echo '<button class="btn btn-default btn-sm btn-upost "><i class="fa fa-unlock"></i> Unpost</button>';
						// }
					?>
				</div>
			</div>
			<div class="row" >       
				<div class="profile-sidebar hidden-sm hidden-xs hidden-md">
					<div class="portlet light bordered  profile-sidebar-portlet ">    			
						<div class="profile-userpic"><img alt="" class="img-responsive" src="{{ asset('assets/admin/layout/img/logo.b.png') }}"></div>
						<br />				
						<div class="profile-usertitle text-center">
							<div class="profile-usertitle-name"> [Student Name] </div>
							<div class="profile-usertitle-job"> [Student No.] </div>
							<div class="text-danger bold font-lg profile-final-grade hidden"> [Final Grade] </div>
							<div class="text-danger bold font-lg profile-club hidden"> [Club] </div>
						</div>
						
						<!-- <div class="profile-userbuttons ">					
							<button class="btn btn-circle btn-warning btn-sm" data-menu="varsity" type="button">Varsity</button>								
						</div> -->	
						<!-- disabled varsity in everest grading system -->
						<div class="profile-usermenu">
							<ul class="nav">
								<li class="active"><a href="#" class="views" data-menu="class-record"><i class="icon-home"></i> Narrative </a></li>
								<li class=""><a  href="#" class="views" data-menu="gradesys" ><i class="icon-check"></i>Grade System </a></li>
								<li class="hidden"><a href="#" class="views" data-menu="help"><i class="icon-info"></i>Transmutation</a></li>
							</ul>
						</div>                                						    							
					</div>
				</div>        
				<div id="class-record-container">
                   <div id="myschedules" class="clearfix"> 
				       @include($views.'student')
				   </div>
				</div>
		  </div>
		</div>
	</div>	
   </div>
</div>	