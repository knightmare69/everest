<?php
$data = App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID',$tblid)->get();
$arr  = ((isset($arr) && $arr)?$arr:false);
if($arr){
 $yrlvl  = $arr[0]->YearLevelID;
 $progid = $arr[0]->ProgID; 
}
?>
<div id="mytblheader" class=""  style="max-height:600px;overflow-x:scroll;overflow-y:scroll;">
  <div class="students">
	<table id="tblnarrative" class="table table-bordered table-condense">
	   <thead> 
	     <tr>
			<th rowspan="<?php echo (($arr && $progid==5 && $yrlvl==1)?3:2);?>" class="autofit" ><input type="checkbox" class="chk-header"></th>
			<th rowspan="<?php echo (($arr && $progid==5 && $yrlvl==1)?3:2);?>"  class="autofit" ><div style='width: 15px; text-align: center; ' >#</div></th>
			<th rowspan="<?php echo (($arr && $progid==5 && $yrlvl==1)?3:2);?>"  class="autofit"><div style='width: 25px; text-align: center; ' >Pic</div></th>
			<th rowspan="<?php echo (($arr && $progid==5 && $yrlvl==1)?3:2);?>"  class="autofit text-center bold">Student Name</th>
			<th rowspan="<?php echo (($arr && $progid==5 && $yrlvl==1)?3:2);?>"  class="autofit text-center bold"><div class="cgender" >Sex</div></th>
		    <th colspan="<?php echo (($arr && $progid==5 && $yrlvl==1)?22:3);?>" class="text-center">Conduct</th>
			<th <?php echo (($arr && $progid==5 && $yrlvl==1)?'colspan="2"':'rowspan="3"');?>  class="text-center">Narrative</th>
		 </tr>
		 <?php 
		 if($arr && $progid==5 && $yrlvl==1){
		 ?>
		 <tr>
		    <th colspan="8" class="text-center">Work Habit</th>
			<th colspan="14" class="text-center">Personal And Social Skills</th>
		 </tr>
		 <?php 
		 }
		 ?>
         <tr>
		   <?php 
		      if($arr && $progid==5 && $yrlvl==1){
				 $arr_title = array(
				                    1  => 'Works independently'
                                   ,2  => 'Cleans up after work/play'																		
								   ,3  => 'Is able to focus in class'
                                   ,4  => 'Follows class rules and routines'
                                   ,5  => 'Respects the work of others'
                                   ,6  => 'Listens attentively and respectfully'
                                   ,7  => 'Stays with an activity to completion'
                                   ,8  => 'Works neatly and carefully'
                                   ,9  => 'Displays self-confidence'
                                   ,10 => 'Takes responsibility for own things'
                                   ,11 => 'Uses good manners'
                                   ,12 => 'Dresses up independently'
                                   ,13 => 'Plays fair'
                                   ,14 => 'Knows how to wait for his/her turn'
                                   ,15 => 'Shares'
                                   ,16 => 'Initiates play'
                                   ,17 => 'Demonstrates leadership skills'
                                   ,18 => 'Uses words to solve conflicts'
                                   ,19 => 'Adjusts well to new situations'
                                   ,20 => 'Attempts new tasks confidently'
								   ,21 => 'Knows how to ask for help'
                                   ,22 => 'Respects the feelings of others'
				                   );
			  }else{
				  $arr_title = array(
				                    1  => 'Acts Responsibly'
                                   ,2  => 'Extends consideration,<br>respect for,<br>and service to others'																		
								   ,3  => 'Exhibits leadership<br>in character and virtue'
				                   );
			  }
			  for($i=1;$i<=count($arr_title);$i++){
			   echo '<th class="bold">
					   <div class="conductRemarks text-center tooltips" data-container="body" data-placement="top" data-original-title="'.$arr_title[$i].'">'.$arr_title[$i].'</div>
					 </th>';
			  }  
			
		      if($arr && $progid==5 && $yrlvl==1){
				echo '<th class="bold text-center" width="25%">Work Habit</th>
			          <th class="bold text-center" width="25%">Social Skills</th>';  
			  }else{
				  
			  }	  
			?>
         </tr>		
	   </thead>
	   <tbody>
	        <tr>
			  @if(isset($arr) && $arr)
			      @include($views.'trdata')
		      @else
				  <td colspan="10" class="text-center" style="height:400px;"></td>
		      @endif
			</tr>
	   </tbody>
	</table>
  </div>	
  <div class="gradesystem hidden">
     <?php 
     if($arr){
     ?>
		<table class="table table-condensed  table-hover" style="cursor: pointer !important;">
			<thead>
				<th>#</th>
				<th>Min</th>
				<th>Max</th>
				<th class="autofit">Letter Grade</th>
				<th class="">Description</th>
				<th class="text-center ">Remarks</th>                    
			</thead>
			<tbody>
			@for($i=0; $i < count($data) ; $i++ )
				@if( $data[$i]->Inactive == 0 )
				<tr data-template="<?= encode($data[$i]->TemplateID) ?>" data-min="<?= $data[$i]->Min ?>" data-max="<?= $data[$i]->Max ?>"
					data-letter="<?= $data[$i]->LetterGrade ?>" data-desc="<?= $data[$i]->Description ?>"
					data-remarks="<?= $data[$i]->Remark ?>" data-id="<?= encode($data[$i]->IndexID) ?>"
				>
					<td class="autofit font-xs">{{$i + 1}}.</td>
					<td class="bold text-primary min"><?= $data[$i]->Min ?></td>
					<td class="bold text-primary max "><?= $data[$i]->Max  ?></a></td>
					<td class="bold letter "><?= $data[$i]->LetterGrade  ?> </a></td>
					<td class="autofit desc "><?= $data[$i]->Description ?></td>
					<td class="remarks "><?= $data[$i]->Remark ?></td>	                             
				</tr> 
				@endif        
			@endfor        
			</tbody>
		</table>
	 <?php
	 }
     ?>
  </div>
</div>
