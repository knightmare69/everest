<?php
$gd          = 1;   
foreach($arr as $r){
$yrlvl       =  $r->YearLevelID;
$progid      =  $r->ProgID;
$StudentName = $r->Fullname;
$lock        = (($r->{'DatePosted'.$period}!='' && $r->{'PostedBy'.$period}!='')?'warning':''); 
$isposted    = (($r->{'DatePosted'.$period}!='' && $r->{'PostedBy'.$period}!='')?'true':'false');   
?>
   <tr data-idno="{{ encode($r->StudentNo) }}" data-name="{{$StudentName}}"  data-student="{{$r->StudentNo}}"
            data-reg="{{encode($r->RegID)}}" data-yr="{{encode($r->YearLevelID)}}" data-posted="{{$isposted}}"      
            class="<?= $lock ?>" >
            <td class="autofit text-center">
                @if($lock == '')
                   <input type="checkbox" class="chk-child">
                @else
                   <div style="width: 13px" ><i class="fa fa-lock"></i></div>
                @endif
            </td>
            <td tabindex="-1" class="seq autofit font-xs"><div style='width: 15px; text-align: center; ' >{{$gd}}.</div></td>
            <td class="img autofit"  >
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($r->StudentNo) }}" class="img"  height="25" width="25" alt="">
            </td>
            <td tabindex="-1" class="name bold autofit " style="" >
                <div style="width: 280px !important;">
                    <a href="javascript:void(0);">{{$StudentName}}</a>
                </div>
            </td>
            <td tabindex="-1" class="status autofit"><div class="cgender" >{{ $r->Gender}}</div></td>
            <?php 
               for($i=1;$i<=count($arr_title);$i++){
                   $score = ((property_exists($r,'Conduct'.$i))?($r->{'Conduct'.$i}):'');
                   if($lock==''){
                     $control = '<input class="text-center numberonly conduct tooltips" data-container="body" data-placement="top" data-original-title="'.$arr_title[$i].'" type="text" style="width:100%;height:100%;border:none;font-size:small;" id="conduct'.$i.'" name="conduct'.$i.'" data-editable="'.$isposted.'" value="'.$score.'"/>';   
                   }else{
                     $control = '<div class="text-center numberonly conduct tooltips" data-container="body" data-placement="top" data-original-title="'.$arr_title[$i].'" style="width:100%;height:100%;border:none;font-size:small;" id="conduct'.$i.'" name="conduct'.$i.'" data-editable="'.$isposted.'">'.$score.'</div>';     
                   }
                   echo '<td class="autofit no-padding" tabindex="-1">'.$control.'</td>';
               }
            ?>
            <td class="autofit" tabindex="-1" >
                @if($lock == '')
                   <textarea style="width:640px;height:64px;border:none;font-size:small;" id="narrativea" name="narrativea" class="" tabindex="{{$gd}}" maxlength="3000"><?php echo $r->{'NarrativeA'.$period};?></textarea>
                @else
                   <span id="narrative">{{ $r->{'NarrativeA'.$period} }}</span>
                @endif
            </td>
             @if($arr && $progid=='5' && $yrlvl=='1')
            <td class="autofit" tabindex="-1" >
                @if($lock == '')
                   <textarea style="width:640px;height:64px;border:none;font-size:small;" id="narrativeb" name="narrativeb" class="" tabindex="{{$gd}}" maxlength="2500"><?php echo $r->{'NarrativeB'.$period};?></textarea>
                @else
                   <span id="narrative">{{ $r->{'NarrativeB'.$period} }}</span>
                @endif
            </td>
            @endif
    </tr>       
<?php
$gd++;
}
?>