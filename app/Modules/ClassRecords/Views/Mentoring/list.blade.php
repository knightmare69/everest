<?php
    
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
    
    $j = 1;

    global $yrlvl;
    $x = 0;

    $yrlvl = App\Modules\Enrollment\Models\YearLevel::where('Inactive',0)->get();

    function selectedYear( $yr, $progid ){
        global $yrlvl;
        $return = '';
        $selected = "";
        $disable = "display:none;";

        foreach($yrlvl as $r){

            if($disable != '' &&  $yr == $r->YLID_OldValue && $r->ProgID == $progid ){
                $disable = "";
            }

            if($yr + 1 == $r->YLID_OldValue){
                $selected = "selected";
            }

            $return .= '<option style="'.$disable.'" '. $selected.' data-prog="'.$r->ProgID.'" value="'. ($r->YLID_OldValue).'">'.$r->YearLevelName.'</option>';
            $selected = "";
        }


        return $return ;
    }
    
    $balance = 0;


?>
<div class="" style="padding-bottom: 5px;">
<div class="btn-group btn-group-circle">
    <?php
        $add = 0;
        $max =  $total / 50;
        if( $total % 50 <= 4) $add = 1;
        $max = floor($max) + $add;
        
        $url = url('enrollment/advising?v='. rand() .'&t='.encode($term).'&f='.($filter));
    ?>
    <a class="btn btn-default" href="{{$url.'&v=1'}}">&LessLess; First</a>
    @for($i =1 ; $i<= $max; $i++ )
        <a class="btn btn-default <?= $page == $i ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a>		
	@endfor
    <a class="btn btn-default" href="{{$url.'&v='.$max}}">Last &GreaterGreater;</a>
</div>
</div>

<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Ref#</th>            
            <th>Student Name</th>
            <th>Program</th>
            <th>Grade Level</th>
            <th>Topic</th>
            <th>Issue</th>
            <th>Goals</th>
            <th>Mentor</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <tr data-id="{{ encode($r->SessionID) }}" data-idno="{{ encode($r->StudentNo) }}"  >
            <td class="autofit">
                <input type="checkbox" class="chk-child" >                                
            </td>
            <td class="font-xs autofit bold">{{$j}}.</td>            
            <td class="advised">
                <a href="javascript:void();" class="options" data-menu="edit">{{$r->SessionID}}</a>            
            </td>    
            <td class="">{{ $r->StudentName }} <br /> <small class="font-xs bold">{{$r->StudentNo}}</small></td>
            <td>Department</td>
            <td>{{$r->YearLevel}} </td>                                     
            <td>{{$r->Topic}}</td>
            <td>{{$r->Issue}}</td>
            <td>{{$r->Goals}}</td>
            <td>{{$r->MentorID}}</td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>
<div class="">
<div class="btn-group btn-group-circle">    
    <a class="btn btn-default" href="{{$url.'&v=1'}}">&LessLess; First</a>
    @for($i =1 ; $i<= $max; $i++ )
        <a class="btn btn-default <?= $page == $i ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a>		
	@endfor
    <a class="btn btn-default" href="{{$url.'&v='.$max}}">Last &GreaterGreater;</a>
</div>
</div>