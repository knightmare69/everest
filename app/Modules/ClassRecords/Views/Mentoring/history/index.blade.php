<?php $i=1; ?>
<div class="portlet light ">
    <div class="portlet-title">
    	<div class="caption caption-md">
    		<i class="icon-bar-chart theme-font font-blue-madison"></i>
    		<span class="caption-subject theme-font font-blue-madison bold uppercase">History</span>
    		<span class="caption-helper">This module is intent to display Mentoring History</span>
    	</div>
    	<div class="actions">
    		 <div class="portlet-input input-inline input-medium">
                <select class="form-control input-sm select2" disabled="" name="term" id="term">
                    <option value="-1"> - Select Academic Year - </option>
                    @if(!empty($at))
                        @foreach($at as $ayt)
                            <option <?= $ayt->TermID == $term ? 'selected':'' ?> value="{{($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>
                        @endforeach
                    @endif
                </select>
            </div>
            <button type="button" class="btn btn-default" id="reload" ><i class="fa fa-refresh"></i> Refresh </button>
            <a href="{{ url('class-record/mentoring?t='. encode($term) ) }}" class="btn btn-sm btn-warning" ><i class="fa fa-ban"></i> Cancel </a>
    	</div>
    </div>
	<div class="portlet-body" >        
		<div class="table-scrollable " oncontextmenu="return false;">
			<table id="tblhistory" oncontextmenu="return false;" class="table table-bordered table-condensed table-hover " style="cursor: pointer;">
                <thead>
        			<tr class="uppercase">
                        <th>#</th>
                        <th>Academic Year/Term</th>
                        <th class="text-center"> Date </th>
        				<th>Topic</th>
        				<th>Issue</th>
                        <th class="text-center"> Goals </th>
        				<th class="text-center"> Mentor Follow Up </th>        				
                        <th class="text-center"> Mentor </th>                             
        			</tr>
                </thead>
    			<tbody>
                    @foreach($history as $r)
                        <tr>
                            <td><?php echo $i; $i++; ?></td>
                            <td>{{$r->YearTerm}}</td>
                            <td>{{$r->TransDate}}</td>
                            <td>{{$r->Topic}}</td>
                            <td>{{$r->Issue}}</td>
                            <td>{{$r->Goals}}</td>
                            <td>{{$r->MentorFollowUp}}</td>
                            <td>{{$r->MentorID}}</td>
                        </tr>
                        
                    @endforeach

                </tbody>
            </table>
		</div>
	</div>
</div>