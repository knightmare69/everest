
<div class="portlet light ">
	<div class="portlet-title">
		<div class="caption caption-md">
			<i class="icon-bar-chart theme-font font-blue-madison"></i>
			<span class="caption-subject theme-font font-blue-madison bold uppercase"> Student's Formation Plan </span>
			<span class="caption-helper hide">This module is intent for generation of transcript of subject</span>
		</div>
		<div class="actions">           
            <button type="button" class="btn btn-default" data-menu="reload"><i class="fa fa-refresh"></i> Refresh </button>
		</div>
	</div>
	<div class="portlet-body" >
		<!-- BEGIN FORM-->
        <form id="formationForm" class="form-horizontal form-bordered form-row-stripped" action="#" method="POST">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        @include('errors.event')
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-bordered-black table-condensed " id="tblformation" >
                            <thead>
                                <tr>
                                    <td class="font-blue-madison font-lg">What's Easy for Me</td>
                                    <td class="font-blue-madison font-lg">What's Hard for Me</td>
                                    <td class="font-blue-madison font-lg">What I want to be in the Future</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td >
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="What's Easy for Me" name="easy">{{ getObjectValue($plan,'Easy') }}</textarea>
                                    </td>
                                    <td>
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="What's Hard for Me" name="hard">{{ getObjectValue($plan,'Hard') }}</textarea>
                                    </td>
                                    <td>
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="What I want to be in the Future" name="future">{{ getObjectValue($plan,'Future') }}</textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                		<table class="table table-bordered table-bordered-black table-condensed " id="tblareas" >
                            <thead>
                                <tr>
                                    <td class="font-blue-madison font-lg" >The 4 Areas of Formation</td>
                                    <td class="font-blue-madison font-lg" >Goals</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="30%" class="bold" rowspan="2" >1. Spiritual<br />
                                    <small class="font-xs text-left">
&nbsp; a.	Knowledge of the Faith<br />
&nbsp; b.	Sacraments<br />
&nbsp; c.	Friendship with Christ/My Prayer<br />
&nbsp; d.	Virtues/Vices<br />
&nbsp; e.	Doubts/Questions<br />
                    </small>
                                    </td>
                                    <td class="bold">
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Student" name="sSpiritual">{{ getObjectValue($plan,'SpiritualStudent') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=" bold" >
                                       <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Mentor" name="mSpiritual">{{ getObjectValue($plan,'SpiritualMentor') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold" rowspan="2" >2. Human<br />
                                    <small class="font-xs text-left">
&nbsp; a.	Friendships/Teamwork<br />
&nbsp; b.	Will Power<br />
&nbsp; c.	School Involvement: Sports, Clubs, Activities, Responsibilities<br />
&nbsp; d.	My presentation, manners, punctuality, etc.<br />
&nbsp; e.	Organization/Order (in and out of school- room, locker, etc.)<br />
&nbsp; f.	Balanced Lifestyle<br />
&nbsp; g.	Growth in my talents
                    </small>
                                    </td>
                                    <td >
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Student" name="sHuman">{{ getObjectValue($plan,'HumanStudent') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                    <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Mentor" name="mHuman">{{ getObjectValue($plan,'HumanMentor') }}</textarea>
                                    </td>
                                </tr>
                                  <tr>
                                    <td class="bold" rowspan="2" > 3.	Intellectual: <br />
                                    <small class="font-xs text-left">
&nbsp; a.	Classroom Etiquette and Behavior<br />
&nbsp; b.	Study Skills<br />
&nbsp; c.	Difficulties/Strengths in Classes<br />
&nbsp; d.	Grades</small>
                                    </td>
                                    <td >
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Student" name="sIntellectual">{{ getObjectValue($plan,'IntellectualStudent') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                    <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Mentor" name="mIntellectual">{{ getObjectValue($plan,'IntellectualMentor') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold" rowspan="2" >4.	Apostolic <br />
                                    <small class="font-xs text-left">
&nbsp; a.	Making a Difference: HOW?<br />
&nbsp; b.	Living as a Christian (School, Home, Community, etc.)<br />
&nbsp; c.	My Outreach Project/My Ideas
</small>
                                    </td>
                                    <td >
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Student" name="sApostolic">{{ getObjectValue($plan,'ApostolicStudent') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                    <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Mentor" name="mApostolic">{{ getObjectValue($plan,'ApostolicMentor') }}</textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
           	    </div>
                <div class="form-group" style="margin-bottom: 0px;" >
                    <div class="col-sm-12">
                		<table class="table table-bordered table-bordered-black table-condensed " id="tblothers" >
                             <thead>
                                <tr>
                                    <td class="font-blue-madison font-lg">Other</td>
                                    <td class="font-blue-madison font-lg">Goal</td>
                                </tr>
                            </thead>
                            <tbody>
                                  <tr>
                                    <td class="bold" rowspan="2" >
&nbsp; 1.	Progress<br />
&nbsp; 2.	Family<br />
&nbsp; 3.	Leadership<br />
&nbsp; 4.	Concerns<br />
&nbsp; 5.	Everest School Spirit
                                    </td>
                                    <td >
                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Student" name="sOther">{{ getObjectValue($plan,'OtherStudent') }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                    <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Mentor" name="mOther">{{ getObjectValue($plan,'OtherMentor') }}</textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
           	    </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 text-center">
                        <button class="btn btn-sm btn-success" type="button" data-menu="formation"><i class="fa fa-save"></i> Save </button>
                        <a href="{{ url('class-record/mentoring?t='. encode($term) ) }}" class="btn btn-sm btn-warning" ><i class="fa fa-ban"></i> Cancel </a>
                    </div>
                </div>
                <input type="hidden" name="refid" id="ref" value="{{ encode( getObjectValue($plan,'RefID') ) }}" />
                <input type="hidden" name="idno"  value="{{ encode($idno) }}" />
            </div>
        </form>
	</div>
</div>