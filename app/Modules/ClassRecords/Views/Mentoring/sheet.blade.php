<?php
    $service = new \App\Modules\Registrar\Services\EvaluationServices;
    $i=1;
    $fullname = isset($data->StudentName)? trimmed($data->StudentName)  : '[Complete Name]' ;
    $balance = isset($balance) ? $balance : 0 ;
    $idno = isset($data->StudentNo)? $data->StudentNo : '';
    //$grades = isset($grades) ? $grades : array() ;
    $tmp="";
    $hei = array();
    $IsSubmitted = false;
?>
<div class="row">
   <div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img class="img-responsive" alt="{{asset('assets/admin/layout/img/logo.png')}}" src="{{ url() .'/general/getPupilPhoto?Idno='. encode($idno) .'&t='. rand()  }}">
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name" id="student-name">
					 <?= $fullname ?>
				</div>
				<div class="profile-usertitle-job text-center " >
                    <center>
                        <?= getObjectValue($data, 'StudentNo') ?>
                    </center>
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<div class="profile-userbuttons">
                <?php if (  isSHS(getObjectValue($data, 'StudentNo') ) ) :  ?>
                <span class="bold font-purple">{{ getObjectValue($data, 'Major') }}</span>
                <br />
                <span class="font-xs font-grey">Strand / Track</span>
				<br />
                <?php endif; ?>
                <span class="bold">{{ getObjectValue($data, 'YearLevel') }}</span>
                <br />
                <span class="font-xs font-grey">Level</span>
                <br />
                <span class="bold">{{ getObjectValue($data, 'CurriculumCode') }}</span>
                <br />
                <span class="font-xs font-grey">Curriculum</span>
			</div>
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active"><a href="javascript:void(0);" class="views" data-menu="sheet"><i class="icon-home"></i> Mentor's Session </a></li>
					<li><a class="views" href="javascript:void(0);" data-menu="formation"><i class="fa fa-file" ></i> Formation </a></li>
                    <li><a class="views" href="javascript:void(0);" data-menu="history"><i class="icon-check"></i> History </a></li>
					<li><a class="views" href="javascript:void(0);"><i class="icon-info"></i>Help </a></li>
				</ul>
			</div>
			<!-- END MENU -->
		</div>
	</div>
	<!-- END BEGIN PROFILE SIDEBAR -->
	<!-- BEGIN PROFILE CONTENT -->
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET -->
                <div id="mentorsheet" class="tabpane">
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="fa fa-file theme-font font-blue-madison"></i>
							<span class="caption-subject theme-font font-blue-madison bold uppercase"> Mentoring Session Sheet </span>
							<span class="caption-helper hide">This module is intent for generation</span>
						</div>
						<div class="actions">
                            <button type="button" class="btn btn-default" data-menu="reload"><i class="fa fa-refresh"></i> Refresh </button>
						</div>
					</div>
					<div class="portlet-body" >
						<!-- BEGIN FORM-->
                        <form id="dataForm" class="form-horizontal form-bordered form-row-stripped" action="#" method="POST">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('errors.event')
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                		<table class="table table-bordered table-bordered-black table-condensed " id="tblareas" >
                                            <tbody>
                                                <tr>
                                                    <td width="30%" class="bold" > Session # </td>
                                                    <td class="bold">
                                                        <input type="text" readonly="" value="<?= getObjectValue($data,'SessionID') != '' ? getObjectValue($data,'SessionID') : 'Auto Generated' ?>"  placeholder="" maxlength="8"  class="form-control input-borderless input-sm">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" class="bold" > Date </td>
                                                    <td class="bold">
                                                        <input type="text" value="<?= getObjectValue($data,'TransDate') ?>" name="date" placeholder="Enter Session Date" maxlength="100"  class="form-control input-borderless input-sm">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" class="bold" > Topic </td>
                                                    <td class="bold">
                                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Topic" name="topic">{{ getObjectValue($data,'Topic') }}</textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" class="bold" > Issue Raised </td>
                                                    <td class="bold">
                                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Issue Raised" name="issue">{{ getObjectValue($data,'Issue') }}</textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" class="bold" > Student Goals </td>
                                                    <td class="bold">
                                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Issue Raised" name="goal">{{ getObjectValue($data,'Issue') }}</textarea>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td width="30%" class="bold" > Mentor will follow up by </td>
                                                    <td class="bold">
                                                        <textarea class="form-control input-borderless " style="resize:  none;" rows="4" placeholder="Mentor will follow up by" name="followup">{{ getObjectValue($data,'MentorFollowUp') }}</textarea>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                           	    </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 text-center">
                                        <button class="btn btn-sm btn-success" type="button" data-menu="save-session"><i class="fa fa-save"></i> Save </button>
                                        <a href="{{ url('class-record/mentoring?t='. encode($term) ) }}" class="btn btn-sm btn-warning" ><i class="fa fa-ban"></i> Cancel </a>
                                    </div>
                                </div>
                                <input type="hidden" name="refid" id="refno" value="{{ encode( getObjectValue($data,'SessionID') ) }}" />
                                <input type="hidden" name="term"  value="{{ encode( $term ) }}" />
                                <input type="hidden" name="prog"  value="{{ getObjectValue($data,'ProgID') }}" />
                                <input type="hidden" name="level"  value="{{ getObjectValue($data,'YearLevelID') }}" />
                                <input type="hidden" name="idno"  value="{{ encode(getObjectValue($data,'StudentNo')) }}" />
                            </div>
                        </form>
					</div>
				</div>
                </div>
                <div id="formation" class="tabpane" style="display:none;">
                    @include($views.'formation')
                </div>
                <div id="history" class="tabpane" style="display:none;">
                    @include($views.'history.index')
                </div>
			</div>
		</div>
	</div>
</div>
</div>
@include($views.'modal')