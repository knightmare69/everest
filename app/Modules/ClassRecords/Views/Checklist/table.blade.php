<?php
    $model = new \App\Modules\ClassRecords\Models\Checklist_model; 
    $table = isset($table) ? $table : array() ;  
?>
<div class="table-scrollable">
    <table class="table table-striped table-hover" style="cursor: pointer;">
        <thead>
            <tr>
                <th class="autofit"> # </th>
                <th> Section Name </th>                             
                <th> Year level </th>
<!--                 <th> # Students </th>                
                <th> Class Adviser </th>
                <th> Last Date Encoded </th>
                <th> Date Grade Posted </th>                
                <th> Options </th> -->
            </tr>
        </thead>
        <tbody>     
            @for($i=0; $i < count($table) ; $i++ )
            
            <tr data-id="<?= $table[$i]->SectionID ?>" data-section="<?= encode($table[$i]->SectionID) ?>" >
                <td class="autofit">{{ $i + 1 }}.</td>
                <td class="autofit"><?= $table[$i]->SectionName ?> </td>
                <td> <?= $table[$i]->YearLevel ?>  </td>                                                        
                <td> <a href="{{ url('class-record/checklist?open='. encode($table[$i]->SectionID) .'&period='.$period .'&term='.$term ) }}"> Checklist </a></td>
            </tr>
            @endfor
        </tbody>
    </table>
</div>