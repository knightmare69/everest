<div class="row">
<div class="col-sm-12">
    @if($facultyId == '')
        <div class="note note-danger">
    		<h4 class="block">Warning! You have no IEncode ID</h4>
    		<p>
            Please contact your system administrator for your faculty id.
    		</p>
    	</div>   
    @else

    <div class="portlet bordered light ">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-calendar"></i> Kindergarten School Check-list </div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>				
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="portlet-body">
            <div class="form-inline">                              
            <select class="bs-select form-control input-medium " id="term" name="term" >
                <option value='-1'> - Select - </option>                
                @if(!empty($academic_year))
                    @foreach($academic_year as $ayt)
                        <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AYTerm }} </option>                                                                 
                    @endforeach
                @endif                                           
            </select>
            <select class="form-control input-medium " id="period" name="period" >
                <option value="-1"> - Select - </option>                
                <option value="1"> 1ST QUARTER</option>
                <option value="2"> 2ND QUARTER</option>
                <option value="3"> 3RD QUARTER</option>
                <option value="4"> 4TH QUARTER</option>                                           
            </select>
            
            <button class="btn btn-default" type="button" data-menu="refresh"><i class="fa fa-refresh"></i> </button>
            </div>
            <hr style="margin-top: 10px; margin-bottom: 5px;" />
            <div id="myschedules">
                @include($views.'table')
            </div>                                        
		</div>
	</div>                                 
    @endif    	
</div>
</div>