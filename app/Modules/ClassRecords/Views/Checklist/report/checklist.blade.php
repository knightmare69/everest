<style>
    .autofit{width:1%;white-space:nowrap !important;}
	.border-bottom {
		border-bottom: 1px solid #000
	}
	.border {
		border: 0.5px solid #000
	}
	.right {
		text-align: right;
	}
	.bold {
		font-weight: bold;
	}
	.center {
		text-align: center;
	}
	.left {
		text-align: left;
	}
	.valign {
		vertical-align: middle;
	}
	.bg {
		background-color: #EDEBE0;
	}
	.pad-5 {
		padding: 10px;
	}
</style>
<?php
	if (!function_exists('setSpace')) {
		function setSpace($s = 2) {
			return str_repeat('&nbsp;', $s);
		}
	}
    
    $model = new \App\Modules\ClassRecords\Models\Checklist_model;
    $students = $model->Students($sec_id, $period );
    $template = ($yearlevel == '2'? 1 : 2);
    $domain  = $model->DomainChecklist($template,$period);
    
    $data = [];
	$i=1;
?>

<table class="" width="100%" border='1'>
	<tbody>
		<tr>
			<td>
                <b>PGS Chechlist Report Card</b> 
            </td>
		</tr>	
	</tbody>
</table>
<br /><br />

<table class="" width="100%" border='1'>
    <thead>
        <tr>
            <th class="border center " width="20" > #</th>
            <th class="border"> &nbsp; Domain</th>
            <th></th>
        </tr>
    </thead>
	<tbody>
		@foreach($students AS $r)
        <tr>
            <td class="border center" width="20"><small>{{$i.'.'}}</small> </td>       
            <td class="border"  data-idno="{{encode($r->StudentNo)}}">                            
                {{$r->StudentName}}
            </td>
        </tr>            
        <?php $i++; ?>
        @endforeach                    
	</tbody>
</table>