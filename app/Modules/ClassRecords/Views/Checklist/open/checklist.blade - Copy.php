<?php
    $model = new \App\Modules\ClassRecords\Models\Checklist_model;    
    $students = $model->Students($sec_id, $period );
    $template = ($yearlevel == '2'? 1 : 2);
    $domain  = $model->DomainChecklist($template,$period);
    $i=1;    
    $class = 0;
    $sub = 0;
    $cri = 0;
?>
<style>
    .criteria{
        height: 55px !important;
        background-color: #8775a7;
    color: white;
    }
    .domain{
        height: 55px !important;
        overflow: hidden;
    }
</style>
<div class="" style="overflow: hidden; float: left; width: 384px; ">
<table class="table table-bordered table-condensed" data-year="{{$yearlevel}}" style="cursor: pointer; margin-bottom: 0px; width: 384px ; " >
    <thead >
     <tr id="student_header_r" style="height: 87px;" >
        <th><div style=" text-align: center; width: 8px;">#</div></th>
        <th>
            <div style="width: 200px; text-align: center;">
                Domain
            </div>                            
        </th>
     </tr>    
     </thead>
</table>
<div id="domaincols"  style=" float: left; width: 385px;  height: 400px !important; overflow-y: hidden; overflow: -moz-scrollbars-horizontal; overflow-x: scroll; " >
<table class="table table-bordered table-striped" id="tbldomain" style="z-index: 999;">    
    <tbody>
        @foreach($domain AS $d)
        
        @if($class != $d->ClassID)
        <tr class="info">            
            <td colspan="2" height="15">                        
                <div class="bold font-lg" style=" text-align: center;   line-height: .80;    height: 15px !important;">
                    {{$d->ClassName}}
                </div>                
            </td>                                    
        </tr>
        <?php $class = $d->ClassID; $i = 1; ?>
        @endif
        
        @if($sub != $d->SubClassID)
        <tr class="warning">            
            <td colspan="2">                        
                <div class="bold autofit " style=" overflow: visible; text-align: center;   line-height: .80;    height: 15px !important;">
                    {{$d->SubClass}}
                </div>                
            </td>                                    
        </tr>
        <?php $sub = $d->SubClassID; $i = 1; ?>
        @endif
        
        @if($cri != $d->CriteriaID && $d->Descriptive !="" )
        <tr class="" style="">    
            <td><div style="width: 15px;">{{$i}}.</div></td>
            <td class="criteria">                            
                <div class="bold ">
                    {{$d->Criteria}}
                </div>                
            </td>                                    
        </tr>
        <?php $cri = $d->CriteriaID; $i++; ?>
        @endif        
        
        <tr>            
            <td><div style="width: 15px;"></div></td>
            <td class="" height="15">            
                <div class="" >
                    <?= $d->Descriptive != "" ? $d->Descriptive : $d->Criteria ?>                    
                </div>                
            </td>                                    
        </tr>    
        @endforeach        
    </tbody>
</table>
</div>
</div>
<div id="mytblheader" class="" style="overflow-x: hidden; overflow: -moz-scrollbars-vertical; overflow-y: scroll;">
<table class="myheader table table-bordered" style="margin-bottom: 0px;">
    <thead>
        <tr>            
            @foreach($students AS $r)
            <th class="text-center"  data-idno="{{encode($r->StudentNo)}}">
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($r->StudentNo) }}" class="img"  height="50" width="50" alt="">
                <span class="autofit" >{{$r->LName}}</span>                
            </th>            
            @endforeach
        </tr>
    </thead>    
</table>
</div>
<!-- DETAILS -->
<?php $class = 0; $cri = 0; ?>
<div class="dataTables_scrollBody"  style=" margin-left: 304px !important; overflow-x: scroll;  overflow-y: scroll; height: 400px !important; " onscroll="scrollme(this);"  >
<table class="table table-bordered table-striped" id="tblbody" style="cursor: pointer;">
    <tbody>
        @foreach($domain AS $d)
        
            @if($class != $d->ClassID)
            <tr class="info">            
                <td colspan="{{ count($students) }}">                            
                    <div class="bold font-lg" style=" text-align: center;   line-height: .80;    height: 15px !important;">
                    
                    
                    
                    </div>                
                </td>                                    
            </tr>
            <?php $class = $d->ClassID; ?>
            @endif
             @if($sub != $d->SubClassID)
            <tr class="warning">            
                <td colspan="{{ count($students) }}" class="text-center">                        
                    <div class="bold  autofit text-center" style=" line-height: .80;    height: 15px !important;">
                        
                    </div>                
                </td>                                    
            </tr>
            <?php $sub = $d->SubClassID; $i = 1; ?>
            @endif
            @if($cri != $d->CriteriaID && $d->Descriptive !="" )
        <tr class="">    
            
            <td colspan="{{ count($students) }}" class="criteria">                            
                <div class="bold" >
                    
                </div>                
            </td>                                    
        </tr>
        <?php $cri = $d->CriteriaID; $i++; ?>
        @endif        
                        
        <tr data-idx="{{encode($d->ChkID)}}" >
            @foreach($students AS $r)     
            <td tabindex="-1" class="text-center domain xgrade editable" data-name="{{$r->StudentName}}"  data-reg="{{ encode($r->RegID) }}"  data-idno="{{ encode($r->StudentNo) }}">
                <div class="bold" >
                <?php 
                    $g = $model->Grade($sec_id, $r->StudentNo,$period, $d->ChkID)->first();
                    echo $g['Grade'] ; 
                ?> 
                </div> 
            </td>
            @endforeach 
        </tr>
        @endforeach        
    </tbody>
</table>
</div>