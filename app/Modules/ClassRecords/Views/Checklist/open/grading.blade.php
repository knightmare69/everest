<div id="grading_modal" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog" ><div class="modal-content">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Prep School Grading System</h4></div>
<div class="modal-body">
    <?php       
        $gradetbl = ( isset($gradetbl) ? $gradetbl : 0 );
        $data = App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID',$gradetbl)->get();     
    ?>
	<table id="tblgrading" data-id="{{getSetting(8)}}" class="table table-condensed  table-hover" style="cursor: pointer !important;">
        <thead>
            <th>#</th>
            <th>Min</th>
            <th>Max</th>
            <th class="autofit">Letter Grade</th>
            <th class="">Description</th>
            <th class="text-center ">Remarks</th>
                        
        </thead>
        <tbody>
        @for($i=0; $i < count($data) ; $i++ )
            @if( $data[$i]->Inactive == 0 )
            <tr data-template="<?= encode($data[$i]->TemplateID) ?>" data-min="<?= $data[$i]->Min ?>" data-max="<?= $data[$i]->Max ?>"
                data-letter="<?= $data[$i]->LetterGrade ?>" data-desc="<?= $data[$i]->Description ?>"
                data-remarks="<?= $data[$i]->Remark ?>" data-id="<?= encode($data[$i]->IndexID) ?>"
            >
                <td class="autofit font-xs">{{$i + 1}}.</td>
                <td class="bold text-primary min"><?= $data[$i]->Min ?></td>
			    <td class="bold text-primary max "><?= $data[$i]->Max  ?></a></td>
                <td class="bold letter "><?= $data[$i]->LetterGrade  ?> </a></td>
                <td class="autofit desc "><?= $data[$i]->Description ?></td>
                <td class="remarks "><?= $data[$i]->Remark ?></td>	
                                 
            </tr> 
            @endif
            
        @endfor        
        </tbody>
    </table>	
</div>
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
</div></div></div></div>