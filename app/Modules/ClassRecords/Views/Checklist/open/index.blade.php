<div class="row">
<div class="col-sm-12">
	<div class="portlet bordered light ">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-users"></i> Section Name : <span class="font-blue-madison">{{$section or '[Section Name]' }} </span> </div>
			<div class="actions">
                <button type="button" class="btn btn-default btn-sm" data-menu="post" > <i class="fa fa-lock"></i> Post </button>
                <button type="button" class="btn btn-default btn-sm" data-menu="print" > <i class="fa fa-print"></i> Print </button>
				<button type="button" class="btn btn-default btn-sm" data-menu="grading" > <i class="fa fa-table"></i> Grading System </button>
                <a href="{{url('class-record/checklist')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back </a>
			</div>
		</div>
		<div class="portlet-body">
            <input type="hidden" value="{{$period}}" id="period" />
            <input type="hidden" value="{{ encode($sec_id) }}" id="section" />                  
            <div id="checklist" class="" >
                @include($views.'open.checklist')
            </div>
            <label id='tbleditor' style='display: none;' class=' input table-editor' data-table="tblgrade">
                <input value="0" class="form-control input-sm text-center " maxlength="9" style="border: none;" />
            </label>                                                                              
		</div>
	</div>
</div>
</div>
@include($views.'open.grading')