<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user-md"></i> Faculty List
        </div>
        <div class="actions"></div>
    </div>
    <div class="portlet-body table_wrapper" id="">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Academic Term</label>
                    <select class="select2 form-control" name="academic-year" id="academic-year">
                        <option value=""></option>
                        @if(!empty($incl['ac']))
                            <!-- <option value="" disabled selected>Please select term</option> -->
                            @foreach($incl['ac'] as $ac)
                            <option value="{{ encode($ac->TermID) }}">{{ $ac->AcademicYear.' - '.$ac->SchoolTerm }}</option>
                            @endforeach
                        @else
                        <option value="">No academic year found.</option>
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Period</label>
                    <select class="select2 form-control" name="period" id="period" >
                        <option value="" disabled selected></option>
                        <option value="1" class="mgs hide">1st Quarter</option>
                        <option value="2" class="mgs hide">2nd Quarter</option>
                        <option value="3" class="mgs hide">3rd Quarter</option>
                        <option value="4" class="mgs hide">4th Quarter</option>
                        <option value="11" class="shs hide">1st Term</option>
                        <option value="12" class="shs hide">2nd Term</option>
                        <option value="14" class="summer hide">Summer</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="checkbox">
					<label><input type="checkbox" id="show-sy-only"> Show school year only.</label>
				</div>
            </div>
        </div>
        <hr style="margin: 10px 0">
        <div class="row hide">
            <div class="col-md-12">
                <div class="input-group">
                    <input type="text" class="form-control stud-find" data-snum="" name="faculty-find" id="faculty-find" placeholder="Search faculty...">
                    <span class="input-group-btn">
                        <button type="button" class="faculty-search-btn btn btn-default stud-find" id="faculty-find-btn">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="row hide">
            <div class="col-md-12">
                <ul class="list-unstyled general-item-list" id="faculty-list" style="max-height: 600px;overflow-y: auto;"></ul>
                <p class="text-center faculty-items-last no-margin hide" style="border-top: 1px solid #F1F4F7">
                    <a href="#" onClick="return false;" id="faculty-list-get"><i class="icon-list"></i> Load More...</a>
                </p>
            </div>
        </div>
    </div>
</div>
