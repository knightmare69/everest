<style media="screen">
.item.active{
    background: rgba(221,221,221,0.5);
}
.item-details{
    width: 100%;
}
.item-details a{
    width: 76%;
    white-space: nowrap;
    overflow: hidden;
    display: block;
    text-overflow: ellipsis;
    margin-right: none;
}
.item-details span{
    display: block;
}
.general-item-list > .item > .item-head > .item-details > .item-label{
    color: #747f8c;
}
.general-item-list > .item > .item-head > .item-details > .item-name{
    margin-right: 0;
}
/* note add*/
.arrow_box {
	position: absolute;
    padding: 10px;
	background: #fff;
	border: 2px solid #930000;
}
.arrow_box:after, .arrow_box:before {
	top: 100%;
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.arrow_box:after {
	border-color: rgba(255, 255, 255, 0);
	border-top-color: #fff;
	border-width: 15px;
	margin-left: -15px;
}
.arrow_box:before {
	border-color: rgba(147, 0, 0, 0);
	border-top-color: #930000;
	border-width: 18px;
	margin-left: -18px;
}
/* End */
.wtBorder {
    font-size: 0;
    position: absolute;
}

.cPercentage, .cTotalItems, .cWeight {
    font-weight: bold;
    text-align: center;
}
.sPercentage, .sTotalItems, .sWeight, .InitialGrade, .TransmutedGrade {
    font-weight: bold;
    text-align: center;
}
.eventScore{
    text-align: center;
    min-width: 35px;
}

.align-middle{
    vertical-align: middle !important;
}
#ibed-table tbody tr td {
    white-space: nowrap;
}
.r-border-grey{
    border-right-color: #ddd !important;
}
</style>
<div class="row">
    <div class="col-md-3">
        @include($views.'.left')
    </div>
    <div class="col-md-9">
        @include($views.'.right')
    </div>
</div>

<div class="arrow_box hide" id="div-note-msg">
    <div class="form-group">
		<label>Note</label>
		<textarea class="form-control" rows="3"></textarea>
	</div>
    <div class="row">
        <div class="col-md-12 right">
            <button class="btn btn-defaul btn-sm" type="button" id="note-btn-close"> Close </button>
            <button class="btn btn-success btn-sm right" type="button" data-menu="event">Save</button>
        </div>
    </div>
</div>