<table class="table table-striped table-bordered table-hover dataTable no-footer" id="ibed-table">
    <thead>
        <tr>
            <th rowspan="2" class="align-middle r-border-grey">#</th>
            <th rowspan="2" class="align-middle r-border-grey">Pic</th>
            <th rowspan="2" class="align-middle r-border-grey">Student Name</th>
            <th rowspan="2" class="align-middle r-border-grey">Gender</th>
            <th colspan="2" scope="colgroup" class="r-border-grey text-center info">{{ $period }} Quarter</th>
            <th rowspan="2" class="align-middle success">Date Posted</th>
        </tr>
        <tr>
            <th scope="col" class="r-border-grey bg-yellow-casablanca">Final Grade</th>
            <th scope="col" class="r-border-grey bg-yellow-casablanca">Conduct Grade</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
            @foreach($data as $key => $_this)
            <tr class="warning">
                <td>{{ $key + 1 }}</td>
                <td><img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($_this->StudentNo) }}" class="img"  height="25" width="25" alt=""></td>
                <td>{{ ucwords(strtolower($_this->Fullname)) }}</td>
                <td>{{ $_this->Gender }}</td>
                <td>{{ number_format($_this->Grade, 2) }}</td>
                <td>{{ $_this->Conduct }}</td>
                <td>{{ date('Y-m-d', strtotime($_this->DatePosted)) }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
