<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <label class="control-label">Sections (Subject)</label>
                <select class="select2 form-control" name="sections" id="sections"></select>
            </div>
            <div class="col-md-4">
                <p><b>Subject Title:</b></p>
                <span id="selected-subj-title" class="bold red-flamingo"></span>
            </div>
        </div>
    </div>
</div>
