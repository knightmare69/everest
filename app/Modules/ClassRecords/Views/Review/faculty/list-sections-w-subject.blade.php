<option value=""></option>
@foreach($data as $_this)
    <option value="{{ $_this->ScheduleID }}" data-subj="{{ encode($_this->SubjectID) }}" data-subj-title="{{ $_this->SubjectTitle }}" data-year-level="{{ $_this->YearLevelName }}" data-prog="{{ $_this->ProgramID }}">
        {{ $_this->SectionName }} ({{ $_this->SubjectCode }})
    </option>
@endforeach
