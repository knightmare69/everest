<?php //error_print($empl); die();?>
@if(!empty($empl))
    @foreach($empl as $_this)
    <?php $fullname = ucwords(strtolower($_this->LastName.', '.$_this->FirstName.' '.$_this->MiddleInitial))?>
    <li class="item">
        <div class="item-head">
            <div class="item-details">
                <img src="" height="35" width="35" alt="Photo" class="item-pic pull-left"/>
                <a class="item-name primary-link faculty-names" title="{{ $fullname }}" data-id="{{ encode($_this->EmployeeID) }}">{{ $fullname }}</a>
                <span class="item-label">
                    {{ empty($_this->DeptName) ? '' : $_this->DeptName  }}
                </span>
            </div>
        </div>
    </li>
    @endforeach
@else
    <li><p class="text-center text-muted" style="margin-top:10px;">
        No Result(s) found.
    </p></li>
@endif
