<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user-md"></i> Class Record Reviewer<span id="faculty-selected-name"></span>
        </div>
        <div class="actions">
            <button type="button" class="btn btn-sm btn-default" id="btn-refresh"><i class="fa fa-refresh "></i></button>
            <button type="button" class="btn btn-sm btn-default fullscreen " data-original-title="" title=""><i class="fa fa-expand "></i></button>
            <button class="btn btn-default btn-sm hide" type="button" data-menu="post-grade" data-mode="post" id="post-grade"><i class="fa fa-lock"></i> Post </button>
            <button class="btn btn-default btn-sm hide" type="button" data-menu="edit-request" data-mode="post" id="post-grade"><i class="fa fa-lock"></i> Edit Request </button>
        </div>
    </div>
    <div class="portlet-body table_wrapper " id="faculty-class-record">
        @include($views.'faculty.info')
        <hr>
        <div id="show-message"></div>
        <div class="row">
            <div class="col-md-12" id="myschedules" ></div>
            <label id='tbleditor' style='display: none;' class=' input table-editor' data-table="tblgrade">
                <input value="0" class="form-control input-sm text-center " maxlength="9" style="border: none;" />
            </label>
        </div>
        <div class="table-responsive hide" id="grading-system-table">
            @include(str_replace('Review', 'eClassRecords', $views).'gradetbl.table')
        </div>
        <div class="hide">
            @include(str_replace('Review', 'eClassRecords', $views).'gradetbl.transmutation')
        </div>
    </div>
</div>
