<?php       
$transid = ( isset($transid) ? $transid : 0 );
$data    = App\Modules\GradingSystem\Models\Transmutation\Transtable_model::where('TemplateID',$transid)->orderby('Max','DESC')->get();        
?>
<table id="tbltransmutation" class="table table-condensed  table-hover" style="cursor: pointer !important;">
    <thead>
        <th>#</th>
        <th>Min</th>
        <th>Max</th>                        
        <th class="">Transmuted Grade</th>        
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        <tr data-template="<?= encode($data[$i]->TemplateID) ?>" data-min="<?= $data[$i]->Min ?>" data-max="<?= $data[$i]->Max ?>"
            data-grade="<?= $data[$i]->TransmutedGrade ?>"
            data-id="<?= encode($data[$i]->IndexID) ?>"
        >
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="min"><?= $data[$i]->Min ?></td>
            <td class="max"><?= $data[$i]->Max ?></td>			    
            <td class="grade"><?= $data[$i]->TransmutedGrade  ?> </a></td>             
        </tr> 
    @endfor        
    </tbody>
</table>	                    	
