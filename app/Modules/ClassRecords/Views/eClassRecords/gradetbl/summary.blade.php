<?php           
    $tblid = isset($tblid) ? $tblid : 0 ;    
    $data = App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID',$tblid)->get();     
?>
<table class="table table-condensed  table-bordered table-striped table-hover" style="cursor: pointer !important;">
    <thead>
        <th>#</th>
        <th>Descriptor</th>        
        <th class="autofit">Male</th>
        <th class="autofit">Female</th>
        <th class="text-center ">Total</th>                    
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        @if( $data[$i]->Inactive == 0 )
        <tr data-template="<?= encode($data[$i]->TemplateID) ?>" data-min="<?= $data[$i]->Min ?>" data-max="<?= $data[$i]->Max ?>"
            data-letter="<?= $data[$i]->LetterGrade ?>" data-desc="<?= $data[$i]->Description ?>"
            data-remarks="<?= $data[$i]->Remark ?>" data-id="<?= encode($data[$i]->IndexID) ?>"
        >
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="bold text-primary ">
                <?= $data[$i]->Description ?>
                <br />
                <?= $data[$i]->Min . '% - ' . $data[$i]->Max . '%' ?>
            </td>
            <td class="male">0</td>
            <td class="female">0</td>
            <td class="total">0</td>		    
        </tr> 
        @endif        
    @endfor        
    </tbody>
</table>