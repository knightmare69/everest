<?php
    $yearterm = '';
    $section = '';
?>
<div class="row"><div class="col-sm-12">
<div class="portlet clearfix light" id="main-portlet" style="margin-bottom: 0px;">
<div class="portlet-title">
	<div class="caption">
        <span class="caption-subject bold text-primary"><i class="fa fa-star"></i> eClass-Records</span> <br />
		<span class="caption-helper text-info hidden-xs hidden-sm hidden-md ">Use this module for your class record management...</span>                                    		
	</div>                
	<div class="tools">
        <div class="portlet-input input-inline input-medium">
            <select class="form-control input-sm" name="term" id="term">
                <option value="-1"> - Select Academic Year - </option>
                @if(!empty($academic_year))
                    @foreach($academic_year as $ayt)
                        @if($yearterm != $ayt->TermID )

                        <?php $yearterm = $ayt->TermID;  ?>

                        <option value="{{encode($ayt->TermID)}}" <?= $ayt->TermID == $term ? 'selected':'' ?> > {{ $ayt->AYTerm }} </option>
                        @endif
                    @endforeach
                @endif
            </select>
        </div>                
        <div class="portlet-input input-inline input-medium">
            <select class="form-control input-sm" name="schedule" id="schedule">
                <option value="-1"> - Select Subject - </option>
                @if(!empty($academic_year))
                    @foreach($academic_year as $ayt)                                                

                        
                        <option <?= $ayt->TermID == $term ? '' :'style="display: none !important;"' ?> value="{{($ayt->ScheduleID)}}" 
                        

                                data-subj="{{encode($ayt->SubjectID)}}"
                                data-code="{{ $ayt->SubjectCode }}"
                                data-title="{{ $ayt->SubjectTitle }}"
                                data-policy="{{ $ayt->PolicyName }}" data-zerobased="{{ $ayt->IsZeroBased }}" 
                                data-term="{{encode($ayt->TermID)}}" >{{$ayt->SectionName}} - {{$ayt->SubjectCode}}</option>                        
                    @endforeach
                @endif
            </select>
        </div> 
        <div class="portlet-input input-inline input-small">
            <select class="form-control input-sm" name="period" id="period">
                <option value="-1"> - Select Period - </option>
                <option value="1"> 1ST QUARTER</option>
                <option value="2"> 2ND QUARTER</option>
                <option value="3"> 3RD QUARTER</option>
                <option value="4"> 4TH QUARTER</option>
            </select>
        </div>                       
        <button type="button" class="btn btn-sm btn-default" data-menu="refresh" ><i class="fa fa-refresh "></i></button>
		<button type="button" class="btn btn-sm btn-default fullscreen " ><i class="fa fa-expand "></i></button>                
	</div>
</div>
<div class="portlet-body">
    <div class="row" >        
        <div class="col-sm-4">                
            <span class="bold hidden-xs hidden-sm hidden-md" id="SubjectName" >[Subject Info]</span> <br />
            <span id="PolicyName" class="text-primary bold hidden-xs hidden-sm hidden-md">[Policy]</span>
        </div>
        <div class="col-sm-8 text-right">
            
            <div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn btn-sm green" aria-expanded="false">
				<i class="fa fa-table"></i> Grading Table <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#" class="clink" data-menu="table" data-mode="conduct" >						
                        <i class="fa fa-pencil"></i> Effort </a>
					</li>
					<li>
						<a href="#" class="clink" data-menu="table" data-mode="homeroom">
						<i class="fa fa-home"></i> Home Room </a>
					</li>
					<li>
						<a href="#" class="clink" data-menu="table" data-mode="club">
						<i class="fa fa-users"></i> Club </a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#" class="clink" data-menu="table" data-mode="grades">
						<i class="fa fa-star"></i> Grades </a>
					</li>
				</ul>
			</div>
            <div class="btn-group ">
				<button class="btn btn-default btn-sm" type="button" data-menu="upload"><i class="fa fa-upload"></i> Upload </button>
				<button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" type="button" aria-expanded="false"><i class="fa fa-angle-down"></i></button>
				<ul role="menu" class="dropdown-menu ">
					<li><a href="#" class="clink" data-menu="layout" > Download Student List </a></li>
				</ul>
			</div>
            <button class="btn btn-info btn-sm" type="button" data-menu="event" data-mode="add" ><i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Add Column</span></button>
            <button class="btn btn-danger btn-sm" type="button" data-menu="event" data-mode="remove" ><i class="fa fa-times"></i> <span class="hidden-xs hidden-sm">Remove Column</span> </button>
            <button class="btn btn-default btn-sm" type="button" data-menu="recompute"><i class="fa fa-calculator"></i> Recompute </button>
            <button class="btn btn-default btn-sm" type="button" data-menu="post-grade" data-mode="post" ><i class="fa fa-lock"></i> Submit </button>
            <button class="btn btn-default btn-sm" type="button" data-menu="print"><i class="fa fa-print"></i> Print </button>                                                                                        
        </div>                
    </div>
    <hr style="margin-top: 8px; margin-bottom: 5px;" />            
    <div class="row">
        <div class="com-sm-12">        
        <div class="profile-sidebar hidden-sm hidden-xs hidden-md">
            <div class="portlet light bordered  profile-sidebar-portlet ">    			
    			<div class="profile-userpic"><img alt="" class="img-responsive" src="{{ asset('assets/admin/layout/img/logo.b.png') }}"></div>
    			<br />				
				<div class="profile-usertitle text-center">
					<div class="profile-usertitle-name"> [Student Name] </div>
                    <div class="profile-usertitle-job"> [Student No.] </div>
                    <div class="text-danger bold font-lg profile-final-grade"> [Final Grade] </div>
                    <div class="text-danger bold font-lg profile-club hidden"> [Club] </div>
				</div>
                
                <!-- <div class="profile-userbuttons ">					
                    <button class="btn btn-circle btn-warning btn-sm" data-menu="varsity" type="button">Varsity</button>								
				</div> -->	
                <!-- disabled varsity in everest grading system -->
                <div class="profile-usermenu">
					<ul class="nav">
						<li class="active"><a href="#" class="views" data-menu="class-record"><i class="icon-home"></i> Class Record </a></li>
						<li><a  href="#" class="views" data-menu="summary" ><i class="icon-check"></i>Summary </a></li>
						<li><a href="#" class="views" data-menu="help"><i class="icon-info"></i>Transmutation</a></li>
					</ul>
				</div>                                						    							
			</div>
        </div>        
            <div id="class-record-container">
                <div id="myschedules" class="clearfix"   >
                    @include($views.'table')
                </div>
            </div>   
            <div id="summary-container"style="display: none;">
                <div class="col-sm-9">                
                    <label class="text-primary bold">Summary</label>
                    <div class="table-responsive">
                        @include($views.'gradetbl.summary')
                    </div>
                </div>                                                
            </div>
            <div id="help-container" style="display: none;">
                <div class="row">
                    <div class="col-sm-4">
                        <label class="text-primary bold">Transmutation</label>
                        <div class="table-responsive">
                            <div style=" height: 400px !important; ">
                                @include($views.'gradetbl.transmutation')
                            </div>                            
                        </div>                                
                    </div>
                    <div class="col-sm-4">
                        <label class="text-success bold">Grading System</label>
                        <div class="table-responsive" id="grading-system-table">
                            @include($views.'gradetbl.table')
                        </div>                                
                    </div>
                </div>                
            </div>                
        </div>                                           
    </div>    
    <label id='tbleditor' style='display: none;' class=' input table-editor' data-table="tblgrade">
        <input value="0" class="form-control input-sm text-center " maxlength="9" style="border: none;" />
    </label>                      
    <div class="clearfix"></div>                  
</div>
</div></div></div>
<div id="event_modal" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog" ><div class="modal-content">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Event Manager</h4></div>
<div class="modal-body"></div>
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
    <button class="btn btn-default btn-event grp-add" data-menu="event" data-mode="reset" type="button">Reset</button>
	<button class="btn btn-success btn-event grp-add" data-menu="event" data-mode="save" type="button">Save Column</button>
    <button class="btn btn-danger btn-event grp-rem" data-menu="event" data-mode="delete" type="button">Remove Column</button>
</div></div></div></div>
<div id="gradetbl_modal" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog" ><div class="modal-content">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Grading System</h4></div>
<div class="modal-body">
    @include($views.'gradetbl.table')    	
</div>
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
</div></div></div></div>
@include($views.'gradetbl.conduct')
@include($views.'upload')
@include($views.'events.varsity')