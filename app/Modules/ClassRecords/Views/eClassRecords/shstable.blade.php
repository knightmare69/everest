<?php
    ini_set('max_execution_time', 300);
    $transid     = ((isset($transid))? $transid : 0 );
    $xtransdata  = new \App\Modules\GradingSystem\Models\Transmutation\Transtable_model;  
    $model       = new \App\Modules\ClassRecords\Models\eClassRecord_model;
    $conduct     = new \App\Modules\ClassRecords\Models\Conduct\Conduct_model;

    $i           = 0;
    $j           = 1;
    $gd          = 1;
    $component1  = '5';
    $quarter     = 1;
    $totalitems  = 0;
    $conductype  = 1;
    $shs         = (isset($shs) ? $shs : 0 );
    $transevent  = 1;
    $weightscore = 0;
    $showtotal   = 0;
    //error_print($xtransdata->TransmuteID(0));

    $css_semestral = ($period == '12' ? '' : 'hide');    
    $view_conduct = isset($IsConduct) ?  $IsConduct : ($period == '14' ? '0':'1' );
?>
@include('ClassRecords.Views.eClassRecords.class_record.studentheader')
<div id="mytblheader" class="" style="overflow-x: hidden; overflow: -moz-scrollbars-vertical; overflow-y: scroll;">
<table class="myheader" style="cursor: pointer; margin-bottom: 0px;  " >
    <thead >
     <tr>
        <th rowspan="5" class="autofit hide" ><input type="checkbox" class="chk-header"></th>
        <th rowspan="5" class="autofit hide" ><div style='width: 15px; text-align: center; ' >#</div></th>
        <th rowspan="5" class="autofit hide"><div style='width: 25px; text-align: center; ' >Pic</div></th>
        <th rowspan="4" class="autofit hide">
            <div class="bold text-center " style='width: 280px !important; text-align: center;'>
            Student Name
            </div>
        </th>
        <th rowspan="4" class="autofit text-center bold hidden"><div class="cgender" >Sex</div></th>
        <th rowspan="4"  class="vertical-container" width="35" style="min-width:45px !important;"><div class="ClassStand"><span class="vertical-text">Current Standing</span></div></th>
        <th rowspan="4"  class="vertical-container" width="35"><div class="InitialGrade"><span class="vertical-text">Qtrly Grade</span></div></th>
        <th rowspan="4"  class="vertical-container <?php echo (($transevent==1)?'hidden':'');?>" width="35"><div class="PeriodGrade "><span class="vertical-text">Transmuted</span></div></th>
        <th rowspan="4" class="vertical-container" width="45" >
            <div class="Descriptor" style='text-align: center; ' ><span class="vertical-text">Descriptor</span></div>
        </th>
        <th rowspan="4" class="vertical-container {{$css_semestral}}" width="35" >
            <div class="Descriptor" style='text-align: center; ' ><span class="vertical-text">Semestral<br>Grade</span></div>
        </th>

        @if(!empty($components))
            @forelse($components AS $c => $v)
                @if( count($v['Child']) > 0 )
					<?php

				     $v['Caption'] = ((strpos($v['Caption'],'<br>')!==false)?$v['Caption']:str_replace('/','/<br>',$v['Caption']));

				    ?>
                    <th data-id="{{ $v['ComID'] }}" colspan="{{ $v['TotalEvents']+(($weightscore==0)?0:1) }}" class="text-center bold event-label "><div style='width:32px;overflow: hidden;' title="{{ $v['Caption'].' '.$v['Weight'].'%' }}">{{ $v['Caption'].' '.$v['Weight'].'%' }}</div></th>
                @else
                    <th data-id="{{ $v['ComID'] }}" colspan="{{ $v['TotalEvents']+(($weightscore==0)?1:2)+($showtotal) }}" rowspan="2" class="text-center bold event-label "> <div style='width:32px;overflow: hidden;' title="{{ $v['Caption'].' '.$v['Weight'].'%' }}">{{ $v['Caption'].' '.$v['Weight'].'%' }}</div></th>
                @endif

            @empty
                <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
            @endforelse
        @else
            <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
        @endif
        @if($view_conduct == '1')
        <th colspan="{{$conductype+1}}" rowspan="2" class="text-center bold"> Effort </th>
        @endif
        <!-- <th colspan="3" rowspan="2" class="text-center bold">Conduct</th> -->
        <th rowspan="5"> <div class="pdclass" >Note</div> </th>
        <th rowspan="5"> <div class="pdclass" >Date Posted</div> </th>

    </tr>
    <tr>
        @if(!empty($components))
            @foreach($components AS $c => $v)
                @if( count($v['Child']) > 0 )
                    @foreach($v['Child'] As $a => $b)
                    <th data-id="{{ $b['ComID'] }}" data-parent="{{$v['ComID']}}" colspan="{{ count($b['Events']) + (($weightscore==0)?2:3) }}" class="text-center csubcomp bold ">{{ $b['Caption']. ' ' . $b['Weight'].'%' }}</th>
                    @endforeach
                    <td rowspan="4" class="text-center bold autofit success">
                        <div style='width: 35px; text-align: center; ' >AVE.</div>
                    </td>
                @endif
            @endforeach
        @endif
    </tr>

    <tr>
        @if(!empty($components))
            @forelse($components AS $c => $v)

                @if( count($v['Child']) > 0 )
                    @foreach($v['Child'] As $a => $b)

                        @for($i=1; $i<=count($b['Events']);++$i)
                          <td class="" >
                            <div style='width: 32px; text-align: center; ' >
                                <a data-id="{{ $b['Events'][$i]['EventID']}}"  class="clink event-name" data-component="{{$b['ComID']}}" data-menu="event" data-mode="edit" href="javascript:void(0);">
                                    <?= trimmed($b['Events'][$i]['EventName']) == '' ? $i : $b['Events'][$i]['EventName'] ;  ?>
                                </a>
                            </div>
                          </td>
                        @endfor
                        <td rowspan="2" class="text-center bold autofit info <?php echo (($showtotal==0)?'hidden':'');?>"><span class="">Total</span> </td>
                        <td rowspan="2" class="text-center bold autofit success"><span class="">PS</span> </td>
                        <td rowspan="2" class="text-center bold autofit success <?php echo (($weightscore==0)?'hidden':'');?>"><span class="">WS</span> </td>

                    @endforeach
                @else

                    @for($i=1; $i<=count($v['Events']);++$i)
                      <td class="text-center bold " data-id="{{ $v['Events'][$i]['EventID']}}" >
                        <div style='width: 32px; text-align: center; ' >
                            <a data-id="{{ $v['Events'][$i]['EventID']}}" class="clink event-name" data-component="{{$v['ComID']}}" data-menu="event" data-mode="edit" href="javascript:void(0);">
                                <?= trimmed($v['Events'][$i]['EventName']) == '' ? $i : $v['Events'][$i]['EventName'] ;  ?>
                            </a>
                        </div>
                      </td>
                    @endfor
                    <td rowspan="2" class="text-center bold autofit info <?php echo (($showtotal==0)?'hidden':'');?>"><span class="">Total</span> </td>
                    <td rowspan="2" class="text-center bold autofit success"><span class="">PS</span> </td>
                    <td rowspan="2" class="text-center bold autofit success <?php echo (($weightscore==0)?'hidden':'');?>"><span class="">WS</span> </td>

                @endif


            @empty
                @for($i=1; $i<=$component1;++$i)
                  <td class="text-center bold "> {{$i}} </td>
                @endfor

                <td rowspan="2" class="text-center bold autofit">Total </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit <?php echo (($weightscore==0)?'hidden':'');?>"><span class="">WS</span> </td>
            @endforelse
        @else
                @for($i=1; $i<=$component1;++$i)
                  <td class="text-center bold "> {{$i}} </td>
                @endfor

                <td rowspan="2" class="text-center bold autofit"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit  <?php echo (($weightscore==0)?'hidden':'');?>"><span class="">WS</span> </td>
        @endif
        
        @if($view_conduct == '1')
          @if($conductype=='1')
	         <th rowspan="3" class="autofit"><div class="conductScore" style='text-align: center; width: 70px !important; '>Score</div></th>
	      @elseif($conductype=='7')
		     <th rowspan="3" colspan="2" class="autofit"><div class="conductScore" style='text-align: center; width: 70px !important; '>Maka<br/>Diyos</div></th>
             <th rowspan="3" colspan="2" class="autofit"><div class="conductScore" style='text-align: center; width: 70px !important; ' >Maka<br />tao</div></th>
             <th rowspan="3" class="autofit"> <div class="conductScore" style='text-align: center; width: 70px !important; ' >Maka<br/>kalikasan</div></th>
             <th rowspan="3" colspan="2" class="autofit"> <div class="conductScore" style='text-align: center; width: 70px !important; ' >Maka<br/>bansa </div></th>
          @endif
		  <th rowspan="3" class="autofit hidden"> <div class="conductFinal" style='text-align: center; ' > Total </div></th>
          <th rowspan="3" class="autofit"> <div class="conductFinal" style='text-align: center; ' > Final </div></th>
          <!-- <th rowspan="3" class="autofit"> <div class="conductRemarks" style='text-align: center; ' > Remarks </div></th> -->
          <!-- Remarks in effort hidden as per ms. cris
            email date: 7/20/2017
            updated by: lendell garcellano -->
        @endif
		<!-- <th rowspan="3" class="autofit"> <div class="conductRemarks" style='text-align: center; ' >Acts Responsibly</div></th>
		<th rowspan="3" class="autofit"> <div class="conductRemarks" style='text-align: center; ' >Extends consideration,<br>respect for,<br>and service to others </div></th>
		<th rowspan="3" class="autofit"> <div class="conductRemarks" style='text-align: center; ' >Exhibits leadership<br>in character and virtue</div></th> -->
    </tr>
    
    <tr data-type="dates">
        @if(!empty($components))
            @forelse($components AS $c => $v )
                @if( count($v['Child']) > 0 )
                    @foreach($v['Child'] As $a => $b)
                        @for($j=1; $j<=count($b['Events']);++$j)
                          <td data-parent="{{$v['ComID']}}" class="font-xs autofit text-center bold eventDates ">{{ $b['Events'][$j]['EventDate']}}</td>
                        @endfor
                    @endforeach
                @else
                    @for($i=1; $i<=count($v['Events']);++$i)
                        <td class="font-xs autofit text-center bold eventDates ">{{ $v['Events'][$i]['EventDate']}}</td>
                    @endfor
                @endif
            @empty
                @for($i=1; $i<=$component1;++$i)
                  <td class="font-xs text-center bold"></td>
                @endfor
            @endforelse
        @else
            @for($i=1; $i<=$component1;++$i)
                <td class="font-xs text-center bold"></td>
            @endfor
        @endif
    </tr>

    <tr data-type="items" class="bg-purple-plum rTotalItems">

        <td colspan="{{ $period =='12' ? '5' : '3' }}" class="bold ">No. of Items</td>

        @if(!empty($components))
            @forelse($components AS $c => $v)
                @if( count($v['Child']) > 0 )

                        @foreach($v['Child'] As $a => $b)
                            <?php $subtotal = 0 ; ?>
                            @for($j=1; $j<=count($b['Events']);++$j)
                              <td class="text-center bold eventTotalItems "
                                data-com="{{$b['ComID']}}"
                                data-parent="{{$v['ComID']}}"
                                data-event="{{ $b['Events'][$j]['EventID']}}"

                                data-total="{{ $b['Events'][$j]['TotalItems']}}" >

                                <div style='width: 32px; text-align: center; ' >{{ $b['Events'][$j]['TotalItems']}}</div>
                              </td>
                              <?php $subtotal = $subtotal + $b['Events'][$j]['TotalItems'] ;  ?>
                            @endfor
                            <td class="cTotalItems info <?php echo (($showtotal==0)?'hidden':'');?>" data-com="{{$b['ComID']}}" data-parent="{{$v['ComID']}}" >
                                <div style='width: 32px; text-align: center; ' >{{$subtotal}}</div>
                            </td>
                            <td class="cPercentage success" data-com="{{$b['ComID']}}" data-parent="{{$v['ComID']}}" >
                                <div style='width: 39px; text-align: center; ' >100</div>
                            </td>
                            <td class="cWeight success <?php echo (($weightscore==0)?'hidden':'');?>" data-com="{{$b['ComID']}}" data-parent="{{$v['ComID']}}">
                                <div style='width: 35px; text-align: center; ' >{{ $b['Weight'] }}%</div>
                            </td>


                        @endforeach
                    @else
                        @for($i=1; $i<=count($v['Events']);++$i)
                            <td class="text-center bold eventTotalItems "
                                data-com="{{$v['ComID']}}"
                                data-event="{{ $v['Events'][$i]['EventID']}}"
                                data-total="{{ $v['Events'][$i]['TotalItems']}}" >
                                <div style='width: 32px; text-align: center; ' >{{ $v['Events'][$i]['TotalItems']}}</div>
                            </td>
                            <?php $totalitems += $v['Events'][$i]['TotalItems'] ; ?>
                        @endfor
                        <td class="cTotalItems info <?php echo (($showtotal==0)?'hidden':'');?>" data-com="{{$v['ComID']}}" >
                            <div style='width: 32px; text-align: center; ' >{{$totalitems}}</div>
                        </td>
                        <td class="cPercentage success" data-com="{{$v['ComID']}}" >
                            <div style='width: 39px; text-align: center; ' >100</div>
                        </td>
                        <td class="cWeight success <?php echo (($weightscore==0)?'hidden':'');?>" data-com="{{$v['ComID']}}">
                            <div style='width: 35px; text-align: center; ' >{{ $v['Weight'] }}%</div>
                        </td>

                    @endif

                <?php $totalitems = 0 ; ?>
            @empty
                @for($i=1; $i<=$component1;++$i)
                  <td class="text-center bold">  </td>
                @endfor
                <td class="total">  </td>
                <td>  </td>
                <td>  </td>
            @endforelse

        @else
            @for($i=1; $i<=$component1;++$i)
                <td class="text-center bold">  </td>
            @endfor
            <td class="total">  </td>
            <td>  </td>
            <td>  </td>
        @endif

    </tr>
</thead>
</table>
</div>
<div class="dataTables_scrollBody"  style=" overflow-x: scroll;  overflow-y: scroll; height: 400px !important; " onscroll="scrollme(this);"  >
<table class="table table-bordered table-condensed table_scroll table-striped " style="cursor: pointer;" id="tblgrade">
<tbody>

    @if(!empty($table))
        <?php $gd=1; ?>
        @foreach($table As $r)
        <?php
            $total = 0; $percent=0; $weight=0; $totalweight = 0;

            //$isposted = $model->IsPosted($period, $r->TermID ,$schedid, $r->StudentNo );
            $editable   = 'editable';
            $lock       = '';
            $grade      = '';
            $dateposted = '';
            $isposted   = 0;
            $raw        = '0.00';

            $class      = '0.00';
            $final_grade = isset($r->final_average)?$r->final_average:'';
             
			if($period == '11'){
                $grade = $r->AverageA;
                $raw = $r->PeriodGrade1;
                if($r->PostedBy1 != '') {
                    $editable = '';
                    $lock = 'warning';
                    $dateposted=$r->DatePosted1;
                    $isposted=1;
                }
            }elseif($period == '14'){
                $grade = $r->Final_Average;
                $raw = $r->PeriodGrade1;
                if($r->PostedBy != '') {
                    $editable = '';
                    $lock = 'warning';
                    $dateposted=$r->DatePosted;
                    $isposted=1;
                }                
            }else{
			  switch($period){
				case '1':
				case '2':
				case '3':
				case '4':
				 $avg    = array('1'=>'A','2'=>'B','3'=>'C','4'=>'D',);
			     $grade  = $r->{'Average'.$avg[$period]};
			     $raw    = $r->{'PeriodGrade'.$period};
                 if($r->{'PostedBy'.$period} != ''){
                   $editable = '';
                   $lock = 'warning';
                   $isposted=1;
                   $dateposted=$r->{'DatePosted'.$period};
                 }
				break;
				default:
				 $grade = $r->AverageB;
                 $raw = $r->PeriodGrade2;
                 if($r->PostedBy2 != ''){
                   $editable = '';
                   $lock = 'warning';
                   $isposted=1;
                   $dateposted=$r->DatePosted2;
                 }
				break;
			  }
            }

            $gsremarks = $model->gradeRemarks($gradingid, $grade);
			$class     = DB::table("ES_PermanentRecord_Details")->where('ScheduleID',$r->ScheduleID)->where('StudentNo',$r->StudentNo)->pluck('ClassStanding'.$period);
			
			#added by ARS 05.14.19 
			$cls_color = "";
			
			if ($raw < 74.6 ) $cls_color = "font-red";

        ?>
        <tr data-idno="{{ encode($r->StudentNo) }}" data-name="{{$r->StudentName}}"  data-student="{{$r->StudentNo}}"
            data-sched="{{$r->ScheduleID}}"   data-posted="{{$isposted}}" data-varsity="{{$r->IsVarsity}}"
            data-reg="{{encode($r->RegID)}}" data-yr="{{encode($r->YearLevelID)}}"
            class="<?= $lock ?>" >
            <td class="autofit hide" >
                @if($lock == '')
                   <input type="checkbox" class="chk-child">
                @else
                   <div style="width: 13px" ><i class="fa fa-lock"></i></div>
                @endif
            </td>

            <td tabindex="-1" class="seq autofit font-xs hide"><div style='width: 15px; text-align: center; ' >{{$gd}}.</div></td>
            <td class="img autofit hide"  >
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($r->StudentNo) }}" class="img"  height="25" width="25" alt="">
            </td>
            <td tabindex="-1" class="name bold autofit hide " style="" >
                <div style="width: 280px !important;">
                    <a href="javascript:void(0);">{{$r->StudentName}}</a>
                </div>
            </td>
            <td tabindex="-1" class="status autofit hidden"><div class="cgender" >{{ $r->Gender}}</div></td>
            <td class="ClassStand autofit" data-homeroom="{{$IsHomeRoom}}"  tabindex="-1"  style='text-align: center; height:36px!important;' ><div class="ClassStand" style='text-align: center; ' >{{ $class =='' ? '0.00' : $class  }}</div></td>
            <td class="InitialGrade autofit" data-homeroom="{{$IsHomeRoom}}"  tabindex="-1" ><div class="InitialGrade {{$cls_color}}" style='text-align: center; ' >{{ $raw =='' ? '0.00' : $raw  }}</div></td>
            <td class="TransmutedGrade font-lg bold autofit <?php echo (($transevent==1)?'hidden':'');?>" data-transevent="<?php echo $transevent;?>" tabindex="-1">
                <div class="PeriodGrade " style='text-align: center; ' >
                {{ $final_grade }}
                </div>
            </td>
            <td class="Description bold font-lg autofit " tabindex="-1">
                <div class="Descriptor {{$cls_color}}" style='text-align: center; ' >
                {{ $gsremarks->LetterGrade or '' }}
                </div>
            </td>

            <?php $ave = number_format(($r->AverageA + $r->AverageB)/2,0) ; ?>

            <td width="45" class="semestral <?= $ave < 75 ? 'font-red-flamingo' : 'font-blue-steel' ?> bold font-lg {{$css_semestral}}  " tabindex="-1" data-avea="{{$r->AverageA}}" >

                <div class="Descriptor" style='text-align: center; ' >
                <?= $ave; ?>
                </div>
            </td>

            @if(!empty($components))
                @forelse($components AS $c => $v)
                     <?php
                        $no_items    = 0;
                        $total_ave   = 0;
						$total_trans = 0;
						$percent     = 0;
                     ?>
                     @if( count($v['Child']) > 0 )
                        <?php
                            $subWeight = 0;
                            $no_items = 0;
                            $total_ave = 0;
                         ?>
                        @foreach($v['Child'] As $a => $b)
                            <?php $total = 0; $no_items = 0; $total_ave = 0;  ?>
                            @for($i=1; $i<=count($b['Events']);++$i)
                                <?php
                                   $score =  $model->Score($b['Events'][$i]['EventID'], $r->StudentNo)->first();
								   $raws  =  isset($score) ? $score->RawScore : 0 ;
                                   $trans =  0;//$xtransdata->TransmuteID($raws);
                                   
								   $total +=  isset($score) ? $score->RawScore : 0 ;
                                   $percent = 0;
                                   $weight = 0;
                                   $ave = 0;

                                   if ($b['Events'][$i]['TotalItems']){
                                       if(isset($score->RawScore)){
                                        if($score->RawScore > 0 ){
                                            $ave = ($score->RawScore / $b['Events'][$i]['TotalItems'] ) * 100  ;
                                        }
                                       }

                                     $total_ave += round($ave);
                                     $no_items++;
                                   }

                                ?>

                              <td width="32" class=" eventScore  {{$editable}}"
                              tabindex="{{ $j . $i }}"
                              data-parent="{{$v['ComID']}}"
                              data-event="{{ $b['Events'][$i]['EventID']}}"
                              data-com="{{$b['ComID']}}"
                              data-ave="<?= round($ave);  ?>"
                              data-trans="<?= round($ave);  ?>"
                              data-items="{{ $b['Events'][$i]['TotalItems']}}" >
                                <div style='width: 32px; text-align: center; ' ><?= isset($score->RawScore) ?  number_format($score->RawScore,2,'.','') : '' ?></div>
                              </td>
                            @endfor

                            <td tabindex="-1" class="subTotalItems info  <?php echo (($showtotal==0)?'hidden':'');?>" data-items="{{$b['TotalItems']}}" data-com="{{$b['ComID']}}" data-parent="{{$v['ComID']}}" >
                                <div style='width: 32px; text-align: center; ' >{{$total}}</div>
                            </td>
                            <td tabindex="-1" class="subPercentage success" data-com="{{$b['ComID']}}" data-parent="{{$v['ComID']}}" >
                                <div style='width: 39px; text-align: center; ' >
                                <?php
                                    //$percent =  ( $b['TotalItems'] > 0 ? $total / $b['TotalItems'] : '0.0' ) * 100 ;
                                    if($total_ave > 0 && $no_items > 0 ){
                                        $percent = $total_ave / $no_items ;
                                    }
                                    echo number_format( $percent,2);
                                ?>
                                </div>
                            </td>
                            <td tabindex="-1" class="subWeight success <?php echo (($weightscore==0)?'hidden':'');?>" data-com="{{$b['ComID']}}" data-weight="{{ $b['Weight'] }}" data-parent="{{$v['ComID']}}" >
                                <div style='width: 35px; text-align: center; ' ><?php $weight =  ($b['Weight'] / 100)  * $percent; echo round($weight); $subWeight += round($weight); ?></div>
                            </td>

                        @endforeach
                        <td tabindex="-1" class="sWeight warning <?php echo (($weightscore==0)?'hidden':'');?>" data-com="{{$v['ComID']}}" data-weight="{{ $v['Weight'] }}" data-value="<?= number_format($subWeight * ($v['Weight'] / 100),2) ?>">
                            <div style='width: 35px; text-align: center; ' ><?= number_format($subWeight * ($v['Weight'] / 100),2) ?> </div>
                        </td>
                    @else
                        <?php $no_items = 0;?>
                        @for($i=1; $i<=count($v['Events']);++$i)
                          <?php
                               $score  =  $model->Score($v['Events'][$i]['EventID'], $r->StudentNo)->first();
							     $empty =  isset($score) ? false : true ; //for checking if event has grade
							     $raws  =  isset($score) ? trim($score->RawScore) : 0 ;
                               $trans  =  $xtransdata->TransmuteID($raws);
                               $total +=  isset($score) ? $score->RawScore : 0 ;

                               
							   if($raws<0)
							   {
								$score->RawScore = 0;
								$trans           = 0;
							   }
							   
							   $percent = 0;
                               $weight  = 0;
                               $ave     = 0;
                                 
                               if ($v['Events'][$i]['TotalItems'] > 0 && $empty==false){
                                         $raws         = (($v['Events'][$i]['TotalItems']<$raws)?0:$raws); 
                                         $ave          = (isset($score->RawScore) && is_numeric($score->RawScore)) ? (floatval($raws) / $v['Events'][$i]['TotalItems'] ) * 100 : '0' ;
                                         $ave          =  round($ave,2);								 
                                         $trans        = ($raws>=0 && $ave<=100)?$xtransdata->TransmuteID($ave):0;
                                                                               
                                  $total_ave   += round($ave,2);
                                  $total_trans += round($trans,2);
                                
                                  $no_items++;
				          }
                          ?>
                          <td width="32" class=" eventScore  {{$editable}}"
                              tabindex="{{ $j . $i }}"
                              data-event="{{ $v['Events'][$i]['EventID']}}"
                              data-com="{{$v['ComID']}}"
                              data-raw="{{$raws}}"
                              data-ave="<?= round($ave,2) ?>"
                              data-trans="<?= round($trans,2);  ?>"
                              data-trial="<?= round($trans,2).' - '.$no_items;  ?>"
                              data-items="{{ $v['Events'][$i]['TotalItems']}}" >
                                <div style='width: 32px; text-align: center; ' ><?= isset($score->RawScore)? number_format($raws,2,'.',''): '' ?></div>
                              </td>
                        @endfor
                        <td width="32" tabindex="-1" class="sTotalItems info <?php echo (($showtotal==0)?'hidden':'');?>" data-items="{{$v['TotalItems']}}"  data-csexempt="{{ $v['IsCSExempt'] }}" data-com="{{$v['ComID']}}" >
                            <div style='width: 32px; text-align: center; ' >{{$total}}</div>
                        </td>
                        <td width="32" tabindex="-1" class="sPercentage success" data-com="{{$v['ComID']}}" data-items="{{$no_items}}">
                            <div style='width: 39px; text-align: center; ' >
                                <?php
                                    //$percent =  ( $v['TotalItems'] > 0 ? $total / $v['TotalItems'] : '0.0' ) * 100 ;
                                    if($no_items>0){
									  if($total_ave>0 ){
                                        $percent = $total_ave / $no_items ;
                                      }
									  $percent = $total_trans / $no_items ;
						       }
                                    echo number_format( $percent,2);
                                ?>
                            </div>
                        </td>
						<?php $weight = ($v['Weight'] / 100)  * $percent; ?>
                        <td width="32" tabindex="-1" class="sWeight success <?php echo (($weightscore==0)?'hidden':'');?>" data-com="{{$v['ComID']}}" data-weight="{{ $v['Weight'] }}" data-value="<?php echo number_format($weight,2); ?>"  data-csexempt="{{ $v['IsCSExempt'] }}">
                            <div style='width: 35px; text-align: center; ' ><?php echo number_format($weight,2); ?></div>
                        </td>

                    @endif


                    <?php $total = 0 ; $totalweight +=  $weight; ?>
                @empty
                    @for($i=1; $i<=$component1;++$i)
                      <td class="ww{{$i}}">  </td>
                    @endfor
                    <td class="total"></td>
                    <td class="percent"></td>
                    <td class="weight <?php echo (($weightscore==0)?'hidden':'');?>"></td>
                @endforelse
            @else
                @for($i=1; $i<=$component1;++$i)
                      <td class="ww{{$i}}">  </td>
                    @endfor
                    <td class="total"></td>
                    <td class="percent"></td>
                    <td class="weight <?php echo (($weightscore==0)?'hidden':'');?>"></td>
            @endif
            
            @if($view_conduct == '1')
            
            <?php $totalconduct = 0; ?>
            @for($i=1; $i<=$conductype;++$i)
                <?php

                    $score =  $conduct->Score( $schedid, $i , $period , $r->StudentNo)->first();
                    if($score['Score'] > 0 ) {
                        $totalconduct  = $totalconduct + floatval($score['Score']);
                    }
                ?>
                <td class="{{$editable}} conductScore" tabindex="{{$i}}" data-conduct="{{$i}}" data-id="{{encode($score['IndexID'])}}" >
                    <div class="conductScore text-center bold" style="width:70px !important;">
                        {{$score['Score']}}
                    </div>
                </td>
            @endfor
            
            <?php
                if($totalconduct > 0) $totalconduct = $totalconduct / $conductype ;
                $conductgrade = $conduct->ConductGrade($condtblid, $totalconduct);
            ?>
            <td class="bold conductTotal text-center hidden" tabindex="{{$i + $j}}" >
                <div class="conductFinal" style='text-align: center; ' ><?= number_format($totalconduct,2); ?></div>
            </td>
            <td class="conductGrade" tabindex="{{$i + $j}}"  >
                <div class="conductFinal text-center bold">
                    <?= isset($conductgrade) ? $conductgrade->LetterGrade  : '' ?>
                </div>
            </td>
            <!-- <td class="bold conductRemarks text-center" tabindex="{{$i + $j}}" >
                <div class="conductRemarks" style='text-align: center; ' >
                <?= isset($conductgrade) ? $conductgrade->Remark  : '' ?>
                </div>
            </td> -->
            <!-- Remarks in effort hidden as per ms. cris
            email date: 7/20/2017
            updated by: lendell garcellano -->
            @endif
			
			
            <!-- @for($i=2; $i<=4;++$i)
                <?php $score =  $conduct->Score( $schedid, $i , $period , $r->StudentNo)->first(); ?>
				<td class="{{$editable}} xconductScore" tabindex="{{$i}}" data-conduct="{{$i}}" data-id="{{encode($score['IndexID'])}}" >
					<div class="xconductScore text-center bold pdclass">
                        {{$score['Score']}}
					</div>
				</td>
            @endfor -->

            <!-- Remarks: Conudct is disabled for Subject Teachers as per Ms. Cris, refer to email dated 12/19/2017 -->
			
            <td class="note" tabindex="-1" ><div class="pdclass"></div> </td>
            <td class="dateposted" tabindex="-1" ><div class="pdclass" >{{$dateposted}}</div> </td>
        </tr>
        <?php $j++; $gd++; ?>
        @endforeach
    @endif
    </tbody>
    <tfoot>

        <tr class="hide">
            <td colspan="4" class="text-right  bold"> Total Students :  </td>
            <td class="text-center bold">{{$gd-1}}</td>
            <td></td>
        </tr>
    </tfoot>
</table>
</div>
<script>
    try{
        var h = $('.myheader thead').height();                    
        $('#student_header_r').height(h);    
    } catch(e){
        

    }
               
</script>