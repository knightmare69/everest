<?php
    $model = new \App\Modules\ClassRecords\Models\eClassRecord_model;
    $conduct = new \App\Modules\ClassRecords\Models\Conduct\Conduct_model;
        
    $i=0;
    $j=1;
    $gd = 1;
    $component1 = '3';
    $quarter = 1;
    $totalitems = 0;    
    $conductype = 1;
    $shs = (isset($shs) ? $shs : 0 );
    $view_conduct = isset($IsConduct) ?  $IsConduct : ($period == '14' ? '0':'1' );
    
?>
@include('ClassRecords.Views.eClassRecords.class_record.studentheader')
<div id="mytblheader" class="" style="overflow-x: hidden; overflow: -moz-scrollbars-vertical; overflow-y: scroll;">
<table class="myheader" style="cursor: pointer; margin-bottom: 0px;  " >
    <thead >
     <tr>
        <th rowspan="4" class="hide" ><input type="checkbox" class="chk-header"></th>
        <th rowspan="4" class="hide" ><div style='width: 15px; text-align: center; ' >#</div></th>
        <th rowspan="4" class="hide"><div style='width: 25px; text-align: center; ' >Pic</div></th>
        <th rowspan="3" class="hide"><div class="bold text-center " style='width: 209px; text-align: center; ' >Student Name</div></th>
        <th rowspan="3" class="autofit text-center bold"><div class="cgender" >Sex</div></th>
        @if(!empty($components))
            @forelse($components AS $c => $v)
                <th data-id="{{ $v['ComID'] }}" colspan="{{ count($v['Events']) + 3 }}" class="text-center bold event-label ">{{ $v['Caption'] }}</th>
            @empty
                <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
            @endforelse
        @else
            <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
        @endif

        <th rowspan="4"  class="text-center bold bg-yellow-casablanca vertical-container ">
            <span class="vertical-text">Initial Grade</span>
            <div class="InitialGrade"></div>
        </th>
        <th rowspan="4"  class="text-center bold bg-yellow-casablanca vertical-container">
            <span class="vertical-text">Final Grade</span>
            <div class="PeriodGrade" style='text-align: center; ' ></div>
        </th>
        <th rowspan="4" class="text-center bg-yellow-casablanca bold vertical-container" >
            <span class="vertical-text">Descriptor</span>
            <div class="Descriptor" style='text-align: center; ' ></div>
        </th>
        
        <th rowspan="4"> <div class="Descriptor" style='text-align: center; ' >Note</div> </th>
        
        @if($view_conduct == '1')
        <th colspan="{{$conductype+3}}" class="text-center bold"> Conduct </th>
        @endif
        
        <th rowspan="4"> <div class="pdclass" >Date Posted</div> </th>
                      
    </tr>
    <tr>
        @if(!empty($components))
            @forelse($components AS $c => $v)

                @for($i=1; $i<=count($v['Events']);++$i)
        		  <td class="text-center bold " data-id="{{ $v['Events'][$i]['EventID']}}" >
                    <div style='width: 32px; text-align: center; overflow: hidden; ' >                    
                        <a class="clink event-name" data-id="{{ $v['Events'][$i]['EventID']}}" data-component="{{$v['ComID']}}" data-menu="event" data-mode="edit" href="javascript:void(0);">
                            <?= trimmed($v['Events'][$i]['EventName']) == '' ? $i : $v['Events'][$i]['EventName'] ;  ?>
                        </a>
                    </div>
                  </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit info"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit success"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit success"><span class="">WS</span> </td>

            @empty
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold "> {{$i}} </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit">Total </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">WS</span> </td>
            @endforelse
        @else
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold "> {{$i}} </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">WS</span> </td>
        @endif
        
        @if($view_conduct == '1')
	      @if($conductype == '1')
            <th rowspan="3" class="autofit"> <div class="conductScore" style='text-align: center; ' >Score</div></th>
          @elseif($conductype == '4')
            <th rowspan="3" class="autofit"><div class="conductScore" style='text-align: center;'>Maka<br/>Diyos</div></th>
            <th rowspan="3" class="autofit"><div class="conductScore" style='text-align: center; ' >Maka<br />tao</div></th>
            <th rowspan="3" class="autofit"> <div class="conductScore" style='text-align: center; ' >Maka<br/>kalikasan</div></th>
            <th rowspan="3" class="autofit"> <div class="conductScore" style='text-align: center; ' >Maka<br/>bansa </div></th> -->
		  @endif
          <th rowspan="3" class="autofit hidden"> <div class="conductFinal" style='text-align: center; ' > Total </div></th>
          <th rowspan="3" class="autofit"> <div class="conductFinal" style='text-align: center; ' > Conduct </div></th>
          <th rowspan="3" class="autofit"> <div class="conductRemarks" style='text-align: center; ' > Remarks </div></th>
        @endif
	</tr>

    <tr data-type="dates">

        @if(!empty($components))
            @forelse($components AS $c => $v )
                @for($i=1; $i<=count($v['Events']);++$i)
      		        <td class="font-xs autofit text-center bold eventDates ">{{ $v['Events'][$i]['EventDate']}}</td>
                @endfor
            @empty
                @for($i=1; $i<=$component1;++$i)
        		  <td class="font-xs text-center bold"></td>
                @endfor
            @endforelse
        @else
            @for($i=1; $i<=$component1;++$i)
        		<td class="font-xs text-center bold"></td>
            @endfor
        @endif
    </tr>

	<tr data-type="items" class="bg-purple-plum rTotalItems">
		<td class="bold ">Items</td>

        @if(!empty($components))
            @forelse($components AS $c => $v)
                @for($i=1; $i<=count($v['Events']);++$i)
        		<td class="text-center bold eventTotalItems " data-com="{{$v['ComID']}}" data-event="{{ $v['Events'][$i]['EventID']}}" data-total="{{ $v['Events'][$i]['TotalItems']}}" > 
                    <div style='width: 32px; text-align: center; ' >{{ $v['Events'][$i]['TotalItems']}}</div>
                </td>
                <?php $totalitems += $v['Events'][$i]['TotalItems'] ; ?>
                @endfor
        		<td class="cTotalItems info" data-com="{{$v['ComID']}}" >
                <div style='width: 32px; text-align: center; ' >{{$totalitems}}</div>
                </td>
                <td class="cPercentage success" data-com="{{$v['ComID']}}" >
                <div style='width: 39px; text-align: center; ' >100</div>
                </td>
                <td class="cWeight success" data-com="{{$v['ComID']}}">
                <div style='width: 35px; text-align: center; ' >{{ $v['Weight'] }}%</div> 
                </td>
                <?php $totalitems = 0 ; ?>
            @empty
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold">  </td>
                @endfor
        		<td class="total">  </td>
                <td>  </td>
                <td>  </td>
            @endforelse

        @else
            @for($i=1; $i<=$component1;++$i)
      		    <td class="text-center bold">  </td>
            @endfor
    		<td class="total">  </td>
            <td>  </td>
            <td>  </td>
        @endif

	</tr>
</thead>
</table>

</div>

<div class="dataTables_scrollBody"  style=" margin-left: 304px !important; overflow-x: scroll;  overflow-y: scroll; height: 400px !important; " onscroll="scrollme(this);"  >
<table class="table table-bordered table-condensed table_scroll table-striped " style="cursor: pointer;" id="tblgrade" data-homeroom="{{$IsHomeRoom or '0'}}">
    <thead class="hide" >
     <tr style="height: 0px !important;">
        <th rowspan="4" class="autofit"><input type="checkbox" class="chk-header"></th>
        <th rowspan="4" class="autofit">#</th>
        <th rowspan="4" class="autofit">Pic</th>
        <th style="width: 200px !important;" rowspan="3" class="text-center bold ">Student Name</th>
        <th rowspan="3" class="bold"><div class="cgender">Sex</div></th>
        @if(!empty($components))
            @forelse($components AS $c => $v)
                <th data-id="{{ $v['ComID'] }}" colspan="{{ count($v['Events']) + 3 }}" class="text-center bold"> {{ $v['Caption'] }}</th>
            @empty
                <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
            @endforelse
        @else
            <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
        @endif

        <th rowspan="4" class="text-center bold bg-yellow-casablanca vertical-container "><span class="vertical-text">Initial Grade</span></th>
        <th rowspan="4" class="text-center bold bg-yellow-casablanca vertical-container"><span class="vertical-text">Period Grade</span> </th>
        <th rowspan="4" class="text-center bg-yellow-casablanca bold vertical-container" ><span class="vertical-text">Descriptor</span> </th>        
        <th rowspan="4"> Note </th>
        @if($view_conduct == '1')
        <th colspan="{{$conductype+3}}" class="text-center bold"> Effort </th>
        @endif               
    </tr>
    <tr style="height: 0px;">

        @if(!empty($components))
            @forelse($components AS $c => $v)

                @for($i=1; $i<=count($v['Events']);++$i)
        		  <td class="text-center bold " data-id="{{ $v['Events'][$i]['EventID']}}" >
                    <a class="clink" data-id="{{ $v['Events'][$i]['EventID']}}" data-component="{{$v['ComID']}}" data-menu="event" data-mode="edit" href="javascript:void(0);">
                        <?= trimmed($v['Events'][$i]['EventName']) == '' ? $i : $v['Events'][$i]['EventName'] ;  ?>
                    </a>
                  </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit info"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit success"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit success"><span class="">WS</span> </td>

            @empty
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold "> {{$i}} </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit">Total </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">WS</span> </td>
            @endforelse
        @else
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold "> {{$i}} </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">WS</span> </td>
        @endif
        @if($view_conduct == '1')
         <th rowspan="3" class="autofit"> Maka-Diyos asdsadsaddsadasd </th>
        <th rowspan="3" class="autofit"> Makatao </th>
        <th rowspan="3" class="autofit"> Makakalikasan </th>
        <th rowspan="3" class="autofit"> Makabansa </th>
        <th rowspan="3" class="autofit"> Total </th>
        <th rowspan="3" class="autofit"> Conduct </th>
        <th rowspan="3" class="autofit"> Remarks </th>
        @endif
        
	</tr>

    <tr data-type="dates" style="height: 0px;">

        @if(!empty($components))
            @forelse($components AS $c => $v )
                @for($i=1; $i<=count($v['Events']);++$i)
        		<td class="font-xs autofit text-center bold eventDates ">{{ $v['Events'][$i]['EventDate']}}</td>
                @endfor
            @empty
                @for($i=1; $i<=$component1;++$i)
        		<td class="font-xs text-center bold"></td>
                @endfor
            @endforelse
        @else
            @for($i=1; $i<=$component1;++$i)
        		<td class="font-xs text-center bold"></td>
                @endfor
        @endif

    </tr>

	<tr data-type="items" class="bg-purple-plum rTotalItems" style="height: 0px;">
		<td colspan="2" class="bold ">No. of Items</td>

        @if(!empty($components))
            @forelse($components AS $c => $v)
                @for($i=1; $i<=count($v['Events']);++$i)
        		<td class="text-center bold eventTotalItems " data-com="{{$v['ComID']}}" data-event="{{ $v['Events'][$i]['EventID']}}" data-total="{{ $v['Events'][$i]['TotalItems']}}" > {{ $v['Events'][$i]['TotalItems']}} </td>
                <?php $totalitems += $v['Events'][$i]['TotalItems'] ; ?>
                @endfor
        		<td class="cTotalItems info" data-com="{{$v['ComID']}}" >{{$totalitems}}</td>
                <td class="cPercentage success" data-com="{{$v['ComID']}}" >100</td>
                <td class="cWeight success" data-com="{{$v['ComID']}}">{{ $v['Weight'] }}%</td>
                <?php $totalitems = 0 ; ?>
            @empty
                 @for($i=1; $i<=$component1;++$i)
        		<td class="text-center bold">  </td>
                @endfor
        		<td class="total">  </td>
                <td>  </td>
                <td>  </td>
            @endforelse

        @else
            @for($i=1; $i<=$component1;++$i)
      		    <td class="text-center bold">  </td>
            @endfor
    		<td class="total">  </td>
            <td>  </td>
            <td>  </td>
        @endif

	</tr>
</thead>
<tbody>

    @if(!empty($table))
        <?php $gender=''; ?>
        @foreach($table As $r)
        <?php 
            $total = 0; $percent=0; $weight=0; $totalweight = 0;
                        
            //$isposted = $model->IsPosted($period, $r->TermID ,$schedid, $r->StudentNo );
                         
            $editable = ($r->PostedBy != '' ? '' :  'editable' );                        
            $isposted = ($r->PostedBy != '' ? 1 :  0 );
            
            if($gender != $r->Gender ) {
                $gd = 1;
                $gender = $r->Gender;
            }
        ?>
    	<tr data-idno="{{ encode($r->StudentNo) }}" data-name="{{$r->StudentName}}"  data-student="{{$r->StudentNo}}"
            data-sched="{{$r->ScheduleID}}"   data-posted="{{$isposted}}"
            data-reg="{{encode($r->RegID)}}" data-yr="{{encode($r->YearLevelID)}}"
            data-varsity="{{$r->IsVarsity}}"
            class="<?= $isposted > 0 ? 'warning' : '' ?>" style="height: 37px !important;" >
            <td class="hide" ><?= $isposted > 0 ? '' : '<input type="checkbox" class="chk-child" data-id="'. encode($r->StudentNo) .'" >' ?></td>            
    		<td tabindex="-1" class="seq hide font-xs"><div style='width: 15px; text-align: center; ' >{{$gd}}.</div></td>
            <td class="img hide"  >
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($r->StudentNo) }}" class="img"  height="25" width="25" alt="">
            </td>
    		<td tabindex="-1" class="name bold hide" style="min-width: 220px; width: 280px;" >
                <?php echo ($isposted > 0  ? '<i class="fa fa-lock"></i> ': ''); ?><a href="javascript:void(0);">{{$r->StudentName}}</a> 
            </td> 
                                   
            <td tabindex="-1" class="status"><div class="cgender" >{{ $r->Gender == 'M' ? 'Boy' : 'Girl' }}</div></td>
            
            @if(!empty($components))
                @forelse($components AS $c => $v)
                    @for($i=1; $i<=count($v['Events']);++$i)
                      <?php
                                                      
                           $score =  $model->Score($v['Events'][$i]['EventID'], $r->StudentNo)->first();
                           $total +=  isset($score) ? $score->RawScore : 0 ; 
                                                                                                            
                           $percent = 0;
                           $weight = 0;
                      ?>
		              <td class=" eventScore  {{$editable}}" 
                          tabindex="{{ $j . $i }}" 
                          data-event="{{ $v['Events'][$i]['EventID']}}" 
                          data-com="{{$v['ComID']}}" 
                          data-items="{{ $v['Events'][$i]['TotalItems']}}" >
                          <div style='width: 32px; text-align: center; ' >
                            <?= ( $v['Events'][$i]['EventID'] > 0 ?  (isset($score->RawScore) ?  number_format($score->RawScore) : '') : '')  ;  ?>
                          </div>
                          </td>
                    @endfor
            		<td tabindex="-1" class="sTotalItems info " data-items="{{$v['TotalItems']}}" data-com="{{$v['ComID']}}" >                        
                        <div style='width: 32px; text-align: center; ' >{{$total}}</div>
                    </td>
                    <td tabindex="-1" class="sPercentage success" data-com="{{$v['ComID']}}" >
                        <div style='width: 39px; text-align: center; ' >
                        <?php $percent =  ( $v['TotalItems'] > 0 ? $total / $v['TotalItems'] : '0.0' ) * 100 ; echo number_format( $percent,2); ?>
                        </div>
                    </td>
                    <td tabindex="-1" class="sWeight success" data-com="{{$v['ComID']}}" data-weight="{{ $v['Weight'] }}" >
                        <div style='width: 35px; text-align: center; ' ><?php $weight =  ($v['Weight'] / 100)  * $percent; echo number_format($weight,2); ?></div>                                        
                    </td>
                    <?php $total = 0 ; $totalweight +=  $weight; ?>
                @empty
                    @for($i=1; $i<=$component1;++$i)
            		  <td class="ww{{$i}}"> <div style="width: 19px;"></div> </td>
                    @endfor
                    <td></td>
                    <td></td>
                    <td></td>
                @endforelse
            @else
                @for($i=1; $i<=$component1;++$i)
            		  <td class="ww{{$i}}"><div style="width: 7px;"></div></td>
                @endfor             
                <td></td>
                <td></td>
                <td></td>
                           
            @endif
            <?php 
                $grade = '';
                if($IsHomeRoom == 0){                                    
                    /*
                    $r->Average
                    
                    $transmute = $model->transmuteGrade($transmuteid, $totalweight);
                    if(isset($transmute)){
                        $gsremarks = $model->gradeRemarks($gradingid, $transmute->TransmutedGrade);    
                    }     
                    $grade = isset( $transmute->TransmutedGrade ) ? $transmute->TransmutedGrade : '' ;
                    */
                    
                    $grade = $r->Average;
                    $gsremarks = $model->gradeRemarks($gradingid, $grade);
                                   
                } else{
                
                    $grade = $r->Average;
                    $gsremarks = $model->gradeRemarks($hrid, $grade);
                    /*
                    $grade = number_format($totalweight,2);
                    $gsremarks = $model->gradeRemarks($hrid, $totalweight);                    
                    */
                                        
                }                                                                                
            ?>
            <td class="InitialGrade autofit" data-homeroom="{{$IsHomeRoom}}"  tabindex="-1" >
            <div class="InitialGrade" style='text-align: center; ' >{{ number_format( $totalweight,2) }}</div></td>
            <td class="TransmutedGrade font-lg bold autofit" tabindex="-1">
                <div class="PeriodGrade" style='text-align: center; ' >
                {{ $grade }}
                </div>
            </td>
            <td class="Description bold font-lg autofit " tabindex="-1">
                <div class="Descriptor" style='text-align: center; ' >
                {{ $gsremarks->LetterGrade or '' }}
                </div>
            </td>
            <td class="note" tabindex="-1" ><div class="Descriptor" style='text-align: center; ' ></div> </td>
            
            @if($view_conduct == '1')
            <?php $totalconduct = 0; ?>
            @for($i=1; $i<=$conductype;++$i)
                <?php 
                     
                    $score =  $conduct->Score( $schedid, $i , $period , $r->StudentNo)->first();
                    if($score['Score'] > 0 ) {
                        $totalconduct  = $totalconduct + floatval($score['Score']);                        
                    }
                ?>
                <td class="{{$editable}} conductScore" tabindex="{{$i}}" data-conduct="{{$i}}" data-id="{{encode($score['IndexID'])}}" >
                    <div class="conductScore text-center bold">
                        {{$score['Score']}}    
                    </div>                
                </td>
            @endfor
            <?php  
                if($totalconduct > 0) $totalconduct = $totalconduct / $conductype; 
                $conductgrade = $conduct->ConductGrade($condtblid, $totalconduct);   
            ?>
            <td class="bold conductTotal text-center hidden" tabindex="{{$i + $j}}" >
                <div class="conductFinal" style='text-align: center; ' >{{$totalconduct}}</div>    
            </td>
            <td class="conductGrade" tabindex="{{$i + $j}}"  >                 
                <div class="conductFinal text-center bold">
                    <?= isset($conductgrade) ? $conductgrade->LetterGrade  : '' ?>
                </div>                
            </td>
            <td class="bold conductRemarks text-center" tabindex="{{$i + $j}}" >
                <div class="conductRemarks" style='text-align: center; ' >                
                <?= isset($conductgrade) ? $conductgrade->Remark  : '' ?>
                </div>
            </td>
            @endif
            
            <td class="dateposted" tabindex="-1" ><div class="pdclass" >{{$r->DatePosted}}</div> </td>
            
    	</tr>
        <?php $j++; $gd++; ?>
        @endforeach
    @endif
	</tbody>
    <tfoot>
        <tr>            
            <td class="text-center bold">{{$j-1}}</td>            
        </tr>
    </tfoot>
</table>
</div>
<script>

    try{
        var h = $('.myheader thead').height();                    
        $('#student_header_r').height(h);    
    } catch(e){
        
    }               
</script>