
<?php $i =1; $tmpstud = ''; ?>
<table id="studentlist" class="table table-bordered table-condensed table_scroll table-striped " data-period="{{$period}}" style="width: 302px ; cursor: pointer;">
    <tbody>
        @if(!empty($table))
        @foreach($table As $r)
            <?php
                
                $posted_by = $r->{'PostedBy'.$period};
            
            ?>        
            @if($tmpstud != $r->StudentNo)
            <tr style="height: 36px !important;" class="{{$posted_by == '' ? '' : 'warning'}}" data-posted="{{$posted_by == '' ? '0' : '1'}}" >
                <td>
                    @if($posted_by == '')
                    <input type="checkbox" class="chk-child" data-id="{{encode($r->StudentNo)}}"  >
                    @else
                    <div class="text-center" style="width: 13px;">
                        <i class="fa fa-lock"></i>
                    </div>                    
                    @endif
                </td>
                <td tabindex="-1" class="seq font-xs" ><?= $i; ?>.</td>                
                <td tabindex="-1" class="img"  >
                    <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($r->StudentNo) }}" class="img"  height="24" width="24" alt="">
                </td>   
                <td tabindex="-1" class="name ">
                    <div style="width: 216.5px !important; height: 36px important; overflow: hidden; white-space: nowrap !important;" class="" >
                        <a href="javascript:void(0);" class=" bold">{{substr(ucwords(strtolower($r->StudentName)),0,32)}}</a>
                    </div>                     
                </td>                        
            </tr>
            <?php $i++; $tmpstud = $r->StudentNo; ?>
            @endif
        @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr class="hide">
            <td colspan="4" class="text-right  bold"> Total Students :  <?= $i-1; ?> </td                    
        </tr>
    </tfoot>    
</table>