<?php //error_print($events); ?>
<form class="form-horizontal" id="event_form" name="event_form" action="?" >
    <input type="hidden" id="idx" name="idx" value="<?=  isset($event) ? $event->EventID : ''  ?>" />
    <input type="hidden" id="sched" name="sched" value="{{ $schedule }}" />
    <input type="hidden" id="period" name="period" value="{{ $period }}" />
    <input type="hidden" id="term" name="term" value="{{ $term }}" />
    <input type="hidden" id="event" name="event" value="save-event" />
    <div class="form-body">

        <label>Select you wish to remove</label>
        <div class="form-group">
            <div class="col-md-12">
                <table class="table" id="tbldelEvent" style="cursor: pointer;">
                    <thead>
                        <th>#</th>
                        <th>Select</th>
                        <th>Component</th>
                        <th>Event</th>
                        <th>Total Items</th>
                    </thead>
                    <tbody>

                    @if(!empty($events))
                        <?php $j=1;  ?>
                        @foreach($events AS $c => $v)
                            @if( count($v['Child']) > 0 )
                            
                                @foreach($v['Child'] As $a => $b)
                                    <?php $subtotal = 0 ; ?>
                                    @for($k=1; $k<=count($b['Events']);++$k)
                                        @if($b['Events'][$k]['EventID'] != 0 )
                            		   <tr data-com="{{ encode($b['ComID']) }}" data-id="{{ encode($b['Events'][$k]['EventID']) }}">
                                            <td><?= $k ?>.</td>
                                            <td class="autofit text-center"><input type="checkbox" class="chk-child"></td>
                                            <td>{{ $b['Caption'] }}</td>
                                            <td><?= trimmed($b['Events'][$k]['EventName']) == '' ? $k : $b['Events'][$k]['EventName'] ;  ?></td>
                                            <td><?= $b['Events'][$k]['TotalItems'] ?></td>
                                        </tr>                                 
                                        @endif     
                                    @endfor                              		                            
                                @endforeach                                                           
                            @else                                                        
                            @for($i=1; $i<=count($v['Events']);++$i)
                                @if($v['Events'][$i]['EventID'] != 0 )
                                <tr data-com="{{ encode($v['ComID']) }}" data-id="{{ encode($v['Events'][$i]['EventID']) }}">
                                    <td><?= $i ?>.</td>
                                    <td class="autofit text-center"><input type="checkbox" class="chk-child"></td>
                                    <td>{{ $v['Caption'] }}</td>
                                    <td><?= trimmed($v['Events'][$i]['EventName']) == '' ? $i : $v['Events'][$i]['EventName'] ;  ?></td>
                                    <td><?= $v['Events'][$i]['TotalItems'] ?></td>
                                </tr>
                                @endif
                            @endfor
                            @endif
                            <?php $j++; ?>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>