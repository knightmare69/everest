<div id="varsity_modal" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog" ><div class="modal-content">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Varisy Grade</h4></div>
<div class="modal-body">
	<form class="form-horizontal" id="varsity_form" name="varsity_form" action="?" >
    <div class="form-body">        
        <div class="form-group">
    		<label class="col-md-3 control-label"> Final Grade </label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter Final Grade" name="varsitygrade" id="varsitygrade" class="form-control numberonly " value="">
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Description</label>
    		<div class="col-md-6">
    			<textarea class="form-control except" style="resize: none !important;" id="desc" name="desc"></textarea>
    		</div>
    	</div>
    </div>        
</form>
</div>
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
    <button class="btn btn-default" data-menu="save-varsity-grade" type="button">Submit</button>    
</div></div></div></div>