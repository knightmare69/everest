
 <?php // error_print($event);
    $i=1;
    $event_id = isset($event) ? $event->EventID : '';
  ?>
 <form class="form-horizontal" id="event_form" name="event_form" action="?" autocomplete="off" >
    <div class="form-body">
        @if($event_id == '')
        <div class="form-group">
    		<label class="col-md-3 control-label">Section/Subject</label>
    		<div class="col-md-6">
                <div style="height: 100px !important; overflow:  auto;">
                
    			<table class="table table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="chk-head"   ></th>       
                            <th>#</th>
                            <th>Section Name</th>
                            <th>Subject Code</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($scheds as $s)
                        <tr data-id="{{encode($s->ScheduleID)}}" >
                            <td><input type="checkbox" class="chk-head" name="scheds[]" value="{{encode($s->ScheduleID)}}" <?= $s->ScheduleID == $schedule ? 'checked':'' ?> ></td>
                            <td>{{$i}}.</td>
                            <td>{{$s->SectionName}}</td>
                            <td>{{$s->SubjectCode}}</td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                    
                </table>
                </div>
                <span>put mark to which schedule the event apply </span>
    		</div>
    	</div>
        @endif
        
        <div class="form-group">
    		<label class="col-md-3 control-label">Component</label>
    		<div class="col-md-6">
    			<select id="component" name="component" class="form-control  bs-select " data-live-search="true">
                    <option value="-1"> - Select Policy - </option>
                    @if(!empty($components))
                        @foreach($components AS $c)
                            <option value="{{ $c->ComponentID }}" {{ ($component == $c->ComponentID  ? 'selected' : '' ) }} > {{ $c->Caption }} </option>
                        @endforeach
                    @endif
                </select>
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Column</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter Event" name="name" id="name" class="form-control " value="<?=  isset($event) ? $event->EventName : $caption  ?>">
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Total Items</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter Items" name="items" id="items" maxlength="3" class="form-control numberonly" value="<?=  isset($event) ? $event->TotalItems : ''  ?>" >
    		</div>
    	</div>

        <div class="form-group">
    		<label class="col-md-3 control-label">Event Date</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Optional" name="date" id="date" class="form-control date-picker except" value="<?=  isset($event) ? setDate($event->EventDate,'m/d/Y') : ''  ?>">
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Description</label>
    		<div class="col-md-6">
    			<textarea class="form-control except" style="resize: none !important;" id="desc" name="desc"><?=  isset($event) ? $event->EventDescription : ''  ?></textarea>
    		</div>
    	</div>
    </div>

    <input type="hidden" id="idx" name="idx" value="<?=  $event_id  ?>" />
    <input type="hidden" id="sched" name="sched" value="{{ $schedule or '' }}" />
    <input type="hidden" id="period" name="period" value="{{ $period or '' }}" />
    <input type="hidden" id="term" name="term" value="{{ $term or '' }}" />
    <input type="hidden" id="event" name="event" value="save-event" />
    <input type="hidden" id="gender" name="gender" value="{{ $gender or '' }}" />
</form>