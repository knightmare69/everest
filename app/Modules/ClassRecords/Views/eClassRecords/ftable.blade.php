<?php
    $model = new \App\Modules\ClassRecords\Models\eClassRecord_model;
    $conduct = new \App\Modules\ClassRecords\Models\Conduct\Conduct_model;
        
    $i=0;
    $j=1;
    $gd = 1;
    $component1 = '5';
    $quarter = 1;
    $totalitems = 0;    
    $conductype = 4;
    $shs = (isset($shs) ? $shs : 0 );
?>
<style>
.myheader th, .myheader td{
    padding: 5px;
    border: 1px solid #ddd;        
}
.myheader{
    border: 1px solid #ddd;
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed !important;
}
.numseq{
    width: 20px !important;
}
.myheader td.info{
    background: #d9edf7 none repeat scroll 0 0;
    color: #31708f;
}
.eventScore div {
    width: 32px !important;
}
.InitialGrade,  .Descriptor{
    width: 35px !important;
}
.PeriodGrade{
    width: 38px !important;
}

.conductScore{
    width: 65px !important;
}
.conductFinal{
    width: 65px !important;
}
.conductRemarks{
    width: 150px !important;
}
.cPercentage, .cWeight{
     background: #dff0d8 none repeat scroll 0 0 !important;
    color: #3c763d !important;
}
.cgender{
    width: 35px !important; 
    text-align: center !important;
}
.pdclass{
    width: 150px !important; 
    text-align: center !important;
}
</style>
<table border="1" cellpadding="0" class="table table-bordered table-condensed table_scroll table-striped " style="cursor: pointer;" id="tblgrade">
    <thead class="" >
     <tr style="height: 0px !important;">        
        <th rowspan="4" class="autofit">#</th>        
        <th style="width: 200px !important;" rowspan="3" class="text-center bold ">Student Name</th>
        <th rowspan="3" class="bold"><div class="cgender">Sex</div></th>
        @if(!empty($components))
            @forelse($components AS $c => $v)
                <th data-id="{{ $v['ComID'] }}" colspan="{{ count($v['Events']) + 3 }}" class="text-center bold"> {{ $v['Caption'] }}</th>
            @empty
                <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
            @endforelse
        @else
            <td data-id="0" colspan="{{ $component1 + 3 }}" class="text-center bold"> [No Component]</td>
        @endif

        <th rowspan="4" class="text-center bold bg-yellow-casablanca vertical-container "><span class="vertical-text">Initial Grade</span></th>
        <th rowspan="4" class="text-center bold bg-yellow-casablanca vertical-container"><span class="vertical-text">Period Grade</span> </th>
        <th rowspan="4" class="text-center bg-yellow-casablanca bold vertical-container" ><span class="vertical-text">Descriptor</span> </th>                
        <th colspan="{{$conductype}}" class="text-center bold"> Conduct </th>
        <th rowspan="4"> Date Posted </th>               
    </tr>
    <tr style="height: 0px;">

        @if(!empty($components))
            @forelse($components AS $c => $v)

                @for($i=1; $i<=count($v['Events']);++$i)
        		  <td class="text-center bold " data-id="{{ $v['Events'][$i]['EventID']}}" ><a class="clink" data-component="{{$v['ComID']}}" data-menu="event" data-mode="edit" href="javascript:void(0);">
                  <?= trimmed($v['Events'][$i]['EventName']) == '' ? $i : $v['Events'][$i]['EventName'] ;  ?>
                  </a></td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit info"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit success"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit success"><span class="">WS</span> </td>

            @empty
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold "> {{$i}} </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit">Total </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">WS</span> </td>
            @endforelse
        @else
                @for($i=1; $i<=$component1;++$i)
        		  <td class="text-center bold "> {{$i}} </td>
                @endfor

        		<td rowspan="2" class="text-center bold autofit"><span class="">Total</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">PS</span> </td>
                <td rowspan="2" class="text-center bold autofit"><span class="">WS</span> </td>
        @endif
        
         <th rowspan="3" class="autofit"> Maka-Diyos </th>
        <th rowspan="3" class="autofit"> Makatao </th>
        <th rowspan="3" class="autofit"> Makakalikasan </th>
        <th rowspan="3" class="autofit"> Makabansa </th>

	</tr>

    <tr data-type="dates" style="height: 0px;">

        @if(!empty($components))
            @forelse($components AS $c => $v )
                @for($i=1; $i<=count($v['Events']);++$i)
        		<td class="font-xs autofit text-center bold eventDates ">{{ $v['Events'][$i]['EventDate']}}</td>
                @endfor
            @empty
                @for($i=1; $i<=$component1;++$i)
        		<td class="font-xs text-center bold"></td>
                @endfor
            @endforelse
        @else
            @for($i=1; $i<=$component1;++$i)
        		<td class="font-xs text-center bold"></td>
                @endfor
        @endif

    </tr>

	<tr data-type="items" class="bg-purple-plum rTotalItems" style="height: 0px;">
		<td colspan="2" class="bold ">No. of Items</td>

        @if(!empty($components))
            @forelse($components AS $c => $v)
                @for($i=1; $i<=count($v['Events']);++$i)
        		<td class="text-center bold eventTotalItems " data-com="{{$v['ComID']}}" data-event="{{ $v['Events'][$i]['EventID']}}" data-total="{{ $v['Events'][$i]['TotalItems']}}" > {{ $v['Events'][$i]['TotalItems']}} </td>
                <?php $totalitems += $v['Events'][$i]['TotalItems'] ; ?>
                @endfor
        		<td class="cTotalItems info" data-com="{{$v['ComID']}}" >{{$totalitems}}</td>
                <td class="cPercentage success" data-com="{{$v['ComID']}}" >100</td>
                <td class="cWeight success" data-com="{{$v['ComID']}}">{{ $v['Weight'] }}%</td>
                <?php $totalitems = 0 ; ?>
            @empty
                 @for($i=1; $i<=$component1;++$i)
        		<td class="text-center bold">  </td>
                @endfor
        		<td class="total">  </td>
                <td>  </td>
                <td>  </td>
            @endforelse

        @else
            @for($i=1; $i<=$component1;++$i)
      		    <td class="text-center bold">  </td>
            @endfor
    		<td class="total">  </td>
            <td>  </td>
            <td>  </td>
        @endif

	</tr>
</thead>
<tbody>

    @if(!empty($table))
        <?php $gender=''; ?>
        @foreach($table As $r)
        <?php 
            $total = 0; $percent=0; $weight=0; $totalweight = 0;
                        
            $isposted = $model->IsPosted($period, $r->TermID ,$schedid, $r->StudentNo );             
            $editable = 'editable';
            
            if( $isposted['Posted'] > 0 ){
                $editable = '';
            }
            
            if($gender != $r->Gender ) {
                $gd = 1;
                $gender = $r->Gender;
            }
        ?>
    	<tr data-idno="{{ encode($r->StudentNo) }}" data-name="{{$r->StudentName}}"  data-student="{{$r->StudentNo}}"
            data-sched="{{$r->ScheduleID}}"   data-posted="{{$isposted['Posted']}}"
            data-reg="{{encode($r->RegID)}}" data-yr="{{encode($r->YearLevelID)}}"
            class="<?= $isposted['Posted'] > 0 ? 'warning' : '' ?>" >
            
    		<td tabindex="-1" class="seq font-xs"><div style='width: 15px; text-align: center; ' >{{$gd}}.</div></td>
          
    		<td tabindex="-1" class="name bold " style="min-width: 220px; width: 280px;" >
                <?php echo ($isposted['Posted'] > 0  ? '<i class="fa fa-lock"></i> ': ''); ?>{{$r->StudentName}} 
            </td>                        
            <td tabindex="-1" class="status">{{ $r->Gender == 'M' ? 'Boy' : 'Girl' }}</td>
            
            @if(!empty($components))
                @forelse($components AS $c => $v)
                    @for($i=1; $i<=count($v['Events']);++$i)
                      <?php
                           $score =  $model->Score($v['Events'][$i]['EventID'], $r->StudentNo)->first();
                           $total +=  isset($score) ? $score->RawScore : 0 ;
                           $percent = 0;
                           $weight = 0;
                      ?>
		              <td class=" eventScore  {{$editable}}" 
                          tabindex="{{ $j . $i }}" 
                          data-event="{{ $v['Events'][$i]['EventID']}}" 
                          data-com="{{$v['ComID']}}" 
                          data-items="{{ $v['Events'][$i]['TotalItems']}}" >
                          <?= isset($score->RawScore) ?  number_format($score->RawScore) : '' ;  ?>
                          </td>
                    @endfor
            		<td class="sTotalItems info " data-items="{{$v['TotalItems']}}" data-com="{{$v['ComID']}}" >                        
                        {{$total}}
                    </td>
                    <td class="sPercentage success" data-com="{{$v['ComID']}}" >
                        <div style='width: 39px; text-align: center; ' >
                        <?php $percent =  ( $v['TotalItems'] > 0 ? $total / $v['TotalItems'] : '0.0' ) * 100 ; echo number_format( $percent,2); ?>
                        </div>
                    </td>
                    <td tabindex="-1" class="sWeight success" data-com="{{$v['ComID']}}" data-weight="{{ $v['Weight'] }}" >
                        <div style='width: 35px; text-align: center; ' ><?php $weight =  ($v['Weight'] / 100)  * $percent; echo number_format($weight,2); ?></div>                                        
                    </td>
                    <?php $total = 0 ; $totalweight +=  $weight; ?>
                @empty
                    @for($i=1; $i<=$component1;++$i)
            		  <td class="ww{{$i}}">  </td>
                    @endfor
                    <td class="total"></td>
                    <td class="percent"></td>
                    <td class="weight"></td>
                @endforelse
            @else
                @for($i=1; $i<=$component1;++$i)
            		  <td class="ww{{$i}}">  </td>
                    @endfor
                    <td class="total"></td>
                    <td class="percent"></td>
                    <td class="weight"></td>
            @endif
            <?php 
                $grade = '';
                if($IsHomeRoom == 0){                                    
                    $transmute = $model->transmuteGrade($transmuteid, $totalweight);
                    if(isset($transmute)){
                        $gsremarks = $model->gradeRemarks($gradingid, $transmute->TransmutedGrade);    
                    }     
                    $grade = isset( $transmute->TransmutedGrade ) ? $transmute->TransmutedGrade : '' ;               
                } else{
                                        
                    $gsremarks = $model->gradeRemarks($hrid, $totalweight);                    
                    $grade = $totalweight;
                                        
                }                                                                                
            ?>
            <td class="InitialGrade autofit" data-homeroom="{{$IsHomeRoom}}"  tabindex="-1" >
            <div class="InitialGrade" style='text-align: center; ' >{{ number_format( $totalweight,2) }}</div></td>
            <td class="TransmutedGrade font-lg bold autofit" tabindex="-1">
                <div class="PeriodGrade" style='text-align: center; ' >
                {{ $grade }}
                </div>
            </td>
            <td class="Description bold font-lg autofit " tabindex="-1">
                <div class="Descriptor" style='text-align: center; ' >
                {{ $gsremarks->LetterGrade or '' }}
                </div>
            </td>           
                                    
            <?php $totalconduct = 0; ?>
            @for($i=1; $i<=$conductype;++$i)
                <?php 
                     
                    $score =  $conduct->Score( $schedid, $i , $period , $r->StudentNo)->first();
                    if($score['Score'] > 0 ) {
                        $totalconduct  = $totalconduct + floatval($score['Score']);                        
                    }
                ?>
                <td class="{{$editable}} conductScore" tabindex="{{$i}}" data-conduct="{{$i}}" data-id="{{encode($score['IndexID'])}}" >
                    <div class="conductScore text-center bold">
                        {{$score['Score']}}    
                    </div>                
                </td>
                @break
            @endfor
           
            <td class="dateposted" tabindex="-1" ><div class="pdclass" >{{$isposted['DatePosted']}}</div> </td>
            
    	</tr>
        <?php $j++; $gd++; ?>
        @endforeach
    @endif
	</tbody>
    <tfoot>
        <tr>
            <td colspan="4" class="text-right  bold"> Total Students :  </td>
            <td class="text-center bold">{{$j-1}}</td>
            <td></td>
        </tr>
    </tfoot>
</table>
</div>