<div id="mdlimport" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog" style="width: 70%;" ><div class="modal-content">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Upload Event Raw Score</h4></div>
<div class="modal-body clearfix">
<form novalidate="novalidate" class="form-horizontal" id="form-mapping" action="" autocomplete="off">                                                                                                                                                                                                       	                                                              	    
<div class="col-sm-12 ">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">                
                <table id="tblmapping" class="table table-bordered table-condensed" >
                    <tbody>
                        <tr class="info">
                            <td>#</td>
                            <td>Column</td>
                            <td>Map</td>
                        </tr>
                        <tr>
                            <td class="autofit">1.</td>
                            <td>Component</td>
                            <td >
                                <div class="input">
                                    <select class="form-control input-sm" name="upload_component" id="upload_component">
                                        <option value="-1"> - Select Component - </option>                                        
                                    </select>            
                                </div>                                        
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td class="autofit">Sub Component</td>
                            <td>
                                <div class="input">
                                    <select class="form-control input-sm" name="upload_subcomponent" id="upload_subcomponent">
                                        <option value="-1"> - Select Sub Component - </option>                                        
                                    </select> 
                                </div>                                                                                
                            </td>
                        </tr>
                         
                        <tr>
                            <td>3.</td>
                            <td>Event</td>
                            <td>
                                <div class="input">
                                    <select class="form-control input-sm" name="upload_event" id="upload_event">
                                        <option value="-1"> - Select Sub Component - </option>                                        
                                    </select>
                                </div>                                                                                
                            </td>
                        </tr>
                        
                        <tr>
                            <td>4.</td>
                            <td>Student No</td>
                            <td>
                                <div class="input">
                                    <input type="text" id="upload_idno" name="idno" value="" class="no-border no-padding input-xs font-bolder" />
                                </div>                                        
                            </td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td class="autofit">Raw Score</td>
                            <td>
                                <div class="input">
                                    <input type="text" id="upload_rawscore" name="rawscore" value="" class="no-border no-padding input-xs font-bolder" />
                                </div>
                                
                            </td>
                        </tr>
                        <tr class="info">
                            <td></td>
                            <td>Setup</td>
                            <td>Value</td>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td class="autofit">Row Start</td>
                            <td>
                                <div class="input">
                                    <input type="text" id="rowstart" name="rowstart" value="1" class="no-border no-padding input-xs font-bolder" />
                                </div>                                
                            </td>
                        </tr>                                                
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8">   
            
                     
                        
                <label class="control label font-blue-madison">Import Details</label>         
                <input type="file" name="file" id="file">
                
                                                                                      
                <hr style="margin-top: 3px; margin-bottom: 3px;" />
                <label class="font-xs bold">File Preview...</label>                        
                <div class="no-padding" style="overflow: auto; height: 300px; "  >                                        
                    <table id="tblimport" class="table table-bordered font-xs table-hover"  style="margin-top: 5px!important; cursor: pointer; width: 100%; ">                   
                        <tbody>                                                                        
                        </tbody>
                    </table>    
                </div>             
                                    
        </div> 
    </div>           
</div>                                     
</form>    	
</div>
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
    <button type="button" class="btn btn-sm btn-success " data-menu="import"><i class="fa fa-check"></i> Import</button>
</div></div></div></div>