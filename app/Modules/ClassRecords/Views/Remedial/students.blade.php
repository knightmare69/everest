<?php 
    $data = isset($data) ? $data : array() ;
    $mSetup = new \App\Modules\Setup\Models\GradingSystemSetup ;              
             
?>
<table id="tblstudents" class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important;">
    <thead>
        <tr>            
            <th  class="autofit" ><input type="checkbox" class="chk-header"></th>
            <th   >#</th>
            <th  >Pic</th>
            <th  >ID No.</th>
            <th  >Complete Name</th>
            <th  class="text-center">Sex</th>  
            <th  class="text-center">Section</th>
            <th  class="text-center">Year Level</th>
            <th  class="text-center">1st Term</th>
            <th  class="text-center">2nd Term</th>                
            <th  class="text-center">Sem Grade</th>
            <th  class="text-center">Remedial Grade</th>
            <th  class="text-center">Final Grade</th>
            <th  class="text-center">Letter</th>
            <th  class="text-center">Remarks</th>
            <th  class="autofit text-center" >Date Posted</th>            
            <th  class="autofit text-center" >Posted by</th>
        </tr>                       
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        
        <tr class="<?= ($data[$i]->PostedBy3 == '' ? '' : 'success' ) ?>"
            data-id="<?= encode($data[$i]->GradeIDX)  ?>" 
            data-name="<?= $data[$i]->FullName  ?>" 
            data-reg="{{encode($data[$i]->RegID)}}"
            data-idno="{{ ($data[$i]->StudentNo)}}"
            data-sched="{{encode($data[$i]->ScheduleID)}}"
            data-posted="<?= ($data[$i]->PostedBy3 == '' ? '0' : '1' ) ?>"
             >
            <td class="autofit" >
                @if($data[$i]->PostedBy3 == '')
                <input type="checkbox" class="chk-child">
                @else
                <i class="fa fa-lock"></i>
                @endif
            </td>
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="pic autofit"  > 
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($data[$i]->StudentNo) . '&t='. rand() }}" class="img"  height="25" width="25" alt="">
            </td>

            <td class="autofit studno" tabindex="-1"><?= $data[$i]->StudentNo ?></td>
		    <td class="bold autofit text-primary" tabindex="-1"><?= $data[$i]->FullName  ?> </a></td>
            <td class="autofit text-center " tabindex="-1"><?= $data[$i]->Gender ?></td>                
            <td class="autofit text-center " tabindex="-1"><?= $data[$i]->SectionName ?></td>
            <td class="autofit text-center " tabindex="-1"><?= $data[$i]->YearLevelID == 5 ? 'Grade 11' : 'Grade 12' ?></td>
            <?php $ave = ($data[$i]->AverageC == '' ?  $data[$i]->Final_Average : $data[$i]->TotalAverageAB) ; ?>

            <td class="autofit text-center " tabindex="-1"><?= $data[$i]->AverageA ?></td>
            <td class="autofit text-center " tabindex="-1"><?= $data[$i]->AverageB ?></td>
            
            <td class="final text-center bold" tabindex="-1" >
            <?= number_format(floatval($ave),0) ?>
            </td>
            
            <td tabindex="-1" class="autofit remedial <?= ($data[$i]->PostedBy3 == '' ? 'editable' : '' ) ?> color-red font-lg warning  text-center ">
                <?= $data[$i]->AverageC ?>
            </td>
            
            <td class="recompute text-center bold" tabindex="-1" >
            <?= $data[$i]->AverageC != '' ?  $data[$i]->Final_Average : '' ?>
            </td>
            
            <td tabindex="-1" class="letter bold text-center" >
                <?= $data[$i]->LG ?>
            </td>
            <td tabindex="-1" class="remarks bold autofit">
                <?= $data[$i]->Final_Remarks ?>                                   
            </td>            
            <td class="dtposted autofit text-center">
            <?= $data[$i]->DatePosted3 ?>            
            </td>
            <td class="byposted autofit text-center">
            <?= $data[$i]->PostedBy3 ?>
            </td>                        	 
        </tr> 
    @endfor        
    </tbody>
</table>           	