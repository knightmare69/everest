
<?php 
    $gradingsystem = App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID',$club_id)->get();
?>
<div style="width: 500px; background-color: #8e44ad !important; overflow: auto; height: 250px;">

<table id="tblgradesystem" class="table table-condensed" style="cursor: pointer !important; color: white;">
    <thead>
        <th>#</th>
        <th>Min</th>
        <th>Max</th>
        <th>Letter</th>
        <th>Descriptors</th>
        <th>Symbols</th>
    </thead>
    <tbody>
    @for($i=0; $i < count($gradingsystem) ; $i++ )
        <tr data-id ="<?= encode($gradingsystem[$i]->IndexID) ?>" >
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="text-center autofit min" ><?= $gradingsystem[$i]->Min ?></td>
		    <td class="text-center autofit max" ><?= $gradingsystem[$i]->Max  ?> </a></td>
            <td class="letter" ><?= $gradingsystem[$i]->LetterGrade  ?></td>
            <td class="desc" ><?= $gradingsystem[$i]->Description  ?></td>
            <td class="remarks text-center"><?= $gradingsystem[$i]->Remark  ?></td>
        </tr>
    @endfor
    </tbody>
</table>
</div>
