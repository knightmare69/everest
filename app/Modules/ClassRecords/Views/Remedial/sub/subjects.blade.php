<?php $table = isset($table) ? $table : array() ; ?>
<option value="-1"> - Select - </option>
@if(!empty($table))
    @foreach( $table AS $r )           
        <option value="<?= encode($r->SubjectID) ?>">
            <?= $r->SubjectCode ?>
        </option>
    @endforeach
@endif    