<?php $table = isset($table) ? $table : array() ; ?>
<option value="-1"> - Select - </option>
@if(!empty($table))
    @foreach( $table AS $r )           
        <option value="<?= encode($r->ScheduleID) ?>">
            <?= $r->SubjectCode . ' ['.$r->SectionName.']' ?>
        </option>
    @endforeach
@endif    