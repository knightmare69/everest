<?php 
    $data = isset($data) ? $data : array() ;
    $conduct = new \App\Modules\ClassRecords\Models\Conduct\Conduct_model;         
?>
<table id="tblstudents" class="table table-condensed table-bordered table-striped table-hover" style="cursor: pointer !important;">
    <thead>
        <tr>            
            <th rowspan="2" class="autofit" ><input type="checkbox" class="chk-header"></th>
            <th rowspan="2" >#</th>
            <th rowspan="2" >Pic</th>
            <th rowspan="2" >ID No.</th>
            <th rowspan="2" >Complete Name</th>
            <th rowspan="2" class="text-center">Sex</th>                
            <th data-conduct="1" colspan="2" class="autofit text-center " >Maka-Diyos</th>
            <th data-conduct="2" colspan="2" class="autofit text-center " >Makatao</th>
            <th data-conduct="3"  class="autofit text-center " >Maka<br/>kalikasan</th>
            <th data-conduct="4" colspan="2"  class="autofit text-center " >Makabansa</th>
            <th colspan="3" class="text-center">Conduct</th>
            <th rowspan="2" class="autofit text-center" >Date Posted</th>
            
        </tr>
        <tr>
            <th data-conduct="1" class=" text-center " >1</th>
            <th data-conduct="1" class=" text-center " >2</th>
            <th data-conduct="2" class=" text-center " >1</th>
            <th data-conduct="2" class=" text-center " >2</th>
            <th data-conduct="3" class=" text-center " >1</th>
            <th data-conduct="4" class=" text-center " >1</th>
            <th data-conduct="4" class=" text-center " >2</th>
            
            <th class="text-center total">Total</th>
            <th class="text-center grade">Grade</th>
            <th class="text-center remarks">Remarks</th>
        </tr>
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        
        <tr class="<?= ($data[$i]->Postedby == '' ? '' : 'warning' ) ?>" data-name="<?= $data[$i]->FullName  ?>" data-idno="{{$data[$i]->StudentNo}}" >
            <td class="autofit" >
                @if($data[$i]->Postedby == '')
                <input type="checkbox" class="chk-child">
                @else
                <i class="fa fa-lock"></i>
                @endif
            </td>
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="pic autofit"  > 
                <img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($data[$i]->StudentNo) }}" class="img"  height="25" width="25" alt="">
            </td>

            <td class="autofit"><?= $data[$i]->StudentNo ?></td>
		    <td class="bold autofit text-primary"><?= $data[$i]->FullName  ?> </a></td>
            <td class="autofit text-center "><?= $data[$i]->Gender ?></td>                
                            
            <td data-conduct="1" tabindex="{{$i}}1" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct1 ?></td>
            <td data-conduct="2" tabindex="{{$i}}2" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct2 ?></td>
            
            <td data-conduct="3" tabindex="{{$i}}3" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct3 ?></td>
            <td data-conduct="4" tabindex="{{$i}}4" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct4 ?></td>
            
            <td data-conduct="5" tabindex="{{$i}}5" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct5 ?></td>
            <td data-conduct="6" tabindex="{{$i}}6" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct6 ?></td>
            <td data-conduct="7" tabindex="{{$i}}7" class="<?= ($data[$i]->Postedby == '' ? 'editable' : '' ) ?> conductScore text-center"><?= $data[$i]->Conduct7 ?></td>
            
            <td class="total text-center">
            <?php
                
                $conduct_grade =  ( $data[$i]->Conduct1 + $data[$i]->Conduct2 + $data[$i]->Conduct3 + $data[$i]->Conduct4 ) / 4  ; 
                
                echo  number_format( $conduct_grade , 2) ;
                
                $conductgrade = $conduct->ConductGrade($conduct_id, $conduct_grade);
                
            ?>
            </td>
            <td class="conduct text-center">
                <?= isset($conductgrade) ? $conductgrade->LetterGrade  : '' ?>
            </td>
            <td class="remarks text-center autofit">
                <?= isset($conductgrade) ? $conductgrade->Remark  : '' ?>
            </td>
            <td class="dtposted autofit text-center"><?= $data[$i]->DatePosted ?></td>
            
            	 
        </tr> 
    @endfor        
    </tbody>
</table>	                    	