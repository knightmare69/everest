
<table class="table table-condensed table-bordered">
    <tr>
        <td rowspan="2">
            Maka-Diyos
        </td>  
        <td>1</td> 
        <td> Expresses one's spiritual beliefs while respecting spiritaul beliefs of others	 </td>     
    </tr>
    <tr>
        <td>2</td>
        <td>Shows adherence to ethical principles by upholding truth</td>
    </tr>
    <tr>
        <td rowspan="2">
            Makatao
        </td>   
        <td>1</td>
        <td> Is sensitive to individual, social, and cultural differences </td>     
    </tr>
    <tr>
        <td>2</td>
        <td>Demonstrate contribution towards solidarity</td>
    </tr>
    <tr>
        <td >
            Makakalikasan
        </td>   
        <td>1</td>
        <td> Cares for the environment and utilizes resources wisely, judiciously, and economically </td>     
    </tr>
    <tr>
        <td rowspan="2">
            Makabansa
        </td>   
        <td>1</td>
        <td> Demonstrate pride in being a Filipino; exercises the rights and responsibilities of a Filipino citizen. </td>     
    </tr>
    <tr>
        <td>2</td>
        <td>Demonstrates appropriate behavior in carrying out activities in the school, community, and country</td>
    </tr>
    
    
    
</table>

<?php 
    $gradingsystem = App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID',$conduct_id)->get();
?>
<div style="width: 500px; background-color: #8e44ad !important; overflow: auto; height: 250px;">

<table id="tblgradesystem" class="table table-condensed" style="cursor: pointer !important; color: white;">
    <thead>
        <th>#</th>
        <th>Min</th>
        <th>Max</th>
        <th>Letter</th>
        <th>Descriptors</th>
        <th>Symbols</th>
    </thead>
    <tbody>
    @for($i=0; $i < count($gradingsystem) ; $i++ )
        <tr data-id ="<?= encode($gradingsystem[$i]->IndexID) ?>" >
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="text-center autofit min" ><?= $gradingsystem[$i]->Min ?></td>
		    <td class="text-center autofit max" ><?= $gradingsystem[$i]->Max  ?> </a></td>
            <td class="letter" ><?= $gradingsystem[$i]->LetterGrade  ?></td>
            <td class="desc" ><?= $gradingsystem[$i]->Description  ?></td>
            <td class="remarks text-center"><?= $gradingsystem[$i]->Remark  ?></td>
        </tr>
    @endfor
    </tbody>
</table>
</div>
