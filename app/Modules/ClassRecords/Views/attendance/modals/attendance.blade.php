<div class="tabbable-line" id="AttendanceTabs">
	<ul class="nav nav-tabs">
		<li class="tbattend hidden"><a href="#Modaltab1" data-toggle="tab" data-type="1">Absences</a></li>
		<li class="tbattend hidden"><a href="#Modaltab2" data-toggle="tab" data-type="2">Tardiness</a></li>
		<li class="hidden"><a href="#Modaltab3" data-toggle="tab" data-type="3">Merits</a></li>
		<li class="hidden"><a href="#Modaltab4" data-toggle="tab" data-type="4">Lost & Found</a></li>
		<li class="tboffense active"><a href="#Modaltab5" data-toggle="tab" data-type="5">Offenses</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane" id="Modaltab1">
			@include($views.'forms.absenses')
		</div>
		<div class="tab-pane" id="Modaltab2">
			@include($views.'forms.tardiness')
		</div>
		<div class="tab-pane" id="Modaltab3">
			@include($views.'forms.merits')
		</div>
		<div class="tab-pane" id="Modaltab4">
			@include($views.'forms.lostfound')
		</div>
		<div class="tab-pane active" id="Modaltab5">
			@include($views.'forms.offenses')
		</div>
	</div>
</div>