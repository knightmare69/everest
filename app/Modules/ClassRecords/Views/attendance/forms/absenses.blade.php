<?php 
    $data = []; 
    if (Request::get('AttendanceKey')) {
        $data = App\Modules\ClassRecords\Models\Attendance\Attendance::where('fldPK',Request::get('AttendanceKey'))->where('Type','1')->first();
    }
?>
<form class="horizontal-form" id="FormAbsenses" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Period</label>
                    <select class="form-control" name="Period">
                        <option value="">- SELECT -</option>
                        @foreach(App\Modules\ClassRecords\Models\Attendance\Quarter::where('GroupClass',1)->get() as $row)
                        <option {{ getObjectValue($data,'PeriodID') == $row->PeriodID ? 'selected' : '' }} value="{{ encode($row->PeriodID) }}">{{ $row->Description2 }}</option>
                        @endforeach
                    </select>
                 </div>
            </div>
             <div class="form-group">
                 <div class="col-md-6">
                    <label class="control-label bold">Date Absent: </label>
                    <input type="text" placeholder="Date" class="form-control input-xs date-picker" name="Date" id="Date" value="{{ setDate(getObjectValue($data,'Date'),'m/d/Y') }}">
                 </div>
                 <!--/span-->
                 <div class="col-md-6">
                    <label class="control-label bold">To: </label>
                    <input type="text" placeholder="Date" class="form-control input-xs date-picker" name="DateEnd" id="DateEnd" value="{{ setDate(getObjectValue($data,'DateEnd'),'m/d/Y') }}">
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label bold">Excused </label>
                    <select class="form-control" name="Excused">
                        <option {{ getObjectValue($data,'IsExcused') == '0' ? 'selected' : '' }} value="0">NO</option>
                        <option {{ getObjectValue($data,'IsExcused') == '1' ? 'selected' : '' }} value="1">YES</option>
                    </select>
                 </div>
                 <!--
                 <div class="col-md-6 hidden">
                    <label class="control-label bold">Demerit </label>
                    <input type="text" placeholder="0"  class="form-control input-xs numberonly" name="Demerit" id="Demerit" value="{{ getObjectValue($data,'Demerit') }}">
                 </div>
                 -->
            </div>
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Reason </label>
                    <input type="text" placeholder="Enter text here" class="form-control input-xs" name="Remarks" id="Remarks" value="{{ getObjectValue($data,'Remarks') }}">
                 </div>
                 <!--/span-->
            </div>
        </div>
    </div>
</form>