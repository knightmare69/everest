<?php 
    $periods = array('01'=>'1','02'=>'1','03'=>'1',
	                 '04'=>'2','05'=>'2','06'=>'2',
	                 '07'=>'3','08'=>'3','09'=>'3',
	                 '10'=>'4','11'=>'4','12'=>'4');
    $data    = []; 
    if (Request::get('AttendanceKey')) {
        $data = App\Modules\ClassRecords\Models\Attendance\Attendance::where('fldPK',Request::get('AttendanceKey'))->where('Type','5')->first();
    }
	
	$data['Date']     = ((getObjectValue($data,'Date')=='')?date('Y-m-d H:i:s'):getObjectValue($data,'Date'));
	$data['PeriodID'] =  $periods[setDate(getObjectValue($data,'Date'),'m')];
    $class = ((isfaculty() || Request::get('AttendanceKey')==false)?'':'disabled');
?>
<form class="horizontal-form" id="FormOffenses" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="form-group hidden">
                 <div class="col-md-12">
                    <label class="control-label bold">Quarter</label>
                    <select class="form-control <?php echo $class;?>" name="Period">
                        <option value="">- SELECT -</option>
                         @foreach(App\Modules\ClassRecords\Models\Attendance\Quarter::where('GroupClass',1)->get() as $row)
                        <option {{ getObjectValue($data,'PeriodID') == $row->PeriodID ? 'selected' : '' }} value="{{ encode($row->PeriodID) }}">{{ $row->Description2 }}</option>
                        @endforeach
                    </select>
                 </div>
            </div>
             <div class="form-group">
                 <div class="col-md-6">
                    <label class="control-label bold">Offense Date: </label>
                    <input type="text" placeholder="Date" class="form-control input-xs date-picker <?php echo $class;?>" name="Date" id="Date" value="{{ setDate(getObjectValue($data,'Date'),'m/d/Y') }}">
                 </div>
                 <!--/span-->
                 <div class="col-md-6">
                    <label class="control-label bold">Offense Time: </label>
                    <div class="input-icon">
                        <i class="fa fa-clock-o"></i>
                        <input type="text" class="form-control timepicker timepicker-no-seconds <?php echo $class;?>" name="DateTime" id="DateTime" value="{{ setDate(getObjectValue($data,'Date'),'h:m A') }}">
                    </div>
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Offense Committed</label>
                    <input type="text" placeholder="Enter text here"  class="form-control input-xs <?php echo $class;?>" name="Particulars" id="Particulars" value="{{ getObjectValue($data,'Particulars') }}">
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group hidden">
                <div class="col-md-6">
                    <label class="control-label bold">Excused </label>
                    <select class="form-control" name="Excused">
                        <option {{ getObjectValue($data,'IsExcused') == '0' ? 'selected' : '' }} value="0">NO</option>
                        <option {{ getObjectValue($data,'IsExcused') == '1' ? 'selected' : '' }} value="1">YES</option>
                    </select>
                 </div>
                 <!--/span-->
            </div>
			<?php 
			if($class=='disabled'){
			?>
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Reason/Action Taken </label>
                    <input type="text" placeholder="Enter text here" class="form-control input-xs" name="Remarks" id="Remarks" value="{{ getObjectValue($data,'Remarks') }}">
                 </div>
                 <!--/span-->
            </div>
			<?php
			}
			?>
            <!--
            <div class="form-group">
                 <div class="col-md-6">
                    <label class="control-label bold">Demerit </label>
                    <input type="text" placeholder="0"  class="form-control input-xs numberonly" name="Demerit" id="Demerit" value="{{ getObjectValue($data,'Demerit') }}">
                 </div>
            </div>
			-->
        </div>
    </div>
</form>