<form class="horizontal-form" id="FormFilter" action="#" method="POST">
    <div class="row">
        <div class="form-group">
             <div class="col-md-12">
                <label class="control-label">School Campus</label>
                <select class="form-control filter" name="SchoolCampus" id="SchoolCampus">
                    <option value="">Select...</option>
                    @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                    <option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                    @endforeach
                </select>
             </div>
             <!--/span-->
        </div>
        <div class="form-group">
             <div class="col-md-12">
                <label class="control-label">School Year</label>
                <?php $i = 0; ?>
                <select class="form-control filter" name="SchoolYear" id="SchoolYear">
                    <option value="">Select...</option>
                    @foreach(App\Modules\Registrar\Models\Reports::AcademicTerm() as $row)
                    <option  {{  $row->Active_OnlineEnrolment == 1 ? 'selected' : '' }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear.' - '.$row->SchoolTerm }}</option>
                    @endforeach
                </select>
             </div>
             <!--/span-->
        </div>
        <div class="form-group <?php echo ((isfaculty())?"hidden":"");?>">
             <div class="col-md-12">
                <label class="control-label">Year Level </label>
                <select class="form-control filter" name="YearLevel" id="YearLevel">
                    <option value="" selected>Select...</option>
                    @foreach(App\Modules\Admission\Models\Admission::getGradeLevel() as $row)
                    <option value="{{ encode($row->YearLevelID) }}" data-code="{{ encode($row->ProgClass) }}">{{ $row->YearLevelName }}</option>
                    @endforeach
                </select>
             </div>
             <!--/span-->
        </div>
        <div class="form-group">
             <div class="col-md-12">
                <label class="control-label">Sections</label>
                <select class="form-control not-required" name="Section" id="Section">
                    <option value="" selected>Select...</option>
					<?php
					if(isset($section)){
					 foreach($section as $s){
					  echo '<option value="'.$s->SecID.'" data-year="'.encode($s->YearLevelID).'">'.$s->Section.'</Section>';
					 }
					}
					?>
                </select>
             </div>
             <!--/span-->
        </div>
    </div>
</form>