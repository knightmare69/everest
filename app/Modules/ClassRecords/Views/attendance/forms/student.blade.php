<div class="col-md-5 center" style="width: 51%">
	<img id="StudentPhoto" src="{{ url('general/getStudentPhotoByStudentNo?StudentNo=') }}" width="200" width="200">
</div>
<div class="col-md-7 center" style="width: 47%;padding-left:0;">
	<form class="form">
		<table class="table font-sm table-condensed " id="TableFilterResult" style="height: 200px;margin-bottom: 0px">
			<tr>				
				<td class="bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					<span id="StudentNo" class="bold text-danger">[Student No]</span> <br />
                    <span class="bold font-xs text-success">Student Number</span>
				</td>
			</tr>
			<tr>
				
				<td  class="bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					<span id="Name" class="bold text-danger">[Student Name]</span> <br />
                    <span class="bold font-xs text-success">Student Name</span>
				</td>
			</tr>
			<tr>
			
				<td class="bold bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">					
                    <span id="Gender" class="bold text-danger">[Gender]</span> <br />
                    <span class="bold font-xs text-success">Gender</span>
                    
				</td>
			</tr>
			<tr>				
				<td class="bold bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					<span id="BirthDate" class="bold text-danger">[Birth Date]</span> <br />
                    <span class="bold font-xs text-success">Birthdate</span>                    
				</td>
			</tr>
		</table>
	</form>
</div>