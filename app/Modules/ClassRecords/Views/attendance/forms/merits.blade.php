<?php 
    $data = []; 
    if (Request::get('AttendanceKey')) {
        $data = App\Modules\ClassRecords\Models\Attendance\Attendance::where('fldPK',Request::get('AttendanceKey'))->where('Type','3')->first();
    }
?>
<form class="horizontal-form" id="FormMerits" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Quarter</label>
                    <select class="form-control" name="Period">
                        <option value="">- SELECT -</option>
                        @foreach(App\Modules\ClassRecords\Models\Attendance\Quarter::get() as $row)
                        <option {{ getObjectValue($data,'PeriodID') == $row->PeriodID ? 'selected' : '' }} value="{{ encode($row->PeriodID) }}">{{ $row->Description2 }}</option>
                        @endforeach
                    </select>
                 </div>
            </div>
             <div class="form-group">
                 <div class="col-md-6">
                    <label class="control-label bold">Date of Merit: </label>
                    <input type="text" placeholder="Date" class="form-control input-xs date-picker" name="Date" id="Date" value="{{ setDate(getObjectValue($data,'Date'),'m/d/Y') }}">
                 </div>
                 <div class="col-md-6">
                    <label class="control-label bold">Merit </label>
                    <input type="text" placeholder="0"  class="form-control input-xs numberonly" name="Merit" id="Merit" value="{{ getObjectValue($data,'Merit') }}">
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Reason </label>
                    <input type="text" placeholder="Enter text here" class="form-control input-xs" name="Remarks" id="Remarks" value="{{ getObjectValue($data,'Remarks') }}">
                 </div>
                 <!--/span-->
            </div>
        </div>
    </div>
</form>