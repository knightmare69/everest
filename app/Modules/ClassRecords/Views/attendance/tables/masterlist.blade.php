<?php
    $i = 1;
	$data = [];

	if (Request::get('Section')) {
		$data = DB::table('ES_Registrations as r')
					->select([
							DB::raw("LastName+', '+FirstName+' '+MiddleInitial as Name"),
							DB::raw("dbo.fn_SectionName(r.ClassSectionID) as Section"),
							's.StudentNo',
							'Gender',
                            's.YearlevelID'
						])
					->leftJoin('ES_Students as s',
							's.StudentNO','=','r.StudentNO'
						)
					->where('r.ClassSectionID',Request::get('Section'))
					->orderBy('s.LastName','asc')
					->get();
	}
	
?>
<table class="table table-condensed table-hover" id="TableMasterList">
	<thead>
		<tr >			
            <th class="autofit"><input type="checkbox" name="chk-head" class="chk-head"></th>						
            <th class="autofit">#</th>
            <th class="sm-font upper-letter cs-td-valign" width="50">Student No</th>
			<th class="sm-font upper-letter cs-td-valign left" width="50">Full Name</th>
			<th class="sm-font upper-letter center cs-td-valign">Sex</th>
			<th class="sm-font upper-letter left cs-td-valign">Section</th>
            <!-- <th class="sm-font uppercase">Year Level</th> -->
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; ?>
		@foreach($data as $row)
		<tr class="cursor-pointer rowSel" data-studentno="{{ encode($row->StudentNo) }}">
			<td class="center"><input type="checkbox" class="form-control chk-child"></td>
			<td class="xs-font">{{$i}}.</td>
			<td class="left">{{ $row->StudentNo }}</td>
			<td class="autofit">{{ $row->Name }}</td>
			<td>{{ $row->Gender }}</td>
			<td class="left">{{ $row->Section }}</td>
            <!-- <td class="left">{{ $row->YearlevelID }}</td> -->
		</tr>
        <?php $i++; ?>
		@endforeach
		@if(count($data) > 0)
		<tr>
			<td colspan="6" class="left">Total records(s): <b>({{ count($data) }})</b></td>
		</tr>
		@endif
	</tbody>
	@if(count($data) <= 0)
	<tfoot>
		<tr>
			<td colspan="6" class="center bold">No Record found.</td>
		</tr>
	</tfoot>
	@endif
</table>