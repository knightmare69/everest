<div class="portlet light bg-inverse">
	<div class="portlet-body" style="min-height: 300px">
        <div class="row">
        @include($views.'forms.student')
        </div>
        <hr />
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable-line" id="TableAttendanceWrapper">
					<ul class="nav nav-tabs hidden" id="UlMainTab">
						<li class="hidden"><a href="#tab1" data-toggle="tab">Absenses</a></li>
						<li class="hidden"><a href="#tab2" data-toggle="tab">Tardiness</a></li>
						<li class="hidden"><a href="#tab3" data-toggle="tab">Merits</a></li>
						<li class="hidden"><a href="#tab4" data-toggle="tab">Lost & Found</a></li>
						<li class="hidden"><a href="#tab5" data-toggle="tab">Offenses</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane" id="tab1">
							@include($views.'tables.absenses')
						</div>
						<div class="tab-pane" id="tab2">
							@include($views.'tables.tardiness')
						</div>
						<div class="tab-pane" id="tab3">
							@include($views.'tables.merits')
						</div>
						<div class="tab-pane" id="tab4">
							@include($views.'tables.lostfound')
						</div>
						<div class="tab-pane active" id="tab5">
							@include($views.'tables.offenses')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>