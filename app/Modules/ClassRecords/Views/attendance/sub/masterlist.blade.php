<div class="portlet box grey-cascade">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-file"></i> List of Students
		</div>
		<div class="actions btn-set">
		</div>
	</div>
	<div class="portlet-body" id="TableWrapper">
		@include($views.'tables.masterlist')
	</div>
</div>