<style>

	#TableMasterList > tbody > tr:hover, .row-selected {
		background-color: #e08283 !important;
		color: #fff !important;
	}

	.cs-row-color {
		background-color: #e7e7e7  !important;
	}

	.cs-row-color:hover {
		background-color: #fff  !important;
	}
	.cs-table-no-border {
		border-top: 0 none !important;
	}

	.cs-tbody-tr-no-border td {
		border: 0px none !important;
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
	}

	.cs-tbody-tr-border-bottom {
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
		/*border-bottom: 1px solid #e5e5e5 !important;*/
	}

	.no-border-top {
		border-top: 0px none !important;
	}

	.cs-td-valign {
		vertical-align: middle !important;
	}

	.cs-input-xs {
		width: 60px !important;
	}
</style>
<div class="portlet light clearfix">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-file"></i> Discipline /&nbsp;<small id="SchoolYearTitle"><u> School Year</u></small>&nbsp;/&nbsp;<small id="SectionTitle"><u>Section</u></small>
		</div>
		<div class="actions btn-set">
			@if(isfaculty()) 
			<div class="portlet-input input-inline input-medium">
            <select class="form-control input-sm adviser_section" name="adviser_section" id="adviser_section">
                <option value="-1"> - Select Section - </option>
                @if(!empty($adviser_section))
                    @foreach($adviser_section as $as)
                    <option value="{{$as->SectionID}}" data-term="{{ encode($as->TermID) }}" data-ay="{{ $as->AcademicYear.' '.$as->SchoolTerm }}">{{$as->SectionName}}</option>                        
                    @endforeach
                @endif
            </select>
            </div>
			@else
			<button class="btn btn-success" id="BtnFilter"><i class="fa fa-file"></i> Filter</button>
			@endif
			<button class="btn blue" id="BtnOffense"><i class="fa fa-file"></i> Create</button>
			<button class="btn red" id="BtnDelete"><i class="fa fa-trash"></i> Delete</button>
			<button class="btn grey" id="BtnReset"><i class="fa fa-history"></i> Refresh</button>
			<div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn yellow dropdown-toggle">
				<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li class="hidden">
						<a href="javascript:void(0);" class="btn-print">
						Print </a>
					</li>
					<li class="devider"></li>
					<li>
						<a href="javascript:void(0);" class="btn-export">
						Export to CSV </a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		<div class="col-md-5">
            <div class="row">
                @include($views.'sub.forms')
            </div>			
		</div>
		<div class="col-md-7">
			@include($views.'sub.masterlist')
		</div>
	</div>
</div>