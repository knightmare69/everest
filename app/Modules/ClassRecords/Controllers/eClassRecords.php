<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\eClassRecord_model As dbcon;
use App\Modules\ClassRecords\Models\GradeEvent_model As event_model;
use App\Modules\ClassRecords\Models\ReportCard_model As reportcard_model;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As reportcardetails_model;
use App\Modules\ClassRecords\Models\Schedules_model As sched_model;
use App\Modules\ClassRecords\Services\ClassRecords\service_provider as service;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use App\Modules\GradingSystem\Models\Components As component_model;
use App\Modules\ClassRecords\Models\Conduct\Conduct_model As conduct_model;
use App\Modules\Registrar\Models\Subjects\Subjects as subject_model;

use App\Modules\Academics\Services\SummaryService as summary_service;

use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF;
use Maatwebsite\Excel\Facades\Excel;

use Request;
use Permission;
use Response;
use DB;

class eClassRecords extends Controller{
    protected $ModuleName = 'eclass-records';

	private $media = [
			'Title'       => 'eClass Records',
			'Description' => 'Welcome To eClass Records!',
			'js'		  => ['ClassRecords/import','ClassRecords/classrecords.js?v=1.1.3'],
			'init'		  => ['ME.init()'],
            'css'         => ['profile','classrecord'],
            'plugin_js'	  => ['bootbox/bootbox.min','bootstrap-datepicker/js/bootstrap-datepicker','bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
                                'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min','bootstrap-fileinput/bootstrap-fileinput','navigator/file-navigator.min','navigator/line-navigator.min',
                                'bootstrap-select/bootstrap-select.min', 'SmartNotification/SmartNotification.min'
                             ],
           'plugin_css' => ['jstree/dist/themes/default/style.min','bootstrap-fileinput/bootstrap-fileinput',
                            'bootstrap-select/bootstrap-select.min',
                            'select2/select2','SmartNotification/SmartNotification',
                            'bootstrap-datepicker/css/datepicker','bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
                            'jquery-multi-select/css/multi-select'],
	];

	private $url = [ 'page' => 'class-record/eclass-records/' ];

	private $views = 'ClassRecords.Views.eClassRecords.';

    protected $dbcon ;

    private $IsHomeRoom;
    private $IsConduct;

	function __construct()
	{
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {

            $faculty_id = getUserFacultyID();
            $conductid =  $this->setup_mod->ConductID();
            $term = 0;
            
            $ayt = $this->dbcon->get_academicyearterm($faculty_id);
            
            if($term == '0' && count($ayt) > 0 ){
            #set default ay term
                $term = $ayt[0]->TermID;    
            }
            
            $data = array(
                'academic_year' => $ayt,
                'facultyId' => $faculty_id,
                'conduct_id' =>  $conductid,
                'tblid' => $this->setup_mod->GradingSystemID(),
                'transid' => $this->setup_mod->TransmutationID(),
                'period' => 0,
                'term' =>   $term   
            );

            SystemLog('eClass-Records','','Page View','page-view','Page: eClass-Records','' );
            return view('layout',array('content'=>view($this->views.'index',$data)->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

    public function iservice(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{

			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
			     case 'students':
                    ini_set('max_execution_time', 300);
                    $_id = Request::get('sched_id');
                    $period = Request::get('period');
                    $this->shs =  Request::get('shs');

                    $facid   = getUserFacultyID();
                    $sched   = $this->sched_model->where('ScheduleID',$_id)->first();
					$section = $this->sched_model->Section($_id);
					$termid  = $sched->TermID;
					$yrlvl   = $section->YearLevelID;
					$progid  = $section->ProgramID;
					$gssetup = DB::select("SELECT * FROM ESv2_GradeSheet_Setup WHERE TermID='".$termid."' AND YearLevelID='".$yrlvl."' AND ProgID='".$progid."'")[0];
					
					
                    $this->gender = '';

                    if($sched->FacultybyGender){
                        if( $facid == $sched->FacultyID ){
                            $this->gender  = 'M';
                        } else if( $facid == $sched->FacultyID_2 ){
                            $gender = 'F';
                        }else{
                            $gender = '';
                        }
                    }

                    $components = $this->dbcon->get_components($_id,$period);
                    $data       = $this->initClassrecords($components);
                    $this->shs  = 1;
                    $query = [ 'table'        => $this->dbcon->get_students($_id,$this->gender ,$this->shs, $period),
                               'schedid'      =>  $_id,
                               'period'       => $period,
                               'condtblid'    => $this->setup_mod->ConductID(),
                               'transmuteid'  => ((getObjectValue($gssetup,'TransmutationID'))?getObjectValue($gssetup,'TransmutationID'):$this->setup_mod->TransmutationID()),
                               'gradingid'    => ((getObjectValue($gssetup,'LetterGradeID'))?getObjectValue($gssetup,'LetterGradeID'):$this->setup_mod->GradingSystemID()),
                               'tblid'        => ((getObjectValue($gssetup,'LetterGradeID'))?getObjectValue($gssetup,'LetterGradeID'):$this->setup_mod->GradingSystemID()),
                               'hrid'         => $this->setup_mod->HomeRoomtbl(),
                               'components'   => $data,
                               'shs'          => $this->shs,
                               'IsHomeRoom'   => $this->IsHomeRoom,
                               'IsConduct'    => $this->IsConduct,
                             ];
                    if( $this->shs != 0 ){
                        $data = view($this->views.'shstable',$query)->render();
                    }else{
                        $data = view($this->views.'table',$query)->render();
                    }
					
					$grading = view($this->views.'gradetbl.table',$query)->render();
                    SystemLog('eClass-Records','','load students','load students', 'Scheduel:'.$_id.', Period:'.$period ,'record successfully saved!' );
                    $response = ['error' => false, 'message' => 'record successfully loaded', 'content' => $data, 'grading'=>$grading ];

                 break;

				 case 'save-grade':

					if ($this->permission->has('add')) {
						$validation = $this->service->isValid(Request::all(), 'save-grade');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							$data   = $this->service->postScore(Request::all());
                            $event  = Request::get('activity');
                            $idno   = Request::get('idno');
                            $regid  = decode( Request::get('regid'));
                            $sched  = (Request::get('sched'));
                            $period = Request::get('period');
                            $grade  = Request::get('igrde');
                            $class  = Request::get('cgrde');
                            $hrum   = Request::get('hmrm');

                            $varsity = Request::get('varsity');

                            $this->dbcon->updateOrCreate(array('EventID'=> $event , 'StudentNo' => decode($idno) ),$data);
                            $finalgrade = ($hrum != 0 ? $grade :  Request::get('finl') );
                            $this->save_grade($regid, $sched, $period,  $grade , $finalgrade );
                            $exec       =  DB::statement("UPDATE ES_PermanentRecord_Details SET ClassStanding".$period."='".$class."' WHERE ScheduleID='".$sched."' AND RegID='".$regid."' AND StudentNo='".decode($idno)."'");
                            if($varsity == 1){

                                $varsity_model = new \App\Modules\ClassRecords\Models\Varsity_model;

                                $whre = array(
                                    'ScheduleID' => $sched
                                    ,'PeriodID' => $period
                                    ,'StudentNo' => decode($idno)
                                );

                                $data = array(
                                    'ScheduleID' => $sched
                                    ,'PeriodID' => $period
                                    ,'StudentNo' => decode($idno)
                                    ,'Grade' => $finalgrade
                                    ,'Createdby' => getUserID()
                                    ,'DateCreated' => systemDate()
                                );

                                $varsity_model->updateOrCreate($whre,$data);

                            }

							$response = ['error'=>false,'message'=>'Successfully Save!'];
                            SystemLog('eClass-Records','','save-grade','Save Score', json_encode($data) ,'record successfully saved!' );
						}
					}
                break;

                case 'save-bulk-grade':
                    if ($this->permission->has('add')) {

						//$data = $this->service->postScore(Request::all());

                        $period = Request::get('period');
                        $term = decode(Request::get('term'));
                        $rs = Request::get('rawscore');
                        $sched = (Request::get('sched'));

                        foreach($rs as $r){

                            $idno = decode(getObjectValue($r, 'idno'));
                            $evnt = getObjectValue($r, 'evnt');

                            $data = array(
                                'EventID' => $evnt,
                                'StudentNo' => $idno,
                     	        'TermID' => $term,
                                'GradingPeriod' => $period,
                                'TransmutedGrade' => '',
                                'LetterGrade'=> '',
                                'InputDate' => date('Y-m-d H:i:s'),
                                'RawScore' => trimmed(getObjectValue($r, 'score')),
                                'IsAbsent' => 0,
                                'LastModifiedBy' => getUserID(),
                                'LastModifiedDate' => date('Y-m-d H:i:s')
                            );

                            $o =  $this->dbcon->updateOrCreate(array('EventID'=> $evnt , 'StudentNo' => $idno ),$data);

                            if($o){
                                SystemLog('eClass-Records','','save-grade','Save Score', json_encode($data) ,'record successfully saved!' );
                            }

                            $grade = getObjectValue($r, 'grde');
                            $hrum = getObjectValue($r, 'hmrm');

                            if($hrum != 0 ){
                                $finalgrade = getObjectValue($r, 'grde');
                            }else{
                                $finalgrade = getObjectValue($r, 'fnl');
                            }
                            $regid = decode( getObjectValue($r, 'regid'));
                            $this->save_grade($regid, $sched, $period,  $grade , $finalgrade );

                        }

						$response = ['error'=>false,'message'=>'Successfully Save!'];
                        SystemLog('eClass-Records','','save-grade','Save Score', json_encode($data) ,'record successfully saved!' );

					}
                break;

				case 'erase-grade':
					if ($this->permission->has('delete')) {

                        $validation = $this->service->isValid(Request::all(),'erase-grade');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {

                            $event = Request::get('activity');
                            $idno = decode(Request::get('idno'));

    						$this->dbcon
                                ->where('EventID',$event)
                                ->where('StudentNo',$idno)
                                ->delete();
    						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                            SystemLog('eClass-Records','','erase-grade','Erase Score', 'EventID='. $event.' StudentNo='. $idno . ' StudentName='.Request::get('name') ,'record successfully erased!' );
						}
					}
					break;

				case 'events':

                        $_id = Request::get('schedule');
                        $period = Request::get('period');
                        $component = Request::get('component');
                        $term = Request::get('term');

                        $sched = $this->sched_model->info($_id);
                        $gender = 0;
                        $facid = getUserFacultyID();

                        if($sched->FacultybyGender){
                            if( $facid == $sched->FacultyID ){
                                $gender = '1';
                            } else if( $facid == $sched->FacultyID_2 ){
                                $gender = '2';
                            }
                        }

                        $data = [ 'schedule' => $_id,
                                  'period' => $period,
                                  'term' => $term,
                                  'components' => $this->dbcon->get_components($_id,$period),
                                  'component' => $component,
                                  'caption' => '',
                                  'gender' => $gender,
                                  'scheds' => $this->dbcon->get_classRecord(decode($term),$facid),
                                ];

						$response = view($this->views.'events.events',$data);

					break;

                case 'edit-event':

                    $id = Request::get('schedule');
                    $period = Request::get('period');
                    $component = Request::get('component');
                    $term = Request::get('term');

                    $eventid = Request::get('index');

                    $sched = $this->sched_model->info($id);
                    $gender = 0;
                    $facid = getUserFacultyID();

                    if($sched->FacultybyGender){
                        if( $facid == $sched->FacultyID ){
                            $gender = '1';
                        } else if( $facid == $sched->FacultyID_2 ){
                            $gender = '2';
                        }
                    }

                    $data = [ 'schedule' => $id,
                              'period' => $period,
                              'term' => $term,
                              'components' => $this->dbcon->get_components($id,$period),
                              'component' => $component,
                              'event' => $this->event_model->GetInfo($eventid)->first(),
                              'gender' => $gender,
                              'caption' => trim(Request::get('caption')) ];

                    //$data = view($this->views.'table',$query)->render();
                    //err_log($data);
					$response = view($this->views.'events.events',$data);

                break;

				case 'save-event':

                    $validation = $this->service->isValid(Request::all());
					if ($validation['error']) {
						$response = Response::json($validation);
					} else {
						$data = $this->service->postEvents(Request::all());
                        $index = Request::get('idx');
                        if($index == 0){

                            $post = Request::all();
                            if($post['scheds']){
                                foreach($post['scheds'] as $s){
                                    $post['sched'] = decode($s);
                                    $data = $this->service->postEvents($post);
                                    $this->event_model->create($data);    
                                }
                            }else{
                                $this->event_model->create($data);    
                            }
                            
                        }else{
                            $this->event_model->where('EventID',$index)->update($data);
                        }

						$response = ['error'=>false,'message'=>'Successfully Save!'];
                        SystemLog('eClass-Records','','save-event','save-event', json_encode($data) ,'record successfully saved!' );
					}

				break;

                case 'remove-event':

                    $_id = Request::get('schedule');
                    $period = Request::get('period');
                    $component = Request::get('component');
                    $term = Request::get('term');
                    $this->shs=0;
                    if($period == 11 || $period == 12){
                        $this->shs=1;
                    }
                    $this->gender = 0;
                    $components = $this->dbcon->get_components($_id,$period);
                    $events = $this->initClassrecords($components);

                    $data = [ 'schedule' => $_id,
                        'period' => $period,
                        'term' => $term,
                        'components' => $components,
                        'component' => $component,
                        'events' => $events ];

                    $response = view($this->views.'events.remove',$data);

                    break;

                case 'erase-event':

                    if ($this->permission->has('delete')) {

                        $validation = $this->service->isValid(Request::all(),'erase-event');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {

                            $ids =  Request::get('index');
                            $comp = '';

                            foreach($ids as $id){

                                $ind = decode($id);
                                $o =  $this->event_model->destroy($ind);
                                if($o){
                                    SystemLog('eClass-Records','','erase-event','Erase Event', 'Component='. $comp .' EventID='. $ind,'record successfully erased!' );
                                }
		                    }

                            $response = ['error'=> false,'message'=>'Successfully Deleted'];

                        }
                    }
                    break;

                case 'save-conduct':

                    $post = Request::all();

                    $validation = $this->service->isValid($post,'save-conduct');

                    if ($validation['error']) {
						$response = Response::json($validation);
					} else {

						$data = $this->service->postConduct($post);
                        $model = new conduct_model();

                        $whre = [
                            'StudentNo' => decode(getObjectValue($post,'idno')),
                            'ScheduleID' => (getObjectValue($post,'sched')),
                            'PeriodID' => getObjectValue($post,'period'),
                            'ConductID' => getObjectValue($post,'conduct'),
                        ];
                        
						
                        $exec = $model->updateOrCreate($whre,$data);
                        $xid  = $model->where($whre)->first();
						
						$response = ['error'=>false,'message'=>'Successfully Save!','xid'=>encode($xid->IndexID)];
                        SystemLog('eClass-Records','','save-conduct','save-conduct', json_encode($data) ,'record successfully saved!' );

					}

                break;
                case 'erase-conduct':
                	if ($this->permission->has('delete')) {

                        $model = new conduct_model();
                        $id = decode(Request::get('index'));
                        $idno = decode(Request::get('idno'));
                        $conid = (Request::get('conduct'));

                        $model->destroy($id);
						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog('eClass-Records','','erase-conduct','Erase Conduct Score', 'ConductID='. $conid.' StudentNo='. $idno,'record successfully erased!' );
					}

                break;

                case 'transmutation':

                    $grade = (Request::get('grade'));
                    $hrum = (Request::get('homeroom'));
                    $regid = decode(Request::get('regid'));
                    $sched = (Request::get('sched'));
                    $period = (Request::get('period'));

                    $conduct = $this->setup_mod->ConductID();
                    $transid = $this->setup_mod->TransmutationID();
                    $gradingid = $this->setup_mod->GradingSystemID();
                    $hrid = $this->setup_mod->HomeRoomtbl();

                    $finalgrade = '';

                    if($hrum == 0 ){
                        $transmute = $this->dbcon->transmuteGrade($transid, $grade);
                        $gsremarks = $this->dbcon->gradeRemarks($gradingid, $transmute->TransmutedGrade);

                        $finalgrade = $transmute->TransmutedGrade ;

                    }else{
                        $finalgrade = number_format($grade,2);
                        $gsremarks = $this->dbcon->gradeRemarks($hrid, $grade);
                    }

                    $this->save_grade($regid, $sched, $period,  $grade , $finalgrade );
                    $response = ['error'=> false, 'transmuted' => $finalgrade, 'message'=> $gsremarks->LetterGrade ];

                break;

                case 'post':

                     $model = new reportcardetails_model();
                     $post = Request::all();
					 
					 $summary_id = '0';
					 
                     $period = getObjectValue($post, 'period');
                     $term = decode(getObjectValue($post, 'term'));
                     $sched =  getObjectValue($post, 'sched');

                     foreach($post['stud'] As $s){

                        $reg_id = decode($s['reg']);
                        $stud = decode($s['idno']);

                        $whre = array(
                            'TermID' => $term,
                            'StudentNo' => $stud,
                            'RegID' => $reg_id,
                            'ScheduleID' => $sched,
                        );

                        $data = array( );

                        switch( $period  ){
                            case '1':
                            case '11':
                                $data['PostedBy1'] =  getUserID();
                                $data['DatePosted1'] =  date('Y-m-d H:i:s');
                            break;
                            case '2':
                            case '12':
                                $data['PostedBy2'] =  getUserID();
                                $data['DatePosted2'] =  date('Y-m-d H:i:s');
                            break;
                            case '3':
                                $data['PostedBy3'] =  getUserID();
                                $data['DatePosted3'] =  date('Y-m-d H:i:s');
                            break;
                            case '4':
                                $data['PostedBy4'] =  getUserID();
                                $data['DatePosted4'] =  date('Y-m-d H:i:s');
                            break;
                            case '14':
                                $data['PostedBy'] =  getUserID();
                                $data['DatePosted'] =  date('Y-m-d H:i:s');
                            break;
                        }

                        $model->where($whre)->update($data);
                        SystemLog('eClass-Records','','Post Grade','Post Grades', $whre ,'record successfully posted!' );

                        $parent = $model->where($whre)->first();
						
						#set summary id
						$summary_id = $parent->SummaryID;
						
                        if($period == 12){
                            #SHS Posting Final Grade

                            $ave1 = $parent->AverageA;
                            $ave2 = $parent->AverageB;

                            $fnl = (floatval($ave1) + floatval($ave2)) / 2 ;

                            $data = array(
                                'Final_Average' => $fnl,
                                'Final_Remarks' => $this->dbcon->gradeRemarks('1', $fnl)->Remark,
                            );
                            $model->where($whre)->update($data);
                        }

                        if ($parent->ParentSubjectID > 0 ){
                            /*Create Parent Entry*/
                            $final = '';
                            $posted_child = true;

                            $child = $model->where([
                                'TermID' => $term ,
                                'StudentNo' => $stud ,
                                'RegID' => $reg_id,
                                'ParentSubjectID' => $parent->ParentSubjectID ,
                            ])->get();

                            foreach($child as $r){

                                switch( $period ){
                                    case '1':
                                    case '11':
                                        $final +=  ($r->SubjectCreditUnits * $r->AverageA);
                                        if($r->PostedBy1 == ''){
                                            $posted_child = false;
                                        }
                                    break;
                                    case '2':
                                    case '12':
                                        $final += ($r->SubjectCreditUnits * $r->AverageB);
                                        if($r->PostedBy2 == ''){
                                            $posted_child = false;
                                        }

                                    break;
                                    case '3':
                                        $final += ($r->SubjectCreditUnits * $r->AverageC);
                                        if($r->PostedBy3 == ''){
                                            $posted_child = false;
                                        }
                                    break;
                                    case '4':
                                        $final += ($r->SubjectCreditUnits * $r->AverageD);
                                        if($r->PostedBy4 == ''){
                                            $posted_child = false;
                                        }
                                    break;

                                }

                            }

                            $parentSchedID = $this->dbcon->get_parentScheduleID($reg_id, $parent->ParentSubjectID );

                            $this->save_grade($reg_id, $parentSchedID , $period , '', $final);

                            if($posted_child){

                                $data = array( );

                                switch( $period  ){
                                    case '1':
                                    case '11':
                                        $data['PostedBy1'] =  getUserID();
                                        $data['DatePosted1'] =  date('Y-m-d H:i:s');
                                    break;
                                    case '2':
                                    case '12':
                                        $data['PostedBy2'] =  getUserID();
                                        $data['DatePosted2'] =  date('Y-m-d H:i:s');
                                    break;
                                    case '3':
                                        $data['PostedBy3'] =  getUserID();
                                        $data['DatePosted3'] =  date('Y-m-d H:i:s');
                                    break;
                                    case '4':
                                        $data['PostedBy4'] =  getUserID();
                                        $data['DatePosted4'] =  date('Y-m-d H:i:s');
                                    break;

                                }

                                $model->where([

									'TermID' => $term ,
									'StudentNo' => $stud ,
									'RegID' => $reg_id,
									'ScheduleID' => $parentSchedID ,
								])->update($data);
                            }

                        }
						
																					
						
                    }
					
					#Recompute General Average
					if($summary_id != '0')
					{
						$summary = new summary_service();
							
						$summary->recompute_ave($summary_id);
						$summary->evaluate_awardee($summary_id);		
					}	


                    $response = ['error'=> false,'message'=> 'Successfully posted!' ];

                break;

                case 'grade-table':

                    $tbl = (Request::get('table'));

                    switch($tbl){
                        case 'homeroom':
                            $tblid = $this->setup_mod->HomeRoomtbl();
                        break;
                        case 'club':
                            $tblid = $this->setup_mod->HomeRoomtbl();
                        break;
                        case 'grades' :
                            $tblid = $this->setup_mod->GradingSystemID();
                        break;
                    }

                    $data = [ 'tblid' => $tblid ];
                    $response = view($this->views.'gradetbl.table',$data);

                break;

                case 'import-score':

                    $event = Request::get('actvty');
                    $students = Request::get('data');
                    $term = decode(Request::get('term'));
                    $period = (Request::get('period'));

                    $total = 0;
                    $success = 0;
                    $fail = 0;

                    foreach($students AS $s){
                        if ( $s['id'] != '' ){
                            $data = array(
                                'StudentNo' => $s['id'],
                                'EventID' => $event,
                                'TransmutedGrade'=> '',
                                'LetterGrade'=>'',
                                'InputDate' => date('Y-m-d H:i:s'),
                       	        'TermID' => $term,
                                'GradingPeriod' => $period,
                                'RawScore' => $s['rw'],
                                'LastModifiedDate' => date('Y-m-d H:i:s'),
                                'LastModifiedBy' => getUserID(),
                            );
                            $output = $this->dbcon->updateOrCreate(array('EventID'=> $event , 'StudentNo' => $s['id']),$data);
                            if($output) $success++;
                        }else{
                            $fail++;
                        }
                        $total++;
                    }

                    if($success){
                        SystemLog('eClass-Records','','Import RawScore','Import Score from CSV', 'EventID='. $event ,'record successfully erased!' );
                    }

                    $response = ['error'=> false,'message'=> 'Successfully imported score(s)!', 'total'=> $total, 'success'=> $success, 'fail'=> $fail ];
                break;
			}
		}
        ob_clean();
		return $response;
    }

    private function save_grade($regid, $sched, $period, $grade, $final){

         $reg = New \App\Modules\Enrollment\Models\Registration;
         $schedules = New \App\Modules\ClassRecords\Models\Schedules_model;

         $model = new reportcard_model();
         $model2 = new reportcardetails_model();
         $subjmodel = new subject_model();

         $ids = Request::get('stud');

         $stud_reg = $reg->selectRaw("*, ISNULL(ClassSectionID,0) As SectionID ,  isnull(dbo.fn_SectionName(ClassSectionID),'') As SectionName")->where('RegID',$regid)->first();

         if(  $schedules->where('ScheduleID',$sched)->count() == 0 ) {
            return false;
         }

         $stud_sched = $schedules->where('ScheduleID',$sched)->first();
         $subj = $subjmodel->where('SubjectID',$stud_sched->SubjectID)->first();

         $idx = 0;

         $whre = array(
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'RegID' => $regid,
            'CampusID' => $stud_reg->CampusID,
         );

         $clsID = $reg->getProgClassID($stud_reg->ProgID)[0]->ClassID;

         $data = array(
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'RegID' => $regid,
            'YearLevelID' => $stud_reg->YearLevelID,
            'YearLevelName'=> $reg->getYearLevelName($stud_reg->YearLevelID, $clsID)[0]->YearLevel,
            'ProgramID' => $stud_reg->ProgID,
            'ClassSectionID' => $stud_reg->SectionID,
            'ClassSectionName'=> $stud_reg->SectionName,
            'ProgClassID'=> $clsID,
         );

         $idx = $model->updateOrCreate($whre,$data);
         $idx = $idx['SummaryID'];

         $whre = array(
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'RegID' => $regid,
            'CampusID' => $stud_reg->CampusID,
            'ScheduleID' => $sched,                
            'YearLevelID' => $stud_reg->YearLevelID,         
            'SummaryID' => $idx,
            'ScheduleID' => $sched,
            'YearLevelID' => $stud_reg->YearLevelID
         );

         $data = array(
            'SummaryID' => $idx,
            'RegID' => $regid,
            'YearLevelID' => $stud_reg->YearLevelID,
            'ProgramID'=> $stud_reg->ProgID,
            'TermID' => $stud_reg->TermID,
            'StudentNo' => $stud_reg->StudentNo,
            'ScheduleID' => $sched,
            'SubjectID' => $subj->SubjectID,
            'SubjectCode' => $subj->SubjectCode,
            'SubjectTitle' => $subj->SubjectTitle,
            'SubjectCreditUnits' => $subj->CreditUnits,
            'IsClubbing' => $subj->IsClubOrganization,
            'IsNonAcademic' => $subj->IsNonAcademic,
            'ParentSubjectID' => ( $subj->SubjParentID == NULL ? 0 : $subj->SubjParentID ),
            'ProgClassID'=> $clsID,
         );

        switch( $period ){
            case '1':
            case '11':
                $data['AverageA'] =  trimmed($final) ;
                $data['PeriodGrade1'] =  trimmed($grade) ;
                //$data['PostedBy1'] =  getUserID();
                //$data['DatePosted1'] =  date('Y-m-d H:i:s');
            break;
            case '2':
            case '12':
                $data['AverageB'] =  trimmed($final) ;
                $data['PeriodGrade2'] =  trimmed($grade) ;
                //$data['PostedBy1'] =  getUserID();
                //$data['DatePosted1'] =  date('Y-m-d H:i:s');
            break;
            case '3':

                $data['AverageC'] =  trimmed($final) ;
                $data['PeriodGrade3'] =  trimmed($grade) ;
                //$data['PostedBy1'] =  getUserID();
                //$data['DatePosted1'] =  date('Y-m-d H:i:s');
            break;

            case '4':
                $data['AverageD'] =  trimmed($final) ;
                $data['PeriodGrade4'] =  trimmed($grade) ;
                //$data['PostedBy1'] =  getUserID();
                //$data['DatePosted1'] =  date('Y-m-d H:i:s');
            break;
            case '14':
                $data['Final_Average'] =  trimmed($final) ;
                $data['PeriodGrade1'] =  trimmed($grade) ;
                $data['Final_Remarks'] = $this->dbcon->gradeRemarks('1', trimmed($final))->Remark;
            break;                       
            break;
        }

        $model2->updateOrCreate($whre,$data);

    }

    public function generate_layout(){
        $p  = Request::all();
            $this->initializer();

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            /*
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=Event_layout.csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            */

            ob_end_clean();
            ob_start();

            $filename = "event_layout.csv";
            $file = fopen($filename, 'w+');

            $total=0;

            $_id = Request::get('sched');
            $shs = Request::get('shs');
            $period = Request::get('period');

            $facid = getUserFacultyID();
            $sched = $this->sched_model->where('ScheduleID',$_id)->first();

            $gender = '';

            if($sched->FacultybyGender){
                if( $facid == $sched->FacultyID ){
                    $gender = 'M';
                } else if( $facid == $sched->FacultyID_2 ){
                    $gender = 'F';
                }else{
                    $gender = '';
                }
            }

            $data = $this->dbcon->get_students($_id,$gender,$shs,1);
            fputcsv($file,array('A'=>'No','B'=>'IDNo','C'=>'Student Name','D'=>'RawScore'));

            $i=1;

            foreach ($data as $r){
        		fputcsv($file, array(
                    'A'=> $i.'.',
                    'B' => $r->StudentNo,
                    'C'=> str_replace(",","",$r->StudentName),
                    'D'=>'',
                    ));
        		//$total=$total+$r->Amount;
        		$i++;
            }

            fclose($file);

           return Response::download($filename, 'event_layout.csv', $headers);


    }

    private function genEvents($c){

        $arr_event = [];
        $totalItems = 0;
        //$compx = new component_model();

        $gen = 0;

        $events = $this->event_model
            ->where('ScheduleID', $c->ScheduleID)
            ->where('GradingPeriod', $c->PeriodID)
            ->where('ComponentID', $c->ComponentID)
            ->where('Gender', $gen)
            ->get();

        if($this->shs == 1){
            $projection = 1;
        } else{
            //$projection = $compx->Projection($c->ComponentID)  ;
            $projection = $c->Cols;
          //$this->gender = 'F';
            switch($this->gender){
            case 'M': $gen = 1; break;
            case 'F': $gen = 2; break;
        }
        }

        if (count($events) > 0) {
            $j=1;
            foreach ($events as $e) {
                $arr_event[$j] = array(
                    'EventID' => $e->EventID,
                    'EventName' => $e->EventName,
                    'Description' => $e->EventDescription,
                    'EventDate' =>  ($e->EventDate != '' ?  date('m-d', strtotime($e->EventDate)) : '[Date]' ),
                    'TotalItems' => $e->TotalItems,
                    'Seq' => $e->SortOrder,
                );
                $totalItems += intval($e->TotalItems);
                $j++;
            }
        }
        $j = count($events)+1;

        if ($j <= $projection) {
            for ($j; $j <= $projection; $j++) {
                $arr_event[$j] = array(
                    'EventID' => 0,
                    'EventName' => $j,
                    'Description' => '',
                    'EventDate' => '[Date]',
                    'TotalItems' => '0',
                    'Seq' => '0',
                );
            }
        }

        return ['total' => $totalItems, 'events' => $arr_event  ];

    }

    private function initClassrecords($components ){
        $result = [];

        foreach($components As $c) {

            $this->IsHomeRoom = $c->IsHomeRoom;
            $this->IsConduct = $c->IsConduct;

            $events = ['total'=>0,'events'=>[]];
            $child_event = [];

            if (!$c->HasChild) {
                $events = $this->genEvents($c);
            }

            if ($c->ParentID == 0 ){
                $result[$c->ComponentID] = array(
                    'ComID'       => $c->ComponentID,
                    'CompCode'    => $c->CompCode,
                    'Caption'     => $c->Caption,
                    'Weight'      => $c->Percentage,
                    'Events'      => $events['events'] ,
                    'TotalItems'  => $events['total'],
                    'TotalEvents' => count($events['events']),
                    'Parent'      => $c->HasChild,
                    'Child'       => $child_event,
                    'totalChild'  => 0,
					'IsCSExempt'  => (($c->Caption=='Quarterly Exam')?1:0)
                );

            }else{

                $events = $this->genEvents($c);
                $child_event = array(
                    'ComID' => $c->ComponentID,
                    'CompCode' => $c->CompCode,
                    'Caption' => $c->Caption,
                    'Weight' => $c->Percentage,
                    'Events' => $events['events'] ,
                    'TotalItems' => $events['total'],
                    'TotalEvents' => count($events['events']),
                    'Parent' => $c->HasChild,
					'IsCSExempt'  => (($c->Caption=='Quarterly Exam')?1:0)
                );

                $totalevent = $result[$c->ParentID]['TotalEvents'];

                array_push($result[$c->ParentID]['Child'], $child_event);
                $totalevent = $totalevent + ( count($events['events']) + 3)  ;
                $result[$c->ParentID]['TotalEvents'] = $totalevent ;
                $result[$c->ParentID]['Parent'] = 1 ;
                $result[$c->ParentID]['Events'] = '' ;
            }

        }
        //err_log($result);
        return $result;
    }

    public function printReport(){
        ob_clean();
        $this->pdf = new PDF;

        set_time_limit(900);
      //ini_set('memory_limit', '600');

        $gradingsys = \App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID','1')->where('Inactive','0')->get();
        $conduct = \App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID','2')->get();
        $schedules = new \App\Modules\ClassRecords\Models\Schedules_model;
        
        try{
            $this->pdf->setPrintHeader(false);
            $this->pdf->setPrintFooter(true);

            $this->pdf->SetMargins(50, 50, 50, true);
            $this->pdf->setPageUnit('pt');

            $_id = Request::get('sched');
            $pid = (Request::get('period'));
            $sched = $schedules->selectRaw("*, dbo.fn_EmployeeName3(FacultyID) As Teacher,dbo.fn_EmployeeName3(FacultyID_2) As Teacher2, dbo.fn_SubjectCode(SubjectID) As Code, dbo.fn_SubjectTitle(SubjectID) As Subject, dbo.fn_SectionName(SectionID) As Section ")->where('ScheduleID', $_id )->first();
             
            // foreach($param as $g){
            $this->pdf->SetFont('helvetica', '', 8);
            $this->pdf->AddPage('L','LEGAL');
            $this->pdf->setTitle('Preview: Class Record');
            $this->pdf->writeHTML(view('reports.class-record',['_id' => $_id,'sched'=>$sched, 'pid' => $pid])->render());
            /*
            echo (string)view('reports.class-record',['_id' => $_id,'sched'=>$sched, 'pid' => $pid]);
            die('for checking');*/
            ob_end_clean();
            set_time_limit(30);
            ini_set('memory_limit', '256M');
            $this->pdf->output();
        }catch (Exception $e){

            $_id = Request::get('sched');
            $pid = (Request::get('period'));
            $sched = $schedules->selectRaw("*, dbo.fn_EmployeeName3(FacultyID) As Teacher,dbo.fn_EmployeeName3(FacultyID_2) As Teacher2, dbo.fn_SubjectCode(SubjectID) As Code, dbo.fn_SubjectTitle(SubjectID) As Subject, dbo.fn_SectionName(SectionID) As Section ")->where('ScheduleID', $_id )->first();
             
            echo (string)view('reports.class-record',['_id' => $_id,'sched'=>$sched, 'pid' => $pid]);
            die('for checking');
        }
    }

    public function print_data()
    {
        $this->printReport();
        die();
        $this->initializer();
        $term = decode(Request::get('term'));
        $_id = Request::get('sched');
        $period = Request::get('period');
        $this->shs = Request::get('shs');
        $hrm = Request::get('hrm');

        $subrep_logo = array(
            'file' => 'Company_Logo',
            'query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'"
        );

        if($hrm == '1'){
            $this->xpdf->filename = 'class-record-homeroom.rpt';
        }else{
            $this->xpdf->filename = 'class-record.rpt';
        }

        $this->xpdf->query = "EXEC sp_classrecord '".$term."','0','".$_id."','".$period."','0'";

        $subrpt = array('subreport1' => $subrep_logo,);
        $this->xpdf->SubReport = $subrpt;
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="class-record.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
        die();
    }

 	private function initializer()
 	{
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;
        $this->service = new service();
        $this->event_model = new event_model();
        $this->sched_model = new sched_model();
        $this->setup_mod = new gs_setup_model();
        $this->xpdf = new xpdf;
 	}
}
