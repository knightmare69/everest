<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\Schedules_model As dbcon;
use Permission;
use Request;

class FacultySchedules extends Controller{
    protected $ModuleName = 'schedules';
    
	private $media = [ 'Title'=> 'Faculty Schedules',
    	               'Description'=> 'Welcome To Faculty Schedules!',
    	               'js'		=> ['ClassRecords/faculty'],
    	               'init'		=> ['ME.init()'],
    	               'plugin_js'	=> ['bootbox/bootbox.min',
                                        'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
                                        'bootstrap-select/bootstrap-select.min',],
                       'plugin_css' => ['jstree/dist/themes/default/style.min',
                                        'bootstrap-select/bootstrap-select.min',                                        
                                        'select2/select2',
                                        'jquery-multi-select/css/multi-select'],  
                     ];

	private $url = [ 'page' => 'class-record/schedules/' ];

	private $views = 'ClassRecords.Views.Schedules.';

	function __construct()
	{
		$this->initializer();
	}
    
 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {
                        
            $faculty_id = getUserFacultyID();
                                                
            $data = array(
                'academic_year' => $this->dbcon->get_academicyearterm($faculty_id),
                'facultyId'=> $faculty_id,
            );
            
            return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}
    
    public function event_service(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{

			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
                case 'schedules':
                    $term = decode(Request::get('term'));
                    //$facid = getUserFacultyID();         
                    if( getUserName() === 'admin' ) {
                        $facid = '';   
                    }else{
                        $facid = getUserFacultyID();
                        $facid = ($facid == '' ? '0': $facid );  
                    }    
                
                    $data = view($this->views.'table',['table'=>$this->dbcon->View($term,$facid)->get()])->render();                                                            
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                 break;

			}
		}
		return $response;
    }
    

 	private function initializer()
 	{
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;
 	}
}