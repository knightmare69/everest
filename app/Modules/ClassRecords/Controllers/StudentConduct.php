<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\Conduct\Conduct_model As dbcon;
use App\Modules\ClassRecords\Models\Schedules_model As sched_model;
use App\Modules\ClassRecords\Models\Attendance\Quarter As period_model;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use Permission;
use Request;

class StudentConduct extends Controller{
    protected $ModuleName = 'student-conduct';

	private $media = [ 
        'Title' => 'Student Conduct',
        'Description'   => 'Welcome To Student Conduct!',
        'js'		=> ['ClassRecords/conduct'],
        'init'		=> ['ME.init()'],
        'css'         => ['profile'],
        'plugin_js'	=> ['bootbox/bootbox.min', 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min', 'bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification.min'],
        'plugin_css' => ['jstree/dist/themes/default/style.min', 'bootstrap-select/bootstrap-select.min', 'select2/select2', 'jquery-multi-select/css/multi-select','SmartNotification/SmartNotification'],
    ];

	private $url = [ 'page' => 'class-record/student-conduct/' ];

	private $views = 'ClassRecords.Views.Conduct.';

	function __construct() {
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {

            $faculty_id = getUserFacultyID();                        
            
            $data = array(
                'academic_year' => $this->sched->get_academicyearterm($faculty_id),                
                'conduct_id' => $this->setup->ConductID(),
            );

            return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

    public function event_service(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
                case 'schedules':                
                                    
                    $term = decode(Request::get('term'));
                    $facid = getUserFacultyID();
                    $period = Request::get('period');
                    $type = Request::get('type');
                    
                    if($period == 0){
                        $periods = $this->periods->where('GroupClass','21');
                    }else{
                        $periods = $this->periods->where('GroupClass','21');
                    }
                    
                    if( $type == 0 ){
                        $arr = $this->dbcon->getClassSections($term,$facid);    
                        $data = view($this->views.'sub.sections',['table' => $arr])->render();
                    }else{
                        $arr = $this->dbcon->getClassSchedules($term,$facid);
                        $data = view($this->views.'sub.schedules',['table' => $arr])->render();
                    }
                                                                                                                       
                    $periods = view($this->views.'sub.period',['table' => $periods->get()])->render();
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data, 'period'=> $periods ];
                 
                 break;
                 
                 case 'students':
                    
                    $term = decode(Request::get('term'));
                    $sec = decode(Request::get('section'));
                    $per = decode(Request::get('period'));
                    $type = Request::get('type');
                    
                    $students = $this->dbcon->getStudents($term,$sec,$per, $type );
                    
                    $data = view($this->views.'students',['conduct_id' => $this->setup->ConductID(),'data'=>$students])->render();
                    
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                    
                 break;
                 
                 case 'save-conduct': 
                    
                    $post = Request::all();
                         
                        $type = getObjectValue($post, 'type');
                         
                         
                         
                       
						$data = array(                                        
                            'StudentNo' => (getObjectValue($post, 'idno')),
                 	        'TermID' => decode(getObjectValue($post, 'term')),
                            'PeriodID' => decode(getObjectValue($post, 'period')),
                            'ConductID' => getObjectValue($post, 'conduct'),
                            'Score' => trim(getObjectValue($post, 'score')),
                                    
                            'Modifiedby' => getUserID(),
                            'DateModified' => date('Y-m-d H:i:s')
                        );                                                                                
                                                                        
                        $whre = [                             
                            'StudentNo' => (getObjectValue($post,'idno')),                            
                            'PeriodID' => decode(getObjectValue($post,'period')),                                
                            'ConductID' => getObjectValue($post,'conduct'),
                        ];       
                        
                        if($type == 0 ){
                            $data['SectionID'] = decode(getObjectValue($post, 'section'));
                            $data['IsAdviser'] = 1;
                            $whre['SectionID'] = decode(getObjectValue($post, 'section'));
                        }else{
                            $data['ScheduleID'] = decode(getObjectValue($post, 'section'));
                            $data['IsAdviser'] = 0;
                            $whre['ScheduleID'] = decode(getObjectValue($post, 'section'));
                        }
                                                                      
                        $rs = $this->dbcon->where($whre)->first();
                        
                        if($rs){
                            $this->dbcon->where($whre)->update($data);
                        }else{
                            $data['Createdby'] = getUserID();
                            $data['DateCreated'] = date('Y-m-d H:i:s');
                            $this->dbcon->create($data);
                        }
                        
						$response = ['error'=>false,'message'=>'Successfully Save!'];
                        SystemLog('Student-Conduct','','save-conduct','save-conduct', json_encode($data) ,'record successfully saved!' );
                        					                    
                break;
                
                case 'erase-conduct':
                	if ($this->permission->has('delete')) {
                	   
                        $model = new conduct_model();
                        $id = decode(Request::get('index'));
                        $idno = decode(Request::get('idno'));
                        $conid = (Request::get('conduct'));
                        
                        $model->destroy($id);						
						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog('eClass-Records','','erase-conduct','Erase Conduct Score', 'ConductID='. $conid.' StudentNo='. $idno,'record successfully erased!' );						
					}
                     
                break;
                
                case 'post':
                    
                                        
                    $post = Request::all();                                         
                                        
                    foreach($post['stud'] As $s){                                                
                        
                        $whre = array(
                            'TermID' => decode(getObjectValue($post, 'term')),
                            'StudentNo' => ($s['idno']),                                                             
                            'PeriodID' => decode(getObjectValue($post, 'period')),                                    	                                    
                        );
                        if(getObjectValue($post, 'type') == 0){
                            $whre['SectionID'] = decode(getObjectValue($post, 'section'));
                        }else{
                            $whre['ScheduleID'] = decode(getObjectValue($post, 'section'));
                        }   
                                                                                                                                            
                        $data = array(
                            'DatePosted' =>  date('Y-m-d H:i:s'),
                            'Postedby' => getUserID(),
                        );
                                                                                             
                        $this->dbcon->where($whre)->update($data);                                                                        
                    }                    
                    
                    $response = ['error'=> false,'message'=> 'Successfully posted!' ];
                    SystemLog('Student-Conduct','','post-conduct','post-conduct', json_encode($whre) ,' posted conduct grade.' );
                    
                break;
                
                 
                 default : break;
			}
		}
		return $response;
    }



 	private function initializer() {
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;        
        $this->periods = new period_model;
        $this->sched = new sched_model();
        $this->setup= new gs_setup_model();
 	}
    
    public function generate_layout(){
        $p  = Request::all();
            $this->initializer();
    		
            $headers = array(
                'Content-Type' => 'text/csv',
            );
                        
            ob_end_clean();
            ob_start();
            
            $filename = "student_conduct.csv";
            $file = fopen($filename, 'w+');
        
            $total=0;
            
            $_id = Request::get('sched');                                                                                     
            $period = Request::get('period');
            
            $facid = getUserFacultyID();
            $sched = $this->sched_model->where('ScheduleID',$_id)->first();
            
            $gender = '';

            if($sched->FacultybyGender){
                if( $facid == $sched->FacultyID ){
                    $gender = 'M';
                } else if( $facid == $sched->FacultyID_2 ){
                    $gender = 'F';
                }else{
                    $gender = '';
                }
            }
            
            $data = $this->dbcon->get_students($_id,$gender,$shs,1);                                            
            fputcsv($file,array('A'=>'No','B'=>'IDNo','C'=>'Student Name','D'=>'RawScore'));
            
            $i=1;        	        	
        	
            foreach ($data as $r){
        		fputcsv($file, array(
                    'A'=> $i.'.', 
                    'B' => $r->StudentNo,
                    'C'=> str_replace(",","",$r->StudentName), 
                    'D'=>'',  
                    ));
        		//$total=$total+$r->Amount;
        		$i++;
            }
            
            fclose($file);
            
           return Response::download($filename, 'event_layout.csv', $headers);    
        
    }
    
    
}