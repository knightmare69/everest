<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\StudentClubs\StudentClubs_model As dbcon;
use App\Modules\ClassRecords\Models\eClassRecord_model As mRawscore;
use App\Modules\Academics\Models\Club\Organization As mClub;
use App\Modules\ClassRecords\Models\Attendance\Quarter As period_model;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use App\Modules\ClassRecords\Services\StudentClubServices;
use App\Libraries\CrystalReport as xpdf;
use Permission;
use Request;

class StudentClubs extends Controller{
    protected $ModuleName = 'student-clubs';

	private $media = [ 
        'Title' => 'Student Clubs',
        'Description'   => 'Welcome To Student Clubs!',
        'js'		 => ['ClassRecords/studentclubs'],
        'init'		 => ['ME.init()'],
        'css'        => ['profile'],
        'plugin_js'	 => ['bootbox/bootbox.min', 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min', 'bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification.min'],
        'plugin_css' => ['jstree/dist/themes/default/style.min', 'bootstrap-select/bootstrap-select.min', 'select2/select2', 'jquery-multi-select/css/multi-select','SmartNotification/SmartNotification'],
    ];

	private $url = [ 'page' => 'class-record/student-clubs/' ];

	private $views = 'ClassRecords.Views.StudentClubs.';
    
    private function initializer() {
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;        
        $this->periods = new period_model;        
        $this->setup= new gs_setup_model();
        $this->service = new StudentClubServices();        
 	}
    
	function __construct() {
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {

            $faculty_id = getUserFacultyID();                        
            
            $data = array(
                'academic_year' => $this->service->academic_year($faculty_id),                
                'club_id' => $this->setup->ClubID(),
                'term' => '',
                'period' => '',
                'club' => ''
            );

            return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

    public function event_service(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
                case 'schedules':                
                    
                    $club=new mClub();
                    
                    $term = decode(Request::get('term'));
                    $facid = getUserFacultyID();
                    $period = Request::get('period');                    
                    
                    if($period == 0){
                        $periods = $this->periods->where('GroupClass','21');
                    }else{
                        $periods = $this->periods->where('GroupClass','1');
                    }
                                       
                    // TEMP :  Highschool Only
                    $whr = array(
                        'TermID'=> $term , 
                        'ProgID' => '1' ,                        
                    );
                    
                    if( getUserGroupID() != '1') {
                        /* If Not Administrator Group */
                        $whr['ModeratorID']  = $facid;    
                    }
                                                            
                    $arr = $club->select(['ClubID','ClubName'])->where($whr)->get();
                    $data = view($this->views.'sub.clubs',['table' => $arr ,])->render();
                                                                                                                                          
                    $periods = view($this->views.'sub.period',['table' => $periods->get()])->render();
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data, 'period'=> $periods ];
                 
                 break;
                 
                 case 'students':
                    
                    $term = decode(Request::get('term'));
                    $club = decode(Request::get('club'));
                    $per = decode(Request::get('period'));
                 
                    $students = $this->dbcon->getStudents($term,$club,$per);                    
                    $data = view($this->views.'students',['club_id' => $this->setup->ClubID(), 'term'=> $term, 'period' => $per,  'club' => $club  ,'data'=>$students])->render();                    
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                    
                 break;
                 
                 case 'save-grade': 
                    
                    $rawscore = new mRawscore();
                    $post = Request::all();                                                 
                                                                                                  
					$data = array(                                        
                        'StudentNo' => decode(getObjectValue($post, 'idno')),
             	        'TermID' => decode(getObjectValue($post, 'term')),
                        'GradingPeriod' => decode(getObjectValue($post, 'period')),      
                        'EventID' => decode(getObjectValue($post, 'idx')),
                        'RawScore' => getObjectValue($post, 'score'),
                        'InputDate' => date('Y-m-d H:i:s')  ,                                                     
                        'LastModifiedBy' => getUserID(),
                        'LastModifiedDate' => date('Y-m-d H:i:s'),
                        'TransmutedGrade'=>0,
                        'LetterGrade'=>'',
                        'IsAbsent'=>0,
                    );                                                                                
                                                                        
                    $whre = [        
                        'TermID' => decode(getObjectValue($post, 'term')),                     
                        'StudentNo' => decode(getObjectValue($post,'idno')),                            
                        'GradingPeriod' => decode(getObjectValue($post,'period')),                                
                        'EventID' => decode(getObjectValue($post,'idx')),
                    ];                                                   
                                                                      
                    $rs = $rawscore->updateOrCreate($whre,$data);                                            
                    $this->service->register_grade(decode($post['reg']),decode($post['sched']),decode($post['period']),$post['grade'],$post['grade']);
                    
					$response = ['error'=>false,'message'=>'Successfully Save!'];
                    SystemLog($this->media['Title'],'','save-grade','event score', $post ,'record successfully saved!' );
                        					                    
                break;
                
                case 'erase-grade':
                	if ($this->permission->has('delete')) {
                	   
                        $model = new mRawscore();
                        //$id = decode(Request::get('index'));
                        $idno = decode(Request::get('idno'));
                        $event = decode(Request::get('activity'));
                        
                        $model->where('EventID',$event)
                                ->where('StudentNo',$idno)
                                ->delete();					
						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog($this->media['Title'],'','erase-grade','Erase Club Score', 'Event ='. $event.' StudentNo='. $idno . ' Name='. Request::get('name') ,'record successfully erased!' );						
					}
                     
                break;
                
                case 'post':
                                                                                                                                                      
                    $res = $this->service->post_grade(Request::all());                    
                    $response = ['error'=> false,'message'=> 'Successfully posted!' ];                    
                    
                break;
                
                 
                 default : break;
			}
		}
		return $response;
    }

    public function print_data()
    {
        
        $this->xpdf = new xpdf;
        $term = decode(Request::get('term'));
        $_id = Request::get('club');
        $period = Request::get('period');
        
        $subrep_logo = array(
            'file' => 'Company_Logo',
            'query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', '".env('APP_REPUBLIC','')."', '".env('APP_ADDRESS','')."', '-1'"
        );
        
        $this->xpdf->filename = 'class-record-clubs.rpt';
        
        $this->xpdf->query = "EXEC sp_classrecord '".($term)."','0','".decode($_id)."','".decode($period)."','0'";
        
        $subrpt = array('subreport1' => $subrep_logo,);
        $this->xpdf->SubReport = $subrpt;
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="class-record.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
        die();                  
    } 
    
    public function generate_layout(){
        $p  = Request::all();
            $this->initializer();
    		
            $headers = array(
                'Content-Type' => 'text/csv',
            );
                        
            ob_end_clean();
            ob_start();
            
            $filename = "student_clubs.csv";
            $file = fopen($filename, 'w+');
        
            $total=0;
            
            $_id = Request::get('sched');                                                                                     
            $period = Request::get('period');
            $facid = getUserFacultyID();                       
            $gender = '';
            
            $data = $this->dbcon->get_students($_id,$gender,$shs,1);                                            
            fputcsv($file,array('A'=>'No','B'=>'IDNo','C'=>'Student Name','D'=>'RawScore'));
            
            $i=1;        	        	
        	
            foreach ($data as $r){
        		fputcsv($file, array(
                    'A'=> $i.'.', 
                    'B' => $r->StudentNo,
                    'C'=> str_replace(",","",$r->StudentName), 
                    'D'=>'',  
                    ));
        		//$total=$total+$r->Amount;
        		$i++;
            }
            
            fclose($file);
            
           return Response::download($filename, 'event_layout.csv', $headers);    
        
    }
    
    
}