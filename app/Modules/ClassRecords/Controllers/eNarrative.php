<?php 
namespace App\Modules\ClassRecords\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\eClassRecord_model As dbcon;
use App\Modules\ClassRecords\Services\ClassRecords\service_narrative as service;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use App\Libraries\CrystalReport as xpdf;

use Request;
use Permission;
use Response;
use DB;

class eNarrative extends Controller{
   protected $ModuleName = 'eclass-records';

	private $media = [
			'Title'       => 'eNarrative',
			'Description' => 'Welcome To eNarrative!',
			'js'		  => ['ClassRecords/narrative.js?v=1.0'],
			'init'		  => [],
            'css'         => ['profile','classrecord'],
            'plugin_js'	  => ['bootbox/bootbox.min','bootstrap-datepicker/js/bootstrap-datepicker','bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
                                'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min','bootstrap-fileinput/bootstrap-fileinput','navigator/file-navigator.min','navigator/line-navigator.min',
                                'bootstrap-select/bootstrap-select.min', 'SmartNotification/SmartNotification.min'
                             ],
           'plugin_css' => ['jstree/dist/themes/default/style.min','bootstrap-fileinput/bootstrap-fileinput',
                            'bootstrap-select/bootstrap-select.min',
                            'select2/select2','SmartNotification/SmartNotification',
                            'bootstrap-datepicker/css/datepicker','bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
                            'jquery-multi-select/css/multi-select'],
	];

    private $url = [ 'page' => 'class-record/eclass-records/' ];

    private $views = 'ClassRecords.Views.eNarrative.';

    protected $dbcon ;
	
    function __construct(){
		$this->initializer();
    }	
   
    public function index(){
	    $this->initializer();
		$drop = $this->service->getDropdown(getUserID());
        $data = array('views'      => $this->views
					 ,'permission' => $this->permission
		             ,'data'       => array()
					 ,'ayterm'     => $drop['term']
					 ,'section'    => $drop['section']
					 ,'tblid'      => $this->setup_mod->GradingSystemID()
					  );
        SystemLog('eClass-Records','','Page View','page-view','Page: eClass-Records','' );
        return view('layout',array('content'=>view($this->views.'index',$data)->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        //return view(config('app.403'));
    }
   
    function initializer(){
	   $this->permission = new Permission($this->ModuleName);
	   $this->dbcon = new dbcon;
	   $this->service = new service();
       $this->setup_mod = new gs_setup_model();
	   $this->xpdf = new xpdf;
    }
   
    function txn(){
	    $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'list':
				   $termid    = Request::get('term');
				   $section   = Request::get('section');
				   $period    = Request::get('period');
				   $exec      = $this->service->LoadList(getUserID(),decode($section),$period);
				   if($exec){
					 $progid  = $exec[0]->ProgID;  
					 $yrlvl   = $exec[0]->YearLevelID;  
					 $gssetup = DB::select("SELECT * FROM ESv2_GradeSheet_Setup WHERE TermID='".decode($termid)."' AND YearLevelID='".$yrlvl."' AND ProgID='".$progid."'")[0];
					 $tblid   = ((getObjectValue($gssetup,'LetterGradeID'))?getObjectValue($gssetup,'LetterGradeID'):$this->setup_mod->GradingSystemID());
					 $tblid   = $this->setup_mod->ConductID();
				   }
				   $content   = (string)view($this->views.'student',array('views'=>$this->views,'arr'=>$exec,'period'=>$period,'tblid'=>$tblid));
				   $response  = ['success'=>true,'content'=>$content];
				break;
				case 'save':
				   $data         = Request::all();
				   $data['term'] = decode($data['term']);
				   $exec    = $this->service->UpdateOrSave($data);
				   $content = '';//(string)view($this->views.'trdata',array('arr'=>$exec,'period'=>$period));
				   $response = ['success'=>true,'content'=>$content];
				break;
				case 'padlock':
				   $data         = Request::all();
				   $data['term'] = decode($data['term']);
				   $exec    = $this->service->UpdateOrSave($data);
				   $content = '';//(string)view($this->views.'trdata',array('arr'=>$exec,'period'=>$period));
				   $response = ['success'=>true,'content'=>$content];
				break;
				case 'remove':
				   $data         = Request::all();
				   $data['term'] = decode($data['term']);
				   $exec     = DB::statement("UPDATE ESv2_Narrative SET Narrative".$data['period']."='' WHERE TermID='".$data['term']."' AND StudentNo='".$data['studno']."'");
				   $content  = '';//(string)view($this->views.'trdata',array('arr'=>$exec,'period'=>$period));
				   $response = ['success'=>true,'content'=>$content];
				break;
			    default:
				   $response['message'] = 'Nothing to process';
                break;				
			}
        }
        ob_clean();
		return Response::json($response);			
    }  
}
?>