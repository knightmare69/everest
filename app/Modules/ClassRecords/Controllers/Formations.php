<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;

use App\Modules\Setup\Models\AcademicYearTerm As ayterm;
use App\Modules\ClassRecords\Models\Formation_model As dbcon;
use App\Modules\ClassRecords\Services\Formations\service_provider as service;

use Request;
use Permission;

class Formations extends Controller{
    
    protected $ModuleName = 'subject-formations';

	private $media = [ 'Title'  => 'Subject Formations', 'Description' => 'Welcome To Subject Formations!',
			'js'		=> ['ClassRecords/formations'],
			'init'		=> ['ME.init()'],
			'plugin_js'	=> ['bootbox/bootbox.min', 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min', 'bootstrap-select/bootstrap-select.min',],
            'plugin_css' => ['jstree/dist/themes/default/style.min',
                            'bootstrap-select/bootstrap-select.min',
                            'select2/select2',
                            'jquery-multi-select/css/multi-select'],
		];

	private $url = [ 'page' => 'class-record/subject-formations/' ];

	private $views = 'ClassRecords.Views.Formations.';

	function __construct(){
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {

            $data = array(
                'academic_year' => $this->term->orderBy('AcademicYear', 'desc')->get()
                ,'term' => 0
            );

            return view('layout',array('content'=>view($this->views.'index',$data)->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

     public function myservice(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{

			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
			 
                case 'policy-setup':
                    $index = trimmed( decode(Request::get('sched')));
                    $shs = Request::get('shs');
                    $query = $this->dbcon->View($index)->get();    
                    $term = decode(Request::get('term'));
                    
                    $param = [ 
                                 'data'=>$query
                                ,'term'=>$term                                 
                                ,'class' => ($shs != 0 ? '21' : 1 )                                  
                             ];
                    $data = view($this->views.'sub.periods',$param)->render();                                                                                    
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                     
                break;
             
                case 'get-policy':
                 
                    $index = trimmed( decode(Request::get('sched')));                                        
                    $query = $this->dbcon->View($index)->get();
                    $data = view($this->views.'policy',['data'=>$query])->render();                        
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                    
                break;
                 
			     case 'get-components':                                         
                    $policy = trimmed( decode(Request::get('policy')));
                    $data = view($this->views.'components',['policy'=>$policy])->render();                        
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];                    
                 break;

			     case 'schedules':
                    $term = decode(Request::get('term'));
                    $data = view($this->views.'table',['table'=>$this->dbcon->get_schedules($term)])->render();
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                 break;

				case 'save':
					if ($this->permission->has('add')) {
						$validation = $this->services->isValid(Request::all());
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							$data = $this->services->post(Request::all());
							$data['created_date'] = systemDate();
							$data['created_by'] = getUserName();
							$this->model->create($data);
							$response = ['error'=>false,'message'=>'Successfully Save!','table'=> $this->refreshTable()];
						}
					}
					break;
				case 'update':
					if ($this->permission->has('edit')) {
						$validation = $this->services->isValid(Request::all(),'update');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							$data = $this->services->post(Request::all());
							$data['modified_date'] = systemDate();
							$data['modified_by'] = getUserName();
							$this->model->where('index_id',decode(Request::get('id')))->update($data);
							$response = ['error'=>false,'message'=>'Successfully Update!','table'=> $this->refreshTable()];
						}
					}
					break;
				case 'delete':
					if ($this->permission->has('delete')) {
						$ids = json_decode('['.Request::get('ids').']',true);
						foreach($ids as $id){
							 $this->model->destroy(decode($id['id']));
						}
						$response = ['error'=> false,'message'=>'Successfully Deleted','table'=> $this->refreshTable()];
					}
					break;
				case 'set-policy':
					if ($this->permission->has('add')) {
						$record = Request::get('setup');

						foreach($record as $id) {
                             $sched = decode($id['sched']);
							 $data = $this->service->postdetails($id);
                             $this->dbcon->updateOrCreate(array('ScheduleID'=> $sched, 'PeriodID' => $id['quarter'] ),$data);
						}

						$response = ['error'=> false,'message'=>'Successfully update policy'];
					}

                break;

				case 'edit':
				    $response = view($this->views.'forms.form',['data'=>$this->model->find(decode(Request::get('id')))]);
				break;
			}
		}
		return $response;
    }

    private function refreshTable(){
		return view($this->views.'table',['table'=>$this->dbcon->get_view()])->render();
	}

 	private function initializer()
 	{
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;
        $this->term = new ayterm();        
        $this->service = new service();
 	}
}