<?php

namespace App\Modules\ClassRecords\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as tModel;
use App\Modules\ClassRecords\Models\Mentoring_model;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Students\Models\FormationPlan_model As mFormplan;
use App\Libraries\CrystalReport as xpdf;

use Permission;
use Request;
use Response;
use DB;

class Mentoring extends Controller
{
    protected $ModuleName = 'mentoring';

    private $media = [
            'Title' => 'Student Mentoring',
            'Description' => 'Welcome To Student Mentoring',
            'js' => ['ClassRecords/mentoring.js?v=1.0'],
            'css' => ['profile'],
            'init' => ['MOD.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'SmartNotification/SmartNotification.min'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'SmartNotification/SmartNotification',
            ],
        ];

    private $url = ['page' => 'class-record/mentoring'];

    private $views = 'ClassRecords.Views.Mentoring.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();

        if ($this->permission->has('read')) {

           $mterm = new tModel;

            $term = decode(Request::get('t'));
            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),
                'data' => $this->getData('list',$term,$f,($v == 'undefined' ? 1 : $v)),
                'total' => $this->getData('total',$term,$f),
                'term' => $term,
                'filter'=>$f,
                'page'=> ($v == 'undefined' ? 1 : $v)
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
            
        }
        return view(config('app.403'));

 	}

    public function sheet()
 	{
 		$this->initializer();

        if ($this->permission->has('read')) {

           $mterm = new tModel;

            $term = decode(Request::get('t'));
            $idno = decode(Request::get('idno'));
            $se = Request::get('ref');
            
            if($se != ''){
                $stud = $this->mentorsession->where('SessionID', decode($se))->selectRaw("*, dbo.fn_StudentName(StudentNo) As StudentName
                            ,dbo.fn_StudentYearLevel_k12(StudentNo) As YearLevel ")->first();;
            }else{
                $stud = $this->model->where('StudentNo', $idno)->selectRaw("StudentNo, LastName, FirstName, ProgID, YearLevelID, dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel, CurriculumID, dbo.fn_CurriculumCode(CurriculumID) AS CurriculumCode , dbo.fn_MajorName(MajorDiscID) As Major ")->first();    
            }      
            
            $plan = $this->plan->where('StudentNo', $idno)->first();
            $history = $this->mentorsession->where('StudentNo', $idno)->selectRaw("*, dbo.fn_StudentName(StudentNo) As StudentName
                            ,dbo.fn_StudentYearLevel_k12(StudentNo) As YearLevel, dbo.fn_AcademicYearTerm(TermID) As YearTerm ")->get();
                            

            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),
                'data' => $stud,
                'term' => $term,
                'idno' => $idno,
                'se_id' => $se
                ,'plan' => $plan
                ,'history' => $history
            ];

            return view('layout',array('content'=>view($this->views.'sheet')->with($_incl),'url'=>$this->url,'media'=>$this->media));
            
        }
        return view(config('app.403'));

 	}
    
    private function getData($mode,$term, $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * ($page == 0 ? 0 : $page-1)));
        $rs = null;
        if($term != ''){

            $whereRaw = "TermID = {$term} ";
            $whereRaw .= " AND ProgID IN (".implode(getUserProgramAccess(),',') .")";

            $query = "SELECT *
                            , dbo.fn_StudentName(StudentNo) As StudentName
                            ,dbo.fn_StudentYearLevel_k12(StudentNo) As YearLevel        
                                               
                      FROM ES_Mentoring WHERE " . $whereRaw . " Order By StudentName ASC
                      OFFSET     {$skip} ROWS
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('ES_Advising')
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }
        }

        return $rs;
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {

               
                case 'get-students':

                    if($this->permission->has('read')){
                        $model = new mStudents();

                        $find = Request::get('find');

                        if( $find == ''){ $find = date('Y').'%'; }
                        $data = $model->studentsSearch($find,'50');

                        if(!empty($data)){
                            $vw  = view($this->views.'students', ['data' => $data])->render();
                            $response = ['error' => false, 'data' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }
                    }

                break;

                case 'remove':
                    $list = Request::get('idno');
                    foreach($list as $r){
                        $ret = $this->mentorsession->where(['SessionID'=>decode($r['ref']) ])->delete();
                    }

                    $response = successDelete();
                break;
                 
                case 'get-grades':

                    $idno   = decode(Request::get('idno'));

               	    $grades = DB::table('ES_CurriculumDetails as cd')
                        ->leftJoin("ES_Students as x",'x.curriculumid','=','cd.curriculumid')
                        ->leftJoin('ES_Subjects as s','s.SubjectID','=','cd.SubjectID' )
                        ->selectRaw("cd.IndexID, cd.CurriculumID, cd.SubjectID, s.SubjectCode, s.SubjectTitle, s.LectHrs As Credit, s.IsPassFail , YearTermID, dbo.fn_YearTerm(YearTermID) AS YearTerm ")
                        ->where('x.studentno', $idno )
                        ->whereRaw("YearTermID <> 22")
                        ->orderBy('cd.YearTermID','asc')
                        ->orderBy('cd.SortOrder','asc')
                        ->get();


                    $vw  = view($this->views.'evaluation.list', ['data' => $grades, 'idno'=> $idno ])->render();

				    $response = Response::json(['success' => true
					                           ,'data' => $vw
					                           ,'error'   => false
											   ,'message' => ''
                    ]);

                break;

                case 'save-session':
                    
                    $post   = Request::all();
                    
                    $cond = array(
                        'SessionID' => decode(getObjectValue($post, 'refid')),
                    );
                    
                    $data = array(
                         'TransDate' =>  getObjectValue($post, 'date')
                          ,'StudentNo' => decode( getObjectValue($post, 'idno'))
                          ,'YearLevelID' =>  getObjectValue($post, 'level')
                          ,'ProgID' => getObjectValue($post, 'prog')
                          ,'TermID' => decode(getObjectValue($post, 'term'))
                          ,'Topic' => getObjectValue($post, 'topic')
                          ,'Issue' => getObjectValue($post, 'issue')
                          ,'Goals' => getObjectValue($post, 'goal')
                          ,'MentorFollowUp' => getObjectValue($post, 'followup')
                          ,'MentorID' => getUserFullName()
                          ,'DateCreated' => systemDate()
                          ,'UserID' => getUserID()
                          ,'DateModified' => systemDate()
                          ,'Modifiedby'	=> getUserID()         
                    );
                    
                    $this->mentorsession->updateOrCreate($cond,$data);
                    
                    $response = ['error' => false, 'message' => 'record successfully saved!'];

                    break;
                
                case 'save-formation':
                    
                    $post   = Request::all();
                    
                    $cond = array(
                        'RefID' => decode(getObjectValue($post, 'refid')),
                    );
                    
                    $data = array(                                                                               
                          
                          'StudentNo' => decode( getObjectValue($post, 'idno'))
                          ,'Easy' =>  getObjectValue($post, 'easy')
                          ,'Hard' => getObjectValue($post, 'hard')
                          ,'Future' => getObjectValue($post, 'future')
                          
                      ,'SpiritualStudent' => getObjectValue($post, 'sSpiritual')
                      ,'SpiritualMentor' => getObjectValue($post, 'mSpiritual')
                      ,'HumanStudent' => getObjectValue($post, 'sHuman')
                      ,'HumanMentor' => getObjectValue($post, 'mHuman')
                      ,'IntellectualStudent' => getObjectValue($post, 'sIntellectual')
                      ,'IntellectualMentor' => getObjectValue($post, 'mIntellectual')
                      ,'ApostolicStudent' => getObjectValue($post, 'sApostolic')
                      ,'ApostolicMentor' => getObjectValue($post, 'mApostolic')
                      ,'OtherStudent' => getObjectValue($post, 'sOther')
                      ,'OtherMentor' => getObjectValue($post, 'mOther')
                      ,'MentorID' => getUserID()
                          ,'DateCreated' => systemDate()
                          ,'UserID' => getUserID()
                          ,'DateModified' => systemDate()
                          ,'Modifiedby'	=> getUserID()         
                    );
                    
                    $this->plan->updateOrCreate($cond,$data);
                    
                    $response = ['error' => false, 'message' => 'record successfully saved!'];

                    break;
                    
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }



    

    public function print_report()
    {

        $idno  = decode(Request::get('idno'));
        $this->xpdf->filename='SHS\crediting_form.rpt';

        $this->xpdf->query="EXEC sp_k12_credit_form '{$idno}',0";
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="Credit Form"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
	}

    private function initializer()
    {
        $this->model = new mStudents();
        $this->permission = new Permission($this->ModuleName);
        $this->mentorsession     = new Mentoring_model;
        $this->plan = new mFormplan;
        $this->xpdf = new xpdf;
    }
}
