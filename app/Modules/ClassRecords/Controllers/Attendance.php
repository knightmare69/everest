<?php namespace App\Modules\ClassRecords\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\Attendance\Attendance as Model;
use Permission;
use Request;
use DB;
use Mail;

class Attendance extends Controller{
	private $media = [
			'Title'=> 'Discipline/Offenses',
			'Description'=> '',
			'js'		=> ['ClassRecords/attendance'],
			'init'		=> ['Me.init()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
				            'jquery-validation/js/jquery.validate.min',
							'bootstrap-datepicker/js/bootstrap-datepicker',
							'bootstrap-timepicker/js/bootstrap-timepicker.min',
							'clockface/js/clockface',
							'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
				           ],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all',
				'clockface/css/clockface',
				'bootstrap-datepicker/css/datepicker3',
				'bootstrap-timepicker/css/bootstrap-timepicker.min',
				'bootstrap-colorpicker/css/colorpicker',
				'bootstrap-daterangepicker/daterangepicker-bs3',
				'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
			]
		];

	private $url = [ 'page' => 'class-record/attendance/' ];

	private $views = 'ClassRecords.Views.attendance.';

	public function __construct()
	{
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {
            $faculty_id = getUserFacultyID();

            $data = array(
                'adviser_section' => $this->model->get_advisersection($faculty_id),
            );

            return view('layout',array('content'=>view($this->views.'index',$data)->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}
    
	
 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'showFilter':
					if ($this->permission->has('search')) {
					    if(!isfaculty())
						$response = ['error' => false,'content'=>view($this->views.'forms.filter',['section'=>$this->getSections()])->render()];
						else
						$response = ['error' => false,'content'=>view($this->views.'forms.filter')->render()];
					}
				break;
				case 'showAttendanceModal':
					if ($this->permission->has('add')) {
						$response = ['error' => false,'content'=>view($this->views.'modals.attendance',['views'=>$this->views])->render()];
					}
				break;
				case 'loadData':
					if ($this->permission->has('search')) {
						$response = ['error' => false,'content'=>view($this->views.'tables.masterlist')->render()];
					}
				break;
				case 'loadSections':
					if ($this->permission->has('search')) {
						$response = ['error' => false,'data'=>$this->getSections()];
					}
				break;
				case 'showProfileData':
						$response = ['error' => false,'data'=>$this->getProfileData()];
				break;
				case 'delete':
					if ($this->permission->has('delete')) {
						if (Request::get('AttendanceKey')) {
							$response = $this->model->where('fldPK',Request::get('AttendanceKey'))->delete() ? successDelete() : errorDelete();
						}
					}
				break;
				case 'save':
					if ($this->permission->has('add')) {
					    $data = $this->getPost();
						if (Request::get('isEdit') == 'false') {
						    $data['CreatedBy']  = getUserID();
						    $data['CreatedDate']= date('Y-m-d H:i:s');
							$status = $this->model->insert($data);
							$response = $status ? successSave() : errorSave();
							if($status){
							 $this->notify();
							}
						} else {
						    $data['UpdatedBy']  = getUserID();
						    $data['UpdatedDate']= date('Y-m-d H:i:s');
							$status = $this->model->where('fldPK',Request::get('AttendanceKey'))->update($data);
							$response = $status ? successUpdate() : errorUpdate();
							if($status){
							 $this->notify();
							}
						}
					}
				break;
			}
		}
		return $response;
	}
	
	public function export(){
	 $data = Request::all();
	 $xtype =$data['type'];
	 $xsect =$data['section'];
	 $xstdno=$data['stdno'];
	 $output='';
	 
	 $tr   = DB::select("SELECT d.Date,d.Particulars,d.CreatedBy,u.FullName,d.Remarks 
	                       FROM ES_GS_MeritDemerits as d 
					  LEFT JOIN ESv2_Users as u ON d.CreatedBy=u.UserIDX
						  WHERE Type='".$xtype."' AND StudentNo='".$xstdno."' ORDER BY fldPK DESC");
	 
	 $i=1;
	 $header="Date \t Infractions \t ReportedBy \t Sanction";
	 foreach($tr as $r){
	    $line  = $i."\t";
		$line .= date('Y-m-d',strtotime($r->Date))."\t"; 
		$line .= $r->Particulars."\t"; 
		$line .= $r->FullName."\t"; 
		$line .= $r->Remarks."\t"; 
		
		$output.=trim($line)."\n";
	    $i++;
	 }
	 
     $data = str_replace( "\r" , "" , $output);
	 header("Content-type: application/octet-stream");
	 header("Content-Disposition: attachment; filename=export.xls");
	 header("Pragma: no-cache");
	 header("Expires: 0");
	 print "$header\n$output";
	}
	
	private function notify(){
		ini_set('max_execution_time', 14400);
		try{
		$post = [
			'studno'    => decode(Request::get('StudentNo')),
			'studname'  => 'STUDENT_NAME',
			'email'     => array('jhe69samson@gmail.com',),
			'link'      => url(),
			'hasremark' => ((trim(Request::get('Remarks'))=='' || Request::get('Remarks')==false)?false:true),
		];
		
		$studinfo = DB::select("SELECT TOP 1 * FROM ES_Students WHERE StudentNo='".decode(Request::get('StudentNo'))."'");
		$studinfo = DB::select("SELECT TOP 1 * FROM ES_Students WHERE StudentNo='".decode(Request::get('StudentNo'))."'");
		if($studinfo && count($studinfo)>0){
		 $post['studname'] = $studinfo[0]->LastName.", ".$studinfo[0]->FirstName;
		}
		
		if($post['hasremark']=='true'){
		$target = DB::select("SELECT TOP 1 UserIDX,
		                             FullName,
									 ISNULL((SELECT TOP 1 Email from ESv2_Users u
									         INNER JOIN ES_GS_MeritDemerits as d ON u.UserIDX=d.CreatedBy
											 WHERE fldPK='".Request::get('AttendanceKey')."'),Email) as Email 
								FROM ESv2_Users as u WHERE UserIDX='".getUserID()."'");
		}else{
		$target = DB::select("SELECT TOP 1 UserIDX,
		                             FullName,
									 ISNULL((SELECT TOP 1 Email from ESv2_Users WHERE DepartmentID=u.DepartmentID AND PositionID=4),Email) as Email 
								FROM ESv2_Users as u WHERE UserIDX='".getUserID()."'");
		}
		
		if($target && count($target)>0){
	    //$post['email'] = trim($target[0]->Email);
		}
		
		Mail::send('email.offense', ['post'=>$post], function ($message) use ($post) {
			$message->from(env('SYS_EMAIL'), 'K-12 Notification');
        	$message->to(getObjectValue($post,'email'));
			$message->subject('K to 12 Application');
            $message->bcc(env('SYS_EMAIL'));
		});
		}catch (Exception $e){
		return 0;
		}
		return 1;
	}

	private function getSections()
	{	
		$whereRaw = "";
		$c = decode(Request::get('ProgCode'));
		$faculty_id = getUserFacultyID();
		$term= decode(Request::get('SchoolYear'));
		$campus = decode(Request::get('SchoolCampus'));
		$yearlevel = decode(Request::get('YearLevel'));
		//$prog_id_group = implode(getUserProgramAccess(),',');

		switch ($c) {
			case 5:
				$prog_id = 5;
				break;

			case 11:
				$prog_id = 1;
				break;

			case 12:
				$prog_id = 8;
				break;

			case 20:
				$prog_id = 7;
				break;

			default:
				$prog_id = 0;
				break;
		}

			$prog_id_group=(empty(getUserProgramAccess())?$prog_id:implode(',',getUserProgramAccess()));

			//$whereRaw = "TermID = {$term} AND CampusID = {$campus}";
			$whereRaw = "TermID = 1001 AND CampusID = 1";
            

            if(isfaculty()) {
                $whereRaw .= " AND AdviserID = '{$faculty_id}' ";
            } else {
            	$whereRaw .= " AND YearLevelID =".(($yearlevel!='')?$yearlevel:0);
                $whereRaw .= " AND ProgramID = {$prog_id} ";
            }

		return 
		DB::table('ES_ClassSections as s')
			->select([
					'SectionID as SecID',
					'SectionName as Section',
					'YearLevelID as YearLevelID'
				])
			->whereRaw($whereRaw)
			// ->where('CampusID',decode(Request::get('SchoolCampus')))
			// ->where('ProgramID', $prog_id)
		 //  //->where(DB::raw("dbo.fn_ProgramClassCode(ProgramID)"),decode(Request::get('ProgCode')))
			// ->where('YearLevelID',decode(Request::get('YearLevel')))
			// ->where('adviserid',$faculty_id)
			->get();	
	}

	private function getProfileData() 
	{
		$StudentNo = decode(Request::get('StudentNo'));
		$data['Profile'] = 
			DB::table('ES_Students')
				->select([
						DB::raw("LastName+', '+FirstName+' '+MiddleInitial as Name"),
						'StudentNo','Gender',
						DB::raw("CONVERT(DATE,DateofBirth) as DateofBirth"),
					])
				->where('StudentNo',$StudentNo)
				->first();

		$data['Absenses'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','IsExcused','Remarks',
						DB::raw("DATEDIFF(day,Date,DateEnd) as Days"),
						DB::raw("convert(varchar(11),Date) as Date"),
						DB::raw("convert(varchar(11),DateEnd) as DateEnd"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','1')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();
				
		$data['Tardiness'] =
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','2')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		$data['Merits'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','3')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		$data['LostFound'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','4')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		$data['Offenses'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','IsExcused','Days','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
						DB::raw("convert(varchar(11),DateEnd) as DateEnd"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','5')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		return $data;
	}

	private function getPost()
	{
		$post = Request::all();

		if(!empty($post['DateEnd'])){
			$sd = date_create($post['Date']);
			$ed = date_create($post['DateEnd']);
			$res = date_diff($sd, $ed);

			if($res->invert){
				return false;
			} else if($res->d == 0) {
				$post['Days'] = 1;
			} else {
				$post['Days'] = $res->d + 1;
			}

		} else {
			$post['Days'] = 1;
		}
		
		$data = [
			'TermID' => getLatestActiveTerm(),
			'PeriodID' => decode(Request::get('Period')),
			'StudentNo' => decode(Request::get('StudentNo')),
			'Type' => Request::get('type'),
			'Date' => getObjectValueWithReturn($post,'Date','').' '.getObjectValueWithReturn($post,'DateTime',''),
			'Particulars' =>  getObjectValueWithReturn($post,'Particulars',''),
			'IsExcused' => getObjectValueWithReturn($post,'Excused','0'),
			'Merit' => getObjectValueWithReturn($post,'Merit','0'),
			'Demerit' => getObjectValueWithReturn($post,'Demerit','0'),
			'Remarks' => getObjectValueWithReturn($post,'Remarks',''),
			'DateEnd' => getObjectValueWithReturn($post,'DateEnd',''),
			'Days' => getObjectValueWithReturn($post,'Days','0'),
			'SectionID' => getObjectValueWithReturn($post,'SectionID','')
		];		
		return $data;
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('attendance');
 		$this->model = new Model;
 	}
}
/*

select * from modules

insert into modules(name,url,slug,sort)
values('Registrar','#','registrar','2')

select * from pages

registrar subjects
insert into pages(name,slug,url,module_id,sort)
values('Subjects','registrar-subject','/registrar/subject',1004,1)

registrar attendance
insert into pages(name,slug,url,module_id,sort)
values('Attendance','registrar-attendance','/registrar/attendance',1004,1)

*/