<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\Attendance\Attendance As dbcon;
use App\Modules\ClassRecords\Models\ClassSection_model As sched_model;
use App\Modules\ClassRecords\Models\Attendance\Quarter As period_model;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use App\Modules\Setup\Models\AcademicYearTerm As term_model;
use Permission;
use Request;

class HomeRoom extends Controller{
    protected $ModuleName = 'student-attendance';

	private $media = [ 
        'Title' => 'Student Attendance',
        'Description'   => 'Welcome To Student Attendance',
        'js'		=> ['ClassRecords/student-attendance'],
        'init'		=> ['ME.init()'],
        'css'         => ['profile'],
        'plugin_js'	=> ['bootbox/bootbox.min', 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min', 'bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification.min'],
        'plugin_css' => ['jstree/dist/themes/default/style.min', 'bootstrap-select/bootstrap-select.min', 'select2/select2', 'jquery-multi-select/css/multi-select','SmartNotification/SmartNotification'],
    ];

	private $url = [ 'page' => 'class-record/student-attendance/' ];

	private $views = 'ClassRecords.Views.homeroom.';

	function __construct() {
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		
          $this->initializer();
        /* TEMP: disabled access rights
        if (!$this->permission->has('read')) {
            return view(config('app.403'));
        }
        */    
        $term = 0;
        $faculty_id = getUserFacultyID();                        
        $ayt = $this->sched->classadviser($faculty_id)->get();
        
        if(count($ayt)>0){
            foreach($ayt as $ay){
            	$term = $ayt[0]->TermID;	
            }
        }else{
		    $ayt = $this->sched->subjectteacher($faculty_id);
            foreach($ayt as $ay){
            	$term = $ayt[0]->TermID;	
            }
        }
        
        $data = array(
            'academic_year' => $ayt                            
            ,'term' => $term 
            ,'period' => $this->getPeriod()
            ,'sections' => $this->getSections($term, $faculty_id)
        );

        return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        
 	}
    
    private function getPeriod()
    {
        $periods = $this->periods->where('GroupClass','1');
        return view($this->views.'sub.period',['table' => $periods->get()])->render();
    }

    private function getSections($term, $fac)
    {
        $arr = $this->sched->view($term,$fac)->get();    
		if($arr && count($arr)>0){
		
		}else{
		 $arr = $this->sched->subjectview($term,$fac);    
		}
		
        return view($this->views.'sub.sections',['table' => $arr])->render();
        
    }
    
    public function event_service(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
                case 'schedules':                
                                    
                    $term = decode(Request::get('term'));
                    $facid = getUserFacultyID();
                    $period = Request::get('period');                  
                    
                    $arr = $this->dbcon->getClassSections($term,$facid);    
                    $data = view($this->views.'sub.sections',['table' => $arr])->render();
                                                                                                                   
                    $periods = $this->getPeriod();
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data, 'period'=> $periods ];
                 
                 break;
                 
                 case 'students':
                    
                    $term = decode(Request::get('term'));
                    $sec = decode(Request::get('section'));
                    $per = decode(Request::get('period'));
                    $month = Request::get('month');
                    $faculty_id = getUserFacultyID();
					
					$mterm = $this->terms->where('TermID', $term)->first();
					
					$sel_year = explode('-', $mterm->AcademicYear);
					
					if($sel_year){
						if( floatval($month)>5 ){
							$sel_year = $sel_year[0];	
						}else{
							$sel_year = $sel_year[1];	
						}
						
					}
					
                    $students = $this->dbcon->getStudents($term,$sec );
                    
                    $data = view($this->views.'students',['conduct_id' => $this->setup->ConductID(),'data'=>$students,'mnth' => $month,'pyear' => $sel_year ])->render();
                    
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data,'sections'=>$this->getSections($term, $faculty_id)];
                    
                 break;
                 
                 case 'save-att': 
                    
                    $post = Request::all();
                    
                    $type = 1; 
                    $excused = 1;
                    
                    $tag = strtoupper(getObjectValue($post, 'score'));
                    
                    switch($tag)
                    {
                        case 'AE': $type = 1; $excused = 1; break;
                        case 'AU': $type = 1; $excused = 0; break;
                        case 'TE': $type = 2; $excused = 1; break;
                        case 'TU': $type = 2; $excused = 0; break;                        
                    }
                                                                          
					$data = array(          
                        'TermID' => decode(getObjectValue($post, 'term')),                              
                        'StudentNo' => (getObjectValue($post, 'idno')),             	        
                        'SectionID' => decode(getObjectValue($post, 'section')),
                        'PeriodID' => (getObjectValue($post, 'period')),
                        'Date' => (getObjectValue($post, 'date')),
                        'Type' => $type,
                        'IsExcused' => $excused,
                        'Remarks' => getObjectValue($post, 'explain')
                    );                                                                                
                                                                        
                        $whre = [                             
                            'StudentNo' => (getObjectValue($post,'idno')),                            
                            'Date' => getObjectValue($post, 'date'),                             
                            'Type' => $type,
                        ];       
                                                                                 
                        $rs = $this->dbcon->where($whre)->first();
                        
                        if($rs){
                            $this->dbcon->where($whre)->update($data);
                        }else{
                           // $data['Createdby'] = getUserID();
                           // $data['DateCreated'] = date('Y-m-d H:i:s');
                            $this->dbcon->create($data);
                        }
                        
						$response = ['error'=>false,'message'=>'Successfully Save!'];
                        SystemLog('student-attendance','','student-attendance','student-attendance', json_encode($data) ,'record successfully saved!' );
                        					                    
                break;
                
                case 'erase-att':
                	
                    if ($this->permission->has('delete')) 
                    {
                	   
                        $post = Request::all();
                        
                        $date = decode(Request::get('date'));
                        $idno = decode(Request::get('idno'));
                        $tag = strtoupper(Request::get('type'));
                        
                        $type = 1;
                        $excused = 0;
                        
                        switch($tag)
                        {
                            case 'AE': $type = 1; $excused = 1; break;
                            case 'AU': $type = 1; $excused = 0; break;
                            case 'TE': $type = 2; $excused = 1; break;
                            case 'TU': $type = 2; $excused = 0; break;                        
                        }
                        
                        $whre = [                             
                            'StudentNo' => (getObjectValue($post,'idno')),                            
                            'Date' => getObjectValue($post, 'date'),                             
                            'Type' => $type,
                        ]; 
                                                
                        $this->dbcon->where($whre)->delete();
                        						
						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog('student-attendance','','student-attendance','student-attendance', ' StudentNo='. $idno.' Date:'. $date. ' Type:'.$type ,'record successfully erased!' );						
					}
                     
                break;
                
                case 'post':
                    
                                        
                    $post = Request::all();                                         
                                        
                    foreach($post['stud'] As $s){                                                
                        
                        $whre = array(
                            'TermID' => decode(getObjectValue($post, 'term')),
                            'StudentNo' => ($s['idno']),                                                             
                            'PeriodID' => decode(getObjectValue($post, 'period')),                                    	                                    
                        );
                        
                        if(getObjectValue($post, 'type') == 0){
                            $whre['SectionID'] = decode(getObjectValue($post, 'section'));
                        }else{
                            $whre['ScheduleID'] = decode(getObjectValue($post, 'section'));
                        }   
                                                                                                                                            
                        $data = array(
                            'DatePosted' =>  date('Y-m-d H:i:s'),
                            'Postedby' => getUserID(),
                        );
                                                                                             
                        $this->dbcon->where($whre)->update($data);                                                                        
                    }                    
                    
                    $response = ['error'=> false,'message'=> 'Successfully posted!' ];
                    SystemLog('Student-Conduct','','post-conduct','post-conduct', json_encode($whre) ,' posted conduct grade.' );
                    
                break;
                
                 
                 default : break;
			}
		}
		return $response;
    }



 	private function initializer() {
 		$this->permission = new Permission('attendance');
        $this->dbcon = new dbcon;        
        $this->periods = new period_model;
        $this->sched = new sched_model();
        $this->setup= new gs_setup_model();
		$this->terms = new term_model();
 	}
    
    public function generate_layout(){
        $p  = Request::all();
            $this->initializer();
    		
            $headers = array(
                'Content-Type' => 'text/csv',
            );
                        
            ob_end_clean();
            ob_start();
            
            $filename = "student_conduct.csv";
            $file = fopen($filename, 'w+');
        
            $total=0;
            
            $_id = Request::get('sched');                                                                                     
            $period = Request::get('period');
            
            $facid = getUserFacultyID();
            $sched = $this->sched_model->where('ScheduleID',$_id)->first();
            
            $gender = '';

            if($sched->FacultybyGender){
                if( $facid == $sched->FacultyID ){
                    $gender = 'M';
                } else if( $facid == $sched->FacultyID_2 ){
                    $gender = 'F';
                }else{
                    $gender = '';
                }
            }
            
            $data = $this->dbcon->get_students($_id,$gender,$shs,1);                                            
            fputcsv($file,array('A'=>'No','B'=>'IDNo','C'=>'Student Name','D'=>'RawScore'));
            
            $i=1;        	        	
        	
            foreach ($data as $r){
        		fputcsv($file, array(
                    'A'=> $i.'.', 
                    'B' => $r->StudentNo,
                    'C'=> str_replace(",","",$r->StudentName), 
                    'D'=>'',  
                    ));
        		//$total=$total+$r->Amount;
        		$i++;
            }
            
            fclose($file);
            
           return Response::download($filename, 'event_layout.csv', $headers);    
        
    }
    
    
}