<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\Checklist_model As dbcon;
use App\Modules\ClassRecords\Models\ClassSection_model As section_mod;
use Permission;
use Request;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\Mmpdf;

class Checklist extends Controller{
    
    protected $ModuleName = 'PGS Checklist';
    
	private $media = [ 'Title'=> 'Checklist',
    	               'Description'=> 'Welcome To Prep School Checklist!',
    	               'js'		=> ['ClassRecords/checklist'],
    	               'init'		=> ['ME.init()'],
    	               'plugin_js'	=> ['bootbox/bootbox.min','SmartNotification/SmartNotification.min',
                                        'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
                                        'bootstrap-select/bootstrap-select.min',],
                       'plugin_css' => ['jstree/dist/themes/default/style.min',
                                        'bootstrap-select/bootstrap-select.min',                                        
                                        'select2/select2', 'SmartNotification/SmartNotification',
                                        'jquery-multi-select/css/multi-select'],  
                     ];

	private $url = [ 'page' => 'class-record/checklist/' ];

	private $views = 'ClassRecords.Views.Checklist.';

	function __construct()
	{
		$this->initializer();
	}
    
 	public function index()
 	{
  		
        $this->initializer();
        
        /*
        if (!$this->permission->has('read')) {
            return view(config('app.403'));
            die();
        }    
        */
        
        $sectionid = Request::get('open');
        $period = Request::get('period');
        
        if($sectionid != ''){
            
            return $this->openSection($sectionid,$period);
            
        }else{
        
            $faculty_id = getUserFacultyID();
                                                    
            $data = array(
                'academic_year' => $this->dbcon->get_academicyearterm($faculty_id),
                'facultyId'=> $faculty_id,
            );
                
            return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
            
        }                                        
 	}
    
    public function openSection($sec_id, $period ){
        $id = decode($sec_id);
         
        $gtbl = getSetting(8);
        $data = ['sec_id'=> $id, 
                 'period' => $period, 
                 'gradetbl' => $gtbl , 
                 'section' => $this->section_mod->SectionName($id),
                 'yearlevel' => $this->section_mod->YearLevelID($id)
        ];
        
        return view('layout',array('content'=>view($this->views.'open.index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
    }
    
    public function event_service(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{

			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
                case 'schedules':
                    $term = decode(Request::get('term'));
                    $period = (Request::get('period'));
                    $facid = getUserFacultyID();   
                          
                    /*
                    if( getUserName() === 'admin' ) {
                        $facid = '';   
                    }else{
                        $facid = getUserFacultyID();
                        $facid = ($facid == '' ? '0': $facid ); 
                    }    
                */
                    // $data = view($this->views.'table',['period'=>$period,'term'=>$term ,'table'=>$this->section_mod->View($term,$facid)->get()])->render();     
                    $data = view($this->views.'table',['period'=>$period,'term'=>$term ,'table'=>$this->dbcon->loadschedules($term,$facid)->get()])->render();                                                       
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                 break;
                 
                 case 'save-grade':
                    //if ($this->permission->has('add')) {
                        
                        $post = Request::all();
                        
                        $where = [
                            'SectionID' => decode(getObjectValue($post, 'sec'))
                            ,'PeriodID' => getObjectValue($post, 'period')
                            ,'StudentNo' => decode(getObjectValue($post, 'idno'))
                            ,'ChecklistID' => decode(getObjectValue($post, 'chkid'))
                        ];
                        
						$data = [
                          'InputDate' => systemDate()
                          ,'SectionID' => decode(getObjectValue($post, 'sec'))
                          ,'PeriodID' => getObjectValue($post, 'period')
                          ,'StudentNo' => decode(getObjectValue($post, 'idno'))
                          ,'ChecklistID' => decode(getObjectValue($post, 'chkid'))
                          ,'Grade' => getObjectValue($post, 'score')
                          ,'TeacherID' => getUserID()                          
                        ];
                        
                        $rs  = $this->dbcon->where($where)->count();
                        
                        if($rs>0){
                        
                          unset($data['InputDate']);
                          $data['Modifiedby'] = getUserID();
                          $data['DateModified'] = systemDate();
                            
                          $this->dbcon->where($where)->update($data);                        
                            
                        } else{
                            $this->dbcon->insert($data);    
                        }                        
                                                                                                                            
						$response = ['error'=>false,'message'=>'Successfully Save!'];
                        SystemLog('Checklist','','save-grade','Save Grade', json_encode($data) ,'record successfully saved!' );

						
					//}
                 break;
                 
                 case 'erase-grade':
					//if ($this->permission->has('delete')) {
                        /*
                        $validation = $this->service->isValid(Request::all(),'erase-grade');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
                        */
                            
                            $chklst = decode(Request::get('chkid'));
                            $idno = decode(Request::get('idno'));
                            $sec = decode(Request::get('sec'));

    						$this->dbcon
                                ->where('SectionID',$sec)
                                ->where('CheckListID',$chklst)
                                ->where('StudentNo',$idno)
                                ->delete();
    						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                            SystemLog('eClass-Records','','erase-grade','Erase Score', 'ChecklistID='. $chklst.' StudentNo='. $idno . ' StudentName='.Request::get('name') ,'record successfully erased!' );
						//}
					//}
					break;

			}
		}
		return $response;
    }
    
     public function print_data()
    {
        $this->exportPDF();        
        die(); 
        
        $this->xpdf = new xpdf;                
        $sched = decode(Request::get('sched'));
        $period = Request::get('period');
        $term =  Request::get('term');

        $subrep_logo = array(
            'file' => 'Company_Logo',
            'query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'"
        );
        
        $this->xpdf->filename = 'checklist.rpt';
        $this->xpdf->query = "EXEC sp_checklist_report '".$term."','".$sched."','".$period."'";
        
        $subrpt = array(
            'subreport1' => $subrep_logo,
        );

        $this->xpdf->SubReport = $subrpt;

        $data = $this->xpdf->generate();

	    header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="checklist.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
        die();                    
    }    
    
    public function exportPDF()
	{	
	    $this->pdf = new Mmpdf;       
		set_time_limit(0);
        ini_set('memory_limit', '-1');

		$sched = decode(Request::get('sched'));
        $period = Request::get('period');
        $term =  Request::get('term');

        $data = array(
            'sec_id' => decode(Request::get('sched')),
            'period' => $period,
            'term' => $term,
            'yearlevel'=> $this->section_mod->YearLevelID($sched),
        );
        
        $this->pdf->setPrintHeader(false);
		$this->pdf->setPrintFooter(false);
		$this->font = 11;
		
        //$this->customFont = 'Libraries/pdf/fonts/Calibri.ttf';
        //$this->pdf->SetFont($this->pdf->addTTFfont(app_path($this->customFont)),'',$this->font);        
                
        $this->pdf->SetMargins(50, 50, 50, true);
        $this->pdf->setPageUnit('pt');

        $this->pdf->setTitle('Checklist Report');
		$this->pdf->AddPage('L','LEGAL');
        $this->pdf->writeHTML(view($this->views.'report.checklist',$data)->render());
                               
        $this->pdf->output();
        //echo view($this->views.'report.checklist')->render();
        
		set_time_limit(30);
        ini_set('memory_limit', '128M');
	}
               

 	private function initializer()
 	{
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;
        $this->section_mod = new section_mod;
 	}
}