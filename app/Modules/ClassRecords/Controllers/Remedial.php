<?php namespace App\Modules\ClassRecords\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As dbcon;
use App\Modules\ClassRecords\Models\Attendance\Quarter As period_model;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use App\Modules\ClassRecords\Services\StudentRemedialServices;
use App\Libraries\CrystalReport as xpdf;
use Permission;
use Request;

class Remedial extends Controller{
    protected $ModuleName = 'remedial';

	private $media = [
        'Title' => 'Student Remedial',
        'Description'   => 'Welcome To Student Remedial!',
        'js'		 => ['ClassRecords/remedial'],
        'init'		 => ['ME.init()'],
        'css'        => ['profile'],
        'plugin_js'	 => ['bootbox/bootbox.min', 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min', 'bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification.min'],
        'plugin_css' => ['jstree/dist/themes/default/style.min', 'bootstrap-select/bootstrap-select.min', 'select2/select2', 'jquery-multi-select/css/multi-select','SmartNotification/SmartNotification'],
    ];

	private $url = [ 'page' => 'class-record/remedial/' ];

	private $views = 'ClassRecords.Views.Remedial.';

    private function initializer() {
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;
        $this->periods = new period_model;
        $this->setup= new gs_setup_model();
        $this->service = new StudentRemedialServices();
 	}

	function __construct() {
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {

            $faculty_id = getUserFacultyID();

            $data = array(
                'academic_year' => $this->service->academic_year($faculty_id),
                'club_id' => $this->setup->ClubID(),
                'term' => '',
                'period' => '',
                'club' => ''
            );

            return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

    public function event(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
                case 'schedules':

                    $term = decode(Request::get('term'));
                    $period = Request::get('period');

                    // TEMP :  SeniorHighschool Only
                    $whr = array(
                        'TermID'=> $term ,
                        'ProgramID' => '29' ,
                    );

                    $arr = $this->dbcon->select(['SubjectID','SubjectCode'])
                        ->where($whr)
                        ->whereRaw(" (CAST( AverageA AS FLOAT)  +  CAST( AverageB AS FLOAT)) / 2 <75")
                        ->whereRaw(" DatePosted1 is not null and DatePosted2 is not null ")
                        ->groupBy(['SubjectID','SubjectCode'])
                        ->get();

                    $data = view($this->views.'sub.subjects',['table' => $arr ,])->render();
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data];

                 break;

                 case 'students':

                    $term = decode(Request::get('term'));
                    $sched = decode(Request::get('sched'));
                    $per = decode(Request::get('period'));

                    $whr = array(
                        'TermID'=> $term ,
                        'ProgramID' => '29' ,
                        'SubjectID' => $sched,
                    );


                    $students = $this->dbcon->selectRaw("GradeIDX, RegID, StudentNo, dbo.fn_StudentName(StudentNo) As FullName, ScheduleID, dbo.fn_SectionNameBySchedID(ScheduleID) AS SectionName,
                        dbo.fn_StudentGender(StudentNo) As Gender, YearLevelID, dbo.fn_YearLevel_k12(YearLevelID, ProgClassID) As YearLevel,
                        AverageA, AverageB, TotalAverageAB, AverageC, Final_Average, Final_Remarks, dbo.fn_gs_gradescriptor('1',Final_Average) AS LG, DatePosted3, PostedBy3 ")
                        ->where($whr)
                        ->whereRaw(" ((CAST( AverageA AS FLOAT)  +  CAST( AverageB AS FLOAT)) / 2) <75")
                        ->whereRaw(" DatePosted1 is not null and DatePosted2 is not null ")
                        ->orderBy('FullName','asc')
                        ->get();

                    $data = view($this->views.'students',['term'=> $term, 'data'=>$students])->render();
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];

                 break;

                 case 'save-remedial':

                    $post = Request::all();
                    
                    $idno = getObjectValue($post,'idno');
                    $code = getObjectValue($post,'code');
                    $term = decode(getObjectValue($post, 'term'));
                    $final = getObjectValue($post,'finl');
                    $remedial = getObjectValue($post, 'score');
                    
					$data = array(
                        'AverageC' => $remedial,
                        'TotalAverageAB' => $final,
                        'Final_Average' => (floatval($remedial) +  floatval($final))/2,
                        'Final_Remarks' => getObjectValue($post,'rmk'),
                    );
                                                            
                    $whre = [
                        'GradeIDX' => decode(getObjectValue($post, 'idx')),
                        'StudentNo' => $idno,
                        'TermID' => $term,
                    ];

                    $rs = $this->dbcon->where($whre)->update($data);

					$response = ['error'=>false,'message'=>'Successfully Save!'];
                    SystemLog($this->media['Title'],'','save-remedial','remedial grade', 'StudentNo='.$idno.' Term='.$term.' SubjectCode='.$code  ,'record successfully saved!' );

                break;

                case 'erase-grade':
                	if ($this->permission->has('delete')) {

                        $model = new mRawscore();
                        //$id = decode(Request::get('index'));
                        $idno = decode(Request::get('idno'));
                        $event = decode(Request::get('activity'));

                        $model->where('EventID',$event)
                                ->where('StudentNo',$idno)
                                ->delete();
						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog($this->media['Title'],'','erase-grade','Erase Club Score', 'Event ='. $event.' StudentNo='. $idno . ' Name='. Request::get('name') ,'record successfully erased!' );
					}

                break;

                case 'post':

                    $res = $this->service->post_grade(Request::all());
                    $response = ['error'=> false,'message'=> 'Successfully posted!' ];

                break;


                 default : break;
			}
		}
		return $response;
    }

    public function print_data()
    {

        $this->xpdf = new xpdf;
        $term = decode(Request::get('term'));
        $_id = Request::get('club');
        $period = Request::get('period');

        $subrep_logo = array(
            'file' => 'Company_Logo',
            'query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', '".env('APP_REPUBLIC','')."', '".env('APP_ADDRESS','')."', '-1'"
        );

        $this->xpdf->filename = 'class-record-clubs.rpt';

        $this->xpdf->query = "EXEC sp_classrecord '".($term)."','0','".decode($_id)."','".decode($period)."','0'";

        $subrpt = array('subreport1' => $subrep_logo,);
        $this->xpdf->SubReport = $subrpt;
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="class-record.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
        die();
    }

    public function generate_layout(){
        $p  = Request::all();
            $this->initializer();

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            ob_end_clean();
            ob_start();

            $filename = "student_clubs.csv";
            $file = fopen($filename, 'w+');

            $total=0;

            $_id = Request::get('sched');
            $period = Request::get('period');
            $facid = getUserFacultyID();
            $gender = '';

            $data = $this->dbcon->get_students($_id,$gender,$shs,1);
            fputcsv($file,array('A'=>'No','B'=>'IDNo','C'=>'Student Name','D'=>'RawScore'));

            $i=1;

            foreach ($data as $r){
        		fputcsv($file, array(
                    'A'=> $i.'.',
                    'B' => $r->StudentNo,
                    'C'=> str_replace(",","",$r->StudentName),
                    'D'=>'',
                    ));
        		//$total=$total+$r->Amount;
        		$i++;
            }

            fclose($file);

           return Response::download($filename, 'event_layout.csv', $headers);

    }


}