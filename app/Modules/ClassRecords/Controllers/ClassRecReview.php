<?php

namespace App\Modules\ClassRecords\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ClassRecords\Services\ClassRecReviewServiceProvider as Services;
use App\Modules\ClassRecords\Models\ClassRecReview_model as Model;
use App\Modules\ClassRecords\Models\eClassRecord_model as ClassRecModel;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;

use Permission;
use Illuminate\Http\Request;
use Response;

class ClassRecReview extends Controller
{
    protected $ModuleName = 'review-records';

    private $media = [
        'Title' => 'Class Record Review',
        'Description' => 'Welcome To Class Record Review',
        'js' => ['ClassRecords/rec-review.js?v1.2', 'ClassRecords/classrecords.js?v1', 'ClassRecords/import'],
        'css' => ['profile', 'classrecord'],
        // 'init' => [],
        'plugin_js' => [
            'bootbox/bootbox.min',
            'datatables/media/js/jquery.dataTables.min',
            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
            'datatables/extensions/Scroller/js/dataTables.scroller.min',
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2.min',
            'SmartNotification/SmartNotification.min'
        ],
        'plugin_css' => [
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2',
            'SmartNotification/SmartNotification'
        ],
    ];

    private $url = ['page' => 'class-record/review/'];

    private $views = 'ClassRecords.Views.Review.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $setup_templ_gs = new gs_setup_model();

            $incl = ['ac' => $this->services->academic_year(), 'campus' => $this->services->campus()];

            SystemLog('Class-Record-Review', '', 'Page View', 'page-view', '', '');

            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'incl' => $incl, 'tblid' => $setup_templ_gs->GradingSystemID(), 'transid' => $setup_templ_gs->TransmutationID()]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event(Request $req)
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if ($req->ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch ($req->get('event')) {
                case 'load-faculties':
                case 'find-faculties':
                    if($this->permission->has('search')){
                        $empl_ids = empty($req->get('e')) ? [] : $req->get('e');

                        $get = $this->model->getFaculty(decode($req->get('t')), $req->get('f'), $empl_ids);

                        // error_print($get);
                        // die();

                        $list = view($this->views.'faculty.list', ['empl' => $get])->render();

                        $response = ['error' => false, 'list' => $list, 'total' => count($get)];
                    }
                    break;

                case 'faculty-photo':
                    if($this->permission->has('read')){
                        $empl_id = decode($req->get('e'));

                        $get = $this->model->getFacultyPhoto($empl_id);

                        if(!empty($get->Photo)){
                            $response = 'data:image/jpeg;base64,'.base64_encode(hex2bin($get->Photo));

                        } else {
                            $response = asset('assets/system/media/images/avatar.png');
                        }
                    }
                    break;

                case 'get-faculty-sections-subjects':
                    if($this->permission->has('search')){
                        $empl_id = decode($req->get('e'));
                        $term_id = decode($req->get('t'));

                        $get = $this->model->getFacultySectionWithSubjects($term_id, $empl_id);

                        if(!empty($get)){
                            $list = view($this->views.'faculty.list-sections-w-subject', ['data' => $get])->render();

                            $response = ['error' => false, 'list' => $list];
                        } else {
                            $response = ['error' => true, 'message' => 'No subject(s) found.'];
                        }
                    }
                    break;

                case 'get-class-record':
                    if($this->permission->has('search')){
                        $period = $req->get('period');
                        $term = decode($req->get('term'));
                        $faculty = decode($req->get('e'));
                        $sched = $req->get('sched_id');

                        $period_list = [1 => '1st', 2 => '2nd', 3 => '3rd', 4 => '4th',11 => '1st',12 => '2nd'];

                        $data = $this->model->getStudents($period, $term, $faculty, $sched);
                        $table = view($this->views.'ibed-class-record.table', ['data' => $data, 'period' => $period_list[$period]])->render();

                        $response = ['error' => false, 'content' => $table, 'ibed_table' => true];
                    }
                    break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }
        return $response;
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new Model();
        $this->permission = new Permission($this->ModuleName);
    }
}
