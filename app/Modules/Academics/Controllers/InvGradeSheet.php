<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
// use App\Modules\Academics\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Academics\Models\InvGradeSheet_model as IGS_Model;

use Permission;
use Request;
use Response;

class InvGradeSheet extends Controller
{
    protected $ModuleName = 'inventory-grade-sheet';

    private $media = [
        'Title' => 'Inventory Grade Sheet',
        'Description' => 'Welcome To Inventory Grade Sheet!',
        'js' => ['Academics/grade-sheet'],
        'init' => [],
        'plugin_js' => [
                        'bootbox/bootbox.min',
                        'datatables/media/js/jquery.dataTables.min',
                        'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                        'datatables/extensions/Scroller/js/dataTables.scroller.min',
                        'datatables/plugins/bootstrap/dataTables.bootstrap',
                        'select2/select2.min',
                    ],
        'plugin_css' => [
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2',
        ],
    ];

    private $url = ['page' => 'academics/faculty-assignment/'];

    private $views = 'Academics.Views.FacultyAssignments.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {


            SystemLog('Faculty-Assignment','','Page View','page-view','','' );
            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }
}
