<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
// use App\Modules\Setup\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Registrar\Models\TermConfig as Rep_Model;
use App\Modules\Setup\Models\GradingSystemSetup as mSetup;

use Permission;
use Request;
use Response;

class AcadSetup extends Controller
{
    protected $ModuleName = 'academic-setup';

    private $media = [ 'Title' => 'Academic Setup', 'Description' => 'Welcome To Academic Setup!',
            'js' => ['academics/setup'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'bootstrap-datepicker/css/datepicker',
            ],
        ];

    private $url = ['page' => 'academics/setup/'];

    private $views = 'Academics.Views.AcademicSetup.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {

            $_incl = [
                'views' => $this->views,
                'at' => $this->model->AcademicTerm(),
                'progs' => $this->model->Programs(),
            ];

            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'get-config':
                    if($this->permission->has('read')){
                        
                        $term = decode(Request::get('term'));                        
                        $where = array( 'TermID' => $term );                                                                        
                        $data = $this->model->where($where)->first();                                                                                                
                        if(!empty($data)){                            
                            
                            $allow = explode(',',$data->AllowedToEnroll );                        
                            $track = '';                        
                            foreach($allow as $r){
                                if($r != '') $track .= encode($r).",";
                            }                        
                            $response = ['error' => false, 'allow' => $track , 'start' =>  date('m/d/Y', strtotime($data->PublishFrom)), 'end'=> date('m/d/Y', strtotime($data->PublishTo))  ];
                            //$response = ['error' => false, 'first' =>  ($data->FirstTerm == 1 ? true : false ) , 'second'=>  ($data->SecondTerm  == 1 ? true : false ) ];
                        } else {
                            $response = ['error' => true, 'message' => 'Not yet configured','first'=>false,'second'=>false];
                        }
                    }
                    break;
                case 'save-config':
                    if($this->permission->has('add')){
                        
                        $setup = new mSetup();
                        
                        $term = decode(Request::get('t'));                        
                        $end = (Request::get('end'));
                        $start = (Request::get('start'));
                        $active = (Request::get('active'));
                        $cls = (Request::get('clsrcd') == 1 ?  1 : 0 );
                        
                        $allow = (Request::get('track'));
                        
                        $track = "";
                        
                        foreach($allow as $r){
                            $track .= decode($r).",";    
                        }
                        
                        
                        $where = array( 'TermID' => $term );
                        
                        $exists = $this->model->where($where)->count();
                        
                        $data = array(
                            'TermID' => $term,
                            'PublishFrom' => $start,
                            'PublishTo' => $end,
                            //'FirstTerm' => (Request::get('first')  =='true'? 1:0 ),
                            //'SecondTerm' => (Request::get('second') =='true' ? 1:0 ),
                            'ClassRecord' => $cls,
                            'AllowedToEnroll'=>$track
                        );
                        
                        if($exists > 0 ){
                            $data['Modifiedby'] = getUserID();
                            $data['DateCreated'] = systemDate();
                        }else{
                            $data['Createdby'] = getUserID();
                            $data['DateCreated'] = systemDate();
                        }
                        
                        if($active == 'true'){
                            $setup->where('SetupID',10)->update(['Value' => $term]);
                        }
                        
                                                                                                
                        $data = $this->model->updateorcreate($where, $data );

                        if(!empty($data)){                            
                            $response = ['error' => false, 'message' => 'Record successfully saved!'];
                        } else {
                            $response = ['error' => true, 'message' => 'No section(s) found.'];
                        }
                    }
                    break;
  
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        // $this->services = new Services();
        $this->model = new Rep_Model();
        $this->permission = new Permission($this->ModuleName);        
    }
}
