<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Academics\Services\FacultyEvaluationServiceProvider as Services;

use Permission;
use Request;
use Response;
use DB;

class FacultyEvaluation extends Controller
{
    #protected $ModuleName = 'summary-of-grades';
    protected $ModuleName = 'dashboard';

    private $media      =  [ 'Title' => 'Faculty Evaluation', 'Description' => 'Use this module to recalculate grades',
            'js'        => ['academics/summary.js?v=1.1'],
            'init'	    => ['ME.init()'],
            'css'       => ['profile'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'SmartNotification/SmartNotification.min'    
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'SmartNotification/SmartNotification'
            ],
        ];

    private $url = ['page' => 'academics/faculty-evaluation'];

    private $views = 'Academics.Views.FacultyEvaluation.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index(){
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
		    $_incl=array('views'  =>$this->views,
			             'summary'=>$this->svc->get_faculties(1001),);
            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }
	
	public function evalform(){
	    $this->views .= 'form.';
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
		    $_incl=array('views'  =>$this->views,
			             'summary'=>$this->svc->get_faculties(1001),);
            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
	}
   
    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
				case 'get-list':
					$ref = decode(Request::get('ref'));
					$view= view($this->views.'grades', ['ref' => $ref])->render();
					$response = ['error' => false, 'message' => 'success', 'view' => $view ];
					
				break;
				
                case 'compute':
                    
                    $rs = Request::get('list');
                    $list = "";
                    set_time_limit(240);
                    foreach($rs as $r => $v){
                        $list .= ($list!=''?',':'').decode($v);
                        $counter = service::recompute_ave(decode($v));
						$counter1 = service::evaluate_awardee(decode($v));																	
                    }
                    set_time_limit(30);
                    $response = ['error' => false, 'message' => 'computed records', 'list' => $list ];
                    
                    break;
                
                case 'sections':
                    if($this->permission->has('read')){
                        
                        $yl_id = (Request::get('level'));
                        $term_id = decode(Request::get('term'));                        
                        
                        
                        $data = $this->get_sections($term_id, $yl_id);
                                               
                        if(!empty($data)){                                                      
                            $response = ['error' => false, 'sections' => $data ];                            
                        } else {
                            $response = ['error' => true, 'message' => 'No section(s) found.'];
                        }
                    }
                    break;
                    
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }
    
    private function initializer(){
        // $this->services = new Services();        
        $this->permission = new Permission($this->ModuleName);   
        $this->svc        = new Services;		
    }
}
