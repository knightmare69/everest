<?php
namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Academics\Models\EmpFaculty;
use App\Modules\Academics\Services\Faculty\SvcFacultyInfo as svc;
use App\Libraries\Mmpdf as pdf;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;
use Storage;
use Image;
use DB;
use Auth;

class Faculty extends Controller
{
  private $media =
  [
        'Title'         => 'Faculty Management',
        'Description'   => 'Maintain Faculty Profile',
        'js'            => ['Academics/faculty',],	
        'plugin_js'     => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
							'jquery-validation/js/jquery.validate.min',
							'jquery-validation/js/additional-methods.min',
							'bootstrap-wizard/jquery.bootstrap.wizard.min',
                           ],
  ];	
  
  private $url = 
  [
	'page'  => 'academics/faculty-management/',
	'form' => 'form',
  ];
  
  public $f_view = 'Academics.Views.Faculty';
  
  private function initializer()
  {
    $this->svc        = new svc;
    $this->trial      = new EmpFaculty;
    $this->pdf        = new pdf;
    $this->xpdf       = new xpdf;
    $this->permission = new Permission('faculty-management');
  }
  
  public function init($apage='main')
  {
    ini_set('max_execution_time', 50);
	$this->initializer();
	$data = array('xview'    => $this->f_view,
				  'apage'    => $apage,);			
				  
    $data['campus']   = $this->svc->dropdown(array('table'    =>'ES_Campus'
	                                             ,'col_id'   =>'CampusID'
												 ,'col_desc' =>array('Acronym','ShortName')
											     ,'col_sep'  =>' - '
												 ,'name'     =>'campus'
												 ,'cls'      =>'form-control required'
												  ,'init'     =>'-1'
												));
    $data['college']  = $this->svc->dropdown(array('table'    =>'ES_Colleges'
	                                             ,'col_id'   =>'CollegeID'
												 ,'col_desc' =>array('CollegeCode','CollegeName')
											     ,'col_sep'  =>' - '
												 ,'name'     =>'college'
												 ,'cls'      =>'form-control required'
												  ,'init'     =>'-1'
												));				  
    $data['dept']     = $this->svc->dropdown(array('table'    =>'ES_Departments'
	                                             ,'col_id'   =>'DeptID'
												 ,'col_desc' =>array('DeptCode','DeptName')
											     ,'col_sep'  =>' - '
												 ,'name'     =>'dept'
												 ,'cls'      =>'form-control required'
												  ,'init'     =>'-1'
												));				  
    $data['position'] = $this->svc->dropdown(array('table'    =>'HR_PositionTitles'
	                                             ,'col_id'   =>'PosnTitleID'
												 ,'col_desc' =>'PositionDesc'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'pos'
												 ,'cls'      =>'form-control required'
												  ,'init'     =>'-1'
												));			  
    $data['emptitle'] = $this->svc->dropdown(array('table'    =>'HR_Titles'
	                                             ,'col_id'   =>'Name'
												 ,'col_desc' =>'Name'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'title'
												 ,'cls'      =>'form-control'
												  ,'init'     =>'-1'
												));			  
    $data['empsuffix'] = $this->svc->dropdown(array('table'  =>'HR_Suffix'
	                                             ,'col_id'   =>'Name'
												 ,'col_desc' =>'Name'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'suffix'
												 ,'cls'      =>'form-control'
												  ,'init'     =>'-1'
												));			
    $data['civilstats'] = $this->svc->dropdown(array('table'  =>'CivilStatus'
	                                             ,'col_id'   =>'StatusID'
												 ,'col_desc' =>'CivilDesc'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'civilstats'
												 ,'cls'      =>'form-control required'
												  ,'init'     =>'-1'
												));				
    $data['facrank'] = $this->svc->dropdown(array('table'  =>'ES_FacultyRanks'
	                                             ,'col_id'   =>'RankID'
												 ,'col_desc' =>'Rank'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'facrank'
												 ,'cls'      =>'form-control input-sm '
												  ,'init'     =>'-1'
												));			
    $data['educlvl'] = $this->svc->dropdown(array('table'  =>'ES_FacultyTeachingLoadEducLevel'
	                                             ,'col_id'   =>'TeachLevelID'
												 ,'col_desc' =>'TeachLoadLevel'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'educlvl'
												 ,'cls'      =>'form-control'
												  ,'init'     =>'-1'
												));		
    $data['discipline'] = $this->svc->dropdown(array('table'  =>'ES_DisciplineMajors'
	                                             ,'col_id'   =>'IndexID'
												 ,'col_desc' =>'MajorDiscDesc'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'discipline'
												 ,'cls'      =>'form-control'
												  ,'init'     =>'-1'
												));			
    $data['prc']       = $this->svc->dropdown(array('table'  =>'ES_PRC_Licensure'
	                                             ,'col_id'   =>'PRC_ID'
												 ,'col_desc' =>'ProfLicense'
											     ,'col_sep'  =>' - '
												 ,'name'     =>'prc'
												 ,'cls'      =>'form-control'
												  ,'init'     =>'-1'
												));				  
	return $data;
  }
  
  public function index()
  {	
    $this->initializer();
	if ($this->permission->has('read')) {
	  ini_set('max_execution_time', 60);
      return view('layout',array('content'=>view($this->f_view.'.index',$this->init('main')),'url'=>$this->url,'media'=>$this->media));
    }
    return view(config('app.403'));
  }
  
  public function photo()
  {
	$this->initializer();
	if ($this->permission->has('read')) {
	 ini_set('max_execution_time', 60);
     $empid   = Request::get('empid');
	 $exec    = $this->svc->getEmpPhoto($empid);
	 if($exec && $exec!='')
	 {	 
	  header('Content-type: image/jpg');	 
	  echo hex2bin($exec); 	   
	 }
	 else
	 {
      $path = asset('assets/global/img/empty.png');
	  header('Content-type: image/png');	 
	  @readfile($path); 	
	 }
    }
	else
	{
     $path = asset('assets/global/img/empty.png');
	 header('Content-type: image/png');	 
	 @readfile($path); 	
	}	
  }
  
  public function txn()
  {
	$this->initializer();
	ini_set('max_execution_time', 60);
    $response = 'No Event Selected';
    if (Request::ajax())
    {
	 $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
     switch(Request::get('event'))
     {
	  case 'emplist':
	   if ($this->permission->has('read')) {
	    $emp = $this->svc->get_emplist();
	    $response['success']=true;
	    $response['content']=(string)view($this->f_view.'.sub.table.employee',array('emp'=>$emp));
	   } else{
	    $response['message']='Permission Denied!';   
	   }
		   
      break;
      case 'emphist':
	   if ($this->permission->has('read')) {
	    $empid = Request::get('empid');
	    $hist  = $this->svc->get_emphistory($empid);
	    $response['success']=true;
	    if($hist)
	    {	   
	     $response['subjs']  = (string)view($this->f_view.'.sub.table.subjects',array('hist'=>$hist));
	     $response['teach'] = (string)view($this->f_view.'.sub.table.teach',array('hist'=>$hist));
	    }
       }
	   else
		$response['message']='Permission Denied!';  		
      break;
      case 'saveemp':
	   $xid     = Request::get('xid');
	   $fid     = Request::get('fid');
	   $empid   = Request::get('empid');
	   $empdata = Request::all();
	   
	   if (($xid=='new' && $this->permission->has('add')) || ($xid!='new' && $this->permission->has('edit'))) {
        $exec    = $this->svc->saveemp($xid,$fid,$empdata);
	    if($exec)
		 $response['success'] = true;
	    else
		 $response['message'] = 'Failed to save data';
	   }
	   else
		$response['message'] = 'Permission Denied';  
	  break;	  
	  case 'getphoto':
	   $empid   = Request::get('empid');
       $exec    = $this->svc->getEmpPhoto($empid);
	   if($exec && $exec!='')
	    echo hex2bin($exec); 	   
	   else
        echo 'empty';	
       break;	   
      case 'delemp':
	   if ($this->permission->has('read')) {
	   	$empid = Request::get('empid');
	    $optid = $this->svc->is_deletable($empid);
	    $exec  = $this->svc->del_employee($optid,$empid);
	   
	    if($exec)
	    {	   
	     $response['success']  = true;
	     $response['isdelete'] = (($optid==0)?true:false);
	    }
	    else
		 $response['message']='Failed to delete employee';  
	   }
	   else
		$response['message']='Permission Denied';  
      break;	  
	  case 'upload_img':
	   $empid = Request::get('empid');
	   $file     = $_FILES;
	   $fname    = '';
	   $ftype    = '';
	   $fsize    = '';
	   $tmpname  = '';
	   
	   if(count($file)>0)
	   {
		 foreach($file as $k=>$data)
         {
		   $fname   = $data['name'];
		   $ftype   = $data['type'];
		   $size    = $data['size'];
		   $tmpname = $data['tmp_name'];
		 }
         
         if($tmpname!='' && file_exists($tmpname))
         {
		  $_content      = file_get_contents($tmpname);
          $file_data     = unpack('H*hex', $_content);
          $file_data_hex = '0x'.$file_data['hex'];	 
		  $exec          = $this->svc->updateimg($empid,$file_data_hex);
		  
		  $response['success']=$exec;
		  $response['message']=(($exec)?'Failed to save image':'');
		 }			 
		 else
		  $response['message']='Invalid file content. No file receive';		   	 
       }
       else
         $response['message']='Invalid file content. Undefined file receive';		   
	   
       //$upl = $this->model->uploadPicture(decode(Request::get('stud_id')), $file_data_hex);
	   $response['success']  = true;
	  break;
	  default:
	   $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
      break;	  
	 }
     return $response;		 
    }
    else	
	{ 
	 return view(config('app.403'));
	}
  }  
  
}
?>