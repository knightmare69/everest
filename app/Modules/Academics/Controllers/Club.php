<?php namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Academics\Models\Club\Club as Model;
use App\Modules\Academics\Models\Club\Organization;
use App\Modules\Academics\Services\Club\StudentsWithoutClub;
use App\Modules\Academics\Services\Club\StudentsWithClub;
use Permission;
use Request;
use DB;

class Club extends Controller{
	private $media =
		[
			'Title'=> 'Club/Organization',
			'Description'=> 'Student Clubs/Organizations',
			'js'		=> ['Academics/club/index','Academics/club/helper','Academics/club/dataTable','datatable'],
			'init'		=> ['Me.init()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'select2/select2.min',
							'jquery-multi-select/js/jquery.multi-select',
			                'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
				            'jquery-validation/js/jquery.validate.min',
							'bootstrap-datepicker/js/bootstrap-datepicker',
							'bootstrap-timepicker/js/bootstrap-timepicker.min',
							'clockface/js/clockface',
							'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
				           ],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all',
				'clockface/css/clockface',
				'bootstrap-datepicker/css/datepicker3',
				'bootstrap-timepicker/css/bootstrap-timepicker.min',
				'bootstrap-colorpicker/css/colorpicker',
				'bootstrap-daterangepicker/daterangepicker-bs3',
				'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
			]
		];

	private $url = [ 'page' => 'academics/club-organization/' ];

	private $views = 'Academics.Views.club.';

	public function __construct()
	{
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'showFilter':
					if ($this->permission->has('search')) {
						$response = ['error' => false,'content'=>view($this->views.'forms.filter')->render()];
					}
				break;
				case 'showStudentsWithoutClubs':
						$response = $this->studentsWithoutClub->data();
				break;
				case 'showStudentsWitClubs':
						$response = $this->studentsWithClub->data();
				break;
				case 'loadClubs':
					if ($this->permission->has('search')) {
						$response = ['error' => false,'content'=>view($this->views.'tables.club')->render()];
					}
				break;
				case 'loadPrograms':
					if ($this->permission->has('search')) {
						$response = ['error' => false,'data'=>$this->loadPrograms()];
					}
				break;
				case 'showProfileData':
						$response = ['error' => false,'data'=>$this->getProfileData()];
				break;
				case 'showClubModal':
					if ($this->permission->has('add')) {
						$response = ['error' => false,'content'=>view($this->views.'forms.club')->render()];
					}
				break;
				case 'moveToClub':
					if ($this->permission->has('add')) {
						if (Request::get('ClubID')) {
							$response = $this->moveToClub() ? ['error' => false,'message' => 'Successfully move to club'] : errorSave();
						}
					}
				break;
				case 'removeFromClub':
					if ($this->permission->has('delete')) {
						if (Request::get('ClubID')) {
							$response = $this->removeFromClub() ? ['error' => false,'message' => 'Successfully remove from club'] : errorSave();
						}
					}
				break;
				case 'delete':
					if ($this->permission->has('delete')) {
						if (Request::get('ClubID')) {
							if ($this->model->where('ClubID',decode(Request::get('ClubID')))->count() <= 0) {
								$response = $this->org->where('ClubID',decode(Request::get('ClubID')))->delete() ? successDelete() : errorDelete();
							} else {
								$response = ['error'=>true,'message' => 'There are data on the Club cannot be deleted.'];
							}
						}
					}
				break;
				case 'saveClub':
					if ($this->permission->has('add')) {
                        $data = $this->getPost();
						if (Request::get('isEdit') == 'false') {						    
							$status = $this->org->insert($data);                            
                            SystemLog($this->media['Title'],'','Create New Club','save action',($data),'Status='.$status );                            
							$response = $status ? successSave() : errorSave();
						} else {
							$status = $this->org->where('ClubID',decode(Request::get('ClubID')))->update($data);
                            SystemLog($this->media['Title'],'','Edit Club Info','update action',($data),'Status='.$status );
							$response = $status ? successUpdate() : errorUpdate();
						}
					}
				break;
			}
		}
		return $response;
	}

	private function loadPrograms()
	{	
		return
		DB::table('ES_Programs as p')
			->select([
					'ProgID',
					'ProgName as Program'
				])
			->where('CampusID',decode(Request::get('SchoolCampus')))
			->where('ProgClass','<=',21)
			->get();
	}

	private function removeFromClub()
	{
		$data = Request::get('data');
		$data = jsonToArray($data);

		if (count($data[0]) <= 0) return false;

		foreach($data[0] as $row) {
			$row = getRawData($row);

			if (decode(getObjectValue($row,'RegID')) && getObjectValue($row,'StudentNo')) {
				$this->model
					->where('RegID',decode(getObjectValue($row,'RegID')))
					->where('StudentNo',getObjectValue($row,'StudentNo'))
					->delete();
			}
		}

		return true;
	}

	private function moveToClub()
	{
		$date = systemDate();
		$data = Request::get('data');
		$data = jsonToArray($data);

		if (count($data[0]) <= 0) return false;

		foreach($data[0] as $row) {
			$row = getRawData($row);
			$this->model
				->create([
					'RegID' => decode(getObjectValue($row,'RegID')),
					'StudentNo' => getObjectValue($row,'StudentNo'),
					'ClubID' => decode(Request::get('ClubID')),
					'CreatedBy' => getUserName(),
					'DateCreated' => $date
				]);
		}

		return true;
	}

	private function getProfileData() 
	{
		$StudentNo = decode(Request::get('StudentNo'));
		$data['Profile'] = 
			DB::table('ES_Students')
				->select([
						DB::raw("LastName+', '+FirstName+' '+MiddleInitial as Name"),
						'StudentNo','Gender',
						DB::raw("CONVERT(DATE,DateofBirth) as DateofBirth"),
					])
				->where('StudentNo',$StudentNo)
				->first();

		$data['Absenses'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','IsExcused','Demerit','Remarks',
						DB::raw("DATEDIFF(day,Date,DateEnd) as Days"),
						DB::raw("convert(varchar(11),Date) as Date"),
						DB::raw("convert(varchar(11),DateEnd) as DateEnd"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','1')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();
				
		$data['Offenses'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','IsExcused','Days','Demerit','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
						DB::raw("convert(varchar(11),DateEnd) as DateEnd"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','2')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		$data['Merits'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Merit','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','3')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		$data['LostFound'] = 
			DB::table('ES_GS_MeritDemerits as a')
				->select([
						DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','Merit','Demerit','Remarks',
						DB::raw("convert(varchar(11),Date) as Date"),
					])
				->where("StudentNo",$StudentNo)
				->where('Type','4')
				->where('TermID',decode(Request::get('SchoolYear')))
				->where('SectionID',Request::get("Section"))
				->get();

		return $data;
	}

	private function getPost()
	{
		$post = Request::all();
		$data = [
			'CampusID' => decode(getObjectValue($post,'SchoolCampus')),
			'TermID' => decode(getObjectValue($post,'SchoolYear')),
			'ClubName' => getObjectValue($post,'Club'),
			'ClubDescription' => getObjectValue($post,'ClubDesc'),
			'BldgID' => decode(getObjectValue($post,'Building')),
			'RoomID' => decode(getObjectValue($post,'Room')),
			'ProgID' => getObjectValue($post,'ProgramSel'),
			'ModeratorID' => decode(getObjectValue($post,'moderator')),
			'CreatedBy' => getUserID(),
			'DateCreated' => systemDate(),
		];		
		return $data;
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('club-organization');
 		$this->model = new Model;
 		$this->org = new Organization;
 		$this->studentsWithoutClub = new StudentsWithoutClub;
 		$this->studentsWithClub = new StudentsWithClub;
 	}
}
/*

select * from modules

insert into modules(name,url,slug,sort)
values('Registrar','#','registrar','2')

select * from pages

registrar subjects
insert into pages(name,slug,url,module_id,sort)
values('Subjects','registrar-subject','/registrar/subject',1004,1)

registrar attendance
insert into pages(name,slug,url,module_id,sort)
values('Attendance','registrar-attendance','/registrar/attendance',1004,1)

*/