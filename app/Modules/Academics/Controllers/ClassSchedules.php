<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Academics\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Academics\Models\FacultyAssignments as FA_Model;

use Permission;
use Request;
use Response;

class ClassSchedules extends Controller
{
    protected $ModuleName = 'faculty-assignment';

    private $media =
        [
            'Title' => 'Faculty Assignment',
            'Description' => 'Welcome To Faculty Assignments!',
            'js' => ['Academics/faculty-assign'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'academics/class-schedules/'];

    private $views = 'Academics.Views.ClassSchedules.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $def_campus = 1;
            $def_ac = 103;
            $_data = $this->model->getSections('', $def_campus, $def_ac);
            $incl = ['ac' => $this->services->academic_year(), 'campus' => $this->services->campus()];
            SystemLog('Faculty-Assignment','','Page View','page-view','','' );
            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'data' => $_data, 'incl' => $incl]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function newsection(){
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $def_campus = 1;
            $def_ac = 103;
            $_data = $this->model->getSections('', $def_campus, $def_ac);
            $incl = ['ac' => $this->services->academic_year(), 'campus' => $this->services->campus()];
            SystemLog('Class-Sections','','Page View','page-view','','' );
            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'data' => $_data, 'incl' => $incl]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'update-bygender':
                
                    if ($this->permission->has('edit')) {

                        $id =  decode(Request::get('sched'));
                        $val = (Request::get('v'));
                        $update = $this->model->where('ScheduleID', $id)->update(['FacultybyGender' => ($val ? 1:0) ]);

                        if($update){
                            $response = ['error' => false, 'message' => 'Successfully saved!'];
                            SystemLog('Faculty-Assignment','','Page View','Faculty by Gender',' ScheduleID:'. $id .'; Value:'.$val ,'' );
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to change '.$type.'.', 'status'=> $update ];
                        }
                    }
                    
                    break;

                case 'update-faculty':
                    if ($this->permission->has('edit')) {

                        $validation = $this->services->isValid(Request::all(), 'update');
                        if ($validation['error']) {
                            $response = Response::json($validation);

                        } else {
                            $mode = Request::get('mode');
                            $type = Request::get('type');
                            $id =  decode(Request::get('sched'));
                            $faculty = decode(Request::get('f'));

                            if($type == 'adviser'){
                                $update = $this->model->updateSection($id, $faculty);
                            } else {
                                if($mode == '1'){
                                    $update = $this->model->where('ScheduleID', $id)->update(['FacultyID' => $faculty]);
                                }else{
                                    $update = $this->model->where('ScheduleID', $id)->update(['FacultyID_2' => $faculty]);
                                }
                            }

                            if($update){
                                $response = ['error' => false, 'message' => 'Successfully changed '.$type.'.'];
                                SystemLog('Faculty-Assignment','','Page View','set-faculty','facultyid:'. $faculty. ' ScheduleID:'. $id .' Type:'.$type ,'' );
                            } else {
                                $response = ['error' => true, 'message' => 'Unable to change '.$type.'.', 'status'=> $update ];
                            }
                        }
                    }
                    break;

                case 'get':
                    if ($this->permission->has('read')) {

                        $sections = decode(Request::get('sections'));
                        $campus = decode(Request::get('campus'));
                        $acad_year = decode(Request::get('ac-year'));
                        // array_walk($sections, function($id){ return decode($id); });
                        $data = $this->model->getSections($sections, $campus, $acad_year);

                        if(!empty($data)){
                            if(empty($sections)){
                                $html = view($this->views.'class-sched', ['data' => $data])->render();
                            } else {
                                $html = view($this->views.'class-sched-rows', ['data' => $data])->render();
                            }
                            $response = ['error' => false, 'recs' => $html];

                        } else {
                            $response = ['error' => true, 'message' => 'No results found!'];
                        }
                    }
                    break;

                case 'get-sched':
                    if ($this->permission->has('read')) {
                        $section = decode(Request::get('section'));

                        $data = $this->model->getSectionsWithSubjects($section);

                        if(!empty($data)){
                            $tr = view($this->views.'class-sched-subj', ['data' => $data])->render();
                            $response = ['error' => false, 'subjs' => $tr];

                        } else {
                            $response = ['error' => true, 'message' => 'No results found!'];
                        }
                    }
                    break;

                case 'search-faculty':
                    if ($this->permission->has('search')) {

                         //$validation = $this->services->isValid(Request::all(), 'search-faculty');

                         //if ($validation['error']) {
                             //$response = Response::json($validation);

                         //} else {
                             $search = Request::get('faculty');
                             $sched = Request::get('id');
                             $type = Request::get('type');
                             $campus = decode(Request::get('campus'));
                             $find = $this->model->getFaculty($search, $campus);

                             if(!empty($find)){
                                 $vw_res = view($this->views.'faculty-res', ['faculty' => $find, 'key' => $sched, 'type' => $type])->render();
                                 $response = ['error' => false, 'message' => count($find).' result(s) found.', 'html' => $vw_res];

                             } else {
                                 $response = ['error' => true, 'message' => 'No coordinator found.'];
                             }
                         //}
                    }
                    break;

                case 'replace-faculty':
                    if ($this->permission->has('delete')){
                        $id = decode(Request::get('f'));
                        $type = Request::get('type');

                        if($type == 'adviser'){
                            $del = $this->model->removeAdviser($id);
                        } else {
                            $del = $this->model->where('ScheduleID', $id)->update(['FacultyID' => '']);
                        }

                        if($del){
                            $response = ['error' => false, 'message' => ucfirst($type).' is now empty.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to replace '.$type.'.'];
                        }
                    }
                    break;
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new FA_Model();
        $this->permission = new Permission($this->ModuleName);
    }
}