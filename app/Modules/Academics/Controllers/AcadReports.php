<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
//use App\Modules\Registrar\Services\ReportCardServiceProvider as service;
use App\Modules\Academics\Models\Academic_Reports as Rep_Model;
use App\Libraries\CrystalReport as xpdf;

use Permission;
use Request;
use Response;

class AcadReports extends Controller
{
    protected $ModuleName = 'academic-reports';

    private $media =
        [
            'Title' => 'Report Card',
            'Description' => 'Welcome To Report Card!',
            'js' => ['Academics/report'],
            'init' => [''],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'academics/reports/'];

    private $views = 'Academics.Views.Reports.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) { return redirect('/security/validation'); }
        $this->initializer();
        if ($this->permission->has('read')) {
            $_incl = [
                'views' => $this->views,
                'at' => $this->model->AcademicTerm(),
                'progs' => $this->model->Programs(),
            ];
            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

     public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'get-clubs':
                    if($this->permission->has('read')){
                        $term = decode(Request::get('t'));
                        $prog = decode(Request::get('p'));
                        
                        $vw = view($this->views.'sub.clubs', ['term' => $term,'prog'=>$prog])->render();
                        $response = ['error' => false, 'html' => $vw, 'period'  ];
                    }
                    break;

                case 'get-year-level':
                    if($this->permission->has('read')){
                        $prog_class = decode(Request::get('pclass'));

                        $data = $this->model->getYearLevel($prog_class);

                        if(!empty($data)){
                            $vw = view($this->views.'sub.year-level', ['yl' => $data])->render();
                            $response = ['error' => false, 'html' => $vw, 'period'  ];
                        } else {
                            $response = ['error' => true, 'message' => 'No year level found.'];
                        }
                    }
                    break;
                    
                case 'get-sections':
                    if($this->permission->has('read')){
                        $yl_id = decode(Request::get('yl'));
                        $term_id = decode(Request::get('t'));
                        $prog_id = decode(Request::get('p'));

                        $data = $this->model->getSections($term_id, $prog_id, $yl_id);

                        if(!empty($data)){

                            $subjectlist = "";

                            foreach($data AS $r){
                                $subjectlist .= $r->SectionID .',';
                            }

                            $subjects = $this->model->getSubjects($term_id, $subjectlist );

                            $vw = view($this->views.'sub.section', ['sec' => $data])->render();
                            $subj = view($this->views.'sub.subjects', ['subjects' => $subjects])->render();

                            $response = ['error' => false, 'html' => $vw , 'subjects' => $subj ];
                        } else {
                            $response = ['error' => true, 'message' => 'No section(s) found.'];
                        }
                    }
                    break;
                case 'get-students':
                    if($this->permission->has('read')){
                        $find = Request::get('student-name');

                        // if(!empty($find)){
                            $term_id = decode(Request::get('academic-term'));
                            $prog_id = decode(Request::get('programs'));
                            $yl_id = decode(Request::get('year-level'));
                            $section_id = decode(Request::get('section'));
                            $data = $this->model->getStudent($find, $term_id, $prog_id, $yl_id, $section_id);

                            if(!empty($data)){
                                $vw  = view($this->views.'sub.students', ['data' => $data])->render();
                                $response = ['error' => false, 'html' => $vw];

                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                    }
                    break;
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }



    public function print_report()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        $this->initializer();
        set_time_limit(0); //60 seconds = 1 minute
        if($this->permission->has('print')){

            $term_id = decode(Request::get('academic-term'));
            $prog_id = decode(Request::get('programs'));
            $yl_id = decode(Request::get('year-level'));
            $section_id = decode(Request::get('section'));
            $stud_num = Request::get('snum');
            $period = Request::get('period');

            $subjectid = decode( Request::get('subject'));

            $report = Request::get('report');

            $section_id = empty($section_id) ? 0 : $section_id;
            $subjectid = empty($subjectid) ? 0 : $subjectid;
            $stud_num = empty($stud_num) ? 0 : $stud_num;
            $configs = array($term_id, $stud_num, $section_id, $yl_id, $prog_id);

            $subrpt = array(
                'subreport1' => array(
                    'file' => 'Company_Logo',
                    'query' => "EXEC ES_rptReportLogo  'Everest Academy Manila', 'Republic', 'Muntinlupa City', '-1'"
                ),
            );

            switch ($report) {
            	case '20':
            		
            		if (empty($subjectid)) { 
            			$reptype=0;
            		} else {
            			$reptype=1;
            		}

            		$printedby=getUserFullName();
            		
                    $this->xpdf->filename = 'Best Students Academic.rpt';
                    $this->xpdf->query = "EXEC ES_rptGS_HS_BestStudentsAcademic_K12 '".$term_id."','".$subjectid."','".$prog_id."','".$yl_id."','".$reptype."','".$printedby."'";
                    $this->xpdf->SubReport = $subrpt;
                    // var_dump( $this->xpdf->query);
                    // die();
                    err_log($this->xpdf->query);
                    break;
                case '19':
                    $this->xpdf->filename = 'checklist2.rpt';
                    $this->xpdf->query = "EXEC sp_checklist_report '".$term_id."','".$section_id."','".$period."'";
                    break;
                case '18':
                    $prog_class = decode(Request::get('pclass'));
                    $this->xpdf->filename = 'unposted_grades.rpt';
                    $this->xpdf->query = "EXEC sp_k12_UnpostedGrades'".$term_id."','".$prog_id."','".$prog_class."','".$yl_id."','".$period."' ";
                    $this->xpdf->extension = 'xls'; // modified to be in excel form as requested
                    $this->xpdf->formatType = 29;
                break;

                case '17':

                    $clubs = decode(Request::get('clubs'));

                    $this->xpdf->filename = 'class-record-clubs.rpt';
                    $this->xpdf->query = "EXEC [sp_classrecord_clubs] '".$term_id."','".$clubs."','".$prog_id."','".$period."','0'";

                break;

                case '16':
                    $this->xpdf->filename = 'class-record-robotics.rpt';
                    $this->xpdf->query = "EXEC [sp_classrecord_robotics] '".$term_id."','".$section_id."','0','".$period."','".$subjectid."'";
                break;

            	case '15':
                	if($section_id==0) {
                		$type=2;
                	}
                	else 
                    {
                		$type=3;
                	}
                    $printedby=getUserFullName();
                    $campusid='1';
                    $this->xpdf->filename = 'shs classlist.rpt';
                    $this->xpdf->query = "ES_rptClassList_GradeSchool'".$type."','".$term_id."','".$campusid."','".$prog_id."','".$yl_id."','".$section_id."','".$printedby."' ";
                    //$subrpt = [];
                    break;

            	case '14':
                    $printedby=getUserFullName();
                    $campusid='1';

                    $this->xpdf->filename = 'list_of_class_schedule_program.rpt';
                    $this->xpdf->query = "ES_rptClassScheduleByProgram '".$campusid."','".$term_id."','".$prog_id."','".$printedby."' ";
                    //$subrpt = [];
                    break;

            	case '13':

                    $faculty = decode(Request::get('teacher'));

                    $this->xpdf->filename = 'Faculty Loading Assignment.rpt';
                    $this->xpdf->query = "sp_k12_GetFacultySchedulesSHS '".$term_id."', '".$faculty."' ";
                    $subrpt = [];
                    break;

                case '12':
                    $prog_class = decode(Request::get('pclass'));
                    $this->xpdf->filename = 'Letter of Academic Probation.rpt';
                    $this->xpdf->query = "ES_rptIBED_AcademicProbation_2;1 '".$term_id."','".$period."','".$yl_id."','".$prog_class."' ";
                    break;

                case '11':
                    $this->xpdf->filename = 'checklist.rpt';
                    $this->xpdf->query = "EXEC sp_checklist_report '".$term_id."','".$section_id."','".$period."'";
                    break;

                case '10':
                    // $is_all_sec = !empty($section_id) ? 2 : 1;

                    $this->xpdf->filename = 'Academic Failures.rpt';
                    $this->xpdf->query = "exec [ES_rptGS_HS_AcademicFailures_3] '".$term_id."', '1', '".$prog_id."', '".$yl_id."', '".$section_id."', '".$period."', 'Don Manolo Blvd.,Alabang Hills Village, Muntinlupa City' ";
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->extension = 'xls';
                    $this->xpdf->formatType = 29;


                    break;

                case '9':
                    if($prog_id == '29'){
                        $this->xpdf->filename = 'academic_failures_shs_summary.rpt';
                        $this->xpdf->query = "exec sp_SHS_AcademicFailuresSummary '".$term_id."', '".$prog_id."', '1' ";
                    }else{
                        $this->xpdf->filename = 'Academic Failures - Summary.rpt';
                        $this->xpdf->query = "exec [ES_rptGS_HS_AcademicFailuresSummary] '".$term_id."', '".$prog_id."', '1' ";
                        $this->xpdf->SubReport = $subrpt;
                        $this->xpdf->extension = 'xls';
                        $this->xpdf->formatType = 29;


                    }
                    //$subrpt = [];

                    break;

                 case '8':

                    $this->xpdf->filename = 'class-record-pe.rpt';
                    $this->xpdf->query = "EXEC [sp_classrecord_pe] '".$term_id."','".$section_id."','0','".$period."','".$subjectid."'";

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );
                break;

                case '7':

                    $this->xpdf->filename = 'class-record-homeroom.rpt';
                    $this->xpdf->query = "EXEC [sp_classrecord_homeroom] '".$term_id."','".$section_id."','0','".$period."','".$subjectid."'";

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );
                break;


                    # Consolidated subject report
                case '6':

                    $pclass = decode(Request::get('pclass'));

                    if(!empty($section_id)){
                        $yl_id = "";
                    }
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'ConsolidatedSubjectGrades.rpt';
                    $this->xpdf->query = "EXEC sp_k12_ConsolidatedSubjectGrades '".$term_id."', '".$period."', '".$yl_id."', '".$section_id."', '".$pclass."'";

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );

                    //var_dump($this->xpdf->query);
                    //die();

                break;
                   # Class record Sheet
                case '5':
                	$this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'class-record.rpt';
                    $this->xpdf->query = "EXEC sp_classrecord '".$term_id."','".$section_id."','0','".$period."','".$subjectid."'";

                    // var_dump($this->xpdf->query);
                    // die();

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );
                break;
                case '1':
                    # Inventory of Class Records
                	$this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'inventory of class records.rpt';
                    $this->xpdf->query = "EXEC [sp_K12_ClassRecordInventory] '".$term_id."','".$period."'";

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );
                    break;

                case '2':
                    # Inventory of Class Records by Faculty
                	$this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'inventory of class records by faculty.rpt';
                    $this->xpdf->query = "EXEC [sp_K12_ClassRecordInventory] '".$term_id."','".$period."'";

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );
                    break;

                case '3': #Academic Warning Notice
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'academic warning notice.rpt';
                    $this->xpdf->query = "EXEC sp_k12_remediation '".$term_id."','". $stud_num ."','".$period."'";

                    err_log($this->xpdf->query);

                    // $subrpt = array( 'subreport1' => $subrep_logo, );
                    break;


                case '4': #Academic Remidiation Notice
                    # Junior High rpt
                	$this->xpdf->SubReport = $subrpt;
                    //echo $stud_num;
                    $this->xpdf->filename = 'academic remediation notice.rpt';
                    $this->xpdf->query = "EXEC sp_k12_remediation '".$term_id."','". $stud_num ."','".$period."'";

                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );

                    break;
                case '21':
                    
                    $printedby=getUserFullName();
                    $campusid='1';

                    $this->xpdf->filename = 'list_of_class_schedule_subjects.rpt';
                    $this->xpdf->query = "ES_rptClassScheduleByProgram '".$campusid."','".$term_id."','".$prog_id."','".$printedby."'";
                    
                break;
                case '22':
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'Consolidated Conduct Grades.rpt';
                    $this->xpdf->query = "EXEC sp_K12_consolidated_conduct_grades '".$term_id."','".$period."','".$prog_id."','".$yl_id."','".$section_id."'";
                    // var_dump($this->xpdf->SubReport);
                    // die();
                break;
                case '23':
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'Consolidated Quarter Grades.rpt';
                    $this->xpdf->query = "EXEC ES_rptIBED_ConsolidatedQuarterGrades_r2 '".$term_id."', '".$prog_id."', '".$period."', '".$yl_id."', '".$section_id."', '".$subjectid."'";
                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );

                break;
                case '24':
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'DailyAttendanceReport.rpt';
                    $this->xpdf->query = "EXEC sp_K12_DailyAttendanceReport  '".$term_id."', '".$yl_id."', '".$prog_id."', '".$section_id."', '".$period."'";
                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );
                    break;
                case '25':
                    $this->xpdf->SubReport = $subrpt;
                    $this->xpdf->filename = 'AttendancewithRemarks.rpt';
                    $this->xpdf->query = "EXEC sp_K12_DailyAttendanceWtihRemarks  '".$term_id."', '".$yl_id."', '".$prog_id."', '".$section_id."'";
                    // $subrpt = array(
                    //     'subreport1' => $subrep_logo,
                    // );

                break;
                default: die('No reports available.'); break;
            }

            $data = $this->xpdf->generate();


            switch ($this->xpdf->extension) {
                case 'xls': $mtype = 'vnd.ms-excel'; break;
                default: $mtype = 'pdf'; break;
            }



		    header('Content-type: application/'.$mtype);
            header('Content-Disposition: inline; filename="'.str_replace('.rpt', '', $this->xpdf->filename).'.'.$this->xpdf->extension.'"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            ob_end_clean();
            readfile($data['file']);
            die();
        }else{
            die("Permission Denied!");
        }
        return $response;
    }

    private function initializer()
    {
        // $this->services = new Services();
        $this->model = new Rep_Model();
        $this->permission = new Permission($this->ModuleName);
        $this->xpdf = new xpdf;
    }
}