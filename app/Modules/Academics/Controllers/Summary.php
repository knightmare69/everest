<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as Rep_Model;

use App\Modules\Academics\Services\SummaryService as service;

use Permission;
use Request;
use Response;

class Summary extends Controller
{
    #protected $ModuleName = 'summary-of-grades';
    protected $ModuleName = 'dashboard';

    private $media = [ 'Title' => 'Summary of Grades', 'Description' => 'Use this module to recalculate grades',
            'js' => ['academics/summary.js?v=1.1'],
            'init'		=> ['ME.init()'],
            'css' => ['profile'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'SmartNotification/SmartNotification.min'    
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'SmartNotification/SmartNotification'
            ],
        ];

    private $url = ['page' => 'academics/summary-of-grades'];

    private $views = 'Academics.Views.summary.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            
            $req = Request::all();
            
            $term = getObjectValue($req, 't');
            $camp = getObjectValue($req, 'c');
            $level = getObjectValue($req, 'l');
            $sec = decode(getObjectValue($req, 'sec'));
            $sections = '';
            
            if($term == '') {
                $term = shsterm();
            }else{
                $term = decode($term);
            }
            
            if($camp != ''){
                $camp = decode($camp);
            }
            
            if($level == ''){
                $level == '0';                
            }else{
                $sections = $this->get_sections($term, $level,$sec);    
            }                                    
                    
            $_incl = [
                'views' => $this->views,
                'at' => $this->term->AcademicTerm(),
                'total' => 0 ,
                'term' => $term,
                'camp' => $camp,
                'lvl' => $level,
                'sections' => $sections,
                'sec' => $sec
            ];
			SystemLog('Summary of Grades','','Page View','page-view','','Page Successfully loaded' );
			
			
            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }
   
    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
				case 'get-details':
					$ref = decode(Request::get('ref'));
					$view= view($this->views.'grades', ['ref' => $ref])->render();
					$response = ['error' => false, 'message' => 'success', 'view' => $view ];
					
				break;
				
                case 'compute':
                    
                    $rs = Request::get('list');
                    $list = "";
                    set_time_limit(240);
                    foreach($rs as $r => $v){
                        $list .= ($list!=''?',':'').decode($v);
                        $counter = service::recompute_ave(decode($v));
						$counter1 = service::evaluate_awardee(decode($v));																	
                    }
                    set_time_limit(30);
                    $response = ['error' => false, 'message' => 'computed records', 'list' => $list ];
                    
                    break;
                
                case 'sections':
                    if($this->permission->has('read')){
                        
                        $yl_id = (Request::get('level'));
                        $term_id = decode(Request::get('term'));                        
                        
                        
                        $data = $this->get_sections($term_id, $yl_id);
                                               
                        if(!empty($data)){                                                      
                            $response = ['error' => false, 'sections' => $data ];                            
                        } else {
                            $response = ['error' => true, 'message' => 'No section(s) found.'];
                        }
                    }
                    break;
                    
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }
    
    private function get_sections($t, $y, $sec = ''){
        $vw= '';
        
        if($y == '0' || $y == '' ){
            $dept = get_deptlevel('1');
        }else{
            $dept = get_deptlevel($y);    
        }
        
        
        $data = service::get_sections($t, $dept->ProgID, $dept->YLID_OldValue);
        foreach($data as $s){
            $vw .= "<option ". ( $sec == $s->SectionID ? "selected":'') ." value=". encode($s->SectionID).">".trimmed($s->SectionName)."</option>";
        }
		$vw .= "<option ". ( $sec == -1 ? "selected":'') ." value='-1'> All Section/s </option>";
        return $vw;
    }
    
    private function initializer()
    {
        // $this->services = new Services();        
        $this->term = new Rep_Model();
        $this->permission = new Permission($this->ModuleName);        
    }
}
