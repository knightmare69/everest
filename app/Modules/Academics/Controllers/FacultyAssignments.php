<?php

namespace App\Modules\Academics\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Academics\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Academics\Models\FacultyAssignments as FA_Model;
use App\Libraries\pdf\TCPDF;

use Permission;
use Request;
use Response;

use DB;


class FacultyAssignments extends Controller
{
    protected $ModuleName = 'faculty-assignment';

    private $media = [
            'Title' => 'Faculty Assignment',
            'Description' => 'Welcome To Faculty Assignments!',
            'js' => ['Academics/faculty-assign.js?v0.1'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
                            'bootstrap-timepicker/js/bootstrap-timepicker.min'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
                'bootstrap-timepicker/css/bootstrap-timepicker.min'
            ],
        ];

    private $url = ['page' => 'academics/faculty-assignment/'];
    private $views = 'Academics.Views.FacultyAssignments.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $def_campus = 1;
            $def_ac = 103;
            $_data = $this->model->getSections('', $def_campus, $def_ac);
            $incl = ['ac' => $this->services->academic_year(), 'campus' => $this->services->campus()];
            SystemLog('Faculty-Assignment','','Page View','page-view','','' );
            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'data' => $_data, 'incl' => $incl]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
                case 'update-special':
                    if ($this->permission->has('edit')) {

                        $id =  decode(Request::get('sched'));
                        $val = (Request::get('v'));
                        $update = $this->model->where('ScheduleID', $id)->update(['IsSpecialClass' => ($val ? 1:0) ]);

                        if($update){
                            $response = ['error' => false, 'message' => 'Successfully saved!'];
                            SystemLog('Faculty-Assignment','','Record update','Special Class',' ScheduleID:'. $id .'; Value:'.$val ,'' );
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to change '.$type.'.', 'status'=> $update ];
                        }

                    }
                    break;
                    
                case 'update-bygender':
                    if ($this->permission->has('edit')) {

                        $id     =  decode(Request::get('sched'));
                        $val    = (Request::get('v'));
                        if($val == 'true'){
						  $update  = $this->model->where('ScheduleID', $id)->update(['FacultybyGender' => ($val ? 1:false) ]);
						}else{
						  $update  = DB::statement("UPDATE ES_ClassSchedules SET FacultyByGender=0 WHERE ScheduleID='".$id."'");
						}

                        if($update){
                            $response = ['error' => false, 'message' => 'Successfully saved!'];
                            SystemLog('Faculty-Assignment','','Page View','Faculty by Gender',' ScheduleID:'. $id .'; Value:'.$val ,'' );
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to change '.$type.'.', 'status'=> $update ];
                        }

                    }
                    break;

                case 'update-faculty':
                    if ($this->permission->has('edit')) {

                        $validation = $this->services->isValid(Request::all(), 'update');
                        if ($validation['error']) {
                            $response = Response::json($validation);

                        } else {
                            $mode = Request::get('mode');
                            $type = Request::get('type');
                            $id =  decode(Request::get('sched'));
                            $faculty = decode(Request::get('f'));

                            if($type == 'adviser'){
                                $update = $this->model->updateSection($id, $faculty);
                            } else {
                                if($mode == '1'){
                                    $update = $this->model->where('ScheduleID', $id)->update(['FacultyID' => $faculty]);
                                }else{
                                    $update = $this->model->where('ScheduleID', $id)->update(['FacultyID_2' => $faculty]);
                                }
                            }

                            if($update){
                                $response = ['error' => false, 'message' => 'Successfully changed '.$type.'.'];
                                SystemLog('Faculty-Assignment','','Page View','set-faculty','facultyid:'. $faculty. ' ScheduleID:'. $id .' Type:'.$type ,'' );
                            } else {
                                $response = ['error' => true, 'message' => 'Unable to change '.$type.'.', 'status'=> $update ];
                            }
                        }
                    }
                    break;

                case 'get':
                    if ($this->permission->has('read')) {

                        $sections = decode(Request::get('sections'));
                        $campus = decode(Request::get('campus'));
                        $acad_year = decode(Request::get('ac-year'));
                        // array_walk($sections, function($id){ return decode($id); });
                        $data = $this->model->getSections($sections, $campus, $acad_year);

                        if(!empty($data)){
                            if(empty($sections)){
                                $html = view($this->views.'class-sched', ['data' => $data])->render();
                            } else {
                                $html = view($this->views.'class-sched-rows', ['data' => $data])->render();
                            }
                            $response = ['error' => false, 'recs' => $html];

                        } else {
                            $response = ['error' => true, 'message' => 'No results found!'];
                        }
                    }
                    break;

                case 'get-sched':
                    if ($this->permission->has('read')) {
                        $section = decode(Request::get('section'));

                        $data = $this->model->getSectionsWithSubjects($section);

                        if(!empty($data)){
                            $tr = view($this->views.'class-sched-subj', ['data' => $data])->render();
                            $response = ['error' => false, 'subjs' => $tr];

                        } else {
                            $response = ['error' => true, 'message' => 'No results found!'];
                        }
                    }
                    break;

                case 'search-faculty':
                    if ($this->permission->has('search')) {

                         //$validation = $this->services->isValid(Request::all(), 'search-faculty');

                         //if ($validation['error']) {
                             //$response = Response::json($validation);

                         //} else {
                             $search = Request::get('faculty');
                             $sched = Request::get('id');
                             $type = Request::get('type');
                             $campus = decode(Request::get('campus'));
                             $find = $this->model->getFaculty($search, $campus);

                             if(!empty($find)){
                                 $vw_res = view($this->views.'faculty-res', ['faculty' => $find, 'key' => $sched, 'type' => $type])->render();
                                 $response = ['error' => false, 'message' => count($find).' result(s) found.', 'html' => $vw_res];

                             } else {
                                 $response = ['error' => true, 'message' => 'No coordinator found.'];
                             }
                         //}
                    }
                    break;

                case 'replace-faculty':
                    if ($this->permission->has('delete')){
                        $id = decode(Request::get('f'));
                        $type = Request::get('type');

                        if($type == 'adviser'){
                            $del = $this->model->removeAdviser($id);
                        } else if ($type == 'Teacher 1'){
                            $del = $this->model->where('ScheduleID', $id)->update(['FacultyID' => '']);
                        } else if ($type == 'Teacher 2'){
                            $del = $this->model->where('ScheduleID', $id)->update(['FacultyID_2' => '']);
                        }

                        if($del){
                            $response = ['error' => false, 'message' => ucfirst($type).' is now empty.'];
                            SystemLog('Faculty-Assignment','','Remove Faculty','remove-faculty','Sched-ID : ' . $id ,'' );
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to replace '.$type.'.'];
                        }
                    }
                    break;

                case 'create-section':

                    $term = decode(Request::get('term'));
                    $vw_res = view($this->views.'sub.section', ['term' => $term])->render();
                    $response = ['error' => false, 'message' => '', 'html' => $vw_res];

                break;

                case 'get-yearterm':

                    $curriculum = decode(Request::get('curriculum'));
                    $vw_res = view($this->views.'sub.yearterm', ['data' => $this->services->yearterm($curriculum) ])->render();
                    $response = ['error' => false, 'message' => '', 'html' => $vw_res];

                break;

                case 'get-subjects':

                    $curr = decode(Request::get('curriculum'));
                    $yt = decode(Request::get('yearterm'));
                    $vw_res = view($this->views.'sub.subjects', ['data' => $this->services->curriculumSubjects($curr,$yt) ])->render();
                    $response = ['error' => false, 'message' => '', 'html' => $vw_res];

                break;

                case 'save-section':

                    $post = Request::all();
                    $vw_res = $this->services->createSection($post);
                    SystemLog('Faculty-Assignment','','Create Section','create-section','Section Name: ' . getObjectValue($post,'section') ,'' );

                    $response = ['error' => false, 'message' => 'record successfully saved!', 'html' => $vw_res];

                break;

                 case 'delete-section':

                    $sec = decode(Request::get('section'));
                    $vw_res = $this->services->deleteSection($sec);
                    $response = ['error' => false, 'message' => 'record successfully deleted!', 'html' => $vw_res];

                break;

                 case 'scheduler':

                    $sched = decode(Request::get('sched'));
                    $vw_res = view($this->views.'sub.scheduler', ['sched' => $sched])->render();
                    $response = ['error' => false, 'message' => '', 'html' => $vw_res];

                break;

                case 'save-schedule':

                    $post = Request::all();
                    $res = $this->services->updateSchedule($post);

                    if( is_array($res) ){

                        $htm = view($this->views.'sub.conflict', ['data' => $res])->render();
                        $response = ['error' => true, 'message' => 'conflict found!', 'html' => $htm];
                    }else{
                        $response = ['error' => false, 'message' => 'record successfully saved!', 'html' => $res];
                    }

                break;

                case 'adjust-class-limit':

                    $post = Request::all();
                    $res = $this->services->updateClassSize($post);

                    if( $res ){
                        SystemLog('Faculty-Assignment','','Adjusted Class Limit','adjust class limit','Section : ' . getObjectValue($post,'section') ,'' );
                        $response = ['error' => false, 'message' => 'class section limit successfully adjusted!'];
                    }else{
                        $response = ['error' => true, 'message' => 'unable to adjust class size'];
                    }

                break;

                 case 'save-new-name':

                    $post = Request::all();
                    $res = $this->services->classRename($post);

                    if( $res ){
                        SystemLog('Faculty-Assignment','','Rename Class Section','save new class name','Old Name : ' . getObjectValue($post,'old') . ' New Name : ' . getObjectValue($post,'new') ,'' );
                        $response = ['error' => false, 'message' => 'class section new name successfully changed!'];
                    }else{
                        $response = ['error' => true, 'message' => 'unable to change class name'];
                    }

                break;

                case 'get-subject-replace':
                    if ($this->permission->has('search')) {
                        $d = $this->model->subjectReplaceSelection();

                        $htm = view($this->views.'sub.subj-replace', ['data' => $d, 'action' => Request::get('action')])->render();
                        $response = ['error' => false, 'html' => $htm];
                    }
                    break;

                case 'save-subject-replace':
                    if ($this->permission->has('edit')) {
                        $sched_id = decode(Request::get('sched'));
                        $subj_id = decode(Request::get('subj'));
                        $term_id = decode(Request::get('term'));

                        $old_subj = $this->model->where(['ScheduleID' => $sched_id])->pluck('SubjectID');
                        $u = $this->model->where(['ScheduleID' => $sched_id])->update(['SubjectID' => $subj_id]);

                        SystemLog('Faculty-Assignment', '', 'Replace old subject', 'replace subject', 'Old Subject ID: '.$old_subj.' | New Subject ID: '.$subj_id.' | SchedID: '.$sched_id, '');
                        $response = ['error' => false, 'message' => 'Successfully replaced subject.'];
                    }
                    break;

                case 'save-added-subject':
                    if ($this->permission->has('edit')) {
                        $section_id = decode(Request::get('sec'));
                        $subj_id = decode(Request::get('subj'));
                        $term_id = decode(Request::get('term'));

                        $find = $this->model->where(['TermID' => $term_id, 'SectionID' => $section_id, 'SubjectID' => $subj_id])->first();

                        if(!empty($find)){
                            $response = ['error' => true, 'message' => 'Subject already been taken.'];

                        } else {
                            $sched = $this->model->insertGetId(['TermID' => $term_id, 'SectionID' => $section_id, 'SubjectID' => $subj_id]);

                            SystemLog('Faculty-Assignment', '', 'Add new subject', 'add subject', 'Subject ID: '.$subj_id.' | Section ID: '.$section_id, '');

                            $data = $this->model->getSectionsWithSubjects($section_id);
                            $tb = view($this->views.'class-sched-subj', ['data' => $data])->render();

                            $response = ['error' => false, 'message' => 'Successfully add subject.', 'html' => $tb];
                        }

                    }
                    break;

                case 'delete-subject':
                    if ($this->permission->has('edit')) {
                        $sched_id = decode(Request::get('sched'));

                        $subj_id = $this->model->where('ScheduleID', $sched_id)->pluck('SubjectID');
                        $this->model->where('ScheduleID', $sched_id)->delete();

                        SystemLog('Faculty-Assignment', '', 'Delete subject', 'delete subject', 'Deleted Subject ID: '.$subj_id, '');

                        $response = ['error' => false, 'message' => 'Successfully deleted subject.'];
                    }
                    break;


                default: return response('Unauthorized.', 401);
                break;
            }
        }

        return $response;
    }

    public function printSectionSched()
    {
        $this->initializer();
        $id = decode(Request::get('id'));

        if ($this->permission->has('edit') && !empty($id)) {
            $type = Request::get('type');

            switch ($type) {
                case 'faculty':
                    $term = decode(Request::get('term'));
                    $fac_details = $this->model->getFaculty($id, 1);

                    $scheds = $this->model->getSectionsWithSubjects('', "TermID = '$term' AND (FacultyID = '$id' OR FacultyID_2 = '$id' OR FacultyID_3 = '$id' OR FacultyID_4 = '$id' OR FacultyID_5 = '$id')");

                    $header_html = '<div><h3 style="line-height: 5%;">Faculty Schedules</h3>Name: <b>'.$fac_details[0]->FacultyName.'</b><br></div>';

                    break;

                case 'section':
                    $secd = $this->model->getSectionDetails($id);
                    $scheds = $this->model->getSectionsWithSubjects($id);

                    $header_html = '<div><h3 style="line-height: 5%;">Class Schedules</h3>
                    Academic Term: <b>'.$secd->AcademicYear.'</b><br/>
                    Section: <b>'.$secd->SectionName.'</b><br></div>';
                    break;

                default:
                    $scheds = [];
                    break;
            }

            if(!empty($scheds)){

                ob_clean();

                $pdf = new PdfHeader;
                // $pdf->header_html = '<h3>Sample mo to!</h3>asdasdasd';
                $pdf->SetPrintHeader(false);
                // $pdf->SetPrintFooter(false);
                $pdf->SetTitle('Class Schedule');
                $pdf->SetAutoPageBreak(TRUE, 21.4);

                $pdf->AddPage();
                $pdf->writeHTML(view($this->views.'print-class-sched', ['schedules' => $scheds, 'header' => $header_html, 'type' => $type])->render());
                $gen = $pdf->Output('Class Schedule', 'I');

                return $gen;

            } else {
                return view(config('app.404'));

            }

        } else {
            return view(config('app.404'));

        }

    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new FA_Model();
        $this->permission = new Permission($this->ModuleName);
    }
}

class PdfHeader extends TCPDF
{
    public $header_html = '';

    // public function Header() {
        // Set font
        // $this->SetFont('helvetica', 'B', 20);
        // Title
        // $this->writeHTMLCell(0, 40, 10, 0, $this->header_html, 0, 0, 0, true, 'L');
        // $this->Cell(0, 0, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    // }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
