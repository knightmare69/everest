<?php 
    $term = isset($term) ?  $term : 0 ;
    $prog = isset($prog) ?  $prog : 0 ; 
?>
@foreach(App\Modules\Academics\Models\Club\Organization::where(['TermID'=>$term,'ProgID'=>$prog])->orderBy('ClubName','ASC')->get() as $r)
<option value="{{ encode($r->ClubID) }}">{{ $r->ClubName }}</option>
@endforeach