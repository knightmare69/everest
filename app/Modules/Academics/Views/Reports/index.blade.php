<?php
// error_print($progs);
// die();
?>
<div class="row">
    <div class="col-sm-8">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i> Report Parameters
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                <form class="horizontal-form" id="report-form" action="#" method="POST">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Select Report</label>
                                    <select class="select2 form-control select2-" name="report" id="report">
                                        <option value="-1">- Select -</option>
                                        <optgroup label="Class Record" >
                                            <option value="5">Class Record Sheet</option>
                                            <!-- <option value="7">Class Record Sheet - Home Room</option>
                                            <option value="8">Class Record Sheet - PE</option>
                                            <option value="16">Class Record Sheet - Robotics</option>
                                            <option value="17">Class Record Sheet - Clubs</option> -->                                        
                                            <option value="1">Inventory of Class Record</option>
                                            <option value="2">Inventory of Class Record by Faculty</option>
                                            <option value="24">Daily Attendance Report</option>
                                            <option value="25">Attendance Report with Remarks</option>
                                        </optgroup>
                                        
                                        <optgroup label="Academic Reports">
                                        <option value="3">Academic Warning Notice</option>
                                        <option value="4">Academic Remediation Notice</option>
                                        <option value="9">Academic Failures - Summary</option>
                                        <option value="10">Academic Failures</option>
                                        <option value="20">Best Students Academic</option>                                        
                                        <option value="6">Consolidated Subject Grades</option>
                                        <option value="23">Consolidated Quarter Grades</option>
                                        <option value="22">Consolidated Conduct Grades</option>
                                        <option value="12">Letter of Academic Probation</option>
                                        <option value="11">Checklist Report</option>
                                        <option value="18">Unposted Grades</option>
                                        <option value="19">Checklist Report per Students</option>
                                        </optgroup>
                                        <optgroup label="Academic Schedules">                                                                                
                                            <option value="15">Class List</option> 
                                            <option value="13">Faculty Loading Assignment</option>
                                            <option value="14">List of Class Schedule Program</option>
                                            <option value="21">List of Class Schedule [Subject]</option>
                                        </optgroup>                                                                                                                                                                                                                                                
                                    </select>
                                </div>
                            </div>
                        </div>
                                     <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Academic Term</label>
                                    <select class="select2 form-control" name="academic-term" id="academic-term">
                                        <option value="-1" selected="">- Select -</option>
                                        @if(!empty($at))
                                            @foreach($at as $_this)
                                            <option value="{{ encode($_this->TermID) }}">{{ $_this->AcademicYear.' - '.$_this->SchoolTerm }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Period</label>
                                    <select class="select2 form-control" name="period" id="period">
                                        <option value="-1" selected=""> - Select - </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Programs</label>
                                    <select class="select2 form-control" name="programs" id="programs">
                                        <option value="" disabled selected></option>
                                        @if(!empty($progs))
                                            @foreach($progs as $_this)
                                            <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                                                {{ $_this->ProgName }}
                                            </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                      
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Year Level</label>
                                    <select class="select2 form-control" name="year-level" id="year-level" disabled>
                                        @include($views.'sub/year-level')
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" data-mode="club" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Club/Organization</label>
                                    <select class="select2 form-control" name="clubs" id="clubs" >
                                        @include($views.'sub/clubs')
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" data-mode="schedules">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Section</label>
                                    <select class="select2 form-control" name="section" id="section" disabled>
                                        @include($views.'sub/section')
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" data-mode="subject">
                                <div class="form-group">
                                    <label class="control-label">Subject</label>
                                    <select class="select2 form-control" name="subject" id="subject" disabled>
                                        @include($views.'sub/subjects')
                                    </select>
                                </div>
                            </div>

                        </div>
                        
                        
                        <!--/row-->
                        <div class="row" data-mode="students" >
                            <div class="col-md-12">
                                <label class="control-label student">Student(s)</label>
                                <div class="input-group">
                                    <input type="text" class="form-control stud-find student" data-snum="" name="student-name" id="student-name" disabled>
                                    <span class="input-group-btn">
                                        <button type="button" class="faculty-search-btn btn btn-default stud-find student" id="student-find" disabled>
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="faculty-search-btn btn btn-default stud-remove student" id="student-remove">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label faculty">Faculty</label>
                                    <select class="form-control select2 faculty" name="teacher" id="teacher">
                                        <option value="">- Select -</option>
                                        <option value="-1">SHS teachers</option>
                                        @foreach(App\Modules\Academics\Models\EmpFaculty::GetAllactive() as $row)
                                        <option value="{{ encode($row->EmployeeID) }}">{{ strtoupper(trimmed($row->LastName)) . ', ' . trimmed($row->FirstName) }}</option>
                                        @endforeach
                                    </select>


                                </div>
                            </div>   
                        </div>
                        <!--/row-->                    </div>
                    <div class="form-actions right">
                        <!-- <button class="btn default btn_reset" type="button">Reset</button> -->
                        <button class="btn blue btn_action btn_save" type="button" id="print-report"><i class="fa fa-print"></i> Print Report</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>