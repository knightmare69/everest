<?php 
    $data = []; 
    if (Request::get('ClubID')) {
        $data = App\Modules\Academics\Models\Club\Organization::where('ClubID',decode(Request::get('ClubID')))->first();
    }

    function isSel($a,$b) {
        return $a == $b ? 'selected' : '';
    }
    
?>
<form class="horizontal-form" id="FormClub" action="#" method="POST">
    <div class="form-body">
        <div class="row">
           <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Campus</label>
                    <select class="form-control Clubfilter do-not-clear" name="SchoolCampus" id="SchoolCampus">
                        @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                        <option {{ isSel(getObjectValue($data,'CampusID'),$row->CampusID) }} value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                        @endforeach
                    </select>
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">School Year</label>
                    <?php $i = 0; ?>
                    <select class="form-control Clubfilter do-not-clear" name="SchoolYear" id="SchoolYear">
                        @foreach(App\Modules\Admission\Models\Admission::getYearTerm() as $row)
                        <option {{ isSel(getObjectValue($data,'TermID'),$row->TermID) }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear }}</option>
                        @endforeach
                    </select>
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Program</label>
                    <input type="hidden" class="form-control" name="ProgramSel" id="ProgramSel" value="{{ getObjectValue($data,'ProgID') }}">
                    <select class="form-control" name="Program" id="Program">
                        <option value="">Select...</option>
                    </select>
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Building</label>
                    <?php $i = 0; ?>
                    <select class="form-control not-required" name="Building" id="Building">
                        <option value="">Select...</option>
                        @foreach(App\Modules\Academics\Models\Club\Buildings::get() as $row)
                        <option {{ isSel(getObjectValue($data,'BldgID'),$row->BldgID) }} value="{{ encode($row->BldgID) }}">{{ $row->BldgName }}</option>
                        @endforeach
                    </select>
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Room</label>
                    <select class="form-control select2 do-not-clear not-required" name="Room" id="Room">
                        <option value="">Select...</option>
                        @foreach(App\Modules\Academics\Models\Club\Rooms::get() as $row)
                        <option {{ isSel(getObjectValue($data,'RoomID'),$row->RoomID) }} value="{{ encode($row->RoomID) }}">{{ $row->RoomName }}</option>
                        @endforeach
                    </select>
                 </div>
                 <!--/span-->
            </div>
             <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Club Name</label>
                    <input type="text" class="form-control" name="Club" id="Club" value="{{ getObjectValue($data,'ClubName') }}">
                 </div>
                 <!--/span-->
            </div>
             <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Club Description</label>
                   <input type="text" class="form-control not-required" name="ClubDesc" id="ClubDesc" value="{{ getObjectValue($data,'ClubDescription') }}">
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group filter_wrapper">
                 <div class="col-md-12">
                    <label class="control-label">Moderator</label>
                   
                   <select class="form-control select2" name="moderator" id="moderator">
                        <option value="">Select...</option>
                        @foreach(App\Modules\Academics\Models\EmpFaculty::GetAllactive() as $row)
                        <option {{ isSel(getObjectValue($data,'ModeratorID'),$row->EmployeeID) }} value="{{ encode($row->EmployeeID) }}">{{ trimmed($row->LastName) . ', ' . trimmed($row->FirstName) }}</option>
                        @endforeach
                    </select>
                    
                 </div>
                 <!--/span-->
            </div>
            
        </div>
    </div>
</form>