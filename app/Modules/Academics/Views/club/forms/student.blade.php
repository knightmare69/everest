<div class="col-md-5 center" style="width: 51%">
	<img id="StudentPhoto" src="{{ url('general/getStudentPhotoByStudentNo?StudentNo=') }}" width="200" width="200">
</div>
<div class="col-md-7 center" style="width: 47%;padding-left:0;">
	<form class="form">
		<table class="table sm-font" id="TableFilterResult" style="height: 200px;margin-bottom: 0px">
			<tr>
				<td width="60%" class="bold bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important;width: 30% !important;">
					Student No.:
				</td>
				<td id="StudentNo" width="40%" class="bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					0001
				</td>
			</tr>
			<tr>
				<td class="bold bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					Name:
				</td>
				<td id="Name" class="bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					Juan Dela Cruz
				</td>
			</tr>
			<tr>
				<td class="bold bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					Gender:
				</td>
				<td class="bold bg-grey-cararra left" id="Gender" style="border-bottom: 1px solid #bfbfbf  !important">
					Male
				</td>
			</tr>
			<tr>
				<td class="bold bg-grey-cararra left" style="border-bottom: 1px solid #bfbfbf  !important">
					Birth Date:
				</td>
				<td class="bold bg-grey-cararra left" id="BirthDate" style="border-bottom: 1px solid #bfbfbf  !important">
					Male
				</td>
			</tr>
		</table>
	</form>
</div>