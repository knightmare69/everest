<form class="horizontal-form" id="FormFilter" action="#" method="POST">
    <div class="row">
        <div class="form-group">
            <div class="col-md-6">
                <label class="control-label">
                   <small id="SchoolYearTitle"><u> School Year</u></small>&nbsp;/&nbsp;<small id="ProgramTitle"><u>Program</u></small>
                </label>
             </div>
             <!--/span-->
             <div class="col-md-6 right">
                <label class="control-label sm-font cursor-pointer showFilter">
                    <input type="checkbox" class="form-control showFilter" id="isShowFilter" checked>
                    Show Filter
                </label>
             </div>
             <!--/span-->
        </div>
        <div class="form-group filter_wrapper">
             <div class="col-md-12">
                <label class="control-label">Campus</label>
                <select class="form-control filter" name="SchoolCampus" id="SchoolCampus">
                    <option value="">Select...</option>
                    @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                    <option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                    @endforeach
                </select>
             </div>
             <!--/span-->
        </div>
        <div class="form-group filter_wrapper">
             <div class="col-md-12">
                <label class="control-label">School Year</label>
                <?php $i = 0; ?>
                <select class="form-control filter" name="SchoolYear" id="SchoolYear">
                    <option value="">Select...</option>
                    @foreach(App\Modules\Admission\Models\Admission::getYearTerm() as $row)
                    <option  {{  $i++ <= 0 ? 'selected' : '' }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear }}</option>
                    @endforeach
                </select>
             </div>
             <!--/span-->
        </div>
        <div class="form-group filter_wrapper">
             <div class="col-md-12">
                <label class="control-label">Program</label>
                <select class="form-control not-required" name="Program" id="Program">
                    <option value="">Select...</option>
                </select>
             </div>
             <!--/span-->
        </div>
        <div class="form-group filter_wrapper">
             <div class="col-md-12 right margin-top-10">
                  <label class="control-label">&nbsp;</label>
                <button class="btn btn-primary btnSearch" type="button"><i class="fa fa-search"></i> Search</button>
             </div>
             <!--/span-->
        </div>
    </div>
</form>