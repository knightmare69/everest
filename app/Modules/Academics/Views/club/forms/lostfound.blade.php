<?php 
    $data = []; 
    if (Request::get('AttendanceKey')) {
        $data = App\Modules\Registrar\Models\Attendance\Attendance::where('fldPK',Request::get('AttendanceKey'))->where('Type','4')->first();
    }
?>
<form class="horizontal-form" id="FormLostFound" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Quarter</label>
                    <select class="form-control" name="Period">
                        <option value="">- SELECT -</option>
                        @foreach(App\Modules\Registrar\Models\Attendance\Quarter::get() as $row)
                        <option {{ getObjectValue($data,'PeriodID') == $row->PeriodID ? 'selected' : '' }} value="{{ encode($row->PeriodID) }}">{{ $row->Description2 }}</option>
                        @endforeach
                    </select>
                 </div>
            </div>
             <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Date Occured: </label>
                    <input type="text" placeholder="Date" class="form-control input-xs date-picker" name="Date" id="Date" value="{{ setDate(getObjectValue($data,'Date'),'m/d/Y') }}">
                 </div>
            </div>
             <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Type </label>
                     <select class="form-control" name="Particulars">
                        <option value="LOST">LOST</option>
                        <option value="FOUND">FOUND</option>
                    </select>
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label bold">Merit </label>
                    <input type="text" placeholder="0"  class="form-control input-xs numberonly" name="Merit" id="Merit" value="{{ getObjectValue($data,'Merit') }}">
                 </div>
                 <!--/span-->
                 <div class="col-md-6">
                    <label class="control-label bold">Demerit </label>
                    <input type="text" placeholder="0"  class="form-control input-xs numberonly" name="Demerit" id="Demerit" value="{{ getObjectValue($data,'Demerit') }}">
                 </div>
                 <!--/span-->
            </div>
            <div class="form-group">
                 <div class="col-md-6">
                    <label class="control-label bold">Item/s </label>
                    <input type="text" placeholder="0"  class="form-control input-xs numberonly" name="Remarks" id="Remarks" value="{{ getObjectValue($data,'Remarks') }}">
                 </div>
                 <!--/span-->
            </div>
        </div>
    </div>
</form>