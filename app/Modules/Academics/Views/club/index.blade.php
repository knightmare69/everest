<style>

	#TableMasterList > tbody > tr:hover, .row-selected {
		background-color: #e08283 !important;
		color: #fff !important;
	}

	.cs-row-color {
		background-color: #e7e7e7  !important;
	}

	.cs-row-color:hover {
		background-color: #fff  !important;
	}
	.cs-table-no-border {
		border-top: 0 none !important;
	}

	.cs-tbody-tr-no-border td {
		border: 0px none !important;
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
	}

	.cs-tbody-tr-border-bottom {
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
		/*border-bottom: 1px solid #e5e5e5 !important;*/
	}

	.no-border-top {
		border-top: 0px none !important;
	}

	.cs-td-valign {
		vertical-align: middle !important;
	}

	.cs-input-xs {
		width: 60px !important;
	}
	#TableWithoutClubStudents tbody tr, #TableWithClubStudents tbody tr{
		cursor: pointer;
	}
</style>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption"> <i class="fa fa-sitemap"></i> Club Organization Management </div>
		<div class="actions btn-set">
			<button class="btn btn-primary" id="BtnCreate"><i class="fa fa-file"></i> Create Club</button>
			<button class="btn red" id="BtnDelete"><i class="fa fa-trash"></i> Delete Club</button>
			<button class="btn grey" id="BtnReset"><i class="fa fa-history"></i> Reset</button>
			<div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn yellow dropdown-toggle">
				<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#">
						Print </a>
					</li>
					<li class="devider"></li>
					<li>
						<a href="#">
						Export to CSV </a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="portlet-body clearfix">
        <div class="row">
            <div class="col-md-4">
    			@include($views.'sub.forms')
    		</div>
    		<div class="col-md-8" style="padding-left: 0;padding: right: 0">
    			@include($views.'sub.masterlist')
    		</div>
        </div>
		
	</div>
</div>