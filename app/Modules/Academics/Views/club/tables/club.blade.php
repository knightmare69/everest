<?php
	$data = [];

	if (Request::get('Program') && Request::get('SchoolYear') && Request::get('SchoolCampus')) {
		$data = DB::table('ES_ClubOrganizations as r')
					->select([
							'ClubID',
							'ClubName',
						])
					->where('r.ProgID',Request::get('Program'))
					->where('r.TermID',decode(Request::get('SchoolYear')))
					->where('r.CampusID',decode(Request::get('SchoolCampus')))
					->orderBy('r.ClubName','asc')
					->get();
	}
?>
<table class="table table-hover" id="TableMasterList">
	<thead>
		<tr>
			<th width="15%" class="sm-font upper-letter left cs-td-valign">Club</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; ?>
		@foreach($data as $row)
		<tr class="center cursor-pointer clubRowSel {{ decode(Request::get('ClubID')) == $row->ClubID ? 'row-selected' : '' }}" data-clubid="{{ encode($row->ClubID) }}" title="click to select">
			<td class="left"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ $row->ClubName }}</td>
		</tr>
		@endforeach
		@if(count($data) > 0)
		<tr>
			<td class="left">Total records(s): <b>({{ count($data) }})</b></td>
		</tr>
		@endif
	</tbody>
	@if(count($data) <= 0)
	<tfoot>
		<tr>
			<td class="center bold">No Record found.</td>
		</tr>
	</tfoot>
	@endif
</table>