<table class="table table-hover" id="TableWithoutClubStudents">
	<thead>
		<tr role="row" class="filter">
			<td colspan="2">
				<input type="text" class="form-control form-filter" id="StudentNo" name="StudentNo" placeholder="Student No">
			</td>
			<td>
				<input type="text" class="form-control form-filter" id="StudentName" name="StudentName" placeholder="Student Name">
			</td>
			<td>
				<div class="margin-bottom-5">
					<button class="btn btn-sm yellow filter-submit margin-bottom btn-sm" id="btnFilter"><i class="fa fa-search"></i> Search</button>
				</div>
			</td>
		</tr>
		<tr>
			<th width="2%" class="center">&nbsp;</th>
			<th width="8%" class="sm-font upper-letter cs-td-valign" width="50">Student No</th>
			<th width="20%" class="sm-font upper-letter cs-td-valign left" width="50">Full Name</th>
			<th width="5%" class="sm-font upper-letter cs-td-valign">Sex</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>