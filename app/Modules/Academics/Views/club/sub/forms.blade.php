<div class="portlet light bg-grey-steel" style="padding: 10px;margin-bottom: 10px">
	<div class="portlet-body">
		<div class="row">
			@include($views.'forms.student')
		</div>
	</div>
</div>
<div class="portlet light bg-inverse">
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				@include($views.'forms.filter')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" id='TableClubWrapper' style="height: 400px; overflow: auto;">
				@include($views.'tables.club')
			</div>
		</div>
	</div>
</div>