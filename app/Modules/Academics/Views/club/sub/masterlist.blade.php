<div class="col-md-6">
	<div class="portlet box grey-cascade">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-file"></i> List of Students without Clubs
			</div>
			<div class="actions btn-set">
				<button class="btn btn-success" id="btnMoveToClub"><i class="fa fa-arrow-right"></i> Move To Club</button>
			</div>
		</div>
		<div class="portlet-body" id="TableWithoutClubsWrapper">
			@include($views.'tables.withoutclubs')
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="portlet box grey-cascade">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-file"></i> List of Students in this club
			</div>
			<div class="actions btn-set">
				<button class="btn btn-success" id="btnRmoveFromClub"><i class="fa fa-times"></i> Remove From Club</button>
			</div>
		</div>
		<div class="portlet-body" id="TableWithClubsWrapper">
			@include($views.'tables.withclubs')
		</div>
	</div>
</div>