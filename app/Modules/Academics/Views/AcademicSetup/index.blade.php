<div class="row">
    <div class="col-md-6">
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-pencil-square-o"></i> Online Enrollment Setup </div>
                <div class="actions">            
        			<a id="save-config" href="#" class="btn btn-default btn-sm"><i class="fa fa-save"></i> Save </a>
        		</div>                            
            </div>
            <div class="portlet-body form form_wrapper">
                @include($views.'shs.setup')           
            </div>
        </div>
    </div>
</div>