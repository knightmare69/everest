<div class="portlet box blue-hoki">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-pencil-square-o"></i> SHS Faculty Schedule </div>
    </div>
  <div class="portlet-body form form_wrapper">
        <form class="horizontal-form" id="shs-publishform" action="#" method="POST">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Academic Term</label>
                            <select class="select2 form-control" name="academic-term" id="academic-term">
                                @if(!empty($at))
                                    @foreach($at as $_this)
                                    <option value="{{ encode($_this->TermID) }}">{{ $_this->AcademicYear.' - '.$_this->SchoolTerm }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group ">
                            <label class="control-label">Put check to allow the following</label>
                            <div class="checkbox-list ">
        				        <label class=""><input id="clsrcd" class="clsrcd" value="0" type="checkbox"> Allowed Class Record Encoding </label>                                
           					</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions right">
                <!-- <button class="btn default btn_reset" type="button">Reset</button> -->
                <button class="btn btn-default" type="button" id="save-config"><i class="fa fa-save"></i> Save </button>
            </div>
        </form>
    </div>
</div>