<?php
$service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
?>
<form class="horizontal-form" id="shs-publishform" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Academic Term</label>
                    <select class="select2 form-control" name="academic-term" id="academic-term">
                        @if(!empty($at))
                            @foreach($at as $r)
                            <option data-active="<?= $r->Active_OnlineEnrolment ?>" value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="checkbox" id="cActive" name="cActive" /> Active Enrollment
        </div>
        <h4 class="form-section">Students Enrollment Dates</h4>        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Start Date</label>
                    <input class="form-control datepicker" id="startdate" name="startdate" value="" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">End Date</label>
                    <input class="form-control datepicker" id="finishdate" name="finishdate" value="" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Allowed to Enroll</label>
                    <table id="tbltrack" class="table table-bordered table-condensed">
                        @foreach($service->GetAcademicTrack() as $at)                        
                            <tr>
                                <td class="autofit"><input  data-id="{{encode($at->IndexID )}}" type="checkbox" class="chk-child" ></td>
                                <td>{{ $at->MajorDiscDesc }}</td>
                            </tr>                        
                        @endforeach                                             
                    </table>
                </div>
            </div>           
        </div>
                          
        <h4 class="form-section">Faculty Schedules</h4>
         <div class="row">
            <div class="col-md-12" >
                <div class="form-group ">
                    <label class="control-label">Put check to allow the following</label>
                    <div class="checkbox-list ">
				        <label class=""><input id="clsrcd" class="clsrcd" value="0" type="checkbox"> Allowed eClass Record Encoding </label>                                
   					</div>
                </div>
            </div>
        </div>
                
    </div>          
</form>