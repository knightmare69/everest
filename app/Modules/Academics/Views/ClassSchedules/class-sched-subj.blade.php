<?php
// error_print($data);
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="subj-records-table">
    <thead>
        <tr>
            <th>Subject Code</th>
            <th>Subject Title</th>
            <th>Schedule</th>
            <th class="text-center autofit">Boy/Girl</th>
            <th >Teacher 1</th>
            <th >Rank</th>
            <th >Teacher 2</th>
            <th >Rank</th>

        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $_this)
        <tr data-id="{{ encode($_this->ScheduleID) }}" data-type="sched" >
            <td>{{ $_this->SubjCode }}</td>
            <td>{{ $_this->SubjTitle }}</td>
            <td>{{ $_this->Sched_1 }}</td>
            <td class="text-center"><label><input {{ $_this->FacultybyGender ? 'checked="checked"' :'' }} type="checkbox" class="check-gender"  data-gender="{{ $_this->FacultybyGender }}" ></label> </td>
            <td>                
                <a class="faculty-search-btn" data-type="1" data-for="teacher" data-id="{{ encode($_this->FacultyID) }}" href="javascript:void(0);"  >
                    {{ $_this->FacultyName =='' ?  '[Change Teacher]' :  $_this->FacultyName  }}
                </a>
                <i class="faculty-search-cancel-btn pull-right text-danger btn fa fa-times"></i>                                          
            </td>
            <td>{{ $_this->FacultyRank }}</td>
            <td>                
                <a class="faculty-search-btn" data-type="2" data-for="teacher" data-id="{{ encode($_this->FacultyID_2) }}" href="javascript:void(0);"  >
                    {{ $_this->FacultyName2 =='' ?  '[Change Teacher]' :  $_this->FacultyName2  }}
                </a>
                <i class="faculty-search-cancel-btn pull-right text-danger btn fa fa-times"></i>                                          
            </td>
            <td>{{ $_this->FacultyRank2 }}</td>        
        </tr>
        @endforeach
        @endif
    </tbody>
</table>
