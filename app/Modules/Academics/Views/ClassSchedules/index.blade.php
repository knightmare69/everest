<?php $i=0; ?>
<style media="screen">
    .table .btn{
        margin-right: 0;
    }
    .faculty-search-cancel-btn i.fa-times{
        color: #f3565d;
    }
    #records-table_filter{
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-users"></i> Class Sections</div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="portlet-body table_wrapper" id="sub-ar-result-holder">
            <div class="form-inline">
                        <select class="select2 form-control input-small " name="campus" id="campus">
                            <!-- <option value="">Nothing to show.</option> -->
                            @if(!empty($incl['campus']))
                                @foreach($incl['campus'] as $campus)
                                <option value="{{ encode($campus->CampusID) }}">{{ $campus->Acronym }}</option>
                                @endforeach
                            @else
                            <option value="">No campus available.</option>
                            @endif
                        </select>
                        <select class="select2 form-control" name="academic-year" id="academic-year">
                            @if(!empty($incl['ac']))
                                @foreach($incl['ac'] as $ac)
                                <option value="{{ encode($ac->TermID) }}">{{ $ac->AcademicYear.' - '.$ac->SchoolTerm }}</option>
                                @endforeach
                            @else
                            <option value="">No academic year available.</option>
                            @endif
                        </select>
                        <button type="button" class="btn btn-default btn-refresh " ><i class="fa fa-refresh"></i> Refresh </button>
                    </div>
                    
                <hr />
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#class-section" data-toggle="tab">Class Sections</a>
                        </li>
                        <li class="hide"><a href="#class-section-sched" data-toggle="tab">None</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="class-section">
                            @include($views.'class-sched')
                        </div>
                        <div class="tab-pane fade" id="class-section-sched"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>