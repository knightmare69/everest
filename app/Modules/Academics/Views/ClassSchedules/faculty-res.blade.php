<?php //error_print($faculty) ?>
@if(!empty($faculty))
<table class="table table-striped table-bordered table-hover table_scroll dataTable no-footer" id="table-faculty-res" data-key="{{ $key }}" data-type="{{ $type }}">
    <thead>
        <tr>
            <td>Name</td>
            <td>Position</td>
            <td width="20%">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($faculty as $_this)
        <tr>
            <td>
                <p>{{ ucwords(strtolower($_this->FacultyName)) }}</p>
            </td>
            <td><p>{{ $_this->FacultyRank }}</p></td>
            <td width="20%" class="text-center">
                <button data-id="{{ encode($_this->EmployeeID) }}" class="btn green btn-sm btn-faculty-select-btn">
                    <i class="fa fa-check"></i> Select
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
