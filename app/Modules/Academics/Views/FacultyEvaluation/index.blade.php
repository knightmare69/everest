<div class="row">
<div class="col-sm-12">
	<div class="portlet light">
		<div class="tab-content">
			<div class="tab-pane active" id="tblist">
			   <div class="row">
				<div class="col-sm-12">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-table"></i>Summary of Faculty Evaluation
							</div>
							<div class="tools">
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row" style="margin-bottom:10px;">
							   <div class="col-sm-3">
								 <select name="term" id="term" class="form-control">
                                    <option value="-1" selected disabled>Academic Year & Term</option>
								 </select>
							   </div>
							   <div class="col-sm-3">
								<button class="btn btn-default btnrefresh"><i class="fa fa-refresh"></i> Refresh</button>
								<button class="btn btn-warning btnprint"><i class="fa fa-print"></i> Print</button>
							   </div>
							   <div class="col-sm-3">
								 <select name="college" id="college" class="form-control">
                                    <option value="-1" selected disabled>College</option>
								 </select>
							   </div>	 
							   <div class="col-sm-3">
							     <input type="text" class="form-control"/>
							   </div>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered table-condensed">
									<thead>
										<th class="text-center">Action</th>
										<th class="text-center">Faculty Name</th>
										<th class="text-center">Department</th>
										<th class="text-center">College</th>
										<th class="text-center">Respondent</th>
										<th class="text-center">Overall</th>
										<th class="text-center">Descriptive</th>
									</thead>
									<tbody>@include($views.'tbsummary')</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>	
			   </div>
			</div>
			
			<div class="tab-pane hidden" id="tbform">
			   <div class="row">
				<div class="col-sm-12">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i> Faculty Evaluation:<b class="lblname"></b> 
							</div>
							<div class="tools">
							</div>
							<div class="actions">
								<button class="btn btn-info btnprint"><i class="fa fa-print"></i> Print</button>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								
							</div>
						</div>
					</div>
				</div>	
			   </div>
			</div>
			
		</div>
	</div>
</div>
</div>