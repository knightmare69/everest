<div class="row">
<div class="col-sm-12">
	<div class="portlet light">
		<div class="tab-content">
			<div class="tab-pane active" id="tblist">
			   <div class="row">
				<div class="col-sm-12">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-table"></i>List Of Faculty
							</div>
							<div class="tools">
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row" style="margin-bottom:10px;">
							   <div class="col-sm-3">
								 <select name="term" id="term" class="form-control">
                                    <option value="-1" selected disabled>Academic Year & Term</option>
								 </select>
							   </div>
							   <div class="col-sm-3">
								<button class="btn btn-default btnrefresh"><i class="fa fa-refresh"></i> Refresh</button>
								<button class="btn btn-warning btnprint"><i class="fa fa-print"></i> Print</button>
							   </div>
							</div>   
							<div class="row" style="margin-bottom:10px;">
								<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-condensed">
										<thead>
											<th></th>
											<th class="text-center">Subject</th>
											<th class="text-center">Faculty Name</th>
											<th class="text-center" width="10px;">Evaluated</th>
										</thead>
									</table>
								</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
               </div>
			</div>
		</div>
	</div>
</div>

</div>	