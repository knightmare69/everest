<?php
 if(isset($summary) && count($summary)>0){
    foreach($summary as $rs){
	  echo '<tr data-emp="'.$rs->FacultyID.'" data-college="'.$rs->CollegeID.'">
				<td><button class="btn btn-sm btn-info btnview"><i class="fa fa-file"></i></button>
					<button class="btn btn-sm btn-warning btnprint"><i class="fa fa-print"></i></button></td>
				<td>'.$rs->Faculty.'</td>	
				<td class="text-center">'.$rs->DeptName.'</td>	
				<td class="text-center">'.$rs->CollegeName.'</td>	
				<td class="text-center">'.$rs->Respondent.'</td>	
				<td class="text-center">'.$rs->Overall.'</td>	
				<td class="text-center"></td>	
	        </tr>';
	}
 }else{
	echo '<tr><td colspan="7" class="text-center"><i class="fa fa-warning text-warning"></i> No Data Available.</td></tr>';
 }
?>