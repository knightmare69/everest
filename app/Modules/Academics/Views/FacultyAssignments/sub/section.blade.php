<?php
    $programs = [5, 1,8,7];
    $service = new App\Modules\Academics\Services\FacultyAssignServiceProvider;
    $rooms = App\Modules\Academics\Models\Club\Rooms::selectRaw(" *, dbo.fn_GetBuildingNameByRoom(RoomID) As Bldg ")->where('IsUsable','1')->orderBy('Bldg')->get();
    $i = 0;
    $tmp = '';

    function isSel($a,$b) {
        return $a == $b ? 'selected' : '';
    }
?>
<form class="horizontal-form" id="form_section" action="#" method="POST">
    <input type="hidden" name="term" value="<?= (Request::get('term')) ?>" />
    <input type="hidden" name="yearlevel" id="yearlevel" value="" />

    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Academic Program</label>
                    <select class="form-control " name="program" id="program">
                        <option value=""> - Select -</option>
                        @foreach(App\Modules\Setup\Models\Programs::whereIn('ProgID', $programs)->orderBy('ProgName','ASC')->get() as $row)
                        <option value="{{ encode($row->ProgID) }}">{{ $row->ProgName }}</option>
                        @endforeach
                    </select>
                </div>
             </div>
             <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Curriculum</label>
                    <select class="form-control select2 " name="curriculum" id="curriculum" disabled>
                        <option value=""> - Select - </option>
                        @foreach($programs as $p)
                            @foreach($service->curriculum($p) as $row)
                            <option value="{{ encode($row->IndexID) }}" data-prog="{{ encode($p) }}">{{ $row->CurriculumCode }}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
             </div>
             <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Year Term</label>
                    <select class="form-control select2 " name="yearterm" id="yearterm">
                        <option value=""> - Select - </option>
                    </select>
                </div>
             </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="subjectlist"></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group ">
                    <label class="control-label">Section Name</label>
                    <input type="text" class="form-control" name="section" id="section" value="">
                </div>
             </div>
             <div class="col-md-2">
                <div class="form-group ">
                    <label class="control-label">Limit</label>
                    <input type="text" class="form-control " name="limit" id="limit" value="40">
                </div>
             </div>
              <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Adviser</label>
                    <select class="form-control select2" name="adviser" id="adviser">
                        <option value="">- Select -</option>
                        @foreach(App\Modules\Academics\Models\EmpFaculty::GetAllactive() as $row)
                        <option value="{{ encode($row->EmployeeID) }}">{{ strtoupper(trimmed($row->LastName)) . ', ' . trimmed($row->FirstName) }}</option>
                        @endforeach
                    </select>
                </div>
             </div>
             <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Room</label>
                    <select class="form-control bootstrap-select do-not-clear not-required" name="room" id="room">
                        <option value="">Select...</option>
                        @foreach($rooms as $row)

                            <option value="{{ encode($row->RoomID) }}">{{ $row->Bldg . ' - ' .$row->RoomNo }}</option>
                        @endforeach
                    </select>
                </div>
             </div>

        </div>
    </div>
</form>
