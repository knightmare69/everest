<div class="alert alert-danger">
    <i class="fa fa-warning"></i> Conflict Found!
</div>
<table class="table table-condensed table-bordered">
    <thead>
        <th>Section</th>
        <th>Code</th>
        <th>Subject Title</th>
        <th>Schedule</th>
    </thead>
    <tbody>
        
         @foreach($data As $r )
         <tr class="danger">
            <td>{{$r->SectionName}}</td>
            <td>{{$r->SubjectCode}}</td>
            <td>{{$r->SubjectTitle}}</td>
            <td>
                
                                                                          
                    {{$r->Sched_1 or ''}}  {{ $r->Room_1 }}
                    @if( $r->Sched_2 != '' )
                    <br /> {{ $r->Sched_2 or ''}}  {{ $r->Room_2 }}
                    @endif 
                     @if( $r->Sched_3 != '')
                    <br /> {{ $r->Sched_3 or ''}} {{ $r->Room_3 }}
                    @endif
                    
            </td>
         </tr>                            
         @endforeach
    </tbody>
</table>