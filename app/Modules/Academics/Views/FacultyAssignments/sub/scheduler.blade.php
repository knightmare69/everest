<?php
    
    $service = new App\Modules\Academics\Services\FacultyAssignServiceProvider;
    $rooms = App\Modules\Academics\Models\Club\Rooms::from('ES_Rooms as r')
        ->selectRaw(" r.*, Case when b.Acronym = '' Then b.BldgOtherName Else b.Acronym END As Bldg ")
        ->leftJoin('ES_Buildings As b','b.BldgID','=','r.BldgID')
        ->where('IsUsable','1')
        ->orderBy('Bldg')
        ->get();
    
    $rs = $service->getSchedules($sched);
    
    $i = 0;
    $tmp = '';
    
    function isSel($a,$b) {
        return $a == $b ? 'selected' : '';
    }
    
    $scheds = array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
    );
    
    
?>
<form class="horizontal-form" id="form_schedule" action="#" method="POST">
    <input type="hidden" name="sched" value="<?= (Request::get('sched')) ?>" />        
    <div class="form-body">           
        
        <div class="row">        
            <div class="col-md-12" >
                <div class="form-group ">            
                    <label class="control-label">Weekday</label>
                    <div class="checkbox-list ">
				        <label class="checkbox-inline"><input id="day1" data-code="Su" class="schedays" value="1" type="checkbox"> Sunday </label>
                        <label class="checkbox-inline"><input id="day2" data-code="M" class="schedays" value="2" type="checkbox"> Monday </label>
                        <label class="checkbox-inline"><input id="day3" data-code="T" class="schedays" value="3" type="checkbox"> Tuesday </label>
                        <label class="checkbox-inline"><input id="day4" data-code="W" class="schedays" value="4" type="checkbox"> Wednesday </label>
                        <label class="checkbox-inline"><input id="day5" data-code="Th" class="schedays" value="5" type="checkbox"> Thursday </label>
                        <label class="checkbox-inline"><input id="day6" data-code="F" class="schedays" value="6" type="checkbox"> Friday </label>
                        <label class="checkbox-inline"><input id="day7" data-code="S" class="schedays" value="7" type="checkbox"> Saturday </label>     
   					</div>
                </div>                                
            </div> 
        </div>
        <div class="row">        
            <div class="col-md-2" >
                <div class="form-group ">            
                    <label class="control-label">Time Start </label>
                    <input type="text" class="form-control timepicker timepicker-no-seconds" name="timestart" id="timestart" value="">                            
                </div>                                
            </div> 
            <div class="col-md-2" >
                <div class="form-group ">            
                    <label class="control-label">Time End</label>
                    <input type="text" class="form-control timepicker timepicker-no-seconds" name="timend" id="timend" value="">                            
                </div>                                
            </div> 
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Room</label>
                    <select class="form-control select2me select2-offscreen do-not-clear not-required" name="room" id="room">
                        <option value="">Select...</option>                        
                        @foreach($rooms as $row)                                                       
                            <option value="{{ encode($row->RoomID) }}">{{ $row->Bldg . ' - ' .$row->RoomNo }}</option>
                        @endforeach
                    </select>
                    
                </div>
            </div>    
             <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Faculty</label>
                   <select class="form-control select2me select2-offscreen" name="teacher" id="teacher">
                        <option value="">- Select -</option>
                        @foreach(App\Modules\Academics\Models\EmpFaculty::GetAllactive() as $row)
                        <option value="{{ encode($row->EmployeeID) }}">{{ strtoupper(trimmed($row->LastName)) . ', ' . trimmed($row->FirstName) }}</option>
                        @endforeach
                    </select>
                    
                    
                </div>
            </div>    
                    
        </div> 
        <div class="text-center">
            <button type="button" class="btn btn-xs btn-primary hide" ><i class="fa fa-save"></i> Save Schedule </button>
        </div>                
        <hr style="margin-top: 10px; margin-bottom: 10px;" />                                           
        <div class="row">
            <div class="col-md-12" >
                <table id="schedlist" class="table table-bordered table-condensed table-hover" data-id="{{ encode($sched) }}" >
                    <thead>
                        <tr>                            
                            <th>Mode</th>
                            <th>Schedule</th>
                            <th>Room</th>
                            <th>Faculty</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($scheds as $r)
                        <tr data-id="{{$r}}" 
                            data-days="<?= getObjectValue($rs,'Days'.$r) ?>"
                            data-stime="<?= getObjectValue($rs,'Time'.$r.'_Start') ?>" 
                            data-etime="<?= getObjectValue($rs,'Time'.$r.'_End') ?>"
                            data-room="<?= encode(getObjectValue($rs,'Room'.$r.'_ID')) ?>"
                            data-faculty="<?= encode(getObjectValue($rs,'FacultyID_'.$r)) ?>" 
                            data-sched="<?= (getObjectValue($rs,'Sched_'.$r)) ?>"
                            data-section="{{encode($rs->SectionID)}}"
                            >                            
                            <td class="autofit">Schedule {{$r}}</td>
                            <td class="schedule"><?= getObjectValue($rs,'Sched_'.$r) ?></td>
                            <td class="room"><?= getObjectValue($rs,'Room'.$r) ?></td>
                            <td class="teacher" ><?= getObjectValue($rs,'FacultyName'.$r) ?></td>
                            <td class="autofit">
                                <a class="update-sched" href="javascript:void(0);"> Update </a> 
                            </td>
                        </tr>
                        @endforeach                                                
                    </tbody>
                </table>
            
            </div>
        </div>
        <div class="row">
         <div class="col-md-12">
                <div class="form-group">                    
                    <div id="tblconflict">
                        
                    </div>                                                            
                </div>
            </div>
            </div>                            
    </div>
</form>