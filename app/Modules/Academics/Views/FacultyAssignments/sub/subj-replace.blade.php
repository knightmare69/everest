<?php $c = ($action == 'replace') ? 'subj-replace-btn' : 'subj-add-btn'; ?>
<table class="table table-condensed table-bordered " id="tbl-subjects-replace">
    <thead>
        <tr>
            <th width="80px">Code</th>
            <th>Subject Title</th>
            <th width="61px">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
            @foreach($data as $_this)
            <tr>
                <td>{{ $_this->SubjectCode }}</td>
                <td>{{ $_this->SubjectTitle }}</td>
                <td><button class="btn btn-xs green {{ $c }}" data-subj="{{ encode($_this->SubjectID) }}"><i class="fa fa-share"></i> Select</button></td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
