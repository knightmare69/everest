<?php $i=1; ?>
<table class="table table-condensed table-bordered " id="tblsubjects" >
    <thead>
        <tr>
            <th class="" ><input type="checkbox" class="chk-header"></th>
            <th>#</th>
            <th>Code</th>
            <th>Subject Title</th>
            <th>Units</th>
            
        </tr>
    </thead>
    <tbody>
         @foreach($data as $r)
            <tr data-subj ="{{ encode($r->SubjectID) }}">
            
            <td class="" ><input type="checkbox" class="chk-child" checked="" > </td>
            <td>{{$i}}</td>
            <td>{{ $r->SubjectCode }}</td>
            <td>{{ $r->SubjectTitle }}</td>
            <td>{{ $r->AcadUnits }}</td>
            </tr>          
            <?php $i++; ?>  
         @endforeach
    </tbody>
</table>