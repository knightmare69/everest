<?php $i=0; ?>
<style media="screen">
    .table .btn{
        margin-right: 0;
    }
    .faculty-search-cancel-btn i.fa-times{
        color: #f3565d;
    }
    #records-table_filter{
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil"></i> Faculty Assignments
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body table_wrapper" id="sub-ar-result-holder">
                <div class="form-inline">
                    <select class="select2 form-control input-small " name="campus" id="campus">
                        <!-- <option value="">Nothing to show.</option> -->
                        @if(!empty($incl['campus']))
                            @foreach($incl['campus'] as $campus)
                            <option value="{{ encode($campus->CampusID) }}">{{ $campus->Acronym }}</option>
                            @endforeach
                        @else
                        <option value="">No campus available.</option>
                        @endif
                    </select>
                    <select class="select2 form-control" name="academic-year" id="academic-year">
                        @if(!empty($incl['ac']))
                            @foreach($incl['ac'] as $ac)
                            <option value="{{ encode($ac->TermID) }}" <?php echo ((property_exists($ac,'Active_OnlineEnrolment') && $ac->Active_OnlineEnrolment==1)?'selected':'');?>>{{ $ac->AcademicYear.' - '.$ac->SchoolTerm }}</option>
                            @endforeach
                        @else
                        <option value="">No academic year available.</option>
                        @endif
                    </select>
                    <button type="button" class="btn btn-default btn-refresh " ><i class="fa fa-refresh"></i> Refresh </button>
                    <div class="pull-right">
                        <button type="button" class="btn btn-info new-section " ><i class="fa fa-file"></i> New Section </button>
                        <button type="button" class="btn btn-danger delete-section " ><i class="fa fa-times"></i> Delete Section </button>
                        <div class="btn-group">
            				<a data-toggle="dropdown" href="#" class="btn btn-default" aria-expanded="false">
            				<i class="fa fa-list"></i> Menu <i class="fa fa-angle-down"></i>
            				</a>
            				<ul class="dropdown-menu pull-right">
            					<li><a href="#" class="rename" ><i class="fa fa-exchange "></i> Rename </a></li>
            					<li><a href="#" id="replace-subj" ><i class="fa fa-book "></i> Replace Subject </a></li>
            					<li><a href="#" id="add-subj" ><i class="fa fa-book "></i> Add Subject </a></li>
            					<li><a href="#" id="remove-subj" ><i class="fa fa-book "></i> Remove Subject </a></li>
            					<li><a href="#" id="print-sec-sched" ><i class="fa fa-print "></i> Print Section Schedule </a></li>
            					<li><a href="#" class="faculty-search-btn" data-type="print-sched"><i class="fa fa-print "></i> Print Faculty Schedule </a></li>
            					<li class="divider"></li>
            					<li><a href="#" > <i class="fa fa-times"></i> Cancel </a></li>
            				</ul>
            			</div>
                    </div>
                </div>

                <hr style="margin-top: 10px; margin-bottom: 10px;" />
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#class-section" data-toggle="tab">Class Sections</a>
                        </li>
                        <li class="hide"><a href="#class-section-sched" data-toggle="tab">None</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="class-section"></div>
                        <div class="tab-pane fade" id="class-section-sched"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
