<?php
// error_print($data);
$service = new \App\Modules\Students\Services\StudentProfileServiceProvider;

$i = 1;
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th>#</th>
            
            <th>Section Name</th>            
            <th>Academic Program</th>
            <th>Year Level</th>
            <th>Block</th>
            <th>Limit</th>
            <th width="30%">Adviser Name</th>
            <th class="" >Rank</th>
            
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $_this)
        <?php
            $total = $service->getTotalEnrolledbySection($_this->TermID, $_this->SectionID);                    
        ?>
                
        <tr data-id="{{ encode($_this->SectionID) }}" data-type="adviser" data-teacher="{{ encode($_this->AdviserID) }}" >
            <!-- <td><label><input type="checkbox" class="check-child"></label></td> <td class="autofit">{{$i}}.</td> -->
            <td>{{$i}}</td>
            
            <td><a href="#!" class="sec-name" data-id="{{ encode($_this->SectionID) }}">{{ $_this->SectionName }}</a></td>            
            <td>{{ $_this->Program }}</td>
            <td>{{ $_this->YearLevel }}</td>
            <td>
            @if($_this->IsBlock == '1')
                <i class="fa fa-check"></i>
            @else
                <i class="fa fa-uncheck"></i>
            @endif
            </td>
            <td><a href="javascript:void(0);" class="adjust-limit">{{ $total. '/'. $_this->Limit }}</a></td>
            <td class="">                
                <a class="autofit faculty-search-btn" data-type="1" data-for="adviser" data-id="{{ encode($_this->AdviserID) }}" href="javascript:void(0);"  >
                    {{ $_this->AdviserName =='' ?  '[Change Adviser]' :  $_this->AdviserName  }}                    
                </a>                                 
                <i class="faculty-search-cancel-btn text-danger btn fa fa-times"></i>
            </td>
            <td>{{ $_this->AdviserRank }}</td>
        </tr>
        <?php $i++; ?>
        @endforeach

        @endif
    </tbody>
</table>
