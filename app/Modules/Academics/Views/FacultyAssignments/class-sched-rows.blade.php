<?php $i=1;?>
@foreach($data as $_this)
<tr data-id="{{ encode($_this->ScheduleID) }}" data-type="sched"  >
    <!-- <td><label><input type="checkbox" class="check-child"></label></td> -->
    <td><?= $i ?></td>
    <td><a href="#!" class="sec-name" data-id="{{ $_this->SectionID }}">{{ $_this->SectionName }}</a></td>
    <td>{{ $_this->Limit }}</td>
    <td>{{ $_this->Schedule }}</td>
    <td>{{ $_this->DAYS }}</td>
    <td>
        <?php $disabled = empty($_this->FacultyName) ? '':'disabled'?>
        <div class="input-group">
            <input type="text" class="form-control coordinator" value="{{ $_this->FacultyName }}" data-id="{{ encode($_this->FacultyID) }}" {{ $disabled }}>
            <span class="input-group-btn">
                <button type="button" class="faculty-search-btn btn btn-default" {{ $disabled }}><i class="fa fa-search"></i></button>
                <button type="button" class="faculty-search-cancel-btn btn btn-default text-danger"><i class="fa fa-times"></i></button>
            </span>
        </div>
    </td>
    <td>{{ $_this->FacultyRank }}</td>
</tr>
<?php $i++; ?>
@endforeach
