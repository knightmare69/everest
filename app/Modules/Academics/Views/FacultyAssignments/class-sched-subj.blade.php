<?php
// error_print($data);
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="subj-records-table">
    <thead>
        <tr>
            <th>Subject Code</th>
            <th>Subject Title</th>
            <th>Schedule</th>            
            <th class="text-center autofit">Boy/Girl</th>
            <th >Teacher 1</th>            
            <th >Teacher 2</th>            
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $_this)
        <tr data-id="{{ encode($_this->ScheduleID) }}" data-type="sched" data-section="{{encode($_this->SectionID)}}" >
            <td class="scode">{{ $_this->SubjCode }}</td>
            <td>{{ $_this->SubjTitle }}</td>
            <td>
                <a href="javascript:void(0);" title="Click to change" class="scheduler" >
                @if( empty($_this->Sched_1) )
                    [Add Schedule]
                @else                                                      
                    {{$_this->Sched_1 or ''}}  {{ $_this->Room1 }}
                    @if( $_this->Sched_2 != '' )
                    <br /> {{ $_this->Sched_2 or ''}}  {{ $_this->Room2 }}
                    @endif 
                     @if( $_this->Sched_3 != '')
                    <br /> {{ $_this->Sched_3 or ''}}
                    @endif
                @endif    
                </a> 
            </td>
            <td class="text-center"><label><input {{ $_this->FacultybyGender ? 'checked="checked"' :'' }} type="checkbox" class="check-gender"  data-gender="{{ $_this->FacultybyGender }}" ></label> </td>
            <td class="autofit">                
                <a title="Click to change" class="faculty-search-btn" data-type="1" data-for="teacher" data-id="{{ encode($_this->FacultyID) }}" href="javascript:void(0);"  >
                    {{ $_this->FacultyName =='' ?  '[Change Teacher]' :  $_this->FacultyName  }}
                </a>
                <i title="Click to Remove Teacher 1" class="faculty-search-cancel-btn  text-danger btn fa fa-times" data-type="Teacher 1" data-id="{{ encode($_this->FacultyID) }}"></i>                                          
            </td>
            
            <td class="autofit">                
                <a title="Click to change" class="faculty-search-btn" data-type="2" data-for="teacher" data-id="{{ encode($_this->FacultyID_2) }}" href="javascript:void(0);"  >
                    {{ $_this->FacultyName2 =='' ?  '[Change Teacher]' :  $_this->FacultyName2  }}
                </a>
                <i class="faculty-search-cancel-btn text-danger btn fa fa-times" data-type="Teacher 2" data-id="{{ encode($_this->FacultyID_2) }}" ></i>                                          
            </td>
                    
        </tr>
        @endforeach
        @endif
    </tbody>
</table>
