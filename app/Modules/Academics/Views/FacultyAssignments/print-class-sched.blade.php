<?php
/**
 * Author: LLoyd Ababao
 * Created Date: June 7, 2017
 */

$new_sched = [];
$subj_colors = [];

// hex bg color list for white text
$color_list = ['2874A6', '5D6D7E', '6C3483', 'B03A2E', '27AE60', 'F1C40F', '1F618D', '9B59B6', '#641E16', '21618C', '512E5F', '4D5656', '717D7E', '1E8449', '3498DB', 'EB984E'];

// hex bg color list with black text
$color_list2 = ['F1C40F'];

$days = ['Su' => 'Sunday', 'M' => 'Monday', 'T' => 'Tuesday', 'W' => 'Wednesday', 'Th' => 'Thursday', 'F' => 'Friday', 'S' => 'Saturday'];

// error_print($schedules); die();

// error_print($section_details);
foreach ($schedules as $k => $v) {
    $scheds = [
        'Scheds' => array_filter([$v->Sched_1, $v->Sched_2, $v->Sched_3, $v->Sched_4, $v->Sched_5]),
        'Rooms' => array_filter([$v->Room1, $v->Room2, $v->Room3, $v->Room4, $v->Room5])
    ];

    for ($a = 0; $a < count($scheds['Scheds']); $a++) {
        $room = !empty($scheds['Rooms'][$a]) ? $scheds['Rooms'][$a] : '';
        $xpl = explode(' ', $scheds['Scheds'][$a], 2);
        $xpl_time = explode(' - ', $xpl[1]);
        $xpl_days = preg_split('/(?=[A-Z])/', $xpl[0], -1, PREG_SPLIT_NO_EMPTY);

        for ($b = 0; $b < count($xpl_days); $b++) {
            $d = $xpl_days[$b];
            $new_sched[$d][$v->ScheduleID] = [
                'ScheduleID' => $v->ScheduleID,
                'SectionID' => $v->SectionID,
                'SubjCode' => $v->SubjCode,
                'SubjTitle' => $v->SubjTitle,
                'Room' => $room,
                'Day' => $d,
                'StartTime' => $xpl_time[0],
                'EndTime' => $xpl_time[1],
                'Faculty1' => $v->FacultyName,
                'Faculty2' => $v->FacultyName2,
            ];
        }

        usort($new_sched[$d], function($ia, $ib) {
          $ad = strtotime($ia['StartTime']);
          $bd = strtotime($ib['StartTime']);

          if ($ad == $bd) {
            return 0;
          }

          return $ad < $bd ? -1 : 1;
        });

        if(empty($subj_colors[$v->SubjCode])){
            // $subj_colors[$v->SubjCode] = '#'.str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $subj_colors[$v->SubjCode] = $color_list[$k];
        }
    }
}

$last_time = '07:00 AM';
$interval = '10 minutes';
$times = [0 => $last_time];

$new_sched2 = [];

while ($last_time != '07:00 PM') {
    $times[] = date('h:i A', strtotime($last_time.' + '.$interval));
    $last_time = date('h:i A', strtotime($last_time.' + '.$interval));
}

$stack_scheds = [];
?>
<style>
    .tbl-data th.with-border{
        border: 1px solid #333;
        font-size: 10px;
    }
    .tbl-data td{
        border-bottom: 1px solid #333;
        font-size: 10px;
    }
    .first-td{
        border-right: 1px solid #333;
        border-left: 1px solid #333;
    }
    .last-td{
        border-right: 1px solid #333;
    }
</style>
{!! $header !!}
<table class="tbl-data" cellpadding="4">
    <thead>
        <tr>
            <th align="center" style="width:67.5px;" class="with-border">Time & Days</th>
            @foreach($days as $d)
            <th align="center" class="with-border">{{ $d }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($times as $tk => $t)
        <tr nobr="true" style="line-height: 10%;">
            @if(count($times) == ($tk + 1))
            <td align="center" class="first-td" height="5">{{ $t }}</td>
            @elseif($tk % 3 == 0)
            <td align="center" class="first-td" rowspan="3" height="5">{!! str_repeat("<br>", 10) !!}{{ $t }}</td>
            @endif
            @foreach($days as $k => $v)
                @if(!empty($new_sched[$k]))
                    <?php $has_sched = 0; $rows_pan = 0; $cnt = 0;?>

                    @if(!empty($stack_scheds[$k]['skip_until']))
                        @if($stack_scheds[$k]['skip_until'] != $t)
                        <?php $has_sched = 1;?>
                        @endif
                    @endif

                    @if($has_sched != 1)

                        @foreach($new_sched[$k] as $nsk => $ns)
                            @if( (strtotime($t) >= strtotime($ns['StartTime']) && strtotime($t) <= strtotime($ns['EndTime'])) )

                                @for($a = $tk; strtotime($times[$a]) <= strtotime($ns['EndTime']); $a++)
                                    <?php $rows_pan++; ?>
                                @endfor

                                <?php $brs = str_repeat('<br>', round($rows_pan / 2)); ?>

                                <td rowspan="{{ $rows_pan }}" valign="middle" align="center" class="first-td"  height="5" style="
                                    background-color: #{{ $subj_colors[$ns['SubjCode']] }};
                                    margin-top: 10px;
                                    font-size: 8px;
                                    color: #{{ in_array($subj_colors[$ns['SubjCode']], $color_list2) ? '000' : 'FFF' }};
                                    line-height: 100%;
                                ">
                                    {!! $brs !!}
                                    {{ $ns['SubjCode'] }}<br>{{ $ns['Room'] }}<br>
                                    @if($type != 'faculty')
                                    {{ getObjectValueWithReturn($ns, 'Faculty1', '- No Teacher -') }}
                                    @endif
                                </td>
                                <?php
                                    $stack_scheds[$k] = ['skip_until' => $times[$a]];
                                    $has_sched = 1;
                                    break;
                                ?>
                            @endif
                        @endforeach

                    @endif

                    @if($has_sched == 0)
                    <?php $stack_scheds[$k]['skip_until'] = ''; ?>
                    <td class="{{ $k == 'S' ? 'last-td' : '' }}" height="5"></td>
                    @endif
                @else
                <td class="{{ $k == 'S' ? 'last-td' : '' }}" height="5"></td>
                @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
