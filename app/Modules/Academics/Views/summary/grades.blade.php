<?php 
    
    $i=1; 
    $unit =0;  

    $model = new \App\Modules\ClassRecords\Models\ReportCard_model;
    $m_grds = new \App\Modules\ClassRecords\Models\ReportCardDetails_model;
	
    $where = ['SummaryID' => $ref];
    $rs = $model->selectRaw("*, dbo.fn_gs_computation_setup(TermID, ProgramID, YearLevelID) as Setup")->where($where)->first();
	$shs = isSHS($rs->ProgramID)? 1:0;
	
	#Get Grading System
	$m_setup = DB::table("ESv2_Gradesheet_Setup")
					->where("TermID", $rs->TermID)
					->where("YearLevelID", $rs->YearLevelID)
					->where("ProgID", $rs->ProgramID)
					->first();
						
	
	$where = "SummaryID = '{$ref}' AND ScheduleID In (select ScheduleID From ES_Registrationdetails where RegiD = ES_permanentrecord_details.RegID)";
	$orderby = "Seq ASC";
	$grades = $m_grds->selectRaw("*, case when SeqNo=0 then 99 else SeqNo end As Seq,  [dbo].[fn_SubjectWeight](SubjectID) As Weight, 
					dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageA) AS GP1
					,dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageB) AS GP2 
					,dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageC) AS GP3 
					,dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageD) AS GP4 
					")
				->whereRaw($where)
				->orderByRaw($orderby)
				->get();
	
	$gpa_total = array(
		 0 => 0
		,1 => 0
		,2 => 0
		,3 => 0
	);		
	
?>
<div class="table-responsive">
<table class="table table-bordered table-condensed table-striped table-hover " style="cursor: pointer;">
    <thead>
        <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">Code</th>
            <th rowspan="2">Descriptive</th>
            <th rowspan="2" class="text-center">Units</th>
            
			@if($shs == 0)
				@if($rs->Setup == '0')
					<th colspan="4" class="text-center">Quarter Grades</th>
				@else
					<th colspan="6" class="text-center">Quarter Grades</th>
				@endif
            @else
            <th colspan="2" class="text-center">Semestral Grades</th>
            @endif
            <th rowspan="2" class="text-center">Final</th>
            
            @if($shs == 1)
            <th rowspan="2" class="text-center">Remedial</th>
            <th rowspan="2" class="text-center">RFG</th>
            @endif
            
            <th rowspan="2" class="text-center">Remarks</th>
            @if($shs == 0)
			<th colspan="5" class="text-center">Effort Grades</th>
            @else
            <th colspan="3" class="text-center">Semestral Conduct</th>
            @endif
			<th colspan="2" class="text-center">Include in Compute</th>
			<th rowspan="2" class="text-center">Weight</th>
			<th colspan="4" class="text-center">Grade Points Average</th>
        </tr>
		<tr>
                                   
            @if($shs == 0)
				@if($rs->Setup == '0')
					<th class="text-center">1st</th>
					<th class="text-center">2nd</th>
					<th class="text-center">3rd</th>
					<th class="text-center">4th</th>
				@else
					<th class="text-center">1st</th>
					<th class="text-center">2nd</th>
					<th class="text-center">Sem1</th>
					<th class="text-center">3rd</th>
					<th class="text-center">4th</th>
					<th class="text-center">Sem2</th>
				@endif
			@else
			<th class="text-center">MID</th>
            <th class="text-center">Pre-FNL</th>
            @endif
            
			@if($shs == 0)
			<th class="text-center">1st</th>
            <th class="text-center">2nd</th>
            <th class="text-center">3rd</th>
            <th class="text-center">4th</th>
			@else
			<th class="text-center">MID</th>
            <th class="text-center">Pre-FNL</th>
            @endif
			
			<th class="text-center">AVE</th>
			<th class="text-center">GWA</th>
			<th class="text-center">CONDUCT</th>
			
			<th class="text-center">1st</th>
			<th class="text-center">2nd</th>
			<th class="text-center">3rd</th>
			<th class="text-center">4th</th>
					
			
        </tr>
		
    </thead>
    <tbody>
        @foreach($grades AS $g)
            <?php
                $css = "";
				$cona = '';
				$conb = '';
				$conc = '';
				$cond = '';
				$gpa1 = 0;
				$gpa2 = 0;
				$gpa3 = 0;
				$gpa4 = 0;

				
                if( $g->ParentSubjectID == 0 ) $unit += trimmed($g->SubjectCreditUnits,'d'); 
                if( strtolower($g->Final_Remarks) == 'failed'){
                    $css = "text-danger";
                }          
				if($g->ConductA != ''){ $cona = round(floatval($g->ConductA)); }
				if($g->ConductB != ''){ $conb = round(floatval($g->ConductB)); }
				if($g->ConductC != ''){ $conc = round(floatval($g->ConductC)); }
				if($g->ConductD != ''){ $cond = round(floatval($g->ConductD)); }
				
				$gpa1 = $g->Weight * (($g->GP1>0)?($g->GP1):($g->AverageA));
				$gpa2 = $g->Weight * (($g->GP2>0)?($g->GP2):($g->AverageB));
				$gpa3 = $g->Weight * (($g->GP3>0)?($g->GP3):($g->AverageC));
				$gpa4 = $g->Weight * (($g->GP4>0)?($g->GP4):($g->AverageD));
				
				$gpa_total[0] += $gpa1;
				$gpa_total[1] += $gpa2;
				$gpa_total[2] += $gpa3;
				$gpa_total[3] += $gpa4;
				
            ?>
        <tr class="{{$css}}" data-idx="{{ encode($g->GradeIDX) }}" data-code="{{$g->SubjectCode}}" data-title="{{$g->SubjectTitle}}" data-parent="{{$g->ParentSubjectID}}">
            <td class="autofit font-xs">{{$i}}.</td>
            <td class="autofit font-xs bold code">
                @if($g->ParentSubjectID > 0 )
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
                {{$g->SubjectCode}}
            </td>
            <td class="title autofit">
                @if($g->ParentSubjectID > 0 )
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
                {{ $g->SubjectTitle}}
            </td>
            <td class="units font-xs text-center bold">{{$g->SubjectCreditUnits}}</td>		
            
            @if($shs == 0)
				@if($rs->Setup == '0')
				<td class="text-center avea bold ">{{$g->AverageA}}</td>
				<td class="text-center aveb bold ">{{$g->AverageB}}</td>
                <td class="text-center avec bold ">{{$g->AverageC}}</td>
                <td class="text-center aved bold ">{{$g->AverageD}}</td> 
				@else
				<td class="text-center avea bold ">{{$g->AverageA}}</td>
				<td class="text-center aveb bold ">{{$g->AverageB}}</td>
				<td class="text-center sem1 bold text-warning ">{{$g->TotalAverageAB}}</td>
                <td class="text-center avec bold ">{{$g->AverageC}}</td>
                <td class="text-center aved bold ">{{$g->AverageD}}</td> 
				<td class="text-center sem2 bold text-warning">{{$g->TotalAverageCD}}</td>
				@endif
            @else
				<td class="text-center avea bold ">{{$g->AverageA}}</td>
				<td class="text-center aveb bold ">{{$g->AverageB}}</td>
                <td class="bold font-blue-madison text-center">{{$g->TotalAverageAB}}</td>
                <td class="bold font-blue-madison avec text-center">{{$g->AverageC}}</td>      
            @endif                                                
            
            <td class="bold font-blue-madison text-center">{{$g->Final_Average}}</td>                        
            <td class="bold font-blue-madison text-center">{{$g->Final_Remarks}}</td>
            
           <td class="text-center cona bold " data-val='{{ $cona }}'>{{$cona}}</td>
           <td class="text-center conb bold " data-val='{{ $conb }}'>{{ $conb }}</td>
		   <td class="text-center conc bold " data-val='{{ $conc }}'>{{$conc}}</td>
		   <td class="text-center cond bold " data-val='{{ $cond }}'>{{$cond}}</td>
		   <td class="text-center con_fnl bold ">{{$g->Conduct_FinalAverage}}</td>
			
			<td class="bold font-blue-madison text-center"><?= $g->IsExclAverageCompute > 0 ? '' : '<i class="fa fa-check"></i>' ?></td>
			<td class="bold font-blue-madison text-center"><?= $g->IsExclConductCompute  > 0 ? '' : '<i class="fa fa-check"></i>' ?></td>
			<td class="text-center bold ">{{number_format($g->Weight,4)}}</td>
			<td class="text-center bold GPA">{{number_format($gpa1,3)}}</td>
			<td class="text-center bold GPA">{{number_format($gpa2,3)}}</td>
			<td class="text-center bold GPA">{{number_format($gpa3,3)}}</td>
			<td class="text-center bold GPA">{{number_format($gpa4,3)}}</td>
						
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
    <tfoot>
        <tr class="warning bold">
            <td></td>
            <td colspan="2" class="bold font-blue-madison text-right">Average</td>
            <td class="total-unit bold text-center">{{number_format($unit,1)}}</td>
			@if($shs == 0)
				<td class="bold text-center">{{$rs->AcademicA_Average}}</td>
				<td class="bold text-center">{{$rs->AcademicB_Average}}</td>
				<?php echo $rs->Setup > 0 ? '<td class="bold text-center"> </td>' : '' ;   ?>
				<td class="bold text-center">{{$rs->AcademicC_Average}}</td>
				<td class="bold text-center">{{$rs->AcademicD_Average}}</td>
				<?php echo $rs->Setup > 0 ? '<td class="bold text-center"> </td>' : '' ;   ?>
			@else
				<td class="bold text-center">{{$rs->AcademicA_Average}}</td>
				<td class="bold text-center">{{$rs->AcademicB_Average}}</td>
				<td class="bold text-center">{{$rs->AcademicC_Average}}</td>
				<td class="bold text-center">{{$rs->AcademicD_Average}}</td>
			@endif
            <td class="bold text-center">{{$rs->Final_Average}}</td>
            <td class="bold text-center">{{$rs->Final_Remarks}}</td>
            <td class="bold text-center">{{$rs->ConductA_Average}}</td>
			<td class="bold text-center">{{$rs->ConductB_Average}}</td>
			<td class="bold text-center">{{$rs->ConductC_Average}}</td>
			<td class="bold text-center">{{$rs->ConductD_Average}}</td>
			<td class="text-center bold ">{{$rs->Final_Conduct}}</td>
			<td colspan="3"></td>
			<td class="text-center bold ">{{number_format($gpa_total[0],3)}}</td>
			<td class="text-center bold ">{{number_format($gpa_total[1],3)}}</td>
			<td class="text-center bold ">{{number_format($gpa_total[2],3)}}</td>
			<td class="text-center bold ">{{number_format($gpa_total[3],3)}}</td>
        </tr>
    </tfoot>
</table>
</div>