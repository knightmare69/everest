<?php 
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
    $campus = get_campus();
    $level = get_deptlevel();    
    
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-tags"></i>
			<span class="caption-subject bold uppercase"> Summary of Grades </span>
			<span class="caption-helper">Use this module recalculate student grades...</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline  input-medium">									
	           <input type="text" class="form-control input-circle  filter-table input-sm " data-table="records-table" />
			</div>                                			                        
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">            
            <div class="portlet-input input-inline input-medium">									
                <select class="select2 form-control" name="term" id="term" title="School Year/Semester">
                    <option value="0" >- Select AY Term -</option>
                    @if(!empty($at))                        
                        @foreach($at as $r)
                        <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
                        @endforeach
                    @endif
                </select>				
			</div> 
            <div class="portlet-input input-inline input-medium">									
                <select class="select2 form-control" name="campus" id="campus" title="School Year/Semester">
                    <option value="0" >- Select Campus -</option>                                          
                        @foreach($campus as $c)
                        <option <?= $camp == $c->CampusID ? 'selected' : '' ?> value="{{ encode($c->CampusID) }}">{{ trimmed($c->CampusName) }}</option>
                        @endforeach                    
                </select>				
			</div>
           
            <div class="portlet-input input-inline input-small">									
                <select class="select2 form-control" name="level" id="level" title="Grade Level">
                    <option value="0" >- Select Level -</option>                                          
                        @foreach($level as $l)
                        <option <?= $lvl == $l->YearLevelID ? 'selected' : '' ?> value="{{ ($l->YearLevelID) }}">{{ trimmed($l->YearLevelName) }}</option>
                        @endforeach                    
                </select>				
			</div>
            <div class="portlet-input input-inline input-medium">									
                <select class="select2 form-control" name="section" id="section" title="Section">
                    <option value="0" >- Select Section -</option>                                          
                    <?= $sections; ?>
                </select>				
			</div>                        
            <button type="button" class="btn btn-sm btn-default " data-menu="refresh"><i class="fa fa-refresh"></i> Refresh </button>
            <button type="button" class="btn btn-sm btn-success pull-right " data-menu="cog"><i class="fa fa-calculator"></i> Recalculate </button>                        
        </div>
        <div class="table-responsive" id="grdList">
            @include($views.'list')            
        </div>
        <div class="well well-sm text-right">                                                          
            <button type="button" class="btn btn-sm btn-success " data-menu="cog"><i class="fa fa-calculator"></i> Recalculate </button>                        
        </div>
          	
	</div>
</div>