<?php
    $model = new \App\Modules\ClassRecords\Models\ReportCard_model;
    
    $useoldlvl = env('USEOLDLVL',false);
    $is_shs = 0;
            
    if($lvl != '0' && $lvl != '' ){
        
        if($useoldlvl){
            $dept = get_deptlevel($lvl);
            $is_shs = isSHS($dept->ProgID) ? 1 : 0;
        }
		
		$whr = "r.ValidationDate IS NOT NULL AND es_permanentrecord_master.TermID = '{$term}' AND r.IsWithdrawal = 0
				AND es_permanentrecord_master.CampusID = '{$camp}'
				AND es_permanentrecord_master.YearLevelID = '{$dept->YLID_OldValue}'
                AND es_permanentrecord_master.ProgramID = '{$dept->ProgID}'
		       ";
        if($sec > 0)
		{
			$whr .= " AND r.ClassSectionID = '{$sec}'";
		}
       
            $data = $model
            ->leftJoin('es_registrations as r','es_permanentrecord_master.RegID','=','r.RegID')            
            ->selectRaw(" es_permanentrecord_master.*, dbo.fn_StudentName(es_permanentrecord_master.StudentNo) As StudentName, 
                dbo.fn_YearLevel_k12(es_permanentrecord_master.YearLevelID, es_permanentrecord_master.ProgClassID) As YearLevel, 
                dbo.fn_StudentMajorByTerm(es_permanentrecord_master.StudentNo, es_permanentrecord_master.TermID) AS Major                 
            ")            
			->whereRaw($whr)
            ->orderBy('StudentName','asc')
            ->get();
    }else{
        $data  =array();
    }
    
    

    $j = 1;
    $x = 0;
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit" rowspan="2"><input type="checkbox" class="chk-header"></th>
            <th rowspan="2" class="autofit">#</th>
            <th rowspan="2">Ref#</th>
            <th rowspan="2">ID #</th>            
            <th rowspan="2">Student Name</th>
            <th rowspan="2">Grade Level</th>
            @if($is_shs == 1)
            <th rowspan="2">Track/Strands</th>
            @endif
            
            @if($is_shs == 1)
            <th rowspan="2" class="center">MID<br />TERM</th>            
            <th rowspan="2" class="center">FINAL<br />TERM</th>
            @else
            <th rowspan="2" class="center">1ST<br />QTR</th>            
            <th rowspan="2" class="center">2ND<br />QTR</th>
            <th rowspan="2" class="center">3RD<br />QTR</th>
            <th rowspan="2" class="center">4TH<br />QTR</th>
            @endif
            
            <th rowspan="2" class="center text-primary">FNL<br />AVE</th>
            <th rowspan="2" class="center">REMARKS</th>
            @if($is_shs == 1)
            <th class="center" colspan="3" style="border: 1px solid #ddd;">EFFORT</th>
            @else
            <th class="center" colspan="5" style="border: 1px solid #ddd;">EFFORT</th>
            @endif
			<th rowspan="2" class="center" style="border: 1px solid #ddd;">AWARDS</th>
			<th rowspan="2" class="center" style="border: 1px solid #ddd;">NOTE</th>
			
        </tr>
        <tr>
            @if($is_shs == 1)
            <th class="center">MID</th>            
            <th class="center">FNL</th>
            @else                        
            <th class="center">A</th>            
            <th class="center">B</th>
            <th class="center">C</th>
            <th class="center">D</th>
            @endif
            <th class="center text-primary">FINAL</th>            
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <tr data-id="{{ encode($r->SummaryID) }}" data-idno="{{ encode($r->StudentNo) }}"  >
            <td class="autofit"> <input type="checkbox" class="chk-child" ></td>
            <td class="font-xs autofit bold">{{$j}}.</td>
            <td class="">
                <a href="javascript:void();" class="options" data-val="{{ encode($r->SummaryID) }}" data-name="<?= $r->StudentName ?>" data-menu="view">{{$r->SummaryID}}</a>
            </td>            
            <td class="bold font-xs">{{$r->StudentNo}}</td>
            <td class="bold font-blue-madison">{{ $r->StudentName }}</td>
            <td>{{$r->YearLevel}} </td>
            @if($is_shs == 1)
            <td class="autofit">{{$r->Major}}</td>
            @endif
            
            @if($is_shs == 1)
            <td class="bold text-center">{{round($r->AcademicA_Average,2)}}</td>            
            <td class="bold text-center">{{round($r->AcademicB_Average,2)}}</td>
            @else
            <td class="bold text-center">{{round($r->AcademicA_Average,2)}}</td>            
            <td class="bold text-center">{{round($r->AcademicB_Average,2)}}</td>
            <td class="bold text-center">{{round($r->AcademicC_Average,2)}}</td>
            <td class="bold text-center">{{round($r->AcademicD_Average,2)}}</td>
            @endif
            
            <td class="bold text-center text-primary">{{round($r->Final_Average,2)}}</td>
            <td class="bold text-center">{{($r->Final_Remarks)}}</td>
            @if($is_shs == 1)
            <td class="bold text-center">{{($r->ConductA_Average)}}</td>
            <td class="bold text-center">{{($r->ConductB_Average)}}</td>
            @else
            <td class="bold text-center">{{($r->ConductA_Average)}}</td>
            <td class="bold text-center">{{($r->ConductB_Average)}}</td>
            <td class="bold text-center">{{($r->ConductC_Average)}}</td>
            <td class="bold text-center">{{($r->ConductD_Average)}}</td>
            @endif
            <td class="bold text-center text-primary">{{($r->Final_Conduct)}}</td>
			<td class="bold text-center">{{($r->Awards)}}</td>
			<td class="bold text-center">{{($r->RemarksA)}}</td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>