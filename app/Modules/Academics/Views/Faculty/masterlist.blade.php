<div id="masterlist" class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user-md"></i>
			<span class="caption-subject bold uppercase"> Faculty Master-list</span><br />
			<span class="caption-helper">Use this module to manage faculty information</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline input-medium">
				<div class="input-icon right">
					<i class="icon-magnifier"></i>
					<input type="text" placeholder="search..." class="form-control input-circle filter-table" data-table="tblfaculty">
				</div>
			</div>
                                
			<a class="btn btn-circle btn-default btnnew" href="javascript:void(0);"><i class="fa fa-plus"></i> Add </a>
            <button type="button" class="btn btn-circle btn-default btnremove" > <i class="fa fa-times"></i> Delete </button>
            <button class="btn btn-default btn-circle btnrefresh"><i class="fa fa-refresh"></i> Refresh </button>            
			<a href="#" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="emplist" style="padding:4px;min-height:400px; max-height:600px;overflow:auto;">
          @include($xview.'.sub.table.employee')
        </div>     	
	</div>
</div>    