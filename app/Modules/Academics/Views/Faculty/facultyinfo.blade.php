<div id="facultyinfo" class="portlet light" style="margin-bottom: 0px; display: none;">
<div class="portlet-title">
	<div class="caption">
		<i class="fa fa-user-md"></i>
		<span class="caption-subject bold uppercase"> Faculty Information</span><br />
		<span class="caption-helper">Use this module to manage faculty information</span>
	</div>
	<div class="actions">        
        <button class="btn btn-circle btn-primary btnsave"><i class="fa fa-save"></i> Save</button>                                        		
        <button type="button" class="btn btn-circle btn-default btncancel " > <i class="fa fa-times"></i> Cancel </button>            
		<a href="#" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
	</div>
</div>
<div class="portlet-body">
    <div id="left_container" >
    <div class="well well-light well-sm">
    
    <form role="form" class="emp_form nonedit" onsubmit="return false;">
    <div class="form-body">
	 <div class="form-group">
	  <div class="row">
	  <div class="col-xs-12 col-sm-6 col-md-6">
	     <span class="btn btn-xs btn-info btnempphoto hidden" style="position:absolute;direction:ltr;margin-bottom:-200px;"><i class="fa fa-search"></i> Browse</span>
		 <img src="<?php echo asset('assets/global/img/empty.png');?>" data-default="<?php echo asset('assets/global/img/empty.png');?>" class="ephoto" height="150px;" style="max-width:180px;">
	  </div>
	  <div class="col-xs-12 col-sm-6 col-md-6">
	   <div class="form-group">
		  <label>EmployeeID:</label>
		  <div class="input-group" style="width:98%">
			<input type="text" class="form-control required" id="empid" name="empid"/>
			<span class="input-group-addon btnsearch hidden"><i class="fa fa-search"></i></span>
		  </div>
	   </div>
	   <div class="form-group">
		  <label>Title:</label>
		  <?php 
		   if(isset($emptitle))
		    echo $emptitle;
		   else
			echo '<input type="text" class="form-control" id="emptitle" name="emptitle"/>';
		  ?>
	   </div>
	  </div>
	  </div>
	 </div>
	 <div class="form-group">
	  <label>LastName:</label>
	  <input type="text" class="form-control required" id="lname" name="lname"/>
	 </div>
	 <div class="form-group">
	  <label>FirstName:</label>
	  <input type="text" class="form-control required" id="fname" name="fname"/>
	 </div>
	 <div class="form-group">
	  <div class="row">
	   <div class="col-xs-12 col-sm-8">
	    <label>MiddleName:</label>
	    <input type="text" class="form-control " id="mname" name="mname"/>
	   </div>
	   <div class="col-xs-12 col-sm-4">
	    <label>MiddleInitial:</label>
	    <input type="text" class="form-control " id="miname" name="miname"/>
	   </div>
	  </div>
	 </div>
	 <div class="form-group">
	  <div class="row">
	   <div class="col-sm-6">
	   <label>Suffix:</label>
	   <?php 
	   if(isset($empsuffix))
		echo $empsuffix;
	   else
		echo '<input type="text" class="form-control" id="empsuffix" name="empsuffix"/>';
	   ?>
	   </div>
	   <div class="col-sm-6">
	    <label>Gender:</label>
	    <select class="form-control required" id="sex" name="sex">
		 <option value="M">Male</option>
		 <option value="F">Female</option>
		</select>
	   </div>
	  </div>
	 </div>
	 <div class="form-group">
	  <div class="row">
	   <div class="col-sm-6">
	    <label>Birthday:</label>
	    <input type="text" class="form-control date-picker required" data-date-format="mm/dd/yyyy" id="bday" name="bday"/>
	   </div>
	   <div class="col-sm-6">
	   <label>Civil Status:</label>
	    <?php 
	    if(isset($civilstats))
		 echo $civilstats;
	    else
		 echo '<input type="text" class="form-control required" id="civilstats" name="civilstats"/>';
	    ?>
	   </div>
	  </div>
	 </div>
	 <hr/>

	<div class="form-group">
	 <div class="checkbox-list">
      <label class="checkbox-inline">
       <div class="checker chkisfac">
       <span><input type="checkbox" id="isfaculty" name="isfaculty"/></span>
       </div>
       Is Faculty
      </label>
      <label class="checkbox-inline">
       <div class="checker chkinactive">
       <span><input type="checkbox" id="inactive" name="inactive"/></span>
       </div>
       Inactive
      </label>
	 </div>
	</div>
	   
	</div>
    </div>
    
   </form>
   </div>
   <div id="right_container">
   <div class="well well-light well-sm">           	            
   <!-- Details -->
   <form role="form" class="emp_data" onsubmit="return false;">
      <div class="form-body">
        <div class="form-group">
	  <div class="row">
	   <div class="col-sm-6">
	    <label class="control-label" >Campus:</label>
	    <?php 
	    if(isset($campus))
	 	 echo $campus;
	    else
		 echo '<input type="text" class="form-control" id="campus" name="campus"/>';
	    ?>
	   </div>
	   <div class="col-sm-6">
	    <label class="control-label" >School </label>
	    <?php 
	    if(isset($college))
		 echo $college;
	    else
		 echo '<input type="text" class="form-control" id="college" name="college"/>';
	    ?>
	   </div>
	  </div>
     </div>
     
	 <div class="form-group">
	  <div class="row">
	   <div class="col-sm-6">
	    <label>Department:</label>
	    <?php 
	     if(isset($dept))
		  echo $dept;
	     else
		  echo '<input type="text" class="form-control required" id="dept" name="dept"/>';
	    ?>
	   </div>
	   <div class="col-sm-6">
	    <label>Position:</label>
	    <?php 
	     if(isset($position))
		  echo $position;
	     else
		  echo '<input type="text" class="form-control required" id="pos" name="pos"/>';
	    ?>
	   </div>
	 </div>  
	</div>
    
     
       <div class="form-group">
            <div class="row">
            <div class="col-md-6">
                <label class="control-label">Faculty Rank</label>
            	<?php 
        		   if(isset($facrank))
        			echo $facrank;
        		   else
        			echo '<input type="text" class="form-control" id="facrank" name="facrank"/>';
        		?>
            </div>                        
            <div class="col-md-6">                                    						
                <label class="control-label">Status</label>
                <select class="form-control input-sm" id="emstatus" name="emstatus">
                    <option value="1">Full Time</option>
                    <option value="0">Part Time</option>
                </select>
            </div>    
            
            </div>
                                
        </div>
                    

	   <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <label>Teaching Level:</label>
        	    <?php 
        	     if(isset($educlvl))
        		  echo $educlvl;
        	     else
        		  echo '<input type="text" class="form-control" id="educlvl" name="educlvl"/>';
        	    ?>
            </div>
            <div class="col-md-6">
             <label>Degree Discipline:</label>
	    <?php 
	     if(isset($discipline))
		  echo $discipline;
	     else
		  echo '<input type="text" class="form-control" id="discipline" name="discipline"/>';
	    ?>
            </div>
        </div>        	   
	   </div>
	
	   <div class="form-group">
	   <div class="row">
	    <div class="col-xs-12 col-sm-8">
	     <label>PRC Licensure:</label>
		<?php 
		   if(isset($prc))
			echo $prc;
		   else
			echo '<input type="text" class="form-control" id="prc" name="prc"/>';
		?>
	    </div>
	    <div class="col-xs-12 col-sm-4">
	     <label>Load Release:</label>
	     <input type="text" class="form-control numberonly" id="load" name="load" value="0.00"/>
	    </div>
	   </div>
	   </div>
	  </div>
     </form>
     
     <!-- Subject Taught -->
     <div class="well well-sm well-light">
     <div class="subjtaught" style="min-height:250px;max-height:360px;overflow:auto;">
       
	   </div>
    </div>
    <!-- Teaching Load -->
    <div class="well well-sm well-light">
     <div class="teachload" style="min-height:250px;max-height:360px;overflow:auto;">
       
	   </div>
    </div>
    </div>
      </div>
        	</div>
        </div>    
    </div>
</div>