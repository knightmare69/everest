<div class="modal fade bs-modal-sm" id="modal_upload" tabindex="-1" role="dialog" aria-hidden="false">
 <div class="modal-backdrop fade in"></div>
 <div class="modal-dialog modal-sm">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
    <h4 class="modal-title"><i class="fa fa-file-image-o"></i> Upload Image</h4>
   </div>
   <div class="modal-body">
    <div class="row">
	 <div class="col-sm-12 text-center" style="margin-bottom:10px;">
	  <img class="previmg" src="<?php echo asset('assets/global/img/empty.png');?>" style="max-width:200px;max-height:200px;"/>
	  <img class="tmpimg hidden" src="<?php echo asset('assets/global/img/empty.png');?>" data-default="<?php echo asset('assets/global/img/empty.png');?>"/>
	 </div>
	 <div class="col-sm-12">
	 <form method="post" class="uploadform"  enctype="multipart/form-data" onsubmit="return false;">
	  <input type="file" class="form-control inpimg" name="inpimg" accept="image/*"/>
	 </form> 
	 </div>
	</div>
   </div>	
   <div class="modal-footer">
    <button class="btn green btnupload">Upload</button>
    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
   </div>
  </div> 
 </div>
</div> 