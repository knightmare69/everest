<table class="table table-bordered table-condense" style="white-space:nowrap;">
 <thead>
  <th class="text-center">AYTerm</th>
  <th class="text-center">Section</th>
  <th class="text-center">SubjectCode</th>
  <th class="text-center">SubjectTitle</th>
  <th class="text-center">Schedule 1</th>
  <th class="text-center">Room 1</th>
  <th class="text-center">Schedule 2</th>
  <th class="text-center">Room 2</th>
 </thead>
 <tbody>
  <?php
  if(isset($hist))
  {
	foreach($hist as $rs)
	{
	 $xtermid   = $rs->TermID;
	 $xterm     = $rs->AYTerm;
	 $xsectid   = $rs->SectionID;
	 $xsection  = $rs->SectionName;
	 $xsubjid   = $rs->SubjectID;
	 $xsubjcode = $rs->SubjectCode;
	 $xsubjname = $rs->SubjectTitle;
	 $xsched1   = $rs->Sched_1;
	 $xroom1    = $rs->RoomName1;
	 $xsched2   = $rs->Sched_2;
	 $xroom2    = $rs->RoomName2;
	 echo '<tr>
            <td>'.$xterm.'</th>
            <td>'.$xsection.'</th>
            <td>'.$xsubjcode.'</th>
            <td>'.$xsubjname.'</th>
            <td>'.$xsched1.'</th>
            <td>'.$xroom1.'</th>
            <td>'.$xsched2.'</th>
            <td>'.(($xsched2!='')?$xroom2:'').'</th>
           </tr>';	
	 
	}
  }	  
  ?>
 </tbody>
</table>