<table class="table table-bordered table-condense table-strife " id="tblfaculty" style=" cursor: pointer; ">
  <thead>
   <th class="autofit"><input type="checkbox" class="chk-header"></th>
   <th>#</th>
   <th class="text-center">ID #</th>
   <th class="text-center">FullName</th>
   <th class="text-center">Birthday</th>
   <th class="text-center">Gender</th>
   <th class="text-center">CivilStatus</th>
   <th class="text-center">Department</th>
   <th class="text-center">Position</th>
   <th class="text-center">Email</th>
   <th class="text-center">Inactive</th>
  </thead>
  <tbody>
   <?php
    if(isset($emp))
	{
	   $i = 1;
	 foreach($emp as $rs)
     {
	  $empid       = $rs->EmployeeID;
      $lname       = $rs->LastName;
	  $fname       = $rs->FirstName;
	  $mname       = $rs->MiddleName;
	  $miname      = $rs->MiddleInitial;
	  $extname     = $rs->ExtName;
	  $prefix      = $rs->Prefix;
	  $suffix      = $rs->Suffix;
	  $bday        = $rs->DateOfBirth;
	  $sex         = $rs->Gender;
	  $civilID     = $rs->CivilStatusID;
	  $civil       = $rs->CivilDesc;
	  $posID       = $rs->PosnTitleID;
	  $pos         = $rs->PositionDesc;
	  $deptID      = $rs->DeptID;
	  $dept        = $rs->DeptName;
	  $campus      = $rs->CampusID;
	  $college     = $rs->CollegeID;
      $email       = $rs->Email;
	  $inactive    = $rs->Inactive;
	  
	  $facultyID   = $rs->FacultyID;
	  $isfaculty   = (($facultyID==0)?0:1);
	  $campus      = $rs->CampusID;
	  $college     = $rs->CollegeID;
	  $rankID      = $rs->RankID;
	  $isregular   = $rs->IsRegularFaculty;
	  $isfulltime  = $rs->IsFullTime;
	  $teachlvl    = $rs->TeachLoadLevel;
	  $degree      = $rs->DegreeDiscipline;
	  $prc         = $rs->PRC_LicenseID;
	  $loadrelease = $rs->LoadReleased;
	  
      
	  if($bday!='')
	  {
	   $tmpdate = new DateTime($bday);	  
	   $bday    = $tmpdate->format('m/d/Y');	  
	  }	  
	  
	  $is_inact = (($inactive==0)?'<i class="fa fa-square-o"></i>':'<i class="fa fa-check-square-o"></i>'); 
	  ?>
	   <tr class="emprow" data-id="<?= $empid ?>" data-fid="<?= $facultyID ?>" data-isfac="<?= $isfaculty ?>" data-rank="<?= $rankID ?>" 
            data-campus="<?= $campus ?>" data-college="<?= $college ?>" data-isreg="<?= $isregular ?>" data-isfull="<?= $isfulltime ?>"
            data-lvl="<?= $teachlvl ?>" data-degree="<?= $degree ?>" data-prc="<?= $prc ?>" data-load="<?= $loadrelease ?>"
       >
             <td><input type="checkbox" class="chk-child"></td>
             <td class="autofit">{{$i}}.</td>
             <td class="empid autofit "><?= $empid ?></td>
             <td class="empname bold autofit uppercase" data-title="<?= $prefix ?>" data-lname="<?= $lname ?>" data-fname="<?= $fname ?>" data-mname="<?= $mname ?>" data-miname="<?= $miname ?>" data-suffix="<?= $extname ?>"><a href="javascript:void(0);" class="btnedit"><?= $lname . (($lname!=$fname)?(', '.$fname.(($miname!='')?(' '.$miname):'')):''); ?></a></td>
             <td class="bday"><?= $bday ?></td>
             <td class="sex"><?= $sex ?></td>
             <td class="civilstat" data-id="<?= $civilID ?>"><?= $civil ?></td>
             <td class="dept" data-id="<?= $deptID ?>"><?= $dept ?></td>
             <td class="position" data-id="<?= $posID ?>"><?= $pos ?></td>
             <td><?= $email ?></td>
             <td class="text-center inactive" data-value="<?= $inactive ?>"><?= $is_inact ?></td>
        </tr>
	 <?php 
        $i++;
     }	 
	}	
   ?>
   
  </tbody>
</table>