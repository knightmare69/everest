<table class="table table-bordered table-condense" style="white-space:nowrap;">
 <thead>
  <th class="text-center">SubjectCode</th>
  <th class="text-center">SubjectTitle</th>
  <th class="text-center">Lect Units</th>
  <th class="text-center">Lab Units</th>
  <th class="text-center">Lect Hrs</th>
  <th class="text-center">Lab Hrs</th>
 </thead>
 <tbody>
  <?php
  if(isset($hist))
  {
	$subjid=array();
	foreach($hist as $rs)
	{
	 $xsubjid   = $rs->SubjectID;
	 $xsubjcode = $rs->SubjectCode;
	 $xsubjname = $rs->SubjectTitle;
	 $xsubjlecu = $rs->AcadUnits;
	 $xsubjlabu = $rs->LabUnits;
	 $xsubjlech = $rs->LectHrs;
	 $xsubjlabh = $rs->LabHrs;
	 if(!array_key_exists($xsubjid,$subjid))
	 {
      $subjid[$xsubjid] = $xsubjid;		 
	  echo '<tr class="subjhist" data-id="'.$xsubjid.'">
			 <td>'.$xsubjcode.'</td>
			 <td>'.$xsubjname.'</td>
			 <td>'.$xsubjlecu.'</td>
			 <td>'.$xsubjlabu.'</td>
			 <td>'.$xsubjlech.'</td>
			 <td>'.$xsubjlabh.'</td>
		    </tr>';	
	 }
	}
  }	  
  ?>
 </tbody>
</table>