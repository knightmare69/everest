<?php 
namespace App\Modules\Academics\Models\Club;

use illuminate\Database\Eloquent\Model;

Class Organization extends Model {

	public $table='ES_ClubOrganizations';
	protected $primaryKey ='ClubID';

	protected $fillable  = array(
			'CampusID',
			'TermID',
			'ClubName',
			'ClubDescription',
			'BldgID',
			'RoomID',
			'ProgID',
			'ModeratorID',
			'CreatedBy',
			'DateCreated',
			'DateModified',
			'ModifiedBy'
	);

	public $timestamps = false;
}