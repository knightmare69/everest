<?php 
namespace App\Modules\Academics\Models\Club;

use illuminate\Database\Eloquent\Model;

Class Buildings extends Model {

	public $table='ES_Buildings';
	protected $primaryKey ='BldgID';

	protected $fillable  = array(
			'CampusID',
			'BldgName',
			'BldgOtherName',
			'Acronym',
			'FloorsCount',
			'BldgPictures',
			'IsLANReady',
			'Elevator',
			'Escalator'
	);

	public $timestamps = false;
}