<?php 
namespace App\Modules\Academics\Models\Club;

use illuminate\Database\Eloquent\Model;

Class Rooms extends Model {

	public $table='ES_Rooms';
	protected $primaryKey ='RoomID';

	protected $fillable  = array(
			'BldgID',
			'Floor',
			'RoomNo',
			'RoomName',
			'RoomTypeID',
			'Capacity',
			'IsAirConditioned',
			'IsUsable',
			'IsLANMember',
			'AllowNightClass',
			'Shared'
	);

	public $timestamps = false;
}