<?php 
namespace App\Modules\Academics\Models\Club;

use illuminate\Database\Eloquent\Model;

Class Club extends Model {

	public $table='ES_StudentsClubbing';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
			'RegID',
			'StudentNo',
			'ClubID',
			'CreatedBy',
			'DateCreated'
	);

	public $timestamps = false;
}