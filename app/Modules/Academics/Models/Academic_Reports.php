<?php

namespace App\Modules\Academics\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Academic_Reports extends Model
{
    public function getYearLevel($prog_class)
    {
        if($prog_class >= 50){
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->whereIn('YearLevel', ['Grade 11', 'Grade 12'])
                ->orderBy('SeqNo', 'ASC')->get();
        } else {
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->where('ProgClass', $prog_class)
                ->orderBy('SeqNo', 'ASC')->get();
        }

        return $at;
    }

    public function Programs()
    {
        $get = DB::table('ES_Programs')->select('ProgID', 'ProgName', 'ProgShortName', 'ProgClass')
        ->whereIn('ProgShortName', ['LS', 'K', 'US','MS'])
                // ->whereNotIn('ProgClass', [50, 80, 60])
                ->orderBy('ProgClass', 'ASC')->get();

        return $get;
    }


    public function AcademicTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')
        // ->where('SchoolTerm', 'School Year')
        ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function getSections($term_id, $prog_id, $yl_id)
    {
        // Patch year level conflict
        // End

        $sec = DB::table('ES_ClassSections')
                ->select('SectionID', 'SectionName')
                ->where(['TermID' => $term_id, 'ProgramID' => $prog_id, 'YearLevelID' => $yl_id])
                ->orderBy('SectionName', 'ASC')->get();

        return $sec;
    }


    public function getSubjects($term_id, $sections)
    {
        $subjlist = explode(",", $sections);

        $sec = DB::table('ES_ClassSchedules')
                ->select(['SubjectID', DB::raw(" dbo.fn_SubjectCode(SubjectID) As SubjectCode ")])
                ->where(['TermID' => $term_id])
                ->whereIn('SectionID',$subjlist)
                ->groupBy("SubjectID")
                ->orderBy('SubjectCode', 'ASC')->get();

        return $sec;
    }

   public function getStudent($find_str, $term, $program, $year_level, $section_id)
   {
        $student = DB::table('ES_Registrations as r')
                    ->select('s.StudentNo', 's.FirstName', 's.LastName', 's.MiddleName', 's.MiddleInitial')
                    ->join('ES_Students as s', 'r.StudentNo', '=', 's.StudentNo')
                    ->where(['r.ProgID' => $program, 'r.YearLevelID' => $year_level, 'r.TermID' => $term, 'r.ClassSectionID' => $section_id])
                    ->where(function($query) use ($find_str){
                        $query->where('r.StudentNo', '=', $find_str)
                        ->orWhere('s.LastName', 'like', '%'.$find_str.'%')
                        ->orWhere('s.FirstName', 'like', '%'.$find_str.'%');
                    })
                    ->get();

        return $student;
    }
}
