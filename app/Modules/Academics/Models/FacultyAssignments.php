<?php

namespace App\Modules\Academics\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class FacultyAssignments extends Model
{
    protected $table = 'ES_ClassSchedules';
    protected $tbl_class_sec = 'ES_ClassSections';
    protected $primaryKey = 'ScheduleID';

    protected $fillable = ['FacultyID'];
    public $timestamps = false;

    public function getSections($sections = array(), $campus, $academic_year)
    {

        $progams = getUserProgramAccess();
        $data = DB::table($this->tbl_class_sec)
                    ->select('SectionID', 'SectionName', 'AdviserID', 'IsBlock','YearLevelID', 'Limit', 'TermID',
                    DB::raw('dbo.fn_EmployeeName(AdviserID) as AdviserName'),
                    DB::raw('dbo.fn_FacultyRank(AdviserID) as AdviserRank'),
                    DB::raw('dbo.fn_ProgramName(ProgramID) as Program, dbo.fn_K12_YearLevel3(YearLevelID, ProgramID) AS YearLevel '))
                    ->where(['TermID' => $academic_year, 'CampusID' => $campus])
                    ->whereIn('ProgramID',$progams)
                    ->orderBy('SectionName');

        if(!empty($sections)){
            $data = $data->whereNotIn('SectionID', $sections)  ;
        }

        return $data->get();
    }

    public function getSectionDetails($section_id)
    {
        $g = DB::table($this->tbl_class_sec.' as S')->select('S.SectionName', 'T.AcademicYear', 'T.SchoolTerm', 'S.SectionID')
                ->join('ES_AYTerm as T', 'T.TermID', '=', 'S.TermID')
                ->where('SectionID', $section_id)
                ->first();
        return $g;
    }

    public function getSectionsWithSubjects($section_id, $q = [])
    {
        $data = DB::table($this->table)
                ->select('ScheduleID', 'SectionID', 'FacultyID','FacultyID_2', 'Sched_1', 'Time1_Start', 'Time1_End', 'Days1', 'Time2_Start', 'Time2_End', 'Days2', 'Sched_2', 'Time3_Start', 'Time3_End', 'Days3', 'Sched_3', 'Time4_Start', 'Time4_End', 'Days4', 'Sched_4', 'Time5_Start', 'Time5_End', 'Days5', 'Sched_5', 'SubjectID','FacultybyGender',
                    DB::raw('dbo.fn_EmployeeName(FacultyID) as FacultyName, dbo.fn_FacultyRank(FacultyID) as FacultyRank'),
                    DB::raw('dbo.fn_EmployeeName(FacultyID_2) as FacultyName2, dbo.fn_FacultyRank(FacultyID_2) as FacultyRank2'),
                    DB::raw('dbo.fn_SectionName(SectionID) as SectionName'),
                    DB::raw('dbo.fn_SubjectCode(SubjectID) as SubjCode'),
                    DB::raw('dbo.fn_SubjectTitle(SubjectID) as SubjTitle, dbo.fn_RoomName(Room1_ID) AS Room1,
                            dbo.fn_RoomName(Room2_ID) AS Room2,
                            dbo.fn_RoomName(Room3_ID) AS Room3,
                            dbo.fn_RoomName(Room4_ID) AS Room4,
                            dbo.fn_RoomName(Room5_ID) AS Room5
                            ')
                );

        if(!empty($q)) {
            $data = $data->whereRaw($q);
        } else {
            $data = $data->where('SectionID', $section_id);
        }

        return $data->get();
    }

    public function getFaculty($string_search, $campus)
    {
        $find = DB::table('ES_Faculty as f')
                    ->select(
                        'f.FacultyID', 'f.EmployeeID',
                        DB::raw('dbo.fn_EmployeeName(f.EmployeeID) as FacultyName'),
                        DB::raw('dbo.fn_FacultyRank(FacultyID) as FacultyRank')
                    )
                    ->join('HR_Employees as hr', 'f.EmployeeID', '=', 'hr.EmployeeID')
                    ->where('f.CampusID', '=', $campus)
                    ->where('LastName', 'like', '%'.$string_search.'%')
                    ->orWhere('FirstName', 'like', '%'.$string_search.'%')
                    ->orWhere('hr.EmployeeID', $string_search)
                    ->get();

        return $find;
    }

    public function updateSection($sec_id, $adviser)
    {
        $update = DB::table($this->tbl_class_sec)->where('SectionID', $sec_id)->update(['AdviserID' => $adviser]);
        return $update;
    }

    public function removeAdviser($sec_id)
    {
        $update = DB::table($this->tbl_class_sec)->where('SectionID', $sec_id)->update(['AdviserID' => '']);
        return $update;
    }

    public static function subjectReplaceSelection()
    {
        $group_class = [1, 2];
        $g = DB::table('ES_Subjects')->select('SubjectID', 'SubjectCode', 'SubjectTitle', 'SubjectDesc')
                ->whereIn('LevelID', DB::table('ES_Program_Class')->select('ClassCode')->whereIn('GroupClass', $group_class))
                ->get();
        return $g;
    }
}
