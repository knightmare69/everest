<?php
namespace App\Modules\Academics\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class EmpFaculty extends Model
{
    protected $table      = 'HR_Employees as emp';
    protected $primaryKey = 'emp.EmployeeID';
    public $timestamps    = false;
	
	public function scopeFaculty($query)
    {
      return $query
	            ->select(DB::raw('emp.EmployeeID,LastName,FirstName,MiddleName,MiddleInitial,ExtName,Prefix,Email,fac.FacultyID'))
	            ->join('ES_Faculty as fac','emp.EmployeeID','=','fac.EmployeeID')
	            ->where('Inactive','=','0')
	            ->whereRaw('ISNULL(fac.FacultyID,0)<>0');
    }
    
    public static function GetAllactive(){
        return DB::table('HR_Employees')
            ->selectRaw('EmployeeID, LastName, FirstName ')
            ->where(['Inactive'=>'0'])
            ->orderBy('LastName','asc') 
            ->orderBy('FirstName')            
            ->get();	            
    }
   	
}