<?php
	Route::group(['prefix' => 'academics','middleware'=>'auth'], function () {

        Route::group(['prefix' => 'faculty-assignment', 'middleware' => 'auth'], function () {
            Route::get('/', 'FacultyAssignments@index');
            Route::post('event', 'FacultyAssignments@event');
			Route::get('/section_schedule','FacultyAssignments@printSectionSched');
        });

        Route::group(['prefix' => 'class-schedules', 'middleware' => 'auth'], function () {
            Route::get('/', 'ClassSchedules@index');
            Route::get('new-section', 'ClassSchedules@newsection');
            Route::post('event', 'ClassSchedules@event');
        });

       	Route::group(['prefix' => 'club-organization'], function () {
		    Route::get('/','Club@index');
		    Route::post('/event','Club@event');
		});

        Route::group(['prefix' => 'faculty-management'], function(){
			Route::get('/', 'Faculty@index');
			Route::get('/txn','Faculty@txn');
			Route::post('/txn','Faculty@txn');
			Route::get('/photo','Faculty@photo');
		});

        Route::group(['prefix' => 'reports'], function () {
		    Route::get('/','AcadReports@index');
		    Route::post('/event','AcadReports@event');
            Route::get('print', 'AcadReports@print_report');
		});

        #Date : Nov. 2, 2016 10:10H
        Route::group(['prefix' => 'setup'], function () {
		    Route::get('/','AcadSetup@index');
		    Route::post('/event','AcadSetup@event');
		});

        #Date : Aug 30, 2017 10:24H
        Route::group(['prefix' => 'decorum'], function () {
		    Route::get('/','Decorum@index');
            Route::get('/form','Decorum@form');
		    Route::post('/event','Decorum@event');
		});
        
        #Date : Aug 30, 2017 10:24H
        Route::group(['prefix' => 'summary-of-grades'], function () {
		    Route::get('/','Summary@index');            
		    Route::post('/event','Summary@event');
		});
        
	});
?>
