<?php

namespace App\Modules\Academics\Services;

use App\Modules\Academics\Services\FacultyAssignments\Validation;
use DB;

class FacultyAssignServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'update')
    {
        if ($action == 'update') {
            $validate = $this->validation->validateFaculty($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else if($action == 'search-faculty') {
            $validate = $this->validation->validateSearchFaculty($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }
        return ['error' => false, 'message' => ''];
    }

    public function academic_year()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm', 'Active_OnlineEnrolment')->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function campus()
    {
        $get = DB::table('ES_Campus')->select('CampusID', 'Acronym', 'ShortName')->get();

        return $get;
    }
    
    public function curriculum($progid){
        $where =array(
            'Inactive' => 0,
            'ProgramID' => $progid
        );
        
        $get = DB::table('ES_Curriculums')
                    ->select('IndexID', 'CurriculumCode', 'CurriculumDesc')
                    ->where($where)
                    ->get();

        return $get;
    }
   
     public function yearterm($curr_id){
      
        
        $get = DB::table('ES_YearTerms')
                    ->select('IndexID', 'YearTermDesc','YearLevelID')
                    ->whereRaw("IndexID  IN (SELECT YearTermID FROM dbo.ES_CurriculumDetails WHERE CurriculumID = '".$curr_id."')")
                    ->get();

        return $get;
    }
    
    public function curriculumSubjects($curr_id, $yearterm){
      $where =array(
            'CurriculumID' => $curr_id,
            'YearTermID' => $yearterm
        );
        
        $get = DB::table('ES_CurriculumDetails as cs')
                    ->leftJoin('ES_Subjects as s','cs.subjectid','=','s.subjectid')
                    ->select('s.SubjectID', 's.SubjectCode','s.SubjectTitle','s.AcadUnits')
                    ->where($where)
                    ->get();

        return $get;
    }
    
    public function createSection($p){
        
        
        $prog = DB::table('ES_Programs')                    
                    ->select('CampusID', 'CollegeID','ProgID')
                    ->where('ProgID', decode(getObjectValue($p,'program')) )
                    ->first();
        
        $data = array(
            'SectionName' => getObjectValue($p,'section'),
            'TermID' => decode(getObjectValue($p,'term')),
            'ProgramID' => $prog->ProgID,
            'CampusID' =>  $prog->CampusID,
            'CollegeID' => $prog->CollegeID,
            'CurriculumID' => decode(getObjectValue($p,'curriculum')),
            'IsBlock' => '1',
            'Limit' => (getObjectValue($p,'limit')),
            'AdviserID' => decode(getObjectValue($p,'adviser')),
            'YearLevelID' => decode(getObjectValue($p,'yearlevel')),
            'RoomID' => decode(getObjectValue($p,'room')),
        );
        
        $sec_id = DB::table('ES_ClassSections')->insertGetId($data);           
        
        $subj = explode(',', getObjectValue($p,'subjects') );
        
        foreach($subj as $k){
            $data = array(
                'SectionID' => $sec_id,
                'TermID' => decode(getObjectValue($p,'term')),
                'SubjectID' => decode($k),            
            );
            $result = DB::table('ES_ClassSchedules')->insert($data);
        }
              
        return $sec_id;
    }  
    
    public function deleteSection($sec_id){
        
        $result = DB::table('ES_ClassSections')->where(['SectionID' => $sec_id])->delete();                 
        return $result;
    }
    
    public function getSchedules($sched_id){
        $result = DB::table('ES_ClassSchedules')
            ->selectRaw("*, dbo.fn_RoomNo(Room1_ID) AS Room1,
                            dbo.fn_RoomName(Room2_ID) AS Room2,
                            dbo.fn_RoomName(Room3_ID) AS Room3,
                            dbo.fn_RoomName(Room4_ID) AS Room4,
                            dbo.fn_RoomName(Room5_ID) AS Room5,
                            FacultyID As FacultyID_1,
                            dbo.fn_EmployeeName(FacultyID) AS FacultyName1,
                            dbo.fn_EmployeeName(FacultyID_2) AS FacultyName2 ")
            ->where('ScheduleID',$sched_id)
            ->first();
        return $result;
    }
    
    public function updateSchedule($post){
        $result = 0;
        $x = getObjectValue($post,'mode');
        $sched_id = decode(getObjectValue($post,'sched_id'));
        
        $term = decode(getObjectValue($post,'term'));
        $section = decode(getObjectValue($post,'section'));
        
        /*
        $time_start = $this->convertStandardToMilitaryTime(getObjectValue($post,'start'));
        $time_end = $this->convertStandardToMilitaryTime(getObjectValue($post,'end'));
        */
        
        $time_start = (getObjectValue($post,'start'));
        $time_end = (getObjectValue($post,'end'));
        $days  = getObjectValue($post,'days');
        $schedule = getObjectValue($post,'sched');
        $room = decode(getObjectValue($post,'room'));
        $teacher = decode(getObjectValue($post,'teacher'));                        
        
        $rsSection = $this->classSection($section);
        
        if($rsSection->IsBlock == 1){
            $conflicts = $this->get_conflict($term, $section, $sched_id, $days, $time_start, $time_end,$x, $room, $teacher );    
        }else{
            $conflicts = 0;
        }
                         
        if(count($conflicts) == 0 ){
                                            
            $data = array(
                'Sched_'.$x => getObjectValue($post,'sched'),
                'Days'.$x => $days,
                'Time'.$x.'_Start' => $time_start,
                'Time'.$x.'_End' => $time_end,
                'Room'.$x.'_ID' => $room,
                'FacultyID'.($x == 1? '' : '_'.$x)  => $teacher,
                'Sched_'. $x => $schedule 
            );
            
            $result = DB::table('ES_ClassSchedules')            
                ->where('ScheduleID',$sched_id)
                ->update($data);            
               
        }else{
            $result = $conflicts;
        }
            
        return $result;
        
    }
    
    
    public function convertStandardToMilitaryTime($time){
        $output = "";
        $is_pm = strpos($time, "PM");
            
        $output =  str_replace(":","",$time);
        $output =  str_replace("AM","",$output);
        $output =  str_replace("PM","",$output);
        
        if($is_pm > 0 && floatval($output) < 1200 ){
            $output =  floatval($output) + 1200 ;
        }
                        
        return $output + '00';
        
    }
    
    public function get_conflict($term, $section, $sched, $days, $tstart, $tend, $mode, $room, $teacher ){
        
        $darr = str_split($days);
        
        foreach($darr as $d){
            
            #Check conflict within subject
            $output = DB::select("EXEC dbo.ES_GetClassScheduleSubjectItSelfConflicts '".$term."', '".$sched."', '%".$d."%','".$tstart."','".$tend."', '".$mode."'");
            
            if( count($output) >0 ){                
                break;
            }
                        
            #Check conflict within section
            $output = DB::select("EXEC dbo.ES_GetClassScheduleConflicts '".$term."','{$section}','".$sched."', '%".$d."%','".$tstart."','".$tend."', '".$mode."'");            
            if( count($output) >0 ){                
                break;
            }

            #Check conflict within rooms
            $output = DB::select("EXEC dbo.ES_GetRoomScheduleConflicts '".$term."','{$room}', '%".$d."%','{$sched}','".$tstart."','".$tend."'");            
            if( count($output) >0 ){                
                break;
            }
            
            if($teacher != ''){                            
                #Check conflict within faculty
                $output = DB::select("EXEC ES_GetFacultyScheduleConflicts '".$term."','{$teacher}', '%".$d."%','{$sched}','".$tstart."','".$tend."'");            
                if( count($output) >0 ){
                    err_log('faculty conflict : ' . $teacher );
                    break;
                }
            }
            
                
        }
        
        return $output;
    }
    
    public function updateClassSize($p){                        
        $data = array('Limit' => (getObjectValue($p,'newsize')));        
        $res = DB::table('ES_ClassSections')->where(['SectionID' => decode(getObjectValue($p,'section'))])->update($data);                                                 
        return $res;
    }
    
    public function classRename($p){                
        $data = array('SectionName' => (getObjectValue($p,'new')));        
        $res = DB::table('ES_ClassSections')->where(['SectionID' => decode(getObjectValue($p,'section'))])->update($data);                                                 
        return $res;
    }
    
    public function classSection($section_id){                                
        $res = DB::table('ES_ClassSections')->select('*')->where('SectionID', $section_id)->first();                                                 
        return $res;
    }                   
    
}