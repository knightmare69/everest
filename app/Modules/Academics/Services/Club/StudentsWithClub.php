<?php
namespace App\Modules\Academics\Services\Club;

use DB;
use Request;
use Response;

Class StudentsWithClub {

	public function data()
	{
		$hasParam = false;
		$data = [];
		$sEcho = 0;
		$iTotalRecords = 0;
		if (Request::get('Program') && Request::get('SchoolYear') && Request::get('SchoolCampus') && Request::get('ClubID')) {
			$this->model = DB::table('ES_StudentsClubbing as r')
					->select([
							DB::raw("LastName+', '+FirstName+' '+MiddleInitial as Name"),
							's.StudentNo',
							'Gender',
							'r.RegID'
						])
					->leftJoin('ES_Students as s',
							's.StudentNO','=','r.StudentNO'
						)
					->where('r.ClubID',decode(Request::get('ClubID')));

			if (Request::get('StudentNo')) {
				$hasParam = true;
				$this->model->where('S.StudentNo',Request::get('StudentNo'));
			}

			if (Request::get('StudentName')) {
				$hasParam = true;
				$this->model->where(B::raw("LastName+', '+FirstName+' '+MiddleInitial"),'like','%'.Request::get('StudentName').'%');
			}

			$iTotalRecords = $this->model->count();
			$iDisplayLength = intval($_REQUEST['length']);
			$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
			$iDisplayStart = intval($_REQUEST['start']);
			$sEcho = intval($_REQUEST['draw']);
			  
			$records = array();
			$records["data"] = array(); 

			$end = $iDisplayStart + $iDisplayLength;
			$end = !$hasParam ? $end > $iTotalRecords ? $iTotalRecords : ($end == $iTotalRecords) ? 0 : $end : 0;
			$data = $this->model->skip($end)->take($iDisplayLength)->get();
		}
		$records['data'] = [];
		foreach($data as $row) {
			$records['data'][] = [
				'<input type="checkbox" class="form-control chk-child" data-regid="'.encode($row->RegID).'">',
				$row->StudentNo,
				$row->Name,
				$row->Gender,
			];
		}
		
		if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
		    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
		    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
		 }
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		return $records;
	}
}	
?>