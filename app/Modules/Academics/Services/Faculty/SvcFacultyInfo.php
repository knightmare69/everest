<?php
namespace App\Modules\Academics\Services\Faculty;
use DateTime;
use DB;
Class SvcFacultyInfo
{
	public function get_emplist($isfaconly=false)
	{
	 $query = "SELECT emp.EmployeeID
				  ,emp.LastName
				  ,emp.FirstName
				  ,emp.MiddleName
				  ,emp.MiddleInitial
				  ,emp.Prefix
				  ,emp.ExtName
				  ,emp.Suffix
				  ,emp.DateOfBirth
				  ,emp.Gender
				  ,emp.CivilStatusID
				  ,cs.CivilDesc
				  ,emp.PosnTitleID
				  ,pos.PositionDesc
				  ,emp.DeptID
                  ,emp.Email
				  ,dept.DeptName
				  ,ISNULL(emp.CampusID,fac.CampusID) as CampusID
				  ,ISNULL(emp.CollegeID,fac.CollegeID) as CollegeID
				  ,emp.Inactive
				  ,ISNULL((fac.FacultyID),0) as FacultyID
				  ,fac.RankID
				  ,fac.IsRegularFaculty
				  ,fac.IsFullTime
				  ,fac.TeachLoadLevel
				  ,fac.DegreeDiscipline
				  ,fac.PRC_LicenseID
				  ,fac.LoadReleased
			  FROM HR_Employees as emp
			  LEFT JOIN ES_Faculty as fac ON emp.EmployeeID=fac.EmployeeID
			  LEFT JOIN ES_Departments as dept ON emp.DeptID=dept.DeptID
			  LEFT JOIN CivilStatus as cs ON emp.CivilStatusID=cs.StatusID
			  LEFT JOIN HR_PositionTitles as pos ON emp.PosnTitleID=pos.PosnTitleID 
              ORDER BY LastName ASC ";	
	 $exec = DB::select($query);
	 return $exec;
	}
	
	public function get_emphistory($empid=0)
	{
	 $query = "SELECT cs.TermID
	                 ,dbo.fn_AcademicYearTerm(cs.TermID) as AYTerm
					 ,cs.ScheduleID
	                 ,cs.SectionID
					 ,sec.SectionName
					 ,cs.SubjectID
					 ,s.SubjectCode
					 ,s.SubjectTitle
					 ,s.SubjectDesc
					 ,s.AcadUnits
					 ,s.LabUnits
					 ,s.LectHrs
					 ,s.LabHrs
					 ,cs.Sched_1
					 ,cs.Room1_ID
					 ,r.RoomName as RoomName1
					 ,cs.Sched_2
					 ,cs.Room2_ID
					 ,r.RoomName as RoomName2
                 FROM ES_ClassSchedules as cs
           INNER JOIN ES_Subjects as s ON cs.SubjectID=s.SubjectID
           INNER JOIN ES_ClassSections as sec ON cs.SectionID=sec.SectionID
            LEFT JOIN ES_Rooms as r ON cs.Room1_ID=r.RoomID OR cs.Room2_ID=r.RoomID
                WHERE FacultyID='2'
			 ORDER BY dbo.fn_AcademicYearTerm(cs.TermID) DESC";	
	 $exec = DB::select($query);
	 return $exec;	
	}
	
	public function del_employee($opt=0,$empid=0)
	{
	 $query  = '';
     if($opt==0)
	 {	 
      $query = "DELETE FROM ES_Faculty WHERE EmployeeID='".$empid."';
                DELETE FROM HR_Employees WHERE EmployeeID='".$empid."'"; 		 
	 } 
	 else
	  $query = "UPDATE HR_Employees SET Inactive=1 WHERE EmployeeID='".$empid."'"; 		 
	 
	 if($query!='')
	 { 
	  $exec = DB::statement($query);
	  return $exec;	
	 }
     else
      return false;		 
	}
	
	public function is_deletable($empid=0)
    {
	 $query = "SELECT ISNULL(COUNT(*),0) as NonDeletable FROM ES_TransactionsLog WHERE UserID='".$empid."'";	
	 $exec = DB::select($query);
	 if($exec)
     {
	   $result = false;
	   foreach($exec as $rs)
       {
		 $result = $rs->NonDeletable;
         return $result;		 
	   }
       
	   return $result;
	 }
     else
      return false;		 
	}
	
	public function saveemp($xid,$fid=0,$empdata=array())
	{
	 $exec   = false;
	 $fields = array('EmployeeID'    => 'empid'
			        ,'LastName'      => 'lname'
			        ,'FirstName'     => 'fname'
					,'MiddleName'    => 'mname'
					,'MiddleInitial' => 'miname'
					,'ExtName'       => 'extname'
					,'Suffix'        => 'extname'
					,'Prefix'        => 'title'
					,'DateOfBirth'   => 'bday'
					,'Gender'        => 'sex'
					,'CivilStatusID' => 'civil'
					,'CampusID'      => 'camp'
					,'CollegeID'     => 'college'
					,'DeptID'        => 'dept'
					,'PosnTitleID'   => 'pos'
					,'LoadReleased'  => 'load'
					,'Inactive'      => 'inactive');
					
	 $xfields = array('EmployeeID'       => 'empid'
			         ,'DeptID'           => 'dept'
			         ,'CollegeID'        => 'college'
					 ,'CampusID'         => 'camp'
					 ,'RankID'           => 'rank'
					 ,'IsRegularFaculty' => 'isfull'
					 ,'IsFullTime'       => 'isfull'
					 ,'DegreeDiscipline' => 'degree'
					 ,'TeachLoadLevel'   => 'educlvl'
					 ,'PRC_LicenseID'    => 'prc'
					 ,'LoadReleased'     => 'load');
	 
     $parama  = array();
     $paramb  = array();
	 foreach($fields as $k=>$d)
     {
	  $parama[$k] = (($this->is_key_exist($empdata,$d))?$empdata[$d]:'');
	 }
	 
	 foreach($xfields as $k=>$d)
     {
	  $paramb[$k] = (($this->is_key_exist($empdata,$d))?$empdata[$d]:'');
	 }
	 
	 if($xid=='new')
	  $exec = DB::table('HR_Employees')->insert($parama);
	 else
	  $exec = DB::table('HR_Employees')->where('EmployeeID', $xid)->update($parama);
	 
	 if(($fid=='new' || $fid=='0') && $empdata['isfac']=='1')
	  $execa = DB::table('ES_Faculty')->insert($paramb);
	 else if($fid!='new' && $empdata['isfac']=='1')
	  $execa = DB::table('ES_Faculty')->where('FacultyID', $fid)->update($paramb);
     else if($fid!='new' && $empdata['isfac']=='0')
      $execa = DB::table('ES_Faculty')->where('FacultyID', $fid)->delete();
     else
      $execa = false;
  
	 return $exec;	
	}
	
	public function getEmpPhoto($empid)
    {
       $photo = DB::table('HR_Employees')->where('EmployeeID', $empid)->value('Photo');
       return $photo;
    }
	
	public function updateimg($empid=0,$imgstr=false)
	{
	  $exec = false;
	  if($empid==0 && ($imgstr==false || $imgstr==''))
       return false;
      else
	  {
	   $exec = DB::update("UPDATE HR_Employees SET Photo = CONVERT(image, ".$imgstr.") WHERE EmployeeID='".$empid."' ");
       return $exec;
	  }		  
	}
	
    public function dropdown($param = array())
	{
	  $sel_field = ((array_key_exists('sel_field',$param))?$param['sel_field']:'*');
	  $table     = ((array_key_exists('table',$param))?$param['table']:'');
	  $col_id    = ((array_key_exists('col_id',$param))?$param['col_id']:'');
	  $col_desc  = ((array_key_exists('col_desc',$param))?$param['col_desc']:'');
	  $col_sep   = ((array_key_exists('col_sep',$param))?$param['col_sep']:' ');
	  $name      = ((array_key_exists('name',$param))?$param['name']:'');
	  $cls       = ((array_key_exists('cls',$param))?$param['cls']:'');
	  $attr      = ((array_key_exists('attr',$param))?$param['attr']:'');
	  $selected  = ((array_key_exists('selected',$param))?$param['selected']:'');
	  $init      = ((array_key_exists('init',$param))?$param['init']:'');
	  $init_lbl  = ((array_key_exists('init_lbl',$param))?$param['init_lbl']:'');
	  $limit     = ((array_key_exists('limit',$param))?$param['limit']:0);
	  $limit_sel = ((array_key_exists('limit_sel',$param))?$param['limit_sel']:0);
	  $condition = ((array_key_exists('condition',$param))?$param['condition']:'');
	  $order     = ((array_key_exists('order',$param))?$param['order']:'');
	  $spc_attr  = '';
	  $count     = 0;
	  $pointer   = '';
	  
	  if(($sel_field)=='*')
	   $result = DB::table($table);
	  else
	   $result = DB::table($table)->select(DB::raw($sel_field));   
   
	  if($condition!='')
	  {
	   $result = $result->whereRaw($condition);	  
	  }
	  
      if($order!='')
      { 
        if(is_array($order))
		{
		 foreach($order as $k=>$v)
         {
		  $result = $result->orderBy($k,$v);	
		 }		 
		}
        else
        {			
		 $split_ord = explode(' ',$order);
      	 $result = $result->orderBy($split_ord[0],$split_ord[1]);	
		} 
	  }		  
	  
	  $result = (($table=='ESv2_YearLevel')? $this->get_yearlvl_list() : $result->get());  
	  
	  $output='<select id="'.$name.'" name="'.$name.'" class="'.$cls.'" disabled><option value="-1" disabled selected>No Option Available</option></select>';
	  if($result)
	  {
		$output='<select id="'.$name.'" name="'.$name.'" class="'.$cls.'" '.$attr.'>';
	    if($init!=''){$output .= '<option value="'.$init.'" '.(($selected=='')?'selected':'').'>'.(($init_lbl!='')?$init_lbl:'-Select One-').'</option>'; }
		foreach($result as $rs)
        {
		 $desc  = '';
		 $value = ((property_exists($rs,$col_id))?$rs->$col_id:'');	
		 if(is_array($col_desc))
		 {	 
		  foreach($col_desc as $arr_desc)
		  {
			$desc .= (($desc!='')?$col_sep:'').((property_exists($rs,$arr_desc))?$rs->$arr_desc:'');	  
		  }
	     }
		 else	 
		  $desc  = ((property_exists($rs,$col_desc))?$rs->$col_desc:'');	
	     
		 if($table=='ESv2_YearLevel')
		  $spc_attr ='data-class="'.((property_exists($rs,'ProgClass'))?$rs->ProgClass:'').'" data-prog="'.((property_exists($rs,'ProgID'))?$rs->ProgID:'').'" data-old-id="'.((property_exists($rs,'Old_YearLevelID'))?$rs->Old_YearLevelID:'').'"  data-code="'.((property_exists($rs,'YearLevelCode'))?$rs->YearLevelCode:'').'" data-withmajor="'.((property_exists($rs,'YearLevelCode') && ($rs->YearLevelCode=='G10' || $rs->YearLevelCode=='G11' || $rs->YearLevelCode=='G12'))?1:0).'"';	  
		 else if($table=='ESv2_PaymentMethods')	 
		  $spc_attr ='data-instruct="'.((property_exists($rs,'Instructions'))?$rs->Instructions:'').'"';	  
		 
		 if($limit>0)
		 {
		   if($pointer=='' && $value==$selected && $count<$limit)
		   {
			 $pointer = $value;
			 $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($count==$limit_sel)?'selected':'').'>'.$desc.'</option>'; 
			 $count++;
	       }
           else if($pointer!='' && $count<$limit)
		   {
			 $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($count==$limit_sel)?'selected':'').'>'.$desc.'</option>'; 
			 $count++;
	       }		   
		 }
		 else if($table=='ESv2_PaymentMethods')
		  $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($rs->SystemDefault==1 || $value==$selected)?'selected':'').'>'.$desc.'</option>';	 
		 else	 
		  $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($value==$selected)?'selected':'').'>'.$desc.'</option>';
		}		
		$output.='</select>';
	  }	  
	  
	  return $output;	
	}
	
	public function is_key_exist($arr=array(),$key,$default=false)
	{
	 $type=((@is_object($arr))? 'object':((@is_array($arr))?'array':'undefined'));
	 switch($type)	
	 {
	  case 'array':
	   return ((@array_key_exists($key,$arr))?$arr[$key]:$default);
      break;	  
	  case 'object':
	   return ((@property_exists($arr,$key))?($arr->$key):$default);
      break;	  
	  default:
	   return $default;
	  break;
	 }
	 return false;
	}	
}//end of the class
?>