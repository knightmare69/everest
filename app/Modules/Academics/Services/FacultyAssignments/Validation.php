<?php

namespace App\Modules\Academics\Services\FacultyAssignments;

use Validator;

class Validation
{
    public function validateSearchFaculty($post)
    {
        $validator = validator::make(
            ['faculty_name' => getObjectValue($post, 'faculty')],
            ['faculty_name' => 'required|string']
        );

        return $validator;
    }

    public function validateFaculty($post)
    {
        $validator = validator::make(
            ['faculty' => decode(getObjectValue($post, 'f')), 'schedule' => decode(getObjectValue($post, 'sched'))],
            ['faculty' => 'required', 'schedule' => 'required|int']
        );

        return $validator;
    }
}
