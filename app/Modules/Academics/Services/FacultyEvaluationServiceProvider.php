<?php
namespace App\Modules\Academics\Services;
use DB;

class FacultyEvaluationServiceProvider{
    public function __construct(){
		
	}
	
	public function get_faculties($termid,$cid='',$did='',$filter=''){
	  $exec = false;
	  $qry  = "SELECT cs.FacultyID
	                 ,sec.CollegeID
					 ,(SELECT TOP 1 CollegeCode FROM ES_Colleges WHERE CollegeID=sec.CollegeID) as CollegeName
					 ,(SELECT TOP 1 d.DeptCode FROM HR_Employees as e LEFT JOIN ES_Departments as d ON e.DeptID=d.DeptID WHERE e.EmployeeID=cs.FacultyID) as DeptCode 
					 ,(SELECT TOP 1 d.DeptName FROM HR_Employees as e LEFT JOIN ES_Departments as d ON e.DeptID=d.DeptID WHERE e.EmployeeID=cs.FacultyID) as DeptName 
				     ,(SELECT TOP 1 CONCAT(LastName,', ',FirstName,' ',MiddleName) FROM HR_Employees WHERE EmployeeID=cs.FacultyID) As Faculty 
                     ,count(fe.EntryID) As Respondent
					 ,isnull(avg(FinalRating),0) As Overall
                FROM es_classSchedules cs
         INNER JOIN ES_ClassSections as sec ON cs.SectionID=sec.SectionID
 		  left join ES_FacultyEvaluation fe on cs.ScheduleID= fe.SchedID  
                WHERE cs.TermID = '{$termid}' AND cs.FacultyID <> '' ANd cs.FacultyID Is NOT NULL 
			 GROUP BY cs.FacultyID,sec.CollegeID
			 ORDER BY CollegeName,DeptName,Faculty";
	  $exec = DB::SELECT($qry);		 
	  return $exec;
	}
}
?>	
	