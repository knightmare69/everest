<?php

namespace App\Modules\Academics\Services;

use App\Modules\ClassRecords\Models\ClassSection_model as cs_model;
use App\Modules\ClassRecords\Models\ReportCard_model as mAve;
use App\Modules\ClassRecords\Models\ReportCardDetails_model as mGrade;

use App\Modules\Registrar\Services\EvaluationServices As evaluation;

use DB;

class SummaryService
{

    public static function get_sections($term ,$prog, $level)
    {
        $get = cs_model::where(['TermID' => $term,'ProgramID'=> $prog, 'YearLevelID' => $level])->get();

        return $get;
    }
    
    public function get_subjects_bysections($term , $level)
    {
        $get = cs_model::where(['TermID' => $term,'ProgramID'=> $prog, 'YearLevelID' => $level])->get();

        return $get;
    }
    
    public static function recompute_ave($sid){
        
        $info = DB::table('es_permanentrecord_master')
				->selectRaw("*, dbo.fn_StudentName(StudentNo) As FullName, dbo.fn_AcademicYearTerm(TermID) as Term  ")
				->where(['SummaryID' => $sid])->first();
         
		 
        if ($info){
            
            $subj = mGrade::where(['StudentNo' => $info->StudentNo, 'TermID' => $info->TermID ])
                    ->orderBy('ParentSubjectID','ASC')
                    ->get();
            
            foreach($subj as $s){
                
                if ($s->ParentSubjectID != 0){
					#compute all child grade for all quarter
                    evaluation::computeSubjectAve($s->GradeIDX, $s->termid, $s->StudentNo , 1);
                    evaluation::computeSubjectAve($s->GradeIDX, $s->termid, $s->StudentNo , 2);
                    evaluation::computeSubjectAve($s->GradeIDX, $s->termid, $s->StudentNo , 3);
                    evaluation::computeSubjectAve($s->GradeIDX, $s->termid, $s->StudentNo , 4);
                } else{
                    evaluation::computeSubjectAve($s->GradeIDX);
                }
				
                evaluation::computeConductAve($s->GradeIDX);
				
            }
			
			if( isSHS($info->ProgramID) )
			{    
				evaluation::computeQuarterAve($info->TermID , $info->StudentNo, 1);
				evaluation::computeQuarterAve($info->TermID , $info->StudentNo, 2);
				evaluation::computeGeneralAveFromSubject($info->StudentNo, $info->TermID, 0,0, 0);
				
			}
            else
			{
						
				/* Compute 1st Quarter */
				evaluation::computeQuarterAve($info->TermID , $info->StudentNo, 1);
				/* Compute 2nd Quarter */
				evaluation::computeQuarterAve($info->TermID , $info->StudentNo, 2);
				/* Compute 3rd Quarter */
				evaluation::computeQuarterAve($info->TermID , $info->StudentNo, 3);
				/* Compute 4th Quarter */
				evaluation::computeQuarterAve($info->TermID , $info->StudentNo, 4);
            }
			
			evaluation::computeGPA( $info->StudentNo, $info->TermID );
			
			SystemLog('Summary of Grades','','Recompute Grades','recalculate','TermID:'. $info->TermID . ', IDNo:'. $info->StudentNo .', Name: '. $info->FullName . ', SY: '.$info->Term ,'Grade Successfully Recalculated' );
			
            return 1;                                               
        }
        
        return 0;
        
    }
	
    public static function evaluate_awardee($sid){
		
		/*
		 gold : no subject final grade lower than 90
		        final average >= 91.5
		 silver: no subject below 88
		         final average > 89.5
	      bronze : no subject below 86
		         final average > 87.5
		*/
		$award = '';
		$gold = 1;
		$silver = 1;
		$bronze =1;
		
		$info = DB::table('es_permanentrecord_master')->where(['SummaryID' => $sid])->first();
		if ($info){
			$subj = mGrade::where(['StudentNo' => $info->StudentNo, 'TermID' => $info->TermID ])
                    ->orderBy('ParentSubjectID','ASC')
                    ->get();
					
			foreach($subj as $s){
				if($s->IsExclAverageCompute == 0){
					if ( floatval($s->Final_Average)  < 90 ) {
						$gold = 0;
					}
					
					if ( floatval($s->Final_Average)  < 88 ) {
						$silver = 0;
					}
					
					if ( floatval($s->Final_Average)  < 86 ) {
						$bronze = 0;
					}					
				}
				
			}
			
			if ( floatval($info->Final_Average)  < 91.5 ) {
				$gold = 0;
			}
			
			if ( floatval($info->Final_Average)  < 89.5 ) {
				$silver = 0;
			}
			
			if ( floatval($info->Final_Average)  < 87.5 ) {
				$bronze = 0;
			}	
			
			if ($gold > 0 ){
				$award = 'Gold';
			}elseif($silver > 0 ){
				$award = 'Silver';
			}elseif($silver > 0 ){
				$award = 'Bronze';
			}

			if ($award <> ''){
				DB::statement("UPDATE es_permanentrecord_master SET Awards = '{$award}' Where SummaryID = '{$sid}' "); 
			}
			
			
			
			return 1;   
		}
		return 0;
		
	}
}
?>