<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;
Class AdmissionStatusRemarks extends Model {

	protected $table='ESV2_AdmissionStatusRemarks';
	public $primaryKey ='ID';

	protected $fillable  = array(
			'AppNo',
			'StatusID',		
			'Remarks',
			'CreatedBy',
			'CreatedDate',
	);

	public $timestamps = false;

	public static function data()
	{
		return
		DB::table('ESV2_AdmissionStatusRemarks as r')
			->select([
				'r.Remarks',
				'r.CreatedDate',
				'u.FullName'
			])
			->leftJoin('ESV2_users as u','u.UserIDX','=','r.CreatedBy');
	}

}