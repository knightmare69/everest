<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class InquiryChild extends Model {

	protected $table='es_inquiry';
	public $primaryKey ='id';

	protected $fillable  = array(
			'TermID',
			'InquiryParentID',
			'ScheduleType',
			'type',
			'fname',		
			'mname',
			'lname',
			'email',
			'contact_no',
			'YearLevelID',
			'BatchID',
			'CreatedDate',
			'CreatedBy',
			'ModifiedDate',
			'ModifiedBy',
			'IsApplied',
			'AppliedDate',
			'HasAdmissionApp',
			'ProgClass',
			'gender',
			'birth_date'
	);

	public $timestamps = false;

	public function data() {
		return 
			DB::table($this->table.' as i')
				->leftJoin('es_ayterm as t',
						't.TermID','=','i.TermID'
					)
				->leftJoin('ESv2_yearlevel as y',
						'y.YearLevelID','=','i.YearLevelID'
					);
	}

	public function getParentID($id) {
		return $this->where('id',$id)->select('InquiryParentID')->pluck('InquiryParentID');
	}

	public function getParentEmail($childId) {
		return \App\Modules\Admission\Models\InquiryParent::email($this->getParentID($childId));
	}

	public function getFamilyID($childId) {
		return \App\Modules\Admission\Models\InquiryParent::FamilyID($this->getParentID($childId));
	}

	public function parentHasAccount($childId) {
		return \App\Modules\Admission\Models\InquiryParent::hasAcount($this->getParentID($childId));
	}

	public function HasAdmissionApp($id) {
		return $this->where(['id' => $id,'HasAdmissionApp' =>1])->count() > 0;
	}
}