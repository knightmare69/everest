<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionReservation_model extends Model {

	protected $table='ES_Admission_Reservation';	
	public $primaryKey ='EntryID';

	protected $fillable  = array(
    	'DateEntry',
    	'IDNo',		
    	'TrackID',
    	'ORNo',
    	'ORDate',
        'TermID',
        'Remarks'
	);

	public $timestamps = false;
}