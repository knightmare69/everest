<?php namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class References extends Model {

	protected $table='ESv2_Admission_References';
	public $primaryKey ='ReferenceID';

	protected $fillable  = array(
			'AppNo',
			'Name',
			'Relationship',
			'School',
			'PhoneNumber',
			'CreatedBy',
			'CreatedDate',
			'ModifiedBy',
			'ModifiedDate'
		);

	public $timestamps = false;
}