<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class Questionaire extends Model {

	protected $table='esv2_admission_questions';
	public $primaryKey ='id';

	protected $fillable  = array(
			'qns_group',
			'qns_parent_id',
			'question',
			'description',
			'type',
			'options',
			'is_header_only',
			'is_inactive',
			'CreatedDate',
			'CreatedBy',
			'ModifiedDate',
			'ModifiedBy'
	);

	public $timestamps = false;

}