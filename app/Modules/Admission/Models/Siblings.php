<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;

Class Siblings extends Model {

	protected $table='ESv2_Admission_Siblings';
	public $primaryKey ='SiblingIDX';

	protected $fillable  = array(
			'SiblingIDX',
			'FamilyID',
			'FullName',
			'DateofBirth',
			'Gender',
			'SchoolAttended',
			'CreatedBy',
			'CreatedDate',
			'ModifiedBy',
			'ModifiedDate'
		);

	public $timestamps = false;
}