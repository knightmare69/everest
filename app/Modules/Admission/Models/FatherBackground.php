<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;

Class FatherBackground extends Model {

	protected $table='ESv2_Admission_FamilyBackground';
	public $primaryKey ='FamilyID';

	protected $fillable  = array(
			'FamilyID',
			'Father_Name',
			'Father_BirthDate',
			'Father_MaritalID',
			'Father_Occupation',
			'Father_Company',
			'Father_CompanyAddress',
			'Father_CompanyPhone',
			'Father_Email',
			'Father_TelNo',
			'Father_Mobile',
			'Father_Address',
			'Father_EducAttainment',
			'Father_SchoolAttended',
			'Father_Photo',
			'Father_ReligionID',
			'Father_BusinessType',
			'Father_NationalityID',
			'Father_CitizenshipID',
		);

	public $timestamps = false;

}