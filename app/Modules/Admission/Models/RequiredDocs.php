<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;

Class RequiredDocs extends Model {

	protected $table='ESv2_Admission_RequiredDocs';
	public $primaryKey ='EntryID';

	protected $fillable  = array(
			'AppNo',
			'TemplateID',
			'TemplateDetailID',
			'DocID',
			'SeqNo',
			'IsReviewed',
			'IsExempted',
			'IsSubmitted',
			'HasAttachment',
			'UploadedBy',
			'UploadedDate',
			'Filename',
			'Remarks'
		);

	public $timestamps = false;
}