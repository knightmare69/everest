<?php namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class AdmissionEvaluationAttachment extends Model {

	protected $table='ESv2_Admission_EvaluationAttachment';
	public $primaryKey ='AttachmentID';

	protected $fillable  = [
							'EvaluationID','AppNo','FileName','Attachment','FileType','FileSize','FileExtension','CreatedBy','CreatedDate','ModifiedDate','ModifiedBy'
						];
			
	public $timestamps = false;

	public function _save($id,$attachment)
	{
		$data['EvaluationID'] = decode($id);
		$data['AppNo'] = decode(getObjectValue($attachment,'AppNo'));
		$data['FileName'] = getObjectValue($attachment,'FileName');
		$data['Attachment'] = DB::raw('0x'.bin2hex(file_get_contents(getObjectValue($attachment,'Attachment'))));
		$data['FileType'] = getObjectValue($attachment,'type');
		$data['FileSize'] = getObjectValue($attachment,'FileSize');
		$data['FileExtension'] = getObjectValue($attachment,'FileExtension');

		if (getObjectValue($attachment,'id') == '') {
			$data['CreatedDate'] = systemDate();
			$data['CreatedBy']= getUserID();
			return $this->create($data);
		} else {
			$data['ModifiedDate'] = systemDate();
			$data['ModifiedBy']= getUserID();
			return $this->where('AttachmentID',getObjectValue($attachment,'id'))->update($data);
		}
	}
}