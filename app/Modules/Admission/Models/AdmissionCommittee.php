<?php namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionCommittee extends Model {

	protected $table='ESv2_Admission_Committee';
	public $primaryKey ='CommitteeID';

	protected $fillable  = [
			'AppNo', 'CommitteeRemarks','Remarks', 'IsAdmitted', 'IsConditional', 'IsWaitPool', 'IsDenied', 'CreatedBy', 'CreatedDate'
		];
			
	public $timestamps = false;
}