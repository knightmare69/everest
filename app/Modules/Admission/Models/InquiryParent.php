<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class InquiryParent extends Model {

	protected $table='es_inquiry_parent';
	public $primaryKey ='id';

	protected $fillable  = array(
			'type',
			'email',
			'fname',		
			'mname',
			'lname',
			'contact_no',
			'YearLevelID',
			'BatchID',
			'CreatedDate',
			'CreatedBy',
			'ModifiedDate',
			'ModifiedBy',
			'HasAccount',
			'FamilyID'
	);

	public $timestamps = false;

	public static function email($id) {
		return DB::table('es_inquiry_parent')->where('id',$id)->select('email')->pluck('email');
	}

	public static function FamilyID($id) {
		return DB::table('es_inquiry_parent')->where('id',$id)->select('FamilyID')->pluck('FamilyID');
	}

	public static function hasAcount($id) {
		return DB::table('es_inquiry_parent')->where(['id' => $id,'HasAccount' => 1])->count() > 0;
	}
}