<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionPhoto extends Model {

	protected $table='AdmissionPhoto';
	protected $connection = "sqlsrvAttachment";
	public $primaryKey ='ID';

	protected $fillable  = array(
			'AppNo',
			'FileName',		
			'Attachment',
			'FileType',
			'FileSize',
			'FileExtension',
			'CreatedDate',
			'CreatedBy',
	);

	public $timestamps = false;
}