<table class="table">
	<thead>
		<th width="15%">Added By</th>
		<th width="15%">Added Date</th>
		<th width="70%">Remarks</th>
	</thead>
	<tbody>
		<?php $data = App\Modules\Admission\Models\AdmissionStatusRemarks::data()->where('AppNo',Request::get('AppNo'))->get();  ?>
		@foreach($data as $row)
		<tr>
			<td>{{ $row->FullName }}</td>
			<td>{{ $row->CreatedDate }}</td>
			<td>{{ $row->Remarks }}</td>
		</tr>
		@endforeach
		@if (count($data) <= 0) 
		<tr>
			<td colspan="3" class="bold center">No Added Remarks!</td>
		</tr>
		@endif
	</tbody>
</table>	