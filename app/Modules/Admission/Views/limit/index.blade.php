<style>
td input[type="text"]{
 width:100% !important;
 height:100% !important;
 border:none !important;
 border-color:transparent !important;
 outline:none !important;
 box-shadow:transparent;
 background: transparent;
 background-color: transparent !important;
}

td select{
 width:100% !important;
 height:100% !important;
 border:none !important;
 border-color:transparent !important;
 outline:none !important;
 box-shadow:transparent;
 background: transparent;
 background-color: transparent !important;
}
</style>
<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-cog fa-lg"></i> Admission Limit</div>
		<div class="actions">
			<a href="javascript:void(0);" class="btn btn-success btnsave"><i class="fa fa-save"></i> Save</a>
			<a href="javascript:void(0);" class="btn btn-warning btncancel">Cancel</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-6">
				    <label class="control-label">Academic Year & Term:</label>
					<select class="form-control" id="ayterm" name="ayterm">
						<option value="-1"> - Select - </option>
					<?php
						$ayterm = new App\Modules\Setup\Models\AcademicYearTerm;
						foreach($ayterm::orderBy('AcademicYear','DESC')->get() as $a){
							echo '<option value="'.$a->TermID.'" '.(($a->IsCurrentTerm==1)?'selected':'').'>'.$a->AcademicYear.' '.$a->SchoolTerm.'</option>';
						}
					?>
					</select>
				</div>
				<div class="col-sm-6">
					<label class="control-label">Campus:</label>
					<select class="form-control" id="campus" name="campus">
						<option value="-1"> - Select - </option>
					<?php
						$campus = new App\Modules\Setup\Models\Campus;
						$i      = 0;
						foreach($campus::get() as $c){
							echo '<option value="'.$c->CampusID.'" '.(($i==0)?'selected':'').'>'.$c->ShortName.'</option>';
							$i++;
						}
					?>
					</select>
				</div>
				<div class="col-sm-12">
					<br/>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="table-responsive">
					<table class="table table-bordered table-condense" id="tblimit">
						<thead>
							<th class="text-center">College Code</th>
							<th class="text-center">Program Code</th>
							<th class="text-center">Program</th>
							<th class="text-center">Major</th>
							<th class="text-center">Limit</th>
						</thead>
						<tbody></tbody>
						<tfoot class="">
							<td colspan="5" class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading..</td>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>