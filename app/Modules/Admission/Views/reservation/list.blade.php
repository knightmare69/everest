<?php
$service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
$i = 1;
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Applicant #</th>            
            <th>Applicant Name</th>
            <th>Year Level</th>
            <th>Track</th>
            <th>OR #</th>
            <th>Date of Payment</th>
            <th>Remarks</th>                                    
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)           
        <tr data-id="{{ encode($r->EntryID) }}"  >
            <td class="autofit">
                  <input type="checkbox" class="chk-child" >                                                    
            </td>            
            <td class="font-xs autofit bold">{{$i}}.</td>
            <td><a href="javascript:void(0);" class="sec-name" data-id="{{ encode($r->SectionID) }}">{{ $r->IDNo }}</a></td>                        
            <td class="">{{ $r->StudentName }}</td>
            <td>{{$r->YearLevel}}</td>
            <td class="autofit">                
               	<select name="track" class="input-borderless input-sm track input-medium form-control ">
                    <option value="0">- Select -</option>                    
                    @foreach($service->GetAcademicTrack() as $at)
                    <option value="{{ $at->IndexID }}" <?= $r->TrackID == $at->IndexID ? 'selected' : ''  ?> >{{ $at->MajorDiscDesc }}</option>
                    @endforeach                    
                </select>                
            </td>
            
            <td class="autofit">
                <input type="text" class="form-control orno input-borderless input-small input-sm" value="{{ $r->ORNo }}" />                
            </td>
            <td class="autofit">
                <input type="text" class="form-control ordate input-borderless input-small datepicker input-sm" value="{{ setDate($r->ORDate,'m/d/Y') }}" />                
            </td>                                   
            <td></td>                    
        </tr>
        <?php $i++; ?>
        @endforeach

        @endif
    </tbody>
</table>