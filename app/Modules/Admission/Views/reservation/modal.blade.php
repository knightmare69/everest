<div id="event_modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" ><div class="modal-content">
    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Add Reservation</h4></div>
    <div class="modal-body">
        <div class="input-group">            
            <input type="text"  value="" class="form-control" id="searchkey" />    
            <div class="input-group-btn">
				<button type="button" class="btn btn-default" tabindex="-1">Search</button>			
			</div>                                                                                                                                                    
        </div>
        <div style="overflow-y: scroll;">
        
        <table class="table table-striped table-bordered table-hover table_scroll dataTable table-condensed no-footer" style="cursor: pointer; margin-bottom: 0px !important;">
            <thead>
                <tr>
                    <th width="20%">Student #</th>
                    <th width="60%">Student Name</th>
                    <th width="20%">Year Level</th>
                </tr>
            </thead>
        </table>
        </div>
        <div class="scrollbox" style="height: 450px; overflow-y: scroll;">
       
       </div>
    </div>
    <div class="modal-footer">	
        <button class="btn btn-success" data-menu="save" type="button">Add Reservation </button>
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i> Close</button>
    </div></div></div>
</div>