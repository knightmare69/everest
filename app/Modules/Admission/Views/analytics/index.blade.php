<div class="row">
    <div class="col-md-8">
		<ul class="nav nav-pills maintab">
			<li class="active"><a href=".monitoring" data-toggle="tab" aria-expanded="false">Admission Monitoring</a></li>
			<li><a href=".total_no" data-toggle="tab" aria-expanded="true">Total No. Of Students</a></li>
			<li><a href=".cond_info" data-toggle="tab" aria-expanded="true">Consolidated Info</a></li>
			<li><a href=".compare" data-toggle="tab" aria-expanded="true">Yr to Yr Comparison</a></li>
		</ul>
	</div>
	<div class="col-md-4">
	<div class="portlet-input input-inline pull-right">	
		<label class="control-label"> AY Term: </label>
		<select class="form-control input-inline" id="ayterm">
		   <option value="-1" selected disabled> - Select Term - </option>
		   <?php 
		    $ayterm = DB::select("SELECT * FROM ES_AYTerm ORDER BY AcademicYear DESC");
			foreach($ayterm as $r){
			echo '<option value="'.base64_encode($r->TermID).'" '.(($r->Active_OnlineEnrolment==1)?'selected':'').'>'.$r->AcademicYear.' - '.$r->SchoolTerm.'</option>';  
			}
		   ?>
		</select>              			
	</div>
	</div>
    <div class="col-md-12 monitoring">
	    <div class="portlet box green">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-table"></i> Admission Monitoring <b class="termLabel"></b></div>
				<div class="tools">
					<a href="javascript:;" class="print" data-event="monitoring" data-original-title="" title="" style="color:white !important;"><i class="fa fa-print"></i></a>
					<a href="javascript:;" class="reload monRefresh" data-original-title="" title=""></a>
				</div>
			</div>
			<div class="portlet-body">
			    <div class="row">
			    <div class="col-sm-12">
			    <div class="portlet-input input-inline pull-right">	
					<label class="control-label"> Filter : </label>								
					<input type="text" id="monFilter" class="tmp_search form-control input-small input-inline">
					<select id="monLength" class="tmp_length form-control input-xsmall input-inline">
					   <option value="10">10</option>
					   <option value="15">15</option>
					   <option value="20">20</option>
					   <option value="25">25</option>
					   <option value="50">50</option>
					   <option value="100">100</option>
					   <option value="-1">All</option>
					  </select>              			
				</div>
			    <div class="portlet-input input-inline pull-right">	
					<label class="control-label"> Admission Status : </label>
					<select id="monStatus" class="form-control input-inline">
					   <option value="-1">All Status</option>
					   <?php
						$ayterm = DB::select("SELECT * FROM ESv2_AdmissionStatus ORDER BY SeqNo ASC");
						foreach($ayterm as $r){
						echo '<option value="'.base64_encode($r->StatusID).'">'.$r->StatusDesc.'</option>';  
						}
					   ?>
					</select>
                    &nbsp;&nbsp;  					
				</div>
				</div>
				<div class="col-sm-12">
				<div class="table-responsive" id="monTable">
				  @include($views.'table.monitoring')
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
    <div class="col-md-12 total_no hidden">
	    <div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-users"></i> No. Of New Students <b class="termLabel"></b></div>
				<div class="tools">
					<a href="javascript:;" class="print" data-event="applicant" data-original-title="" title="" style="color:white !important;"><i class="fa fa-print"></i></a>
					<a href="javascript:;" class="reload noRefresh" data-original-title="" title=""></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive" id="noApplicant">
				  @include($views.'table.applicant')
				</div>
			</div>
		</div>
	</div>
    <div class="col-md-12 total_no hidden">
	    <div class="portlet box yellow">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-bar-chart-o"></i> No. Of Students <b class="termLabel"></b></div>
				<div class="tools">
					<a href="javascript:;" class="print" data-event="students" data-original-title="" title="" style="color:white !important;"><i class="fa fa-print"></i></a>
					<a href="javascript:;" class="reload noRefresh" data-original-title="" title=""></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive" id="noStudent">
				  @include($views.'table.student')
				</div>
			</div>
		</div>
	</div>
    <div class="col-md-12 cond_info hidden">
	    <div class="portlet box red">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-cogs"></i> Consolidation of Personal Information </div>
				<div class="tools">
					<a href="javascript:;" class="print" data-event="consolidate" data-original-title="" title=""><i class="fa fa-print"></i></a>
					<a href="javascript:;" class="reload condRefresh" data-original-title="" title=""></a>
				</div>
			</div>
			<div class="portlet-body">
			    <div class="row">
			    <div class="col-md-12">
				<div class="portlet-input input-inline pull-right">	
					<label class="control-label"> List : </label>
					<select id="conList" class="form-control input-inline">
					   <option value="-1" selected disabled> - Select List - </option>
					   <option value="parent">Parent Information</option>
					   <option value="parentemp">Parent Employment Info</option>
					   <option value="p_enroll">Parent Info. of Enrolled Students</option>
					   <option value="pe_enroll">Parent Empl.Info of Enrolled Students</option>
					   <option value="parish">Parish Information</option>
					   <option value="siblings">Non Everest Siblings</option>
					   <option value="nation">Nationality</option>
					   <option value="religion">Religion</option>
					   <option value="prevschool">Previous School</option>
					</select>              		              			
				</div>
				<div class="portlet-input input-inline pull-right">	
				    <label class="control-label">Filter</label>
					<input id="conFilter" class="form-control input-inline"/>
				</div>
				
				</div>
				<div class="col-md-12">
				<div class="table-responsive" id="conTable">
				  @include($views.'table.consolidate')
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 compare hidden">
	    <div class="portlet box yellow">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-bar-chart-o"></i> Year To Year Comparison </div>
				<div class="tools">
					<a href="javascript:;" class="print" data-event="compare" data-original-title="" title="" style="color:white !important;"><i class="fa fa-print"></i></a>
					<a href="javascript:;" class="reload compRefresh" data-original-title="" title=""></a>
				</div>
			</div>
			<div class="portlet-body">
			    <div class="row">
				    <div class="col-sm-12">
				    <div class="portlet-input input-inline pull-right">	
						<label class="control-label"> Compare To: </label>
						<select class="form-control input-inline" id="xterm">
						   <option value="-1" selected disabled> - Select Term - </option>
						   <?php 
							$ayterm = DB::select("SELECT * FROM ES_AYTerm ORDER BY AcademicYear DESC");
							foreach($ayterm as $r){
							echo '<option value="'.base64_encode($r->TermID).'" '.(($r->Active_OnlineEnrolment==1)?'selected':'').'>'.$r->AcademicYear.' - '.$r->SchoolTerm.'</option>';  
							}
						   ?>
						</select>              			
					</div>
					</div>
					<div class="col-md-6">
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="fa fa-users"></i> No. Of New Students <b class="termLabel"></b></div>
								<div class="tools">
									<a href="javascript:;" class="reload mainRefresh" data-original-title="" title=""></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-responsive" id="mainApplicant">
								  @include($views.'table.applicant')
								</div>
							</div>
						</div>
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"><i class="fa fa-bar-chart-o"></i> No. Of Students <b class="termLabel"></b></div>
								<div class="tools">
									<a href="javascript:;" class="reload mainRefresh" data-original-title="" title=""></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-responsive" id="mainStudent">
								  @include($views.'table.student')
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="fa fa-users"></i> No. Of New Students <b class="xLabel"></b></div>
								<div class="tools">
									<a href="javascript:;" class="reload xRefresh" data-original-title="" title=""></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-responsive" id="xApplicant">
								  @include($views.'table.applicant')
								</div>
							</div>
						</div>
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="fa fa-bar-chart-o"></i> No. Of Students <b class="xLabel"></b></div>
								<div class="tools">
									<a href="javascript:;" class="reload xRefresh" data-original-title="" title=""></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-responsive" id="xStudent">
								  @include($views.'table.student')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	