<table class="table  table-bordered" id="conTable">
<?php
if(isset($list)){
 $list = $list;
}else{
 $list = '';
}

if(isset($termid)){
$termid = $termid;
}else{
$tmpterm = DB::SELECT("SELECT TOP 1 * FROM ES_AYTerm WHERE Active_OnlineEnrolment=1");
$termid  = (($tmpterm)?($tmpterm[0]->TermID):1);
}

switch($list){
  case 'parent':
    $result = DB::select("SELECT a.FamilyID,Father_Name,Father_Mobile,Father_Email,Mother_Name,Mother_Mobile,Mother_Email,Guardian_Name,Guardian_Mobile,Guardian_Email 
                            FROM ESv2_Admission as a
                      INNER JOIN ESv2_Admission_FamilyBackground as fb ON a.FamilyID=fb.FamilyID
                           WHERE (Father_Name<>'' OR Mother_Name<>'' OR Guardian_Name<>'') AND a.TermID='".$termid."' AND a.AppNo NOT IN (SELECT AppNo FROM ES_Students)
						   ORDER BY a.LastName,a.FamilyID");
	echo '<thead>
	         <tr><th rowspan="2">FamilyID</th>
			     <th colspan="3">Father</th>
			     <th colspan="3">Mother</th>
				 <th colspan="3">Guardian</th></tr>
	         <tr><th>Name</th>
			     <th>Mobile No.</th>
				 <th>E-mail</th>
				 <th>Name</th>
			     <th>Mobile No.</th>
				 <th>E-mail</th>
				 <th>Name</th>
			     <th>Mobile No.</th>
				 <th>E-mail</th></tr>
	      </thead><tbody>';
	//fb.Father_Name,Father_Mobile,Father_Email,Mother_Name,Mother_Mobile,Mother_Email,Guardian_Name,Guardian_Mobile,Guardian_Email	  
	$i=1;
	$previd='';
	foreach($result as $rs){
	 if($previd==$rs->FamilyID){
	   continue;
	 }else{
	   $previd=$rs->FamilyID; 
	 }
     echo '<tr><td>'.$rs->FamilyID.'</td>
	           <td>'.$rs->Father_Name.'</td>
			   <td>'.$rs->Father_Mobile.'</td>
			   <td>'.$rs->Father_Email.'</td>
	           <td>'.$rs->Mother_Name.'</td>
			   <td>'.$rs->Mother_Mobile.'</td>
			   <td>'.$rs->Mother_Email.'</td>
	           <td>'.$rs->Guardian_Name.'</td>
			   <td>'.$rs->Guardian_Mobile.'</td>
			   <td>'.$rs->Guardian_Email.'</td></tr>';	
	 $i++;		   
	}	  
    echo '</tbody>';      					   
    break;
  case 'parentemp':
    $result = DB::select("SELECT DISTINCT a.FamilyID,a.LastName,Father_Name,Father_Occupation,Father_Company,Father_CompanyAddress,Father_CompanyPhone
                                ,Mother_Name,Mother_Occupation,Mother_Company,Mother_CompanyAddress,Mother_CompanyPhone
                            FROM ESv2_Admission as a
                      INNER JOIN ESv2_Admission_FamilyBackground as fb ON a.FamilyID=fb.FamilyID
                           WHERE (Father_Name<>'' OR Mother_Name<>'' OR Guardian_Name<>'') AND a.TermID='".$termid."' AND a.AppNo NOT IN (SELECT AppNo FROM ES_Students)
						   ORDER BY a.LastName,a.FamilyID");
	echo '<thead>
	         <tr><th>FamilyID</th>
			     <th>Parent Name</th>
				 <th>Occupation</th>
				 <th>Company</th>
				 <th>Address</th>
			     <th>Contact No.</th></tr>
	      </thead><tbody>';
	//fb.Father_Name,Father_Mobile,Father_Email,Mother_Name,Mother_Mobile,Mother_Email,Guardian_Name,Guardian_Mobile,Guardian_Email	  
	$i=1;
	$previd = '';
	foreach($result as $rs){
	 if($previd==$rs->FamilyID){
	   continue;
	 }else{
	   $previd=$rs->FamilyID;
	 }
	 if($rs->Father_Name!='' && $rs->Father_Company!=''){
	 echo '<tr><td>'.$rs->FamilyID.'</td>
	           <td>'.$rs->Father_Name.'</td>
	           <td>'.$rs->Father_Occupation.'</td>
			   <td>'.$rs->Father_Company.'</td>
			   <td>'.$rs->Father_CompanyAddress.'</td>
	           <td>'.$rs->Father_CompanyPhone.'</td></tr>';	
	 }
	 if($rs->Mother_Name!='' && $rs->Mother_Company!=''){
	 $i++;
	 echo '<tr><td>'.$rs->FamilyID.'</td>
	           <td>'.$rs->Mother_Name.'</td>
	           <td>'.$rs->Mother_Occupation.'</td>
			   <td>'.$rs->Mother_Company.'</td>
			   <td>'.$rs->Mother_CompanyAddress.'</td>
	           <td>'.$rs->Mother_CompanyPhone.'</td></tr>';	
	 }
     
	 $i++;		   
	}	  
    echo '</tbody>';      					   
    break;
  case 'p_enroll':
    $result = DB::select("SELECT a.FamilyID,Father_Name,Father_Mobile,Father_Email,Mother_Name,Mother_Mobile,Mother_Email,Guardian_Name,Guardian_Mobile,Guardian_Email 
                            FROM ESv2_Admission as a
                      INNER JOIN ESv2_Admission_FamilyBackground as fb ON a.FamilyID=fb.FamilyID
                           WHERE (Father_Name<>'' OR Mother_Name<>'' OR Guardian_Name<>'') AND a.TermID='".$termid."' AND a.AppNo IN (SELECT s.AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo WHERE r.TermID='".$termid."')
						   ORDER BY a.LastName,a.FamilyID");
	echo '<thead>
	         <tr><th rowspan="2">FamilyID</th>
			     <th colspan="3">Father</th>
			     <th colspan="3">Mother</th>
				 <th colspan="3">Guardian</th></tr>
	         <tr><th>Name</th>
			     <th>Mobile No.</th>
				 <th>E-mail</th>
				 <th>Name</th>
			     <th>Mobile No.</th>
				 <th>E-mail</th>
				 <th>Name</th>
			     <th>Mobile No.</th>
				 <th>E-mail</th></tr>
	      </thead><tbody>';
	//fb.Father_Name,Father_Mobile,Father_Email,Mother_Name,Mother_Mobile,Mother_Email,Guardian_Name,Guardian_Mobile,Guardian_Email	  
	$i=1;
	$previd='';
	foreach($result as $rs){
	 if($previd==$rs->FamilyID){
	   continue;
	 }else{
	   $previd=$rs->FamilyID; 
	 }
     echo '<tr><td>'.$rs->FamilyID.'</td>
	           <td>'.$rs->Father_Name.'</td>
			   <td>'.$rs->Father_Mobile.'</td>
			   <td>'.$rs->Father_Email.'</td>
	           <td>'.$rs->Mother_Name.'</td>
			   <td>'.$rs->Mother_Mobile.'</td>
			   <td>'.$rs->Mother_Email.'</td>
	           <td>'.$rs->Guardian_Name.'</td>
			   <td>'.$rs->Guardian_Mobile.'</td>
			   <td>'.$rs->Guardian_Email.'</td></tr>';	
	 $i++;		   
	}	  
    echo '</tbody>';      					   
    break;
  case 'pe_enroll':
    $result = DB::select("SELECT DISTINCT a.FamilyID,a.LastName,Father_Name,Father_Occupation,Father_Company,Father_CompanyAddress,Father_CompanyPhone
                                ,Mother_Name,Mother_Occupation,Mother_Company,Mother_CompanyAddress,Mother_CompanyPhone
                            FROM ESv2_Admission as a
                      INNER JOIN ESv2_Admission_FamilyBackground as fb ON a.FamilyID=fb.FamilyID
                           WHERE (Father_Name<>'' OR Mother_Name<>'' OR Guardian_Name<>'') AND a.TermID='".$termid."' AND a.AppNo IN (SELECT s.AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo WHERE r.TermID='".$termid."')
						   ORDER BY a.LastName,a.FamilyID");
	echo '<thead>
	         <tr><th>FamilyID</th>
			     <th>Parent Name</th>
				 <th>Occupation</th>
				 <th>Company</th>
				 <th>Address</th>
			     <th>Contact No.</th></tr>
	      </thead><tbody>';
	//fb.Father_Name,Father_Mobile,Father_Email,Mother_Name,Mother_Mobile,Mother_Email,Guardian_Name,Guardian_Mobile,Guardian_Email	  
	$i=1;
	$previd = '';
	foreach($result as $rs){
	 if($previd==$rs->FamilyID){
	   continue;
	 }else{
	   $previd=$rs->FamilyID;
	 }
	 if($rs->Father_Name!='' && $rs->Father_Company!=''){
	 echo '<tr><td>'.$rs->FamilyID.'</td>
	           <td>'.$rs->Father_Name.'</td>
	           <td>'.$rs->Father_Occupation.'</td>
			   <td>'.$rs->Father_Company.'</td>
			   <td>'.$rs->Father_CompanyAddress.'</td>
	           <td>'.$rs->Father_CompanyPhone.'</td></tr>';	
	 }
	 if($rs->Mother_Name!='' && $rs->Mother_Company!=''){
	 $i++;
	 echo '<tr><td>'.$rs->FamilyID.'</td>
	           <td>'.$rs->Mother_Name.'</td>
	           <td>'.$rs->Mother_Occupation.'</td>
			   <td>'.$rs->Mother_Company.'</td>
			   <td>'.$rs->Mother_CompanyAddress.'</td>
	           <td>'.$rs->Mother_CompanyPhone.'</td></tr>';	
	 }
     
	 $i++;		   
	}	  
    echo '</tbody>';      					   
    break;
  case 'siblings':
    $result = DB::select("SELECT * FROM ESv2_Admission_Siblings WHERE FullName<>'' AND SchoolAttended<>'EAM' ORDER BY FamilyID");
	echo '<thead>
	         <tr><th>Sibling Name</th>
			     <th>Date Of Birth</th>
				 <th>School Attended</th></tr>
	      </thead><tbody>';
	foreach($result as $rs){
     echo '<tr><td>'.$rs->FullName.'</td>
			   <td>'.date('m-d-Y',strtotime($rs->DateofBirth)).'</td>
			   <td>'.$rs->SchoolAttended.'</td></tr>';	
	}	  
    echo '</tbody>';      		  
    break;
  case 'parish':
    $result = DB::select("SELECT BaptizedIn_Church,BaptizedIn_City,COUNT(*) as Items FROM ESv2_Admission WHERE BaptizedIn_Church IS NOT NULL AND BaptizedIn_Church<>'' AND TermID='".$termid."' GROUP BY BaptizedIn_Church,BaptizedIn_City");
	echo '<thead>
	         <tr><th>Church Name</th>
			     <th>Church Addr</th>
				 <th>No. of Applicant</th></tr>
	      </thead><tbody>';
	foreach($result as $rs){
     echo '<tr><td>'.$rs->BaptizedIn_Church.'</td>
			   <td>'.$rs->BaptizedIn_City.'</td>
			   <td>'.$rs->Items.'</td></tr>';	
	}	  
    echo '</tbody>';      		  
    break;
  case 'nation':
    $result = DB::select("SELECT n.NationalityID,n.Nationality,n.ShortName, COUNT(a.AppNo) as Items FROM ESv2_Admission as a INNER JOIN Nationalities as n ON a.NationalityID=n.NationalityID WHERE a.TermID='".$termid."' GROUP BY n.NationalityID,n.Nationality,n.ShortName");
	echo '<thead>
	         <tr><th>Nationality</th>
			     <th>No. of Applicant</th></tr>
	      </thead><tbody>';
	foreach($result as $rs){
     echo '<tr><td>'.$rs->Nationality.'</td>
			   <td>'.$rs->Items.'</td></tr>';	
	}	  
    echo '</tbody>';      		  
    break;
  case 'religion':
    $result = DB::select("SELECT r.Religion,r.ShortName,COUNT(a.AppNo) as Items FROM ESv2_Admission as a INNER JOIN ES_Religions as r ON a.ReligionID=r.ReligionID WHERE a.TermID='".$termid."' GROUP BY r.Religion,r.ShortName");
	echo '<thead>
	         <tr><th>Religion</th>
			     <th>No. of Applicant</th></tr>
	      </thead><tbody>';
	foreach($result as $rs){
     echo '<tr><td>'.$rs->Religion.'</td>
			   <td>'.$rs->Items.'</td></tr>';	
	}	  
    echo '</tbody>';      		  
    break;
  case 'prevschool':
    $prevdat= '';
    $result = DB::select("SELECT PresentSchool,PresentSchoolAddress,PresentSchoolContactNo,COUNT(*) as Items FROM ESv2_Admission WHERE PresentSchool<>'' AND PresentSchool IS NOT NULL AND TermID='".$termid."' GROUP BY PresentSchool,PresentSchoolAddress,PresentSchoolContactNo ORDER BY PresentSchool,LEN(PresentSchoolAddress) DESC,PresentSchoolContactNo DESC");
	echo '<thead>
	         <tr><th>School</th>
			     <th>Address</th>
			     <th>ContactNo</th>
			     <th>No. of Applicant</th></tr>
	      </thead><tbody>';
	foreach($result as $rs){
	 if($prevdat==$rs->PresentSchool)
	  continue;
	 else
	  $prevdat=$rs->PresentSchool;
     echo '<tr><td>'.$rs->PresentSchool.'</td>
               <td>'.$rs->PresentSchoolAddress.'</td>
               <td>'.$rs->PresentSchoolContactNo.'</td>
			   <td>'.$rs->Items.'</td></tr>';	
	}	  
    echo '</tbody>';      		  
    break;
  default:
    echo '<tbody>
	        <tr><td class="text-center">
			  <i class="fa fa-warning"></i> Select a list to view.
			</td></tr>
	      </tbody>';
    break;  
}
?>
</table>