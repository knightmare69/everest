<table class="table  table-bordered ">
	<thead>
	<tr>
		<th class="text-center" rowspan="2">Level</th>
		<th class="text-center" colspan="3">New</th>
		<th class="text-center" colspan="3">Returnee</th>
		<th class="text-center" rowspan="2">Total</th>
	</tr>
	<tr>
		<th>Male</th>
		<th>Female</th>
		<th>Total</th>
		<th>Male</th>
		<th>Female</th>
		<th>Total</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	  if(isset($termid)){
		$termid = $termid;
	  }else{
		$tmpterm = DB::SELECT("SELECT TOP 1 * FROM ES_AYTerm WHERE Active_OnlineEnrolment=1");
		$termid  = (($tmpterm)?($tmpterm[0]->TermID):1);
	  }
	  
	  $rsnew = DB::select("SELECT (SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalStudent
							  ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND s.TermID='".$termid."' AND s.Gender<>'F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewMale
							  ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND s.TermID='".$termid."' AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewFemale
							  ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND ISNULL(s.TermID,0)<>'".$termid."' AND s.Gender<>'F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldMale
							  ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND ISNULL(s.TermID,0)<>'".$termid."' AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldFemale
							  ,YearLevelCode   
							  ,YearLevelName
						  FROM ESv2_YearLevel as y
						  ORDER BY y.YearLevelID");
						  
	   $nm=0;
	   $nf=0;			  
	   $rm=0;
	   $rf=0;
	   foreach($rsnew as $r){
	    $nm+=$r->TotalNewMale;
	    $nf+=$r->TotalNewFemale;			  
	    $rm+=$r->TotalOldMale;
	    $rf+=$r->TotalOldFemale;
		echo '<tr>
				<td>'.$r->YearLevelName.'</td>
				<td>'.$r->TotalNewMale.'</td>
				<td>'.$r->TotalNewFemale.'</td>
				<td><b>'.($r->TotalNewMale+$r->TotalNewFemale).'</b></td>
				<td>'.$r->TotalOldMale.'</td>
				<td>'.$r->TotalOldFemale.'</td>
				<td><b>'.($r->TotalOldMale+$r->TotalOldFemale).'</b></td>
				<td><strong>'.($r->TotalNewMale+$r->TotalNewFemale+$r->TotalOldMale+$r->TotalOldFemale).'</strong></td>
			  </tr>';
	   }	
	?>	
	</tbody>
	<tfoot><?php 
		echo '<tr>
				<td>Total</td>
				<td>'.$nm.'</td>
				<td>'.$nf.'</td>
				<td><b>'.($nm+$nf).'</b></td>
				<td>'.$rm.'</td>
				<td>'.$rf.'</td>
				<td><b>'.($rm+$rf).'</b></td>
				<td><strong>'.($nm+$nf+$rm+$rf).'</strong></td>
			  </tr>';
	?></tfoot>
  </table>