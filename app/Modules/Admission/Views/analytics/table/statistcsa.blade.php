<?php 
  $heada = '<th rowspan="2">&nbsp;</th>';
  $headb = '';
  $headc = '';
  $stats = array(
				'1'=>array('label'=>'Inquired','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'2'=>array('label'=>'Applied','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'3'=>array('label'=>'Tested','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'4'=>array('label'=>'Admitted','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'5'=>array('label'=>'Denied','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'6'=>array('label'=>'Waiting Pool','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'7'=>array('label'=>'Reserved','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'7'=>array('label'=>'Enrolled','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'8'=>array('label'=>'Not Enrolled','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'9'=>array('label'=>'Re-Enrolled','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
			   ,'10'=>array('label'=>'Withdrawn','1M'=>0,'1F'=>0,'2M'=>0,'2F'=>0,'3M'=>0,'3F'=>0,'4M'=>0,'4F'=>0,'5M'=>0,'5F'=>0,'6M'=>0,'6F'=>0,'7M'=>0,'7F'=>0,'8M'=>0,'8F'=>0,'9M'=>0,'9F'=>0,'10M'=>0,'10F'=>0,'11M'=>0,'11F'=>0,'12M'=>0,'12F'=>0,'13M'=>0,'13F'=>0,'total'=>0)
           );
  
	  if(isset($termid)){
		$termid = $termid;
	  }else{
		$tmpterm = DB::SELECT("SELECT TOP 1 * FROM ES_AYTerm WHERE Active_OnlineEnrolment=1");
		$termid  = (($tmpterm)?($tmpterm[0]->TermID):1);
	  }
	  
	    $rsnew = DB::select("SELECT (SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplicantM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplicantF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND StatusID IN (8,1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalInquireM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND StatusID IN (8,1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalInquireF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND StatusID IN (1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplyM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND StatusID IN (1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplyF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND (StatusID=10 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=10)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalTestM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND (StatusID=10 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=10)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalTestF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND (StatusID=14 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=14)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalWaitM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND (StatusID=14 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=14)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalWaitF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND StatusID=2 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalAdmittedM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND StatusID=2 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalAdmittedF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND StatusID=3 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalDeniedM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND StatusID=3 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalDeniedF
								
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='".$termid."' AND AppNo IN (SELECT AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL ) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalEnrollM
								,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='".$termid."' AND AppNo IN (SELECT AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL ) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalEnrollF
								
								,(SELECT COUNT(*) FROM ESv2_Admission as a INNER JOIN ES_Students as s ON a.AppNo=s.AppNo WHERE a.Gender='M' AND a.TermID='".$termid."' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID)) AND s.StudentNo NOT IN (SELECT StudentNo FROM ES_Registrations WHERE TermID='".$termid."' AND ValidationDate IS NOT NULL)) as TotalNotEnrollM
								,(SELECT COUNT(*) FROM ESv2_Admission as a INNER JOIN ES_Students as s ON a.AppNo=s.AppNo WHERE a.Gender='F' AND a.TermID='".$termid."' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID)) AND s.StudentNo NOT IN (SELECT StudentNo FROM ES_Registrations WHERE TermID='".$termid."' AND ValidationDate IS NOT NULL)) as TotalNotEnrollF

								,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalStudent
								,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND s.TermID='".$termid."' AND s.Gender<>'F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewM
								,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND s.TermID='".$termid."' AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewF
								,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND ISNULL(s.TermID,0)<>'".$termid."' AND s.Gender<>'F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldM
								,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' AND r.ValidationDate IS NOT NULL AND ISNULL(s.TermID,0)<>'".$termid."' AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldF
								
								,(SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=1 AND TermID='".$termid."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE Gender='M'))  as TotalWithM
                                ,(SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=1 AND TermID='".$termid."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE Gender='F'))  as TotalWithF
								
								,y.YearLevelID
								,YearLevelCode   
								,YearLevelName
								,p.ProgID
								,p.ProgCode
								,p.ProgName
						FROM ESv2_YearLevel as y 
						INNER JOIN ES_Programs as p ON y.ProgID=p.ProgID
						ORDER BY y.YearLevelID");	
        if($rsnew){
		    $prev_prog =0;
			$prev_yrlvl=0;
			$colspan   =2;
			foreach($rsnew as $r){
			  if($r->ProgID!=$prev_prog){
			  $heada    = str_replace("<x>",$colspan,$heada);
			  $heada   .= '<th class="text-center" colspan="<x>">'.$r->ProgName.'</th>';
			  $prev_prog=$r->ProgID;
			  $colspan  = 0;
			  }
			  
			  if($r->YearLevelID!=$prev_yrlvl){
			  $headb   .= '<th class="text-center" colspan="2">'.$r->YearLevelName.'</th>';
			  $colspan +=2;
			  }
			  
			  $tmpid = $r->YearLevelID;
			  $stats[1][$tmpid.'M'] += intval($r->TotalInquireM); 
			  $stats[1][$tmpid.'F'] += intval($r->TotalInquireF);
			  $stats[1]['total']    += intval($r->TotalInquireM) + intval($r->TotalInquireF);
			  
			  $stats[2][$tmpid.'M'] += intval($r->TotalApplyM); 
			  $stats[2][$tmpid.'F'] += intval($r->TotalApplyF); 
			  $stats[2]['total']    += intval($r->TotalApplyM) + intval($r->TotalApplyF);
			  
			  $stats[3][$tmpid.'M'] += intval($r->TotalTestM); 
			  $stats[3][$tmpid.'F'] += intval($r->TotalTestF); 
			  $stats[3]['total']    += intval($r->TotalTestM) + intval($r->TotalTestF);
			  
			  
			  $stats[4][$tmpid.'M'] += intval($r->TotalAdmittedM); 
			  $stats[4][$tmpid.'F'] += intval($r->TotalAdmittedF);
			  $stats[4]['total']    += intval($r->TotalAdmittedM) + intval($r->TotalAdmittedF);
			  $stats[5][$tmpid.'M'] += intval($r->TotalDeniedM); 
			  $stats[5][$tmpid.'F'] += intval($r->TotalDeniedF); 
			  $stats[5]['total']    += intval($r->TotalDeniedM) + intval($r->TotalDeniedF);
			  
			  $stats[6][$tmpid.'M'] += intval($r->TotalWaitM); 
			  $stats[6][$tmpid.'F'] += intval($r->TotalWaitF); 
			  $stats[6]['total']    += intval($r->TotalWaitM) + intval($r->TotalWaitF);
			  
			  $stats[7][$tmpid.'M'] += intval($r->TotalEnrollM); 
			  $stats[7][$tmpid.'F'] += intval($r->TotalEnrollF); 
			  $stats[7]['total']    += intval($r->TotalEnrollM) + intval($r->TotalEnrollF);
			  
			  $stats[8][$tmpid.'M'] += intval($r->TotalNotEnrollM); 
			  $stats[8][$tmpid.'F'] += intval($r->TotalNotEnrollF); 
			  $stats[8]['total']    += intval($r->TotalNotEnrollM) + intval($r->TotalNotEnrollF);
			  $stats[9][$tmpid.'M'] += intval($r->TotalOldM); 
			  $stats[9][$tmpid.'F'] += intval($r->TotalOldF); 
			  $stats[9]['total']    += intval($r->TotalOldM) + intval($r->TotalOldF);
			  
			  $stats[10][$tmpid.'M'] += intval($r->TotalWithM); 
			  $stats[10][$tmpid.'F'] += intval($r->TotalWithF); 
			  $stats[10]['total']    += intval($r->TotalWithM) + intval($r->TotalWithF);
			}
			$heada    = str_replace("<x>",$colspan,$heada);
		}						
?>
<table id="table" class="table table-bordered ">
	<thead>
		<tr><?php echo $heada.'<th rowspan="2">TOTAL</th>';?></tr>
		<tr><?php echo $headb;?></tr>
	</thead>
	<tbody>
	<?php 			  
	   foreach($stats as $k=>$r){
		echo '<tr>
				<td>'.$r['label'].'</td>
				<td class="info text-center">'.$r['1M'].'</td>
				<td class="danger text-center">'.$r['1F'].'</td>
				<td class="info text-center">'.$r['2M'].'</td>
				<td class="danger text-center">'.$r['2F'].'</td>
				<td class="info text-center">'.$r['3M'].'</td>
				<td class="danger text-center">'.$r['3F'].'</td>
				<td class="info text-center">'.$r['4M'].'</td>
				<td class="danger text-center">'.$r['4F'].'</td>
				<td class="info text-center">'.$r['5M'].'</td>
				<td class="danger text-center">'.$r['5F'].'</td>
				<td class="info text-center">'.$r['6M'].'</td>
				<td class="danger text-center">'.$r['6F'].'</td>
				<td class="info text-center">'.$r['7M'].'</td>
				<td class="danger text-center">'.$r['7F'].'</td>
				<td class="info text-center">'.$r['8M'].'</td>
				<td class="danger text-center">'.$r['8F'].'</td>
				<td class="info text-center">'.$r['9M'].'</td>
				<td class="danger text-center">'.$r['9F'].'</td>
				<td class="info text-center">'.$r['10M'].'</td>
				<td class="danger text-center">'.$r['10F'].'</td>
				<td class="info text-center">'.$r['11M'].'</td>
				<td class="danger text-center">'.$r['11F'].'</td>
				<td class="info text-center">'.$r['12M'].'</td>
				<td class="danger text-center">'.$r['12F'].'</td>
				<td class="info text-center">'.$r['13M'].'</td>
				<td class="danger text-center">'.$r['13F'].'</td>
				<td class="text-center">'.$r['total'].'</td>
			  </tr>';
	   }	
	?>	
	</tbody>
  </table>