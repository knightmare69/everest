  SELECT (SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplicantM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplicantF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND StatusID IN (8,1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalInquireM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND StatusID IN (8,1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalInquireF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND StatusID IN (1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplyM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND StatusID IN (1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplyF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND (StatusID=10 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=10)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalTestM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND (StatusID=10 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=10)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalTestF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND (StatusID=14 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=14)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalWaitM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND (StatusID=14 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=14)) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalWaitF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND StatusID=2 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalAdmittedM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND StatusID=2 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalAdmittedF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND StatusID=3 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalDeniedM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND StatusID=3 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalDeniedF
		
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='M' AND TermID='1006' AND AppNo IN (SELECT AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL ) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalEnrollM
		,(SELECT COUNT(*) FROM ESv2_Admission WHERE Gender='F' AND TermID='1006' AND AppNo IN (SELECT AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL ) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalEnrollF
		
		,(SELECT COUNT(*) FROM ESv2_Admission as a INNER JOIN ES_Students as s ON a.AppNo=s.AppNo WHERE a.Gender='M' AND a.TermID='1006' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID)) AND s.StudentNo NOT IN (SELECT StudentNo FROM ES_Registrations WHERE TermID='1006' AND ValidationDate IS NOT NULL)) as TotalNotEnrollM
        ,(SELECT COUNT(*) FROM ESv2_Admission as a INNER JOIN ES_Students as s ON a.AppNo=s.AppNo WHERE a.Gender='F' AND a.TermID='1006' AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID)) AND s.StudentNo NOT IN (SELECT StudentNo FROM ES_Registrations WHERE TermID='1006' AND ValidationDate IS NOT NULL)) as TotalNotEnrollF

		,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalStudent
		,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL AND s.TermID='1006' AND s.Gender<>'F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewMale
		,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL AND s.TermID='1006' AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewFemale
		,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL AND ISNULL(s.TermID,0)<>'1006' AND s.Gender<>'F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldMale
		,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='1006' AND r.ValidationDate IS NOT NULL AND ISNULL(s.TermID,0)<>'1006' AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldFemale
		,YearLevelCode   
		,YearLevelName
FROM ESv2_YearLevel as y
ORDER BY y.YearLevelID