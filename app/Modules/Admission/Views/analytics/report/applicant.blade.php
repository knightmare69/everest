<html>
<style>
@page{
  size:  auto;   /* auto is the initial value */
  margin: 0mm;  /* this affects the margin in the printer settings */
}
html{
  background-color: #FFFFFF; 
  margin: 0mm;  /* this affects the margin on the html before sending to printer */
}
body{
  padding:30px; /* margin you want for the content */
  /* width: 210mm; height: 297mm;*/
  height: 210mm; width:300mm;
}

.border-bottom { border-bottom: 1px solid #000 }
.border-total { border-bottom: 2px solid #000; border-top: 1px solid #000; border-bottom-style:double;  }
.border-header { border-bottom: 1px solid #000; border-top: 1px solid #000; }
.border-bottom tr > th{ border-bottom: 1px solid #000 }
.border-top { border-top: 1px solid #000 }
.border-top tr > td{ border-top: 1px solid #000  }
.border { border: 0.5px solid #000; padding-left: 0px; padding-right: 0px; }
.right { text-align: right; }
.bold { font-weight: bold; }
.center { text-align: center; }
.text-center { text-align: center; }
.left { text-align: left; }
.valign { vertical-align: middle; }
.bg { background-color: #EDEBE0; }
.info { background-color: #c4f5eb; }
.danger { background-color: #ed7b9c; }
.bg-green{ background-color: #dff0d8;}
.bg-grey {background-color: #e5e5e5 !important;}
.pad-5 { padding: 10px; }
.autofit{width:1%;white-space:nowrap !important;}
.font-xs{ font-size: 8px; }
.font-sm{ font-size: 10px; }
.font-md{ font-size: 12px; }
.pull-left { left: auto; }
.ucase { text-transform: uppercase !important; }
.vertical-text{
  transform-origin: 0 50%;
  transform: rotate(-90deg); 
  white-space: nowrap; 
  display: block;
  position: absolute;
  bottom: 0;
  left: 50%;
}
.vertical-container{
    position: relative;
    padding: 10px;
    width: 10px !important;
}
.table { border: 1px solid #000;
         border-collapse: collapse;
		 width:200mm;}
.table th { border: 1px solid #000; }
.table td { border: 1px solid #000;
            padding:1mm; }
			
.bg-green{ background-color: #dff0d8;}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button-blue {
  background-color: blue;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.jfixed{
  position: fixed;
  top: 0;
  left: 80%;
}
			
@media print
{    
	.no-print, .no-print *
	{
		display: none !important;
	}
}
</style>
<header>
</header>
<body>
 <?php 
  $ainfo = DB::select("SELECT TOP 1 * FROM ES_AYTerm WHERE TermID='".$termid."'");
 ?>
 <a id="dlink"  style="display:none;"></a>
 <span class="jfixed">
	<input  title="Occupancy S.Y. <?php echo $ainfo[0]->AcademicYear.' '.$ainfo[0]->SchoolTerm;?>" class="button no-print" type="button" onclick="tableToExcel('table', 'name', 'Occupancy S.Y. <?php echo $ainfo[0]->AcademicYear.' '.$ainfo[0]->SchoolTerm;?>.xls')" value="Export to Excel">
	<input  title="Occupancy S.Y. <?php echo $ainfo[0]->AcademicYear.' '.$ainfo[0]->SchoolTerm;?>" class="button-blue no-print" type="button" onclick="window.print();" value="Print">
 </span>
 <div style="position:absolute;">
 <img src="<?php echo url().'/assets/system/media/images/report_title_v2.png';?>" width="50%"/><br/>
 </div>
 <div style="position:absolute;top:40mm;padding-left:28mm;margin-top:-11mm;">
 <strong>Occupancy</strong><br/>
 <strong>S.Y. <?php echo $ainfo[0]->AcademicYear.' '.$ainfo[0]->SchoolTerm;?></strong>
 </div>
 <div style="position:absolute;top:40mm;">
 <br/><br/>
 @include($views.'table.statistcsa')
 </div>
</body>
<script type="text/javascript">
//window.print();
function fnExcelReport(id_table) {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById(id_table); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
      
            var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name, filename) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
    
                document.getElementById("dlink").href = uri + base64(format(template, ctx));
                document.getElementById("dlink").download = filename;
                document.getElementById("dlink").click();
    
            }
        })()
    
</script>
</html>