<html>
<style>
@page{
  <?php
   switch($list){
     case 'parent':
     case 'parentemp':
     case 'sibling':
       echo "size:  landscape;";	
	   echo "transform: scale(0.8);
             transform-origin: 0 0;";
	   break;
	 default: 
	   echo "size:  auto;";
	   break;
   }
  ?>
  margin: 0mm;  /* this affects the margin in the printer settings */
}
html{
  background-color: #FFFFFF; 
  margin: 0mm;  /* this affects the margin on the html before sending to printer */
}
body{
  padding:30px; /* margin you want for the content */
  <?php
   switch($list){
     case 'parent':
     case 'parentemp':
     case 'sibling':
       echo "width: 300mm; height:210mm;";	
	   break;
	 default: 
	   echo "width: 210mm; height:300mm;";
	   break;
   }
  ?>
}

.border-bottom { border-bottom: 1px solid #000 }
.border-total { border-bottom: 2px solid #000; border-top: 1px solid #000; border-bottom-style:double;  }
.border-header { border-bottom: 1px solid #000; border-top: 1px solid #000; }
.border-bottom tr > th{ border-bottom: 1px solid #000 }
.border-top { border-top: 1px solid #000 }
.border-top tr > td{ border-top: 1px solid #000  }
.border { border: 0.5px solid #000; padding-left: 0px; padding-right: 0px; }
.right { text-align: right; }
.bold { font-weight: bold; }
.center { text-align: center; }
.left { text-align: left; }
.valign { vertical-align: middle; }
.bg { background-color: #EDEBE0; }
.bg-green{ background-color: #dff0d8;}
.bg-grey {background-color: #e5e5e5 !important;}
.pad-5 { padding: 10px; }
.autofit{width:1%;white-space:nowrap !important;}
.font-xs{ font-size: 8px; }
.font-sm{ font-size: 10px; }
.font-md{ font-size: 12px; }
.pull-left { left: auto; }
.ucase { text-transform: uppercase !important; }
.vertical-text{
  transform-origin: 0 50%;
  transform: rotate(-90deg); 
  white-space: nowrap; 
  display: block;
  position: absolute;
  bottom: 0;
  left: 50%;
}
.vertical-container{
    position: relative;
    padding: 10px;
    width: 10px !important;
}
.table { border: 1px solid #000;
         border-collapse: collapse;
		 width:100%;}
.table th { border: 1px solid #000; }
.table td { border: 1px solid #000;
            padding:1mm; }
</style>
<header>
</header>
<body>
 <div style="position:absolute;">
 <img src="<?php echo url().'/assets/system/media/images/report_title_v2.png';?>" width="50%"/><br/>
 </div>
 <div style="position:absolute;top:40mm;padding-left:25mm;margin-top:-11mm;">
 <strong>Consolidated Information</strong><br/>
 <strong></strong>
 </div>
 <div style="position:absolute;top:40mm;">
 <br/><br/>
 @include($views.'table.consolidate')
 </div>
</body>
<script type="text/javascript">
window.print();
</script>
</html>