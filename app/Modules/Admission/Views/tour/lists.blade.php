<!-- <div class="portlet-body">
	<div class="row">
		<div class="col-md-12">
			<div class="form-body">
				<div class="col-md-12">
					<h4 class="form-section">List of Inquiries</h4>
					@include($views.'tables.lists')
				</div> -->
				<!--/span-->
<!-- 			</div>
		</div>
	</div>
</div> -->


<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift font-green-sharp"></i>
           	<span class="caption-subject font-green-sharp bold">Lists of Inquiries</span>
           	<span class="caption-helper">&nbsp;This module allows you to manage inquiries...</span>
		</div>
		<div class="actions">
		</div>
	</div>
	<div class="portlet-body">
		<div class="row margin-bottom-20 filter">
		    <div class="form-group">
		    	<div class="col-md-2" style="padding-right: 5px !important;width: 100px !important;">
					<p style="margin-top: 3px !important;font-size: 14px;">School Year<p/>
				</div>	
		        <div class="col-md-2" style="padding: 0 0 0 0% !important;width: 18%;">
					<select class="form-control form-filter input-sm" name="schoolYear" id="schoolYear">
    					@foreach(App\Modules\Setup\Models\AcademicYearTerm::where('Hidden',0)->orderBy('AcademicYear','DESC')->get() as $row)
    					<option  {{  (($row->Active_OnlineEnrolment==1) ? 'selected' : '') }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear . ' ' . $row->SchoolTerm }}</option>
    					@endforeach
					</select>
		        </div>
		        <div class="col-md-2" style="padding-right: 5px !important;width: 90px !important;">
					<p style="margin-top: 3px !important;font-size: 14px;">Year Level<p/>
				</div>	
		        <div class="col-md-2" style="padding: 0 0 0 0% !important;">
			        <select class="form-control form-filter input-sm" name="gradeLevel" id="gradeLevel">
						<option value="0-0">All Year Levels</option>
						@foreach(App\Modules\Admission\Models\Admission::getYearLevelList() as $row)
						<option {{  Request::get('gradeLevel') == encode($row->YLID_OldValue) ? 'selected' : ''  }} value="{{ encode($row->YLID_OldValue).'-'.($row->ProgClass < 50 ? 0 : 1) }}">{{ $row->YearLevelName }}</option>
						@endforeach
					</select>
		        </div>
		        <div class="col-md-2" style="padding-right: 5px !important;width: 70px !important;">
					<p style="margin-top: 3px !important;font-size: 14px;">Quarter<p/>
				</div>
		        <div class="col-md-2" style="padding: 0 0 0 0% !important;">
			        <select class="form-control form-filter input-sm" name="quarter" id="quarter">
						<option value="0-0">All Quarter</option>
						@foreach($quarters as $q => $label)
						<option value="{{ $q }}">{{ $label }}</option>
						@endforeach
					</select>
				</div>
		        <div class="col-md-2 hide" style="padding: 0 0 0 1% !important;width: 90px !important;">
		            <button class="btn btn-sm green-haze filter-submit" id="btnFilter"><i class="fa fa-search"></i> Filter&nbsp;&nbsp;</button>
		        </div>
		       <!--  <div class="col-md-2" style="padding: 0 0 0 0% !important;width: 90px !important;">
		            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset&nbsp;&nbsp;</button>
		        </div> -->
		    </div> 
		</div>
		<div class="row">
				@include($views.'tables.lists')
			</div>			<div class="col-md-12">

		</div>
	</div>
</div>
@include('Admission.Views.listing.modals.modal_email')

	