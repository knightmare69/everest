<table class="table table-striped table-bordered table-hover table_scroll" id="table-nonsched" style="cursor: pointer;">
    <thead>
        <tr><th colspan="6">
		        <button type="button" onclick="Batch.addApplicant()" id="add-applicant" data-tab-open="" class="btn btn-primary btn-sm pull-right" style="margin-top:5px;">
					<i class="fa fa-check-square"></i> Add To Schedule
				</button>
		    </th>
		</tr>
        <tr>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Applicant Name</th>
            <th>Year Level</th>
            <th>School Term</th>
            <th width="10%">Type</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;  ?>
        @foreach($data as $row)           
        <tr data-id="{{ $row->id }}"  >
            <td class="font-xs autofit bold">{{$i++}}.</td>
            <td width="3%">
                <input type="checkbox" class="chk-nonsched">
            </td>
            <td class="">{{ $row->lname.', '.$row->fname }}</td>
            <td class="">{{ ucfirst($row->YearLevel) }}</td>
            <td class="">{{ $row->SchoolTerm.' - '.$row->AcademicYear }}</td>
            <td class="">{{ ucfirst($row->type) }}</td>
        </tr>
        @endforeach
        @if(count($data) <= 0)
        <tr>
            <td colspan="6">No Records available.</td>
        </tr>
        @endif
    </tbody>
</table>