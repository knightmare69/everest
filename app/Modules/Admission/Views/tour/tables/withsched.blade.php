<table class="table table-striped table-bordered table-hover table_scroll" style="cursor: pointer;">
    <thead>
        <tr class="hidden">
            <th colspan="5">
                <a href="javascript:;" class="btn btn-sm btn-success" onclick="Batch.emailBlast()"><i class="fa fa-envelope"></i> Email Blast</a>
            </th>
        </tr>
        <tr>
            <th>Option</th>
            <th>Applicant Name</th>
            <th>Year Level</th>
            <th>School Term</th>
            <th width="10%">Type</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;  ?>
        @foreach($data as $row)           
        <tr data-id="{{ $row->id }}">
            <td class=""><button type="button" class="btn blue confirm"><i class="fa fa-envelope"></i>
			             <button type="button" class="btn red remove"><i class="fa fa-times"></i></button></td>
            <td class="">{{ $row->lname.', '.$row->fname }}</td>
            <td class="">{{ ucfirst($row->YearLevel) }}</td>
            <td class="">{{ $row->SchoolTerm.' - '.$row->AcademicYear }}</td>
            <td class="">{{ ucfirst($row->type) }}</td>
        </tr>
        @endforeach
        @if(count($data) <= 0)
        <tr>
            <td colspan="5">No Records available.</td>
        </tr>
        @endif
    </tbody>
</table>