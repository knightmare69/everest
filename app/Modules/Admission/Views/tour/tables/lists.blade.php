<form id="FormTableApplicants" method='POST' class="hide">
	<div class="form-group">
		<label class="control-label"><p class="field-filter">Is Higher Level</p></label>
			<input type="checkbox" class="form-control" id="IsHigherLevel">
	</div>	
</form>
<table class="table table-striped table-bordered table-hover" id="table-inquiries">
	<thead>
		<tr class="heading">
			<th width="3%">
				<small>Current Status</small>
			</th>
			<th width="9%">
				<small>Option</small>
			</th>
			<th width="2%">
				<small>School Year</small>
			</th>
			<th width="2%">
				<small>Year Level</small>
			</th>
			<th width="3%">
				<small>Name of Child</small>
			</th>
			<th width="2%">
				<small>Sex of Child</small>
			</th>
			<th width="2%">
				<small>Birthdate of Child</small>
			</th>
			<th width="3%">
				<small>Name of Parent</small>
			</th>
			<th width="2%">
				<small>Email</small>
			</th>
			<th width="2%">
				<small>Contact No.</small>
			</th>
		</tr>
	<thead>
	<tbody>
	</tbody>
</table>
