
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-child"></i> Phone Inquiry</div>
		<div class="actions btn-set">
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-body text-left">
					<div class="col-md-12">
						<form action="#" class="form-horizontal inquiry-form" id="FormTour">
						@include($views.'forms.form-parent',['status' => $status])
						</form>
						<form action="#" class="form-horizontal inquiry-app-form" style="display: none" id="FormTourApp">
						@include($views.'forms.form-child',['status' => $status])
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
