<input type="hidden" id="gradeProgramID" name="gradeProgramID" value="">
<div class="form-group">
	<label class="control-label col-md-3">&nbsp;</label>
	<div class="icheck-inline">
		<input type="text" class="form-control hide not-required do-not-clear do-not-review" name="IsHigherLevel" id="IsHigherLevel" value="{{ AppConfig()->isHigherLevel() }}">
		<label><input type="radio" name="EducLevel" value="basic" checked class="icheck EducLevel" data-radio="iradio_square-grey"> Basic Education </label>
		<label><input type="radio" name="EducLevel" value="senior" class="icheck EducLevel" data-radio="iradio_square-grey"> Senior Highschool</label>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">School Year</label>
	<div class="col-md-8">
		<?php $i = 0; ?>
		<select class="form-control" name="schoolYear" id="schoolYear">
			<option value="">- Select -</option>
			@foreach(App\Modules\Setup\Models\AcademicYearTerm::where('Hidden',0)->orderBy('AcademicYear','DESC')->get() as $row)
			<option  {{  $i++ <= 0 ? 'selected' : '' }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear }} {{ $row->SchoolTerm }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">School Campus</label>
	<div class="col-md-8">
		<select class="form-control {{ AppConfig()->isHigherLevel() ? 'not-required' : '' }}" name="schoolCampus" id="schoolCampus">
			<option value="">- Select -</option>
			@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
			<option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Application type</label>
	<div class="col-md-8">
		<select class="form-control" name="applicationType" id="applicationType">
			<option value="">- Select -</option>
			@foreach(App\Modules\Admission\Models\Admission::getApplicationType() as $row)
				@if(isVisibleAppType($row->AppTypeID))
				<option {{ $row->SeqNo == 1 ? 'selected' : '' }} value="{{ encode($row->AppTypeID) }}">{{ $row->ApplicationType }}</option>
				@endif
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Year Level</label>
	<div class="col-md-8">
		<input type="hidden" class="form-control not-required" id="SetProgClass" name="SetProgClass" value="">
		<input type="hidden" class="form-control not-required" id="SetGradeLevel" name="SetGradeLevel" value="">
		<select class="form-control" name="gradeLevel" id="gradeLevel">
			<option value="">- Select -</option>
			@foreach(App\Modules\Admission\Models\Admission::getYearLevelList() as $row)
				<option value="{{ encode($row->YearLevelID) }}">{{ $row->YearLevelName }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group majorWrapper">
	<label class="control-label col-md-3">Strands</label>
	<div class="col-md-8">
		<select class="form-control" name="ProgramMajor" id="ProgramMajor">
            <option value="0">- Select -</option>
            @foreach(App\Modules\Setup\Models\ProgramMajors::selectRaw('*, fn_MajorName(MajorDiscID) As major')->where('ProgID',29)->get() as $r)
			<option value="{{ encode($r->MajorDiscID) }}">{{ $r->major }}</option>
			@endforeach
            
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Schedule Date</label>
	<div class="col-md-8">
		<input type="hidden" readonly class="form-control not-required" name="ScheduleDateID" id="ScheduleDateID">
		<select class="form-control not-required" name="ScheduleDate" id="ScheduleDate">
			<option value="">- Select -</option>
		</select>
	</div>
</div>