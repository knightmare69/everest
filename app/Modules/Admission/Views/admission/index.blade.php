<style>
.tabbable-line > .nav-tabs > li.active > a {
	background-color: #ccc !important;
	color: #fff !important;
}
.answer{
  border: 1px solid #e5e5e5 !important;
  border-color: #e5e5e5 !important;
}
</style>
<?php
	// function getStatusID() {
	// 	 return 
	// 	 Request::get('AppNo') ?
	// 	 App\Modules\Admission\Models\Admission
	//  	::select('StatusID')
	//  	->where('AppNo',Request::get('AppNo'))
	//  	->pluck('StatusID')
	//  	: '';
	// }

    function isVisibleAppType($id) {
    	if (in_array($id,AppConfig()->parentAppTypeIDs())) {
    		return true;
    	}
    	return false;
    }
	
	$inquiry  = DB::select("SELECT TOP 1 i.*  
							  FROM ES_inquiry as i
						INNER JOIN ES_inquiry_parent as ip ON i.InquiryParentID=ip.id
						INNER JOIN ESv2_Users as u ON ip.FamilyID=u.FamilyID
						 LEFT JOIN ESv2_Admission as a ON CONVERT(VARCHAR(250),i.lname)=CONVERT(VARCHAR(250),a.LastName) AND ip.FamilyID=a.FamilyID
							 WHERE u.UserIDX='".getuserID()."' AND i.AppNo='' 
						  ORDER BY i.id DESC");
	
	$inquiry  = ((count($inquiry)>0)?$inquiry[0]:false);
?>
<div class="portlet light bg-inverse">
	<div class="portlet-title hide">
		<div class="caption">
			<i class="fa fa-edit"></i> Create an Application
		</div>
		<div class="tools">
			
		</div>
	</div>
	<div class="portlet-body clearfix" id="admissionMainTab">
		<div class="col-md-2 col-sm-2 col-xs-2 hide">
			@include($views.'sub.left')
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12"> 
			<div class="tab-content mainTabContent">
				<div class="row">
		            <div class="col-md-12">
		                @include('errors.event')
		            </div>
		        </div>
				@include($views.'sub.right')
				<div class="form-actions">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-offset-4 col-md-9">
									<button type="submit" class="btn bg-grey-cascade hide" id="btnPrev">Previous</button>
									<button type="submit" class="btn green" id="btnNext">Proceed to next step</button>
									<?php if(!isParentOrStudents()){?>
									 <button type="submit" class="btn bg-yellow-casablanca final-button hide" id="btnSubmitTemp">Save Application</button>
									 <button type="submit" class="btn green hide final-button hide" id="btnSubmit">Submit Application</button>
									 <button type="submit" class="btn btn-primary hide" id="btnNewApp">Create New Application</button>
									<?php
									}else{
									 echo '<button type="submit" class="btn green hide final-button hide" id="btnSubmit">Submit Application</button>';	
									}
									?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>