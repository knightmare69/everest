<form action="#" id="formSignatures" class="form-horizontal form-bordered">
	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Parent Initials</label>
					<div class="col-md-8">
						<input type="text" class="form-control" placeholder="Initial" name="ParentInitial" value="" data-minlength="3">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Date</label>
					<div class="col-md-9">
						<input type="text" readonly class="form-control do-not-clear date-picker" data-date-format="mm/dd/yyyy" placeholder="" name="ParentInitialDate" value="{{ date('m/d/Y') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
	</div>
</form>