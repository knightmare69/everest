<?php 
	$data = App\Modules\Admission\Models\FatherBackground::where('FamilyID',getGuardianFamilyID( decode( Request::get('AppNo')) ))->get();
	$data = isset($data[0]) ? $data[0] : [];

	$admission = App\Modules\Admission\Models\Admission::getAdmission(decode(Request::get('AppNo')),['Family_Correspondence','Family_HealthStatus','Family_Situation','Family_PaymentResponsible']);
	$admission = isset($admission[0]) ? $admission[0] : [];	
?>	
<form action="#" class="form-horizontal" id="formFather">
	<div class="form-body">
		<legend style="font-size: 15px;font-weight: bold;">Father Information</legend>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Full Name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherName" value="{{ getObjectValue($data,'Father_Name') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Employer</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-building-o"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherCompany" value="{{ getObjectValue($data,'Father_Company') }}">
						</div>
					</div>
				</div>
			</div>
		</div> 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Date of Birth</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker" data-date-format="mm/dd/yyyy" name="FatherDateOfBirth" value="{{ setDateFormat(getObjectValue($data,'Father_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Position</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-credit-card"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherOccupation" value="{{ getObjectValue($data,'Father_Occupation') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Civil Status</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control" name="FatherMarital" id="FatherMarital">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
							<option {{ getObjectValue($data,'Father_MaritalID') == $row->StatusID ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->CivilDesc }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Type of Business</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-cube"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherBusinessType" value="{{ getObjectValue($data,'Father_BusinessType') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Home Address</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-home"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherAddress" value="{{ getObjectValue($data,'Father_Address') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Work Address</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherCompanyAddress" value="{{ getObjectValue($data,'Father_CompanyAddress') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Mobile Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-tablet"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherMobile" value="{{ getObjectValue($data,'Father_Mobile') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Work Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherCompanyPhone" value="{{ getObjectValue($data,'Father_CompanyPhone') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Home Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherTelNo" value="{{ getObjectValue($data,'Father_TelNo') }}">
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Email</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-envelope-o"></i>
							<input {{ disabledIfAdmitted() }} type="email" class="form-control" name="FatherEmailAddress" value="{{ getObjectValue($data,'Father_Email') }}">
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4" style="padding-left: unset;padding-right: unset;">Educational Attainment</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-mortar-board"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FatherEducation" value="{{ getObjectValue($data,'Father_EducAttainment') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Religion</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2" name="FatherReligion" id="FatherReligion">
							@foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
							@if($row->ReligionID != 31 && $row->Inactive != 1)
								<option {{ getObjectValue($data,'Father_ReligionID') == $row->ReligionID ? 'selected' : '' }} {{ ($row->isDefault && getObjectValue($data,'Father_ReligionID') == '') ? 'selected' : '' }} value="{{ encode($row->ReligionID) }}">{{ $row->Religion }}</option>
							@endif
							@endforeach
							<option value="{{ encode(31) }}">Not Applicable</option>
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addFatherReligion" value="" placeholder="Please specify"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Nationality</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2" name="FatherNationality" id="FatherNationality">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{ getObjectValue($data,'Father_NationalityID') == $row->NationalityID ? 'selected' : '' }} {{ ($row->NationalityID == 1 && getObjectValue($data,'Father_NationalityID') == '') ? 'selected' : '' }} value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addFatherNationality" value="" placeholder="Please specify"/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Citizenship</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2" name="FatherCitizenship" id="FatherCitizenship">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{ getObjectValue($data,'Father_CitizenshipID') == $row->NationalityID ? 'selected' : '' }} {{ ($row->NationalityID == 1 && getObjectValue($data,'Father_CitizenshipID') == '') ? 'selected' : '' }} value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addFatherCitizenship" value="" placeholder="Please specify"/>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</form>

<form action="#" class="form-horizontal" id="formMother">
	<div class="form-body">
		<legend style="font-size: 15px;font-weight: bold;">Mother Information</legend>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Full Name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherName" value="{{ getObjectValue($data,'Mother_Name') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Employer</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-building-o"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherCompany" value="{{ getObjectValue($data,'Mother_Company') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Date of Birth</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker" data-date-format="mm/dd/yyyy" name="MotherDateOfBirth" value="{{ setDateFormat(getObjectValue($data,'Mother_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Position</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-credit-card"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherOccupation" value="{{ getObjectValue($data,'Mother_Occupation') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Civil Status</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control" name="MotherMarital" id="MotherMarital">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
							<option {{ getObjectValue($data,'Mother_MaritalID') == $row->StatusID ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->CivilDesc }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Type of Business</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-cube"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherBusinessType" value="{{ getObjectValue($data,'Mother_BusinessType') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Home Address</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-home"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherAddress" value="{{ getObjectValue($data,'Mother_Address') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Work Address</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherCompanyAddress" value="{{ getObjectValue($data,'Mother_CompanyAddress') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Mobile Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-tablet"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherMobile" value="{{ getObjectValue($data,'Mother_Mobile') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Work Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherCompanyPhone" value="{{ getObjectValue($data,'Mother_CompanyPhone') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Home Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherTelNo" value="{{ getObjectValue($data,'Mother_TelNo') }}">
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Email</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-envelope-o"></i>
							<input {{ disabledIfAdmitted() }} type="email" class="form-control" name="MotherEmailAddress" value="{{ getObjectValue($data,'Mother_Email') }}">
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4" style="padding-left: unset;padding-right: unset;">Educational Attainment</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-mortar-board"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="MotherEducation" value="{{ getObjectValue($data,'Mother_EducAttainment') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Religion</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2" name="MotherReligion">
							@foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
							@if($row->ReligionID != 31 && $row->Inactive != 1)
								<option {{ (getObjectValue($data,'Mother_ReligionID') == $row->ReligionID) ? 'selected' : '' }} {{ ($row->isDefault && getObjectValue($data,'Mother_ReligionID') == '') ? 'selected' : '' }} value="{{ encode($row->ReligionID) }}">{{ $row->Religion }}</option>
							@endif
							@endforeach
							<option value="{{ encode(31) }}">Not Applicable</option>
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addMotherReligion" value=""/>
					</div>
				</div>
			</div>
		</div>
		<div class="row"> 
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Nationality</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2" name="MotherNationality" id="MotherNationality">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{ getObjectValue($data,'Mother_NationalityID') == $row->NationalityID ? 'selected' : '' }} {{ ($row->NationalityID == 1 && getObjectValue($data,'Mother_NationalityID') == '') ? 'selected' : '' }} value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addMotherNationality" value=""/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Citizenship</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2" name="MotherCitizenship" id="MotherCitizenship">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{ getObjectValue($data,'Mother_CitizenshipID') == $row->NationalityID ? 'selected' : '' }} {{ ($row->NationalityID == 1 && getObjectValue($data,'Mother_CitizenshipID') == '') ? 'selected' : '' }} value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addMotherCitizenship" value=""/>
					</div>
				</div>
			</div>
		</div>
		<legend></legend>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Date of Marriage</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker not-required" data-date-format="mm/dd/yyyy" name="DateOfMarriage" value="{{ setDateFormat(getObjectValue($data,'FatherMother_MarriageDate'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4" style="padding:unset;">To whom should admission correspondence be sent?</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control" name="FamilyCorrespondence">
							<option value="">Select...</option>
							<option {{ getObjectValue($admission,'Family_Correspondence') == 'Father' ? 'selected' : '' }} value="Father">Father</option>
							<option {{ getObjectValue($admission,'Family_Correspondence') == 'Mother' ? 'selected' : '' }} value="Mother">Mother</option>
							<option {{ getObjectValue($admission,'Family_Correspondence') == 'Both' ? 'selected' : '' }} value="Both">Both</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4" style="padding:unset;">Emergency Contact Person if parents cannot be reached</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="EmergencyContactName" value="{{ getObjectValue($data,'EmergencyContact_Name') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4" style="padding-left:unset;padding-right:unset;">Relationship to applicant</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-child"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="EmergencyContactRelationship" value="{{ getObjectValue($data,'EmergencyContact_Relationship') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Address</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-home"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="EmergencyContactAddress" value="{{ getObjectValue($data,'EmergencyContact_Address') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Home Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="EmergencyContactTelNo" value="{{ getObjectValue($data,'EmergencyContact_TelNo') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Email</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-envelope-o"></i>
							<input {{ disabledIfAdmitted() }} type="email" class="form-control" name="EmergencyContactEmail" value="{{ getObjectValue($data,'EmergencyContact_Email') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Mobile Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-tablet"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="EmergencyContactMobile" value="{{ getObjectValue($data,'EmergencyContact_Mobile') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Work Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="EmergencyContactCompanyPhone" value="{{ getObjectValue($data,'EmergencyContact_CompanyPhone') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form action="#" class="form-horizontal" id="formGuardian">
	<div class="form-body">
		<legend style="font-size: 15px;font-weight: bold;">Guardian Information</legend>
		<div class="row">
			<div class="col-md-6">
				<!-- <div class="form-group">
					<label class="control-label col-md-3">Guardian</label>
					<div class="col-md-9">
						<div class="radio-list" >
					        <label class="radio-inline"><div class="radio"><input type="radio" name="optguardian" value="father" checked></div> Father</label>
							<label class="radio-inline"><div class="radio"><input type="radio" name="optguardian" value="mother" ></div> Mother</label>
							<label class="radio-inline"><div class="radio"><input type="radio" name="optguardian" value="other" ></div> Other</label>
					  	</div>
					</div>
				</div> -->
				<div class="form-group">
					<label class="control-label col-md-3">Full Name</label>
					<div class="col-md-9">
						<input {{ disabledIfAdmitted() }} type="text" class="form-control" placeholder="Name" name="name" value="{{ getObjectValue($data,'Guardian_Name') }}">
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-3">Living with</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-family"></i>
							<select {{ disabledIfAdmitted() }} class="form-control not-required" name="livingWith" id="livingWith">
								<option value="">Select...</option>
								@foreach(App\Modules\Admission\Models\GuardianBackround::getLivingWith() as $key => $label)
								<option {{ getObjectValue($data,'Guardian_LivingWith') == $key ? 'selected' : '' }} value="{{ $key }}">{{ $label }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-3">Parent Marital</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-family"></i>
							<select {{ disabledIfAdmitted() }} class="form-control not-required" name="marital" id="marital">
								<option value="">Select...</option>
								@foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
								<option {{ getObjectValue($data,'Guardian_ParentMaritalID') == $row->StatusID ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->CivilDesc }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-3">If others</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="livingWithOther" id="livingWithOther"  value="{{ getObjectValue($data,'Guardian_LivingWith') == '4' ? App\Modules\Admission\Models\GuardianBackround::getLivingWith(getObjectValue($data,'Guardian_LivingWith')) : ''  }}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Date of Birth</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker" data-date-format="mm/dd/yyyy" name="GuardianDateOfBirth" value="{{ setDateFormat(getObjectValue($data,'Guardian_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Telephone</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="telephone" value="{{ getObjectValue($data,'Guardian_TelNo') }}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Mobile</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-mobile"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="mobile" value="{{ getObjectValue($data,'Guardian_Mobile') }}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Email</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-envelope-o"></i>
							<input {{ disabledIfAdmitted() }} type="email" class="form-control" name="email" value="{{ getObjectValue($data,'Guardian_Email') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-12 photo-message center {{ !isParent() ? 'hide' : '' }}"><small class="font-red">Photo is required</small></label>
					<label class="control-label col-md-12 center">Photo <small>(<b>JPG, PNG Only</b>)</small></label>
					<div class="col-md-12 center">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
								<img src="{{ url('general/getGuardianPhoto?FamilyID='.encode(getGuardianFamilyID(decode( Request::get('AppNo'))))) }}" id="GuardianPhotoThumbnail" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
							</div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input {{ disabledIfAdmitted() }} type="file" name="file" accept="image/x-png, image/jpeg" id="GuardianPhoto" class="GuardianPhoto {{ !isParent() ? 'not-required' : '' }}">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">
							<small>NOTE!</small> </span>
							&nbsp;<small>Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+, Opera11.1+ and FireFox6.0+. In older browsers the filename is shown instead.</small>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Address</h4>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Resident</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="resident" value="{{ getObjectValue($data,'Guardian_Address') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Street</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="street" value="{{ getObjectValue($data,'Guardian_Street') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Barangay</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="barangay" value="{{ getObjectValue($data,'Guardian_Barangay') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Country</label>
					<div class="col-md-9">
						<select {{ disabledIfAdmitted() }} class="form-control" name="GuardianCountry" id="GuardianCountry">
						@foreach(App\Modules\Setup\Models\Country::get() as $row)
							<option {{ trim(getObjectValue($data,'Guardian_CountryCode')) == '' ? (strtoupper($row->Code) == defaultCountry()) ? 'selected' : '' : '' }} {{ getObjectValue($data,'Guardian_CountryCode') == $row->Code ? 'selected' : '' }} value="{{ $row->Code }}">{{ $row->Country }}</option>
						@endforeach
						</select>						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">City <br /><input {{ disabledIfAdmitted() }} type="checkbox" name="isOtherCity" id="isOtherCity">&nbsp;<i><small>If Other</small></i></label>
					<div class="col-md-9">
						<input type="hidden" class="form-control not-required CitySel" id="GuardianCitySel" name="GuardianCitySel" value="{{ getObjectValue($data,'Guardian_CityID') }}">
						<select {{ disabledIfAdmitted() }} class="form-control select2 CitySelOption {{ getObjectValue($data,'Guardian_isOtherCity') ? 'hide not-required' : '' }}" name="GuardianCity" id="GuardianCity">
							<option value="">Select...</option>
						</select>		
						<input {{ disabledIfAdmitted() }} type="text" name="otherCity" id="otherCity" value="{{ getObjectValue($data,'Guardian_OtherCity') }}" class="form-control {{ getObjectValue($data,'Guardian_isOtherCity') ? '' : 'hide not-required' }}">							
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-12">Person financially responsible for tuition payments</label>
			<div class="col-md-6">
				<div class="input-icon">
					<i class="fa fa-user"></i>
					<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="paymentsResponsible" value="{{ getObjectValue($admission,'Family_PaymentResponsible') }}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Billing Address <small>(<i>Pls check if same addess as above.</i>)</small> <input {{ disabledIfAdmitted() }} type="checkbox" class="form-control" id="CopyAddress"></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Resident</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="BillingResident" value="{{ getObjectValue($data,'Guardian_Billing_Address') }}">
						</div>
					</div>
						
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Street</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="BillingStreet" value="{{ getObjectValue($data,'Guardian_Billing_Street') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Barangay</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="BillingBrangay" value="{{ getObjectValue($data,'Guardian_Billing_Barangay') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Country</label>
					<div class="col-md-9">
						<select {{ disabledIfAdmitted() }} class="form-control" name="BillingCountry" id="BillingCountry">
						@foreach(App\Modules\Setup\Models\Country::get() as $row)
							<option {{ trim(getObjectValue($data,'Guardian_Billing_CountryCode')) == '' ? (strtoupper($row->Code) == defaultCountry()) ? 'selected' : '' : '' }} {{ getObjectValue($data,'Guardian_Billing_CountryCode') == $row->Code ? 'selected' : '' }} value="{{ $row->Code }}">{{ $row->Country }}</option>
						@endforeach
						</select>						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">City <br /><input {{ disabledIfAdmitted() }} type="checkbox" name="BillingIsOtherCity" id="BillingIsOtherCity">&nbsp;<i><small>If Other</small></i></label>
					<div class="col-md-9">
						<input type="hidden" class="form-control not-required CitySel" id="BillingCitySel" name="BillingCitySel" value="{{ getObjectValue($data,'Guardian_Billing_CityID') }}">
						<select {{ disabledIfAdmitted() }} class="form-control CitySelOption {{ getObjectValue($data,'Guardian_Billing_IsOtherCity') ? 'hide not-required' : ''}}" name="BillingCity" id="BillingCity" data-placeholder="City">
							<option value="">Select...</option>						
						</select>		
						<input {{ disabledIfAdmitted() }} type="text" name="BillingOtherCity" id="BillingOtherCity"  value="{{ getObjectValue($data,'Guardian_Billing_OtherCity') }}" class="form-control {{ getObjectValue($data,'Guardian_Billing_IsOtherCity') ? '' : 'hide not-required' }}">				
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12" style="text-align:left !important;">Please check the following if applicable</label>
					<div class="col-md-12">
						<?php 
							$status = explode(',',getObjectValue($admission,'Family_HealthStatus')); $arr = [];
							foreach($status as $stats) {
								$arr[] = $stats;
							}
						?>
						<input {{ disabledIfAdmitted() }} {{ in_array("Mother deceased", $arr) ? 'checked' : '' }} type="checkbox" name="parentHealthStatus1" value="Mother deceased"> Mother deceased
						<input {{ disabledIfAdmitted() }} {{ in_array("Father deceased", $arr) ? 'checked' : '' }} type="checkbox" name="parentHealthStatus2" value="Father deceased"> Father deceased
						<input {{ disabledIfAdmitted() }} {{ in_array("Parents divorced", $arr) ? 'checked' : '' }} type="checkbox" name="parentHealthStatus3" value="Parents divorced"> Parents divorced <br />
						<input {{ disabledIfAdmitted() }} {{ in_array("Parents separated", $arr) ? 'checked' : '' }} type="checkbox" name="parentHealthStatus4" value="Parents separated"> Parents separated
						<input {{ disabledIfAdmitted() }} {{ in_array("Mother remarried", $arr) ? 'checked' : '' }} type="checkbox" name="parentHealthStatus5" value="Mother remarried"> Mother remarried
						<input {{ disabledIfAdmitted() }} {{ in_array("Father remarried", $arr) ? 'checked' : '' }} type="checkbox" name="parentHealthStatus6" value="Father remarried"> Father remarried
					</div>
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-12" style="text-align:left !important;">If applicant does not live with parents please explain family situation</label>
					<div class="col-md-12">
						<textarea {{ disabledIfAdmitted() }} class="form-control not-required" name="familySituation">{{ getObjectValue($admission,'Family_Situation') }}</textarea>
					</div>
				</div>
			</div>
		</div>

	</div>
</form>
