<?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $data = isset($data[0]) ? $data[0] : [];  ?>
<form action="#" class="form-horizontal" id="formMother">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Mother Information</h4>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Name</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Name" name="MotherName" value="{{ getObjectValue($data,'Mother_Name') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Date of Birth</label>
					<div class="col-md-9">
						<input type="text" class="form-control date-picker not-required" data-date-format="mm/dd/yyyy" placeholder="DOB" name="MotherDateOfBirth" value="{{ setDateFormat(getObjectValue($data,'Mother_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Company</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Company" name="MotherCompany" value="{{ getObjectValue($data,'Mother_Company') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Occupation</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Occupation" name="MotherOccupation" value="{{ getObjectValue($data,'Mother_Occupation') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Business Address</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Address" name="MotherBusinessAddress" value="{{ getObjectValue($data,'Mother_CompanyAddress') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Business Phone</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Phone" name="MotherBusinessPhone" value="{{ getObjectValue($data,'Mother_CompanyPhone') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Email Address</label>
					<div class="col-md-9">
						<input type="email" class="form-control not-required" placeholder="Email" name="MotherEmailAddress" value="{{ getObjectValue($data,'Mother_Email') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Mobile Number</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Mobile" name="MotherMobile" value="{{ getObjectValue($data,'Mother_Mobile') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Educational Attainment</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Education" name="MotherEducation" value="{{ getObjectValue($data,'Mother_EducAttainment') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">School Attended</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="School" name="MotherSchool" value="{{ getObjectValue($data,'Mother_SchoolAttended') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
	</div>
</form>