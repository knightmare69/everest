<?php 
	$admission = App\Modules\Admission\Models\Admission::getAdmission(decode(Request::get('AppNo')),
	['LRN','IsAttendingKdg','PresentSchool','PresentSchoolYrsProgram','PresentSchoolAddress','PresentSchoolWebsite','PresentSchoolHead','PresentSchoolContactNo']);
	$admission = isset($admission[0]) ? $admission[0] : [];	

	function getAnswerByID($key){
		$scholastic = App\Modules\Admission\Models\QuestionaireAnswer::where(['app_no'=>decode(Request::get('AppNo')),'question_id'=>$key])->get();
		return isset($scholastic[0]) ? $scholastic[0]->answer : '';
	}

	function disabledIfAdmitted() {
        $status = App\Modules\Admission\Models\Admission::where('AppNo',decode(Request::get('AppNo')))->pluck('StatusID');
        return ($status == '2' && !empty(Request::get('AppNo')) && isParent()) ? 'disabled' : '';
	}
?>
<form action="#" class="form-horizontal" id="formScholastic">
	<div class="form-body">	
		<legend style="font-size: 15px;font-weight: bold;">Learner Reference Number</legend>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">LRN</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-ticket"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="LRN" maxlength="12" value="{{ getObjectValue($admission,'LRN') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<legend style="font-size: 15px;font-weight: bold;">Scholastic Information</legend>
		<div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2"></label>
                    <div class="col-md-10">
                        <input {{ disabledIfAdmitted() }} type="checkbox" name="isAttendingKdg" id="isAttendingKdg" {{ getObjectValue($admission,'IsAttendingKdg') ? 'checked' : '' }}>
                        <label class="control-label">Is your currently child attending any pre-school or Kindergarten program ?</label>
                   </div> 
                </div>
            </div>
        </div> 
        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Present School</label>
					<div class="col-md-6">
						<div class="input-icon">
							<i class="fa fa-building-o"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PresentSchool" value="{{ getObjectValue($admission,'PresentSchool') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-6" style="padding-right: unset;padding-left: unset;text-align: center;">No. of years in program</label>
							<div class="col-md-6">
								<div class="input-icon">
									<i class="fa fa-folder-open"></i>
									<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PresentSchoolYrsProgram" value="{{ getObjectValue($admission,'PresentSchoolYrsProgram') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Address</label>
					<div class="col-md-6">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PresentSchoolAddress" value="{{ getObjectValue($admission,'PresentSchoolAddress') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-3">Website</label>
							<div class="col-md-9">
								<div class="input-icon">
									<i class="fa fa-external-link"></i>
									<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="PresentSchoolWebsite" value="{{ getObjectValue($admission,'PresentSchoolWebsite') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Head of School</label>
					<div class="col-md-6">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="PresentSchoolHead" value="{{ getObjectValue($admission,'PresentSchoolHead') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-3">Phone</label>
							<div class="col-md-9">
								<div class="input-icon">
									<i class="fa fa-phone"></i>
									<input {{ disabledIfAdmitted() }} type="number" class="form-control not-required" name="PresentSchoolPhone" value="{{ getObjectValue($admission,'PresentSchoolContactNo') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table" id="tableScholastic">
					<tbody>
						@foreach(\App\Modules\Admission\Models\Questionaire::where(['qns_group'=>'SCH-I','is_inactive'=>0])->where('kndr_gr1_sort','<>',0)->orderby('kndr_gr1_sort','asc')->get() as $row)
						<tr data-id="{{$row->id}}" data-question="{{$row->question}}" data-parentid="{{$row->qns_parent_id}}">
							<td style="border-top: unset;">
							<?php 
								$bullet = '&#10687;&nbsp;'; $tab = ''; $adjust = ''; $pull = '';
								if($row->qns_parent_id != 0){
									$bullet = '';
									$tab = str_repeat('&nbsp;', 14);
									$adjust = 'style=width:96%!important;'; 
									$pull = 'pull-right';
								}
							?>
							{{$tab.$bullet.$row->question}}
							@if($row->description != '')
								<br />{{$tab.$row->description}} 
							@endif
							@if(!$row->is_header_only)
								@if($row->type == 'select')
									<select {{ disabledIfAdmitted() }} class="form-control answer {{$pull}}" name="select-{{$row->id}}" <?php$adjust?>>
										<?php $options = explode(';',$row->options); ?>
										@foreach($options as $opt)
										<option {{ getAnswerByID($row->id) == $opt ? 'selected' : '' }}>{{$opt}}</option>
										@endforeach
									</select>
								@else
									<input {{ disabledIfAdmitted() }} type="text" class="form-control answer {{$pull}} <?= $row->kndr_gr1_isrequired ? '' : 'not-required'?>" {{$adjust}} name="answer-{{$row->id}}" value="{{ getAnswerByID($row->id) }}">
								@endif
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<legend style="font-size: 15px;font-weight: bold;">Personal History</legend>
		<div class="row">
			<div class="col-md-12">
				<table class="table" id="tableQuestions">
					<tbody>
						@foreach(\App\Modules\Admission\Models\Questionaire::where(['qns_group'=>'P-H','is_inactive'=>0])->where('kndr_gr1_sort','<>',0)->orderby('kndr_gr1_sort','asc')->get() as $row)
						<tr data-id="{{$row->id}}" data-question="{{$row->question}}" data-parentid="{{$row->qns_parent_id}}">
							<td style="border-top: unset;">
							<?php 
								$bullet = '&#10687;&nbsp;'; $tab = '';  $adjust = ''; $pull = '';
								if($row->qns_parent_id != 0){
									$bullet = '';
									$tab = str_repeat('&nbsp;', 14);
									$adjust = "style=width:96%!important;"; 
									$pull = 'pull-right';
								}
							?>
							{{$tab.$bullet.$row->question}}
							@if($row->description != '')
								<br />{{$tab.$row->description}} 
							@endif
							@if(!$row->is_header_only)
								@if($row->type == 'select')
									<select {{ disabledIfAdmitted() }} class="form-control answer {{$pull}}" name="select-{{$row->id}}" {{$adjust}}>
										<?php $options = explode(';',$row->options); ?>
										@foreach($options as $opt)
										<option {{ getAnswerByID($row->id) == $opt ? 'selected' : '' }}>{{$opt}}</option>
										@endforeach
									</select>
								@else
									<input {{ disabledIfAdmitted() }} type="text" class="form-control answer {{$pull}} <?= $row->kndr_gr1_isrequired ? '' : 'not-required'?>" {{$adjust}} name="answer-{{$row->id}}" value="{{ getAnswerByID($row->id) }}">
								@endif
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<b style="font-style:italic;">{{$tab}}* Please submit a photocopy of your child’s complete evaluation and therapy report.</b>
			</div>
		</div>
		
	</div>
</form>

