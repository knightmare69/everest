<?php
    $lname  = '';
    $fname  = '';
    $mname  = '';
    $bdate  = '';
    $gender = 'M';
	if($inquiry){ 
	  $lname  = $inquiry->lname;
	  $fname  = $inquiry->fname;
	  $mname  = $inquiry->mname;
	  $bdate  = $inquiry->birth_date;
	  $gender = $inquiry->gender;
	}
?>
<form action="#" class="form-horizontal" id="formStudent">
	<div class="form-body">		
		<div class="row">		
        	<div class="col-md-6">
                <div class="form-group">
					<label class="control-label col-md-4">Last Name </label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control" name="LastName" value="<?php echo $lname;?>">
						</div>
					</div>
				</div>
                
				<div class="form-group">
					<label class="control-label col-md-4">First Name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control" name="FirstName" value="<?php echo $fname;?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Middle Name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control not-required" name="MiddleName" id="MiddleName" value="<?php echo $mname;?>">
						</div>
					</div>
				</div>
            	<div class="form-group">
					<label class="control-label col-md-4">Middle Initial</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control not-required" name="MiddleInitial" id="MiddleInitial" value="<?php echo strlen($mname)>0 ? (ucwords(substr($mname,0,1)).'.') : '';?>">
						</div>					
					</div>
				</div>
                <div class="form-group">
					<label class="control-label col-md-4">Extension Name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control not-required" name="ExtensionName">
						</div>	
					</div>
				</div>
				 <div class="form-group">
					<label class="control-label col-md-4">Nickname</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control not-required" name="NickName">
						</div>
					</div>
				</div>
                <div class="form-group">
					<label class="control-label col-md-4">Gender</label>
					<div class="col-md-8">
						<select class="form-control" name="Gender">
							<option value='M' <?php echo(($gender=='M')?'selected':'');?>>Male</option>
							<option value='F' <?php echo (($gender=='F')?'selected':'');?>>Female</option>
						</select>
					</div>
				</div>
                
			</div>
			<!--/span-->
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-12 photo-message center {{ !isParent() ? 'hide' : '' }}"><small class="font-red">Photo is required</small></label>
					<label class="control-label col-md-12 center">Photo <small>(<b>JPG, PNG Only</b>)</small></label>
					<div class="col-md-12 center">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
								<img src="{{ url('general/defaultThumnail') }}" id="StudentPhotoThumbnail" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="file" accept="image/x-png, image/jpeg" id="StudentPhoto" class="StudentPhoto {{ !isParent() ? 'not-required' : '' }}">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">
							<small>NOTE!</small> </span>
							&nbsp;<small>Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+, Opera11.1+ and FireFox6.0+. In older browsers the filename is shown instead.</small>
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
		</div>	

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Date of Birth <br /><small>(<i>ex: 01/20/1991</i>)</small></label>
					<div class="col-md-5">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input type="text" data-date-format="mm/dd/yyyy" class="form-control date-picker" data-minage="4" name="DateOfBirth" id="DateOfBirth" value="<?php echo (($bdate!='')?date('m/d/Y',strtotime($bdate)):'');?>">
						</div>
					</div>
					<div class="col-md-3">
						<label class="form-control not-required center" name="StudentAge" id="StudentAge"></label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label col-md-5">Place of Birth</label>
					<div class="col-md-7" style="padding: unset;">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input type="text" class="form-control not-required" name="PlaceOfBirth">
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-2" style="padding-left: 30px;">
				<div class="form-group">
					<label class="control-label">Is Adopted</label>
					<input type="checkbox" name="isAdopted" id="isAdopted">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Home Address</label>
					<div class="col-md-2">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="HomeAddressUnitNo" placeholder="House/Unit No">
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="HomeAddressStreet" placeholder="Street">
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="HomeAddressBrgy" placeholder="Barangay">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2"></label>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="HomeAddressCity" placeholder="City">
						</div>
					</div>
					<div class="col-md-2">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="HomeAddressZipCode" placeholder="Postal Code">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-5">Home Phone</label>
							<div class="col-md-7">
								<div class="input-icon">
									<i class="fa fa-phone"></i>
									<input type="number" class="form-control not-required" name="HomePhone">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="row not-rtn">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Nationality</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control" name="isForeign" id="isForeign" value="0">
						<select class="form-control select2 notreturnFields" name="Nationality" id="Nationality">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{$row->NationalityID == 1 ? 'selected' : ''}} data-isforeign="{{ $row->IsForeign }}" value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option data-isforeign="1" value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addNationality" value=""/>
					</div>
				</div>
			</div>
		</div>

		<div class="row not-rtn">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Citizenship</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control" name="isForeignCitizen" id="isForeignCitizen" value="0">
						<select class="form-control select2 notreturnFields" name="Citizenship" id="Citizenship">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{$row->NationalityID == 1 ? 'selected' : ''}} data-isforeign="{{ $row->IsForeign }}" value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option data-isforeign="1" value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addCitizenship" value="" placeholder="Please specify"/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label col-md-5">Religion</label>
					<div class="col-md-7" style="padding: unset;">
						<select class="form-control select2 notreturnFields" name="Religion">
							@foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
							@if($row->ReligionID != 31 && $row->Inactive != 1)
								<option {{ $row->isDefault ? 'selected' : '' }} value="{{ encode($row->ReligionID) }}">{{ $row->Religion }}</option>
							@endif
							@endforeach
							<option value="{{ encode(31) }}">Not Applicable</option>
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addReligion" value=""/>
					</div>
				</div>
			</div>
			<div class="col-md-2" style="padding-left: 30px;">
				<div class="form-group">
					<label class="control-label">Is Baptized</label>
					<input type="checkbox" name="isBaptized" id="isBaptized" checked>
				</div>
			</div>
		</div>

		<div class="row not-rtn">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label col-md-3" style="padding-left: 12px;">Religion Baptized in</label>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input type="text" class="form-control religionFields notreturnFields date-picker" data-date-format="mm/dd/yyyy" name="BaptizedDate" placeholder="Date">
						</div>
					</div>
					<div class="col-md-5">
						<div class="input-icon">
							<i class="fa fa-institution"></i>
							<input type="text" class="form-control religionFields notreturnFields" name="BaptizedChurch" placeholder="Church">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input type="text" class="form-control religionFields notreturnFields" name="BaptizedCity" placeholder="City">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row not-rtn">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2" style="padding-left: unset;padding-right: unset;">Parish/Place of worship</label>
					<div class="col-md-10">
						<div class="input-icon">
							<i class="fa fa-institution"></i>
							<input type="text" class="form-control religionFields notreturnFields" name="WorshipPlace" id="WorshipPlace" placeholder="">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row not-rtn">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Parish Address</label>
					<div class="col-md-6">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input type="text" class="form-control religionFields notreturnFields" name="WorshipAddress" id="WorshipAddress" placeholder="">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-2">Phone</label>
							<div class="col-md-10">
								<div class="input-icon">
									<i class="fa fa-phone"></i>
									<input type="number" class="form-control not-required" name="WorshipTelNo">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<!-- 		<div class="row rtn hidden">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-4" style="width:38%; padding-left:unset; padding-right:unset;">Emergency Contact Person <small>(<i>if parents cannot be reached</i>)</small></label>
					<div class="col-md-7" style="width:62%;">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control not-required returnFields" name="EmergencyContactPerson" id="EmergencyContactPerson" placeholder="">
						</div>					
					</div>
				</div>
			</div>
		</div>
		<div class="row rtn hidden">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2" style="padding-left: unset;padding-right: unset;">Address</label>
					<div class="col-md-10">
						<div class="input-icon">
							<i class="fa fa-home"></i>
							<input type="text" class="form-control not-required returnFields" name="EmergencyContactPersonAddress" id="EmergencyContactPersonAddress" placeholder="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row rtn hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4 ">Home Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input type="text" class="form-control not-required returnFields" name="EmergencyContactPersonPhone">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Work Phone</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input type="text" class="form-control not-required returnFields" name="EmergencyContactPersonWorkPhone">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row rtn hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4 ">Mobile Phone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-tablet"></i>
							<input type="text" class="form-control not-required returnFields" name="EmergencyContactPersonMobilePhone">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Email</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-envelope-o"></i>
							<input type="email" class="form-control not-required returnFields" name="EmergencyContactPersonEmail">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row rtn hidden">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2" style="padding-left: unset;padding-right: unset;">Relationship to applicant</label>
					<div class="col-md-10">
						<div class="input-icon">
							<i class="fa fa-child"></i>
							<input type="text" class="form-control not-required returnFields" name="RelationshipToApplicant" id="RelationshipToApplicant" placeholder="">
						</div>
					</div>
				</div>
			</div>
		</div> -->
		
		<!--
		<legend class="foreign hidden" style="font-size: 15px;font-weight: bold;">If Foreign</legend>
		<div class="row foreign hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4 ">Passport Number</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-ticket"></i>
							<input type="text" class="form-control not-required foreignFields" name="PassportNo">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Visa Status</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-certificate"></i>
							<input type="text" class="form-control not-required foreignFields" name="VisaStatus">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row foreign hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">SSP Number</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-ticket"></i>
							<input type="text" class="form-control not-required foreignFields" name="SSPNo">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4" style="padding-left: unset;">Validity of Stay</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input type="text" class="form-control date-picker not-required foreignFields" data-date-format="mm/dd/yyyy" name="ValidityStay" id="ValidityStay">
						</div>
					</div>
				</div>
			</div>
		</div>
		-->
		<h3 class="form-section hidden">Exam Schedule <small class="order-error hide font-red">OR # invalid inputs.</small></h3>
		<div class="row hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3 hidden">Schedule Date</label>
					<div class="col-md-9">
						<input type="hidden" readonly class="form-control not-required-default" name="ScheduleDateID" id="ScheduleDateID">
						<select class="form-control  hidden not-required" name="ScheduleDate" id="ScheduleDate">
							<option value="">Select...</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3  hidden">OR #</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required or-field  hidden" name="ORCode" id="ORCode">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3  hidden">Security Code</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required or-field  hidden" name="ORSecurityCode" id="ORSecurityCode">
					</div>
				</div>
			</div>
		</div>

	</div>
</form>