 <?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $data = isset($data[0]) ? $data[0] : [];  ?>
<form action="#" class="form-horizontal" id="formGuardian">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Other Information</h4>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3 hidden">Name</label>
					<div class="col-md-9">
						<input type="hidden" class="form-control" name="FamilyID" id="FamilyID"  value="">
						<input type="text" class="form-control hidden not-required" placeholder="Name" name="name" value="{{ getObjectValue($data,'Guardian_Name') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 hidden">Living with</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-family"></i>
							<select class="form-control not-required hidden" name="livingWith" id="livingWith">
								<option value="">Select...</option>
								@foreach(App\Modules\Admission\Models\GuardianBackround::getLivingWith() as $key => $label)
								<option {{ getObjectValue($data,'Guardian_LivingWith') == $key ? 'selected' : '' }} value="{{ $key }}">{{ $label }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Parent Marital</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-family"></i>
							<select class="form-control" name="marital" id="marital">
								<option value="">Select...</option>
								@foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
								<option {{ getObjectValue($data,'Guardian_ParentMaritalID') == $row->StatusID ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->CivilDesc }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">If others</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control not-required" name="livingWithOther" id="livingWithOther"  value="{{ getObjectValue($data,'Guardian_LivingWith') == '4' ? App\Modules\Admission\Models\GuardianBackround::getLivingWith(getObjectValue($data,'Guardian_LivingWith')) : ''  }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-12 photo-message center {{ !isParent() ? 'hide' : '' }}"><small class="font-red">Photo is required</small></label>
					<label class="control-label col-md-12 center">Photo <small>(<b>JPG, PNG Only</b>)</small></label>
					<div class="col-md-12 center">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
								<img src="{{ url('general/getGuardianPhoto?FamilyID='.encode(getGuardianFamilyID())) }}" id="GuardianPhotoThumbnail" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
							</div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="file" accept="image/x-png, image/jpeg" id="GuardianPhoto" class="GuardianPhoto {{ !isParent() ? 'not-required' : '' }}">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">
							<small>NOTE!</small> </span>
							&nbsp;<small>Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+, Opera11.1+ and FireFox6.0+. In older browsers the filename is shown instead.</small>
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Address</h4>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Resident</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="resident"  value="{{ getObjectValue($data,'Guardian_Address') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Street</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="street"  value="{{ getObjectValue($data,'Guardian_Street') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Barangay</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="barangay" value="{{ getObjectValue($data,'Guardian_Barangay') }}">
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Country</label>
					<div class="col-md-9">
						<select class="form-control" name="GuardianCountry" id="GuardianCountry">
						@foreach(App\Modules\Setup\Models\Country::get() as $row)
							<option {{ trim(getObjectValue($data,'Guardian_CountryCode')) == '' ? (strtoupper($row->Code) == defaultCountry()) ? 'selected' : '' : '' }} {{ getObjectValue($data,'Guardian_CountryCode') == $row->Code ? 'selected' : '' }} value="{{ $row->Code }}">{{ $row->Country }}</option>
						@endforeach
						</select>						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">City {{ getObjectValue($data,'Guardian_CityID') }} <br /><input type="checkbox" name="isOtherCity" id="isOtherCity">&nbsp;<i><small>If Other</small></i></label>
					<div class="col-md-9">
						<input type="hidden" class="form-control not-required CitySel" id="GuardianCitySel" name="GuardianCitySel" value="{{ getObjectValue($data,'Guardian_CityID') }}">
						<select class="form-control select2 CitySelOption {{ getObjectValue($data,'Guardian_isOtherCity') ? 'hide not-required' : '' }}" name="GuardianCity" id="GuardianCity">
							<option value="">Select...</option>
						</select>		
						<input type="text" name="otherCity" id="otherCity" value="{{ getObjectValue($data,'Guardian_OtherCity') }}" class="form-control  {{ getObjectValue($data,'Guardian_isOtherCity') ? '' : 'hide not-required' }}">							
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="form-group">
			<label class="col-md-12">Person financially responsible for tuition payments</label>
			<div class="col-md-12">
				<input type="text" class="form-control" name="paymentsResponsible">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Billing Address <small>(<i>Pls check if same addess as above.</i>)</small> <input type="checkbox" class="form-control" id="CopyAddress"></h4>
			</div>
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Resident</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="BillingResident"  value="{{ getObjectValue($data,'Guardian_Billing_Address') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Street</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="BillingStreet"  value="{{ getObjectValue($data,'Guardian_Billing_Street') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Barangay</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="BillingBrangay" value="{{ getObjectValue($data,'Guardian_Billing_Barangay') }}">
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Country</label>
					<div class="col-md-9">
						<select class="form-control" name="BillingCountry" id="BillingCountry">
						@foreach(App\Modules\Setup\Models\Country::get() as $row)
							<option {{ strtoupper($row->Code) == defaultCountry() ? 'selected' : '' }} value="{{ $row->Code }}">{{ $row->Country }}</option>
						@endforeach
						</select>						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">City <br /><input type="checkbox" name="BillingIsOtherCity" id="BillingIsOtherCity">&nbsp;<i><small>If Other</small></i></label>
					<div class="col-md-9">
						<input type="hidden" class="form-control not-required CitySel" id="BillingCitySel" name="BillingCitySel" value="{{ getObjectValue($data,'Guardian_Billing_CityID') }}">
						<select class="form-control CitySelOption" name="BillingCity" id="BillingCity" data-placeholder="City">
							<option value="">Select...</option>						
						</select>		
						<input type="text" name="BillingOtherCity" id="BillingOtherCity" class="form-control hide">				
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Contact Info</h4>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Telephone</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input type="text" class="form-control" name="telephone" value="{{ getObjectValue($data,'Guardian_TelNo') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Mobile</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-mobile"></i>
							<input type="text" class="form-control" name="mobile" value="{{ getObjectValue($data,'Guardian_Mobile') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Email</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-envelope"></i>
							<input type="email" class="form-control" name="email" value="{{ getObjectValue($data,'Guardian_Email') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-12">Please check the following if applicable</label>
			<div class="col-md-12">
				<input type="checkbox" name="parentHealthStatus1" value="Mother deceased"> Mother deceased
				<input type="checkbox" name="parentHealthStatus2" value="Father deceased"> Father deceased
				<input type="checkbox" name="parentHealthStatus3" value="Parents divorced"> Parents divorced <br />
				<input type="checkbox" name="parentHealthStatus4" value="Parents separated"> Parents separated
				<input type="checkbox" name="parentHealthStatus5" value="Mother remarried"> Mother remarried
				<input type="checkbox" name="parentHealthStatus6" value="Father remarried"> Father remarried
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-12">If applicant does not live with parents please explain family situation:</label>
			<div class="col-md-12">
				<textarea class="form-control not-required" name="familySituation"></textarea>
			</div>
		</div>
	</div>
</form>