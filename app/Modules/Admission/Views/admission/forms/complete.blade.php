<?php $iswfh=1;?>
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissable">
			@if($isSubmit)
			  @if($iswfh==1)
			  <message>Thank you for submitting the application form for admission to Everest Academy. You will receive an email from admissions@everestmanila.edu.ph to arrange your initial admissions interview. Please submit a printed copy of the accomplished application form when we return to the campus along with the other required application documents. We look forward to assisting you in the admissions process.</message>
			  @else
			  <message>Thank you for submitting the application form for admission to Everest Academy. You will receive an email from admissions@everestmanila.edu.ph to arrange your initial admissions interview. Please bring a printed copy of the accomplished application form on the day of your interview. We look forward to assisting you in the admissions process.</message>
			  @endif
			@else
			<message>Hi, You have successfully save the application form.</message>
			@endif
		</div>
	</div>
</div>