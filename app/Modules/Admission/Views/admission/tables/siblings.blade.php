<?php 
	$siblings = App\Modules\Admission\Models\Siblings::where('FamilyID',getGuardianFamilyID( decode( Request::get('AppNo')) ))->get();
	$siblings = isset($siblings) ? $siblings : [];

	$admission = App\Modules\Admission\Models\Admission::getAdmission(decode(Request::get('AppNo')),['Familly_Activities']);
	$admission = isset($admission[0]) ? $admission[0] : [];	
?>	
<table class="table table-bordered" id="tableSiblings">
	<thead>
		<tr>
			<th class="right" colspan="6">
				<button {{ disabledIfAdmitted() }} type="button" class="btn btn-primary" id="btnAddSibling">Add new</button>
			</th>
		</tr>
		<tr class="heading">
			<th width="2%">&nbsp;</th>
			<th width="30%"><small>Full Name</small></th>
			<th width="15%"><small>Date of Birth</small></th>
			<th width="10%"><small>Age</small></th>
			<th width="15%"><small>Gender</small></th>
			<th width="24%"><small>School Attended</small></th>
		</tr>
	</thead>
	<tbody>
		@foreach($siblings as $row)
		<tr data-id="{{ $row->SiblingIDX }}">
			<td>
				<button {{ disabledIfAdmitted() }} class="btn btn-sm center red btnSiblingRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="siblingName" class="form-control siblingName" placeholder="Name" value="{{ $row->FullName }}">
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="siblingDOB" class="form-control siblingDOB date-picker" data-date-format="mm/dd/yyyy" placeholder="DOB" value="{{ setDateFormat($row->DateofBirth,'yyyy-mm-dd','mm/dd/yyyy') }}">
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="siblingAge" readonly class="form-control siblingAge" placeholder="Age">
			</td>
			<td>
				<select {{ disabledIfAdmitted() }} class="form-control siblingGender" name="siblingGender">
					<option {{ $row->Gender == '0' ? 'selected' : '' }} value="0">M</option>
					<option {{ $row->Gender == '1' ? 'selected' : '' }} value="1">F</option>
				</select>
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="siblingSchool" class="form-control siblingSchool" placeholder="School" value="{{ $row->SchoolAttended }}">
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<table class="table hide" id="tableDefaultSibling">
	<tbody>
		<tr>
			<td>
				<button class="btn btn-sm center red btnSiblingRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input type="text" name="siblingName" class="form-control siblingName" placeholder="Name">
			</td>
			<td>
				<input type="text" name="siblingDOB" class="form-control siblingDOB date-picker" data-date-format="mm/dd/yyyy" placeholder="DOB">
			</td>
			<td>
				<input type="text" name="siblingAge" readonly class="form-control siblingAge" placeholder="Age">
			</td>
			<td>
				<select class="form-control siblingGender" name="siblingGender">
					<option value="0">M</option>
					<option value="1">F</option>
				</select>
			</td>
			<td>
				<input type="text" name="siblingSchool" class="form-control siblingSchool" placeholder="School">
			</td>
		</tr>
	</tbody>
</table>

<form action="#" class="form-horizontal" id="formSibling">
	<div class="form-body">	
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">What types of activities do you enjoy as a family? (put "N/A" if not applicable)</label>
						<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FamilyActivities" value="{{ getObjectValue($admission,'Familly_Activities') }}">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
