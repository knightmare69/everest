<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit">#</th>
            <th>Panel Name</th>  
            <th>Title/Position</th>            
            <th>Academic Year & Term</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        @foreach($data as $row)           
        <tr data-id="{{ $row->id }}"  >
            <td class="font-xs autofit bold">{{$i++}}.</td>
            <td class="">{{ $row->FullName }}</td>
            <td class="">{{ $row->Title }}</td>
            <td>{{$row->AcademicYear.' '.$row->SchoolTerm}}</td>                   
            <td class="text-center">
                <a href="javascript:;" class="btn btn-danger btn-sm" onclick="Panels.delete({{$row->id}})">
                    <i class="fa fa-trash"></i>
                </a>
                <a href="javascript:;" class="btn btn-success btn-sm" onclick="Panels.edit({{$row->id}})">
                    <i class="fa fa-edit"></i>
                </a>
            </td>
        </tr>
        @endforeach
        @if(count($data) <= 0)
        <tr>
            <td colspan="6">No Records available.</td>
        </tr>
        @endif
    </tbody>
</table>