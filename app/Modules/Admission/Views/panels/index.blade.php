<form action="#" class="form-horizontal" id="FormPanels">
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-child"></i> Panels</div>
			<div class="actions btn-set">
				
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-body text-left">
    					<div class="col-md-4">
    						@include($views.'forms.form')
    					</div>
    					<div class="col-md-8" id="table-wrapper">
    					</div>
    					<!--/span-->
					</div>
				</div>
			</div>
		</div>
	</div>
</form>