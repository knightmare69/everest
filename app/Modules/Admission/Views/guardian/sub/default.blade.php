<?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $family_info = isset($data[0]) ? $data[0] : [];  ?>
<style>
	.bg-inverse{
		background-color:white !important;
	}
</style>
<div class="row">
    <div class="col-md-12">
        @include('errors.event')
    </div>
</div>
<div class="portlet light bg-inverse">
	<div class="portlet-title hidden">
		<div class="caption">
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body form">
			<div class="tabbable-line nav-tabs-sub">
                <ul class="nav nav-tabs pull-left">
                    <li class="active">
                        <a href="#guardian-tab" data-toggle="tab">Family Info<span class="hidden-xs hidden-sm">rmation</span></a>
                    </li>
                    <li>
                        <a href="#father-tab" data-toggle="tab">Father</a>
                    </li>
                    <li>
                        <a href="#mother-tab" data-toggle="tab">Mother</a>
                    </li>
                    <li>
                        <a href="#emergency-tab" data-toggle="tab">Emergency</a>
                    </li>
                </ul>
                <br/><br/>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="guardian-tab">
		            @include($views.'forms.guardian_profile')
					</div>
                    <div class="tab-pane" id="father-tab">
					<form action="#" class="form-horizontal" id="formFather">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		                @include('Students.views.student-profile.family-info.father-form')
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<a href="javascript:void(0);" data-previous="#guardian-tab" class="btn blue btnprev">Previous</a>
									<button type="submit" class="btn green pull-right" id="submitFather">Next</button>
								</div>
							</div>
						</div>
					</div>
					</form>
					</div>
                    <div class="tab-pane" id="mother-tab">
					<form action="#" class="form-horizontal" id="formMother">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						@include('Students.views.student-profile.family-info.mother-form')
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<a href="javascript:void(0);" data-previous="#father-tab" class="btn blue btnprev">Previous</a>
									<button type="submit" class="btn green pull-right" id="submitMother">Next</button>
								</div>
							</div>
						</div>
					</div>
					</form>	
					</div>
					
                    <div class="tab-pane" id="emergency-tab">
					<form action="#" class="form-horizontal" id="formEmergency">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						@include('Students.views.student-profile.family-info.emergency-form')
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<a href="javascript:void(0);" data-previous="#mother-tab" class="btn blue btnprev">Previous</a>
									<button type="submit" class="btn green pull-right" id="submitEmergency">Save</button>
								</div>
							</div>
						</div>
					</div>
					</form>	
					</div>
					
					
				</div>	
		    </div>		
		<!-- END FORM-->
	</div>
</div>
