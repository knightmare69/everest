<?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $data = isset($data[0]) ? $data[0] : [];  ?>
<form action="#" class="form-horizontal" id="formGuardian">
<div>
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-body">
		<h3 class="form-section">Primary Contact Person</h3>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Name <span class="required">*</span></label>
					<div class="col-md-8">
						<input type="hidden" class="form-control" name="FamilyID" id="FamilyID"  value="{{ encode(getFamilyID()) }}">
						<input  type="text" class="form-control" placeholder="Name" name="name" value="{{ getObjectValue($data,'Guardian_Name') }}" required/>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-4">Living with</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<select  class="form-control not-required" name="livingWith" id="livingWith">
								<option value="">Select...</option>
								@foreach(App\Modules\Admission\Models\GuardianBackround::getLivingWith() as $key => $label)
								<option {{ getObjectValue($data,'Guardian_LivingWith') == $key ? 'selected' : '' }} value="{{ $key }}">{{ $label }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Marital Status <span class="required">*</span></label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<select class="form-control" name="marital" id="marital" required>
								<option value="">Select...</option>
								@foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
								<option  {{ getObjectValue($data,'Guardian_ParentMaritalID') == $row->StatusID ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->CivilDesc }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-4">If others</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control not-required" name="livingWithOther" id="livingWithOther"  value="{{ getObjectValue($data,'Guardian_RelationshipOthers') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				
					<label class="control-label col-md-12 photo-message center hide"><small class="font-red">Photo is required</small></label>
				
					<label class="control-label col-md-12 center">Photo <small>(<b>JPG, PNG Only</b>)</small></label>
					<div class="col-md-12 center">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
								<img src="{{ url('/general/getGuardianPhoto?FamilyID='.encode(getObjectValue($data,'FamilyID'))) }}" id="GuardianPhotoThumbnail" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
							</div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="file" accept="image/x-png, image/jpeg" id="GuardianPhoto" class="GuardianPhoto {{ !isParent() ? 'not-required' : '' }}">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">
							<small>NOTE!</small> </span>
							&nbsp;<small>Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+, Opera11.1+ and FireFox6.0+. In older browsers the filename is shown instead.</small>
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<h3 class="form-section"> Home Address</h3>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Address<span class="required">*</span></label>
					<div class="col-md-10">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="resident"  value="{{ getObjectValue($data,'Guardian_Address') }}" maxlength="240" required/>
						</div>
					</div>
				</div>
			</div>
			<!--
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Address line 2 <span class="required">*</span></label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="street"  value="{{ getObjectValue($data,'Guardian_Street') }}" required/>
						</div>
					</div>
				</div>
			</div>
			-->
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Barangay</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="barangay" value="{{ getObjectValue($data,'Guardian_Barangay') }}">
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Country<span class="required">*</span></label>
					<div class="col-md-8">
						<select class="form-control" name="GuardianCountry" id="GuardianCountry" required>
						@foreach(App\Modules\Setup\Models\Country::get() as $row)
							<option {{ trim(getObjectValue($data,'Guardian_CountryCode')) == '' ? (strtoupper($row->Code) == defaultCountry()) ? 'selected' : '' : '' }} {{ getObjectValue($data,'Guardian_CountryCode') == $row->Code ? 'selected' : '' }} value="{{ $row->Code }}">{{ $row->Country }}</option>
						@endforeach
						</select>						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">City  <span class="required">*</span><br /><input type="checkbox" name="isOtherCity" {{ ((getObjectValue($data,'Guardian_IsOtherCity')==1) ? 'checked' : '') }} id="isOtherCity">&nbsp;<i><small>If Other</small></i></label>
					<div class="col-md-8">
						<input type="hidden" class="form-control not-required CitySel" id="GuardianCitySel" name="GuardianCitySel" value="{{ getObjectValue($data,'Guardian_CityID') }}">
						<select class="form-control select2 {{ (getObjectValue($data,'Guardian_IsOtherCity')==1) ? 'hide not-required' : '' }}" name="GuardianCity" id="GuardianCity">
							<option value="">Select...</option>
						@foreach(App\Modules\Setup\Models\City::where('CountryCode',getObjectValueWithReturn($data,'Guardian_CountryCode',defaultCountry()))->get() as $city)
							<option data-id={{ $city->CityID }} {{ trim(strtolower(getObjectValue($data,'Guardian_CityID'))) == trim(strtolower($city->CityID)) ? 'selected' : '' }}>{{ $city->City }}</option>
						@endforeach					
						</select>		
						<input type="text" name="otherCity" id="otherCity" value="{{ getObjectValue($data,'Guardian_OtherCity') }}" class="form-control  {{ getObjectValue($data,'Guardian_isOtherCity') ? '' : 'hide not-required' }}">				
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Province</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="province" value="{{ getObjectValue($data,'Guardian_Province') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Zip Code</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="zipcode" value="{{ getObjectValue($data,'Guardian_ZipCode') }}">
						</div>
					</div>
				</div>
			</div>
		</div>	
		<!--/row-->
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Billing Address <small>(<i>Pls check if same addess as above.</i>)</small> <input type="checkbox" class="form-control" id="CopyAddress"></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Address<span class="required">*</span></label>
					<div class="col-md-10">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="BillingResident"  value="{{ getObjectValue($data,'Guardian_Billing_Address') }}" maxlength="240" required/>
						</div>
					</div>
				</div>
			</div>
			<!--
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Address line 2 <span class="required">*</span></label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text" class="form-control" name="BillingStreet"  value="{{ getObjectValue($data,'Guardian_Billing_Street') }}" required/>
						</div>
					</div>
				</div>
			</div>
			-->
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Barangay</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input type="text"  class="form-control" name="BillingBrangay" value="{{ getObjectValue($data,'Guardian_Billing_Barangay') }}">
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Country <span class="required">*</span></label>
					<div class="col-md-8">
						<select class="form-control" name="BillingCountry" id="BillingCountry" required>
						@foreach(App\Modules\Setup\Models\Country::get() as $row)
						<option {{ trim(getObjectValue($data,'Guardian_Billing_CountryCode')) == '' ? (strtoupper($row->Code) == defaultCountry()) ? 'selected' : '' : '' }} {{ getObjectValue($data,'Guardian_Billing_CountryCode') == $row->Code ? 'selected' : '' }} value="{{ $row->Code }}">{{ $row->Country }}</option>
						@endforeach
						</select>						
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">City <span class="required">*</span> <br /><input type="checkbox" name="BillingIsOtherCity" id="BillingIsOtherCity" {{ getObjectValue($data,'Guardian_Billing_IsOtherCity') ? 'checked' : '' }}>&nbsp;<i><small>If Other</small></i></label>
					<div class="col-md-8">
						<input type="hidden" class="form-control not-required CitySel" id="BillingCitySel" name="BillingCitySel">
						<select  class="form-control CitySelOption select2 {{ getObjectValue($data,'Guardian_Billing_IsOtherCity') ? 'hide not-required' : '' }}" name="BillingCity" id="BillingCity">
							<option value="">Select...</option>
						@foreach(App\Modules\Setup\Models\City::where('CountryCode',getObjectValueWithReturn($data,'Guardian_CountryCode',defaultCountry()))->get() as $city)
							<option data-id={{ $city->CityID }} {{ trim(strtolower(getObjectValue($data,'Guardian_Billing_CityID'))) == trim(strtolower($city->CityID)) ? 'selected' : '' }}>{{ $city->City }}</option>
						@endforeach	
						</select>		
						<input type="text" name="BillingOtherCity" id="BillingOtherCity" value="{{ getObjectValue($data,'Guardian_Billing_OtherCity') }}" class="form-control {{ getObjectValue($data,'Guardian_Billing_IsOtherCity') ? '' : 'hide not-required' }}">				
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Province</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="BillingProvince" value="{{ getObjectValue($data,'Guardian_Billing_Province') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Zip Code</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input  type="text" class="form-control" name="BillingZipCode" value="{{ getObjectValue($data,'Guardian_Billing_ZipCode') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/row-->
		<h3 class="form-section">Contact Info</h3>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Telephone</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input type="text" class="form-control" name="telephone" value="{{ getObjectValue($data,'Guardian_TelNo') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Mobile <span class="required">*</span></label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-mobile"></i>
							<input type="text" class="form-control" name="mobile" value="{{ getObjectValue($data,'Guardian_Mobile') }}" required/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label col-md-3">Email <span class="required">*</span></label>
					<div class="col-md-6">
						<div class="input-icon">
							<i class="fa fa-envelope"></i>
							<input type="text" class="form-control" name="email" value="{{ getObjectValue($data,'Guardian_Email') }}" required/>
						</div>
					</div>
					<div class="col-sm-9 col-sm-offset-3">
						<h5><strong>*This will be used for all official school communication including payment links.</strong></h5>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-12">
				<div class="col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10">
					<label class="control-label">I am authorizing Everest Academy Manila to include this mobile no. in the homeroom class Viber group.</label><br/>
					<label><input type="radio" id="IncludeGroupa" name="IncludeGroup"  value="1" {{ ((getObjectValue($data,'IncludeViberGroup')=='1')?'checked':'') }} required/> Yes</label>
				    <label><input type="radio" id="IncludeGroupb" name="IncludeGroup"  value="0" {{ ((getObjectValue($data,'IncludeViberGroup')=='0')?'checked':'') }}/> No</label>
					<span id="IncludeGroup-error" class="help-block" style="display: none;">This field is required.</span>
				</div>
			</div>
		</div>
		<div class="row">
		    
			<div class="col-sm-12"><hr/></div>
			<div class="col-sm-12">
				<label class="checkbox">
					<input type="checkbox" id="datapriv" name="datapriv" {{ ((getObjectValue($data,'DataPrivacy')=='1')?'checked':'') }} required/>
					&nbsp;I/We hereby affirm that all information supplied is complete and accurate. I/We also hereby allow Everest Academy Manila to collect, use and process the above mentioned information for legitimate purposes specifically for enrolment and allow authorized personnel to process such information as contained in the <a href="javascript:void(0);" class="dataprivacy">Data Privacy Policy</a> of Everest Academy Manila.</label>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<button type="submit" class="btn green pull-right" id="submitGuardian">Next</button>
				</div>
			</div>
		</div>
	</div>
</div>
</form>