<div class="row result hide">
	<div class="col-md-5" style="padding-right: 0px !important;width: 36% !important">
		<img id="ChildPhoto" src="{{ url('guardian-connect/getPhoto?key='.Request::get('key')) }}" width="200" height="200">
	</div>
	<div class="col-md-7" style="width: 64% !important;">
		<form class="form">
			<table class="table" id="TableFilterResult">
				<tr>
					<td id="ChildNo" class="bold bg-grey-cascade" style="border-bottom: 1px solid #bfbfbf  !important;width: 30% !important;">
						App No.
					</td>
					<td id="AppNo">
						0001
					</td>
				</tr>
				<tr>
					<td class="bold bg-grey-cascade" style="border-bottom: 1px solid #bfbfbf  !important">
						Name
					</td>
					<td id="name">
						Juan Dela Cruz
					</td>
				</tr>
				<tr>
					<td class="bold bg-grey-cascade" style="border-bottom: 1px solid #bfbfbf  !important">
						Year Level
					</td>
					<td class="bold" id="YearLevel">
						Grade 1
					</td>
				</tr>
				<tr>
					<td class="bold bg-grey-cascade" style="border-bottom: 1px solid #bfbfbf  !important">
						Gender
					</td>
					<td class="bold" id="Gender">
						Male
					</td>
				</tr>
				<tr>
					<td class="bold bg-grey-cascade" style="border-bottom: 1px solid #bfbfbf  !important">
						Birth Date
					</td>
					<td class="bold" id="BirthDate">
						2017/1/2
					</td>
				</tr>
				<tr>
					<td class="bold right" colspan="2">
						<button type="button" class="btn btn-xs yellow showFilter">Not Your Child?</button>	
						<button type="button" class="btn btn-xs btn-primary showFilter"><i class="fa fa-search"></i> Show Filter</button>
						<button type="button" class="btn btn-xs btn-success" id="BtnConnectChild"><i class="fa fa-arrow-right"></i> Connect</button>
					</td>
				
				</tr>
			</table>
		</form>
	</div>
</div>
<div class="row filter">
	<div class="col-md-12">
		<form class="form" id="ConnectChildForm">
			<table class="table">
				<tr>
					<td colspan="2">
						<label class="label-control">Input Student No, if Old student otherwise Appplication No., <i>(optional)</i></label>
						<input type="text" class="form-control not-required" name="ChildNo" placeholder="Student No./App No." value="">
					</td>
				</tr>
				<tr>
					<td>
						<label class="label-control">First Name</label>
						<input type="text" class="form-control" name="FirstName" placeholder="First Name" value="">
						<input type="hidden" class="form-control" name="UserKey" id="UserKey" value="{{ encode(getUserID()) }}">
						<input type="hidden" class="form-control" name="isOld" id="isOld" value="">
					</td>
					<td>
						<label class="label-control">Last Name</label>
						<input type="text" class="form-control" name="LastName" placeholder="Last Name" value="">
					</td>
				</tr>
				<tr>
					<td>
						<label class="label-control">Middle Name <i>(Optional)</i></label>
						<input type="text" class="form-control not-required" name="MiddleName" placeholder="Middle Name (optional) ">
					</td>
					<td>
						<label class="label-control">Date of Birth</label>
						<input type="text" class="form-control date-picker" name="BirthDate" id="GuardianConnectDOB" data-date-format="dd/mm/yyyy" placeholder="Birth Date (DD/MM/YYYY)" value="">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button type="button" class="btn btn-success col-md-12" id="BtnSearchConnectChild"><i class="fa fa-search"></i> Search</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>