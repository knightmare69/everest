<form class="form">
	<table class="table">
		<tr>
			<td>
				<input type="text" class="form-control" name="FirstName" placeholder="First Name">
			</td>
			<td>
				<input type="text" class="form-control" name="LastName" placeholder="Last Name">
			</td>
		</tr>
		<tr>
			<td>
				<input type="text" class="form-control not-required" name="MiddleName" placeholder="Middle Name (optional) ">
			</td>
			<td>
				<input type="text" class="form-control date-picker" name="BirthDate" data-date-format="mm/dd/yyyy" placeholder="DD/MM/YYYY">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" class="btn btn-success"><i class="fa fa-search"></i>Search</button>
			</td>
		</tr>
	</table>
</form>