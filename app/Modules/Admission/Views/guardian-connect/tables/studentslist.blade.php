<?php

    $model = new \App\Modules\Registrar\Models\StudentRequests_model;
    $student = new \App\Modules\Students\Models\StudentProfile;
    
    
    $rs = $model->where('RequestBy', getUserID())
            ->where('RequestTypeID',3)
            ->get();
    $i=1;
?>
<table class="table table-hover table-stripe" id="tblstudents">
	<thead>
		<tr>
			<th>#</th>
			<th class="autofit" >IDNo.</th>
			<th>Name</th>
			<th>Gender</th>
            <th>Birth Date</th>
            <th>Year Level</th>						
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
        @foreach($rs as $r)
            <?php $data = $student->select(['LastName','FirstName', 'Gender', 'DateOfBirth', DB::raw(' dbo.fn_YearLevel_k12(YearLevelID, dbo.fn_ProgramClassCode(ProgID)) As YearLevel') ])->where('StudentNo', $r->StudentNo )->first(); ?>
            <tr>
                <td>{{$i}}</td>
                <td>{{$r->StudentNo}}</td>
                <td>{{$data->LastName.', '.$data->FirstName}}</td>
                <td>{{$data->Gender}}</td>
                <td>{{$data->DateOfBirth}}</td>
                <td>{{$data->YearLevel}}</td>
                <td><?= $r->IssuedBy == '' ? 'In-progress' : 'Granted' ?></td>
            </tr>
        @endforeach
	</tbody>
</table>