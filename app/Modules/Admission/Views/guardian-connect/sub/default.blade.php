<div class="row">
    <div class="col-md-12">
        @include('errors.event')
    </div>
</div>
<div class="portlet light bg-inverse">
	<div class="portlet-body form">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12" style="background-color: #fff !important;min-height: 400px !important">
	        	@include($views.'tables.studentslist')
	   		</div>
   			<div class="col-md-6 col-sm-6 col-xs-12">
	        	@include($views.'tables.filter')
	   		</div>
        </div>
	</div>
</div>
