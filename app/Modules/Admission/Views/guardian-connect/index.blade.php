<div class="portlet">
	<div class="portlet-body">
        <div class="alert alert-info">
            <i class="fa fa-info"></i> <span class="bold">Connect to your children.</span> This module enable your account to connect with your children's information in this academy            
        </div>
		@include($views.'sub.default')
	</div>
</div>