<?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $data = isset($data[0]) ? $data[0] : [];  ?>
<form action="#" class="form-horizontal" id="formGuardian">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-body">
		<h3 class="form-section">Student Info</h3>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Name</label>
					<div class="col-md-9">
						<input  type="text" class="form-control" placeholder="Name" name="name" value="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Status</label>
					<div class="col-md-3">
						<input  type="text" readonly class="form-control" placeholder="Status" name="name" value="">
					</div>
					<label class="control-label col-md-2 left" style="padding-left: 0px !important">App. No.</label>
					<div class="col-md-3" style="padding-left: 0px !important;width: 33% !important">
						<input  type="text" readonly class="form-control" placeholder="App No" name="name" value="">
					</div>
				</div>	
				<div class="form-group">
					<label class="control-label col-md-3">Gender</label>
					<div class="col-md-3">
						<input  type="text" readonly class="form-control" placeholder="Gender" name="name" value="">
					</div>
					<label class="control-label col-md-2 left" style="padding-left: 0px !important">Date of Birth</label>
					<div class="col-md-3" style="padding-left: 0px !important;width: 33% !important">
						<input  type="text" readonly class="form-control" placeholder="DOB" name="name" value="">
					</div>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="form-group">
					@if(App\Modules\Admission\Models\AdmissionGuardianPhoto::where('FamilyID',getGuardianFamilyID())->count() <= 0 )
					<label class="control-label col-md-12 photo-message center {{ !isParent() ? 'hide' : '' }}"><small class="font-red">Photo is required</small></label>
					@endif
					<label class="control-label col-md-12 center">Photo <small>(<b>JPG, PNG Only</b>)</small></label>
					<div class="col-md-12 center">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
								<img src="{{ url('/general/getGuardianPhoto?FamilyID='.encode(getObjectValue($data,'FamilyID'))) }}" id="GuardianPhotoThumbnail" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
							</div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input type="file" name="file" accept="image/x-png, image/jpeg" id="GuardianPhoto" class="GuardianPhoto {{ !isParent() ? 'not-required' : '' }}">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">
							<small>NOTE!</small> </span>
							&nbsp;<small>Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+, Opera11.1+ and FireFox6.0+. In older browsers the filename is shown instead.</small>
						</div>
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
	</div>
	<div class="form-actions">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn green" id="submitGuardian">Submit</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			</div>
		</div>
	</div>
</form>