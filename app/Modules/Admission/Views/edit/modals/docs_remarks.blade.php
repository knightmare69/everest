<table class="table">
	<thead>
		<tr>
			<th width="20%">Created By</th>
			<th width="20%">Created Date</th>
			<th>Remarks</th>
		</tr>
	</thead>
	<tbody>
	@foreach($data as $row)
	<tr>
		<td>{{ $row->FullName }}</td>
		<td>{{ $row->CreatedDate }}</td>
		<td>{{ $row->Remarks }}</td>
	</tr>
	@endforeach
	</tbody>
</table>