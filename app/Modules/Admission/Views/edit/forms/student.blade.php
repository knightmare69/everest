<?php 
	$data = App\Modules\Admission\Models\Admission::getAdmission(
		decode(Request::get('AppNo')),[
			'CampusID','TermID',
			'AppTypeID','GradeLevelID','ProgramID','MajorID',
			'LastName','FirstName','MiddleName','MiddleInitial','ExtName','NickName',
			'Gender','DateOfBirth','PlaceOfBirth','IsAdopted','HomeAddressUnitNo','HomeAddressStreet',
			'HomeAddressBrgy','HomeAddressCity','HomeAddressZipCode','HomePhone','CivilStatusID','NationalityID',
			'CitizenshipID','ForeignStudent','PassportNumber','VisaStatus','SSP_Number','Validity_of_Stay',
			'ReligionID','FamilyID','DenyReason',
			'Elem_School','Elem_Address','Elem_InclDates','Elem_GradeLevel','Elem_GWA','Elem_AwardHonor',
			'HS_School','HS_Address','HS_InclDates','HS_GradeLevel','HS_GWA','HS_AwardHonor',
			'TestingSchedID','ORNo','ORDate','ORSecurityCode','LRN','ProgClass',
			'Family_HealthStatus',
			'Family_Situation',
			'IsBaptized',
			'BaptizedIn_Date',
			'BaptizedIn_Church',
			'BaptizedIn_City',
			'WorshipPlace',
			'WorshipAddress', 'WorshipTelNo',
			'Marketing',
			'Family_PaymentResponsible',
			'PresentSchoolHead',
			'PresentSchoolContactNo',
			'PresentSchoolReasonLeaving',
			'PresentSchool',
			'PresentSchoolAddress',
			'PresentSchoolDateAttended',
			'PresentGradeLevel'
		]
	);
	$data = isset($data[0]) ? $data[0] : [];	
 ?>
<form action="#" class="form-horizontal" id="formStudent">
	<div class="form-body">	
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">First name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="FirstName" value="{{ getObjectValue($data,'FirstName') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Middle name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="MiddleName" id="MiddleName" value="{{ getObjectValue($data,'MiddleName') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Last name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="LastName" value="{{ getObjectValue($data,'LastName') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Middle Initial</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="MiddleInitial" id="MiddleInitial" value="{{ getObjectValue($data,'MiddleInitial') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Extension name</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="ExtensionName" value="{{ getObjectValue($data,'ExtName') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Nickname</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="NickName" value="{{ getObjectValue($data,'NickName') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Gender</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control" name="Gender">
							<option {{ strtoupper(getObjectValue($data,'Gender')) == 'M' ? 'selected' : '' }} value='M'>Male</option>
							<option {{ strtoupper(getObjectValue($data,'Gender')) == 'F' ? 'selected' : '' }} value='F'>Female</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4 hidden">Civil Status</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control hidden" name="CivilStatus">
							@foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
							<option {{ getObjectValue($data,'CivilStatusID') == $row->StatusID ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->CivilDesc }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Date of Birth <br /><small>(<i>ex: 01/20/1991</i>)</small></label>
					<div class="col-md-5">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker" data-minage="2" data-date-format="mm/dd/yyyy" name="DateOfBirth" id="DateOfBirth" value="{{ setDateFormat(getObjectValue($data,'DateOfBirth'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
					<div class="col-md-3">
						<label {{ disabledIfAdmitted() }} class="form-control not-required center" name="StudentAge" id="StudentAge"><label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label col-md-5">Place of Birth</label>
					<div class="col-md-7" style="padding: unset;">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PlaceOfBirth" value="{{ getObjectValue($data,'PlaceOfBirth') }}">
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-2" style="padding-left: 50px;">
				<div class="form-group">
					<label class="control-label">Is Adopted</label>
					<input {{ disabledIfAdmitted() }} type="checkbox" name="isAdopted" id="isAdopted" {{ getObjectValue($data,'IsAdopted') == '1' ? 'checked' : '' }}>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Home Address</label>
					<div class="col-md-2">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="HomeAddressUnitNo" placeholder="House/Unit No" value="{{ getObjectValue($data,'HomeAddressUnitNo') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="HomeAddressStreet" placeholder="Street" value="{{ getObjectValue($data,'HomeAddressStreet') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="HomeAddressBrgy" placeholder="Barangay" value="{{ getObjectValue($data,'HomeAddressBrgy') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2"></label>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="HomeAddressCity" placeholder="City" value="{{ getObjectValue($data,'HomeAddressCity') }}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="input-icon">
							<i class="fa fa-location-arrow"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="HomeAddressZipCode" placeholder="Postal Code" value="{{ getObjectValue($data,'HomeAddressZipCode') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-5">Home Phone</label>
							<div class="col-md-7">
								<div class="input-icon">
									<i class="fa fa-phone"></i>
									<input {{ disabledIfAdmitted() }} type="number" class="form-control not-required" name="HomePhone" value="{{ getObjectValue($data,'HomePhone') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row not-rtn">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Nationality</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control" name="isForeign" id="isForeign" value="0">
						<select {{ disabledIfAdmitted() }} class="form-control select2 notreturnFields" name="Nationality" id="Nationality">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{ getObjectValue($data,'NationalityID') == $row->NationalityID ? 'selected' : '' }}  data-isforeign="{{ $row->IsForeign }}" value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option data-isforeign="1" value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addNationality" value=""/>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Citizenship</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control" name="isForeignCitizen" id="isForeignCitizen" value="0">
						<select  {{ disabledIfAdmitted() }} class="form-control select2 notreturnFields" name="Citizenship" id="Citizenship">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
							<option {{ getObjectValue($data,'CitizenshipID') == $row->NationalityID ? 'selected' : '' }}  data-isforeign="{{ $row->IsForeign }}" value="{{ encode($row->NationalityID) }}">{{ $row->Nationality }}</option>
							@endforeach
							<option data-isforeign="1" value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addCitizenship" value=""/>
					</div>
				</div>
			</div>
		</div>
		<div class="row not-rtn">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Religion</label>
					<div class="col-md-8">
						<select {{ disabledIfAdmitted() }} class="form-control select2 notreturnFields" name="Religion">
							@foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
							@if($row->ReligionID != 31 && $row->Inactive != 1)
								<option {{ getObjectValue($data,'ReligionID') == $row->ReligionID ? 'selected' : '' }} value="{{ encode($row->ReligionID) }}">{{ $row->Religion }}</option>
							@elseif($row->ReligionID != 31 && getObjectValue($data,'ReligionID') ==  $row->Religion)
								<option selected value="{{ encode($row->ReligionID) }}">{{ Other.' » '.$row->Religion }}</option>
							@endif
							@endforeach
							<option {{ getObjectValue($data,'ReligionID') == '31' ? 'selected' : '' }} value="{{ encode(31) }}">Not Applicable</option>
							<option value="x">- Others -</option>
						</select>
						<input type="hidden" class="form-control not-required" name="addReligion" value=""/>
					</div>
				</div>
			</div>
			<div class="col-md-2" style="padding-left: 50px;">
				<div class="form-group">
					<label class="control-label">Is Baptized</label>
					<input {{ disabledIfAdmitted() }} type="checkbox" name="isBaptized" id="isBaptized" {{ getObjectValue($data,'IsBaptized') == '1' ? 'checked' : '' }}>
				</div>
			</div>
		</div>

		<div class="row not-rtn">
			<div class="col-md-8">
				<div class="form-group">
					<label class="control-label col-md-3" style="padding-left:unset;">Religion Baptized in</label>
					<div class="col-md-4">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker religionFields notreturnFields" data-date-format="mm/dd/yyyy" name="BaptizedDate" placeholder="Date" value="{{ setDateFormat(getObjectValue($data,'BaptizedIn_Date'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
					<div class="col-md-5">
						<div class="input-icon">
							<i class="fa fa-institution"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control religionFields notreturnFields" name="BaptizedChurch" placeholder="Church" value="{{ getObjectValue($data,'BaptizedIn_Church') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control religionFields notreturnFields" name="BaptizedCity" placeholder="City" value="{{ getObjectValue($data,'BaptizedIn_City') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row not-rtn">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2" style="padding-left:unset;padding-right:unset;">Parish / Place of worship</label>
					<div class="col-md-10">
						<div class="input-icon">
							<i class="fa fa-institution"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control religionFields notreturnFields" name="WorshipPlace" id="WorshipPlace" placeholder="" value="{{ getObjectValue($data,'WorshipPlace') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row not-rtn">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Parish Address</label>
					<div class="col-md-6">
						<div class="input-icon">
							<i class="fa fa-map-marker"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control religionFields notreturnFields" name="WorshipAddress" id="WorshipAddress" placeholder="" value="{{ getObjectValue($data,'WorshipAddress') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-md-2">Phone</label>
							<div class="col-md-10">
								<div class="input-icon">
									<i class="fa fa-phone"></i>
									<input {{ disabledIfAdmitted() }} type="number" class="form-control not-required" name="WorshipTelNo" value="{{ getObjectValue($data,'WorshipTelNo') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<legend class="foreign hidden " style="font-size: 15px;font-weight: bold;">If Foreign</legend>
		<div class="row foreign hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">Passport Number</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-ticket"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required foreignFields " name="PassportNo" value="{{ getObjectValue($data,'PassportNumber') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Visa Status</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-certificate"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required foreignFields" name="VisaStatus" value="{{ getObjectValue($data,'VisaStatus') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row foreign hidden">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">SSP Number</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-ticket"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required foreignFields" name="SSPNo" value="{{ getObjectValue($data,'SSP_Number') }}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3" style="padding-left: unset;">Validity of Stay</label>
					<div class="col-md-9">
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control date-picker not-required foreignFields" data-date-format="mm/dd/yyyy" name="ValidityStay" value="{{ setDateFormat(getObjectValue($data,'Validity_of_Stay'),'yyyy-mm-dd','mm/dd/yyyy') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>