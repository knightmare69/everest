<?php 

	$data = isset($dataEval) ? $dataEval : array(); 

	$type = '';
	if ( isPermissionHas('admission-evaluation','evaluate-initial-interview') ) {
		$type = 'INITIAL';
	} else if ( isPermissionHas('admission-evaluation','evaluate-student-interview') ) {
		$type = 'STUDENT';
	} else if ( isPermissionHas('admission-evaluation','evaluate-final-interview') ) {
		$type = 'FINAL';
	} 

?>
<form action="#" class="form-horizontal" id="formEvaluation" data-interview-type="{{$type}}" data-id="{{ encode(getObjectValue($data,'EvaluationID')) }}">
	<div class="form-body">	
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="col-md-12 text-left">Date Interviewed</label>
					<div class="col-md-12">
						<input type="text" class="form-control date-picker" data-date-format="mm/dd/yyyy" name="DateInterviewed" id="DateInterviewed" value="{{ setDateFormat(getObjectValue($data,'DateInterviewed'),'yyyy-mm-dd','mm/dd/yyyy') }}">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="col-md-12 text-left">&nbsp;&nbsp;</label>
					<div class="col-md-12">
						<select {{ isPermissionHas('admission-evaluation','evaluate-final-interview') ? '' : 'disabled' }} class="form-control" name="InterviewType" id="InterviewType">
							<option value=''>- SELECT -</option>
							<option {{ isPermissionHas('admission-evaluation','evaluate-initial-interview') ? 'selected' : '' }} {{ getObjectValue($data,'InterviewType') == 'INITIAL' ? 'selected' : '' }} value='INITIAL'>Initial Admission Interview</option>
							<option {{ isPermissionHas('admission-evaluation','evaluate-student-interview') ? 'selected' : '' }} {{ getObjectValue($data,'InterviewType') == 'STUDENT' ? 'selected' : '' }} value='STUDENT'>Student Interview</option>
							<option {{ isPermissionHas('admission-evaluation','evaluate-final-interview') ? 'selected' : '' }} {{ getObjectValue($data,'InterviewType') == 'FINAL' ? 'selected' : '' }} value='FINAL'>Final Parent Interview</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Remarks</label>
					<div class="col-md-12">
						<textarea {{ isPermissionHas('admission-evaluation','evaluation-remarks') ? '' : 'readonly' }} class="form-control {{ isPermissionHas('admission-evaluation','evaluation-remarks') ? '' : 'not-required' }}" name="Remarks" maxlength="300">{{ getObjectValue($data,'Remarks') }}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row {{ (getUserGroup() == 'principal' || getUserGroup() == 'director') ? 'hide' : '' }}">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Recommendation</label>
					<div class="col-md-12">
						<textarea {{ isPermissionHas('admission-evaluation','evaluation-remarks') ? '' : 'readonly' }} class="form-control {{ isPermissionHas('committee-remarks','evaluation-recommendation') ? '' : 'not-required' }}" name="Recommendation" maxlength="300">{{ getObjectValue($data,'Recommendation') }}</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
