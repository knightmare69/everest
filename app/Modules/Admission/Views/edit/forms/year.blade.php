<?php 
    $model = new App\Modules\Admission\Models\Admission;
    
    $data  = $model->selectRaw("*, dbo.fn_Admmision_ApplicationStatus(AppTypeID) As AppType, StatusID ")
            ->where('AppNo',decode(Request::get('AppNo')))
	 	     ->first();

	function setCampus($campus = '',$campusKey) {
		if ($campus != '') {
			if ($campus == $campusKey) {
				return 'selected';
			}
		}

		if ($campusKey == AppConfig()->defaultCampus()) {
			return 'selected';
		}
	}
 ?>
<form action="#" class="form-horizontal" id="formYear">
	<div class="form-body">
        <input type="hidden" readonly class="form-control not-required" name="gradeProgramID" id="gradeProgramID" value="{{ encode(getObjectValue($data,'ProgramID')) }}">
		<input type="hidden" readonly class="form-control not-required" name="gradeProgramClass" id="gradeProgramClass">
		<input type="hidden" readonly class="form-control not-required" name="gradeMajorID" id="gradeMajorID" value="{{ encode(getObjectValue($data,'MajorID')) }}">
		<input type="hidden" readonly class="form-control not-required" name="gradeProgramMajorID" id="gradeProgramMajorID" value="{{ getObjectValue($data,'MajorID') ? encode(getObjectValue($data,'MajorID')) : '' }}">
                
		<input type="text" class="form-control hide not-required do-not-review" name="IsHigherLevel" id="IsHigherLevel" value="{{ AppConfig()->isHigherLevel() }}">
		@if(AppConfig()->isHigherLevel())
		<div class="row hidden">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">&nbsp;</label>
					<div class="icheck-inline">
						<!-- <label><input type="radio" {{ disabledIfAdmitted() }}  name="EducLevel" value="basic" {{ getObjectValue($data,'IsCollege') == '1' ? '' : 'checked' }} class="icheck EducLevel hide" data-radio="iradio_square-grey"> Basic Education </label>
						<label><input type="radio" {{ disabledIfAdmitted() }} name="EducLevel"  value="higher" {{ getObjectValue($data,'IsCollege') == '1' ? 'checked' : '' }} class="icheck EducLevel" data-radio="iradio_square-grey"> College</label> -->
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		@endif
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">Academic Year &amp; Term </label>
					<div class="col-md-5">
						<select {{ disabledIfAdmitted() }} class="form-control" name="schoolYear" id="schoolYear">
							<option value="">- Select -</option>
							@foreach(App\Modules\Setup\Models\AcademicYearTerm::where('Hidden',0)->orderBy('AcademicYear','DESC')->get() as $row)
							<option  {{  getObjectValue($data,'TermID') == $row->TermID ? 'selected' : '' }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear }} {{ $row->SchoolTerm }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row BasicControl">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3 hidden">Campus Name</label>
					<div class="col-md-5">
						<select {{ disabledIfAdmitted() }} class="form-control {{ AppConfig()->isHigherLevel() ? 'not-required' : '' }} hidden" name="schoolCampus" id="schoolCampus">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{ setCampus(getObjectValue($data,'CampusID'),$row->CampusID) }} value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
	@if(AppConfig()->isHigherLevel())
		@if(AppConfig()->campusAndCourse1())
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-3">School Campus</label>
					<div class="col-md-8">
						<select class="form-control {{ !AppConfig()->campusAndCourse1Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse1Required() }}" name="schoolCampus1" id="schoolCampus1">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{ setCampus(getObjectValue($data,'Choice1_CampusID'),$row->CampusID) }}  value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Course First Choice</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control not-required" name="Course1Choice" id="Course1Choice" value="{{ encode(getObjectValue($data,'Choice1_Course')) }}">
						<input type="hidden" class="form-control not-required" name="Course1MajorChoice" id="Course1MajorChoice" value="{{ encode(getObjectValue($data,'Choice1_CourseMajor')) }}">
						<select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse1Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse1Required() }}" name="Course1" id="Course1" data-placeholder="Course">
						</select>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if(AppConfig()->campusAndCourse2())
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">School Campus</label>
					<div class="col-md-8">
						<select class="form-control {{ !AppConfig()->campusAndCourse2Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse2Required() }}" name="schoolCampus2" id="schoolCampus2">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{ setCampus(getObjectValue($data,'Choice2_CampusID'),$row->CampusID) }} value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Course Second Choice</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control not-required" name="Course2Choice" id="Course2Choice" value="{{ encode(getObjectValue($data,'Choice2_Course')) }}">
						<input type="hidden" class="form-control not-required" name="Course2MajorChoice" id="Course2MajorChoice" value="{{ encode(getObjectValue($data,'Choice2_CourseMajor')) }}">
						<select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse2Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse2Required() }}" name="Course2" id="Course2" data-placeholder="Course">
						</select>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if(AppConfig()->campusAndCourse3())
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">School Campus</label>
					<div class="col-md-8">
						<select class="form-control {{ !AppConfig()->campusAndCourse2Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse3Required() }}" name="schoolCampus3" id="schoolCampus3">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{ setCampus(getObjectValue($data,'Choice3_CampusID'),$row->CampusID) }}  value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Course Third Choice</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control not-required" name="Course3Choice" id="Course3Choice" value="{{ encode(getObjectValue($data,'Choice3_Course')) }}">
						<input type="hidden" class="form-control not-required" name="Course3MajorChoice" id="Course3MajorChoice" value="{{ encode(getObjectValue($data,'Choice3_CourseMajor')) }}">
						<select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse3Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse3Required() }}" name="Course3" id="Course3" data-placeholder="Course">
						</select>
					</div>
				</div>
			</div>
		</div>
		@endif
		@if(AppConfig()->campusAndCourse4())
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">School Campus</label>
					<div class="col-md-8">
						<select class="form-control {{ !AppConfig()->campusAndCourse4Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse4Required() }}" name="schoolCampus4" id="schoolCampus4">
							<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{ setCampus(getObjectValue($data,'Choice4_CampusID'),$row->CampusID) }} value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row HigherControl hide">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Course Fourth Choice</label>
					<div class="col-md-8">
						<input type="hidden" class="form-control not-required" name="Course4Choice" id="Course4Choice" value="{{ encode(getObjectValue($data,'Choice4_Course')) }}">
						<input type="hidden" class="form-control not-required" name="Course4MajorChoice" id="Course4MajorChoice" value="{{ encode(getObjectValue($data,'Choice4_CourseMajor')) }}">
						<select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse4Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse4Required() }}" name="Course4" id="Course4" data-placeholder="Course">
						</select>
					</div>
				</div>
			</div>
		</div>
		@endif
	@endif

    	<div class="form-group">
			<label class="control-label col-md-3">Application Type</label>
			<div class="col-md-5">
				<select  {{ disabledIfAdmitted() }} class="form-control" name="applicationType" id="applicationType">
					<option value=""> - Select -</option>
					<?php $hasSel = false; ?>
					@foreach(App\Modules\Admission\Models\Admission::getApplicationType() as $row)
						@if(isVisibleAppType($row->AppTypeID))
							@if(getObjectValue($data,'AppTypeID') == $row->AppTypeID)
							<?php $hasSel = true; ?>
							@endif
							@if($row->AppTypeID != 3 && $row->AppTypeID != 4)
							<option {{ (getObjectValue($data,'AppTypeID') == 3 && $row->AppTypeID == 1) ? 'selected' : '' }} {{ (getObjectValue($data,'AppTypeID') == 4 && $row->AppTypeID == 2) ? 'selected' : '' }}  {{ (getObjectValue($data,'AppTypeID') == $row->AppTypeID)  ? 'selected' : '' }} value="{{ encode($row->AppTypeID) }}">{{ $row->ApplicationType }}</option>
							@endif
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Grade Level</label>
			<div class="col-md-5">
				<input type="hidden" class="form-control not-required" id="SetProgClass" name="SetProgClass" value="{{ getObjectValue($data,'ProgClass') }}">
				<input type="hidden" class="form-control not-required" id="SetGradeLevel" name="SetGradeLevel" value="{{ encode(getObjectValue($data,'GradeLevelID')) }}">
				<select  {{ disabledIfAdmitted() }} class="form-control" name="gradeLevel" id="gradeLevel">
					<option value="">- SELECT -</option>
				</select>
			</div>
		</div>
        <div class="form-group">
            <label class="control-label col-md-3"></label>
            <div class="col-md-9">
                <input {{ disabledIfAdmitted() }} type="checkbox" name="wExistSib" id="wExistSib" {{ (getObjectValue($data,'AppTypeID') == 3 || getObjectValue($data,'AppTypeID') == 4) ? 'checked' : '' }}>
                <label class="control-label">I already have a child enrolled in Everest Academy</label>
           </div> 
        </div>


		
        <div class="form-group BasicControl" style="display: none;">
        	<label class="control-label col-md-5">Program Name</label>
        	<div class="col-md-7">
        		<input type="text" readonly class="form-control not-required" name="PogramName" id="gradeProgramName" value="{{ getObjectValue((isset($academicTrack[0]) ? $academicTrack[0] : []),'ProgramName') }}">        		
        	</div>
        </div>                
	</div>
    <div class="form-group majorWrapper {{ getObjectValue($data,'ProgClass') <= 11 ? 'hide' : '' }}">
		<label class="control-label col-md-3 hidden">Track</label>
		<div class="col-md-7">
			<!-- <select class="form-control hidden not-required" name="ProgramMajor" id="ProgramMajor">
				@foreach(App\Modules\Setup\Models\ProgramMajors::selectRaw('*, dbo.fn_MajorName(MajorDiscID) As major')->where('ProgID',29)->get() as $r)
				<option {{ getObjectValue($data,'MajorID') == $r->MajorDiscID ? 'selected' : ''  }} value="{{ encode($r->MajorDiscID) }}">{{ $r->major }}</option>
				@endforeach
			</select> -->
		</div>
	</div>    
    <h3 class="form-section hidden">Testing Schedule <small class="order-error hide font-red">OR # invalid inputs.</small></h3>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3 hidden">Schedule Date</label>
					<div class="col-md-9">
						<input type="hidden" readonly class="form-control not-required" name="ScheduleDateID" id="ScheduleDateID" value="{{ getObjectValue($data,'TestingSchedID') ? encode(getObjectValue($data,'TestingSchedID')) : '' }}">
						<?php 
							$ScheduleDates =  App\Modules\Admission\Models\Admission::getExamSchedDates(getObjectValue($data,'CampusID'),getObjectValue($data,'TermID'),getObjectValue($data,'GradeLevelID')); 
							$mySchedDate = App\Modules\Admission\Models\Admission::getMyExamSchedDate(getObjectValue($data,'TestingSchedID')); 
							$mySchedDate = isset($mySchedDate[0]) ? $mySchedDate[0] : [];
							$hasSched = false;
						?>

						<select {{ disabledIfAdmitted() }} class="form-control hidden not-required" name="ScheduleDate" id="ScheduleDate">
							<option value="">Select...{{ getObjectValue($mySchedDate,'TestingSchedID') }}</option>

							@foreach($ScheduleDates as $row)
								@if($row->TestingSchedID == getObjectValue($data,'TestingSchedID'))
									$hasSched = true;
								@endif
								<option {{ getObjectValue($data,'TestingSchedID') == $row->TestingSchedID ? 'selected' : '' }} value="{{ encode($row->TestingSchedID) }}">{{ $row->TestingSchedDesc }}</option>
							@endforeach
							@if(!$hasSched)
								<option selected value="{{  encode(getObjectValue($mySchedDate,'TestingSchedID')) }}">{{  getObjectValue($mySchedDate,'Description') }}</option>
							@endif
							@if(count($ScheduleDates) <= 0 && getObjectValue($mySchedDate,'TestingSchedID'))
							<option value="">None</option>
							@endif
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 hidden">Security Code</label>
					<div class="col-md-9">
						<input  {{ disabledIfAdmitted() }} type="text" class="form-control not-required hidden" name="ORSecurityCode" id="ORSecurityCode" value="{{ getObjectValue($data,'ORSecurityCode') }}">
					</div>
				</div>
			</div>
            <div class="col-md-6">
            <div class="form-group">
					<label class="control-label col-md-3 hidden">OR #</label>
					<div class="col-md-9">
						<input  {{ disabledIfAdmitted() }} type="text" class="form-control not-required hidden" name="ORCode" id="ORCode" value="{{ getObjectValue($data,'ORNo') }}">
					</div>
				</div>
            </div>
			<!--/span-->
		</div>
		<!--/row-->
                    
</form>