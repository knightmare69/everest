<form action="#" class="form-horizontal" id="formAppEval" data-interview-type="0" data-id="0">
	<div class="form-body">	
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="col-md-12 text-left">Date:</label>
					<div class="col-md-12">
						<input type="text" class="form-control date-picker" data-date-format="mm/dd/yyyy" name="DateEvaluate" id="DateEvaluate" value="{{ setDateFormat(getObjectValue($data,'DateInterviewed'),'yyyy-mm-dd','mm/dd/yyyy') }}">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="col-md-12 text-left">&nbsp;&nbsp;</label>
					<div class="col-md-12">
						<select class="form-control" name="RemarkType" id="RemarkType">
							<option value=''>- SELECT -</option>
							<option value='psycho'>Psychometrician Remarks</option>
							<option value='principal'>Principal/VP Remarks</option>
							<option value='proctor'>Proctor Remarks</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Remarks</label>
					<div class="col-md-12">
						<textarea class="form-control {{ isPermissionHas('admission-evaluation','evaluation-remarks') ? '' : 'not-required' }}" name="EvalRemarks" maxlength="1000">{{ getObjectValue($data,'Remarks') }}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label><input type="radio" id="recommenda" name="optrecommend" value="4"/> Highly recommended</label>
			</div>
			<div class="col-md-3">
				<label><input type="radio" id="recommendb" name="optrecommend" value="3"/> Recommended</label>
			</div>
			<div class="col-md-3">
				<label><input type="radio" id="recommendc" name="optrecommend" value="2"/> With Reservation</label>
			</div>
			<div class="col-md-3">
				<label><input type="radio" id="recommendd" name="optrecommend" value="1"/> Not recommended</label>
			</div>
		</div>
	</div>
</form>