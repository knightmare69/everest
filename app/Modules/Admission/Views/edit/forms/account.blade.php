<?php
	$data = App\Modules\Security\Models\Users\User::where('FamilyID',getGuardianFamilyID(Request::get('AppNo')))->get();
	$data = isset($data[0]) ? $data[0] : [];
?>
<form action="#" class="form-horizontal" id="formLoginAccount">
	<div class="form-body">

		<input type="hidden" id="IsValidateLoginAccount" name="IsValidateLoginAccount" value="{{ isParent() ? 0 : 1 }}">
		<input type="hidden" id="LoginID" name="LoginID" value="{{ encode(getObjectValue($data,'UserIDX')) }}">
		<input type="hidden" id="IsLoginValidated" name="IsLoginValidated" value="{{ trim(getObjectValue($data,'UserIDX')) ? 1 : 0 }}">
		<input type="hidden" id="IsAccountEdit" name="IsAccountEdit" value="{{ trim(getObjectValue($data,'UserIDX')) ? 1 : 0 }}">

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-12 left">
						Send Account to Guardian Email. 
						<input type="checkbox" class="form-control" name="IsEmailSend" id="IsEmailSend" {{ getObjectValue($data,'UserIDX') ? 'disabled' : '' }}>

						Create/Edit Account
						<input type="checkbox" class="form-control" {{ getObjectValue($data,'UserIDX') ? 'disabled' : '' }}   name="IsManageAccount" id="IsManageAccount">
					</label>
				</div>
				<!--/span-->
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label col-md-4">Username</label>
					<div class="col-md-8">
						<input type="text" class="form-control not-required" name="username" id="username" value="{{ getObjectValue($data,'Username') }}">
					</div>
				</div>
				<!--/span-->
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label col-md-4">Password</label>
					<div class="col-md-8">
						<input type="password" class="form-control not-required" name="password" id="password">
					</div>
				</div>
				<!--/span-->
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label col-md-4">Repeat Password</label>
					<div class="col-md-8">
						<input type="password" class="form-control not-required" name="rpassword" id="rpassword">
					</div>
				</div>
				<!--/span-->
			</div>
		</div>
	</div>
</form>