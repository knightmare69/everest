<div class="portlet-input input-inline pull-right"> 
    <label class="control-label"> Search : </label>                             
    <input type="text" class="dtcomm_search form-control input-sm input-inline"/>                        
</div> 
<table class="table table-bordered table-hover" id="tblcommittee">
    <thead>
        <tr class="heading">
            <th width="10"><input type="checkbox" class="chk-header"></th>
            <th><small>Admission Committee Remarks</small></th>
            <th class="center" width="20"><small>Admitted</small></th>
            <th class="center" width="20"><small>Conditional</small></th>
            <th class="center" width="20"><small>Wait&nbsp;Pool</small></th>
            <th class="center" width="20"><small>Denied</small></th>
            <th class="center"><small>Remarks By</small></th>
            <th class="center"><small>Date Entry</small></th>
        </tr>
    </thead>
    <tbody>
                
    </tbody>
</table>