<legend></legend>
<div class="portlet box grey-salt">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-helper bold" style="color:black;">&nbsp;Summary</span>
        </div>
    </div>
    <div class="portlet-body">
         <div class="row">
            <div class="col-md-12"> 
                <div class="portlet-input input-inline pull-right"> 
                    <label class="control-label"> Search : </label>                             
                    <input type="text" class="dteval_search form-control input-sm input-inline"/>                        
                </div> 
                <table class="table table-bordered table-hover" id="tblappeval">
                    <thead>
                        <tr class="heading">
                            <th width="10"><input type="checkbox" class="chk-header"></th>
                            <th class="center"><small>Date</small></th>
                            <th class="center"><small>Type</small></th>
                            <th class="center"><small>Remarks</small></th>
                            <th class="center"><small>Recommendation</small></th>
                            <th class="center"><small>Evaluated By</small></th>
                            <th class="center"><small>Evaluated Date</small></th>
                        </tr>
                    </thead>
                    <tbody>
                                
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
