<?php 
	$data = App\Modules\Admission\Models\Siblings::where('FamilyID',getFamilyIDByAppNo(Request::get('AppNo')))->get();
 ?>
<table class="table" id="tableSiblings">
	<thead>
		<tr>
			<th class="right" colspan="6">
				<button type="button" class="btn btn-primary" id="{{ !isAdmitted() ? 'btnAddSibling' : '' }}">Add new</button>
			</th>
		</tr>
		<tr>
			<td width="3">&nbsp;</td>
			<th width="30%">Full Name</th>
			<th width="15%">Date of Birth</th>
			<th width="10%">Age</th>
			<th width="10%">Gender</th>
			<th width="24%">School Attended</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr data-id="{{ encode($row->SiblingIDX) }}">
			<td>
				<button class="btn btn-sm  {{ !isAdmitted() ? 'btnSiblingRemove red' : 'font-grey' }}"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input type="text" {{ disabledIfAdmitted() }} name="siblingName" class="form-control siblingName" placeholder="Name" value="{{ $row->FullName }}">
			</td>
			<td>
				<input type="text" {{ disabledIfAdmitted() }} name="siblingDOB" class="form-control siblingDOB date-picker" data-date-format="mm/dd/yyyy" placeholder="DOB" value="{{ setDateFormat($row->DateofBirth,'yyyy-mm-dd','mm/dd/yyyy') }}">
			</td>
			<td>
				<input type="text" {{ disabledIfAdmitted() }} name="siblingAge" readonly class="form-control siblingAge" placeholder="Age" value="">
			</td>
			<td>
				<select {{ disabledIfAdmitted() }} class="form-control siblingGender" name="siblingGender">
					<option {{ strtoupper($row->Gender) == 'M' ? 'selected' : '' }} value="M">M</option>
					<option {{ strtoupper($row->Gender) == 'F' ? 'selected' : '' }} value="F">F</option>
				</select>
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="siblingSchool" class="form-control siblingSchool" placeholder="School" value="{{ $row->SchoolAttended }}">
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<table class="table hide" id="tableDefaultSibling">
	<tbody>
		<tr>
			<td>
				<button class="btn btn-sm red btnSiblingRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input type="text" name="siblingName" class="form-control siblingName" placeholder="Name">
			</td>
			<td>
				<input type="text" name="siblingDOB" class="form-control siblingDOB date-picker" data-date-format="mm/dd/yyyy" placeholder="DOB">
			</td>
			<td>
				<input type="text" name="siblingAge" readonly class="form-control siblingAge" placeholder="Age">
			</td>
			<td>
				<select class="form-control siblingGender" name="siblingGender">
					<option value="0">M</option>
					<option value="0">F</option>
				</select>
			</td>
			<td>
				<input type="text" name="siblingSchool" class="form-control siblingSchool" placeholder="School">
			</td>
		</tr>
	</tbody>
</table>

