<?php 
	$data = App\Modules\Admission\Models\Admission::getAdmission(
		Request::get('AppNo'),[
			'AppTypeID','GradeLevelID','ProgramID','NationalityID','ForeignStudent','FamilyID'
		]
	);

	$data = isset($data[0]) ? $data[0] : [];
	$ProgClass = App\Modules\Admission\Models\Admission::getProgClass(getObjectValue($data,'ProgramID'));
 ?>
<table class="table" id="tableDocuments"  data-nationality="{{ encode(getObjectValue($data,'NationalityID')) }}" data-YearLevel="{{ encode(getObjectValue($data,'GradeLevelID')) }}" data-AppType="{{ encode(getObjectValue($data,'AppTypeID')) }}" data-ProgClass="{{ encode(getObjectValue($ProgClass,'ProgClass')) }}">
	<thead>
		<tr>
			<td colspan="3" id="tableDocumentsMessage" class="font-red"></td>
		</tr>
		<tr>
			<th width="72%">Name</th>
			<th width="10%">Reviewed?</th>
			<th width="10%">Exempted?</th>
			<th width="10%">Required?</th>
			<th width="5%" class="center"><i class="fa fa-file"></i></th>
		</tr>
	</thead>
	<tbody>
	<?php $swipe_index = 0; ?>
	@foreach(App\Modules\Admission\Models\Admission::getRequiredDocsEditMode(decode(Request::get('AppNo')),getObjectValue($data,'AppTypeID'),getObjectValue($data,'ForeignStudent'),$ProgClass,getObjectValue($data,'GradeLevelID')) as $row)
		<tr data-has-attachment="{{ $row->HasAttachment }}" data-swipe-index="{{ $row->HasAttachment ? $swipe_index++ : 0 }}" data-tempID="{{ $row->TemplateID }}" data-family="{{ encode(getObjectValue($data,'FamilyID')) }}" data-id="{{ $row->DetailID }}" data-isRequired="{{ $row->IsRequired }}" data-entryid="{{ $row->EntryID }}" data-doc="{{ $row->RequirementID }}">
			<td>
			@if($row->HasAttachment)
				<a href="javascript:void(0)" class=" aViewDocs fancybox-button" data-rel="fancybox-button">
					{{ $row->DocDesc }}
				</a>
			@else
				{{ $row->DocDesc }}
			@endif
			</td>
			<td class="center">
			@if(getUserGroup() != 'parent')
				<input type="checkbox" {{ disabledIfAdmitted() }} class="form-control IsReviewed" {{ $row->IsReviewed ? 'checked' : '' }}>
			@else
				 {!! $row->IsReviewed ? '<label class="font-green">Yes</label>' : '<label class="font-default">No</label>' !!}
			@endif
			</td>
			<td class="center">
			@if(getUserGroup() != 'parent')
				<input type="checkbox" {{ disabledIfAdmitted() }} class="form-control IsExempted" {{ $row->IsExempted ? 'checked' : '' }}>
			@else
				 {!! $row->IsExempted ? '<label class="font-green">Yes</label>' : '<label class="font-default">No</label>' !!}
			@endif
			</td>
			<td class="center">
				{!! $row->IsRequired ? '<label class="font-green">Yes</label>' : '<label class="font-default">No</label>' !!}
			</td>
			<td class="center">
				<div class="fileUpload btn {{ $row->HasAttachment == '1' ? 'btn-success' : ($row->IsRequired ?  'btn-danger' : 'bg-grey-cascade') }}">
	                <span><i class="fa fa-file"></i></span>
	                <input {{ disabledIfAdmitted() }} type="file" class="upload" />
	            </div>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


