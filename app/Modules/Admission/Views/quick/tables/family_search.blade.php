<?php $data = isset($data) ? $data : []; ?>
<table class="table" id="tableFamilySearch" style="overflow: auto !important" max-height="300px !important;">
	<thead>
		<tr>
			
			<td colspan="4">
				<div class="input-group">
					<input type="text" class="form-control" id="FamilyFilter" placeholder="Search By Name, Birth Date and email" value="{{ Request::get('filter') }}">
					<span class="input-group-addon cursor-pointer btn_search_family">
						<i class="fa fa-search"></i>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<th width="65%">Name</th>
			<th width="20%">Email</th>
			<th width="10%">DOB</th>
			<th width="5%" class="center"><i class="fa fa-search"></i></th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr data-id="{{ $row->FamilyID }}">
			<td>{{ $row->name }}</td>
			<td>{{ $row->email }}</td>
			<td>{{ setDateFormat($row->dob,'yyyy-mm-dd','mm/dd/yyyy') }}</td>
			<td>
				<a href="javascript:;" class="btn btn-xs default btn-editable btnFamilySel"><i class="glyphicon glyphicon-chevron-up"></i> Select</a>
			</td>
		</tr>
		@endforeach
		@if(count($data) <= 0)
		<tr>
			<td colspan="4">No result found.</td>
		</tr>
		@endif
	</tbody>
</table>


