
<div class="form-group">
	<label class="control-label col-md-3">Last name</label>
	<div class="col-md-8">
		<input type="text" class="form-control" name="LastName">
	</div>
</div>
		
<div class="form-group">
	<label class="control-label col-md-3">First name</label>
	<div class="col-md-8">
		<input type="text" class="form-control" name="FirstName">
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Middle name</label>
	<div class="col-md-8">
		<input type="text" class="form-control not-required" name="MiddleName" id="MiddleName">
		<span class="margin-top-10 hide">
			<input type="checkbox" clasbs="form-control" id="IsElegitimateChild">
			Ilegetimate Child
		</span>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Date of Birth</label>
	<div class="col-md-5">
		<input type="text" data-date-format="mm/dd/yyyy" class="form-control date-picker" data-minage="2" name="DateOfBirth" id="DateOfBirth">
		<small style="width: 100% !important;">(<i>ex: mm/dd/yyyy</i>)</small>
	</div>
	<div class="col-md-3">
		<label class="form-control not-required center" name="StudentAge" id="StudentAge"></label>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Gender</label>
	<div class="col-md-8">
		<select class="form-control" name="Gender">
			<option value="">- SELECT -</option>
			<option value='M'>Male</option>
			<option value='F'>Female</option>
		</select>
	</div>
</div>


