<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Modules\Security\Controllers\VCLogin;

use App\Modules\Admission\Services\RegisterServiceProvider as registerService;
use App\Modules\Security\Models\Users\User;
use App\Modules\Admission\Models\GuardianBackround;
use Request;
use Response;
use Permission;
use DB;

class Account extends Controller{

	function __construct()
	{
		$this->initializer();
	}

    public function parentCreateAccount() {
        $model = new \App\Modules\Admission\Models\InquiryParent;
        $data = $model->where('id',decode(Request::get('p')))->first();
        if(env('AUTO_INQUIRY_ACCT')==1){
		
		}
		return view('auth/parent_create_account',$data);     
    }

	public function register()
 	{
        $result = ['error'=> false, 'message' => '' ,'trial'=>'testing'];
 		$post = Request::all();

 		$validator = $this->service->isValid($post);

 		if (!$validator['error']) {

            $type = getObjectValueWithReturn($post,'acctype', 0);

            if($type != 0 ) {
               //parent|guardian
               $guardianData = $this->guardian->create($this->service->guardian($post));
            }

			$accountData = $this->account->create($this->service->account($this->service->FamilyID,$post));
			if (getObjectValue($accountData,'UserIDX')) {
                $admission = new \App\Modules\Admission\Models\Admission;
                $childModel = new  \App\Modules\Admission\Models\InquiryChild;
                $parentModel = new  \App\Modules\Admission\Models\InquiryParent;

                // check if has parent inquiry ID
                $ParentInquiryID = decode(Request::get('ParentInquiryID'));
				$post['Inquired']= 0;
                // then merge inquiry child form to parent
                if ($ParentInquiryID != '') {
                    $child = $childModel
                                ->where('IsApplied',1)
                                ->where('InquiryParentID',$ParentInquiryID);

                    $helper = new \App\Modules\Admission\Services\Admission\Helper;
                    $post['Inquired'] = 1;
                    foreach($child->where('HasAdmissionApp',0)->get() as $row) {
                        $status = $admission->create([
                            'LastName'     => $row->lname,
                            'FirstName'    => $row->fname,
							'FamilyID'     => $this->service->FamilyID,
                            'GradeLevelID' => $row->YearLevelID,
                            'DateOfBirth'  => date('Y-m-d',strtotime($row->birth_date)),
							'TermID'       => $row->TermID,
                            'ProgClass'    => DB::table('ESv2_yearlevel')->where('YearLevelID',$row->YearLevelID)->select('ProgClass')->pluck('ProgClass'),
                            'ProgID'       => DB::table('ESv2_yearlevel')->where('YearLevelID',$row->YearLevelID)->select('ProgID')->pluck('ProgID'),
                            'AppNo'        => $helper->getAppNo(['schoolYear' => encode($row->TermID)]),
                            'AppDate'      => systemDate()
                        ]);
                        if ($status) {
                            $childModel->where('id',$row->id)
                                ->update([
                                    'HasAdmissionApp' => 1
                                ]);
                        }
                    }
                }

                $parentModel->where('id',$ParentInquiryID)
                    ->update(['HasAccount' => 1,'FamilyID' => $this->service->FamilyID]);

				$post['code']     = $accountData->ConfirmNo;
				$post['Email']    = 'jhe69samson@gmail.com';
 				$res = $this->service->email($post);
				$result = ['error'   => false, 
				           'message' => 'successfully registered', 
						   'email'   => $res, 
						   'redirect'=> url('registration/confirm?code=').$post['code'], 
						   ];
 			}

 			return $result;
 		}
 		return Response::json($validator);
 	}

 	public function registerConfirm()
 	{   
	    if(env('AUTO_INQUIRY_LOGIN')==1){
		  $uname = $this->account->where('ConfirmNo',trimmed(Request::get('code')))->get(['UserIDX','Username','Email']);
		  if($uname && count($uname)>0){
		    $inquire = DB::table('es_inquiry_parent')->where('email',$uname[0]->Email)->get();
			if(count($inquire)<=0){
				$account = new User;
				if($this->account->where('ConfirmNo',trimmed(Request::get('code')))->update(['IsConfirmed'=>1])) {
					return view('auth.confirmation',['confirmData' => successConfirm()]);
				} else {
					return view('auth.confirmation',['confirmData' => errorConfirm()]);
				}
			}
			
			if(Auth::loginUsingId($uname[0]->UserIDX)){
				$exec = DB::table('ESv2_Users')->where('UserIDX',$uname[0]->UserIDX)->update(['IsConfirmed'=>1,]);      
				putSessionData('inquired',1);
				putSessionData('currentStep',1);
				return redirect('/admission/apply');
			}
		  }else{
			return view('auth.confirmation',['confirmData'=> (object)['error'=>true,'message'=>'Invalid Code!!!']]);
		  }
		}else{
	      $validator = $this->service->isValid(Request::get('code'),'confirmation');
		  if (!$validator['error']) {
 			$account = new User;
 			if($this->account->where('ConfirmNo',trimmed(Request::get('code')))->update(['IsConfirmed'=>1])) {
				return view('auth.confirmation',['confirmData' => successConfirm()]);
 			} else {
 				return view('auth.confirmation',['confirmData' => errorConfirm()]);
 			}
 		  }
 		  return view('auth.confirmation',['confirmData'=> $validator]);
		}
 	}

 	public function sendVCode()
 	{  
        if(base64_encode(base64_decode(Request::get('key')))==Request::get('key')){
        if (decodeToken(Request::get('key'))) {
            if($this->service->emailVCode(decodeToken(Request::get('key')))) {
                return ['error'=>false,'message'=>'Successfully sent confirmation code.'];
            }
            return ['error'=>false,'message'=>'There was an error while send an email to your account. Please try again.'];
        }
        }else{
        if (Request::get('key')) {
            if($this->service->emailVCode(Request::get('key'))) {
                return ['error'=>false,'message'=>'Successfully sent confirmation code.'];
            }
            return ['error'=>false,'message'=>'There was an error while send an email to your account. Please try again.'];
        }
        }
 		return ['error'=>true,'message'=>'Invalid token. Could not find confirmation code for this account.'];
 	}

    public function RequestAccessCode()
 	{
        $email = Request::get('key');
 		if ( $email != '') {
 			if($this->service->requestAccessCode($email)) {
 				return ['error'=>false,'message'=>'Successfully sent confirmation code.'];
 			}
 			return ['error'=>false,'message'=>'There was an error while send an email to your account. Please try again.'];
 		}
 		return ['error'=>true,'message'=>'Invalid token. Could not find confirmation code for this account.'];
 	}
    
 	private function initializer()
 	{
 		$this->service = new registerService;
 		$this->account = new User;
 		$this->guardian = new GuardianBackround;
 	}
}
