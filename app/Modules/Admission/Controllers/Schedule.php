<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Security\Models\Users\User;
use App\Modules\Admission\Models\TourSchedule as Model;

use App\Modules\Admission\Services\Register\Validation as Validate;
use App\Modules\Admission\Models\InquiryTemp;
use App\Modules\Admission\Models\InquiryChild;
use App\Modules\Admission\Models\InquiryParent;
use App\Modules\Admission\Models\InquirySchedule;
use App\Modules\Admission\Services\InquiriesDatatable;
use App\Modules\Admission\Services\InquiryProperties;

use Request;
use Response;
use Permission;
use DB;
use CrystalReport;
use Mail;
use Config;

class Schedule extends Controller {

	use InquiryProperties;

	private $media =
		[
			'Title'=> 'Admission',
			'Description'=> 'Use this module to create admission tour schedule.',
			'js'		=> [
				'Admission/TourNew','Admission/inquirylisting'
			],
			'init'		=> ['Batch.init()','FN.datePicker()','ListingData.init()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'bootstrap-datetimepicker/js/bootstrap-datetimepicker',
				'bootstrap-timepicker/js/bootstrap-timepicker.min',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck',
                'bootstrap-summernote/summernote.min',
                'datatables/media/js/jquery.dataTables.min',
	            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
	            'datatables/extensions/Scroller/js/dataTables.scroller.min',
	            'datatables/plugins/bootstrap/dataTables.bootstrap'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
				'bootstrap-timepicker/css/bootstrap-timepicker.min',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all',
				'bootstrap-summernote/summernote'
			],
			'closeSidebar' => true
		];

	private $url = [ 'page' => 'admission/tour/' ];

	private $views = 'Admission.Views.tour.';

	private $status = ['PENDING' => 0,'APPROVED' => 1,'DISAPPROVED' => 2,'RESCHEDULE' => 3];

	private $schedule_type = 'tour';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{	
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views, 'status' => array_flip($this->status),'yearlevel' => $this->getYearLevel(),'terms' => $this->getTerms()]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function lists()
 	{	
        if ($this->permission->has('read')) {
        	$this->media['js'] = ['datatable','Admission/inquirylisting','Admission/email'];
        	$this->media['init'] = ['ListingData.init()','FN.multipleSelect()',"$('#summernote_1').summernote({height: 300});"];
            return view('layout',array('content'=>view($this->views.'lists')->with(['views'=>$this->views, 'yearlevel' => $this->getYearLevel(),'quarters' => $this->QuarterLists()]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function tourSchedule()
 	{	
        if ($this->permission->has('read')) {
        	$this->schedule_type = 'tour';
            return view('layout',array('content'=>view($this->views.'tour')->with(['views'=>$this->views, 'inquiries' => $this->getInquiries() ,'yearlevel' => $this->getYearLevel(),'schedtype' => $this->schedule_type, 'terms' => $this->getTerms()]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function examSchedule()
 	{	
        if ($this->permission->has('read')) {
        	$this->schedule_type = 'exam';
            return view('layout',array('content'=>view($this->views.'exam')->with(['views'=>$this->views, 'inquiries' => $this->getInquiries() ,'yearlevel' => $this->getYearLevel(),'schedtype' => $this->schedule_type, 'terms' => $this->getTerms()]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

	public function get() {
		$data = Model::select(['id','Schedule','ReSchedule','Status','Remarks'])
						->get();
		return ['result' => view($this->views.'table',['data' => $data, 'status' => array_flip($this->status)])->render()];
	}

	public function listings()
 	{
 		return $this->Datatable->filter();
 	}

	private function getInquiries() {
		return $this->inquiryChildModel
					->where('BatchID',0)
					->where('ScheduleType',$this->schedule_type)
						->get();
	}

	public function getYearLevel($key = null)
    {
        $columns = ['YearLevelID','YearLevel as YearLevelName','ProgClass'];
        $model = DB::table('vw_YearLevel');
        return $model->select($columns)
                    // ->where('ProgClass','=',21)
        			->where('YearLevelID','<>',16)
                    ->orderBy('SeqNo','asc')
                    ->get();
    }

    public function saveBatch() {
    	$data = [
    		'title' => Request::get('title'),
    		'max' => Request::get('Limit'),
    		'ScheduleType' => $this->schedule_type,
    		'BatchDate' => setDate(Request::get('schedule_date'),'Y-m-d').' '.setDate(Request::get('schedule_time'),'H:i:s'),
    	];

    	return $this->batch->insert(assertCreated($data)) ? successSave() : errorSave();
    }

    public function emailContent() {
    	return view($this->views.'forms.email');
    }

    public function formBatch() {
    	return view($this->views.'forms.batch');
    }

    public function scheduleBatch() {
    	return view($this->views.'tables.batch',['data' => $this->getBatch()])->render();	
    }

    public function nonSchedApplicants() {
    	return view($this->views.'tables.nonsched',['data' => $this->getNonSchedApplicants()])->render();	
    }

    public function withSchedApplicants($batchID) {
    	return view($this->views.'tables.withsched',['data' => $this->getWithSchedApplicants($batchID)])->render();	
    }

    public function getNonSchedApplicants() {
    	$model = $this->inquiryChildModel->data()
    				->select(['i.id','AcademicYear','SchoolTerm','YearLevelName as YearLevel','type','fname','lname'])
    				->where('BatchID',0)
    				->where('ScheduleType',$this->schedule_type);
    	if (Request::get('yearlevel')) {
    		$model = $model->where('i.YearLevelID',Request::get('yearlevel'));
    	}
    	if (Request::get('term')) {
    		$model = $model->where('i.TermID',Request::get('term'));
    	}
    	return $model->get();
    }

    public function getWithSchedApplicants($batchID) {
    	return $this->inquiryChildModel->data()
    					->select(['i.id','AcademicYear','SchoolTerm','YearLevelName as YearLevel','type','fname','lname'])
    					->where('BatchID',$batchID)
    					->where('ScheduleType',$this->schedule_type)
    						->get();
    }

    public function getTerms() {
        $columns = ['AcademicYear','SchoolTerm','TermID','Active_OnlineEnrolment'];
        $model = DB::table('es_ayterm');
        return $model->select($columns)
                    ->get();
    }

	public function getBatch() {
    	return $this->batch
    				->where('ScheduleType',$this->schedule_type)
    					->get();
    }    

    public function addApplicant($BatchID) {
    	$ids = Request::get('ids');
    	$ids = jsonToArray($ids);
    	foreach($ids[0] as $row) {
    		$this->inquiryChildModel
    			->where('id',$row['id'])
    			->update(['BatchID' => $BatchID]);
			$this->emailParentSchedule(
				$this->inquiryChildModel->getParentID($row['id']),
				$BatchID,
				$this->inquiryChildModel->getParentEmail($row['id']));
    	}
    	return successSave();
    }
	
	
    public function remApplicant($BatchID) {
    	$ids = Request::get('ids');
    	$ids = jsonToArray($ids);
    	foreach($ids[0] as $row) {
    		$this->inquiryChildModel
    			->where('id',$row['id'])
    			->update(['BatchID' => '0']);
    	}
    	return successSave();
    }
	
    public function conApplicant($BatchID) {
    	$ids = Request::get('ids');
    	$ids = jsonToArray($ids);
    	foreach($ids[0] as $row) {
    		ini_set('max_execution_time', 14400);
			$xid  = $this->inquiryChildModel->getParentID($row['id']);
			$post = $this->inquiryParentModel->where('id',$xid)->first();
			$post = [
				'fname'   => $post->fname,
				'email'   => $post->email,
				'confirm' => 1,
			];
			Mail::send('email.inquiry_interview', ['post'=>$post], function ($message) use ($post) {
				$message->from(env('SYS_EMAIL'), 'K-12 Notification');
				$message->to(getObjectValue($post,'email'))->subject('K to 12 Notification');
				$message->bcc(env('SYS_EMAIL'));
			});
    	}
    	return successSave();
    }

    public function saveParent()
 	{
        $result = ['error'=> false, 'message' => '' ];
 		$post = Request::all();

 		$validator = $this->isValidParent($post);

 		if (!$validator['error']) {

			$data = $this->createParent();
            $id = getObjectValue($data,'id');
			if ($id) {
                $result = ['error'=> false, 'id' => $id,'message' => 'Successfully created.'];
 			}

 			return $result;
 		}
 		return Response::json($validator);
 	}

 	public function saveChild($parentKey)
 	{
        $result = ['error'=> false, 'message' => '' ];
 		$post = Request::all();

 		$validator = $this->isValidChild($post);

 		if (!$validator['error']) {

			$data = $this->createChild($parentKey);
            $id = getObjectValue($data,'id');
			if ($id) {
                $result = ['error'=> false, 'message' => 'Successfully created.'];
 			}

 			return $result;
 		}
 		return Response::json($validator);
 	}

 	protected $message = [
		'error' => 'Verification link is invalid. Please try again.',
		'success' => "You're inquiry was successfully verified. Please monitor your email. We'll send you the schedule of tour."
	];
    
	public function isValidParent($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validate->inquiry($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		return ['error'=>false,'message'=> ''];
	}

	public function isValidChild($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validate->childInquiry($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		return ['error'=>false,'message'=> ''];
	}

	public function createParent()
	{
		$data = [
			'type' =>  'phone',
			'fname' =>  Request::get('fname'),
			'mname'	=>  Request::get('mname'),
			'lname' =>  Request::get('lname',0),
			'email' =>  Request::get('email'),
			'contact_no' =>  Request::get('contact_no',0),
			'CreatedBy'	=>  '',
			'CreatedDate' =>  systemDate(),
		];
		return $this->inquiryParentModel->create($data);
	}

	public function createChild($parentKey)
	{
		$data = [
			'TermID' => Request::get('schoolyear'),
			'InquiryParentID' => $parentKey,
			'type' =>  'phone',
			'ScheduleType' =>  Request::get('schedule_type'),
			'YearLevelID' =>  Request::get('yearlevel'),
			'ProgClass' =>  Request::get('progclass'),
			'fname' =>  Request::get('fname'),
			'mname'	=>  Request::get('mname'),
			'lname' =>  Request::get('lname'),
			'email' =>  '',
			'contact_no' =>  '',
			'CreatedBy'	=>  '',
			'CreatedDate' =>  systemDate(),
		];
		return $this->inquiryChildModel->create($data);
	}

	public function updateType($id) {
		$id = (int)numberonly($id);
		$model = $this->inquiryChildModel->where('id',$id);
		if (Request::get('type') == 'apply') {
			$status = $model->update([
						'IsApplied' => 1,
						'ScheduleType' => Request::get('type'),
						'AppliedDate' => systemDate()
					]);
			if ($status) {
				$parentHasAccount = $this->inquiryChildModel->parentHasAccount($id);
				if (!$parentHasAccount) {
					$this->emailParentAccountCreation(
						$this->inquiryChildModel->getParentID($id),
						$this->inquiryChildModel->getParentEmail($id)
					);
				}

				if ($parentHasAccount){
					if (!$this->inquiryChildModel->HasAdmissionApp($id)) {
						// update child status
						$this->inquiryChildModel->where('id',$id)
	                        ->update([
	                            'HasAdmissionApp' => 1
	                        ]);

	                    $admission = new \App\Modules\Admission\Models\Admission;
	                    $helper = new \App\Modules\Admission\Services\Admission\Helper;

	                    $childData = $this->inquiryChildModel->where('id',$id)->first();
	                    $status = $admission->create([
	                        'LastName'  => $childData->lname,
	                        'FirstName' => $childData->fname,
	                        'FamilyID'  => $this->inquiryChildModel->getFamilyID($id),
	                        'GradeLevelID' => $childData->YearLevelID,
	                        'TermID' => $childData->TermID,
	                        'ProgClass' => DB::table('es_yearlevel')->where('YearLevelID',$childData->YearLevelID)->select('ProgClass')->pluck('ProgClass'),
	                        'AppNo' => $helper->getAppNo(['schoolYear' => encode($childData->TermID)]),
	                        'AppDate' => systemDate()
	                    ]);

	                    $this->emailParentAdditionalApp(
							$this->inquiryChildModel->getParentID($id),
							$id,
							$this->inquiryChildModel->getParentEmail($id)
						);

					}
				}
			}
		} else if(Request::get('type')=='wait') {
			$status = $model->update(assertModified([
						'ScheduleType' => Request::get('type'),
					]));
		    $this->emailParentToWait(
							$this->inquiryChildModel->getParentID($id),
							$this->inquiryChildModel->getParentEmail($id)
						);
		}else if(Request::get('type')=='exam') {

			$status = $model->update(assertModified([
						'ScheduleType' => Request::get('type'),
					]));
		    $xid   = $this->inquiryChildModel->getParentID($id);
			$post  = $this->inquiryParentModel->where('id',$xid)->first();
			$child = $this->inquiryChildModel->where('id',$id)->first();
			$post = [
				'fname'  => $post->fname,
				'lname'  => $post->lname,
				'email'  => $post->email,
				'child'  => $child->fname.' '.$child->lname,
			];

			ini_set('max_execution_time', 14400);
	        Config::set('mail.host', 'smtp.googlemail.com');
	        Config::set('mail.port', '465');
	        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
	        Config::set('mail.password', 'everest123!');
	        // Mail::send('email.inquiry_interview', ['post'=>$post], function ($message) use ($post) {
	        //  $message->from(env('SYS_EMAIL'), 'K-12 Notification');
	        //  $message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
	        //  $message->bcc(env('SYS_EMAIL'));
	        // });
	        Mail::send('email.inquiry_interview', ['post'=>$post], function ($message) use ($post) {
	            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
	            $message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
	            // $message->bcc('admissions@everestmanila.edu.ph');
	            $message->bcc(env('SYS_EMAIL'));
	        });
		} else if(Request::get('type')=='denied'){
		    $xid    = $this->inquiryChildModel->getParentID($id);
			$post   = $this->inquiryParentModel->where('id',$xid)->first();
			$child  = $this->inquiryChildModel->where('id',$id)->first();
			$yrlvl  = DB::table("ESv2_YearLevel")->where('YearLevelID',$child->YearLevelID)->pluck('YearLevelName');
			$ayterm = DB::table("ES_AYTerm")->where('TermID',$child->TermID)->pluck('AcademicYear');
			$post = [
				'fname'  => $post->fname,
				'lname'  => $post->lname,
				'email'  => $post->email,
				'child'  => $child->fname.' '.$child->lname,
				'yrlvl'  => $yrlvl,
				'ayterm' => $ayterm,
			];
			// ini_set('max_execution_time', 14400);
	  //       Config::set('mail.host', 'smtp.googlemail.com');
	  //       Config::set('mail.port', '465');
	  //       Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
	  //       Config::set('mail.password', '@dmissions2019');
	  //       // Mail::send('email.inquiry_denied', ['post'=>$post], function ($message) use ($post) {
	  //       //  $message->from(env('SYS_EMAIL'), 'K-12 Notification');
	  //       //  $message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
	  //       //  $message->bcc(env('SYS_EMAIL'));
	  //       // });
	  //       Mail::send('email.inquiry_denied', ['post'=>$post], function ($message) use ($post) {
	  //           $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
	  //           $message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
	  //           // $message->bcc('admissions@everestmanila.edu.ph');
	  //           $message->bcc(env('SYS_EMAIL'));
	  //       });
			$deny   = DB::table('ESv2_Admission')->where(array('LastName'=>$child->lname,'FirstName'=>$child->fname,'TermID'=>$child->TermID))->update(array('StatusID'=>3));
			$status = $model->update(assertModified([
						'ScheduleType' => Request::get('type'),
					]));
		} else {
			$status = $model->update(assertModified([
						'ScheduleType' => Request::get('type'),
					]));
		}
		return $status ? successSave() : errorSave();
	}	
    
	private function emailParentToWait($id, $email) {
		ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
        $post = $this->inquiryParentModel->where('id',$id)->first();
        $post = [
            'fname' => $post->fname,
            'lname' => $post->lname,
            'email' => $post->email,
            'link' => url('parent/create-account?p='.encode($id))
        ];
        // Mail::send('email.inquiry_wait', ['post'=>$post], function ($message) use ($post) {
        //  $message->from(env('SYS_EMAIL'), 'K-12 Notification');
        //  $message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
        //  $message->bcc(env('SYS_EMAIL'));
        // });
        Mail::send('email.for_waiting', ['post'=>$post], function ($message) use ($post) {
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
            $message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
            // $message->bcc('admissions@everestmanila.edu.ph');
            $message->bcc(env('SYS_EMAIL'));
        });
        return 1;
	}
	
	private function emailParentSchedule($id,$batch, $email) {
		ini_set('max_execution_time', 14400);
		$post = $this->inquiryParentModel->where('id',$id)->first();
		$post = [
			'fname' => $post->fname,
			'email' => $post->email,
			'sched' => DB::SELECT("SELECT TOP 1 * FROM es_inquiry_batch WHERE id='".$batch."'"),
		];
		Mail::send('email.inquiry_interview', ['post'=>$post], function ($message) use ($post) {
			$message->from(env('SYS_EMAIL'), 'K-12 Notification');
        	$message->to(getObjectValue($post,'email'))->subject('K to 12 Notification');
            $message->bcc(env('SYS_EMAIL'));
		});
		return 1;
	}
	
	private function emailParentAccountCreation($id, $email) {
		ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
        $post = $this->inquiryParentModel->where('id',$id)->first();
        $post = [
            'fname' => $post->fname,
            'lname' => $post->lname,
            'email' => $post->email,
            'link' => url('parent/create-account?p='.encode($id))
        ];
        // Mail::send('email.inquiry_apply', ['post'=>$post], function ($message) use ($post) {
        //  $message->from(env('SYS_EMAIL'), 'K-12 Notification');
        //  $message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
        //  $message->bcc(env('SYS_EMAIL'));
        // });
        Mail::send('email.for_application', ['post'=>$post], function ($message) use ($post) {
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
            $message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
            // $message->bcc('admissions@everestmanila.edu.ph');
            $message->bcc(env('SYS_EMAIL'));
        });
        return 1;
	}

	private function emailParentAdditionalApp($id, $childid, $email) {
		ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
        $post = $this->inquiryParentModel->where('id',$id)->first();
		$child = $this->inquiryChildModel->where('id',$childid)->first();
        $post = [
            'fname' => $post->fname,
            'lname' => $post->lname,
            'email' => $post->email,
            'child' => $child->fname
        ];
  //      Mail::send('email.inquiry_apply_added', ['post'=>$post], function ($message) use ($post) {
		// 	$message->from(env('SYS_EMAIL'), 'K-12 Notification');
  //       	$message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
  //           $message->bcc(env('SYS_EMAIL'));
		// });
        Mail::send('email.inquiry_apply_added', ['post'=>$post], function ($message) use ($post) {
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
            $message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
            $message->bcc('sis.sbca.20151023@gmail.com');
            $message->bcc(env('SYS_EMAIL'));
        });
        return 1;
	}
	
	public function email($post)
	{	
		ini_set('max_execution_time', 14400);
		Mail::send('email.inquiry_confirmation', ['post'=>$post], function ($message) use ($post) {
			$message->from(env('SYS_EMAIL'), 'K-12 Notification');
        	$message->to(getObjectValue($post,'email'))->subject('K to 12 Email Confirmation');
            $message->bcc(env('SYS_EMAIL'));
		});
		return 1;
	}

	public function sendManualEmail() 
	{
		ini_set('max_execution_time', 14400);
        Mail::send('email.template', array(), function ($message) {
        	$emails = explode(',', Request::get('parent'));
			$message->from(env('SYS_EMAIL'), 'K-12 Notification');
			foreach($emails as $e) {
			    $message->to($e)->subject(Request::get('title'));
			}
			$message->bcc(env('SYS_EMAIL'));
		});
		return 1;
	}


    /*
	public function edit($id) {

		$response = [
			'date' => '',
			'time' => '',
			'remarks' => ''
		];

		$data = Model::select(['id','Schedule','Status','Remarks'])
				->where('id',$id)
					->first();

		if ($data){
			$response = [
				'date' => setDate($data->Schedule,'m/d/Y'),
				'time' => setDate($data->Schedule,'h:i A'),
				'remarks' => $data->Remarks,
				'status' => $data->Status,
				'is_admin' => isAdmin()
			];
		}

		return $response;

	}

	private function save() {
		return Model::insert(assertCreated([
			'TermID' => 1,
			'Schedule' => setDate(Request::get('schedule_date'),'Y-m-d').' '.setDate(Request::get('schedule_time'),'H:i:s'),
			'Remarks' => Request::get('remarks'),
			'Status' => $this->status['PENDING'],
		]));
	}

	private function update($id) {
		$id = (int)$id;
		
		if (isAdmin()) {
			$update = Model::where('id',$id)
				->update(assertModified([
					'TermID' => 1,
					'Remarks' => Request::get('remarks'),
				]));
			if (Request::get('schedule_status') != -1) {
				Model::where('id',$id)
					->update(assertModified([
						'Status' => Request::get('schedule_status'),
					]));
			}

			if ($update) {
				$date = setDate(Request::get('schedule_date'),'Y-m-d');
				$time = setDate(Request::get('schedule_date'),'H:i');
				if (Model::where(DB::raw('DATE(Schedule)'),$date)->where(DB::raw('TIME(Schedule)'),$time)->count() <= 0) {
					Model::where('id',$id)
						->update(assertModified([
							'ReSchedule' => setDate(Request::get('schedule_date'),'Y-m-d').' '.setDate(Request::get('schedule_time'),'H:i:s')
						]));
				}
			}
		} else {
			$update = Model::where('id',$id)
				->update(assertModified([
					'TermID' => 1,
					'Schedule' => setDate(Request::get('schedule_date'),'Y-m-d').' '.setDate(Request::get('schedule_time'),'H:i:s'),
					'Remarks' => Request::get('remarks'),
				]));
		}
				
			

		return $update;
	}

	private function delete($id) {
		$id = (int)$id;
		return Model::where('id',$id)->delete();
	}
	*/

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-tour');
 		$this->inquiryParentModel = new InquiryParent;
        $this->inquiryChildModel = new InquiryChild;
        $this->validate = new Validate;
        $this->batch = new InquirySchedule;
        $this->Datatable = new InquiriesDatatable;
        if (Request::get('schedtype')) {
        	$this->schedule_type = Request::get('schedtype');
        }
 	}
}
