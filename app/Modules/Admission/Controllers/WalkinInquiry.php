<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;

use App\Modules\Admission\Services\InquiryService;
use App\Modules\Admission\Services\Register\Validation as Validate;
use App\Modules\Admission\Models\InquiryTemp;
use App\Modules\Admission\Models\InquiryParent;
use App\Modules\Admission\Models\InquiryChild;

use Request;
use Response;
use Permission;
use DB;

class WalkinInquiry extends Controller{

    use InquiryService;

	private $media = [ 'Title'=> 'Inqyuiry',
            'Description'=> 'Student admission module',
            'js'        => [
                'Admission/review','Admission/admission',
                'Admission/admissionHelper','Admission/admissionData',
                'Admission/wizzard',
                'Admission/College','Admission/helper'
            ],
            'init'      => ['ADMISSION.init()','FN.datePicker()','FN.multipleSelect()'],
            'plugin_js' => [
                'bootbox/bootbox.min',
                'jquery-validation/js/jquery.validate.min',
                'bootstrap-datepicker/js/bootstrap-datepicker',
                'select2/select2.min',
                'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
            ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'bootstrap-select/bootstrap-select.min',
                'select2/select2',
                'jquery-multi-select/css/multi-select',
                'wizzard-tab/css/gsdk-base',
                'bootstrap-fileinput/bootstrap-fileinput',
                'icheck/skins/all'
            ]
        ];

    private $url = [ 'page' => 'inquiry/' ];

    private $views = 'Admission.Views.inquiry.';

    private $type = 'walk-in';

    function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        return view('auth/inquiry', ['yearlevel' => $this->getYearLevel(),'terms' => $this->getTerms(),'type' => $this->type]);     
    }

    public function getYearLevel($key = null)
    {
        $columns = ['YearLevelID','YearLevel as YearLevelName'];
        $model = DB::table('vw_YearLevel');
        return $model->select($columns)
                    ->where('YearLevelID','<>',16)
                    ->orderBy('SeqNo','asc')
                    ->get();
    }

    public function getTerms() {
        $columns = ['AcademicYear','SchoolTerm','TermID','Active_OnlineEnrolment'];
        $model = DB::table('es_ayterm');
        return $model->select($columns)
                    ->get();
    }

	public function register()
 	{
        $result = ['error'=> false, 'message' => '' ];
 		$post = Request::all();

 		$validator = $this->isValid($post);

 		if (!$validator['error']) {

			$data = $this->createParent();
            $id = getObjectValue($data,'id');
			if ($id) {
                $link = url('inquiry/verify/'.base64_encode($id));
                $email = [
                    'fname' => $data->fname,
                    'email' => $data->email,
                    'verification_link' => $link
                ];
                // $this->email($email);
                $result = [
                    'error'=> false, 
                    'id' => $id,
                    'message' => '<h2>Congratulations!</h2> Your information was submitted successfully!. You will be redirected to child account application page.'
                ];
                // $result = ['error'=> false, 'message' => '<h2>Congratulations!</h2> Your information was submitted successfully!. You will be redirected to child account application page.'];
 			}

 			return $result;
 		}
 		return Response::json($validator);
 	}

    public function registerChild($ParentKey)
    {
        $result = ['error'=> false, 'message' => '' ];
        $post = Request::all();

        $validator = $this->isValidChild($post);

        if (!$validator['error']) {

            $data = $this->createChild($ParentKey);
            $id = getObjectValue($data,'id');
            if ($id) {
                $link = url('inquiry/verify/'.base64_encode($id));
                $email = [
                    'fname' => $data->fname,
                    'email' => $data->email,
                    'verification_link' => $link
                ];
                // $this->email($email);
                $result = ['error'=> false, 'message' => '<h2>Congratulations!</h2> Your information was submitted successfully!.'];
                // $result = ['error'=> false, 'message' => '<h2>Congratulations!</h2> Your information was submitted successfully!. You will be redirected to child account application page.'];
            }

            return $result;
        }
        return Response::json($validator);
    }

 	public function verify($id)
 	{
        $id = (int)base64_decode($id);
        $verified = false;
 		if ($this->inquiryTempModel->where('id',$id)->limit(1)->count() > 0) {
            $verified = true;
            //create inquiry when verified
            $this->create($id);
        }
        return view('auth/inquiry_verified',['verified' => $verified,'message' => $this->message[$verified?'success' : 'error']]);
 	}

    
 	private function initializer()
 	{
        $this->inquiryParentModel = new InquiryParent;
        $this->inquiryChildModel = new InquiryChild;
        $this->validate = new Validate;
 	}
}
