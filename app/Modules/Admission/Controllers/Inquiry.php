<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;

use App\Modules\Security\Models\Users\User as UserModel;
use App\Modules\Security\Services\UserServiceProvider;

use App\Modules\Admission\Services\InquiryService;
use App\Modules\Admission\Services\Register\Validation as Validate;
use App\Modules\Admission\Models\InquiryTemp;
use App\Modules\Admission\Models\InquiryParent;
use App\Modules\Admission\Models\InquiryChild;

use Request;
use Response;
use Permission;
use DB;
use Mail;
use Config;

class Inquiry extends Controller{

    use InquiryService;

	private $media = [ 'Title'=> 'Inqyuiry',
            'Description'=> 'Student admission module',
            'js'        => [
                'Admission/review','Admission/admission',
                'Admission/admissionHelper','Admission/admissionData',
                'Admission/wizzard',
                'Admission/College','Admission/helper'
            ],
            'init'      => ['ADMISSION.init()','FN.datePicker()','FN.multipleSelect()'],
            'plugin_js' => [
                'bootbox/bootbox.min',
                'jquery-validation/js/jquery.validate.min',
                'bootstrap-datepicker/js/bootstrap-datepicker',
                'select2/select2.min',
                'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
            ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'bootstrap-select/bootstrap-select.min',
                'select2/select2',
                'jquery-multi-select/css/multi-select',
                'wizzard-tab/css/gsdk-base',
                'bootstrap-fileinput/bootstrap-fileinput',
                'icheck/skins/all'
            ]
        ];

    private $url = [ 'page' => 'inquiry/' ];

    private $views = 'Admission.Views.inquiry.';

    private $type = 'email';

    function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        return view('auth/inquiry', ['yearlevel' => $this->getYearLevel(),'terms' => $this->getTerms(),'type' => $this->type]);     
    }

    public function getYearLevel($key = null)
    {
        $columns = ['YearLevelID','YearLevel as YearLevelName','ProgClass'];
        $model = DB::table('vw_YearLevel');
        return $model->select($columns)
                    ->where('YearLevelID','<>',16)
                    ->orderBy('SeqNo','asc')
                    ->get();
    }

    public function getTerms() {
        $columns = ['AcademicYear','SchoolTerm','TermID','Active_OnlineEnrolment'];
        $model = DB::table('es_ayterm');
        return $model->select($columns)
                    ->get();
    }

	public function register()
 	{
        $result = ['error'=> false, 'message' => '' ];
 		$post = Request::all();

 		$validator = $this->isValid($post);
 		if (!$validator['error']){
			$chk  = DB::select("SELECT TOP 1 * FROM es_inquiry_parent WHERE Email='".getObjectValue($post,'email')."' ORDER BY id DESC");
			if(count($chk)<=0){
			$tmp  = [ 'Username'      => getObjectValue($post,'email'),
					  'password'      => bcrypt(getObjectValue($post,'email')),
					  'FullName'      => getObjectValue($post,'lname').','.getObjectValue($post,'fname'),
					  'Email'         => getObjectValue($post,'email'),
					  'PositionID'	  => getObjectValue($post,'position',''),
					  'DepartmentID'  => getObjectValue($post,'department',''),
					  'FacultyID'     => getObjectValue($post,'facultyid',''),
					  'UsersGroupID'  => 2,		
					  'PwdExpiryDate' => date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').' +5 years')),
					  'CreatedBy'	  => 0,
					  'CreatedDate'	  => systemDate(),
					  'IsConfirmed'   => 1,
					];
			$user = ((env('AUTO_INQUIRY_ACCT')==1) ? $this->umodel->create($tmp) : true);
			$data = $this->createParent();
			$id   = getObjectValue($data,'id');
			}else{
			$data = $chk[0];
            $id   = getObjectValue($data,'id');			
			}
			if ($id) {
                $link = url('inquiry/verify/'.base64_encode($id));
                $email = [
                    'fname' => $data->fname,
                    'email' => $data->email,
                    'verification_link' => $link
                ];
				
                // $this->email($email);
                $result = [
                    'error'=> false, 
                    'id' => $id,
                    'message' => '<h3>Congratulations!</h3> Your information was submitted successfully! You will be redirected to child account application page.'
                ];
 			}

 			return $result;
 		}
 		return Response::json($validator);
 	}

    public function registerChild($ParentKey){
        $result = ['error'=> false, 'message' => '' ];
        $post = Request::all();

        $validator = $this->isValidChild($post);

        if (!$validator['error']) {
            $sy = DB::table('es_ayterm')->select('StartofAY')->where('TermID',$post['schoolyear'])->get();
            $date = $sy[0]->StartofAY; // pass the value of input first.
            $date = explode('-', $date); // explode to get array of YY-MM-DD
            $date[2] = '31'; // this would change the previous value of DD/Day to this one.
            // then implode the array again for datetime format.
            $date = implode('-', $date); // that will output 'YY-MM-DD'.

            $age = date_diff(date_create($post['bdate']), date_create($date))->y;
                
            if ($age > 4) {
                $chk  = DB::select("SELECT TOP 1 i.id,ip.fname,ip.email FROM es_inquiry as i INNER JOIN es_inquiry_parent as ip ON i.InquiryParentID=ip.id WHERE InquiryParentID='".$ParentKey."'");
                if(count($chk)<=0){
                $data = $this->createChild($ParentKey);
                }else{
                $data = $chk[0];        
                }
                $id = getObjectValue($data,'id');
                if ($id){
                    $link = url('inquiry/verify/'.base64_encode($id));
                    $email = [
                        'fname' => $data->fname,
                        'email' => $data->email,
                        'verification_link' => $link
                    ];
                    $this->emailNotifyAdmissions($ParentKey,$email);
                    /*
                    if(env('AUTO_INQUIRY_EMAIL')==1){
                      if(env('AUTO_INQUIRY_ACCT')==1){  
                        $this->emailParentApplication($ParentKey,$email);
                      }else{
                        $this->emailParentAccountCreation($ParentKey,$email);
                      }
                    }
                    */
                    $result = ['error'=> false, 'message' => '<h2>Congratulations!</h2> Your information was submitted successfully! Please expect an email from admissions.'];
                }

                return $result;

            } else {
                return ['error'=>true,'message'=>'Thank you for your interest in Everest Academy. We regret to inform you that your child does not meet the age requirement for Kindergarten, which is 5 years old by August 31 of the school year. We look forward to your future inquiry and application.'];
            }

        }

        return Response::json($validator);
    }

 	public function verify($id)
 	{
        $id = (int)base64_decode($id);
        $verified = false;
 		if ($this->inquiryTempModel->where('id',$id)->limit(1)->count() > 0) {
            $verified = true;
            //create inquiry when verified
            $this->create($id);
        }
        return view('auth/inquiry_verified',['verified' => $verified,'message' => $this->message[$verified?'success' : 'error']]);
 	}
	
    private function emailNotifyAdmissions($id, $email) {
        ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
        $post = $this->inquiryParentModel->where('id',$id)->first();
        $post = [
            'parent'=> $id,
            'fname' => $post->fname,
            'lname' => $post->lname,
            'email' => $post->email,
            'link'  => url('parent/create-account?p='.encode($id))
        ];
        // Mail::send('email.inquiry_apply', ['post'=>$post], function ($message) use ($post) {
        //  $message->from(env('SYS_EMAIL'), 'K-12 Notification');
        //  $message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
        //  $message->bcc(env('SYS_EMAIL'));
        // });
        Mail::send('email.inquiry_admission_notify', ['post'=>$post], function ($message) use ($post) {
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
          //$message->to('admissions@everestmanila.edu.ph')->subject('Everest Academy Manila Admissions Inquiry');
            $message->to('jhe69samson@gmail.com')->subject('Everest Academy Manila Admissions Inquiry');
            $message->cc('jhe69samson@gmail.com');
            $message->bcc('sis.sbca.20151023@gmail.com');
            $message->bcc(env('SYS_EMAIL'));
        });
        return 1;

    }

	private function emailParentAccountCreation($id, $email) {
        ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
        $post = $this->inquiryParentModel->where('id',$id)->first();
        $post = [
            'fname' => $post->fname,
            'lname' => $post->lname,
            'email' => $post->email,
            'link' => url('parent/create-account?p='.encode($id))
        ];
        // Mail::send('email.inquiry_apply', ['post'=>$post], function ($message) use ($post) {
        //  $message->from(env('SYS_EMAIL'), 'K-12 Notification');
        //  $message->to(getObjectValue($post,'email'))->subject('K to 12 Application');
        //  $message->bcc(env('SYS_EMAIL'));
        // });
        Mail::send('email.inquiry_apply', ['post'=>$post], function ($message) use ($post) {
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
            $message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
          //$message->cc('admissions@everestmanila.edu.ph');
            $message->bcc('sis.sbca.20151023@gmail.com');
            $message->bcc(env('SYS_EMAIL'));
        });
        return 1;

	}
	
	private function emailParentApplication($id,$email){
		ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
		$post = $this->inquiryParentModel->where('id',$id)->first();
		$post = [
			'fname' => $post->fname,
			'email' => $post->email,
			'link' => url('auth/override?key='.encode($id))
		];
		Mail::send('email.inquiry_apply', ['post'=>$post], function ($message) use ($post) {
			$message->from(env('SYS_EMAIL'), 'Everest Academy Manila Admissions Inquiry');
        	$message->to(getObjectValue($post,'email'))->subject('Everest Academy Manila Admissions Inquiry');
          //$message->cc('admissions@everestmanila.edu.ph');
            $message->bcc('sis.sbca.20151023@gmail.com');
            $message->bcc(env('SYS_EMAIL'));
		});
		return 1;
	}
    
 	private function initializer()
 	{
		$this->umodel = new UserModel;
		$this->uservice = new UserServiceProvider;
        $this->inquiryParentModel = new InquiryParent;
        $this->inquiryChildModel = new InquiryChild;
        $this->validate = new Validate;

        if (Request::get('type')) {
            $this->type = Request::get('type');
        }
 	}
}
