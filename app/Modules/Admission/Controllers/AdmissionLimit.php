<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;

use Request;
use Response;
use Permission;
use DB;

class AdmissionLimit extends Controller{

	private $media = [ 'Title'=> 'Admission Limit',
            'Description'=> 'Admission Limit Management',
            'js'        => ['Admission/admissionlimit'],
            'init'      => [],
            'plugin_js' => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
                            'SmartNotification/SmartNotification.min',],
            'plugin_css' => ['bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification',]
        ];

    private $url = [ 'page' => 'admission-limit/' ];

    private $views = 'Admission.Views.limit.';

    private $type = 'walk-in';

    function __construct()
    {
        $this->initializer();
    }

    public function index(){
        return view('layout',array('content'=>view($this->views.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
    }
	
	public function txn(){
      $this->initializer();
	  $post     = Request::all();
      $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
	  $content  = '';
      switch(Request::get('event')){
	    case 'save':
		    $adata = getObjectValue($post,'xdata');
			if($adata && trim($adata)!=''){
				$rs = @explode("<n>",$adata);
				if($rs && count($rs)>0){
					foreach($rs as $k=>$v){
					   if($v==''){
					    continue;
					   }
					   
					   list($count,$term,$campus,$college,$prog,$mjr,$limit)= explode("<c>",$v);
					   $check = DB::SELECT("SELECT * FROM es_admissionlimits WHERE TermID='".$term."' AND CampusID='".$campus."' AND ProgID='".$prog."' AND MajorID='".$mjr."'");
					   if($check && count($check)>0){
					    $update = DB::STATEMENT("UPDATE es_admissionlimits SET `Limit`='".$limit."' WHERE TermID='".$term."' AND CampusID='".$campus."' AND ProgID='".$prog."' AND MajorID='".$mjr."'"); 
					   }else{
					    $insert = DB::table('es_admissionlimits')->insert(array('TermID'=>$term,'CampusID'=>$campus,'ProgID'=>$prog,'MajorID'=>$mjr,'Limit'=>$limit));
					   } 
					}
					$response = ['success'=>true,'content'=>$content,'message'=>'Successful'];
				}
			}else{
				$response = ['success'=>false,'content'=>$content,'message'=>'Nothing to save'];
			}
		break;
	    case 'list':
		    $content= '';
		    $termid = getObjectValue($post,'termid');
		    $campus = getObjectValue($post,'campusid');
			$progs  = DB::SELECT("SELECT p.CampusID,p.CollegeID,CollegeCode,
										p.ProgID,p.ProgCode,p.ProgName,
										IFNULL(dm.IndexID,0) as MajorID,dm.MajorDiscCode,dm.MajorDiscDesc,
										IFNULL((SELECT `Limit` FROM es_admissionlimits 
										         WHERE TermID='".$termid."' AND CampusID=p.CampusID
												   AND ProgID=p.ProgID AND MajorID=IFNULL(dm.IndexID,0) LIMIT 1),0) as `Limit`
								   FROM es_programs as p 
							 INNER JOIN es_colleges as c ON p.CollegeID=c.CollegeID
							  LEFT JOIN es_programmajors as pm ON p.ProgID=pm.ProgID
							  LEFT JOIN es_disciplinemajors as dm On pm.MajorDiscID=dm.IndexID
							      WHERE p.CampusID='".$campus."'
							   ORDER BY p.CollegeID,p.ProgName");
			
			if($progs && count($progs)>0){
				$campusid =0;
				$collegeid=0;
				$progid   =0;
				$rcount   =0;
				$tmprow   ='';
				foreach($progs as $p){
				  if($collegeid!=$p->CollegeID){
					if($tmprow!=''){
						if($rcount>1){
						$tmprow = str_replace("<x>",'rowspan="'.$rcount.'"',$tmprow);
						}else{
						$tmprow = str_replace("<x>",'',$tmprow);
						}
						$content.= $tmprow;
						$rcount = 0;
					}
					$collegeid = $p->CollegeID;
					$tmprow = '<tr data-campus="'.$p->CampusID.'" data-college="'.$p->CollegeID.'" data-prog="'.$p->ProgID.'" data-major="'.intval($p->MajorID).'">
								 <td <x> class="text-center" width="180px;">'.$p->CollegeCode.'</td>
								 <td class="text-center" width="180px;">'.$p->ProgCode.'</td>
								 <td class="">'.$p->ProgName.'</td>
								 <td class="">'.$p->MajorDiscDesc.'</td>
								 <td class="" width="100px;"><input type="text" class="form-control numberonly text-right txtquota" value="'.$p->Limit.'"/></td>
							   </tr>';
					$rcount++;		   
				  }else{
					$tmprow .= '<tr data-campus="'.$p->CampusID.'" data-college="'.$p->CollegeID.'" data-prog="'.$p->ProgID.'" data-major="'.intval($p->MajorID).'">
								 <td class="text-center" width="180px;">'.$p->ProgCode.'</td>
								 <td class="">'.$p->ProgName.'</td>
								 <td class="">'.$p->MajorDiscDesc.'</td>
								 <td class="" width="100px;"><input type="text" class="form-control numberonly text-right txtquota" value="'.$p->Limit.'"/></td>
							   </tr>';
					$rcount++;		   
				  }
				}
				
				if($tmprow!=''){
					if($rcount>1){
					$tmprow = str_replace("<x>",'rowspan="'.$rcount.'"',$tmprow);
					}else{
					$tmprow = str_replace("<x>",'',$tmprow);
					}
					$content.= $tmprow;
					$rcount = 0;
				}
			    $response = ['success'=>true,'content'=>$content,'message'=>'Successful'];
			}else{
			    $response = ['success'=>false,'content'=>$content,'message'=>'Nothing to load'];
			}
		break;
	  }
	  return Response::json($response);
	}
	
 	private function initializer(){
	
 	}
	
	private function init(){
	  return array();
	}
}
