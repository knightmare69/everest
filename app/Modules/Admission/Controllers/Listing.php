<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Models\Admission;
use App\Modules\Admission\Services\Admission\Helper as helper;
use App\Modules\Admission\Services\AdmissionServiceProvider as Services;
use App\Modules\Admission\Services\Datatable;
use App\Modules\Admission\Models\InquiryParent;
use Request;
use Response;
use Permission;
use DB;
use Mail;
use Config;
class Listing extends Controller{
	private $media =
		[
			'Title'=> 'Applicant List',
			'Description'=> 'Admission Applicant List',
			'js'		=> ['Admission/listingDataTable','dataTable','Admission/listing','Admission/helper','Admission/email'],
			'init'		=> ['Listing.init()','FN.multipleSelect()',"$('#summernote_1').summernote({height: 300});"],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
				            'bootstrap-summernote/summernote.min',
				            'jquery-multi-select/js/jquery.multi-select',
				            'select2/select2.min'
						],
			'plugin_css' => ['bootstrap-select/bootstrap-select.min',
							'bootstrap-summernote/summernote',
							'jquery-multi-select/css/multi-select',
							'select2/select2'
						],
			'closeSidebar' => true
		];
	private $url = [ 'page' => 'admission/listing/' ];
	private $views = 'Admission.Views.listing.';
	function __construct()
	{
		$this->initializer();
	}
 	public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}
 	public function search(){
 		return $this->Datatable->filter();
 	}
 	public function getYearLevel(){
 		if (Request::get('IsHigherLevel') == '0') {
 			return DB::table('vw_YearLevel')->where('ProgClass','<','50')->get();
 		}
 		return DB::table('vw_YearLevel')->where('ProgClass','>=','50')->get();
 	}
 	public function showRemarks()
 	{
 		return
 		view($this->views.'modals.show_remarks')->render();
 	}

 	public function updateStatus()
	{
		$data = Request::get('data');
		$status = decode(Request::get('statusid'));
		jsonToArray($data);

		if (count($data[0]) > 0) {
			
			foreach($data[0] as $row) {
				$AppNo = ((encode(decode($row))==$row)?decode($row):$row);
				if ($AppNo) {
					$this->helper->updateStatus($AppNo,$status);
				}
				
				if ($status == env('AO_ADMISSION_ADMITTED_STATUSID')) {
					$s = $this->admission->saveAdmittedToStudent($AppNo);
				} 
			}
		}

		return successSave();
		$ref = $this->remarks->create(
			$this->repository->getPostRemarks(Request::all())
		);
	}
    
	public function removeApp(){
		$result= array('success'=>false,'error'=>true,'message'=>'');
		$get   = Request::all();
		$appno = getObjectValueWithReturn($get,'appno','');
		$exec  = DB::table('ESv2_Admission')->where('AppNo',trim($appno))->delete();
		if($exec){
		    $result['success']=true;
		}else{
			$result['error']='Failed to remove item';
		}
		SystemLog('Admission','Listing','remove','', ('AppNo:'.$appno), '');
		return ($result);
	}
	
	public function paymentStatus()
	{
		$data = Request::get('data');
		jsonToArray($data);
		$email = [];

		if (count($data[0]) > 0) {
			foreach($data[0] as $row) {
				$AppNo = ((encode(decode($row))==$row)?decode($row):$row);
				if ($AppNo) {
				    $this->admission->where(['AppNo'=>$AppNo])->update(['PaymentStatus'=>Request::get('ispaid') == 'true' ? 1 : 0]); 

				    $email = $this->guardian->select('Father_Email','Mother_Email','Guardian_Email')->where('FamilyID',getGuardianFamilyID($AppNo))->get();
					$email = isset($email[0]) ? $email[0] : [];       
				}
			}
		}
		return ['error'=>false,'message'=>'<b>Success!</b>. Saved!','femail'=>encode(getObjectValue($email,'Father_Email')),'memail'=>encode(getObjectValue($email,'Mother_Email')),'gemail'=>encode(getObjectValue($email,'Guardian_Email'))];
	}

	public function hasInterviewed()
	{
		$data = Request::get('data');
		jsonToArray($data);

		if (count($data[0]) > 0) {
			foreach($data[0] as $row) {
				$AppNo = ((encode(decode($row))==$row)?decode($row):$row);
				if ($AppNo) {
				    $this->admission->where(['AppNo'=>$AppNo])->update(['HasInterviewed'=>Request::get('hasinterviewed') == 'true' ? 1 : 0]);        
				}
			}
		}
		return successSave();	
	}

	public function sendManualEmail() 
	{
		ini_set('max_execution_time', 14400);
        Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
  //       Mail::send('email.template', array(), function ($message) {
  //       	$emails = explode(',', Request::get('recipient'));
		// 	$message->from(env('SYS_EMAIL'), 'Everest Academy Manila Admissions Inquiry');
		// 	foreach($emails as $e) {
		// 	    $message->to($e)->subject(Request::get('title'));
		// 	}
		// 	$message->bcc(env('SYS_EMAIL'));
		// });
        Mail::send('email.template', array(), function ($message) {
        	$emails = explode(',', Request::get('recipient'));
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
            foreach($emails as $e) {
			    $message->to($e)->subject(Request::get('title'));
			}
            $message->cc('admissions@everestmanila.edu.ph');
            $message->bcc('sis.sbca.20151023@gmail.com');
            $message->bcc(env('SYS_EMAIL'));
        });
        return 1;
	}	

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-listing');
 		$this->guardian = new GuardianBackround;
 		$this->services = new Services;
 		$this->Datatable = new Datatable;
 		$this->admission = new Admission;
 		$this->helper = new helper;
 		$this->inquiry_parent = new InquiryParent;
 	}
}