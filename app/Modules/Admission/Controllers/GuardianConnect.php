<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Services\GuardianConnectServices as Services;
use App\Modules\Setup\Models\City;
use Request;
use Response;
use Permission;
use DB;
use Image;
class GuardianConnect extends Controller{

	private $media =
		[
			'Title'=> 'Home',
			'Description'=> 'Welcome!',
			'js'		=> ['Admission/connect'],
			'init'		=> ['Connect.init()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min','jquery-validation/js/jquery.validate.min',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'bootstrap-datepicker/js/bootstrap-datepicker',
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
			]
		];

	private $url = [ 'page' => 'guardian-connect/' ];

	private $views = 'Admission.Views.guardian-connect.';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{
        if ($this->permission->has('read')) {
                        
            return view('layout',array(
                'content'=>view($this->views.'index')->with(['views'=>$this->views]),
                'url'=>$this->url,
                'media'=>$this->media,                
            ));
        }
        return view(config('app.403'));
 	}

 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'search':
					//if ($this->permission->has('search-student')) {
						$rs  = $this->services->search();
                        $data = $rs->first();
                        if($rs->count()>0){
                            $response=[
                                'error' => false,
                                'message' => '1 record found!',
                                'idno' => $rs->pluck('StudentNo'), 
                                'fullname' => $data->LastName .', '. $data->FirstName ,
                                'yearlevel' => $data->YearLevel,
                            ];
                        }else{
                            $response=[
                                'error' => true,
                                'message' => 'No record found!',
                            ];
                        }
					//}
				break;
				case 'connectChild':
					//if ($this->permission->has('search-student')) {
						//if (Request::get('isOld')) {
							$res = $this->services->connectOld();
						//} else {
						//	$res = $this->services->connectNew();
						//}
                        
						$response = $res ? successConnect() : errorConnect();	
					//}
				break;
				case 'showModalPhoto':
					if ($this->permission->has('search-student')) {
						$response = view($this->views.'modals.photo')->render();
					}
				break;
				case 'showModal':
					if ($this->permission->has('search-student')) {
						$response = view($this->views.'modals.search')->render();
					}
				break;
			}
		}
		return $response;
	}

	public function getPhoto()
	{
		ob_clean();
		set_time_limit(0);
        ini_set('memory_limit', '128M');
		ini_set("odbc.defaultlrl", "100K");
		
		if (Request::get('isOld')) {
			$data = DB::table('ES_Students')->select('StudentPicture as Photo')->where('StudentNo',Request::get('key'));
		} else {
			$data = DB::table('ES_Admission')->where('AppNo',Request::get('key'));
		}
		
		if ($data->count() > 0) {
			$data = $data->get()[0];
			if ($data->Photo) {
				header("Content-Type: image/jpg");
				readfile(
						Image::make(hex2bin($data->Photo))
						->fit(200, 200, function ($constraint) {
						    $constraint->upsize();
						})
						->encode('data-url')
				);
				die();
			}
		}
		
		header("Content-Type: image/png");
		return 
			Image::make(public_path('assets/system/media/images/no-image.png'))
			->response();
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('guardian');
 		$this->services = new Services;

 	}
}