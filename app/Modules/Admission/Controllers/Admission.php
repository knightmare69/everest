<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Services\AdmissionServiceProvider as Services;
use App\Modules\Setup\Models\City;
use App\Modules\Admission\Services\Admission\Repository;
use App\Modules\Registrar\Models\TermConfig as term_model;
use App\Modules\Admission\Models\Siblings;
use Request;
use Response;
use Permission;
use DB;
use Mail;
use Config;

class Admission extends Controller{

	private $media = [ 'Title'=> 'Student Admission',
			'Description'=> 'Student admission module',
			'js'		=> [
				'Admission/review','Admission/admission',
				'Admission/admissionHelper','admission/admissionData',
				'Admission/wizzard',
				'Admission/College','Admission/helper'
			],
			'init'		=> ['ADMISSION.init()','FN.datePicker()','FN.multipleSelect()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all'
			],
			'closeSidebar' => true
		];

	private $url = [ 'page' => 'admission/apply/' ];

	private $views = 'Admission.Views.admission.';

	function __construct()
	{   
		$this->initializer();
	}

    public function trial(){
		$this->initializer();
		print_r($this->services->addReligion('Sample'));
	}
	
 	public function index()
 	{
 		$this->initializer();
 		$c = new \App\Modules\Admission\Services\Completion\Completion;
 		if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'updateGuardian':
					if ($this->permission->has('update-guardian')) {
						$validation = $this->services->isValid(Request::all(),'guardian');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							if($this->services->updateGuardian(Request::all())) {
								$response = successSave();
							} else {
								$response = errorSave();
							}
						}
					}
					break;
				case 'setCurrentStep':
					putSessionData('currentStep',Request::get('step'));
					$response = Response::json(1);
				break;
				case 'getCurrentStep':
					$response = (!getSessionData('currentStep')) ? 1 : getSessionData('currentStep');
				break;
				case 'save':
					$result = $this->services->saveAdmission(Request::all());
					if (!$result['error']) {
						if(getSessionData('inquired')==1 || isParent()){
							ini_set('max_execution_time', 14400);
							$link = ['link' => url('admission/redirectReport?AppNo='.encode($result['AppNo']))];
							$post = DB::select("SELECT TOP 1 *,'".$result['AppNo']."' as AppNo FROM ESv2_Users WHERE UserIDX='".getUserID()."'")[0];
					        Config::set('mail.host', 'smtp.googlemail.com');
					        Config::set('mail.port', '465');
					        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
					        Config::set('mail.password', 'everest123!');
							
							//add CC to admissions for notification of submission
					        Mail::send('email.admission_submit', ['post'=>$post,'link'=>$link], function ($message) use ($post) {
					            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
					            $message->to(getObjectValue($post,'Email'))->subject('Application Form Received');
					          //$message->to('jhe69samson@gmail.com')->subject('Application Form Received');
					            $message->cc('admissions@everestmanila.edu.ph');
					            $message->bcc('sis.sbca.20151023@gmail.com');
					            $message->bcc(env('SYS_EMAIL'));
					        });
						}
						
						$response = [
							'error'=>false,
							'AppNo' => $result['AppNo'],
							'FamilyID' => encode($result['FamilyID']),
							'message'=>view($this->views.'forms.complete',['isSubmit'=>Request::get('isTemp')])->render()
						];
					} else {
						$response = ['error'=>true,'message'=>$result['error']];
					}
				break;
				case 'showGradeLevelProgram':
					$response = Response::json($this->services->showProgram(decode(Request::get('key')),(Request::get('class'))));
				break;
				case 'getRequiredDocs':
					$response = $this->services->showRequiredDocs(Request::all());
				break;
				case 'saveDocuments':
					$response = Response::json($this->services->saveRequiredDocs(Request::all()));
				break;
				case 'savePhoto':
					$response = Response::json($this->services->savePhoto(Request::all()));
				break;
				case 'ShowExamScheduleDates':
					$response = ['error'=>false,'data'=>$this->services->showExamSchedDates(Request::all())];
				break;
				case 'verifyCode':
					$response = Response::json($this->services->verifyORCode(Request::all()));
				break;
				case 'showModalFamilSearch':
					$response = ['error'=>false,'content'=>view($this->views.'tables.family_search')->render()];
				break;
				case 'searchFamily':
					$response = ['error'=>false,'content'=>view($this->views.'tables.family_search',['data'=>$this->services->familySearch(Request::all())])->render()];
				break;
				case 'getFamilyBG':
					$response = ['error'=>false,'data'=>$this->services->getFamilyBackground(decode(Request::get('key')))];
				break;
				case 'validateGuardianEmail':
					$response = Response::json($this->services->validateGuardianEmail(Request::all()));
				break;
				case 'getCity':
					$response = $this->city->where('CountryCode',Request::get('code'))->get();
				break;
				case 'getYearLevel':
					$response = $this->services->getYearLevel(Request::get('type'));
				break;
				case 'getCourses':
					$response = $this->services->getCourses();
				break;
				case 'validateUsername':
					$response = $this->services->isValidUserName();
				break;
				case 'byYrLvlScholastic';
					switch (Request::get('yrlvl')) {
						case 'kinder-grade1':
							$response = view($this->views.'forms.sch_kndr_gr1');
							break;
						case 'grade2-grade11':
							$response = view($this->views.'forms.sch_gr2_gr11');
							break;
						case 'returning';
							$response = view($this->views.'forms.sch_rtn');
							break;
					}
				break;	
			}
		}
		return $response;
	}

	public function edit()
	{
		$this->initializer();
		$this->views = 'Admission.Views.edit.';
        if ($this->permission->has('edit')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,
                        'media'=>$this->media));
        }
        return view(config('app.403'));
	}

	public function redirecToApply()
	{
		putSessionData('currentStep',1);
		return redirect('/admission/apply');
	}

 	private function initializer($menu = 'admission-apply')
 	{
 	    $this->permission = new Permission($menu);
 		$this->guardian = new GuardianBackround;
 		$this->services = new Services;
 		$this->city = new City;
 		$this->siblings = new Siblings;
 	}
}
