<?php
namespace App\Modules\Admission\Services;

use App\Modules\Admission\Services\Admission\Validation as validate;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Models\FatherBackground;
use App\Modules\Admission\Models\MotherBackground;
use App\Modules\Admission\Models\Admission as AdmissionModel;
use App\Modules\Admission\Models\Siblings as SiblingsModel;
use App\Modules\Admission\Models\References as ReferencesModel;
use App\Modules\Admission\Models\Attachment;
use App\Modules\Admission\Models\RequiredDocs;
use App\Modules\Admission\Models\RequiredDocsRemarks;
use App\Modules\Admission\Models\AdmissionStatusRemarks;
use App\Modules\Admission\Services\Admission\Repository;
use App\Modules\Admission\Services\Admission\Helper;
use App\Modules\Admission\Services\Admission\Photo as AdmissionPhoto;
use App\Modules\Admission\Services\RegisterServiceProvider as RegisterServices;
use App\Modules\Security\Models\Users\User;
use App\Modules\Admission\Models\Approval;
use App\Modules\Admission\Models\SchoolsAttended as SchoolsAttendedModel;
use App\Modules\Admission\Models\QuestionaireAnswer as QuestionaireAnswerModel;
use App\Modules\Admission\Models\AdmissionEvaluation;
use App\Modules\Admission\Models\AdmissionCommittee;
use Mail;
use DB;
use Request;
use Illuminate\Http\Request as BRequest;
use Storage;

Class AdmissionEditServiceProvider {

	public function __construct()
	{
		$this->initializer();
	}

	public function isValid($post,$action)
	{	
		$validate = $this->validate->$action($post);
		if ($validate->fails())
		{
			return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
		}
		return ['error'=>false,'message'=> ''];
	}

	public function saveRemarksStatus()
	{
		$ref = $this->remarks->create(
			$this->repository->getPostRemarks(Request::all())
		);

		if (getObjectValue($ref,'ID')) {
			$this->helper->updateStatus(decode(Request::get('AppNo')),decode(Request::get('status')));
		}
		
		if (getObjectValue($ref,'StatusID') == env('AO_ADMISSION_ADMITTED_STATUSID')) {
			$s = $this->admission->saveAdmittedToStudent(getObjectValue($ref,'AppNo'));
		} 
		else {
			$s = $this->admission->deleteStudentProfileByAppNo(getObjectValue($ref,'AppNo'));
		}
	}

	public function savePanelRemarksStatus()
	{
		$ref = Approval::create(
			$this->repository->getPostPanelRemarks(Request::all())
		);

		if (getObjectValue($ref,'id')) {
			if (Approval::isDone(decode(Request::get('AppNo')))) {
				$status = env('AO_ADMISSION_DISAPPROVED_STATUSID');
				if (Approval::isFinalApproved(decode(Request::get('AppNo')))) {
					$status = env('AO_ADMISSION_ADMITTED_STATUSID');
				}
				$this->helper->updateStatus(decode(Request::get('AppNo')),$status);
			}
		}
	}

	public function addNationality($data){
		$nid   = DB::table('Nationalities')->count()+1;
		$xdata = array('NationalityID'=>$nid,'Nationality'=>$data,'ShortName'=>$data,'MotherTongue'=>'','IsForeign'=>1,'SeqNo'=>0);
		$exec  = DB::table('Nationalities')->insert($xdata);
		return $nid;
	}

	public function addReligion($data){
		$rid   = DB::table('ES_Religions')->count()+1;
		$xdata = array('ReligionID'=>$rid,'Religion'=>$data,'ShortName'=>$data,'IsDefault'=>0);
		$exec  = DB::table('ES_Religions')->insert($xdata);
		return $rid;
	}

	private function NationalityChecker($add,$field){
		if($add!=''){
			$qry = DB::table('Nationalities')->select('NationalityID')->where(['Nationality'=>$add,'ShortName'=>$add])->get();
			if(isset($qry[0]->NationalityID)){
				$field = $qry[0]->NationalityID;
			} else {
				$field = encode($this->addNationality($add));
			}
		}
		unset($add);
	}

	private function ReligionChecker($add,$field){
		if($add!=''){
			$qry = DB::table('ES_Religions')->select('ReligionID')->where(['Religion'=>$add,'ShortName'=>$add])->get();
			if(isset($qry[0]->ReligionID)){
				$field = $qry[0]->ReligionID;
			} else {
				$field = encode($this->addReligion($add));
			}
		}
		unset($add);
	}

	public function saveAdmission($data) {

		if($data['addNationality']!=''){
			$data['Nationality'] = encode($this->addNationality($data['addNationality']));
		}
		unset($data['addNationality']);

		$this->NationalityChecker($data['addCitizenship'],$data['Citizenship']);
		
		if($data['addReligion']!=''){
			$data['Religion'] = encode($this->addReligion($data['addReligion']));
		}
		unset($data['addReligion']);

		$this->NationalityChecker($data['addFatherNationality'],$data['FatherNationality']);
		$this->NationalityChecker($data['addFatherCitizenship'],$data['FatherCitizenship']);
		$this->NationalityChecker($data['addMotherNationality'],$data['MotherNationality']);
		$this->NationalityChecker($data['addMotherCitizenship'],$data['MotherCitizenship']);

		$this->ReligionChecker($data['addFatherReligion'],$data['FatherReligion']);
		$this->ReligionChecker($data['addMotherReligion'],$data['MotherReligion']);
		
        $this->saveFamilyBackground($data);
        
		$admission = $this->admission->where('AppNo',$this->AppNo)->update(
			$this->repository->getSubmissionData($this->FamilyID,$data,$this->AppNo)
		);

		/*save course choices if config college enabled*/
		$this->helper->saveCourseChoices(
			$this->AppNo,Request::get('CourseChoicesData')
		);

		$this->insertSiblingsData(getObjectValue($data,'siblingsData'));

		$this->insertQuestionsData(getObjectValue($data,'scholasticData'));
		$this->insertQuestionsData(getObjectValue($data,'questionData'));

		$this->inserSchoolsAttended(getObjectValue($data,'schoolsAttended'));

		$this->insertReferences(getObjectValue($data,'referencesData'));

		/* set completion status */
		$c = new \App\Modules\Admission\Services\Completion\Completion;
 		$c->updateStatus($this->AppNo);

		/*register guardian if don't have account yet*/
		/*this will email to their respective email account*/
		//$this->setAccountsLogin($data);
		
		return  [
			'error'=>false,
			'FamilyID' => $this->FamilyID,
			'AppNo'=> $this->AppNo
		];
	}

	public function saveRequiredDocs($data)
	{
		set_time_limit(0);
        ini_set('memory_limit', '-1');

		$templates = getObjectValueWithReturn($data,'details',[]);
		$this->AppNo = (($this->AppNo == encode(decode($this->AppNo)))?decode($this->AppNo):$this->AppNo);
		if (count($templates) <= 0) {
			return true;
		}
		$TempData = [];$TempDetailsData = [];
		foreach($templates as $index => $row) {
			$row = json_decode('['.$row.']',true);	
			$row = isset($row[0]) ? $row[0] : [];
			$TempData[] = getObjectValue($row,'TempID');
			$TempDetailsData[] = getObjectValue($row,'TempDetailsID');
                $this->saveRequiredDocsDetails($this->AppNo,$row,$index);
		}
	
		$this->docs
			->whereNotIn('TemplateID',$TempData)
			->whereNotIn('TemplateDetailID',$TempDetailsData)
			->where('AppNo',$this->AppNo)
			->delete();

		set_time_limit(50);
        ini_set('memory_limit', '128M');
		return true;
	}

	public function saveAttachment($TempDetailsID,$index,$EntryID)
	{	
		set_time_limit(0);
        ini_set('memory_limit', '-1');

        if (!isset($_FILES['file']['tmp_name'][$index])) return false;

		$file = getFileInfo($_FILES['file'],true,$index);
		$filename = 'docs_'.$this->AppNo.'_'.$TempDetailsID.'.'.$file['FileExtension'];
		Storage::disk('local')
       	->put(
       		env('STORAGE_ADMISSION').
       		'Family/'.
       		$this->FamilyID.
       		'/'.
       		'students/'.
       		$this->AppNo.
       		'/'.
       		$filename,
       		file_get_contents($file['Attachment'])
       	);
			
		$this->helper->updateDocFilename($EntryID,$filename);	

		set_time_limit(50);
        ini_set('memory_limit', '128M');

        return true;
	}

	public function savePhoto()
	{	
		set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $this->photo->AppNo = $this->AppNo;
        $this->photo->FamilyID = $this->FamilyID;

        $this->photo->AdmissionPhoto();
       
		set_time_limit(50);
        ini_set('memory_limit', '128M');

        return true;
	}

	public function isValidUserName()
	{
		$min = 6;
		$error = true;
		$message = '';

		$username = Request::get('username');
		
		if ($username) {
			if (strlen($username) >= $min) {
				if($this->helper->isLoginUsernameNotExist($username)) {
					$error = false;
				} else {
					$message = 'Username has already been taken.';
				}
			} else {
				$message = 'Username minimum lenth is 6.';
			}
		} else {
			$message = 'Please provide username';
		}
		return ['error'=>$error,'message'=>$message];
	}

	public function saveDocRemarks()
	{
		$this->docsRemarks
			->create(
				assertCreated([
					'DocID' => decode(Request::get('key')),
					'Remarks'=> Request::get('remarks')
				])
			);		
	}

	public function showProgram($key,$ProgClass) 
	{
		$data = DB::select("select * from ESv2_fn_AcademicTrack('".trimmed($key)."') where ProgClass=".trimmed($ProgClass));
		$retData = [];
		foreach($data as $row) {
			$retData[] = [
				'PID' => encode(getObjectValue($row,'ProgID')),
				'PClass' => encode(getObjectValue($row,'ProgClass')),
				'PName' => getObjectValue($row,'ProgramName'),
				'MName' => getObjectValue($row,'MajorDiscDesc'),
				'MID' => encode(getObjectValue($row,'MajorID'))
			];
		}
		return $retData;
	}

	public function showExamSchedDates($post)
	{
		$data = [];
		foreach($this->helper->getExamSchedDates($post) as $row) {
			if ($row->Registered < $row->Limit) {
				$data[] = [
					'SchedID' => encode($row->TestingSchedID),
					'SchedDate' => $row->TestingSchedDesc
				];
			}
		}
		return $data;
	}

	public function getDocsPath()
	{
		$data = DB::table('ESv2_Admission_RequiredDocs as d')->leftJoin('ESv2_RequirementsList as r','r.RequirementID','=','d.DocID')						
				->select(['R.Desc','d.Filename','d.EntryID','d.HasAttachment','d.remarks'])
				->where('d.AppNo',decode(Request::get('AppNo')))->get();
		$datas = [];
		foreach($data as $row) {
			if ($row->HasAttachment) {
				$datas[] = [
					'FamilyID' => encode(getFamilyIDByAppNo(Request::get('AppNo'))),
					'key' => encode($row->EntryID),
					'desc' => $row->Desc,
					'remarks' => $row->remarks,
					'w' => '964',
					'h' => '1024'
				];
			}
		}
		return $datas;
	}

	public function getYearLevel($type) 
	{
		if (strtolower($type) == 'higher') {
			return $this->repository->getHigherYearLevel();
		} else {
			return $this->repository->getBasicYearLevel();
		}
	}

	public function getCourses() 
	{
		return $this->repository->getCourses();
	}

	public function getDocsRemarks()
	{
		return $this->docsRemarks->data()->where('DocID',decode(Request::get('key')))->get();
	}

	public function showRequiredDocs($post)
	{
		return $this->admission->getRequiredDocsEditMode($this->AppNo,decode(getObjectValue($post,'AppID')),getObjectValue($post,'IsForeign'),decode(getObjectValue($post,'ProgClass')),decode(getObjectValue($post,'YearLevelID')));
	}

	public function verifyORCode($post)
	{
		return $this->helper->verifyORCode($post);
	}

	public function getStudentPhoto($AppNo)
	{
		return $this->admission->select('PhotoFile')->where('AppNo',$AppNo)->pluck('PhotoFile');
	}

	public function getGuardianPhoto($FamilyID)
	{
		return $this->guardian->select('Guardian_Photo')->where('FamilyID',$FamilyID)->pluck('Guardian_Photo');
	}

	public function siblingRemove($key)
	{
		return $this->siblings->where('SiblingIDX',$key)->delete();
	}

	public function isAdmitted()
	{
		return !isParent() ? false : $this->helper->isAdmitted($this->AppNo);
	}

	public function saveEvaluation($post){

		$id = decode(getObjectValue($post,'id'));
    	if ($id == '') {
    		$data = [
	    		'AppNo' => decode(getObjectValue($post, 'AppNo')),
	    		'InterviewType' => getObjectValue($post, 'InterviewType'),
	    		'DateInterviewed' => getObjectValue($post, 'DateInterviewed'),	
	    		'Remarks' => getObjectValue($post, 'Remarks'),
	    		'Recommendation' => getObjectValue($post, 'Recommendation'),
	    		'CreatedBy'	=> getUserID(),
	    		'CreatedDate' => systemDate()
	    	];
	    	$status = $this->evaluation->create($data);
	    	$id = $this->evaluation->where('AppNo', $data['AppNo'])->orderBy('EvaluationID','desc')->limit(1)->get()->toArray()[0]['EvaluationID'];
    	} else {
    		$data = [
	    		'InterviewType' => getObjectValue($post, 'InterviewType'),
	    		'DateInterviewed' => getObjectValue($post, 'DateInterviewed'),	
	    		'Remarks' => getObjectValue($post, 'Remarks'),
	    		'Recommendation' => getObjectValue($post, 'Recommendation')
	    	];
	    	$status = $this->evaluation->where('EvaluationID',$id)->update($data);
    	}

    	return $status ? ['error'=>false,'message'=>'<b>Success!</b>. Saved!','id'=>encode($id)] : errorSave();
    }

    public function saveCommittee($post){
    	$id = decode(getObjectValue($post,'id'));
    	if ($id == '') {
    		$data = [
    			'AppNo' => decode(getObjectValue($post, 'AppNo')),
    			'CommitteeRemarks' => getObjectValue($post, 'AdmissionCommitteeRemarks'),
    			'Remarks' => getObjectValue($post, 'AdmissionCommitteeRemarks'),
    			'IsAdmitted' => getObjectValue($post, 'admitted'),	
    			'IsConditional'	=> getObjectValue($post, 'condition'),
    			'IsWaitPool' => getObjectValue($post, 'waitpool'),
    			'IsDenied' => getObjectValue($post, 'denied'),
    			'CreatedBy' => getUserID(),
    			'CreatedDate' => systemDate()
    		];
			$status = $this->committee->create($data);
			$id = $this->committee->where('AppNo', $data['AppNo'])->orderBy('CommitteeID','desc')->limit(1)->get()->toArray()[0]['CommitteeID'];
    	} else {
    		$data = [
    			'CommitteeRemarks' => getObjectValue($post, 'AdmissionCommitteeRemarks'),
    			'Remarks' => getObjectValue($post, 'AdmissionCommitteeRemarks'),
    			'IsAdmitted' => getObjectValue($post, 'admitted'),	
    			'IsConditional'	=> getObjectValue($post, 'condition'),
    			'IsWaitPool' => getObjectValue($post, 'waitpool'),
    			'IsDenied' => getObjectValue($post, 'denied')
    		];
			$status = $this->committee->where('CommitteeID',$id)->update($data);
    	}
    	
    	return $status ? ['error'=>false,'message'=>'<b>Success!</b>. Saved!','id'=>encode($id)] : errorSave();
    }

	protected function getPhoto($AppNo) 
	{
		return $this->photo->select(['FileType','Attachment','FileName','FileSize','FileExtension'])->where('AppNo',$AppNo)->get();
	}

	protected function getCurrentAppNo($date)
	{
		return $this->admission->select('AppNo')->where(['CreatedDate'=>$date,'CreatedBy'=>getUserName()])->pluck('AppNo');
	}

	protected function setAppNo()
	{
		// return decode(Request::get('AppNo'));
		return((Request::get('AppNo') == encode(decode(Request::get('AppNo'))))?decode(Request::get('AppNo')):Request::get('AppNo'));
	}

	protected function setFamilyID($AppNo)
	{
           $AppNo = (($AppNo == encode(decode($AppNo)))?decode($AppNo):$AppNo);
		return $this->admission->select('FamilyID')->where('AppNo',$AppNo)->pluck('FamilyID');
	}

	protected function generateFamilyID()
	{
		return $this->FamilyID = $this->guardian->count()+1;
	}
	
	protected function updatePhotoFileName($fileName)
	{
		return $this->admission->where('AppNo',$this->AppNo)->update(['PhotoFile'=>$fileName]);
	}

	protected function updateGuardianPhotoFileName($fileName)
	{
		return $this->guardian->where('FamilyID',$this->FamilyID)->update(['Guardian_Photo'=>$fileName]);
	}

	protected function checkStatus($data,$key = '')
	{
		if (getObjectValue($data,$key) != '') {
			return true;
		}
		return false;
	}

	protected function saveRequiredDocsDetails($AppNo,$row,$index)
	{
		$AppNo = (($AppNo == encode(decode($AppNo)))?decode($AppNo):$AppNo);
		$where = [
			'AppNo'=> $AppNo,
			'TemplateID' => getObjectValue($row,'TempID'),
			'TemplateDetailID' => getObjectValue($row,'TempDetailsID'),
			'EntryID' => getObjectValue($row,'EntryID')
		];

		if ($this->docs->where($where)
			->count() > 0) {
			$temp = 
			$this->docs
			->where($where)
			->update([
				'AppNo' => $AppNo,
				'DocID' => getObjectValue($row,'Doc'),
				'TemplateID' => getObjectValue($row,'TempID'),
				'TemplateDetailID' => getObjectValue($row,'TempDetailsID'),
				'IsReviewed' => getObjectValue($row,'IsReviewed'),
				'IsExempted' => getObjectValue($row,'IsExempted'),
			]);
			
			if (getObjectValue($row,'EntryID')) {
				if($this->docs->where([
						'EntryID' => getObjectValue($row,'EntryID'),
						'HasAttachment' => 1
					])->count() <= 0) {
					$this->docs->where('EntryID',getObjectValue($row,'EntryID'))->update([
							'HasAttachment' => $this->saveAttachment(
								getObjectValue($row,'TempDetailsID'),
								$index,
								getObjectValue($row,'EntryID')
							)
						]
					);
				} else {
					if ($this->saveAttachment(getObjectValue($row,'TempDetailsID'),$index,getObjectValue($row,'EntryID'))) {
						$this->docs->where('EntryID',getObjectValue($row,'EntryID'))->update([
								'HasAttachment' => true
							]
						);
					}
				}				
			}

		} else {
			$temp = $this->docs
			->create([
				'AppNo' => $AppNo,
				'DocID' => getObjectValue($row,'Doc'),
				'TemplateID' => getObjectValue($row,'TempID'),
				'TemplateDetailID' => getObjectValue($row,'TempDetailsID'),
				'IsReviewed' => getObjectValue($row,'IsReviewed'),
				'IsExempted' => getObjectValue($row,'IsExempted'),
				'UploadedBy' => getUserName(),
				'UploadedDate' => systemDate(),
			]);

			if (getObjectValue($temp,'EntryID')) {
				$this->docs->where('EntryID',getObjectValue($temp,'EntryID'))->update([
						'HasAttachment' => $this->saveAttachment(
							getObjectValue($row,'TempDetailsID'),
							$index,
							getObjectValue($temp,'EntryID')
						)
					]
				);
			}
		}
		
		$this->admission
		->where('AppNo',$AppNo)
		->update([
			'DocTemplateID'=> getObjectValue($row,'TempID')
		]);
	}

	protected function setAccountsLogin($data)
	{	
		$IsEmailSend = getObjectValue($data,'IsEmailSend');
		$IsManageAccount = getObjectValue($data,'IsManageAccount');
		
		$data = [
			'Username' => getObjectValue($data,'username'),
			'upassword' => getObjectValue($data,'password'),
			'fullname' => getObjectValue($data,'name'),
			'Email' => getObjectValue($data,'email'),
			'position' => 1,
			'department' => 1
		];
		
		$accountData = $this->RegisterServices->account(
			$this->FamilyID,$data
		);

		if ($IsManageAccount) {
			if ($this->account->where('FamilyID',$this->FamilyID)->count() <= 0) {	
				
				$this->account->create($accountData);

				$data['code'] = $accountData['ConfirmNo'];
			} else {
				$this->account->where('FamilyID',$this->FamilyID)->update($accountData);
			}
		}
		
		if ($IsEmailSend) {
			$this->RegisterServices->email($data);
		}
	}

	protected function saveFamilyBackground($data)
	{
		$where = ['FamilyID' => $this->FamilyID];
		if( $this->guardian->where($where)->count() > 0) {

			$this->guardian->where($where)->update(
				$this->repository->getGuardianData($this->FamilyID,$data)
			);
			$this->father->where($where)->update(
				$this->repository->getFatherData($this->FamilyID,$data)
			);
			$this->mother->where($where)->update(
				$this->repository->getMotherData($this->FamilyID,$data)
			);
		} else {
			$this->generateFamilyID();

			$guardianData = $this->repository->getGuardianData($this->FamilyID,$data); 
			$guardianData['FamilyID'] = $this->FamilyID;
			$where = ['FamilyID' => $this->FamilyID];

			$guardianData = $this->guardian->create(
				$guardianData
			);

			$fatherData = $this->repository->getFatherData($this->FamilyID,$data);
			$fatherData['FamilyID'] = $this->FamilyID;
			$this->father->where($where)->update(
				$fatherData
			);
			
			$motherData = $this->repository->getMotherData($this->FamilyID,$data);
			$motherData['FamilyID'] = $this->FamilyID;
			$this->mother->where($where)->update(
				$motherData
			);
		}
	}

	protected function insertSiblingsData($data)
	{
		$siblings = json_decode('['.$data.']',true);
		$retData = [];
		foreach($siblings as $row) {
			$retData = [
				'FamilyID' => $this->FamilyID,
				'FullName' => getObjectValue($row,'name'),
				'DateofBirth' => getObjectValue($row,'dob'),
				'Gender' => getObjectValue($row,'gender'),
				'SchoolAttended' => getObjectValue($row,'school')
			];
			if (getObjectValue($row,'key')) {
				$retData['ModifiedBy'] = getUserName();
				$retData['ModifiedDate'] = systemDate();
				$this->siblings->where('SiblingIDX',getObjectValue($row,'key'))->update($retData);
			} else {
				$retData['SiblingIDX'] = $this->siblings->count()+1;
				$retData['CreatedBy'] = getUserName();
				$retData['CreatedDate'] =  systemDate();
				$this->siblings->create($retData);
			}
		}
		return true;
	}

	protected function insertReferences($data){
		$references = json_decode('['.$data.']',true);
		$retData = [];
		foreach($references as $row) {
			$retData = [
				'AppNo' => $this->AppNo,
				'Name' => getObjectValue($row,'name'),
				'Relationship' => getObjectValue($row,'rel'),
				'School' => getObjectValue($row,'school'),
				'PhoneNumber' => getObjectValue($row,'phone')
			];
			$this->references->create($retData);
			if ($this->references->where('ReferenceID',getObjectValue($row,'key'))->count() <= 0) {
				$this->references->create(assertCreated($retData));
			} else {
				$this->references->where('ReferenceID',getObjectValue($row,'key'))->update(assertModified($retData));
			}
		}
		return true;
	}

	protected function inserSchoolsAttended($data)
	{
		$schools = json_decode('['.$data.']',true);
		$retData = [];
		foreach($schools as $row) {
			$retData = [
				'AppNo' => $this->AppNo,
				'school' => getObjectValue($row,'school'),
				'address' => getObjectValue($row,'address'),
				'fromDate' => getObjectValue($row,'fromMonth').'/'.getObjectValue($row,'fromYear'),
				'toDate' => getObjectValue($row,'toMonth').'/'.getObjectValue($row,'toYear'),
				'yearLevel' => getObjectValue($row,'yearLevel'),
			];
			if ($this->schoolsAttended->where('id',getObjectValue($row,'id'))->count() <= 0) {
				$this->schoolsAttended->create(assertCreated($retData));
			} else {
				$this->schoolsAttended->where('id',getObjectValue($row,'id'))>update(assertModified($retData));
			}
		}
		return true;
	}

	protected function insertQuestionsData($data)
	{
		$answers = json_decode('['.$data.']',true);
		$retData = [];
		foreach($answers as $row) {
			$retData = [
				'app_no' => $this->AppNo,
				'question_id' => (int)getObjectValue($row,'id'),
				'answer' => getObjectValue($row,'answer')			
			];
			if ($this->answers->where(['question_id'=>getObjectValue($row,'id'),'app_no'=>$this->AppNo])->count() <= 0) {
				$retData['created_by'] = getUserID();
				$retData['created_date'] =  systemDate();
				$this->answers->create($retData);
			} else {
				$retData['modified_by'] = getUserID();
				$retData['modified_date'] = systemDate();
				$this->answers->where(['question_id'=>getObjectValue($row,'id'),'app_no'=>$this->AppNo])->update($retData);
			}
		}
		return true;
	}
	protected function initializer()
	{

		$this->validate = new validate;
		$this->guardian = new GuardianBackround;
		$this->father = new FatherBackground;
		$this->mother = new MotherBackground;
		$this->admission = new AdmissionModel;
		$this->siblings = new SiblingsModel;
		$this->references = new ReferencesModel;
		$this->docs = new RequiredDocs;
		$this->docsRemarks = new RequiredDocsRemarks;
		$this->remarks = new AdmissionStatusRemarks;
		$this->attachment = new Attachment;
		$this->repository = new Repository;
		$this->helper = new Helper;
		$this->photo = new AdmissionPhoto;
		$this->RegisterServices = new RegisterServices;
		$this->account =  new User;
		$this->schoolsAttended = new SchoolsAttendedModel;
		$this->answers = new QuestionaireAnswerModel;

		$this->evaluation = new AdmissionEvaluation;
		$this->committee = new AdmissionCommittee;

		$this->AppNo = $this->setAppNo();
		$this->AppNo = (($this->AppNo == encode(decode($this->AppNo)))?decode($this->AppNo):$this->AppNo);
		$this->FamilyID = $this->setFamilyID($this->AppNo);
	}
}	
?>


