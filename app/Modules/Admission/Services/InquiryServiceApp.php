<?php

namespace App\Modules\Admission\Services;

use App\Modules\Admission\Services\Admission\Validation as validate;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Models\FatherBackground;
use App\Modules\Admission\Models\MotherBackground;
use App\Modules\Admission\Models\Admission as AdmissionModel;
use App\Modules\Admission\Models\Siblings as SiblingsModel;
use App\Modules\Admission\Models\AdmissionReservation_model as mReservation;
use App\Modules\Admission\Models\Attachment;
use App\Modules\Admission\Models\RequiredDocs;
use App\Modules\Security\Models\Users\User;
use App\Modules\Admission\Services\Admission\Repository;
use App\Modules\Admission\Services\Admission\Helper;
use App\Modules\Admission\Services\RegisterServiceProvider as RegisterServices;
use App\Modules\Admission\Services\Admission\Photo as AdmissionPhoto;

use Mail;
use DB;
use Request;
use Storage;

Class InquiryServiceApp {

	public function __construct()
	{
		$this->initializer();
	}

	public function isValid($post,$action)
	{	
		$validate = $this->validate->$action($post);
		if ($validate->fails())
		{
			return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
		}
		return ['error'=>false,'message'=> ''];
	}

	public function updateGuardian($data) {
		if ($this->guardian->where('FamilyID',$this->FamilyID)->count() > 0) {
			return 
				$this->guardian->where('FamilyID',$this->FamilyID)
				->update($this->repository->guardianData($this->FamilyID,$data));
		} else {
			return 
				$this->guardian
				->create($this->repository->guardianData(
						$this->generateFamilyID(),$data
					)
				);
		}
	}

	public function validateGuardianEmail($post) 
	{	
		if ($this->FamilyID) {
			$email = $this->guardian->select('Guardian_Email')->where('FamilyID',$this->FamilyID)->pluck('Guardian_Email');
			if ($email == getObjectValue($post,'email')) { 				
				return true;
			}
		}
		if($this->guardian->select('Guardian_email')->where('Guardian_email',getObjectValue($post,'email'))->count() > 0) {
			return false;
		}
		return true;
	}

	public function saveAdmission($data) {

		$this->saveFamilyBackground($data);

		$this->insertSiblingsData(getObjectValue($data,'siblingsData'));
		
		$admission = $this->admission->create(
			$this->repository->getSubmissionData($this->FamilyID,$data)
		);

		$this->AppNo = $this->getCurrentAppNo($admission->CreatedDate);

		/*save course choices if config college enabled*/
		$this->helper->saveCourseChoices(
			$this->AppNo,Request::get('CourseChoicesData')
		);

		/*set account family id*/ 
		$this->setAcountFamilyID($this->FamilyID, getUserID());

		/*register guardian if don't have account yet*/
		/*this will email to their respective email account*/
		$this->setAccountsLogin($data);
		
		return  [
			'error' => false,
			'FamilyID' => $this->FamilyID,
			'AppNo' => $this->AppNo
		];
	}

	public function saveQuickAdmission($data)
	{		
		$admission = $this->admission->create(
			$this->repository->getQuickData($this->FamilyID,$data)
		);

		$this->AppNo = $this->getCurrentAppNo($admission->CreatedDate);

		/*save course choices if config college enabled*/
		$this->helper->saveCourseChoices(
			$this->AppNo,Request::get('CourseChoicesData')
		);
		
		return  [
			'error' => false,
			'FamilyID' => $this->FamilyID,
			'AppNo' => $this->AppNo
		];
	}

	public function saveRequiredDocs($data)
	{
		
		set_time_limit(0);
        ini_set('memory_limit', '-1');

		$templates = getObjectValueWithReturn($data,'details',[]);

		if (count($templates) <= 0) {
			return true;
		}

		foreach($templates as $index => $row) {
			$row = json_decode('['.$row.']',true);	
			$row = isset($row[0]) ? $row[0] : [];
			$this->saveRequiredDocsDetails($this->AppNo,$row,$index);
		}
		
		set_time_limit(50);
        ini_set('memory_limit', '128M');
		return true;
	}

	public function saveAttachment($TempDetailsID,$index,$EntryID)
	{	
		set_time_limit(0);
        ini_set('memory_limit', '-1');

        if (!isset($_FILES['file']['tmp_name'][$index])) {
        	return false;
        } 

		$file = getFileInfo($_FILES['file'],true,$index);
		$filename = 'docs_'.$this->AppNo.'_'.$TempDetailsID.'.'.$file['FileExtension'];
		Storage::disk('local')
       	->put(
       		env('STORAGE_ADMISSION').
       		'Family/'.
       		$this->FamilyID.
       		'/students/'.
       		$this->AppNo.
       		'/'.
       		$filename,
       		file_get_contents($file['Attachment'])
       	);
		$this->helper->updateDocFilename($EntryID,$filename);
		set_time_limit(50);
        ini_set('memory_limit', '128M');

        return true;
	}

	public function savePhoto()
	{	
		set_time_limit(0);
        ini_set('memory_limit', '-1');
       	
        $this->photo->AppNo = $this->AppNo;
        $this->photo->FamilyID = $this->FamilyID;

        $this->photo->AdmissionPhoto();

		set_time_limit(50);
        ini_set('memory_limit', '128M');

        return true;
	}

	public function showProgram($key,$ProgClass) 
	{	
		// echo "call sp_ESv2_fn_AcademicTrack(".trimmed($key).",".trimmed($ProgClass).")";die();
		// $data = DB::select("select * from ESv2_fn_AcademicTrack('".trimmed($key)."') where ProgClass=".trimmed($ProgClass));
		// $oldYearLevelID = DB::select("select YLID_OldValue from es_yearlevel where YearLevelID={trimmed($key)}");
		// $oldYearLevelID = isset($oldYearLevelID[0]) ? $oldYearLevelID[0]->YLID_OldValue : $key;
		$data = DB::select("call sp_ESv2_fn_AcademicTrack(".trimmed($key).",".trimmed($ProgClass).")");
		$retData = [];
		foreach($data as $row) {
			$retData[] = [
				'PID' => encode(getObjectValue($row,'ProgID')),
				'PClass' => encode(getObjectValue($row,'ProgClass')),
				'PName' => getObjectValue($row,'ProgramName'),
				'MName' => getObjectValue($row,'MajorDiscDesc'),
				'MID' => encode(getObjectValue($row,'MajorID'))
			];
		}
		return $retData;
	}

	public function showExamSchedDates($post)
	{
		$data = [];
		foreach($this->helper->getExamSchedDates($post) as $row) {
			if ($row->Registered < $row->Limit || $row->Limit == '0') {
				$data[] = [
					'SchedID' => encode($row->TestingSchedID),
					'SchedDate' => $row->TestingSchedDesc
				];
			}
		}
		return $data;
	}

	public function familySearch($post)
	{
		$dataGuardian = DB::table('ESv2_Admission_FamilyBackground')
			->select(['FamilyID','Guardian_Name as name',DB::raw("'' as dob"),'Guardian_email as email'])
			->Where('Guardian_Name','like','%'.trimmed(getObjectValue($post,'filter')).'%')
			->OrWhere('Guardian_email',trimmed(getObjectValue($post,'filter')));
			// ->limit(5);

		$dataFather = DB::table('ESv2_Admission_FamilyBackground')
			->select(['FamilyID','Father_Name as name','Father_BirthDate as dob',DB::raw("'' as email")])
			->orWhere('Father_Name','like','%'.trimmed(getObjectValue($post,'filter')).'%')
			->orWhereRaw("Father_BirthDate =
					(case 
						when IFNULL(DAYNAME('".trimmed(getObjectValue($post,'filter'))."'),0) = 1 then '".trimmed(getObjectValue($post,'filter'))."'
					 	when IFNULL(DAYNAME('".trimmed(getObjectValue($post,'filter'))."'),0) = 0 then ''
					END)");
			// ->limit(5)
			// ->union($dataGuardian);

		$dataMother = DB::table('ESv2_Admission_FamilyBackground')
			->select(['FamilyID','Mother_Name as name','Mother_BirthDate as dob',DB::raw("'' as email")])
			->orWhere('Mother_Name','like','%'.trimmed(getObjectValue($post,'filter')).'%')
			->orWhereRaw("Mother_BirthDate = 
					(case 
						when IFNULL(DAYNAME('".trimmed(getObjectValue($post,'filter'))."'),0) = 1 then '".trimmed(getObjectValue($post,'filter'))."'
					 	when IFNULL(DAYNAME('".trimmed(getObjectValue($post,'filter'))."'),0) = 0 then ''
					END)")
			->union($dataFather)
			->union($dataGuardian)
			->limit(5)
			->get();

			// echo $dataMother->toSql();
		
		
		return $dataMother;
	}

	public function isValidUserName()
	{
		$min = 6;
		$error = true;
		$message = '';

		$username = Request::get('username');
		
		if ($username) {
			if (strlen($username) >= $min) {
				if($this->helper->isLoginUsernameNotExist($username)) {
					$error = false;
				} else {
					$message = 'Username has already been taken.';
				}
			} else {
				$message = 'Username minimum lenth is 6.';
			}
		} else {
			$message = 'Please provide username';
		}
		return ['error'=>$error,'message'=>$message];
	}

	public function showRequiredDocs($post)
	{
		return $this->admission->getRequiredDocs(decode(getObjectValue($post,'AppID')),getObjectValue($post,'IsForeign'),getObjectValue($post,'ProgClass'),decode(getObjectValue($post,'YearLevelID')));
	}

	public function verifyORCode($post)
	{
		return $this->helper->verifyORCode($post);
	}

	public function getStudentPhoto($AppNo)
	{
		return $this->admission->select('PhotoFile')->where('AppNo',$AppNo)->pluck('PhotoFile');
	}

	public function siblingRemove($key)
	{
		return $this->siblings->where('SiblingIDX',$key)->delete();
	}

	public function getFamilyBackground($FamilyID)
	{
		
		$data = DB::table('ESv2_Admission_FamilyBackground')
		->where('FamilyID',$FamilyID)
		->get();
		return isset($data[0]) ? $this->repository->getFamilyCustoms($FamilyID,$data) : [];
	}

	public function getYearLevel($type) 
	{
		if (strtolower($type) == 'higher') {
			return $this->repository->getHigherYearLevel();
		} else {
			return $this->repository->getBasicYearLevel();
		}
	}

	public function getCourses() 
	{
		return $this->repository->getCourses();
	}

	protected function getCurrentAppNo($date)
	{
		return $this->admission->select('AppNo')->where(['CreatedDate'=>$date,'CreatedBy'=>getUserName()])->pluck('AppNo');
	}

	protected function updatePhotoFileName($fileName)
	{
		return $this->admission->where('AppNo',$this->AppNo)->update(['PhotoFile'=>$fileName]);
	}

	protected function updateGuardianPhotoFileName($fileName)
	{
		return $this->guardian->where('FamilyID',$this->FamilyID)->update(['Guardian_Photo'=>$fileName]);
	}

	protected function checkStatus($data,$key = '')
	{
		if (getObjectValue($data,$key) != '') {
			return true;
		}
		return false;
	}

	protected function setFamilyID()
	{
		if (getUserGroup() == 'parent') {
			return getGuardianFamilyID();
		} else {
			return decode(Request::get('FamilyID'));
		}
	}

	protected function setFamilyIDWithBG($data)
	{
		if (getUserGroup() == 'parent') {
			$this->FamilyID = getGuardianFamilyID();
		} else {
			$this->FamilyID = $this->guardian->select('FamilyID')->where($data)->pluck('FamilyID');
		}
	}

	protected function generateFamilyID($name)
	{
		return $this->FamilyID = str_shuffle(trim(str_replace(' ','',strtolower(getLetterEach($name,2))))).''.($this->guardian->count()+1);
	}

	protected function setAppNo()
	{
		return Request::get('AppNo');
	}
		
	protected function saveRequiredDocsDetails($AppNo,$row,$index)
	{
		$where = [
			'AppNo'=> $AppNo,
			'TemplateID' => getObjectValue($row,'TempID'),
			'TemplateDetailID' => getObjectValue($row,'TempDetailsID'),
			'EntryID' => getObjectValue($row,'EntryID')
		];

		$temp = $this->docs
		->create([
			'AppNo' => $AppNo,
			'DocID' => getObjectValue($row,'Doc'),
			'TemplateID' => getObjectValue($row,'TempID'),
			'TemplateDetailID' => getObjectValue($row,'TempDetailsID'),
			'IsReviewed' => getObjectValue($row,'IsReviewed'),
			'IsExempted' => getObjectValue($row,'IsExempted'),
			// 'HasAttachment' => ,
			'UploadedBy' => getUserName(),
			'UploadedDate' => systemDate(),
		]);

		if (getObjectValue($temp,'EntryID')) {
			$this->docs->where('EntryID',getObjectValue($temp,'EntryID'))->update([
					'HasAttachment'=>$this->saveAttachment(getObjectValue($row,'TempDetailsID'),$index,getObjectValue($temp,'EntryID'))
				]
			);
		}
		
		$this->admission
		->where('AppNo',$AppNo)
		->update([
			'DocTemplateID'=> getObjectValue($row,'TempID')
		]);
	}

	protected function setAcountFamilyID($FamilyID, $user) {
		$this->account->where('UserIDX',$user)
				->update(['FamilyID' => $FamilyID]);
	}

	protected function setAccountsLogin($data)
	{	
		$IsEmailSend = getObjectValue($data,'IsEmailSend');
		$IsManageAccount = getObjectValue($data,'IsManageAccount');
		$data = [
			'Username' => getObjectValue($data,'username'),
			'upassword' => getObjectValue($data,'password'),
			'fullname' => getObjectValue($data,'name'),
			'Email' => getObjectValue($data,'email')
		];

		$accountData = $this->RegisterServices->account(
			$this->FamilyID,$data
		);

		if ($IsManageAccount) {
			if ($this->account->where('FamilyID',$this->FamilyID)->count() <= 0) {	
				
				$this->account->create($accountData);

				$data['code'] = $accountData['ConfirmNo'];
			} else {
				$this->account->where('FamilyID',$this->FamilyID)->update($accountData);
			}
		}
		
		if ($IsEmailSend) {			
			$this->RegisterServices->email($data);
		}
	}

	protected function saveFamilyBackground($data)
	{
		$where = ['FamilyID' => $this->FamilyID];

		if( $this->guardian->where($where)->count() > 0) {
			$this->guardian->where($where)->update(
				$this->repository->getGuardianData($this->FamilyID,$data)
			);
			$this->father->where($where)->update(
				$this->repository->getFatherData($this->FamilyID,$data)
			);
			$this->mother->where($where)->update(
				$this->repository->getMotherData($this->FamilyID,$data)
			);

		} else {

			/*Generate Family ID*/
			$this->generateFamilyID(getObjectValue($data,'name'));
			
			$guardianData = $this->repository->getGuardianData($this->FamilyID,$data); 
			$guardianData['FamilyID'] = $this->FamilyID;
			$where = ['FamilyID' => $this->FamilyID];
			
			$guardianData = $this->guardian->create(
				$guardianData
			);

			$fatherData = $this->repository->getFatherData($this->FamilyID,$data);
			$fatherData['FamilyID'] = $this->FamilyID;
			$this->father->where($where)->update(
				$fatherData
			);
			
			$motherData = $this->repository->getMotherData($this->FamilyID,$data);
			$motherData['FamilyID'] = $this->FamilyID;
			$this->mother->where($where)->update(
				$motherData
			);
		
		}
	}

	protected function insertSiblingsData($data)
	{
		$siblings = json_decode('['.$data.']',true);
		$retData = [];
		foreach($siblings as $row) {
			$retData = [
				'SiblingIDX' => ($this->siblings->count())+1,
				'FamilyID' => $this->FamilyID,
				'FullName' => getObjectValue($row,'name'),
				'DateofBirth' => getObjectValue($row,'dob'),
				'Gender' => getObjectValue($row,'gender'),
				'SchoolAttended' => getObjectValue($row,'school'),
				'CreatedBy' => getUserName(),
				'CreatedDate' => systemDate()
			];
			$this->siblings->create($retData);
		}
		return true;
	}

	public function  initializer()
	{
		$this->validate = new validate;
		$this->guardian = new GuardianBackround;
		$this->father = new FatherBackground;
		$this->mother = new MotherBackground;
		$this->admission = new AdmissionModel;
		$this->siblings = new SiblingsModel;
		$this->docs = new RequiredDocs;
		$this->attachment = new Attachment;
		$this->photo = new AdmissionPhoto;
		$this->account =  new User;
		$this->RegisterServices = new RegisterServices;
		$this->repository = new Repository;
		$this->helper = new Helper;

		// $this->FamilyID = $this->setFamilyID();
		// $this->AppNo = $this->setAppNo();
	}
    
    
    public function saveReservation(){
        $model = new mReservation();
        $p = Request::all();
        
        $data = [
            'DateEntry' => systemDate(),
            'IDNo' => decode(getObjectValue($p,'idno')),
            'TrackID' => getObjectValue($p,'track'),
            'ORNo' => getObjectValue($p,'orno'),
            'ORDate' => setDate(getObjectValue($p,'ordate'),'Y-m-d'),        
            'TermID' => decode(getObjectValue($p,'term')),
            'Remarks'=>getObjectValue($p,'remarks'),
        ];        
        $model->create($data);        
        return ['error'=>false,'msg' => 'successfully saved!'];        
    }


}	
?>


