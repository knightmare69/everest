<?php
namespace App\Modules\Admission\Services;


use Mail;
use sysAuth;
use DB;
use Request;

Trait InquiryProperties {

	public function QuarterLists()
	{
		return [
			1 => 'Quater I',
			2 => 'Quater II',
			3 => 'Quater III',
			4 => 'Quater IV',
		];
	}

	public function getQuarters($key) {
		return array_search($key, array_flip($this->QuarterLists()));
	}

	public function quarter($key) {
		$quaters = [
			1 => [7,8,9],
			2 => [10,11,12],
			3 => [1,2,3],
			4 => [4,5,6],
		];

		return isset($quaters[$key]) ? $quaters[$key] : '';
	}

	public function setQuarter() {
		return [
			1 => [7,8,9],
			2 => [10,11,12],
			3 => [1,2,3],
			4 => [4,5,6],
		];

	}

}
?>
