<?php
namespace App\Modules\Admission\Services;

use App\Modules\Admission\Models\Panels;

use App\Modules\Admission\Services\InquiryProperties;

use DB;
use Request;
use Response;

Class InquiriesDatatable {

	use InquiryProperties;
   
	public function __construct(){
		$this->model = DB::table('es_inquiry as a')
							->join('es_inquiry_parent as p',
									'p.id','=','a.InquiryParentID'
								)
							->join('vw_YearLevel as y', function($join) {
									$join->on('y.YearLevelID','=','a.YearLevelID');
									$join->on('y.ProgClass','=','a.ProgClass');

								})->whereRaw("ISNULL(a.AppNo,'')=''");
	}

	public function filter(){

		$hasParam = false;
		$status   = array(
		             'inquiry'=>'INQUIRED',
		             'wait'=>'WAIT POOL',
		             'apply'=>'INQUIRED',
		             'tour'=>'INTERVIEW',
		             'exam'=>'EXAM',
		             'denied'=>'DENIED',
		            );
		$columns = array(
			'a.id',
			DB::raw("concat(a.lname,', ',a.fname) as name"),
			'y.YearLevel',
			'p.email',
			'p.contact_no',
			'a.ScheduleType',
			'a.IsApplied',
			DB::raw('MONTH(a.CreatedDate) as Month'),
			DB::raw('dbo.fn_AcademicYear(a.TermID) AS SY'),
			'a.birth_date',
			'a.gender',
			DB::raw("concat(p.lname,', ',p.fname) as pname")
		);

		$model = $this->model->select($columns);
		
		$gradeLevel = explode('-',Request::get('gradeLevel'));
		$gradeLevelSel = decode($gradeLevel[0]);
		
		if (Request::get('filter')) {	
 			$model->where('ApplicantName','like','%'.trimmed(Request::get('filter')).'%');
 			$model->Orwhere('AppNo','like','%'.trimmed(Request::get('filter')).'%');
 			$hasParam = true;
 		}

 		if (Request::get('schoolCampus')) {
 			$model->where('CampusID',decode(Request::get('schoolCampus')));
 			$hasParam = true;
 		}

 		if (Request::get('schoolYear')) {
 			$model->where('TermID',decode(Request::get('schoolYear')));
 			$hasParam = true;
 		}

 		if (Request::get('quarter')) {
 			if ($this->quarter(Request::get('quarter'))) {
 				$model->whereIn(DB::raw('MONTH(a.CreatedDate)'),$this->quarter(Request::get('quarter')));
 				$hasParam = true;
 			}
 		}

 		if ($gradeLevelSel && $gradeLevelSel != 0) {
 			$model->where('y.YearLevelID',$gradeLevelSel);
 			$hasParam = true;
 		}

 		if (isParent()) {
 			$model->where('FamilyID',getGuardianFamilyID());
 		}

 		if (IsPanel()) {
 			$model->where('a.StatusID',env('AO_APPROVED_STATUSID'));
 			$model->whereRaw("
 				(
 					select count(*) from esv2_admission_panel_approval as ap
 					inner join esv2_admission_panels as p
 					on p.id=ap.PanelID
 					where ap.AppNo=a.AppNo and p.UserID in(".getUserID().")
 				) <= 0"
 			);
 		}
		
		$iTotalRecords = $model->count();
		  
		$records = array();
		$records["data"] = array(); 

		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);

		$data = $model->skip($iDisplayStart)->take($iDisplayLength)->get();

		foreach($data as $row) {
			$options = $row->ScheduleType == 'wait' ? '<option selected value="wait">Wait Pool</option>' : '<option value="wait">Wait Pool</option>';
		  //$options .= $row->ScheduleType == 'tour' ? '<option selected value="tour">For Tour/Interview</option>' : '<option value="tour">For Tour/Interview</option>';
			$options .= $row->ScheduleType == 'apply' ? '<option selected value="apply">For Application</option>' : '<option value="apply">For Application</option>';
		  //$options .= $row->ScheduleType == 'exam' ? '<option selected value="exam">For Exam</option>' : '<option value="exam">For Exam</option>';
			$options .= $row->ScheduleType == 'denied' ? '<option selected value="denied">Denied</option>' : '<option value="denied">Denied</option>';

			$records['data'][] = [
				$status[$row->ScheduleType],
				"<div class='col-md-10' style='padding: 0 40px 0 0 !important'>
					<select style='width:150px !important;' class='form-control list-type' name='type'>
						<option value=''>CHOOSE</option>
						".$options."
					</select>
				</div>
				<div class='col-md-2 hide' style='padding: 0 !important'>
					<button type='button' class='btn btn-sm btn-small btn-primary btn-action' data-id='".$row->id."'><i class='fa fa-edit'></i></button>
				</div>",
				$row->SY,
				$row->YearLevel,
				// $this->getQuarterLabel($row->Month),
				$row->name,
				$row->gender == 'M' ? 'Male' : 'Female',
				$row->birth_date,
				$row->pname,
				$row->email,
				$row->contact_no
				
				/*
				$row->IsApplied ? '': "
				<div class='col-md-10' style='padding: 0 5px 0 0 !important'>
					<select class='form-control list-type' name='type'>
						<option value=''>CHOOSE</option>
						<option value='apply'>For Application</option>
						<option value='tour'>For Tour</option>
						<option value='exam'>For Exam</option>
					</select>
				</div>
				<div class='col-md-2' style='padding: 0 !important'>
					<button type='button' class='btn btn-sm btn-small btn-primary btn-action' data-id='".$row->id."'><i class='fa fa-edit'></i></button>
				</div>
				"
				*/
			];
		}
		
		if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
		    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
		    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
		 }
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		return $records;
	}

	public function getQuarterLabel($month) {
		foreach($this->setQuarter() as $q => $values) {
			if (in_array($month, $values)) {
				return $this->getQuarters($q);
			}
		}
	}

}	

?>

