<?php
namespace App\Modules\Admission\Services\Admission;
use Validator;
use DB;
Class Validation {
	
	public function guardian($data)
	{
		return Validator::make($data, [
            'name' => 'required|max:255',
            'livingWith' => 'required',
            // 'province' => 'required',
            'resident' => 'required',
            // 'address' => 'required',
            // 'city' => 'required',
            'mobile' => 'required',
            'email' => 'required|email'
        ]);
	}
}	
?>