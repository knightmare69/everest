<?php
namespace App\Modules\Admission\Services\Admission;

use App\Modules\Admission\Models\AdmissionPhoto;
use App\Modules\Admission\Models\AdmissionGuardianPhoto;

use DB;

Class Photo {
	
	public function __construct()
	{
		$this->photo = new AdmissionPhoto;
		$this->guardianPhoto = new AdmissionGuardianPhoto;
	}

	public $photo = '';

	public $FamilyID = '';

	public function AdmissionPhoto()
	{
		 /* student */
       	if (isset($_FILES['photo'])) {
	       	$file = getFileInfo($_FILES['photo']);

	       	$data = [
				'AppNo' => $this->AppNo,
				'FileName' => getObjectValue($file,'FileName'),		
				'Attachment' => DB::raw('0x'.bin2hex(file_get_contents(getObjectValue($file,'Attachment')))),
				'FileType' => getObjectValue($file,'FileType'),
				'FileSize' => getObjectValue($file,'FileSize'),
				'FileExtension' => getObjectValue($file,'FileExtension')
			];

	       	if ($this->photo->where('AppNo',$this->AppNo)->count() <= 0) {
	       		$data['CreatedDate'] = systemDate();
	       		$data['CreatedBy'] = getUserID();
	       		$this->photo->insert($data);
	       	} else {
	       		$data['ModifiedDate'] = systemDate();
	       		$data['ModifiedBy'] = getUserID();
				$this->photo->where('AppNo',$this->AppNo)->update($data);
	       	}
       }
       	/* student end */

       	/* Guardian */
       	if (isset($_FILES['guardianPhoto'])) {
	       	$file = getFileInfo($_FILES['guardianPhoto']);
	       	
	      	$data = [
				'FamilyID' => $this->FamilyID,
				'FileName' => getObjectValue($file,'FileName'),		
				'Attachment' => DB::raw('0x'.bin2hex(file_get_contents(getObjectValue($file,'Attachment')))),
				'FileType' => getObjectValue($file,'FileType'),
				'FileSize' => getObjectValue($file,'FileSize'),
				'FileExtension' => getObjectValue($file,'FileExtension')
			];

	       	if ($this->guardianPhoto->where('FamilyID',$this->FamilyID)->count() <= 0) {
	       		$data['CreatedDate'] = systemDate();
	       		$data['CreatedBy'] = getUserID();
	       		$this->guardianPhoto->insert($data);
	       	} else {
	       		$data['ModifiedDate'] = systemDate();
	       		$data['ModifiedBy'] = getUserID();
				$this->guardianPhoto->where('FamilyID',$this->FamilyID)->update($data);
	       	}

	       	/* Guardian End */
       }
	}
}	
?>