<?php
namespace App\Modules\Admission\Services\Admission;

use App\Modules\Admission\Models\Admission as AdmissionModel;
use App\Modules\Admission\Models\RequiredDocs;
use Validator;
use DB;
Class Helper {

	public function __construct()
	{
		$this->admission =  new AdmissionModel();
		$this->docs = new RequiredDocs;
	}

	public function getAppNo($data){  
	    $exec    = DB::select("SELECT TOP 1 AppNo FROM ESv2_Admission ORDER BY AppNo DESC");
		$appno   = (($exec && count($exec))?$exec[0]->AppNo:date('Y-0000'));
		$tmp_arr = explode('-',$appno);
		$tmp_arr[0] = date('Y');
		$tmp_arr[1] = str_pad(($tmp_arr[1]+1),4,'0',STR_PAD_LEFT);
		return implode('-',$tmp_arr);
	}

	public function verifyORCode($post)
	{
		$data = DB::select("sp_K12_Verify_ORSecurityCode @ORNo = '".getObjectValue($post,'ORCode')."' , @SecurityCode = '".getObjectValue($post,'ORSecurityCode')."'");
		return isset($data[0]->VerifiedCorrect) ? $data[0]->VerifiedCorrect : 0;
	}

	public function getExamSchedDates($post)
	{
		return DB::select("EXEC sp_K12_TestingSchedules '".decode(getObjectValue($post,'campus'))."','".decode(getObjectValue($post,'term'))."','".decode(getObjectValue($post,'gradeLevel'))."'");
	}

	public function getStatus($tempStatus,$AdmissionStatus)
	{
		// return !isParent() ? $tempStatus == '1' ? $tempStatus : decode($AdmissionStatus) : $tempStatus;
		return !isParent() ? $tempStatus == '1' ? $tempStatus : decode($AdmissionStatus) : decode($AdmissionStatus);
	}

	public function isAdmitted($AppNo)
	{
		if($this->admission
		 	->select('StatusID')
		 	->where('AppNo',$AppNo)
		 	->pluck('StatusID') == AppConfig()->AdmittedStatusID()
		 ) {
		 	return true;
		 }
		 return false;
	}

	public function updateDocFilename($EntryID,$filename)
	{
		$this->docs->where('EntryID',$EntryID)->update(['Filename'=>$filename]);
	}

	public function updateStatus($AppNo,$StatusID)
	{
	    $exec = DB::statement("INSERT INTO ESv2_AdmissionStatusRemarks(AppNo,StatusID,Remarks,CreatedBy,CreatedDate) VALUES ('".$AppNo."','".$StatusID."','update','".getUserID()."','".date('Y-m-d H:i:s')."')");
		$this->admission->where('AppNo',$AppNo)->update(['StatusID'=>$StatusID]);
	}

	public function getHigherYearLevel()
	{
		return $this->admission->getHigherYearLevel();
	}

	public function getBasicYearLevel()
	{
		return $this->admission->getGradeLevel();
	}

	public function hasLoginAccount($FamilyID)
	{
		return (DB::table('ESV2_Users')->where('FamilyID',$FamilyID)->count() > 0);
	}

	public function gerLoginAccount($FamilyID)
	{
		return DB::table('ESV2_Users')->select(['Username','UserIDX'])->where('FamilyID',$FamilyID)->get();
	}

	public function isLoginUsernameNotExist($username)
	{
		return (DB::table('ESV2_Users')->where('Username',$username)->count() <= 0);
	}

	public function getCourses()
	{
		return
		DB::table('ES_Programs as p')
			->select([
				DB::raw('ISNULL(p.ProgID, 0) as ProgID'),
				DB::raw('ISNULL(d.IndexID, 0) AS MajorID'),
				DB::raw("ISNULL(p.ProgName,'') +ISNULL(case when (d.MajorDiscDesc = '') then '' else ' Major '+d.MajorDiscDesc end,'') as course"),

			])
			->leftJoin('ES_ProgramMajors as pm',
				'pm.ProgID','=','p.ProgID'
			)
			->leftJoin('ES_DisciplineMajors as d',
				'd.IndexID','=','pm.MajorDiscID'
			)
			->where(DB::raw('ISNULL(p.ProgID, 0)'),'<>',1)
			->get();
	}

	public function saveCourseChoices($key,$data)
	{
		//return if not enabled college
		if (!config('app.college')) return;
		$choices = [];
		$data = json_decode('['.$data.']',true);
		$data = isset($data[0]) ? $data[0] : [];
		if ($key) {
			$choices = [
				'Choice1_CampusID' => decode(getObjectValue($data,'campus1')),
				'Choice2_CampusID' => decode(getObjectValue($data,'campus2')),
				'Choice3_CampusID' => decode(getObjectValue($data,'campus3')),
				'Choice4_CampusID' => decode(getObjectValue($data,'campus3')),
				'Choice1_Course' => decode(getObjectValue($data,'course1')),
				'Choice1_CourseMajor' => decode(getObjectValue($data,'course1Major')),
				'Choice2_Course' => decode(getObjectValue($data,'course2')),
				'Choice2_CourseMajor' => decode(getObjectValue($data,'course2Major')),
				'Choice3_Course' => decode(getObjectValue($data,'course3')),
				'Choice3_CourseMajor' => decode(getObjectValue($data,'course3Major')),
				'Choice4_Course' => decode(getObjectValue($data,'course4')),
				'Choice4_CourseMajor' => decode(getObjectValue($data,'course4Major')),
			];

			$this->admission->where('AppNo',$key)
			 	->update($choices);
		}
	}
}
?>
