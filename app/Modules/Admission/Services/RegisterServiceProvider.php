<?php
namespace App\Modules\Admission\Services;

use App\Modules\Admission\Services\Register\Validation as validate;
use App\Modules\Security\Models\Users\User;
use Mail;
use sysAuth;
use DB;
use Config;

Class RegisterServiceProvider {

	protected $code;
	public $FamilyID;
	public function __construct()
	{
		$this->validate = new validate;
        $this->pswd = new sysAuth;
	}

	public function isValid($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validate->register($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}

		elseif ($action == 'confirmation') {
			if ($this->validate->isConfirmed($post)) {
				return alreadyConfirmCode();
			}
		}
		return ['error'=>false,'message'=> ''];
	}

	public function account($FamilyID,$post)
	{

       $group = (getObjectValueWithReturn($post,'acctype', 0) != 0 ? getParentGroupID() : getStudentsGroupID() ) ;

		return [
			'Username' => getObjectValue($post,'Username'),
			'password' =>  bcrypt(getObjectValue($post,'upassword')),
			'FullName'	=>  getObjectValue($post,'fullname'),
			'Email' =>  getObjectValue($post,'Email'),
			'PositionID' =>  getObjectValueWithReturn($post,'position',0),
			'DepartmentID' =>  getObjectValueWithReturn($post,'department',0),
			'UsersGroupID' =>  $group,
			'CreatedBy'	=>  '',
			'CreatedDate' =>  systemDate(),
			'FamilyID' => (getObjectValueWithReturn($post,'acctype', 0) != 0 ? $FamilyID : 0 ) ,
			'ConfirmNo' => $this->getConfirmNo(),
            'PwdExpiryDate' => $this->pswd->getExpiryDate(),
		];
	}

	public function guardian($post){
		return [
			'FamilyID'      => $this->getFamilyID(getObjectValue($post,'fname')),
			'Guardian_Name' => getObjectValue($post,'fullname'),
			'Guardian_Address'=> getObjectValue($post,'address'),
			'Guardian_TownCity' => getObjectValue($post,'city'),
			'Guardian_ZipCode' => getObjectValue($post,'zipcode'),
			'Guardian_TelNo' => getObjectValue($post,'telno'),
			'Guardian_Mobile' => getObjectValue($post,'mobile'),
			'Guardian_Email' => getObjectValue($post,'Email'),
		];
	}

	public function email($post)
	{
		ini_set('max_execution_time', 14400);
		Config::set('mail.host', 'smtp.googlemail.com');
        Config::set('mail.port', '465');
        Config::set('mail.username', 'admissionsnoreply@everestmanila.edu.ph');
        Config::set('mail.password', 'everest123!');
		// Mail::send('email.confirmation', ['post'=>$post], function ($message) use ($post) {
			// $message->from('k12@princetech.com.ph', 'K to 12 System');
        	// $message->to(getObjectValue($post,'Email'))->subject('K to 12 Email Confirmation');
            // $message->bcc('k12@princetech.com.ph');
		// });
		Mail::send('email.confirmation', ['post'=>$post], function ($message) use ($post) {
            $message->from(Config::get('mail.username'), 'Everest Academy Manila Admissions Inquiry');
            $message->to(getObjectValue($post,'Email'))->subject('Everest Academy Manila Admissions Inquiry');
            // $message->bcc('admissions@everestmanila.edu.ph');
            $message->bcc(env('SYS_EMAIL'));
        });
		return 1;
	}

	public function emailVCode($username)
	{
		ini_set('max_execution_time', 14400);
		$account = new User;

		$data = $account->where('Username',$username)->get();
		$data = isset($data[0]) ? $data[0] : [];
		Mail::send('email.resend_vcode', ['data'=>$data], function ($message) use ($data) {
			$message->from('k12@princetech.com.ph', 'K to 12 System');
        	$message->to(getObjectValue($data,'Email'))->subject('K to 12 Email Confirmation');
            $message->bcc('k12@princetech.com.ph');
		});
		return 1;
	}

	public function genereteIdentityAccount($guardianName)
	{
		$account = new User;
		$username = getLetterEach($guardianName,3);

		$usernameCount = $account->where('username',$username)->count();
		return (int)$usernameCount > 0 ? $username.($usernameCount + 1) : $username;
	}

	protected function setConfirmNo()
	{
		$code = str_shuffle(md5(DB::table('ESv2_users')->count()+1));
		return  substr($code,-10);
	}

	public function getConfirmNo()
	{
		if (DB::table('ESv2_users')->where('ConfirmNo',$this->setConfirmNo())->count() > 0) {
			return $this->getConfirmNo();
		} else {
			return $this->code = $this->setConfirmNo();
		}
	}

	protected function getFamilyID($fname=''){
	    if(trim($fname)!=''){
		  $tmpname = trim(substr($fname,0,3));
		  $chk     = DB::SELECT("SELECT FamilyID FROM ESv2_Admission_FamilyBackground WHERE FamilyID LIKE '".$tmpname."0%' ORDER BY FamilyID DESC");
		  if($chk && count($chk)>0){
		    $chkid = trim($chk[0]->FamilyID);
			$fcount= intval(substr($chkid,3,4));
			if($fcount>0){
			   $fcount = $fcount+1;
			   return $this->FamilyID = $tmpname.str_pad($fcount, 4, "0", STR_PAD_LEFT);
			}else{
			   return $this->FamilyID = $tmpname.str_pad('1', 4, "0", STR_PAD_LEFT);
			}
		  }else{
		    return $this->FamilyID = $tmpname.str_pad('1', 4, "0", STR_PAD_LEFT);
		  }
		}else
		  return $this->FamilyID = DB::table('ESv2_Admission_FamilyBackground')->count()+1;
	}
}
?>
