<?php
namespace App\Modules\Admission\Services;

use App\Modules\Admission\Models\Admission as AdmissionModel;
use App\Modules\Students\Models\StudentProfile as StudProfileModel;
use App\Modules\Enrollment\Models\Registration as RegsModel;
use App\Modules\Setup\Models\GradingSystemSetup as EsGs_Setup;

use DB;
use Request;
Class GuardianConnectServices {

    public function __construct()
    {
        $this->initializer();
    }

    public function search() {
        
		//$date = explode('/',trimmed(Request::get('BirthDate')));
		//$date = $date[2].'-'.$date[1].'-'.$date[0];
        $p = Request::all();
        
        $idno = getObjectValue($p,'idno');        
                
        $student = DB::table('ES_Students as s')
                        ->select(['StudentNo','LastName', 'FirstName', DB::raw(' dbo.fn_YearLevel_k12(YearLevelID, dbo.fn_ProgramClassCode(ProgID)) As YearLevel') ])
                        ->where('StudentNo',trimmed($idno));
                
        $isOld = true;
        
        /*
        if ($student->count() > 0) {
            
            $model = $student->select([ 'Gender', 'StudentNo as ChilNo',
                    DB::raw('1 as isOld'),
                    DB::raw("(case when (FamilyID = '' OR FamilyID is null) then 0 else 1 end) as isConnected"),
                    DB::raw("LastName+', '+FirstName+' '+MiddleName as name"),
                    DB::raw('CONVERT(date,DateOfBirth) as DateOfBirth'),
                    DB::raw("(
                                select YearLevel from vw_YearLevel
                                where ProgClass in (select  ProgClass from ES_Programs where ProgID=s.ProgID)
                                and YearLevelID = s.YearLevelID
                          ) as YearLevel"
                    )
            ]);
            
        } else {

              $isOld = false;
              $model = DB::table('ES_Admission as a')
              ->select([
                    'Gender',
                    'AppNo as ChilNo',
                    DB::raw('0 as isOld'),
                    DB::raw("(case when (FamilyID = '' OR FamilyID is null) then 0 else 1 end) as isConnected"),
                    DB::raw("(select (case when (FamilyID = '' OR FamilyID is null) then 0 else 1 end) from ESv2_Admission where AppNo=a.AppNo) as parentConnected"),
                    DB::raw("LastName+', '+FirstName+' '+MiddleName as name"),
                    DB::raw('CONVERT(date,DateOfBirth) as DateOfBirth'),
                    DB::raw("(
                                select YearLevel from vw_YearLevel
                                where ProgClass in (select  ProgClass from ES_Programs where ProgID=a.Choice1_Course)
                                and YearLevelID in (select YearLevelID from ES_Students where AppNo=a.AppNo)
                          ) as YearLevel"
                    )
              ])
              ->where('a.TermID',
                    AppConfig()->currentTermiID()
              )
              ->where('FirstName',
                    trimmed(Request::get('FirstName'))
              )
              ->where('LastName',
                    trimmed(Request::get('LastName'))
              )
              ->where(DB::raw('convert(date,DateOfBirth)'),
                    $date
              );
        }

		if (Request::get('ChildNo')) {
                  if ($isOld) {
                        $model = $model->where('StudentNo',trimmed(Request::get('ChildNo')));
                  } else {
                        $model = $model->where('AppNo',trimmed(Request::get('ChildNo')));
                  }
            }

		if (Request::get('MiddleName')) {
			$model = $model->where('MiddleName',trimmed(Request::get('MiddleName')));
		}
        */
        
		return $student;

	}

    public function connectOld() {
        
        $model = new \App\Modules\Registrar\Models\StudentRequests_model;
        
        $p = Request::all();
        
        //get family id
        $id = getUserID();
        $data = array(
            'StudentNo' => getObjectValue($p,'ChildNo'),
            'RequestTypeID' => 3,
            'RequestDate' => systemDate(),
            'RequestBy' => $id,
        );
        $result = $model->create($data);
        /*
        DB::table("ES_GS_StudentRequests")->insert();
                  ->where('StudentNo',Request::get('ChildNo'))
                  ->update(['FamilyID' => $FamilyID]);
        if ( DB::table("ES_Students")
                        ->where('StudentNo',
                              Request::get('ChildNo')
                        )
                        ->where('FamilyID',
                              $FamilyID
                        )
                        ->count()
                        > 0
            ){
            return true;
        }
        */
        
        return $result;
    }


    public function connectStudentsData($family_id = null)
    {
        $family_id = !empty($family_id) ? $family_id : getGuardianFamilyID();
        $q = DB::table('es_students as S')->select('S.LastName', 'S.FirstName', 'S.MiddleName', 'S.MiddleInitial', 'S.ExtName', 'S.Gender', 'S.DateOfBirth', 'S.StudentNo', DB::raw('dbo.fn_K12_YearLevel3(S.YearLevelID, S.ProgID) as YearLevel'))
              //->join('ESv2_ParentSubscription as Subs', 'S.StudentNo', '=', 'Subs.StudentNo')
                ->where('S.FamilyID', $family_id)
                ->get();

        return $q;
    }

	public function connectNew()
	{
            //select table admission
		$model = DB::table('ES_Admission')->where('AppNo',Request::get('ChildNo'))->get();

            //get family id
            $FamilyID = getGuardianFamilyIDByUserIDX(decode(Request::get('UserKey')));

            if (count($model) > 0) {
                  $row = getRawData($model);

                  //checking of program class
                  $ProgClass = DB::table('ES_Programs')->select('ProgClass')->where('ProgID',$row->Choice1_Course)->pluck('ProgClass');

                  //is higher level ? base is 21 basic education
                  $isCollege = $ProgClass > 21 ? 1 : 0;

                  //get year level/grade level id

                  $YearLevelID = DB::table('ES_Students')->select('YearLevelID')->where('AppNo',$row->AppNo)->pluck('YearLevelID');

                  $admission = DB::table("ESV2_Admission")
                        ->where('AppNo',Request::get('ChildNo'));

                  if ($admission->count() > 0) {

                        $admission->update(['FamilyID'=>$FamilyID]);

                        $model = DB::table('ES_Admission')
                              ->where('AppNo',Request::get('ChildNo'))
                              ->update(['FamilyID'=>$FamilyID]);

                        return true;
                  };

                  //get data repository for esv2_admission transfer from es_admission
                  $data = [
                        'AppNo' =>  $row->AppNo,
                        'AppDate' => $row->AppDate,
                        'CampusID' => $row->Choice1_CampusID,
                        'TermID' => $row->TermID,
                        'AppTypeID' => $row->ApplyTypeID,
                        'GradeLevelID' => $YearLevelID,
                        'ProgramID' => $row->Choice1_Course,
                        'MajorID' => $row->Choice1_CourseMajor,
                        'LastName' => $row->LastName,
                        'FirstName' => $row->FirstName,
                        'MiddleName' => $row->MiddleName,
                        'MiddleInitial' => $row->MiddleInitial,
                        'ExtName' => $row->ExtName,
                        'Gender' => $row->Gender,
                        'DateOfBirth' => $row->DateOfBirth,
                        'PlaceOfBirth' => $row->PlaceOfBirth,
                        'CivilStatusID' => $row->CivilStatusID,
                        'NationalityID' => $row->NationalityID,
                        'ForeignStudent' => $row->ForeignStudent,
                        'PassportNumber' => $row->PassportNumber,
                        'VisaStatus' => $row->VisaStatus,
                        'SSP_Number' => $row->SSP_Number,
                        'Validity_of_Stay' => $row->Validity_of_Stay,
                        'ReligionID' => $row->ReligionID,
                        'FamilyID' => $FamilyID,
                        'DenyReason' => $row->DenyReason,
                        'Elem_School' => $row->Elem_School,
                        'Elem_Address' => $row->Elem_Address,
                        'Elem_InclDates' => $row->Elem_InclDates,
                        'Elem_GradeLevel' => '',
                        'Elem_GWA' => '',
                        'Elem_AwardHonor' => $row->Elem_AwardHonor,
                        'HS_School' => $row->HS_School,
                        'HS_Address' => $row->HS_Address,
                        'HS_InclDates' => $row->HS_InclDates,
                        'HS_GradeLevel' => '',
                        'HS_GWA' => '',
                        'HS_AwardHonor' => $row->HS_AwardHonor,
                        'TestingSchedID' => $row->TestingSchedID,
                        'ORNo' => $row->ORno,
                        'ORSecurityCode' => '',
                        'CreatedDate' => systemDate(),
                        'CreatedBy' => getUserName(),
                        'IsOnlineApplication' => isParent() ? 1 : 0,
                        'StatusID' => $row->AdmStatusID,
                        'ESigInitial' => '',
                        'ESigDate' => '',
                        'LRN' => '',
                        'IsCollege' => $isCollege,
                        'ProgClass' => $ProgClass,
                        'Choice1_CampusID' =>  $row->Choice1_CampusID,
                        'Choice2_CampusID' =>  $row->Choice2_CampusID,
                        'Choice3_CampusID' => $row->Choice3_CampusID,
                        'Choice4_CampusID' =>  $row->Choice4_CampusID,
                        'Choice1_Course' => $row->Choice1_Course,
                        'Choice1_CourseMajor' => $row->Choice1_CourseMajor,
                        'Choice2_Course' => $row->Choice2_Course,
                        'Choice2_CourseMajor' => $row->Choice2_CourseMajor,
                        'Choice3_Course' => $row->Choice3_Course,
                        'Choice3_CourseMajor' => $row->Choice3_CourseMajor,
                        'Choice4_Course' => $row->Choice4_Course,
                        'Choice4_CourseMajor' => $row->Choice4_CourseMajor,

                  ];

                  //insert data to esv2_admission
                  $status = $this->model->create($data);

                  //update es_admission family id
                  if ($status->AppDate) {
                        $model = DB::table('ES_Admission')->where('AppNo',$row->AppNo)->update(['FamilyID'=>$FamilyID]);
                        return true;
                  }
            }
            return false;
	}

      private function initializer() {
            $this->model = new AdmissionModel;
      }
}
?>