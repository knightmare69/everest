<?php
namespace App\Modules\Admission\Services\Completion;
use Validator;
use DB;

use App\Modules\Admission\Models\Admission;
use App\Modules\Admission\Services\Completion\Helper;

Class Completion {

    use Helper;
	
    protected $AppNo;
    protected $model;

    public function __construct() {
        $this->model = new Admission;
    }
	 
    public function updateStatus($AppNo) {
        $this->AppNo = $AppNo;
        
        $this->model->where('AppNo',$this->AppNo)
            ->update([
                'CompletionStatus' => $this->progress()
            ]);
    }

    public function all() {
        foreach($this->model->select(['AppNo'])->get() as $row) {
            $this->updateStatus($row->AppNo);
        }
    }

    private function progress() {
        return $this->percentage();
    }

    private function percentage() {
        $total = $this->getTotalFilledRequiredDocs() + $this->getTotalFilledFields();
        // echo $total.'/'.$this->totalRequired().'-';
        return  ($total/$this->totalRequired())*100;
    }

    private function getTotalFilledFields() {
        $fill = 0;
        foreach($this->getData() as $key =>  $value) {
            if (!$this->isEmpty($value)) {
                $fill += 1;
            }
        }
        return $fill;
    }

    private function getTotalFilledRequiredDocs() {
        $data = new \App\Modules\Admission\Models\RequiredDocs;
        $fill = 0;
        foreach($data->where('AppNo',$this->AppNo)->get() as $row) {
            if (in_array($row->DocID,$this->getRequiredFields()['docs'])) {
                $fill++;
            }
        }
        return $fill;
    }
   
    private function totalRequired() {
        $required = count($this->getRequiredFields()['required']); 
        $gs = count($this->getRequiredFields()['optional']['gs']);
        $hs = count($this->getRequiredFields()['optional']['hs']);
        $docs = count($this->getRequiredFields()['docs']);
        if ($this->isGS()) {
            $hs = 0;
        }
        if ($this->isHS()) {
            $gs = 0;
        }
        return $required + $gs + $hs + $docs;
    }

    private function getData() {
        $fields = array_merge(
            $this->getRequiredFields()['required'],
            $this->getRequiredFields()['optional']['gs'],
            $this->getRequiredFields()['optional']['hs']
        );
        return $this->model
                ->where('AppNo',$this->AppNo)
                ->select($fields)
                    ->first()->toArray();
    }

    private function isGS() {
        return $this->getProgClass() <= 20;
    }

    private function isHS() {
        return $this->getProgClass() > 20;
    }

    private function getProgClass() {
        return $this->model->select('ProgClass')->where('AppNo',$this->AppNo)->pluck('ProgClass');
    }

   


}	
?>