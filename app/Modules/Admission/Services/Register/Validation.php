<?php
namespace App\Modules\Admission\Services\Register;
use Validator;
use DB;
Class Validation {
	
	public function register($data)
	{
		return Validator::make($data, [
            'fullname' => 'required|max:255',
            'Email' => 'required|email|max:255|unique:ESv2_users',
            'upassword' => 'required|confirmed|min:6',
            'Username' => 'required|unique:ESv2_users',
            'upassword' => 'required',
            'acctype' => 'required',
        ]);
        
        /*
                    'mobile' => 'required',
            'address' => 'required',
            'city' => 'required',

        */
        
	}

    public function inquiry($data)
    {
        return Validator::make($data, [
            'fname' => 'required|max:50',
            // 'mname' => 'required|max:50',
            'lname' => 'required|max:50',
            'email' => 'required|email|max:255|unique:es_inquiry',
            // 'yearlevel' => 'required',
            'contact_no' => 'required'
        ]);
        
    }

    public function childInquiry($data)
    {
        return Validator::make($data, [
            'fname' => 'required|max:50',
            // 'mname' => 'required|max:50',
            'lname' => 'required|max:50',
            // 'schedule_type' => 'required',
            // 'email' => 'required|email|max:255|unique:es_inquiry',
            'yearlevel' => 'required',
            // 'contact_no' => 'required'
        ]);
        
    }

    public function isConfirmed($code)
    {
        if (DB::table('ESv2_users')->where(['IsConfirmed'=>1,'ConfirmNo'=>trimmed($code)])->count() > 0) {
            return true;
        }
        return false;
    }   

}	
?>