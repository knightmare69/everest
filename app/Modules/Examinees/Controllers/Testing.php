<?php

namespace App\Modules\Examinees\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Examinees\Services\ExamineesServiceProvider as Services;
use App\Modules\Examinees\Models\Examinees as ExamineesModel;

use Mail;
use Request;
use Response;
use Permission;

class Testing extends Controller
{
    private $media =
        [
            'Title' => 'Testing',
            'Description' => 'List of examinees ready for testing.',
            'js' => ['Examinees/main'],
            'init' => ['Metronic.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'select2/select2.min',
                        ],
            'plugin_css' => ['bootstrap-datepicker/css/datepicker', 'select2/select2', 'datatables/plugins/bootstrap/dataTables.bootstrap'],
        ];

    private $url = ['page' => 'examinees/testing/', 'form' => '.exam-testing-form'];

    public $views = 'Examinees.Views.Testing.';
    private $parent_view = 'Examinees.Views.';

    public function index()
    {
        $this->initializer();
        if ($this->permission->has('read')) {
            return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'update':
                    if ($this->permission->has('edit')) {
                        $validation = $this->services->isValid(Request::all(), 'update');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $data = $this->services->postTesting(Request::all());
                            $this->model->where('AppNo', decode(Request::get('id')))->update($data);
                            if (!empty(Request::get('send-email'))) {
                                $email = $this->sendEmail(Request::all());
                                if($email != false){
                                    $response = ['error' => false, 'message' => 'Successfully updated record and sent email to guardian/parent.'];
                                } else {
                                    $response = ['error' => true, 'message' => 'Unable to send email. Guardian/Parent email is missing.'];
                                }
                            } else {
                                $response = ['error' => false, 'message' => 'Successfully Update!'];
                            }
                        }
                    }
                    break;
                case 'edit':
                    $id = decode(Request::get('id'));
                    $response = view($this->parent_view.'modal-default-form', array(
                        'info' => $this->model->getInfoByAppNo($id),
                        'results' => $this->model->getResult('testing'),
                        'form_class' => 'exam-testing-form',
                    ))->render();
                    break;
                case 'search':
                    $_search = Request::get('search');

                    if(empty($_search['value'])){
                        $limit = Request::get('length'); $val = '';
                    } else {
                        $limit = 0; $val = $_search['value'];
                    }

                    $default_filter = array(1, getLatestActiveTerm(), getUserID(), $limit, $val);
                    $data = $this->model->getRecords('testing', $default_filter);

                    $response = $this->formatDataToDisplay($data);
                    break;
                case 'table_get':
                    $data = json_decode(Request::get('search'));
                    $search_for = array(decode($data->campus), decode($data->ac_year), getUserID(), 10, '');
                    $response = array('error' => false, 'table' => view($this->views.'list', ['table' => $this->model->getRecords('testing', $search_for)])->render());
                    break;
            }
        }

        return $response;
    }

    private function init($key = null)
    {
        $this->initializer();

        return array(
            'academic_year' => $this->services->academic_year(),
            'campus' => $this->services->campus(),
        );
    }

    private function formatDataToDisplay($data)
    {
        $return_arr = [];

        if(!empty($data)){
            $stat_classes = array('muted', 'success', 'primary', 'danger');
            $total_count = count($data);
            $return_arr = array(
                'recordsTotal' => $total_count,
                'recordsFiltered' => $total_count,
            );

            for($a = 0; $a < $total_count; $a++){

                $_this = $data[$a];
                $cur_count = $a + 1;

                if(empty($_this->TestingSched)){
                    $sched_date = '';
                } else {
                    $sched = explode(' ', $_this->TestingSched, 2);
                    $sched_date = $sched[0].'<br>'.$sched[1];
                }
                

                $test_actual_date = !empty($_this->TestingActualSched) ? date('m/d/Y', strtotime($_this->TestingActualSched)) : '';

                $return_arr['data'][] = array(
                    $cur_count,
                    $sched_date,
                    $_this->YearLevelName,
                    $_this->ProgName,
                    '<a href="#" onclick="return false;" class="a-select" data-id="'.encode($_this->AppNo).'">'. ucwords(strtolower($_this->ApplicantName)).'</a>',
                    $_this->AppNo,
                    $_this->Gender,
                    round($_this->Age, 2),
                    date('M. j, Y', strtotime($_this->DateOfBirth)),
                    $test_actual_date,
                    '<span class="text-'.$stat_classes[$_this->FinalTestExamResultID].'" id="stat">'.$_this->TestResultDesc.'</span>'
                );
            }

        } else {
            $return_arr = array(
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => []
            );
        }

        return $return_arr;
    }

    private function refreshTable()
    {
        $default_filter = array(1, getLatestActiveTerm(), getUserID());

        return view($this->views.'list', ['table' => $this->model->getRecords('testing', $default_filter)])->render();
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new ExamineesModel();
        $this->permission = new Permission('examinee-testing');
    }

    private function sendEmail($data)
    {
        $info = $this->model->getInfoByAppNo(decode($data['id']));
        $parents = $this->model->getParents($info->FamilyID);

        if(!empty($parents->Guardian_Email)){
            $parent_name = explode(',', $parents->Guardian_Name);
            $colors = array(1 => '#45b6af', 2 => '#428bca', 3 => '#f3565d');
            $status = $this->model->getResult('testing', decode($data['result']));
            $remarks = !empty($data['remarks']) ? $data['remarks'] : '';

            $data = array('parent_fullname' => $parents->Guardian_Name, 'parent_last_name' => $parent_name[0], 'applicant_name' => $info->ApplicantName, 'label' => 'testing', 'status_id' => $status->TestResultID, 'status' => $status->TestResultDesc, 'status_color' => $colors[$status->TestResultID], 'remarks' => $remarks, 'actual_sched_date' => date('M. d, Y', strtotime($data['act-sched-date'])),
            'signed_date' => date('M. d, Y', strtotime($data['signed-date'])), 'email_to' => $parents->Guardian_Email);

            Mail::send($this->parent_view.'exam-email', ['data' => $data], function($m) use ($data){

                // change this for testing
                $email = $data['email_to']; $name = $data['parent_fullname'];
				// end

                $m->from('no-reply@sbca.com.ph', 'San Beda Alabang Online Enrollment');

                $m->to($email, $name)->subject('SBCA Testing Exam Result');
            });

            return true;
        } else {
            return false;
        }
    }
}
