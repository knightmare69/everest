<?php

Route::group(['prefix' => 'examinees', 'middleware' => 'auth'], function () {

    Route::group(['prefix' => 'testing'], function () {
        Route::get('/', 'Testing@index');
        Route::post('event', 'Testing@event');
    });

    Route::group(['prefix' => 'medical'], function () {
        Route::get('/', 'Medical@index');
        Route::post('event', 'Medical@event');
    });

});
