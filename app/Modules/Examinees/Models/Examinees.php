<?php

namespace App\Modules\Examinees\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Examinees extends Model
{
    protected $table = 'ESv2_Admission';
    private $view = 'vw_K12_Admission';
    protected $primaryKey = 'AppNo';

    protected $fillable = array(
            'MedicalSchedID',
            'MedicalActualSched',
            'MedicalResults',
            'MedicalSignedBy',
            'MedicalSignedDate',
            'TestingSchedID',
            'TestingActualSched',
            'TestingExamRefNo',
            'TestingExamRemarks',
    );

    public $timestamps = false;

    public function getResult($type, $id = null)
    {
        // $response = 'false';

        switch ($type) {
            case 'medical':
                $table = 'Esv2_MedicalResults';
                $key = 'MedicalResultID';
                break;
            case 'testing':
                $table = 'Esv2_TestingResults';
                $key = 'TestResultID';
                break;
        }

        if(!empty($id)){
            $response = DB::table($table)->where($key, $id)->first();
        } else {
            $response = DB::table($table)->get();
        }

        return $response;
    }

    public function getInfoByAppNo($id)
    {
        $get = DB::table($this->view)->select('AppNo', 'ApplicantName', 'YearLevelName', 'MedicalActualSched', 'TestingActualSched', 'MedicalSignedDate', 'TestingSignedDate', 'MedicalResults', 'TestingExamRemarks', 'FinalMedicalResultID', 'FinalTestExamResultID', 'FamilyID', 'ProgName')->where('AppNo', $id)->first();

        // $photo = DB::connection('sqlsrvAttachement')->table('AdmissionPhoto')->select('Attachment as Photo', 'FileType', 'FileExtension')->where('AppNo', $id)->first();
        // $get = array_merge($get, $photo);

        return $get;
    }

    public function getRecords($type, $filters)
    {
        switch ($type) {
            case 'testing':
                $get = DB::select('exec sp_k12_listofexaminees_for_testing ?, ?, ?, ?, ?', $filters);
                break;
            case 'medical':
                $get = DB::select('exec sp_k12_listofexaminees_for_medical ?, ?, ?, ?, ?', $filters);
                break;
        }

        return $get;
    }

    public function getParents($familyID)
    {
        $get = DB::table('ESv2_Admission_FamilyBackground')->select('Guardian_Name', 'Guardian_Email', 'Father_Name', 'Father_Email', 'Mother_Name', 'Mother_Email')->where('FamilyID', $familyID)->first();

        return $get;
    }

}
