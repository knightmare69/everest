<?php

namespace App\Modules\Examinees\Services;

use App\Modules\Examinees\Services\Examinees\Validation;
use DB;

class ExamineesServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'save')
    {
        if ($action == 'save') {
            $validate = $this->validation->validateDefault($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else {
            $validate = $this->validation->validateDefault($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }

        return ['error' => false, 'message' => ''];
    }

    public function postTesting($post)
    {
        return [
            'TestingActualSched' => date('Y-m-d', strtotime(getObjectValue($post, 'act-sched-date'))),
			'TestingSignedDate' => date('Y-m-d', strtotime(getObjectValue($post, 'signed-date'))),
			'TestingSignedBy' => getUserID(),
            'FinalTestExamResultID' => decode(getObjectValue($post, 'result')),
            'TestingExamRemarks' => getObjectValue($post, 'remarks'),
        ];
    }

	public function postMedical($post)
	{
		return [
            'MedicalActualSched' => date('Y-m-d', strtotime(getObjectValue($post, 'act-sched-date'))),
			'MedicalSignedDate' => date('Y-m-d', strtotime(getObjectValue($post, 'signed-date'))),
			'MedicalSignedBy' => getUserID(),
            'FinalMedicalResultID' => decode(getObjectValue($post, 'result')),
            'MedicalResults' => getObjectValue($post, 'remarks'),
        ];
	}

	public function academic_year()
    {
        $get = DB::table('vw_K12_AcademicYears')->where('Active_OnlineEnrolment', 1)->orderBy('AcademicYear', 'DESC')->get();
        return $get;
    }

    public function campus()
    {
        $get = DB::table('ES_Campus')->select('CampusID', 'Acronym', 'ShortName')->get();
        return $get;
    }
}
