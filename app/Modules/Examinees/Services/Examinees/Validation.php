<?php

namespace App\Modules\Examinees\Services\Examinees;

use Validator;

class Validation
{
    public function validateDefault($post)
    {
        $test = array(
            'sched_date' => getObjectValue($post, 'act-sched-date'),
            'signed_date' => getObjectValue($post, 'signed-date'),
            'result_id' => decode(getObjectValue($post, 'result')),
            'remarks' => getObjectValue($post, 'remarks'),
        );

        $rules = array(
            'sched_date' => 'required|date',
            'signed_date' => 'required|date',
            'result_id' => 'required|numeric',
            'remarks' => 'string',
        );

        $validator = validator::make($test, $rules);

        return $validator;
    }
}
