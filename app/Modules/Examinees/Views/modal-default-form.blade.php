<?php
$data=isset($data[0]) ? $data[0] : array();

if($form_class == 'exam-testing-form'){
    $ac_date = !empty($info->TestingActualSched) ? date('m/d/Y', strtotime($info->TestingActualSched)) : date('m/d/Y');
    $s_date = !empty($info->TestingSignedDate) ? date('m/d/Y', strtotime($info->TestingSignedDate)) : date('m/d/Y');
    $result_id = $info->FinalTestExamResultID;
    $remarks = $info->TestingExamRemarks;

} else if($form_class == 'exam-medical-form'){
    $ac_date = !empty($info->MedicalActualSched) ? date('m/d/Y', strtotime($info->MedicalActualSched)) : date('m/d/Y');
    $s_date = !empty($info->MedicalSignedDate) ? date('m/d/Y', strtotime($info->MedicalSignedDate)) : date('m/d/Y');
    $result_id = $info->FinalMedicalResultID;
    $remarks = $info->MedicalResults;
}
?>
<!-- BEGIN FORM-->
<style media="screen">
textarea {
    resize: vertical;
}
</style>
<form class="horizontal-form {{ $form_class }}">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img class="img-responsive" style="width: 150px; margin:0 auto;" src="{{ url('/general/getStudentPhoto?AppNo='. $info->AppNo.'&date='.date('m/d/Yh:i:s'))}}" />
                <hr style="marg:10px 0;">
                @if(!empty($info))
                <p class="text-center text-muted">
                    <b >{{ ucwords(strtolower($info->ApplicantName)) }}</b><br>{{ $info->YearLevelName.' ('.$info->ProgName.')' }}
                </p>
                @endif
            </div>
            <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="act-sched-date" class="control-label">Actual Schedule Date</label>
                        <div class="input-icon">
                            <i class="fa fa-calendar"></i>
                            <input type="text" class="form-control date-picker" name="act-sched-date" id="act-sched-date" value="{{ !empty($ac_date) ? $ac_date : date('m/d/Y') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="signed-date" class="control-label">Signed Date</label>
                        <div class="input-icon">
                            <i class="fa fa-calendar"></i>
                            <input type="text" class="form-control date-picker" name="signed-date" id="signed-date" value="{{ !empty($s_date) ? $s_date : date('m/d/Y') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="radio-list">
                        @if(!empty($results))

                            @foreach($results as $res)
                            <?php
                            $id = 0; $name = ''; $class = array('1' => 'success', '2' => 'primary', '3' => 'danger');
                            if(isset($res->TestResultID)){
                                $id = $res->TestResultID;
                                $name = $res->TestResultDesc;
                            } else if(isset($res->MedicalResultID)) {
                                $id = $res->MedicalResultID;
                                $name = $res->MedicalResultDesc;
                            }

                            if(!empty($result_id) && $id >= 1){
                                if($id == $result_id){
                                    $checked = 'checked';
                                } else {
                                    $checked = '';
                                }
                            } else if($id == 1){
                                $checked = 'checked';
                            } else {
                                $checked = '';
                            }
                            ?>
                            @if($id >= 1)
                            <label>
                                <input type="radio" name="result" class="result" id="result" value="{{ encode($id) }}" data-name="{{ $name }}" {{ $checked }}> <span class="text-{{ $class[$id] }}">{{ $name }}</span>
                            </label>
                            @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" name="send-email" id="send-email"> Send email to guardian/parent?</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <?php
                    if(isset($remarks)){
                        $disabled = ($remarks == '') ? 'disabled' : '';
                        $remarks = $remarks;
                    } else {
                        $disabled = 'disabled';
                        $remarks = '';
                    }
                    ?>
                    <div class="form-group">
                        <textarea name="remarks" id="remarks" class="form-control not-required" placeholder="Place remarks here." rows="5" {{ $disabled }}>{{ $remarks }}</textarea>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>
</form>
<!-- END FORM-->
