<?php //$i=1; $stat_classes = array('muted', 'success', 'primary', 'danger');?>
<style>
    table tbody td{
        white-space: nowrap !important;
    }
</style>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table">
    <thead>
        <tr>
            <th>#</th>
            <th>Schedule</th>
            <th>Grade Level</th>
            <th>Program</th>
            <th>Full Name</th>
            <th>Application No.</th>
            <th>Gender</th>
            <th>Age</th>
            <th>Birth Date</th>
            <th>Actual Exam Date</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
