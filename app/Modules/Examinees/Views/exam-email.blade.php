<?php
// error_print($data);
// die();
 ?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{{ $data['label'] }} Exam Email</title>
<style>
</style>
</head>

<body bgcolor="#f6f6f6" style='font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
font-size: 100%;
line-height: 1.6em;
margin: 0;
padding: 0; -webkit-font-smoothing: antialiased;
 height: 100%;
 -webkit-text-size-adjust: none;
 width: 100% !important;'>

<!-- body -->
<table class="body-wrap" style="padding: 20px;
width: 100%;" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td class="container" style="padding: 20px; clear: both !important;
    display: block !important;
    Margin: 0 auto !important;
    max-width: 600px !important; border: 1px solid #f0f0f0;" bgcolor="#FFFFFF">

      <!-- content -->
      <div class="content" style=" display: block;
       margin: 0 auto;
       max-width: 600px;">
      <table width="100%">
        <tr>
          <td>
            <h3 style='font-size: 22px; color: #111111;
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 200;
            line-height: 1.2em;
            margin: 40px 0 10px;'>Hello {{ ucwords(strtolower($data['parent_last_name'])) }}.</h3>
            <p style="font-size: 14px;
            font-weight: normal;
            margin-bottom: 10px;">These are the result of the {{ $data['label'] }} exam of your child, <strong>{{ ucwords(strtolower($data['applicant_name'])) }}</strong></p>
            <p style="font-size: 14px;
            font-weight: normal;
            margin-bottom: 10px;">
                Status: <strong><span style="color:{{ $data['status_color'] }} ">{{ $data['status'] }}</span></strong><br>
                Remarks: {{ ($data['status_id'] == 1) ? 'None.' : $data['remarks'] }}<br>
                Actual Schedule Date: {{ $data['actual_sched_date'] }}<br>
                Signed Date: {{ $data['signed_date'] }}
            </p>
          </td>
        </tr>
      </table>
      </div>
      <!-- /content -->

    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->

</body>
</html>
