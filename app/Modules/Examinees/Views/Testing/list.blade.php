<?php $i=1; $stat_classes = array('muted', 'success', 'primary', 'danger'); ?>
<style>
    table tbody td{
        white-space: nowrap !important;
    }
</style>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table">
    <thead>
        <tr>
            <th>#</th>
            <th>Schedule</th>
            <th>Grade Level</th>
            <th>Program</th>
            <th>Full Name</th>
            <th>Application No.</th>
            <th>Gender</th>
            <th>Age</th>
            <th>Birth Date</th>
            <th>Actual Exam Date</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($table))
            @foreach($table as $data)
            <tr>
                <td>{{ $i }}</td>
                <?php $sched = explode(' ', $data->TestingSched, 2);?>
                <td>{{ $sched[0] }}<br>{{ $sched[1] }}</td>
                <td>{{ $data->YearLevelName }}</td>
                <td>{{ $data->ProgName }}</td>
                <td>
                    <a href="#" onclick="return false;" class="a-select" data-id="{{ encode($data->AppNo) }}">
                        {{ ucwords(strtolower($data->ApplicantName)) }}
                    </a>
                </td>
                <td>{{ $data->AppNo }}</td>
                <td>{{ $data->Gender }}</td>
                <td>{{ round($data->Age, 2) }}</td>
                <td>{{ date('M. j, Y', strtotime($data->DateOfBirth)) }}</td>
                <td>{{ !empty($data->TestingActualSched) ? date('m/d/Y', strtotime($data->TestingActualSched)) : '' }}</td>
                <td>
                    <span class="text-{{ $stat_classes[$data->FinalTestExamResultID] }}" id="stat">
                        {{ $data->TestResultDesc }}
                    </span>
                </td>
            </tr>
            <?php $i++;?>
            @endforeach
        @endif
    </tbody>
</table>
