<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List
                </div>
                <div class="actions">
                    <div class="form-inline">
                        <div class="form-group">
                            <select name="search-ac-year" id="search-ac-year" value="" class="form-control select2">
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                    <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear }}</option>
                                @endforeach
                            @else
                            <option value="">No selection.</option>
                            @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control select2" name="search-campus" id="search-campus">
                                @if(!empty($campus))
                                    @foreach($campus as $_campus)
                                    <option value="{{ encode($_campus->CampusID) }}">{{ $_campus->ShortName }}</option>
                                    @endforeach
                                @else
                                <option value="">No selection.</option>
                                @endif
                            </select>
                        </div>
                    </div>
				</div>
            </div>
            <div class="portlet-body table_wrapper">
                @include($views.'list')
            </div>
        </div>
    </div>
</div>
