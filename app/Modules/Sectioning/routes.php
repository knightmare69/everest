<?php
	Route::group(['prefix' => 'enrollment', 'middleware' => 'auth'], function () {
	    Route::group(['prefix' => 'sectioning', 'middleware' => 'auth'], function () {
	        Route::get('/', 'ClassSectioning@index');
	        Route::post('event', 'ClassSectioning@event');
	    });
   });
