<?php

namespace App\Modules\Sectioning\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Sectioning\Services\SectioningServiceProvider as Services;
use App\Modules\Sectioning\Models\ClassSchedules as ClassSectioningModel;
use App\Modules\Students\Services\StudentProfileServiceProvider as StudProfile_Servc;

use Request;
use Response;
use Permission;

class ClassScheduling extends Controller
{
    private $media = [
            'Title' => 'Class Scheduling',
            'Description' => 'arrange the students to their respective section.',
            'js' => ['Sectioning/class-sectioning'],
            'init' => ['Metronic.init()'],
            'plugin_js' => [
                    'bootbox/bootbox.min',
                    'bootstrap-select/bootstrap-select.min',
                    'datatables/media/js/jquery.dataTables.min',
                    'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                    'datatables/extensions/Scroller/js/dataTables.scroller.min',
                    'datatables/plugins/bootstrap/dataTables.bootstrap',
                    'select2/select2.min',
                    'jstree/dist/jstree.min',
            ],
            'plugin_css' => [
                    'select2/select2',
                    'datatables/plugins/bootstrap/dataTables.bootstrap',
                    'jstree/dist/themes/default/style.min',
            ],
        ];

    private $url = ['page' => 'enrollment/scheduling', 'form' => ''];

    public $views = 'Sectioning.Views.ClassScheduling.';

    public function index()
    {
        $this->initializer();
        if ($this->permission->has('read')) {
            return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'save':
                    if ($this->permission->has('add')) {
                        $validation = $this->services->isValid(Request::all());
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $data = $this->services->post(Request::all());
                            $data['CreationDate'] = systemDate();
                            $data['CreatedBy'] = getUserID();
                            $save = $this->model->create($data);
                            $response = ['error' => false, 'message' => 'Successfully save section!', 'return_id' => encode($save->SectionID)];
                        }
                    }
                    break;
                case 'update':
                    if ($this->permission->has('edit')) {
                        $validation = $this->services->isValid(Request::all(), 'update');
                        if ($validation['error']) {
                        	$response = Response::json($validation);
                        } else {
                        	$data = $this->services->post(Request::all());
                        	$this->model->where('SectionID', decode(Request::get('id')))->update($data);
                        	$response = ['error'=>false,'message'=>'Successfully updated section!'];
                        }
                    }
                    break;
                case 'delete':
                    if ($this->permission->has('delete')) {
                        $id = decode(Request::get('id'));
                        $del = $this->model->removeSection($id);
                        if($del == false){
                            $response = array('error' => true, 'message' => 'Unable to remove this section.');
                        } else {
                            $response = ['error'=> false, 'message'=> 'Successfully removed section'];
                        }
                    }
                    break;
                case 'form':
                    if (empty(Request::get('id'))) {
                        $data = ['includes' => $this->include_in_form()];
                    } else {
                        $data = ['data' => $this->model->find(decode(Request::get('id'))), 'includes' => $this->include_in_form()];
                    }

                    $response = view($this->views.'form', $data);

                    break;
                case 'search':
                    $post = Request::all();
                    $is_sh = (decode($post['prog']) == 29) ? true : false;

                    if(!empty($post['with_section'])){
                        $arr = ['students' => $this->model->getSStudent($post, true), 'senior_high' => $is_sh];
                        $students = view($this->views.'table2', $arr)->render();
                        $response = array('error' => false, 'students' => $students, 'total' => count($arr['students']));
                    } else {
                        $is_second_section = !empty($post['is_second_class']) ? true : false;
                        $data_sections = $this->model->getSections(decode($post['srch-yl']), decode($post['srch-ay']), $is_second_section, decode($post['prog']));
                        $data_schedule = $this->model->getSchedules(decode($post['srch-yl']), decode($post['srch-ay']), decode($post['prog']));
                        $students = view($this->views.'table', ['students' => $this->model->getSStudent($post, false), 'senior_high' => $is_sh])->render();
                        $sections = view($this->views.'sections', ['schedule' => $data_schedule,'sections' => $data_sections, 'is_2nd_section' => $is_second_section])->render();
                        $response = array('error' => false, 'students' => $students, 'sections' => $sections);
                    }
                    break;
                case 'optional':
                    $termid    = decode(Request::get('termid'));
                    $yearlevel = decode(Request::get('termid'));
                    $sectid    = decode(Request::get('section'));
                    $exec      = $this->model->getOptionalSubj($termid,$yearlevel,$sectid);
                    $content   = '';
					$groupid   = 0;
					$icount    = 0;
                    foreach($exec as $r){
					  if($groupid!=$r->GroupID){
					   $content .= '<hr/>';	  
					   $icount   = 0;
					  }	
                      $content .= '<label class="radio-inline"><input type="radio" class="optionaldata" name="optsubj'.$r->GroupID.'" id="optionsRadios1" value="'.$r->SubjectID.'">'.$r->SubjectCode." - ".$r->SubjectTitle.'</label>';
					  if($icount==2){
					   $content .= '<br/>';
                       $icount   = 0;					  
					  }
					  $groupid  = $r->GroupID;
					  $icount++;
                    }
                    
                    if($content!=''){
                      $content = "<div class='col-sm-12'>
                                   <label style='margin-left:-10px;'>Select one on the list:</label>
                                   <div class='radio-list' style='margin-left:15px;'>".$content."</div>
                                  </div>";  
                    }
                    
                    $response = array('error' => false, 'content' => $content);
                    break;              
                case 'transfer':
                    $post = Request::all();

                    if(!empty($post['students'])){
                        $sps = new StudProfile_Servc;
                        $section = decode($post['section']);
                        $term_id = decode($post['term']);

                        if(!empty($post['section'])){
                            $enrolled = $sps->getTotalEnrolledbySection($term_id, $section);
                            $sec_limit = $this->model->where('SectionID', $section)->pluck('Limit');
                            $total_stud = $enrolled + count($post['students']);

                            if($sps->sectionIsFull($term_id, $section)){
                                $response = array('error' => true, 'message' => "Unable to transfer student(s). The seleted section is full.");

                            } else if($total_stud > $sec_limit){
                                $remaining = ($sec_limit - $enrolled);
                                $response = array('error' => true, 'message' => "Unable to transfer student(s). Only ".$remaining." remaining slot(s).");

                            } else {
                                $save = $this->model->studentTransfer($post);
                                $exempt = ((array_key_exists('exempt',$post) && $post['exempt']!='')?$post['exempt']:false);
                                //echo count($post['students'];  
                                for($a = 0; $a < count($post['students']); $a++){
                                    $sps->saveStudentLoad(decode($post['students'][$a]), $section, $exempt);
                                };
                                
                                if($save == true){
                                    SystemLog('Class Sectioning', 'enrollment/sectioning', 'update', 'update student section', $post, '');

                                    $response = array('error' => false, 'message' => 'Successfully transfer student(s) on selected section.');
                                } else {
                                    $response = array('error' => true, 'message' => "There's something wrong transfering students.");
                                }

                            }

                        } else {
                            $save = $this->model->studentTransfer($post);

                            if($save == true){
                                SystemLog('Class Sectioning', 'enrollment/sectioning', 'update', 'update student section', $post, '');

                                $response = array('error' => false, 'message' => 'Successfully transfer student(s) to no section.');
                            } else {
                                $response = array('error' => true, 'message' => "There's something wrong transfering students.");
                            }

                        }



                    } else {
                        $response =  array('error' => true, 'message' => 'Unable to proceed.');
                    }

                    break;
            }
        }

        return $response;
    }

    private function include_in_form()
    {
        return array(
            'grade_level' => $this->services->grade_level(),
            'class_adviser' => $this->services->class_adviser(),
            'buildings' => $this->services->buildings(),
            'rooms' => $this->services->rooms(),
        );
    }

    private function init($key = null)
    {
        $this->initializer();
        $term = decode(Request::get('t'));
        return array(
            'year_level' => $this->services->grade_level(),
            'campus' => $this->services->campus(),
            'academic_year' => $this->services->academic_year(),
            'term' =>$term
        );
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new ClassSectioningModel();
        $this->permission = new Permission('sectioning');
    }
}
