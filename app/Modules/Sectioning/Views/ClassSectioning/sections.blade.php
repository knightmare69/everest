<div id="sections-jstree">
    @if(!empty($sections))
    <ul>
        @foreach($sections as $sec)
			<li data-jstree='{"icon" : "icon-home"}' data-id="{{ encode($sec->SectionID) }}">{{ $sec->SectionName }}</li>
        @endforeach
    </ul>
    @endif
</div>
