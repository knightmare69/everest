<?php //error_print($year_level); die(); ?>
<style media="screen">
    .form-inline > .form-group {
        margin-right: 10px;
    }
</style>
<div class="well well-sm" style="margin-bottom: 5px;">
        <form class="form-inline search-form">
            <div class="form-group">
                <select class="form-control search-selec2" name="srch-campus" id="srch-campus">
                    @if(!empty($campus))
                        @foreach($campus as $cmps)
                        <option value="{{ encode($cmps->CampusID) }}">{{ $cmps->ShortName }}</option>
                        @endforeach
                    @else
                    <option value="">Nothing to show.</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <select class="form-control search-selec2" name="srch-ay" id="srch-ay" placeholder="Select Term">
                    @if(!empty($academic_year))
                        <option value=""></option>
                        @foreach($academic_year as $ay)
                        <option value="{{ encode($ay->TermID) }}" <?= $term == $ay->TermID ? 'selected=""' : '' ?> >{{ $ay->AcademicYear.' '.$ay->SchoolTerm }}</option>
                        @endforeach
                    @else
                    <option value="">Nothing to show.</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <select class="form-control search-selec2" id="srch-yl" placeholder="Select Year Level">
                    @if(!empty($year_level))
                        <option value=""></option>
                        @foreach($year_level as $yl)
                            <?php $semestral = ($yl->ProgID == 29) ? 1 : 0; ?>
                            <option data-id="{{ encode($yl->YLID_OldValue) }}" data-prog="{{encode($yl->ProgID)}}" data-by-sem="{{ $semestral }}" data-col="{{ encode($yl->CollegeID) }}">{{ $yl->YearLevelName }}</option>
                        @endforeach
                    @else
                    <option value="">Nothing to show.</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label><input id="is_second_class" name="is_second_class" class="search-checbox" type="checkbox"> 2nd Class Section?</label>
            </div>
            <div class="form-group">
                <label><input id="is_validated" name="is_validated" class="search-checbox" type="checkbox" checked="checked"> Show validated only?</label>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary btn-sm" id="btn-search"><i class="fa fa-search"></i> Search</button>
            </div>
        </form>
</div>
