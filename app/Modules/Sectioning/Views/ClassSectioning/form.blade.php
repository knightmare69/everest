<?php
if(!empty($data)){
    // error_print($data);
}
 ?>
<form class="section-form">
    <div class="row">
        <div class="col-md-12">
            @include('errors.event')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Grade Level</label><br>
                <span><strong id="section-gl-text" style="margin-left:10px;"></strong></span>
            </div>
            <div class="form-group">
                <label for="section-name">Section Name</label>
                <div class="input-icon">
                    <i class="glyphicon glyphicon-comment"></i>
                    <input type="text" name="section-name" id="section-name" class="form-control" value="{{ !empty($data->SectionName) ? $data->SectionName : ''  }}">
                </div>
            </div>
            <div class="form-group">
                <label for="section-name2">2nd Section Name</label>
                <div class="input-icon">
                    <i class="glyphicon glyphicon-comment"></i>
                    <input type="text" name="section-name2" id="section-name2" class="form-control not-required" value="{{ !empty($data->SectionName_2) ? $data->SectionName_2 : ''  }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="section-limit">Class Limit</label>
                <div class="input-icon">
                    <i class="icon-users"></i>
                    <input type="text" name="section-limit" id="section-limit" class="form-control numberonly" value="{{ !empty($data->Limit) ? $data->Limit : '35'  }}">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Class Adviser</label>
                <select class="form-control not-required" name="section-adviser">
                    @if(!empty($includes['class_adviser']))
                        <option value="">Not yet decided</option>
                        @foreach($includes['class_adviser'] as $ca)
                        <?php
                            $checked = '';
                            if(!empty($data->AdviserID)){
                                if($data->AdviserID == $ca->FacultyID){
                                    $checked = 'selected';
                                } else {
                                    $checked = '';
                                }
                            }
                        ?>
                        <option value="{{ encode($ca->FacultyID) }}" {{ $checked }}>{{ ucwords(strtolower($ca->FullName)) }}</option>
                        @endforeach
                    @else
                    <option value="">Nothing to show.</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="">Building</label>
                <?php
                    $bldg_id = !empty($data->RoomID) ? App\Modules\Sectioning\Services\SectioningServiceProvider::rooms($data->RoomID)->BldgID : 0;
                ?>
                <select class="form-control not-required" name="section-building" id="section-building">
                    @if(!empty($includes['buildings']))
                        <option value="">Not yet decided</option>
                        @foreach($includes['buildings'] as $bldg)
                        <?php $checked = ($bldg_id == $bldg->BldgID) ? 'selected' : ''; ?>
                        <option value="{{ encode($bldg->BldgID) }}" {{ $checked }}>{{ ucwords($bldg->BldgName) }}</option>
                        @endforeach
                    @else
                    <option value="">Nothing to show.</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="">Room</label>
                <select class="form-control not-required" name="section-room" id="section-room">
                    @if(!empty($includes['rooms']))
                        <option value="">Not yet decided</option>
                        @foreach($includes['rooms'] as $rooms)
                        <?php
                            $checked = '';
                            if(!empty($data->RoomID)){
                                if($data->RoomID == $rooms->RoomID){
                                    $checked = 'selected';
                                } else {
                                    $checked = '';
                                }
                            }
                        ?>
                        <option value="{{ encode($rooms->RoomID) }}" {{ $checked }} data-bldg="{{ encode($rooms->BldgID) }}" class="">{{ ucwords($rooms->RoomName) }}</option>
                        @endforeach
                    @else
                    <option value="">Nothing to show.</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label><input type="checkbox" id="check-addnew"> Create new upon save?</label>
            </div>
        </div>
    </div>
</form>
