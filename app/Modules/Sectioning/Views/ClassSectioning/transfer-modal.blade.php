<div class="student-transfer-section">
    <div class="row">
        <div class="col-md-12">
            @include('errors.event')
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="">Transfer student to</label>
                <select class="form-control input-sm" name="transfer-section-options" id="transfer-section-options">
                        <option value="">None</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group pull-right">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue btn_modal green btn-transfer-save"><i class="icon-action-redo"></i> Transfer</button>
            </div>
        </div>
    </div>

    <hr class="divider">
    <div class="row hidden">
        <div class="col-md-12">
          <div class="form-group optsubj">
            
          </div>
        </div>
    </div>
    
    <hr class="divider">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover table_scroll dataTable" id="transfer-table" style="min-width:900px">
                <thead>
                    <tr>
                        <th class="hide"><label><input type="checkbox" id="check-parent"></label></th>
                        <th>Reg. ID</th>
                        <th>Student No.</th>
                        <th class="th-full-name">Full Name</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>Date Validated</th>
                        <th>Validated By</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
