
<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="no-section-tbl">
    <thead>
        <tr>
            <th><label><input type="checkbox" id="check-parent"></label></th>
            <th>Reg. ID</th>
            <th>Student No.</th>
            <th>Full Name</th>
            <!-- @if(!empty($senior_high))
            <th>Strand/Track</th>
            @endif -->
            <th>Gender</th>
            <th>Age</th>
            <th>Date Validated</th>
            <th>Validated By</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($students))
            @foreach($students as $data)
            <tr>
                <td><label><input type="checkbox" name="students[]" value="{{ encode($data->REGID) }}" class="check-child"></label></td>
                <td>{{ $data->REGID }}</td>
                <td>{{ $data->{"STUDENT NO."} }}</td>
                <td>{{ ucwords(strtolower($data->FULLNAME)) }}</td>
                <!-- @if(!empty($senior_high))
                <td>{{ $data->{"STRAND/TRACK"} }}</td>
                @endif -->
                <td>{{ $data->GENDER }}</td>
                <td>{{ round($data->AGE, 2) }}</td>
                <td>
                    @if(!empty($data->{"VALIDATION DATE"}) || $data->{"VALIDATION DATE"} != '')
                        {{ date('M. d, Y (g:i A)', strtotime($data->{"VALIDATION DATE"})) }}
                    @endif
                </td>
                <td>{{ $data->{"VALIDATED BY"} }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
