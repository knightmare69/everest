
<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="no-section-tbl">
    <thead>
        <tr>
            <th><label><input type="checkbox" id="check-parent"></label></th>
            <th>Reg. ID</th>
            <th>Student No.</th>
            <th>Full Name</th>
            <!-- @if(!empty($senior_high))
            <th>Strand/Track</th>
            @endif -->
            <th>Gender</th>
            <th>Age</th>
            <th>Date Validated</th>
            <th>Validated By</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($students))
            @foreach($students as $data)
            <tr>
                <td><label><input type="checkbox" name="students[]" value="{{ encode($data->RegID) }}" class="check-child"></label></td>
                <td>{{ $data->RegID }}</td>
                <td>{{ $data->StudentNo }}</td>
                <td>{{ ucwords(strtolower($data->Fullname)) }}</td>
                <!-- @if(!empty($senior_high))
                <td>{{ $data->{"STRAND/TRACK"} }}</td>
                @endif -->
                <td>{{ $data->Gender }}</td>
                <td>{{ round($data->Age, 2) }}</td>
                <td>
                    @if(!empty($data->ValidationDate) || $data->ValidationDate != '')
                        {{ date('M. d, Y (g:i A)', strtotime($data->ValidationDate)) }}
                    @endif
                </td>
                <td>{{ $data->ValidatedBy }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
