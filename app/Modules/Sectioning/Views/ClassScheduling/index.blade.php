<style >
.jstree-anchor{
    padding-right: 10px;
}
.jstree-icon{
    color: #2a6496;
}
table tbody td{
    white-space: nowrap !important;
}
</style>
<div class="row">
    <div class="col-md-12">
        @include($views.'search-main')
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="portlet box green-haze" id="try">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cubes"></i>Schedules
                </div>
                <div class="actions hide" id="div-sec-action">
                    <div class="btn-group">
						<a class="btn btn-sm btn-default" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
    						<i class="fa fa-reorder"></i>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li>
								<a href="javascript:;" id="section-new">
								<i class="fa fa-cube"></i> New </a>
							</li>
							<li>
								<a href="javascript:;" id="section-modify">
								<i class="fa fa-pencil"></i> Modify </a>
							</li>
							<li>
								<a href="javascript:;" id="section-remove">
								<i class="fa fa-trash-o"></i> Remove </a>
							</li>
						</ul>
					</div>
                </div>
            </div>
            <div class="portlet-body table_wrapper" id="section-result-holder" style="overflow-y: auto;">
                @include($views.'sections')
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> List of Students
                </div>
            </div>
            <div class="portlet-body table_wrapper">
                <div class="tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#student-result-holder1" data-toggle="tab" aria-expanded="true">Not In Schedule</a>
                        </li>
                        <li><a href="#student-result-holder2" data-toggle="tab" aria-expanded="true">In Schedule</a></li>
                        <li class="pull-right">
                            <button type="button" id="section-student-transfer" data-tab-open="" class="btn btn-primary btn-sm" style="margin-top:5px;">
                                <i class="fa fa-check-square"></i> Transfer Selected
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="student-result-holder1">
                                @include($views.'table')
                        </div>
                        <div class="tab-pane fade in" id="student-result-holder2">
                                @include($views.'table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="hidden-transfer-modal">
    @include($views.'transfer-modal')
</div>
