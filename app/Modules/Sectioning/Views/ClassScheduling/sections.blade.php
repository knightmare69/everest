<div id="sections-jstree">
	@if(!empty($sections) && 1==0)
    <ul>
        @foreach($sections as $sec)
			<li data-jstree='{"icon" : "icon-home"}' data-id="{{ encode($sec->SectionID) }}">{{ $sec->SectionName }}</li>
        @endforeach
    </ul>
    @endif
    @if(!empty($schedule))
    <ul>
        @foreach($schedule as $sch)
			<li data-jstree='{"icon" : "icon-home"}' data-id="{{ encode($sch->ScheduleID) }}">{{ $sch->SectionName.' - '.$sch->SubjectTitle }}</li>
        @endforeach
    </ul>
    @endif
</div>
