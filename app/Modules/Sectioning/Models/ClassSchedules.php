<?php

namespace App\Modules\Sectioning\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class ClassSchedules extends Model
{
    protected $table = 'ES_ClassSchedule';
    protected $primaryKey = 'ScheduleID';

    protected $fillable = array('TermID','SectionID','SubjectID','FacultyID','Limit','IsSpecialClass');

    public $timestamps = false;

	public function getStudent($post, $with_section)
	{
		$is_second_sec = !empty($post['is_second_class']) ? 1 : 0;
		$is_valid = !empty($post['is_validated']) ? 1 : 0;

		if($with_section){
            $post = array(decode($post['section']), decode($post['srch-yl']), decode($post['prog']), $is_valid, $is_second_sec);
            $get = DB::select('exec sp_K12_GetStudentList_GSHSClassSectioning_ThisClassSection ?, ?, ?, ?, ?', $post);
		} else {
            $post = array(decode($post['srch-campus']), decode($post['srch-ay']), decode($post['srch-yl']), decode($post['prog']), $is_valid, $is_second_sec);
            $get = DB::select('exec sp_K12_GetStudentList_GSHSClassSectioning ?, ?, ?, ?, ?, ?', $post);
		}
		return $get;
	}

	public function getSStudent($post, $with_section)
	{
		$is_second_sec = !empty($post['is_second_class']) ? 1 : 0;
		$is_valid = !empty($post['is_validated']) ? 1 : 0;

		if($with_section && $post['section']!=''){
            $sql = "SELECT  DISTINCT R.RegID ,
							R.StudentNo ,
							dbo.fn_StudentName(R.StudentNo) AS Fullname ,
							S.Gender ,
							dbo.fn_Age(S.DateOfBirth, GETDATE()) AS Age ,
							R.ValidationDate ,
							dbo.fn_EmployeeName(R.ValidatingOfficerID) AS ValidatedBy ,
							R.ORNo ,
							( CASE WHEN S.Father <> ''
								   THEN '(Father) ' + dbo.Proper(S.Father) + ', '
								   ELSE ''
							  END ) + ( CASE WHEN S.Mother <> ''
											 THEN '(Mother) ' + dbo.Proper(S.Mother)
											 ELSE ''
										END ) + ( CASE WHEN S.Guardian <> ''
														THEN ', (Guardian) ' + dbo.Proper(S.Guardian)
														ELSE ''
							END ) AS Parents
					FROM    ES_Students AS S
							INNER JOIN ES_Registrations AS R ON S.StudentNo = R.StudentNo
							INNER JOIN ES_RegistrationDetails AS RD ON R.RegID=RD.RegID
					WHERE   RD.ScheduleID='".decode($post['section'])."'";
            $get = DB::select($sql);
		} else {
		    $sql  = "SELECT DISTINCT R.RegID ,
							R.StudentNo ,
							dbo.fn_StudentName(R.StudentNo) AS Fullname ,
							S.Gender ,
							dbo.fn_Age(S.DateOfBirth, GETDATE()) AS Age ,
							R.ValidationDate ,
							dbo.fn_EmployeeName(R.ValidatingOfficerID) AS ValidatedBy ,
							R.ORNo ,
							( CASE WHEN S.Father <> ''
								   THEN '(Father) ' + dbo.Proper(S.Father) + ', '
								   ELSE ''
							  END ) + ( CASE WHEN S.Mother <> ''
											 THEN '(Mother) ' + dbo.Proper(S.Mother)
											 ELSE ''
										END ) + ( CASE WHEN S.Guardian <> ''
														THEN ', (Guardian) ' + dbo.Proper(S.Guardian)
														ELSE ''
							END ) AS Parents
					FROM    ES_Students AS S
							INNER JOIN ES_Registrations AS R ON S.StudentNo = R.StudentNo
							INNER JOIN ES_RegistrationDetails AS RD ON R.RegID=RD.RegID
					WHERE   ( ( CASE WHEN ".$is_valid." = 0 THEN 1 ELSE ( CASE WHEN R.ValidationDate IS NOT NULL THEN 1 ELSE 0 END ) END ) = ( CASE WHEN ".$is_valid." = 0 THEN 1 ELSE ".$is_valid." END ) )
							AND ( R.IsWithdrawal = 0 ) AND (r.YearLevelID = ".decode($post['srch-yl']).") AND (r.ProgID = '".decode($post['prog'])."')";
            $post = array(decode($post['srch-campus']), decode($post['srch-ay']), decode($post['srch-yl']), decode($post['prog']), $is_valid, $is_second_sec);
            $get  = DB::select($sql);
		}
		return $get;
	}
	
    public function getSchedules($year_level_id,$academic_year,$prog_id){
        if($prog_id == 29 && $year_level_id == 5){
            $year_level_id = [5, 1];

        } else if($prog_id == 29 && $year_level_id == 5){
            $year_level_id = [6, 2];
        } else {
            $year_level_id = [$year_level_id];
        }

		$get = DB::table('ES_ClassSchedules')
		            ->join('ES_ClassSections','ES_ClassSchedules.SectionID','=','ES_ClassSections.SectionID')
		            ->join('ES_Subjects','ES_ClassSchedules.SubjectID','=','ES_Subjects.SubjectID')
		            ->select('ScheduleID','ES_ClassSections.SectionID','ES_ClassSections.SectionName','ES_Subjects.SubjectID','SubjectTitle','SubjectCode')
                    ->where(['ES_ClassSchedules.TermID' => $academic_year,])					
                    ->get();
		return $get;
	
	}
	
	public function getSections($year_level_id, $academic_year, $is_second_sec, $prog_id)
	{
        $_2nd_sec = ($is_second_sec == true) ? 1:0;

        if($prog_id == 29 && $year_level_id == 5){
            $year_level_id = [5, 1];

        } else if($prog_id == 29 && $year_level_id == 5){
            $year_level_id = [6, 2];
        } else {
            $year_level_id = [$year_level_id];
        }

		$get = DB::table('ES_ClassSections')->select('SectionID', 'SectionName', 'SectionName_2', 'Is2ndSection')
                    ->where(['TermID' => $academic_year, 'Is2ndSection' => $_2nd_sec, 'ProgramID' => $prog_id])
                    ->whereIn('YearLevelID', $year_level_id)
                    ->get();
		return $get;
	}

    public function removeSection($id)
    {
        $find = DB::table('ES_Registrations')->select('RegID')->where('ClassSectionID', $id)->first();

        if(!empty($find)){
            return false;
        } else {
            $this->destroy($id);
            return true;
        }
    }

    public function studentTransfer($post){
        for($a = 0; $a < count($post['students']); $a++){
             $post['students'][$a] = decode($post['students'][$a]);
        };

        $section_id = !empty($post['section']) ? decode($post['section']) : null;

        if(!empty($post['is_second_sec'])){
            $update = DB::table('ES_Registrations')->whereIn('RegID', $post['students'])->update(array('ClassSectionID_2' => $section_id, 'ClassSectionID' => null));
        } else {
            $update = DB::table('ES_Registrations')->whereIn('RegID', $post['students'])->update(array('ClassSectionID' => $section_id, 'ClassSectionID_2' => null));
        }

        return $update;
    }

    public function studentSectionID($reg_id){
        $section_id = DB::table('ES_Registrations')->where('RegID', $reg_id)->pluck('ClassSectionID');
        return $section_id;
    }
    
    public function getOptionalSubj($termid,$yearlevel,$sectionid){
        $qry = "SELECT s.SubjectID
                      ,s.SubjectCode
                      ,s.SubjectTitle 
                      ,os.GroupID
                      ,os.GroupOpt
                  FROM ES_ClassSections as sec
            INNER JOIN ES_ClassSchedules as cs ON sec.SectionID=cs.SectionID
            INNER JOIN ES_Subjects as s ON cs.SubjectID=s.SubjectID
            INNER JOIN ESv2_OptionalSubjects as os ON cs.SubjectID=os.SubjectID AND s.SubjectID=os.SubjectID
                 WHERE cs.TermID       = '".$termid."'
                   AND sec.SectionID   = '".$sectionid."'
              ORDER BY os.GroupID,os.GroupOpt,s.SubjectID";
        $rs =  DB::select($qry);
        return $rs;     
    }

}
