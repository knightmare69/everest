<?php

namespace App\Modules\Sectioning\Services\Sectioning;

use Validator;

class Validation
{
    public function validatePost($post)
    {
        $has_id = !empty($post['section-data-id']) ? '' : 'SectionID';

        $data = array(
            'section-name' => $post['section-name'],
            '2nd-name' => !empty($post['section-name2']) ? $post['section-name2'] : null,
            'limit' => $post['section-limit'],
            'grade-level' => decode($post['section-grade-level']),
            'adviser' => decode($post['section-adviser']),
            'building' => decode($post['section-building']),
            'room' => decode($post['section-building']),
            'prog-id' => decode($post['prog']),
            'college-id' => decode($post['college']),
            'campus-id' => decode($post['campus']),
            'term-id'=> decode($post['term'])
        );

        $rules = array(
            // 'section-name' => 'required|string|unique:ES_ClassSections,SectionName,NULL,'.$has_id.',YearLevelID,'.decode($post['section-grade-level']),
            '2nd-name' => 'string',
            'limit' => 'required|numeric',
            'grade-level' => 'required|integer',
            'adviser' => 'string',
            'building' => 'integer',
            'room' => 'integer',
            'prog-id' => 'required|integer',
            'college-id' => 'required|integer',
            'campus-id' => 'required|required',
            'term-id'=> 'required|required'
        );

        if($post['event'] !== 'update'){
            $rules['section-name'] = 'required|string|unique:ES_ClassSections,SectionName,NULL,'.$has_id.',YearLevelID,'.decode($post['section-grade-level']);
        }

        $validator = validator::make($data, $rules);

        return $validator;
    }
}
