<?php

namespace App\Modules\Sectioning\Services;

use App\Modules\Sectioning\Services\Sectioning\Validation;
use DB;

class SectioningServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'save')
    {
        if ($action == 'save') {
            $validate = $this->validation->validatePost($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else {
            $validate = $this->validation->validatePost($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }

        return ['error' => false, 'message' => ''];
    }

    public function post($post)
    {
        $set_post = array(
            'SectionName' => $post['section-name'],
            'TermID' => decode($post['term']),
            'CampusID' => decode($post['campus']),
            'CollegeID' => decode($post['college']),
            'ProgramID' => decode($post['prog']),
            // 'ProgClass' => decode($post['prog-class']),
            'YearLevelID' => decode($post['section-grade-level']),
            'CurriculumID' => null,
            'IsBlock' => 1,
            'RoomID' => !empty($post['section-room']) ? decode($post['section-room']) : null,
            'AdviserID' => !empty($post['section-adviser']) ? decode($post['section-adviser']) : '',
            'Limit' => $post['section-limit'],
        );

        if(!empty($post['section-name2'])){
            $set_post['SectionName_2'] = $post['section-name2'];
            $set_post['Is2ndSection'] = 1;
        }

        return $set_post;
    }

    public function grade_level()
    {
        $return_arr = array();
        $get_yl = DB::table('ESv2_YearLevel')
                ->select('YearLevelID', 'YearLevelName','YLID_OldValue', 'ProgClass', 'ProgID', 'SeqNo', DB::raw('dbo.fn_ProgramCollegeID(ProgID) as CollegeID'))
                //->where('ProgClass', '!=', 50)
                ->get();

        /*
        $programs = collect($get_yl)->groupBy('ProgClass')->toArray();

        $pluck_prog_class = array_pluck($get_yl, 'ProgClass');
        $prog_class_ids = array_values(array_unique($pluck_prog_class, SORT_NUMERIC));

        $get_prog_class = DB::table('ES_Program_Class as pc')->select('pc.ClassCode', 'pc.ClassDesc', 'ep.ProgID', 'ep.CollegeID')->whereIn('ClassCode', $prog_class_ids)->join('ES_Programs as ep', 'ep.ProgClass', '=', 'pc.ClassCode')->orderBy('pc.ClassCode', 'ASC')->get();

        foreach($get_prog_class as $pc){
            $return_arr[] = array(
                'ProgClassName' => $pc->ClassDesc,
                'ProgClassID' => $pc->ClassCode,
                'ProgID' => $pc->ProgID,
                'CollegeID' => $pc->CollegeID,
                'Programs' => $programs[$pc->ClassCode]
            );
        }
        */
        return $get_yl;
    }

    public function academic_year()
    {
        // $get = DB::table('vw_K12_AcademicYears')->where('Active_OnlineEnrolment', 1)->orderBy('AcademicYear', 'DESC')->get();
        //$get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')->where(['Active_OnlineEnrolment' => 1])->orderBy('AcademicYear', 'DESC')->get();
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')->where(['Hidden' => 0])->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function campus()
    {
        $get = DB::table('ES_Campus')->select('CampusID', 'Acronym', 'ShortName')->get();

        return $get;
    }

    public function class_adviser()
    {
        $get = DB::table('Esv2_Users')->select('UserIDX', 'FullName', 'FacultyID')->whereBetween('UsersGroupID', array(2, 11))->get();

        return $get;
    }

    public function buildings()
    {
        $get = DB::table('ES_Buildings')->select('BldgID', 'BldgName', 'BldgOtherName')->get();

        return $get;
    }

    public static function rooms($room_id = 0)
    {
        $db = DB::table('ES_Rooms')->select('RoomID', 'RoomName', 'RoomNo', 'BldgID');

        $get = !empty($room_id) ? $db->where('RoomID', $room_id)->first() : $db->get();

        return $get;
    }
}
