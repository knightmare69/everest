<?php

namespace App\Modules\Cashier\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Cashier\Services\CashierServices as services;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF;

use Permission;
use Request;
use Response;
use DB;

class OfficialReceipts extends Controller
{
    protected $ModuleName = 'cashiering';

    private $media = [
            'Title' => 'Official Receipts',
            'Description' => 'Welcome to official receipt module',
            'js' => ['Cashier/receipt.js?v=1'],
            'css' => ['profile'],
            'init' => ['Receipts.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                           ,'bootstrap-datepicker/js/bootstrap-datepicker'
                           ,'bootstrap-timepicker/js/bootstrap-timepicker.min'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification'
                ,'bootstrap-datepicker/css/datepicker3'
                ,'bootstrap-timepicker/css/bootstrap-timepicker.min'
            ],
        ];

    private $url = ['page' => 'cashier/official-receipts'];

    private $views = 'Cashier.Views.OfficialReceipts.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = [
                'views' => $this->views
               ,'data' => $this->getData()
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode='', $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";
        $current_year = systemDate('Y');
        $current_month = systemDate('m');
        
        $start = Request::get('start'); 
        $end = Request::get('end'); 
        $payer = Request::get('payer');
        $or_start = Request::get('orbegin');
        $or_end = Request::get('orend');
        
        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;        
        
        if($start == ''){
            $whereRaw = "Month(date) = '{$current_month}' AND Year(Date) = '{$current_year}' ";    
        }else{
            $whereRaw = " (date between '{$start}' AND '{$end} 23:59:59') ";
        }
        
        if($payer != ''){
            $whereRaw .= " AND (PayorName like '%". trimmed($payer) ."%') ";
        }
        
        if($or_start != '' && $or_end != '' ){
            $whereRaw .= " AND (ORNo between '{$or_start}' and '{$or_end}') ";
        }
        
        
      
       if($mode == 'total'){
            $rs =  DB::table('es_officialreceipts As a')                        
                    ->whereRaw($whereRaw)
                    ->count();
        }else{
              $rs = DB::table('es_officialreceipts')
                 ->selectRaw("*, fn_CurrencyCode(CurrencyID) As Curr, fn_TotalReceiptAmount('20',ORNo) As Descripancy ")
                 ->whereRaw($whereRaw)
                 ->orderBy('ORNo','ASC')
                 ->skip($skip)
                 ->take($take)
                 ->get();
        }
        

        return $rs;
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        
        if (Request::ajax()) {
        
            $this->initializer();
        
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) 
            {
                 case  'delete':                
                
                    $or_num = $post['ornum'];
                    $data =  $this->services->deleteReceipt($or_num);
                    if($data =='1'){
                        $response = ['error'=>false,'message'=>'success', 'data' => $data ];    
                    }else{
                        $response = ['error'=>true,'message'=>'error', 'data' => $data ];
                    }
                                        
                break;
                
                case  'void':                
                
                    $or_num = $post['ornum'];
                    $data =  $this->services->voidReceipt($or_num);
                    if($data =='1'){
                        $response = ['error'=>false,'message'=>'success', 'data' => $data ];    
                    }else{
                        $response = ['error'=>true,'message'=>'error', 'data' => $data ];
                    }
                                        
                break;
                
                case 'orsearch':
                    $lst = view($this->views.'list',  ['data' =>  $this->getData() ] )->render(); 
                    $response = ['error'=>false,'message'=>'success', 'view' => $lst ];   
                break;           
                
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }
    
    private function initializer()
    {
        $this->permission = new Permission($this->ModuleName);
        $this->services = new services;        
    }
}