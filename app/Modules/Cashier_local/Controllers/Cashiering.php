<?php
/*
    created by   : ARS
    Description  : This module is for cash collection of the system.
    Release Date : TBA
    Version      : 1.0
    Update Notes :

            2018.02.14 13:31H  -  Activate Fees Selection Templates
            2018.02.16 12:23H  -  Activate Schedule of payment

*/

namespace App\Modules\Cashier\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Cashier\Services\CashierServices as services;
use App\Modules\Accounting\Services\AccountingServiceProvider as sFinance;

use App\Libraries\PDF;

use Permission;
use Request;
use Response;
use DB;

class Cashiering extends Controller
{
    protected $ModuleName = 'cashiering';

    private $media = [
            'Title' => 'Cashiering',
            'Description' => 'Welcome to cashiering module.',
            'js' => ['Cashier/cashier.js?v=1.26'],
            'css' => ['profile'],
            'init' => ['Cashiering.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                           ,'bootstrap-datepicker/js/bootstrap-datepicker'
                           ,'bootstrap-timepicker/js/bootstrap-timepicker.min'
                        ],
             'plugin_css' =>
             [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification'
                ,'bootstrap-datepicker/css/datepicker3'
                ,'bootstrap-timepicker/css/bootstrap-timepicker.min'
             ],
        ];

    private $url = ['page' => 'cashier/cashiering'];

    private $views = 'Cashier.Views.Cashiering.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));
            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = ['views' => $this->views];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode,$term, $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;
        if($term != ''){

            $whereRaw = "a.TermID = {$term} ";
           // $whereRaw .= " AND r.ProgID IN (".implode(getUserProgramAccess(),',') .")";

            $query = "SELECT a.*, fn_CurrencyCode(a.CurrencyID) As Curr
                      FROM  es_tableoffees a
                      WHERE " . $whereRaw . " Order By TemplateCode ASC
                      OFFSET {$skip} ROWS
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('es_tableoffees As a')
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }
        }

        return $rs;
    }

    public function view(){
        $this->initializer();
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));
            $ref = decode(Request::get('ref'));

            $_incl = [
                'views' => $this->views
                ,'term' => $term
                ,'ref' => ($ref)
            ];

            SystemLog( $this->media['Title'] ,'','Page View','page-view','','' );
            return view('layout',array('content'=>view($this->views.'view')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];

        if (Request::ajax()) {

            $this->initializer();

            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {

                case 'get-fees':

                    $idno = decode($post['idno']);
                    $term = decode($post['term']);
                    $fee = decode($post['fee']);
                    $refno = decode($post['refno']);
                    $txn = ($post['txn']);
                    $type = '1';

                    $data = sFinance::createAssessment($term, $idno,$txn, $refno, $fee);

                    if ($data['err']){
                        $response = ['error'=>true,'message'=>$data['msg']];
                    }else{
                        $response = ['error'=>false,'message'=>'success'];
                    }


                break;

                case 'fees':

                    $idno = decode($post['idno']);
                    $term = decode($post['term']);

                    $data = sFinance::getFeesTemplatebyStudent($term, $idno);

                    $view =  view($this->views.'sub.fees',  ['data' => $data ] )->render();

                    $response = ['error'=>false,'message'=>'success', 'list' => $view ];

                break;

                case 'orsearch':

                    $date = systemDate('Y-m-d');

                    $where = "";

                    if ($this->permission->has('search-view-all')) {
                        $where = "";
                    }

                    $search = trimmed($post['search']);

                    $qry = "SELECT *, fn_TotalReceiptAmount('20',ORNo) As Descripancy FROM es_officialreceipts
                            WHERE (Date Between '{$date} 00:00:00' AND '{$date} 23:59:59')
                                AND  ORNo LIKE '%{$search}%' LIMIT 50 ";

                    $data = DB::select($qry);

                    $view =  view($this->views.'sub.orlist',  ['data' => $data ] )->render();

                    $response = ['error'=>false,'message'=>'success', 'list' => $view ];

                break;

                case 'get-schedule':
                    $idno = $post['idno'];
                    $type = $post['type'];
                    $txn = $post['txn'];
                    $ref = decode($post['ref']);

                    if($idno != '' ){

                        $qry = "SELECT j.*, a.AcctName, a.AcctCode
                                FROM es_journals j
                                LEFT JOIN es_accounts a on j.AccountID=a.AcctID
                                WHERE j.TransID = '{$txn}' AND j.ReferenceNo = '{$ref}'";
                        $data = DB::select($qry);
                        $dues = DB::table('es_registrations')->where('RegID', $ref)->first();

                        $schedule = view($this->views.'sub.schedule',  ['data' => $data,'dues'=> $dues  ] )->render();;


                        $response = ['error'=>false,'message'=>'success', 'schedule' => $schedule];

                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }


                break;

                case 'get-payment-schedule':

                    $idno = decode($post['idno']);
                    $type = $post['type'];
                    $txn = $post['txn'];
                    $col = $post['col'];
                    $ref = ($post['refno']);

                    if($idno != '' ){

                       $scheme = array(

                            1 => '1st Payment',
                            2 => '2nd Payment',
                            3 => '3rd Payment',
                            4 => '4th Payment',
                            5 => '5th Payment',
                            6 => '6th Payment',
                            7 => '7th Payment',
                            8 => '8th Payment',
                            9 => '9th Payment',
                            10 => '10th Payment',
                        );
                        if($col == 0){

                            $qry = "SELECT J.*, a.AcctName, a.AcctCode , Debit-ActualPayment As Due
                                    FROM es_journals j
                                    LEFT JOIN es_accounts a on j.AccountID=a.AcctID
                                    WHERE TransID = '{$txn}' AND ReferenceNo = '{$ref}' AND (Debit-ActualPayment) > 0 ";

                        } else{

                            $qry = "SELECT j.*, a.AcctName, a.AcctCode , `".$scheme[$col]."` As Due
                                    FROM es_journals j
                                    LEFT JOIN es_accounts a on j.AccountID=a.AcctID
                                    WHERE TransID = '{$txn}' AND ReferenceNo = '{$ref}' AND `".$scheme[$col]."` > 0 ";
                        }


                        $data = DB::select($qry);

                        $view =  view($this->views.'schedassess', ['data' => $data]  )->render();
                        $response = ['error'=>false,'message'=>'success', 'view' =>  $view ];

                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }


                break;

                case  'get-refs':

                    $idno = decode($post['idno']);
                    $type = $post['type'];
                    $txn = $post['txn'];
                    $all = $post['all'];

                    if($idno != '' ){

                        $obalance = getOutstandingBalance($type , $idno, systemDate() );
                        $data =  $this->services->setPayer($type, $idno, $txn);
                        $refs =  view($this->views.'sub.refs',['data' =>  $data['refs']])->render();


                        $data['details']['obalance'] = $obalance;
                        if(!empty($data['refs'])){
                            $data['details']['regid'] = $data['refs'][0]->RegID ;
                        }

                        $data['details']['all'] = $all ;

                        $assessment =  view($this->views.'list',  $data['details'] )->render();

                        unset($data['details']);
                        unset($data['refs']);

                        $response = ['error'=>false,'message'=>'success', 'data' => $refs,'assessment' => $assessment , 'ref' => $data, 'obalance' => number_format($obalance,2) ];

                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }

                break;

                case 'verify':

                    $or_num = $post['or_no'];
                    $data =  $this->services->verifyORNo($or_num);
                    if($data =='1'){
                        $response = ['error'=>false,'message'=>'success', 'data' => $data ];
                    }else{
                        $response = ['error'=>true,'message'=>'error', 'data' => $data ];
                    }

                break;

                case 'assessment':

                    $idno = decode($post['idno']);
                    $type = $post['type'];

                    $data = $this->services->getAssessmentDetails($type , $idno);

                    $data['regid'] = $post['refno'];
                    $data['all'] = $post['all'];
                    $data['obalance'] = getOutstandingBalance($type , $idno, systemDate() );

                    $view = view($this->views.'list',  $data )->render();

                    $response = ['error'=>false,'message'=>'success', 'view' => $view];

                break;

                case 'get-payment':

                    $validation = $this->services->validate(Request::all(), 'payment-form' );

                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $view =  view($this->views.'sub.payment', $post )->render();
                        $response = ['error'=>false,'message'=>'success', 'form' => $view];
                    }

                break;

                case 'save-payment':

                    $validation = $this->services->validate(Request::all(), 'checkout' );
                     if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                      // die;

                        $or_num = $post['orno'];
                        ob_clean();
                        if (  $this->services->verifyORNo($or_num) == '1' ){
                            $r = $this->services->saveOR($post);

                            if( isset( $post['validate'])  ){
                                $txn = decode(getObjectValue($post,'txn'));
                                $ref = decode(getObjectValue($post,'ref'));
                                $type = decode(getObjectValue($post,'type'));
                                $idno = decode(getObjectValue($post,'idno'));
                                $orno = (getObjectValue($post,'orno'));

                                sFinance::validateTxn($txn, $ref, $type, $idno, $orno);
                            }

                            #Recompute payment
                            sFinance::RecomputeTotalPayment($txn, $ref, $type, $idno, systemDate());

                            #$view = view('reports.official_receipts.acknowledgement_receipt', array('orno' => $or_num) )->render();
                            $view='';
                            $response = ['error'=>false,'message'=>'official receipt successfully saved!', 'view' => $view];
                        }else{
                            $response = ['error'=>true,'message'=>'official receipt already used!'];
                        }
                    }

                break;

                default: return response('Unauthorized.', 401); break;
            }
        }
        ob_clean();
        return $response;
    }


        public function eprint(){

            $this->pdf = new PDF;
        set_time_limit(0);
            ini_set('memory_limit', '-1');

            $pdf = 0;

            $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(true);
        $this->font = 11;
        //$this->customFont = 'Libraries/pdf/fonts/Calibri.ttf';
            //$this->pdf->SetFont($this->pdf->addTTFfont(app_path($this->customFont)),'',$this->font);

            $this->pdf->SetMargins(50, 50, 50, true);
            $this->pdf->setPageUnit('pt');

            $data = array(
                'orno' => (Request::get('or')),
            );


            if($pdf == 1){

                $this->pdf->AddPage('P','LETTER');
                $this->pdf->setTitle('Print Preview');
                $this->pdf->writeHTML(view('reports.official_receipts.acknowledgement_receipt',$data)->render());

                $this->pdf->output();

            } else {

                echo view('reports.official_receipts.or_1_15',$data)->render();
                // echo view('reports.official_receipts.acknowledgement_receipt',$data)->render();
            }


		set_time_limit(30);
        ini_set('memory_limit', '128M');
    }

    private function initializer()
    {
        $this->permission = new Permission($this->ModuleName);
        $this->services = new services;
    }
}
