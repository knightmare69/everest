<?php

namespace App\Modules\Cashier\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class OfficialReceipts extends Model
{
    protected $table = 'es_officialreceipts';
    protected $primaryKey = 'IndexID';
    
   	protected $fillable  = array(
	   'CampusID'
      ,'ORNo'
      ,'Date'
      ,'PayorType'
      ,'PayorID'
      ,'PayorName'
      ,'Particular'
      ,'CurrencyID'
      ,'AmountDue'
      ,'AmountInWords'
      ,'CashReceive'
      ,'CheckReceive'
      ,'Change' 
      ,'Discount'
      ,'IsVoid'
      ,'DiscountOffsetBackAccount'
      ,'IsAudited'
      ,'ForPosting'
      ,'SecurityCode'
      ,'DateModified'
      ,'ModifiedBy'
       ,'CashierID'
       ,'TransType'
       ,'RefNo'
       ,'SupervisorID'
       ,'CardReceive'      
       ,'CardTypeID'       
       ,'CardNo'
       ,'CardTypeName'
       ,'CardApprovalNo'
       ,'CardAmount'
       ,'CardTypeID2'
       ,'CardNo2'
       ,'CardTypeName2'
       ,'CardApprovalNo2'
       ,'CardAmount2'
       ,'CreatedBy'
       ,'CreatedDate'
       ,'CreatedTerminal'
       ,'ModifiedTerminal'
       
	);

	public $timestamps = false;
    
    
}