<?php

namespace App\Modules\Cashier\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Journals extends Model
{
    protected $table = 'es_journals';
    protected $primaryKey = 'EntryID';
    
   	protected $fillable  = array(
	   'ServerDate'
      ,'TransDate'
      ,'TermID'
      ,'CampusID'
      ,'TransID'
      ,'ReferenceNo'
      ,'Description'
      ,'Particular'
      ,'IDType'
      ,'IDNo'
      ,'Payor'
      ,'AccountID'
      ,'CurrencyID'      
      ,'Debit'
      ,'Credit'
      ,'Remarks'
      ,'UserID'
      ,'DateModified' 
      ,'ModifiedBy'
      ,'Assess Fee'
      ,'Discount'
      ,'1st Payment'
      ,'2nd Payment'
      ,'3rd Payment'
      ,'4th Payment'
      ,'5th Payment'
      ,'6th Payment'
      ,'7th Payment'
      ,'8th Payment'
      ,'9th Payment'
      ,'10th Payment'
       ,'PaymentDiscount'
       ,'ActualPayment'
       ,'Refund'
       ,'ADCRefund'
       ,'DMCMRefNo'
       ,'TransType'      
       ,'TransRefNo'       
       ,'CreditMemo'
       ,'SeqNo'
       ,'SeqNo2'
       ,'NonLedger'
       ,'Deferred'
       ,'SubCodeID'
       ,'DMCode'
       ,'InstallmentExcluded'
       ,'SpecialFee'
       ,'Adjustment'
       ,'SpecialTransNo'
       ,'FinancialAidExternal'
       ,'DeptID'
       ,'DeptCode'
       ,'YrLvlID'
       ,'YrLvlName'
       
	);

	public $timestamps = false;
    
    
}