<?php

namespace App\Modules\Cashier\Services;

use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Cashier\Models\OfficialReceipts As mOR;
use App\Modules\Cashier\Models\Journals As mJournals;
use App\Modules\Cashier\Services\Validation\CashieringValidation;

use DB;
use Request;

class CashierServices
{
    public function __construct()
	{
		$this->validation = new CashieringValidation;
	}

    public function validate($post,$action = '')
	{
	    if($action =='payment-form'){
	       $validate = $this->validation->payment_form($post);
	    }elseif($action =='checkout'){
           $validate = $this->validation->checkout($post);
	    }else{
	       return ['error'=>true,'message'=> 'validation type not found'];
	    }

    	if ($validate->fails())
    	{
        if($post['type'] == 4 || decode($post['type']) == 4){ //Applicant
          return ['error'=>false,'message'=> ''];
        }else if($post['type'] == 5 || decode($post['type']) == 5){ //Other Payer
          return ['error'=>false,'message'=> ''];
        }else{
          return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
        }

    	}
		return ['error'=>false,'message'=> ''];
	}

    public static function setPayer($type, $idno, $txn)
    {
        switch($type){
            case '1': /* Students */
                $return = self::setStudent($idno, $txn);
            break;
            case '2': /* Employee */
                $return = self::setEmployee($idno);
            break;
            case '3': /* Scholarship Provider */
                $return = self::setSchoprovider($idno);
            break;
            case '4': /* Applicant */
                $return = self::setApplicant($idno);
            break;
            case '5': /* Other Payer */
                $return = self::setOtherpayer($idno);
            break;
        }

        return $return;

    }

    public static function setStudent($idno, $txn)
    {
        $model = new mStudents();

        $ret = [
             'IDNo' => $idno
            ,'BadAccount' => 0
            ,'OldAccount' => 0
            ,'refs' => ''
            ,'details' => ''
        ];

        $rs = $model->where('StudentNo', $idno)->first();

        if($rs->BadAccount == '1'){
            $ret['BadAccount'] = 1;
        }

        $qry =  "SELECT  Remarks, (TotalNetAssessed) - (TotalPayment + TotalDiscount + TotalCreditMemo) as Balance
                 from es_othertransactions WHERE TransType = 0 AND IDType = 1 AND IDNo = '{$idno}' AND (TotalNetAssessed) - (TotalPayment + TotalDiscount + TotalCreditMemo) > 0";

        /* Has Old Account */
        $old_account = DB::select($qry);

        foreach($old_account as $o){
            If ($o->Balance > 0 ){
                $ret['OldAccount'] = 1;
            }
        }

        $ret['refs'] = self::getAssessment($idno, $txn);
        $ret['details'] = self::getAssessmentDetails('1', $idno);

        return $ret;
    }

    public static function setEmployee($idno){

        $model = new mStudents();
        return $ret;
    }

    public static function setSchoprovider($idno){

        $model = new mStudents();
        return $ret;
    }

    public static function setApplicant($idno){

        $model = new mStudents();
        $ret = [
             'IDNo' => ''
            ,'BadAccount' => 0
            ,'OldAccount' => 0
            ,'refs' => ''
            ,'details' => ''
        ];
        return $ret;
    }

    public static function setOtherpayer($idno)
    {

        $model = new mStudents();
        return $ret;
    }

    public static function getAssessment($idno, $txn)
    {
        $ret = 0;

        if($txn == '1') /* Enrollment */
        {
         $qry = "SELECT R.RegID, fn_AcademicYearTerm(R.TermID) as `Term`, R.RegDate, R.ValidationDate, R.CampusID,
                  fn_CampusName(R.CampusID) as CampusName, R.TermID,
                  fn_RegStudentStatus2('76', R.TermID, R.StudentNo) + ' Student / ' + fn_Session(R.SessionID) + ' Session' + fn_FinancialAidProviderName_Remarks(SchoProviderType, SchoProviderID) as Remarks,
                  IFNULL(TableofFeeID, 0) as ScheduleFeeID,
                  fn_TemplateCode(TableofFeeID) As TemplateCode,
                  R.CollegeID, fn_CollegeCode(R.CollegeID) AS DeptCode,
                  R.ProgID, fn_ProgramCode(R.ProgID) AS ProgCode, R.YearLevelID,
                  fn_Yearlevel2(R.YearLevelID, fn_ProgramClassCode(R.ProgID)) AS YearLevelName
                  ,AssessedFees,TotalBalance
                 FROM es_registrations R
                 WHERE R.StudentNo = '{$idno}' AND R.TotalAssessment >= 0
                 ORDER BY R.RegID DESC, R.TermID DESC, R.RegDate ASC" ;

           $ret = DB::select($qry);

        }

        return $ret;
    }

    public static function getAssessmentDetails($type, $idno)
    {
        $ret = 0;

         $strSummary = "call ES_GetOutstandingBalanceSummary( '{$type}', '{$idno}', '1')";
         $strBalance = "call ES_GetOutstandingAllBalance( '{$type}', '{$idno}','".systemDate()."')";

         #err_log($strSummary);
         #err_log(DB::select($strSummary));

         $ret = array(
             'summary' => DB::select($strSummary)
            ,'balance' => DB::select($strBalance)
            ,'obalance'=> 0
         );

        return $ret;
    }

    public static function verifyORNo($ORNo)
    {
        $model = new mOR();

        $rs = $model->where('ORNo', $ORNo)->get();

        return ( count($rs) > 0 ? '0': '1') ;
    }

    public static function saveOR($post)
    {
        $model = new mOR();

        $orno = (getObjectValue($post,'orno'));

        $data = self::set_OR_data($post);

        $rs = $model->create($data);
        SystemLog('Cashiering','','Create OR','save transaction','OR:'. $orno ,'' );

        self::saveORDetails($post);
        self::saveORChecks($post);

        return ( count($rs) > 0 ? '0': '1') ;

    }

    public static function saveORDetails($post)
    {
        $model = new mJournals();

        $details = $post['details'];
        unset($post['details']);

        $i = 0;

        foreach($details as $d){
            $newdata = array_merge($d, $post);

            $data = self::set_journal_data($newdata, $i);
            //err_log($data);
            $i++;
            $rs = $model->create($data);

            /* $model->updateOrCreate($whr, $data); */
        }

        return 1;

    }

    public static function saveORChecks($post)
    {
        $j=0;


            $checks = getObjectValue($post,'checks');
        if( $checks != '' ){

            foreach($checks as $d)
            {
                if(getObjectValue($d,'checkno') != '')
                {
                    $data = self::set_checks_data($d, getObjectValue($post,'orno' ));
                    //err_log($data);
                    $j++;
                    $rs = DB::table('es_bankchecks')->insert($data);

                }

                /* $model->updateOrCreate($whr, $data); */
            }
        }
            return $j;




    }

    public static function set_checks_data($post, $orno)
    {

        $date = setDateFormat(getObjectValue($post,'chkdate'),"mm/dd/yyyy","yyyy-mm-dd");
        $data = array(

             'ORNo' => $orno
            ,'CheckType' => getObjectValue($post,'chktype')
            ,'CheckNo' => getObjectValue($post,'checkno')
            ,'CheckDate' => $date
            ,'CheckBank' => trimmed(getObjectValue($post,'chkbank'))
            ,'CheckAmount' => getObjectValue($post,'chkamount')
        );

        return $data;
    }

    public static function set_OR_data($post){

        $cash = trimmed( getObjectValue($post,'cash'),'d');
        $check = trimmed( getObjectValue($post,'check'),'d');

        $card1 = trimmed( getObjectValue($post,'card1'),'d');
        $card2 = trimmed( getObjectValue($post,'card2'),'d');

        $due = trimmed( getObjectValue($post,'due'),'d') ;
        $discount = trimmed( getObjectValue($post,'discount'),'d') ;
        $type = decode(getObjectValue($post,'type'));
        $ref;
        if($type == 5){
          $ref =  getObjectValue($post,'ref');
        }else{
          $ref =  decode(getObjectValue($post,'ref'));
        }

        $data = array(
             'CampusID' => '1'
            ,'ORNo' => getObjectValue($post,'orno')
            ,'Date' => getObjectValue($post,'date')
            ,'PayorType' => decode(getObjectValue($post,'type'))
            ,'PayorID' => decode(getObjectValue($post,'idno'))
            ,'PayorName' => trimmed(getObjectValue($post,'name'))
            ,'Particular' => getObjectValue($post,'particular')
            ,'CurrencyID' => getObjectValueWithReturn($post,'currency','1')
            ,'AmountDue' => $due
            ,'AmountInWords' => trimmed( getObjectValue($post,'amtinwrd'))
            ,'CashReceive' => $cash
            ,'CheckReceive'  => $check
            ,'Change' => ( $cash + $check + $card1 + $card1) - $due
            ,'Discount' => $discount
            ,'DiscountOffsetBackAccount' => 0
            ,'ForPosting' => 0
            ,'SecurityCode' => ''
            ,'CashierID' => getUserID()
            ,'TransType' => decode(getObjectValue($post,'txn'))
            ,'RefNo' => $ref
            ,'CardReceive' => $card1 + $card2
            ,'CardTypeID' => getObjectValue($post,'cardtype1')
            ,'CardNo' => getObjectValue($post,'cardno1')
            ,'CardTypeName' => ''
            ,'CardApprovalNo' => getObjectValue($post,'cardapproval1')
            ,'CardAmount' => $card1
            ,'CardTypeID2' => getObjectValue($post,'cardtype2')
            ,'CardNo2' => getObjectValue($post,'cardno2')
            ,'CardTypeName2' => ''
            ,'CardApprovalNo2' => getObjectValue($post,'cardapproval1')
            ,'CardAmount2' => $card2
            ,'CreatedBy' => getUserID()
            ,'CreatedDate' => systemDate()
            ,'CreatedTerminal' => Request::ip()
        );

        return $data;
    }

     public static function set_journal_data($post, $seqno ){

        $cash = trimmed( getObjectValue($post,'cash'),'d');
        $check = trimmed( getObjectValue($post,'check'),'d');

        $card1 = trimmed( getObjectValue($post,'card1'),'d');
        $card2 = trimmed( getObjectValue($post,'card2'),'d');

        $due = trimmed( getObjectValue($post,'due'),'d') ;
        $discount = trimmed( getObjectValue($post,'discount'),'d') ;

        $data = array(
           'ServerDate' => systemDate()
          ,'TransDate' => getObjectValue($post,'date')
          ,'TermID' => decode(getObjectValue($post,'term'))
          ,'CampusID' => '1'
          ,'TransID' => '20'
          ,'ReferenceNo' => getObjectValue($post,'orno')
          ,'Description' => ''
          ,'Particulars' => getObjectValue($post,'particular')
          ,'IDType' => decode(getObjectValue($post,'type'))
          ,'IDNo' => decode(getObjectValue($post,'idno'))
          ,'Payor' => trimmed(getObjectValue($post,'name'))
          ,'AccountID' => trimmed(getObjectValue($post,'id'))
          ,'CurrencyID' => trimmed(getObjectValue($post,'curr'))
          ,'Debit' => 0
          ,'Credit' => trimmed(getObjectValue($post,'amt'),'d')
          ,'Remarks' => trimmed(getObjectValue($post,'remarks'))
          ,'UserID' => getUserID()
           ,'PaymentDiscount' => trimmed(getObjectValue($post,'disc'),'d')
           ,'Refund' => '0'
           ,'ADCRefund' => '0'
           ,'TransType' => decode(getObjectValue($post,'txn'))
           ,'TransRefNo' => trimmed(getObjectValue($post,'trn'))
           ,'SeqNo' => $seqno
           ,'NonLedger' => trimmed(getObjectValueWithReturn($post,'nonledger',0))
           ,'Deferred' => 0
           ,'SubCodeID' => 0
           ,'DMCode' => 0
           ,'DeptID' => trimmed(getObjectValue($post,'deptid'))
           ,'DeptCode' => trimmed(getObjectValue($post,'deptcode'))
           ,'YrLvlID' => trimmed(getObjectValue($post,'yearid'))
           ,'YrLvlName' => trimmed(getObjectValue($post,'level'))
        );

        return $data;
    }

    public static function deleteReceipt($orno)
    {
        $model = new mOR();

        $where = ['ORNo' => $orno];

        #delete from OR
        $model->where($where)->delete();

        $where = ['TransID' => 20, 'ReferenceNo'=>$orno];

        #delete or details
        mJournals::where($where)->delete();

        $where = ['ORNo' => $orno];

        #delete check details
        DB::table('es_bankchecks')->where($where)->delete();

        SystemLog('Cashiering','','Delete Receipt','Permanently delete receipt','OR:'. $orno ,'' );

        return true;

    }

    public static function voidReceipt($orno)
    {
        $model = new mOR();

        $where = ['ORNo' => $orno];

        #void OR
        $data = array(
            'PayorName' =>'*** Void Transaction ***'
            ,'IsVoid' => 1
            ,'AmountDue'=>0
            ,'AmountInWords' => ''
            ,'CashReceive' => 0
            ,'CheckReceive'  => 0
            ,'Change' => 0
            ,'DateModified' => systemDate()
            ,'ModifiedBy' => getUserID()
        );

        $model->where($where)->update($data);

        $where = ['TransID' => 20, 'ReferenceNo'=>$orno];

        #delete or details
        mJournals::where($where)->delete();

        $where = ['ORNo' => $orno];

        #delete check details
        DB::table('es_bankchecks')->where($where)->delete();

        SystemLog('Cashiering','','Void Receipt','Void receipt','OR:'. $orno ,'' );

        return true;

    }


    public static function searchOfficialReceipts($post ){

    }


}
