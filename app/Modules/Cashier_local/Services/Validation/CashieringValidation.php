<?php

namespace App\Modules\Cashier\Services\Validation;

use Validator;

class CashieringValidation
{
    public function payment_form($post)
    {
        
        $validator = validator::make(
            [
                'ref' => decode(getObjectValue($post, 'ref')),
                'due' => trimmed( getObjectValue($post, 'due') , 'd')
            ],
            [
                'ref' => 'required|integer',                
                'due' => 'required|numeric'
            ]
        );

        return $validator;
    }

    public function checkout($post)
    {
        
        $validator = validator::make(
            [
                 'ref' => decode(getObjectValue($post, 'ref'))
                ,'due' => trimmed( getObjectValue($post, 'due') , 'd')
                ,'idno' => trimmed( getObjectValue($post, 'idno'))
                ,'txn' => decode(getObjectValue($post, 'ref')) 
                ,'type' => decode(getObjectValue($post, 'type'))
                ,'orno' => decode(getObjectValue($post, 'orno'))
            ],
            [
                'ref' => 'required|integer'    
               ,'due' => 'required|numeric'
               ,'idno' => 'required|string'
               ,'txn' => 'required|integer'
               ,'type' => 'required|integer'
               ,'orno' => 'required|string'
            ]
        );

        return $validator;
    }
    
   
    
}
?>