<?php

Route::group(['prefix' => 'cashier','middleware'=>'auth'], function () {

    Route::group(['prefix' => 'cashiering', 'middleware' => 'auth'], function () {
        Route::get('/', 'Cashiering@index');
        Route::post('event', 'Cashiering@event');
        Route::get('/print', 'Cashiering@eprint');
    });
    
    Route::group(['prefix' => 'official-receipts', 'middleware' => 'auth'], function () {
        Route::get('/', 'OfficialReceipts@index');
        Route::post('event', 'OfficialReceipts@event');
        
    });

    
});

?>