<?php $j=1; ?>
    @if(!empty($data))
        @foreach($data as $r)
        
<tr data-id="{{$r->EntryID}}" data-due="{{$r->Due}}"  data-ac="{{$r->AccountID}}" data-code="{{$r->AcctCode}}" data-discount="0" data-first="{{ $r->{'1st Payment'} }}" data-curr="1" data-txn="{{$r->TransID}}" data-ref="{{$r->ReferenceNo}}" data-trn="{{$r->ReferenceNo.';'.$r->EntryID.';1'}}" data-remarks="{{$r->Remarks}}"
data-nonledger="0" 
            data-deptid="{{$r->DeptID}}" 
            data-dept="{{getObjectValue($r,'DeptCode')}}" 
            data-yr="{{$r->YrLvlID}}" 
            data-lvl="{{$r->YrLvlName}}"


>
    <td class="font-xs autofit bold">{{$j}}.</td>
    <td class="autofit">{{$r->AcctCode}}</td>
    <td>{{$r->AcctName}}</td>
    <td class="autofit text-right balance bold ">
        <input type="text" class="numberonly text-right balance-amt input-no-border input-xsmall " value="{{number_format($r->Due,2)}}" />
    </td>
    <td class="autofit text-right discount bold "><input type="text" class="numberonly text-right discount-amt input-no-border input-xsmall " value="0.00" /></td>
    <td class="autofit danger bold text-right due">{{number_format($r->Due,2)}}</td>
    <td>{{$r->Remarks}}</td>
    <td class="autofit text-center"><input type="checkbox" class="non-ledger" /> </td>
    <td class="autofit">{{$r->SeqNo}}</td>
    <td class="autofit text-center">{{$r->TransID}}</td>
    <td class="autofit">{{$r->ReferenceNo.';'.$r->EntryID.';1'}}</td>
</tr>
 <?php $j++; ?>
        @endforeach
        @endif
        