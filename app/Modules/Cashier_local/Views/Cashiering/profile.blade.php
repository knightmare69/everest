<div class="row">
    <div class="col-sm-12">
        <div class="input-group">
            <div class="input-group-btn">
				<button id="ptype" type="button" data-id="1" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="bold">Student</span> <i class="fa fa-angle-down"></i></button>
				<ul class="dropdown-menu">
					<li> <a href="javascript:void(0);" class="idtype" data-type="1"> Students </a></li>
          <li>
            <a href="javascript:void(0);" class="idtype" data-type="5"> Other Payer </a>
          </li>
                    <li>
						<a href="javascript:void(0);" class="idtype" data-type="2"> Employee </a>
					</li>
                    <li>
						<a href="javascript:void(0);" class="idtype" data-type="3"> Scho. Provider </a>
					</li>
                    <li>
						<a href="javascript:void(0);" class="idtype" data-type="4" > Applicant </a>
					</li>


					<li class="divider"></li>
					<li><a href="javascript:void(0);">Cancel </a></li>
				</ul>
			</div>
            <input type="text" id="txtname" name="txtname" class="form-control bold font-blue-madison" value="" placeholder="Search by IDNo, Name" />
            <span class="input-group-btn">
			 <button class="btn blue" type="button" data-menu="search">Search</button>
			</span>

        </div>
        <div class="help-block">
            <strong id="idlabel" class="font-blue-sharp font-xs">ID #:</strong> <strong id="tidno" class="font-blue-sharp font-xs">ID Number</strong> |
            <strong class="font-blue-sharp font-xs">Level :</strong> <strong id="tyr" class="font-blue-sharp font-xs">Year Level</strong> |
            <strong class="font-blue-sharp font-xs">Department :</strong> <strong id="tdept" class="font-blue-sharp font-xs">&nbsp;</strong> |
            <strong class="font-blue-sharp font-xs"> Nationality : </strong> <strong id="tnat" class="font-blue-sharp font-xs">&nbsp;</strong>
		</div>
    </div>
</div>
<hr style="margin-top: 5px;margin-bottom: 5px;">
<div class="row">
    <div class="col-sm-5">
        <div class="row static-info">
            <div class="col-md-12 name">
                <strong class="font-purple-plum">Assessment / Billing</strong>
            </div>
        </div>
        <div class="row static-info">
    		<div class="col-md-5 name bold text-right">
    			 Transaction :
    		</div>
    		<div class="col-md-7 value">
    			<select id="txn" name="txn" class="form-control input-sm ">
                    <option value="1">Enrollment</option>
                    <option value="2">Add/Drop/Change</option>
					<option value="5">Admission/Testing</option>
                    <option value="15">Other Assessment</option>
                </select>
    		</div>
    	</div>
        <div class="row static-info">
    		<div class="col-md-5 name text-right bold">
    			 Reference # :
    		</div>
    		<div class="col-md-7 value form-inline">
    			<select id="refs" name="refs" class="form-control input-sm  input-xsmall " style="padding-left: 5px; padding-right: 5px;">
                    <option>- select -</option>
                </select>
                <button type="button" class="btn btn-default btn-xs " data-menu="ref-refresh"><i class="fa fa-refresh"></i></button>
                <input type="checkbox" id="chk_all" /> All
    		</div>
    	</div>
    </div>
    <div class="col-sm-7">
        <div class="row static-info">
            <div class="col-md-12">
                <div class="well well-sm well-light" style="margin-bottom: 0px;">
                <strong class="font-purple-plum">Transaction Details</strong>
                <table class="">
                    <tr><td class="">A.Y. Term : </td><td class="term bold"> </td></tr>
                    <tr><td>Reg. Date :</td><td class="date bold"></td></tr>
                    <tr><td>Validation Date :</td><td class="validation bold"></td></tr>
                    <tr><td>Remarks :</td><td class="remarks bold"></td></tr>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
