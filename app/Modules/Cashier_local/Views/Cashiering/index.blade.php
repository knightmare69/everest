<style>
.profile{
    float: left;
    width: 150px;
    margin-right: 20px;
}
.content{
    overflow: hidden;
}
.modal-lg{
  width: 1300px !important;
}
</style>

<div class="portlet light hidden-print" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-money"></i>
			<span class="caption-subject bold uppercase"> Cashiering </span>
			<span class="caption-helper hidden-xs">Use this module to accept payment/s.</span>
        </div>

		<div class="actions hidden-xs ">
            <div class="portlet-input hide input-inline input-medium ">
                <div class="input-group  ">
	             <input type="text" class="form-control input-sm " id="orsearch" placeholder="Search O.R"  />
                 <span class="input-group-btn ">
			     <button class="btn btn-default btn-sm" style="height: 28px;" data-menu="orsearch" type="button">Go!</button>
			     </span>
               </div>
            </div>

            <a href="javascript:void();" class="btn btn-sm btn-info hide" data-menu="edit" data-original-title="Edit" title="Edit"><i class="fa fa-edit"></i> Edit</a>
            <a href="javascript:void();" class="btn btn-sm btn-danger hide" data-menu="void" data-original-title="" title="Void"><i class="fa fa-times"></i> Void</a>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row">
            <div class="col-sm-9">
                <div class="profile">
                    <img id="stud_photo" class="entryphoto pull-left" width="150" height="180" src="<?php echo url('assets/system/media/images/no-image.png').'?'.rand(0,9999);?>"/>
                </div>
                <div class="content">
                    @include($views.'profile')
                </div>
            </div>
            <div class="col-sm-3 util-btn-margin-bottom-5">
                    <button class="btn btn-sm pull-right btn-info" type="button" data-menu="setup" ><i class="fa fa-cog"></i> Setup</button>
                    <h3 class="bold" style="margin-bottom: 5px; margin-top: 0px;">
                        O.R. No : <span id="or_num" class="font-red"></span>
                    </h3>
                    <h5 class="bold" >
                        O.R. Date : <span id="or_date"  data-date="{{systemDate()}}" class="font-red">{{systemDate()}}</span>
                    </h5>
                    <h5 class="bold" >
                        Currency :
                        <select id="or_curr" name="or_curr" class="input-small">
                            <option value="1">PHP</option>
                            <option value="2">USD</option>
                        </select>
                    </h5>
                    <hr />
                    <div class="clearfix">
                        <button class="btn btn-success" type="button" data-menu="full"><i class="fa fa-money"></i> Full Payment</button>
                        <button class="btn bg-yellow-crusta" type="button" data-menu="schedule"><i class="fa fa-calendar"></i> Payment Schedule</button>
                        <button class="btn btn-info" type="button" data-menu="auto"><i class="fa fa-calculator"></i> Auto Distribute</button>
                    </div>
              </div>
        </div>
        <div id="transaction_details" style="margin-bottom: 5px;">
		  @include($views.'list')
        </div>
        <div class="row">
            <div class="col-xs-5">
                <a data-menu="soa" class="btn btn-md green hidden-print margin-bottom-5">S.O.A</a>
                <a id="ledger" href="{{url('accounting/studentledger' )}}" class="btn btn-md blue hidden-print margin-bottom-5">Ledger</a>
                <a id="balance_summary" class="btn btn-md purple hidden-print margin-bottom-5">Summary of Balance</a> |
                <a data-menu="template" class="btn btn-md red-pink hidden-print margin-bottom-5">Fees Template</a>
            </div>
            <div class="col-xs-4">
                <div class="input-group">
                    <span class="input-group-addon">
    				    Particular
    				</span>
                    <input type="text" id="particular" name="particular" class="form-control bold" value="" placeholder="Particular" />

                </div>
            </div>
            <div class="col-xs-3 text-right invoice-block">

                <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">Print <i class="fa fa-print"></i></a>
                <a class="btn btn-lg green hidden-print margin-bottom-5" data-menu="accept">Accept Payment <i class="fa fa-check"></i></a>
            </div>
        </div>
	</div>
</div>
@include($views.'sub.setup')
@include($views.'sub.add')
<div class="for_printing">
</div>
