<style >
  .modal-lg{
    width: 1300px !important; 
  }
</style>

<?php $j =1;
 $status= array( 0 => 'All Students', 1 => 'New Student Only', 2 => 'Old Student Only' );

    $scheme = array(
        0 => 'Cash',
        1 => '1st Payment',
        2 => '2nd Payment',
        3 => '3rd Payment',
        4 => '4th Payment',
        5 => '5th Payment',
        6 => '6th Payment',
        7 => '7th Payment',
        8 => '8th Payment',
        9 => '9th Payment',
        10 => '10th Payment',
    );

    $total=array(
        0 => '0',
        1 => '0',
        2 => '0',
        3 => '0',
        4 => '0',
        5 => '0',
        6 => '0',
        7 => '0',
        8 => '0',
        9 => '0',
        10 => '0',
        11 => '0'
    );

    foreach($data as $r){
        $total[1] = floatval($total[1]) + $r->{'1st Payment'};
        $total[2] = floatval($total[2]) + $r->{'2nd Payment'};
        $total[3] = floatval($total[3]) + $r->{'3rd Payment'};
        $total[4] = floatval($total[4]) + $r->{'4th Payment'};
        $total[5] = floatval($total[5]) + $r->{'5th Payment'};
        $total[6] = floatval($total[6]) + $r->{'6th Payment'};
        $total[7] = floatval($total[7]) + $r->{'7th Payment'};
        $total[8] = floatval($total[8]) + $r->{'8th Payment'};
        $total[9] = floatval($total[9]) + $r->{'9th Payment'};
        $total[10] = floatval($total[10]) + $r->{'10th Payment'};
        $total[11] = floatval($total[11]) + $r->{'ActualPayment'};
    }

?>

<div class="table-scrollable" style="height: 300px; overflow-y: scroll; margin-bottom:0px !important;" >
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="schedule-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit">#</th>
            <th>Account</th>
            <th class="text-center"><a href="#" data-scheme="0" class="pay-option">Full<br/>Payment</a></th>
            <th class="text-center">
                @if( (floatval($total[1]) - floatval($total[11])) > 0 )
                <a href="#" data-scheme="1" class="pay-option">1st<br/>Payment</a>
                @else
                1st<br/>Payment
                @endif
            </th>
            <th class="text-center">
                @if( ( floatval($total[1]) + floatval($total[2]) ) - floatval($total[11]) > 0 )
                <a href="#" data-scheme="2" class="pay-option">2nd<br/>Payment</a>
                @else
                2nd<br/>Payment
                @endif
            </th>
            <th class="text-center"><a href="#" data-scheme="3" class="pay-option">3rd<br/>Payment</a></th>
            <th class="text-center"><a href="#" data-scheme="4" class="pay-option">4th<br/>Payment</a></th>
            <th class="text-center"><a href="#" data-scheme="5" class="pay-option">5th<br/>Payment</a></th>
            <th class="text-center"><a href="#" data-scheme="6" class="pay-option">6th<br/>Payment</a></th>
            <th class="text-center">Actual<br/>Payment</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <?php
         $full = floatval($r->Debit) - floatval($r->{'ActualPayment'});
         $total[0] = floatval($total[0]) + $full;



        ?>
        <tr data-id="{{ encode($r->EntryID) }}"
            data-due="0"
            data-ac="{{$r->AccountID}}"
            data-code="{{$r->AcctCode}}"
            data-discount="0"
            data-first="{{ $r->{'1st Payment'} }}"
            data-curr="1"
            data-txn="{{$r->TransID}}"
            data-ref="{{$r->ReferenceNo}}"
            data-trn="{{$r->ReferenceNo.';'.$r->EntryID.';1'}}"
            data-remarks="{{$r->Remarks}}"
            data-nonledger="0"
            data-deptid="{{$r->DeptID}}"
            data-dept="{{getObjectValue($r,'DeptCode')}}"
            data-yr="{{$r->YrLvlID}}"
            data-lvl="{{$r->YrLvlName}}"

        >
            <td class="font-xs autofit bold">{{$j}}.</td>
            <td class="">
                <a href="javascript:void();" class="options" data-menu="view">{{$r->AcctName}}</a>
                | <small class="font-xs bold tcode">{{ $r->AcctCode }}</small>
            </td>
            <td width="80" data-scheme="0" class="text-right danger">{{number_format($full,2)}}</td>
            <td width="80" data-scheme="1" class="text-right"><?= number_format($r->{'1st Payment'},2) ?></td>
            <td width="80" data-scheme="2" class="text-right"><?= number_format($r->{'2nd Payment'},2) ?></td>
            <td width="80" data-scheme="3" class="text-right"><?= number_format($r->{'3rd Payment'},2) ?></td>
            <td width="80" data-scheme="4" class="text-right"><?= number_format($r->{'4th Payment'},2) ?></td>
            <td width="80" data-scheme="5" class="text-right"><?= number_format($r->{'5th Payment'},2) ?></td>
            <td width="80" data-scheme="6" class="text-right"><?= number_format($r->{'6th Payment'},2) ?></td>
            <td width="80" data-scheme="11" class="text-right"><?= number_format($r->{'ActualPayment'},2) ?></td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>
</div>
<div class="" style="overflow-y: scroll;" >
<table class="table table-striped table-bordered table-condensed table-hover table_scroll "  style="margin-bottom: 0px !important;">
    <tfoot>
    <tr >
            <td class="font-xs autofit bold"></td>
            <td class="bold text-right">
               Total
            </td>
            <td width="80" data-scheme="0" class="text-right ctotal danger bold">{{number_format($total[0],2)}}</td>
            <td width="80" data-scheme="1" class="text-right ctotal bold">{{number_format($total[1],2)}}</td>
            <td width="80" data-scheme="2" class="text-right ctotal bold">{{number_format($total[2],2)}}</td>
            <td width="80" data-scheme="3" class="text-right ctotal bold">{{number_format($total[3],2)}}</td>
            <td width="80" data-scheme="4" class="text-right ctotal bold">{{number_format($total[4],2)}}</td>
            <td width="80" data-scheme="5" class="text-right ctotal bold">{{number_format($total[5],2)}}</td>
            <td width="80" data-scheme="6" class="text-right ctotal bold">{{number_format($total[6],2)}}</td>
            <td width="80" data-scheme="11" class="text-right bold">{{number_format($total[11],2)}}</td>


        </tr>
        <tr>
            <td class="font-xs autofit bold"></td>
             <td class="bold text-right">
               Due Dates
            </td>
            <td width="80" data-scheme="0" class="text-right ctotal danger bold">{{setDateFormat(getObjectValue($dues,'FirstPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="1" class="text-right ctotal danger bold">{{setDateFormat(getObjectValue($dues,'FirstPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="2" class="text-right ctotal  bold">{{setDateFormat(getObjectValue($dues,'SecondPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="3" class="text-right ctotal  bold">{{setDateFormat(getObjectValue($dues,'ThirdPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="4" class="text-right ctotal  bold">{{setDateFormat(getObjectValue($dues,'FourthPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="5" class="text-right ctotal  bold">{{setDateFormat(getObjectValue($dues,'FifthPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="6" class="text-right ctotal  bold">{{setDateFormat(getObjectValue($dues,'SixthPaymentDueDate'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
            <td width="80" data-scheme="11" class="text-right ctotal  bold"></td>
        </tr>
    </tfoot>
</table>
</div>
