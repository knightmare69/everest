<?php
    $j=1;    
    $descripancy=0;
       
?>     
<div class="input-group  ">
 <input type="text" class="form-control input-sm " id="orsearch2" placeholder="Search O.R"  />
 <span class="input-group-btn ">
 <button class="btn btn-default btn-sm" style="height: 28px;" data-menu="orsearch2" type="button">Begin Search</button>
 </span>
</div>     
                                              
<div class="table-scrollable" style="height: 270px; overflow-y: scroll;" > 
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="orlist" style="cursor: pointer;">
    <thead>
        <tr>            
            <th class="autofit">#</th>
            <th>OR #</th>
            <th>OR Date</th>
            <th>Payer Name</th>
            <th>Particular</th>
            <th>Paid Amount</th>
            <th>Descripancy</th>
            <th>Cashier</th>
            <th>Void</th>
        </tr>
    </thead>
    <tbody>
        
          @if($data)  
                
            @foreach($data as $r)
                
                <?php
                
                  if($r->Descripancy > 0){
                        $descripancy = '('.number_format($r->Descripancy,2).')';
                    }
                    
                ?>
            <tr data-id="{{$r->ORNo}}" data-payer="{{$r->PayorID}}"  >
                <td class="font-xs autofit bold">{{$j}}.</td>
                <td class="autofit">{{$r->ORNo}}</td>
                
                <td class="autofit ">{{$r->Date}}</td>
                <td>{{$r->PayorName}}</td>
                <td>{{$r->Particular}}</td>
                
                <td class="autofit danger bold text-right due">{{number_format($r->AmountDue,2)}}</td>
                <td class="autofit danger bold text-right ">{{$descripancy}}</td>
                <td>{{$r->CashierID}}</td>
                <td></td>
            </tr>                                                   
            @endforeach
            
        @else
        <tr>
            <td colspan="7">No Record Found.</td>
        </tr>
        @endif
           
    </tbody>
</table>
</div>