@if(!empty($data))
  @foreach($data as $d)
  <option value="{{encode($d->RegID)}}"
      data-remarks="{{$d->Remarks}}"
      data-term="{{$d->Term}}"
      data-tid="{{encode($d->TermID)}}"
      data-date="{{$d->RegDate}}" data-validation="{{$d->ValidationDate}}" >{{$d->RegID}}</option>
  @endforeach
@endif
