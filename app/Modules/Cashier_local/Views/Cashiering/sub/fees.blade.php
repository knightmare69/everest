<?php $j =1; 
 $status= array( 0 => 'All Students', 1 => 'New Student Only', 2 => 'Old Student Only' );

    $scheme = array(
        0 => 'Cash',
        1 => 'Semestral',
        2 => 'Quarterly',
        3 => 'Monthly',
    );
    
?>
<div class="btn-group">
    <button type="button" class="btn btn-default active fees-filter" data-val="0" >Show All</button>
    @foreach($scheme as $s => $k)
	   <button type="button" class="btn btn-default fees-filter" data-val="{{$s}}">{{$k}}</button>
    @endforeach
</div>
    
<div class="table-scrollable" style="height: 270px; overflow-y: scroll;" >
   
                                                    
<table class="table table-striped table-bordered table-hover table_scroll" id="fees-table" style="cursor: pointer;">
    <thead>
        <tr>            
            <th class="autofit">#</th>
            <th>Template Name</th>            
            <th>Foreign</th>
            <th>Currency</th>
            <th>Applies to</th>
            <th>Payment <br /> Scheme</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <tr data-id="{{ encode($r->TemplateID) }}" data-campus="<?= encode($r->CampusID)?>" data-curr="<?= ($r->CurrencyID)?>" data-status="{{$r->StudentStatus}}" data-scheme="{{$r->PaymentScheme}}" >
            <td class="font-xs autofit bold">{{$j}}.</td>
            <td class="">
                <a href="javascript:void();" class="options" data-menu="view">{{$r->TemplateDesc}}</a>
                <br /><small class="font-xs bold tcode">{{ $r->TemplateCode }}</small>
            </td>           
            <td class="autofit text-center"><?= $r->ForForeign == 1 ? '<i class="fa fa-check"></i>': '' ?></td>
            <td class="autofit text-center">{{$r->Curr}}</td>
            <td class="autofit">{{$status[$r->StudentStatus]}}</td>
            <td class="autofit">{{$scheme[$r->PaymentScheme] }}</td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>
</div>