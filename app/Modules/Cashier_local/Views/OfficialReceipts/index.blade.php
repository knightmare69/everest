<style>
.profile{
    float: left;
    width: 350px;
    margin-right: 20px;
}
.content{
    overflow: hidden;
}
</style>
<?php

    $date_start = date('Y-m-01');
    $end_date = date("Y-m-d",strtotime("+1 month", strtotime($date_start)));
    $end_date = date("Y-m-d",strtotime("-1 day", strtotime($end_date)));
    
    
?>
<div class="row">
    <div class="col-sm-12">
    
    <div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption">
         <div class="caption"><i class="fa fa-money"></i>
			<span class="caption-subject bold uppercase"> List of Official Receipts </span>
			<span class="caption-helper hide hidden-xs">Use this module to manage official receipts</span>
		</div>
        
        </div>
		<div class="actions hidden-xs">
            <div class="portlet-input input-inline  input-medium">
	           <input type="text" class="form-control input-circle input-sm " title="Search O.R"  />
			</div>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
            <a href="javascript:void(0);" class="btn btn-sm btn-warning" data-menu="void" data-original-title="" title="Void"><i class="fa fa-ban"></i> Void</a>
            <a href="javascript:void(0);" class="btn btn-sm btn-danger" data-menu="delete" data-original-title="" title="Delete"><i class="fa fa-times"></i> Delete </a>
            
		</div>
	</div>
	<div class="portlet-body">
        <form id="frm_search" action="?" class="form-horizontal">
                
        <div class="row static-info">
            <div class="col-md-3">
                <div class="row static-info">
            		<div class="col-md-4 name bold "><span class="autofit">Date Start :</span></div>
            		<div class="col-md-8 value">
            			<input  type="text" id="dtbegin" name="start" class="form-control bold input-sm date-picker"  placeholder="Date Start" value="<?= $date_start ?>" />
            		</div>         	
            	</div>
            	<div class="row static-info">
        		  <div class="col-md-4 name bold "><span class="autofit">Date End :</span></div>
        		  <div class="col-md-8 value">
        			<input  type="text" id="dtend" name="end" value="<?= $end_date ?>" class="form-control input-sm bold date-picker" placeholder="Date Finish" />
        		  </div>         	
        	   </div>            
            </div>
            <div class="col-md-3">
            <div class="row static-info">
        		<div class="col-md-4 name bold "><span class="autofit">O.R. Start :</span></div>
        		<div class="col-md-8 value">
        			<input  type="text" id="orbegin" name="orbegin" value="" class="form-control input-sm" placeholder="O.R. Start" />
        		</div>         	
        	</div>
			<div class="row static-info">
        		<div class="col-md-4 name bold "><span class="autofit">O.R. End :</span></div>
        		<div class="col-md-8 value">
        			<input  type="text" id="orend" name="orend" value="" class="form-control input-sm" placeholder="O.R. Finish" />
        		</div>         	
        	</div>
            </div>
            <div class="col-md-3">
            	<div class="row static-info">
        		<div class="col-md-4 name bold "><span class="autofit">Payer</span></div>
        		<div class="col-md-8 value">
        			<input  type="text" id="payer" name="payer" value="" class="form-control input-sm" placeholder="Payer ID/Name" />
        		</div>         	
        	</div>
            <div class="row static-info">
        		<div class="col-md-4 name bold "><span class="autofit">Cashier</span></div>
        		<div class="col-md-8 value">
        			<input  type="text" id="cashier_id" name="cashier_id" value="" class="form-control input-sm" placeholder="Cashier id / Cashier name" />
                    <span class="font-xs red">Leave it blank if all cashier</span>
        		</div>         	
        	</div>
            </div>
                        <div class="col-md-3">            
                            <div class="row static-info">
                        		<div class="col-md-4 name bold "><span class="autofit">Status</span></div>
                        		<div class="col-md-8 value">
                        			<select id="status" class="form-control" name="status" >
                                        <option value="0">Show All</option>
                                        <option value="1">Posted Only</option>
                                        <option value="2">Unposted Only</option>
                                        <option value="2">With Descripancy</option>
                                    </select>
                        		</div>         	
                        	</div>
                            <div class="row static-info">
                                <div class="col-md-4 name bold "> </div>
                                <div class="col-md-8 value">                    
                                	<button class="btn btn-success btn-sm" data-menu="search" type="button"><i class="fa fa-check-circle"></i> Search</button>
                                </div>         	
                            </div>
                        </div>            
                    </div>
                </form>        
                <div class="tx_list">
                    @include($views.'list',['data'=> $data])
                </div>	                                                 
            </div>
        </div>
    </div>    
</div>