<?php namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use App\Modules\Setup\Models\CampusDeptHeads;
use App\Modules\Setup\Services\Campus\Validation;
use DB;

Class Campus extends Model {

	protected $table='es_campus';
	public $primaryKey ='CampusID';

	protected $fillable  = array(
      'InstCode'
      ,'Acronym'
      ,'ShortName'
      ,'ShortNameBySite'
      ,'ShortNameByConstituent'
      ,'DateEstablished'
      ,'LatestSec'
      ,'DateGranted'
      ,'ColConversion'
      ,'UniConversion'
      ,'CampusHead'
      ,'HeadPosTitleID'
      ,'RegionID'
      ,'Barangay'
      ,'TownCity'
      ,'District'
      ,'Province'
      ,'ZipCode'
      ,'MailingAddress'
      ,'Email'
      ,'TelNo'
      ,'FaxNo'
      ,'Website'
      ,'LastModified'
      ,'LastModifiedBy'
      ,'CampusName'
      ,'RegistrarOffice'
      ,'Registrar'
      ,'RegistrarPositionID'
      ,'AccountingOffice'
      ,'Accountant'
      ,'AccountantPositionID'
      ,'CashierOffice'
      ,'Cashier'
      ,'CashierPositionID'
      ,'CampusCode'
      ,'Dept01_Office'
      ,'Dept01_Name'
      ,'Dept01_Position'
      ,'Dept02_Office'
      ,'Dept02_Name'
      ,'Dept02_Position'
      ,'Dept03_Office'
      ,'Dept03_Name'
      ,'Dept03_Position'
      ,'Dept04_Office'
      ,'Dept04_Name'
      ,'Dept04_Position'
      ,'Dept05_Office'
      ,'Dept05_Name'
      ,'Dept05_Position'	
	);

	public $timestamps = false;

  public function _save($post)
  {
    $validation = new Validation;
    $deptheads_model = new CampusDeptHeads;

    $validate = $validation->validate($post);

    if ($validate->passes()) {
          $data = array(
            'Acronym'           => getObjectValue($post, 'acronym'),
            'ShortName'         => getObjectValue($post, 'campusname'),
            'CampusName'        => getObjectValue($post, 'shortname'),
            'Barangay'          => getObjectValue($post, 'barangay'),
            'TownCity'          => getObjectValue($post, 'towncity'),
            'Province'          => getObjectValue($post, 'province'),
            'RegionID'          => getObjectValue($post, 'region'),
            'District'          => getObjectValue($post, 'district'),
            'ZipCode'           => getObjectValue($post, 'zipcode'),
            'MailingAddress'    => getObjectValue($post, 'mailingaddress'),
            'Email'             => getObjectValue($post, 'email'),
            'Website'           => getObjectValue($post, 'website'),
            'TelNo'             => getObjectValue($post, 'tel'),
            'FaxNo'             => getObjectValue($post, 'fax'),
      );
            
      $status = $this->create($data);

      if ($status) 
      {
        $data = array_filter($data);
        $post['id'] = $this->where('Acronym', $data['Acronym'])->get()->toArray()[0]['CampusID'];

        $deptheads = json_decode('['.$post['deptheadsData'].']',true);
        if ( $post['id'] )
        {
          $deptheads_model->_save($post['id'],$deptheads); 
        }
          return ['error'=>false,'message'=>'','id'=>encode($post['id'])];
      }
    }
      return getModelMessage(true,getErrorMessages($validate->errors()->getMessages()));
  }

  public function _update($post)
  {
    $validation = new Validation;
    $deptheads_model = new CampusDeptHeads;

    $validate = $validation->validate($post);

    if ( $validate->passes() ){
      $obj                 = $this->find(decode($post['key']));
      $obj->Acronym        = $post['acronym'];
      $obj->ShortName      = $post['campusname'];
      $obj->CampusName     = $post['shortname'];
      $obj->Barangay       = $post['barangay'];
      $obj->TownCity       = $post['towncity'];
      $obj->Province       = $post['province'];
      $obj->RegionID       = $post['region'];
      $obj->District       = $post['district'];
      $obj->ZipCode        = $post['zipcode'];
      $obj->MailingAddress = $post['mailingaddress'];
      $obj->Email          = $post['email'];
      $obj->Website        = $post['website'];
      $obj->TelNo          = $post['tel'];
      $obj->FaxNo          = $post['fax'];

      $status = $obj->update();

      if ( $status )
      {
        $deptheads = json_decode('['.$post['deptheadsData'].']',true);
        if ( decode($post['key']) )
        {
          $deptheads_model->_save(decode($post['key']),$deptheads); 
        }
          return ['error'=>false,'message'=>'','id'=>$post['key']];    
      }
    }
      return getModelMessage(true,getErrorMessages($validate->errors()->getMessages()));
  }

}