<?php
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class SchoGrantTemplate extends Model {

    public $timestamps = false;
	protected $table='ES_SchoGrantTemplates';
	protected $primaryKey ='GrantTemplateID';

	protected $fillable  = array('TermID'
								,'CampusID'
								,'SchoProviderID'
								,'TemplateCode'
								,'ShortName'
								,'Description'
								,'SchoType'
								,'Inactive'
								,'CreatedBy'
								,'CreatedDate'
								,'LastModifiedBy'
								,'LastModifiedDate'
							);
							
	public function get_data($xid=0){
		$qry = "SELECT t.*,p.ProvCode,p.ProvName,p.ProvShort FROM ES_SchoGrantTemplates as t LEFT JOIN ES_SchoProviders as p ON t.SchoProviderID=p.SchoProviderID";
		if($xid>0){
		$qry = $qry." WHERE t.GrantTemplateID='".$xid."'";
		}
		$exec = DB::select($qry);
		return $exec;
	}
	
	public function get_detail($xid=0){
	    $qry = "SELECT a.AcctCode,a.AcctName,td.* FROM ES_SchoGrantTemplate_Details as td INNER JOIN ES_Accounts as a ON td.AcctID=a.AcctID WHERE td.GrantTemplateID='".$xid."'";
		$exec = DB::select($qry);
		return $exec;
	}
	
    public function save_details($tempid='',$acctid=0,$type='',$sqno=0,$amt=0,$remark=''){
	   $exec = DB::statement("INSERT INTO ES_SchoGrantTemplate_Details(GrantTemplateID,AcctID,TypeUsed,SeqNo,Amount,Remarks) VALUES ('".$tempid."','".$acctid."','".$type."','".$sqno."','".$amt."','".$remark."')"); 
	   return $exec;
	}	
	
    public function list_detatils($tempid=''){
	 return false;
	}
}
?>