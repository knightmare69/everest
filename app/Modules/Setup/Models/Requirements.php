<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class Requirements extends Model {

	protected $table='esv2_requirementslist';
	protected $primaryKey ='RequirementID';

	protected $fillable  = array(
		'Code',
    	'Desc'
	);

	public $timestamps = false;

	public static $ReEntryPlanID = 11;

	public static $ThesisDissertionID = 8;
        
}