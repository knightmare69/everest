<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class RequiredTemplatesItems extends Model {

	protected $table='esv2_requirementstemplatedetails';
	protected $primaryKey ='DetailID';

	protected $fillable  = array(
    	'TemplateID',
    	'RequirementID',
    	'IsRequired',
    	'SeqNo'
	);

	public $timestamps = false;

	public static function data($keys)
	{
		$data = DB::table('esv2_requirementslist as r')
			->select([
				'r.Code',
				DB::raw('r.RequirementID'),
				'r.Desc',
				DB::raw("'0' as IsRequired"),
				DB::raw("'0' as Inactive"),
				DB::raw("'' as Template"),
				DB::raw("'' as TemplateID"),
				DB::raw("'' as TemplateItemID"),
			])
			->whereRaw('r.RequirementID not in (select RequirementID from esv2_requirementstemplatedetails where TemplateID in('.$keys.'))');
		return
		DB::table('esv2_requirementstemplate as t')
			->select([	
				'r.Code',
				DB::raw('i.RequirementID'),
				'r.Desc',
				'i.IsRequired',
				't.Inactive',
				DB::raw("IFNULL(t.TemplateName,'') as Template"),
				DB::raw("IFNULL(i.TemplateID,0) as TemplateID"),
				DB::raw("IFNULL(i.DetailID,0) as TemplateItemID"),
			])
			->leftJoin(
				'esv2_requirementstemplatedetails as i',
				'i.TemplateID','=','t.TemplateID'
			)
			->leftJoin(
				'esv2_requirementslist as r',
				'i.RequirementID','=','r.RequirementID'
			)
			->orderBy('i.TemplateID','asc')
			->whereRaw('i.TemplateID in('.$keys.')')
			->union($data);
	}	

	public static function dataSelOnly($keys)
	{
		
		return
		DB::table('esv2_requirementstemplatedetails as t')
			->select([
				'i.DetailID as ItemID',
				DB::raw('t.RequirementID as ID'),
				'r.Desc',
				'r.Inactive',
				'r.Code',
				DB::raw("IFNULL(t.Template,'') as Template"),
				DB::raw("IFNULL(i.TemplateID,0) as TemplateID"),
				DB::raw("IFNULL(i.DetailID,0) as TemplateItemID"),
			])
			->leftJoin(
				'kt_required_templates_items as i',
				'i.TemplateID','=','t.RequirementID'
			)
			->leftJoin(
				'kt_requirements as r',
				'i.RequirementID','=','r.RequirementID'
			)
			->orderBy('i.TemplateID','asc')
			->whereRaw('i.TemplateID in('.$keys.')');
	}	
        
}