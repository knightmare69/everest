<?php
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class GradingSystemSetup extends Model {

    public $table='es_gs_setup';
    protected $primaryKey ='SetupID';

    protected $fillable  = array(
         'Setup'
        ,'Value'
    );

    public $timestamps = false;

    public function scopeConductID($query)
    {
        return $query->where('SetupID','2')->pluck('Value');
    }

    public function scopeTransmutationID($query)
    {
        return $query->where('SetupID','3')->pluck('Value');
    }

    public function scopeGradingSystemID($query)
    {
        return $query->where('SetupID','1')->pluck('Value');
    }

    public function scopeCurrentTermID($query){
        return $query->where('SetupID','6')->pluck('Value');
    }

    public function scopeIsSHS($query){
        return $query->where('SetupID','7')->pluck('Value');
    }

    public function scopeHomeRoomtbl($query){
        return $query->where('SetupID','4')->pluck('Value');
    }

    public function scopeClubID($query)
    {
        return $query->where('SetupID','5')->pluck('Value');
    }

    public function scopeClubPolicy($query)
    {
        return $query->where('SetupID','9')->pluck('Value');
    }

    public function scopeSHSCurrentTerm($query)
    {
        return $query->where('SetupID','10')->pluck('Value');
    }

    public function scopeADCServiceFeeAmt($query)
    {
        return $query->where('SetupID','11')->pluck('Value');
    }
    
    public function scopeADCSplFeesID($query)
    {
        return $query->where('SetupID','12')->pluck('Value');
    }
    
    public function scopeFinalGradeComponent($query)
    {
        return $query->where('SetupID','13')->pluck('Value');
    }
    
}
