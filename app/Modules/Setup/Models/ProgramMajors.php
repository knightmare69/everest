<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class ProgramMajors extends Model {

	protected $table='es_programmajors';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
    	'ProgID',
    	'MajorDiscID',
    	'Inactive',
	);

	public $timestamps = false;
     
 
 
}