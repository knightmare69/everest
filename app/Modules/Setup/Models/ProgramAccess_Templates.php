<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class ProgramAccess_Templates extends Model {

	protected $table='esv2_programaccess_templates';
	protected $primaryKey ='PATemplateID';

	protected $fillable  = array(
			'PATemplateName',
	);

	public $timestamps = false;
}