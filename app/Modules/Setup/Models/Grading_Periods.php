<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class Grading_Periods extends Model {

	public $table='es_gs_gradingperiods';
	protected $primaryKey ='PeriodID';

	protected $fillable  = array(
         'PeriodID'
        ,'PeriodCode'
        ,'Description1'
        ,'Description2'
        ,'GroupClass'
	);

	public $timestamps = false;
}