<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class RequiredTemplates extends Model {

	protected $table='esv2_requirementstemplate';
	protected $primaryKey ='TemplateID';

	protected $fillable  = array(
    	'TemplateName',
    	'ApplicationTypeID',
		'AppliedProgClassID',
		'AppliedGradeLevelID',
		'ForForeigner',
		'Inactive',
	);

	public $timestamps = false;

	public static function data() {
		return 
			DB::table('esv2_requirementstemplate as t')
				->join('es_program_class as p',
						'p.ClassCode','=','t.AppliedProgClassID'
					)
				->join('esv2_applicationtypes as a',
						'a.AppTypeID','=','t.ApplicationTypeID'
					);
	}
}