<?php
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class Department extends Model {

	public $table='es_departments';
	protected $primaryKey ='DeptID';

	protected $fillable  = array(
			'DeptCode',
			'DeptName',
			'CollegeID',
			'CampusID',
			'ShortName',
			'DeptHead_EmployeeID',
			'DeptVice_EmployeeID'
	);

	public $timestamps = false;

	public function getEmployee($empl_id)
	{
		$find = DB::table('HR_Employees')
                ->select(DB::raw('dbo.fn_EmployeeName(EmployeeID) as EmployeeName'))
              //->select(DB::raw('fn_EmployeeName3(EmployeeID) as EmployeeName'))
                ->where('EmployeeID', $empl_id)
                ->first();
        return $find->EmployeeName;
	}
}
