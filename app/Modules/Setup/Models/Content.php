<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class Content extends Model {

	protected $table='es_cms';
	protected $primaryKey ='ContentID';

	protected $fillable  = array(
			'ContentType',
			'ContentCode',
			'ContentName',
			'Title',
			'ShortDesc',
			'Content',
			'CreatedBy',
			'CreatedDate',
			'ModifiedBy',
			'ModifiedDate'
	);

	public $timestamps = false;

	public static function getTemplate($code) {
		return DB::table('es_cms')->where('ContentCode',$code)->first();
	}

	public static function getContentTemplate($code) {
		return DB::table('es_cms')->where('ContentCode',$code)->select('Content')->pluck('Content');
	}
}