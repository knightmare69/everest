<?php namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class CampusDeptHeads extends Model {

	protected $table='es_campus_dept_heads';
	protected $primaryKey ='DeptHeadsID ';

	protected $fillable  =  [ 
								'CampusID','Dept_Office','Dept_Name','Dept_Position'
							];

	public $timestamps = false;

	public function _save($id,$deptheads)
	{	
		if ( $id != '' )
		{
			$this->saveDeptHeads($id,$deptheads);
		}
	}

	private function saveDeptHeads($id,$deptheads)
	{
		foreach($deptheads as $depthead)
		{
			if ( $depthead['id'] == '' ) 
			{
				$data = array(
					'CampusID'		=> $id,
					'Dept_Office'	=> $depthead['offname'],
					'Dept_Name'		=> $depthead['headsname'],
					'Dept_Position'	=> $depthead['position'],
					
				);
				$this->create($data);
			}
			else
			{
				$data = array(
					'CampusID'		=> $id,
					'Dept_Office'	=> $depthead['offname'],
					'Dept_Name'		=> $depthead['headsname'],
					'Dept_Position'	=> $depthead['position'],
				);
				$this->where('DeptHeadsID',$depthead['id'])->update($data);
			}
		}
		return true;
	}

}