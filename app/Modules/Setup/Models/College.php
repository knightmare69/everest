<?php
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class College extends Model {

	public $table='ES_Colleges';
	protected $primaryKey ='CollegeID';

	protected $fillable  = array(
			'CampusID',
			'CollegeCode',
			'CollegeName',
			'CollegeDean',
			'CollegeNumberCode',
	);

	public $timestamps = false;

	public function getEmployee($empl_id)
	{
		$find = DB::table('HR_Employees')
                ->select(DB::raw('fn_EmployeeName3(EmployeeID) as EmployeeName'))
                ->where('EmployeeID', $empl_id)
                ->first();
        return $find->EmployeeName;
	}
}
