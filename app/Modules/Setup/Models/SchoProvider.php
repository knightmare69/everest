<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class SchoProvider extends Model {

	protected $table='es_schoproviders';
	protected $primaryKey ='SchoProviderID';

	protected $fillable  = array(
        'ProvCode',
        'ProvName',
        'ProvAcronym',
        'ProvShort',
        'IsGov'
       ,'IsAutoCredit' 
       ,'Inactive'
	);

	public $timestamps = false;
        
}