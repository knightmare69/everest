<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class UserGroup_ProgramAccess extends Model {

	protected $table='esv2_usergroup_programaccess';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
			'UserGroupID',
			'IsGroup',
			'ProgID',
	);

	public $timestamps = false;

	public static function get($key)
	{
		return
		DB::table('es_programs as p')
			->select([
				'p.ProgID',
				'ProgCode',
				'ProgName',
				'ProgShortName',
				'ProgYears',
				'ProgSems',
				DB::raw("(select count(*) from esv2_usergroup_programaccess a
						where UserGroupID='".$key."' and IsGroup=0
						and a.ProgID=p.ProgID limit 1
					) as isExist"
				)
			])
			->orderBy('ProgName','asc');
	}
}