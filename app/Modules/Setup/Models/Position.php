<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class Position extends Model {

	protected $table='hr_positiontitles';
	protected $primaryKey ='PosnTitleID';

	protected $fillable  = array(
			'PositionCode',
			'PositionDesc',
			'ShortName',
	);

	public $timestamps = false;
}