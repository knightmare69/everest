<?php
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class Buildings extends Model {

	public $table='ES_Buildings';
	protected $primaryKey ='BldgID';

	public function scopeBuildingName($query,$bldgid=0){
		return $query->where('BldgID',$bldgid)->pluck('BldgName');
	}
}
