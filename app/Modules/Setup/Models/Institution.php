<?php namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use App\Modules\Setup\Services\Institution\Validation;
use DB;

Class Institution extends Model {

	protected $table='es_institution';
	public $primaryKey ='InstID';

	protected $fillable  = array(   
        'InstClassID',
        'CampusCount',
        'PPCode',
        'HeadID',
        'HeadPosTitleID',
        'MapOwnershipClass',
        'FormerName1',
        'FormerName2',
        'FormerName3',
        'FormerName4',
        'FormerName5',
        'FormerName6',
        'FormerName7',
        'FormerName8',
        'InstLogo',
        'InstLicense',
        'UniqueID',
        'Address_Street',
        'Address_Municipality',
        'Address_CityProvince',
        'Address_Region',
        'Address_ZipCode',
        'TelNo',
        'FaxNo',
        'HeadTelNo',
        'EmailAddress',
        'WebSite',
        'YearEstablished',
        'LatestSecReg',
        'DateGranted',
        'YearConvertedCollege',
        'YearConvertedUniversity',
        'Report_RepublicTitle',
        'Report_CompleteAddress',
        'Report_InstitutionName',
        'LogoTop',
        'LogoLeft',
        'TitleFontName',
        'TitleFontSize',
        'TitleFontBold',
        'TitleFontItalic',
        'TitleHeight',
        'TitleWidth',
        'CompanyCode',
        'CompanyName',
        'InstLogo_png',
        'CRM_LicenseNo',
        'CRM_LicenseInfo'
	);

	public $timestamps = false;

  public function _save($post)
  {
    $validation = new Validation;
    $validate = $validation->validateSave($post);

    if ($validate->passes()) {
      $data = array(
          'InstClassID'           => getObjectValue($post, 'classinst'),
          'HeadID'                => getObjectValue($post, 'headinst'),
          'HeadPosTitleID'        => getObjectValue($post, 'headtitle'),
          'FormerName1'           => getObjectValue($post, 'name'),
          'UniqueID'              => getObjectValue($post, 'identifier'),
          'Address_Street'        => getObjectValue($post, 'street'),
          'Address_Municipality'  => getObjectValue($post, 'municipality'),
          'Address_CityProvince'  => getObjectValue($post, 'provincecity'),
          'Address_Region'        => getObjectValue($post, 'region'),
          'Address_ZipCode'       => getObjectValue($post, 'zipcode'),
          'TelNo'                 => getObjectValue($post, 'tel'),
          'FaxNo'                 => getObjectValue($post, 'fax'),
          'HeadTelNo'             => getObjectValue($post, 'headtel'),
          'EmailAddress'          => getObjectValue($post, 'email'),
          'WebSite'               => getObjectValue($post, 'website'),
          'YearEstablished'       => getObjectValue($post, 'yrestablished'),
          'LatestSecReg'          => getObjectValue($post, 'sec'),
          'DateGranted'           => getObjectValue($post, 'granted'),
          'YearConvertedCollege'  => getObjectValue($post, 'convcollege'),
          'YearConvertedUniversity' => getObjectValue($post, 'convuniversity'),
      );
            
      $status = $this->create($data);

      if ($status) 
      {
        $data = array_filter($data);
        $post['id'] = $this->where('UniqueID', $data['UniqueID'])->get()->toArray()[0]['InstID'];

        return ['error'=>false,'message'=>'','id'=>encode($post['id'])];
      }
    }

      return getModelMessage(true, getErrorMessages($validate->errors()->getMessages()));
  }

  public function _update($post)
  {
    $validation = new Validation;
    $validate = $validation->validateUpdate($post);

    if ( $validate->passes() ){
        $obj                    = $this->find(decode($post['key']));
        $obj->InstClassID       = $post['classinst'];
        $obj->HeadID            = $post['headinst'];
        $obj->HeadPosTitleID    = $post['headtitle'];
        $obj->FormerName1       = $post['name'];
        $obj->UniqueID          = $post['identifier'];
        $obj->Address_Street        = $post['street'];
        $obj->Address_Municipality  = $post['municipality'];
        $obj->Address_CityProvince  = $post['provincecity'];
        $obj->Address_Region        = $post['region'];
        $obj->Address_ZipCode        = $post['zipcode'];
        $obj->TelNo             = $post['tel'];
        $obj->FaxNo             = $post['fax'];
        $obj->HeadTelNo         = $post['headtel'];
        $obj->EmailAddress      = $post['email'];
        $obj->WebSite           = $post['website'];
        $obj->YearEstablished   = $post['yrestablished'];
        $obj->LatestSecReg      = $post['sec'];
        $obj->DateGranted       = $post['granted'];
        $obj->YearConvertedCollege   = $post['convcollege'];
        $obj->YearConvertedUniversity = $post['convuniversity'];

        $status = $obj->update();

      if ( $status )
      {
        return ['error'=>false,'message'=>'','id'=>$post['key']];    
      }
    }

      return getModelMessage(true,getErrorMessages($validate->errors()->getMessages()));
  }

}
