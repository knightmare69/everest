<?php
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class AcademicYearTerm extends Model {

	public $table='ES_AYTerm';
	protected $primaryKey ='TermID';

	protected $fillable  = array(
    	'AcademicYear',
    	'SchoolTerm',
    	'StartofAY',
    	'EndofAY',
    	'ExpiredReg',
    	'ExpireDays',
    	'Lock',
        'LastModified',
        'LastModifiedDate',
        'NumWeeks',
        'Hidden',
        'ExpireSaturday',
        'OverrideExpReg',
        'IsCurrentTerm',
        'ActiveOnlineEnrollment',
	);

	public $timestamps = false;

    public function scopeActiveterm($query){
        return $query->where('Hidden','0');
    }

    public static function scopeIsSummer($query, $term){

		return $query->where('SchoolTerm', 'Summer')
            ->orWhere('SchoolTerm', 'like', '%Summer%')
            ->where('TermID','=',$term);
    }

	public function isTermActive($term_id)
	{
		$regs_count = DB::table('ES_Registrations')->where('TermID', $term_id)->count();
		$r = !empty($regs_count);
		return $r;
	}

    public function getSchoolDays($t)
	{
		$r = DB::table('ES_PermanentRecord_SchoolDays')->where('TermID', $t)->first();
		return $r;
	}

    static public function updateSchoolDays($t, $data)
	{
        $tbl = 'ES_PermanentRecord_SchoolDays';
		$r = DB::table($tbl)->where('TermID', $t)->get();
        if( count($r)>0){
            DB::table($tbl)->where('TermID', $t)->update($data);
        }else{
            DB::table($tbl)->insert($data);
        }
		return $r;
	}

    public function getEnrollmentSchedules($term, $type ='')
	{

        $whereRaw = "TermID = {$term}";
        // dd($type)
        if($type != ''){
            $whereRaw .= " AND TypeID = {$type}"; 
        }

		$r = DB::table('es_enrollment_schedules')
                ->selectRaw("*,fn_AcademicYearTerm(TermID) As Term, fn_CollegeCode(CollegeID) As College, fn_ProgramCode(ProgramID) As Program, fn_YearLevel(LevelID) As Level ")
                    ->whereRaw($whereRaw)
                    //->where('TypeID', $type)
                    ->orderBy('ProgramID','Desc')
                    ->get();
		return $r;
	}
    
    public function deleteEnrollmentSched($sched)
	{
		$r = DB::table('es_enrollment_schedules')                
                    ->where('IndexID', $sched)
                    ->delete();
                    
        SystemLog('Enrollment','Enrollment Schedules','event','Remove Schedule','ScheduleID:'.$sched,'');
        
		return $r;
	}
    
    public function saveEnrollmentSchedules($data)
	{
	   $sched = getObjectValue($data,'SchedID');
       
       $t = decode(getObjectValue($data,'term'));
       $values = array(
            'TermID' => $t
           ,'TypeID' => getObjectValue($data,'type') 
           ,'CampusID' => getObjectValue($data,'campus') != '' ? decode(getObjectValue($data,'campus')) : 0 
           ,'CollegeID' => getObjectValue($data,'college') != '' ? decode(getObjectValue($data,'college')) : 0
           ,'ProgramID' => getObjectValue($data,'program') != '' ? decode(getObjectValue($data,'program')) : 0
           ,'LevelID' => getObjectValue($data,'gradeLevel') != '' ? decode(getObjectValue($data,'gradeLevel')) : 0
           ,'StartDate' => setDate( (getObjectValue($data,'period_from')),'Y-m-d')
           ,'FinishDate' => setDate( (getObjectValue($data,'period_to')),'Y-m-d')
           ,'LateDate' => setDate( (getObjectValue($data,'late_date')),'Y-m-d')
           ,'AcceptDate' => setDate((getObjectValue($data,'accept_date')),'Y-m-d')
           ,'Expiry' => getObjectValue($data,'exp_activate') != '' ? 1 : 0
           ,'ExpiryDays' => getObjectValue($data,'exp_days')
       );
       if($sched!='' && $sched != '0'){
        $r = DB::table('es_enrollment_schedules')->where('IndexID', decode($sched))->update($values);
        SystemLog('Enrollment','Enrollment Schedules','event','Update Schedule',json_encode($values),'');
        
       }else{
        $r = DB::table('es_enrollment_schedules')->insert($values);
        SystemLog('Enrollment','Enrollment Schedules','event','Insert Schedule',json_encode($values),'');         
       }
    
       return $r;
	}
	
    public static function appDrpSY() {
        $query = "select * FROM
            (
                select *
                FROM dbo.ES_AYTerm AS tbl1
                WHERE DATEPART(YEAR, tbl1.StartofAY) > DATEPART(YEAR, CURRENT_TIMESTAMP)
                UNION
                select *
                FROM
                (
                    select TOP (3)
                           *
                    FROM dbo.ES_AYTerm AS tbl2
                    WHERE DATEPART(YEAR, tbl2.StartofAY) <= DATEPART(YEAR, CURRENT_TIMESTAMP)
                    ORDER BY tbl2.AcademicYear DESC
                ) AS tbl3
            ) results WHERE results.Hidden = 0
            ORDER BY AcademicYear DESC";

        return DB::select(DB::raw($query));
    }

}
