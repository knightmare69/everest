<?php
namespace App\Modules\Setup\Models;
use DB;
use illuminate\Database\Eloquent\Model;

Class Medical extends Model {

	protected $table = 'esv2_admission_medicalschedules';
	protected $primaryKey = 'MedicalSchedID';

	protected $fillable = array('TermID', 'CampusID', 'YearLevelID', 'ProgClass', 'BatchName', 'IsSpecificDate', 'TestingDate', 'TestingDays', 'TimeFrom', 'TimeTo', 'Limit', 'Description');

	public $timestamps = false;

	public function get_view($filters = array())
	{
		// $get = DB::table('vw_AdmMedSchedules');

		$get = $this->select('*', DB::raw('fn_getTotalMedical(MedicalSchedID) AS Total, fn_K12_YearLevel(YearLevelID) as YearLevelName, fn_ProgramName(ProgClass) as ProgName'));

		if (!empty($filters)) {
			$get->whereRaw('TermID = ? AND CampusID = ?', $filters);

		} else {
			$active_term = DB::table('es_ayterm')->orderBy('AcademicYear', 'DESC')->orderBy('SchoolTerm', 'DESC')->first();
			$get->where('TermID', $active_term->TermID);

		}

		return $get->orderBy('YearLevelID', 'ASC')->get();
	}

	public function check_remove($id)
	{
		$find = DB::select(DB::raw('select fn_getTotalMedical('.$id.') as total'));

		return $find[0]->total;
	}
	public static function AcademicTerm()
	{
			$get = DB::table('es_ayterm')->select('TermID', 'AcademicYear', 'SchoolTerm')
			// ->where('SchoolTerm', 'School Year')
			->orderBy('AcademicYear', 'DESC')->get();

			return $get;
	}
}
