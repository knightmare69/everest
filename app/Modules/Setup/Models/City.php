<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;
Class City extends Model {

	protected $table='esv2_city';
	public $primaryKey ='CityID';

	protected $fillable  = array(
			'CountryID',
			'CountryCode',
			'City',		
	);

	public $timestamps = false;

	public static function data()
	{
		return
		DB::table('countries as c')
			->select('*')
			->leftJoin('esv2_city as city','city.CountryCode','=','c.CountryCode');
	}
}