<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class Group extends Model {

	protected $table='esv2_usersgroup';
	public $primaryKey ='GroupID';

	protected $fillable  = array(
			'GroupName',
			'GroupDesc',			
			'Inactive',
			'CreatedDate',
			'CreatedBy',
			'ModifiedBy',
			'ModifiedDate',
	);

	public $timestamps = false;
}