<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class Programs extends Model {

	protected $table='es_programs';
	protected $primaryKey ='ProgID';

	protected $fillable  = array(
			'CampusID',
			'CollegeID',
			'ProgCode',
			'ProgName',
			'ProgShortName',
			'ProgYears',
			'ProgSems',
			'MaxResidency',
			'TotalAcadSubject',
			'TotalAcadCreditUnits',
			'TotalNormalLoad',
			'TotalGenEdUnits',
			'TotalMajorUnits',
			'TotalElectiveUnits',
			'TotalLectureUnits',
			'TotalNonLectUnits',
			'ProgDiscipline',
			'ProgRecognize',
			'ProgRevise',
			'ProgClass',
			'ProgStatus',
			'ProgLadder',
			'ProgParent',
			'BoardExam',
			'ThesisReqID',
			'Semestral',
			'ProgChairID',
			'MajorDiscGroupCode',
	);

	public $timestamps = false;
     
}