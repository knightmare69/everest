<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class Country extends Model {

	protected $table='esv2_country';
	public $primaryKey ='CountryID';

	protected $fillable  = array(
			'Code',
			'Country',			
	);

	public $timestamps = false;
}