<?php 
namespace App\Modules\Setup\Models;

use illuminate\Database\Eloquent\Model;

Class ProgramClass extends Model {

	protected $table='es_program_class';
	protected $primaryKey ='ClassCode';

	protected $fillable  = array(
			'ClassDesc',
			'Shortname',
			'GroupClass',
	);

	public $timestamps = false;
}