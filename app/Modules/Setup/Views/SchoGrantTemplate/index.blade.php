<?php $at = getAYTerm(); ?>
<div class="row">
	<div class="col-md-12 col-sm-12">
	<div class="portlet light">
		<div class="tab-content">
			<div class="tab-pane active" id="tbprogs">
			   <div class="row">
				<div class="col-sm-4 tempinfo">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i>Template Info
							</div>
							<div class="tools">
								<button type="button" class="btn btn-sm btn-warning btnsave"><i class="fa fa-save"></i></button>
								<button type="button" class="btn btn-sm btn-default btnrefresh"><i class="fa fa-refresh"></i></button>
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-sm-12">
									<form id="frmdata" onsubmit="return false;">
									 	<div class="form-group">
									 		<label for="term">Academic Year Term</label>
						                    <select class="form-control" name="term" id="term">
						                        <option value="0" >- Select AY Term -</option>
						                        @foreach($at as $r)
						                        <option value="{{ $r->TermID }}">{{ $r->AYTerm }}</option>
						                        @endforeach
						                    </select>
						    			</div>
										<div class="form-group">
											<label for="code">Code</label>
											<input type="text" class="form-control" id="code" name="code"/>
										</div>
										<div class="form-group">
											<label for="name">Template</label>
											<input type="text" class="form-control" id="name" name="name"/>
										</div>
										<div class="form-group">
											<label for="desc">Description</label>
											<input type="text" class="form-control" id="desc" name="desc"/>
										</div>
										<div class="form-group">
											<label for="provider">Scho. Provider</label>
											<select class="form-control" id="provider" name="provider">
												<option value="-1" disabled selected> - Select One - </option>
												<?php
												$scho = DB::SELECT("SELECT * FROM ES_SchoProviders WHERE Inactive=0");
												foreach($scho as $r){
												 echo '<option value="'.$r->SchoProviderID.'">'.$r->ProvName.'</option>';
												}
												?>
											</select>
										</div>
										<div class="form-group">
											<label for="type">Type</label>
											<select class="form-control" id="type" name="type">
												<option value="0">Deduct Assessment</option>
												<option value="1">Replace Assessment</option>
											</select>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-8 listoftemp">
					<div class="portlet box blue dvtable">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i> List of Template
							</div>
							<div class="tools"></div>
							<div class="actions"></div>
						</div>
						<div class="portlet-body">
							<div class="row dvlist">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table class="table table-bordered table-condense" id="grantlist">
											<thead>
												<th><button class="btn btn-sm btn-success btnnew"><i class="fa fa-file"></i> New</button></th>
												<th>Template Code</th>
												<th>Template Name</th>
												<th>Provider</th>
												<th>Inactive</th>
											</thead>
											<tbody>
												@include($views.'tdata')
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row dvdetails hidden">
							    <div class="col-sm-12">
									<button type="button" class="btn btn-sm btn-danger btnremove pull-right"><i class="fa fa-times"></i></button>
									<button type="button" class="btn btn-sm btn-info btnadd pull-right"><i class="fa fa-plus"></i></button>
									<button type="button" class="btn btn-sm btn-refresh-list btnrefresh pull-right"><i class="fa fa-refresh"></i> View template</button>
									<!-- <button type="button" class="btn btn-sm btn-default btnrefresh"><i class="fa fa-refresh"></i></button> -->
								</div>
							    <div class="col-sm-12 acctform">
									<div class="col-sm-12 col-md-4">
										<label for="acctname">Account:<b class="acctcode"></b></label>
										<div class="input-group">
								       <!--  <span class="input-group-addon btnsearch"><i class="fa fa-search"></i></span>
										<input type="text" class="form-control" id="acctname" name="acctname" placeholder="Account Name" /> -->
											 <select id="acct" name="acct" class="form-control select2me" data-col="acct">
		                                <option value="-1">- select -</option>
		                                @foreach(\App\Modules\Accounting\Models\ChartOfAccounts::orderBy('AcctName','ASC')->get() as $a)
		                                <option value="{{$a->AcctID}}" acctcode="{{$a->AcctCode}}">{{$a->AcctName}}</option>
		                                @endforeach
		                            </select>
										</div>
									</div>
									<div class="col-sm-6 col-md-3">
										<label for="accttype">Adjustment</label>
										<select class="form-control" id="accttype" name="accttype">
											<option value="0">By Percentage</option>
											<option value="1">By Rate</option>
										</select>
									</div>
									<div class="col-sm-6 col-md-2" style="padding-left:1px !important;padding-right:1px !important;">
										<label for="rate">Amount</label>
										<input type="text" class="form-control numberonly text-right" id="rate" name="rate" value="0.00"/>
									</div>
									<div class="col-sm-12 col-md-3">
										<label for="remarks">Remarks</label>
										<input type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks"/>
									</div>
									<div class="col-sm-12"><hr/></div>
								</div>
							    <div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-condense" id="tbldetails" style="white-space:nowrap !important;">
										<thead>
											<th><button class="btn btn-sm btn-refresh-list"><i class="fa fa-refresh"></i> Refresh</button></th>
											<th class="text-center">Acct. Code</th>
											<th class="text-center">Acct. Name</th>
											<th class="text-center">Adjustment</th>
											<th class="text-center">Amount/Rate</th>
											<th class="text-center">Remarks</th>
										</thead>
										<tbody></tbody>
										<tfoot class="hidden">
											<tr data-id="tmp" data-acct="0">
											   <td><button class="btn btn-sm btn-danger btn-remove-list"><i class="fa fa-trash-o"></i> Remove</button></td>
											   <td class="text-center"></th>
											   <td class="text-center"></th>
											   <td class="text-center"></th>
											   <td class="text-right"></th>
											   <td></th>
											</tr>
										</tfoot>
									</table>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<div id="modal_filter" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select from the list</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="input-group">
				    <input class="form-control" id="txt-modal-search" data-button=".btn-modal-search" placeholder="StudentNo, LastName, Firstname" type="text">
					<span class="input-group-addon btn-modal-search" data-source="#txt-modal-search"><i class="fa fa-search"></i></span>
				  </div>
				  <div class="table-responsive modal_list" style="max-height:400px; overflow:scroll;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>Acct Code</th>
						    <th>Acct Name</th>
						    <th>Amount</th>
						</thead>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-modal-select">Select</button>
				</div>
			</div>
		</div>
	</div>
</div>