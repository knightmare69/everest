<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-edit"></i> Academic Year &amp; Term
        </div>
        <div class="tools">
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <div class="form_wrapper">
  		    @include($views.'forms.form')
        </div>
    </div>
</div>