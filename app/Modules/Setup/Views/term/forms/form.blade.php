<style>
.att{
    width: 30px;
    text-align: center;
}
</style>
<?php
$i=1;
$x=5; //Starting Month
$month_name = getMonths();
$model = new \App\Modules\Setup\Models\AcademicYearTerm;
$tid = isset($tid)? $tid:0; 
$schooldays = $model->getSchoolDays($tid);
$seqno=[];
$seqval=[];
if( isset($schooldays->SeqNo) ){
    if ($schooldays->SeqNo!= '') {
        $seqval = explode(",",$schooldays->SeqNo);        
        for($j=1;$j <= 12; $j++ ){            
            $seqno[$seqval[$j-1]] = $j  ;       
        }   
    }
}else{
    for($i=1;$i < 12; $i++ ){
        $seqno[$i] = $i ;       
    }
    $i=0;
}

?>
<table class="table table-striped table-bordered table-condensed table-selection" id="att-table" data-id="{{encode($tid)}}" style="cursor: pointer; margin: 0px; width: 100%; margin-bottom: 0px;">
	 <thead>
		<tr>                            
			<th class=""> </th>              
			<th class="text-center ">MONTHS</th>                                  
			<th class="text-center ">DAYS</th>
			<th class="text-center ">SEQ</th>
		</tr>
	</thead>
	<tbody>
		@for($i=1;$i <= 12; $i++ )
		<?php 
			$x++;
			$code = substr( strtoupper($month_name[$x]),0,3);                 
		?>
		<tr>
			<td class="autofit">{{$i}}.</td>
			<td class=" bold  months" data-id="{{$x}}">{{$month_name[$x]}}</td>
			<td width="70" class="" style="padding: 0px !important;" >
				<input type="text"  data-code="{{$code}}" class="days numberonly bold text-center input-no-border" value="<?= isset($schooldays->$code) ? $schooldays->$code : '0' ?>" />
			</td>
			<td width="70" class="" style="padding: 0px !important;" >
				<input type="text"  data-code="{{$i}}" class="seqno numberonly bold text-center input-no-border" value="<?=  isset($seqno[$i]) ? $seqno[$i] : 0 ?>" />
			</td>
		</tr>
		<?php if ($x>=12) $x=0; ?>
		@endfor
		<tr>
			<td colspan="4" class="text-right"> 
				<button type="button" class="btn green setup-btn-save-days">Update</button>
			</td>
		</tr>       
	</tbody>
</table>
