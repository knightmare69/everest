<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table table-bordered table-condense">
				<thead>
				    <tr>
						<th colspan="3">
							<select class="form-control">
								<option value="-1" selected> - All Programs - </option>
							</select>
						</th>
					</tr>
				    <tr>
						<th class="text-center">Period</th>
						<th class="text-center">Start</th>
						<th class="text-center">End</th>
					</tr>
				</thead>
				<tfoot>				
					<tr>
						<td colspan="3" class="text-right"> 
							<button type="button" class="btn green setup-btn-period">Save</button>
						</td>
					</tr>  
				</tfoot>
			</table>
		</div>
	</div>
</div>