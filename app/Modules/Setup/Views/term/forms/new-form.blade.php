<?php
$data = !empty($data) ? $data : [];

$dterm = getObjectValue($data, 'SchoolTerm');
$from = !empty($data->StartofAY) ? date('m/d/Y', strtotime($data->StartofAY)) : date('m/d/Y');
$to = !empty($data->EndofAY) ? date('m/d/Y', strtotime($data->EndofAY)) : '';
?>
<form class="setup-form" role="form" id="ayterm" data-id="{{ !empty($data->TermID) ? encode($data->TermID) : 0 }}">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Period</label>
                    <div class="input-group input-medium date-picker input-daterange" data-date-format="mm/dd/yyyy">
						<input type="text" class="form-control datepicker" name="period_from" id="period_from" value="{{ $from }}" data-date-start-date="{{ $from }}">
						<span class="input-group-addon">to</span>
						<input type="text" class="form-control datepicker" name="period_to" id="period_to" value="{{ $to }}">
					</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
					<label class="control-label">Academic Year</label>
                    <input type="text" class="form-control datatime" name="academic_year" id="academic_year" value="{{ getObjectValue($data, 'AcademicYear') }}" readonly >
                    <span class="help-block"></span>
				</div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
					<label class="control-label">School Term</label>
                    <select class="select2me select2-offscreen form-control setup-select2" name="school_term" id="school_term">
                        @forelse($terms as $a)
                        <?php $selected = ($a == $dterm) ? 'selected' : '' ?>
                        <option {{$selected}} value="{{ $a }}">{{ $a }}</option>
                        @empty
                        <option value="">Not Available</option>
                        @endforelse
                    </select>
                    <span class="help-block"></span>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
					<label class="control-label">No. of Weeks</label>
                    <input type="text" class="form-control" name="weeks" id="weeks" value="{{ getObjectValue($data, 'NumWeeks') }}" readonly >
                    <span class="help-block"></span>
				</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="checkbox-list">
                    <label class=""><input type="checkbox" name="lock" id="lock" {{ !empty($data->Lock) ? 'checked' : '' }}> Locked</label>
                    <label class=""><input type="checkbox" name="current_term" id="current_term" {{ !empty($data->IsCurrentTerm) ? 'checked' : '' }}> Current Term</label>
                    <label class=""><input type="checkbox" name="active_enroll" id="active_enroll" {{ !empty($data->Active_OnlineEnrolment) ? 'checked' : '' }}> Enrollment</label>
                </div>
            </div>
        </div>
    </div>
    @if($with_action)
    <hr>
    <div class="form-actions">
		<div class="row">
			<div class="col-md-12">
                <div class="right">
                    <button type="button" class="btn default setup-btn-cancel">Cancel</button>
                    <button type="submit" class="btn green setup-btn-save">Save</button>
                </div>
			</div>
		</div>
	</div>
    @endif
</form>
