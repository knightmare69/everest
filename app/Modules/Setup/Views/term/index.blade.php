<style media="screen">
    td {
        white-space: nowrap;
    }
</style>
<div class="row" id="tbl-rec-holder">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption"> <i class="fa fa-edit"></i> Academic Year & Term</div>
                <div class="action hide setup-tbl-action">
                    <div class="btn-group btn-group-solid" style="float: right">
                        <div class="input-group input-medium" style="float: left">
                            <input type="text" class="form-control setup-search-dt-txt">
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
                        <a class="btn red setup-btn-remove"><i class="fa fa-trash"></i></a>
                		<a class="btn blue setup-btn-new"><i class="fa fa-plus"></i></a>
                	</div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="tabbable-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#form-ayterm" data-toggle="tab">New</a>
                                </li>
                                <li class="">
                                    <a href="#form-days" data-toggle="tab">School Days</a>
                                </li>
                                <li class="">
                                    <a href="#form-period" data-toggle="tab">Period</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="form-ayterm">
                                    @include($views.'.forms.new-form', ['with_action' => true])
                                </div>
                                <div class="tab-pane" id="form-days">
                                    @include($views.'.forms.form', ['with_action' => true])
                                </div>
                                <div class="tab-pane" id="form-period">
                                    @include($views.'.forms.period', ['with_action' => true])
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="tabbable-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#table-ayterm" data-toggle="tab">Record(s)</a>
                        		</li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="table-ayterm">
                                    @include($views.'.tables.new-tbl')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
