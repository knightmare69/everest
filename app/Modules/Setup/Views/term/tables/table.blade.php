<?php
    $model = new \App\Modules\Setup\Models\AcademicYearTerm;
     
    $table = $model->orderBy('AcademicYear','desc')->get(); 
?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>Academic Year</th>
            <th>School Term</th>
            <th>Short Name</th>
            <th width="10">Status</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->TermID) }}"  class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td>{{ $row->AcademicYear }}</td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->SchoolTerm }}</a></td>
            <td>{{ $row->ShortName }}</td>
            <td class="center"><?= !$row->Lock ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
        </tr>
    @endforeach
    </tbody>
</table>