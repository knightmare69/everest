<table class="table table-striped table-bordered table-hover dataTable" id="ayterm-res-tbl">
    <thead>
        <tr>
            <th width="10px" class="bg-grey"></th>
            <th>Academic Year</th>
            <th>Term</th>
            <th>Period</th>
            <th>Locked</th>
            <th>Weeks</th>
            <th>Current Term</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($ayterm))
            @foreach($ayterm as $k => $d)
            <tr>
                <td class="setup-row-count">{{ $k + 1 }}</td>
                <td><a href="#" class="setup-tbl-dt" data-for="ayterm" data-id="{{ encode($d->TermID) }}">{{ $d->AcademicYear }}</a></td>
                <td>{{ $d->SchoolTerm }}</td>
                <td>{{ date('M. d, Y', strtotime($d->StartofAY)).' - '.date('M. d, Y', strtotime($d->EndofAY)) }}</td>
                <td>
                    <?php $clr = !empty($d->Lock) ? 'green' : 'grey'; ?>
                    <i class="fa fa-check-circle font-{{ $clr }}"></i>
                </td>
                <td>{{ $d->NumWeeks }}</td>
                <td>
                    <?php $clr = !empty($d->IsCurrentTerm) ? 'green' : 'grey'; ?>
                    <i class="fa fa-check-circle font-{{ $clr }}"></i>
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
