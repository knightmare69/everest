<?php
$info = isset($info)? $info:array();
?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST" autocomplete="off">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Code</label>
                    <input type="text" class="form-control" name="code"  maxlength="8" id="code" value="{{(isset($info) && isset($info->ProvCode))?$info->ProvCode:''}}">
                    @if (isset($info) && isset($info->SchoProviderID))
                    <input type="hidden" class="form-control" name="id" id="id" value="{{$info->SchoProviderID}}">
                    @endif
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{(isset($info) && isset($info->ProvName))?$info->ProvName:''}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Acronym</label>
                    <input type="text" class="form-control not-required" maxlength="8" name="acronym" id="acronym" value="{{(isset($info) && isset($info->ProvAcronym))?$info->ProvAcronym:''}}">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label class="control-label">Short Name</label>
                    <input type="text" class="form-control" name="shortname" id="shortname" value="{{(isset($info) && isset($info->ProvShort))?$info->ProvShort:''}}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Remarks</label>
            <input type="text" class="form-control not-required" name="remarks" id="remarks" value="{{(isset($info) && isset($info->Remarks))?$info->Remarks:''}}">
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
    				<label class="bold">Options</label>
    				<div class="checkbox-list">
    					<label><input type="checkbox" id="gov" name="gov" <?= getObjectValue($info,'IsGov')=='1'?'checked':''  ?> > Government Subsidy </label>
    					<label><input type="checkbox" id="billing" name="billing" <?= getObjectValue($info,'IsAutoCredit')=='1'?'checked':''  ?>  > Non Billing Financial Assistance</label>
                        <label><input type="checkbox" id="validate" name="validate" <?= getObjectValue($info,'ValidateReg')=='1'?'checked':''  ?>  > Validate Enrollment upon applies </label>
                        <label><input type="checkbox" id="inactive" name="inactive" <?= getObjectValue($info,'Inactive')=='1'?'checked':''  ?>  > Inactive </label>
    				</div>
    			</div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button"><i class="fa fa-clear"></i> Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
        <button class="btn default" data-dismiss="modal" type="button"><i class="fa fa-times"></i> Close</button>
    </div>
</form>
<!-- END FORM-->