<div class="row">
<div class="col-md-12">
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>Financial Assistance Provider
            </div>
            <div class="tools">

            </div>
            <div class="actions">
                <a data-toggle="dropdown" href="#" class="btn green dropdown-toggle" id="scho_provider_add_btn">
                    <i class="fa fa-plus"></i> Add</i>
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12" id="table_wrapper">
                <table class="table table-striped table-bordered table-condensed table-hover normal-font table_scroll" id="scho_provider">
                    <thead>
                        <tr>
                            <th>
                                Code  
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Acronym
                            </th>
                            <th>
                                Short Name
                            </th>
                            
                            <th class="autofit">Inactive</th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                   
                </table>
                <div id="action_str" style="display:none">
                    <a data-id="##ID##" class="btn btn-sm green dropdown-toggle scho_provider_update_btn">
                        <i class="fa fa-edit"></i> Edit</i>
                    </a>
                    <a data-id="##ID##" class="btn btn-sm red dropdown-toggle scho_provider_remove_btn">
                        <i class="fa fa-delete"></i> Delete</i>
                    </a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
