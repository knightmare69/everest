<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-9" style="padding-right: 5px !important;margin-bottom: 10px;">
						<div class="input-group">
							<input style="height: 30px !important;" type="text" placeholder='Search' name="filter" id="filter" class="not-required form-control custom-input-sm" value="">
							<span class="input-group-addon cursor-pointer span-search">
								<i class="fa fa-search"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 search-result-wrapper">
				@include($views.'sub.search_result')
			</div>
		</div>
	</div>
</div>