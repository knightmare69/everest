<?php $data = isset($data) ? $data : array(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form-setup" action="#" method="POST" data-form="institution">
    <div class="form-body">
        <div class="row">
          <div class="col-md-3">
              <div class="form-group">
                  <div class="fileinput {{ !empty($data->InstLogo) ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                           @if (!empty($data->InstLogo))
                          <img id="photo-attach" src="{{ url('general/getInsitutionalPhoto?InstID='.getObjectValue($data,'InstID')) }}" width="200" height="200" alt=""/>
                          @endif
                      </div>
                      <div>
                          <span class="btn default btn-file">
                          <span class="fileinput-new">
                          Select logo </span>
                          <span class="fileinput-exists">
                          Change </span>
                          <input type="file" name="..."  accept="image/x-png, image/jpeg" id="file" class="not-required">
                          </span>
                          <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                          Remove </a>
                      </div>
                  </div>
              </div>
          </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">Official Name of the Higher Education Institution <small><i class="text-danger">*</i></small></label>
                            <input type="text" class="form-control input-sm" name="name" id="name" value="{{ getObjectValue($data,'FormerName1') }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="btn-group btn-group-devided" data-toggle="buttons" style="float: right;">
                            <label class="btn red btn-outline btn-circle btn-sm btn-reset ">
                            <input type="radio" name="options" class="toggle"><i class="fa fa-angle-left"></i> Reset</label>
                            <label class="btn blue btn-outline btn-circle btn-sm btn-action btn-save">
                            <input type="radio" name="options" class="toggle"><i class="fa fa-check"></i> Save</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Classification of this Institution <small><i class="text-danger">*</i></small></label>
                            <select  class="select2 form-control input-sm not-required" name="classinst" id="classinst">
                                <option class="default-option" value=''>Select</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Head of this Institution <small><i class="text-danger">*</i></small></label>
                            <select  class="select2 form-control input-sm not-required" name="headinst" id="headinst">
                                <option class="default-option" value=''>Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Official Title of the Head <small><i class="text-danger">*</i></small></label>
                            <select  class="select2 form-control input-sm not-required" name="headtitle" id="headtitle">
                                <option class="default-option" value=''>Select</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Unique Identifier <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control input-sm" name="identifier" id="identifier" value="{{ getObjectValue($data,'UniqueID') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Address <small><i class="text-danger">*</i></small></label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="street" id="street" value="{{ getObjectValue($data,'Address_Street') }}">
                    <span class="help-block">&nbsp;(Street)</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="municipality" id="municipality" value="{{ getObjectValue($data,'Address_Municipality') }}">
                    <span class="help-block">&nbsp;(Municipality)</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="provincecity" id="provincecity" value="{{ getObjectValue($data,'Address_CityProvince') }}">
                    <span class="help-block">&nbsp;(Province / City)</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="region" id="region" value="{{ getObjectValue($data,'Address_Region') }}">
                    <span class="help-block">&nbsp;(Region)</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="zipcode" id="zipcode" value="{{ getObjectValue($data,'Address_ZipCode') }}">
                    <span class="help-block">&nbsp;(Zip Code)</span>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Tel No. <small><i class="text-danger">*</i></small></label>
                    <input type="text" name="tel" id="tel" class="form-control input-sm" value="{{ getObjectValue($data,'TelNo') }}">
                    <span class="help-block">&nbsp;* Include Area Code(e.g.(02) 366-43234)</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Fax No. <small><i class="text-danger">*</i></small></label>
                    <input type="text" name="fax" id="fax" class="form-control input-sm" value="{{ getObjectValue($data,'FaxNo') }}">
                    <span class="help-block">&nbsp;* Include Area Code</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Head's Tel No. <small><i class="text-danger">*</i></small></label>
                    <input type="text" name="headtel" id="headtel" class="form-control input-sm" value="{{ getObjectValue($data,'HeadTelNo') }}">
                    <span class="help-block">&nbsp;* Include Area Code</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Email Address <small><i class="text-danger">*</i></small></label>
                    <input type="email" class="form-control input-sm" name="email" id="email" value="{{ getObjectValue($data,'EmailAddress') }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Website <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control input-sm" name="website" id="website" value="{{ getObjectValue($data,'WebSite') }}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">Year Established <small><i class="text-danger">*</i></small></label>
                    <input type="number" class="form-control input-sm" name="yrestablished" id="yrestablished" value="{{ getObjectValue($data,'YearEstablished') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Latest SEC Registration <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control input-sm" name="sec" id="sec" value="{{ getObjectValue($data,'LatestSecReg') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Date Granted / Approved ? <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control input-sm" name="granted" id="granted" value="{{ getObjectValue($data,'DateGranted') }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Year Converted to College <small><i class="text-danger">*</i></small></label>
                    <input type="number" class="form-control input-sm" name="convcollege" id="convcollege" value="{{ getObjectValue($data,'YearConvertedCollege') }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Year Converted to University <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control input-sm" name="convuniversity" id="convuniversity" value="{{ getObjectValue($data,'YearConvertedUniversity') }}">
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->
