<?php $data = isset($data) ? $data : array(); ?>
<div class="scroller" style="position: relative; overflow: hidden; width: 100% !important; height: 350px;">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr class="heading">
				<th width="20"><small>#</small></th>
				<th width="150" class="center"><small>Unique Identifier</small></th>
				<th class="center"><small>Name</small></th>
				<th width="200" class="center"><small>Email Address</small></th>
			</tr>
		</thead>
		<tbody>
		{{--*/$i=1/*--}}
		@foreach($data as $row)
			<tr data-id="{{ encode($row->InstID) }}" class="cursor-pointer">
				<td><small>{{ $i++ }}</small></td>
				<td class="a-select"><small>{{ $row->UniqueID }}</small></td>
				<td class="a-select"><small>{{ ucfirst($row->FormerName1) }}</small></td>
				<td class="a-select"><small>{{ $row->EmailAddress }}</small></td>
			</tr>
		@endforeach
		@if ( count($data) <= 0 )
			<tr>
				<td colspan="4" align="center">No Result Found!</td>
			</tr>
		@endif
		</tbody>
	</table>
</div>