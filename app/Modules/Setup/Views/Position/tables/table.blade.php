<?php $table = isset($table) ? $table : array(); ?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>Code</th>
            <th>Position Title</th>
            <th>Short Name</th>            
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->PosnTitleID) }}"  data-status="" class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td>{{ $row->PositionCode }}</td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->PositionDesc }}</a></td>
            <td>{{ $row->ShortName }}</td>
           
        </tr>
    @endforeach
    </tbody>
</table>