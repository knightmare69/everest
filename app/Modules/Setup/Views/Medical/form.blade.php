<?php
$data = isset($data) ? $data : array();
$xpl_days = array();
if (!empty($data)) {
    if ($data->IsSpecificDate == 0) {
        $xpl_days = explode(',', $data->TestingDays);
    }
}
// error_print($data);
 ?>
<form class="mt-form" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="bc">Batch Code</label>
                    <input type="text" name="bc" id="bc" value="{{ getObjectValue($data,'BatchName') }}" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="app">Application Type</label>
                    <select class="form-control select2" name="app" id="app">
                        @if(!empty($grade_level))
                            <option value=""></option>
                            @foreach($grade_level as $gl)
                            <optgroup label="{{ $gl['ProgClassName'] }}" data-id="{{ encode($gl['ProgClassID']) }}">
                                @foreach($gl['Programs'] as $progs)
                                <?php $stat = ($progs->YearLevelID == getObjectValue($data, 'YearLevelID') && $progs->ProgClass == getObjectValue($data, 'ProgClass')) ? 'selected' : ''; ?>
                                <option data-id="{{ encode($progs->YearLevelID) }}" {{ $stat }}>{{ $progs->YearLevelName }}</option>
                                @endforeach
                            </optgroup>
                            @endforeach
                        @else
                        <option value="">No selection.</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <!--/row-->
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Specific Date?</label> <input type="checkbox" name="is-days" id="is-days" value="<?= !empty($data->IsSpecificDate) ? $data->IsSpecificDate : 0?>" <?= !empty($data->IsSpecificDate) ? 'checked' : ''?> >
                    <div class="checkbox-list <?= !empty($data->IsSpecificDate) ? 'hide' : ''?>" id="test-days">
                        <?php $days = array('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'); ?>
                        <div class="row">
                            @for($a = 0; $a < count($days); $a++)
                                <?php $count = $a + 1; ?>
                                @if($a % 2)
                                <label><input type="checkbox" name="test-days[]" value="{{ $count }}" <?= in_array((string) $count, $xpl_days) ? 'checked' : ''?> > {{ $days[$a]}}</label>
                                </div>
                                @else
                                <div class="col-md-3">
                                    <label><input type="checkbox" name="test-days[]" value="{{ $count }}" <?= in_array((string) $count, $xpl_days) ? 'checked' : ''?>> {{ $days[$a]}}</label>
                                @endif
                            @endfor
                                </div>
                        </div>
					</div>
                    <div class="input-icon <?= !empty($data->IsSpecificDate) ? '' : 'hide'?>">
                        <i class="fa fa-calendar"></i>
                        <input type="text" class="form-control date-picker" name="test-date" id="test-date" value="{{ date('m/d/Y') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="limit">Limit</label>
                    <input type="text" class="form-control numberonly not-required" name="limit" value="<?= !empty($data->Limit) ? $data->Limit : '' ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="time-start">Time Start</label>
                    <div class="input-icon">
                        <i class="fa fa-clock-o"></i>
                        <input type="text" class="form-control timepicker timepicker-no-seconds" value="<?= !empty($data->TimeFrom) ? date('g:i A', strtotime($data->TimeFrom)) : '9:00 AM' ?>" name="time-start">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="time-end">Time End</label>
                    <div class="input-icon">
                        <i class="fa fa-clock-o"></i>
                        <input type="text" class="form-control timepicker timepicker-no-seconds" value="<?= !empty($data->TimeTo) ? date('g:i A', strtotime($data->TimeTo)) : '10:00 AM' ?>" name="time-end">
                    </div>
                </div>
            </div>
        </div>
        <!--/row-->
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn green btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
