<?php
// if(!empty($data)){
//
// }
$cnt = 1;
?>
<style>
    table{

        border:1px solid #ddd;
    }
    .thead-style{
        border:1px solid #ddd;
        padding:8px;
    }
</style>
<h2>{{ $label }} Schedules</h2>
<table >
    <thead>
        <tr>
            <th class="thead-style" style="width:5%"> #</th>
            <th class="thead-style" style="width:20%"> Grade Level</th>
            <th class="thead-style" style="width:15%"> Batch Code</th>
            <th class="thead-style" style="width:15%"> Date</th>
            <th class="thead-style" style="width:25%"> Time</th>
            <th class="thead-style" style="width:10%"> Limit</th>
            <th class="thead-style" style="width:10%"> Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $_this)
        <?php $explode = explode(' ', $_this->Description, 2); $date = $explode[0]; $time = $explode[1];?>
        <tr>
            <td class="thead-style" style="width:5%"> {{ $cnt }}</td>
            <td class="thead-style" style="width:20%"> {{ $_this->YearLevel }}</td>
            <td class="thead-style" style="width:15%"> {{ $_this->BatchName }}</td>
            <td class="thead-style" style="width:15%"> {{ $date }}</td>
            <td class="thead-style" style="width:25%"> {{ $time }}</td>
            <td class="thead-style" style="width:10%; text-align:center"> {{ $_this->Limit }}</td>
            <td class="thead-style" style="width: 10%; text-align:center"> {{ $_this->Total }}</td>
        </tr>
        <?php $cnt++?>
        @endforeach
    </tbody>
</table>
