<?php
if(!empty($table)){
    // echo "<pre>";
    // print_r($table);
    // echo "</pre>";

}
$i = 1;
$days = array('1' => 'M', '2' => 'T', '3' => 'W', '4' => 'TH', '5' => 'F', '6' => 'SAT', '7' => 'SUN');

?>
<table class="table table-striped table-bordered table-hover table_scroll dataTable">
    <thead>
        <tr>
            <th>#</th>
            <th>Grade Level</th>
            <th>Program</th>
            <th>Batch Code</th>
            <th>{{ $form }} Date/Time</th>
            <th>Limit</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($table))
            @foreach($table as $data)
            <?php
            if(!empty($data->MedicalSchedID)){
                $id = $data->MedicalSchedID;
            } else if(!empty($data->TestingSchedID)){
                $id = $data->TestingSchedID;
            }
            ?>
            <tr data-id="{{ encode($id) }}">
                <td>{{ $i }}</td>
                <td>{{ $data->YearLevelName }}</td>
                <td>{{ !empty($data->ProgName) ? $data->ProgName : 'None' }}</td>
                <td><a href="#" class="a_select">{{ $data->BatchName }}</a></td>
                <td>{{ $data->Description }}</td>
                <td>
                    @if(empty($data->Limit))
                    {{ 'No limit.' }}
                    @elseif(!empty($data->Limit) && $data->Limit == $data->Total)
                    <span class="text-danger">{{ empty($data->Limit) ? 'No limit.' : $data->Total.'/'.$data->Limit }}</span>
                    @else
                    {{ $data->Total.'/'.$data->Limit }}
                    @endif
                </td>
                <td>
                    @if($data->Total == 0)
                        <a href="#" onclick="return false;" class="remove text-danger"><i class="fa fa-trash-o"></i></a>
                    @endif
                </td>
            </tr>
            <?php $i++; ?>
            @endforeach
        @endif
    </tbody>
</table>
