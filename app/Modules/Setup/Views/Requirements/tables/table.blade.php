<?php 
    if (!Request::get('key'))
        $table = App\Modules\Setup\Models\Requirements::get();
    else 
    $table = App\Modules\Setup\Models\RequiredTemplatesItems::data(decode(Request::get('key')))->get();

?>
<table class="table table-striped table-bordered table-condensed table-hover normal-font table_scroll" id="TableRequirements">
    <thead>
        <tr>
            <th colspan="4">
                <label class="label-control">Template</label>
                <select class="form-control" id="Template">
                    <option value="">Select</option>
                    @foreach(App\Modules\Setup\Models\RequiredTemplates::data()->where('t.Inactive','0')->get() as $row)
                    <option {{ decode(Request::get('key')) == $row->TemplateID ? 'selected' : '' }} value="{{ encode($row->TemplateID) }}">{{ ucfirst($row->ClassDesc).' - '.ucfirst($row->TemplateName).' - '.$row->ApplicationType }}</option>
                    @endforeach
                </select>
            </th>
        </tr>
        <tr>
            <th width="3%"><input type="checkbox" class="chk-header"></th>
            <th>Description</th>
            <th width="10%">Code</th>
            <th width="5%">Required?</th>
            <th width="5%">Active?</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->RequirementID) }}"  data-status="{{ $row->Inactive }}" class="without-bg">
            <td><input type="checkbox" class="chk-child" {{ $row->TemplateItemID ? 'checked' : '' }}></td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->Desc }}</a></td>
            <td><input type="text" class="form-control Code" value="{{ $row->Code }}"></td>
            <td class="center">
                <input type="checkbox" class="form-control IsRequired" name="IsRequired" {{ $row->IsRequired ? 'checked' : '' }}>
            </td>
            <td class="center"><?= !$row->Inactive ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
        </tr>
    @endforeach
    </tbody>
</table>