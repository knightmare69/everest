<?php $data = isset($data) ? $data : array(); ?>
<?php
function isVisibleAppType($id) {
    if (in_array($id,AppConfig()->parentAppTypeIDs())) {
        return true;
    }
    return false;
}
?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Is Foreigner</label>
                    <input type="checkbox" class="" name="isForeigner" id="isForeigner">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Application type</label>
                    <div class="col-md-12">
                        <select class="form-control" name="applicationType" id="applicationType">
                            <option value="0">- Select -</option>
                            @foreach(App\Modules\Admission\Models\Admission::getApplicationType() as $row)
                                @if(isVisibleAppType($row->AppTypeID))
                                <option value="{{$row->AppTypeID}}">{{ $row->ApplicationType }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Program Class</label>
                    <div class="col-md-12">
                        <select class="form-control" name="programClass" id="programClass">
                            <option value="0">- Select -</option>
                            @foreach(App\Modules\Setup\Models\ProgramClass::whereIn('ClassCode',[11,20,50])->get() as $row)
                                <option value="{{ $row->ClassCode }}">{{ $row->ClassDesc }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Template</label>
                    <input type="text" class="template form-control" name="template">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
<!-- END FORM-->