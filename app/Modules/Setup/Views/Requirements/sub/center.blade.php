<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Manage
        </div>
        <div class="tools">

        </div>
        <div class="actions">
            <a data-toggle="dropdown" href="#" class="btn green dropdown-toggle" id="BtnSaveTemplate">
                <i class="fa fa-save"></i> Save</i>
            </a>
            <div class="btn-group">
                <a data-toggle="dropdown" href="#" class="btn yellow dropdown-toggle">
                <i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                   <li>
                        <a href="javascript:void(0)" id="aAddNewTemplate">
                        <i class="fa fa-check  font-blue"></i> Add new Template</a>
                    </li>
                     <li>
                        <a href="javascript:void(0)" id="aAddNewRequirement">
                        <i class="fa fa-check  font-blue"></i> Add new Requirements</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body">
    	<div class="row">
	       	<div class="col-md-12" id="table_wrapper">
                @include($views.'tables.table')
			</div>
		</div>
    </div>
</div>
