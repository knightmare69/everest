<?php
 $gsmodel   = new \App\Modules\Setup\Models\GradingSystemSetup;
 $gmodel    = new \App\Modules\GradingSystem\Models\Grading\Settings;
 $tmodel    = new \App\Modules\GradingSystem\Models\Transmutation\Transmutation_model;
 $gsystem   = $gmodel->orderBy('TemplateID','desc')->get();
 $transmute = $tmodel->orderBy('TranstblID','desc')->get();
 $value     = 0;
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"></div>
		<div class="tools">				                                        
            <button class="btn btn-sm btn-warning btn-save"><i class="fa fa-save"></i> Save</button>
            <button class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i> Delete</button>
            <button class="btn btn-sm btn-default btn-cancel">Cancel</button>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row" style="margin-bottom:5px;">
		  <form role="form" id="frmGradeSetup" onsubmit="return false;">
		    <div class="form-group">
			  <label>YearLevel:</label>
			   <select class="form-control" id="yrlvl" name="yrlvl">
			     @foreach($yrlvl as $r)
				    <option value="{{ $r->YLID_OldValue }}" data-progid="{{ $r->ProgID }}">{{$r->YearLevelName}}</option>
				 @endforeach
			   </select>
		  	</div>
		    <div class="form-group hidden">
			   <label>ProgID:</label>
			   <select class="form-control read-only" id="progid" name="progid">
			     @foreach($progid as $r)
				    <option value="{{ $r->ProgID }}">{{$r->ProgName}}</option>
				 @endforeach
			   </select>
		  	</div>
		    <div class="form-group" hidden>
			  <label>MajorID:</label>
			   <select class="form-control" id="major" name="major">
			    <option value="-1" selected disabled>-Select one-</option>
			   </select>
		  	</div>
		    <div class="form-group">
			  <label>Transmutation:</label>
			   <select class="form-control" id="transmute" name="transmute">
			     @foreach($transmute as $r)
				  <option value="{{ $r->TranstblID }}" <?php echo(($r->TranstblID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
			   </select>
		  	</div>
		    <div class="form-group">
			  <label>Option:</label>
			   <select class="form-control" id="transopt" name="transopt">
			     <option value="0" selected>Average Only</option>
			     <option value="1" selected>Event Only</option>
			   </select>
		  	</div>
		    <div class="form-group">
			  <label>Letter Grade:</label>
			   <select class="form-control" id="gradsys" name="gradsys">
			     @foreach($gsystem as $r)
				  <option value="{{ $r->TemplateID }}" <?php echo(($r->TemplateID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
			   </select>
		  	</div>
		  </form>
		</div>
	</div>
</div>	