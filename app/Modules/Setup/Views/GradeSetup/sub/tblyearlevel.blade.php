<?php
$gsmodel   = new \App\Modules\Setup\Models\GradingSystemSetup;
$amodel    = new \App\Modules\Setup\Models\AcademicYearTerm;
$gmodel    = new \App\Modules\GradingSystem\Models\Grading\Settings;
$tmodel    = new \App\Modules\GradingSystem\Models\Transmutation\Transmutation_model;
$ayterm    = $amodel->orderBy('AcademicYear','desc')->get(); 
$gsystem   = $gmodel->orderBy('TemplateID','desc')->get();
$transmute = $tmodel->orderBy('TranstblID','desc')->get();
$value     = 0;
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"> 
            <i class="fa fa-trophy"></i>
            <span class="caption-subject bold "> Grading System Setup </span><br />
            <span class="caption-helper"> Use this module to configure grading system...</span>
        </div>
		<div class="tools">				                                        
            <button class="btn btn-sm blue btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row" style="margin-bottom:5px;">
		 <div class="col-sm-4">
		   <label>Campus:</label>
		   <select class="form-control" id="campus">
		     @foreach($campus as $r)
			   <option value="{{ $r->CampusID }}">{{$r->ShortName}}</option>
		     @endforeach
		   </select>
		 </div>
		 <div class="col-sm-6">
		   <label>AY Term:</label>
		   <select class="form-control" id="ayterm">
		     <?php
		      $value = $gsmodel->CurrentTermID();
		      $value = ((is_object($value))?0:$value);
		     ?>
		     @foreach($ayterm as $r)
			   <option value="{{ $r->TermID }}" <?php echo (($r->TermID==$value)?'selected':'');?>>{{ $r->AcademicYear.' '.$r->SchoolTerm }}</option>
		     @endforeach
		   </select>
		 </div>
		</div> 
        <div class="row">
		  <div class="col-sm-12">
			 <div class="table-responsive">
			 <table id="tblyrlvl" class="table table-bordered table-condense table-hover'" style="white-space:nowrap;">
			  <thead>
				<th>YearLevel</th>
				<th>Transmutation</th>
				<th>Option</th>
				<th>Letter Grade</th>
				<th class="hidden">Conduct</th>
				<th class="hidden">HomeRoom</th>
				<th class="hidden">Club Policy</th>
			  </thead>
			  <tbody>
			   <?php
				 foreach($yrlvl as $y){
				   $opt = '';
				   switch($y->TransmuteOption){
					  case '0':
					    $opt = 'Average Only';
                      break;
					  case '1':
					    $opt = 'Event Only';
                      break;					  
				   }
				   echo '<tr data-id="'.$y->YLID_OldValue.'" data-prog="'.$y->ProgID.'">
				            <td>'.$y->YearLevelName.'</td>
							<td>'.$y->Transmutation.'</td>
							<td>'.$opt.'</td>
							<td>'.$y->LetterGrade.'</td>
							<td class="hidden"></td>
							<td class="hidden"></td>
							<td class="hidden"></td>
						 </tr>';	 
				 } 
			   ?>
			  </tbody>
			 </table>
			 </div>
		  </div>
		</div>
    </div>
</div>	
 		