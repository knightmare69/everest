<?php
    $gsmodel   = new \App\Modules\Setup\Models\GradingSystemSetup;
    $amodel    = new \App\Modules\Setup\Models\AcademicYearTerm;
	$gmodel    = new \App\Modules\GradingSystem\Models\Grading\Settings;
	$tmodel    = new \App\Modules\GradingSystem\Models\Transmutation\Transmutation_model;
    $ayterm    = $amodel->orderBy('AcademicYear','desc')->get(); 
	$gsystem   = $gmodel->orderBy('TemplateID','desc')->get();
	$transmute = $tmodel->orderBy('TranstblID','desc')->get();
	$value     = 0;
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"> 
            <i class="fa fa-trophy"></i>
            <span class="caption-subject bold "> Grading System Setup </span><br />
            <span class="caption-helper"> Use this module to configure grading system...</span>
        </div>
		<div class="tools">				                                        
            <button class="btn btn-sm btn-success btn-save"><i class="fa fa-save"></i> Save</button>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row">
		 <div class="table-responsive">
		 <table class="table table-bordered table-condense'">
		  <thead>
		    <th>Description</th>
		    <th>Value</th>
		  </thead>
		  <tbody>
		    <tr>
			  <td>Current School Year</td>
			  <td>
			    <select class="form-control" data-id="6" data-desc="Current School Year">
				 <?php
				   $value = $gsmodel->CurrentTermID();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($ayterm as $r)
				  <option value="{{ $r->TermID }}" <?php echo (($r->TermID==$value)?'selected':'');?>>{{ $r->AcademicYear.' '.$r->SchoolTerm }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr>
			  <td>Current SHS Term</td>
			  <td>
			    <select class="form-control" data-id="10" data-desc="Current SHS Term">
				 <?php
				   $value = $gsmodel->SHSCurrentTerm();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($ayterm as $r)
				  <option value="{{ $r->TermID }}" <?php echo (($r->TermID==$value)?'selected':'');?>>{{ $r->AcademicYear.' '.$r->SchoolTerm }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr>
			  <td>Grading System</td>
			  <td>
			    <select class="form-control" data-id="1" data-desc="Grading System">
				 <?php
				   $value = $gsmodel->GradingSystemID();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($gsystem as $r)
				  <option value="{{ $r->TemplateID }}" <?php echo(($r->TemplateID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr>
			  <td>Conduct</td>
			  <td>
			    <select class="form-control" data-id="2" data-desc="Conduct">
				 <?php
				   $value = $gsmodel->ConductID();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($gsystem as $r)
				  <option value="{{ $r->TemplateID }}" <?php echo(($r->TemplateID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr>
			  <td>Home Room</td>
			  <td>
			    <select class="form-control" data-id="4" data-desc="Home Room">
				 <?php
				   $value = $gsmodel->HomeRoomtbl();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($gsystem as $r)
				  <option value="{{ $r->TemplateID }}" <?php echo(($r->TemplateID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr>
			  <td>Club</td>
			  <td>
			    <select class="form-control" data-id="5" data-desc="Club">
				 <?php
				   $value = $gsmodel->ClubID();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($gsystem as $r)
				  <option value="{{ $r->TemplateID }}" <?php echo(($r->TemplateID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr>
			  <td>Transmutation</td>
			  <td>
			    <select class="form-control" data-id="3" data-desc="Transmutation">
				 <?php
				   $value = $gsmodel->TransmutationID();
				   $value = ((is_object($value))?0:$value);
				 ?>
				 @foreach($transmute as $r)
				  <option value="{{ $r->TranstblID }}" <?php echo(($r->TranstblID==$value)?'selected':'');?>>{{ $r->TemplateName }}</option>
				 @endforeach
				</select>
			  </td>
			</tr>
		    <tr><td>Club Policy</td><td></td></tr>
		    <tr>
			  <td>Nursery School</td>
			  <td>
			    <?php
				   $value = $gsmodel->IsSHS();
				   $value = ((is_object($value))?0:$value);
				 ?>
             	<input type="text" class="form-control" data-id="8" data-desc="Nursery School" value="1"/> 			 
			  </td>
			</tr>
		    <tr>
			  <td>Senior High School</td>
			  <td>  
			     <?php
				   $value = $gsmodel->IsSHS();
				   $value = ((is_object($value))?0:$value);
				 ?>
             	<input type="text" class="form-control" data-id="7" data-desc="SHS" value="{{$value}}"/> 			 
			  </td>
			</tr>
		  </tbody>
		 </table>
		 </div>
		</div>
	</div>
</div>