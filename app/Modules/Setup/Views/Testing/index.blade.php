<div class="row">
    <div class="col-md-8">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Schedules
                </div>
                <div class="actions">
                    <div class="form-inline">
                        <div class="form-group">
                            <select name="search-ac-year" id="search-ac-year" value="" class="form-control select2">
                            @if(!empty($at))
                                @foreach($at as $ayt)
                                    <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' '.$ayt->SchoolTerm }}</option>
                                @endforeach
                            @else
                            <option value="">No selection.</option>
                            @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control select2" name="search-campus" id="search-campus">
                                @if(!empty($campus))
                                    @foreach($campus as $_campus)
                                    <option value="{{ encode($_campus->CampusID) }}">{{ $_campus->Acronym }}</option>
                                    @endforeach
                                @else
                                <option value="">No selection.</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <a href="{{ url('setup/testing/print') }}" class="btn btn-default"><i class="fa fa-print"></i></a>
                        </div>
                    </div>
				</div>
            </div>
            <div class="portlet-body table_wrapper">
                @include('Setup.Views.Medical.list')
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i>Form
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                @include('Setup.Views.Medical.form')
            </div>
        </div>

    </div>
</div>
