<div class="portlet box green-haze tasks-widget">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-bell-o"></i>Manage Dashboard Content and Email Templates
		</div>
		<div class="actions">
			<a href="{{ url('cms/new') }}" class="btn yellow btn-sm">Add New</a>
			<div class="btn-group">
				<a class="btn btn-sm btn-default dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				Filter By <i class="fa fa-angle-down"></i>
				</a>
				<div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
					<label><input type="checkbox"/> K12</label>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		@include($views.'table')
	</div>
</div>