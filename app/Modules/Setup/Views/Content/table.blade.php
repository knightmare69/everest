<?php $table = \App\Modules\Setup\Models\Content::get(); ?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th>Type</th>
            <th>Code</th>
            <th>Title</th>   
            <th>Short Description</th>            
            <th width="10">Action</th>
        </tr>
    </thead>
    <tbody>
   <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->ContentID) }}" class="without-bg">
            <td><b>{{ $row->ContentType }}</b></td>
            <td><b>{{ $row->ContentCode }}</b></td>
            <td>{{ $row->Title }}</td>
            <td>{{ $row->ShortDesc }}</td>
            <td><a href="{{ url('cms/edit/'.$row->ContentID) }}" class="btn btn-sm btn-grey btn-edit">Edit</a></td>
        </tr>
    @endforeach
    </tbody>
</table>