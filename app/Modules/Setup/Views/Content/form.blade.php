<?php $data = \App\Modules\Setup\Models\Content::where('ContentID',$key)->first(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <input type="hidden" name="key" id="key" value="{{ $key }}">
    <div class="form-actions right">
        <a href="{{ url('cms') }}" class="btn default" type="button">Back</a>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label bold">Code</label>
                    <input type="text" maxlength="20" {{ $key ? 'readonly' : '' }} placeholder="Enter Title Here" class="form-control input-xs" name="code" id="code" value="{{ getObjectValue($data,'ContentCode') }}">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label bold">Type</label>
                    <select class="form-control" name="type" id="type">
                        <option {{ getObjectValue($data,'ContentType') == 'K12' ? 'selected' : '' }} value="K12">K12</option>
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
      
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label bold">Title</label>
                    <input type="text" placeholder="Enter Title Here" class="form-control input-xs not-required" name="Title" id="Title" value="{{ getObjectValue($data,'Title') }}">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label bold">Short Description</label>
                    <textarea class="form-control not-required" placeholder="Enter Short Desciption Here" name="ShortDesc" id="ShortDesc">{{ getObjectValue($data,'ShortDesc') }}</textarea>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label bold">Content</label>
                    <textarea class="form-control not-required" rows="30" placeholder="Enter Content Here" id="Content" name="Content">{{ getObjectValue($data,'Content') }}</textarea>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
    </div>
    <div class="form-actions right">
        <a href="{{ url('setup/maintenance/content') }}" class="btn default" type="button">Back</a>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
<!-- END FORM-->