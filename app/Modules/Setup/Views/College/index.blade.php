<div class="row">
<div class="col-md-12">
	<div class="col-sm-12 col-md-6 left_wrapper">
		@include($views.'sub.left')
	</div>
	<div class="col-sm-12 col-md-6 right_wrapper">
		@include($views.'sub.right')
	</div>
</div>
<div class="modal" id="frm_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
