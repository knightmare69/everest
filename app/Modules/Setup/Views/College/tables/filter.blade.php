<div class="row">
<div class="col-sm-12">
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">Filter:</span>
			<input type="text" class="form-control lfilter" />
			<span class="input-group-addon">
			<i class="fa fa-search"></i>
			</span>
		</div>
	</div>
</div>
<div class="col-sm-12">
<div class="table-responsive" style="max-height:400px;overflow:auto;">
<table class="table table-striped table-hover table_scroll tblfilter">
    <thead>
        <tr>
            <th>###</th>
            <th>EmployeeID</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($filter as $r)
        <tr data-id="{{ $r->EmployeeID }}" data-name="{{$r->LastName.', '.$r->FirstName.' '.$r->MiddleInitial }}"  class="without-bg">
            <td><button type="button" class="btn btn-sm btnselect">Select</button></td>
            <td>{{ $r->EmployeeID }}</td>
            <td><a href="javascript:void(0)" class="btnselect">{{$r->LastName.', '.$r->FirstName.' '.$r->MiddleInitial }}</a></td> 
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>