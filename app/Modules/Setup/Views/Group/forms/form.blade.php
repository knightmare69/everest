<?php $data = isset($data) ? $data : array(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>        
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="control-label">Name <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Name" class="form-control input-xs" name="name" id="name" value="{{ getObjectValue($data,'GroupName') }}">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea style="resize: none !important;" class="form-control" name="description">{{ getObjectValue($data,'GroupDesc') }}</textarea>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
    </div>
    <hr style="margin-top: 3px; margin-bottom: 10px;" />
    <div class="form-actions right  ">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
<!-- END FORM-->