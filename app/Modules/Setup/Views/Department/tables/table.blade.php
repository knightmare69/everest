<?php $table = isset($table) ? $table : array(); ?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
            <th width="10">Status</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->DeptID) }}"  class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td>{{ $row->DeptCode }}</td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->DeptName }}</a></td>
            <td>{{ $row->ShortName }}</td>
            <td class="center"><?= !$row->is_inactive ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
        </tr>
    @endforeach
    </tbody>
</table>