<?php
    $data = isset($data) ? $data : array();
    $campus = App\Modules\Setup\Models\Campus::get();

?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <h4><b>General Information</b></h4>
        <div class="form-group">
            <label class="control-label">Campus Name <small><i class="text-danger">*</i></small></label>
            <select class="form-control search-selec2" name="campus" id="campus">
                @if(!empty($campus))
                    @foreach($campus as $cmps)
                    <option value="{{ encode($cmps->CampusID) }}" <?php (getObjectValue($data,'CampusID') == $cmps->CampusID ? 'selected' : '' )  ?>    }} >{{ $cmps->ShortName }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Code <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Code" class="form-control input-xs" name="code" value="{{ getObjectValue($data,'DeptCode') }}">
                </div>
            </div>
            <!--/span-->
            <div class="col-md-8">
                <div class="form-group">
                    <label class="control-label">Name <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Name" class="form-control input-xs" name="name" id="name" value="{{ getObjectValue($data,'DeptName') }}">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Head</label>
                    <div class="input-group">
                        <input type="text" class="form-control employee not-required" name="dept-head-name" value="{{ $head or '' }}">
                        <div class="input-group-btn">
                            <button type="button" class="employee-search-btn btn btn-default" data-for="head"><i class="fa fa-search"></i></button>
                            <button type="button" class="employee-remove-btn btn btn-default text-danger" data-empty-val="#dept-head"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Vice </label>
                    <div class="input-group">
                        <input type="text" class="form-control employee not-required" name="dept-vice-name" value="{{ $vice or '' }}">
                        <div class="input-group-btn">
                            <button type="button" class="employee-search-btn btn btn-default" data-for="vice"><i class="fa fa-search"></i></button>
                            <button type="button" class="employee-remove-btn btn btn-default text-danger" data-empty-val="#dept-vice"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row-->
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea style="resize: none !important;" class="form-control not-required " name="description">{{ getObjectValue($data,'ShortName') }}</textarea>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
    <div class="">
        <input type="hidden" name="dept-head" id="dept-head" value="{{ getObjectValue($data,'DeptHead_EmployeeID') }}">
        <input type="hidden" name="dept-vice" id="dept-vice" value="{{ getObjectValue($data,'DeptVice_EmployeeID') }}">
    </div>
</form>
<!-- END FORM-->
