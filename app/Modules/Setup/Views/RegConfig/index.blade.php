  <style>
     .table tbody td{
	   padding-top:0px !important;
	   padding-left:8px !important;
	   padding-bottom:0px !important;
	   padding-right:8px !important;
	 }
  </style>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <?php 
   if(isset($configlist)){
	echo '<form id="configform" action="'.url().'/setup/regconfig/event'.'" method="POST">';
	echo '<div class="row" data-pointer="tblconfig" style="max-height:94%;overflow:auto;">';
	echo $configlist;
	echo '</div>';
	echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="pull-right"><button type="button" class="btn btn-success btnsave"><i class="fa fa-save"></i> Save</button></div></div>';
	echo '</form>';
   }else{
    echo '<div class="alert alert-danger"><i class="fa fa-warning"></i> Unable to load configuration.</div>';
   }
  ?>
  </div>