<?php $data = isset($data) ? $data : array(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <h4><b>Information</b></h4>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="control-label">City <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Name" class="form-control input-xs" name="name" id="name" value="{{ getObjectValue($data,'City') }}">
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Country</label>
                    <select class="form-control select2" name="country">
                    @foreach(App\Modules\Setup\Models\Country::get() as $row)
                        <option {{ getObjectValue($data,'CountryCode') == $row->Code ? 'selected' : '' }} value="{{ $row->CountryID.':'.$row->Code }}">{{ $row->Country }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
<!-- END FORM-->