<?php $table = isset($table) ? $table : array(); ?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>Name</th>
            <th>Country Code</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->CityID) }}"  data-status="{{ $row->Inactive }}" class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->City  }}</a></td>
            <td>{{ $row->CountryCode }}</td>
        </tr>
    @endforeach
    </tbody>
</table>