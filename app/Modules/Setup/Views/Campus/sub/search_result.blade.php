<?php $data = isset($data) ? $data : array(); ?>
<div class="scroller" style="position: relative; overflow: hidden; width: 100% !important; height: 350px;">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr class="heading">
				<th width="10px">&nbsp;</th>
				<th width="20"><small>#</small></th>
				<th width="150" class="center"><small>Acronym</small></th>
				<th class="center"><small>Campus Name</small></th>
				<th width="200" class="center"><small>Short Name</small></th>
			</tr>
		</thead>
		<tbody>
		{{--*/$i=1/*--}}
		@foreach($data as $row)
			<tr data-id="{{ encode($row->CampusID) }}" class="cursor-pointer">
				<td class="td-rem" style="vertical-align: middle;">
                    <span class='cursor-pointer span-remove-campus' data-id="{{ encode($row->CampusID)}}"><i class='fa fa-trash font-red'></i></span>
                </td>
				<td><small>{{ $i++ }}</small></td>
				<td class="a-select"><small>{{ $row->Acronym }}</small></td>
				<td class="a-select"><small>{{ ucfirst($row->ShortName) }}</small></td>
				<td class="a-select"><small>{{ ucfirst($row->CampusName) }}</small></td>
			</tr>
		@endforeach
		@if ( count($data) <= 0 )
			<tr>
				<td colspan="5" align="center">No Result Found!</td>
			</tr>
		@endif
		</tbody>
	</table>
</div>