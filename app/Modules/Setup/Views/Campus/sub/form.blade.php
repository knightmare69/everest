<?php $data = isset($data) ? $data : array(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form form-setup" action="#" method="POST" data-form="campus">
    <div class="form-body">
        <div class="row">
            <div class="col-md-9" >
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Acronym <small><i class="text-danger">*</i></small></label>
                            <input type="text" class="form-control input-sm" name="acronym" id="acronym" value="{{ getObjectValue($data,'Acronym') }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Campus Name <small><i class="text-danger">*</i></small></label>
                            <input type="text" class="form-control input-sm" name="campusname" id="campusname" value="{{ getObjectValue($data,'ShortName') }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Short Name <small><i class="text-danger">*</i></small></label>
                            <input type="text" class="form-control input-sm" name="shortname" id="shortname" value="{{ getObjectValue($data,'CampusName') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group btn-group-devided" data-toggle="buttons" style="float: right;">
                            <label class="btn red btn-outline btn-circle btn-sm btn-reset ">
                            <input type="radio" name="options" class="toggle"><i class="fa fa-angle-left"></i> Reset</label>
                            <label class="btn blue btn-outline btn-circle btn-sm btn-action btn-save">
                            <input type="radio" name="options" class="toggle"><i class="fa fa-check"></i> Save</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <legend style="font-size: unset;">&nbsp;</legend>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="barangay" id="barangay" value="{{ getObjectValue($data,'Barangay') }}">
                    <span class="help-block">&nbsp;(Barangay)</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="towncity" id="towncity" value="{{ getObjectValue($data,'TownCity') }}">
                    <span class="help-block">&nbsp;(Town / City)</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control input-sm not-required" name="province" id="province" value="{{ getObjectValue($data,'Province') }}">
                    <span class="help-block">&nbsp;(Province)</span>
                </div>
            </div>
             <div class="col-md-3">
                 <div class="form-group">
                    <select  class="select2 form-control input-sm not-required" name="region" id="region">
                        <option class="default-option" value=''>Select</option>
                               
                    </select>
                    <span class="help-block">&nbsp;(Region)</span>
                </div>
           </div>
          
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control input-sm not-required" name="district" id="district" value="{{ getObjectValue($data,'District') }}">
                    <span class="help-block">&nbsp;(District ID)</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control input-sm" name="zipcode" id="zipcode" value="{{ getObjectValue($data,'ZipCode') }}">
                    <span class="help-block">&nbsp;(Zip Code)</span>
                </div>
            </div>
        </div>
        <legend style="font-size: unset;">&nbsp;</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Mailing Address <small><i class="text-danger">*</i></small></label>
                    <input type="email" class="form-control input-sm" name="mailingaddress" id="mailingaddress" value="{{ getObjectValue($data,'MailingAddress') }}">
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Email Address <small><i class="text-danger">*</i></small></label>
                    <input type="email" class="form-control input-sm not-required" name="email" id="email" value="{{ getObjectValue($data,'Email') }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Website <small><i class="text-danger">*</i></small></label>
                    <input type="text" class="form-control input-sm" name="website" id="website" value="{{ getObjectValue($data,'Website') }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Tel No. <small><i class="text-danger">*</i></small></label>
                    <input type="text" name="tel" id="tel" class="form-control input-sm" value="{{ getObjectValue($data,'TelNo') }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Fax No. <small><i class="text-danger">*</i></small></label>
                    <input type="text" name="fax" id="fax" class="form-control input-sm" value="{{ getObjectValue($data,'FaxNo') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet bordered light">
                    <div class="portlet-title">
                        <div class="caption" style="font-size: 15px;font-weight: bold;color: #9eacb4;">
                            Department Heads
                        </div>
                        <div class="tools">
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-outline btn-default a-addnew" title="Add Department Head" href="javascript:void(0)">
                                <i class="fa fa-plus"> Add New</i>
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-department-heads">
                                    <thead>
                                        <tr class="heading">
                                            <th class="center" width="10px">&nbsp;</th>
                                            <th style="width: 30px !important">&nbsp;</th>
                                            <th class="center" width="150"><small>Office Name</small></th>
                                            <th class="center" width="200"><small>Department Head's Fullname</small></th>
                                            <th class="center" width="150"><small>Head Position / Title</small></th>    
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {{--*/$i=1/*--}}
                                    @if(!empty($deptheads))
                                        @foreach($deptheads as $row)
                                            <tr class="include" data-id="{{ $row->DeptHeadsID }}">
                                                <td class="td-rem center" style="vertical-align: middle;">
                                                    <span class='cursor-pointer span-remove' data-id="{{ $row->DeptHeadsID }}"><i class='fa fa-trash font-red' style="font-size: 16px;"></i></span>
                                                </td>
                                                <td class="td-seq" style="vertical-align: middle;">Dept. {{ $i++ }}</td>
                                                <td class="td-offname">
                                                    <input type="text" class="form-control custom-input-sm offname" value="{{ $row->Dept_Office }}">
                                                </td>
                                                <td class="td-headsname">
                                                    <input type="text" class="form-control custom-input-sm headsname" value="{{ $row->Dept_Name }}">
                                                </td>
                                                <td class="td-position">
                                                    <input type="text" class="form-control custom-input-sm position" value="{{ $row->Dept_Position }}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->



