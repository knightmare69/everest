<div class="portlet bordered light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift font-green-sharp"></i>
           	<span class="caption-subject font-green-sharp bold">{{ isset($page_title) ? ucfirst($page_title) : '' }}</span>
           	<span class="caption-helper">&nbsp;{{ isset($page_description) ? ucfirst($page_description) : '' }}</span>
		</div>
		<div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
            	<input type="text" class="form-control input-sm btn-circle" id="filter-search" placeholder="Search" value="">
	        </div>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12 form-wrapper">
				@include($views.'sub.form')
			</div>
		</div>
	</div>
</div>


