<div class="row">
<div class="col-sm-12">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:-10px;">
		<ul class="nav nav-tabs progtabs">
			<li class="active">
				<a href="#tbprogs" data-toggle="tab">
				Programs </a>
			</li>
			<li>
				<a href="#tbclass" data-toggle="tab">
				ProgClass </a>
			</li>
			<li>
				<a href="#tbmajor" data-toggle="tab">
				Major Discipline</a>
			</li>
			<li class="hidden">
				<button class="btn btn-sm btn-test">Testing</button>
			</li>
		</ul>
	</div>
	<div class="col-md-12 col-sm-12">
	<div class="portlet light">
		<div class="tab-content">
			<div class="tab-pane active" id="tbprogs">
			   <div class="row">
				<div class="col-sm-6">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i>Form
							</div>
							<div class="tools">
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12 form_wrapper">
									@include($views.'forms.form')
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-building"></i> List of Program/s
							</div>        
							<div class="actions">
								<div class="btn-group">
									<a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#" class="btn bg bg-purple btn-sm dropdown-toggle" aria-expanded="false">
									Actions <i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="javascript:void(0)" class="a_status">
											<i class="fa fa-check  font-blue"></i> Active/Deactivate</a>
										</li>
										<li>
											<a href="javascript:void(0)" class='a_remove'>
											<i class="fa fa-trash font-red"></i> Remove</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12 table_wrapper">
									@include($views.'tables.table')
								</div>
							</div>
						</div>
					</div>
				</div>
			   </div>
			</div>
			<div class="tab-pane" id="tbclass">
				<div class="row">
				<div class="col-sm-6">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i>Form
							</div>
							<div class="tools">
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12 form_wrapper">
									@include($views.'forms.fclass')
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-building"></i> Program Class
							</div>        
							<div class="actions"></div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12 table_wrapper">
									@include($views.'tables.tclass')
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<div class="tab-pane" id="tbmajor">
				<div class="row">
					<div class="col-sm-6">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file"></i>Form
							</div>
							<div class="tools">
							</div>
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12 form_wrapper">
									@include($views.'forms.fmajor')
								</div>
							</div>
						</div>
					</div>
					</div>
				
					<div class="col-sm-6">
					<div class="portlet box green-haze">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-building"></i> Major Discipline/s
							</div>        
							<div class="actions"></div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12 table_wrapper">
									@include($views.'tables.tmajor')
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	</div>
</div>
</div>
<div class="modal" id="frm_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
