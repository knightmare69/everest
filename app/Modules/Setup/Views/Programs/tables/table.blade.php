<?php $table = isset($table) ? $table : array(); ?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ 'program@'.encode($row->ProgID) }}"  class="without-bg">
            <td><input type="checkbox" class="chk-child"><i></i></td>
            <td>{{ $row->ProgCode }}</td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->ProgName }}</a></td>
            <td>{{ $row->ProgShortName }}</td>
        </tr>
    @endforeach
    </tbody>
</table>