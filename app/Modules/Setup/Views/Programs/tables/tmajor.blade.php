<?php $table = isset($tmajor) ? $tmajor : array(); ?>
<table class="table table-striped table-hover table_scroll">
    <thead>
        <tr>
            <th><button class="btn blue btn-sm btnrefresh"><i class="fa fa-refresh"></i> Refresh</button></th>
            <th>Code</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ 'major@'.encode($row->IndexID) }}"  class="without-bg">
            <td><a href="javascript:void(0)" class='btn btn-sm btn-danger a_delete'><i class="fa fa-trash"></i> Remove</a></td>
            <td>{{ $row->MajorDiscCode }}</td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->MajorDiscDesc }}</a></td>
        </tr>
    @endforeach
    </tbody>
</table>