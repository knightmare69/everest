<?php
$data   = isset($mdata) ? $mdata : array();
$mgroup = DB::select("SELECT * FROM ES_DisciplineMajorGroups");
?>
<form id="major" class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
	    <input type="hidden" class="hidden do-not-clear" id="xtarget" name="xtarget" value="major"/>
		<div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Group <small><i class="text-danger">*</i></small></label>
					<select class="form-control" id="selgroup" data-xtarget=".groupform" data-xvalue="#group" data-xlabel="#gname">
					  <?php
					    if($mgroup && count($mgroup)>0){
							foreach($mgroup as $rs){
							  echo '<option value="'.$rs->IndexID.' '.(($rs->IndexID==getObjectValue($data,'MajorGroup'))?"selected":"").'">'.$rs->MajorGroupDesc.'</option>';
							}
						}
					  ?>
					  <option value="-1">- Other/New -</option>
					</select>
                    <input type="hidden" class="form-control input-xs groupform numberonly" name="group" id="group" value="{{ getObjectValue($data,'MajorGroup') }}">
                    <input type="hidden" class="form-control input-xs groupform" name="gname" id="gname" value="{{ getObjectValue($data,'MajorGroupDesc') }}">
                </div>
            </div>
        </div>
		<div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Code <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Code" class="form-control input-xs numberonly" name="code" value="{{ getObjectValue($data,'MajorDiscCode') }}">
                </div>
            </div>
		</div>	
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Name <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Name" class="form-control input-xs" name="name" id="name" value="{{ getObjectValue($data,'MajorDiscDesc') }}">
                </div>
            </div>
        </div>
		
	</div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>	