<?php
    $data = isset($data) ? $data : array();
    $campus = App\Modules\Setup\Models\Campus::get();

?>
<!-- BEGIN FORM-->
<form id="program" class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
	    <input type="hidden" class="hidden do-not-clear" id="xtarget" name="xtarget" value="program"/>
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Campus <small><i class="text-danger">*</i></small></label>
            <select class="form-control search-selec2" name="campus" id="campus">
                @if(!empty($campus))
                    @foreach($campus as $cmps)
                    <option value="{{ encode($cmps->CampusID) }}" <?php echo (getObjectValue($data,'CampusID') == $cmps->CampusID ? 'selected' : '' )  ?>>{{ $cmps->ShortName }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">College <small><i class="text-danger">*</i></small></label>
            <select class="form-control search-selec2" name="college" id="college">
                <?php $college = DB::select("SELECT * FROM ES_Colleges");?>
				@if(!empty($college))
                    @foreach($college as $col)
                    <option value="{{ encode($col->CollegeID) }}" <?php echo (getObjectValue($data,'CollegeID') == $col->CollegeID ? 'selected' : '' )  ?>>{{ $col->CollegeName }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Code <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Code" class="form-control input-xs" name="code" value="{{ getObjectValue($data,'ProgCode') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Short <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="ShortName" class="form-control input-xs" name="short" id="short" value="{{ getObjectValue($data,'ProgShortName') }}">
                </div>
            </div>
		</div>	
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Name <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Name" class="form-control input-xs" name="name" id="name" value="{{ getObjectValue($data,'ProgName') }}">
                </div>
            </div>
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Alias 1</label>
                    <input type="text" placeholder="Alias 1" class="form-control input-xs not-required" name="aliasa" id="aliasa" value="{{ getObjectValue($data,'ProgAlias') }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Alias 2</label>
                    <input type="text" placeholder="Alias 2" class="form-control input-xs not-required" name="aliasb" id="aliasb" value="{{ getObjectValue($data,'ProgAlias2') }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">ProgClass <small><i class="text-danger">*</i></small></label>
            <select class="form-control search-selec2" name="progclass" id="progclass">
                <?php $class = DB::select("SELECT * FROM ES_Program_Class");?>
				@if(!empty($class))
                    @foreach($class as $cls)
                    <option value="{{ encode($cls->ClassCode) }}" <?php echo ((intval(getObjectValue($data,'ProgClass')) == intval($cls->ClassCode)) ? 'selected' : '' )  ?>>{{ $cls->ClassDesc }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Term <small><i class="text-danger">*</i></small></label>
            <select class="form-control search-selec2" name="progsems" id="progsems">
                <?php $sems = DB::select("SELECT * FROM ES_Program_Semestral");?>
				@if(!empty($sems))
                    @foreach($sems as $sms)
                    <option value="{{ encode($sms->SemestralCode) }}" <?php echo (getObjectValue($data,'ProgSems') == $sms->SemestralCode ? 'selected' : '' )  ?>>{{ $sms->SemestralDesc }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">No of Year/s</label>
                    <input type="text" placeholder="No of Year/s" class="form-control input-xs not-required numberonly" name="noyrs" id="noyrs" value="{{ getObjectValue($data,'ProgYears') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Max Residency</label>
                    <input type="text" placeholder="Max Residency" class="form-control input-xs not-required numberonly" name="maxres" id="maxres" value="{{ getObjectValue($data,'MaxResidency') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Total No. of Academic Subj.</label>
                    <input type="text" placeholder="No. Of Academic Subj." class="form-control input-xs not-required" name="acadsubj" id="acadsubj" value="{{ getObjectValue($data,'TotalAcadSubject') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Total Academic Credit Units</label>
                    <input type="text" placeholder="Academic Credit Units" class="form-control input-xs not-required" name="acadunit" id="acadunit" value="{{ getObjectValue($data,'TotalAcadCreditUnits') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Normal Load</label>
                    <input type="text" placeholder="Normal Load" class="form-control input-xs not-required" name="normal" id="normal" value="{{ getObjectValue($data,'TotalNormalLoad') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Total Gen. Education Units</label>
                    <input type="text" placeholder="Gen. Education Units" class="form-control input-xs not-required" name="geunit" id="geunit" value="{{ getObjectValue($data,'TotalGenEdUnits') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Total Major Units</label>
                    <input type="text" placeholder="Major Units" class="form-control input-xs not-required" name="majorunit" id="majorunit" value="{{ getObjectValue($data,'TotalMajorUnits') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Total Elective Units</label>
                    <input type="text" placeholder="Elective Units" class="form-control input-xs not-required" name="electunit" id="electunit" value="{{ getObjectValue($data,'TotalElectiveUnits') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Total Lecture Units</label>
                    <input type="text" placeholder="Lecture Units" class="form-control input-xs not-required" name="lectunit" id="lectunit" value="{{ getObjectValue($data,'TotalLectureUnits') }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Total Non-Lecture Units</label>
                    <input type="text" placeholder="Non-Lecture Units" class="form-control input-xs not-required" name="nlectunit" id="nlectunit" value="{{ getObjectValue($data,'TotalNonLectUnits') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
<!-- END FORM-->
