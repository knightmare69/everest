<?php
$data = isset($cdata) ? $cdata : array();
?>
<form id="class" class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
	    <input type="hidden" class="hidden do-not-clear" id="xtarget" name="xtarget" value="class"/>
		<div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
		<div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Code <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Code" class="form-control input-xs numberonly" name="code" value="{{ getObjectValue($data,'ClassCode') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Short <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="ShortName" class="form-control input-xs" name="short" id="short" value="{{ getObjectValue($data,'ShortName') }}">
                </div>
            </div>
		</div>	
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Name <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Name" class="form-control input-xs" name="name" id="name" value="{{ getObjectValue($data,'ClassDesc') }}">
                </div>
            </div>
        </div>
		
	</div>
    <div class="form-actions right">
        <button class="btn default btn_reset" type="button">Reset</button>
        <button class="btn blue btn_action btn_save" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>	