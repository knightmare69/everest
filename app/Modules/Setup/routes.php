<?php
	Route::group(['prefix' => 'setup','middleware'=>'auth'], function () {

		Route::group(['prefix' => 'grade-setup'], function () {
			Route::get('/','GradeSetup@index');
			Route::get('event','GradeSetup@event');
			Route::post('event','GradeSetup@event');
		});
		
		Route::group(['prefix' => 'grade-oldsetup'], function () {
			Route::get('/','GradeSetup@index_old');
			Route::get('event','GradeSetup@event');
			Route::post('event','GradeSetup@event');
		});
		
		Route::group(['prefix' => 'scho-provider'], function () {
			Route::get('/','SchoProvider@index');
			Route::post('event','SchoProvider@event');
			Route::get('data','SchoProvider@data');
		});
		
		Route::group(['prefix' => 'scho-granttemplate'], function () {
			Route::get('/','SchoGrantTemplate@index');
			Route::post('event','SchoGrantTemplate@txn');
			Route::get('data','SchoGrantTemplate@txn');
		});
		
		Route::group(['prefix' => 'department'], function () {
			Route::get('/','Department@index');
			Route::post('event','Department@event');
		});
		Route::group(['prefix' => 'group'], function () {
			Route::get('/','Group@index');
			Route::post('event','Group@event');
		});
		Route::group(['prefix' => 'position'], function () {
			Route::get('/','Position@index');
			Route::post('event','Position@event');
		});

		Route::group(['prefix' => 'city'], function () {
			Route::get('/','City@index');
			Route::post('event','City@event');
		});

		Route::group(['prefix' => 'medical'], function(){
			Route::get('/', 'Medical@index');
			Route::post('event','Medical@event');
			Route::get('print', 'Medical@print_data');
		});

		Route::group(['prefix' => 'testing'], function(){
			Route::get('/', 'Testing@index');
			Route::post('event','Testing@event');
			Route::get('print','Testing@print_data');
		});

		Route::group(['prefix' => 'institution'], function () {
			Route::get('/','Institution@index');
			Route::post('event','Institution@event');
			Route::post('SearchForm','Institution@SearchForm');
			Route::post('SearchResult','Institution@SearchResult');
		});

		Route::group(['prefix' => 'campus'], function () {
			Route::get('/','Campus@index');
			Route::post('event','Campus@event');
			Route::post('SearchForm','Campus@SearchForm');
			Route::post('SearchResult','Campus@SearchResult');
		});

    //     Route::group(['prefix' => 'academic-yearterm'], function () {
		// 	Route::get('/','ayterm@index');
		// 	Route::post('event','ayterm@event');
		// });

    //     Route::group(['prefix' => 'term'], function () {
		// 	Route::get('/','ayterm@index');
		// 	Route::post('event','ayterm@event');
		// });
		Route::get('/{type}', 'SetupManager@index');
			Route::post('/{type}/event', 'SetupManager@event');

		Route::group(['prefix' => 'requirements'], function () {
		    Route::get('/','Requirements@index');
			Route::post('event','Requirements@event');
		});


	});

	Route::group(['prefix' => 'cms'], function () {
    	Route::get('/', 'Content@index');
    	Route::get('/edit/{key}', 'Content@edit');
    	Route::get('/new', 'Content@newTemplate');
	    Route::post('event', 'Content@event');
	});



?>
