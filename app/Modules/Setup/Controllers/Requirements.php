<?php 
namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Models\Requirements as Model;
use App\Modules\Setup\Services\RequirementsServiceProvider as Services;
use Request;
use Response;
use Permission;

class Requirements extends Controller {
	private $media =
		[
			'Title'=> 'Requirements',
			'Description'=> 'Manage',
			'js'		=> ['setup/requirements'],
			'init'		=> ['Me.init()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'select2/select2.min',
							'jquery-multi-select/js/jquery.multi-select',
			                'bootstrap-select/bootstrap-select.min',
						],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
				'bootstrap-select/bootstrap-select.min',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
			]
		];

	private $url = [ 'page' => 'setup/requirements/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.Requirements.';

 	function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = ['error'=>true,'message'=>'No Event Selected'];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'modalRequirements':
					$response = view($this->views.'modals.requirements')->render();
				break;	
				case 'modalTemplate':
					$response = view($this->views.'modals.template')->render();
				break;
				case 'saveRequirements':
					if ($this->permission->has('add')) {
						if ($this->services->saveRequirements()) {
							$response = successSave();
						} else {
							$response = errorSave();
						}
					}
				break;
				case 'saveTemplateItems':
					if ($this->permission->has('add')) {
						if ($this->services->saveTemplateRequirements()) {
							$response = successSave();
						} else {
							$response = errorSave();
						}
					}
				break;
				case 'saveTemplate':
					if ($this->permission->has('add')) {
						if ($this->services->saveTemplate()) {
							$response = successSave();
						} else {
							$response = errorSave();
						}
					}
				break;
				case 'updateIsRequired':
					$response = $this->services->updateIsRequired() ? successSave() : errorSave();
				break;
				case 'updateAllowUpload':
					$response = $this->services->updateAllowUpload() ? successSave() : errorSave();
				break;
				case 'updateSlot':
					$response = $this->services->updateSlot() ? successSave() : errorSave();
				break;
				case 'updateCode':
					$response = $this->services->updateCode() ? successSave() : errorSave();
				break;
				case 'showRequirements':
					$response = view($this->views.'tables.table')->render();
				break;
				case 'showTemplates':
					$response = $this->services->showTemplates();
				break;
				

			}
		}
		return $response;
	}

	private function init($key = null) 
	{
		$this->initializer();
		return array(
			'table' => $key == null ?  $this->model->all() : array()
		);
	}

	private function refreshTable()
	{
		return view($this->views.'tables.table',['table'=>$this->model->all()])->render();
	}

	private function initializer()
	{
		$this->model = new Model;
		$this->services = new Services;
		$this->permission = new Permission('requirements');
	}
}