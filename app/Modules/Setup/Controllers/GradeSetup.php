<?php

namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\DepartmentServiceProvider as Services;
use Request;
use Response;
use Permission;
use DB;

Class GradeSetup extends Controller{
	private $media =
		[
			'Title'=> 'Grade Setup',
			'Description'=> 'Manage School academic year and term',
			'js'		=> ['Setup/gradesetup',],
			'init'		=> [],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap'
						]
		];

	private $url = [ 'page' => 'setup/term/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.GradeSetup.';


 	function index_old()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}
	
	function index(){
		$this->media['js'] = ['Setup/gradesheet'];
		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'gradesheet',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
	}
	
 	function event()
	{
		$response = ['error'=>true,'message'=>'No Event Selected'];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'load':
				    $response = ['error'=>false,'message'=>'Successfully Save!','table'=> ""];
				break;
				case 'save':
				     $setup = Request::get('setup');
					 /**/
					 foreach($setup as $k=>$r){
					  $exec = DB::statement("INSERT INTO ES_GS_Setup(SetupID,Setup,Value) SELECT '".$r['xid']."','".$r['desc']."','".$r['data']."' WHERE '".$r['xid']."' NOT IN (SELECT SetupID FROM ES_GS_Setup)
					                         UPDATE ES_GS_Setup SET Value='".$r['data']."' WHERE SetupID='".$r['xid']."'");	 
					 }
					 $response = ['error'=>false,'message'=>'Successfully Save!','table'=> ""];
				break;
				case 'xload':
				     $termid   = Request::get('term');
				     $campus   = Request::get('campus');
				     $exec = DB::select("SELECT y.YearLevelID
											   ,y.YearLevelCode
											   ,y.YearLevelName
											   ,y.SeqNo
											   ,y.YLID_OldValue
											   ,y.ProgID
											   ,y.ProgClass
											   ,gs.MajorID
											   ,gs.TransmutationID
											   ,t.TemplateName as Transmutation
											   ,gs.TransmuteOption
											   ,gs.LetterGradeID
											   ,g.TemplateName as LetterGrade 
										   FROM ESv2_YearLevel as y
									  LEFT JOIN ESv2_Gradesheet_Setup as gs ON gs.TermID='".$termid."' AND y.YLID_OldValue=gs.YearLevelID AND y.ProgID=gs.ProgID
									  LEFT JOIN ES_GS_Transmutation as t ON gs.TransmutationID=t.TranstblID
									  LEFT JOIN ES_GS_GradingSystem as g ON gs.LetterGradeID=g.TemplateID
										  WHERE y.Inactive=0");
					 $content = '';					  
					 foreach($exec as $y){
					   $opt = '';
					   switch($y->TransmuteOption){
						  case '0':
							$opt = 'Average Only';
						  break;
						  case '1':
							$opt = 'Event Only';
						  break;					  
					   }
					   $content .= '<tr data-id      = "'.$y->YLID_OldValue.'" 
					                    data-prog    = "'.$y->ProgID.'" 
										data-option  = "'.$y->TransmuteOption.'" 
										data-transid = "'.$y->TransmutationID.'" 
										data-letter  = "'.$y->LetterGradeID.'">
										<td>'.$y->YearLevelName.'</td>
										<td>'.$y->Transmutation.'</td>
										<td>'.$opt.'</td>
										<td>'.$y->LetterGrade.'</td>
										<td class="hidden"></td>
										<td class="hidden"></td>
										<td class="hidden"></td>
									</tr>';	 
					 }
					 $response = ['error'=>false,'content'=>$content];						   					  
				break;
				case 'xsave':
				    $termid   = Request::get('term');
				    $campus   = Request::get('campus');
				    $yrlvl    = Request::get('yrlvl');
				    $progid   = Request::get('progid');
				    $transid  = Request::get('transmute');
				    $transopt = Request::get('transopt');
				    $lettergr = Request::get('gradsys');
					
					$exec     = DB::statement("INSERT INTO ESv2_Gradesheet_Setup(TermID,YearLevelID,ProgID,TransmutationID,TransmuteOption,LetterGradeID)
					                           SELECT '".$termid."' as TermID,'".$yrlvl."' as YearLevelID,'".$progid."' as ProgID,'".$transid."' as TransID,'".$transopt."' as TransOpt,'".$lettergr."' as LttrGrd 
											    WHERE CONCAT('".$termid."',':','".$yrlvl."',':','".$progid."') NOT IN (SELECT CONCAT(TermID,':',YearLevelID,':',ProgID) as EntryID FROM ESv2_Gradesheet_Setup)
												
											   UPDATE ESv2_Gradesheet_Setup SET LetterGradeID='".$lettergr."'
											                                   ,TransmuteOption='".$transopt."'
                                                                               ,TransmutationID='".$transid."'															   
																		  WHERE TermID='".$termid."' AND YearLevelID='".$yrlvl."' AND ProgID='".$progid."'");
					$response = ['error'=>false,'message'=>'Successfully Save!','table'=> ""];						   
				break;
			}
		}
		return $response;
	}

	private function init($key = null)
	{
		$this->initializer();
		return array(
		         'campus' => DB::select("SELECT * FROM ES_Campus"),
				 'yrlvl'  => DB::select("SELECT y.YearLevelID
											   ,y.YearLevelCode
											   ,y.YearLevelName
											   ,y.SeqNo
											   ,y.YLID_OldValue
											   ,y.ProgID
											   ,y.ProgClass
											   ,gs.MajorID
											   ,gs.TransmutationID
											   ,t.TemplateName as Transmutation
											   ,gs.TransmuteOption
											   ,gs.LetterGradeID
											   ,g.TemplateName as LetterGrade 
										   FROM ESv2_YearLevel as y
									  LEFT JOIN ESv2_Gradesheet_Setup as gs ON gs.TermID=1 AND y.YLID_OldValue=gs.YearLevelID AND y.ProgID=gs.ProgID
									  LEFT JOIN ES_GS_Transmutation as t ON gs.TransmutationID=t.TranstblID
									  LEFT JOIN ES_GS_GradingSystem as g ON gs.LetterGradeID=g.TemplateID
										  WHERE y.Inactive=0"),
				 'progid' => DB::select("SELECT * FROM ES_Programs"),
		      );
	}

	private function initializer()
	{
		$this->services   = new Services;
		$this->permission = new Permission('department');
	}
}