<?php namespace App\Modules\Setup\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Setup\Models\Campus as CampusModel;
use App\Modules\Setup\Models\CampusDeptHeads;
use Request;
use Response;
use Permission;
use DB;

class Campus extends Controller{
	private $media =
		[
			'Title'=> 'Campus',
			'Description'=> 'This module allows you to manage campus...',
			'js'		=> ['setup'],
			'init'		=> ['SETUP.init()','FN.multipleSelect()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min','select2/select2.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap','bootstrap-fileinput/bootstrap-fileinput'
							],
			'plugin_css' => [
							'bootstrap-select/bootstrap-select.min',
							'select2/select2',
							'bootstrap-fileinput/bootstrap-fileinput'
							]
		];

	private $url = ['page' => 'setup/campus/','form'=>'.form-setup'];

	public $views = 'Setup.Views.Campus.';

 	function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',['page_title'=>$this->media['Title'],'page_description'=>$this->media['Description']])->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = ['error'=>true,'message'=>'No Event Selected'];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'save':
					if ($this->permission->has('add')) {
						$post = Request::all();
	        			$result = $this->model->_save($post); 
	        			$message = $result['message'];
	        			$response = [ 'error'=> $result['error'],'message'=> getActionMessage($result['error'],$message)];
	        			unset($result['message']);
	        			$response  = json_encode(array_merge($response,$result));
					}
					break;
				case 'update':
					if ($this->permission->has('edit')) {
						$post = Request::all();
						$result = $this->model->_update($post);
						$message = $result['message'];
						$response = [ 'error'=> $result['error'],'message'=> getActionMessage($result['error'],$message,'update')];
						unset($result['message']);
						$response = json_encode(array_merge($response,$result));
					}
					break;
				case 'edit': 
					if ($this->permission->has('edit')) { 
						$response = view($this->views.'sub.form',['data'=>$this->model->find(decode(Request::get('id'))), 'deptheads'=>DB::table('es_campus_dept_heads')->where('CampusID',(decode(Request::get('id'))))->get()]);
					}
					break;
				case 'delete-deptheads':
					if ($this->permission->has('delete')) {
						$response = json_encode($this->deptheads->where(['DeptHeadsID'=>Request::get('id')])->delete());
					}
				break;
				case 'delete-campus':
					if ($this->permission->has('delete')) {
						$del = $this->deptheads->where(['CampusID'=>decode(Request::get('id'))])->delete();
						$response = json_encode($this->model->where(['CampusID'=>decode(Request::get('id'))])->delete());
					}
				break;
			}
		}
		return $response;
	}

	function SearchForm()
	{	
		$views = view($this->views.'sub.search',['views'=>$this->views])->render();
		echo $views;
	}

	function SearchResult()
	{	
		$this->initializer();
		$post 	= Request::all();
		$filter	= trimmed($post['filter']);
		$where  = "Acronym like '%".trimmed($filter)."%' OR ShortName like '%".trimmed($filter)."%' OR CampusName like '%".trimmed($filter)."%'";

		$data = DB::table('es_campus')->select("*")->whereRaw($where)->limit(100)->get();

		$views = view($this->views.'sub.search_result',['data'=>$data])->render();
		echo $views;
	}

	private function initializer()
	{
		$this->permission = new Permission('campus');
		$this->model 	  = new CampusModel;
		$this->deptheads  = new CampusDeptHeads;
	}
}
