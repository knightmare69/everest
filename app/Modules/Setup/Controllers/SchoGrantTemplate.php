<?php
namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\SchoGrantTemplateServiceProvider as Services;
use App\Modules\Setup\Models\SchoGrantTemplate as ScholarModel;
use Request;
use Response;
use Permission;
use DB;

class SchoGrantTemplate extends Controller{
	private $media =
		[
			'Title'=> 'Grant Template',
			'Description'=> 'Manage Scholarship Grant Template/s',
			'js'		=> ['Setup/schogrant'],
			'init'		=> [],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap'
						]
		];

	private $url = [ 'page' => 'setup/schogranttemeplate/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.SchoGrantTemplate.';


 	function index(){
 		$this->initializer();
        $read = true;//$this->permission->has('read');
		if ($read) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	} 
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax()){
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event')){
			    case 'filter':
				      $src_type            = Request::get('src'); 
					  $content             = '';
					  $thead               = '';
					  $tfoot               = '';
					  $qry                 = "SELECT * FROM ES_Accounts";
					  $response['content'] = '';
			
					  switch($src_type){
					   case 'account':
					     $thead    = '<table class="table table-condensed table-bordered tblfilter" style="white-space:nowrap;">
										 <thead>
											<th>Acct Code</th>
											<th>Acct Name</th>
											<th>Amount</th>
										 </thead>
										 <tbody>';
						 $tfoot    = '</tbody></table>';			 
					     $qry      = "SELECT * FROM ES_Accounts";
					   break;
					  }
					  
					  $exec = DB::select($qry);
					  if($exec){
						foreach($exec as $rs){
					     $response['content'].= '<tr data-id="'.$rs->AcctID.'" data-list="'.$rs->AcctID.'">
													<td>'.$rs->AcctCode.'</td>
													<td>'.$rs->AcctName.'</td>
													<td>0.00</td>
												 </tr>';
					    }
					    $response['success'] = true;
					    $response['content'] = $thead.$response['content'].$tfoot;
					  }else{
					    $response['message'] = "Failed To Load Data";
					  }
					  
					  return Response::json($response);
					break;
				break;
				case 'info':
				      $tmpid = Request::get('schoid');
				      $tdata = $this->model->get_data($tmpid);
				      $dtail = $this->model->get_detail($tmpid);
					  if($tdata){
					    $response = array('success'=>true,'content'=>$tdata[0],'detail'=>$dtail);
					  }else
					    $response['message'] = 'Failed to load';
						
					  return Response::json($response);
				case 'list':
					  $exec = $this->model->get_data(0);
					  $response = Response::json([
												'success' => true
					                           ,'content' => (string)view($this->views.'tdata',array('tdata'=>$exec))
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
					break;
				case 'remove':
				    $tmpid = Request::get('schoid');
				    $exec = DB::statement("DELETE FROM ES_SchoGrantTemplates WHERE GrantTemplateID='".$tmpid."'");
				    $dxec = DB::statement("DELETE FROM ES_SchoGrantTemplate_Details WHERE GrantTemplateID='".$tmpid."'");
					if($exec){
					$response = Response::json([
												'success' => true
					                           ,'content' => ''
					                           ,'error'   => false
											   ,'message' => 'Successfully Removed'
											   ]);
					}else{
					$response = Response::json([
												'success' => false
					                           ,'content' => ''
					                           ,'error'   => false
											   ,'message' => 'Failed to Remove'
											   ]);
					}						   
                    break;				
				case 'save':
					$post = Request::all();
					$data = $this->services->post($post);
					$xid  = getObjectValue($post,'xid',0);
					if(getObjectValue($post,'xid',0)<= 0){
					  $data['CreatedBy']  = getUserID();
					  $data['CreatedDate']= date('Y-m-d H:i');
					  $exec = $this->model->insertGetId($data);
					  $xid  = $exec;
					}else{
					  //$data['ModifiedBy']  = getUserID();
					  //$data['ModifiedDate']= date('Y-m-d H:i');
					  $exec = $this->model->where('GrantTemplateID',getObjectValue($post,'xid',0))->update($data);
					}
					
					if($exec){
					   $detail = getObjectValue($post,'detail','');
					   $delxec = DB::statement("DELETE FROM ES_SchoGrantTemplate_Details WHERE GrantTemplateID='".$xid."'");
					   if($detail!=''){
					    $tmparr = explode(',',$detail);
						$sqno   = 1;
						foreach($tmparr as $k=>$v){
						  $tmprs = explode('|',$v);
						  $exec  = $this->model->save_detatils($xid,$tmprs[0],$tmprs[1],$sqno,$tmprs[2],$tmprs[3]);
						  $sqno++;
						}
					   }
					}
					
					$response = Response::json([
												'success' => true
					                           ,'content' => ''
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
				break;
			}
			return $response;
        }   
        return $response;		
	}
	
	private function init($key = null)
	{
		$this->initializer();
		return array('tdata'=>$this->model->get_data(0));
	}

	private function initializer(){
		$this->services   = new Services;
		$this->model 	  = new ScholarModel;
		$this->permission = new Permission('department');
	}
}	
?>