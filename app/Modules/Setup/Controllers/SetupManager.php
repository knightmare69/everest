<?php

namespace App\Modules\Setup\Controllers;

use App\Modules\Setup\Services\SetupManagerSP as Services;
use App\Modules\Setup\Models\Buildings;
use App\Modules\Setup\Models\Rooms;
use App\Modules\Setup\Models\RoomTypes;
use App\Modules\Setup\Models\Campus;
use App\Modules\Setup\Models\AcademicYearTerm as AY_Term;

use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;
use Permission;
use DB;

class SetupManager extends Controller
{
    private $module_name = '';

    private $media = [
        'Title' => '',
        // 'Description' => 'Welcome To Chart of Accounts',
        'js' => ['Setup/index'],
        'init' => [1 => 'SetupManager.run()'],
        'plugin_js' => [
            'bootbox/bootbox.min',
            'datatables/media/js/jquery.dataTables.min',
            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
            'datatables/extensions/Scroller/js/dataTables.scroller.min',
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2.min',
            'smartnotification/smartnotification.min',
        ],
        'plugin_css' => [
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2',
            'smartnotification/smartnotification',
        ],
    ];

    public $views = 'Setup.Views.';

    // public function __construct($type)
    // {
    //     $this->initializer();
    // }

    public function index($type)
    {
        if (isParent()) {
            return redirect('/security/validation');
        }

        $this->module_name = $type;
        $this->initializer();

        if ($this->permission->has('read')) {
            $url = ['page' => 'setup/'.$type];
            $view = $this->views;

            switch ($type) {
                case 'rooms-buildings':
                    $bldg = new Buildings;
                    $rms = new Rooms;

                    $view .= 'RoomsBldg';

                    $this->media['Title'] = 'Rooms & Buildings';
                    $this->media['js'][1] = 'Setup/rooms-bldgs';
                    $this->media['init'][0] = 'RoomsBuildings.run()';

                    $data = ['bldg' => $bldg->get(), 'rooms' => $rms->get(), 'campus' => Campus::select('CampusID', 'ShortName')->get(), 'room_types' => DB::select("SELECT * FROM ES_RoomTypes")];
                    break;

                case 'academic-yearterm':
                    $srvc = new Services;

                    $data = ['ayterm' => AY_Term::get(), 'terms' => $srvc->terms];
                    $view .= 'term.';
                    array_push($this->media['plugin_css'], 'bootstrap-daterangepicker/daterangepicker-bs3');
                    array_push($this->media['plugin_js'], 'bootstrap-daterangepicker/moment.min');
                    array_push($this->media['plugin_js'], 'bootstrap-daterangepicker/daterangepicker');
                    $this->media['Title'] = 'Academic Year & Term';
                    $this->media['js'][1] = 'Setup/ayterm';
                    $this->media['init'][0] = 'AY_Term.run()';

                    break;

                default:
                    $view .= '';
                    $data = [];
                    break;
            }

            return view('layout', array('content' => view($view.'.index')->with(array_merge(['views' => $view], $data)), 'url' => $url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event($type, Request $req)
    {
        $this->module_name = $type;
        $this->initializer();
        $services = new Services();

        $form = $req->get('form');
        $action = $req->get('e');
        $id = decode($req->get('id'));
        parse_str($req->get('data'), $data);

        $response = ['error' => false, 'message' => 'Unable to perform action.'];

        if ($this->permission->has('read') && $action == 'get') {
            switch ($form) {
                case 'bldg':
                case 'rooms':
                    if (!empty($id)) {
                        if ($form == 'bldg') {
                            $data = ['data' => Buildings::find($id), 'campus' => Campus::select('CampusID', 'ShortName')->get(), 'with_action' => true];

                        } else if ($form == 'rooms') {
                            $data = ['data' => Rooms::find($id), 'campus' => Campus::select('CampusID', 'ShortName')->get(), 'bldg' => Buildings::select('BldgID', 'BldgName')->get(), 'room_types' => RoomTypes::get(), 'with_action' => true];
                        }

                        $vw = view($this->views.'.RoomsBldg.'.$form.'.form', $data)->render();

                        $response = ['error' => false, 'message' => 'Done!', 'form' => $vw];

                    } else {
                        $response = ['error' => true, 'message' => 'Unable to find record.'];
                    }
                    break;

                case 'ayterm':
                    if (!empty($id)) {
                        $data = ['data' => AY_Term::find($id), 'terms' => $services->terms, 'with_action' => true];

                        $vw = view($this->views.'.term.forms.new-form', $data)->render();

                        $response = ['error' => false, 'message' => 'Done!', 'form' => $vw];

                    } else {
                        $response = ['error' => true, 'message' => 'Unable to find record.'];
                    }
                    break;
            }

        } else if ($this->permission->has('add') && $action == 'save') {
            switch ($form) {
                case 'room-type':
                    $name = $req->get('name');

                    if (!empty($id)) {
                        RoomTypes::where('TypeID', $id)->update(['RoomType' => $name]);
                    } else {
                        $id = RoomTypes::insertGetId(['RoomType' => $name]);
                    }

                    $response = ['error' => false, 'message' => 'Successfully saved.', 'name' => $req->get('name'), 'id' => encode($id)];
                    break;

                case 'bldg':
                case 'rooms':
                    if ($form == 'bldg') {
                        $data['campus'] = decode($data['campus']);
                        $instance = new Buildings;
                        $updt_field = 'BldgID';

                    } else if ($form == 'rooms') {
                        $data['campus'] = decode($data['campus']);
                        $data['building'] = decode($data['building']);
                        $data['type'] = !empty($data['type']) ? decode($data['type']) : 0;
                        $instance = new Rooms;
                        $updt_field = 'RoomID';
                    }

                    $v =  $services->_validate($data, $form);

                    if (empty($v['messages'])) {
                        $fields = $services->dbfields($data, $form);

                        if (!empty($id)) {
                            $instance->where($updt_field, $id)->update($fields);
                        } else {
                            $id = $instance->insertGetId($fields);
                        }

                        $data['id'] = $id;

                        if ($form == 'bldg') {
                            $data['campus_name'] = Campus::find($data['campus'])->ShortName;

                        } else if ($form == 'rooms') {
                            $data['campus_name'] = Campus::find($data['campus'])->ShortName;
                            $data['type'] = !empty($data['type']) ? RoomTypes::find($data['type'])->RoomType : 'None';
                            $data['building'] = Buildings::find($data['building'])->BldgName;

                        }

                        $row = $services->tbleTD($form, $data);

                        $response = ['error' => false, 'message' => 'Successfully saved record.', 'row' => $row];

                    } else {
                        $response = ['error' => true, 'message' => 'Unable to proceed.', 'err' => $v];

                    }
                    break;

                case 'ayterm':
                    $v =  $services->_validate($data, $form);

                    if (empty($v['messages'])) {
                        $fields = $services->dbfields($data, $form);

                        if (!empty($id)) {
                            AY_Term::where('TermID', $id)->update($fields);
                        } else {
                            $id = AY_Term::insertGetId($fields);
                        }

                        $data['id'] = $id;

                        $row = $services->tbleTD($form, $data);

                        $response = ['error' => false, 'message' => 'Successfully saved record.', 'row' => $row];

                    } else {
                        $response = ['error' => true, 'message' => 'Unable to proceed.', 'err' => $v];

                    }
                    break;
                case 'school-days':
                    $data = $req->get('update');
                    $seq = $req->get('seq');
                    $cols = [];
                    $total=0;
                    $seqdata="";
                    foreach($data as $d){
                        $cols[$d['code']] = $d['val'];
                        $total += floatval($d['val']);
                    }
                                        
                    asort($seq);                    
                    foreach($seq as $s => $v){
                        if ($s !='') $seqdata .= $s.",";                        
                    }
                    
                    $cols['TermID'] = $id;
                    $cols['TOTAL'] = $total;
                    $cols['SeqNo'] = $seqdata;
                    
                    $r= AY_Term::updateSchoolDays($id, $cols);
                    $response = ['error' => false, 'message' => 'Successfully saved record.'];
                break;
            }

        } else if ($this->permission->has('delete') && $action == 'remove') {
            switch ($form) {
                case 'bldg':
                case 'rooms':

                    if ($form == 'bldg') {
                        Buildings::where('BldgID', $id)->delete();
                    } else if ($form == 'rooms') {
                        Rooms::where('RoomID', $id)->delete();

                    } else {
                        return ['error' => true, 'message' => 'Unable to remove this record.'];
                    }

                    $response = ['error' => false, 'message' => 'Successfully removed record.'];
                    break;

                case 'ayterm':
                    $ayt_m = new AY_term;

                    $active_term = $ayt_m->isTermActive($id);

                    if ($active_term == false) {
                        $ayt_m->where('TermID', $id)->delete();

                        $response = ['error' => false, 'message' => 'Successfully removed record.'];
                    } else {
                        $response = ['error' => true, 'message' => 'Unable to remove this record.'];
                    }
                    break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->permission = new Permission($this->module_name);
    }
}
