<?php

namespace App\Modules\Setup\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\MedicalServiceProvider as Services;
use App\Modules\Setup\Models\Testing as TestingModel;
use Request;
use Response;
use Permission;
use App\Libraries\pdf\TCPDF;

class Testing extends Controller
{
    private $media =
        [
            'Title' => 'Testing',
            'Description' => 'manage testing schedule',
            'js' => ['Setup/medical'],
            'init' => ['Metronic.init()', 'Medical.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'bootstrap-timepicker/js/bootstrap-timepicker',
                            'select2/select2.min',
                        ],
            'plugin_css' => ['bootstrap-datepicker/css/datepicker', 'bootstrap-timepicker/css/bootstrap-timepicker', 'select2/select2', 'datatables/plugins/bootstrap/dataTables.bootstrap'],
        ];

    private $url = ['page' => 'setup/testing/', 'form' => '.mt-form'];

    public $views = 'Setup.Views.Testing.';
    private $inherit_view = 'Setup.Views.Medical.';

    private $module = 'Testing';

    public function index()
    {
        $this->initializer();
        if ($this->permission->has('read')) {
          $_incl = [
              'views' => $this->views,
              'at' => $this->model->AcademicTerm(),
            ];
            return view('layout', array('content' => view($this->views.'index', $this->init())->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'save':
                    if ($this->permission->has('add')) {
                        $validation = $this->services->isValid(Request::all());

                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $data = $this->services->post(Request::all());
                            $this->model->create($data);
                            $response = ['error' => false, 'message' => 'Successfully Save!', 'table' => $this->refreshTable()];
                        }
                    }
                    break;
                case 'update':
                    if ($this->permission->has('edit')) {
                        $validation = $this->services->isValid(Request::all(), 'update');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $data = $this->services->post(Request::all());
                            $this->model->where('TestingSchedID', decode(Request::get('id')))->update($data);
                            $response = ['error' => false, 'message' => 'Successfully Update!', 'table' => $this->refreshTable()];
                        }
                    }
                    break;
                case 'delete':
                    if ($this->permission->has('delete')) {
                        $id = decode(Request::get('id'));
                        $count = $this->model->check_remove($id);
                        if($count >= 1){
                            $response = ['error' => true, 'message' => 'Unable to delete this record.'];
                        } else {
                            $this->model->destroy($id);
                            $response = ['error' => false, 'message' => 'Successfully Deleted '.$count, 'table' => $this->refreshTable()];
                        }
                    }
                    break;
                case 'edit':
                    $response = view($this->inherit_view.'form', [
                         'data' => $this->model->find(decode(Request::get('id'))), 'grade_level' => $this->services->grade_level(),
                         'at' => $this->services->academic_year(),
                         'campus' => $this->services->campus(),
                     ]);
                    break;
				case 'table_get':
					$data = json_decode(Request::get('search'));
					$search_for = array(decode($data->ac_year), decode($data->campus));
					$response = array('error' => false, 'table' => view($this->inherit_view.'list', ['table' => $this->model->get_view($search_for), 'form' => $this->module])->render());
					break;
            }
        }

        return $response;
    }

    public function print_data()
    {
        ob_clean();
        // $get_data = Request::all();
        $test_model = new TestingModel();
        $data = $test_model->get_view();

        $pdf = new TCPDF;
        $pdf->SetPrintHeader(false);
        // $pdf->setHeaderData('', '', '','Medical Schedule');
        $pdf->SetPrintFooter(false);
        $pdf->SetTitle('Testing Schedules');
        // $header_widths = array(10, 30, 30, 30, 50, 20, 20);
        // $headers = array('#', 'Grade Level', 'Batch Code', 'Medical Date', 'Medical Time', 'Limit', 'Total');

        $pdf->AddPage();
        // $pdf->SetDrawColor(221, 221, 221);
        // $this->SetFont('helvetica', 'B', 10);
        // $pdf->setHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Medical Schedule', PDF_HEADER_STRING);
        // $pdf->Ln();
        // for($a = 0; $a < count($headers); $a++){
        //     $pdf->Cell($header_widths[$a], '', $headers[$a], 'LTRB', 0, 'C', false);
        // }
        //
        // $pdf->Ln();
        // $cnt = 1;
        // foreach($data as $this_data){
        //     $explode = explode(' ', $this_data->Description, 2);
        //     $date = $explode[0];
        //     $time = $explode[1];
        //
        //     $pdf->Cell($header_widths[0], '', $cnt, 'LRB', 0, 'C', false);
        //     $pdf->Cell($header_widths[1], '', $this_data->YearLevelName, 'LRB', 0, 'L', false);
        //     $pdf->Cell($header_widths[2], '', $this_data->BatchName, 'LRB', 0, 'L', false);
        //     $pdf->Cell($header_widths[3], '', $date, 'LRB', 0, 'L', false);
        //     $pdf->Cell($header_widths[4], '', $time, 'LRB', 0, 'L', false);
        //     $pdf->Cell($header_widths[5], '', $this_data->Limit, 'LRB', 0, 'C', false);
        //     $pdf->Cell($header_widths[6], '', $this_data->Total, 'LRB', 0, 'C', false);
        //     $pdf->Ln();
        //     $cnt++;
        // }
        $pdf->writeHTML(view($this->inherit_view.'print', ['data' => $data, 'label' => 'Testing'])->render());
        $try = $pdf->Output('Sample.pdf', 'I');
        return $try;
    }

    private function init($key = null)
    {
        $this->initializer();

        return array(
            'table' => $key == null ?  $this->model->get_view() : array(),
            'grade_level' => $this->services->grade_level(),
            'academic_year' => $this->services->academic_year(),
            'campus' => $this->services->campus(),
            'form' => $this->module,
        );
    }

    private function refreshTable()
    {
        return view($this->inherit_view.'list', ['table' => $this->model->get_view(), 'form' => $this->module])->render();
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new TestingModel();
        $this->permission = new Permission('setup-testing');
    }
}
