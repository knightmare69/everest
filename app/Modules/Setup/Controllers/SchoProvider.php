<?php
namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Models\SchoProvider as Model;
use Request;
use Response;
use Permission;
use DB;
use URL;

/**
 * Modified by : Allan Robert B. Sanchez
 * Feb. 29, 2020 13:17H
 * Added Column
 * - IsGov
 * - Non Billing Financial Assistance
 * - Inactive
*/
class SchoProvider extends Controller {
	private $media =
		[
			'Title'=> 'Scho Provider',
			'Description'=> 'Manage',
			'js'		=> [
                'Setup/scho-provider.js?v1.0.2'
            ],
			'init'		=> [
                'Me.init()',
            ],
			'plugin_js'	=> [
                'bootbox/bootbox.min',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
            ],
			'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
			]
		];

	private $url = [ 'page' => 'setup/scho-provider/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.SchoProvider.';

 	function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = ['error'=>true,'message'=>'No Event Selected'];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'addProvider':
                    $response = view($this->views.'modals.scho_provider_form')->render();
                    break;
                case 'updateProvider':
                    if (Request::has('id')){
                        $id = Request::get('id');
                        if (Model::find($id)){
                            $model = Model::find($id);
                            $response = view($this->views.'modals.scho_provider_form',array('info' => $model))->render();
                        }
                    }
					break;
				case 'removeProvider':
					if (Request::has('id')){
						$id = Request::get('id');
						$exist = DB::table('es_othertransactions')->where('IDType', $id)->count();
						if($exist > 0) {
							$response = ['error'=>true,'message'=>'Provider was used!'];
						} else {
							$query = Model::where('SchoProviderID',$id)->delete();
							if($query) {
								$response = ['error'=>false,'message'=>'Successfully removed provider!'];
							} else {
								$response = ['error'=>true,'message'=>'Please check setup, somethings wrong!'];
							}
						}
					}
					break;
                case 'saveProvider':
                    $response['message'] = 'Unable to add provider, Please try again later!';
                    $post = Request::all();
                    if (Request::has('code') && Request::has('name') && Request::has('shortname')){
                        $code = Request::get('code');
                        $name = Request::get('name');
                        $shortname = Request::get('shortname');
                        $acronym = Request::has('acronym')?Request::get('acronym'):'';
                        $gov = Request::has('gov')? 1 : 0 ;
                        $validate = Request::has('validate')? 1 : 0 ;
                        $billing = Request::has('billing')? 1 : 0 ;
                        $inactive = Request::has('inactive')? 1 : 0 ;
                        $id = Request::has('id')?Request::get('id'):0;

                        $model = new Model;

                        $status = true;

                        if ($id == 0 && $model->where('ProvCode',$code)->count() > 0){
                            $status = false;
                            $response['message'] = 'Unable to add provider, Code already exists!';
                        }
                        else if ($id != 0 && $model->where('ProvCode',$code)->where('SchoProviderID','<>',$id)->count() > 0){
                            $status = false;
                            $response['message'] = 'Unable to add provider, Code already exists!';
                        }

                        if ($status){
                            if ($id != 0 && Model::find($id)){
                                $model = Model::find($id);
                            }
                            $model->fill(array(
                                'ProvCode' => $code,
                                'ProvName' => $name,
                                'ProvShort' => $shortname,
                                'ProvAcronym' => $acronym,
                                'Remarks' => getObjectValue($post,'remarks'),
                                'ValidateReg'=> $validate,
                                'IsGov' => $gov,
                                'IsAutoCredit' => $billing,
                                'Inactive' => $inactive

                            ));

                            if ($model->save()){
                                $response = ['error'=>false,'message'=>'Done saving provider!'];
                            }
                        }

                    }
				    break;
			}
		}
		return $response;
	}

	private function init($key = null)
	{
		$this->initializer();
		return array(
			'table' => $key == null ?  $this->model->where('IsGov',0)->get() : array()
		);
	}

	public function data()
	{
	    $hasParam = false;
		if (Request::ajax())
		{
			if (Request::has('draw') && Request::get('draw')){
				$data = array();
				$select = array(
                    'SchoProviderID',
					'ProvCode',
					'ProvName',
					'ProvAcronym',
					'ProvShort',
                    'Remarks',
                    'IsGov',
                    'IsAutoCredit',
                    'Inactive',
				);

				$start = Request::get('start');
				$length = Request::get('length');
				$search = Request::get('search');
				$order = Request::get('order');
				$model = Model::select($select);

				if (isset($search) && isset($search['value']) && !empty($search['value'])){
					$new_search = $search['value'];
					$model = $model->where(function($q) use ($new_search, $select){
						$q->where($select[1],'like',$new_search.'%')
							->orWhere($select[2],'like',$new_search.'%')
							->orWhere($select[4],'like','%'.$new_search.'%');
					});
                }

				// $data = $model->where('IsGov',0)->get();
				$data = $model->get();
				
				$iTotalRecords = $model->count();

				$iDisplayLength = intval($length);
				$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
				$iDisplayStart = intval($start);

				$end = $iDisplayStart + $iDisplayLength;
				$end = !$hasParam ? $end > $iTotalRecords ? $iTotalRecords : ($end == $iTotalRecords) ? 0 : $end : 0;
				$data = $model->skip($end)->take($iDisplayLength)->get();
				$data = $model->offset($iDisplayStart)->limit($iDisplayLength)->get();
				$data = $model->skip($iDisplayStart)->take($iDisplayLength)->get();

				return json_encode(array(
					'data' => $data,
					'recordsTotal' => count($data),
					'recordsFiltered' => count($data),
					'draw' =>intval(Request::get('draw')),
					'recordsTotal' => $iTotalRecords,
					'recordsFiltered' => $iTotalRecords,
				));
			}
		}
	}

	private function initializer()
	{
		$this->model = new Model;
		$this->permission = new Permission('department');
	}
}