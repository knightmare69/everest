<?php
namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\ProgramServiceProvider as Services;
use App\Modules\Setup\Models\Scholarship as ScholarModel;
use Request;
use Response;
use Permission;
use DB;

class Scholarship extends Controller{
	private $media =
		[
			'Title'=> 'Grant Template',
			'Description'=> 'Manage Scholarship Grant Template/s',
			'js'		=> ['crud', 'Setup/programs'],
			'init'		=> ['CRUD.init()','FN.dataTable()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap'
						]
		];

	private $url = [ 'page' => 'setup/programs/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.Programs.';


 	function index(){
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	} 
	
	
	private function init($key = null)
	{
		$this->initializer();
		return array();
	}

	private function initializer(){
		$this->services   = new Services;
		$this->model 	  = new ScholarModel;
		$this->permission = new Permission('department');
	}
}	
?>