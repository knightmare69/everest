<?php 
namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Models\Content as ContentModel;
use Request;
use Response;
use Permission;

class Content extends Controller {
	private $media =
		[
			'Title'=> 'Content Management',
			'Description'=> 'Manage',
			'js'		=> ['Setup/Content'],
			'init'		=> ['Content.init()','FN.dataTable()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
				            'bootstrap-summernote/summernote.min',
						],
			'plugin_css' => [
				'bootstrap-summernote/summernote'
			],
		];

	private $url = [ 'page' => 'cms/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.Content.';

 	function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function edit($key)
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'edit_index',['key'=>$key])->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function newTemplate()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'edit_index',['key'=>''])->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = ['error'=>true,'message'=>'No Event Selected'];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
				case 'save':
						$response = $this->save();
					break;
			}
		}
		return $response;
	}

	private function save()
	{	
		if (Request::get('key')) {
			$this->model
				->where('ContentID',Request::get('key'))
				->update(
						assertModified([
								'ContentType' => trim(Request::get('type')),
								'Title' => trim(Request::get('Title')),
								'ShortDesc' => trim(Request::get('ShortDesc')),
								'Content' => trim(Request::get('Content')),
							])
					);
			SystemLog('Setup','Content','save','save',Request::all(),'Success!');
			return successSave(['key'=>Request::get('key')]);
		} else {
			$status = $this->model
				->create(
						assertCreated([
								'ContentType' => trim(Request::get('type')),
								'ContentCode'=> trim(Request::get('code')),
								'ContentName' => str_replace([' ','_'],'-',strtolower(trim(Request::get('Title')))),
								'Title' => trim(Request::get('Title')),
								'ShortDesc' => trim(Request::get('ShortDesc')),
								'Content' => trim(Request::get('Content')),
							])
					);
			SystemLog('Setup','Content','save','save',Request::all(),'Success!');
			return successSave(['key'=>$status->ContentID]);
		}
		return errorSave();
	}

	private function init($key = null) 
	{
		$this->initializer();
		return array(
			'table' => $key == null ?  $this->model->all() : array()
		);
	}

	private function refreshTable()
	{
		return view($this->views.'tables.table',['table'=>$this->model->all()])->render();
	}

	private function initializer()
	{
		$this->model = new ContentModel;
		$this->permission = new Permission('CMS');
	}
}