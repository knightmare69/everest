<?php

namespace App\Modules\Setup\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\RegConfigServiceProvider as Services;
use Request;
use Response;
use Permission;
use DB;

class RegConfig extends Controller
{
    private $media =
        [
            'Title' => 'Registration Configuration',
            'Description' => 'manage configuration',
            'js' => ['Setup/regconfig'],
            'init' => ['Metronic.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'bootstrap-timepicker/js/bootstrap-timepicker',
                            'select2/select2.min',
                        ],
            'plugin_css' => ['bootstrap-datepicker/css/datepicker', 'bootstrap-timepicker/css/bootstrap-timepicker', 'select2/select2', 'datatables/plugins/bootstrap/dataTables.bootstrap'],
        ];

    private $url = ['page' => 'setup/regconfig/', 'form' => '.mt-form'];

    public $views = 'Setup.Views.RegConfig.';
    private $inherit_view = 'Setup.Views.RegConfig.';

    public function index()
    {
        $this->initializer();
        if ($this->permission->has('read')) {
            return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event(){
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'save':
				    $input = Request::all();
					foreach($input as $key => $value){
					 $this->xconfig->sp_xconfigMgmt($key,$value);
					 SystemLog('Registration Configuration','','Registration Configuration','Alter Configuration','Key:'.$key.' Val:'.$value,'');
                    }
                    $response = ['error' => false, 'message' => 'Successfully Save!', 'table' => ''];
                    break;
            }
        return $response;
		}else{
		echo 'hello';
		}
    }

    private function init($key = null)
    {
        $this->initializer();

        return array('configlist'=>$this->xconfig->loadlist(1)
		            ,'testing'   => 'testing lang'
					);
    }

    private function refreshTable()
    {
        return view($this->inherit_view.'list', ['table' => $this->model->get_view()])->render();
    }

    private function initializer()
    {
        $this->permission = new Permission('department');
        $this->xconfig    = new Services();
    }
}
