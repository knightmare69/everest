<?php

namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\ProgramServiceProvider as Services;
use App\Modules\Setup\Models\Programs as ProgramsModel;
use App\Modules\Setup\Models\ProgramClass as ClassModel;
use App\Modules\Setup\Models\ProgramMajors as MajorsModel;
use Request;
use Response;
use Permission;
use DB;

class Programs extends Controller{
	private $media =
		[
			'Title'=> 'Programs',
			'Description'=> 'Manage Program/s,Class/es and Major Discipline/s',
			'js'		=> ['crud', 'Setup/programs'],
			'init'		=> ['CRUD.init()','FN.dataTable()'],
			'plugin_js'	=> [
							'bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap'
						]
		];

	private $url = [ 'page' => 'setup/programs/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.Programs.';


 	function index(){
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = ['error'=>true,'message'=>'No Event Selected'];
		if (Request::ajax())
		{
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
			    case 'empl':
				    $empl     = DB::select("SELECT EmployeeID,LastName,FirstName,MiddleInitial FROM HR_Employees WHERE Inactive=0");
					$content  = (string)view($this->views.'tables.filter',['filter'=> $empl,]);
					$response = ['error'=>false,'message'=>'','content'=>$content];
				break;
				case 'save':
					if ($this->permission->has('add')) {
					    switch(Request::get('xtarget')){
						case 'program':
							$validation = $this->services->isValid(Request::all());
							if ($validation['error']) {
								$response = Response::json($validation);
							} else {
								$data = $this->services->post(Request::all());
								$data['created_date'] = systemDate();
								$data['created_by'] = getUserName();
								$this->model->create($data);
								$response = ['error'=>false,'message'=>'Successfully Save!','table'=> $this->refreshTable()];
							}
						break;
						case 'class':
						    $pdata= Request::all();
							$data = array('ClassCode'=>((intval($pdata['code'])>0)?intval($pdata['code']):0)
							             ,'ClassDesc'=>$pdata['name']
							             ,'ShortName'=>$pdata['short']
							        );
							$this->cmodel->create($data);
							$response = ['error'=>false,'message'=>'Successfully Save!','table'=>$this->refreshTable('class')];
						break;
						case 'major':
						    $pdata  = Request::all();
							$lastid = DB::select("SELECT TOP 1 * FROM ES_DisciplineMajors ORDER BY IndexID DESC");
							$lastig = DB::select("SELECT TOP 1 * FROM ES_DisciplineMajorGroups ORDER BY IndexID DESC");
							if($pdata['group']=='-1'){
							   $gdata = array('MajorGroupCode' => (($lastig && count($lastig)>0)?(intval($lastig[0]->MajorGroupCode)+1):0)
							                 ,'MajorGroupDesc' => $pdata['gname']
									    );
							   $pdata['group'] = DB::table('ES_DisciplineMajorGroups')->insertGetId($gdata);
							}
							
							$data   = array('MajorDiscCode' => ((intval($pdata['code'])>0)?intval($pdata['code']):0)
							               ,'MajorDiscDesc' => $pdata['name']
							               ,'MajorGroup'    => ((intval($pdata['group'])>0)?intval($pdata['group']):1)
							            );
							$exec     = DB::table('ES_DisciplineMajors')->insert($data);
							$response = ['error'=>false,'message'=>'Successfully Save!','table'=>$this->refreshTable('major')];
						break;
						}
					}
					break;
				case 'update':
					if ($this->permission->has('edit')) {
					  $pdata   = Request::all();
					  $xtarget = ((@array_key_exists('xtarget',$pdata))?($pdata['xtarget']):'program');
					  if(@strpos($pdata['id'],$xtarget)!==false){
					    list($xtarget,$pdata['id']) = explode("@",$pdata['id']); 
					  }
					  
					  switch($xtarget){
					    case 'class':
							$data = array('ClassCode'=>((intval($pdata['code'])>0)?intval($pdata['code']):0)
							             ,'ClassDesc'=>$pdata['name']
							             ,'ShortName'=>$pdata['short']
							        );
							$this->cmodel->where('ClassCode',decode($pdata['id']))->update($data);
						    $response = ['error'=>false,'message'=>'Successfully Update!','table'=> $this->refreshTable('class'),'data'=>$data];
						break;
					    default:
						$validation = $this->services->isValid($pdata,'update');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							$data = $this->services->post($pdata);
							$this->model->where('ProgID',decode($pdata['id']))->update($data);
							$response = ['error'=>false,'message'=>'Successfully Update!','table'=> $this->refreshTable(),'data'=>$data];
						}
						break;
					 }	
					}
					break;
				case 'delete':
					if ($this->permission->has('delete')) {
					    $ids = json_decode('['.Request::get('ids').']',true);
						$xtarget= 'program';
						foreach($ids as $id){
							$xid    = $id['id'];
							if(@strpos($xid,"@")!==false){
							 list($xtarget,$xid) = explode("@",$xid);
							}
							switch($xtarget){
							  case 'class':
							  $this->cmodel->destroy(decode($xid));
							  break;
							  case 'major':
							  $exec = DB::statement("DELETE FROM ES_DisciplineMajors WHERE IndexID='".decode($xid)."'");
							  break;
							  default:
							  $this->model->destroy(decode($xid));
							  break;
							}
						}
						$response = ['error'=> false,'message'=>'Successfully Deleted','target'=>$xtarget,'table'=> $this->refreshTable($xtarget)];
					}
					break;
				case 'acOrdc':
					if ($this->permission->has('activate/deactivate')) {
						$ids = json_decode('['.Request::get('ids').']',true);
						foreach($ids as $id)
						{
							 $this->model->where('index_id',decode($id['id']))->update(['is_inactive'=>$id['status']]);
						}
						$response = ['error'=> false,'message'=>'Successfully update status','table'=> $this->refreshTable()];
					}
					break;
				case 'edit':
				    $xtarget= 'program';
				    $xid    = Request::get('id');
					if(@strpos($xid,"@")!==false){
					 list($xtarget,$xid) = explode("@",$xid);
					}
					if($xtarget=='class'){
						$data = $this->cmodel->find(decode($xid));
						$response = view($this->views.'forms.fclass',['cdata'=> $data,]);
					}else if($xtarget=='major'){
						$data = DB::SELECT("SELECT * FROM ES_DisciplineMajors WHERE IndexID='".decode($xid)."'");
						$response = view($this->views.'forms.fmajor',['mdata'=> (($data && count($data)>0)?$data[0]:array()),]);
					}else{
						$data = $this->model->find(decode($xid));
						$response = view($this->views.'forms.form',['data'=> $data,]);
					}
					break;
			}
		}
		return $response;
	}

	private function init($key = null)
	{
		$this->initializer();
		return array(
			'table'  => $key == null ?  $this->model->all() : array()
		   ,'tclass' => $key == null ?  $this->cmodel->all() : array()
		   ,'tmajor' => $key == null ?  (DB::select("SELECT * FROM ES_DisciplineMajors")) : array()
		);
	}

	private function refreshTable($target='program'){
	    switch($target){
		  case 'class':
		  return view($this->views.'tables.tclass',['tclass'=>$this->cmodel->all()])->render();
		  break;
		  case 'major':
		  return view($this->views.'tables.tmajor',['tmajor'=>(DB::select("SELECT * FROM ES_DisciplineMajors"))])->render();
		  break;
		  default:
		  return view($this->views.'tables.table',['table'=>$this->model->all()])->render();
		  break;
		}
	}

	private function initializer(){
		$this->services   = new Services;
		$this->model 	  = new ProgramsModel;
		$this->cmodel 	  = new ClassModel;
		$this->mmodel 	  = new MajorsModel;
		$this->permission = new Permission('department');
	}
}
?>