<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\College\Validation;

Class CollegeServiceProvider {

	public function __construct()
	{
		$this->validation = new Validation;
	}

	public function isValid($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}

		return ['error'=>false,'message'=> ''];
	}

	public function post($post)
	{
		$dept_head = !empty($post['dept-head-name']) ? $post['dept-head-name'] : null;

		return [
				'CollegeCode'       => getObjectValue($post,'code'),
				'CollegeName'       => getObjectValue($post,'name'),
				'CampusID'          => decode(getObjectValueWithReturn($post,'campus',0)),
				'CollegeDean'       => $dept_head,
				'CollegeNumberCode' => getObjectValue($post,'idcode'),
			];
	}
}	
?>