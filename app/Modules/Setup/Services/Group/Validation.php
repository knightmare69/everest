<?php
namespace App\Modules\Setup\Services\Group;
use Validator;
Class Validation {
	
	public function validateSave($post)
	{
		$validator = validator::make(
			['name'	=> getObjectValue($post,'name')],			
			['name'		=> 'required']
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			['name'	=> getObjectValue($post,'name')],			
			['name'		=> 'required']
		);
		return $validator;
	}
}	
?>