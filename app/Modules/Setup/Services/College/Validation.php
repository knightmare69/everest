<?php
namespace App\Modules\Setup\Services\College;
use Validator;
Class Validation {

	public function validateSave($post)
	{
		$validator = validator::make(
			[
				'CollegeCode' => getObjectValue($post,'code'),
				'CollegeName' => getObjectValue($post,'name')
			],
		    []
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			[
				'CollegeCode' => getObjectValue($post,'code'),
				'CollegeName' => getObjectValue($post,'name')
			],
			[]
		);
		return $validator;
	}
}	
?>