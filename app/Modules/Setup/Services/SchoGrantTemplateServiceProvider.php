<?php
namespace App\Modules\Setup\Services;

Class SchoGrantTemplateServiceProvider {

	public function __construct(){
		$this->validation = array();
	}
	
	public function isValid($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails()){
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}

		return ['error'=>false,'message'=> ''];
	}

	public function post($post){
		return [
				'TermID'              => getObjectValue($post,'term'),
				'CampusID'            => 1,
				'TemplateCode'        => getObjectValue($post,'code'),
				'ShortName'           => getObjectValue($post,'name'),
				'Description'         => getObjectValue($post,'desc'),
				'SchoProviderID'      => getObjectValue($post,'provider'),
				'SchoType'            => getObjectValue($post,'type'),
				'Inactive'            => getObjectValue($post,'inactive'),
			];
	}
	 
	public function post_class($post){
		return [
				'ClassDesc' => getObjectValue($post,'desc'),
				'ShortName' => getObjectValue($post,'short'),
				'GroupClass'=> 0,
			];
    }			
}	        
?>