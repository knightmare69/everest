<?php namespace App\Modules\Setup\Services\Institution;

use Validator;

Class Validation {

	public function validateSave($post)
	{
		 $validator = validator::make(
            [
                //'InstClassID'           => getObjectValue($post, 'classinst'),
                //'HeadID'                => getObjectValue($post, 'headinst'),
                //'HeadPosTitleID'        => getObjectValue($post, 'headtitle'),
                'FormerName1'           => getObjectValue($post, 'name'),
                'UniqueID'              => getObjectValue($post, 'identifier'),
                'Address_Street'        => getObjectValue($post, 'street'),
                'Address_Municipality'  => getObjectValue($post, 'municipality'),
                'Address_CityProvince'  => getObjectValue($post, 'provincecity'),
                'Address_Region'        => getObjectValue($post, 'region'),
                'Address_ZipCode'       => getObjectValue($post, 'zipcode'),
                'TelNo'                 => getObjectValue($post, 'tel'),
                'FaxNo'                 => getObjectValue($post, 'fax'),
                'HeadTelNo'             => getObjectValue($post, 'headtel'),
                'EmailAddress'          => getObjectValue($post, 'email'),
                'WebSite'               => getObjectValue($post, 'website'),
                'YearEstablished'       => getObjectValue($post, 'yrestablished'),
                'LatestSecReg'          => getObjectValue($post, 'sec'),
                'DateGranted'           => getObjectValue($post, 'granted'),
                'YearConvertedCollege'  => getObjectValue($post, 'convcollege'),
                'YearConvertedUniversity' => getObjectValue($post, 'convuniversity')

            ],
            [
            	//'InstClassID'           => 'required',
                //'HeadID'                => 'required',
                //'HeadPosTitleID'        => 'required',
            	'FormerName1'        	=> 'required',
                'UniqueID'              => 'required|unique:es_institution',
                'Address_Street'        => 'required',
                'Address_Municipality'  => 'required',
                'Address_CityProvince'  => 'required',
                'Address_Region'        => 'required',
                'Address_ZipCode'       => 'required',
                'TelNo'                 => 'required',
                'FaxNo'                 => 'required',
                'HeadTelNo'             => 'required',
                'EmailAddress'          => 'required',
                'WebSite'               => 'required',
                'YearEstablished'       => 'required',
                'LatestSecReg'          => 'required',
                'DateGranted'           => 'required',
                'YearConvertedCollege'  => 'required',
                'YearConvertedUniversity' => 'required',
            ]
        );

		return $validator;
	}

	public function validateUpdate($post)
	{
		 $validator = validator::make(
            [
                //'InstClassID'           => getObjectValue($post, 'classinst'),
                //'HeadID'                => getObjectValue($post, 'headinst'),
                //'HeadPosTitleID'        => getObjectValue($post, 'headtitle'),
                'FormerName1'           => getObjectValue($post, 'name'),
                'UniqueID'              => getObjectValue($post, 'identifier'),
                'Address_Street'        => getObjectValue($post, 'street'),
                'Address_Municipality'  => getObjectValue($post, 'municipality'),
                'Address_CityProvince'  => getObjectValue($post, 'provincecity'),
                'Address_Region'        => getObjectValue($post, 'region'),
                'Address_ZipCode'       => getObjectValue($post, 'zipcode'),
                'TelNo'                 => getObjectValue($post, 'tel'),
                'FaxNo'                 => getObjectValue($post, 'fax'),
                'HeadTelNo'             => getObjectValue($post, 'headtel'),
                'EmailAddress'          => getObjectValue($post, 'email'),
                'WebSite'               => getObjectValue($post, 'website'),
                'YearEstablished'       => getObjectValue($post, 'yrestablished'),
                'LatestSecReg'          => getObjectValue($post, 'sec'),
                'DateGranted'           => getObjectValue($post, 'granted'),
                'YearConvertedCollege'  => getObjectValue($post, 'convcollege'),
                'YearConvertedUniversity' => getObjectValue($post, 'convuniversity')

            ],
            [
            	//'InstClassID'           => 'required',
                //'HeadID'                => 'required',
                //'HeadPosTitleID'        => 'required',
            	'FormerName1'        	=> 'required',
                'UniqueID'              => 'required',
                'Address_Street'        => 'required',
                'Address_Municipality'  => 'required',
                'Address_CityProvince'  => 'required',
                'Address_Region'        => 'required',
                'Address_ZipCode'       => 'required',
                'TelNo'                 => 'required',
                'FaxNo'                 => 'required',
                'HeadTelNo'             => 'required',
                'EmailAddress'          => 'required',
                'WebSite'               => 'required',
                'YearEstablished'       => 'required',
                'LatestSecReg'          => 'required',
                'DateGranted'           => 'required',
                'YearConvertedCollege'  => 'required',
                'YearConvertedUniversity' => 'required',
            ]
        );

		return $validator;
	}
}	
?>