<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Models\Requirements;
use App\Modules\Setup\Models\RequiredTemplates;
use App\Modules\Setup\Models\RequiredTemplatesItems;
use Request;

Class RequirementsServiceProvider {

	public function __construct()
	{
		$this->req = new Requirements;
		$this->temp = new RequiredTemplates;
		$this->items = new RequiredTemplatesItems;
	}

	public function isValid($post,$action = 'save')
	{	
		return ['error'=>false,'message'=> ''];
		if ($action == 'save') {
			$validate = $this->validation->validate($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} 
		
		return ['error'=>false,'message'=> ''];
	}

	public function showTemplates()
	{
		$data = [];
		foreach($this->temp->data()->where('t.Inactive','0')->get() as $row) {
			$data[] = [
				'id' => encode($row->TemplateID),
				'name' => ucfirst($row->TemplateName),
				'prog' => ucfirst($row->ClassDesc),
				'type' => $row->ApplicationType
			];
		}

		return $data;
	}

	public function saveRequirements()
	{
		$status = $this->req->create($data = $this->postRequiments());

		// if (getObjectValue($status,'ID')) {
		// 	if (count($this->getAttachment()) > 0) {
		// 		toStorage(AppConfig()->format.$this->getAttachment()['FileName'],$this->getAttachment()['Attachment']);
		// 	}
		// }

		return true;
	}

	public function saveTemplate()
	{
		$this->temp->create($this->postTemplates());
		return true;
	}

	public function saveTemplateRequirements()
	{	
		$this->removeUnselectedItems();
		$this->postItems();

		return true;
	}

	public function updateIsRequired()
	{
		if (Request::get('key'))
			return
				$this->items
					->where('TemplateID',decode(Request::get('key')))
					->where('RequirementID',decode(Request::get('RequirementID')))
					->update(['IsRequired'=>Request::get('IsRequired')]);

		return
			$this->req
				->where('ID',decode(Request::get('RequirementID')))
				->update(['IsRequired'=>Request::get('IsRequired')]);
	}


	public function updateAllowUpload()
	{
		if (Request::get('key'))
			return
				$this->items
					->where('TemplateID',decode(Request::get('key')))
					->where('RequirementID',decode(Request::get('RequirementID')))
					->update(['AllowUpload'=>Request::get('AllowUpload')]);
		return
			$this->req
				->where('ID',decode(Request::get('RequirementID')))
				->update(['AllowUpload'=>Request::get('AllowUpload')]);
	}

	public function updateSlot()
	{
		if (Request::get('key'))
			return
				$this->items
					->where('TemplateID',decode(Request::get('key')))
					->where('RequirementID',decode(Request::get('RequirementID')))
					->update(['Slot'=>Request::get('Slot')]);
	}

	public function updateCode()
	{
		if (Request::get('key'))
			return
				$this->req
					->where('ID',decode(Request::get('RequirementID')))
					->update(['Code'=>Request::get('Code')]);
	}

	private function postRequiments()
	{
		$FileName = '';



		return [
				'Code'=>  Request::get('code'),
				'Desc'=>  Request::get('description')
			];			
	}

	private function getAttachment() {
		if (isset($_FILES['file'])) {
			return getFileInfo($_FILES['file']);
		}
		return [];
	}

	private function postTemplates()
	{
		return [
				'TemplateName'=>  Request::get('template'),
				'ApplicationTypeID'=>  Request::get('applicationType'),
				'AppliedProgClassID'=>  Request::get('programClass'),
				'ForForeigner'=>  Request::get('isForeigner') ? 1 : 0
			];			
	}

	private function postTemplatesItem()
	{
		return [
				'TemplateName'=>  Request::get('template'),
			];			
	}

	private function getPostItem($data)
	{
		return [
			'TemplateID' => decode(Request::get('key')),
    		'RequirementID' => decode(getObjectValue($data,'id')),
    		'IsRequired' => getObjectValue($data,'IsRequired')			
		];
	}

	private function removeUnselectedItems()
	{
		$this->items
			->where('TemplateID',decode(Request::get('key')))
			->whereNotIn('RequirementID',$this->getPostSelItems())
			->delete();
	}

	private function getPostSelItems()
	{
		$data = Request::get('data');
		$datas = [];
		foreach(jsonToArray($data)[0] as $row) {
			$PK = 'id';
			$row = getRawData($row);
			if(hasRef($PK,$row)) {
				$datas[] = decode(getObjectValue($data,'key'));
			}
		}

		return $datas;
	}

	private function postItems()
	{	
		$data = Request::get('data');
		foreach(jsonToArray($data)[0] as $row) {
			$PK = 'key';
			$row = getRawData($row);

			if (!hasRef($key,$row)) {
				$this->items->create(
						assertCreated(
							$this->getPostItem($row)
						)
					);
			}
		}
	}
}	
?>