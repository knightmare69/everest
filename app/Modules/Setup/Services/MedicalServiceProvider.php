<?php

namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\Medical\Validation;
use DB;

class MedicalServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'save')
    {
        if ($action == 'save') {
            $validate = $this->validation->validatePost($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else {
            $validate = $this->validation->validatePost($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }

        return ['error' => false, 'message' => ''];
    }

    public function post($post)
    {
        $array = array(
            'TermID' => decode(getObjectValue($post, 'ac-year')),
            'CampusID' => decode(getObjectValue($post, 'campus')),
            'YearLevelID' => decode(getObjectValue($post, 'yl')),
            'ProgClass' => decode(getObjectValue($post, 'prog')),
            'BatchName' => getObjectValue($post, 'bc'),
            'Limit' => empty(getObjectValue($post, 'limit')) ? 0 : getObjectValue($post, 'limit'),
            'TimeFrom' => date('H:i', strtotime(getObjectValue($post, 'time-start'))),
            'TimeTo' => date('H:i', strtotime(getObjectValue($post, 'time-end'))),
        );

        $is_date = getObjectValue($post, 'is-days');

        if ($is_date == 0 || $is_date == '') {
            $days_by_abbr = $this->numToDays(getObjectValue($post, 'test-days'));
            $array['TestingDays'] = implode(',', getObjectValue($post, 'test-days'));
            $array['Description'] = $days_by_abbr.' '.getObjectValue($post, 'time-start').' - '.getObjectValue($post, 'time-end');
        } else {
            $array['IsSpecificDate'] = 1;
            $array['TestingDate'] = date('Y-m-d H:i:s', strtotime(getObjectValue($post, 'test-date')));
            $array['Description'] = getObjectValue($post, 'test-date').' '.getObjectValue($post, 'time-start').' - '.getObjectValue($post, 'time-end');
        }

        return $array;
    }

    private function numToDays($num_in_days)
    {
        $days = array('1' => 'M', '2' => 'T', '3' => 'W', '4' => 'Th', '5' => 'F', '6' => 'S', '7' => 'Su');
        $str = '';
        for ($a = 0; $a < count($num_in_days); $a++) {
            $str .= $days[(int) $num_in_days[$a]];
        }

        return $str;
    }

    public function grade_level()
    {
        $return_arr = array();

        $get_yl = DB::table('es_yearlevel')
                ->select('YearLevelID', 'YearLevelName', 'ProgClass', 'SeqNo', 'ProgID')
                ->get();

        $programs = collect($get_yl)->groupBy('ProgID')->toArray();

        $pluck_prog_class = array_fetch($get_yl, 'ProgID');
        $prog_class_ids = array_values(array_unique($pluck_prog_class, SORT_NUMERIC));

        $get_prog_class = DB::table('es_programs')->select('ProgID', 'ProgName', 'ProgCode')->whereIn('ProgID', $prog_class_ids)->orderBy('ProgName', 'ASC')->get();

        foreach($get_prog_class as $pc){
            $return_arr[] = array(
                'ProgClassName' => $pc->ProgName,
                'ProgClassID' => $pc->ProgID,
                'Programs' => $programs[$pc->ProgID]
            );
        }

        return $return_arr;
    }

    public function academic_year($limit = 0)
    {
        $get = DB::table('es_ayterm')->select('TermID', 'AcademicYear', 'SchoolTerm')->where(['Hidden' => 0])->orderBy('AcademicYear', 'DESC')->limit($limit)->get();
        return $get;
    }

    public function campus()
    {
        $get = DB::table('es_campus')->select('CampusID', 'Acronym', 'ShortName')->get();

        return $get;
    }
}
