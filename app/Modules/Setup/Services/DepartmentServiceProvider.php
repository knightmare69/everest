<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\Department\Validation;

Class DepartmentServiceProvider {

	public function __construct()
	{
		$this->validation = new Validation;
	}

	public function isValid($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}

		return ['error'=>false,'message'=> ''];
	}

	public function post($post)
	{
		$dept_head = !empty($post['dept-head-name']) ? $post['dept-head'] : null;
		$dept_vice = !empty($post['dept-vice-name']) ? $post['dept-vice'] : null;

		return [
				'DeptCode' => getObjectValue($post,'code'),
				'DeptName' => getObjectValue($post,'name'),
				'CollegeID' => getObjectValueWithReturn($post,'college',0),
				'CampusID' => decode(getObjectValueWithReturn($post,'campus',1)),
				'ShortName' => getObjectValue($post,'shortName'),
				'DeptHead_EmployeeID' => $dept_head,
				'DeptVice_EmployeeID' => $dept_vice,
			];
	}
}	
?>