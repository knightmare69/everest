<?php

namespace App\Modules\Setup\Services;

use DB;
use Validator;

class SetupManagerSP
{
    public $terms = ['School Year', '1st Semester', '2nd Semester', 'Summer'];

    public function _validate($post, $form)
    {

        switch ($form) {
            case 'bldg':
                $rules = [
                    'campus' => 'required|integer',
                    'name' => 'required|string',
                    'othername' => 'string',
                    'acronym' => 'string',
                    'floor' => 'required|integer',
                ];
                break;

            case 'rooms':
                $rules = [
                    'building' => 'required|integer',
                    'room_no' => 'required|string',
                    'name' => 'required|string',
                    'type' => 'integer',
                    'floor' => 'required|integer',
                    'capacity' => 'required|integer',
                ];
                break;

            case 'ayterm':
                $rules = [
                    'period_from' => 'required|date',
                    'period_to' => 'required|date',
                    'school_term' => 'required|in:'.implode(',', $this->terms),
                    'academic_year' => 'required|regex:/^[\d\-]+$/',
                    'weeks' => 'required|integer',
                ];
                break;

            default:
                return ['messages' => 'Unable to validate', 'keys' => []];
                break;
        }


        $custom_attribs = [];

        $v = Validator::make($post, $rules)->addCustomAttributes($custom_attribs);
        return ['messages' => $v->errors()->all(), 'keys' => $v->errors()->keys()];
    }

    public function dbfields($post, $form)
    {
        $add = [];

        switch ($form) {
            case 'bldg':
                $fields = ['CampusID', 'BldgName', 'BldgOtherName', 'Acronym', 'FloorsCount'];

                $add = [
                    'BldgPictures' => null,
                    'IsLANReady' => !empty($post['has-lan']) ? 1 : 0,
                    'Elevator' => !empty($post['has-elevator']) ? 1 : 0,
                    'Escalator' => !empty($post['has-escalator']) ? 1 : 0,
                ];

                unset($post['has-lan'], $post['has-elevator'], $post['has-escalator']);
                break;

            case 'rooms':
                $fields = ['BldgID', 'RoomNo', 'RoomName', 'RoomTypeID', 'Floor', 'Capacity'];

                $add = [
                    'IsAirConditioned' => !empty($post['has-aircon']) ? 1 : 0,
                    'IsUsable' => !empty($post['usable']) ? 1 : 0,
                    'IsLANMember' => !empty($post['has-lan']) ? 1 : 0,
                    'AllowNightClass' => !empty($post['has-night-class']) ? 1 : 0,
                    'Shared' => !empty($post['shared']) ? 1 : 0,
                ];

                unset($post['has-aircon'], $post['usable'], $post['has-lan'], $post['has-night-class'], $post['shared'], $post['campus']);
                break;

            case 'ayterm':
                $post['period_from'] = date('Y-m-d', strtotime($post['period_from']));
                $post['period_to'] = date('Y-m-d', strtotime($post['period_to']));

                $fields = ['StartofAY', 'EndofAY', 'AcademicYear', 'SchoolTerm', 'NumWeeks'];

                $add = [
                    'Lock' => !empty($post['lock']) ? 1 : 0,
                    'IsCurrentTerm' => !empty($post['current_term']) ? 1 : 0,
                    'Active_OnlineEnrolment' => !empty($post['active_enroll']) ? 1 : 0,
                    'LastModified' => getUserID(),
                    'LastModifiedDate' => systemDate(),
                ];

                unset($post['lock'], $post['current_term'], $post['active_enroll']);
                break;

            default:
                return false;
                break;
        }

        $r = array_merge(array_combine($fields, $post), $add);

        return $r;
    }

    public function tbleTD($form, $data)
    {
        $r = [];

        switch ($form) {
            case 'bldg':
                $r = [
                    '<a href="#" class="setup-tbl-dt" data-for="bldg" data-id="'.encode($data['id']).'">'.$data['name'].'</a>',
                    $data['other_name'],
                    $data['floor'],
                    $data['campus_name'],
                ];
                break;

            case 'rooms':
                $usbl_txt = !empty($data['usable']) ? 'green' : 'grey';
                $ac_txt = !empty($data['has-aircon']) ? 'green' : 'grey';

                $r = [
                    $data['campus_name'],
                    $data['building'],
                    $data['room_no'],
                    '<a href="#" class="setup-tbl-dt" data-for="rooms" data-id="'.encode($data['id']).'">'.$data['name'].'</a>',
                    $data['type'],
                    $data['capacity'],
                    $data['floor'],
                    '<i class="fa fa-check-circle font-'.$usbl_txt.'"></i>',
                    '<i class="fa fa-check-circle font-'.$ac_txt.'"></i>',
                ];
                break;

            case 'ayterm':
                $lock_txt = !empty($data['lock']) ? 'green' : 'grey';
                $ct_txt = !empty($data['current_term']) ? 'green' : 'grey';

                $r = [
                    '<a href="#" class="setup-tbl-dt" data-for="ayterm" data-id="'.encode($data['id']).'">'.$data['academic_year'].'</a>',
                    $data['school_term'],
                    date('M. d, Y', strtotime($data['period_from'])).' - '.date('M. d, Y', strtotime($data['period_to'])),
                    '<i class="fa fa-check-circle font-'.$lock_txt.'"></i>',
                    $data['weeks'],
                    '<i class="fa fa-check-circle font-'.$ct_txt.'"></i>',
                ];
                break;

            default:
                return [];
                break;
        }

        return $r;
    }
}
