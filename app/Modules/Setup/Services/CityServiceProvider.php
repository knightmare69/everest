<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\City\Validation;

Class CityServiceProvider {

	public function __construct()
	{
		$this->validation = new Validation;
	}

	public function isValid($post,$action = 'save')
	{	
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		
		return ['error'=>false,'message'=> ''];
	}

	public function postSave($post)
	{
		return [
				'CountryID' => explode(':',getObjectValue($post,'country'))[0],
				'CountryCode' => explode(':',getObjectValue($post,'country'))[1],
				'City' => getObjectValue($post,'name')
			];			
	}
	
	public function postUpdate($post)
	{
		return [
				'CountryID' => explode(':',getObjectValue($post,'country'))[0],
				'CountryCode' => explode(':',getObjectValue($post,'country'))[1],
				'City' => getObjectValue($post,'name')
			];			
	}
}	
?>