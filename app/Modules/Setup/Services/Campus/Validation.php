<?php namespace App\Modules\Setup\Services\Campus;

use Validator;

Class Validation {

	public function validate($post)
	{
		$validator = validator::make(
	        [
	            'Acronym'       => getObjectValue($post,'acronym'),
	            'ShortName'     => getObjectValue($post,'campusname'),
	            'CampusName'    => getObjectValue($post,'shortname'),
	            'Barangay'      => getObjectValue($post,'barangay'),
	            'TownCity'      => getObjectValue($post,'towncity'),
	            // 'Province'      => getObjectValue($post,'province'),
	            // 'RegionID'      => getObjectValue($post,'region'),
	            // 'District'      => getObjectValue($post,'district'),
	            'ZipCode'       => getObjectValue($post,'zipcode'),
	            'MailingAddress'=> getObjectValue($post,'mailingaddress'),
	            // 'Email'         => getObjectValue($post,'email'),
	            'Website'       => getObjectValue($post,'website'),
	            'TelNo'         => getObjectValue($post,'tel'),
	            'FaxNo'         => getObjectValue($post,'fax')
	        ],
	        [
	            'Acronym'       => 'required',
	            'ShortName'     => 'required',
	            'CampusName'    => 'required',
	            'Barangay'      => 'required',
	            'TownCity'      => 'required',
	            // 'Province'      => 'required',
	            // 'RegionID'      => 'required',
	            // 'District'      => 'required',
	            'ZipCode'       => 'required',
	            'MailingAddress'=> 'required',
	            // 'Email'         => 'required',
	            'Website'       => 'required',
	            'TelNo'         => 'required',
	            'FaxNo'         => 'required'
	        ]
        );

		return $validator;
	}
}	
?>