<?php
namespace App\Modules\Setup\Services;
use DB;
Class RegConfigServiceProvider {

	public function __construct()
	{
	}
	
	public function sp_configMgmt($opt='0'){
	  $query = "SELECT  IndexID,Section,[Key],Caption,Value,DataType,Remarks,SeqNo,ListOption,GroupName,Global,IsHidden FROM ES_Configuration ORDER BY SeqNo";
	  $result = DB::select($query);
	  return $result;
	}
	 
	public function sp_xconfigMgmt($indx=0,$value=''){
	   $query = "UPDATE ES_Configuration SET Value='".$value."' WHERE IndexID='".intval($indx)."'";
	   $result = DB::statement($query);
	   return $result;
	}
	 
	public function loadlist($opt=0){
	  $data='';
	  $tbl='<xdata>';
	  $result=false;
	  
	  if($opt==1)
	  {
	   $data='<tr><td colspan="2"><i class="fa fa-warning"></i> No Data available.</td></tr>';
	   $tbl='<table class="table table-bordered">
			 <thead><tr><th>Config</th><th>Value</th></tr></thead>
			 <tbody><xdata></tbody>
			 </table>';
	   $result=$this->sp_configMgmt();		
	  }
	  elseif($opt==2)
	  {
	   $data='';
	   $tbl='<select class="form-control"><option value="" selected disabled> -Select One- </option><xdata></select>';
	   $result=$this->sp_configMgmt('0.1');
	  }
	  elseif($opt==3)
	  {
	   $data='<tr><td colspan="2"><i class="fa fa-warning"></i> No Data available.</td></tr>';
	   $tbl='<table class="table table-bordered">
			 <thead><tr><th>Config</th><th>Value</th></tr></thead>
			 <tbody><xdata></tbody>
			 </table>';
	   $result=$this->sp_configMgmt('0.3');
	  }
	  
	  if($result)
	  {
	   $tmpdata='';
	   foreach($result as $rs)
	   {
		$seqno= property_exists($rs,'SeqNo')? $rs->SeqNo :0;
		$indx= property_exists($rs,'IndexID')? $rs->IndexID :0;
		$key= property_exists($rs,'Key')? $rs->Key :'';
		$section=property_exists($rs,'Section')? $rs->Section :'';
		$type=property_exists($rs,'DataType')? $rs->DataType :'';
		$listopt=property_exists($rs,'ListOption')? $rs->ListOption :'';
		$caption=property_exists($rs,'Caption')? $rs->Caption :'';
		$group=property_exists($rs,'GroupName')? $rs->GroupName :'';
		$value=property_exists($rs,'Value')? $rs->Value :'';
		$remarks=property_exists($rs,'Remarks')? $rs->Remarks :'';
		//$global=property_exists($rs,'Global')? $rs->Global :0;
		$hidden=property_exists($rs,'IsHidden')? $rs->IsHidden :0;
		
		if($opt==1 || $opt==3)
		{
		 if($caption!='' && $hidden==0 && $key!='' && $seqno!=0)
		 {
		  if($type=='header')
		   $tmpdata.='<tr style="background-color:#95bdc5;"><td colspan="2"><b>'.$caption.'</b></td></tr>';
		  else
		   $tmpdata.='<tr><td>'.utf8_encode($caption).'</td><td>'.$this->getListOption($listopt,(($opt==1)?$indx:$key),$value).'</td></tr>';
		 }
		}
		else
		{
		 $tmpdata.='<option value="'.$caption.'">'.$caption.'</option>';
		}
	   }
	   
	   if($tmpdata!='')
	   {$data=$tmpdata;}
	  }
	  
	  return str_replace('<xdata>',$data,$tbl);
	 }
	 
	 
	public function getListOption($opt=0,$key='',$value=0){
	  $input='';
	  if($opt==4)
	  {
	   if($key=='UseLocalConfig')
	   {
	   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this)">
				  <option value="0" '. ($value==1?'':'selected') .'>Yes</option>
				  <option value="1" '. ($value==1?'selected':'') .'>No</option>
				</select>';
	   }
	   else
	   {
	   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this)">
				  <option value="0" '. ($value==1?'':'selected') .'>No</option>
				  <option value="1" '. ($value==1?'selected':'') .'>Yes</option>
				</select>';
	   }   
	  }
	  elseif($opt==0)
	  {
	   $campus = DB::SELECT('SELECT * FROM ES_Campus');
	   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this)"><xdata></select>';
	   $data='';
	   foreach($campus as $rs)
	   {$data.='<option value="'.$rs->CampusID.'" '. ($value==$rs->CampusID?'selected':'') .'>'.$rs->CampusName.'</option>';}   
	   $input = str_replace('<xdata>',$data,$input);
	  }   
	  elseif($opt==1 or $opt==2 or $opt==3)
	  {
	   $ayterm  = DB::SELECT("SELECT * FROM ES_AYTerm");
	   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);"><xdata></select>';
	   $data='';
	   foreach($ayterm as $rs)
	   {$data.='<option value="'.$rs->TermID.'" '. ($value==$rs->TermID?'selected':'') .'>'.$rs->AcademicYear.''.$rs->SchoolTerm.'</option>';}   
	   $input = str_replace('<xdata>',$data,$input);
	  }   
	  elseif($opt==6)
	  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
				  <option value="0" '. ($value==0?'selected':'') .'>Manual</option>
				  <option value="1" '. ($value==1?'selected':'') .'>Compute base on StudentNo</option>
				  <option value="2" '. ($value==2?'selected':'') .'>Compute by Scholastic Deliquency</option>
				  <option value="3" '. ($value==3?'selected':'') .'>Plus 1 Every 1st Semester</option>
				</select>';}
	  elseif($opt==12)
	  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
				  <option value="0" '. ($value==0?'selected':'') .'>No Charge</option>
				  <option value="1" '. ($value==1?'selected':'') .'>Charge Once</option>
				  <option value="2" '. ($value==2?'selected':'') .'>Charge per Section</option>
				  <option value="3" '. ($value==3?'selected':'') .'>Charge per Transaction</option>
				</select>';}
	  elseif($opt==13)
	  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
				  <option value="1" '. ($value==1?'selected':'') .'>51x51mm</option>
				  <option value="0" '. ($value==1?'':'selected') .'>45x35 mm</option>
				</select>';}
	  elseif($opt==14)
	  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
				  <option value="0" '. ($value==0?'selected':'') .'>Year/Term, SeqNo</option>
				  <option value="1" '. ($value==1?'selected':'') .'>Year/Term, Code, Title</option>
				  <option value="2" '. ($value==2?'selected':'') .'>Year/Term, Title, Code</option>
				</select>';}
	  elseif($opt==9)
	  {$input ='<input data-pointer="'.$key.'" type="text" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" value="'.$value.'" onchange="modifyvalue(this);"/>';}
	  else
	  {$input ='<input data-pointer="'.$key.'" type="text" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" value="'.$value.'" onchange="modifyvalue(this);"/>';}
	 
	  return $input;
	}
}	
?>