<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\Programs\Validation;

Class ProgramServiceProvider {

	public function __construct()
	{
		$this->validation = new Validation;
	}

	public function isValid($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails()){
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}

		return ['error'=>false,'message'=> ''];
	}

	public function post($post){
		return [
				'CampusID'            => decode(getObjectValueWithReturn($post,'campus',0)),
				'CollegeID'           => decode(getObjectValueWithReturn($post,'college',0)),
				'ProgCode'            => getObjectValue($post,'code'),
				'ProgShortName'       => getObjectValue($post,'short'),
				'ProgName'            => getObjectValue($post,'name'),
				'ProgAlias'           => getObjectValue($post,'aliasa'),
				'ProgAlias2'          => getObjectValue($post,'aliasb'),
				'ProgClass'           => decode(getObjectValue($post,'progclass')),
				'ProgSems'            => decode(getObjectValue($post,'progsems')),
				'Semestral'           => decode(getObjectValue($post,'progsems')),
				'ProgYears'           => intval(getObjectValue($post,'noyrs')),
				'MaxResidency'        => intval(getObjectValue($post,'maxres')),
				'TotalAcadSubject'    => intval(getObjectValue($post,'acadsubj')),
				'TotalAcadCreditUnits'=> intval(getObjectValue($post,'acadunit')),
				'TotalNormalLoad'     => intval(getObjectValue($post,'normal')),
				'TotalGenEdUnits'     => intval(getObjectValue($post,'geunit')),
				'TotalMajorUnits'     => intval(getObjectValue($post,'majorunit')),
				'TotalElectiveUnits'  => intval(getObjectValue($post,'electunit')),
			];
	}
	
	public function post_class($post){
		return [
				'ClassDesc' => getObjectValue($post,'desc'),
				'ShortName' => getObjectValue($post,'short'),
				'GroupClass'=> 0,
			];
    }			
}	        
?>