<?php
namespace App\Modules\Setup\Services\Programs;
use Validator;
Class Validation {

	public function validateSave($post)
	{
		$validator = validator::make(
			[
				'ProgCode' => getObjectValue($post,'code'),
				'ProgName' => getObjectValue($post,'name')
			],
		    []
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			[
				'ProgCode' => getObjectValue($post,'code'),
				'ProgName' => getObjectValue($post,'name')
			],
			[]
		);
		return $validator;
	}
}	
?>