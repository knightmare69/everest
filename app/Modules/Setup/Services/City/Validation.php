<?php
namespace App\Modules\Setup\Services\City;
use Validator;
Class Validation {
	
	public function validateSave($post)
	{
		$validator = validator::make(
			$post,			
			[
				'name'		=> 'required',
				'country'		=> 'required',
			]
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			$post,			
			[
				'name'		=> 'required',
				'country'		=> 'required',
			]
		);
		return $validator;
	}
}	
?>