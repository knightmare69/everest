<?php
namespace App\Modules\Setup\Services;

use App\Modules\Setup\Services\Group\Validation;

Class GroupServiceProvider {

	public function __construct()
	{
		$this->validation = new Validation;
	}

	public function isValid($post,$action = 'save')
	{	
		if ($action == 'save') {
			$validate = $this->validation->validateSave($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		} else {
			$validate = $this->validation->validateUpdate($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		
		return ['error'=>false,'message'=> ''];
	}

	public function postSave($post)
	{
		return [
				'GroupName' => getObjectValue($post,'name'),
				'GroupDesc' => getObjectValue($post,'description'),
				'CreatedBy'	=> getUserID(),
				'CreatedDate' => systemDate(),

			];			
	}
	
	public function postUpdate($post)
	{
		return [
				'GroupName' => getObjectValue($post,'name'),
				'GroupDesc' => getObjectValue($post,'description'),
				'CreatedBy'	=> getUserID(),
				'CreatedDate' => systemDate(),

			];			
	}
}	
?>