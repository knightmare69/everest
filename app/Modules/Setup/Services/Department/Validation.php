<?php
namespace App\Modules\Setup\Services\Department;
use Validator;
Class Validation {

	public function validateSave($post)
	{
		$validator = validator::make(
			[
				'DeptCode' => getObjectValue($post,'code'),
				'DeptName' => getObjectValue($post,'name')
			],
			[
				'DeptName' => 'required',
				'DeptCode' => 'required|unique:es_departments'
			]
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			[
				'DeptCode' => getObjectValue($post,'code'),
				'DeptName' => getObjectValue($post,'name')
			],
			[
				'DeptName' => 'required',
				'DeptCode'=>'required'
			]
		);
		return $validator;
	}
}	
?>