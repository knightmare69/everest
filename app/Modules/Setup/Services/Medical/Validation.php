<?php
namespace App\Modules\Setup\Services\Medical;
use Validator;
Class Validation {

	public function validatePost($post)
	{
		// error_print(getObjectValue($post, 'is-days'));
		// die();

		$test = array(
			'ac-year-id' => decode(getObjectValue($post,'ac-year')),
			'campus' => decode(getObjectValue($post,'campus')),
			'batch-code' => getObjectValue($post,'bc'),
			'app-type' => decode(getObjectValue($post,'yl')),
			'prog-class' => decode(getObjectValue($post, 'prog')),
			'limit' => getObjectValue($post,'limit'),
			'time-start' => getObjectValue($post,'time-start'),
			'time-end' => getObjectValue($post,'time-end')
		);

		$rules = array(
			'ac-year-id' => 'required|integer',
			'batch-code' => 'required|string',
			'campus' => 'required|integer',
			'app-type' => 'required|integer',
			'prog-class' => 'required|integer',
			'limit' => 'numeric',
			'time-start' => 'required|date_format:g:i A',
			'time-end' => 'required|date_format:g:i A'
		);

		$custom_message = array(
			'time-start.date_format' => 'Undefined format for Time Start',
			'time-end.date_format' => 'Undefined format for Time Start'
		);

		$is_date = getObjectValue($post, 'is-days');

		if($is_date == 0 || $is_date == '' || empty($is_date)){
			$test['test-days'] = getObjectValue($post,'test-days');
			$rules['test-days'] = 'required|array';
			$custom_message['test-days.required'] = 'There must be day(s) selected';
		} else {
			$test['test-date'] = getObjectValue($post,'test-date');
			$rules['test-date'] = 'required|date';
		}

		$validator = validator::make($test, $rules, $custom_message);

		return $validator;
	}

}
?>
