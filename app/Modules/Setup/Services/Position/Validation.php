<?php
namespace App\Modules\Setup\Services\Position;
use Validator;
Class Validation {
	
	public function validateSave($post)
	{
		$validator = validator::make(
			[
				'PositionCode' => getObjectValue($post,'code'),
				'name' => getObjectValue($post,'name')
			],			
			[
				'name' => 'required',
				'PositionCode'=>'required|unique:hr_positiontitles'
			]
		);
		return $validator;
	}

	public function validateUpdate($post)
	{
		$validator = validator::make(
			[
				'PositionCode' => getObjectValue($post,'code'),
				'name' => getObjectValue($post,'name')
			],			
			[
				'name' => 'required',
				'PositionCode'=>'required|unique:hr_positiontitles'
			]
		);
		return $validator;
	}
}	
?>