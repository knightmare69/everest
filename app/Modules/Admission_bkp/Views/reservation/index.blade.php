<?php $service = new \App\Modules\Students\Services\StudentProfileServiceProvider; ?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="icon-wrench"></i>
			<span class="caption-subject bold uppercase"> Admission Reservation </span>
			<span class="caption-helper">Use this module to monitor applicant reservation/s...</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline  input-medium">
	           <input type="text" class="form-control input-circle  filter-table input-sm " data-table="records-table" />
			</div>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">
            <div class="portlet-input input-inline input-medium">
                <select class="select2 form-control" name="academic-term" id="academic-term">
                    <option value="0" >- Select AY Term -</option>
                    @if(!empty($at))
                        @foreach($at as $r)
                        <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
                        @endforeach
                    @endif
                </select>
			</div>
            <button type="button" class="btn btn-sm btn-default " data-menu="add"><i class="fa fa-plus"></i> Add Reservation </button>
            <button type="button" class="btn btn-sm btn-default " data-menu="remove"><i class="fa fa-times"></i> Remove </button>
            <!-- <button type="button" class="btn btn-sm btn-default " data-menu="print-table" id="print-def-tbl"><i class="fa fa-print"></i> Print </button> -->
            <button type="button" class="btn btn-sm btn-default " data-menu="print"><i class="fa fa-print"></i> Print </button>
        </div>
        <div class="scroller" id="grdList">
            @include($views.'list')
        </div>
	</div>
</div>
@include($views.'modal')