<table class="table" id="TableConnectedStudent">
	<thead>
		<tr class="bg-grey-cascade">
			<td colspan="6">
				<p>
					List of students to connect with.
				</p>
			</td>
		</tr>
		<tr>
			<th>App. No.</th>
			<th>Name</th>
			<th>Year Level</th>
			<th>Gender</th>
			<th>Birth Date</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>