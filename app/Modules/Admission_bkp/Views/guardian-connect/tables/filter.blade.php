<div class="well">
    <div class="row">
       
        <form class="form-horizontal" role="form" id="studentsearch" name="studentsearch" autocomplete="off" method="post">
            <div class="form-group">
                <label for="term" class="col-sm-2 control-label">Student No</label>
				<div class="col-sm-3">
                    <input name="idno" id="idno" class="form-control input-md" type="text" maxlength="15" placeholder="ex. 11-01202" value="" required>
				</div>
                <button type="button" class="btn btn-primary" id="btnsearch" name="btnsearch"><i class="fa fa-search"></i> Search</button>
            </div>
            <input type='hidden' name='hidno' id='hidno' value=''/>
            <div class="form-group">
				<label for="term" class="col-sm-2 control-label">Student Name</label>
				<div class="col-sm-8">
                    <label for="studentname" name="studentname" id="studentname" class="col-sm-8 bold control-label" style="text-align: left;"><strong>[Student Name]</strong></label>
                </div>
			</div>
            <div class="form-group">
                <label for="yearlevel" class="col-sm-2 control-label">Year Level</label>
                <div class="col-sm-8">
                    <label for="yearlevel" id="yrlvl" class="col-sm-8 control-label bold " style="text-align: left;">[Year Level]</label>
                </div>
            </div>
            <div class="form-group">
				<label for="term" class="col-sm-2 control-label">Date of Birth</label>
				<div class="col-sm-3">
                    <input id="dateofbirth" name="dateofbirth" class="form-control input-md datepicker" data-dateformat='mm/dd/yy' type="text" autoccomplete="off" placeholder="ex. 08/23/1982" value="" data-mask="99/99/9999" data-mask-placeholder= "-" required>
				</div>
			</div>
          	<div class="form-group">
		        <label for="term" class="col-sm-2 control-label">Relationship</label>
			     <div class="col-sm-3">
                    <select class='form-control input-sm' id='relationship' name='relationship' required>
						<option value="Family">Family</option>
                        <option value="Guardian">Guardian</option>
                        <option value="Parent">Parent</option>
					</select>
				</div>
			</div>
            <div class="form-group">
                <div class="col-sm-offset-3">
                    <button type="button" class="btn btn-success" id="btnrequest" ><i class="fa fa-send"></i> Send Request</button>
                    <button type="button" class="btn btn-default" id="btncancel" ><i class="fa fa-times"></i> Cancel</button>
                </div>
            </div>
            <span class="note">
                Note: Request to subcribes into student account is subject for the approval of the management. Thank you.
            </span>
        
        </form>
    </div>
</div>