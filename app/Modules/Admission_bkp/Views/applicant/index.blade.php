<?php
  $opt_gs   = '';
  $opt_jhs  = '';
  $opt_shs  = '';
  $opt_col  = '';
  $opt_grd  = '';
  $db_yrlvl = DB::select("SELECT * FROM ESv2_YearLevel");
  if($db_yrlvl && count($db_yrlvl)>0){
    foreach($db_yrlvl as $l){
	  switch($l->ProgClass){
	    case 5: 
	    case 11:
			$opt_gs .= '<option value="'.$l->YearLevelID.'" data-class="'.$l->ProgClass.'">'.$l->YearLevelName.'</option>';
		break;
		case 50: //for development only
			$opt_col .= '<option value="'.$l->YearLevelID.'" data-class="'.$l->ProgClass.'">'.$l->YearLevelName.'</option>';
		break;
		case 60: //for development only
		case 70: //for development only
			$opt_grd .= '<option value="'.$l->YearLevelID.'" data-class="'.$l->ProgClass.'">'.$l->YearLevelName.'</option>';
		break;
		default:
		    if($l->YearLevelCode=='G11' || $l->YearLevelCode=='G12'){
			$opt_shs .= '<option value="'.$l->YearLevelID.'" data-class="'.$l->ProgClass.'">'.$l->YearLevelName.'</option>';
			}else{
			$opt_jhs .= '<option value="'.$l->YearLevelID.'" data-class="'.$l->ProgClass.'">'.$l->YearLevelName.'</option>';
			}
		break;
	  }
	}
  }
?>
<script type="text/javascript">
	var opt_gs  = '<?php echo $opt_gs;?>';
	var opt_jhs = '<?php echo $opt_jhs;?>';
	var opt_shs = '<?php echo $opt_shs;?>';
	var opt_col = '<?php echo $opt_col;?>';
	var opt_grd = '<?php echo $opt_grd;?>';
</script>
<div class="row">
   <div class="col-sm-12">
	<h1>Application Form</h1>
   </div>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="portlet box purple">
			<div class="portlet-title">
				<div class="caption">Admision Wizard</div>
				<div class="actions">
					<button class="btn btn-sm btn-primary btn-prev disabled"><i class="fa fa-arrow-left"></i> Prev</button>
					<button class="btn btn-sm btn-success btn-next">Next <i class="fa fa-arrow-right"></i></button>
				</div>
			</div>
			<div class="portlet-body">
			    <div class="row">
					<div class="col-sm-12 div1">
						<h3><strong class="hidden">Step 1 - </strong> Applying for</h3>
						<input class="hidden" type="hidden" id="UserName" name="UserName" value="<?php echo ((isset($userinfo) && is_array($userinfo))? $userinfo['id'] : '' );?>"/>
						    <section class="col col-sm-6">
								<label>Select Department :</label>
								<select name="department" id="department" class="form-control">
									<option value="gs">Grade School</option>
									<option value="jhs">High School</option>
									<option value="shs">Senior High School</option>
									<option value="college">Undergraduate/College/Professional</option>
									<option value="graduate">Graduate</option>
								</select>
							</section>
							<section class="col col-sm-3 hidden">
							    <?php
									$rsayterm = array();
									foreach ($rsayterm as $rsayterm){
									 echo "<option value='". $rsayterm->TermID ."' selected>". $rsayterm->AcademicYearTerm."</option>";
									}
								?>
								<input type="hidden" name="Choice1_campusID" id="Choice1_campusID" class="form-control" value="1"/>
								<input type="hidden" name="TermID" id="TermID" class="form-control" value="1"/>
							</section>
							<section class="col col-sm-4 dvapptype">
								<label id="AppType_Label">Application Type :</label>
								<select name="ApplyTypeID" id="ApplyTypeID" class="form-control">
									<option value="-1" selected disabled>Select an Application Type</option>
									<?php
									 $i=0;
									 $atype = DB::select("SELECT * FROM ESv2_ApplicationTypes WHERE AppTypeID<5");
									 if($atype && count($atype)>0){
									    foreach($atype as $r){
											echo "<option value='". $r->AppTypeID ."' ".(($i==0)?"selected":"").">". $r->ApplicationType."</option>";
											$i++;
										}
									 }
									?>
								</select>
							</section>
							<section class="col col-sm-4 dvlevel hidden">
								<label id="GradeLevelID_Label">Grade Level :</label>
								<select name="GradeLevelID" id="GradeLevelID" class="form-control">
									<option value="-1" selected disabled>Select a level</option>
									<?php echo $opt_gs;?>
								</select>
							</section>
							<section class="col col-sm-6 firstchoice">
								<label id="Choice1_Label">First Choice :</label>
								<select name="Choice1_Course" id="Choice1_Course" class="form-control"></select>
								<input type="hidden" class="hidden" id="Choice1_CourseMajor" name="Choice1_CourseMajor" value="0"/>
							</section>

							<section class="col col-sm-6 secondchoice">
								<label>Second Choice :</label>
								<select name="Choice2_Course" id="Choice2_Course" class="form-control"></select>
							</section>

							<section class="col col-sm-6 hidden">
								<label>Third Choice :</label>
								<select name="Choice3_Course" id="Choice3_Course" class="form-control"></select>
							</section>	
				    </div>
					<div class="col-sm-12 div2 hidden">
						<h3><strong class="hidden">Step 2 -</strong> Basic Information</h3>
						<div class="col-sm-12">
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="LastName" id="LastName"  placeholder="Last name" value="<?php if(isset($userinfo['lastname'])) echo $userinfo['lastname']; ?>">
							</section>
							<section class="col col-sm-4">
								<input type="text" class="form-control" name="FirstName" id="FirstName" placeholder="First name" value="<?php if(isset($userinfo['firstname'])) echo $userinfo['firstname']; ?>">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="MiddleName" id="MiddleName" placeholder="Middle name" value="<?php if(isset($userinfo['middlename'])) echo $userinfo['middlename']; ?>">
							</section>
							<section class="col col-sm-2">
								<select class="form-control" id="drp_ExtName">
								  <option value="" selected disabled>Extension name: (e.g Jr, Sr, III, IV, etc.)</option>
								  <option value="Jr">Jr</option>
								  <option value="Sr">Sr</option>
								  <option value="II">II</option>
								  <option value="III">III</option>
								  <option value="IV">IV</option>
								  <option value="V">V</option>
								  <option value="-1">Other</option>
								</select>  <!-- value="<?php if(isset($userinfo['extname'])) echo $userinfo['extname']; ?>"> -->
								<input type="hidden" name="ExtName" id="ExtName" placeholder="Extension name: (e.g Jr, Sr, III, IV, etc.)">
							</section>
						</div>
						
						<div class="col-sm-12" style="margin-top:10px !important;">
							<section class="col col-sm-2">
								<label class="control-label">Gender:</label>
								<select name="Gender" class="form-control">
									<?php
									$gender ='';
									if(isset($userinfo['gender']))
									{$gender = trim($userinfo['gender']) ;}

									 echo  '<option value="0" ' . ($gender == ''? 'selected':'') . ' disabled>Gender</option>
											<option value="M" ' . ($gender == 'M'? 'selected':'') . ' >Male</option>
											<option value="F" ' . ($gender == 'F'? 'selected':'') . ' >Female</option>' ;
									?>
								</select>
							</section>
							<section class="col col-sm-2">
								<label class="control-label">Place of Birth:</label>
								<input type="text" class="form-control" name="PlaceOfBirth" id="PlaceOfBirth" placeholder="Birth place" value="<?php if(isset($userinfo['birthplace'])) echo $userinfo['birthplace']; ?>">								
							</section>
							<section class="col col-sm-3">
								<label class="control-label">Date of Birth:</label>
								<input type="text" class="form-control" id="DateOfBirth" name="DateOfBirth" placeholder="Date of Birth (mm/dd/yyyy)" class="form-control"  data-format="MM/DD/YYYY" data-template="MMM/DD/YYYY" value=""/>
							</section>
							<section class="col col-sm-3 for_shs hidden">
								<input type="text" class="form-control" name="LRN" id="LRN" placeholder="Learner Reference Number (LRN)" >
							</section>

							<section class="col col-sm-2 for_shs hidden">
								<label>Indigenous Group?</label>
								<div class="inline-group">
									<label class="radio">
										<input type="radio" name="isIndigenous"  checked="" value="0">
										<i></i>No</label>
									<label class="radio">
										<input type="radio" name="isIndigenous"  value="1">
										<i></i>Yes</label>
								</div>
							</section>
							<section class="col col-sm-2">
								<input type="hidden" name="Indigenous_Group" id="Indigenous_Group" placeholder="Indigenous Group"/>
							</section>

						</div>
						
						<div class="col-sm-12" style="margin-top:10px !important;">
							<section class="col col-sm-2">
								<select name="CivilStatusID" class="form-control">
									<option value="0" select disabled>- Civil Status -</option>
									<?php
										$res = '';
										$rscs = ((isset($rscs))?$rscs:array());
										if(isset($userinfo['cstatus']))
										{$res = trim($userinfo['cstatus']);}

										foreach ($rscs as $rscs){
										 if($rscs->CivilDesc=='Deceased'){continue;}
										 echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
										}

									?>
								</select>
							</section>
							<!-- Religion -->
							<section class="col col-sm-2">
								<select name="ReligionID" id="ReligionID" class="form-control">
									<option value="0" selected="" disabled="">Religion</option>
									<?php
										$selrel = '';
										$rsreligion = ((isset($rsreligion))?$rsreligion:array());
										if(isset($userinfo['relid']))
										{$selrel = trim($userinfo['relid']);}

										foreach ($rsreligion as $rsreligion){
										 echo "<option value='". $rsreligion->ReligionID ."'".  ($selrel == $rsreligion->ReligionID ? 'selected':'') .">". $rsreligion->ShortName."</option>";
										}
									?>
									<option value="-1">Other</option>
								</select>
								<input type="hidden" id="ReligionOther" placeholder="Religion"/>
							</section>
							<!-- Nationality -->
							<section class="col col-sm-2">
								<select name="NationalityID" id="NationalityID" class="form-control">
									<option value="0" selected="" disabled="">Nationality</option>
									<?php
									    $rsnat = ((isset($rsnat))?$rsnat:array());
										foreach ($rsnat as $rs){
										   echo "<option value='". $rs->NationalityID ."'>". $rs->Nationality."</option>";
										}
									?>
								</select>
							</section>
							
							<section class="col col-sm-2 for_shs hidden">
								<input type="text" class="form-control" name="Mother_Tongue" id="Mother_Tongue" placeholder="Mother Tongue" >
							</section>
							
							<section class="col col-sm-2 for_shs hidden">
								<input type="text" class="form-control" name="Other_Language" id="Other_Language" placeholder="Other Language Spoken" >
							</section>
						</div>
						
						<div class="col-sm-12" style="margin-top:10px !important;">
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Res_Address" id="Res_Address" placeholder="Residence/Current">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Res_Street" id="Res_Street" placeholder="Street">
							</section>
							<section class="col col-sm-2">
								<select name="Res_TownCity" id="Res_TownCity" class="form-control" placeholder="Town">
								<option value='' selected disabled> - Select Town - </option>";
								<?php
									  $prevVal = '';
									  $rscit   = ((isset($rscit))?$rscit:array());
									  foreach ($rscit as $rs){
										 if($prevVal==$rs->CityName){
										  continue;
										 }else{
										  $prevVal=$rs->CityName;
										 }
										 echo "<option value='". utf8_encode($rs->CityName)."' data-prov='".$rs->ProvinceID."'>". utf8_encode($rs->CityName)."</option>";
									  }
								?>
								</select>
							</section>
							<section class="col col-sm-2">
								<select name="Res_Province" id="Res_Province" class="form-control" placeholder="Province">
								<option value='' selected disabled> - Select Province - </option>";
								<?php
								      $rsprov = ((isset($rsprov))?$rsprov:array());
									  foreach ($rsprov as $rs){
										 echo "<option value='". utf8_encode($rs->ProvinceName)."'  data-prov='".$rs->ProvinceID."'>". utf8_encode($rs->ProvinceName)."</option>";
									  }
								?>
								</select>
							</section>
							<section class="col col-sm-2">
								<input type="text" class="form-control numberonly" name="Res_ZipCode" id="Res_Zipcode" placeholder="Postal Code">
							</section>
						</div>
						
						<div class="col-sm-12" style="margin-top:10px !important;">
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Perm_Address" id="Perm_Address" placeholder="Permanent">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Perm_Street" id="Perm_Street" placeholder="Street">
							</section>
							<section class="col col-sm-2">
								<select name="Perm_TownCity" id="Perm_TownCity" class="form-control" placeholder="Town">
								<option value='' selected disabled> - Select Town - </option>";
								<?php
									  $prevVal = '';
									  $rscit   = ((isset($rscit))?$rscit:array());
									  foreach ($rscit as $rs){
										 if($prevVal==$rs->CityName){
										  continue;
										 }else{
										  $prevVal=$rs->CityName;
										 }
										 echo "<option value='". utf8_encode($rs->CityName)."' data-prov='".$rs->ProvinceID."'>". utf8_encode($rs->CityName)."</option>";
									  }
								?>
								</select>
							</section>
							<section class="col col-sm-2">
								<select name="Perm_Province" id="Perm_Province" class="form-control" placeholder="Province">
								<option value='' selected disabled> - Select Province - </option>";
								<?php
								      $rsprov = ((isset($rsprov))?$rsprov:array());
									  foreach ($rsprov as $rs){
										 echo "<option value='". utf8_encode($rs->ProvinceName)."'  data-prov='".$rs->ProvinceID."'>". utf8_encode($rs->ProvinceName)."</option>";
									  }
								?>
								</select>
							</section>
							<section class="col col-sm-2">
								<input type="text" class="form-control numberonly" name="Perm_ZipCode" id="Perm_Zipcode" placeholder="Postal Code">
							</section>

							<section class="col col-sm-12">
								<label class="checkbox">
									<input value="1" type="checkbox"  id="same_as_res"><i></i>
									<small>Same as Residence Address?</small>
								</label>
							</section>
						</div>
						<div class="col-sm-12" style="margin-top:10px !important;">
							<section class="col col-sm-4">
								<input type="text" class="form-control numberonly" name="TelNo" id="Telno" maxlength="14" placeholder="Telephone number (e.g 7356011)">
							</section>
							<section class="col col-sm-4">
								<input type="text" class="form-control numberonly" name="MobileNo" id="MobileNo" maxlength="14" placeholder="Mobile number (e.g. 09234567890)">
							</section>
							<section class="col col-sm-4">
								<input type="text" class="form-control" name="Email" id="Email" placeholder="E-mail" value=""/>
							</section>
						</div>
						<div class="col-sm-12" style="margin-top:10px !important;">
							<section class="col col-sm-12">
								<label>	State any disability or ailment that should be taken into consideration in planning your study program and daily activities (e.g. hearing, reading speech difficulties, physical disabilities, allergies, psychological/emotional disturbances, etc.)</label>
								<input type="text" class="form-control" name="Disability" id="Disability"  placeholder="e.g. hearing, reading speech difficulties, physical disabilities, allergies, psychological/emotional disturbances, etc.">
							</section>
						</div>
						
					</div>
					
					<div class="col-sm-12 div3 hidden">
						<h3><strong class="hidden">Step 3 -</strong> Family Background </h3>
						<div class="col-sm-12">
							<header style="font-size:0.9em;"><strong>Father</strong></header>
							<br>

							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Father" id="Father"  placeholder="Father's Name" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Father_Occupation" id="Father_Occupation"  placeholder="Father's Occupation" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control numberonly" name="Father_TelNo" id="Father_TelNo"  maxlength="14" placeholder="Father's Contact (e.g. 09234567890)" value="">
							</section>

							<!-- Civil Status -->
							<section class="col col-sm-2">
								<select name="Father_CivilStatusID" class="form-control">
									<option value="0" disabled=""> - Civil Status -</option>
									<?php
										$rscs1 = array();
										foreach ($rscs1 as $rscs){
										 if ($rscs->CivilDesc != 'Single'){
										  echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
										 }
										}
									?>
								</select>
							</section>
							<section class="col col-sm-1">
							   <div class="inline-group">
								<label class="checkbox">
									<input value="1" type="checkbox" name="Father_isOFW" id="Father_isOFW"> Is OFW
									<i></i>
								</label>
							  </div>
							</section>
							<div class="">
								<section class="col col-sm-3">
									<select class="form-control" name="Father_Nationality" id="Father_Nationality"  placeholder="Father's Nationality">
										<option value="0" selected disabled=""> - Father's Nationality -</option>
										<?php
											foreach ($rsnat as $rsf){
											 echo "<option value='". $rsf->NationalityID ."'>". $rsf->Nationality."</option>";
											}
										?>
									</select>
								</section>
								<section class="col col-sm-3">
									<input type="text" class="form-control" name="Father_Address" id="Father_Address"  placeholder="Father's Address" value="">
									<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Father_Address"/><i></i><small>same as residence address of applicant</small></label>
									<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Father_Address"/><i></i><small>same as permanent address of applicant</small></label>
								</section>
								<section class="col col-sm-3">
									<input type="text" class="form-control" name="Father_Email" id="Father_Email"  placeholder="Father's E-mail" value="">
								</section>
							</div>

						</div> <!-- father row -->
						<div class="col-sm-12">
							<header style="font-size:0.9em;"><strong>Mother</strong></header>
							<br>

							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Mother" id="Mother"  placeholder="Mother's Name" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Mother_Occupation" id="Mother_Occupation"  placeholder="Mother's Occupation" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control numberonly" name="Mother_TelNo" id="Mother_TelNo"  maxlength="14" placeholder="Mother's Contact (e.g. 09234567890)" value="">
							</section>

							<!-- Civil Status -->
							<section class="col col-sm-2">
								<select name="Mother_CivilStatusID" class="form-control">
									<option value="0" disabled=""> - Civil Status -</option>
									<?php
										$rscs1 = array();
										foreach ($rscs1 as $rscs){
										 if ($rscs->CivilDesc != 'Single'){
										  echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
										 }
										}
									?>
								</select>
							</section>
							<section class="col col-sm-1">
							   <div class="inline-group">
								<label class="checkbox">
									<input value="1" type="checkbox" name="Mother_isOFW" id="Mother_isOFW"> Is OFW
									<i></i>
								</label>
							  </div>
							</section>
							<div class="">
								<section class="col col-sm-3">
									<select class="form-control" name="Mother_Nationality" id="Mother_Nationality"  placeholder="Mother's Nationality">
										<option value="0" selected disabled=""> - Mother's Nationality -</option>
										<?php
											foreach ($rsnat as $rsf){
											 echo "<option value='". $rsf->NationalityID ."'>". $rsf->Nationality."</option>";
											}
										?>
									</select>
								</section>
								<section class="col col-sm-3">
									<input type="text" class="form-control" name="Mother_Address" id="Mother_Address"  placeholder="Mother's Address" value="">
									<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Mother_Address"/><i></i><small>same as residence address of applicant</small></label>
									<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Mother_Address"/><i></i><small>same as permanent address of applicant</small></label>
								</section>
								<section class="col col-sm-3">
									<input type="text" class="form-control" name="Mother_Email" id="Mother_Email"  placeholder="Mother's E-mail" value="">
								</section>
							</div>

						</div> <!-- mother row -->
						<div class="col-sm-12">
							<header style="font-size:0.9em;"><strong>Guardian</strong></header>
							<br>

							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Guardian" id="Guardian"  placeholder="Guardian's Name" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Guardian_Address" id="Guardian_Address"  placeholder="Guardian's Address" value="">
								<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Guardian_Address"/><i></i><small>same as residence address of applicant</small></label>
								<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Guardian_Address"/><i></i><small>same as permanent address of applicant</small></label>
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control numberonly" name="Guardian_TelNo" id="Guardian_TelNo"  maxlength="14" placeholder="Guardian's Contact (e.g. 09234567890)" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Guardian_Relationship" id="Guardian_Relationship"  maxlength="200" placeholder="Guardian Relationship" value="">
							</section>
						</div> <!-- Guardian row -->
						<div class="col-sm-12">
							<header style="font-size:0.9em;"><strong>Spouse(For married applicants)</strong></header>
							<br>

							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Spouse" id="Spouse"  placeholder="Spouse's Name" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Spouse_Address" id="Spouse_Address"  placeholder="Spouse's Address" value="">
								<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Spouse_Address"/><i></i><small>same as residence address of applicant</small></label>
								<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Spouse_Address"/><i></i><small>same as permanent address of applicant</small></label>
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control numberonly" name="Spouse_TelNo" id="Spouse_TelNo"  maxlength="14" placeholder="Spouse's Contact (e.g. 09234567890)" value="">
							</section>
							<section class="col col-sm-3">
								<input type="text" class="form-control" name="Spouse_Email" id="Spouse_Email"  maxlength="200" placeholder="Spouse's Email" value="">
							</section>
						</div> <!-- spouse row -->
					</div> <!-- div3 -->
					
					<div class="col-sm-12 div4 hidden">
						<h3><strong class="hidden">Step 4 -</strong> Educational Background </h3>
						<div class="col-sm-12 elemschool" style="margin-bottom:5px !important;">
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="JHS_School" id="JHS_School"  placeholder="School Name" value="">
							</section>
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="JHS_Address" id="JHS_Address"  placeholder="School Address" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="JHS_Degree" id="JHS_Degree"  placeholder="Academic Track" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="JHS_Graduated" id="JHS_Graduated"  placeholder="Inclusive Year" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="JHS_Award" id="JHS_Award"  placeholder="Award/Honor" value="">
							</section>
						</div>
						<div class="col-sm-12 shsschool" style="margin-bottom:5px !important;">
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="SHS_School" id="SHS_School"  placeholder="School Name" value="">
							</section>
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="SHS_Address" id="SHS_Address"  placeholder="School Address" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="SHS_Degree" id="SHS_Degree"  placeholder="Academic Track" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="SHS_Graduated" id="SHS_Graduated"  placeholder="Inclusive Year" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="SHS_Award" id="SHS_Award"  placeholder="Award/Honor" value="">
							</section>
						</div>
						<div class="col-sm-12 college" style="margin-bottom:5px !important;">
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="College_School" id="College_School"  placeholder="College Name" value="">
							</section>
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="College_Address" id="College_Address"  placeholder="College Address" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="College_Degree" id="College_Degree"  placeholder="College Degree" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="College_InclDates" id="College_InclDates"  placeholder="Inclusive Year" value="">
							</section>
						    <section class="col col-sm-2">
								<input type="text" class="form-control" name="College_Award" id="College_Award"  placeholder="Award/Honor" value="">
							</section>
						</div>
					</div>	<!-- div4 -->
					
					<div class="col-sm-12 div5 hidden">
						<h3><strong class="hidden">Step 5 -</strong> Other Information </h3>
						<div class="col-sm-12" style="margin-bottom:10px !important;">
							<section class="col col-sm-6">
								<label>Licensure Exam/s Passed?:</label>
								<input type="text" name="lexam_passed" id="lexam_passed" class="form-control"/>
							</section>
							<section class="col col-sm-6">
								<label>What other schools your applied to?:</label>
								<input type="text" name="Other_school" id="Other_school" class="form-control"/>
							</section>
						</div>
						<div class="col-sm-12">
							<section class="col col-sm-12">
								<label>How did you come to know this school?:</label>
							</section>	
							<section class="col col-sm-3">
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbya" name="knowbya"/><i></i> from parents/siblings</label><br/>
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbye" name="knowbye"/><i></i> from internet/webpage</label><br/>
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbyh" name="knowbyh"/><i></i> other</label>
							</section>
							<section class="col col-sm-3">
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbyb" name="knowbyb"/><i></i> from friends/classmates</label><br/>
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbyf" name="knowbyf"/><i></i> from school brochures/posters</label><br/>
							</section>
							<section class="col col-sm-3">
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbyc" name="knowbyc"/><i></i> from own initiative</label><br/>
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbyg" name="knowbyg"/><i></i> from career orientation talks</label><br/>
							</section>
							<section class="col col-sm-3">
							    <label style="padding-left:20px !important;"><input type="checkbox" id="knowbyd" name="knowbyd"/><i></i> from school website</label><br/>
							</section>
						</div>
						<div class="col-sm-12 college" style="margin-bottom:5px !important;">
						    <h3>Working Experience </h3>
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="wnamea" id="wnamea"  placeholder="Company Name" value="">
							</section>
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="waddra" id="waddra"  placeholder="Company Address" value="">
							</section>
						    <section class="col col-sm-3">
								<input type="text" class="form-control" name="wpositiona" id="wpositiona"  placeholder="Position" value="">
							</section>
						    <section class="col col-sm-3">
								<input type="text" class="form-control numberonly" name="wcontacta" id="wcontacta"  placeholder="ContactNo/TelNo" value="">
							</section>
						</div>
					</div> <!-- div5-->	
					<div class="col-sm-12 div6 hidden">
						<div class="col-sm-12 dvsummary">
							<div class="alert alert-danger"><i class="fa fa-warning"></i> No data available.</div>
						</div>
					</div>
			    </div>		
		    </div>
	    </div>
	</div>
</div>