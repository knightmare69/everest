<div class="form-group">
	<label class="col-md-12 text-left">Academic Year & Term</label>
	<div class="col-md-12">
		<?php $i = 0; ?>
		<select class="form-control" name="schoolyear" id="schoolyear">
			<option value="">-- SELECT --</option>
			@foreach(App\Modules\Setup\Models\AcademicYearTerm::where('Hidden',0)->orderBy('AcademicYear','DESC')->get() as $row)
			<option  {{  $i++ <= 0 ? 'selected' : '' }} value="{{ $row->TermID }}">{{ $row->AcademicYear }} {{ $row->SchoolTerm }}</option>
			@endforeach
		</select>
	</div>
</div>


<div class="form-group">
	<label class="col-md-12 text-left">Title/Position</label>
	<div class="col-md-12">
		<input type="text" class="form-control" name="title" id="position-title">
	</div>
</div>

<div class="form-group">
	<label class="col-md-12 text-left">Panel/User</label>
	<div class="col-md-12">
		<input type="text" class="form-control" name="panel" id="panel-user">
	</div>
</div>

<div class="form-group hide">
	<label class="col-md-12 text-left">Level (By Index start at 1)</label>
	<div class="col-md-12">
		<input type="text" class="form-control not-required" name="level" id="level">
	</div>
</div>

<div class="form-group">
	<div class="col-md-12 text-right">
		<input type="button" class="btn btn-primary" onclick="Panels.save();" value="Save">
		<input type="button" class="btn btn-default" onclick="Panels.cancel();" value="Cancel">
	</div>
</div>

