<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissable">
			@if($isSubmit)
			<message>Hi, You have successfully submitted the application form. <br />You can now enroll the student.</message>
			@else
			<message>Hi, You have successfully save the application form.</message>
			@endif
		</div>
	</div>
</div>