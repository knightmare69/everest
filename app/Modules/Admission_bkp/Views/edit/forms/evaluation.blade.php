<?php $data = isset($dataEval) ? $dataEval : array(); ?>
<form action="#" class="form-horizontal" id="formEvaluation" data-id="{{ encode(getObjectValue($data,'IndexID')) }}">
	<div class="form-body">	
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label class="col-md-12 text-left">Interviewee</label>
					<div class="col-md-12">
						<select class="form-control" name="InterviewType">
							<option {{ getObjectValue($data,'Interviewee') == 'STUDENT' ? 'selected' : '' }} value='STUDENT'>Student</option>
							<option {{ getObjectValue($data,'Interviewee') == 'PARENT' ? 'selected' : '' }} value='PARENT'>Parent</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="col-md-12 text-left">Date Interviewed</label>
					<div class="col-md-12">
						<input type="text" class="form-control date-picker" data-date-format="mm/dd/yyyy" name="DateInterviewed" id="DateInterviewed" value="{{ setDateFormat(getObjectValue($data,'DateInterviewed'),'yyyy-mm-dd','mm/dd/yyyy') }}">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Results and Recommendation</label>
					<div class="col-md-12">
						<textarea class="form-control" name="ResultsandRecommendation" maxlength="300">{{ getObjectValue($data,'Results') }}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Notes</label>
					<div class="col-md-12">
						<textarea class="form-control" name="Notes" maxlength="300">{{ getObjectValue($data,'Notes') }}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Remarks and Recommendation</label>
					<div class="col-md-12">
						<textarea class="form-control" name="RemarksandRecommendation" maxlength="300">{{ getObjectValue($data,'Remarks') }}</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<legend></legend>