<?php $data = isset($dataComm) ? $dataComm : array(); ?>
<form action="#" class="form-horizontal" id="formCommittee" data-id="{{ encode(getObjectValue($data,'IndexID')) }}">
	<div class="form-body">	
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-12 text-left">Admission Committee Remarks</label>
					<div class="col-md-12">
						<textarea class="form-control" name="AdmissionCommitteeRemarks" maxlength="300">{{ getObjectValue($data,'CommitteeRemarks') }}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2" style="width: 17%;">
						<input type="checkbox" class="chk-admitted" {{ getObjectValue($data,'IsAdmitted') ? 'checked' : '' }} > Admitted
					</div>
					<div class="col-md-2" style="width: 19.5%;">
						<input type="checkbox" class="chk-condition" {{ getObjectValue($data,'IsConditional') ? 'checked' : '' }} > Conditional
					</div>
					<div class="col-md-2" style="width: 18%;">
						<input type="checkbox" class="chk-waitpool" {{ getObjectValue($data,'IsWaitPool') ? 'checked' : '' }} > Wait Pool
					</div>
					<div class="col-md-2" style="width: 17%;">
						<input type="checkbox" class="chk-denied" {{ getObjectValue($data,'IsDenied') ? 'checked' : '' }} > Denied
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<legend></legend>