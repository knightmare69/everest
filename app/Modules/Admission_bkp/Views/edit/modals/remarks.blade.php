<?php
    
    $status = Request::get('status');

?>
<div class="row">
    <p>Please enter your remarks. </p>
	<div class="col-md-12">
		<textarea class="form-control" id="StatusRemarks" data-status="{{$status}}" placeholder="Type Remarks"></textarea>
	</div>
</div>