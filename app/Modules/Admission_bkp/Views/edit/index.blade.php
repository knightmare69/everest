<style>
.tabbable-line > .nav-tabs > li.active > a {
	background-color: #ccc !important;
	color: #fff !important;
}
</style>
<?php
    $model = new App\Modules\Admission\Models\Admission;
    $appno = decode(Request::get('AppNo'));
    $data  = $model->selectRaw("LastName , FirstName, dbo.fn_Admmision_ApplicationStatus(AppTypeID) As AppType, StatusID ")
            ->where('AppNo',$appno)
	 	     ->first();
    $isAdmitted = false;
    $disabled = '';
    if($data->StatusID == '1') $isAdmitted = true;            
    $disabled = ( $isAdmitted || isParent() ? 'disabled' : '');
	function disabledIfAdmitted() {
        $status = App\Modules\Admission\Models\Admission::where('AppNo',decode(Request::get('AppNo')))->pluck('StatusID');
        return ($status == '2' && !empty(Request::get('AppNo')) && isParent()) ? 'disabled' : '';
	}
    function IsAdmitted(){
	   return false;
	}
    	function isVisibleAppType($id) {
		if (in_array($id,AppConfig()->parentAppTypeIDs())) {
			return true;
		}
		return false;
	}
?>
<div class="row">
    <div class="col-md-12">
        <div class="profile-sidebar ">
            <div class="portlet light bordered profile-sidebar-portlet">
                <div class="clearfix">
					<label class="control-label col-md-12 photo-message hide center"><small class="note font-red">Photo is required</small></label>
					<label class="control-label col-md-12 center">Photo<br /><small class="font-xs">(JPG, PNG Only)</small></label>
					<div class="col-md-12 center">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
								<img src="{{ url('/general/getStudentPhoto?AppNo='.decode(Request::get('AppNo')).'&date='.date('m/d/Y h:i:s')) }}" id="StudentPhotoThumbnail" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
							</div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new">
								Select image </span>
								<span class="fileinput-exists">
								Change </span>
								<input {{ disabledIfAdmitted() }} type="file" name="file" accept="image/x-png, image/jpeg" id="StudentPhoto" class="StudentPhoto not-required">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">
								Remove </a>
							</div>
						</div>
						<div class="clearfix margin-top-10 hide">
							<span class="label label-danger">
							<small>NOTE!</small> </span>
							&nbsp;<small>Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+, Opera11.1+ and FireFox6.0+. In older browsers the filename is shown instead.</small>
						</div>
					</div>
                </div>
                <div class="profile-usertitle ">
                    <div class="profile-usertitle-name uppercase">
                        {{ getObjectValue($data,'LastName') }} <br />
                        {{ getObjectValue($data,'FirstName')}}  
                    </div>
                    <?php 
                    	$hasMade = \App\Modules\Admission\Models\Approval::hasMade(decode(Request::get('AppNo')),getUserID()); 
                    	$isDone = \App\Modules\Admission\Models\Approval::isDone(decode(Request::get('AppNo')));
                    ?>
                    @if (!$isDone)
	                    @if(isPanel() && !$hasMade)
	                    <a href="javascript:;" class="btn btn-success btn-panel-approval" data-status="1">Approve</a>
	                    <a href="javascript:;" class="btn btn-danger btn-panel-approval" data-status="0">Disapprove</a>
	                    @endif
	                    @if($hasMade)
	                    @if (\App\Modules\Admission\Models\Approval::isApproved(decode(Request::get('AppNo')),getUserID()))
	                    	<a href="javascript:;" class="btn btn-success" data-status="1">Approved</a>
	                    @else
	                    	<a href="javascript:;" class="btn btn-danger" data-status="1">Disapproved</a>
	                    @endif
	                    @endif
                    @endif
                </div>
                <div class="profile-usermenu" style="margin-top: 10px;">
                    <ul class="nav" id="TabProfile">
                        <li class="active" data-tab="info" data-title="Admission">
                            <a href="#year" data-toggle="tab"><i class="fa fa-tags"></i> Admission </a>
                        </li>
                        <li data-tab="info" data-title="Personal Profile">
                            <a href="#student" data-toggle="tab"><i class="icon-user"></i> Personal Profile </a>
                        </li>
                       <!--  <li data-tab="educ" data-title="Educational Attainment">
                            <a href="#educ" data-toggle="tab"><i class="fa fa-graduation-cap"></i> Educational Attainment </a>
                        </li> -->
                         <li data-tab="scholastic" data-title="Scholasctic Record">
                            <a href="#scholastic" data-toggle="tab"><i class="fa fa-folder-open"></i> Scholastic Record </a>
                        </li>
                        <li data-tab="family" data-title="Family">
                            <a href="#family" data-toggle="tab"><i class="icon-users"></i> Family Background </a>
                        </li>
                        <li data-tab="siblings" data-title="Siblings">
                            <a href="#siblings" data-toggle="tab"><i class="fa fa-child"></i> Siblings </a>
                        </li>
                        <li data-tab="references" data-title="References">
                            <a href="#references" data-toggle="tab"><i class="fa fa-ticket"></i> References </a>
                        </li>
                        <li data-tab="docs" data-title="Document">
                            <a href="#documents" data-toggle="tab"><i class="fa fa-book"></i> Documents </a>
                        </li>
                        <li class="{{ isPermissionHas('admission-evaluation','read') ? '' : 'hide' }}" data-tab="evaluation" data-title="Admissions Evaluation">
                            <a href="#evaluation" data-toggle="tab"><i class="fa fa-file-archive-o"></i> Admissions Evaluation </a>
                        </li>
                        <li class="{{ isPermissionHas('committee-remarks','read') ? '' : 'hide' }}" data-tab="committee" data-title="Committee Remarks">
                            <a href="#committee" data-toggle="tab"><i class="fa fa-users"></i> Committee Remarks </a>
                        </li>                                            
                    </ul>
                </div>
                <div class="profile-usertitle hide" style="margin-top: 10px;margin-bottom: 10px;">
                	<div class="profile-usertitle-name uppercase">
                    	<em>Comments</em>
                	</div>
                	@foreach(\App\Modules\Admission\Models\Approval::getComments(decode(Request::get('AppNo'))) as $row)
                	<div style="border-bottom: 1px solid #f0f4f7;text-align: left;font-size: 8px;padding-left: 10px;overflow-wrap: break-word;">
                		<p>{{$row->Comment}} - <small><i>{{$row->FullName}}</i></small></p>
                	</div>
                	@endforeach
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="col-sm-12">                        
            <div class="tab-content mainTabContent">
				<div class="row">
		            <div class="col-md-12">
		                @include('errors.event')
		            </div>
		        </div>
				@include($views.'sub.right')
				<div class="form-actions">
					<div class="row">
						<div class="col-md-12">      
						      <button type="submit" class="btn green hide final-button" id="btnSubmit" {{$disabled}}>Submit Admission</button>						      
						      <button type="submit" class="btn btn-primary hide" id="btnNewApp">Create New Application</button>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
			</div>
            </div>
        </div>                    
    </div>
</div>
@include($views.'modals.modals')
@include($views.'sub.photoswipe')