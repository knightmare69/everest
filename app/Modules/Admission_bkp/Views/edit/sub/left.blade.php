<div class="tabbable-line">
	<ul class="nav nav-tabs tabs-left" id="adminssionSteps">
		<li class="active">
			<a href="#year" data-toggle="tab" class="a-adminssionSteps">Year Applying for</a>
		</li>
		<li>
			<a href="#student" data-toggle="tab" class="a-adminssionSteps">Student</a>
		</li>
		<li>
			<a href="#family" data-toggle="tab" class="a-adminssionSteps">Family</a>
		</li>
		<li>
			<a href="#siblings"  data-toggle="tab" class="a-adminssionSteps">Siblings</a>
		</li>
		<li>
			<a href="#documents" data-toggle="tab" class="a-adminssionSteps">Documents</a>
		</li>
		<li>
			<a href="#review" data-toggle="tab" class="a-adminssionSteps review">Review</a>
		</li>
		<li>
			<a href="#complete" class="last-step a-adminssionSteps" data-toggle="tab">Complete</a>
		</li>
	</ul>
</div>