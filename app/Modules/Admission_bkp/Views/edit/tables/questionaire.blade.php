<table class="table" id="tableQuestions">
	<thead>
	
		<tr>
			<td width="3%">&nbsp;</td>
			<th width="97%">Question & Answer</th>
		</tr>
	</thead>
	<tbody>
		<?php $i=1;?>
		@foreach(\App\Modules\Admission\Models\QuestionaireAnswer::where('AppNo',decode(Request::get('AppNo')))->get() as $row)
		<tr data-id="{{$row->id}}" data-question="{{$row->question}}">
			<td>{{$i++}}</td>
			<td>
			{{$row->question}} <br />
			{{$row->description}}
			<br />
			@if($row->type == 'select')
			<select class="form-control answer">
				<?php $options = explode(',',$row->options); ?>
				@foreach($options as $opt)
				<option {{ strtolower($row->answer) == strtolower($opt) ? 'selected' : '' }}>{{$opt}}</option>
				@endforeach
			</select>
			@else
			<input type="text" class="form-control answer" name="answer-"{{$row->id}} value="{{$row->answer}}">
			@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>


