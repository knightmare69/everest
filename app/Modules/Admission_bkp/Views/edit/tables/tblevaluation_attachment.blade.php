<?php
    $attach = App\Modules\Admission\Models\AdmissionEvaluationAttachment::where('AppNo',decode(Request::get('AppNo')))->get();
    $attach = isset($attach) ? $attach : []; 
?>

<div class="portlet box grey-salt">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-helper bold" style="color:black;">&nbsp;Supporting Documents</span>
        </div>
        <div class="tools">
        </div>
        <div class="actions">
           <a {{ (getUserGroup() == 'principal' || getUserGroup() == 'director') ? 'disabled' : '' }} class="btn default btnattach-add" href="javascript:void(0);"><i class="fa fa-paperclip"></i> Attachment</a> 
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">   
                <table class="table table-bordered table-evalattach"> 
                    <thead>
                        <tr class="heading">
                            <th class="center" width="10">&nbsp;</th>
                            <th width="400"><small>Files</small></th>
                            <th width="300" class="center"><small>Type</small></th>
                            <th width="150" class="center"><small>Size (Bytes)</small></th>
                            <th class="center"><i class="fa fa-folder-open-o"></i></th>
                            <!-- <th class="center"><i class="fa fa-download"></i></th> -->
                        </tr>
                    </thead>
                <tbody>
                @foreach($attach as $row)
                    <tr class="include" data-id="{{ encode($row->AttachmentID) }}">
                        <td style="vertical-align: middle;">
                            <span class="cursor-pointer btnattach-remove"><i class="fa fa-trash font-red"></i></span>
                        </td>
                        <td width="300">
                            <span class="fileinput fileinput-exists" data-provides="fileinput">
                            <span class="btn blue-steel btn-sm btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span><input {{ disabledIfAdmitted() }} type="file" class="file" id="fileUpload" name="..."></span>
                            <span class="fileinput-filename span_validate">{{$row->FileName}}</span><span class="fileinput-new" style="color: #ff0000">No file selected</span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a></span>
                        </td>
                        <td class="center"><input type="text" class="form-control custom-input-sm filetype" name="filetype">{{ $row->FileType }}</td>
                        <td class="center"><input type="text" class="form-control custom-input-sm filesize" name="filesize">{{ $row->FileSize }}</td>
                        <td>
                            <span style="white-space:nowrap;"><a class="btn btn-sm default btnpreview" href=""><i class="fa fa-file-photo-o font-green"></i> Preview</a>
                        </td>
                    </tr>
                @endforeach
                @if ( count($attach) <= 0 )
                    <tr class="default">
                        <td class="center" colspan="6">No Attachment Found!</td>
                    </tr>
                @endif
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<table class="table hide" id="table-defaultevalattach">
    <tbody>
        <tr class="include">
            <td style="vertical-align: middle;">
                <span class="cursor-pointer btnattach-remove"><i class="fa fa-trash font-red"></i></span>
            </td>
            <td width="300">
                <span class="fileinput fileinput-new" data-provides="fileinput">
                <span class="btn blue-steel btn-sm btn-file">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span><input type="file" class="file" id="fileUpload" name="..."></span>
                <span class="fileinput-filename span_validate"></span><span class="fileinput-new" style="color: #ff0000">No file selected</span>
                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a></span>
            </td>
            <td class="center"><input disabled type="text" class="form-control custom-input-sm filetype" name="filetype"></td>
            <td class="center"><input disabled type="text" class="form-control custom-input-sm filesize" name="filesize"></td>
            <td>
                <span style="white-space:nowrap;"><a disabled class="btn btn-sm default btnattach-preview" href=""><i class="fa fa-file-photo-o font-green"></i> Preview</a>
            </td>
            <!-- <td>
                <span style="white-space:nowrap;"><a  class="btn btn-sm default btnattach-dl" href=""><i class="fa fa-download font-blue"></i> Download</a>
            </td> -->
        </tr>
    </tbody>
</table>