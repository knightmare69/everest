<table class="table" id="tableSchoolsAttended">
	<thead>
		<tr>
			<th class="right" colspan="5">
				<button type="button" class="btn btn-primary" id="btnAddSchoolAttended">Add new</button>
			</th>
		</tr>
		<tr>
			<td width="3%">&nbsp;</td>
			<th width="15%">Name of School's Attended</th>
			<th width="15%">Address</th>
			<th width="20%">Dates Attended <br /> <small>(Month-Year to Month-Year)</small></th>
			<th width="10%">Grade Level</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$month = [
				'January','February','March','April','May',
				'June','July','August','September','October',
				'November','December'
			];
		?>
		@foreach(\App\Modules\Admission\Models\SchoolsAttended::where('AppNo',decode(Request::get('AppNo')))->get() as $row)
		<?php
			$fromDate=explode('/',$row->fromDate);
			$fromMonth = $fromDate[0];
			$fromYear = isset($fromDate[1]) ? $fromDate[1] : '';

			$toDate=explode('/',$row->toDate);
			$toMonth = $toDate[0];
			$toYear = isset($toDate[1]) ? $toDate[1] : '';
		?>
		<tr>
			<td>
				<button type="button" class="btn btn-sm red btnSchoolsAttendedRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input type="text" maxlength="500" name="school" class="form-control school" placeholder="School" value="{{ $row->school }}">
			</td>
			<td>
				<input type="text" maxlength="1000" name="address" class="form-control address"  placeholder="Address" value="{{ $row->address }}">
			</td>
			<td>
				<div class="row" style="background-color: #fff !important;">
					FROM: <br />
					<div class='col-md-6' style="padding-right: 2px !important">
						<select class="form-control FromMonthAttended" name="FromMonthAttended">
							<option>--MONTH --</option>
							@foreach($month as $m)
							<option {{ strtolower($m) == strtolower($fromMonth) ? 'selected="selected"' : '' }}>{{ $m }}</option>
							@endforeach
						</select>
					</div>
					<div class='col-md-6' style="padding-left: 2px !important">
						<select class="form-control FromYearAttended" name="FromYearAttended">
							<option>--YEAR --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option {{ strtolower($year) == strtolower($fromYear) ? 'selected="selected"' : '' }}>{{ $year }}</option>
							@endfor
						</select>
					</div>
				</div>
				<div class="row" style="background-color: #fff !important;">
					TO: <br />
					<div class='col-md-6' style="padding-right: 2px !important">
						<select class="form-control ToMonthAttended" name="ToMonthAttended">
							<option>--MONTH --</option>
							@foreach($month as $m)
							<option {{ strtolower($m) == strtolower($toMonth) ? 'selected="selected"' : '' }}>{{ $m }}</option>
							@endforeach
						</select>
					</div>
					<div class='col-md-6' style="padding-left: 2px !important">
						<select class="form-control ToYearAttended" name="ToYearAttended">
							<option>--YEAR --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option {{ strtolower($year) == strtolower($toYear) ? 'selected="selected"' : '' }} >{{ $year }}</option>
							@endfor
						</select>
					</div>
					</div>
			</td>
			<td>
				<input type="text" name="GradeLevel" class="form-control GradeLevel" maxlength="30" placeholder="Year Level" value="{{ $row->yearLevel }}">
			</td>
		</tr>
		@endforeach
	</tbody>
</table>


