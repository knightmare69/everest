<?php
	function getCurrentSY(){
		$response = '';
		$termid = 0;
	    if(isset($inquiry)){
	      $termid = $inquiry->TermID;   
	    }

	    foreach(App\Modules\Setup\Models\AcademicYearTerm::where('Hidden',0)->orderBy('AcademicYear','DESC')->get() as $row){
            if(substr($row->AcademicYear,0,4)>=date('Y') && (($row->Active_OnlineEnrolment==1 && $termid==0) || $row->TermID==$termid)){
				$response = $row->AcademicYear;
            }
        }
        return $response;
	}

	function getStatusID() {
		 return 
		 Request::get('AppNo') ?
		 App\Modules\Admission\Models\Admission
	 	::select('StatusID')
	 	->where('AppNo',Request::get('AppNo'))
	 	->pluck('StatusID')
	 	: '';
	}
?>
<!-- <div class="tab-pane active" id="year">
	<div class="row">
		<div class="portlet light bg-inverse">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-equalizer font-green-haze"></i>
					<span class="caption-subject font-green-haze bold uppercase">Applying For</span>
					<span class="caption-helper"></span>
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form">
                
			</div>
		</div>
	</div>
</div> -->
<div class="tab-pane active" id="student">
	<div class="row">
		<div class="portlet bordered light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit font-green-haze"></i>
					<span class="caption-subject font-green-haze bold">Create an Application</span>
				</div>
				<div class="tools">
					<div class="form-group hide">
						<label class="control-label col-md-2" style="margin-top: 4% !important;margin-right: 4% !important">Status</label>
						<div class="col-md-8">
							<select class="form-control" name="AdmissionStatus" id="AdmissionStatus">
								<option value="">Select...</option>
								<?php $i = 0; ?>
								@foreach(App\Modules\Admission\Models\AdmissionStatus::orderBy('SeqNo','ASC')->get() as $row)
								<option {{ getStatusID() ? $row->StatusID == getStatusID() ? 'selected' : '' : $i <=0 ? 'selected' : '' }}   value="{{ encode($row->StatusID) }}">{{ $row->StatusDesc }}</option>
								<?php $i++; ?>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet-body form">	
				<div class="alert alert-info">
	                <i class="fa fa-info-circle"></i> On this page you will find a few pointers to get you started with the application.
	            </div>	
				@include($views.'forms.year')
				<legend style="font-size: 15px;font-weight: bold;">Student Information</legend>
				@include($views.'forms.student')		
			</div>
		</div>
	</div>
</div>
<div class="tab-pane" id="scholastic">
	<div class="row">
		<div class="portlet bordered light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-folder-open font-green-haze"></i>
					<span class="caption-subject font-green-haze bold">Scholastic Record</span>
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form sch_wrapper">	
				@include($views.'forms.sch_kndr_gr1')		
			</div> 
		</div>
	</div>
</div>
<div class="tab-pane" id="parents">
	<div class="row">
		<div class="portlet bordered light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-folder-open font-green-haze"></i>
					<span class="caption-subject font-green-haze bold">Parent Information</span>
				</div>
				<div class="tools">
					@if(getUserGroup() != 'parent')
					<!-- <a href="javascript:;" data-original-title="Search Family here" title="" id="FamilySearch">
						<i class="fa fa-search"></i> Search Family
					</a> -->
					<!-- <a href="javascript:;" data-original-title="Clear Family Background" title="" id="FamilyClearBG">
						<i class="fa fa-trash"></i> Clear
					</a> -->
					<button class="btn btn-default btn-sm" id="FamilySearch"><i class="fa fa-search"></i> Search Family</button>
					<button class="btn btn-default btn-sm" id="FamilyClearBG"><i class="fa fa-trash-o"></i> Clear</button>
					@endif
				</div>
			</div>
			<div class="portlet-body form">
				@include($views.'forms.parent')
			</div>
		</div>
	</div>
</div>
<div class="tab-pane" id="siblings">
	<div class="row">
		<div class="portlet bordered light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-folder-open font-green-haze"></i>
					<span class="caption-subject font-green-haze bold">Siblings Information</span>
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form">
				<p style="font-style:italic;">
					Please input the following siblings: <br/> Only Siblings who are not currently attending this school and sibling also applying for this school.
				</p>
				<!-- BEGIN FORM-->
				@include($views.'tables.siblings')
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<div class="tab-pane" id="references">
	<div class="row">
		<div class="portlet bordered light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-folder-open font-green-haze"></i>
					<span class="caption-subject font-green-haze bold">References</span>
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form">
				<p style="font-style:italic;">
					Please list below the names, schools, and telephone numbers of those who will be completing the recommendation forms.
				</p>
				<!-- BEGIN FORM-->
				@include($views.'tables.references')
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php if(!isParentOrStudents()){?>
<!-- <div class="tab-pane" id="documents">
	<div class="portlet light bg-inverse ">
		<div class="portlet-title">
			<div class="caption">
				<i class="icon-equalizer font-green-haze"></i>
				<span class="caption-subject font-green-haze bold uppercase">Documents</span>
				<span class="caption-helper">Information</span>
			</div>
			<div class="tools">
			</div>
		</div>
		<div class="portlet-body form">
			<p>
				Please input the following siblings: Only Siblings who are not currently attending this school and sibling also applying for this school.
			</p> -->
			<!-- BEGIN FORM-->
<!-- 			@include($views.'tables.documents') -->
			<!-- END FORM-->
<!-- 		</div>
	</div>
</div> -->
<?php } ?>
<div class="tab-pane" id="review">
	<div class="row">
		<div class="portlet light ">
			<div class="portlet-title" style="border-bottom: unset; margin-bottom: unset;">
				<div class="caption">
					<i class="fa fa-info-circle font-green-haze"></i>
					<span class="caption-subject font-green-haze bold">Review Information</span>
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form">
			    <div class="panel panel-default hide">
				  <div class="panel-heading">
				    <h4 class="panel-title">
							Privacy Policy
					</h4>
				  </div>
				  <div class="panel-body">
				            Policy goes in here.
				  </div>
				</div>
				<div class="panel-group accordion accordionReview" id="accordion3">
	<!-- 				<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_year">
							Year Applying For </a>
							</h4>
						</div>
						<div id="collapse_year" class="panel-collapse in">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="1">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div> -->
					<div class="panel panel-default">
						<div class="panel-heading hide">
							<h4 class="panel-title">
							<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_student">
							Student Information </a>
							</h4>
						</div>
						<div id="collapse_student" class="panel-collapse in">
							<div class="panel-body" style="border-top: unset;">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="1">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading hide">
							<h4 class="panel-title">
							<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_scholastic">
							Scholastic Record </a>
							</h4>
						</div>
						<div id="collapse_scholastic" class="panel-collapse in">
							<div class="panel-body" style="border-top: unset;">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="2">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading hide">
							<h4 class="panel-title">
							<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_parents">
							Parent Information </a>
							</h4>
						</div>
						<div id="collapse_parents" class="panel-collapse in">
							<div class="panel-body" style="border-top: unset;">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="3">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading hide">
							<h4 class="panel-title" style="border-top: unset;">
							<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_siblings">
							Siblings </a>
							</h4>
						</div>
						<div id="collapse_siblings" class="panel-collapse in">
							<div class="panel-body" style="border-top: unset;">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="4">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div>
				<!-- 	<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_documents">
							Documents</a>
							</h4>
						</div>
						<div id="collapse_documents" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="5">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div> -->
					<div class="panel panel-default">
						<div class="panel-heading hide">
							<h4 class="panel-title">
							<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_references">
							References</a>
							</h4>
						</div>
						<div id="collapse_references" class="panel-collapse in">
							<div class="panel-body" style="border-top: unset;">
								<div class="row">
									<div class="col-md-12 right">
										<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit pull-right" data-step="5">Edit</a>
									</div>
								</div>
								<div class="row wrapper_body">
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tab-pane" id="complete">
	<div class="row">
		<div class="portlet bordered light">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-equalizer font-green-haze"></i>
					<span class="caption-subject font-green-haze bold uppercase">Complete</span>
					<span class="caption-helper"></span>
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form complete-form">
				
			</div>
		</div>
	</div>
</div>