<div class="tabbable-line">
	<ul class="nav nav-tabs tabs-left" id="adminssionSteps">
		<!-- <li class="active">
			<a href="#year" data-toggle="tab" class="a-adminssionSteps">Year Applying for</a>
		</li> -->
		<li class="active">
			<a href="#student" data-toggle="tab" class="a-adminssionSteps">Student</a>
		</li>
		<li>
			<a href="#scholastic" data-toggle="tab" class="a-adminssionSteps">Scholastic</a>
		</li>
		<li>
			<a href="#parents" data-toggle="tab" class="a-adminssionSteps">Family</a>
		</li>
		<li>
			<a href="#siblings"  data-toggle="tab" class="a-adminssionSteps">Siblings</a>
		</li>
		<li>
			<a href="#references"  data-toggle="tab" class="a-adminssionSteps">References</a>
		</li>
		<?php if(!isParentOrStudents()){?>
		<!-- <li>
			<a href="#documents" data-toggle="tab" class="a-adminssionSteps">Documents</a>
		</li> -->
		<?php }?>
		<li>
			<a href="#review" data-toggle="tab" class="a-adminssionSteps review">Review</a>
		</li>
		<li>
			<a href="#complete" class="last-step a-adminssionSteps" data-toggle="tab">Complete</a>
		</li>
	</ul>
</div>