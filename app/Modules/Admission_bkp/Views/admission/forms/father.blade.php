<?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $data = isset($data[0]) ? $data[0] : [];  ?>
<form action="#" class="form-horizontal" id="formFather">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-body">
		<div class="row">
			<div class="col-md-12">
				<h4 class="form-section">Father Information</h4>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Name</label>
					<div class="col-md-9">
						<input type="text" class="form-control" placeholder="Name" name="FatherName" value="{{ getObjectValue($data,'Father_Name') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Date of Birth</label>
					<div class="col-md-9">
						<input type="text" class="form-control date-picker {{ isAdmin() ? 'not-required' : '' }}" data-date-format="mm/dd/yyyy" placeholder="DOB" name="FatherDateOfBirth" value="{{ setDateFormat(getObjectValue($data,'Father_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Company</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Company" name="FatherCompany" value="{{ getObjectValue($data,'Father_Company') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Occupation</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Occupation" name="FatherOccupation" value="{{ getObjectValue($data,'Father_Occupation') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Business Address</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Address" name="FatherBusinessAddress" value="{{ getObjectValue($data,'Father_CompanyAddress') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Business Phone</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Phone" name="FatherBusinessPhone" value="{{ getObjectValue($data,'Father_CompanyPhone') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Email Address</label>
					<div class="col-md-9">
						<input type="email" class="form-control not-required" placeholder="Email" name="FatherEmailAddress" value="{{ getObjectValue($data,'Father_Email') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Mobile Number</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="Mobile" name="FatherMobile" value="{{ getObjectValue($data,'Father_Mobile') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">Educational Attainment</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required"  placeholder="Education" name="FatherEducation" value="{{ getObjectValue($data,'Father_EducAttainment') }}">
					</div>
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">School Attended</label>
					<div class="col-md-9">
						<input type="text" class="form-control not-required" placeholder="School" name="FatherSchool" value="{{ getObjectValue($data,'Father_SchoolAttended') }}">
					</div>
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
	</div>
</form>