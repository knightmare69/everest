<?php
    $termid = 0;
    $campus = 0;
    $atype  = 0;
    $yrlvl  = 0;
    $prgcls = 0;
    if($inquiry){
      $termid = $inquiry->TermID;   
      $yrlvl  = $inquiry->YearLevelID;
      $prgcls = $inquiry->ProgClass;
      $atype  = 1;
      $campus = 1;      
    }
?>
<form action="#" class="form-horizontal" id="formYear">
    <div class="form-body">
        <input type="text" class="form-control hide not-required do-not-review" name="IsHigherLevel" id="IsHigherLevel" value="{{ AppConfig()->isHigherLevel() }}">
        <input type="hidden" readonly class="form-control not-required" name="PogramName" id="gradeProgramName">
        <input type="hidden" readonly class="form-control not-required" name="gradeProgramID" id="gradeProgramID">
        <input type="hidden" readonly class="form-control not-required" name="gradeProgramClass" id="gradeProgramClass">                
        <input type="hidden" readonly class="form-control not-required" name="gradeProgramMajorID" id="gradeProgramMajorID">
                
        @if(AppConfig()->isHigherLevel())
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="form-group">
                    <label class="control-label col-md-2">&nbsp;</label>
                    <div class="icheck-inline">
                        <label><input type="radio" name="EducLevel" value="basic" checked class="icheck EducLevel" data-radio="iradio_square-grey"> K to 12 (Basic Education) </label>
                        <label><input type="radio" name="EducLevel"  value="higher" class="icheck EducLevel" data-radio="iradio_square-grey"> College</label>
                    </div>
                </div> -->
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">Academic Year & Term</label>
                    <div class="col-md-4">
                        <?php $i = 0; ?>
                        <select class="form-control" name="schoolYear" id="schoolYear">
                            <option value="">Select...</option>
                            @foreach(App\Modules\Setup\Models\AcademicYearTerm::where('Hidden',0)->orderBy('AcademicYear','DESC')->get() as $row)
                            @if(substr($row->AcademicYear,0,4)>=date('Y') || $row->Active_OnlineEnrolment==1)
                            <option  {{ (($row->Active_OnlineEnrolment==1 && $termid==0) || $row->TermID==$termid) ? 'selected' : '' }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear }} {{ $row->SchoolTerm }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row BasicControl hidden">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">School Campus</label>
                    <div class="col-md-4">
                        <select class="form-control {{ AppConfig()->isHigherLevel() ? 'not-required' : '' }}" name="schoolCampus" id="schoolCampus">
                            <option value="">Select...</option>
                            @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                            <option value="{{ encode($row->CampusID) }}" {{  ($row->CampusID==1 || $row->CampusID==$campus) ? 'selected' : '' }}>{{ $row->Code }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>         
        <!--/row-->

        <div class="row BasicControl">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">Grade Level</label>
                    <div class="col-md-4">
                        <input type="hidden" class="form-control not-required" id="SetProgClass" name="SetProgClass" value="<?php echo $prgcls;?>">
                        <input type="hidden" class="form-control not-required" id="SetGradeLevel" name="SetGradeLevel" value="<?php echo $yrlvl;?>">
                        <select class="form-control" name="gradeLevel" id="gradeLevel" data-inquiry='<?php echo encode($yrlvl);?>'>
                            <option value="">- SELECT -</option>
                            <!-- @foreach(App\Modules\Admission\Models\Admission::getGradeLevel() as $row)
                                <option  {{ ($row->YearLevelID==$yrlvl && $row->ProgClass==$prgcls) ? 'selected' : '' }} value="{{ encode($row->YearLevelID) }}" data-code="{{ encode($row->ProgClass) }}">{{ $row->YearLevelName }}</option>
                            @endforeach -->
                         </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row BasicControl">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">Application type</label>
                    <div class="col-md-4">
                        <select class="form-control" name="applicationType" id="applicationType">
                            <option value="">- SELECT -</option>
                            @foreach(App\Modules\Admission\Models\Admission::getApplicationType() as $row)
                                @if($row->AppTypeID != 3)
                                <option {{ ($row->SeqNo == 1 || $row->AppTypeID==$atype)? 'selected' : '' }} value="{{ encode($row->AppTypeID)}}">{{ $row->ApplicationType }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>   
        </div> 

        <div class="row BasicControl">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2"></label>
                    <div class="col-md-10">
                        <input type="checkbox" name="wExistSib" id="wExistSib">
                        <label class="control-label">I already have a child enrolled in Everest Academy</label>
                   </div> 
                </div>
            </div>
        </div> 

        
        @if(AppConfig()->isHigherLevel())
            @if(AppConfig()->campusAndCourse1())
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">School Campus</label>
                        <div class="col-md-8">
                            <select class="form-control {{ !AppConfig()->campusAndCourse1Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse1Required() }}" name="schoolCampus1" id="schoolCampus1">
                                <option value="">Select...</option>
                                @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                                <option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label id="lblCourse1Choice" class="control-label col-md-2">First Choice</label>
                        <div class="col-md-8">
                            <input type="hidden" class="form-control not-required" name="Course1Choice" id="Course1Choice">
                            <input type="hidden" class="form-control not-required" name="Course1MajorChoice" id="Course1MajorChoice">
                            <select class="form-control not-required CourseSel" data-isRequired="{{ AppConfig()->campusAndCourse1Required() }}" name="Course1" id="Course1" data-placeholder="Course">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(AppConfig()->campusAndCourse2())
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">School Campus</label>
                        <div class="col-md-8">
                            <select class="form-control {{ !AppConfig()->campusAndCourse2Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse2Required() }}" name="schoolCampus2" id="schoolCampus2">
                                <option value="">Select...</option>
                                @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                                <option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">Course Second Choice</label>
                        <div class="col-md-8">
                            <input type="hidden" class="form-control not-required" name="Course2Choice" id="Course2Choice">
                            <input type="hidden" class="form-control not-required" name="Course2MajorChoice" id="Course2MajorChoice">
                            <select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse2Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse2Required() }}" name="Course2" id="Course2" data-placeholder="Course">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(AppConfig()->campusAndCourse3())
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">School Campus</label>
                        <div class="col-md-8">
                            <select class="form-control {{ !AppConfig()->campusAndCourse2Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse3Required() }}" name="schoolCampus3" id="schoolCampus3">
                                <option value="">Select...</option>
                                @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                                <option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">Course Third Choice</label>
                        <div class="col-md-8">
                            <input type="hidden" class="form-control not-required" name="Course3Choice" id="Course3Choice">
                            <input type="hidden" class="form-control not-required" name="Course3MajorChoice" id="Course3MajorChoice">
                            <select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse3Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse3Required() }}" name="Course3" id="Course3" data-placeholder="Course">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(AppConfig()->campusAndCourse4())
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">School Campus</label>
                        <div class="col-md-8">
                            <select class="form-control {{ !AppConfig()->campusAndCourse4Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse4Required() }}" name="schoolCampus4" id="schoolCampus4">
                                <option value="">Select...</option>
                                @foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
                                <option selected value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row HigherControl hide">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">Course Fourth Choice</label>
                        <div class="col-md-8">
                            <input type="hidden" class="form-control not-required" name="Course4Choice" id="Course4Choice">
                            <input type="hidden" class="form-control not-required" name="Course4MajorChoice" id="Course4MajorChoice">
                            <select class="form-control select2 CourseSel {{ !AppConfig()->campusAndCourse4Required() ? 'not-required' : '' }}" data-isRequired="{{ AppConfig()->campusAndCourse4Required() }}" name="Course4" id="Course4" data-placeholder="Course">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endif
        <!-- <div class="row BasicControl">
            <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">Year Level</label>
                        <div class="col-md-5">
                            <input type="hidden" class="form-control not-required" id="SetProgClass" name="SetProgClass" value="<?php echo $prgcls;?>">
                            <input type="hidden" class="form-control not-required" id="SetGradeLevel" name="SetGradeLevel" value="<?php echo $yrlvl;?>">
                            <select class="form-control" name="gradeLevel" id="gradeLevel" data-inquiry='<?php echo encode($yrlvl);?>'>
                                <option value="">- Select -</option>
                                @foreach(App\Modules\Admission\Models\Admission::getGradeLevel() as $row)
                                <option  {{ ($i++ <= 0 || ($row->YearLevelID==$yrlvl && $row->ProgClass==$prgcls))? 'selected' : '' }} value="{{ encode($row->YearLevelID) }}" data-code="{{ encode($row->ProgClass) }}">{{ $row->YearLevelName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
        </div> -->
        <div class="row BasicControl">
            <div class="col-md-12">                
            <div class="form-group strands " style="display: none;">
                <label class="control-label col-md-2">Strands</label>
                <div class="col-md-5">
                <select class="form-control" name="gradeMajorID" id="gradeMajorID">
                    <option value="0">- Select -</option>
                    @foreach(App\Modules\Setup\Models\ProgramMajors::selectRaw('*, dbo.fn_MajorName(MajorDiscID) As major')->where('ProgID',29)->get() as $r)
                    <option value="{{ encode($r->MajorDiscID) }}">{{ $r->major }}</option>
                    @endforeach                
                </select>
                </div>
            </div>
            </div>
        </div>                        
    </div>
</form>