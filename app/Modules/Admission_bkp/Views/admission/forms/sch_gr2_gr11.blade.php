<?php 
	$admission = App\Modules\Admission\Models\Admission::getAdmission(decode(Request::get('AppNo')),
	['LRN','PresentSchool','PresentSchoolCurriculum','PresentSchoolCurriculumOthers','PresentSchoolWebsite','PresentSchoolHead',
	'PresentSchoolContactNo','PresentGradeLevel','PresentSchoolDateAttended','PresentSchoolCalendarMonths','PresentSchoolReasonLeaving',
	'PresentSchoolPassAllSubjects','PresentSchoolSubjectsFailing']);
	$admission = isset($admission[0]) ? $admission[0] : [];	

	function getAnswerByID($key){
		$scholastic = App\Modules\Admission\Models\QuestionaireAnswer::where(['app_no'=>decode(Request::get('AppNo')),'question_id'=>$key])->get();
		return isset($scholastic[0]) ? $scholastic[0]->answer : '';
	}

	function disabledIfAdmitted() {
        $status = App\Modules\Admission\Models\Admission::where('AppNo',decode(Request::get('AppNo')))->pluck('StatusID');
        return ($status == '2' && !empty(Request::get('AppNo')) && isParent()) ? 'disabled' : '';
	}
?>
<form action="#" class="form-horizontal" id="formScholastic">
	<div class="form-body">	
		<legend style="font-size: 15px;font-weight: bold;">Learner Reference Number</legend>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-4">LRN</label>
					<div class="col-md-8">
						<div class="input-icon">
							<i class="fa fa-ticket"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="LRN" maxlength="12" value="{{ getObjectValue($admission,'LRN') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<legend style="font-size: 15px;font-weight: bold;">Scholastic Information</legend>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-6">
						<label class="control-label">Present School</label>
						<div class="input-icon">
							<i class="fa fa-building-o"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PresentSchool" value="{{ getObjectValue($admission,'PresentSchool') }}">
						</div>
					</div>
					<div class="col-md-3">
						<label class="control-label">Present School’s Curriculum</label>
						<select {{ disabledIfAdmitted() }} class="form-control" name="PresentSchoolCurriculum" id="PresentSchoolCurriculum">
							<option {{ getObjectValue($admission,'PresentSchoolCurriculum') == 'Philippine' ? 'selected' : '' }}>Philippine</option>
							<option {{ getObjectValue($admission,'PresentSchoolCurriculum') == 'American' ? 'selected' : '' }}>American</option>
							<option {{ getObjectValue($admission,'PresentSchoolCurriculum') == 'British' ? 'selected' : '' }}>British</option>
							<option {{ getObjectValue($admission,'PresentSchoolCurriculum') == 'Other' ? 'selected' : '' }}>Other</option>
						</select>
					</div>
					<div class="col-md-3">
						<label class="control-label">If Others</label>
						<div class="input-icon">
							<i class="fa fa-folder-open"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="PresentSchoolCurriculumOthers" id="PresentSchoolCurriculumOthers" value="{{ getObjectValue($admission,'PresentSchoolCurriculumOthers') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Head of School</label>
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="PresentSchoolHead" value="{{ getObjectValue($admission,'PresentSchoolHead') }}">
						</div>
					</div>
					<div class="col-md-4">
						<label class="control-label">Website</label>
						<div class="input-icon">
							<i class="fa fa-external-link"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required" name="PresentSchoolWebsite" value="{{ getObjectValue($admission,'PresentSchoolWebsite') }}">
						</div>
					</div>
					<div class="col-md-4">
						<label class="control-label">Phone</label>
						<div class="input-icon">
							<i class="fa fa-phone"></i>
							<input {{ disabledIfAdmitted() }} type="number" class="form-control not-required" name="PresentSchoolPhone" value="{{ getObjectValue($admission,'PresentSchoolContactNo') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label class="control-label">Last grade level completed</label>
						<div class="input-icon">
							<i class="fa fa-folder-open"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PresentGradeLevel" value="{{ getObjectValue($admission,'PresentGradeLevel') }}">
						</div>
					</div>
					<div class="col-md-3">
						<label class="control-label">Last Month Attended</label>
						<?php $options = explode(',',getObjectValue($admission,'PresentSchoolDateAttended')); ?>
						<select {{ disabledIfAdmitted() }} class="form-control ES" name="LastMonthAttended">
							<option>-- CHOOSE --</option>
							<option {{ $options[0] == 'January' ? 'selected' : '' }}>January</option>
							<option {{ $options[0] == 'February' ? 'selected' : '' }}>February</option>
							<option {{ $options[0] == 'March' ? 'selected' : '' }}>March</option>
							<option {{ $options[0] == 'April' ? 'selected' : '' }}>April</option>
							<option {{ $options[0] == 'May' ? 'selected' : '' }}>May</option>
							<option {{ $options[0] == 'June' ? 'selected' : '' }}>June</option>
							<option {{ $options[0] == 'July' ? 'selected' : '' }}>July</option>
							<option {{ $options[0] == 'August' ? 'selected' : '' }}>August</option>
							<option {{ $options[0] == 'September' ? 'selected' : '' }}>September</option>
							<option {{ $options[0] == 'October' ? 'selected' : '' }}>October</option>
							<option {{ $options[0] == 'November' ? 'selected' : '' }}>November</option>
							<option {{ $options[0] == 'November' ? 'selected' : '' }}>November</option>
						</select>
					</div>
					<div class="col-md-3">
						<label class="control-label">Last Year Attended</label>
						<select {{ disabledIfAdmitted() }} class="form-control ES" name="LastYearAttended">
							<option>-- CHOOSE --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option {{ !empty(getObjectValue($admission,'PresentSchoolDateAttended')) ? $options[1] == $year ? 'selected' : '' : '' }}>{{ $year }}</option>
							@endfor
						</select>
					</div>
					<div class="col-md-3">
						<label class="control-label">School Calendar Months</label>
						<div class="input-icon">
							<i class="fa fa-calendar"></i>
							<input {{ disabledIfAdmitted() }} type="text" class="form-control" name="PresentSchoolCalendarMonths" value="{{ getObjectValue($admission,'PresentSchoolCalendarMonths') }}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Reason for Leaving</label>
						<input {{ disabledIfAdmitted() }} type="text" class="form-control ES not-required-default" name="PresentSchoolReasonLeaving" value="{{ getObjectValue($admission,'PresentSchoolReasonLeaving') }}">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Did the applicant pass all subjects during</label>
						<label class="control-label">the last grading period?</label>
						<select {{ disabledIfAdmitted() }} class="form-control" name="PassAllSubjects">
							<option {{ getObjectValue($admission,'PresentSchoolPassAllSubjects') == 'Yes' ? 'selected' : ''  }}>Yes</option>
							<option {{ getObjectValue($admission,'PresentSchoolPassAllSubjects') == 'No' ? 'selected' : ''  }}>No</option>
						</select>
					</div>
					<div class="col-md-8">
						<label class="control-label" style="margin-top: 29px;">Subject/s failing are:</label>
						<input {{ disabledIfAdmitted() }} type="text" class="form-control not-required-default" name="SubjectsFailing" value="{{ getObjectValue($admission,'PresentSchoolSubjectsFailing') }}">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('Admission.Views.admission.tables.schools_attended')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table" id="tableScholastic">
					<tbody>
						@foreach(\App\Modules\Admission\Models\Questionaire::where(['qns_group'=>'SCH-I','is_inactive'=>0])->where('gr2_gr11_sort','<>',0)->orderby('gr2_gr11_sort','asc')->get() as $row)
						<tr data-id="{{$row->id}}" data-question="{{$row->question}}" data-parentid="{{$row->qns_parent_id}}">
							<td style="border-top: unset;">
							<?php 
								$bullet = '&#10687;&nbsp;'; $tab = ''; $adjust = ''; $pull = '';
								if($row->qns_parent_id != 0){
									$bullet = '';
									$tab = str_repeat('&nbsp;', 14);
									$adjust = 'style=width:96%!important;'; 
									$pull = 'pull-right';
								}
							?>
							{{$tab.$bullet.$row->question}}
							@if($row->description != '')
								<br />{{$tab.$row->description}} 
							@endif
							@if(!$row->is_header_only)
								@if($row->type == 'select')
									<select {{ disabledIfAdmitted() }} class="form-control answer {{$pull}}" name="select-{{$row->id}}" {{$adjust}}>
										<?php $options = explode(';',$row->options); ?>
										@foreach($options as $opt)
										<option {{ getAnswerByID($row->id) == $opt ? 'selected' : '' }}>{{$opt}}</option>
										@endforeach
									</select>
								@else
									<input {{ disabledIfAdmitted() }} type="text" class="form-control answer {{$pull}} <?= $row->gr2_gr11_isrequired ? '' : 'not-required'?>" {{$adjust}} name="answer-{{$row->id}}" value="{{ getAnswerByID($row->id) }}">
								@endif
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<legend style="font-size: 15px;font-weight: bold;">Personal History</legend>
		<div class="row">
			<div class="col-md-12">
				<table class="table" id="tableQuestions">
					<tbody>
						@foreach(\App\Modules\Admission\Models\Questionaire::where(['qns_group'=>'P-H','is_inactive'=>0])->where('gr2_gr11_sort','<>',0)->orderby('gr2_gr11_sort','asc')->get() as $row)
						<tr data-id="{{$row->id}}" data-question="{{$row->question}}" data-parentid="{{$row->qns_parent_id}}">
							<td style="border-top: unset;">
							<?php 
								$bullet = '&#10687;&nbsp;'; $tab = ''; $adjust = ''; $pull = '';
								if($row->qns_parent_id != 0){
									$bullet = '';
									$tab = str_repeat('&nbsp;', 14);
									$adjust = 'style=width:96%!important;'; 
									$pull = 'pull-right';
								}
							?>
							{{$tab.$bullet.$row->question}} 
							@if($row->description != '')
								<br />{{$tab.$row->description}} 
							@endif
							@if(!$row->is_header_only)
								@if($row->type == 'select')
									<select {{ disabledIfAdmitted() }} class="form-control answer {{$pull}}" name="select-{{$row->id}}" {{$adjust}}>
										<?php $options = explode(';',$row->options); ?>
										@foreach($options as $opt)
										<option {{ getAnswerByID($row->id) == $opt ? 'selected' : '' }}>{{$opt}}</option>
										@endforeach
									</select>
								@else
									<input {{ disabledIfAdmitted() }} type="text" class="form-control answer {{$pull}} <?= $row->gr2_gr11_isrequired ? '' : 'not-required'?>" {{$adjust}} name="answer-{{$row->id}}" value="{{ getAnswerByID($row->id) }}">
								@endif
							@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<b style="font-style:italic;">{{$tab}}* Please submit a photocopy of your child’s complete evaluation and therapy report.</b>
			</div>
		</div>
		
	</div>
</form>

