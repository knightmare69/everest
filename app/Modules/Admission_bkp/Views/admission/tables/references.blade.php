<?php 
	$data = App\Modules\Admission\Models\References::where('AppNo',decode(Request::get('AppNo')))->get();
	$data = isset($data) ? $data : [];
?>
<table class="table table-bordered" id="tableReferences">
	<thead>
		<tr>
			<th class="right" colspan="5">
				<button {{ disabledIfAdmitted() }} type="button" class="btn btn-primary" id="btnAddReferences">Add new</button>
			</th>
		</tr>
		<tr class="heading">
			<th width="2%">&nbsp;</th>
			<th width="28%"><small>Name</small></th>
			<th width="20%"><small>Relationship</small></th>
			<th width="26%"><small>School</small></th>
			<th width="20%"><small>Phone Number</small></th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr data-id="{{$row->ReferenceID}}">
			<td>
				<button {{ disabledIfAdmitted() }} class="btn btn-sm center red btnReferencesRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="referencesName" class="form-control referencesName" placeholder="Name" value="{{ $row->Name }}">
			</td>
			<td>
				<input {{ disabledIfAdmitted() }}  type="text" name="referencesRelationship" class="form-control referencesRelationship" placeholder="Relationship" value="{{ $row->Relationship }}">
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="referencesSchool" class="form-control referencesSchool" placeholder="School" value="{{ $row->School }}">
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="referencesPhoneNumber" class="form-control referencesPhoneNumber" placeholder="Phone Number" value="{{ $row->PhoneNumber }}">
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<table class="table hide" id="tableDefaultReferences">
	<tbody>
		<tr>
			<td>
				<button class="btn btn-sm center red btnReferencesRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input type="text" name="referencesName" class="form-control referencesName" placeholder="Name">
			</td>
			<td>
				<input type="text" name="referencesRelationship" class="form-control referencesRelationship" placeholder="Relationship">
			</td>
			<td>
				<input type="text" name="referencesSchool" class="form-control referencesSchool" placeholder="School">
			</td>
			<td>
				<input type="text" name="referencesPhoneNumber" class="form-control referencesPhoneNumber" placeholder="Phone Number">
			</td>
		</tr>
	</tbody>
</table>


