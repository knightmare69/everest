<?php $data = isset($data) ? $data : []; ?>
<div class="row">
	<div class="col-md-8" style="padding-left:5px;margin-bottom:10px;">
		<div class="input-group">
			<input type="text" placeholder='Search By Name, Birth Date and Email' id="FamilyFilter" class="form-control" value="{{ Request::get('filter') }}">
			<span class="input-group-addon cursor-pointer btn_search_family">
				<i class="fa fa-search"></i>
			</span>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding-left:5px;padding-right:5px;">
		<div class="table-responsive modal_template_list" style="max-height:400px;width:590px;verflow:scroll;">
			<table class="table table-bordered" id="tableFamilySearch">
				<thead>
					<!-- <tr>
						<td colspan="5">
							<div class="input-group">
								<input type="text" class="form-control" id="FamilyFilter" placeholder="Search By Name, Birth Date and email" value="{{ Request::get('filter') }}">
								<span class="input-group-addon cursor-pointer btn_search_family">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</td>
					</tr> -->
					<tr class="heading">
						<th width="9%" class="center"><i class="fa fa-search"></i></th>
						<th width="30%"><small>Father Information</small></th>
						<th width="30%"><small>Mother Information</small></th>
						<th width="30%"><small>Guardian Information</small></th>
						<!-- <th width="65%"><small>Guardian Name</small></th>
						<th width="20%"><small>Email</small></th>
						<th width="10%"><small>DOB</small></th> -->
					</tr>
				</thead>
				<tbody>
					@foreach($data as $row)
					<tr data-id="{{ encode($row->FamilyID) }}">
						<td>
							<a href="javascript:;" class="btn btn-xs default btn-editable btnFamilySel"><i class="fa fa-check"></i> Select</a>
						</td>
						<td>
							<strong>Name:</strong> {{ $row->Father_Name }}<br/>
							<strong>Bdate:</strong> {{ setDateFormat($row->Father_BirthDate,'yyyy-mm-dd','mm/dd/yyyy') }}<br/>
							<strong>Email:</strong> {{ $row->Father_Email }}
						</td>
						<td>
							<strong>Name:</strong> {{ $row->Mother_Name }}<br/>
							<strong>Bdate:</strong> {{ setDateFormat($row->Mother_BirthDate,'yyyy-mm-dd','mm/dd/yyyy') }}<br/>
							<strong>Email:</strong> {{ $row->Mother_Email }}
						</td>
						<td>
							<strong>Name:</strong> {{ $row->Guardian_Name }}<br/>
							<strong>Bdate:</strong> {{ setDateFormat($row->Guardian_BirthDate,'yyyy-mm-dd','mm/dd/yyyy') }}<br/>
							<strong>Email:</strong> {{ $row->Guardian_Email }}
						</td>
					</tr>
					@endforeach
					@if(count($data) <= 0)
					<tr>
						<td colspan="4" align="center">No Result Found!</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
