<table class="table" id="tableDocuments">
	<thead>
		<tr>
			<td colspan="3" id="tableDocumentsMessage" class="font-red"></td>
		</tr>
		<tr>
			<th width="72%">Name</th>
			<th width="10%">Reviewed?</th>
			<th width="10%">Exempted?</th>
			<th width="10%">Required?</th>
			<th width="5%" class="center"><i class="fa fa-file"></i></th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>


