<?php 
	$sch_attended = App\Modules\Admission\Models\SchoolsAttended::where(['AppNo'=>decode(Request::get('AppNo'))])->get();
	$sch_attended = isset($sch_attended) ? $sch_attended : [];

	$month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	$indent = str_repeat('&nbsp;', 5); 
?>
<table class="table table-bordered" id="tableSchoolsAttended">
	<thead>
		<tr>
			<th class="right" colspan="5">
				<button {{ disabledIfAdmitted() }} type="button" class="btn btn-primary" id="btnAddSchoolAttended">Add new</button>
			</th>
		</tr>
		<tr class="heading">
			<th width="2%">&nbsp;</th>
			<th class="center" width="15%"><small>Name of School/s Attended</small></th>
			<th class="center" width="15%"><small>Address</small></th>
			<th class="center" width="20%"><small>Dates Attended (Month-Year to Month-Year)</small></th>
			<th class="center" width="10%"><small>Grade Level</small></th>
		</tr>
	</thead>
	<tbody>
		@foreach($sch_attended as $row)
		<tr data-id="{{$row->id}}">
			<td class="center">
				<button {{ disabledIfAdmitted() }} type="button" class="btn btn-sm red btnSchoolsAttendedRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="school" class="form-control school" placeholder="School" value="{{ $row->school }}">
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="address" class="form-control address"  placeholder="Address" value="{{ $row->address }}">
			</td>
			<td>
				<div class="row">
					<?php 
						$options = explode('/', $row->fromDate);
						$fromYear = isset($options[1]) ? $options[1] : '';
					?>
					{{$indent}}FROM: <br />
					<div class='col-md-6' style="padding-right: 2px !important">
						<select {{ disabledIfAdmitted() }} class="form-control FromMonthAttended" name="FromMonthAttended">
							<option>--MONTH --</option>
							@foreach($month as $m)
							<option {{ $m == $options[0] ? 'selected="selected"' : '' }}>{{ $m }}</option>
							@endforeach
						</select>
					</div>
					<div class='col-md-6' style="padding-left: 2px !important">
						<select {{ disabledIfAdmitted() }} class="form-control FromYearAttended" name="FromYearAttended">
							<option>--YEAR --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option {{ $fromYear == $year ? 'selected' : '' }}>{{ $year }}</option>
							@endfor
						</select>
					</div>
				</div>
				<div class="row">
					<?php 
						$options = explode('/', $row->toDate); 
						$toYear = isset($options[1]) ? $options[1] : '';
					?>
					{{$indent}}TO: <br />
					<div class='col-md-6' style="padding-right: 2px !important">
						<select {{ disabledIfAdmitted() }} class="form-control ToMonthAttended" name="ToMonthAttended">
							<option>--MONTH --</option>
							@foreach($month as $m)
							<option {{ $m == $options[0] ? 'selected="selected"' : '' }}>{{ $m }}</option>
							@endforeach
						</select>
					</div>
					<div class='col-md-6' style="padding-left: 2px !important">
						<select {{ disabledIfAdmitted() }} class="form-control ToYearAttended" name="ToYearAttended">
							<option>--YEAR --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option {{ $toYear == $year ? 'selected' : '' }}>{{ $year }}</option>
							@endfor
						</select>
					</div>
				</div>
			</td>
			<td>
				<input {{ disabledIfAdmitted() }} type="text" name="GradeLevel" class="form-control GradeLevel" maxlength="30" placeholder="Year Level" value="{{ $row->yearLevel }}">
			</td>
		</tr>
		@endforeach
		@if(count($sch_attended) <= 0)
		<tr>
			<td class="center">
				<button type="button" class="btn btn-sm red btnSchoolsAttendedRemove"><i class="fa fa-times"></i></button>
			</td>
			<td>
				<input type="text" name="school" class="form-control school" placeholder="School">
			</td>
			<td>
				<input type="text" name="address" class="form-control address"  placeholder="Address">
			</td>
			<td>
				<div class="row">
					{{$indent}}FROM: <br />
					<div class='col-md-6' style="padding-right: 2px !important">
						<select class="form-control FromMonthAttended" name="FromMonthAttended">
							<option>--MONTH --</option>
							@foreach($month as $m)
							<option>{{ $m }}</option>
							@endforeach
						</select>
					</div>
					<div class='col-md-6' style="padding-left: 2px !important">
						<select class="form-control FromYearAttended" name="FromYearAttended">
							<option>--YEAR --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option>{{ $year }}</option>
							@endfor
						</select>
					</div>
				</div>
				<div class="row">
					{{$indent}}TO: <br />
					<div class='col-md-6' style="padding-right: 2px !important">
						<select class="form-control ToMonthAttended" name="ToMonthAttended">
							<option>--MONTH --</option>
							@foreach($month as $m)
							<option>{{ $m }}</option>
							@endforeach
						</select>
					</div>
					<div class='col-md-6' style="padding-left: 2px !important">
						<select class="form-control ToYearAttended" name="ToYearAttended">
							<option>--YEAR --</option>
							@for($year = date('Y');$year>=1960;$year--)
							<option>{{ $year }}</option>
							@endfor
						</select>
					</div>
				</div>
			</td>
			<td>
				<input type="text" name="GradeLevel" class="form-control GradeLevel" maxlength="30" placeholder="Year Level">
			</td>
		</tr>
		@endif
	</tbody>
</table>


