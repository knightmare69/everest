<?php
function isVisibleAppType($id) {
	if (in_array($id,AppConfig()->parentAppTypeIDs())) {
		return true;
	}
	return false;
}
?>
<form action="#" class="form-horizontal" id="FormQuick">
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-child"></i> Quick Admission</div>
			<div class="actions btn-set">
				<button class="btn green" type="button" id="BtnSave"><i class="fa fa-check"></i> Save</button>
				<button class="btn blue" type="button" id="BtnSavePrint"><i class="fa fa-check"></i> Save & Print</button>
				<button class="btn yellow" type="button" id="ClearForm"><i class="fa fa-check"></i> Clear</button>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-body">
    					<div class="col-md-6" style="width: 40% !important;padding-right: 0px !important;">
    						<h4 class="form-section">Personal Information</h4>
    						@include($views.'forms.personal')
    					</div>
    					<!--/span-->
    					<div class="col-md-6" style="padding-left: 0px !important;">
    						<h4 class="form-section">General Admission Information</h4>
    						@include($views.'forms.admission')
    					</div>
    					<!--/span-->
					</div>
				</div>
			</div>
		</div>
	</div>
</form>