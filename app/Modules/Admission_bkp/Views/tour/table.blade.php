<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit">#</th>
            <th>Tour Schedule</th>  
            <th>Remarks</th>            
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;  ?>
        @foreach($data as $row)           
        <tr data-id="{{ $row->id }}"  >
            <td class="font-xs autofit bold">{{$i++}}.</td>
            <td class="">{{ setDate($row->Schedule,'m/d/Y H:i A') }}</td>
            <td class="">{{ $row->Remarks }}</td>
            <td>{{ $row->Schedule == $row->ReSchedule ? $status[$row->Status] : $status[$row->Status].' - '.$status[3] }}</td>                   
            <td class="text-center">
                @if(strtoupper($status[$row->Status]) == 'PENDING')
                @if(!isAdmin())
                <a href="javascript:;" class="btn btn-danger btn-sm" onclick="Tour.delete({{$row->id}})">
                    <i class="fa fa-trash"></i>
                </a>
                @endif
                <a href="javascript:;" class="btn btn-success btn-sm" onclick="Tour.edit({{$row->id}})">
                    <i class="{{ !isAdmin() ? 'fa fa-edit' : 'fa fa-eye' }}"></i>
                </a>
                @else
                    Not Available
                @endif
            </td>
        </tr>
        @endforeach
        @if(count($data) <= 0)
        <tr>
            <td colspan="5">No Records available.</td>
        </tr>
        @endif
    </tbody>
</table>