<input type="text" style="display: none;" name="pagescheduletype" id="pagescheduletype" value="{{$schedtype}}">
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-child"></i> Schedule a tour</div>
		<div class="actions btn-set">
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
		    <div class="col-md-12">
		        @include($views.'search-filter')
		    </div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="portlet box blue">
				    <div class="portlet-title">
				        <div class="caption">
				            <i class="fa fa-book"></i> Tour/Interview Groups
				        </div>
				        <div class="actions">
				           <a href="javascript:;" class="btn btn-default" onclick="Batch.new()">Create Group</a>
				        </div>

				    </div>
				    <div class="portlet-body" id="table-batch">
				    	
				    </div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="portlet box blue">
				    <div class="portlet-title">
				        <div class="caption">
				            <i class="fa fa-book"></i> List of Applicants
				        </div>
				        <div class="actions">
				           
				        </div>

				    </div>
				    <div class="portlet-body">
				    	<div class="tabbable-custom">
		                    <ul class="nav nav-tabs">
		                        <li class="active">
		                            <a href="#no-schedule" data-toggle="tab" aria-expanded="true">Requested</a>
		                        </li>
		                        <li><a href="#with-schedule" data-toggle="tab" aria-expanded="true">Confirmed</a></li>
		                    </ul>
		                    <div class="tab-content">
		                        <div class="tab-pane fade active in" id="no-schedule">
		                                
		                        </div>
		                        <div class="tab-pane fade in" id="with-schedule">
		                                
		                        </div>
		                    </div>
		                </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>