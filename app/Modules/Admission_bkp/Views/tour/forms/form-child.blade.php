<center><h3>Child Inquiry Form</h3></center>
<br />
<div class="form-group">
	<div class="row">
		<div class="col-md-4">
			<label class="text-left">Request Type</label>
			<select class="form-control not-required" name='schedule_type' id="schedule_type">
				<option value="">-- CHOOSE --</option>
				<option value="tour">TOUR</option>
				<option value="exam">EXAM</option>
			</select>
		</div>
		<div class="col-md-4">
			<label class="col-md-12 text-left ">Incoming Year Level</label>
		    <select name="yearlevel" id="yearlevel" class="form-control">
		    	<option value="" selected="">-- CHOOSE --</option>
		        @foreach($yearlevel as $row)
		        <option data-progclass="{{$row->ProgClass}}" value="{{$row->YearLevelID}}" >{{$row->YearLevelName}}</option>
		        @endforeach
		    </select>
		</div>
		<div class="col-md-4">
			<label class="control-label">Year Applying for</label>
		    <select name="schoolyear" id="schoolyear" class="form-control">
		    	<option value="" selected="">-- CHOOSE--</option>
		    	@foreach($terms as $row)
		    	<option selected="{{ $row->Active_OnlineEnrolment ? 'selected' : '' }}" value="{{$row->TermID}}" >{{ $row->AcademicYear.' - '.$row->SchoolTerm }}</option>
		    	@endforeach
            </select>
		</div>
	</div>
</div>


<div class="form-group">
	<div class="row">
		<div class="col-md-4">
			<label class="">Last Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lname" value="{{ old('lname') }}"/>
		</div>
		<div class="col-md-4">
			<label class="col-md-12 text-left ">First Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="fname" value=""/>
		</div>
		<div class="col-md-4">
			<label class="col-md-12 text-left ">Middle Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Middle Name" name="mname" value=""/>
		</div>
	</div>
	
</div>

<div class="form-group">
	<div class="col-md-12 text-right">
		<input type="button" class="btn btn-primary" id="tour-btn" onclick="Tour.back();" value="Back">
		<input type="button" class="btn btn-primary" id="tour-btn" onclick="Tour.saveChild();" value="Save">
		<input type="button" class="btn btn-default" onclick="Tour.cancel();" value="Cancel">
	</div>
</div>

