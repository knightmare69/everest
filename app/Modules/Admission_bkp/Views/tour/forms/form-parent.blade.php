<center><h3>Parent Inquiry Form</h3></center>
<br />
@if(!isParent())
<div class="form-group hide">
	<div class="row">
		<div class="col-md-4">
		<label class="text-left">Type</label>
		<select class="form-control not-required" name='type' id="type">
			<option value="">-- CHOOSE --</option>
			<option value="phone">Phone</option>
			<option value="walkin">Walkin</option>
		</select>
		</div>
	</div>
</div>
@endif

<div class="form-group">
	<div class="row">
		<div class="col-md-4">
			<label class="">Last Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lname" value="{{ old('lname') }}"/>
		</div>
		<div class="col-md-4">
			<label class="col-md-12 text-left ">First Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="fname" value=""/>
		</div>
		<div class="col-md-4">
			<label class="col-md-12 text-left ">Middle Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Middle Name" name="mname" value=""/>
		</div>
	</div>
	
</div>
 <div class="form-group">
 	<div class="row">
		<div class="col-md-4">
			<label class="col-md-12 text-left ">Email</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" id="email" value="{{ old('email') }}"/>
		</div>
		<div class="col-md-4">
			<label class="col-md-12 text-left ">Contact Number</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Contact Number" name="contact_no" id="contact_no" value="{{ old('contact_no') }}"/>
		</div>
	</div>
</div>

		
<div class="form-group">
	<div class="row">
		<div class="col-md-12">
			<label class="col-md-12 text-left">Remarks</label>
			<textarea class="form-control not-required" name="remarks" id="remarks" maxlength="300"></textarea>
		</div>
	</div>
</div>


<div class="form-group">
	<div class="col-md-12 text-right">
		<input type="button" class="btn btn-primary" id="tour-btn" onclick="Tour.saveParent();" value="Save">
		<input type="button" class="btn btn-default" onclick="Tour.cancel();" value="Cancel">
	</div>
</div>

