<table class="table table-striped table-bordered table-hover table_scroll" style="cursor: pointer;">
    
    <tbody>
        <?php $i = 1;  ?>
        @foreach($data as $row)           
        <tr data-id="{{ $row->id }}"  >
            <td class="font-xs autofit bold">{{$i++}}.</td>
            <td class="">
                <b>{{ $row->title }}</b>
                <br />
                <small class="font-red">{{ setDate($row->BatchDate,'F d, Y H:i A') }}</small>
            </td>
            <td width="3%"><a href="javascript:;" onclick="Batch.getWithSched({{$row->id}})" class="btn btn-small btn-sm btn-warning"><i title="Click to View/Add Applicants" class="fa fa-eye"></i></a></td>
        </tr>
        @endforeach
        @if(count($data) <= 0)
        <tr>
            <td colspan="5">No Records available.</td>
        </tr>
        @endif
    </tbody>
</table>