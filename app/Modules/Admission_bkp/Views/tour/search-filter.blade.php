<?php //error_print($year_level); die(); ?>
<style media="screen">
    .form-inline > .form-group {
        margin-right: 10px;
    }
</style>
<div class="well well-sm" style="margin-bottom: 5px;">
    <form class="form-inline search-form">
        <div class="form-group">
            <select class="form-control search-selec2" id="schoolyear" placeholder="Select Year Level">
                @if(!empty($yearlevel))
                    <option value="">-- CHOOSE SCHOOL YEAR--</option>
                    @foreach($terms as $row)
                        <option selected="{{ $row->Active_OnlineEnrolment ? 'selected' : '' }}" value="{{$row->TermID}}">{{ $row->SchoolTerm.' - '.$row->AcademicYear }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
            <select class="form-control search-selec2" id="yearlevel" placeholder="Select Year Level">
                @if(!empty($yearlevel))
                    <option value="">-- CHOOSE YEAR LEVEL --</option>
                    @foreach($yearlevel as $yl)
                        <option value="{{$yl->YearLevelID}}">{{ $yl->YearLevelName }}</option>
                    @endforeach
                @else
                <option value="">Nothing to show.</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-primary btn-sm" onclick="Batch.mainSearch()"><i class="fa fa-search"></i> Search</button>
        </div>
    </form>
</div>
