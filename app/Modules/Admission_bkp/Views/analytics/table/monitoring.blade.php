<table class="table  table-bordered ">
<thead>
<tr>
	<th>App. No</th>
	<th>Name</th>
	<th>Level</th>
	<th>Status</th>
</tr>

</thead>
<tbody>
<?php 
    if(isset($termid)){
	 $termid = $termid;
	}else{
	  $tmpterm = DB::SELECT("SELECT TOP 1 * FROM ES_AYTerm WHERE Active_OnlineEnrolment=1");
	  $termid  = (($tmpterm)?($tmpterm[0]->TermID):1);
	}	
	
	$where = '';
	if(isset($limit)){
	  $limit = $limit;
	}else
	  $limit = 0;
	
	if(isset($status) && $status>0){
	 $where.=" AND a.StatusID='".$status."'";
	}
	
	if(isset($filter) && trim($filter)!=''){
	 $filter = strip_tags($filter); 
	 $where .= " AND (a.LastName LIKE '%".$filter."%' OR a.FirstName LIKE '%".$filter."%' OR a.MiddleName LIKE '%".$filter."%' OR y.YearLevelName LIKE '%".$filter."%')";
	}
	
	$applicant = DB::select("SELECT ".(($limit>0)?('TOP '.$limit):'')." s.StatusDesc,y.YearLevelName,a.* 
							FROM ESv2_Admission as a 
					  INNER JOIN ESv2_AdmissionStatus as s ON a.StatusID=s.StatusID 
					  INNER JOIN ESv2_YearLevel as y ON ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))
					  WHERE a.TermID='".$termid."' ".$where."
					  ORDER BY y.YearLevelID,a.AppNo ASC");
	foreach($applicant as $r){
	 echo '<tr>
			<td>'.$r->AppNo.'</td>
			<td>'.$r->LastName.', '.$r->FirstName.'</td>
			<td>'.$r->YearLevelName.'</td>
			<td>'.$r->StatusDesc.'</td>
		   </tr>';
	}				  
?>	
</tbody>
<tfoot></tfoot>
</table>