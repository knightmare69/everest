<table class="table  table-bordered ">
<thead>
<tr>
	<th class="text-center" rowspan="2">Level</th>
	<th class="text-center" colspan="7">Admission Steps</th>
</tr>
<tr>	
	<th>Inquiried</th>
	<th>Applied</th>
	<th>Tested</th>
	<th>Waitlisted</th>
	<th>Denied</th>
	<th>Admitted</th>
	<th>Enrolled</th>
</tr>
</thead>
<tbody>
<?php 
  $xmonth   = ((isset($xmonth))?$xmonth:'-1');
  $logrange = "";
  $apprange = "";
  $regrange = "";
  if(isset($termid)){
	$termid = $termid;
  }else{
	$tmpterm = DB::SELECT("SELECT TOP 1 * FROM ES_AYTerm WHERE Active_OnlineEnrolment=1");
	$termid  = (($tmpterm)?($tmpterm[0]->TermID):1);
  }
  
  if($xmonth>0){
    $ayterm   = DB::SELECT("SELECT TOP 1 * FROM ES_AYTerm WHERE TermID='".$termid."'");
	$year     = substr($ayterm[0]->AcademicYear,0,4);
    $lastday  = date('t',strtotime($year.'-'.(($xmonth<10)?('0'.$xmonth):$xmonth).'-01'));
    $logrange = " AND CreatedDate<='".$year.'-'.(($xmonth<10)?('0'.$xmonth):$xmonth).'-'.$lastday.' 23:59:59'."'";
    $apprange = " AND AppDate<='".$year.'-'.(($xmonth<10)?('0'.$xmonth):$xmonth).'-'.$lastday.' 23:59:59'."'";
    $regrange = " AND RegDate<='".$year.'-'.(($xmonth<10)?('0'.$xmonth):$xmonth).'-'.$lastday.' 23:59:59'."'";
  }
  
  $rsnew = DB::select("SELECT (SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' ".$apprange." AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplicant
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' ".$apprange." AND StatusID IN (8,1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalInquired
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' ".$apprange." AND StatusID IN (1,2,3,10,14) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplied
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' AND (StatusID=10 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=10 ".$logrange.")) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalTested
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' AND (StatusID=14 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=14 ".$logrange.")) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalWaiting
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' AND (StatusID=2 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=2 ".$logrange.")) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalAdmitted
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' AND (StatusID=3 OR AppNo IN (SELECT AppNo FROM ESV2_AdmissionStatusRemarks WHERE StatusID=3 ".$logrange.")) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7 AND y.ProgID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalDenied
						  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID='".$termid."' AND AppNo IN (SELECT AppNo FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID='".$termid."' ".$regrange.") AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalEnrolled
						  ,YearLevelCode   
						  ,YearLevelName
				   FROM ESv2_YearLevel as y
				   ORDER BY y.YearLevelID");
   $i  = 0;				   
   $ap = 0;				   
   $t  = 0;
   $tx = 0;   
   $w  = 0;				   
   $d  = 0;				   
   $ad = 0;				   
   $en = 0;				   
   foreach($rsnew as $r){
	echo '<tr>
			<td>'.$r->YearLevelName.'</td>
			<td>'.$r->TotalInquired.'</td>
			<td>'.$r->TotalApplied.'</td>
			<td>'.$r->TotalTested.'</td>
			<td>'.$r->TotalWaiting.'</td>
			<td>'.$r->TotalDenied.'</td>
			<td>'.$r->TotalAdmitted.'</td>
			<td>'.$r->TotalEnrolled.'</td>
		  </tr>';
	$i +=$r->TotalInquired;				   
    $ap+=$r->TotalApplied;				   
    $t +=$r->TotalTested;			
    $tx+=($r->TotalWaiting + $r->TotalDenied + $r->TotalAdmitted);	//tested formula from everest
    $w +=$r->TotalWaiting;				   
    $d +=$r->TotalDenied;				   
    $ad+=$r->TotalAdmitted;				   
    $en+=$r->TotalEnrolled;		  
   }				   
?>
</tbody>
<tfoot>
  <?php echo '<tr><td><strong>Total</strong></td>
                  <td><strong>'.$i.'</strong></td>
                  <td><strong>'.$ap.'</strong></td>
                  <td><strong>'.$t.'</strong></td>
                  <td><strong>'.$w.'</strong></td>
                  <td><strong>'.$d.'</strong></td>
                  <td><strong>'.$ad.'</strong></td>
                  <td><strong>'.$en.'</strong></td></tr>';?>
</tfoot>
</table>