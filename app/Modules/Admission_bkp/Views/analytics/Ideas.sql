/* TOTAL NO OF NEW Students*/
SELECT (SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplicant
      ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND StatusID=8 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalInquiried
      ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND StatusID=1 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalApplied
      ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND StatusID=10 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalTested
      ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND StatusID=14 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalWaiting
      ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND StatusID=2 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalAdmitted
      ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND StatusID=3 AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalDenied
	  ,(SELECT COUNT(*) FROM ESv2_Admission WHERE TermID=1001 AND AppNo IN (SELECT AppNo FROM ES_Students WHERE TermID=1001) AND ((GradeLevelID=y.YearLevelID AND ProgramID<>7) OR (GradeLevelID=y.YLID_OldValue AND ProgramID=y.ProgID))) as TotalEnrolled
	  ,YearLevelCode   
	  ,YearLevelName
  FROM ESv2_YearLevel as y
ORDER BY y.YearLevelID
/* TOTAL NO OF Students */  
SELECT (SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID=1001 AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalStudent
      ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID=1001 AND s.TermID=1001 AND s.Gender='M' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewMale
      ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID=1001 AND s.TermID=1001 AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalNewFemale
      ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID=1001 AND ISNULL(s.TermID,1)<1001 AND s.Gender='M' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldMale
      ,(SELECT COUNT(r.RegID) FROM ES_Students as s INNER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo WHERE r.TermID=1001 AND ISNULL(s.TermID,1)<1001 AND s.Gender='F' AND ((r.YearLevelID=y.YearLevelID AND r.ProgID<>7) OR (r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID))) as TotalOldFemale
      ,YearLevelCode   
	  ,YearLevelName
  FROM ESv2_YearLevel as y
  
  
-- Consolidated Information
-- Parent
SELECT fb.* 
  FROM ESv2_Admission as a
INNER JOIN ESv2_Admission_FamilyBackground as fb ON a.FamilyID=fb.FamilyID
WHERE Father_Name<>'' OR Mother_Name<>'' OR Guardian_Name<>''

-- Religion
SELECT r.Religion,r.ShortName,COUNT(a.AppNo) as Items FROM ESv2_Admission as a INNER JOIN ES_Religions as r ON a.ReligionID=r.ReligionID GROUP BY r.Religion,r.ShortName

-- Nationality
SELECT n.NationalityID,n.Nationality,n.ShortName, COUNT(a.AppNo) as Items FROM ESv2_Admission as a INNER JOIN Nationalities as n ON a.NationalityID=n.NationalityID GROUP BY n.NationalityID,n.Nationality,n.ShortName  

--
SELECT DISTINCT BaptizedIn_Church,BaptizedIn_City FROM ESv2_Admission WHERE BaptizedIn_Church IS NOT NULL AND BaptizedIn_Church<>''