<?php $data = App\Modules\Admission\Models\GuardianBackround::where('FamilyID',getGuardianFamilyID())->get(); ?>	
<?php $family_info = isset($data[0]) ? $data[0] : [];  ?>
<div class="row">
    <div class="col-md-12">
        @include('errors.event')
    </div>
</div>
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption"></div>
		<div class="tools"></div>
	</div>
	<div class="portlet-body form">
			<div class="tabbable-line nav-tabs-sub">
                <ul class="nav nav-tabs pull-left">
                    <li class="active" data-target="Guardian">
                        <a href="#guardian-tab" data-toggle="tab">Parent Info</a>
                    </li>
                    <li data-target="Father">
                        <a href="#father-tab" data-toggle="tab">Father</a>
                    </li>
                    <li data-target="Mother">
                        <a href="#mother-tab" data-toggle="tab">Mother</a>
                    </li>
                </ul>
				<br/><br/>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="guardian-tab">
		            @include($views.'forms.guardian_profile')
					</div>
                    <div class="tab-pane" id="father-tab">
					<form action="#" class="form-horizontal" id="formFather">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
		                @include('Students.views.student-profile.family-info.father-form')
					<div class="form-actions hidden">
						<div class="row">
							<div class="col-md-9">
								<div class="row">
								    <div class="col-md-12">
									   <label class="control-label col-md-9"><input id="agreeb" type="checkbox"> I/We hereby affirm that all information supplied is complete and accurate.  I/We also hereby allow Everest Academy Manila to collect, use, and process the above mentioned information for legitimate purposes specifically for enrollment and allow authorized personnel to process such information as contained in the Data Privacy Policy of Everest Academy Manila.</label>
									</div>
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green" id="submitFather">Save</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
					</form>
					</div>
                    <div class="tab-pane" id="mother-tab">
					<form action="#" class="form-horizontal" id="formMother">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						@include('Students.views.student-profile.family-info.mother-form')
					<div class="form-actions hidden">
						<div class="row">
							<div class="col-md-9">
								<div class="row">
								    <div class="col-md-12">
									   <label class="control-label col-md-9"><input id="agreec" type="checkbox"> I/We hereby affirm that all information supplied is complete and accurate.  I/We also hereby allow Everest Academy Manila to collect, use, and process the above mentioned information for legitimate purposes specifically for enrollment and allow authorized personnel to process such information as contained in the Data Privacy Policy of Everest Academy Manila.</label>
									</div>
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green" id="submitMother">Save</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
					</form>	
					</div>
				</div>
                <div class="row"><br/><br/>
					<div class="col-md-12">
					   <label class="control-label col-md-12"><input id="dataprivacy" type="checkbox"> I/We hereby affirm that all information supplied is complete and accurate.  I/We also hereby allow Everest Academy Manila to collect, use, and process the above mentioned information for legitimate purposes specifically for enrollment and allow authorized personnel to process such information as contained in the Data Privacy Policy of Everest Academy Manila.</label>
					</div>
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn green" id="submitForm">Save</button>
					</div>
                </div>				
		    </div>		
		<!-- END FORM-->
	</div>
</div>
