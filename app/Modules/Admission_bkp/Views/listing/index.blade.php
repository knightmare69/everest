<style>
	.dataTables_filter,.dataTables_length {
		/*display: none !important;*/
	}
	.table-scrollable{
		min-height:320px !important;
	}
</style>
<?php
$admissionstat = new App\Modules\Admission\Models\AdmissionStatus;

?>
<div class="row">
   	<div class="col-md-12 table_wrapper">
   		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold">Application Lists</span>
           			<span class="caption-helper">&nbsp;This module allows you to manage applicants...</span>
				</div>
				<div class="actions">
					<!-- <div class="btn-group">
						<a class="btn btn-sm btn-default" href="#" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-user"></i> Status <i class="fa fa-angle-down "></i>
						</a>
						<ul class="dropdown-menu pull-right" >
                            @foreach($admissionstat->get() as $row)
                            <li class="">
								<a href="javascript:void(0);" class="admstatus" onclick="Listing.updateStatus('{{ encode($row->StatusID) }}');" data-val="{{ encode($row->StatusID) }}">
								<i class="fa fa-pencil"></i> {{ $row->StatusDesc }} </a>
							</li>                            
						    @endforeach
                        
						</ul>
					</div> --> 
					<div class="btn-group {{ getUserGroup() == 'parent' ? 'hidden' : '' }}">
						<a class="btn btn-default btn-circle" href="javascript:;" data-toggle="dropdown">
						  <i class="fa fa-share"></i> Tools <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li><a href="javascript:;">Export to Excel </a></li>
							<li><a href="javascript:;">Export to CSV </a></li>
							<li><a href="javascript:;">Export to XML </a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container" id="tableApplicationsWrapper">
		   			@include($views.'tables.applicants')
		   		</div>
			</div>
		</div>
	</div>
</div>
@include($views.'modals.modal_email')
