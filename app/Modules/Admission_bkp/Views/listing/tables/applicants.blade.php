<style>
	.dataTables_filter label,.dataTables_filter .form-control {
		width: 100% !important;
	}
	.field-filter {
		margin: 18% 0 0 !important;
	}
</style>
<?php
	function isEnrollNow($status) {
		$setStatus = ['Passed','Conditional'];
		if (in_array(ucfirst($status), $setStatus)) {
			return true;
		}
		return false;
	};

    function isCurrentYrSept(){
    	$response = false;
    	$date = explode('-', systemDate());
    	if ($date[1] == 9){
    		$response = true;
    	}
    	return $response;
    };

    function getNextLastSYTermID(){
    	$nxtTermID = 0; 
    	$lastTermID = 0;
    	$count = count(App\Modules\Setup\Models\AcademicYearTerm::appDrpSY());
    	$date = explode('-', systemDate());
    	$i=0;
    	foreach(App\Modules\Setup\Models\AcademicYearTerm::appDrpSY() as $row) {
    		$i++;
    		$start = explode('-', $row->StartofAY);
    		if(($date[0]+1) == $start[0]){
    			$nxtTermID = $row->TermID;
    		}
    		if($count == $i){
    			$lastTermID = $row->TermID;
    		}
    	}
    	return explode('-', $nxtTermID.'-'.$lastTermID);
    };
?>
<form id="FormTableApplicants" method='POST' class="hide">
	<div class="form-group">
		<label class="control-label"><p class="field-filter">Is Higher Level</p></label>
			<input type="checkbox" class="form-control" id="IsHigherLevel">
	</div>	
</form>
<table class="table table-bordered {{ isParent() ? 'hidden' : '' }}" id="tableFilter">
     <thead>
	     <tr role="row" class="filter">
	     	<td colspan="2">
				<label class="control-label col-md-4" style="width: 25%;margin-top: 4px;"><p class="field-filter">Status</p></label>
				<div class="col-md-8" style="padding-right: 0 !important">
					<select class="form-control form-filter" name="admissionStatus" id="admissionStatus">
						@foreach($admissionstat->get() as $row)
						<option {{ encode(8) == encode($row->StatusID) ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->StatusDesc }}</option>                       
						@endforeach
					</select>
				</div>
			</td>
			<td colspan="3" class='hide'>
				<label class="control-label col-md-4"><p class="field-filter">Campus</p></label>
				<div class="col-md-8" style="width: 65% !important;padding-right: 0 !important">
					<select class="form-control form-filter" name="schoolCampus" id="schoolCampus">
						<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{  Request::get('schoolCampus') ? Request::get('schoolCampus') == encode($row->CampusID) ? 'selected' : '' : 'selected' }} value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
					</select>
				</div>
			</td>
			<td colspan="5">
				<label class="control-label col-md-2" style="width: 5%;margin-top: 8px;"><p class="field-filter">SY</p></label>
				<div class="col-md-5" style="padding-right: 0 !important">
					<?php $i = 0; ?>
					<select class="form-control form-filter" name="schoolYear" id="schoolYear">
    					@foreach(App\Modules\Setup\Models\AcademicYearTerm::appDrpSY() as $row)
    						@if(isCurrentYrSept() && getNextLastSYTermID()[1] != $row->TermID)
								<option {{ (getNextLastSYTermID()[0] == $row->TermID ? 'selected' : '' ) }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear . ' ' . $row->SchoolTerm }}</option>
    						@endif
    						@if(!isCurrentYrSept())
    							<option  {{ (($row->Active_OnlineEnrolment==1) ? 'selected' : '') }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear . ' ' . $row->SchoolTerm }}</option>
    						@endif
    					@endforeach
					</select>
				</div>
				<div class="col-md-5">
					<select class="form-control form-filter" name="gradeLevel" id="gradeLevel">
						<option value="0-0">All Grade Levels</option>
						<option value="0-1">All Year Levels</option>
						@foreach(App\Modules\Admission\Models\Admission::getYearLevelList() as $row)
						<option {{  Request::get('gradeLevel') == encode($row->YearLevelID) ? 'selected' : ''  }} value="{{ encode($row->YearLevelID).'-'.($row->ProgClass < 50 ? 0 : 1) }}">{{ $row->YearLevelName }}</option>
						@endforeach
					</select>
				</div>
			</td>
			<td colspan="4">
				<label class="control-label col-md-3" style="width: 20%;margin-top: 4px;"><p class="field-filter">Filter</p></label>
				<div class="col-md-8" style="padding-right: 0 !important;padding-left: 0 !important">
					<input type="text" class="form-control form-filter" id="filter" name="filter" placeholder="By App No, Applicant Name" value="{{ Request::get('filter') }}">
				</div>
			</td>
			<td>
				<button class="btn btn-sm yellow filter-submit margin-bottom" id="btnFilter" style="margin-top: 4px;"><i class="fa fa-search"></i></button>
				<button class="btn btn-sm red filter-cancel" style="margin-top: 4px;"><i class="fa fa-times"></i></button>
			</td>
		</tr>
	 </thead>
</table>
<table class="table table-striped table-bordered table-hover" id="tableApplicants">
	<thead>
	     <tr role="row" class="filter hidden">
	     	<td>&nbsp;</td>
	     	<td colspan="2">
				<label class="control-label col-md-4" style="width: 25%;margin-top: 4px;"><p class="field-filter">Status</p></label>
				<div class="col-md-8" style="padding-right: 0 !important">
					<select class="form-control form-filter" name="admissionStatus" id="admissionStatus">
						@foreach($admissionstat->get() as $row)
						<option {{ encode(8) == encode($row->StatusID) ? 'selected' : '' }} value="{{ encode($row->StatusID) }}">{{ $row->StatusDesc }}</option>                       
						@endforeach
					</select>
				</div>
			</td>
			<td colspan="3">
				<label class="control-label col-md-4"><p class="field-filter">Campus</p></label>
				<div class="col-md-8" style="width: 65% !important;padding-right: 0 !important">
					<select class="form-control form-filter" name="schoolCampus" id="schoolCampus">
						<option value="">Select...</option>
							@foreach(App\Modules\Admission\Models\Admission::getCampus() as $row)
							<option {{  Request::get('schoolCampus') ? Request::get('schoolCampus') == encode($row->CampusID) ? 'selected' : '' : 'selected' }} value="{{ encode($row->CampusID) }}">{{ $row->Code }}</option>
							@endforeach
					</select>
				</div>
			</td>
			<td colspan="6">
				<label class="control-label col-md-1"><p class="field-filter">SY:</p></label>
				<div class="col-md-5" style="padding-right: 0 !important">
					<?php $i = 0; ?>
					<select class="form-control form-filter" name="schoolYear" id="schoolYear">
    					@foreach(App\Modules\Setup\Models\AcademicYearTerm::appDrpSY() as $row)
    						@if(isCurrentYrSept() && getNextLastSYTermID()[1] != $row->TermID)
								<option {{ (getNextLastSYTermID()[0] == $row->TermID ? 'selected' : '' ) }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear . ' ' . $row->SchoolTerm }}</option>
    						@endif
    						@if(!isCurrentYrSept())
    							<option  {{ (($row->Active_OnlineEnrolment==1) ? 'selected' : '') }} value="{{ encode($row->TermID) }}">{{ $row->AcademicYear . ' ' . $row->SchoolTerm }}</option>
    						@endif
    					@endforeach
					</select>
				</div>
				
				<div class="col-md-5">
					<select class="form-control form-filter" name="gradeLevel" id="gradeLevel">
						<option value="0-0">All Grade Levels</option>
						<option value="0-1">All Year Levels</option>
						@foreach(App\Modules\Admission\Models\Admission::getYearLevelList() as $row)
						<option {{  Request::get('gradeLevel') == encode($row->YearLevelID) ? 'selected' : ''  }} value="{{ encode($row->YearLevelID).'-'.($row->ProgClass < 50 ? 0 : 1) }}">{{ $row->YearLevelName }}</option>
						@endforeach
					</select>
				</div>
			</td>
			<td colspan="3">
				<label class="control-label col-md-2"><p class="field-filter">Filter</p></label>
				<div class="col-md-10" style="padding-right: 0 !important;padding-left: 0 !important">
					<input type="text" class="form-control form-filter" id="filter" name="filter" placeholder="By AppNo,Name" value="{{ Request::get('filter') }}">
				</div>
			</td>
			<td>
				<button class="btn btn-sm yellow filter-submit margin-bottom" id="btnFilter"><i class="fa fa-search"></i></button>
				<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i></button>
			</td>
		</tr>
		<tr class="heading">
			<th>
				 &nbsp;
			</th>
			<th class="center">
				 <small>Option</small>
			</th>
			<th style="text-align: center !important;">
				 <small>Status</small>
			</th>
			<th class="notParentFields">
				 <small>Status Result</small>
			</th>
			<th>
				 <small>App No{{ str_repeat('&nbsp;', 5) }}</small>
			</th>
			<th>
				 <small>App Date{{ str_repeat('&nbsp;', 5) }}</small>
			</th>
			<th>
				 <small>Type</small>
			</th>
			<th>
				 <small>Full Name{{ str_repeat('&nbsp;', 27) }}</small>
			</th>
			<th class="center">
				 <small>Gender</small>
			</th>
			<th>
				 <small>Birthdate{{ str_repeat('&nbsp;', 5) }}</small>
			</th>
			<th>
				 <small>Year Level</small>
			</th>
			
			<th class="hide">
				 <small>Testing Date</small>
			</th>
			<th class="hide">
				 <small>Test Results</small>
			</th>
			<th class="hide">
				 <small>Medical Result</small>
			</th>
			<th class="notParentFields">
				 <small>Payment Status</small>
			</th>
			<th class="notParentFields">
				 <small>Initial Interviewed</small>
			</th>
		</tr>
	<thead>
	<tbody>
	</tbody>
</table>