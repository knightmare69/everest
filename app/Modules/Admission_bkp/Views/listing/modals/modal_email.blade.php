<div class="modal fade modal_email" id="modal_email" tabindex="-1" style="z-index: 9998 !important;" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color:#555555;"> 
				<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
				<h5 class="modal-title" style="font-weight:bold;color:white;">New Message</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<form method="post" class="form-horizontal">
							<div class="col-md-12">
						       	<div class="form-group">
							        <label class="control-label"> To:</label>
							        <select multiple name="RecepientsEmail" id="RecepientsEmail" class="select2 no-border-color form-control">
							       		
							        </select>
						        </div>
						    </div>
						   <!--  <div class="col-md-12">
						       	<div class="form-group">
							        <label class="control-label"> Bcc:</label>
							        <select multiple name="CcEmail" id="CcEmail" class="select2 no-border-color form-control CcEmail">

							        </select>
						        </div>
						    </div> -->
						     <div class="col-md-12">
						        <div class="form-group">
						            <label class="control-label">Subject</label>
						            <input type="text" name="sendSubject" id="sendSubject" class="form-control no-border-color" placeholder="Subject" value="">
						        </div>
						    </div>
						</form>
					 </div>
					<div class="col-md-12">
				   		<div class="portlet box grey-steel">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-envelope"></i>
           							<span class="caption-helper bold">&nbsp;Email Content</span>
           							<span class="badge email_message"></span>
								</div>
								<div class="tools">
									<button type="button" class="btn red-intense btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
									<button type="button" class="btn blue btn-sm btn_send_email"><i class="fa fa-envelope"></i> Send</button>
								</div>
							</div>
							<div class="portlet-body form">
								<form class="form-horizontal form-bordered">
									<div class="form-body">
										<div class="form-group">
											<div class="col-md-12">
												<div name="summernote" id="summernote_1">
												</div>
												<input type="text" name="mail_body" id="mail_body" class="hide" value="">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer hide">
				<button type="button" class="btn default btn-sm" data-dismiss="modal">Close</button>
				<button type="button" class="btn blue btn-sm btn_send_email"><i class="fa fa-envelope"></i> Send</button>
			</div>
		</div>
	</div>
</div>
