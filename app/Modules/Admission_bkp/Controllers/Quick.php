<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Services\AdmissionServiceProvider as Services;
use App\Modules\Setup\Models\City;
use App\Modules\Admission\Services\Admission\Repository;
use Request;
use Response;
use Permission;
use DB;
use CrystalReport;

class Quick extends Controller{

	private $media =
		[
			'Title'=> 'Admission : Staff Module',
			'Description'=> 'Use this module to create applicant profile',
			'js'		=> [
				'Admission/quick',
				'Admission/admissionData',
				'Admission/helper'
			],
			'init'		=> ['Quick.init()','FN.datePicker()','FN.multipleSelect()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all'
			]
		];

	private $url = [ 'page' => 'admission/quick/' ];

	private $views = 'Admission.Views.quick.';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{	
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'saveQuick':
					$result = $this->services->saveQuickAdmission(Request::all());
					if (!$result['error']) {
						$response = [
							'error'=>false,
							'AppNo' => $result['AppNo'],
						];
					} else {
						$response = ['error'=>true,'message'=>$result['error']];
					}
				break;
				case 'showGradeLevelProgram':
					$response = Response::json($this->services->showProgram(decode(Request::get('key')),Request::get('class')));
				break;
				case 'ShowExamScheduleDates':
					$response = ['error'=>false,'data'=>$this->services->showExamSchedDates(Request::all())];
				break;
				case 'showModalFamilSearch':
					$response = ['error'=>false,'content'=>view($this->views.'tables.family_search')->render()];
				break;
				case 'searchFamily':
					$response = ['error'=>false,'content'=>view($this->views.'tables.family_search',['data'=>$this->services->familySearch(Request::all())])->render()];
				break;
				case 'getYearLevel':
					$response = $this->services->getYearLevel(Request::get('type'));
				break;
				case 'getCourses':
					$response = $this->services->getCourses();
				break;

			}
		}
		return $response;
	}

	public function printPDF()
	{
		$CrystalReport = new CrystalReport;

		$CrystalReport->filename = 'admission_exampermit.rpt';

		$CrystalReport->query = "EXEC dbo.ES_rptAdmissionExaminationPermit '400454', 'San Beda College Alabang', '', 'Benavidez St., Sta. Cruz, Manila', 'Alabang Campus', 'admin'";

		$file = $CrystalReport->generate();

		header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $file['filename'] . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file['file']);
        unlink($file['file']);
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-quick');
 		$this->guardian = new GuardianBackround;
 		$this->services = new Services;
 		$this->city = new City;

 	}
}
