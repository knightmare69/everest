<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Security\Models\Users\User;
use App\Modules\Admission\Models\Panels as Model;

use Request;
use Response;
use Permission;
use DB;
use CrystalReport;

class Panels extends Controller {

	private $media =
		[
			'Title'=> 'Admission : Panels',
			'Description'=> 'Use this module to create admission panels.',
			'js'		=> [
				'Admission/panels'
			],
			'init'		=> ['Panels.init()','FN.datePicker()','FN.multipleSelect()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all'
			]
		];

	private $url = [ 'page' => 'admission/panels/' ];

	private $views = 'Admission.Views.panels.';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{	
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'search-user':
					$response = $this->searchUser();
				break;
				case 'save':
					$response = $this->save() ? successSave() : errorSave();
				break;
				case 'update':
					$response = $this->update(Request::get('id')) ? successSave() : errorSave();
				break;
				case 'delete':
					$response = $this->delete(Request::get('id')) ? successDelete() : errorSave();
				break;
				case 'edit':
					$response = ['data' => $this->edit(Request::get('id'))];
				break;
			}
		}
		return $response;
	}

	public function searchUser() {
		return User::select(['FullName','UserIDX'])
					->whereRaw("FullName like '%".Request::get('term')."%'")
					->limit(100)
						->get();
	}

	public function get() {
		$data = Model::data()
					->select(['id','FullName','AcademicYear','SchoolTerm','p.Level','p.Title'])
						->get();
		return ['result' => view($this->views.'table',['data' => $data])->render()];
	}

	public function edit($id) {
		return Model::data()
				->select(['p.UserID','p.TermID','p.Level','p.Title','FullName'])
				->where('id',$id)
					->first();
	}

	private function save() {
		return Model::insert(assertCreated([
			'Title' => Request::get('title'),
			'UserID' => Request::get('panel'),
			'Level' => Request::get('level'),
			'TermID' => Request::get('schoolyear')
		]));
	}

	private function update($id) {
		$id = (int)$id;
		return
			Model::where('id',$id)
				->update(assertModified([
					'Title' => Request::get('title'),
					'UserID' => Request::get('panel'),
					'Level' => Request::get('level'),
					'TermID' => Request::get('schoolyear')
				]));
	}

	private function delete($id) {
		$id = (int)$id;
		return Model::where('id',$id)->delete();
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-quick');
 	}
}
