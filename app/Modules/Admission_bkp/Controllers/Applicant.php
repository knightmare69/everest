<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;

use App\Modules\Security\Models\Users\User as UserModel;
use App\Modules\Security\Services\UserServiceProvider;

use App\Modules\Admission\Services\InquiryService;
use App\Modules\Admission\Services\Register\Validation as Validate;
use App\Modules\Admission\Models\InquiryTemp;
use App\Modules\Admission\Models\InquiryParent;
use App\Modules\Admission\Models\InquiryChild;

use Request;
use Response;
use Permission;
use DB;
use Mail;
use Config;

class Applicant extends Controller{

    use InquiryService;

	private $media = [ 'Title'=> 'Admission Applicant',
            'Description'=> 'Student admission module',
            'js'        => ['admission/applicant'],
            'init'      => [],
            'plugin_js' => [
                'bootbox/bootbox.min',
                'jquery-validation/js/jquery.validate.min',
                'bootstrap-datepicker/js/bootstrap-datepicker',
                'select2/select2.min',
                'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
            ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'bootstrap-select/bootstrap-select.min',
                'select2/select2',
                'jquery-multi-select/css/multi-select',
                'wizzard-tab/css/gsdk-base',
                'bootstrap-fileinput/bootstrap-fileinput',
                'icheck/skins/all'
            ]
        ];

    private $url = [ 'page' => 'inquiry/' ];

    private $views = 'Admission.Views.applicant.';

    private $type = 'email';

    function __construct(){
        $this->initializer();
    }

    public function index(){
	  return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));    
    }
	
	public function initializer(){
	  return false;
	}
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax()){
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event')){
			  case 'get_program':
			     $dept    = Request::get('progclass');
				 $choices = DB::select("");
			  break;
			}
	    }
        return $response;	
	}
}	
?>	