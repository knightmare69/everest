<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Services\AdmissionServiceProvider as Services;
use App\Modules\Admission\Models\AdmissionReservation_model As mReservation;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Registrar\Models\TermConfig as term_model;
use App\Modules\Admission\Models\GuardianBackround;
use Request;
use Response;
use Permission;
use DB;
use CrystalReport;

class Reservation extends Controller{

	private $media =
		[
			'Title'=> 'Admission Reservation',
			'Description'=> 'Admission Reservation!',
			'js'		=> ['Admission/reservation'],
			'init'		=> ['MOD.init()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'icheck/icheck',
                'SmartNotification/SmartNotification.min'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',				
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all',
                'SmartNotification/SmartNotification',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
			]
		];

	private $url = [ 'page' => 'admission/reservation' ];

	private $views = 'Admission.Views.reservation.';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{	
 		$this->initializer();
        if ($this->permission->has('read')) {
                
            $mterm = new term_model;
            
            $term = decode(Request::get('t'));
            
            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),   
                'data' => $this->getlist($term),      
                'term' => $term,       
            ];        
            
            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
            
 	}
    
    private function getlist($term){
        $mReserv = new mReservation();
        return $mReserv->selectRaw("*, dbo.fn_StudentName(IDNo) As StudentName, dbo.fn_StudentYearLevel_k12(IDNo) As YearLevel ")
                            ->where('TermID', $term )
                            ->orderBy('StudentName','asc')
                            ->get();
                            
    }
    
 	function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
			    case 'set':
                    
                    if(Request::get('name') == 'track')
                    {
                        $data = ['TrackID' =>  Request::get('val')];    
                        $where = ['EntryID' => decode(Request::get('id'))];
                        $res = $this->model->where($where)->update($data);
                    }
                    
                    $response = successUpdate();
                                        
                break;
				 case 'get-students':
                    if($this->permission->has('read')){
                        $model = new mStudents();
                        
                        $find = Request::get('find');
                        
                        if( $find == ''){
                            $find = date('Y').'%';
                        }
                        
                        // if(!empty($find)){
                            //$term_id = decode(Request::get('academic-term'));
                            //$prog_id = decode(Request::get('programs'));
                            //$yl_id = decode(Request::get('year-level'));
                            
                            $data = $model->studentsSearch($find,'50');

                            if(!empty($data)){
                                $vw  = view($this->views.'students', ['data' => $data])->render();
                                $response = ['error' => false, 'data' => $vw];

                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                    }
                    break;
                    
				case 'save':
                    
                    $term = decode(Request::get('term'));
					$result = $this->services->saveReservation();
				
                	if (!$result['error']) 
                    {
					   $data = $this->getlist($term);
                       $vw  = view($this->views.'list', ['data' => $data])->render();
                       $response = [ 'error'=>false, 'message'=> "Record successfully saved!", 'data'=> $vw];
					} 
                    else 
                    {					   
						$response = errorSaveAdmission();
					}
				
                break;
			    case 'remove':
                    
                    $list = Request::get('idno');
                    
                    foreach($list as $r){
                        $ret = $this->model->where(['EntryID' => decode($r) ])->delete();    
                    }
                    
                    $response = successDelete();
                    
                break;
			}
		}
		return $response;
	}
    
       
	public function printPDF()
	{
		$rpt = new CrystalReport;
		$rpt->filename = 'reservation.rpt';
        
        $term = decode(Request::get('t'));
        $user = getUserName();
        
		$rpt->query = "EXEC rpt_admission_reservation '{$term}',1,'{$user}'";
         
        $subrpt = array(
            'subreport1' => array(
                'file' => 'Company_Logo',
                'query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'"
            ),
        );
        
        $rpt->SubReport = $subrpt;
            
		$file = $rpt->generate();

		header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $file['filename'] . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file['file']);
        unlink($file['file']);
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-reservation');
 		$this->model = new mReservation;
        $this->services =new Services;
 	}
    
}