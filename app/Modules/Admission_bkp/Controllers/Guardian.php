<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Services\AdmissionServiceProvider as Services;
use App\Modules\Setup\Models\City;
use Request;
use Response;
use Permission;
use DB;

class Guardian extends Controller{

	private $media =
		[
			'Title'=> 'Home',
			'Description'=> 'Welcome!',
			'js'		=> ['Admission/guardian'],
			'init'		=> ['GUARDIAN.init()','FN.multipleSelect()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min','jquery-validation/js/jquery.validate.min',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
			],
			'plugin_css' => [
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'bootstrap-fileinput/bootstrap-fileinput'
			]
		];

	private $url = [ 'page' => 'guardian/' ];

	private $views = 'Admission.Views.guardian.';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'updateGuardian':
					if ($this->permission->has('update-guardian')) {
						$validation = $this->services->isValid(Request::all(),'guardian');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							if($this->services->updateGuardian(Request::all())) {
								$response = successSave();
							} else {
								$response = errorSave();
							}
						}
					}
				break;
				case 'updateFather':
				case 'updateMother':
					$column = array('father-full-name'       =>'Father_Name',
                                    'father-dob'             =>'Father_BirthDate',
                                    'father-ocptn'           =>'Father_Occupation',
                                    'father-company'         =>'Father_Company',
                                    'father-business-addr'   =>'Father_CompanyAddress',
                                    'father-business-contact'=>'Father_CompanyPhone',
                                    'father-email'           =>'Father_Email',
                                    'father-contact-no'      =>'Father_TelNo',
                                    'father-contact-no'      =>'Father_Mobile',
                                    'father-educ-attain'     =>'Father_EducAttainment',
                                    'father-school-attended' =>'Father_SchoolAttended',
									'mother-full-name'       =>'Mother_Name',
                                    'mother-dob'             =>'Mother_BirthDate',
                                    'mother-ocptn'           =>'Mother_Occupation',
                                    'mother-company'         =>'Mother_Company',
                                    'mother-business-addr'   =>'Mother_CompanyAddress',
                                    'mother-business-contact'=>'Mother_CompanyPhone',
                                    'mother-email'           =>'Mother_Email',
                                    'mother-contact-no'      =>'Mother_TelNo',
                                    'mother-contact-no'      =>'Mother_Mobile',
                                    'mother-educ-attain'     =>'Mother_EducAttainment',
                                    'mother-school-attended' =>'Mother_SchoolAttended');
				    $update  = array();
					$fid     = getGuardianFamilyID();
					foreach(Request::all() as $k=>$v){
					 if($v!='' && $v!='-1'){
					  $v = strip_tags($v);
                      $v = str_replace("DELETE","",$v);
					  if($k=='father-dob' || $k=='mother-dob'){
					  $v = date('Y-m-d',strtotime($v));
					  }
					  if(array_key_exists($k,$column)){
					  $update[$column[$k]] = $v;
					  }
					 }
					}
					
					if(count($update)>0 && ($fid!='-1' && $fid!='-1')){
					 $exec     = DB::table('ESv2_Admission_FamilyBackground')->where('FamilyID',$fid)->update($update);
					 $response = (($exec)?successSave():errorSave());
					}else{
					 $response = errorSave();
					}
				break;
				case 'savePhoto':
					$response = Response::json($this->services->savePhoto(Request::all()));
				break;
				case 'getCity':
					$response = $this->city->where('CountryCode',Request::get('code'))->get();
				break;
			}
		}
		return $response;
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('guardian');
 		$this->guardian = new GuardianBackround;
 		$this->city = new City;
 		$this->services = new Services;

 	}
}

