<?php 
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Services\AdmissionEditServiceProvider as Services;
use App\Modules\Setup\Models\City;
use App\Modules\Admission\Services\Datatable\CommitteeDatatable;
use App\Modules\Admission\Services\Datatable\EvaluationDatatable;
use App\Modules\Admission\Models\AdmissionEvaluation;
use App\Modules\Admission\Models\AdmissionCommittee;
use Request;
use Response;
use Permission;
use DB;
use Storage;
use Image;
use App\Libraries\PDF;

class Edit extends Controller{

	private $media = [ 'Title'=> 'Admission : Profile', 'Description'=> 'Use this module to manage Applicant Profile',
			'js'		=> [
				'Admission/review','Admission/admission_edit',
				'Admission/admissionEditHelper','Admission/admissionData',
				'Admission/photoswipe','Admission/wizzard',
				'Admission/College','Admission/helper'
			],
            'css' => ['profile'],
			'init'		=> ['ADMISSION.init()','FN.datePicker()','FN.multipleSelect()'],
			'plugin_js'	=> [
				'fancybox/source/jquery.fancybox.pack',
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'photoswipe/asset/photoswipe',
                'photoswipe/asset/photoswipe-ui-default',
                'icheck/icheck',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap'
			],
			'plugin_css' => [
				'fancybox/source/jquery.fancybox',
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'photoswipe/asset/photoswipe',
				'photoswipe/asset/default-skin/default-skin',
				'icheck/skins/all'
			],
			'closeSidebar' => true
		];

	private $url = [ 'page' => 'admission/edit/' ];

	private $views = 'Admission.Views.edit.';

	function __construct()
	{
		$this->initializer();
	}
 	
 	public function index()
 	{	
 		$this->initializer();
        if ($this->permission->has('edit')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'updateGuardian':
					if ($this->permission->has('update-guardian')) {
						$validation = $this->services->isValid(Request::all(),'guardian');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
							if($this->services->updateGuardian(Request::all())) {
								$response = successSave();
							} else {
								$response = errorSave();
							}
						}
					}
					break;
				case 'setCurrentStep':
					putSessionData('editCurrentStep',Request::get('step'));
					$response = Response::json(1);
				break;
				case 'getCurrentStep':
					$response = (!getSessionData('editCurrentStep')) ? 1 : getSessionData('editCurrentStep');
				break;
				case 'save':
					if(!$this->services->isAdmitted()) {
						$result = $this->services->saveAdmission(Request::all());
						if (!$result['error']) {
							$response = [
								'error'=>false,
								'AppNo' => $result['AppNo'],
								'FamilyID' => encode($result['FamilyID']),
								'message'=>view($this->views.'forms.complete',['isSubmit'=>Request::get('isTemp')])->render()
							];
						} else {
							$response = errorSaveAdmission();
						}
					} else {
						$response = errorAdmittedAlready();
					}
				break;
				case 'showGradeLevelProgram':
					$response = Response::json($this->services->showProgram(decode(Request::get('key')),Request::get('class')));
				break;
				case 'getRequiredDocs':
					$response = $this->services->showRequiredDocs(Request::all());
				break;
				case 'saveDocuments':
					$response = Response::json($this->services->saveRequiredDocs(Request::all()));
				break;
				case 'savePhoto':
					$response = Response::json($this->services->savePhoto());
				break;
				case 'ShowExamScheduleDates':
					$response = ['error'=>false,'data'=>$this->services->showExamSchedDates(Request::all())];
				break;
				case 'verifyCode':
					$response = Response::json($this->services->verifyORCode(Request::all()));
				break;
				case 'removeSibling':
					$status = $this->services->siblingRemove(decode(Request::get('key')));
					$response = successDelete();
				break;
				case 'viewDocs':
					$response = view($this->views.'modals/view_doc');
				break;
				case 'getDocsPath':
					$response = Response::json($this->services->getDocsPath());
				break;
				case 'saveDocRemarks':
					$this->services->saveDocRemarks();
					$response = successSave();
				break;	
				case 'getDocsRemarks':
					$response = view($this->views.'modals.docs_remarks',['data'=>$this->services->getDocsRemarks()])->render();
				break;	
				case 'saveRemarksStatus':
					$this->services->saveRemarksStatus();
					$response = successSave();
				break;
				case 'savePanelRemarksStatus':
					$this->services->savePanelRemarksStatus();
					$response = successSave();
				break;
				case 'modalRemarks':
					$response = view($this->views.'modals.remarks');
				break;
				case 'modalPanelRemarks':
					$response = view($this->views.'modals.panel-remarks');
				break;
				case 'getCity':
					$response = $this->city->where('CountryCode',Request::get('code'))->get();
				break;
				case 'getYearLevel':
					$response = $this->services->getYearLevel(Request::get('type'));
				break;
				case 'getCourses':
					$response = $this->services->getCourses();
				break;
				case 'validateUsername':
					$response = $this->services->isValidUserName();
				break;
				case 'dataTableEvaluation':
					$response = $this->dtevaluation->filter();
				break;
				case 'saveEvaluation':
					$result = $this->services->saveEvaluation(Request::all()); 
					$message = $result['message'];
					$response = ['error'=> $result['error'],'message'=> getActionMessage($result['error'],$message)];
					unset($result['message']);
					$response = array_merge($response,$result);
					break;
				case 'editEvaluation':
						$response = view($this->views.'forms.evaluation',['dataEval'=>$this->evaluation->find(decode(Request::get('id')))]);
					break;
				case 'deleteEvaluation':
				        $post = Request::all();
				        $data = json_decode('['.$post['ids'].']', true);
				        foreach ($data as $row) {
				            $this->evaluation->destroy(decode($row['id']));
				        }
				        $response = json_encode(['error'=>false,'message'=>getActionMessage(false,'','delete')]);
					break;
				case 'dataTableCommittee':
					$response = $this->dtcommittee->filter();
				break;
				case 'saveCommittee':
					$result = $this->services->saveCommittee(Request::all()); 
					$message = $result['message'];
					$response = ['error'=> $result['error'],'message'=> getActionMessage($result['error'],$message)];
					unset($result['message']);
					$response = array_merge($response,$result);
					break;
				case 'editCommittee':
						$response = view($this->views.'forms.committee',['dataComm'=>$this->committee->find(decode(Request::get('id')))]);
					break;
				case 'deleteCommittee':
				        $post = Request::all();
				        $data = json_decode('['.$post['ids'].']', true);
				        foreach ($data as $row) {
				            $this->committee->destroy(decode($row['id']));
				        }
				        $response = json_encode(['error'=>false,'message'=>getActionMessage(false,'','delete')]);
					break;
				case 'byYrLvlScholastic';
					switch (Request::get('yrlvl')) {
						case 'kinder-grade1':
							$response = view('Admission.Views.admission.forms.sch_kndr_gr1',['AppNo'=>Request::get('AppNo')]);
							break;
						case 'grade2-grade11':
							$response = view('Admission.Views.admission.forms.sch_gr2_gr11',['AppNo'=>Request::get('AppNo')]);
							break;
						case 'returning';
							$response = view('Admission.Views.admission.forms.sch_rtn',['AppNo'=>Request::get('AppNo')]);
							break;
					}
				break;	
			}
		}
		return $response;
	}

	public function printApplication()
    {	
    	switch (Request::get('templ')) {
    		case '1':
    			echo view('reports.admission.application-form-kinder-grade-1')->render();
    			break;
    		case '2':
    			echo view('reports.admission.application-form-grade-2-11')->render();
    			break;
    		case '3':
    			echo view('reports.admission.application-form-returning')->render();
    			break;
    	}
	    die();
	}

	public function redirectEdit()
	{
		putSessionData('editCurrentStep',2);
		return redirect('/admission/edit?AppNo='.Request::get('AppNo'));
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('admission-apply');
 		$this->guardian = new GuardianBackround;
 		$this->services = new Services;
 		$this->city = new City;
 		$this->dtevaluation = new EvaluationDatatable;
 		$this->dtcommittee = new CommitteeDatatable;
 		$this->evaluation = new AdmissionEvaluation;
 		$this->committee = new AdmissionCommittee;
 	}
}


