<?php
namespace App\Modules\Admission\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admission\Models\GuardianBackround;
use App\Modules\Admission\Services\AdmissionServiceProvider as Services;
use App\Modules\Setup\Models\City;
use App\Modules\Admission\Services\Admission\Repository;
use App\Modules\Registrar\Models\TermConfig as term_model;

use Request;
use Response;
use Permission;
use DB;
use Mail;

class Analytics extends Controller{

	private $media = [
			'Title'=> 'Admission Analytics',
			'Description'=> 'Admission Analytics Module',
			'js'		=> ['Admission\analytics',],
			'init'		=> [],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'jquery-validation/js/jquery.validate.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'jquery-inputmask/jquery.inputmask.bundle.min',
                'icheck/icheck'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'wizzard-tab/css/gsdk-base',
				'bootstrap-fileinput/bootstrap-fileinput',
				'icheck/skins/all'
			]
		];

	private $url = [ 'page' => 'admission/analytics/' ];

	private $views = 'Admission.Views.analytics.';
  
	function __construct()
	{   
		$this->initializer();
	}
    
	public function index(){
 		$this->initializer();
 		$c = new \App\Modules\Admission\Services\Completion\Completion;
 		if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	function txn(){
		$response = noEvent();
		if (Request::ajax()){
			$this->initializer();
			$response = permissionDenied();
			switch(Request::get('event')){
				case 'consolidated':
				     $data           = array();
					 $data['termid'] = decode(Request::get('termid'));  
					 $data['list']   = Request::get('param');
					 $txn            = (string)view($this->views.'table.consolidate',$data)->render();
					 $response = json_encode(array('success'=>true,'content'=>$txn));
                     break;
				case 'monitoring':
				     $data           = array();
					 $data['termid'] = decode(Request::get('termid'));  
				     $data['status'] = decode(Request::get('status'));  
				     $data['filter'] = Request::get('filter');  
				     $data['limit']  = Request::get('limit');  
					 $txn            = (string)view($this->views.'table.monitoring',$data)->render();
					 $response = json_encode(array('success'=>true,'content'=>$txn));
                     break;			
				case 'total':
				     $data           = array();
					 $data['termid'] = decode(Request::get('termid'));  
					 $data['xmonth'] = Request::get('xmonth');  
				     $txna           = (string)view($this->views.'table.applicant',$data)->render();
				     $txnb           = (string)view($this->views.'table.student',$data)->render();
					 $response = json_encode(array('success'=>true,'applicant'=>$txna,'student'=>$txnb));
                     break;								 

			}
		}
		return $response;
	}
	
 	function printHTML(){
		$response = noEvent();
		$this->initializer();
		$event        = Request::get('event');
		$term         = Request::get('termid');
		$termb        = Request::get('xtermid');
		$status       = Request::get('status');
		$param        = Request::get('param');
		$data         = array();
		$data['views']= $this->views;
		$xdata        = $data;
		switch(Request::get('event')){
			case 'compare':
				 $data['termid']    = decode($term);
				 $data['termb']     = decode($termb);
				 $xdata['termid']   = decode($termb);
				 $data['applicant'] = (string)view($this->views.'table.applicant',$xdata)->render();
				 $data['student']   = (string)view($this->views.'table.student',$xdata)->render();
				 echo (string)view($this->views.'report.compare',$data)->render();
				 break;
			case 'consolidate':
				 $data['list']   = $param;
				 echo (string)view($this->views.'report.consolidate',$data)->render();
				 break;
			case 'monitoring':
				 $data['termid'] = decode($term);  
				 $data['status'] = decode($status);  
				 echo (string)view($this->views.'report.monitoring',$data)->render();
				 break;			
			case 'applicant':
				 $data['termid'] = decode($term);  
				 echo (string)view($this->views.'report.applicant',$data)->render();
				 break;			
			case 'students':
				 $data['termid'] = decode($term);  
				 echo (string)view($this->views.'report.student',$data)->render();
				 break;		
			default:
			     echo 'Invalid Parameters';
				 break;
		}
	}

 	private function initializer($menu = 'admission-apply')
 	{
 	    $this->permission = new Permission($menu);
 		$this->guardian = new GuardianBackround;
 		$this->services = new Services;
 		$this->city = new City;
 	}
}
