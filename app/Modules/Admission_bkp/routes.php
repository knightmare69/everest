<?php
	Route::group(['prefix' => 'admission','middleware'=>'auth'], function () {

		Route::get('redirecToApply','Admission@redirecToApply');
	    Route::group(['prefix' => 'apply','middleware'=>'auth'], function () {
		    Route::get('/','Admission@index');
		    Route::post('event','Admission@event');
		});

		Route::group(['prefix' => 'quick','middleware'=>'auth'], function () {
		    Route::get('/','Quick@index');
		    Route::get('print','Quick@printPDF');
		    Route::post('event','Quick@event');
		});

	    Route::group(['prefix' => 'listing','middleware'=>'auth'], function () {
		    Route::get('/','Listing@index');
		    Route::post('update-status','Listing@updateStatus');
		    Route::post('event','Listing@event');
		    Route::post('search','Listing@search');
		    Route::post('getYearLevel','Listing@getYearLevel');
		    Route::post('showRemarks','Listing@showRemarks');
		    Route::post('paymentStatus','Listing@paymentStatus');
		    Route::post('hasInterviewed','Listing@hasInterviewed');
		});
		Route::group(['prefix' => 'edit','middleware'=>'auth'], function () {
		    Route::get('/','Edit@index');
		    Route::post('event','Edit@event');
		    Route::post('dataTableCommittee','Edit@dataTableCommittee');
		    Route::get('printApplication','Edit@printApplication');
		});
		Route::group(['prefix' => 'redirectEdit','middleware'=>'auth'], function () {
		    Route::get('/','Edit@redirectEdit');
		});
        
        Route::group(['prefix' => 'reservation','middleware'=>'auth'], function () {
		    Route::get('/','Reservation@index');
		    Route::post('event','Reservation@event');
            Route::get('/print','Reservation@printPDF');
		});

		Route::group(['prefix' => 'panels','middleware'=>'auth'], function () {
		    Route::get('/','Panels@index');
		    Route::post('/get','Panels@get');
		    Route::get('search-user','Panels@searchUser');
		    Route::post('event','Panels@event');
		});

		Route::group(['prefix' => 'tour','middleware'=>'auth'], function () {
		    Route::get('/','Schedule@index');
		    Route::post('/save-batch','Schedule@saveBatch');
		    Route::post('/update-batch','Schedule@updateBatch');
		    Route::post('/batch-form','Schedule@formBatch');
		    Route::post('/get-batch','Schedule@scheduleBatch');
		    Route::post('/get-nonsched','Schedule@nonSchedApplicants');
		    Route::post('/get-withsched/{batchID}','Schedule@withSchedApplicants');
		    Route::post('/add-applicant/{batchID}','Schedule@addApplicant');
		    Route::post('/rem-applicant/{batchID}','Schedule@remApplicant');
		    Route::post('/con-applicant/{batchID}','Schedule@conApplicant');
		    Route::post('/email-content/{batchID}','Schedule@emailContent');

		    Route::post('/parent-save','Schedule@saveParent');
		    Route::post('/parent-update','Schedule@updateUpdate');

		    Route::post('/child-save/{parentKey}','Schedule@saveChild');
		    Route::post('/child-update/{childKey}','Schedule@updateChild');

		    Route::post('/listings','Schedule@listings');
		    Route::post('/update-type/{id}','Schedule@updateType');
		});

		Route::group(['prefix' => 'exam','middleware'=>'auth'], function () {
		    Route::get('/','Schedule@index');
		    Route::post('/save-batch','Schedule@saveBatch');
		    Route::post('/update-batch','Schedule@updateBatch');
		    Route::post('/batch-form','Schedule@formBatch');
		    Route::post('/get-batch','Schedule@scheduleBatch');
		    Route::post('/get-nonsched','Schedule@nonSchedApplicants');
		    Route::post('/get-withsched/{batchID}','Schedule@withSchedApplicants');
		    Route::post('/add-applicant/{batchID}','Schedule@addApplicant');
		    Route::post('/email-content/{batchID}','Schedule@emailContent');

		    Route::post('/parent-save','Schedule@saveParent');
		    Route::post('/parent-update','Schedule@updateUpdate');

		    Route::post('/child-save/{parentKey}','Schedule@saveChild');
		    Route::post('/child-update/{childKey}','Schedule@updateChild');
		});

		Route::group(['prefix' => 'schedule/inquiry','middleware'=>'auth'], function () {
		    Route::get('/','Schedule@index');
		    Route::get('/list','schedule@lists');
		    Route::post('/get','Schedule@get');
		    Route::post('/parent-save','Schedule@saveParent');
		    Route::post('/parent-update','Schedule@updateUpdate');
		});

		Route::group(['prefix' => 'tour/schedule'], function () {
		    Route::get('/','Schedule@tourSchedule');
		});

		Route::group(['prefix' => 'exam/schedule'], function () {
		    Route::get('/','Schedule@examSchedule');
		});        
	});

	Route::group(['prefix' => 'guardian','middleware'=>'auth'], function () {
	    Route::get('/','Guardian@index');
	    Route::post('event','Guardian@event');
	});

	Route::group(['prefix' => 'subscription','middleware'=>'auth'], function () {
	    Route::get('/','GuardianConnect@index');
	    Route::post('event','GuardianConnect@event');
	    Route::get('getPhoto','GuardianConnect@getPhoto');
	});

	Route::group(['prefix' => 'register'], function () {
	    Route::post('/','Account@register');
	});
	
	Route::group(['prefix' => 'admission/analytics'], function () {
	    Route::get('/','analytics@index');
	    Route::get('event','analytics@txn');
	    Route::post('event','analytics@txn');
	    Route::get('print','analytics@printHTML');
	});

	Route::group(['prefix' => 'parent/create-account'], function () {
	    Route::get('/','Account@parentCreateAccount');
	});

	Route::group(['prefix' => 'sendVCode'], function () {
	    Route::post('/','Account@sendVCode');
	});
    
    Route::group(['prefix' => 'RequestAccessCode'], function () {
	    Route::post('/','Account@RequestAccessCode');
	});
    
	
	Route::group(['prefix' => 'registration'], function () {
	    Route::group(['prefix' => 'confirm'], function () {
		    Route::get('/','Account@registerConfirm');
		});
	});
    
	Route::group(['prefix' => 'applicant'], function () {
		Route::get('/','Applicant@index');
		Route::post('txn','Applicant@txn');
	});
	
	Route::group(['prefix' => 'inquiry'], function () {
		Route::get('/','Inquiry@index');
		Route::post('register','Inquiry@register');
		Route::post('register-child/{parentKey}','Inquiry@registerChild');
		Route::post('verify/{id}','Inquiry@verify');
		Route::group(['prefix' => 'app'], function () {
		    Route::get('/','InquiryChildApp@index');
		    Route::get('print','InquiryChildApp@printPDF');
		    Route::post('event','InquiryChildApp@event');
		});
	});

	Route::group(['prefix' => 'walkin'], function () {
		Route::get('/','WalkinInquiry@index');
		Route::post('register','WalkinInquiry@register');
		Route::post('register-child/{parentKey}','WalkinInquiry@registerChild');
		Route::post('verify/{id}','WalkinInquiry@verify');
		Route::group(['prefix' => 'app'], function () {
		    Route::get('/','InquiryChildApp@index');
		    Route::get('print','InquiryChildApp@printPDF');
		    Route::post('event','InquiryChildApp@event');
		});
	});
?>