<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class Attachment extends Model {

	protected $table='Admission';
	protected $connection = "sqlsrvAttachment";
	public $primaryKey ='ID';

	protected $fillable  = array(
			'EntryID',
			'FileName',		
			'Attachment',
			'FileType',
			'FileSize',
			'FileExtension',
			'CreatedDate',
			'CreatedBy',
	);

	public $timestamps = false;

}