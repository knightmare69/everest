<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionStatus extends Model {

	protected $table='ESv2_AdmissionStatus';
	public $primaryKey ='StatusID';

	protected $fillable  = array(
	   'StatusDesc',
	   'SeqNo'
	);

	public $timestamps = false;

}