<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class InquiryTemp extends Model {

	protected $table='es_inquiry_temp';
	public $primaryKey ='id';

	protected $fillable  = array(
			'type',
			'email',
			'fname',		
			'mname',
			'lname',
			'contact_no',
			'YearLevelID',
			'CreatedDate',
			'CreatedBy',
			'ModifiedDate',
			'ModifiedBy'
	);

	public $timestamps = false;
}