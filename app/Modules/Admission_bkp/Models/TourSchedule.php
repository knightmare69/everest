<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class TourSchedule extends Model {

	protected $table='esv2_tour_schedule';
	public $primaryKey ='id';

	protected $fillable  = array(
			'TermID',
			'Schedule',
			'Remarks',
			'Status',
			'CreatedBy',
			'CreatedDate',
			'ModifiedDate',
			'ModifiedBy'
		);

	public $timestamps = false;
}