<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;

Class GuardianBackround extends Model {

	protected $table='ESv2_Admission_FamilyBackground';
	public $primaryKey ='FamilyID';

	protected $fillable  = array(
			'FamilyID',
			'Guardian_Name',
			'Guardian_BirthDate',
			'Guardian_ParentMaritalID',
			'Guardian_LivingWith',
			'Guardian_RelationshipOthers',
			'Guardian_AddressFull',
			'Guardian_Address',
			'Guardian_Street',
			'Guardian_Barangay',
			'Guardian_TownCity',
			'Guardian_Province',
			'Guardian_ZipCode',
			'Guardian_TelNo',
			'Guardian_Mobile',
			'Guardian_Email',
			'Guardian_Photo',
			'Guardian_CountryCode',
			'Guardian_isOtherCity',
			'Guardian_OtherCity',
			'Guardian_CityID',
			'Guardian_Billing_AddressFull',
			'Guardian_Billing_Address',
			'Guardian_Billing_Street',
			'Guardian_Billing_Barangay',
			'Guardian_Billing_TownCity',
			'Guardian_Billing_CountryCode',
			'Guardian_Billing_CityID',
			'Guardian_Billing_IsOtherCity',
			'Guardian_Billing_OtherCity',
			'IncludeViberGroup',
			'FatherMother_MarriageDate',
			'EmergencyContact_Name',
			'EmergencyContact_Relationship',
			'EmergencyContact_Address',
			'EmergencyContact_Email',
			'EmergencyContact_TelNo',
			'EmergencyContact_Mobile',
			'EmergencyContact_CompanyPhone',
		);

	public $timestamps = false;

	public static function getLivingWith($key = null) 
	{
		$data = [
			'Both Parents',
			'Father',
			'Mother',
			'Guardian',
			'Others'
		];

		return $key ? $data[$key] : $data;
	}
}