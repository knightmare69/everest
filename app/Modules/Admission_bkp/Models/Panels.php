<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Panels extends Model {

	protected $table='esv2_admission_panels';
	public $primaryKey ='id';

	protected $fillable  = array(
			'Title',
			'UserID',
			'Level',
			'TermID',
			'CreatedBy',
			'CreatedDate',
			'ModifiedBy',
			'ModifiedDate'
		);

	public $timestamps = false;

	public static function data() {
		return
			DB::table('esv2_admission_panels as p')
				->join('ES_AYTerm as t',
						't.TermID','=','p.TermID'
					)
				->join('ESv2_Users as u',
						'u.UserIDX','=','p.UserID'
					);
	}

	public static function getLevel() {
		return DB::table('esv2_admission_panels as p')
					->select('Level')
					->where('UserID',getUserID())
						->pluck('Level');
	}
}