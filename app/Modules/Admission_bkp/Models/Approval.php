<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Approval extends Model {

	protected $table='esv2_admission_panel_approval';
	public $primaryKey ='id';

	protected $fillable  = array(
			'PanelID',
			'AppNo',
			'Level',
			'Comment',
			'Status',
			'CreatedBy',
			'CreatedDate'
		);

	public $timestamps = false;

	public static function hasMade($AppNo, $user) {
		return DB::table('esv2_admission_panel_approval')
					->where('AppNo',$AppNo)
					->where('CreatedBy',$user)
					// ->orderBy('id','desc')
					->limit(1)
						->count();
	}

	public static function isApproved($AppNo, $user) {
		return DB::table('esv2_admission_panel_approval')
					->where('AppNo',$AppNo)
					->where('CreatedBy',$user)
					->where('Status','1')
					// ->orderBy('id','desc')
					->limit(1)
						->count();
	}

	public static function isDone($AppNo) {
		$approver = DB::table('esv2_admission_panel_approval')
					->where('AppNo',$AppNo)
						->count();

		$panel = DB::table('esv2_admission_panels')
					->where('TermID',\App\Modules\Admission\Models\Admission::getTermID($AppNo))
						->count();

		return  ($approver >= $panel);
	}

	public static function isFinalApproved($AppNo) {
		$approver = DB::table('esv2_admission_panel_approval')
					->where('AppNo',$AppNo)
					->where('Status','1')
						->count();

		$panel = DB::table('esv2_admission_panels')
					->where('TermID',\App\Modules\Admission\Models\Admission::getTermID($AppNo))
						->count();
		return  ($approver >= $panel);
	}

	public static function getComments($AppNo) {
		return DB::table('esv2_admission_panel_approval as a')
					->join('esv2_users as u',
								'u.UserIDX','=','a.CreatedBy'
							)
					->select(['u.FullName','Comment'])
					->where('AppNo',$AppNo)
						->get();
	}
}