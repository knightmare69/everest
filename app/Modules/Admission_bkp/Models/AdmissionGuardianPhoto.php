<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionGuardianPhoto extends Model {

	protected $table='AdmissionGuardianPhoto';
	//protected $connection = "sqlsrvAttachment";
	public $primaryKey ='ID';

	protected $fillable  = array(
			'FamilyID',
			'FileName',		
			'Attachment',
			'FileType',
			'FileSize',
			'FileExtension',
			'CreatedDate',
			'CreatedBy',
	);

	public $timestamps = false;
}