<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;

Class ParentsAccounts extends Model {

	protected $table='ESv2_ParentAccounts';
	public $primaryKey ='AccountID';

	protected $fillable  = array(
			'FamilyID',
			'Username',
			'password',
			'Remember_Token',
			'RegisteredDate',
			'ConfirmNo',
			'LastLoginDate',
			'IsConfirmed'
		);

	public $timestamps = false;
}