<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class InquirySchedule extends Model {

	protected $table='es_inquiry_batch';
	public $primaryKey ='id';

	protected $fillable  = array(
			'ScheduleType',
			'title',
			'max',
			'BatchDate',		
			'CreatedDate',
			'CreatedBy',
			'ModifiedDate',
			'ModifiedBy'
	);

	public $timestamps = false;
}