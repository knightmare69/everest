<?php namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionEvaluation extends Model {

	protected $table='ESv2_Admission_Evaluation';
	public $primaryKey ='EvaluationID';

	protected $fillable  = [
							'AppNo','InterviewType','DateInterviewed','Remarks','Recommendation','CreatedBy','CreatedDate'
						];
			
	public $timestamps = false;
}