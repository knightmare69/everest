<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;

Class MotherBackground extends Model {

	protected $table='ESv2_Admission_FamilyBackground';
	public $primaryKey ='FamilyID';

	protected $fillable  = array(
			'FamilyID',
			'Mother_Name',
			'Mother_BirthDate',
			'Mother_MaritalID',
			'Mother_Occupation',
			'Mother_Company',
			'Mother_CompanyAddress',
			'Mother_CompanyPhone',
			'Mother_Email',
			'Mother_TelNo',
			'Mother_Mobile',
			'Mother_Address',
			'Mother_EducAttainment',
			'Mother_SchoolAttended',
			'Mother_Photo',
			'Mother_ReligionID',
			'Mother_BusinessType',
			'Mother_NationalityID',
			'Mother_CitizenshipID',
		);

	public $timestamps = false;

}