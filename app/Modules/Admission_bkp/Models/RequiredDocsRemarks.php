<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;
use DB;
Class RequiredDocsRemarks extends Model {

	protected $table='ESv2_Admission_RequiredDocsRemarks';
	public $primaryKey ='ID';

	protected $fillable  = array(
			'DocID',
			'Remarks',
			'CreatedBy',
			'CreatedDate'
		);

	public $timestamps = false;

	public function data()
	{
		return
		DB::table('ESv2_Admission_RequiredDocsRemarks as r')
			->select([
				'r.DocID',
				'r.Remarks',
				'r.CreatedDate',
				'u.FullName'
			])
			->leftJoin('ESV2_users as u','u.UserIDX','=','r.CreatedBy');
	}
}