<?php namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;

Class AdmissionCommittee extends Model {

	protected $table='ES_AdmissionCommittee';
	public $primaryKey ='IndexID';

	protected $fillable  = [
							'AppNo', 'CommitteeRemarks', 'IsAdmitted', 'IsConditional', 'IsWaitPool', 'IsDenied', 'CreatedBy', 'CreatedDate'
						];
			
	public $timestamps = false;
}