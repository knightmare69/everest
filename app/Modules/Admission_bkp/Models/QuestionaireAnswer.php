<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class QuestionaireAnswer extends Model {

	protected $table='esv2_admission_questions_answer';
	public $primaryKey ='id';

	protected $fillable  = array(
			'app_no',
			'question_id',
			'answer',
			'created_date',
			'created_by',
			'modified_date',
			'modified_by'
	);

	public $timestamps = false;

}