<?php 
namespace App\Modules\Admission\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class SchoolsAttended extends Model {

	protected $table='esv2_admission_schools_attended';
	public $primaryKey ='id';

	protected $fillable  = array(
			'AppNo',
			'school',
			'address',
			'fromDate',
			'toDate',
			'yearLevel',
			'CreatedDate',
			'CreatedBy',
			'ModifiedDate',
			'ModifiedBy'
	);

	public $timestamps = false;

}