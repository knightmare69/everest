<?php 

namespace App\Modules\Admission\Models;
use illuminate\Database\Eloquent\Model;
use App\Modules\Admission\Models\AdmissionPhoto;
use DB;

Class Admission extends Model {

	protected $table='ESv2_Admission';
	public $primaryKey ='AppNo';

	protected $fillable  = 
		[
			'AppNo', 'AppDate', 'CampusID', 'TermID', 'AppTypeID', 'GradeLevelID', 'ProgramID', 'MajorID', 'LastName', 'FirstName', 'MiddleName',
			'MiddleInitial', 'ExtName', 'NickName', 'Gender', 'DateOfBirth', 'PlaceOfBirth', 'IsAdopted', 'HomeAddressUnitNo', 'HomeAddressStreet',
			'HomeAddressBrgy', 'HomeAddressCity', 'HomeAddressZipCode', 'HomePhone', 'CivilStatusID', 'NationalityID', 'CitizenshipID', 'ForeignStudent',
			'IsForeignCitizen', 'PassportNumber', 'VisaStatus', 'SSP_Number', 'Validity_of_Stay', 'ReligionID', 'FamilyID', 'DenyReason', 'Elem_School',
			'Elem_Address', 'Elem_InclDates', 'Elem_GradeLevel', 'Elem_GWA', 'Elem_AwardHonor', 'HS_School', 'HS_Address', 'HS_InclDates', 'HS_GradeLevel',
			'HS_GWA', 'HS_AwardHonor', 'TestingSchedID', 'ORNo', 'ORSecurityCode', 'CreatedDate', 'CreatedBy', 'LastModifiedDate', 'LastModifiedBy',
			'IsOnlineApplication', 'IsCompleteApplication', 'StatusID', 'ESigInitial', 'ESigDate', 'PhotoFile', 'LRN', 'Choice1_CampusID', 'Choice2_CampusID',
			'Choice3_CampusID', 'Choice4_CampusID', 'Choice1_Course', 'Choice1_CourseMajor', 'Choice2_Course', 'Choice2_CourseMajor', 'Choice3_Course',
			'Choice3_CourseMajor', 'IsCollege', 'ProgClass', 'CompletionStatus', 'Family_HealthStatus', 'Family_Situation', 'Familly_Activities', 'Family_Correspondence',
			'Family_PaymentResponsible', 'IsBaptized', 'BaptizedIn_Date', 'BaptizedIn_Church', 'BaptizedIn_City', 'WorshipPlace', 'WorshipAddress', 'WorshipTelNo', 'Marketing', 
			'IsAttendingKdg', 'PresentSchool', 'PresentSchoolYrsProgram', 'PresentSchoolCurriculum', 'PresentSchoolCurriculumOthers', 'PresentSchoolHead',
			'PresentSchoolAddress', 'PresentGradeLevel', 'PresentSchoolDateAttended', 'PresentSchoolCalendarMonths', 'PresentSchoolContactNo',
			'PresentSchoolWebsite', 'PresentSchoolReasonLeaving', 'PresentSchoolReasonReApplying', 'PresentSchoolPassAllSubjects', 'PresentSchoolSubjectsFailing',
			'PaymentStatus', 'HasInterviewed'
		];

	public $timestamps = false;

	public static function getTermID($AppNo) {
		return DB::table('ESv2_Admission')
					->where('AppNo',$AppNo)
					->select('TermID')
						->pluck('TermID');
	}

	public static function getApplicationType($key = null)
	{
		return DB::table('ESv2_ApplicationTypes')->select(['AppTypeID','ApplicationType','SeqNo'])->where('AppTypeID','<=',3)->get();
	}

	public static function getProgClass($key)
	{
		return DB::table('ES_Programs')->select('ProgClass')->where('ProgID',$key)->pluck('ProgClass');
	}

	public static function getGradeLevel($key = null)
	{
		$columns = ['YLID_OldValue as YearLevelID','YearLevelName','ProgClass','ProgID'];
		$model = DB::table('ESv2_YearLevel');
		if ($key) {
			return $model->select($columns)
						->where(['YearLevelID'=>$key])
						//->where('ProgClass','<=',21)
						->get();
		}
		return $model->select($columns)
					//->where('ProgClass','<=',21)
					->orderBy('ProgClass','asc')
					->get();
	}

	public static function getHigherYearLevel($key = null)
	{
		$columns = ['YearLevelID','YearLevel as YearLevelName','ProgClass'];
		$model = DB::table('vw_YearLevel');
		if ($key) {
			return $model->select($columns)
						->where(['YearLevelID'=>$key])
						//->where('ProgClass','>',21)
						->orderBy('SeqNo','asc')
						->get();
		}
		return $model->select($columns)
					//->where('ProgClass','>',21)
					->orderBy('SeqNo','asc')
					->get();
	}

	public static function getYearLevelList()
	{
		//return DB::select("exec sp_K12_YearLevelList 1,'".getUserID()."'");
		//return DB::table('vw_YearLevel')->get();
		return DB::table('ESv2_YearLevel')->get();
	}


	public static function getCivilStatus($key = null)
	{
		return DB::table('CivilStatus')->select(['StatusID','CivilDesc'])->get();
	}

	public static function getNationality($key = null)
	{
		return DB::table('Nationalities')->select(['NationalityID','Nationality','IsForeign'])->orderBy('Nationality','asc')->get();
	}

	public static function getReligion($key = null)
	{
		return DB::table('ES_Religions')->select(['ReligionID','Religion','isDefault'])->orderBy('Religion','asc')->get();
	}

	public static function getCampus($key = null)
	{
		return DB::table('vw_Campus')->select(['CampusID','Code'])->get();
	}

	public static function getYearTerm()
	{
		return DB::table('vw_K12_AcademicYears')->select(['TermID','AcademicYear','SchoolTerm'])->where('Active_OnlineEnrolment',1)->orderBy('AcademicYear','desc')->get();
	}

	public static function getExamSchedDates($CampusID,$TermID,$YearLevelID)
	{
		return DB::select("EXEC sp_K12_TestingSchedules '".$CampusID."','".$TermID."','".$YearLevelID."'");
	}

	public static function getMyExamSchedDate($TestingSchedID)
	{
		return DB::table('ESv2_Admission_TestingSchedules')
					->where('TestingSchedID',$TestingSchedID)
					->get();
	}

	public static function getAcademicTrack($key,$ProgClass='11') 
	{	
		$ProgClass = !$ProgClass ? 11 : $ProgClass;
		return DB::select("select * from ESv2_fn_AcademicTrack('".trimmed($key)."') where ProgClass=".trimmed($ProgClass));
	}

	public static function getApplicants()
	{
		if (getUserGroup() == 'parent') {
			$where = ['FamilyID'=>getGuardianFamilyID()];
			return 
			DB::table('vw_K12_Admission')->where($where)->get();
		} else {
			return DB::table('vw_K12_Admission')->get();
		}
	}

	public static function getAdmission($AppNo,$columns = array())
	{
		return DB::table('ESv2_Admission')
		->select($columns)
		->where('AppNo',$AppNo)
		->get();
	}

	public function getRequiredDocs($AppTypeID,$IsForeign,$ProgClassID,$YearLevelID)
	{
		return
		DB::table('ESv2_RequirementsList as R')
		->select([
			'TD.DetailID',
			'TD.TemplateID',
			'T.TemplateName',
			'TD.RequirementID',
			'R.Code AS DocCode',
			'R.Desc AS DocDesc',
			'TD.IsRequired',
			'TD.SeqNo'
		])
		->rightJoin(
			'ESv2_RequirementsTemplateDetails as TD',
			'R.RequirementID','=','TD.RequirementID'
		)
		->leftJoin('ESv2_RequirementsTemplate AS T',
			'TD.TemplateID','=','T.TemplateID'
		)
		->where([
			// 'T.ApplicationTypeID' => $AppTypeID,
			// 'T.ForForeigner' => $IsForeign,
			// 'T.AppliedProgClassID' => $ProgClassID
		])
		//->limit(1)
		// ->whereIn('T.AppliedGradeLevelID',[0,$YearLevelID])
		->get();
	}

	public static function getRequiredDocsEditMode($AppNo,$AppTypeID,$IsForeign,$ProgClassID,$YearLevelID)
	{
		$hasDocs = DB::table('ESv2_RequirementsList as R')
		->select([
			'docs.EntryID',
			'docs.IsReviewed',
			'docs.IsExempted',
			'docs.HasAttachment',
			'docs.Filename',
			'TD.RequirementID',
			'TD.DetailID',
			'TD.TemplateID',
			'T.TemplateName',
			'R.Desc AS DocDesc',
			'TD.IsRequired',
			'TD.SeqNo',
			'P.ProgClass'
		])
		->rightJoin(
			'ESv2_RequirementsTemplateDetails as TD',
			'R.RequirementID','=','TD.RequirementID'
		)
		->leftJoin('ESv2_RequirementsTemplate AS T',
			'TD.TemplateID','=','T.TemplateID'
		)
		->leftJoin('ESv2_Admission_RequiredDocs as docs',
			'docs.TemplateID','=','T.TemplateID'
		)
		->leftJoin('ESv2_Admission as A',
			'A.AppNo','=','docs.AppNo'
		)
		->leftJoin('ES_Programs as P',
			'P.ProgID','=','A.ProgramID'
		)
		// ->where([
		// 	'T.ApplicationTypeID' => $AppTypeID,
		// 	'T.ForForeigner' => $IsForeign,
		// 	'T.AppliedProgClassID' => $ProgClassID
		// ])
		// ->whereRaw('docs.TemplateDetailID=TD.DetailID')
		// ->whereIn('T.AppliedGradeLevelID',[0,$YearLevelID])
		->where('A.AppNo',$AppNo);

		$NoDocs = DB::table('ESv2_RequirementsList as R')
		->select([
			DB::raw('0 as EntryID'),
			DB::raw('0 as IsReviewed'),
			DB::raw('0 as IsExempted'),
			DB::raw('0 as HasAttachment'),
			DB::raw("'' as Filename"),
			'TD.RequirementID',
			'TD.DetailID',
			'TD.TemplateID',
			'T.TemplateName',
			'R.Desc AS DocDesc',
			'TD.IsRequired',
			'TD.SeqNo',
			'Y.ProgClass'
		])
		->rightJoin(
			'ESv2_RequirementsTemplateDetails as TD',
			'R.RequirementID','=','TD.RequirementID'
		)
		->leftJoin('ESv2_RequirementsTemplate AS T',
			'TD.TemplateID','=','T.TemplateID'
		)
		->leftJoin('ESv2_Admission_RequiredDocs as docs',
			'docs.TemplateID','=','T.TemplateID'
		)
		->leftJoin('ESv2_Admission as A',
			'A.AppNo','=','docs.AppNo'
		)
		->leftJoin('ESv2_YearLevel as Y',
			'Y.YearLevelID','=','A.GradeLevelID'
		)
		// ->where([
		// 	'T.ApplicationTypeID' => $AppTypeID,
		// 	'T.ForForeigner' => $IsForeign,
		// 	'T.AppliedProgClassID' => $ProgClassID
		// ])
		->whereRaw("TD.DetailID NOT IN(select D.TemplateDetailID from ESv2_Admission_RequiredDocs D where D.AppNo='".$AppNo."')")
		// ->limit(1)
		// ->whereIn('T.AppliedGradeLevelID',[0,decode($YearLevelID)])
		// ->whereRaw('[Y].[ProgClass] is not null')
		// ->where('A.AppNo',$AppNo)
		->union($hasDocs)
		->orderBy('TD.SeqNo')
		->get();

		return $NoDocs;
	}

	public function getAdmission2()
	{
		$columns = [
			'A.AppNo',
			'A.AppDate',
			'A.ORNo',
			'A.TermID',
			'A.CampusID',
			'A.AppTypeID',
			'A.GradeLevelID',
			'A.ProgramID',
			'P.ProgCode',
			'P.ProgName',
			'A.MajorID',
			'DM.MajorDiscCode AS StrandTrackCode',
			'DM.MajorDiscDesc AS StrandTrackDesc',
			'T.ApplicationType',
			'Y.YearLevelCode',
			'Y.YearLevelName',
			DB::raw("
				UPPER(A.LastName)+ ', ' + A.FirstName +(
			      CASE
			      WHEN A.ExtName = '' THEN
			         ' '
			      ELSE
			         ' ' + UPPER(A.ExtName)+ ' '
			      END
			   )+ A.MiddleInitial AS ApplicantName"),
			'A.DateOfBirth',
			DB::raw('dbo.fn_Age(A.DateOfBirth, A.AppDate)AS Age'),
			'A.Gender',
			'A.MedicalSchedID',
			'A.MedicalActualSched',
			'A.MedicalResults',
			'A.MedicalSignedBy',
			'A.MedicalSignedDate',
			'A.TestingSchedID',
			'A.TestingActualSched',
			'A.TestingExamRefNo',
			'A.TestingExamRemarks',
			'A.TestingSignedDate',
			'A.TestingSignedBy',
			'A.FinalMedicalResultID',
			'A.FinalTestExamResultID',
			'TR.TestResultDesc',
			'MR.MedicalResultDesc',
			'S.StatusDesc',
			'TS.Description AS TestingSched',
			'MS.Description AS MedicalSched',
			'A.FamilyID',
			'A.DenyReason',
			'A.PassCode',
			'A.StatusID',
			'A.IsOnlineApplication',
			'A.IsCompleteApplication',
			'A.Photo',
			'S_1.StudentNo',
			DB::raw("
				(
			      CASE
				      WHEN S_1.StudentNo IS NULL THEN
				         0
				      ELSE
				         1
				      END
			   ) AS AllowToEnroll"
			),

		];

		return DB::table('ESv2_Admission as A')
				->select($columns)
				->leftJoin('ES_Students AS S_1','A.AppNo','=','S_1.AppNo')
				->leftJoin('ES_DisciplineMajors AS DM','A.MajorID','=','DM.IndexID')
				->leftJoin('ES_Programs AS P','A.ProgramID','=','P.ProgID')
				->leftJoin('ESv2_Admission_TestingSchedules AS TS','A.TestingSchedID','=','TS.TestingSchedID')
				->leftJoin('ESv2_Admission_MedicalSchedules AS MS','A.MedicalSchedID','=','MS.MedicalSchedID')
				->leftJoin('ESv2_AdmissionStatus AS S','A.StatusID','=','S.StatusID')
				->leftJoin('ESv2_TestingResults AS TR','A.FinalTestExamResultID','=','TR.TestResultID')
				->leftJoin('ESv2_MedicalResults AS MR','A.FinalMedicalResultID','=','MR.MedicalResultID')
				->leftJoin('ESv2_YearLevel AS Y','A.GradeLevelID','=','Y.YearLevelID')
				->leftJoin('ESv2_ApplicationTypes AS T','A.AppTypeID','=','T.AppTypeID');

	}

	public function saveAdmittedToStudent($appno)
	{
		if($appno){
			$s = DB::table('ES_Students')->where('AppNo', $appno)->first();

			if (empty($s)) {
				$qry   = "INSERT  INTO ES_Students
								( StudentNo,
								  AppNo,
								  CampusID,
								  TermID ,
								  ProgID ,
								  YearLevelID ,
								  MajorDiscID ,
								  CurriculumID ,
								  LastName ,
								  FirstName ,
								  Middlename ,
								  MiddleInitial ,
								  ExtName ,
								  Fullname ,
								  DateOfBirth ,
								  PlaceOfBirth ,
								  Gender ,
								  CivilStatusID ,
								  ReligionID ,
								  NationalityID ,
								  TelNo ,
								  MobileNo ,
								  Email ,
								  Res_Address ,
								  Res_Street ,
								  Res_Barangay ,
								  Res_TownCity ,
								  Res_Province ,
								  Res_ZipCode ,
								  Perm_Address ,
								  Perm_Street ,
								  Perm_Barangay ,
								  Perm_TownCity ,
								  Perm_Province ,
								  Perm_ZipCode ,
								  Father ,
								  Father_Occupation ,
								  Father_Company ,
								  Father_CompanyAddress ,
								  Father_TelNo ,
								  Father_Email ,
								  Mother ,
								  Mother_Occupation ,
								  Mother_Company ,
								  Mother_CompanyAddress ,
								  Mother_TelNo ,
								  Mother_Email ,
								  Guardian ,
								  Guardian_Relationship ,
								  Guardian_Address ,
								  Guardian_Street ,
								  Guardian_Barangay ,
								  Guardian_TownCity ,
								  Guardian_Province ,
								  Guardian_ZipCode ,
								  Guardian_Occupation ,
								  Guardian_Company ,
								  Guardian_TelNo ,
								  Guardian_Email ,
								  Elem_School ,
								  Elem_Address ,
								  Elem_InclDates ,
								  HS_School ,
								  HS_Address ,
								  HS_InclDates ,
								  StudentPicture ,
								  FamilyID
								)
								SELECT 
										dbo.fn_K12_GenerateSN(CampusID, TermID, GradeLevelID),
										AppNo ,
										CampusID ,
										TermID ,
										ProgramID ,
										GradeLevelID,
										MajorID ,
										0 AS CurriculumID ,
										LastName ,
										FirstName ,
										MiddleName ,
										MiddleInitial ,
										ExtName ,
										CONCAT(UPPER(LastName), ', ', FirstName, ' ',MiddleInitial) AS Fullname ,
										DateOfBirth ,
										PlaceOfBirth ,
										Gender ,
										CivilStatusID ,
										ReligionID ,
										NationalityID ,
										fb.Guardian_TelNo ,
										fb.Guardian_Mobile ,
										fb.Guardian_Email ,
										fb.Guardian_Address ,
										fb.Guardian_Street ,
										fb.Guardian_Barangay ,
										fb.Guardian_TownCity ,
										fb.Guardian_Province ,
										fb.Guardian_ZipCode ,
										fb.Guardian_Address ,
										fb.Guardian_Street ,
										fb.Guardian_Barangay ,
										fb.Guardian_TownCity ,
										fb.Guardian_Province ,
										fb.Guardian_ZipCode ,
										fb.Father_Name ,
										fb.Father_Occupation ,
										fb.Father_Company ,
										fb.Father_CompanyAddress ,
										fb.Father_TelNo ,
										fb.Father_Email ,
										fb.Mother_Name ,
										fb.Mother_Occupation ,
										fb.Mother_Company ,
										fb.Mother_CompanyAddress ,
										fb.Mother_TelNo ,
										fb.Mother_Email ,
										fb.Guardian_Name ,
										fb.Guardian_RelationshipOthers ,
										fb.Guardian_Address ,
										fb.Guardian_Street ,
										fb.Guardian_Barangay ,
										fb.Guardian_TownCity ,
										fb.Guardian_Province ,
										fb.Guardian_ZipCode ,
										'' AS Guardian_Occupation ,
										'' AS Guardian_Company ,
										fb.Guardian_TelNo ,
										fb.Guardian_Email ,
										Elem_School ,
										Elem_Address ,
										Elem_InclDates ,
										HS_School ,
										HS_Address ,
										HS_InclDates ,
										Photo ,
										a.FamilyID
								FROM    ESv2_Admission AS a
						   INNER JOIN   ESv2_Admission_FamilyBackground AS fb ON a.FamilyID = fb.FamilyID
								WHERE   AppNo = '".$appno."' AND StatusID=2
									AND a.AppNo NOT IN ( SELECT top 1 AppNo FROM   ES_Students )";
		 		
		 		
		 		$exec = DB::statement($qry);
				
				return $exec;

			 } else {
				return 'exists';
			 }

	 } else {
		 return false;
	 }
	}

	public function deleteStudentProfileByAppNo($appno)
	{
		$d = DB::table('ES_Students')->where('AppNo', $appno)->delete();
		return $d;
	}
}