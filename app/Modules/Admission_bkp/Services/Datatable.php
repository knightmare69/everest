<?php
namespace App\Modules\Admission\Services;

use DB;
use Request;
use Response;

Class Datatable {

	public function __construct()
	{
		$this->model = DB::table('vw_K12_Admission');
	}

	public function filter()
	{
        $admissionstat = DB::table('ESv2_AdmissionStatus')->orderBy('SeqNo','asc');
		$hasParam      = false;
		
		$columns = array(
			'AllowToEnroll',
			'StatusDesc',
			'AppNo',
			'AppDate',
			'ApplicationType',
			'ApplicantName',
			'Gender',
			'DateOfBirth',
			'YearLevelName',
			'StrandTrackDesc',
			'TestingActualSched',
			'TestResultDesc',
			'MedicalResultDesc',
			'CompletionStatus',
			'PaymentStatus',
			'HasInterviewed'
		);

		$model = $this->model->select($columns);
		$gradeLevel = explode('-',Request::get('gradeLevel'));
		$gradeLevelSel = decode($gradeLevel[0]);
		
		if (!isParent()) {
			if (Request::get('filter')) {	
	 			$model->where('ApplicantName','like','%'.trimmed(Request::get('filter')).'%');
	 			$model->Orwhere('AppNo','like','%'.trimmed(Request::get('filter')).'%');
	 			// $model->Orwhere('StatusDesc','like','%'.trimmed(Request::get('filter')).'%');
	 			$hasParam = true;
	 		}

	 		//if (Request::get('schoolCampus')) {
	 			// $model->where('CampusID',decode(Request::get('schoolCampus')));
	 			// $hasParam = true;
	 		//}

	 		if (Request::get('schoolYear')) {
	 			$model->where('TermID',decode(Request::get('schoolYear')));
	 			$hasParam = true;
	 		}

	 		if (Request::get('admissionStatus')) {	
	 			$model->where('StatusID',decode(Request::get('admissionStatus')));
	 			$hasParam = true;
	 		}
		}
		

		$r = Request::all();
		$sort_col_index = $r['order'][0]['column'];
		$sort = $r['order'][0]['dir'];
		$colum_sorts = [1=>'[AllowToEnroll]', 2 => '[CompletionStatus]', 3 => '[StatusDesc]', 4 => '[AppNo]', 5 => '[AppDate]', 6 => '[ApplicationType]', 7 => '[ApplicantName]', 8 => '[Gender]', 9 => '[DateOfBirth]', 10 => '[YearLevelName]', 11 => '[TestingActualSched]', 12 => '[TestResultDesc]', 13 => '[MedicalResultDesc]', 14 => '[PaymentStatus]', 15 => '[HasInterviewed]'];
							if (isset($colum_sorts[$sort_col_index])) {
								if (is_array($colum_sorts[$sort_col_index])) {
									$arr = array_map(function ($a) use ($sort) { return $a.' '.$sort; } , $colum_sorts[$sort_col_index]);
									$sorting = implode(', ', $arr);

								} else {
									$sorting = $colum_sorts[$sort_col_index].' '.$sort;
								}
							}

		if (!isParent()) {
			if ($gradeLevelSel && $gradeLevelSel != 0) {
	 			$exec = DB::select("SELECT TOP 1 YLID_OldValue,ProgID FROM ESv2_Yearlevel WHERE YearlevelID='".$gradeLevelSel."'");
				if($exec){
				 $yrlvl  = $exec[0]->YLID_OldValue;
				 $progid = $exec[0]->ProgID;
				 $model->where('GradeLevelID',$yrlvl);
				 if ($progid > 0) {
				 	$model->where('ProgramID',$progid);
				 }
	 			}else{
				 $model->where('GradeLevelID',$gradeLevelSel);
				}
				$hasParam = true;
	 		}

	 		$IsHigherLevel = isset($gradeLevel[1]) ? $gradeLevel[1] : 0;

	 		if ($IsHigherLevel) {
	 			$model->where('ProgClass','>=',50);
	 		} else {
	 			$model->where('ProgClass','<',50);
	 		}
		}
        

 		if (isParent()) {
 			$model->where('FamilyID',getGuardianFamilyID());
 		}

 		//$model->orderBy('ApplicantName','ASC');
		
		if (IsPanel()) {
 			$model->where('a.StatusID',env('AO_APPROVED_STATUSID'));
 			$model->whereRaw("
 				(
 					select count(*) from esv2_admission_panel_approval as ap
 					inner join esv2_admission_panels as p
 					on p.id=ap.PanelID
 					where ap.AppNo=a.AppNo and p.UserID in(".getUserID().")
 				) <= 0"
 			);
 		}
		$iTotalRecords = $this->model->count();
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		  
		$records = array();
		$records["data"] = array(); 


		$end = $iDisplayStart + $iDisplayLength;
		$end = !$hasParam ? $end > $iTotalRecords ? $iTotalRecords : ($end == $iTotalRecords) ? 0 : $end : 0;
		if (!empty($sorting)) {
			$model=$model->orderByRaw($sorting);	
		}
		$data  = $model->offset($_REQUEST['start'])->limit($_REQUEST['length'])->get();
		$i=1;
		$drp   = '';
		foreach($admissionstat->get() as $row){
		 $drp  .= '<li class="">
					 <a href="javascript:void(0);" class="admstatus" onclick="Listing.updateStatus('."'".encode($row->StatusID)."'".');" data-val="'.encode($row->StatusID).'">
				     <i class="fa fa-pencil"></i>'.$row->StatusDesc.'</a>
				   </li>';
		} 
		foreach($data as $row) {
            //(strtolower($row->StatusDesc)=='admitted' || strtolower($row->StatusDesc)=='waitlisted')
            $enrol = (strtolower($row->StatusDesc)=='admitted')? '<a class="btn btn-xs btn-success" href="'.url('enrollment?AppNo='.$row->AppNo).'"><i class="fa fa-location-arrow"></i> OK to Enroll </a>' : '<span style="white-space:nowrap;"><a class="btn btn-xs default btn-editable" href="'.url('admission/redirectEdit?AppNo='.encode($row->AppNo)).'"><i class="fa fa-search"></i> View Profile</a>';
            $enrol = !isParent() ? $enrol : '<span style="white-space:nowrap;"><a class="btn btn-xs default btn-editable" href="'.url('admission/redirectEdit?AppNo='.encode($row->AppNo)).'"><i class="fa fa-search"></i> View Profile</a>';
            $chk_ispaid = $row->PaymentStatus ? "checked" : "";
            $chk_hasinterviewed = $row->HasInterviewed ? "checked" : "";
            $records['data'][] = [
			    $i,
				$enrol,
				'<a href="javascript:;" class="txt-center btn btn-sm btn-small '.($row->CompletionStatus <= 50 ? "btn-danger" : "btn-success").'">'.$row->CompletionStatus.'%</a>',
			    // (strtolower($row->StatusDesc)=='admitted' || strtolower($row->StatusDesc)=='waitlisted') ? '<a class="btn btn-xs btn-success" href="'.url('enrollment?AppNo='.$row->AppNo).'"><i class="fa fa-location-arrow"></i> Enroll </a>' : 'No',
				'<div class="btn-group '.(($i>5)?'dropup':'').'">
					<a class="btn btn-sm btn-default" href="#" data-toggle="dropdown" aria-expanded="false">
					<i class="fa fa-user"></i> '.$row->StatusDesc.' <i class="fa fa-angle-down "></i>
					</a>
				   <ul class="dropdown-menu pull-right" >'.$drp.'</ul>
				 </div>',
				$row->AppNo,
				explode(' ',$row->AppDate)[0],
				$row->ApplicationType,
				$row->ApplicantName,
				$row->Gender,
				setDate($row->DateOfBirth,'m-d-Y'),
				$row->YearLevelName,
				// $row->StrandTrackDesc,
				explode(' ',$row->TestingActualSched)[0],
				$row->TestResultDesc,
				$row->MedicalResultDesc,
				'<input type="checkbox" class="chk-ispaid" '.$chk_ispaid.'> Paid', 
				'<input type="checkbox" class="chk-hasinterviewed" '.$chk_hasinterviewed.'> Done'
			];
		    $i++;	
		}
		
		if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
		    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
		    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
		 }
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		$records["isParent"] = isParent();

		return $records;
	}
}	
?>