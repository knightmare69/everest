<?php
namespace App\Modules\Admission\Services\Completion;
use Validator;
use DB;

Trait Helper {
	
	public function getRequiredFields() {
		return [
			'required' => [
				'AppNo',
				'AppDate',
				'CampusID',
				'TermID',
				'AppTypeID',
				'GradeLevelID',
				'ProgramID',
				'MajorID',
				'LastName',
				'FirstName',
				'MiddleName',
				'MiddleInitial',
				'Gender',
				'DateOfBirth',
				'PlaceOfBirth',
				'CivilStatusID',
				'NationalityID',
				'ReligionID',
				'FamilyID',
				'ProgClass',
			],
			'optional' =>  [
				'gs' =>  ['Elem_School','Elem_Address','Elem_GradeLevel'],
				'hs' => ['HS_School','HS_Address','HS_GradeLevel']
			],
			'docs' => [
				env('DOCS_BIRTCERT'),
				env('DOCS_2X2ID'),
				env('DOCS_SCHOOLRECORDS'),
				env('DOCS_LTRRECOMMENDATION')
			]

		];
	} 

	private function isEmpty($value) {
        $value = trim($value);
        if (empty($value) || $value == null || $value == '') {
            return true;
        }
        return false;
    }

}	
?>