<?php namespace App\Modules\Admission\Services\Datatable;

use App\Modules\Admission\Models\AdmissionEvaluation;
use DB;
use Response;

Class EvaluationDatatable {
   
	public function __construct()
	{
		$this->evaluation = new AdmissionEvaluation;
	}

	public function filter()
	{
		$iTotalRecords = count($this->evaluation->where('AppNo',decode($_REQUEST['AppNo']))->get());
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		  
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$col='IndexID';
		$sort = $_REQUEST['order'][0]['dir'];
		switch ($_REQUEST['order'][0]['column']) {
			case '1':
				$col = 'DateInterviewed';
				break;
			case '2':
				$col = 'Interviewee';
				break;
			case '3':
				$col = 'Results';
				break;
			case '4':
				$col = 'Notes';
				break;
			case '5':
				$col = 'Remarks';
				break;
			case '6':
				$col = 'CreatedBy';
				break;
			case '7':
				$col = 'CreatedDate';
				break;
		}

		$data = collect($this->evaluation->where('AppNo',decode($_REQUEST['AppNo']))->get())
				->slice($iDisplayStart)
                ->take($iDisplayLength);
                if ($_REQUEST['order'][0]['dir'] == 'asc') {
                	$data->sortBy($col);
            	}
                if ($_REQUEST['order'][0]['dir'] == 'desc') {
                	$data->sortByDesc($col);
            	}

        $ret = $data;
        if ($_REQUEST['search']['value'] != '') {
			$filtered = $data->filter(function($value) {
    		return (stripos($value->DateInterviewed,$_REQUEST['search']['value']) !== false
    			|| stripos($value->Interviewee,$_REQUEST['search']['value']) !== false
    			|| stripos($value->Results,$_REQUEST['search']['value']) !== false
    			|| stripos($value->Notes,$_REQUEST['search']['value']) !== false
    			|| stripos($value->Remarks,$_REQUEST['search']['value']) !== false
    			);
			});

        	$ret = $filtered->all();
        } 

		foreach($ret as $row) {
			$records['data'][]=[
				'<input type="checkbox" name="id" value="'.encode($row->IndexID).'" class="chk-child">',
				'<a href="javascript:void(0)" class="a-select-evaluation">'.date_format(date_create($row->DateInterviewed),"m/d/Y").'</a>',
				$row->Interviewee == 'STUDENT' ? "<label class='badge bg-green'>".$row->Interviewee."</label>" : "<label class='badge bg-purple-plum'>".$row->Interviewee."</label>",
				$row->Results,
				$row->Notes,
				$row->Remarks,
				DB::table('ESv2_Users')->select('FullName')->where('UserIDX',$row->CreatedBy)->pluck('FullName'),
				date_format(date_create($row->CreatedDate),"m/d/Y")
			];
		}     
		
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		return Response::json($records);
	}


}	
?>

