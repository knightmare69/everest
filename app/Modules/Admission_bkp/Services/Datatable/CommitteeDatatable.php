<?php namespace App\Modules\Admission\Services\Datatable;

use App\Modules\Admission\Models\AdmissionCommittee;
use DB;
use Response;

Class CommitteeDatatable {
   
	public function __construct()
	{
		$this->committee = new AdmissionCommittee;
	}

	public function filter()
	{
		$iTotalRecords = count($this->committee->where('AppNo',decode($_REQUEST['AppNo']))->get());
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		  
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$col='IndexID';
		$sort = $_REQUEST['order'][0]['dir'];
		switch ($_REQUEST['order'][0]['column']) {
			case '1':
				$col = 'CommitteeRemarks';
				break;
			case '2':
				$col = 'IsAdmitted';
				break;
			case '3':
				$col = 'IsConditional';
				break;
			case '4':
				$col = 'IsWaitPool';
				break;
			case '5':
				$col = 'IsDenied';
				break;
			case '6':
				$col = 'CreatedBy';
				break;
			case '7':
				$col = 'CreatedDate';
				break;
		}

		$data = collect($this->committee->where('AppNo',decode($_REQUEST['AppNo']))->get())
				->slice($iDisplayStart)
                ->take($iDisplayLength);
                if ($_REQUEST['order'][0]['dir'] == 'asc') {
                	$data->sortBy($col);
            	}
                if ($_REQUEST['order'][0]['dir'] == 'desc') {
                	$data->sortByDesc($col);
            	}

        $ret = $data;
        if ($_REQUEST['search']['value'] != '') {
			$filtered = $data->filter(function($value) {
    		return (stripos($value->CommitteeRemarks,$_REQUEST['search']['value']) !== false);
			});

        	$ret = $filtered->all();
        } 

		foreach($ret as $row) {
			$records['data'][]=[
				'<input type="checkbox" name="id" value="'.encode($row->IndexID).'" class="chk-child">',
				'<a href="javascript:void(0)" class="a-select-committee">'.$row->CommitteeRemarks.'</a>',
				$row->IsAdmitted ? '<span class="label label-success label-round"><i class="fa fa-check"></i>&nbsp;</span>' : '<span class="label label-default label-round"><i class="fa fa-check"></i>&nbsp;</span>',
				$row->IsConditional ? '<span class="label label-success label-round"><i class="fa fa-check"></i>&nbsp;</span>' : '<span class="label label-default label-round"><i class="fa fa-check"></i>&nbsp;</span>',
				$row->IsWaitPool ? '<span class="label label-success label-round"><i class="fa fa-check"></i>&nbsp;</span>' : '<span class="label label-default label-round"><i class="fa fa-check"></i>&nbsp;</span>',
				$row->IsDenied ? '<span class="label label-success label-round"><i class="fa fa-check"></i>&nbsp;</span>' : '<span class="label label-default label-round"><i class="fa fa-check"></i>&nbsp;</span>',
				DB::table('ESv2_Users')->select('FullName')->where('UserIDX',$row->CreatedBy)->pluck('FullName'),
				date_format(date_create($row->CreatedDate),"m/d/Y")
			];
		}     
		
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		return Response::json($records);
	}


}	
?>

