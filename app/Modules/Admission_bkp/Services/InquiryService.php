<?php
namespace App\Modules\Admission\Services;


use Mail;
use sysAuth;
use DB;
use Request;

Trait InquiryService {

	protected $message = [
		'error' => 'Verification link is either invalid or verified already.',
		'success' => "You're inquiry was successfully verified. Please monitor your email. We will send you updates to your email."
	];
    
	public function isValid($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validate->inquiry($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		return ['error'=>false,'message'=> ''];
	}

	public function isValidChild($post,$action = 'save')
	{
		if ($action == 'save') {
			$validate = $this->validate->childInquiry($post);
			if ($validate->fails())
			{
				return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
			}
		}
		return ['error'=>false,'message'=> ''];
	}

	public function createParent()
	{
		$data = [
			'type' =>  $this->type,
			'fname' =>  Request::get('fname'),
			'mname'	=>  Request::get('mname'),
			'lname' =>  Request::get('lname',0),
			'email' =>  Request::get('email'),
			'contact_no' =>  Request::get('contact_no',0),
			// 'YearLevelID' =>  0,
			'CreatedBy'	=>  '',
			'CreatedDate' =>  systemDate(),
		];
		return $this->inquiryParentModel->create($data);
	}

	public function createChild($ParentKey)
	{
		$data = [
			'type' =>  $this->type,
			'TermID' => Request::get('schoolyear'),
			'InquiryParentID' => $ParentKey,
			'fname' =>  Request::get('fname'),
			'mname'	=>  Request::get('mname'),
			'lname' =>  Request::get('lname',0),
			'email' =>  '',
			'contact_no' =>  '',
			'YearLevelID' =>  Request::get('yearlevel'),
			'ProgClass' =>  Request::get('progclass'),
			'ScheduleType' =>  Request::get('schedule_type') ? 'tour' : 'inquiry',
			'CreatedBy'	=>  '',
			'CreatedDate' =>  systemDate(),
			'gender' => Request::get('gender'),
			'birth_date' => Request::get('bdate'),
		];
		return $this->inquiryChildModel->create($data);
	}

	public function create($id)
	{	
		$model = $this->inquiryTempModel->select(['fname','mname','lname','email','contact_no','type'])->where('id',$id);
		$temp = $model->first()->toArray();
		// receive inquiry when verified
		if($this->inquiryModel->insert($temp)) {
			// remove inquiry temp when verified
			$model->delete();
		}

		return;
	}
	
	public function email($post)
	{	
		ini_set('max_execution_time', 14400);
		Mail::send('email.inquiry_confirmation', ['post'=>$post], function ($message) use ($post) {
			$message->from(env('SYS_EMAIL'), 'K-12 Notification');
        	$message->to(getObjectValue($post,'email'))->subject('K to 12 Email Confirmation');
            $message->bcc(env('SYS_EMAIL'));
		});
		return 1;
	}

	public function emailSchedule($post)
	{	
		ini_set('max_execution_time', 14400);
		Mail::send('email.inquiry_confirmation', ['post'=>$post], function ($message) use ($post) {
			$message->from(env('SYS_EMAIL'), 'K-12 Notification');
        	$message->to(getObjectValue($post,'email'))->subject('K to 12 Email Confirmation');
            $message->bcc(env('SYS_EMAIL'));
		});
		return 1;
	}
}
?>
