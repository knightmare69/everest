<?php
namespace App\Modules\Admission\Services\Admission;

use App\Modules\Admission\Services\Admission\Helper;
use Validator;
use DB;

Class Repository {
	
    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function getPostRemarks($data)
    {
        return [
            'AppNo' => decode(getObjectValue($data,'AppNo')),
            'StatusID' => decode(getObjectValue($data,'status')),
            'Remarks' => getObjectValue($data,'remarks'),
            'CreatedBy' => getUserID(),
            'CreatedDate' => systemDate()
        ];
    }

    public function getPostPanelRemarks($data)
    {
        return [
            'AppNo' => decode(getObjectValue($data,'AppNo')),
            'Status' => getObjectValue($data,'status'),
            'Comment' => getObjectValue($data,'remarks'),
            'CreatedBy' => getUserID(),
            'CreatedDate' => systemDate()
        ];
    }

	public function guardianData($FamilyID,$data)
    {
        return [
            'FamilyID' => $FamilyID,
            'Guardian_Name' => getObjectValue($data,'name'),
            'Guardian_ParentMaritalID' => decode(getObjectValue($data,'marital')),
            'Guardian_LivingWith' => getObjectValue($data,'livingWith'),
            'Guardian_RelationshipOthers' => getObjectValue($data,'livingWithOther'),
            'Guardian_AddressFull' => getObjectValue($data,'resident').' '.getObjectValue($data,'street').', '.getObjectValue($data,'barangay').', '.getObjectValue($data,'city'),
            'Guardian_Address' => getObjectValue($data,'resident'),
            'Guardian_Street' => getObjectValue($data,'street'),
            'Guardian_Barangay' => getObjectValue($data,'barangay'),
            'Guardian_TownCity' => getObjectValue($data,'GuardianCity'),
            'Guardian_Province' => getObjectValue($data,'province'),
            'Guardian_ZipCode' => getObjectValue($data,'zipcode'),
            'Guardian_TelNo' => getObjectValue($data,'telephone'),
            'Guardian_Mobile' => getObjectValue($data,'mobile'),
            'Guardian_Email' => getObjectValue($data,'email'),
            'Guardian_CountryCode' => getObjectValue($data,'GuardianCountry'),
            'Guardian_CityID' => getObjectValue($data,'GuardianCitySel'),
            'Guardian_isOtherCity' => getObjectValue($data,'isOtherCity') ? 1 : 0,
            'Guardian_OtherCity' => getObjectValue($data,'otherCity'),
            'Guardian_Billing_AddressFull' => getObjectValue($data,'BillingResident').' '.getObjectValue($data,'BillingStreet').', '.getObjectValue($data,'BillingBrangay').', '.getObjectValue($data,'BillingTownCity'),
            'Guardian_Billing_Address'  => getObjectValue($data,'BillingResident'),
            'Guardian_Billing_Street'  => getObjectValue($data,'BillingStreet'),
            'Guardian_Billing_Barangay'  => getObjectValue($data,'BillingBrangay'),
            'Guardian_Billing_TownCity'  => getObjectValue($data,'BillingTownCity'),
            'Guardian_Billing_CountryCode'  => getObjectValue($data,'BillingCountry'),
            'Guardian_Billing_CityID'  => getObjectValue($data,'BillingCitySel'),
            'Guardian_Billing_IsOtherCity'  => getObjectValue($data,'BillingIsOtherCity') ? 1 : 0,
            'Guardian_Billing_OtherCity'  => getObjectValue($data,'BillingOtherCity'),
        ];
    }

    public function getGuardianData($FamilyID,$data)
    {
        return [
            'FamilyID' => $FamilyID,
            'Guardian_Name' => getObjectValue($data,'name'),
            'Guardian_BirthDate' => setDate(getObjectValue($data,'GuardianDateOfBirth'),'Y-m-d'),
            'Guardian_ParentMaritalID' => decode(getObjectValue($data,'marital')),
            'Guardian_LivingWith' => getObjectValue($data,'livingWith'),
            'Guardian_RelationshipOthers' => getObjectValue($data,'livingWithOther'),
            'Guardian_AddressFull' => getObjectValue($data,'resident').' '.getObjectValue($data,'street').', '.getObjectValue($data,'barangay').', '.getObjectValue($data,'GuardianCity'),
            'Guardian_Address' => getObjectValue($data,'resident'),
            'Guardian_Street' => getObjectValue($data,'street'),
            'Guardian_Barangay' => getObjectValue($data,'barangay'),
            'Guardian_TownCity' => getObjectValue($data,'GuardianCity'),
            'Guardian_Province' => getObjectValue($data,'province'),
            'Guardian_ZipCode' => getObjectValue($data,'zipcode'),
            'Guardian_TelNo' => getObjectValue($data,'telephone'),
            'Guardian_Mobile' => getObjectValue($data,'mobile'),
            'Guardian_Email' => getObjectValue($data,'email'),
            'Guardian_CountryCode' => getObjectValue($data,'GuardianCountry'),
            'Guardian_CityID' => getObjectValue($data,'GuardianCitySel'),
            'Guardian_isOtherCity' => getObjectValue($data,'isOtherCity') ? 1 : 0,
            'Guardian_OtherCity' => getObjectValue($data,'otherCity'),
            'Guardian_Billing_AddressFull' => getObjectValue($data,'BillingResident').' '.getObjectValue($data,'BillingStreet').', '.getObjectValue($data,'BillingBrangay').', '.getObjectValue($data,'BillingCity'),
            'Guardian_Billing_Address'  => getObjectValue($data,'BillingResident'),
            'Guardian_Billing_Street'  => getObjectValue($data,'BillingStreet'),
            'Guardian_Billing_Barangay'  => getObjectValue($data,'BillingBrangay'),
            'Guardian_Billing_TownCity'  => getObjectValue($data,'BillingCity'),
            'Guardian_Billing_CountryCode'  => getObjectValue($data,'BillingCountry'),
            'Guardian_Billing_CityID'  => getObjectValue($data,'BillingCitySel'),
            'Guardian_Billing_IsOtherCity'  => getObjectValue($data,'BillingIsOtherCity') ? 1 : 0,
            'Guardian_Billing_OtherCity'  => getObjectValue($data,'BillingOtherCity'),
            'FatherMother_MarriageDate' => setDate(getObjectValue($data,'DateOfMarriage'),'Y-m-d'),
            'EmergencyContact_Name' => getObjectValue($data,'EmergencyContactName'),
            'EmergencyContact_Relationship' => getObjectValue($data,'EmergencyContactRelationship'),
            'EmergencyContact_Address' => getObjectValue($data,'EmergencyContactAddress'),
            'EmergencyContact_Email' => getObjectValue($data,'EmergencyContactEmail'),
            'EmergencyContact_TelNo' => getObjectValue($data,'EmergencyContactTelNo'),
            'EmergencyContact_Mobile' => getObjectValue($data,'EmergencyContactMobile'),
            'EmergencyContact_CompanyPhone' => getObjectValue($data,'EmergencyContactCompanyPhone'),
        ];
    }

    public function getFatherData($FamilyID,$data)
    {
        return [
            'FamilyID' => $FamilyID,
            'Father_Name' => getObjectValue($data,'FatherName'),
            'Father_BirthDate' => setDate(getObjectValue($data,'FatherDateOfBirth'),'Y-m-d'),
            'Father_MaritalID' => decode(getObjectValue($data,'FatherMarital')),
            'Father_Occupation' => getObjectValue($data,'FatherOccupation'),
            'Father_Company' =>getObjectValue($data,'FatherCompany'),
            'Father_CompanyAddress' => getObjectValue($data,'FatherCompanyAddress'),
            'Father_CompanyPhone' => getObjectValue($data,'FatherCompanyPhone'),
            'Father_Email' => getObjectValue($data,'FatherEmailAddress'),
            'Father_TelNo' => getObjectValue($data,'FatherTelNo'),
            'Father_Mobile' => getObjectValue($data,'FatherMobile'),
            'Father_Address' => getObjectValue($data,'FatherAddress'),
            'Father_EducAttainment' => getObjectValue($data,'FatherEducation'),
            // 'Father_SchoolAttended' => getObjectValue($data,'FatherSchool'),
            'Father_ReligionID' => decode(getObjectValue($data,'FatherReligion')),
            'Father_BusinessType' => getObjectValue($data,'FatherBusinessType'),
            'Father_NationalityID' => decode(getObjectValue($data,'FatherNationality')),
            'Father_CitizenshipID' => decode(getObjectValue($data,'FatherCitizenship')),
        ];
    }

    public function getMotherData($FamilyID,$data)
    {
        return [
            'FamilyID' => $FamilyID,
            'Mother_Name' => getObjectValue($data,'MotherName'),
            'Mother_BirthDate' => setDate(getObjectValue($data,'MotherDateOfBirth'),'Y-m-d'),
            'Mother_MaritalID' => decode(getObjectValue($data,'MotherMarital')),
            'Mother_Occupation' => getObjectValue($data,'MotherOccupation'),
            'Mother_Company' =>getObjectValue($data,'MotherCompany'),
            'Mother_CompanyAddress' => getObjectValue($data,'MotherCompanyAddress'),
            'Mother_CompanyPhone' => getObjectValue($data,'MotherCompanyPhone'),
            'Mother_Email' => getObjectValue($data,'MotherEmailAddress'),
            'Mother_TelNo' => getObjectValue($data,'MotherTelNo'),
            'Mother_Mobile' => getObjectValue($data,'MotherMobile'),
            'Mother_Address' => getObjectValue($data,'MotherAddress'),
            'Mother_EducAttainment' => getObjectValue($data,'MotherEducation'),
            // 'Mother_SchoolAttended' => getObjectValue($data,'MotherSchool'),
            'Mother_ReligionID' => decode(getObjectValue($data,'MotherReligion')),
            'Mother_BusinessType' => getObjectValue($data,'MotherBusinessType'),
            'Mother_NationalityID' => decode(getObjectValue($data,'MotherNationality')),
            'Mother_CitizenshipID' => decode(getObjectValue($data,'MotherCitizenship')),
        ];
    }

    public function getFamilyCustoms($FamilyID,$data)
    {
        $retData = [];
        foreach($data as $row) {
            $retData[]= [
                'FamilyID' => encode($FamilyID),
                'GuardianName' => $row->Guardian_Name,
                'ParentMaritalID' => encode($row->Guardian_ParentMaritalID),
                'Guardian_LivingWith' => $row->Guardian_LivingWith,
                'GDOB' => explode(' ',$row->Guardian_BirthDate)[0],
                'RelationshipOthers' => $row->Guardian_RelationshipOthers,
                'AddressFull' => $row->Guardian_AddressFull,
                'Address' => $row->Guardian_Address,
                'Street' => $row->Guardian_Street,
                'Barangay' =>  $row->Guardian_Barangay,
                'TownCity' => $row->Guardian_TownCity,
                'CityID' => $row->Guardian_CityID,
                'Province' => $row->Guardian_Province,
                'BillingAddress' => $row->Guardian_Billing_Address,
                'BillingStreet' => $row->Guardian_Billing_Street,
                'BillingBarangay' =>  $row->Guardian_Billing_Barangay,
                'BillingTownCity' => $row->Guardian_Billing_TownCity,
                'BillingCityID' => $row->Guardian_Billing_CityID,
                'Province' => $row->Guardian_Province,
                'ZipCode' => $row->Guardian_ZipCode,
                'TelNo' => $row->Guardian_TelNo,
                'Mobile' => $row->Guardian_Mobile,
                'Email' => $row->Guardian_Email,
                'GuardianCountryCode' => !trim($row->Guardian_CountryCode) ? defaultCountry() : $row->Guardian_CountryCode,
                'GuardianBillingCode' => !trim($row->Guardian_Billing_CountryCode) ? defaultCountry() : $row->Guardian_Billing_CountryCode,

                'FName' => $row->Father_Name,
                'FDOB' => explode(' ',$row->Father_BirthDate)[0],
                'FOccupation' => $row->Father_Occupation,
                'FMaritalID' => encode($row->Father_MaritalID),
                'FCompany' => $row->Father_Company,
                'FBusinessType' => $row->Father_BusinessType,
                'FAddress' => $row->Father_Address,
                'FCompanyAddress' => $row->Father_CompanyAddress,
                'FCompanyPhone' => $row->Father_CompanyPhone,
                'FEmail' => $row->Father_Email,
                'FTelNo' => $row->Father_TelNo,
                'FMobile' => $row->Father_Mobile,
                'FEducAttainment' => $row->Father_EducAttainment,
                'FReligion' => $row->Father_ReligionID,
                'FNationality' => $row->Father_NationalityID,
                'FCitizenship' => $row->Father_CitizenshipID,
                'FSchoolAttended' => $row->Father_SchoolAttended,

                'MName' => $row->Mother_Name,
                'MDOB' => explode(' ',$row->Mother_BirthDate)[0],
                'MOccupation' => $row->Mother_Occupation,
                'MMaritalID' => encode($row->Mother_MaritalID),
                'MCompany' => $row->Mother_Company,
                'MBusinessType' => $row->Mother_BusinessType,
                'MAddress' => $row->Mother_Address,
                'MCompanyAddress' => $row->Mother_CompanyAddress,
                'MCompanyPhone' => $row->Mother_CompanyPhone,
                'MEmail' => $row->Mother_Email,
                'MTelNo' => $row->Mother_TelNo,
                'MMobile' => $row->Mother_Mobile,
                'MEducAttainment' => $row->Mother_EducAttainment,
                'MReligion' => $row->Mother_ReligionID,
                'MNationality' => $row->Mother_NationalityID,
                'MCitizenship' => $row->Mother_CitizenshipID,
                'MSchoolAttended' => $row->Mother_SchoolAttended,
                'HasAlreadyAccount' => $this->helper->hasLoginAccount($FamilyID),
                'LoginAccount' => $this->gerLoginAccount($FamilyID),

                'MarriageDate' => $row->FatherMother_MarriageDate,
                'ECName' => $row->EmergencyContact_Name,
                'ECRelationship' => $row->EmergencyContact_Relationship,
                'ECAddress' => $row->EmergencyContact_Address,
                'ECTelNo' => $row->EmergencyContact_TelNo,
                'ECEmail' => $row->EmergencyContact_Email,
                'ECMobile' => $row->EmergencyContact_Mobile,
                'ECCompanyPhone' => $row->EmergencyContact_CompanyPhone
            ];
        }
        return isset($retData[0]) ? $retData[0] : [];
    }

    public function getQuickData($FamilyID,$data,$AppNo = '') 
    {
        return [
            'AppNo' =>  !$AppNo ? $this->helper->getAppNo($data) : $AppNo,
            'AppDate' => date('Y-m-d'),
            'CampusID' => ((decode(getObjectValue($data,'schoolCampus'))==0)?1:decode(getObjectValue($data,'schoolCampus'))),
            'TermID' => decode(getObjectValue($data,'schoolYear')),
            'AppTypeID' => decode(getObjectValue($data,'applicationType')),
            'GradeLevelID' => decode(getObjectValue($data,'gradeLevel')),
            'ProgramID' => decode(getObjectValue($data,'gradeProgramID')),
            'MajorID' => decode(getObjectValue($data,'ProgramMajor')),
            'LastName' => getObjectValue($data,'LastName'),
            'FirstName' => getObjectValue($data,'FirstName'),
            'MiddleName' => getObjectValue($data,'MiddleName'),
            'MiddleInitial' => substr(getObjectValue($data,'MiddleName'),1,1),
            'ExtName' => getObjectValue($data,'ExtensionName'),
            'Gender' => getObjectValue($data,'Gender'),
            'DateOfBirth' => setDate(getObjectValue($data,'DateOfBirth'),'Y-m-d'),
            'PlaceOfBirth' => getObjectValue($data,'PlaceOfBirth'),
            'CivilStatusID' => decode(getObjectValue($data,'CivilStatus')),
            'NationalityID' => decode(getObjectValue($data,'Nationality')),
            'ForeignStudent' => getObjectValue($data,'isForeign'),
            'PassportNumber' => getObjectValue($data,'PassportNo'),
            'VisaStatus' => getObjectValue($data,'VisaStatus'),
            'SSP_Number' => getObjectValue($data,'SSPNo'),
            'Validity_of_Stay' => getObjectValue($data,'ValidityStay'),
            'ReligionID' => decode(getObjectValue($data,'Religion')),
            'FamilyID' => $FamilyID ? $FamilyID : getObjectValue($data,'familyid') ,
            'DenyReason' => getObjectValue($data,'DenyReason'),
            'Elem_School' => getObjectValue($data,'ElemSchool'),
            'Elem_Address' => getObjectValue($data,'ElemSchoolAddress'),
            'Elem_InclDates' => getObjectValue($data,'ElemSchoolIncDate'),
            'Elem_GradeLevel' => getObjectValue($data,'ElemGradeLevel'),
            'Elem_GWA' => getObjectValue($data,'ElemGWA'),
            'Elem_AwardHonor' => getObjectValue($data,'ElemAwardHonor'),
            'HS_School' => getObjectValue($data,'HighSchool'),
            'HS_Address' => getObjectValue($data,'HighSchoolAddress'),
            'HS_InclDates' => getObjectValue($data,'HighSchoolIncDate'),
            'HS_GradeLevel' => getObjectValue($data,'HighSchoolGradeLevel'),
            'HS_GWA' => getObjectValue($data,'HighSchoolGWA'),
            'HS_AwardHonor' => getObjectValue($data,'HighSchoolAwardHonor'),
            'TestingSchedID' => decode(getObjectValue($data,'ScheduleDate')),
            'ORNo' => getObjectValue($data,'ORCode'),
            'ORSecurityCode' => getObjectValue($data,'ORSecurityCode'),
            'CreatedDate' => systemDate(),
            'CreatedBy' => getUserID(),
            'IsOnlineApplication' => isParent() ? 1 : 0,
            // 'StatusID' => getObjectValue($data,'isTemp'),
            'StatusID' => $this->helper->getStatus(getObjectValue($data,'isTemp'),getObjectValue($data,'AdmissionStatus')),
            'ESigInitial' => getObjectValue($data,'ParentInitial'),
            'ESigDate' => getObjectValue($data,'ParentInitialDate'),
            'LRN' => getObjectValue($data,'LRN'),
            'IsCollege' => strtolower(getObjectValue($data,'EducLevel')) == 'higher' ? 1 : 0,
            'ProgClass' => (getObjectValue($data,'SetProgClass')),
            
        ];
    }

    public function getSubmissionData($FamilyID,$data,$AppNo = '') 
    {
        return [
            'AppNo' =>  !$AppNo ? $this->helper->getAppNo($data) : $AppNo,
            'AppDate' => date('Y-m-d'),
            'CampusID' => ((decode(getObjectValue($data,'schoolCampus'))==0)?1:decode(getObjectValue($data,'schoolCampus'))),
            'TermID' => decode(getObjectValue($data,'schoolYear')),
            'AppTypeID' => getObjectValue($data,'wExistSib') ? decode(getObjectValue($data,'applicationType')) == 1 ? 3 : 4 : decode(getObjectValue($data,'applicationType')),
            'GradeLevelID' => decode(getObjectValue($data,'gradeLevel')),
            'ProgramID' => decode(getObjectValue($data,'gradeProgramID')),
            'MajorID' => decode(getObjectValue($data,'gradeMajorID')),
            'LastName' => getObjectValue($data,'LastName'),
            'FirstName' => getObjectValue($data,'FirstName'),
            'MiddleName' => getObjectValue($data,'MiddleName'),
            'MiddleInitial' => getObjectValue($data,'MiddleInitial'),
            'ExtName' => getObjectValue($data,'ExtensionName'),
            'NickName' => getObjectValue($data,'NickName'),
            'Gender' => getObjectValue($data,'Gender'),
            'DateOfBirth' => setDate(getObjectValue($data,'DateOfBirth'),'Y-m-d'),
            'PlaceOfBirth' => getObjectValue($data,'PlaceOfBirth'),
            'IsAdopted' => getObjectValue($data,'isAdopted') ? 1 : 0,
            'HomeAddressUnitNo' => getObjectValue($data,'HomeAddressUnitNo'),
            'HomeAddressStreet' => getObjectValue($data,'HomeAddressStreet'),
            'HomeAddressBrgy' => getObjectValue($data,'HomeAddressBrgy'),
            'HomeAddressCity' => getObjectValue($data,'HomeAddressCity'),
            'HomeAddressZipCode' => getObjectValue($data,'HomeAddressZipCode'),
            'HomePhone' => getObjectValue($data,'HomePhone'),
            'CivilStatusID' => decode(getObjectValue($data,'CivilStatus')),
            'NationalityID' => decode(getObjectValue($data,'Nationality')),
            'CitizenshipID' => decode(getObjectValue($data,'Citizenship')),
            'ForeignStudent' => getObjectValue($data,'isForeign'),
            'IsForeignCitizen' => getObjectValue($data,'isForeignCitizen'),
            'PassportNumber' => getObjectValue($data,'PassportNo'),
            'VisaStatus' => getObjectValue($data,'VisaStatus'),
            'SSP_Number' => getObjectValue($data,'SSPNo'),
            'Validity_of_Stay' => getObjectValue($data,'ValidityStay'),
            'ReligionID' => decode(getObjectValue($data,'Religion')),
            'FamilyID' => $FamilyID,
            'DenyReason' => getObjectValue($data,'DenyReason'),
            'Elem_School' => getObjectValue($data,'ElemSchool'),
            'Elem_Address' => getObjectValue($data,'ElemSchoolAddress'),
            'Elem_InclDates' => getObjectValue($data,'ElemSchoolIncDate'),
            'Elem_GradeLevel' => getObjectValue($data,'ElemGradeLevel'),
            'Elem_GWA' => getObjectValue($data,'ElemGWA'),
            'Elem_AwardHonor' => getObjectValue($data,'ElemAwardHonor'),
            'HS_School' => getObjectValue($data,'HighSchool'),
            'HS_Address' => getObjectValue($data,'HighSchoolAddress'),
            'HS_InclDates' => getObjectValue($data,'HighSchoolIncDate'),
            'HS_GradeLevel' => getObjectValue($data,'HighSchoolGradeLevel'),
            'HS_GWA' => getObjectValue($data,'HighSchoolGWA'),
            'HS_AwardHonor' => getObjectValue($data,'HighSchoolAwardHonor'),
            'TestingSchedID' => decode(getObjectValue($data,'ScheduleDate')),
            'ORNo' => getObjectValue($data,'ORCode'),
            'ORSecurityCode' => getObjectValue($data,'ORSecurityCode'),
            'CreatedDate' => systemDate(),
            'CreatedBy' => getUserID(),
            'IsOnlineApplication' => isParent() ? 1 : 0,
            // 'StatusID' => getObjectValue($data,'isTemp'),
            'StatusID' => $this->helper->getStatus(getObjectValue($data,'isTemp'),getObjectValue($data,'AdmissionStatus')),
            'ESigInitial' => getObjectValue($data,'ParentInitial'),
            'ESigDate' => getObjectValue($data,'ParentInitialDate'),
            'LRN' => getObjectValue($data,'LRN'),
            'IsCollege' => strtolower(getObjectValue($data,'EducLevel')) == 'higher' ? 1 : 0,
            'ProgClass' => (getObjectValue($data,'SetProgClass')),
            'Family_HealthStatus' => getObjectValue($data,'parentHealthStatus1').','.getObjectValue($data,'parentHealthStatus2').','.getObjectValue($data,'parentHealthStatus3').','.getObjectValue($data,'parentHealthStatus4'),
            'Family_Situation' => getObjectValue($data,'familySituation'),
            'Family_Correspondence' => getObjectValue($data,'FamilyCorrespondence'),
            'Familly_Activities' => getObjectValue($data,'FamilyActivities'),
            'Family_PaymentResponsible' => getObjectValue($data,'paymentsResponsible'),
            'IsBaptized' => getObjectValue($data,'isBaptized') ? 1 : 0,
            'BaptizedIn_Date' => setDate(getObjectValue($data,'BaptizedDate'),'Y-m-d'),
            'BaptizedIn_Church' => getObjectValue($data,'BaptizedChurch'),
            'BaptizedIn_City' => getObjectValue($data,'BaptizedCity'),
            'WorshipPlace' => getObjectValue($data,'WorshipPlace'),
            'WorshipAddress' => getObjectValue($data,'WorshipAddress'),
            'WorshipTelNo' => getObjectValue($data,'WorshipTelNo'),
            'IsAttendingKdg' => getObjectValue($data,'isAttendingKdg') ? 1 : 0,
            'PresentSchool' => getObjectValue($data,'PresentSchool'),
            'PresentSchoolYrsProgram' => getObjectValue($data,'PresentSchoolYrsProgram'),
            'PresentSchoolCurriculum' => getObjectValue($data,'PresentSchoolCurriculum'),
            'PresentSchoolCurriculumOthers' => getObjectValue($data,'PresentSchoolCurriculumOthers'),
            'PresentSchoolHead' => getObjectValue($data,'PresentSchoolHead'),
            'PresentSchoolAddress' => getObjectValue($data,'PresentSchoolAddress'),
            'PresentSchoolContactNo' => getObjectValue($data,'PresentSchoolPhone'),
            'PresentSchoolWebsite' => getObjectValue($data,'PresentSchoolWebsite'),
            'PresentGradeLevel' => getObjectValue($data,'PresentGradeLevel'),
            'PresentSchoolDateAttended' => getObjectValue($data,'LastMonthAttended').','.getObjectValue($data,'LastYearAttended'),
            'PresentSchoolCalendarMonths' => getObjectValue($data,'PresentSchoolCalendarMonths'),
            'PresentSchoolReasonLeaving' => getObjectValue($data,'PresentSchoolReasonLeaving'),
            'PresentSchoolReasonReApplying' => getObjectValue($data,'PresentSchoolReasonReApplying'),
            'PresentSchoolPassAllSubjects' => getObjectValue($data,'PassAllSubjects'),
            'PresentSchoolSubjectsFailing' => getObjectValue($data,'SubjectsFailing'),
        ];
    }

    public function getHigherYearLevel()
    {
        $data = [];
        foreach($this->helper->getHigherYearLevel() as $row) {
            $data[] = [
                'id' => encode($row->YearLevelID),
                'name' => $row->YearLevelName,
                'class' => $row->ProgClass,
            ];
        }
        return $data;
    }

    public function getBasicYearLevel()
    {
        $data = [];
        foreach($this->helper->getBasicYearLevel() as $row) {
            $data[] = [
                'id' => encode($row->YearLevelID),
                'name' => $row->YearLevelName,
                'class' => ($row->ProgClass),
                'prog' => encode($row->ProgID),
            ];
        }
        return $data;
    }

    public function getCourses()
    {
        $data = [];
        foreach($this->helper->getCourses() as $row) {
            $data[] = [
                'id' => encode($row->ProgID),
                'mid' => encode($row->MajorID),
                'course' => $row->course,
            ];
        }
        return $data;
    }

    public function gerLoginAccount($FamilyID)
    {
        $data = [];
        foreach($this->helper->gerLoginAccount($FamilyID) as $row) {
            $data = [
                'UserID' => encode($row->UserIDX),
                'UserName' => $row->Username,
            ];
        }
        return $data;
    }
	
	public function isExisting($data){
	  //created by: JEREMIAH JAMES SAMSON - 2017-07-31
      $lname = getObjectValue($data,'LastName');
      $lname = str_replace("'", "`", $lname);
      $fname = getObjectValue($data,'FirstName');
      $fname = str_replace("'", "`", $fname);
      $bdate = getObjectValue($data,'DateOfBirth');
      $exec  = DB::select("SELECT COUNT(AppNo) as Items FROM ESv2_Admission WHERE DateOfBirth='".$bdate."' AND LastName='".$lname."' AND FirstName LIKE '%".$fname."%'");
      return (($exec && count($exec))?$exec[0]->Items:0);	  
	}

}	
?>