<?php
// error_print($progs);
// die();
?>
<div class="row">
    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i>Report Settings
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                <form class="horizontal-form" id="report-form" action="#" method="POST">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Programs</label>
                                    <select class="select2 form-control" name="programs" id="programs">
                                        <option value="" disabled selected></option>
                                        @if(!empty($progs))
                                            @foreach($progs as $_this)
                                            <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                                                {{ $_this->ProgName }}
                                            </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Report Type</label>
                                    <select class="select2 form-control" name="report-type" id="report-type">
                                        <!-- <option value="" selected>Please Select</option> -->
                                        @foreach($rep_types as $key => $_this)
                                        <option value="{{ $key + 1 }}">{{ $_this }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Academic Term</label>
                                    <select class="select2 form-control" name="academic-term" id="academic-term">
                                        @if(!empty($at))
                                            @foreach($at as $_this)
                                            <option value="{{ encode($_this->TermID) }}">{{ $_this->AcademicYear.' - '.$_this->SchoolTerm }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Quarter / Period</label>
                                    <select class="select2 form-control" name="period" id="period">
                                        <?php $qtr = App\Modules\Registrar\Services\RegistrarServiceProvider::Period(); ?>
                                        @foreach($qtr as $key => $_this)
                                        <option value="{{ encode($_this->PeriodID) }}" data-group="{{ $_this->GroupClass }}" class="{{ ($_this->GroupClass == 21) ? 'hide' : '' }}">
                                            {{ ($_this->GroupClass == 21) ? $_this->Description1 : $_this->Description2 }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Year Level</label>
                                    <select class="select2 form-control" name="year-level" id="year-level" disabled>
                                        @include($views.'sub/year-level')
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Section</label>
                                    <select class="select2 form-control" name="section" id="section" disabled>
                                        @include($views.'sub/section')
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                         <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Student(s)</label>
                                <div class="input-group">
                                    <input type="text" class="form-control stud-find" data-snum="" name="student-name" id="student-name" disabled>
                                    <span class="input-group-btn">
                                        <button type="button" class="faculty-search-btn btn btn-default stud-find" id="student-find" disabled>
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="faculty-search-btn btn btn-default stud-remove" id="student-remove">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <input type="checkbox" name="print-empty-subj"></input> Exclude subjects with empty grades?
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="reply-slip">
                                <label>
                                    <input type="checkbox" name="reply-slip" id="reply-slip" class="form-control"></input> With Reply slip?
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"  id="exclude-gwa"">
                                <label>
                                    <input type="checkbox" name="excludegwa" id="exclude-gwa" class="form-control"></input> Exclude GPA/GWA.
                                </label>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <div class="form-actions right">
                        <button class="btn pull-left default" style="margin-left: 10px;" id="btnsetup" type="button"><i class="fa fa-cog"></i> Setup </button>
                        <!--<button class="btn default" id="export" type="button">Export</button>-->
                        <button class="btn blue btn_action btn_save" type="button" id="print-report"><i class="fa fa-print"></i> Print Report</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include($views.'sub.modal')