@foreach($data as $r)
    <tr data-term="{{$r->TermID}}" data-prog="{{$r->ProgID}}" data-major="{{$r->MajorID}}" data-level="{{$r->YearLevelID}}" >
        <td>{{$r->YearLevel}}</td>
        <td>{{$r->Major}}</td>
        <td class="sect"><input class="form-control  text-center input-borderless input-sm input-small "  value="{{$r->NoSections}}" /></td>
        <td class="stud"><input class="form-control  text-center input-borderless input-sm input-small" value="{{$r->NoStudents}}" /></td>        
    </tr>
@endforeach