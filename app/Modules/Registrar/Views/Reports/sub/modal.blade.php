<div id="setup_modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" ><div class="modal-content">
    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Select Student</h4></div>
    <div class="modal-body">       
        <table id="tblprojection" class="table table-striped table-bordered table-hover table_scroll dataTable table-condensed no-footer" style="cursor: pointer; margin-bottom: 0px !important;">
            <thead>
                <tr>
                    <th>Year Level</th>
                    <th>Strand/Track</th>
                    <th>No. of Class Section/s</th>
                    <th>No. of Student/s</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn btn-success btnsaveproj" data-menu="save" type="button"> Save </button>
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i> Close</button>
    </div></div></div>
</div>