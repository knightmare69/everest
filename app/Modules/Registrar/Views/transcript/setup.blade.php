<div class="portlet light ">
    <div class="portlet-title">
    	<div class="caption caption-md">
    		<i class="icon-bar-chart theme-font font-blue-madison"></i>
    		<span class="caption-subject theme-font font-blue-madison bold uppercase">Transcript Setup</span>
    		<span class="hide caption-helper">This module is intent for crediting of subject</span>
    	</div>
    	<div class="actions">
            <button type="button" class="btn btn-default btn-sm" data-menu="print-transcript"><i class="fa fa-check-square-o"></i> Print Transcript </button>    	
            <button type="button" class="btn btn-default" id="reload" ><i class="fa fa-refresh"></i> Refresh </button>        
    	</div>
    </div>
	<div class="portlet-body" >
        <div class="well ">
        </div>
		
	</div>
</div>