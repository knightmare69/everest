<?php 
    $service = new \App\Modules\Registrar\Services\EvaluationServices;
    
    $i=1;
    $model = new \App\Modules\ClassRecords\Models\ReportCardDetails_model;
    $yearterm = '';
    $credits = 0;
    
?>
@foreach($data as $r)
    <?php
        $rs = $model->select('Final_Average','Final_Remarks')->where(['SubjectID' => $r->SubjectID, 'StudentNo' => $idno ])->get();
        $credited = 0;
        $grade = '';
        $remarks = '';
        $status = '';
        $credits += floatval($r->Credit);
        $prereq = $service->getPrereqSubCode($r->IndexID, $r->SubjectID);
        if(count($rs) > 0) {
                    
            foreach($rs as $g){
                if( strtolower($g->Final_Remarks) == 'passed' ){
                    $grade = number_format($g->Final_Average,0);
                    $remarks = $g->Final_Remarks;
                    $credited = 1;    
                }elseif(strtolower($g->Final_Remarks) == 'credited'){
                    $grade = 'CRD';
                    $remarks = 'Credited';
                    $credited = 1;
                    $status = 'font-blue-hoki bold';
                }else{
                    if($g->Final_Average !=''){                                            
                        $grade = number_format($g->Final_Average,0);
                        $remarks = $g->Final_Remarks;
                        $credited = 0;    
                        $status = 'text-danger';
                    }
                }
            }                          
        }
        
        if( $yearterm == '') $yearterm = $r->YearTerm; 
    ?>
    @if($yearterm != $r->YearTerm)  
          
    <tr class="warning" >
        <td class="text-right" colspan="4"> Total : </td>
        <td class="text-center bold">{{$credits}}</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
        <?php 
            $i = 1; 
            $credits= 0;
            $yearterm = $r->YearTerm; 
        ?>
        
    @endif
    
    <tr data-id="{{$r->SubjectID}}" data-credited="{{$credited}}"  class="{{$status}}" data-code="{{$r->SubjectCode}}">
        <td class="autofit font-xs"><?php echo $i; $i++; ?>.</td>
        <td class="autofit">{{$r->YearTerm}}</td>
        <td class="autofit">{{$r->SubjectCode}}</td>        
        <td>{{$r->SubjectTitle}}</td>
        <td class="autofit text-center">{{$r->Credit}}</td>
        @if($r->IsPassFail == '0')
            <td class="grade text-center bold">{{$grade}}</td>
            <td class="remarks text-center bold">{{$remarks}}</td>
        @else
            <td colspan="2" class="remarks text-center bold">{{$remarks}}</td>
        @endif          
        <td class="font-xs">{{$prereq}}</td>              
    </tr>    
@endforeach
<tr class="warning" >
    <td class="" colspan="4"> Average : </td>
    <td class="text-center bold">{{$credits}}</td>
    <td></td>
    <td></td>
    <td></td>
</tr>