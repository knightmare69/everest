<?php 
    $service = new \App\Modules\Registrar\Services\EvaluationServices;
    $i=1;
    $fullname = isset($data->LastName)? trimmed($data->LastName) . ', ' . trimmed($data->FirstName) : '[Complete Name]' ; 
    $balance = isset($balance) ? $balance : 0 ;
    $idno = isset($data->StudentNo)? $data->StudentNo : '';    
    $grades = isset($grades) ? $grades : array() ;
    $tmp="";
?>
<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img class="img-responsive" alt="{{asset('assets/admin/layout/img/logo.png')}}" src="{{ url() .'/general/getPupilPhoto?Idno='. encode($idno) .'&t='. rand()  }}">
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name" id="student-name">
					 <?= $fullname ?>
				</div>
				<div class="profile-usertitle-job text-center " >
                    <center>
                        <input id="idno" name="idno"  class="form-control text-center bold font-blue-madison input-sm input-medium" value="<?= getObjectValue($data, 'StudentNo') ?>" />
                    </center>                    					 
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<div class="profile-userbuttons">
            
                <span class="bold font-purple">{{ getObjectValue($data, 'Major') }}</span>
                <br />
                <span class="font-xs font-grey">Strand / Track</span>
				<br />
                <span class="bold">{{ getObjectValue($data, 'YearLevel') }}</span>
                <br />
                <span class="font-xs font-grey">Level</span>
                <br />
                <span class="bold">{{ getObjectValue($data, 'CurriculumCode') }}</span>
                <br />
                <span class="font-xs font-grey">Curriculum</span>
				
			</div>
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active"><a href="javascript:void(0);" class="views" data-menu="transcript"><i class="icon-home"></i> Transcript of Record </a></li>
					<li><a href="javascript:void(0);" class="views" data-menu="setup"><i class="fa fa-print"></i> Report Setup </a></li>
                    <li><a href="javascript:void(0);" class="views" data-menu="evaluation"><i class="icon-user"></i> Evaluation </a></li>
                    <li><a href="javascript:void(0);" class="views" data-menu="grade-encoding"><i class="fa fa-book"></i> Grade Encoding </a></li>
					<li><a class="views" href="javascript:void(0);"><i class="icon-check"></i> Conduct </a></li>
					<li><a class="views" href="javascript:void(0);"><i class="icon-info"></i>Help </a></li>
				</ul>
			</div>
			<!-- END MENU -->
		</div>												
		
	</div>
	<!-- END BEGIN PROFILE SIDEBAR -->
	<!-- BEGIN PROFILE CONTENT -->
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET -->
                <div id="transcript" class="tabpane">                                                                        
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font font-blue-madison"></i>
							<span class="caption-subject theme-font font-blue-madison bold uppercase"> Transcript of Record</span>
							<span class="caption-helper">This module is intent for generation of transcript of subject</span>
						</div>
						<div class="actions">                            	
                            <select class="input-sm">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>						
                            <button type="button" class="btn btn-default" data-menu="reload"><i class="fa fa-refresh"></i> Refresh </button>                                                          
						</div>
					</div>
					<div class="portlet-body" >	
						<div class="table-scrollable ">
							<table id="tbladvised" class="table table-bordered table-condensed table-hover " style="cursor: pointer;">
							<thead>
							<tr class="uppercase text-center">
                                <th>#</th>
                                <th class="text-center">ACADEMIC<br/>YEAR</th>
								<th> COURSE </th>
								<th> DESCRIPTIVE TITLE </th>
                                <th class="text-center">UNIT</th>
                                <th class="text-center">1ST</th>
                                <th class="text-center">2ND</th>
                                <th class="text-center">3RD</th>
                                <th class="text-center">4TH</th>
                                <th class="text-center">FINAL</th>
                                <th class="text-center">REMARKS</th>
								<th>DEPORTMENT</th>
								<th>1ST</th>
                                <th>2ND</th>
                                <th>3RD</th>
                                <th>4TH</th>
                                <th>FINAL</th>
                                <th>SECTION</th>								        
							</tr>
							</thead>
							<tbody>
                            @foreach($grades as $r)
                                <?php
                                    $index = getObjectValue($r,'IndexID');
                                    $subjid = getObjectValue($r,'SubjectID');
                                    
                                    $credited = $service->prereqPassed($index, $subjid, $idno);
                                    $prereq = $service->getPrereqSubCode($index, $subjid);
                                                                            
                                ?>
                                
                                @if($tmp != $r->YearTerm)
                                    
                                    @if($i != '1')
                                    <tr data-id="{{$r->SubjectID}}" data-sched="0" class="bg-grey" >
                                        <td class="autofit font-xs"><?php echo $i; $i=1; ?>.</td>                                    
                                        <td class="bold  " colspan="2">  Enrolled Courses</td>
                                        
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        <td class="autofit"></td>
                                        
                                        <td class="autofit"></td>
                                                                            
                                    </tr>
                                    
                                    
                                    @endif
                                    
                                <tr data-id="{{$r->SubjectID}}" data-sched="0" >
                                    <td class="autofit font-xs"><?php echo $i; $i++; ?>.</td>
                                    <td rowspan="<?= floatval( $r->TotalEnrolledSubjects) + 1 ?>" class="autofit bold font-lg text-center">
                                        <center>
                                        
                                        <p style="writing-mode: tb-rl; text-align: center !important; height: 100%; "><?= getObjectValue($r,'YearTerm') . ' ' . $r->YearLevel ?></p>
                                        </center>          
                                    </td>
                                    <td class="autofit">
                                        @if($r->ParentID != '0')
                                        &nbsp;&nbsp;
                                        @endif
                                        <?= $r->SubjectCode ?>
                                    </td>
                                    <td class="font-xs autofit">
                                         @if($r->ParentID != '0')
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        @endif
                                        {{$r->SubjectTitle}}
                                    </td>
                                    <td class="autofit text-center">
                                        <?= floatval($r->CreditedUnits)<0 ? '('. -1 * number_format( getObjectValue($r,'CreditedUnits') , '1') . ')' : number_format( getObjectValue($r,'CreditedUnits') , '1')  ?>
                                    </td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageA) != '' ?  number_format( trimmed($r->AverageA), '0') : '' ; ?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageB) != '' ?  number_format( trimmed($r->AverageB), '0') : '' ; ?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageC) != '' ?  number_format( trimmed($r->AverageC), '0') : '' ;?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageD) != '' ?  number_format( trimmed($r->AverageD), '0') : '' ;?></td>
                                    <td class="autofit text-center bold">{{$r->Final_Average}}</td>
                                    <td class="autofit text-center bold">{{$r->Final_Remarks}}</td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"><?= $r->SectionName != '' ? $r->SectionName : '' ?></td>
                                                                        
                                </tr>
                                <?php  $tmp = $r->YearTerm  ; ?>
                                @else
                                <tr data-id="{{$r->SubjectID}}" data-sched="0" >
                                    <td class="autofit font-xs"><?php echo $i; $i++; ?>.</td>                                    
                                    <td class="autofit">
                                        @if($r->ParentID != '0')
                                        &nbsp;&nbsp;
                                        @endif
                                        <?= $r->SubjectCode ?>
                                    </td>
                                    <td class="font-xs autofit">
                                         @if($r->ParentID != '0')
                                        &nbsp;&nbsp;
                                        @endif
                                        {{$r->SubjectTitle}}
                                    </td>
                                    <td class="autofit text-center"><?= floatval($r->CreditedUnits)<0 ? '('. -1 * number_format( getObjectValue($r,'CreditedUnits') , '2') . ')' : number_format( getObjectValue($r,'CreditedUnits') , '1')  ?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageA) != '' ?  number_format( trimmed($r->AverageA), '0') : '' ; ?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageB) != '' ?  number_format( trimmed($r->AverageB), '0') : '' ; ?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageC) != '' ?  number_format( trimmed($r->AverageC), '0') : '' ;?></td>
                                    <td class="autofit text-center"><?= trimmed($r->AverageD) != '' ?  number_format( trimmed($r->AverageD), '0') : '' ;?></td>
                                    <td class="autofit text-center bold">{{$r->Final_Average}}</td>
                                    <td class="autofit text-center bold">{{$r->Final_Remarks}}</td>
                                    
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    <td class="autofit"></td>
                                    
                                    <td class="autofit"><?= $r->SectionName != '' ? $r->SectionName : '' ?></td>
                                                                        
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                            </table>
						</div>                                                                   
					</div>
				</div>	
                </div>
                  <div id="evaluation" class="tabpane" style="display: none;">
                        @include($views.'evaluation.index')
                  </div>
                  <div id="setup" class="tabpane" style="display: none;">
                        @include($views.'setup')
                  </div>
                     			
			</div>			
		</div>		
	</div>	
</div>
</div>
@include($views.'modal')