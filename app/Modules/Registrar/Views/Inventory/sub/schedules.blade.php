<?php
// error_print($data);
$i = 1;
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>#</th>
            <th>Section Name</th>            
            <th>Year Level</th>
            <th>Subject Code</th>
            <th>Subject Title</th>
            <th>Schedule</th>                        
            <th>Teacher Name</th>         
            <th class="autofit">Date Posted</th>
            
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
            <?php
                $url = url('registrar/grading-sheet-inventory/view?t='.encode($term).'&s='. encode($r->ScheduleID) );
            ?>
        <tr data-id="{{ encode($r->SectionID) }}" data-type="0" data-teacher="{{ encode($r->AdviserID) }}" >
            <td class="autofit">
                  <input type="checkbox" class="chk-child" >                                                    
            </td>            
            <td class="font-xs autofit bold">{{$i}}.</td>
            <td><a href="{{$url}}" class="sec-name" data-id="{{ encode($r->SectionID) }}">{{ $r->SectionName }}</a></td>                        
            <td class="">{{ $r->YearLevel }}</td>
            <td class="">{{ $r->SubjectCode }}</td>
            <td class=""> {{ $r->SubjectTitle }}</td>
            <td class="">{{ $r->Sched_1 }}</td>            
            <td class="">{{$r->TeacherName}}</td>
            <td class="">                
                {{$r->GradesPostingDate}}                 
            </td>                               
        </tr>
        <?php $i++; ?>
        @endforeach

        @endif
    </tbody>
</table>