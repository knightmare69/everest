<?php
    $term = decode(Request::get('t'));    
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-wrench"></i>
			<span class="caption-subject bold uppercase"> Inventory of Class Record(s)</span>
			<span class="caption-helper">Use this module to monitor class records posting dates...</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline input-medium">									
	           <input type="text" class="form-control filter-table " data-table="records-table" />
			</div>                                			                        
            <a href="#" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">
            
            <div class="portlet-input input-inline input-medium">									
                <select class="select2 form-control" name="academic-term" id="academic-term">
                    <option value="0" >- Select Academic Year &amp; Term -</option>
                    @if(!empty($at))                        
                        @foreach($at as $r)
                        <option <?= $term == $r->TermID ? 'selected' : '' ?> value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
                        @endforeach
                    @endif
                </select>				
			</div>
            
            <button type="button" class="btn btn-sm btn-default" data-menu="unpost"><i class="fa fa-unlock"></i> Unpost Grade</button>
        </div>

        <div class="scroller" style="height: 450px;" id="grdSections">
            @include($views.'sub.schedules')            
        </div>        	
	</div>
</div>