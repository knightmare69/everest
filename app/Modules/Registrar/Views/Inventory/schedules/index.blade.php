<?php
    $term = decode(Request::get('t'));
    $sched = decode(Request::get('s'));
    //error_print($progs);
?>
<input type="hidden" id="term" value="{{encode($term)}}" />
<input type="hidden" id="sched" value="{{encode($sched)}}" />

<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-wrench"></i>
			<span class="caption-subject bold uppercase"> Class Record View</span>
			<span class="caption-helper">Use this module to monitor class records posting dates...</span>
		</div>
		<div class="actions">
                 
            
            <a href="#" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">
        <div class="portlet-input input-inline input-medium">									
	           <select class="select2 form-control" id="period">                    
                    @if(!empty($period))                        
                        @foreach($period as $r)
                        <option value="{{ encode($r->PeriodID) }}">{{$r->Description2}}</option>
                        @endforeach
                    @endif
                </select>
			</div>
            <button type="button" class="btn btn-sm btn-default" data-menu="unpost" ><i class="fa fa-unlock"></i> Unpost Grade</button>
        </div>
        <div class="scroller" style="height: 370px;" id="grdSchedules">
              @include($views.'schedules.students')          
        </div>        	
	</div>
</div>