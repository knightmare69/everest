<?php
    //error_print($sched);
    $i = 1;
    $per = !isset($per) ? '1' : $per ; 
    
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>#</th>
            <th>Student #</th>
            <th>Student Name</th>                     
            <th>Year Level</th>
            @if($per == '1' || $per == '11' )
                <th class="text-center"> 1st </th>
            @elseif($per == '2' || $per == '12' )
                <th class="text-center"> 2nd </th>
            @elseif($per == '13' )
                <th class="text-center"> Remedial </th>
            @elseif($per == '3' )
                <th class="text-center"> 3rd </th>
            @elseif($per == '4' )
                <th class="text-center"> 4th </th>            
            @endif                        
            <th class="text-center"> Remarks </th>                                                         
            <th>Date Posted</th>            
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)            
            <tr data-id="{{ encode($r->StudentNo) }}" data-type="1" >
                <td class="autofit text-center">
                
                @if($per == '1' || $per == '11' )
                    @if($r->DatePosted1 != '')
                        <input type="checkbox" class="chk-child" >
                    @else
                        <i class="fa fa-lock"></i>
                    @endif
                @elseif($per == '2' || $per == '12' )
                    @if($r->DatePosted2 != '')
                        <input type="checkbox" class="chk-child" >
                    @else
                        <i class="fa fa-lock"></i>
                    @endif
                @elseif($per == '3' || $per == '13' )
                    @if($r->DatePosted3 != '')
                        <input type="checkbox" class="chk-child" >
                    @else
                        <i class="fa fa-lock"></i>
                    @endif
                @elseif($per == '4' )
                    @if($r->DatePosted4 != '')
                        <input type="checkbox" class="chk-child" >
                    @else
                        <i class="fa fa-lock"></i>
                    @endif                                    
                @endif
            
                                                                        
                </td>
            
            <td class="autofit">{{$i}}.</td>
            <td>{{$r->StudentNo}}</td>
            <td>
                {{$r->StudentName}}                 
            </td>                                                
            <td>{{ $r->YearLevel }}</td>     
            
            @if($per == '1' || $per == '11' )
            <td class="text-center bold">{{ $r->AverageA }}</td>
            @elseif($per == '2' || $per == '12' )
            <td class="text-center bold">{{ $r->AverageB }}</td>
            @elseif($per == '3' || $per == '13' )
            <td class="text-center bold">{{ $r->AverageC }}</td>
            @elseif($per == '4' )
            <td class="text-center bold">{{ $r->AverageD }}</td>
            @endif   
            <td class="text-center bold"></td>
            <td class="text-center bold">
                
                @if($per == '1' || $per == '11' )
                    {{ $r->DatePosted1 }}
                @elseif($per == '2' || $per == '12' )
                    {{ $r->DatePosted2 }}
                @elseif($per == '3' || $per == '13' )
                    {{ $r->DatePosted3 }}
                @elseif($per == '4' )
                    {{ $r->DatePosted4 }}
                @endif
            
            </td>
        </tr>
        <?php $i++; ?>
        @endforeach

        @endif
    </tbody>
</table>