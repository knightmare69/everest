<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-list"></i> Optional Subjects
		</div>
		<div class="actions btn-set">
			<button class="btn green" id="btnadd"><i class="fa fa-plus"></i> Add</button>
			<button class="btn blue" id="btnrefresh"><i class="fa fa-refresh"></i> Refresh</button>
			<div class="btn-group hidden">
				<a data-toggle="dropdown" href="#" class="btn yellow dropdown-toggle">
				<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#">
						Print </a>
					</li>
					<li class="devider"></li>
					<li>
						<a href="#">
						Export to CSV </a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="portlet-body" id="tbloptional">
		
	</div>
</div>