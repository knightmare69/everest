<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-pencil-square-o"></i> Publish Grade Restriction </div>
    </div>
  <div class="portlet-body form form_wrapper">
        <form class="horizontal-form" id="shs-publishform" action="#" method="POST">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Academic Term</label>
                            <select class="select2 form-control" name="academic-term" id="academic-term">
                                @if(!empty($at))
                                    @foreach($at as $_this)
                                    <option value="{{ encode($_this->TermID) }}" data-term="{{ $_this->SchoolTerm }}">{{ $_this->AcademicYear.' - '.$_this->SchoolTerm }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group ">
                            <label class="control-label">Put check to publish the posted grade(s)</label>
                            <div class="checkbox-list chk-term">
        				        <label class=""><input id="first" class="term" value="11" type="checkbox"> <span class="lbfirst">First Quarter</span></label>
                                <label class=""><input id="second" class="term" value="12" type="checkbox"> <span class="lbsecond">Second Quarter</span></label>
        				        <label class=""><input id="third" class="term" value="13" type="checkbox"> <span class="lbthird">Third Quarter</span></label>
                                <label class=""><input id="fourth" class="term" value="14" type="checkbox"> <span class="lbfourth">Fourth Quarter</span></label>
           					</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions right">
                <!-- <button class="btn default btn_reset" type="button">Reset</button> -->
                <button class="btn btn-default" type="button" id="save-config"><i class="fa fa-save"></i> Save </button>
            </div>
        </form>
    </div>
</div>