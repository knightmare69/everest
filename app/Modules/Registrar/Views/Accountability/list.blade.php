<?php
    $model = new App\Modules\Registrar\Models\Accountability_model;
    $progams = getUserProgramAccess();

    $data = $model->selectRaw("*, dbo.fn_StudentName(StudentNo) As StudentName, dbo.fn_StudentYearLevel_k12(StudentNo) AS YearLevel ")
            ->where('TermID', $term)
            ->whereRaw("dbo.fn_StudentProgID(StudentNo) IN  (".join(',',$progams).")")
            ->orderBy('StudentName','ASC')
            ->get();
    $i=1;
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Student #</th>
            <th>Student Name</th>
            <th>Year Level</th>
            <th class="text-center"> Cleared </th>
            <th>Reason</th>

            <th>Date Cleared</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <tr data-id="{{ encode($r->IndexID) }}"  >
            <td class="autofit text-center">
                <input type="checkbox" class="chk-child" >
            </td>
            <td class="autofit">{{$i}}.</td>
            <td>{{$r->StudentNo}}</td>
            <td>
                {{$r->StudentName}}
            </td>
            <td>{{ $r->YearLevel }}</td>
            <td class="text-center autofit">
                <?= $r->Cleared == 1 ? '<i class="fa fa-check"></i> ' : '<i class="fa fa-times font-red"></i>' ?>
            </td>
            <td class=" bold">{{ $r->Reason }}</td>

            <td class="text-center bold">
                {{ $r->DateUpdate }}
            </td>
        </tr>
        <?php $i++; ?>
        @endforeach

        @endif
    </tbody>
</table>
