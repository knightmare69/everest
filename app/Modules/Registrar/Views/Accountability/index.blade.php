<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
			<i class="fa fa-download font-blue-steel"></i>
			<span class="caption-subject bold font-blue-steel"> List of Student with Accountability </span>
		</div>
        <div class="actions">

        </div>
    </div>
    <div class="portlet-body">
          <div class="well well-sm">
            <div class="portlet-input input-inline input-medium">
                <select class="select2 form-control" name="academic-term" id="academic-term">
                    <option value="0" >- Select AY Term -</option>
                    @if(!empty($at))
                        @foreach($at as $r)
                        <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
                        @endforeach
                    @endif
                </select>
			</div>
            <button type="button" class="btn btn-sm btn-default " data-menu="add"><i class="fa fa-plus"></i> Add Student </button>
            <button type="button" class="btn btn-sm btn-default " data-menu="remove"><i class="fa fa-times"></i> Remove </button>
        </div>
        <div class="scroller" id="grdList">
            @include($views.'list')
        </div>
    </div>
</div>
@include($views.'modal')