<?php
	$data = new \App\Modules\Registrar\Models\Subjects\Subjects;

	$data = $data->select([
			'SubjectID','SubjectCode','SubjectTitle','SubjectDesc',
			'AcadUnits','LabUnits','CreditUnits','LectHrs','LabHrs',
			'SubjectElective','SubjectGE','SubjectMajor','SubjectComputer',
			'InclGWA','IsNonAcademic','IsClubOrganization'
		]);
		// ->where('LevelID','<=',50);

	$odata = $data;

	if (Request::has('filterClass') && Request::get('filterClass')) {
		$data = $data->where('LevelID',trim(Request::get('filterClass')));
	}

	if (Request::has('CourseCode')) {
		$data = $data->where('SubjectCode',trim(Request::get('CourseCode')));
	}

	if (Request::has('CourseTitle')) {
		$data = $data->where('SubjectTitle',trim(Request::get('CourseTitle')));
	}

	if (Request::has('ParentCode')) {
		$data = $data->where('SubjParentID',trim(Request::get('ParentCode')));
	}

	if (Request::has('CourseLevel')) {
		$data = $data->where('LevelID',trim(Request::get('CourseLevel')));
	}

	$data = $data->limit(300);
?>
<table class="table table-hover table-condensed table-striped" id="tblmain">
	<thead>
		<tr>
			<td colspan="5">
				<div class="input-group">
					<select class="form-control" name="filterClass" id="filterClass">
						<option value="">All</option>
						@foreach(App\Modules\Setup\Models\ProgramClass::where('ClassCode','<=',21)->get() as $row)
						<option {{ Request::get('filterClass') == $row->ClassCode ? 'selected' : '' }} value="{{ $row->ClassCode }}">{{ $row->ClassDesc }}</option>
						@endforeach
					</select>
					<span class="input-group-addon cursor-pointer BtnSearch" title="Search Filter">
						<i class="fa fa-search"></i>
					</span>
					<span class="input-group-addon cursor-pointer btnFilterSettings" title="More Filter Settings">
						<i class="fa fa-wrench"></i>
					</span>
				</div>
			</td>
		</tr>
		<tr class="center">
			<th class="sm-font upper-letter center cs-td-valign" width="50"><br /> # </th>
			<th class="sm-font upper-letter center cs-td-valign" width="50">Course <br /> ID</th>
			<th class="sm-font upper-letter center cs-td-valign" width="50">Course <br />Code</th>
			<th class="sm-font upper-letter center cs-td-valign">Description Title</th>
			<th class="sm-font upper-letter center cs-td-valign">LEC <br />Unit</th>
			<th class="sm-font upper-letter center cs-td-valign">LAB <br />Unit</th>
			<th class="sm-font upper-letter center cs-td-valign">Credit <br />Unit</th>
			<th class="sm-font upper-letter center cs-td-valign">LEC <br />Hour</th>
			<th class="sm-font upper-letter center cs-td-valign">LAB <br />Hour</th>
			<th class="sm-font upper-letter center cs-td-valign">General<br />Education</th>
			<th class="sm-font upper-letter center cs-td-valign">Elective<br />Subject</th>
			<th class="sm-font upper-letter center cs-td-valign">Major<br />Subject</th>
			<th class="sm-font upper-letter center cs-td-valign">Computer<br />Subject</th>
			<th class="sm-font upper-letter center cs-td-valign">Include <br />in "GWA" or "GPA"</th>
			<th class="sm-font upper-letter center cs-td-valign">Non-Academic<br />Course</th>
			<th class="sm-font upper-letter center cs-td-valign">Club<br />Organization Course</th>
		</tr>
	</thead>
	<tbody>
		<?php $count=1; ?>
		@foreach($data->get() as $row)
		<tr class="center cs-tbody-td cursor-pointer cs-row-color rowSel" data-id="{{ encode($row->SubjectID) }}">
			<td>{{ str_pad($count, 4, "0", STR_PAD_LEFT) }}.</td>
			<td>{{ $row->SubjectID }}</td>
			<td class="autofit left">{{ $row->SubjectCode }}</td>
			<td class="left" title="{{ trimLength($row->SubjectDesc,50,'...') }}" >{{ $row->SubjectTitle }}</td>
			<td>{{ number_format($row->AcadUnits,2) }}</td>
			<td>{{ number_format($row->LabUnits,2) }}</td>
			<td>{{ number_format($row->CreditUnits,2) }}</td>
			<td>{{ number_format($row->LectHrs,2) }}</td>
			<td>{{ number_format($row->LabHrs,2) }}</td>
			<td><input type="checkbox" class="form-control" {{ $row->SubjectGE ? 'checked' : '' }} name="SubjectGE1" id="SubjectGE1"></td>
			<td><input type="checkbox" class="form-control" {{ $row->SubjectElective ? 'checked' : '' }} name="SubjectElective1" id="SubjectElective1"></td>
			<td><input type="checkbox" class="form-control" {{ $row->SubjectMajor ? 'checked' : '' }} name="SubjectMajor1" id="SubjectMajor1"></td>
			<td><input type="checkbox" class="form-control" {{ $row->SubjectComputer ? 'checked' : '' }} name="SubjectComputer1" id="SubjectComputer1"></td>
			<td><input type="checkbox" class="form-control" {{ $row->InclGWA ? 'checked' : '' }} name="InclGWA1" id="InclGWA1"></td>
			<td><input type="checkbox" class="form-control" {{ $row->IsNonAcademic ? 'checked' : '' }} name="IsNonAcademic1" id="IsNonAcademic1"></td>
			<td><input type="checkbox" class="form-control" {{ $row->IsClubOrganization ? 'checked' : '' }} name="IsClubOrganization1" id="IsClubOrganization1"></td>
		</tr>
		<?php $count++; ?>
		@endforeach
		<tr class="cs-row-color">
			<td colspan="15" class="left">Total records(s): <b>({{ $odata->count() }})</b></td>
		</tr>
		@if($data->count() <= 0)
		<tr class="cs-row-color">
			<td colspan="15" class="center bold">No result found.</td>
		</tr>
		@endif
	</tbody>
</table>