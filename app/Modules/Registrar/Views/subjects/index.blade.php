<style>
	.cs-row-color {
		background-color: #e7e7e7  !important;
	}

	.cs-row-color:hover {
		background-color: #fff  !important;
	}
	.cs-table-no-border {
		border-top: 0 none !important;
	}

	.row-selected {
		background-color: #fff  !important;
	}
	.cs-tbody-tr-no-border td {
		border: 0px none !important;
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
	}

	.cs-tbody-tr-border-bottom {
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
		/*border-bottom: 1px solid #e5e5e5 !important;*/
	}

	.no-border-top {
		border-top: 0px none !important;
	}

	.cs-td-valign {
		vertical-align: middle !important;
	}

	.cs-input-xs {
		width: 60px !important;
	}

</style>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-list"></i> Subjects Masterlist
		</div>
		<div class="actions btn-set">
			<button class="btn green" id="BtnCreate"><i class="fa fa-file"></i> Create</button>
			<button class="btn red" id="BtnDelete"><i class="fa fa-trash"></i> Delete</button>
			<div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn yellow dropdown-toggle">
				<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#">
						Print </a>
					</li>
					<li class="devider"></li>
					<li>
						<a href="#">
						Export to CSV </a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="portlet-body" id="TableWrapper">
		@include($views.'tables.masterlist')
	</div>
</div>