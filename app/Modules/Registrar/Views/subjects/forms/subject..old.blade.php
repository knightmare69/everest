<?php
    $data = App\Modules\Registrar\Models\Subjects\Subjects::limit(1)->where('SubjectID',decode(Request::get('key')))->first();

    function isCheck($isCheck = false) {
        return $isCheck ? 'checked' : '';
    }

    function isSel($a,$b) {
        return $a == $b ? 'selected' : '';
    }

    function lock($isLock = false) {
        return $isLock ? 'disabled' : '';
    }
?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <input type="hidden" name="IsLock" id="IsLock" value="{{ getObjectValue($data,'LockedBy') ? 1 : 0 }}">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                 <div class="col-md-4">
                    <label class="control-label bold">Course Code <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Course Code" {{ lock(getObjectValue($data,'LockedBy')) }}  class="form-control input-xs" name="SubjectCode" id="SubjectCode" value="{{ getObjectValue($data,'SubjectCode') }}">
                 </div>
                 <!--/span-->
                 <div class="col-md-8">
                    <label class="control-label bold">Course Title <small><i class="text-danger">*</i></small></label>
                    <input type="text" placeholder="Course Title" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control input-xs" name="SubjectTitle" id="SubjectTitle" value="{{ getObjectValue($data,'SubjectTitle') }}">
                 </div>
                 <!--/span-->
            </div>

            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Course Description <small><i class="text-danger">*</i></small></label>
                    <textarea class="form-control" name="SubjectDesc" {{ lock(getObjectValue($data,'LockedBy')) }} id="SubjectDesc">{{ getObjectValue($data,'SubjectDesc') }}</textarea>
                 </div>
            </div>

            <div class="form-group">
                 <div class="col-md-12">
                    <label class="control-label bold">Chinese Name</label>
                    <input type="text" placeholder="Chinese Name" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control input-xs not-required" name="ChineseSubject" id="ChineseSubject" value="{{ trimmed(getObjectValue($data,'ChineseSubject')) }}">
                 </div>
            </div>

            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="row">
                    <div class="col-md-4" style="width: 20% !important;padding-right: 0 !important;">
                        <table class="table margin-top-10 no-border-top">
                            <tbody>
                                <tr>
                                    <td class="bold center cs-tbody-tr-border-bottom" colspan="2">Units</td>
                                </tr>
                                <tr class="cs-tbody-tr-no-border">
                                    <td class="cs-td-valign">
                                        Lecure:
                                    </td>
                                    <td>
                                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="0" class="form-control numberonly center cs-input-xs input-small" name="AcadUnits" id="AcadUnits" value="{{ getObjectValueWithReturn($data,'AcadUnits',1) }}">
                                    </td>
                                </tr>
                                <tr class="cs-tbody-tr-no-border">
                                    <td  class="cs-td-valign">
                                        Laboratory:
                                    </td>
                                    <td >
                                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="0" class="form-control numberonly center cs-input-xs input-small not-required" name="LabUnits" id="LabUnits" value="{{ getObjectValueWithReturn($data,'LabUnits',0) }}">
                                    </td>
                                </tr>
                                <tr class="cs-tbody-tr-no-border">
                                    <td  class="cs-td-valign">
                                        Credit:
                                    </td>
                                    <td >
                                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="0" class="form-control numberonly center cs-input-xs input-small" name="CreditUnits" id="CreditUnits" value="{{ getObjectValueWithReturn($data,'CreditUnits',1) }}">
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="bold center cs-tbody-tr-border-bottom" colspan="2">Hours</td>
                                </tr>
                                <tr class="cs-tbody-tr-no-border">
                                    <td  class="cs-td-valign">
                                        Lecture:
                                    </td>
                                    <td >
                                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="0" class="form-control numberonly center cs-input-xs input-small" name="LectHrs" id="LectHrs" value="{{ getObjectValueWithReturn($data,'LectHrs',1) }}">
                                    </td>
                                </tr>
                                <tr class="cs-tbody-tr-no-border">
                                    <td  class="cs-td-valign">
                                        Laboratory:
                                    </td>
                                    <td >
                                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="0" class="form-control numberonly center cs-input-xs input-small not-required" name="LabHrs" id="LabHrs" value="{{ getObjectValueWithReturn($data,'LabHrs',0) }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    <div class="col-md-8" style="width: 80% !important;padding-left: 0px !important;border-left: 1px solid #e5e5e5 !important;">
                       <div class="row">
                            <div class="col-md-4" style="width: 30% !important;">
                                <table class="table margin-top-10 no-border-top cs-table-no-border">
                                    <tbody>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="cs-td-valign">
                                                Alias Code 1:
                                            </td>
                                            <td >
                                                <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="Alias" class="form-control center cs-input-xs input-small not-required" name="Alias1" id="Alias1" value="{{ getObjectValueWithReturn($data,'Alias1','') }}">
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="cs-td-valign">
                                                Alias Code 2:
                                            </td>
                                            <td >
                                                <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="Alias" class="form-control center cs-input-xs input-small not-required" name="Alias2" id="Alias2" value="{{ getObjectValueWithReturn($data,'Alias2','') }}">
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="cs-td-valign">
                                                Parent Code:
                                            </td>
                                            <td >
                                                <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="Code" class="form-control center cs-input-xs input-small not-required" name="SubjParentID" id="SubjParentID" value="{{ getObjectValueWithReturn($data,'SubjParentID','') }}">
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="cs-td-valign">
                                                Subject Weights:
                                            </td>
                                            <td >
                                                <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="%" class="form-control center cs-input-xs numberonly input-small not-required" name="Weight" id="Weight" value="{{ getObjectValueWithReturn($data,'Weight','') }}">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                             </div>
                             <div class="col-md-8" style="width: 70% !important;padding-left: 0px !important;">
                                <table class="table margin-top-10">
                                    <tbody>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="bold" colspan="2">SETTINGS</td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectGE" id="SubjectGE" {{ isCheck(getObjectValue($data,'SubjectGE')) }}>
                                                <label class="xs-font" for="SubjectGE">General Education</label>
                                            </td>
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="IsTransmutedGrade" id="IsTransmutedGrade"  {{ isCheck(getObjectValue($data,'IsTransmutedGrade')) }}>
                                                <label class="xs-font" for="IsTransmutedGrade">Use Transmuted Grade</label>
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectMajor" id="SubjectMajor" {{ isCheck(getObjectValue($data,'SubjectMajor')) }}>
                                                <label class="xs-font" for="SubjectMajor">Major Course</label>
                                            </td>
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="IsPracticum" id="IsPracticum" {{ isCheck(getObjectValue($data,'IsPracticum')) }}>
                                                <label class="xs-font" for="IsPracticum">Practicum Course</label>
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectElective" id="SubjectElective" {{ isCheck(getObjectValue($data,'SubjectElective')) }}>
                                                <label class="xs-font" for="SubjectElective">Elective Course</label>
                                            </td>
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="ThesisSubject" id="ThesisSubject" {{ isCheck(getObjectValue($data,'ThesisSubject')) }}>
                                                <label class="xs-font" for="ThesisSubject">Thesis Course</label>
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectComputer" id="SubjectComputer" {{ isCheck(getObjectValue($data,'SubjectComputer')) }}>
                                                <label class="xs-font" for="SubjectComputer">Computer Course</label>
                                            </td>
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="LetterGradeOnly" id="LetterGradeOnly" {{ isCheck(getObjectValue($data,'LetterGradeOnly')) }}>
                                                <label class="xs-font" for="LetterGradeOnly">Letter Grade Only</label>
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectELearning" id="SubjectELearning" {{ isCheck(getObjectValue($data,'SubjectELearning')) }}>
                                                <label class="xs-font" for="SubjectELearning">E-Learning</label>
                                            </td>
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="IsNonAcademic" id="IsNonAcademic" {{ isCheck(getObjectValue($data,'IsNonAcademic')) }}>
                                                <label class="xs-font" for="IsNonAcademic">None-Academic Course</label>
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectWithInternet" id="SubjectWithInternet" {{ isCheck(getObjectValue($data,'SubjectWithInternet')) }}>
                                                <label class="xs-font" for="SubjectWithInternet">Course With Internet</label>
                                            </td>
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="IsClubOrganization" id="IsClubOrganization" {{ isCheck(getObjectValue($data,'IsClubOrganization')) }}>
                                                <label class="xs-font" for="IsClubOrganization">Club Organization Course</label>
                                            </td>
                                        </tr>
                                        <tr class="cs-tbody-tr-no-border">
                                            <td  class="upper-letter xs-font">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="InclGWA" id="InclGWA" {{ isCheck(getObjectValue($data,'InclGWA')) }}>
                                                <label class="xs-font" for="InclGWA">Include in "GWA" or "GPA"</label>
                                            </td>
                                            <td  class="upper-letter xs-font hide">
                                                <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="OtherSchool" id="OtherSchool" {{ isCheck(getObjectValue($data,'OtherSchool')) }}>
                                                <label class="xs-font" for="OtherSchool">From Other School</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-border-top">
                                    <tr>
                                        <td colspan="3" class="bold">OTHER INFORMATION</td>
                                    </tr>
                                    <tr class="cs-tbody-tr-no-border">
                                        <td>
                                            Course Level
                                        </td>
                                        <td>
                                            Course Area
                                        </td>
                                        <td>
                                            Course Mode
                                        </td>
                                    </tr>
                                    <tr class="cs-tbody-tr-no-border">
                                        <td>
                                            <select {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="LevelID">
                                                <option value="">- SELECT -</option>
                                                @foreach(App\Modules\Setup\Models\ProgramClass::where('ClassCode','<=',21)->get() as $row)
                                                <option {{ isSel(getObjectValue($data,'LevelID'),$row->ClassCode) }} value="{{ $row->ClassCode }}">{{ $row->ClassDesc }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectAreaID">
                                                <option value="">- SELECT -</option>
                                                @foreach(App\Modules\Registrar\Models\Subjects\Area::where('Inactive','0')->get() as $row)
                                                <option {{ isSel(getObjectValue($data,'SubjectAreaID'),$row->SubjectAreaID) }} value="{{ $row->SubjectAreaID }}">{{ $row->SubjectAreaName }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control" name="SubjectMode">
                                                <option value="">- SELECT -</option>
                                                @foreach(App\Modules\Registrar\Models\Subjects\Mode::get() as $row)
                                                <option {{ isSel(getObjectValue($data,'SubjectMode'),$row->SubjectMode) }} value="{{ $row->SubjectMode }}">{{ $row->SubjectModeDesc }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="cs-tbody-tr-no-border">
                                        <td class="cs-td-valign" colspan="3">
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    Min/Max:
                                                    <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control numberonly cs-input-xs" name="MinSize" value="{{ getObjectValueWithReturn($data,'MinSize',0) }}">
                                                </div>
                                                <div class="col-md-2">
                                                     Class Limit
                                                    <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control numberonly cs-input-xs" name="MaxSize" value="{{ getObjectValueWithReturn($data,'MaxSize',0) }}">
                                                </div>
                                                <div class="col-md-8">
                                                     <input {{ lock(getObjectValue($data,'LockedBy')) }} {{ isCheck(getObjectValue($data,'LockedBy') ) }} type="checkbox" class="form-control" name="LockSubject" {{ isCheck(getObjectValue($data,'LockSubject')) }}>
                                                    Lock Subject - <small><i>No more modifications allowed.</i></small>
                                                    <input {{ lock(getObjectValue($data,'LockedBy')) }} type="checkbox" class="form-control" name="Inactive" {{ isCheck(getObjectValue($data,'Inactive')) }}>
                                                    Inactive Record
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->