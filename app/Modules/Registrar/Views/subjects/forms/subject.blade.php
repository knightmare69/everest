<?php
    $data = App\Modules\Registrar\Models\Subjects\Subjects::limit(1)->where('SubjectID',decode(Request::get('key')))->first();

    function isCheck($isCheck = false) {
        return $isCheck ? 'checked' : '';
    }

    function isSel($a,$b) {
        return $a == $b ? 'selected' : '';
    }

    function lock($isLock = false) {
        return $isLock ? 'disabled' : '';
    }

    $css_lock = lock(getObjectValue($data,'LockedBy'));

?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <input type="hidden" name="IsLock" id="IsLock" value="{{ getObjectValue($data,'LockedBy') ? 1 : 0 }}">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">

             <div class="col-md-3">
                <div class="form-group">
                <label class="control-label bold">Course Code <small><i class="text-danger">*</i></small></label>
                <input type="text" placeholder="Course Code" {{$css_lock}}  class="form-control input-xs" name="SubjectCode" id="SubjectCode" value="{{ getObjectValue($data,'SubjectCode') }}">
                </div>
             </div>
             <!--/span-->
             <div class="col-md-9">
               <span class="pull-right">
                <input {{ lock(getObjectValue($data,'LockedBy')) }} {{ isCheck(getObjectValue($data,'LockedBy') ) }} type="checkbox" class="form-control" name="LockSubject" {{ isCheck(getObjectValue($data,'LockSubject')) }}>
                                                    Lock Subject - <small><i>No more modifications allowed.</i></small>
                </span>

                <div class="form-group">
                <label class="control-label bold">Course Title <small><i class="text-danger">*</i></small>


                </label>
                <input type="text" placeholder="Course Title" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control input-xs" name="SubjectTitle" id="SubjectTitle" value="{{ getObjectValue($data,'SubjectTitle') }}">
                </div>

             </div>

             <div class="col-md-5">
                <div class="form-group">
                <label class="control-label bold">Chinese Name</label>
                <input type="text" placeholder="Chinese Name" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control input-xs not-required" name="ChineseSubject" id="ChineseSubject" value="{{ trimmed(getObjectValue($data,'ChineseSubject')) }}">
                </div>
             </div>

             <div class="col-md-7">
                <div class="form-group">
                <label class="control-label bold">Course Description <small><i class="text-danger">*</i></small></label>
                <textarea style="resize: none !important;" class="form-control input-sm" name="SubjectDesc" {{ lock(getObjectValue($data,'LockedBy')) }} id="SubjectDesc">{{ getObjectValue($data,'SubjectDesc') }}</textarea>
                </div>
             </div>
        </div>
        <div class="row">

                 <div class="col-md-2">
                    <div class="form-group">
                        <div class="input-group">
                            <span  class="input-group-addon">Lec Unit</span>
                            <input type="text" {{ $css_lock }} placeholder="Unit" class="form-control numberonly center cs-input-xs input-sm" name="AcadUnits" id="AcadUnits" value="{{ getObjectValueWithReturn($data,'AcadUnits',1) }}">
                        </div>
                    </div>
                 </div>
                 <div class="col-md-2">
                    <div class="input-group">
                        <span  class="input-group-addon">Lec Hrs</span>
                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="0" class="form-control numberonly center input-sm " name="LectHrs" id="LectHrs" value="{{ getObjectValueWithReturn($data,'LectHrs',1) }}">
                    </div>
                 </div>

                 <div class="col-md-2">
                    <div class="input-group">
                        <span  class="input-group-addon">Lab Unit</span>
                        <input type="text" {{ $css_lock }} placeholder="0" class="form-control numberonly center input-sm not-required" name="LabUnits" id="LabUnits" value="{{ getObjectValueWithReturn($data,'LabUnits',0) }}">
                    </div>
                 </div>

                 <div class="col-md-2">
                    <div class="input-group">
                        <span  class="input-group-addon">Lab Hrs</span>
                        <input type="text" {{ $css_lock }} placeholder="0" class="form-control numberonly center input-sm not-required" name="LabHrs" id="LabHrs" value="{{ getObjectValueWithReturn($data,'LabHrs',0) }}">
                    </div>
                 </div>

                 <div class="col-md-2">
                    <div class="input-group">
                        <span  class="input-group-addon">Credit</span>
                        <input type="text" {{ $css_lock }} placeholder="0" class="form-control numberonly center input-sm" name="CreditUnits" id="CreditUnits" value="{{ getObjectValueWithReturn($data,'CreditUnits',1) }}">
                    </div>
                 </div>
                 <div class="col-md-2">
                    <div class="input-group">
                        <span  class="input-group-addon">Weight</span>
                        <input type="text" {{ $css_lock }} placeholder="%" class="form-control center input-sm numberonly not-required" name="Weight" id="Weight" value="{{ getObjectValueWithReturn($data,'Weight','') }}">
                    </div>
                 </div>


            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group">
                        <span  class="input-group-addon">Alias 1</span>
                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="Alias" class="form-control center input-sm not-required" name="Alias1" id="Alias1" value="{{ getObjectValueWithReturn($data,'Alias1','') }}">
                    </div>
                 </div>
                 <div class="col-md-4">
                    <div class="input-group">
                        <span  class="input-group-addon">Alias 2</span>
                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="Alias" class="form-control center input-sm not-required" name="Alias2" id="Alias2" value="{{ getObjectValueWithReturn($data,'Alias2','') }}">
                    </div>
                 </div>
                 <div class="col-md-4">
                    <div class="input-group">
                        <span  class="input-group-addon">Parent Code</span>
                        <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} placeholder="Code" class="form-control center input-sm not-required not-required" name="SubjParentID" id="SubjParentID" value="{{ getObjectValueWithReturn($data,'SubjParentID','') }}">
                    </div>
                 </div>

            </div>
            <div class="row">

                <div class="col-md-3" >
                    <h4 class="bold font-blue-madison">Subject Setup</h4>
                    <div class="checkbox-list">
                        <label class="checker bold">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="InclGWA" id="InclGWA" {{ isCheck(getObjectValue($data,'InclGWA')) }}>
                            Include in "GWA/GPA"
                        </label>
                        <label class="checker bold">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="IsNonAcademic" id="IsNonAcademic" {{ isCheck(getObjectValue($data,'IsNonAcademic')) }}>
                            Non-Academic Course
                        </label>
                        <label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="IsClubOrganization" id="IsClubOrganization" {{ isCheck(getObjectValue($data,'IsClubOrganization')) }}>
                            Club Organization Course
                        </label>
                        <label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="OtherSchool" id="OtherSchool" {{ isCheck(getObjectValue($data,'OtherSchool')) }}>
                            From Other School
                        </label>
                    </div>
                </div>
                <div class="col-md-3" >
                    <h5 class="bold">&nbsp;</h5>
                    <div class="checkbox-list">
                        <label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} name="IsTransmutedGrade" id="IsTransmutedGrade"  {{ isCheck(getObjectValue($data,'IsTransmutedGrade')) }}>
                            Use Transmuted Grade
                        </label>
                        <label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="LetterGradeOnly" id="LetterGradeOnly" {{ isCheck(getObjectValue($data,'LetterGradeOnly')) }}>
                            Letter Grade Only
                        </label>
                         <label class="checker">
                            <input type="checkbox" {{ $css_lock }} class="" name="PassFail" id="PassFail" {{ isCheck(getObjectValue($data,'IsPassFail')) }}>
                            Pass/Fail Only
                        </label>
						<label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="Semestral" id="Semestral" {{ isCheck(getObjectValue($data,'IsSemestral')) }}>
                            Semestral
                        </label>


                        
                    </div>
                </div>
                <div class="col-md-3" >
                    <h5 class="bold">Course Type</h5>
                    <div class="checkbox-list">
                    <label class="checker">
                    <input  type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} name="SubjectGE" id="SubjectGE" {{ isCheck(getObjectValue($data,'SubjectGE')) }}>
                    General Education
                    </label>

                    <label class="checker">
                    <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} name="SubjectMajor" id="SubjectMajor" {{ isCheck(getObjectValue($data,'SubjectMajor')) }}>
                    Major Course
                    </label>
                    <label class="checker">
                        <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="SubjectComputer" id="SubjectComputer" {{ isCheck(getObjectValue($data,'SubjectComputer')) }}>
                        Computer Course
                    </label>
                     <label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="SubjectELearning" id="SubjectELearning" {{ isCheck(getObjectValue($data,'SubjectELearning')) }}>
                            E-Learning
                        </label>

                    </div>
                </div>
                <div class="col-md-3" >
                    <h5 class="bold">&nbsp;</h5>
                    <div class="checkbox-list">
                     <label class="checker">
                    <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} name="IsPracticum" id="IsPracticum" {{ isCheck(getObjectValue($data,'IsPracticum')) }}>
                    Practicum Course
                    </label>
                    <label class="checker">
                    <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="SubjectElective" id="SubjectElective" {{ isCheck(getObjectValue($data,'SubjectElective')) }}>
                    Elective Course
                    </label>
                    <label class="checker">
                    <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="ThesisSubject" id="ThesisSubject" {{ isCheck(getObjectValue($data,'ThesisSubject')) }}>
                    Thesis Course
                    </label>
					
					<label class="checker">
                            <input type="checkbox" {{ lock(getObjectValue($data,'LockedBy')) }} class="" name="SubjectWithInternet" id="SubjectWithInternet" {{ isCheck(getObjectValue($data,'SubjectWithInternet')) }}>
                            Course With Internet
                        </label>
						
                    </div>
                </div>
            </div>
            <div class="row">                
                <div class="col-md-12">
                    <h4 class="bold font-blue-madison">Other Information</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label bold">Course Level</label>
                                <select {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control not-required" name="LevelID">
                                    <option value="">- SELECT -</option>
                                    @foreach(App\Modules\Setup\Models\ProgramClass::where('GroupClass','<=',2)->get() as $row)
                                    <option {{ isSel(getObjectValue($data,'LevelID'),$row->ClassCode) }} value="{{ $row->ClassCode }}">{{ $row->ClassDesc }}</option>
                                    @endforeach
                                </select>
                            </div>                
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label bold">Course Area</label>
                                <select {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control not-required" name="SubjectAreaID">
                                    <option value="">- SELECT -</option>
                                    @foreach(App\Modules\Registrar\Models\Subjects\Area::where('Inactive','0')->get() as $row)
                                    <option {{ isSel(getObjectValue($data,'SubjectAreaID'),$row->SubjectAreaID) }} value="{{ $row->SubjectAreaID }}">{{ $row->SubjectAreaName }}</option>
                                    @endforeach
                                </select>
                            </div>                
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label bold">Course Mode</label>
                                <select {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control not-required" name="SubjectMode">
                                    <option value="">- SELECT -</option>
                                    @foreach(App\Modules\Registrar\Models\Subjects\Mode::get() as $row)
                                    <option {{ isSel(getObjectValue($data,'SubjectMode'),$row->SubjectMode) }} value="{{ $row->SubjectMode }}">{{ $row->SubjectModeDesc }}</option>
                                    @endforeach
                                </select>
                            </div>                
                        </div>
                    </div>
                    <div class="row">
                    
                        <div class="col-md-4">
                            <div class="input-group">
                                <span  class="input-group-addon" title="Minimum Class Size">Min Size</span>
                                <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control numberonly " name="MinSize" value="{{ getObjectValueWithReturn($data,'MinSize',35) }}">
                            </div>                                                                                                              
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span  class="input-group-addon" title="Maximum Class Size">Max Size</span>
                                <input type="text" {{ lock(getObjectValue($data,'LockedBy')) }} class="form-control numberonly" name="MaxSize" value="{{ getObjectValueWithReturn($data,'MaxSize',35) }}">
                            </div>                                                    
                        </div>
                        
                        <div class="col-md-4">
                            <label class="checker bold font-red-flamingo">
                            <input {{ lock(getObjectValue($data,'LockedBy')) }} type="checkbox" class="form-control" name="Inactive" {{ isCheck(getObjectValue($data,'Inactive')) }}>
                            Set as In-Active Subject
                            </label>
                            
                        </div>
                    </div>
                                        
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->