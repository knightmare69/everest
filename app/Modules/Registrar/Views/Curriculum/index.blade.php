<div class="row">
 <div class="col-sm-6 col-md-4">
  <div class="portlet box blue">
  <div class="portlet-title">
   <div class="caption">
    <i class="fa fa-table"></i> Details
   </div>
   <div class="tools">
    <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
    <a href="javascript:;" class="reload" data-original-title="" title=""></a>
   </div>
  </div>
  <div class="portlet-body" style="min-height:400px;">
   <form role="form" class="curr_form" onsubmit="return false;">
    <div class="form-body">
	 <div class="form-group">
	  <label class="hidden">Campus:</label>
		<div class="input-group hidden">
		<span class="input-group-addon hidden"><i class="fa fa-bank"></i></span>
		<?php echo $campus;?>
		</div>
	  <label>Program:</label>
		<div class="input">
		<?php echo $program;?>
		</div>
	  <label class="hidden">Major:</label>
	    <div class="input">
		<select class="form-control hidden" name="major" id="major" placeholder="Major">
		 <option value="-1" disabled selected>- Select one -</option>
		</select>
		</div>
	 </div>
	 <div class="proginfo">
	 </div>
     <hr/>
	</div>
	
	<div class="form-group">
	  <div class="row">
	   <div class="col-sm-12">
	    <button class="btn btn-default btn-sm btncancel pull-right" type="button" disabled>Cancel</button>
	    <button class="btn btn-warning btn-sm btnsave pull-right" type="button" style="margin-right:2px;" disabled><i class="fa fa-save"></i> Save</button>
	    <button class="btn btn-danger btn-sm btndelete pull-right" type="button" style="margin-right:2px;" disabled><i class="fa fa-times"></i> Delete</button>
	    <button class="btn btn-success btn-sm btnnew pull-right" type="button" style="margin-right:2px;"><i class="fa fa-file"></i> New</button>
	   </div>
	  </div>
	  <label>Curriculum Code:</label>
		<div class="input-group">
		<input type="text" class="form-control curr_data" name="code" id="code" placeholder="Curriculum"/>
		<span class="input-group-btn">
		 <button type="button" class="btn green dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-search"></i></button>
         <ul class="dropdown-menu pull-right btncurrcontent custom-scroll" style="max-width:300px;max-height:256px;overflow:auto;">
		  <li><a href="javascipt:void(0);" data-id="-1">No Data</a></li>
		 </ul>
		</span>
		</div>
	  <label>Description:</label>
		<div class="input">
		<input type="text" class="form-control curr_data" name="desc" id="desc" placeholder="Description"/>
		</div>
	  <label>Notes:</label>
		<div class="textarea">
		<textarea class="form-control curr_data" name="note" id="note" style="resize:none;"></textarea>
		</div>
	 <div class="form-group">
	  <hr style="margin:10px;"/>
	  <div class="row">
	   <div class="col-sm-6">
	   <label>Regular Load</label>
	   <div class="input-group">
		<span class="input-group-addon">Min&nbsp</span>
		<input type="text" class="form-control curr_data numberonly" name="rmin" id="rmin" value="15.0" placeholder="Min"/>
	   </div>
	   <div class="input-group">
		<span class="input-group-addon">Max</span>
		<input type="text" class="form-control curr_data numberonly" name="rmax" id="rmax" value="AUTO" placeholder="Max"/>
	   </div>
	   </div>
	   <div class="col-sm-6">
	   <label>Summer Load</label>
	   <div class="input-group">
		<span class="input-group-addon">Min&nbsp</span>
		<input type="text" class="form-control curr_data numberonly" name="smin" id="smin" value="3.0" placeholder="Min"/>
	   </div>
	   <div class="input-group">
		<span class="input-group-addon">Max</span>
		<input type="text" class="form-control curr_data numberonly" name="smax" id="smax" value="6.0" placeholder="Max"/>
	   </div>
	   </div>
	  </div>
	 </div>
	 <div class="form-group">
	  <button type="button" class="btn btn-default btn-xs btnzerobase">Non-Zero Base</button>
	  <button type="button" class="btn btn-default btn-xs btninactive"><i class="fa fa-exclamation-circle"></i> Inactive</button>
	  <button type="button" class="btn btn-warning btn-xs btnlock"><i class="fa fa-lock"></i> Lock</button>
	  <button type="button" class="btn btn-warning btn-xs hidden btnunlock"><i class="fa fa-unlock"></i> Unlock</button>
	  <button type="button" class="btn btn-info btn-xs pull-right btncurrlvl"><i class="fa fa-bar-chart-o"></i> Curr. Lvl</button>
	 </div>
	</div>
   </form>
  </div>
  </div>
 </div>
 <div class="col-sm-6 col-md-8">
  <div class="portlet box yellow">
  <div class="portlet-title">
   <div class="caption">
    <i class="fa fa-table"></i> Curriculum:<strong class="currcode"></strong>
   </div>
   <div class="tools">
    <span class="btn-group">
	<button class="btn red btn-xs btnaddterm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-plus"></i> Year Term</button>
	<ul class="dropdown-menu pull-right yrtermcontent custom-scroll" style="max-width:600px;max-height:256px;overflow:auto;">
	 <li class="yrterm" data-id="-1" data-yrterm="Nothing to display">
	  <a href="javascript:void(0);">Nothing to display</a>
	 </li>
	</ul>
	</span>
    <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
    <a href="javascript:;" class="reload" data-original-title="" title=""></a>
    <a href="javascript:;" class="remove" data-original-title="" title=""></a>
   </div>
  </div>
  <div class="portlet-body" style="min-height:400px;">
   <div class="well" style="min-height:480px;max-height:600px;padding:4px;margin-bottom:5px;overflow:auto;">
    <div class="dvcontent" style="white-space:nowrap;">@include($xview.'.sub.curriculum')</div>
   </div>
  </div>
  </div>
 </div>
</div>
@include($xview.'.sub.prerequisite')
@include($xview.'.sub.subject')
@include($xview.'.sub.curricular')