<?php
 if(!isset($detail) || !$detail)
 {
?>	 
  <table class="table table-condensed table-bordered table-hover" style="background-color:white;white-space:nowrap;">
   <thead>
	<tr class="warning">
	 <th colspan="2">CourseID</th>
	 <th>SubjectCode</th>
	 <th>SubjectTitle</th>
	 <th>LEC</th>
	 <th>LAB</th>
	 <th>CREDIT</th>
	 <th>LEC HR</th>
	 <th>LAB HR</th>
	 <th>Pre-Requisite</th>
	 <th>Co-Requisite</th>
	 <th>Equivalent</th>
	 <th>YearStanding</th>
	</tr> 
   </thead>
   <tbody>
    <tr><td colspan="13"><i class="fa fa-warning"></i> Nothing to display.</td></tr>
   </tbody>
  </table>
<?php			   
 }
 else
 {
  $output   = '<table class="table table-condensed table-bordered table-hover" style="white-space:nowrap;">';
  $thead    = '<thead>
                <tr class="warning">
                 <th colspan="3">CourseID</th>
                 <th>SubjectCode</th>
                 <th>SubjectTitle</th>
                 <th>LEC</th>
                 <th>LAB</th>
                 <th>CREDIT</th>
                 <th>LEC HR</th>
                 <th>LAB HR</th>
                 <th>Pre-Requisite</th>
                 <th>Co-Requisite</th>
                 <th>Equivalent</th>
                 <th>YearStanding</th>
				</tr> 
               </thead>';
  $template = '<tfoot class="hidden">
               <tr class="bg-white trsubj" data-main="0" data-id="tmpsubj" data-parent="0">
	            <td></td>
	            <td></td>
	            <td class="subjid"></td>
	            <td class="subjcode"></td>
	            <td class="subjtitle"></td>
	            <td class="slecu text-center"></td>
	            <td class="slabu text-center"></td>
	            <td class="scrdu text-center"></td>
	            <td class="slech text-center"></td>
	            <td class="slabh text-center slabhr"></td>
	            <td class="prereq"></td>
	            <td class="coreq"></td>
	            <td class="equiv"></td>
	            <td class="yearstand"></td>
	           </tr>
			   </tfoot>';		
			   
  $tbody    = '';
  $tmprmain = '';
  $yrterm   = 0;
  $yrlvl    = 0;
  $yrstd    = '';
  $tmprow   = '';
  $tmpfoot  = '';
  $count    = 0;
  $lect     = 0;
  $lab      = 0;
  $crdt     = 0;
  $datelock = '';
  $btnadd   = '';
  $btnedit  = '<i class="fa fa-fw"></i>';
  $btndel   = '';
  $btnclose = '';
  $prevsubj = '';
  foreach($detail as $rs)
  {
	$indxid = $rs->IndexID;  
	if($rs->DateLocked!='')
    {
	 $tmpdate  = new DateTime($rs->DateLocked);
     $datelock = $tmpdate->format('Y-m-d');	 
	}		
	else
	{
     $btnadd   = '<button class="btn btn-xs btn-success btnadd"><i class="fa fa-plus"></i></button>';
     $btnedit  = '<i class="fa fa-edit text-warning btnedit"></i>';
     $btndel   = '<i class="fa fa-trash-o text-danger btnremove"></i>';
	 $btnclose = '<button class="btn btn-xs red btnremyt"><i class="fa fa-times"></i></button>';
	}	
	
    if($yrterm<>$rs->YearTermID)
	{
	 $tbody   .= $tmprmain.$tmprow.$tmpfoot;
     $yrterm   = $rs->YearTermID;
	 $termcode = $rs->TermCode;
	 $progclas = $rs->ProgClass;
	 $yrlvl    = $rs->YearLevelID;
	 $yrstd    = $rs->YearLevel;
	 $tmprow   = '';
     $count    = 0;
     $lect     = 0;
     $lab      = 0;
     $crdt     = 0;	 
	}
	
    if($yrterm>0)
    {
	 $tmprmain = '<tr class="warning trmain'.(($indxid=='')?' hidden':'').'" data-id="'.$yrterm.'" data-code="'.$termcode.'" data-class="'.$progclas.'" data-yrlvl="'.$yrlvl.'" data-yrstd="'.$yrstd.'">
	               <td width="1px" class="minmax text-center"><i class="fa fa-minus indicator"></i></td>
				   <td class="yrterm-main" colspan="13" data-id="'.$yrterm.'">
				    <span style="float:left;">'.$btnadd.$btnclose.'</span>
					Year Term:<b>'.$rs->YearTermDesc.'</b>
				   </td>
				  </tr>';
	}
	
    if($rs->SubjectID!='')	
    {
	 $indxid    = $rs->IndexID;	
	 $subjid    = $rs->SubjectID;	
	 $subjcode  = $rs->SubjectCode;	
	 $subjtitle = $rs->SubjectTitle;
	 $subjlec   = $rs->AcadUnits;
	 $subjlab   = $rs->LabUnits;
	 $subjcrd   = $rs->CreditUnits;
	 $subjlech  = $rs->LectHrs;
	 $subjlabh  = $rs->LabHrs;
	 $lect      = $lect+$subjlec;
     $lab       = $lab+$subjlab;
     $crdt      = $crdt+$subjcrd;
	 $parent    = $rs->SubjParentID;
	 $prereq    = $rs->PreRequisites;
	 $coreq     = $rs->CoRequisites;
	 $equiv     = $rs->Equivalent;
	 
	 if($prevsubj!='' && $prevsubj == $parent)
	  $subjtitle = '<i class="fa fa-fw"></i>'.$subjtitle;	 
	 else
      $prevsubj = $subjid;		 
	 
	 $count++;
	 
     $tmprow   .= '<tr class="bg-white trsubj" data-main="'.$yrterm.'" data-id="'.$subjid.'" data-parent="'.$parent.'" data-indx="'.$indxid.'">
	                <td>'.$btndel.'</td>
					<td>'.$btnedit.'</td>
	                <td class="subjid">'.$subjid.'</td>
	                <td class="subjcode">'.$subjcode.' </td>
	                <td class="subjtitle">'.$subjtitle.'</td>
	                <td class="slecu text-center">'.number_format($subjlec,1,'.',',').'</td>
	                <td class="slabu text-center">'.number_format($subjlab,1,'.',',').'</td>
	                <td class="scrdu text-center">'.number_format($subjcrd,1,'.',',').'</td>
	                <td class="slech text-center">'.number_format($subjlech,1,'.',',').'</td>
	                <td class="slabh text-center">'.number_format($subjlabh,1,'.',',').'</td>
	                <td class="prereq">'.$prereq.'</td>
	                <td class="coreq">'.$coreq.'</td>
	                <td class="equiv">'.$equiv.'</td>
	                <td class="yrstand">'.$yrstd.'</td>
	               </tr>';   					   
	}
	
    if($yrterm>0)
    {
	 $tmpfoot  = '<tr class="info trfoot'.(($indxid=='')?' hidden':'').'" data-main="'.$yrterm.'">
	                <td colspan="4">TOTAL: Subject(s):<b class="count">'.$count.'</b></td>
	                <td>Lect Units:<b class="lecu">'.number_format($lect,1,'.',',').'</b></td>
	                <td colspan="4">Lab Units:<b class="labu">'.number_format($lab,1,'.',',').'</b></td>
	                <td colspan="5">Credit Units:<b class="crdu">'.number_format($crdt,1,'.',',').'</b></td>
	              </tr>'; 		
	}		
	
	$template = '<tfoot class="hidden">
				  <tr class="bg-white trsubj" data-main="0" data-id="tmpsubj" data-parent="" data-indx="">
				   <td>'.$btndel.'</td>
				   <td>'.$btnedit.'</td>
				   <td class="subjid"></td>
				   <td class="subjcode"></td>
				   <td class="subjtitle"></td>
				   <td class="slecu text-center"></td>
				   <td class="slabu text-center"></td>
				   <td class="scrdu text-center"></td>
				   <td class="slech text-center"></td>
				   <td class="slabh text-center slabhr"></td>
				   <td class="prereq"></td>
				   <td class="coreq"></td>
				   <td class="equiv"></td>
				   <td class="yrstand"></td>
				  </tr>
				 </tfoot>';			
  }
  
  if($yrterm>0)
  {
	$tbody   .= $tmprmain.$tmprow.$tmpfoot; 
  }	  
  
  $output.= $thead.'<tbody>'.$tbody.'</tbody>'.$template.'</table>';  
  echo $output;  
 }
?>
