<div class="modal fade bs-modal-md" id="modal_prereq" tabindex="-1" role="dialog" aria-hidden="false">
 <div class="modal-backdrop fade in"></div>
 <div class="modal-dialog modal-md">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
    <h4 class="modal-title"><i class="fa fa-list-ul"></i> Pre-Requisite,Co-Requisite & Equivalent</h4>
   </div>
   <div class="modal-body">
    <div class="row">
	 <div class="portlet-body form">
	  <div class="well text-center" style="border:1px solid black;padding:4px;">
	   <h5 class="itemsubj">SUBJCODE - SUBJTITLE</h5>
	  </div>
	  <ul class="nav nav-tabs">
	   <li class="active"><a href="#req" data-toggle="tab">Pre-Requisite & Co-Requisite</a></li>
	   <li class=""><a href="#equiv" data-toggle="tab">Equivalent</a></li>
	  </ul>
  	  <div class="tab-content">
       <div class="tab-pane fade active in" id="req">
	    <div class="well"style="min-height:240px;max-height:300px;padding:5px;margin-bottom:5px;overflow:auto;">
          <div class="reqcontent" style="white-space:nowrap;">
		  @include($xview.'.sub.table.prerequisite')
		  </div>
		</div>
	   </div>
       <div class="tab-pane fade" id="equiv">
	    <div class="col-sm-12">
	    <button class="btn btn-xs btn-default equivadd"><i class="fa fa-plus" style="color:green;"></i> Add</button>
	    <button class="btn btn-xs btn-default equivdel"><i class="fa fa-ban"  style="color:red;"></i> Remove</button>
		</div>
	    <div class="well"style="min-height:240px;max-height:300px;padding:5px;margin-bottom:5px;overflow:auto;">
          <div class="equivcontent" style="white-space:nowrap;">
		   <i class="fa fa-warning"></i> Nothing to display.
		  </div>
		</div>
	   </div>
	   
	  </div>
	  <div class="col-sm-12">
	   <p>Pre-Requisite:<b class="sumpre"></b></p>
	   <p>Co-Requisite:<b class="sumco"></b></p>
	   <p>Equivalent:<b class="sumequi"></b></p>
	  </div>
	 </div>
    </div>	
   </div>	
   <div class="modal-footer">
    <button class="btn green btnreqsave">Save</button>
    <button class="btn btn-default btnreqcancl" data-dismiss="modal" aria-hidden="true">Cancel</button>
   </div>
  </div> 
 </div>
</div> 