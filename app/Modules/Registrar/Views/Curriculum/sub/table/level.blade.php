<table class="table table-condense table-bordered" style="background-color:white;white-space:nowrap;">
 <thead>
  <th class="text-center">Year Level</th>
  <th class="text-center">Total Course</th>
  <th class="text-center">Total Units</th>
  <th class="text-center">Min</th>
  <th class="text-center">Max</th>
 </thead>
 <tbody>
 <?php
  if(isset($yrlvl) && $yrlvl)
  {
	$count=0;  
	foreach($yrlvl as $k=>$v)
    {
 ?>	
  <tr class="currlvl" data-id="<?php echo $v['YearLevelID'];?>">
   <td class="yrterm"><?php echo $v['YearLevel'];?></td>
   <td class="subjno text-center"><?php echo number_format($v['TotalSubjects'],0,'.',',');?></td>
   <td class="units text-center"><?php echo number_format($v['TotalUnits'],2,'.',',');?></td>
   <td class="min text-center" style="padding-right:0px;"><input type="text" class="form-control txtmin numberonly" style="width:90%;border:0px;text-align:center;" value="<?php echo number_format($v['MinTarget'],2,'.',',');?>" <?php echo (($count==0)?'disabled':'');?>/></td>
   <td class="max text-center" style="padding-right:0px;"><input type="text" class="form-control txtmax numberonly" style="width:90%;border:0px;text-align:center;" value="<?php echo number_format($v['MaxTarget'],2,'.',',');?>" <?php echo ((($count+1)==count($yrlvl))?'disabled':'');?>/></td>
  </tr>	
 <?php
	$count++;
   }	
  }	  
 ?>
 </tbody>
</table>