<?php
 if(!isset($detail) || !$detail)
 {
?>	 
  <table class="table table-condensed table-bordered table-hover" style="white-space:nowrap;">
   <thead>
	<tr class="warning">
	 <th>YearTerm</th>
	 <th>SubjectCode</th>
	 <th>SubjectTitle</th>
	 <th>Pre-Requisite</th>
	 <th>Co-Requisite</th>
	</tr> 
   </thead>
   <tbody>
    <tr><td colspan="5"><i class="fa fa-warning"></i> Nothing to display.</td></tr>
   </tbody>
  </table>
<?php			   
 }
 else
 {
  $output   = '<table class="table table-condensed table-bordered table-hover" style="background-color:white;white-space:nowrap;">';
  $thead    = '<thead>
                <tr class="warning">
				 <th>YearTerm</th>
				 <th>SubjectCode</th>
				 <th>SubjectTitle</th>
				 <th>Pre-Requisite</th>
				 <th>Co-Requisite</th>
				</tr> 
               </thead>';
			   
  $tbody    = '';
  $tmprmain = '';
  $yrterm   = 0;
  $yrlvl    = 0;
  $yrstd    = '';
  $tmprow   = '';
  $count    = 0;
  $datelock = '';
  $prereq   = '';
  $coreq    = '';
  $prevsubj = '';
  foreach($detail as $rs)
  {
	$indxid   = $rs->IndexID;  
	$datelock = $rs->DateLocked;
	
    if($yrterm<>$rs->YearTermID)
	{
	 $tbody   .= str_replace('<x>',$count,$tmprow);
     $yrterm   = $rs->YearTermID;
	 $termcode = $rs->TermCode;
	 $progclas = $rs->ProgClass;
	 $yrlvl    = $rs->YearLevelID;
	 $yrstd    = $rs->YearLevel;
	 $tmprow   = '';
     $count    = 0;
     $lect     = 0;
     $lab      = 0;
     $crdt     = 0;	 
	}
	
	if($rs->SubjectID!='')	
    {
	 $indxid    = $rs->IndexID;	
	 $subjid    = $rs->SubjectID;	
	 $parent    = $rs->SubjParentID;	
	 $subjcode  = $rs->SubjectCode;	
	 $subjtitle = $rs->SubjectTitle;
	 $option    = $rs->Options;
	 
	 
	 if($prevsubj!='' && $prevsubj == $parent)
	  $subjtitle = '<i class="fa fa-fw"></i>'.$subjtitle;	 
	 else
      $prevsubj = $subjid;		 
	 
	 if($datelock=='')
	 {
	  $ispre  = (($option=='Pre-Requisite')?'checked':'');	 
	  $isco   = (($option=='Co-Requisite')?'checked':'');	 
	  $prereq = '<label><div class="checker chkprereq"><span class="'.$ispre.'"><input type="checkbox" '.$ispre.'/></span></div></label>';	 
	  $coreq  = '<label><div class="checker chkcoreq"><span class="'.$isco.'"><input type="checkbox" '.$isco.'/></span></div></label>';	 
	 }	 
	 else
     {
	  $ispre  = (($option=='Pre-Requisite')?'-check':'');	 
	  $isco   = (($option=='Co-Requisite')?'-check':'');
	  $prereq = '<i class="fa fa-1x fa'.$ispre.'-square-o chkprereq"></i>';	 
	  $coreq  = '<i class="fa fa-1x fa'.$isco.'-square-o chkcoreq"></i>';	 
	 }	
	 
	 $count++;
	 $tmprow   .= '<tr class="bg-white eqsubj" data-main="'.$yrterm.'" data-id="'.$subjid.'" data-parent="'.$parent.'" data-indx="'.$indxid.'">
	                '.(($count==1)?'<td class="subjid" rowspan="<x>">'.$yrstd.'</td>':'').'
					<td class="subjcode">'.$subjcode.' </td>
	                <td class="subjtitle">'.$subjtitle.'</td>
	                <td class="text-center">'.$prereq.'</td>
	                <td class="text-center">'.$coreq.'</td>
	               </tr>';   					   
	}
  }
  
  if($yrterm>0)
  {
	$tbody   .= str_replace('<x>',$count,$tmprow); 
  }	  
  
  $output.= $thead.'<tbody>'.$tbody.'</tbody></table>';  
  echo $output;  
 }
?>
