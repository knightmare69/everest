<?php
 if(!isset($subject) || $subject==false)
  echo '<p><i class="fa fa-warning"></i> Nothing to display.</p>';
 else
 {
  $thead   = '<table class="table table-bordered table-condensed" style="background-color:white;white-space:nowrap;">
               <thead>
			    <th>SubjectID</th>
			    <th>SubjectCode</th>
			    <th>SubjectTitle</th>
			    <th>Lect Units</th>
			    <th>Lab Units</th>
			    <th>Credit Units</th>
			   </thead>
			   <tbody>';
  $tfoot   = '</tbody></table>'; 
  $content = '';
  foreach($subject as $sub)
  {
	$content .= '<tr class="modsubj" data-id="'.$sub->SubjectID.'" data-parent="'.$sub->SubjParentID.'">
	              <td class="subjid">'.$sub->SubjectID.'</td>
	              <td class="subjcode">'.$sub->SubjectCode.'</td>
	              <td class="subjtitle">'.$sub->SubjectTitle.'</td>
	              <td class="lecu" data-hrs="'.$sub->LectHrs.'">'.number_format($sub->AcadUnits,1,'.',',').'</td>
	              <td class="labu" data-hrs="'.$sub->LabHrs.'">'.number_format($sub->LabUnits,1,'.',',').'</td>
	              <td class="crdu">'.number_format($sub->CreditUnits,1,'.',',').'</td>
				 </tr>';  
  }
  
  echo $thead.$content.$tfoot;  
 } 
?>