<div class="modal fade bs-modal-md" id="modal_subject" tabindex="-1" role="dialog" aria-hidden="false">
 <div class="modal-backdrop fade in"></div>
 <div class="modal-dialog modal-md">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
    <h4 class="modal-title"><i class="fa fa-file"></i> Subjects</h4>
   </div>
   <div class="modal-body">
    <div class="row">
	<div class="portlet-body form">
	 <form role="form" class="curr_form" onsubmit="return false;">
     <div class="form-body">
	  <div class="form-group">
	   <label class="col-xs-4 col-sm-2">Search:</label>
	   <div class="col-xs-8 col-sm-9 input-group">
	    <input type="text" class="form-control" name="txtfilter" id="txtfilter"/>
		<span class="input-group-addon btnsearch"><i class="fa fa-search"></i></span>
	   </div>
	  </div>
	 </div>
     </form> 	 
	</div>
	<div class="well vwsubj" style="min-height:200px;max-height:400px;padding:3px;white-space:nowrap;overflow:auto;">
      <i class="fa fa-warning"></i> Nothing to display.	
    </div>	
	</div>
   </div>	
   <div class="modal-footer">
    <button class="btn green btnsubmit">Submit</button>
    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
   </div>
  </div> 
 </div>
</div> 