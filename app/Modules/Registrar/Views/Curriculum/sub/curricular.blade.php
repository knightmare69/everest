<div class="modal fade bs-modal-md" id="modal_currlvl" tabindex="-1" role="dialog" aria-hidden="false">
 <div class="modal-backdrop fade in"></div>
 <div class="modal-dialog modal-md">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
    <h4 class="modal-title"><i class="fa fa-list-ul"></i> Curricular Level</h4>
   </div>
   <div class="modal-body">
    <div class="row">
	 <div class="portlet-body form">
	  <div class="well vwlvl" style="min-height:200px;max-height:400px;padding:3px;white-space:nowrap;overflow:auto;">
	   @include($xview.'.sub.table.level')
	  </div>
	 </div>
	</div>
   </div>
   <div class="modal-footer">
    <button class="btn red btnreset pull-left"><i class="fa fa-refresh"></i> Reset</button>
    <button class="btn green btncursave"><i class="fa fa-save"></i> Save</button>
    <button class="btn btn-default btnreqcancl" data-dismiss="modal" aria-hidden="true">Cancel</button>
   </div>
  </div>
 </div>
</div> 