<?php //error_print($data); die();?>
@if(!empty($data))
<table class="table table-striped table-bordered table-hover table_scroll dataTable no-footer" id="table-students-res">
    <thead>
        <tr>
            <td>Name</td>
            <td>Student No.</td>
            <td width="20%">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $_this)
        <tr>
            <td>
                <p>{{ ucwords(strtolower($_this->LastName)).', '.ucwords(strtolower($_this->FirstName)) }}</p>
            </td>
            <td>{{ $_this->StudentNo }}</td>
            <td width="20%" class="text-center">
                <button data-id="{{ $_this->StudentNo }}" class="btn green btn-sm btn-stud-select-btn">
                    <i class="fa fa-check"></i> Select
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
