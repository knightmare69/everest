<?php
// error_print($progs);
// die();
?>
<div class="row">
    <div class="col-md-4">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i>Report Settings
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                <form class="horizontal-form" id="report-form" action="#" method="POST">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Report Type</label>
                                    <select class="select2 form-control" name="report-type" id="report-type">
                                        <!-- <option value="" selected>Please Select</option> -->
                                        @foreach($rep_types as $key => $_this)
                                        <option value="{{ $key + 1 }}">{{ $_this }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row hide">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Quarter</label>
                                    <select class="select2 form-control" name="period" id="period">
                                        <?php $qtr = ['1st', '2nd', '3rd', '4th']; ?>
                                        @foreach($qtr as $key => $_this)
                                        <option value="{{ $key + 1}}">{{ $_this }} Quarter</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Academic Term</label>
                                    <select class="select2 form-control" name="academic-term" id="academic-term">
                                        @if(!empty($at))
                                            @foreach($at as $_this)
                                            <option value="{{ encode($_this->TermID) }}">{{ $_this->AcademicYear.' - '.$_this->SchoolTerm }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Programs</label>
                                    <select class="select2 form-control" name="programs" id="programs">
                                        <option value="" disabled selected></option>
                                        @if(!empty($progs))
                                            @foreach($progs as $_this)
                                            <option value="{{ encode($_this->ProgID) }}" data-pclass="{{ encode($_this->ProgClass) }}">
                                                {{ $_this->ProgName }}
                                            </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Year Level</label>
                                    <select class="select2 form-control" name="year-level" id="year-level" disabled>
                                        @include($views.'sub/year-level')
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Section</label>
                                    <select class="select2 form-control" name="section" id="section" disabled>
                                        @include($views.'sub/section')
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                         <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Student(s)</label>
                                <div class="input-group">
                                    <input type="text" class="form-control stud-find" data-snum="" name="student-name" id="student-name" disabled>
                                    <span class="input-group-btn">
                                        <button type="button" class="faculty-search-btn btn btn-default stud-find" id="student-find" disabled>
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="faculty-search-btn btn btn-default stud-remove" id="student-remove">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <input type="checkbox" name="print-empty-subj"></input> Exclude subjects with empty grades?
                                </label>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <div class="form-actions right">
                        <!-- <button class="btn default btn_reset" type="button">Reset</button> -->
                        <button class="btn blue btn_action btn_save" type="button" id="print-report"><i class="fa fa-print"></i> Print Report</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
