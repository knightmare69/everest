<style media="screen">
    .align-middle{
        vertical-align: middle !important;
    }
    #request-table tbody tr td {
        white-space: nowrap;
    }
    .r-border-grey{
        border-right-color: #ddd !important;
    }
</style>
<div class="profile-sidebar">
    @include($views.'student')
</div>
<div class="profile-content">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
				<i class="fa fa-download font-blue-steel"></i>
				<span class="caption-subject bold font-blue-steel"> Request Report Card</span>
			</div>
            <div class="actions">
                @include($views.'search')
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    @include($views.'table')
                </div>
            </div>
        </div>
    </div>
</div>
