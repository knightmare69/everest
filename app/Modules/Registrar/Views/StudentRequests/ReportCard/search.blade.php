<form class="form-inline" id="search-form">
    <div class="form-group">
        <select class="form-control select2" name="academic-year" id="academic-year">
            @if(!empty($academic_year))
                @foreach($academic_year as $ay)
                <option value="{{ encode($ay->TermID) }}">{{ $ay->AcademicYear.' '.$ay->SchoolTerm }}</option>
                @endforeach
            @else
            <option value="">Nothing to show.</option>
            @endif
        </select>
    </div>
    <div class="form-group">
        <select class="form-control select2" id="period" name="period">
            @if(!empty($periods))
                @foreach($periods as $p)
                    @if($p->GroupClass == 21)
                    <option value="{{ encode($p->PeriodID) }}">{{ $p->Description1 }}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</form>
