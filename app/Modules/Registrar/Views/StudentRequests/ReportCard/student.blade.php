
<div class="portlet light bordered  profile-sidebar-portlet ">
    <div class="profile-userpic"><img alt="" class="img-responsive" id="student-photo" src="{{ asset('assets/admin/layout/img/logo.png') }}"></div>
    <br />
    <div class="profile-usertitle text-center">
        <div class="profile-usertitle-name"> [Student Name] </div>
        <div class="profile-usertitle-job"> [Student No.] </div>
    </div>

    <div class="profile-userbuttons ">
        <!-- <button class="btn btn-circle btn-primary btn-sm" type="button">Accept</button> -->
        <button class="btn btn-circle green btn-sm" type="button" id="btn-issue" disabled>Issue Report Card</button>
    </div>
    <div class="profile-usermenu"></div>
</div>
