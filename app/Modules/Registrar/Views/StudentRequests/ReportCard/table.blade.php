<table class="table table-striped table-bordered table-hover dataTable no-footer" id="request-table">
    <thead>
        <tr>
            <th rowspan="2" class="align-middle r-border-grey bg-grey-steel">#</th>
            <th rowspan="2" class="align-middle r-border-grey bg-grey-steel">Student No</th>
            <th rowspan="2" class="align-middle r-border-grey bg-grey-steel">Full Name</th>
            <th rowspan="2" class="align-middle r-border-grey bg-grey-steel">Gender</th>
            <th colspan="2" scope="colgroup" class="text-center bg-blue">Request</th>
            <th colspan="2" scope="col" class="text-center bg-green">Issued</th>
        </tr>
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Status</th>
            <th scope="col">Date</th>
            <th scope="col">By</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
