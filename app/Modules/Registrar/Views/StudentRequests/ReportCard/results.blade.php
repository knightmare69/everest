@if(!empty($data))
    @foreach($data as $cnt => $_this)
    <tr>
        <td>{{ $cnt + 1 }}</td>
        <td>{{ $_this->StudentNo }}</td>
        <td><a href="#!" class="student-select" data-id="{{ encode($_this->StudReqID) }}" data-img-src="{{ url() .'/general/getPupilPhoto?Idno='. encode($_this->StudentNo) }}">{{ $_this->LastName.', '.$_this->FirstName.' '.$_this->MiddleInitial }}</a></td>
        <td>{{ $_this->Gender }}</td>
        <td>{{ date('Y-m-d', strtotime($_this->RequestDate)) }}</td>
        <td>{{ $_this->IssuedDate != '' ? 'Done' : 'Pending' }}</td>
        <td>@if($_this->IssuedDate != '')
                {{ date('Y-m-d', strtotime($_this->IssuedDate)) }}
            @endif</td>
        <td>{{ $_this->IssuedBy }}</td>
    </tr>
    @endforeach
@endif
