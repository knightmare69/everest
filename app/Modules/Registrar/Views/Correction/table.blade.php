<?php 
    $i=1;
    
    $model = new \App\Modules\Students\Models\StudentProfile;
    
    global $conduct;
    
    $conduct = $model->getConductGrade($term, $stud)->orderBy('PeriodID','asc')->get();
    
    function getConduct($idx, $per){
        global $conduct;
        $output='';
        
        foreach($conduct as $r){            
            if ($r->ConductID == $idx && $r->PeriodID == $per ){
                $output = $r->Score;
                break; 
            }                                          
        }        
        return $output;        
    }                

?>

<table class="table table-bordered table-condensed table-striped table-hover " style="cursor: pointer;">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Descriptive</th>
            <th class="text-center">Units</th>
            <th class="text-center">1st</th>
            <th class="text-center">2nd</th>
            @if($shs == 0)
            <th class="text-center">3rd</th>
            <th class="text-center">4th</th>
            @endif
            <th class="text-center">Final</th>
            
            @if($shs == 1)
            <th class="text-center">Remedial</th>
            <th class="text-center">RFG</th>
            @endif
            
            <th class="text-center">Remarks</th>
            <th>Action</th>
        </tr>        
    </thead>
    <tbody>
        @foreach($grades AS $g)
            <?php
                $css = "";
                if( strtolower($g->Final_Remarks) == 'failed'){
                    $css = "text-danger";
                }
                
            ?>
        <tr class="{{$css}}" data-idx="{{ encode($g->GradeIDX) }}" data-code="{{$g->SubjectCode}}" data-title="{{$g->SubjectTitle}}">
            <td class="autofit font-xs">{{$i}}.</td>
            <td class="autofit font-xs bold code" data-weight="{{ $g->Weight }}" data-units="{{$g->CreditUnit}}">
                @if($g->ParentSubjectID > 0 )
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
                {{$g->SubjectCode}}
            </td>
            <td class="title">
                @if($g->ParentSubjectID > 0 )
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
                {{ $g->SubjectTitle}}
            </td>
            <td class="units font-xs text-center bold">{{$g->CreditUnit}}</td>
            <td class="text-center avea bold ">{{$g->AverageA}}</td>
            <td class="text-center aveb bold ">{{$g->AverageB}}</td>
            
            @if($shs == 0)
                <td class="text-center avec bold ">{{$g->AverageC}}</td>
                <td class="text-center aved bold ">{{$g->AverageD}}</td>  
            @else
                <td class="bold font-blue-madison text-center">{{$g->TotalAverageAB}}</td>
                <td class="bold font-blue-madison avec text-center">{{$g->AverageC}}</td>      
            @endif                                                
            
            <td class="bold font-blue-madison text-center">{{$g->Final_Average}}</td>                        
            <td class="bold font-blue-madison text-center">{{$g->Final_Remarks}}</td>
            
            <td class="action autofit"> 
                <a href="javascript:void(0);" class="change-grade" > Change </a> |
                <a href="javascript:void(0);" class="history" > History </a> 
            </td>
            
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
</table>
<hr />
<!-- <h3>Conduct Grade </h3>
<table class="table table-condensed table-striped table-bordered">
    <thead>
        <tr>
            <th rowspan="2" class="text-center">CORE VALUES</th>
            <th rowspan="2" class="text-center">BEHAVIOR STATEMENT</th>
            <th colspan="2" class="text-center">TERM</th>
        </tr>
        <tr>
            <th class="text-center">1st Term</th>
            <th class="text-center">2nd Term</th>
        </tr>
    </thead>
    <tbody>
    
    <tr>
        <td rowspan="2" class="bold text-center">Maka-Diyos</td>           
        <td> Expresses one's spiritual beliefs while respecting spiritaul beliefs of others	 </td>
        <td class="text-center bold"><?= getConduct(1,11); ?></td>
        <td class="text-center bold"><?= getConduct(1,12); ?></td>     
    </tr>
    <tr>
        
        <td>Shows adherence to ethical principles by upholding truth</td>
        <td class="text-center bold"><?= getConduct(2,11); ?></td>
        <td class="text-center bold"><?= getConduct(2,12); ?></td>     
    </tr>
    <tr>
        <td rowspan="2" class="bold text-center">
            Makatao
        </td>           
        <td> Is sensitive to individual, social, and cultural differences </td>
        <td class="text-center bold"><?= getConduct(3,11); ?></td>
        <td class="text-center bold"><?= getConduct(3,12); ?></td>     
    </tr>
    <tr>        
        <td>Demonstrate contribution towards solidarity</td>
        <td class="text-center bold"><?= getConduct(4,11); ?></td>
        <td class="text-center bold"><?= getConduct(4,12); ?></td>             
    </tr>
    <tr>
        <td class="bold text-center" >
            Makakalikasan
        </td>   
        
        <td> Cares for the environment and utilizes resources wisely, judiciously, and economically </td>
        <td class="text-center bold"><?= getConduct(5,11); ?></td>
        <td class="text-center bold"><?= getConduct(5,12); ?></td>     
    </tr>
    <tr>
        <td rowspan="2" class="bold text-center">
            Makabansa
        </td>   
        
        <td> Demonstrate pride in being a Filipino; exercises the rights and responsibilities of a Filipino citizen. </td>
        <td class="text-center bold"><?= getConduct(6,11); ?></td>
        <td class="text-center bold"><?= getConduct(6,12); ?></td>     
    </tr>
    <tr>        
        <td>Demonstrates appropriate behavior in carrying out activities in the school, community, and country</td>
        <td class="text-center bold"><?= getConduct(7,11); ?></td>
        <td class="text-center bold"><?= getConduct(7,12); ?></td>     
    </tr>
    </tbody>
</table> -->