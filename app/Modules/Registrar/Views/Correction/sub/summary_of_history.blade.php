<?php
    $model = new \App\Modules\Registrar\Models\Correction;
    $history = $model->HistoryByStudents($term,$student)->get();
    $i=1;        
?>
@if(!empty($history))
<table class="table table-striped table-bordered table-hover table_scroll " id="table-summaryhistory">
    <thead>
        <tr>
            <th class="autofit">#</th>
            <th>Subject Code</th>
            <th>Period</th>
            <th class="autofit">Old Grade</th>
            <th class="autofit">New Grade</th>
            <th>Reason</th>
            <th>Corrected by</th>
            <th>Date Corrected</th>
        </tr>
    </thead>
    <tbody>
        @foreach($history as $r)
        <tr data-id="{{encode($r->IndexID)}}" >
            <td>{{ $i }}.</td>
            <td>{{$r->SubjectCode}}</td>
            <td>{{ getPeriodName($r->PeriodID) }}</td>
            <td>{{$r->OldGrade}}</td>
            <td>{{ $r->NewGrade }}</td>
            <td>{{ $r->Reason }}</td>
            <td>{{ getUserNameByID($r->UserID)}}</td>
            <td class="autofit">{{ $r->DateCorrected }}</td>            
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
</table>
@endif
