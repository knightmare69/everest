 <?php // error_print($event); ?>
 <form class="form-horizontal" id="event_form" name="event_form" action="?" >
    <div class="form-body">
        
        <div class="form-group">
    		<label class="col-md-3 control-label">Subject Code </label>
    		<div class="col-md-6">
    			<input type="text" placeholder="" name="code" id="code" class="form-control " readonly="" value="">
    		</div>
    	</div>
        
        <div class="form-group">
    		<label class="col-md-3 control-label">Subject Title </label>
    		<div class="col-md-6">
    			<input type="text" placeholder="" name="title" id="title" class="form-control " readonly="" value="">
    		</div>
    	</div>
        
        <div class="form-group">
    		<label class="col-md-3 control-label">Period</label>
    		<div class="col-md-6">
    			<select id="period" name="period" class="form-control  bs-select " data-live-search="true">
                    <option value="-1"> - Select Period - </option>
                    
                </select>
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Grade </label>
    		<div class="col-md-6">
    			<input type="text" placeholder="" readonly="" name="grade" id="grade" class="form-control " value="">
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Correction</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter Correction" name="correction" id="correction"  class="form-control " value="" >
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">GPA</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter GPA" name="gpa" id="gpa"  class="form-control " value="" >
    		</div>
    	</div>

        <div class="form-group">
    		<label class="col-md-3 control-label">Remarks</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="" readonly="" name="remarks" id="remarks" class="form-control" value="">
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Reason</label>
    		<div class="col-md-6">
    			<textarea class="form-control" style="resize: none !important;" id="reason" name="reason"></textarea>
    		</div>
    	</div>
    </div>
        <input type="hidden" id="idx" name="idx" value="" />
</form>