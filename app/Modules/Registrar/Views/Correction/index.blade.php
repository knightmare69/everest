<div class="row">
<div class="col-md-12">
	<!-- BEGIN PROFILE SIDEBAR -->
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img alt="" class="img-responsive" src="{{ asset('assets/admin/layout/img/logo.png') }}">
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name" id="student-name">
					 [Student Name]
				</div>
				<div class="profile-usertitle-job" id="student-num">
					 [Student No.]
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<div class="profile-userbuttons">
				<button class="btn btn-circle green-haze btn-sm" id="student" type="button"> Search </button>
				
			</div>
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active">
						<a href="javascript:void(0);" data-menu="grades"><i class="icon-home"></i> Correction of Grades </a>
					</li>
					<li>
						<a href="javascript:void(0);" data-menu="history"><i class="icon-user"></i> History </a>
					</li>
					<!-- <li>
						<a target="_blank" href="javascript:void(0);">
						<i class="icon-check"></i>
						Conduct </a>
					</li> -->
					<li>
						<a href="javascript:void(0);">
						<i class="icon-info"></i>
						Help </a>
					</li>
				</ul>
			</div>
			<!-- END MENU -->
		</div>												
		
	</div>
	<!-- END BEGIN PROFILE SIDEBAR -->
	<!-- BEGIN PROFILE CONTENT -->
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET -->
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font font-blue-madison"></i>
							<span class="caption-subject theme-font font-blue-madison bold uppercase">Student Grades</span>
							<span class="caption-helper">This module is intent for correction of student grades</span>
						</div>
						<div class="actions">
							 <div class="portlet-input input-inline input-medium">
                                <select class="form-control input-sm select2" name="term" id="term">
                                    <option value="-1"> - Select Academic Year - </option>
                                    @if(!empty($academic_year))
                                        @foreach($academic_year as $ayt)                                                                                    
                                            <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>                                                            
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <button type="button" class="btn btn-default" id="reload" ><i class="fa fa-refresh"></i> Refresh </button>  
						</div>
					</div>
					<div class="portlet-body" >	
                        <div id="grades">
                        									
						<div class="table-scrollable table-scrollable-borderless">
							<table class="table table-hover table-light">
							<thead>
							<tr class="uppercase">
								<th colspan="2">
									 Subject Code
								</th>
								<th>
									 Descriptive Title
								</th>
								<th>
									 Grade
								</th>
								<th>
									 Correction
								</th>
								<th>
									 Remarks
								</th>
							</tr>
							</thead>
							<tbody>
                            
                            </tbody>
                            </table>
						</div>
                        </div>
                        <div id="history">
                        </div>
                        
					</div>
				</div>				
			</div>			
		</div>		
	</div>	
</div>
</div>
<div class="hide" id="correction-container">
    @include($views.'sub.correction')
</div>