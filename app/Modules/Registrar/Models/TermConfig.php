<?php

namespace App\Modules\Registrar\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class TermConfig extends Model
{
    
    protected $table='ES_GS_TermConfig';
	protected $primaryKey ='SetupID';

	protected $fillable  = array(
    	'TermID',
    	'PublishFrom',
    	'PublishTo',
    	'Createdby',
        'DateCreated',
        'FirstTerm',
        'SecondTerm',
        'ClassRecord',
        'AllowedToEnroll',
        'Modifiedby',
        'DateModified',
		'Period1',
		'Period2',
		'Period3',
		'Period4'
	);

	public $timestamps = false;
    
    public function AcademicTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm','IsCurrentTerm','Active_OnlineEnrolment')
        // ->where('SchoolTerm', 'School Year')
        ->where('Hidden','0')        
        ->orderBy('AcademicYear', 'DESC')
        ->orderBy('SchoolTerm', 'DESC')
        ->get();

        return $get;
    }

    public function Programs()
    {
        $get = DB::table('ES_Programs')->select('ProgID', 'ProgName', 'ProgShortName', 'ProgClass')
        ->whereIn('ProgShortName', ['Grade School', 'High School', 'Senior High School'])
                // ->whereNotIn('ProgClass', [50, 80, 60])
                ->orderBy('ProgClass', 'ASC')->get();

        return $get;
    }

    public function getYearLevel($prog_class)
    {
        if($prog_class >= 50){
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->whereIn('YearLevel', ['Grade 11', 'Grade 12'])
                ->orderBy('SeqNo', 'ASC')->get();
        } else {
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->where('ProgClass', $prog_class)
                ->orderBy('SeqNo', 'ASC')->get();
        }
        
        return $at;
    }

   

    public function getStudent($term)
    {
        $student = DB::table('ES_Registrations as r')
            ->select('s.StudentNo', 's.FirstName', 's.LastName', 's.MiddleName', 's.MiddleInitial')
            ->join('ES_Students as s', 'r.StudentNo', '=', 's.StudentNo')
            ->where([
                'r.TermID' => $term,                
            ])            
            ->whereIn('r.ProgID',['1','21','29'])
            ->get();

        return $student;
    
    }
    
    public function getPeriod($progid)
    {
         $get = DB::table('ES_GS_GradingPeriods')         
                ->where('GroupClass', $progid)                
                ->get();
        return $get;
    }
    
    public function getTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm');        
        return $get;
    }

    
}