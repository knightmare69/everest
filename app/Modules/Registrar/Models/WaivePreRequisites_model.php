<?php

namespace App\Modules\Registrar\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class WaivePreRequisites_model extends Model
{
    protected $table='ES_WaivePreRequisites';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(        
        'StudentNo',
        'CurriculumPK',
        'CourseID' ,
	    'PreRequisiteCourseID',
	    'CreatedBy',
        'CreatedDate'	    
	);

	public $timestamps = false;   
}