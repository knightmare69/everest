<?php

namespace App\Modules\Registrar\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Reports extends Model
{
    public static function AcademicTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm', 'Active_OnlineEnrolment')
        // ->where('SchoolTerm', 'School Year')
        ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function Programs()
    {
        $get = DB::table('ES_Programs')->select('ProgID', 'ProgName', 'ProgShortName', 'ProgClass')
        ->whereIn('ProgShortName', ['LS', 'K', 'US', 'MS'])
                // ->whereNotIn('ProgClass', [50, 80, 60])
                ->orderBy('SortOrder', 'ASC')->get();

        return $get;
    }

    public function getYearLevel($prog_class)
    {
        if($prog_class >= 50){
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->whereIn('YearLevel', ['Grade 11', 'Grade 12'])
                ->orderBy('SeqNo', 'ASC')->get();
        } else {
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->where('ProgClass', $prog_class)
                ->orderBy('SeqNo', 'ASC')->get();
        }

        return $at;
    }

    public function getSections($term_id, $prog_id, $yl_id)
    {
        $sec = DB::table('ES_ClassSections')->select('SectionID', 'SectionName')
                ->where(['TermID' => $term_id, 'ProgramID' => $prog_id, 'YearLevelID' => $yl_id])
                ->orderBy('SectionName', 'ASC')->get();

        return $sec;
    }

    public function getStudent($find_str, $term, $program, $year_level, $section_id)
    {
        $student = DB::table('ES_Registrations as r')
                    ->select('s.StudentNo', 's.FirstName', 's.LastName', 's.MiddleName', 's.MiddleInitial')
                    ->join('ES_Students as s', 'r.StudentNo', '=', 's.StudentNo')
                    ->where(['r.ProgID' => $program, 'r.YearLevelID' => $year_level, 'r.TermID' => $term])
                    ->where(function($query) use ($find_str){
                        $query->where('r.StudentNo', '=', $find_str)
                        ->orWhere('s.LastName', 'like', '%'.$find_str.'%')
                        ->orWhere('s.FirstName', 'like', '%'.$find_str.'%');
                    });

        if($section_id != 0){
            $student->where(['r.ClassSectionID' => $section_id]);
        }

        return $student->get();
    }

    public function getStudentSectionID($student_no, $term_id, $prog_id, $year_level_id)
    {
        $get = DB::table('ES_Registrations')->select('ClassSectionID')
                ->where(['StudentNo' => $student_no, 'TermID' => $term_id, 'ProgID' => $prog_id, 'YearLevelID' => $year_level_id])->first();

        $ret = !empty($get->ClassSectionID) ? $get->ClassSectionID : 0;
        return $ret;
    }

    public function getProjections($term, $prog){
  
        $get = DB::select("SELECT ISNULL(p.IndexID,0) AS IndexID, ProgClass, {$term} AS TermID , 1 AS CampusID, y.ProgID, y.YLID_OldValue AS YearLevelID, y.YearLevelName AS YearLevel,
                        dbo.fn_ProgramCollegeID(y.ProgID) AS CollegeID, 0 AS MajorID, 0 AS Major,
                        ISNULL(p.NoSections,0) As NoSections, 
                        ISNULL(p.NoStudents,0) As NoStudents
                        FROM dbo.ESv2_YearLevel y 
                        LEFT JOIN ES_EnrollmentProjections p ON p.ProgID = y.ProgID  AND p.YearLevelID=y.YLID_OldValue 
                        WHERE y.ProgID = {$prog}");

        return $get;

    }

    public function setProjections($term, $post){

        $prog = decode(getObjectValue($post,'prog'));

        $whre = array(
            'TermID' => $term,
            'ProgID' => $prog,
            'MajorID' => getObjectValue($post,'major'),
            'YearLevelID' => getObjectValue($post,'level'),

        );

        $data = array(
            'TermID' => $term,
            'CampusID' => 1,
            'ProgID' => $prog,
            'MajorID' => getObjectValue($post,'major'),
            'YearLevelID' => getObjectValue($post,'level'),
            'NoSections' => getObjectValue($post,'sec'),
            'NoStudents' => getObjectValue($post,'stu'),
        );

        $ret = DB::table('ES_EnrollmentProjections')->where($whre)->count();
        
        if($ret > 0 ){
            $ret = DB::table('ES_EnrollmentProjections')->where($whre)->update($data);
        }else{
            $ret = DB::table('ES_EnrollmentProjections')->insert($data);
        }
        
        return $ret;
    }
}