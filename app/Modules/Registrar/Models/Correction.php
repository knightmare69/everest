<?php

namespace App\Modules\Registrar\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Correction extends Model
{
    
    protected $table='ES_GS_GradeCorrection';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
    	'GradeIDX',
    	'PeriodID',
    	'OldGrade',
    	'NewGrade',
        'Reason',
    	'UserID',
    	'DateCorrected',
	);

	public $timestamps = false;
    
    public function AcademicTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')
        // ->where('SchoolTerm', 'School Year')
        ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function HistoryByStudents($term, $student)
    {        
        $get = DB::table('ES_GS_GradeCorrection As c')
            ->leftJoin('ES_PermanentRecord_Details As d', 'd.GradeIDX','=','c.GradeIDX')
            ->select([DB::raw("c.IndexID, c.PeriodID, d.SubjectCode, d.SubjectTitle, c.OldGrade, c.NewGrade, c.Reason, c.UserID, c.DateCorrected  ")])
            ->where(['d.StudentNo' => $student, 'd.TermID'=> $term ])
            ->orderBy('DateCorrected', 'Desc');

        return $get;
    }

    public function getYearLevel($prog_class)
    {
        if($prog_class >= 50){
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->whereIn('YearLevel', ['Grade 11', 'Grade 12'])
                ->orderBy('SeqNo', 'ASC')->get();
        } else {
            $at = DB::table('vw_YearLevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->where('ProgClass', $prog_class)
                ->orderBy('SeqNo', 'ASC')->get();
        }
        
        return $at;
    }

   

    public function getStudent($term)
    {
        $student = DB::table('ES_Registrations as r')
            ->select('s.StudentNo', 's.FirstName', 's.LastName', 's.MiddleName', 's.MiddleInitial')
            ->join('ES_Students as s', 'r.StudentNo', '=', 's.StudentNo')
            ->where([
                'r.TermID' => $term,                
            ])            
            // ->whereIn('r.ProgID',['1','21','29'])
            ->get();

        return $student;
    
    }
}
