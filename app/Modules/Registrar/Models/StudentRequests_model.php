<?php

namespace App\Modules\Registrar\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class StudentRequests_model extends Model
{
    protected $table='ES_GS_StudentRequests';
	protected $primaryKey ='StudReqID';

	protected $fillable  = array(
        'StudentNo',
        'RequestTypeID',
        'RequestDate',
        'RequestBy' ,
	   'IssuedDate',
	   'IssuedBy'
	);

	public $timestamps = false;

    public function getStudents($term, $period, $request_type)
    {
        $get = DB::table($this->table.' as sr')
                ->select('sr.StudReqID', 'sr.StudentNo', 's.LastName', 's.FirstName', 's.MiddleInitial', 's.Gender', 'sr.RequestDate', 'sr.IssuedDate', 'u.FullName as IssuedBy')
                ->join('ES_Students as s', 's.StudentNo', '=', 'sr.StudentNo')
                ->leftjoin('Esv2_Users as u', 'u.UserIDX', '=', 'sr.IssuedBy')
                ->where(['sr.TermID' => $term, 'sr.PeriodID' => $period, 'sr.RequestTypeID' => $request_type])
                ->get();

        return $get;
    }

    public function getReportDetails($stud_num, $term_id)
    {
        $get = DB::table('ES_Registrations')->select('YearLevelID', 'ProgID', 'ClassSectionID')->where(['TermID' => $term_id, 'StudentNo' => $stud_num])->first();

        return $get;
    }
}
