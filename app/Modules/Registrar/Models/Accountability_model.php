<?php

namespace App\Modules\Registrar\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Accountability_model extends Model
{
    protected $table='ES_StudentAccountabilities';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
        'TermID',
        'StudentNo',
        'Reason',
        'Cleared' ,
	    'DateEntered',
	    'EnteredBy',
        'DateUpdate',
	    'UpdateBy'
	);

	public $timestamps = false;   
}