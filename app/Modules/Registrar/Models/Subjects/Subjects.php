<?php 
namespace App\Modules\Registrar\Models\Subjects;

use illuminate\Database\Eloquent\Model;
use DB;

Class Subjects extends Model {

	protected $table='ES_Subjects';
	public $primaryKey ='SubjectID';

	protected $fillable  = array(
    	'SubjectCode',
    	'SubjectTitle',
    	'SubjectDesc',
    	'SubjectGE',
    	'SubjectElective',
    	'SubjectMajor',	
    	'SubjectComputer',
    	'InclGWA',
    	'Inactive',
    	'AcadUnits',
    	'CreditUnits',
    	'LectHrs',
    	'LabUnits',
    	'LabHrs',
    	'SubjectAreaID',
    	'LevelID',
    	'SubjectMode',
    	'MinSize',
    	'MaxSize',
    	'InclTFCompute',
    	'InclLFCompute',
    	'IsNonAcademic',
    	'Alias1',
    	'Alias2',
    	'CollegeOwner',
    	'OtherSchool',
    	'OldSubjectCode',
    	'OldSubjectTitle',
    	'OldSubjectDescription',
    	'ChargeAirconLecUnit',
    	'ChargeAirconLabUnit',
    	'DateLocked',
    	'SubjectELearning',
    	'IsTransmutedGrade',
    	'SubjectWithInternet',
    	'IsClubOrganization',
    	'SubjParentID',
    	'SubjCompPercentage',
    	'LockedBy',
    	'CreatedBy',
    	'CreatedDate',
    	'ModifiedBy',
    	'ModifiedDate',
    	'IsPracticum',
    	'SubjGroupID',
    	'ChargeAirconCreditUnit',
    	'Alias3',
    	'TargetHoursPerWeek',
    	'TransferedSubjectID',
    	'ThesisSubject',
    	'ChineseSubject',
    	'Weight',
    	'LetterGradeOnly',
    	'IsChinese',
    	'ShowGradeComponents',
    	'ShowPrelimColumn',
    	'ShowMidtermColumn',
    	'ShowFinalColumn',
        'IsSemestral'
       ,'IsNonConduct'
       ,'SuppressSubject'
       ,'SuppressAcademicGrade'
       ,'SuppressConductGrade'
	);

	public $timestamps = false;
}