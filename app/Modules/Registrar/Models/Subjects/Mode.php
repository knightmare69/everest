<?php 
namespace App\Modules\Registrar\Models\Subjects;

use illuminate\Database\Eloquent\Model;

Class Mode extends Model {

	protected $table='ES_SubjectMode';
	protected $primaryKey ='SubjectMode';

	protected $fillable  = array(
			'SubjectModeDesc',
			'ShortName',
			'RequiredSchedule',
	);

	public $timestamps = false;
}