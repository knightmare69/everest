<?php 
namespace App\Modules\Registrar\Models\Subjects;

use illuminate\Database\Eloquent\Model;

Class Area extends Model {

	protected $table='ES_SubjectAreas';
	protected $primaryKey ='SubjectAreaID';

	protected $fillable  = array(
			'SubjectAreaName',
			'ShortName',
			'NoMeetings',
			'SessionSingle',
			'SessionDouble',
			'SeqNo',
			'ProgClassID',
			'Inactive',
			'StatusID',
			'TermAssignment',
			'TargetHoursPerWeek',
	);

	public $timestamps = false;
}