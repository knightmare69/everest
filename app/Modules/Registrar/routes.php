<?php
	Route::group(['prefix' => 'registrar','middleware'=>'auth'], function () {
	    Route::group(['prefix' => 'subjects'], function () {
		    Route::get('/','Subjects@index');
		    Route::post('/event','Subjects@event');
		});		
		
		Route::group(['prefix' => 'curriculum'], function(){
			Route::get('/', 'Curriculum@index');
			Route::get('/txn','Curriculum@txn');
			Route::post('/txn','Curriculum@txn');
		});
		
		Route::group(['prefix' => 'report-card', 'middleware' => 'auth'], function () {
            Route::get('/', 'Reports@report_card_only');
            Route::post('event', 'Reports@event');
			Route::get('print', 'Reports@print_report');
            Route::get('export', 'Reports@export');
        });

		Route::group(['prefix' => 'reports', 'middleware' => 'auth'], function () {
            Route::get('/', 'Reports@index');
            Route::post('event', 'Reports@event');
			Route::get('print', 'Reports@print_report');
            Route::get('export', 'Reports@export');
        });
        
        Route::group(['prefix' => 'correction-of-grades', 'middleware' => 'auth'], function () {
            Route::get('/', 'Correction@index');
            Route::post('event', 'Correction@event');			
        });

        Route::group(['prefix' => 'request-report-card', 'middleware' => 'auth'], function () {
            Route::get('/', 'StudentRequests@requestReportIndex');
        });

        Route::group(['prefix' => 'student-requests', 'middleware' => 'auth'], function () {
            // Route::get('/', 'StudentRequests@index');
            Route::post('event', 'StudentRequests@event');
        });
        
        Route::group(['prefix' => 'school-policy', 'middleware' => 'auth'], function () {
            Route::get('/', 'SchoolPolicy@index');
            Route::post('event', 'SchoolPolicy@event');			
        });

        #Date : Nov.29.2016 08:53H
        Route::group(['prefix' => 'grading-sheet-inventory', 'middleware' => 'auth'], function () {
            Route::get('/', 'Inventory@index');
            Route::get('/view', 'Inventory@view');
            Route::post('event', 'Inventory@event');			
        });
        
        #Date : Mar.13.2017 12:03H
        Route::group(['prefix' => 'accountability', 'middleware' => 'auth'], function () {
            Route::get('/', 'Accountability@index');
            Route::post('event', 'Accountability@event');			
        });
        
        #Date : MAY2520171510H
        Route::group(['prefix' => 'transcript', 'middleware' => 'auth'], function () {
            Route::get('/', 'Transcript@index');
            Route::post('event', 'transcript@event');			
        });
        
	
	});
?>