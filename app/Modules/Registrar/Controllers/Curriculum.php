<?php
namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Services\Curriculum\SvcCurriculum as svc;
use App\Libraries\Mmpdf as pdf;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;
use DB;
use Auth;

class Curriculum extends Controller
{
  private $media =
  [
        'Title'         => 'Curriculum',
        'Description'   => 'Process',
        'js'            => ['Registrar/curriculum/curriculum',],
        'plugin_js'     => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
							'jquery-validation/js/jquery.validate.min',
							'jquery-validation/js/additional-methods.min',
							'bootstrap-wizard/jquery.bootstrap.wizard.min',
                           ],
  ];	
  
  private $url = 
  [
	'page'  => 'registrar/curriculum/',
	'form' => 'form',
  ];
  
  public $c_view = 'Registrar.Views.Curriculum';
  
  private function initializer()
  {
    $this->svc        = new svc;
    $this->pdf        = new pdf;
    $this->xpdf       = new xpdf;
    $this->permission = new Permission('registrar-curriculum');
  }
  
  public function init($apage='main')
  {
	$this->initializer();
	$data = array('xview'    => $this->c_view,
				  'apage'    => $apage,);
	if($apage=='main')
	{	
     $data['detail'] = '';
	 $data['campus'] = $this->svc->dropdown(array('table'    =>'ES_Campus'
	                                             ,'col_id'   =>'CampusID'
												 ,'col_desc' =>array('Acronym','ShortName')
											     ,'col_sep'  =>' - '
												 ,'name'     =>'campus'
												 ,'cls'      =>'form-control'
												));
	 $data['program'] = $this->svc->dropdown(array('table'    =>'ES_Programs'
	                                              ,'col_id'   =>'ProgID'
												  ,'col_desc' =>array('ProgCode','ProgName')
											      ,'col_sep'  =>' - '
												  ,'name'     =>'program'
												  ,'cls'      =>'form-control'
												  ,'init'     =>'-1'
												  ,'order'    =>array('ProgClass'=>'ASC','ProgID'=>'ASC')
												 ));
	}											 
	return $data;
  }
  
  public function index()
  {	
    $this->initializer();
	if ($this->permission->has('read')) {
      return view('layout',array('content'=>view($this->c_view.'.index',$this->init('main')),'url'=>$this->url,'media'=>$this->media));
    }
    return view(config('app.403'));
  }
  
  public function txn()
  {
	$this->initializer();
	ini_set('max_execution_time', 60);
    $response = 'No Event Selected';
    if (Request::ajax())
    {
	 $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
     switch(Request::get('event'))
     {
	  case 'progdata':
	    if(!$this->permission->has('read')){break;}
	    $progid = Request::get('progid');
		$data   = $this->svc->get_program_details($progid);
		if($data)
		{	
		 $major = $this->svc->dropdown(array('table'     =>'ES_ProgramMajors'
								             ,'col_id'    =>'MajorDiscID'
								             ,'col_desc'  =>array('MajorCode','MajorName')
								             ,'col_sep'   =>' - '
								             ,'sel_field' =>'IndexID, MajorDiscID, dbo.fn_MajorCode(MajorDiscID) AS MajorCode, dbo.fn_MajorName(MajorDiscID) AS MajorName, ProgID'
								             ,'name'      =>'major'
								             ,'cls'       =>'form-control'
											 ,'init'      =>'-1'
							                 ,'condition' =>"ProgID='".$progid."'"));
		 $curr  = $this->svc->get_curriculum($progid);
	     $cdata = $this->svc->get_cdetails($progid,0);
		 
		 $response['success'] = true;
         $response['content'] = (string)view($this->c_view.'.sub.curriculum',array('detail'=>$cdata));
		 $response['pdata']   = $data;
		 $response['major']   = $major;
		 $response['curr']    = $curr;
		}
        else
         $response['message']='Failed to load Program Details';			
      break;	
      case 'currdata':
	   if(!$this->permission->has('read')){break;}
	   $prog  = Request::get('progid');
	   $curr  = Request::get('currid');
	   $cdata = $this->svc->get_cdetails($prog,$curr);
	   $response['success'] = true;
       $response['content'] = (string)view($this->c_view.'.sub.curriculum',array('detail'=>$cdata));
      break;	 
      case 'savecurr':
	   if(!$this->permission->has('edit')){break;}
	   $curr  = Request::get('cid');
	   $prog  = Request::get('progid');
	   $cdata = Request::all();
	   $exec  = $this->svc->save_curriculum($curr,$cdata);
	   if($exec!=false)
	   {
		 $curr  = (($curr=='new')?$exec:$curr);
		 $cdata = $this->svc->get_cdetails($prog,$curr);
	     $response['success'] = true;  
		 $response['cid']     = $curr;
	     $response['content'] = (string)view($this->c_view.'.sub.curriculum',array('detail'=>$cdata));
	   }
	   else
		 $response['message']='Failed to save data';
      break;
      case 'delcurr':
	   if(!$this->permission->has('delete')){break;}
	   $curr  = Request::get('currid');
	   $exec  = $this->svc->del_curriculum($curr);
       $response['success'] = $exec['result'];       
	   $response['message'] = $exec['message'];
	  break;	  
	  case 'getsubj':
	   if(!$this->permission->has('read')){break;}
	   $class  = Request::get('progcls');
	   $filter = Request::get('filter');
	   $exec   = $this->svc->get_subjects($class,$filter);
	   $response['success'] = true;
       $response['content'] = (string)view($this->c_view.'.sub.table.tsubject',array('subject'=>$exec));	   
	  break;
	  case 'savecdetail':
	   if(!$this->permission->has('edit')){break;}
	   $xid   = Request::get('xid');
	   $sdata = Request::all();
	   $exec  = $this->svc->save_cdetail($xid,$sdata);
	   if($exec)
	   {
		$response['success'] = true;
		$response['indxid']  = $exec;
	   }
       else	   
	    $response['message']='Failed to save data';
	  break;
	  case 'delcdetail':
	   if(!$this->permission->has('delete')){break;}
	   $xid   = Request::get('xid');
	   $exec  = $this->svc->del_cdetail($xid);
	   if($exec)
		 $response['success']=true;
	   else
		 $response['message']='Unable to delete data';   
	 
	  break;
	  case 'reorder':
	   if(!$this->permission->has('edit')){break;}
	   $xid   = Request::get('xid');
	   $response['message']='Failed to execute';
	   if(is_array($xid))
	   {
		foreach($xid as $k=>$v)
        {
		 $exec  = $this->svc->reorder($k,$v);	
		}
        $response['success']=true;
	   }
	  break;
	  case 'pre_co_equiv':
	   if(!$this->permission->has('read')){break;}
	   $prog  = Request::get('progid');
	   $curr  = Request::get('currid');
	   $indx  = Request::get('indx');
	   $cdata = $this->svc->get_reqdetails($prog,$curr,$indx);
	   $equiv = $this->svc->get_equivsubj($indx); 
	   if($cdata)
	   {	   
	    $response['success'] = true;  
	    $response['content'] = (string)view($this->c_view.'.sub.table.prerequisite',array('detail'=>$cdata));
		$response['equiv']   = (string)view($this->c_view.'.sub.table.equivalent',array('subject'=>$equiv));
	   }
       else
        $response['message'] = 'Failed to execute process';
	
	  break;
	  case 'saveprereq':
	   if(!$this->permission->has('edit')){break;}
	   $indx    = Request::get('indx');
	   $csubj   = Request::get('subj');
	   $prereq  = Request::get('prereq');
	   $coreq   = Request::get('coreq');
	   $equiv   = Request::get('equiv');
	   $exec    = $this->svc->process_pcesubj('del',$indx,$csubj,0);
	   
	   if($prereq)
	   {
		 foreach($prereq as $p)
		 {
		   $exec = $this->svc->process_pcesubj('add',$indx,$csubj,$p,'Pre-Requisite');
		 }
	   }
	   
	   if($coreq)
	   {
		 foreach($coreq as $c)
		 {
		   $exec = $this->svc->process_pcesubj('add',$indx,$csubj,$c,'Co-Requisite');
		 }
	   }	   
	   
	   if($equiv)
	   {
		 foreach($equiv as $e)
		 {
		   $exec = $this->svc->process_pcesubj('add',$indx,$csubj,$e,'Equivalent');
		 }
	   }	   
	   
	   if($exec)
	    $response['success'] = true;
	   else
		$response['message'] = 'Failed to execute';  
	  break;
	  case 'equivdel':
	   if(!$this->permission->has('delete')){break;}
	   $indx  = Request::get('indx');
	   $csubj = Request::get('csubj');
	   $xsubj = Request::get('xsubj');
	   $exec  = $this->svc->process_equivsubj('del',$indx,$csubj,$xsubj);
	   if($exec)
	    $response['success'] = true;
	   else
		$response['message'] = 'Failed to execute';
	  case 'currlvl':
	   if(!$this->permission->has('read')){break;}
	   $curr  = Request::get('curr');
	   $opt   = Request::get('opt');
	   if($opt && $curr)
	   {	   
	    $exec  = $this->svc->process_currlvl($opt,$curr);
	    $response['success'] = true;
	    $response['content'] = (string)view($this->c_view.'.sub.table.level',array('yrlvl'=>$exec));
	   }
       else
        $response['message'] = 'Failed to execute';		   
	  break;
	  case 'savecurrlvl':
	   if(!$this->permission->has('edit')){break;}
	   $opt   = 'manual';
	   $curr  = Request::get('curr');
	   $yrlvl = Request::get('yrlvl');
	   $exec  = false;
	   if($opt && $curr && $yrlvl)
	   {	
        foreach($yrlvl as $k=>$v)
		{
	     $exec = $this->svc->process_currlvl($opt,$curr,$v['yrlvl'],$v['min'],$v['min']);
		}
		$response['success'] = $exec;
	    $response['content'] = 'Failed to execute';
	   }
       else
        $response['message'] = 'Failed to execute';		   
	  break;
	  default:
	   $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
      break;	  
	 }	 
    }
	else
	{
	 echo bcrypt('admin');
	 die();
	 /*	
	 if(Auth::attempt(['UserIDX'=>getUserID(),'password'=>getPassword()]))
	   echo 'true';
     else
       echo 'false';		
     
	 die();
     */	 
	} 
	
    return $response;	
  }  
  
}
?>