<?php

namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as tModel;
use App\Modules\Enrollment\Models\mAdvising;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Enrollment\Services\Registration\Advising as advise;
use App\Modules\Registrar\Services\EvaluationServices As evaluation;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Libraries\CrystalReport as xpdf;

use Permission;
use Request;
use Response;
use DB;

class Transcript extends Controller
{
    protected $ModuleName = 'transcript';

    private $media = [
            'Title' => 'Transcript of Record',
            'Description' => 'Welcome To Transcript of Record',
            'js' => ['registrar/transcript.js?v=1.3'],
            'css' => ['profile'],
            'init' => ['MOD.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'SmartNotification/SmartNotification.min'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'SmartNotification/SmartNotification',
            ],
        ];

    private $url = ['page' => 'registrar/transcript'];

    private $views = 'Registrar.Views.transcript.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();

        if ($this->permission->has('read')) {

            $grades = array();
            $stud = array();
            $balance= 0 ;
            $idno = decode(Request::get('idno'));

            if( $idno != ''){

                $stud = $this->model->where('StudentNo', $idno)->selectRaw("StudentNo, AppNo,  LastName, FirstName, ProgID, YearLevelID, dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel,  CurriculumID, dbo.fn_CurriculumCode(CurriculumID) AS CurriculumCode , dbo.fn_MajorName(MajorDiscID) As Major ")->first();

                $grades = $this->getPermanentRecord($idno);
                $balance = number_format( getOutstandingBalance('1',$idno, systemDate()), 2 , ".", ",") ;

            }

            $_incl = [
                 'views' => $this->views
                ,'idno' => $idno
                ,'term' => 0
                ,'data' => $stud
                ,'balance' => $balance
                ,'grades' => $grades
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode,$term, $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * ($page == 0 ? 0 : $page-1)));
        $rs = null;
        if($term != ''){

            $whereRaw = "TermID = {$term} ";
            $whereRaw .= " AND ProgID IN (".implode(getUserProgramAccess(),',') .")";

            if($filter == '0'){
                $whereRaw .= " AND dbo.fn_Promoted(RegID) = '0'";
            }elseif($filter == '1'){
                $whereRaw .= " AND dbo.fn_Promoted(RegID) = '1'";
            }

            $query = "SELECT *, dbo.fn_StudentName(StudentNo) As StudentName,
                            dbo.fn_StudentYearLevel_k12(StudentNo) As YearLevel,
                            dbo.fn_majorName(MajorID) As Major,
                            dbo.fn_Promoted(RegID) As Promoted,
                            IsNull((SELECT TOP 1 SA.Reason FROM ES_StudentAccountabilities SA WHERE SA.StudentNo = ES_Advising.StudentNo AND Cleared = 0), '') as Accountabilities,
                            IsNull((SELECT Top 1 TrackID FROM dbo.ES_Admission_Reservation WHERE IDNo = ES_Advising.StudentNo),0) As TrackID
                      FROM  ES_Advising WHERE " . $whereRaw . " Order By StudentName ASC
                      OFFSET     {$skip} ROWS
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('ES_Advising')
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }
        }

        return $rs;
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {

                case 'credit':
                    $idno = decode(Request::get('idno'));
                    $subj = (Request::get('subj'));
                    $credit = (Request::get('credit'));

                    if($credit == 'true'){
                        $data = $this->service->creditSubject($idno, $subj);
                        $response = ['error' => false, 'message' => 'record successfully', 'grade'=> 'CRD', 'remarks'=>'Credited' ];
                    }else{
                        $data = $this->service->remove_creditSubject($idno, $subj);
                        $response = ['error' => false, 'message' => 'record successfully', 'grade'=> '', 'remarks'=>'' ];
                    }

                break;

                case 'register':

                    if($this->permission->has('read')){

                        $model = new mStudents();


                        $idno = decode(Request::get('idno'));
                        $term = (Request::get('term'));

                        $campus=1;
                        $regid = 0;
                        $method = 0 ;
                        $appno = '';
                        $student = $model->getStudentInfo($idno, $term);

                        $major = $student->MajorDiscID;
                        $appno = '';
                        $amt = 0;
                        $useoldlvl = 1;
                        $yrlvl = $student->YearLevelID;
                        //$yrlvl = 15;

                        $section = (Request::get('section'));
                        $book = (Request::get('option'));

                        $option = '';
                        $status = '1';

                        if($book == '0'){
                            $option = " AND t.TemplateCode like '%TEXTBOOK%'";
                        }else{
                            $option = " AND t.TemplateCode like '%EBOOK%'";
                        }

                        if($student->IsEldest == '1'){
                            $option .= " AND t.TemplateCode like '%POF%'";
                        }

                        if( strtolower($student->Status) == 'old'){
                            $status = '2';
                        }


                        $fee_id = $this->enrolment->getFeesTemplate($term, $student->ProgID, $major, $student->YearLevelID, $student->IsForeign, $status , $option);;

                        $rs = DB::table("ES_Registrations")->selectRaw("RegID As Idx")->where("TermID",$term)->where('StudentNo',$idno)->get();

                        if(count($rs) > 0){
                            $reg = $rs[0]->Idx;

                            DB::statement("UPDATE ES_Registrations SET TableofFeeID = '{$fee_id}', ClassSectionID ='{$section}' WHERE RegID = '{$reg}'");

                        }else{
                            //$reg = DB::select("EXEC K12_RegisterStudent @StudentNo='{$idno}' , @TermID='".$term."',@GradeLevelID='".$yrlvl."',@StrandID='".$major."',@FeesID='".$fees_id."',@PayMethodID='".$method."',@Amount='".$amt."',@AppNo='".$appno."',@UserID='".getUserID()."',@UseOldLvl=1");

                            $data = array(
                                'StudentNo' => $idno,
                                'TermID' =>  $term,
                                'CampusID' => '1',
                                'ClassSectionID' => $section ,
                                'RegDate' => systemDate(),
                                'CollegeID' => $student->CollegeID,
                                'ProgID' => $student->ProgID,
                                'MajorID' => $student->MajorDiscID,
                                'YearLevelID' => $student->YearLevelID,
                                'IsOnlineEnrolment' => 1,
                                'IsRegularStudent' => 1,
                                'TableofFeeID' => $fee_id

                            );

                            $reg = DB::table('ES_Registrations')->insertGetId($data);


                        }

                        $subj = (Request::get('subj'));
                        $unit = 0;

                        /* Clear Subjects */
                        DB::statement("DELETE ES_RegistrationDetails WHERE RegID = '{$reg}'");

                        foreach($subj as $r){
                            $unit = $unit + $this->getUnit(($r));
                            DB::statement("INSERT INTO dbo.ES_RegistrationDetails ( RegID , ScheduleID , RegTagID ) VALUES  ( {$reg} , ".($r)." , 0  )");
                        }

                        //err_log($unit);

                        $dues = DB::table("ES_TableofFee_Details AS d")
                                            ->leftJoin("dbo.ES_Accounts AS a",'d.AccountID','=','a.AcctID')
                                            ->selectRaw("d.*, a.AcctOption")
                                            ->where('TemplateID',$fee_id)
                                            ->get();

                        /* Clear Journals */
                        DB::statement("DELETE ES_Journals WHERE TransID = 1 AND TermID = {$term} AND IDType = 1 AND idno = '{$idno}' AND ReferenceNo = '{$reg}'");

                        foreach($dues as $d){
                            if($d->AcctOption != '0'){
                                $amt = round( $d->Amount * $unit );
                            }else{
                                $amt = $d->Amount;
                            }
                            $acct=$d->AccountID;

                            DB::statement("INSERT INTO dbo.ES_Journals ( ServerDate , TransDate , TermID , CampusID , TransID , ReferenceNo , AccountID ,
                            		          IDType ,
                            		          IDNo ,
                                              Debit ,
                                              [Assess Fee] ,
                                              [1st Payment]
                                              ) VALUES  ( Getdate(), Getdate(),{$term},1,1,'{$reg}',{$acct},1,'". $idno ."', ".($amt)." , {$amt},{$amt}  )");
                        }

                        $whre = array(
                            'StudentNo' => $idno,
                            'TermID' => $term
                        );

                        $data = array(
                            'TermID'  => $term,
                            'CampusID' => 1 ,
                            'CollegeID' => 6 ,
                            'ProgID' => 29 ,
                            'StudentNo' => $idno,
                            'YearLevel' => 5,
                            'MinLoad' => 3,
                            'MaxLoad' => 6,
                            'AdviserID' => getUserID() ,
                            'DateAdvised' => systemDate() ,
                            'FeesID' => $fee_id ,
                            'RegID' =>  $reg,
                            'IsOnlineAdvising'=> 1,
                            'MajorID' => $major,
                        );


                        $this->model->updateOrCreate($whre,$data);

                        DB::statement("UPDATE ES_Registrations SET TotalLecUnits = '{$unit}' WHERE RegID = '{$reg}'");

                        $response = ['error' => false, 'message' => 'record successfully', 'content'=> $reg, 'reg'=> encode($reg)];

                    }


                break;

                case 'get-students':

                    if($this->permission->has('read')){
                        $model = new mStudents();

                        $find = Request::get('find');

                        if( $find == ''){ $find = date('Y').'%'; }
                        $data = $model->studentsSearch($find,'50');

                        if(!empty($data)){
                            $vw  = view($this->views.'students', ['data' => $data])->render();
                            $response = ['error' => false, 'data' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }
                    }

                break;


                 case 'remove':

                    $list = Request::get('idno');

                    foreach($list as $r){
                        $ret = $this->model->where(['AdvisedID' => decode($r['ref']) ])->delete();
                    }

                    $response = successDelete();

                break;
                 case 'schedule':
			        $termid   = (Request::get('termid'));
					$progid   = Request::get('progid');
					$subjid   = (Request::get('subjid'));
					$scheds   = DB::select("SELECT cs.ScheduleID
							     ,s.SectionID ,s.SectionName
							     ,(CASE WHEN cs.Limit=0 THEN s.Limit ELSE cs.Limit END) as Limit
							     ,(SELECT COUNT(*) FROM ES_RegistrationDetails WHERE ScheduleID=cs.ScheduleID) as Registered
							     ,(SELECT COUNT(*) FROM ES_Advising_Details WHERE ScheduleID=cs.ScheduleID) as Advised
							     ,Time1_Start
							     ,Time1_End
							     ,Days1
							     ,Sched_1 as Sched1
							     ,Time2_Start
							     ,Time2_End
							     ,Days2
							     ,Sched_2 as Sched2
							     ,Time3_Start
							     ,Time3_End
							     ,Days3
							     ,Sched_3 as Sched3
							     ,Time4_Start
							     ,Time4_End
							     ,Days4
							     ,Sched_4 as Sched4
                                 ,Room1_ID
                                 ,dbo.fn_RoomName(Room1_ID) AS Room1
							FROM ES_ClassSchedules as cs
					  INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
						   WHERE cs.SubjectID='".$subjid."' AND cs.TermID=".$termid." AND s.ProgramID=".$progid);

                    $vw  = view($this->views.'offered', ['data' => $scheds])->render();

				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'content' => $vw
					                           ,'error'   => false
											   ,'message' => ''
											   ]);
                    break;
                case 'schedule-by-section':

                    $termid   = (Request::get('termid'));
					$progid   = Request::get('progid');
					$section   = (Request::get('section'));
                    $scheds   = DB::select("SELECT cs.ScheduleID
							     ,s.SectionID ,s.SectionName, cs.SubjectID
							     ,(CASE WHEN cs.Limit=0 THEN s.Limit ELSE cs.Limit END) as Limit
							     ,(SELECT COUNT(*) FROM ES_RegistrationDetails WHERE ScheduleID=cs.ScheduleID) as Registered
							     ,(SELECT COUNT(*) FROM ES_Advising_Details WHERE ScheduleID=cs.ScheduleID) as Advised
							     ,Time1_Start
							     ,Time1_End
							     ,Days1
							     ,Sched_1 as Sched1
							     ,Time2_Start
							     ,Time2_End
							     ,Days2
							     ,Sched_2 as Sched2
							     ,Time3_Start
							     ,Time3_End
							     ,Days3
							     ,Sched_3 as Sched3
							     ,Time4_Start
							     ,Time4_End
							     ,Days4
							     ,Sched_4 as Sched4
                                 ,Room1_ID
                                 ,dbo.fn_RoomName(Room1_ID) AS Room1
							FROM ES_ClassSchedules as cs
					  INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID
						   WHERE s.SectionID ='".$section."' AND cs.TermID=".$termid." AND s.ProgramID=".$progid);


				    $response = Response::json([
												'success' => (($scheds)?true:false)
					                           ,'list' => $scheds
					                           ,'error'   => false
											   ,'message' => ''
											   ]);

                break;
                case 'get-grades':

                    $idno   = decode(Request::get('idno'));

               	    $grades = DB::table('ES_CurriculumDetails as cd')
                        ->leftJoin("ES_Students as x",'x.curriculumid','=','cd.curriculumid')
                        ->leftJoin('ES_Subjects as s','s.SubjectID','=','cd.SubjectID' )
                        ->selectRaw("cd.IndexID, cd.CurriculumID, cd.SubjectID, s.SubjectCode, s.SubjectTitle, s.LectHrs As Credit, s.IsPassFail , YearTermID, dbo.fn_YearTerm(YearTermID) AS YearTerm ")
                        ->where('x.studentno', $idno )
                        ->whereRaw("YearTermID <> 22")
                        ->orderBy('cd.YearTermID','asc')
                        ->orderBy('cd.SortOrder','asc')
                        ->get();


                    $vw  = view($this->views.'evaluation.list', ['data' => $grades, 'idno'=> $idno ])->render();

				    $response = Response::json(['success' => true
					                           ,'data' => $vw
					                           ,'error'   => false
											   ,'message' => ''
                    ]);

                break;

                case 'check-conflict':
                    $test_scheds = Request::get('test_scheds');
                    $sched_id = Request::get('sched');
                    $exclude_id = Request::get('exclude_sched');

                    if(!empty($test_scheds)){
                        foreach ($test_scheds as $ts) {
                            if($exclude_id != $ts){
                                $chk = DB::select('exec dbo.sp_CheckSchedConflicts ?, ?', [$sched_id, $ts]);

                                if(!empty($chk[0]->Conflict)){
                                    return ['error' => true, 'message' => 'Conflict subject. Please select another.', 'sched' => $ts];
                                }
                            }
                        }
                    }

                    $response = ['error' => false];

                    break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }



    private function getPermanentRecord($idno){
         $qry = "SELECT  (SELECT COUNT(X.GradeIDX) FROM dbo.ES_PermanentRecord_Details X WHERE X.TermID = PD.TermID AND X.StudentNo = PD.StudentNo)  [TotalEnrolledSubjects],
                            PM.TotalSubjects, PD.TermID, dbo.fn_AcademicYear(PD.TermID) AS [YearTerm], PD.StudentNo, PD.RegID, PD.ScheduleID, CS.SectionID,
                     dbo.fn_ClassScheduleSectionName(PD.ScheduleID) [SectionName], PD.ParentSubjectID [ParentID], PD.SubjectID, PD.SubjectCode, PD.SubjectTitle,
                     PD.AverageA, PD.AverageB, PD.AverageC, PD.AverageD, PD.Final_Average, PD.Final_Remarks, PD.Conduct_FinalAverage,
                     ( CASE WHEN PD.IsNonAcademic = 1 THEN 0 - PD.SubjectCreditUnits ELSE PD.SubjectCreditUnits END ) AS [CreditUnits],
                     ( CASE WHEN PD.IsNonAcademic = 1 THEN 0 - PD.SubjectCreditUnits ELSE PD.SubjectCreditUnits END ) AS [CreditedUnits],
                     ( CASE WHEN PM.TotalUnitsEnrolled = PM.TotalUnitsEarned THEN '0' ELSE '1' END ) AS [Lacking],
                    PD.GradeIDX, PD.LastModifiedDate, PD.DatePosted, dbo.fn_K12_YearLevel3(PD.YearLevelID,PD.ProgramID) [YearLevel],
                     PD.IsNonAcademic, PM.Final_Average [GeneralAverage], PM.Final_Conduct [ConductAverage], PD.YearLevelID, PD.SeqNo [SortOrder]
             FROM    dbo.ES_PermanentRecord_Details PD
                     LEFT OUTER JOIN dbo.ES_PermanentRecord_Master PM ON PD.StudentNo = PM.StudentNo AND PD.TermID = PM.TermID
                    LEFT OUTER JOIN dbo.ES_Registrations R ON PD.StudentNo = R.StudentNo AND PD.TermID = R.TermID
                     LEFT OUTER JOIN dbo.ES_ClassSchedules CS ON PD.ScheduleID = CS.ScheduleID
                     LEFT JOIN ES_Subjects S ON S.SubjectID=PD.SubjectID
             WHERE   PD.StudentNo = '" . $idno . "' ORDER BY [YearTerm], [SortOrder] ";

        return DB::select($qry);

    }


    private function getUnit($subj){

        $this->SectionID = 0;
        $rs = DB::table("ES_Subjects as s")
                ->leftJoin('es_classSchedules as cs ','cs.subjectid','=','s.subjectid')
                ->selectRaw("s.LectHrs, cs.SectionID")
                ->where('ScheduleID',$subj)
                ->get();

        if(count($rs) > 0){
            return $rs[0]->LectHrs;
            $this->SectionID = $rs[0]->SectionID;
        }else{
            return 0;
        }

    }

    public function print_report()
    {

        $idno  = decode(Request::get('idno'));
        $this->xpdf->filename='SHS\crediting_form.rpt';

        $this->xpdf->query="EXEC sp_k12_credit_form '{$idno}',0";
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="Credit Form"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
	}

    private function initializer()
    {
        $this->model = new mStudents();
        $this->permission = new Permission($this->ModuleName);
        $this->advise     = new advise;
        $this->service = new evaluation;
        $this->enrolment = new Services;
        $this->xpdf = new xpdf;
    }
}
