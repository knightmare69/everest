<?php

namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Services\RegistrarServiceProvider as Services;
use App\Modules\Registrar\Models\StudentRequests_model as Model;
use App\Libraries\CrystalReport as xpdf;

use Permission;
use Request;
use Response;

class StudentRequests extends Controller
{
    protected $ModuleName = 'student-requests';

    private $media =
        [
            'Title' => 'Student Requests',
            'Description' => 'Welcome To Student Requests!',
            'js' => ['registrar/stud-req'],
            'css' => ['profile'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'registrar/student-requests/'];

    private $views = 'Registrar.Views.StudentRequests.';

    public function __construct()
    {
        $this->initializer();
    }

    public function requestReportIndex()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $this->views =  $this->views.'ReportCard.';

            $_incl = [
                'views' => $this->views,
                'academic_year' => $this->services->AcademicTerm(),
                'periods' => $this->services->Period(),
            ];

            return view('layout', array('content' => view($this->views.'.index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }


    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'get-students':
                    if($this->permission->has('read')){
                        $term = decode(Request::get('academic-year'));
                        $type = Request::get('type');
                        $period = decode(Request::get('period'));

                        $data = $this->model->getStudents($term, $period, $type);

                        if($type == 1){
                            if(!empty($data)){
                                $vw = view($this->views.'ReportCard.results', ['data' => $data])->render();
                                $response = ['error' => false, 'rows' => $vw];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                        } else {
                            $response = ['error' => true, 'message' => 'Request type does not exists.'];
                        }

                    }
                    break;

                case 'issue-report-card':
                    if($this->permission->has('add')){
                        $req_id = decode(Request::get('request'));
                        $snum = Request::get('snum');
                        $term = decode(Request::get('academic-year'));

                        if(!empty($req_id)){
                            $update = $this->model->find($req_id)->update(['IssuedDate' => date('Y-m-d H:i:s'), 'IssuedBy' => getUserID()]);

                            if($update){
                                $details = $this->model->getReportDetails($snum, $term);

                                $response = [
                                    'error' => false,
                                    'date' => date('Y-m-d'),
                                    'name' => getUserFullName(),
                                    'yl' => encode($details->YearLevelID),
                                    'prog' => encode($details->ProgID),
                                    'sec' => encode($details->ClassSectionID),
                                ];
                            } else {
                                $response = ['error' => true, 'message' => 'Unable to update request'];
                            }
                        } else {
                            $response = ['error' => true, 'message' => 'Missing argument.'];
                        }
                    }
                    break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new Model();
        $this->permission = new Permission($this->ModuleName);
    }
}
