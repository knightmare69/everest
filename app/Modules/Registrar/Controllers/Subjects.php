<?php namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\Subjects\Subjects as Model;
use Permission;
use Request;
use DB;

class Subjects extends Controller{
	private $media =
		[
			'Title'=> 'Subject',
			'Description'=> 'Welcome To Dashboard!',
			'js'		=> ['Registrar/subjects/index','Registrar/subjects/helper'],
			'init'		=> ['Me.init()'],
			'plugin_js'	=> ['bootbox/bootbox.min']
		];

	private $url = [ 'page' => 'registrar/subjects/' ];

	private $views = 'Registrar.Views.subjects.';

	public function __construct()
	{
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'showModal':
					if ($this->permission->has('add')) {
						$response = ['error' => false,'content'=>view($this->views.'forms.subject')->render()];
					}
				break;
				case 'showFilter':
					if ($this->permission->has('add')) {
						$response = ['error' => false,'content'=>view($this->views.'forms.filter')->render()];
					}
				break;
				case 'search':
					if ($this->permission->has('search')) {
						$response = ['error' => false,'content'=>view($this->views.'tables.masterlist')->render()];
					}
				break;
				case 'delete':
					if ($this->permission->has('delete')) {
						if ($this->isUsed(decode(Request::get('subjectKey')))) {
						 	$status = $this->model->where('SubjectID',decode(Request::get('subjectKey')))->delete();
							$response = $status ? successDelete() : errorDelete();
						} else {
							$response = ['error' => true,'message' => 'This Subject is already in used. It cannot be deleted.'];
						}
					}
				break;
				case 'save':
					if ($this->permission->has('add')) {
						if (Request::get('isEdit') == 'false') {
							$status = $this->model->insert(assertCreated($this->getFields()));
							$response = $status ? successSave() : errorSave();
						} else {
							if (!$this->isLock()) {
								$status = $this->model->where('SubjectID',decode(Request::get('subjectKey')))->update(assertModified($this->getFields()));
								$response = $status ? successUpdate() : errorUpdate();
							} else {
								$response = ['error' => true,'message' => "There's nothing to update. This record is currently locked."];
							}
							
						}
					}
				break;
			}
		}
		return $response;
	}

	private function isLock() 
	{
		if($this->model->where('SubjectID',decode(Request::get('subjectKey')))->where('LockedBy','<>','')->count() > 0) {
			return true;
		}
		return false;
	}

	private function isUsed($key)
	{	
		$count = DB::select("select dbo.fn_IsSubjectLinked('".$key."') as total");
		$count = isset($count[0]) ? $count[0]->total > 0 ? true : false : false;

		if ($count > 0) {
			return true;
		}
		return false;
	}

	private function getFields()
	{
		$data = [
			'SubjectCode' => Request::get('SubjectCode'),
			'SubjectTitle' => Request::get('SubjectTitle'),
			'SubjectDesc' => Request::get('SubjectDesc'),
			'SubjectGE' => Request::get('SubjectGE') ? 1 : 0,
			'SubjectElective' => Request::get('SubjectElective')? 1 : 0,
			'SubjectMajor' => Request::get('SubjectMajor') ? 1 : 0,
			'SubjectComputer' => Request::get('SubjectComputer')? 1 : 0,
			'InclGWA' => Request::get('InclGWA') ? 1 : 0,
			'AcadUnits' => Request::get('AcadUnits'),
			'CreditUnits' => Request::get('CreditUnits'),
			'LectHrs' => Request::get('LectHrs'),
			'LabUnits' => Request::get('LabUnits'),
			'LabHrs' => Request::get('LabHrs'),
			'SubjectAreaID' => Request::get('SubjectAreaID'),
			'LevelID' => Request::get('LevelID'),
			'SubjectMode' => Request::get('SubjectMode'),
			'MinSize' => Request::get('MinSize'),
			'MaxSize' => Request::get('MaxSize'),
			'IsNonAcademic' => Request::get('IsNonAcademic') ? 1 : 0,
			'Alias1' => Request::get('Alias1'),
			'Alias2' => Request::get('Alias2'),
			'OtherSchool' => Request::get('OtherSchool') ? 1 : 0,
			'SubjectELearning' => Request::get('SubjectELearning') ? 1 : 0,
			'IsTransmutedGrade' => Request::get('IsTransmutedGrade') ? 1 : 0,
			'SubjectWithInternet' => Request::get('SubjectWithInternet') ? 1 : 0,
			'IsClubOrganization' => Request::get('IsClubOrganization') ? 1 : 0,
			'SubjParentID' => Request::get('SubjParentID'),
			// 'SubjCompPercentage' => Request::get('SubjCompPercentage'),
			'IsPracticum' => Request::get('IsPracticum') ? 1 : 0,
			'SubjGroupID' => Request::get('SubjGroupID'),
			'ThesisSubject' => Request::get('ThesisSubject') ? 1 : 0,
			'ChineseSubject' => Request::get('ChineseSubject') ? 1 : 0,
			'Weight' => Request::get('Weight'),
			'LetterGradeOnly' => Request::get('LetterGradeOnly') ? 1 : 0,
			'IsChinese' => Request::get('ChineseSubject') ? 1 : 0,
			'Inactive' => Request::get('Inactive') ? 1 : 0,
			//'LockedBy' => Request::get('LockSubject') ? getUserID()  : '',
			//'DateLocked' => Request::get('LockSubject') ? systemDate()  : '',
			'IsSemestral' => Request::get('Semestral') ? 1 : 0,

		];
		
		return $data;
	}

 	private function initializer()
 	{
 		$this->permission = new Permission('registrar-subjects');
 		$this->model = new Model;
 	}
}
/*

select * from modules

insert into modules(name,url,slug,sort)
values('Registrar','#','registrar','2')

select * from pages

insert into pages(name,slug,url,module_id,sort)
values('Subjects','registrar-subject','/registrar/subject',1004,1)

*/