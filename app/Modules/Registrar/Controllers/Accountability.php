<?php

namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
// use App\Modules\Setup\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Registrar\Models\Accountability_model as Model;
use App\Modules\Registrar\Models\TermConfig as term_model;
use App\Modules\Students\Models\StudentProfile As mStudents;

use Permission;
use Request;
use Response;

class Accountability extends Controller
{
    protected $ModuleName = 'student-accountability';

    private $media =
        [
            'Title' => 'Student Accountability',
            'Description' => 'Welcome To Student Accountability',
            'js' => ['registrar/accountability'],
            'init' => ['MOD.load()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'SmartNotification/SmartNotification.min'

                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'bootstrap-datepicker/css/datepicker',
                'SmartNotification/SmartNotification',
            ],
        ];

    private $url = ['page' => 'registrar/accountability'];

    private $views = 'Registrar.Views.Accountability.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        $mterm = new term_model;
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));

            $_incl = [
                'views' => $this->views,
                'at' => $mterm->AcademicTerm(),
                'term' => $term,
            ];

            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {

               case 'save':

                    $term = decode(Request::get('term'));

                    $p = Request::all();

                    $data = [
                        'TermID'      => decode(getObjectValue($p,'term')),
                        'DateEntered' => systemDate(),
                        'StudentNo'   => decode(getObjectValue($p,'idno')),
                        'Reason'      => getObjectValue($p,'reason'),
                        'Cleared'     => getObjectValue($p,'cleared'),
                        'EnteredBy'   => getUserID()
                    ];

                    $result = $this->model->create($data);

                	if ($result)
                    {
                       $vw  = view($this->views.'list', ['term' => decode(getObjectValue($p,'term'))])->render();
                       $response = [ 'error'=>false, 'message'=> "Record successfully saved!", 'data'=> $vw];
					}
                    else
                    {
						$response = errorSaveAdmission();
					}

                break;
			    case 'remove':

                    $list = Request::get('idno');

                    foreach($list as $r){
                        $ret = $this->model->where(['IndexID' => decode($r) ])->delete();
                    }

                    $response = successDelete();

                break;


                case 'get-students':
                    if($this->permission->has('read')){
                        $model = new mStudents();

                        $find = Request::get('find');

                        if( $find == ''){
                            $find = date('Y').'%';
                        }

                        // if(!empty($find)){
                            //$term_id = decode(Request::get('academic-term'));
                            //$prog_id = decode(Request::get('programs'));
                            //$yl_id = decode(Request::get('year-level'));

                            $data = $model->studentsSearch($find,'50');

                            if(!empty($data)){
                                $vw  = view($this->views.'students', ['data' => $data])->render();
                                $response = ['error' => false, 'data' => $vw];

                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                    }
                    break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->model = new Model();
        $this->permission = new Permission($this->ModuleName);
    }
}
