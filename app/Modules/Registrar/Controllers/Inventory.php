<?php
namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as term_model;
use App\Modules\ClassRecords\Models\Schedules_model as Rep_Model;
use App\Modules\Registrar\Services\RegistrarServiceProvider as services;
use Permission;
use Request;
use Response;

class Inventory extends Controller
{
    protected $ModuleName = 'inventory-class-records';

    private $media = [
            'Title' => 'Inventory of Class Records',
            'Description' => 'Welcome To Inventory of Class Records',
            'js' => ['registrar/inventory'],
            'init' => [],
            'plugin_js' => ['bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min','SmartNotification/SmartNotification.min',
                            'bootstrap-datepicker/js/bootstrap-datepicker',],
            'plugin_css' => [ 'datatables/plugins/bootstrap/dataTables.bootstrap', 'select2/select2', 'bootstrap-datepicker/css/datepicker','SmartNotification/SmartNotification'],
        ];

    private $url = ['page' => 'registrar/grading-sheet-inventory'];

    private $views = 'Registrar.Views.Inventory.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) { return redirect('/security/validation'); }        
        
        $this->initializer();
        
        #TEMP :  Disabled privileges
        //if (!$this->permission->has('read')) { return view(config('app.403')); die(); }
        $term = decode(Request::get('t'));                                                                                                                    
        $data = $this->model->View($term,'')->get();
                    
        $_incl = [
            'views' => $this->views,
            'at' => $this->mTerm->AcademicTerm(),
            'progs' => $this->mTerm->Programs(),
            'data' => $data,                              
        ];

        return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
                
    }
    
    public function view()
    {
        if (isParent()) { return redirect('/security/validation'); }        
        
        $this->initializer();
        
        #TEMP :  Disabled privileges
        //if (!$this->permission->has('read')) { return view(config('app.403')); die(); }
        $term = decode(Request::get('t'));
        
        $rsTerm = $this->mTerm->getTerm()->where('TermID', $term )->first();
                
        $per = ($rsTerm->SchoolTerm == 'School Year' ? '1' : '21' );    
         
        
        $sched = decode(Request::get('s'));                                                                                                                    
        $data = $this->model->StudentsWithGrade($sched);
                    
        $_incl = [
            'views' => $this->views,
            'at' => $this->mTerm->AcademicTerm(),
            'period' => $this->mTerm->getPeriod($per),
            'data' => $data,    
            'sched'=>  $sched,                                    
        ];

        return view('layout', array('content' => view($this->views.'schedules.index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
                
    }
    
    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'get-sections':
                                           
                    $term = decode(Request::get('term'));                        
                    $where = array( 'TermID' => $term );                                                                        
                    $data = $this->model->View($term,'')->get();
                    
                    if(!empty($data)){            
                        $vw  = view($this->views.'sub.schedules', ['data' => $data])->render();
                        $response = ['error' => false, 'message' => 'record successfully loaded', 'data' => $vw ];
                    } else {
                        $response = ['error' => true, 'message' => 'Not yet configured','first'=>false,'second'=>false];
                    }                   
                break;
                case 'get-schedules':
                                           
                    $sched = decode(Request::get('sched'));
                    $per = decode(Request::get('period'));                                                                                                                    
                    $data = $this->model->StudentsWithGrade($sched);
                    
                    if(!empty($data)){            
                        $vw  = view($this->views.'schedules.students', ['data' => $data , 'per' => $per ])->render();
                        $response = ['error' => false, 'message' => 'record successfully loaded', 'data' => $vw ];
                    } else {
                        $response = ['error' => true, 'message' => 'Not yet configured','first'=>false,'second'=>false];
                    }                   
                break;
                case 'unpost': 
                                                            
                    if( $this->services->UnpostGrade() )
                        $response = ['error' => false, 'message' => 'record successfully unposted'];
                    else
                        $response = ['error' => true, 'message' => 'unposting of grade failed.'];
                    
                break;
                default: return response('Unauthorized.', 401); break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        // $this->services = new Services();
        $this->model = new Rep_Model();
        $this->mTerm = new term_model();
        $this->permission = new Permission($this->ModuleName);
        $this->services = new services();
                
    }
}
