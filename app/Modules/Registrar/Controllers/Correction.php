<?php

namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\Correction as Rep_Model;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As grade_model;
use App\Modules\Registrar\Services\EvaluationServices As evaluation;
use App\Libraries\CrystalReport as xpdf;

use Permission;
use Request;
use Response;

class Correction extends Controller
{
    protected $ModuleName = 'correction-of-grades';

    private $media = [
            'Title' => 'Correction of Grades',
            'Description' => 'Welcome To Correction of Grades!',
            'js' => ['registrar/correction'],
            'css' => ['profile'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'registrar/correction-of-grades/'];

    private $views = 'Registrar.Views.Correction.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {

            $_incl = [
                'views' => $this->views,
                'academic_year' => $this->model->AcademicTerm(),                                
            ];

            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
                case 'save-changes': 
                    if($this->permission->has('edit')){
                                            
                        $term_id = decode(Request::get('term'));
                        $gradeid = decode($post['subj']);
                        $where = [
                            'GradeIDX' => $gradeid,
                        ];  
                        
                        $data = [
                            'GradeIDX' => $gradeid,
                            'PeriodID' => $post['period'],
                            'OldGrade' => $post['old'],
                            'NewGrade' => $post['grade'],
                            'Reason' => $post['reason'],
                            'DateCorrected' => systemDate(),
                            'UserID' => getUserID(),                                
                        ];
                                    
                        $this->model->create($data);
                                                
						$data = [
                            'CorrectionDate' => systemDate(),
                            'CorrectionBy' => getUserID(),                                                        
                        ];
                        
                        switch($post['period']){
                            
                            case '1': 
                                $data['AverageA'] = $post['grade'];
								$data['GPA1']     = $post['gpa'];
                                $this->service->computeSubjectAve($gradeid); 
                            break;
                            
                            case '11':
                                $data['AverageA'] = $post['grade'];
								$data['GPA2']     = $post['gpa'];                                                                
                            break;
                            
                            case '2':                             
                                $data['AverageB'] = $post['grade'];
                                $this->service->computeSubjectAve($gradeid);
                            break;
                            
                            case '12': 
                                $data['AverageB'] = $post['grade'];
                            break;                            
                            
                            case '3': 
                                $data['AverageC'] = $post['grade'];
								$data['GPA3']     = $post['gpa']; 
                                $this->service->computeSubjectAve($gradeid);
                            break; 
                            
                            case '4': 
                                $data['AverageD'] = $post['grade'];
								$data['GPA4']     = $post['gpa'];
                                $this->service->computeSubjectAve($gradeid);
                            break;
                            
                            case '13': 
                                
                                $rs = $this->grade->where($where)->first();
                                
                                $data['AverageC'] = $post['grade']; 
								$data['GPA3']     = $post['gpa'];
                                $data['Final_Average'] = (floatval($post['grade']) +  floatval($rs->TotalAverageAB))/2; 
                                
                            break;
                        }
                        
						$this->grade->where($where)->update($data);                            
						$response = ['error'=>false,'message'=>'Successfully Save!','refresh'=> 1];
						SystemLog('Correction of Grades','','Grade Correction','save changes','Student : ' . $post['stud'] .' GradeIDX='. $post['subj']  ,'' );
                    }
                    
                
               break;
               
               case 'get-history':
               
                    if($this->permission->has('read')){                        
                        $subj = decode(Request::get('subj'));                                                
                        $vw  = view($this->views.'sub.history', ['subj' => $subj])->render();
                        $response = ['error' => false, 'html' => $vw];                        
                    }
               
               break;
                    
                case 'get-students':
                    if($this->permission->has('read')){
                                            
                        $term_id = decode(Request::get('term'));
                        $data = $this->model->getStudent($term_id);

                        if(!empty($data)){
                            $vw  = view($this->views.'sub.students', ['data' => $data])->render();
                            $response = ['error' => false, 'html' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }

                    }
                    break;
                case 'grades' :
                
                    $filters = [
                         'TermID' => decode($post['term']),
                         'StudentNo' => $post['stud'],                         
                    ];
                    $data = $this->grade->gradeView(decode($post['term']),$post['stud'])->get();                    
                    
                    if (!empty($data)) {
                        $view = view($this->views.'table', ['term' => decode($post['term']),'shs' => $post['shs'] , 'stud' => $post['stud'], 'grades' => $data])->render();
                        $history = view($this->views.'sub.summary_of_history', ['term' => decode($post['term']),'shs' => $post['shs'] , 'student' => $post['stud'] ])->render();
                        $response = ['error' => false, 'list' => $view,  'history'=>$history ];
                    } else {
                        $response = ['error' => true, 'list' => 'No record(s) found.', 'history'=>''];
                    }
                     
                break;
                
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }
    


    private function initializer()
    {
        // $this->services = new Services();
        $this->model = new Rep_Model();
        $this->permission = new Permission($this->ModuleName);
        $this->grade = new grade_model();
        $this->service = new evaluation();
        $this->xpdf = new xpdf;
    }
}
