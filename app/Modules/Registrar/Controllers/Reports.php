<?php

namespace App\Modules\Registrar\Controllers;

use App\Http\Controllers\Controller;
use DB;
// use App\Modules\Setup\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Registrar\Models\Reports as Rep_Model;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF as lpdf;
use mikehaertl\wkhtmlto\Pdf;
use Spipu\Html2Pdf\Html2Pdf;

use Permission;
use Request;
use Response;

class Reports extends Controller
{
    protected $ModuleName = 'report-card';

    private $media = [
            'Title' => 'Report Card',
            'Description' => 'Welcome To Report Card!',
            'js' => ['registrar/report-settings'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'registrar/report-card/'];

    private $views = 'Registrar.Views.Reports.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {

            $_incl = [
                'views' => $this->views,
                'at' => $this->model->AcademicTerm(),
                'progs' => $this->model->Programs(),
                'rep_types' => [
                    'Report Card',
                    'Consolidated Subject Grade',
                    'Student Directory',
                    'Official List of Enrollment',
                    'List of Withdrawal Students',
                    'List of Students by Parents',
                    'List of Students by Nationalities',
                    'List of New Students',
                    'Enrollment Statistics by Gender',
                    'Enrollment Statisctics by Age',
                    'Enrollment Profile',
                    'Class List',
                    'Summary of Enrollment',
                    'Unofficially Enrolled Students',
                    'Unposted Grades',
                    'Academic Ranking',
                    'Enrollment Statisctics by Section',
                    //'Report Card - back to back',
                    //'Academic Failures with Remedial Grade',
                    'Grade Correction History'
                ]
            ];

            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function reports()
    {
        if(getUserID() == 'admin'){
            $this->initializer();
            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }
        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
                case 'save-profile':
                    $term = decode(Request::get('term'));
                    $p = Request::get('data');
                    
                    foreach($p as $r){
                        $data = $this->model->setProjections($term,$r);    
                    }
                                        
                    $response = ['error' => false, 'message' => 'record successfully saved!'];
                
                break;
                
                case 'profile':
                
                    $term = decode(Request::get('term'));
                    $prog = decode(Request::get('prog'));
                    
                    $data = $this->model->getProjections($term,$prog);
                    $vw = view($this->views.'sub.list', ['data' => $data])->render();
                    $response = ['error' => false, 'html' => $vw];
                            
                     
                break;
                
                case 'get-year-level':
                    if($this->permission->has('read')){
                        $prog_class = decode(Request::get('pclass'));

                        $data = $this->model->getYearLevel($prog_class);

                        if(!empty($data)){
                            $vw = view($this->views.'sub.year-level', ['yl' => $data])->render();
                            $response = ['error' => false, 'html' => $vw];
                        } else {
                            $response = ['error' => true, 'message' => 'No year level found.'];
                        }
                    }
                    break;
                case 'get-sections':
                    if($this->permission->has('read')){
                        $yl_id = decode(Request::get('yl'));
                        $term_id = decode(Request::get('t'));
                        $prog_id = decode(Request::get('p'));

                        $data = $this->model->getSections($term_id, $prog_id, $yl_id);

                        if(!empty($data)){
                            $vw = view($this->views.'sub.section', ['sec' => $data])->render();
                            $response = ['error' => false, 'html' => $vw];
                        } else {
                            $response = ['error' => true, 'message' => 'No section(s) found.'];
                        }
                    }
                    break;
                case 'get-students':
                    if($this->permission->has('read')){
                        $find = Request::get('student-name');

                        $term_id = decode(Request::get('academic-term'));
                        $prog_id = decode(Request::get('programs'));
                        $yl_id = decode(Request::get('year-level'));
                        $section_id = decode(Request::get('section'));
                        $data = $this->model->getStudent($find, $term_id, $prog_id, $yl_id, $section_id);

                        if(!empty($data)){
                            $vw  = view($this->views.'sub.students', ['data' => $data])->render();
                            $response = ['error' => false, 'html' => $vw];

                        } else {
                            $response = ['error' => true, 'message' => 'No result(s) found.'];
                        }
                    }
                    break;
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    public function print_report()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];

        if($this->permission->has('print')){

            $rep_type = Request::get('report-type');

            // Common Params
            $term_id = decode(Request::get('academic-term'));
            $yl_id = decode(Request::get('year-level'));
            $section_id = decode(Request::get('section'));
            $prog_id = decode(Request::get('programs'));
            $section_id = empty($section_id) ? 0 : $section_id;


            if($rep_type == 1){
                $stud_num = Request::get('snum');
                $period_id = decode(Request::get('period'));
                $replyslip = empty(Request::get('reply-slip')) ? 0 : 1;
                $excludegwa = empty(Request::get('excludegwa')) ? 0 : 1;
                $stud_num = empty($stud_num) ? 0 : $stud_num;

                $exclude_subj = empty(Request::get('print-empty-subj')) ? 0 : 1;
                $dl_fname = 'ReportCard';

                if($section_id == 0 && $stud_num != 0) {
                    $section_id = $this->model->getStudentSectionID($stud_num, $term_id, $prog_id, $yl_id);
                }

                $configs = array($term_id, $stud_num, $section_id, $yl_id, $prog_id, $exclude_subj, $period_id,$replyslip,$excludegwa);

                $this->proc_report('report-card', $configs, $dl_fname, $prog_id);


            } else if($rep_type == 2){
                $period_id = decode(Request::get('period'));
                $pclass = decode(Request::get('pclass'));
                $dl_fname = 'ConsolidatedSubjectGrade';

                if(!empty($section_id)){
                    $yl_id = "''";
                }

                $configs = array($term_id, $period_id, $yl_id, $section_id, $pclass);

                $this->proc_report('subject-grade', $configs, $dl_fname);

            } else if($rep_type == 14){
                $prog_id = decode(Request::get('programs'));
                $configs = array($term_id, $prog_id, $yl_id);

                $this->proc_report('unofficially-enrolled-SHS', $configs);

            }else if($rep_type == 15){
                $prog_id = decode(Request::get('programs'));
                $period_id = decode(Request::get('period'));
                $prog_class = decode(Request::get('pclass'));
                $configs = array($term_id, $prog_id, $prog_class, $yl_id,$period_id);

                $this->proc_report('unposted-grades-SHS', $configs);

             }else if($rep_type == 16){
                $configs = array($term_id, $prog_id);

                $this->proc_report('shs academic ranking', $configs);

            }else if($rep_type == 17){
                $configs = array($term_id, $prog_id);

                $this->proc_report('enrollment-stat-section', $configs);

            }
            // elseif($rep_type == 18){

            //     $stud_num = Request::get('snum');
            //     $period_id = decode(Request::get('period'));


            //     $period_id = empty($period_id) ? 11 : $period_id;
            //     $stud_num = empty($stud_num) ? 0 : $stud_num;
            //     $exclude_subj = empty(Request::get('print-empty-subj')) ? 0 : 1;
            //     $dl_fname = 'ReportCard';

            //     if($section_id == 0 && $stud_num != 0) {
            //         $section_id = $this->model->getStudentSectionID($stud_num, $term_id, $prog_id, $yl_id);
            //     }

            //     $configs = array($term_id, $stud_num, $section_id, $yl_id, $prog_id, $exclude_subj, $period_id);

            //     $this->proc_report('backtoback-report-card', $configs, $dl_fname, $prog_id);


            // }
            // else if($rep_type == 19){
            //     $prog_class = decode(Request::get('pclass'));
            //     $configs = array($term_id, $prog_id, $prog_class);

            //     $this->proc_report('acad-failure-remedial-grades', $configs);

            // }
            else if($rep_type == 18){
                $configs = array($term_id);

                $this->proc_report('grade-correction-history', $configs);

            }
             else if($rep_type >= 3) {
                $prog_id = decode(Request::get('programs'));
                $configs = array($term_id, 1, getUserName(), 2, 'rep-type' => $rep_type, 'prog-id' => $prog_id);

                $this->proc_report('common-report-params', $configs, 'RegistarReport');

            } else {
                die('Unable to find report.');
            }

        }else{
		  if(isParent() || isStudent()){
			$rep_type = Request::get('report-type');

            // Common Params
            $term_id = decode(Request::get('academic-term'));
            $yl_id = decode(Request::get('year-level'));
            $section_id = decode(Request::get('section'));
            $prog_id = decode(Request::get('programs'));
            $section_id = empty($section_id) ? 0 : $section_id;
			
			$stud_num = Request::get('snum');
			$period_id = decode(Request::get('period'));
			$replyslip = empty(Request::get('reply-slip')) ? 0 : 1;
			$excludegwa = empty(Request::get('excludegwa')) ? 0 : 1;
			$stud_num = empty($stud_num) ? 0 : $stud_num;

			$exclude_subj = empty(Request::get('print-empty-subj')) ? 0 : 1;
			$dl_fname = 'ReportCard';

			if($section_id == 0 && $stud_num != 0) {
				$section_id = $this->model->getStudentSectionID($stud_num, $term_id, $prog_id, $yl_id);
			}

			$configs = array($term_id, $stud_num, $section_id, $yl_id, $prog_id, $exclude_subj, $period_id,$replyslip,$excludegwa);

			$this->proc_report('preview-card', $configs, $dl_fname, $prog_id);
		  }else{
			die('testing');
		  }
		}
        return $response;
    }

    private function proc_report($for, $configs = array(), $dl_name = '', $prog_id = null)
    {
        $this->initializer();
        $subrpt = array(
            'subreport1' => array(
                'file' => 'Company_Logo',
                'query' => "EXEC ES_rptReportLogo 'Everest Manila Academy', 'Republic', 'Bonifacio Global City', '-1'"
            )
        );
        if($for == 'report-card'){
            switch ($prog_id) {
                case '1':
                case '8':
		                $file = 'Report Card MSLS';
                        $this->xpdf->filename = $file.'.rpt';
                        $this->xpdf->query = 'EXEC sp_k12_ReportCard_MSLS '.implode(', ', $configs);
                        break;
                case '7':
                        $file = 'Report Card US';
                        $this->xpdf->filename = $file.'.rpt';
                        $this->xpdf->query = 'EXEC sp_k12_ReportCard_US '.implode(', ', $configs);

                        break;
                case '5':
                    die('No reports available.');
                    break;
                default:
                    die('No reports available.');
                    break;
            }

        }else if($for == 'preview-card'){
            switch ($prog_id) {
                case '1':
                case '8':
                        $file = 'Preview Card MSLS';
                        $this->xpdf->filename = $file.'.rpt';
                        $this->xpdf->query = 'EXEC sp_k12_ReportCard_MSLS '.implode(', ', $configs);
                        break;
                case '7':
                        $file = 'Preview Card US';
                        $this->xpdf->filename = $file.'.rpt';
                        $this->xpdf->query = 'EXEC sp_k12_ReportCard_US '.implode(', ', $configs);

                        break;
                case '5':
                    die('No reports available.');
                    break;
                default:
                    die('No reports available.');
                    break;
            }

        }
		else if($for == 'subject-grade') {
            $this->xpdf->filename = 'ConsolidatedSubjectGrades.rpt';
            $this->xpdf->query = 'EXEC sp_k12_ConsolidatedSubjectGrades '.implode(', ', $configs);

        }
        else if($for == 'shs academic ranking') {
            $this->xpdf->filename = 'shs academic ranking.rpt';
            $this->xpdf->query = 'EXEC sp_shs_ranking  '.implode(', ', $configs);

        }else if($for == 'unposted-grades-SHS') {
            $this->xpdf->filename = 'unposted_grades.rpt';
            $this->xpdf->query = 'EXEC sp_k12_UnpostedGrades  '.implode(', ', $configs);
        }
        else if($for == 'unofficially-enrolled-SHS') {
            $this->xpdf->filename = 'registrar_unofficially_enrolled_students_SHS.rpt';
            $this->xpdf->query = 'EXEC sp_k12_registrar_SHS_unofficially_enrolled '.implode(', ', $configs);


        } else if($for == 'enrollment-stat-section') {
            $this->xpdf->filename = 'EnrollmentStatisticsBySection_SHS.rpt';
            $this->xpdf->query = 'EXEC ES_rptEnrolmentStatisticsBySection_SHS  '.implode(', ', $configs);
        }
        // else if($for == 'backtoback-report-card') {
        //     $this->xpdf->filename = 'reportcardk12_SH_Final.rpt';
        //             $this->xpdf->query = 'EXEC sp_k12_ReportCard_SH_Final '.implode(', ', $configs);
        // }
        // else if($for == 'acad-failure-remedial-grades') {
        //     $this->xpdf->filename = 'AcademicFailuresWithRemedial.rpt';
        //     $this->xpdf->query = 'EXEC sp_k12_AcademicFailures_withRemedial   '.implode(', ', $configs);
        // }
        else if($for == 'grade-correction-history') {
            $this->xpdf->filename = 'GradeCorrectionHistory.rpt';
            $this->xpdf->query = 'EXEC sp_k12_GradeCorrectionHistory   '.implode(', ', $configs);
        }
         else if($for == 'common-report-params') {

            $programs = [5 => 'Kindergarten', 1 => 'LowerSchool', 8 => 'MiddleSchool', 7 => 'HighSchool'];
            $cur_prg = $programs[$configs['prog-id']];
            $printed_by = getUserFacultyID();

            switch ($configs['rep-type']) {

                case 3:
                    $filename = 'StudentDir';
                    $this->xpdf->filename = 'StudentDir.rpt';
                    $sp = 'ES_rptStudentDirectory';
                    $params = "'".$configs[0]."','1','".$configs['prog-id']."', '".$printed_by."'";

                    //var_dump($params);
                    //die();
                    break;
                case 4:
                    $filename = 'OfficialList_of_Enrollment';
                    $this->xpdf->filename = 'OfficialList_of_Enrollment.rpt';
                    $sp = 'ES_rptOfficialListofEnrollment3';
                    break;
                case 5:
                    $filename = 'Withdrawal_Students';
                    $this->xpdf->filename = 'Withdrawal_Students.rpt';
                    $sp = 'ES_rptListOfWithdrawalStudents';
                    break;
                case 6:
                    $filename = 'Students_by_Parents';
                    $this->xpdf->filename = 'Students_by_Parents.rpt';
                    $sp = 'ES_rptListofStudentsByParents';
                    break;
                case 7:
                    $filename = 'Students_by_Nationalities';
                    $this->xpdf->filename = 'Students_by_Nationalities.rpt';
                    $sp = 'ES_rptListofStudentsBy_Nationalities';
                    break;
                case 8:
                    $filename = 'NewStudents';
                    $this->xpdf->filename = 'NewStudents.rpt';
                    $sp = 'ES_rptListofEnrolledStudentsPerLevel_New';
                    $params = "'".$configs[0]."', '".$configs[1]."', '".$configs[2]."', '".$configs['prog-id']."'";
                    break;
                case 9:
                    $filename = 'EnrollmentStatistics_by_Gender';
                    $this->xpdf->filename = 'EnrollmentStatistics_by_Gender.rpt';
                    $sp = 'ES_rptEnrollmentStatisticByGender_v2';
                    $params = "'".$configs[0]."', '".$configs[1]."', '".$printed_by."', '".$configs['prog-id']."'";
                    break;
                case 10:
                    $filename = 'EnrollmentStatisctics_by_Age';
                    $this->xpdf->filename = 'EnrollmentStatisctics_by_Age.rpt';
                    $sp = 'ES_rptEnrollmentStatisticsByAge';
                    $params = "'1', '".$configs[0]."', '".$configs['prog-id']."', '".$configs[2]."'";
                    break;
                case 11:
                    $filename = 'EnrollmentProfile';
                    $this->xpdf->filename = 'EnrollmentProfile.rpt';
                    $sp = 'ES_rptEnrollmentProfile';
                    $params = "'".$configs[0]."', '".$configs['prog-id']."', '1', '".$printed_by."'";
                    break;
                case 12:
                    $filename = 'ClassList';
                    $this->xpdf->filename = 'ClassList.rpt';
                    $sp = 'ES_rptReportOnEnrollment';
                    $params = "'".$configs[0]."', '".$configs[1]."', '".$configs['prog-id']."', '".$printed_by."'";
                    break;
                case 13:
                    $filename = 'SummaryEnrollment';
                    $this->xpdf->filename = 'OfficialList_of_Enrollment.rpt';
                    $sp = 'ES_rptSummaryofEnrollment_r2';
                    $params = "'".$configs[0]."', '1', '0', '".$configs['prog-id']."', '".$printed_by."'";
                    break;

                default:
                    return false;
                    break;
            }


            if($configs['rep-type'] == 3 || $configs['rep-type'] == 12 || $configs['rep-type'] == 4 || $configs['rep-type'] == 5 || $configs['rep-type'] == 6 || $configs['rep-type'] == 7|| $configs['rep-type'] == 8 || $configs['rep-type'] == 9 || $configs['rep-type'] == 10 || $configs['rep-type'] == 11 || $configs['rep-type'] == 14) {
                $filename = $filename;

            }
            else {
                $filename = $filename.'_'.$cur_prg;

            }



            if($configs['rep-type'] == 8 || $configs['rep-type'] == 3 || $configs['rep-type'] == 12 || $configs['rep-type'] == 9 || $configs['rep-type'] == 10 || $configs['rep-type'] == 11 || $configs['rep-type'] == 13){
                $params = $params;    
            } 
            else {
                $params = "'".$configs[0]."', '".$configs[1]."', '".$configs[2]."'";
            }

            if($configs['rep-type'] == 3 || $configs['rep-type'] == 12 || $configs['rep-type'] == 4 || $configs['rep-type'] == 8 || $configs['rep-type'] == 9 || $configs['rep-type'] == 10 || $configs['rep-type'] == 11 || $configs['rep-type'] == 1 || $configs['rep-type'] == 13) {
                $sp = $sp;
            }
            else {
                $sp = $sp.'_'.$cur_prg;
            } 

            $this->xpdf->query = "EXEC $sp $params";
            // var_dump($this->xpdf->query);
            // die();

        } else {
            die('Unable to find report type!');
        }


        if($for == 'unofficially-enrolled-SHS' || $for == 'acad-failure-remedial-grades' || $for == 'grade-correction-history') {  

             $data = $this->xpdf->generate();
        }
        else 
        { 
            $this->xpdf->SubReport = $subrpt;
            $data = $this->xpdf->generate();

        }
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename='.$dl_name.'.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
        die();
    }

    public function export(){

        $this->pdf = new lpdf;
		set_time_limit(0);
        ini_set('memory_limit', '-1');
		$this->pdf->setPrintHeader(false);
		$this->pdf->setPrintFooter(false);
		$this->font = 11;
		//$this->customFont = 'Libraries/pdf/fonts/Calibri.ttf';
        //$this->pdf->SetFont($this->pdf->addTTFfont(app_path($this->customFont)),'',$this->font);

        $this->pdf->SetMargins(25, 25, 25, true);
        $this->pdf->setPageUnit('pt');

        $data = array( 'key' => decode(Request::get('key')));
        $this->pdf->AddPage('L','LETTER');
        $this->pdf->setTitle('Report Card');
        $this->pdf->writeHTML(view('reports.pgsreportcard',$data)->render());
        //echo view('reports.pgsreportcard',$data)->render();

        $this->pdf->output();
		set_time_limit(30);
        ini_set('memory_limit', '128M');
    }
	
	public function report_card_only()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }

        $this->ModuleName = 'report-card';
        $this->media['Title'] = 'Report Card';

        $this->initializer();
        if ($this->permission->has('read')) {
            $_incl = [
                'views' => $this->views,
                'at' => $this->model->AcademicTerm(),
                'progs' => $this->model->Programs(),
                'rep_types' => [
                    'report-card' => 'Report Card',
                    'report-of-grades' => 'Report of Grades',
                    'report-card-shs-full' =>'SHS Reporct Card - 1st and 2nd Sem',
                    'report-of-grades-r2' => 'Report of Grades 2'
                ]
            ];

            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    private function initializer()
    {
        // $this->services = new Services();
        $this->model = new Rep_Model();
        $this->permission = new Permission($this->ModuleName);
        $this->xpdf = new xpdf;
    }
}
