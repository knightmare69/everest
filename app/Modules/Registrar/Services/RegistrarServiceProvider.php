<?php

namespace App\Modules\Registrar\Services;

// use App\Modules\GradingSystem\Services\Areas\Validation;
use App\Modules\ClassRecords\Models\ReportCardDetails_model as mGrade;
use DB;
use Request;

class RegistrarServiceProvider
{
    public static function AcademicTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')
        // ->where('SchoolTerm', 'School Year')
        ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public static function Period()
    {
        $get = DB::table('ES_GS_GradingPeriods')->select('PeriodID', 'Description1', 'Description2', 'GroupClass')->get();
        return $get;
    }
    
    public static function UnpostGrade(){
        
        
        $res = true;
                
        $sched = decode(Request::get('sched'));
        $term = decode(Request::get('term'));
        $per = decode(Request::get('period'));
        
        $studs = Request::get('stud');
        
        foreach($studs as $r){
            $where = array(
                'TermID' => $term,
                'ScheduleID' => $sched
            );
            
            if($per == '11' ){
                $per = '1'; 
            }elseif($per == '2' || $per == '12'){
                $per = '2';
            }elseif($per == '13'){
                $per = '3';
            }
            
            try { 
                if($r['type'] == '1'){
                    DB::statement("UPDATE ES_PermanentRecord_Details SET DatePosted".$per." = NULL, PostedBy".$per." = '' WHERE TermID = '". $term ."' AND ScheduleID= '". $sched ."' AND StudentNo ='". decode($r['idno']) ."'");
                    
                    SystemLog('Unposting of Grades','','unposting of grades','unposting of grades', 'Student No:'.decode($r['idno']).', Period:'.$per.', ScheduleID = '. $sched . ', TermID = ' . $term  ,'record successfully unposted!' );
                        
                }elseif ( $r['type'] == '0'){
                    DB::statement("UPDATE ES_PermanentRecord_Details SET DatePosted".$per." = NULL, PostedBy".$per." = '' WHERE TermID = '". $term ."' AND ScheduleID= '". decode($r['idno']) ."'");
                    SystemLog('Unposting of Grades','','unposting of grades','unposting of grades', 'Student No:'.decode($r['idno']).', Period:'.$per.', ScheduleID = '. $sched . ', TermID = ' . $term  ,'record successfully unposted!' );
                    
                } 
            
            } catch(\Illuminate\Database\QueryException $ex){ 
              err_log($ex->getMessage()); 
              // Note any method of class PDOException can be called on $ex.
              $res = false;
            }                                                                                              
        }
                        
        return $res ;
           
    }
    
}