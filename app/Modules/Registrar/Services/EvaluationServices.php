<?php

namespace App\Modules\Registrar\Services;

// use App\Modules\GradingSystem\Services\Areas\Validation;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\ClassRecords\Models\ReportCardDetails_model as mGrade;
use App\Modules\ClassRecords\Models\ReportCard_model as mSummary;
use App\Modules\Registrar\Models\Subjects\Subjects as mSubjects;
use App\Modules\Setup\Models\GradingSystemSetup As gs_setup_model;
use App\Modules\Registrar\Models\WaivePreRequisites_model as mWaivePreReq;

use DB;
use Request;

class EvaluationServices
{
    public static function get_waivedSubject($idno, $subj, $prerq = 0, $mode='pre-requisite')
    {
        $ret = null;
        if($mode == 'pre-requisite')
        {                        
            $model = new mWaivePreReq();
            
            $whre = array(
                'StudentNo' => $idno ,
                'CourseID'  => $subj,
            );
            
            if($prerq != 0 ){
                $whre['PreRequisiteCourseID']=$prerq;    
            }
            
            $ret = count($model->where($whre)->get()) > 0 ? 1: 0 ;
        }
        
        return $ret;
        
    }
    
    public static function waive_prerequisite($idno,$index, $subj, $prereq )
    {
        $model = new mWaivePreReq();
        $whre = array(
            'StudentNo' => $idno ,
            'CourseID'  => $subj,
    	    'PreRequisiteCourseID' => $prereq,
        );
        
        $data = array(
            'StudentNo' => $idno ,
            'CurriculumPK' => $index,
            'CourseID'  => $subj,
    	    'PreRequisiteCourseID' => $prereq,
    	    'CreatedBy' => getUserID(),
            'CreatedDate' => systemDate()
        );
        
        $rs = $model->where($whre)->get();
        
        if(count($rs) > 0) {
            $model->where($whre)->update($data);
        }else{
            $model->create($data);    
        } 
        
        return true;
    }
    
    public static function prereqPassed($cIndex, $subjID, $idno)
    {        
        $model = new mGrade();
        
        $rs = self::getPrerequisite($cIndex, $subjID);
        $credited = 0;
        $credit_remarks = array(
            'passed', 'credited'
        );
        if(count($rs) > 0) {
            foreach($rs  as $r){
                $grades = $model->Student($idno)->Subject($r->SubjectID)->select('Final_Average','Final_Remarks')->get();
                if(count($grades) > 0) {                    
                    foreach($grades as $g){
                        if( in_array(strtolower($g->Final_Remarks), $credit_remarks )  ){
                            $credited = 1;    
                        }
                    }                          
                }        
            }
        } else{
            $credited = 1;    
        }
           
        return $credited;
        
    }
        
    public static function getPrereqSubCode($cIndex, $subjID){
        $rs = self::getPrerequisite($cIndex, $subjID);

        $ret ="";
        foreach($rs  as $r){
            $ret .= ($ret != '' ? ',':''). $r->Code;
        }

        return $ret;

    }

    public static function getPrerequisite($currIndex, $subjectID)
    {
        return DB::table('ES_PreRequisites')
                    ->selectRaw("SubjectID, dbo.fn_SubjectCode(SubjectID) As Code, dbo.fn_SubjectTitle(SubjectID) As Title ")
                    ->where(['Options'=>'Pre-Requisite','CurriculumIndexID'=> $currIndex])
                    ->where('SubjectID_Curriculum',$subjectID )
                    ->get();
    }
    
    public static function remove_creditSubject($idno, $subj){
        $model = new mGrade();
        
        $mSubj = getSubjectInfo($subj);
        
        $data = array(            
            'Final_Average' => '', 
            'Final_Remarks' => '',
            'LastModifiedDate' => systemDate(),
            'LastModifiedBy' => getUserID(),
        );
        
        $rs = $model->Student($idno)->subject($subj)->whereRaw("Final_Average = 'CRD' ")->update($data);
        SystemLog('Evaluation', '', 'Un-credit This Subject', 'manually remove credited subject', 'Subject ID:= '. $subj . ' SubjectCode:='. $mSubj->SubjectCode , '' );
        
        return $rs ;
    }
    
    public static function creditSubject($idno, $subj){
        
        $stud = new mStudents();
        $model = new mGrade();
        
        
        $mStu = $stud->student($idno)->selectRaw("*, dbo.fn_ProgramClassCode(ProgID) As ProgClass ")->first();
        $mSubj = getSubjectInfo($subj);
        
        $data = array(            
            'Final_Average' => 'CRD', 
            'Final_Remarks' => 'Credited',
            'LastModifiedDate' => systemDate(),
            'LastModifiedBy' => getUserID(),
            'DatePosted' => systemDate(),
            'PostedBy' => getUserID()
        );
        
        $rs = $model->Student($idno)->subject($subj);
        
        if( $rs->count() > 0 ){
            $ret = $rs->update($data);
        }else{
            $newdata = array(
                'CampusID' => 1,
                'TermID' => 0 ,
                'ProgClassID'=> $mStu->ProgClass,
                'ProgramID' => $mStu->ProgID,
                'YearLevelID' => $mStu->YearLevelID,
                'StudentNo' => $mStu->StudentNo,
                'ScheduleID' => 0,
                'SubjectID' => $subj,
                'SubjectCode' => $mSubj->SubjectCode,
                'SubjectTitle' => $mSubj->SubjectTitle,
                'SubjectCreditUnits' => $mSubj->CreditUnits,
                'CreditedUnits' => $mSubj->CreditUnits,
                'IsClubbing' => 0,
                'IsNonAcademic' => $mSubj->IsNonAcademic,
                'SubjectGroupID' => getObjectValueWithReturn($mSubj, 'SubjGroupID',0),
                'ParentSubjectID' => $mSubj->SubjParentID == '' ? 0 : $mSubj->SubjParentID,
                'IsSemestral' => $mSubj->IsSemestral,
                'IsNonConduct' => $mSubj->IsNonConduct,
                'SuppressSubject' => $mSubj->SuppressSubject,
                'SuppressAcademicGrade' => $mSubj->SuppressAcademicGrade,
                'SuppressConductGrade' => $mSubj->SuppressConductGrade,
            );
            
            $newdata = array_merge($newdata, $data);
            
            //$ret = $rs->create($newdata);
            $ret = $rs->insert($newdata);
            
        }
        
        SystemLog('Evaluation', '', 'Credit This Subject', 'manually credit the subject', 'Subject ID:= '. $subj . ' SubjectCode:='. $mSubj->SubjectCode , '' );
        
        return '1';
        
    }
    
    public static function computeSubjectAve($gradeIdx = 0 , $term = '', $Idno = '' ){
        
        $final = '';
        $remarks = '';
        $ave_ab = 0;
		$ave_cd = 0;
		
        $model = new mGrade();
        
        if($gradeIdx != 0){
            $where = ['GradeIDX' => $gradeIdx];    
        }else{
            $where = ['StudentNo' => $Idno, 'TermID'=> $term];
        }
                  
        $rs = $model->selectRaw("*, dbo.fn_gs_computation_setup(TermID, ProgramID, YearLevelID) as Computation")->where($where)->first();
        
        if(isSHS($rs->ProgramID) == '1'){
            
            if( trimmed($rs->AverageA) == '' || trimmed($rs->AverageB) == '' ){
                //atleast 1 term grade is missing
                $final = 0;
                
            }else{
                          
                //ready to compute final grade
                $final =  ((floatval($rs->AverageA) + floatval($rs->AverageB)) / 2 ) ;
                                
                 //DB::statement("UPDATE ES_PermanentRecord_Details SET Final_Average = '{$final}' , Final_Remarks = dbo.fn_gs_graderemarks(1, '{$final}' )  where GradeIDX = '". $gradeIdx ."'");
                 $data = array(
                    'Final_Average' => number_format($final,0),
                    'Final_Remarks' => grade_remarks('1', number_format($final,0))
                 );
                $model->where( $where)->update($data );
                         
            }    
             
        }else{
            
			if($rs->IsSemestral != '0' ){
				
				$ave_a = floatval($rs->AverageA) ;
				$ave_b = floatval($rs->AverageB);
				$ave_c = floatval($rs->AverageC);
				$ave_d = floatval($rs->AverageD);
				
				$ave_ab = 0;
				$ave_cd = 0;
				
				#select which type of semestral
				if($ave_a != '' && $ave_c != '')
				{
					$ave_ab = floatval($rs->AverageA);
					$ave_cd = floatval($rs->AverageC);
				}else if($ave_b != '' && $ave_d != ''){
					$ave_ab = floatval($rs->AverageB);
					$ave_cd = floatval($rs->AverageD);
				}
				
				$final = (floatval($ave_ab) + floatval($ave_cd)) / 2 ;
				
				$data = array(
					'TotalAverageAB' => number_format($ave_ab,2),
					'TotalAverageCD' => number_format($ave_cd,2),
					'Final_Average' => number_format($final,4),
					'Final_Remarks' => grade_remarks('1', number_format($final,0))
				);			
				
				$model->where( $where)->update($data );
					
				
			}else{						
				if($rs->AverageA == '' || $rs->AverageB == '' || $rs->AverageC == '' || $rs->AverageD == '' ){
					//atleast 1 quarter grade is missing
					$final = false;
					
				}else{
			
					if($rs->Computation == '1')
					{											
						//ready to compute final grade
						//$final = ((floatval($rs->AverageA) + floatval($rs->AverageB)  + floatval($rs->AverageC)  + floatval($rs->AverageD)) / 4);
						
						#IEncode Formula - san beda alabang formula                
						$ave_ab = (floatval($rs->AverageA) * (1/3)) + ( floatval($rs->AverageB) * (2/3));
						$ave_cd = (floatval($rs->AverageC) * (1/3)) + ( floatval($rs->AverageD) * (2/3));
						
						$final = (floatval($ave_ab) + floatval($ave_cd)) / 2 ;
										
									
						//DB::statement("UPDATE ES_PermanentRecord_Details SET Final_Average = '{$final}' , Final_Remarks = dbo.fn_gs_graderemarks(1,'{$final}')  where GradeIDX = '". $gradeIdx ."'");
						$data = array(
							'TotalAverageAB' => number_format($ave_ab,2),
							'TotalAverageCD' => number_format($ave_cd,2),
							'Final_Average' => number_format($final,4),
							'Final_Remarks' => grade_remarks('1', number_format($final,0))
						);					
					} else{	
						
						#Default - averaging (DEPED)
						$ave_ab = (floatval($rs->AverageA) +  floatval($rs->AverageB))/2;
						$ave_cd = (floatval($rs->AverageC)  +  floatval($rs->AverageD) )/2;
						
						$final = number_format( ( floatval($rs->AverageA) +  floatval($rs->AverageB) + floatval($rs->AverageC) +  floatval($rs->AverageD))/4,4);
						
						$data = array(
							'TotalAverageAB' => number_format($ave_ab,2),
							'TotalAverageCD' => number_format($ave_cd,2),
							'Final_Average' => number_format($final,4),
							'Final_Remarks' => grade_remarks('1', number_format($final,0))
						);
					}
					
					$model->where( $where)->update($data );
							
				}
			}			
        
        }
        
        return $final;
    } 
    
    public static function computeQuarterAve($term , $studentno, $period )
    {
    
        #Date Created :  2017.10.10 20:10H ARS 
       
        $final = '';
        $remarks = '';
        $prog_id = '';
         
        $id = array(
                '1' => 'A',
                '11' => 'A',
                '2' => 'B',
                '12' => 'B',
                '3' => 'C',
                '4' => 'D',
                '13' => 'c',
            );
        
        
        $col = 'Average'.$id[$period];
        $conduct = 'Conduct'.$id[$period];
		
        $model = new mGrade();                
        $where = ['StudentNo' => $studentno , 'TermID' => $term,  's.IsPassFail' => 0 ];  
        $qry = "ProgramID, SubjectCreditUnits, IsExclAverageCompute, IsExclConductCompute,  {$col} , {$conduct} ";
                                  
        //$rs = $model->selectRaw($qry)->where($where)->groupBy($col,'SubjectCreditUnits','SubjectID','ProgramID')->get();
        $rs = $model
                ->leftJoin("ES_Subjects as s", 's.SubjectID','=','ES_PermanentRecord_Details.SubjectID')
                ->selectRaw($qry)->where($where)
				->whereRaw("ScheduleID In (select ScheduleID From ES_Registrationdetails where RegiD = ES_permanentrecord_details.RegID)")
				->get();
        
        $inc = 0;
        $unit = 0;
        $sum = 0;
        $cnt = 0;
        $earned=0;
        $points=0;
		
		$avc_total = 0;
		$avc_cnt = 0;
        #err_log($rs);
        
		foreach($rs as $r)
		{
            
            $prog_id = $r->ProgramID;
                                    
            if( ($r->$col =='' || $r->$col == '0') && ($r->IsExclAverageCompute == '0' ) && $r->IsSemestral == '0' ) {
                $inc = 1;
                break;
            }else{
                If( is_numeric($r->$col) ) {
                                                            
                    if( isSHS($prog_id) ){    
                        $sum += trimmed($r->$col,'d');
                        $unit++;
                        
						#conduct compute
						$avc_total += trimmed($r->$conduct,'d');
						$avc_cnt++;
						
                    }else{
						
						if( $r->IsExclAverageCompute == '0') {
																	
							$sum += trimmed($r->$col,'d') * trimmed($r->SubjectCreditUnits,'d');
							$unit += floatval($r->SubjectCreditUnits) ;
							
						}
						
						if( $r->IsExclConductCompute == '0'){
												
							#conduct compute
							$avc_total += trimmed($r->$conduct,'d');
							$avc_cnt++;
							
						}
                    }					
                }                
            }
            
            $cnt++;
            
        }                                       
                
        if($inc == 0 && $unit > 0 ){
            
			$final = number_format($sum / $unit,2);
			$avc_final = number_format($avc_total / $avc_cnt,2);
			
            $col = 'Academic'.$id[$period].'_Average'; 
			$avc_col = 'Conduct'.$id[$period].'_Average'; ;
			$qry = "UPDATE ES_PermanentRecord_Master SET {$col} = {$final}, {$avc_col} = {$avc_final} Where TermID = '{$term}' AND StudentNo = '{$studentno}'";
			#err_log($qry);
            DB::statement($qry);             
        }
        
        if(floatval($final) > 0)
        {
            //self::computeGeneralAve($studentno,$term, $cnt, $unit,$earned, $points);
            #DO NOT CHANGE
			self::computeGeneralAveFromSubject($studentno,$term, $cnt, $unit,$earned, $points);
        }       
                        
        return $final;
    } 
    
    public static function computeParentSubjectAve($gradeIdx, $term, $idno , $period ){
        
        $model = new mGrade();
        
        $final = '';
        $remarks = '';        
        
        $id = array(
                '1' => 'A',
                '11' => 'A',
                '2' => 'B',
                '12' => 'B',
                '3' => 'C',
                '4' => 'D',
                '13' => 'c',
            );
            
        $col = 'Average'.$id[$period];
        
        $where = ['GradeIDX' => $gradeIdx];  
        
        $rs = $model->where($where)->first();
        
        if( $rs->ParentSubjectID != 0 ) {
            
            $parentID = $rs->ParentSubjectID;
            
            $where = ['TermID' => $term , 'StudentNo' => $idno, 'ParentSubjectID' => $parentID ];
            $rs = $model
                    ->selectRaw(" {$col} as Average, SubjectCreditUnits ")
                    ->where($where)
                    ->groupBy('SubjectID',$col,'SubjectCreditUnits')
                    ->get();
            
            $inc = 0;
            $unit = 0;
            $sum = 0;
        
            foreach($rs as $r){
                if( $r->Average =='' || $r->Average == '0') {
                    $inc = 1;
                    break;
                }else{
                    If( is_numeric($r->Average) ) {
                        $sum += trimmed($r->Average,'d') * trimmed($r->SubjectCreditUnits,'d') ;
                        $unit += trimmed($r->SubjectCreditUnits,'d') ;        
                    }                
                }
            }
            
            if($inc == 0){
                $final = $sum / $unit;
                DB::statement("UPDATE ES_PermanentRecord_Details SET {$col} = '{$final}' Where TermID = '{$term}' AND StudentNo = '{$idno}' AND SubjectID = '{$parentID}'"); 
            }else{
                $final = false;
            }                              
            
        }else{
            $final = false;
        }
                
        return $final;
    } 
    
    public function computeSectionQuaterGrade($term, $section, $period){
        $list = DB::table('es_registrations as r')            
            ->selectRaw(" StudentNo, ClassSectionID ")
            ->where('r.TermID', $term )
            ->where("ClassSectionID",$section)
            ->get();
        
        foreach($list as $r){
            $this->computeQuarterAve( $term , $r->StudentNo, $period );
        }
        
        return true;
    }
    
    public function getEvaluation($idno=''){
                
        $grades = DB::table('ES_CurriculumDetails as cd')
            ->leftJoin("ES_Students as x",'x.curriculumid','=','cd.curriculumid')
            ->leftJoin('ES_Subjects as s','s.SubjectID','=','cd.SubjectID' )
            ->selectRaw("cd.IndexID, cd.CurriculumID, cd.SubjectID, s.SubjectCode, s.SubjectTitle, s.LectHrs As Credit, s.IsPassFail , YearTermID, dbo.fn_YearTerm(YearTermID) AS YearTerm,
                
                (SELECT COUNT(cdx.IndexID) FROM dbo.ES_CurriculumDetails cdx WHERE cdx.YearTermID = cd.YearTermID AND cdx.CurriculumID=cd.CurriculumID ) AS [TotalSubjects]
            
            ")
            ->where('x.studentno', $idno )
            ->whereRaw("YearTermID <> 22")
            ->orderBy('cd.YearTermID','asc')
            ->orderBy('cd.SortOrder','asc')
            ->get();
                               
        return $grades;
            
    }
    
    #Compute Conduct Subject Average
    public static function computeConductAve($gradeIdx){
        
		$conA = '';
		$conB = '';
		$conC = '';
		$conD = '';
		
		$cancel = false;
		
        $final = '';
        $remarks = '';
        
        $model = new mGrade();
        
        $where = ['GradeIDX' => $gradeIdx];  
		
        #Case condition was hard coded for SHS
        $rs = $model->selectRaw("*, dbo.fn_gs_computation_setup(TermID, ProgramID, YearLevelID) as Computation,  
				dbo.fn_get_conductave_bysubj(termid, case when ProgramID = 29 THEN 11 ELSE 1 END , studentno, scheduleid) as NewConductA,
				dbo.fn_get_conductave_bysubj(termid, case when ProgramID = 29 THEN 12 ELSE 2 END , studentno, scheduleid) as NewConductB,
				dbo.fn_get_conductave_bysubj(termid, 3, studentno, scheduleid) as NewConductC,
				dbo.fn_get_conductave_bysubj(termid, 4, studentno, scheduleid) as NewConductD
			")->where($where)->first();
        
        if($rs->ConductA == '' || $rs->ConductB == '' || $rs->ConductC == '' || $rs->ConductD == '' 
			|| floatval($rs->ConductA) == 0 || floatval($rs->ConductB) == 0 
			|| floatval($rs->ConductC) == 0 || floatval($rs->ConductD) == 0 
		  )
		{
            //at least 1 quarter conduct grade is missing, computation canceled
            $cancel = true;
			
			if($rs->Computation =='0')
			{							
				if( floatval($rs->ConductA) == 0){ 
					$conA = floatval($rs->NewConductA) > 0 ? $rs->NewConductA : '';
					DB::statement("UPDATE ES_PermanentRecord_Details SET ConductA = round('{$conA}',2) where GradeIDX = '". $gradeIdx ."'");
				}
				if( floatval($rs->ConductB) == 0){ 
					$conB = floatval($rs->NewConductB) > 0 ? $rs->NewConductB : '';
					DB::statement("UPDATE ES_PermanentRecord_Details SET ConductB = round('{$conB}',2) where GradeIDX = '". $gradeIdx ."'");
				}
				if( floatval($rs->ConductC) == 0){ 
					$conC = floatval($rs->NewConductC) > 0 ? $rs->NewConductC : '';
					DB::statement("UPDATE ES_PermanentRecord_Details SET ConductC = round('{$conC}',2) where GradeIDX = '". $gradeIdx ."'");
				}
				if( floatval($rs->ConductD) == 0){ 
					$conD = floatval($rs->NewConductD) > 0 ? $rs->NewConductD : '';
					DB::statement("UPDATE ES_PermanentRecord_Details SET ConductD = round('{$conD}',2) where GradeIDX = '". $gradeIdx ."'");
				}
			}
			            
        }else{
			
            $conA = $rs->ConductA == '0.0000' ? '' : $rs->ConductA ;
			$conB = $rs->ConductB == '0.0000' ? '' : $rs->ConductB ;
			$conC = $rs->ConductC == '0.0000' ? '' : $rs->ConductC ;
			$conD = $rs->ConductD == '0.0000' ? '' : $rs->ConductD ;            					
			
        } 

		if(!$cancel){
			if( isSHS($rs->ProgramID) )
			{
				$final = round((floatval($conA) + floatval($conB)) / 2);                                            
			} else{
				$final = round((floatval($conA) + floatval($conB)  + floatval($conC)  + floatval($conD)) / 4);                                            
			}
							            
			DB::statement("UPDATE ES_PermanentRecord_Details SET Conduct_FinalAverage = '{$final}' , Conduct_FinalRemarks = ISNULL(dbo.fn_gs_graderemarks(2,'{$final}'),'') where GradeIDX = '". $gradeIdx ."'");
		}
        
        return $final;
    } 
    
    #compute raw grade 
    public static function computeRawgrade($sched, $period, $gender, $idno ){
        
        $sql = "SELECT c.ComponentID, c.Percentage as weight , ISNULL(SUM(g.TotalItems),0) AS TotalItems, 	
	           ISNULL( (SELECT SUM(r1.RawScore) 
                		FROM dbo.ES_GS_RawScores r1 
                			LEFT JOIN dbo.ES_GS_GradeEvents g1 ON g1.EventID=r1.EventID AND g1.ScheduleID = {$sched}
                		WHERE  StudentNo = '{$idno}' AND g1.ComponentID = g.ComponentID AND g1.GradingPeriod = {$period} AND g1.Gender ={$gender}  ),0) AS Scores
                FROM ES_GS_PolicySetup p
                	LEFT JOIN dbo.ES_GS_PolicyComponents c ON p.PolicyID=c.PolicyID
                	LEFT JOIN dbo.ES_GS_GradeEvents g ON g.ScheduleID=p.ScheduleID AND g.ComponentID=c.ComponentID	
                WHERE p.ScheduleID ={$sched} AND p.PeriodID = {$period} AND g.GradingPeriod = {$period} AND isnull(g.Gender, {$gender}) = {$gender}                
                GROUP BY c.ComponentID,  c.Percentage, g.ComponentID, g.Gender ";
                
        $rs  = DB::select($sql);
        
        $rawgrade = 0;
                    
        foreach($rs as $r){
                                
            if($r->TotalItems > 0 ){
                $percent = ( $r->Scores / $r->TotalItems)  ;
                $rawgrade += ($percent * $r->weight );     
            }                                                           
        }
        
        return $rawgrade ;
        
    }
    
    #Compute Subject Average & Conduct
    public static function computeGeneralAve($idno, $term, $subjs =0, $units = 0,$earned=0, $points=0  ){
        
        $ave = '';
        $remarks = '';
        $avc = 0;
        $model = new mSummary();
        
        $where = ['StudentNo' => $idno, 'TermID' => $term];  
        
        $rs = $model->where($where)->first();
        
        if( isSHS($rs->ProgramID) ){
            if( floatval($rs->AcademicA_Average) > 0 && floatval($rs->AcademicB_Average) > 0  ){
                $ave = ( floatval($rs->AcademicA_Average) + floatval($rs->AcademicB_Average) ) / 2 ;    
            }                
        }else{
            if(floatval($rs->AcademicA_Average) > 0 && floatval($rs->AcademicB_Average) > 0 && floatval($rs->AcademicC_Average) && floatval($rs->AcademicD_Average) > 0  ){
                
                #default formula
                $ave = ( floatval($rs->AcademicA_Average) + 
                         floatval($rs->AcademicB_Average) + 
                         floatval($rs->AcademicC_Average) + 
                         floatval($rs->AcademicD_Average) 
                       ) / 4 ;                                                                
				
				#conduct average
                $avc = (floatval($rs->ConductA_Average) + floatval($rs->ConductB_Average) +  floatval($rs->ConductC_Average) +  floatval($rs->ConductD_Average)) / 4 ; 
            }            
        }       
                 
        if( floatval($ave) > 0){
            $data = ['TotalSubjects' => $subjs,
                     'TotalUnitsEnrolled' => $units,
                     'TotalUnitsEarned' => $earned,
                     'Final_Average' => number_format($ave,4), 
                     'Final_Remarks'=> $remarks,
					 'Final_Conduct'=> number_format($avc,4),
					 ];
                     
            $rs->where($where)->update($data);    
        }
                                          
        return $ave;
    } 
    
	 #Compute Subject Average & Conduct by Subject Final Ave.
    public static function computeGeneralAveFromSubject($idno, $term, $subjs =0, $units = 0,$earned=0, $points=0  ){
        
        $ave = '';
        $remarks = '';
        
		$avc = 0;
		$conduct =0;
			
        $model = new mSummary();
        
        $where = ['StudentNo' => $idno, 'TermID' => $term];  
        
        $rs = $model->selectRaw("*, dbo.fn_gs_computation_setup(TermID, ProgramID, YearLevelID) as Computation")->where($where)->first();
        
		$m_grade = new mGrade();
		$grades = $m_grade->selectRaw("*, dbo.[fn_IsParentSubject](SubjectID) as ParentSubject, dbo.fn_IsPassFail(SubjectID) As PassFail ")
					->where(['StudentNo' => $idno, 'TermID' => $term ])
					->whereRaw("ScheduleID In (select ScheduleID From ES_Registrationdetails where RegiD = ES_permanentrecord_details.RegID)")
					->get();
		
		#Reset general Average.
		$data = ['TotalSubjects' => $subjs,
				 'TotalUnitsEnrolled' => $units,
				 'TotalUnitsEarned' => $earned,
				 'Final_Average' => '0.00', 
				 'Final_Remarks'=> '',
				 'Final_Conduct'=> '',
				 ];
					 
        $rs->where($where)->update($data); 
			
        if( isSHS($rs->ProgramID) ){
         
   		    $ave = 0;
			$unit =0 ;
			
			foreach($grades as $d){
				
				if($d->IsExclAverageCompute == 0 && $d->ParentSubject == 0 && $d->PassFail == 0 ){
					$ave += floatval($d->Final_Average);
					$unit ++;
				}
								
				if($d->IsExclConductCompute == 0 && $d->PassFail == 0 ){
					$avc += floatval($d->Conduct_FinalAverage);				
					$conduct++;	
				}
				
			}
			
			$ave = $ave / $unit;
			$avc = $avc / $conduct;
			
			
        }else{
			
			
						
			$ave = 0;
			$unit =0 ;
			$note = '';
			
			foreach($grades as $d){
				
				if($d->IsExclAverageCompute == 0 && $d->ParentSubject == 0 )
				{
					if( $rs->Computation == '1') {
						#SBCA Custom Computation  (IENCODE)	
						$ave += ( (floatval($d->TotalAverageAB) + floatval($d->TotalAverageCD)) / 2)   * floatval($d->SubjectCreditUnits) ;
						$unit += floatval($d->SubjectCreditUnits);	
					}else{
						
						#Averaging (DEPED)
						if( trimmed($d->Final_Average) == '')
						{
							$ave=0;
							break;
						}
						else
						{
							$ave += floatval($d->Final_Average) * floatval($d->SubjectCreditUnits) ;
							$unit += floatval($d->SubjectCreditUnits);	
						}
						
					}
					
				}
								
				if($d->IsExclConductCompute == 0){
					$avc += floatval($d->Conduct_FinalAverage);				
					$conduct++;	
				}
				
			}
			
			#err_log('ave : '. $ave . ' unit : '. $unit);
			if($ave > 0)
            {
                $ave = $ave / $unit;
            }
			//$avc = $avc / $conduct;
			#Revised : 2018-05-28 9:13H By ARS
			$avc = (floatval($rs->ConductA_Average)+
				   floatval($rs->ConductB_Average)+	
				   floatval($rs->ConductC_Average)+
				   floatval($rs->ConductD_Average))/4;
                  
        }       
                 
        if( floatval($ave) > 0){
            $data = ['TotalSubjects' => $subjs,
                     'TotalUnitsEnrolled' => $units,
                     'TotalUnitsEarned' => $earned,
                     'Final_Average' => number_format($ave,4), 
                     'Final_Remarks'=> $remarks,
					 'Final_Conduct'=> number_format($avc,4),
					 ];
					 
            $rs->where($where)->update($data);    
        }
                                          
        return $ave;
    } 
	
	#Compute Grade Point Average
    public static function computeGPA($idno, $term ){
		$model = new mSummary();
		$m_grade = new mGrade();
		
		$GradSysID=0;
		
		$where = ['StudentNo' => $idno, 'TermID' => $term];          
        $rs = $model->selectRaw("*")->where($where)->first();
		
		#Get Grading System
		$m_setup = DB::table("ESv2_Gradesheet_Setup")
						->where("TermID", $term)
						->where("YearLevelID", $rs->YearLevelID)
						->where("ProgID", $rs->ProgramID)
						->first();
						
        $grades = $m_grade->selectRaw("*, 
							dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', Final_Average) AS GradePoints, 
							dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageA) AS GP1, 
							dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageB) AS GP2, 
							dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageC) AS GP3, 
							dbo.[fn_gs_gradepoints]('".$m_setup->LetterGradeID."', AverageD) AS GP4, 
							[dbo].[fn_SubjectWeight](SubjectID) As Weight ")
						->where($where)
						->whereRaw("ScheduleID In (select ScheduleID From ES_Registrationdetails where RegiD = ES_permanentrecord_details.RegID)")
						->get();
		
		$gpa = 0;
		$gpa1 = 0;
		$gpa2 = 0;
		$gpa3 = 0;
		$gpa4 = 0;
		
		foreach($grades as $r){
			$gpa += $r->GradePoints * $r->Weight;
			if ($r->AverageA != '') $gpa1 += $r->GP1 * $r->Weight;
			if ($r->AverageB != '') $gpa2 += $r->GP2 * $r->Weight;
			if ($r->AverageC != '') $gpa3 += $r->GP3 * $r->Weight;
			if ($r->AverageD != '') $gpa4 += $r->GP4 * $r->Weight;
		}
		
		$data =['GPA' => $gpa, 'GPA1'=> $gpa1 ,'GPA2'=> $gpa2 ,'GPA3'=> $gpa3 ,'GPA4'=> $gpa4 ];
		
		$model->where($where)->update($data);
		
		return $gpa;
		
	}
    
	
    
}