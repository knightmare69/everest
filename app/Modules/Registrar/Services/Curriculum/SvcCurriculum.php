<?php
namespace App\Modules\Registrar\Services\Curriculum;
use DateTime;
use DB;
Class SvcCurriculum
{
    public function dropdown($param = array())
	{
	  $sel_field = ((array_key_exists('sel_field',$param))?$param['sel_field']:'*');
	  $table     = ((array_key_exists('table',$param))?$param['table']:'');
	  $col_id    = ((array_key_exists('col_id',$param))?$param['col_id']:'');
	  $col_desc  = ((array_key_exists('col_desc',$param))?$param['col_desc']:'');
	  $col_sep   = ((array_key_exists('col_sep',$param))?$param['col_sep']:' ');
	  $name      = ((array_key_exists('name',$param))?$param['name']:'');
	  $cls       = ((array_key_exists('cls',$param))?$param['cls']:'');
	  $attr      = ((array_key_exists('attr',$param))?$param['attr']:'');
	  $selected  = ((array_key_exists('selected',$param))?$param['selected']:'');
	  $init      = ((array_key_exists('init',$param))?$param['init']:'');
	  $init_lbl  = ((array_key_exists('init_lbl',$param))?$param['init_lbl']:'');
	  $limit     = ((array_key_exists('limit',$param))?$param['limit']:0);
	  $limit_sel = ((array_key_exists('limit_sel',$param))?$param['limit_sel']:0);
	  $condition = ((array_key_exists('condition',$param))?$param['condition']:'');
	  $order     = ((array_key_exists('order',$param))?$param['order']:'');
	  $spc_attr  = '';
	  $count     = 0;
	  $pointer   = '';
	  
	  if(($sel_field)=='*')
	   $result = DB::table($table);
	  else
	   $result = DB::table($table)->select(DB::raw($sel_field));   
   
	  if($condition!='')
	  {
	   $result = $result->whereRaw($condition);	  
	  }
	  
      if($order!='')
      { 
        if(is_array($order))
		{
		 foreach($order as $k=>$v)
         {
		  $result = $result->orderBy($k,$v);	
		 }		 
		}
        else
        {			
		 $split_ord = explode(' ',$order);
      	 $result = $result->orderBy($split_ord[0],$split_ord[1]);	
		} 
	  }		  
	  
	  $result = (($table=='ESv2_YearLevel')? $this->get_yearlvl_list() : $result->get());  
	  
	  $output='<select id="'.$name.'" name="'.$name.'" class="'.$cls.'" disabled><option value="-1" disabled selected>No Option Available</option></select>';
	  if($result)
	  {
		$output='<select id="'.$name.'" name="'.$name.'" class="'.$cls.'" '.$attr.'>';
	    if($init!=''){$output .= '<option value="'.$init.'" '.(($selected=='')?'selected':'').'>'.(($init_lbl!='')?$init_lbl:'-Select One-').'</option>'; }
		foreach($result as $rs)
        {
		 $desc  = '';
		 $value = ((property_exists($rs,$col_id))?$rs->$col_id:'');	
		 if(is_array($col_desc))
		 {	 
		  foreach($col_desc as $arr_desc)
		  {
			$desc .= (($desc!='')?$col_sep:'').((property_exists($rs,$arr_desc))?$rs->$arr_desc:'');	  
		  }
	     }
		 else	 
		  $desc  = ((property_exists($rs,$col_desc))?$rs->$col_desc:'');	
	     
		 if($table=='ESv2_YearLevel')
		  $spc_attr ='data-class="'.((property_exists($rs,'ProgClass'))?$rs->ProgClass:'').'" data-prog="'.((property_exists($rs,'ProgID'))?$rs->ProgID:'').'" data-old-id="'.((property_exists($rs,'Old_YearLevelID'))?$rs->Old_YearLevelID:'').'"  data-code="'.((property_exists($rs,'YearLevelCode'))?$rs->YearLevelCode:'').'" data-withmajor="'.((property_exists($rs,'YearLevelCode') && ($rs->YearLevelCode=='G10' || $rs->YearLevelCode=='G11' || $rs->YearLevelCode=='G12'))?1:0).'"';	  
		 else if($table=='ESv2_PaymentMethods')	 
		  $spc_attr ='data-instruct="'.((property_exists($rs,'Instructions'))?$rs->Instructions:'').'"';	  
		 
		 if($limit>0)
		 {
		   if($pointer=='' && $value==$selected && $count<$limit)
		   {
			 $pointer = $value;
			 $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($count==$limit_sel)?'selected':'').'>'.$desc.'</option>'; 
			 $count++;
	       }
           else if($pointer!='' && $count<$limit)
		   {
			 $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($count==$limit_sel)?'selected':'').'>'.$desc.'</option>'; 
			 $count++;
	       }		   
		 }
		 else if($table=='ESv2_PaymentMethods')
		  $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($rs->SystemDefault==1 || $value==$selected)?'selected':'').'>'.$desc.'</option>';	 
		 else	 
		  $output .= '<option value="'.$value.'" '.$spc_attr.' '.(($value==$selected)?'selected':'').'>'.$desc.'</option>';
		}		
		$output.='</select>';
	  }	  
	  
	  return $output;	
	}
    
    public function get_program_details($progid=0)
	{
	  if($progid==0){return false;}
	  $query = "SELECT * FROM ES_Programs as p 
                    LEFT JOIN ES_Program_Semestral as ps ON p.Semestral=ps.SemestralCode
                        WHERE ProgID='".$progid."'";
	  $exec = DB::select($query);
      if(!$exec)
	   return false;
      else
      {	
       $result = array();  
	   foreach($exec as $rs)
	   {
		$result['ProgID']         = $this->is_key_exist($rs,'ProgID',0);    
		$result['CampusID']       = $this->is_key_exist($rs,'CampusID',0);    
		$result['CollegeID']      = $this->is_key_exist($rs,'CollegeID',0);    
		$result['ProgCode']       = $this->is_key_exist($rs,'ProgCode','');    
		$result['ProgName']       = $this->is_key_exist($rs,'ProgName','');    
		$result['ProgYears']      = $this->is_key_exist($rs,'ProgYears',0);    
		$result['ProgSems']       = $this->is_key_exist($rs,'ProgSems',0);    
		$result['ProgDiscipline'] = $this->is_key_exist($rs,'ProgDiscipline',0);    
		$result['ProgClass']      = $this->is_key_exist($rs,'ProgClass',0);    
		$result['ProgStatus']     = $this->is_key_exist($rs,'ProgStatus',0);    
		$result['ProgParent']     = $this->is_key_exist($rs,'ProgParent',0);    
		$result['Semestral']      = $this->is_key_exist($rs,'Semestral',0);    
		$result['SemestralDesc']  = $this->is_key_exist($rs,'SemestralDesc','');    
	   }
	   if($this->is_key_exist($result,'ProgID')==false)
		 return false;
       else
         return $result;		   
	  }
	}	
    
	public function get_curriculum($progid=0)
	{
	  if($progid==0){return false;}
	  $query = "SELECT * FROM ES_Curriculums WHERE ProgramID='".$progid."'";
	  $exec  =  DB::select($query);
      if(!$exec)
	   return false;
      else
	  {	  
       $output='';
	   foreach($exec as $rs)
	   {
		$xid   = $this->is_key_exist($rs,'IndexID',-1);
		$code  = $this->is_key_exist($rs,'CurriculumCode','');
		$desc  = $this->is_key_exist($rs,'CurriculumDesc','');
		$note  = $this->is_key_exist($rs,'Notes','');
        $major = $this->is_key_exist($rs,'MajorID',-1);
		$rmin  = $this->is_key_exist($rs,'RegularMinLoad',0);
		$rmax  = $this->is_key_exist($rs,'RegularMaxLoad',0);
		$smin  = $this->is_key_exist($rs,'SummerMinLoad',0);
		$smax  = $this->is_key_exist($rs,'SummerMaxLoad',0);
		$zero  = $this->is_key_exist($rs,'NonZeroBased',0);
		$inact = $this->is_key_exist($rs,'Inactive',0);
		$lock  = (($this->is_key_exist($rs,'DateLocked','')=='')?0:1);
		   
		$output.='<li class="curriculum" data-id="'.$xid.'" data-major="'.$major.'" data-rmin="'.$rmin.'" data-rmax="'.$rmax.'" data-smin="'.$smin.'" data-smax="'.$smax.'">
		            <a href="javascript:void(0);">'.$this->is_key_exist($rs,'CurriculumCode','').' - '.$this->is_key_exist($rs,'CurriculumDesc','').'</a>
					<p class="notes hidden" data-code="'.$code.'" data-desc="'.$desc.'" data-zero="'.$zero.'" data-inactive="'.$inact.'" data-lock="'.$lock.'">'.$note.'</p>
				  </li>';
	   }
	   return $output;
	  } 
	}
	
	public function get_cdetails($prog=0,$curr=0)
	{
	 if($prog<=0 && $curr<=0){return false; }
	 $query = "SELECT cd.IndexID
					 ,cd.SortOrder
					 ,c.ProgramID
					 ,c.IndexID as CurriculumID
					 ,c.DateLocked
					 ,yt.YearTermID as YearTermID
					 ,yt.TermCode
					 ,yt.YearLevelID
					 ,yt.YearLevel
					 ,yt.ProgClass
					 ,yt.YearTermDesc
					 ,cd.SortOrder
					 ,cd.SubjectID
					 ,s.SubjectCode
					 ,s.SubjectTitle
					 ,s.AcadUnits
					 ,s.LabUnits
					 ,s.CreditUnits
					 ,s.LectHrs
					 ,s.LabHrs
					 ,s.SubjParentID
					 ,cd.YearStandingID
					 ,cd.EquivalentSubjectID
					 ,cd.TransferedSubjectID
					 ,cd.TransferedEquivalentSubjectID
					 ,cd.SubjectElective
					 ,cd.SubjectMajor
					 ,dbo.fn_PreRequisites(cd.IndexID, 'Pre-Requisite') AS PreRequisites
					 ,dbo.fn_PreRequisites(cd.IndexID, 'Co-Requisite') AS CoRequisites
                     ,CASE WHEN cd.SubjectElective = 1 AND cd.SubjectMajor = 1
                           THEN dbo.fn_sisGetElectiveCurriculumSubjects(c.IndexID)
                           ELSE dbo.fn_PreRequisites(cd.IndexID, 'Equivalent')
                      END AS Equivalent
				 FROM ES_Programs as p
				 LEFT OUTER JOIN(SELECT yr.YearLevelID,Description2 as YearLevel,11 as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
								   FROM ES_YearLevel as yr 
								   LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=12
								  WHERE Description2<>'' AND LevelGroup1='Primary'
								 UNION ALL
								 SELECT yr.YearLevelID,Description2 as YearLevel,12 as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
								   FROM ES_YearLevel as yr 
								   LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=12
								  WHERE Description2<>'' AND LevelGroup1='Middle' 
								 UNION ALL
								 SELECT yr.YearLevelID,HSDescription as YearLevel,20 as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
								   FROM ES_YearLevel as yr 
								   LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=11
								  WHERE HSDescription<>'' AND yr.YearLevelID<5
								 UNION ALL
								 SELECT yr.YearLevelID,HSDescription as YearLevel,29 as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
								   FROM ES_YearLevel as yr 
								   LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=11
								  WHERE HSDescription<>'' AND yr.YearLevelID>=5
								 UNION ALL
								 SELECT yr.YearLevelID,YearLevel as YearLevel,'' as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
								   FROM ES_YearLevel as yr
								   LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND (yt.TermCode<>11 AND yt.TermCode<>12 AND yt.IndexID NOT IN (21,22,78,79,83))
								 UNION ALL
								 SELECT 0 as YearLevelID,'' as YearLevel,'' as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,CASE WHEN yt.YearStatus=0 THEN 100 ELSE yt.YearStatus END as YearStatus  
								   FROM ES_YearTerms as yt 
								  WHERE yt.TermCode<>11 AND yt.TermCode<>12 AND yt.IndexID IN (21,22,78,79,83)
								) as yt ON p.Semestral = yt.TermCode
				 LEFT JOIN ES_Curriculums as c ON p.ProgID=c.ProgramID AND c.IndexID='".$curr."'
				 LEFT JOIN ES_CurriculumDetails as cd ON c.IndexID=cd.CurriculumID AND cd.YearTermID=yt.YearTermID
				 LEFT JOIN ES_Subjects as s ON cd.SubjectID=s.SubjectID
				WHERE ".(($prog>0)?" p.ProgID='".$prog."'":" c.IndexID='".$curr."'")."
				ORDER BY yt.YearStatus,cd.YearTermID ASC, cd.SortOrder ASC";	
	 $exec  =  DB::select($query);
     if($exec)
      return $exec;
     else   
	  return false;	
	}
	
	public function get_reqdetails($prog=0,$curr=0,$indx=0)
	{
	 if($prog<=0 && $curr<=0 && $indx<=0){return false; }
	 $query = "SELECT cd.IndexID
					 ,cd.SortOrder
					 ,c.ProgramID
					 ,c.IndexID as CurriculumID
					 ,c.DateLocked
					 ,yt.YearTermID as YearTermID
					 ,yt.TermCode
					 ,yt.YearLevelID
					 ,yt.YearLevel
					 ,yt.ProgClass
					 ,yt.YearTermDesc
					 ,cd.SortOrder
					 ,cd.SubjectID
					 ,s.SubjectCode
					 ,s.SubjectTitle
					 ,s.AcadUnits
					 ,s.LabUnits
					 ,s.CreditUnits
					 ,s.LectHrs
					 ,s.LabHrs
					 ,s.SubjParentID
					 ,cd.YearStandingID
					 ,cd.EquivalentSubjectID
					 ,cd.TransferedSubjectID
					 ,cd.TransferedEquivalentSubjectID
					 ,cd.SubjectElective
					 ,cd.SubjectMajor
					 ,dbo.fn_PreRequisites(cd.IndexID, 'Pre-Requisite') AS PreRequisites
					 ,dbo.fn_PreRequisites(cd.IndexID, 'Co-Requisite') AS CoRequisites
                     ,CASE WHEN cd.SubjectElective = 1 AND cd.SubjectMajor = 1
                           THEN dbo.fn_sisGetElectiveCurriculumSubjects(c.IndexID)
                           ELSE dbo.fn_PreRequisites(cd.IndexID, 'Equivalent')
                      END AS Equivalent
					 ,pre.Options
				 FROM ES_Programs as p
				 LEFT OUTER JOIN(SELECT yr.YLID_OldValue as YearLevelID,YearLevelName as YearLevel,ProgClass as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
					            FROM ESv2_YearLevel as yr 
					       LEFT JOIN ES_YearTerms as yt ON yr.YLID_OldValue=yt.YearLevelID AND yt.TermCode=12
					           WHERE yr.YearLevelName<>''AND ProgClass<20
					       UNION ALL
					          SELECT yr.YLID_OldValue as YearLevelID,YearLevelName as YearLevel,ProgClass as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
					            FROM ESv2_YearLevel as yr 
					       LEFT JOIN ES_YearTerms as yt ON yr.YLID_OldValue=yt.YearLevelID AND yt.TermCode=11
					           WHERE yr.YearLevelName<>'' AND ProgClass=20
					       UNION ALL
					          SELECT yr.YearLevelID,YearLevel as YearLevel,'' as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
					            FROM ES_YearLevel as yr
					       LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND (yt.TermCode<>11 AND yt.TermCode<>12 AND yt.IndexID NOT IN (21,22,78,79,83))
						     ) as yt ON p.Semestral = yt.TermCode
				 LEFT JOIN ES_Curriculums as c ON p.ProgID=c.ProgramID AND c.IndexID='".$curr."'
				 LEFT JOIN ES_CurriculumDetails as cd ON c.IndexID=cd.CurriculumID AND cd.YearTermID=yt.YearTermID
				 LEFT JOIN ES_Subjects as s ON cd.SubjectID=s.SubjectID
				 LEFT JOIN ES_PreRequisites as pre ON pre.CurriculumIndexID='".$indx."' AND pre.SubjectID=cd.SubjectID AND (pre.Options='Pre-Requisite' OR pre.Options='Co-Requisite')
				WHERE ".(($prog>0)?" p.ProgID='".$prog."'":" c.IndexID='".$curr."'")." AND cd.IndexID<>'".$indx."' AND cd.YearTermID<=(SELECT YearTermID FROM ES_CurriculumDetails WHERE IndexID='".$indx."') 
				ORDER BY yt.YearStatus,cd.YearTermID ASC, cd.SortOrder ASC";	
	 $exec  =  DB::select($query);
     if($exec)
      return $exec;
     else   
	  return false;	
	}
	
	public function get_equivsubj($indx=0)
	{
	 if($indx<=0){return false; }
	 $query = "SELECT pre.IndexID,s.* 
				 FROM ES_PreRequisites as pre
				INNER JOIN ES_Subjects as s ON pre.SubjectID=s.SubjectID 
				WHERE pre.Options='Equivalent' AND pre.CurriculumIndexID='".$indx."'";	
	 $exec  =  DB::select($query);
     if($exec)
      return $exec;
     else   
	  return false;	
	}
	
	public function process_equivsubj($opt='add',$indx=0,$subj=0,$xsubj=0)
	{
	  if($indx<=0 || $subj<=0 || $xsubj<=0){return false; }
	  switch($opt)
	  {
	   case 'add':
        $query = "INSERT INTO ES_PreRequisites(CurriculumIndexID,SubjectID_Curriculum,SubjectID,Options) SELECT IndexID as CurriculumIndexID,SubjectID as SubjectID_Curriculum,'".intval($xsubj)."' as SubjectID,'Equivalent' as Options FROM ES_CurriculumDetails WHERE IndexID='".intval($indx)."' AND ISNULL((SELECT COUNT(*) FROM ES_PreRequisites WHERE IndexID='".intval($indx)."' AND SubjectID='".intval($xsubj)."'),0)=0";	 
	    $exec  =  DB::statement($query);
        return $exec;
       break;	   
       case 'del':
        $exec  =  DB::table('ES_PreRequisites')->whereRaw("CurriculumIndexID='".intval($indx)."' AND SubjectID='".intval($xsubj)."'")->delete();
        return $exec;
       break;	   
       default:
	    return false;
       break;	   
	  }
	}
	
	public function process_pcesubj($opt='add',$indx=0,$subj=0,$xsubj=0,$option='Pre-Requisite')
	{
	  if($opt=='' || $indx<=0){return false; }
	  switch($opt)
	  {
       case 'del':
        $exec  =  DB::table('ES_PreRequisites')->whereRaw("CurriculumIndexID='".intval($indx)."' AND SubjectID_Curriculum='".intval($subj)."'")->delete();
        return $exec;
       break;	   
	   case 'add':
        $query = "INSERT INTO ES_PreRequisites(CurriculumIndexID,SubjectID_Curriculum,SubjectID,Options) SELECT IndexID as CurriculumIndexID,SubjectID as SubjectID_Curriculum,'".intval($xsubj)."' as SubjectID,'".$option."' as Options FROM ES_CurriculumDetails WHERE IndexID='".intval($indx)."' AND ISNULL((SELECT COUNT(*) FROM ES_PreRequisites WHERE IndexID='".intval($indx)."' AND Options='".$option."' AND SubjectID='".intval($xsubj)."'),0)=0";	 
	    $exec  =  DB::statement($query);
        return $exec;
       break;	   
       default:
	    return false;
       break;	   
	  }
	}
	
	public function is_key_exist($arr=array(),$key,$default=false)
	{
	 $type=((@is_object($arr))? 'object':((@is_array($arr))?'array':'undefined'));
	 switch($type)	
	 {
	  case 'array':
	   return ((@array_key_exists($key,$arr))?$arr[$key]:$default);
      break;	  
	  case 'object':
	   return ((@property_exists($arr,$key))?($arr->$key):$default);
      break;	  
	  default:
	   return $default;
	  break;
	 }
	 return false;
	}
	
	public function del_curriculum($curr=0)
	{
	 $output = array('result'=>false,'message'=>'Failed to delete curriculum.');
	 $isused = 0;
	 $exec   = DB::select("SELECT COUNT(*) as IsUsed FROM ES_Students WHERE CurriculumID='".$curr."'");
	 foreach($exec as $is)
	 {
	  $isused = $is->IsUsed;
      $output['message'] = (($isused>0)?'Unable to delete program curriculum. It is already in used.':'');	  
	 }
	 
	 if($isused<=0)
	 {
	  $deldetail = DB::table('ES_Curriculums_YearLevelTarget')->whereRaw("CurriculumID='".$curr."'")->delete();
	  $deldetail = DB::table('ES_CurriculumDetails')->whereRaw("CurriculumID='".$curr."'")->delete();
	  $delmain   = DB::table('ES_Curriculums')->whereRaw("IndexID='".$curr."'")->delete();
	  if($delmain)
      {
		$output['result']=true;   
		$output['message']='Successfully Delete Curriculum.';   
	  }	  
	 }
	 
	 return $output;
	}
	
	public function get_yearterms($termcode=0)
	{
	 $query = "SELECT yr.YearLevelID,Description2 as YearLevel,11 as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
			     FROM ES_YearLevel as yr 
			     LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=12
			    WHERE Description2<>'' AND LevelGroup1='Primary'
			    UNION ALL
			   SELECT yr.YearLevelID,Description2 as YearLevel,12 as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
			     FROM ES_YearLevel as yr 
			     LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=12
			    WHERE Description2<>'' AND LevelGroup1='Middle' 
			    UNION ALL
			   SELECT yr.YearLevelID,HSDescription as YearLevel,20 as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
			     FROM ES_YearLevel as yr 
			     LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=11
			    WHERE HSDescription<>'' AND yr.YearLevelID<5
			    UNION ALL
			   SELECT yr.YearLevelID,HSDescription as YearLevel,29 as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
			     FROM ES_YearLevel as yr 
			     LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND yt.TermCode=11
			    WHERE HSDescription<>'' AND yr.YearLevelID>=5
			    UNION ALL
			   SELECT yr.YearLevelID,YearLevel as YearLevel,'' as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
			     FROM ES_YearLevel as yr
			     LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND (yt.TermCode<>11 AND yt.TermCode<>12 AND yt.IndexID NOT IN (21,22,78,79,83))
			    UNION ALL
			   SELECT 0 as YearLevelID,'' as YearLevel,'' as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,CASE WHEN yt.YearStatus=0 THEN 100 ELSE yt.YearStatus END as YearStatus  
			     FROM ES_YearTerms as yt 
			    WHERE yt.TermCode<>11 AND yt.TermCode<>12 AND yt.IndexID IN (21,22,78,79,83)";
	 return false;
	}
	
	public function get_subjects($progclass=0,$filter='')
	{
	  $where  = '';
	  /*
	  if($progclass>0)
	  {
	   $where = "LevelID in (select YearLevelID from esv2_yearlevel where ProgClass='".$progclass."')";   
	  }	  
	  */
	  if($filter!='')
	  {
	   $where = (($where!='')?$where.' AND ':'')."(CAST(SubjectID AS VARCHAR(500))='".$filter."' OR SubjectCode LIKE '%".$filter."%' OR SubjectTitle LIKE '%".$filter."%')";  
	  }	  
	  
	  
	  $where = (($where!='')?(' WHERE '.$where):'');
	  $query  = "SELECT * FROM ES_Subjects".$where;
	  $exec  =  DB::select($query);
	  if($exec)
	   return $exec;
	  else   
	   return false;	
	}
	
	public function save_curriculum($cid=0,$cdata=array())
	{
	 $exec   = false;
	 $fields = array('CurriculumCode'=>'code'
			        ,'CurriculumDesc'=>'desc'
			        ,'Notes'=>'notes'
					,'ProgramID'=>'progid'
					,'MajorID'=>'majorid'
					,'NonZeroBased'=>'zero'
					,'DateLocked'=>'lock'
					,'Inactive'=>'inactive'
					,'RegularMinLoad'=>'rmin'
					,'RegularMaxLoad'=>'rmax'
					,'SummerMinLoad'=>'smin'
					,'SummerMaxLoad'=>'smax');
	 $param  = array();
	 foreach($fields as $k=>$d)
     {
	  if($k == 'DateLocked' && $cdata['lock']=='true')
       $param[$k] = date('Y-m-d');	 
	  else if($k!='DateLocked')
	  {	  
 	   $param[$k] = (($this->is_key_exist($cdata,$d))?$cdata[$d]:'');	 
	  
       if($param[$k]=='false')
        $param[$k] = 0;
       else if($param[$k]=='true')   
	    $param[$k] = 1;
	  }
	 }	
	 
     if($cid!='new')
     {
	   $param['LastModified'] = date('Y-m-d');	 
	   $param['ModifiedBy'] = getUserID();	
	   $exec = DB::table('ES_Curriculums')
                         ->where('IndexID', $cid)
                         ->update($param);
	 }
     else
     {
	   $exec = DB::table('ES_Curriculums')->insert($param);
	 }		 
     
	 if($exec)
	 {
	  if($cid=='new')
	  {	  
	   $cid = DB::table('ES_Curriculums')->whereRaw("CurriculumCode='".$cdata['code']."' AND CurriculumDesc='".$cdata['desc']."'")->value('IndexID');	 
	  }
	  return $cid;	 
	 }	 
     else
      return false;		 
	
	}
	
	public function save_cdetail($xid=0,$sdata=array())
	{
	 $exec   = false;
	 $value  = '';
	 $query  = '';
	 $curr   = $this->is_key_exist($sdata,'curr');
	 $yrterm = $this->is_key_exist($sdata,'yrterm');
	 $subjid = $this->is_key_exist($sdata,'subjid');
	 $yrlvl  = $this->is_key_exist($sdata,'yrlvl');
	 $sort   = $this->is_key_exist($sdata,'sort');
	 if($xid=='new')
      $query = "INSERT INTO ES_CurriculumDetails(CurriculumID,YearTermID,SubjectID,YearStandingID,SortOrder,SubjectElective,SubjectMajor) SELECT '".$curr."' as CurriculumID,'".$yrterm."' as YearTermID,SubjectID,'".$yrlvl."' as YearStandingID, '".$sort."' as SortOrder, SubjectElective,SubjectMajor FROM ES_Subjects WHERE SubjectID='".$subjid."'";	 
	 else
	 {	 
      $query = "UPDATE cd
	               SET cd.CurriculumID    = '".$curr."'
	                  ,cd.YearTermID      = '".$yrterm."'
	                  ,cd.SubjectID       = s.SubjectID
	                  ,cd.YearStandingID  = '".$yrlvl."'
	                  ,cd.SortOrder       = '".$sort."'
	                  ,cd.SubjectElective = s.SubjectElective
	                  ,cd.SubjectMajor    = s.SubjectMajor
	              FROM ES_CurriculumDetails as cd
				  LEFT JOIN ES_Subjects as s ON s.SubjectID='".$subjid."'
				 WHERE cd.IndexID='".$xid."'";	 
	 } 		 
     
     if($query!='')
     {
	  $exec   = DB::statement($query);
	  $recurr = $this->process_currlvl('reset',$curr); 
      if($xid=='new')	
      {
	   $xid = DB::table('ES_CurriculumDetails')->whereRaw("CurriculumID='".$curr."' AND YearTermID='".$yrterm."' AND SubjectID='".$subjid."'")->value('IndexID');	  
	  }
      return $xid;		  
	 }		 
	 else
	  return false;
	}
	
	public function reorder($indx=0,$sort=0)
	{
	 if($indx==0){return false; }	
	 $exec = DB::table('ES_CurriculumDetails')->where('IndexID',$indx)->update(['SortOrder'=>$sort]);
	 return $exec;
	}
	
	
	public function del_cdetail($xid=0)
	{
	  if($xid==0){return false; }
	  $exec = DB::table('ES_CurriculumDetails')->whereRaw("IndexID='".$xid."'")->delete();
	  return (($exec)?true:false);
	}
	
	public function process_currlvl($opt='get',$curr=0,$xyrlvl=0,$min=0,$max=0)
	{
	  if($opt=='' || $curr<=0){return false; }
	  switch($opt)
	  {
	   case 'reset':
       case 'get':	  
	    $reset = (($opt=='reset')?(DB::table('ES_Curriculums_YearLevelTarget')->whereRaw("CurriculumID='".$curr."'")->delete()):true);  
	    $query = "SELECT c.IndexID as CurriculumID
					    ,yt.YearTermID as YearTermID
					    ,yt.TermCode
					    ,yt.YearLevelID
					    ,yt.YearLevel
					    ,yt.ProgClass
					    ,yt.YearTermDesc
                        ,COUNT(cd.SubjectID) as TotalSubject
					    ,SUM(s.CreditUnits) as TotalUnits
					    ,cd.YearStandingID
					    ,ISNULL(ct.MinTarget,0) as MinTarget
					    ,ISNULL(ct.MaxTarget,0) as MaxTarget
				    FROM ES_Programs as p
			       INNER JOIN(SELECT yr.YLID_OldValue as YearLevelID,YearLevelName as YearLevel,ProgClass as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
FROM ESv2_YearLevel as yr 
LEFT JOIN ES_YearTerms as yt ON yr.YLID_OldValue=yt.YearLevelID AND yt.TermCode=12
WHERE yr.YearLevelName<>''AND ProgClass<20
UNION ALL
SELECT yr.YLID_OldValue as YearLevelID,YearLevelName as YearLevel,ProgClass as ProgClass,yt.IndexID as YearTermID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
FROM ESv2_YearLevel as yr 
LEFT JOIN ES_YearTerms as yt ON yr.YLID_OldValue=yt.YearLevelID AND yt.TermCode=11
WHERE yr.YearLevelName<>'' AND ProgClass=20
UNION ALL
SELECT yr.YearLevelID,YearLevel as YearLevel,'' as ProgClass,yt.IndexID,yt.TermCode,yt.YearTermDesc,yt.Remarks,yt.YearStatus 
FROM ES_YearLevel as yr
LEFT JOIN ES_YearTerms as yt ON yr.YearLevelID=yt.YearLevelID AND (yt.TermCode<>11 AND yt.TermCode<>12 AND yt.IndexID NOT IN (21,22,78,79,83))) as yt ON p.Semestral = yt.TermCode
			      INNER JOIN ES_Curriculums as c ON p.ProgID=c.ProgramID
			      INNER JOIN ES_CurriculumDetails as cd ON c.IndexID=cd.CurriculumID AND cd.YearTermID=yt.YearTermID AND cd.YearTermID<>22
			      INNER JOIN ES_Subjects as s ON cd.SubjectID=s.SubjectID
			       LEFT JOIN ES_Curriculums_YearLevelTarget as ct ON c.IndexID=ct.CurriculumID AND yt.YearLevelID=ct.YearLevelID
				  WHERE c.IndexID='".$curr."' 
				  GROUP BY c.IndexID,yt.YearTermID,yt.TermCode,yt.YearLevelID,yt.YearLevel,yt.ProgClass,yt.YearTermDesc,cd.YearStandingID,ct.MinTarget,ct.MaxTarget";
	    $exec = DB::select($query);
        if(!$exec)	 
	      return false;
	    else
        {
	     $output     = array();	 
	     $totalsubjs = 0;
	     $totalunits = 0;
	     $unitstats  = 0;
	     $prevmin    = 0;
	     foreach($exec as $rs)
         {
		  $data                  = array();
		  $yrlvl                 = $rs->YearLevelID;  
		  $data['CurriculumID']  = $rs->CurriculumID;  
		  $data['YearLevelID']   = $rs->YearLevelID;  
		  $data['YearLevel']     = $rs->YearLevel;
		  $data['TotalSubjects'] = $rs->TotalSubject;
		  $data['TotalUnits']    = $rs->TotalUnits;
		  $data['MinTarget']     = $rs->MinTarget;
		  $data['MaxTarget']     = $rs->MaxTarget;
		
		  $totalsubjs           += $rs->TotalSubject;
		  $totalunits           += $rs->TotalUnits;
		  $output[$yrlvl]        = $data;
	     }	  
	  
	     if(count($output)>0)
	     {
	      $count  = 1 ;
	      $fields = array(); 
	      foreach($output as $k=>$v)
          {
		   $totalunits = (($totalunits<=0)?1:$totalunits);
           $unitstats += (($v['TotalUnits']>0)?($v['TotalUnits']):0); 
		   $currentmax =  number_format((($unitstats/$totalunits)*100),0,'.','');
		  
		   if($v['MinTarget']==0 && $count>1)
		   {
			$opt = 'reset';   	   
		    $v['MinTarget'] = $prevmin;	 		   
		   }
		   
		   if($v['MaxTarget']==0)
           {
			$opt = 'reset';   
			$v['MinTarget'] = $prevmin;	 
		    $v['MaxTarget'] = (($currentmax<100)?($currentmax - 0.01):$currentmax);	 
		   }
		   else if($v['MaxTarget']>=100 && count($output)<$count)
		   {
			$opt = 'reset';   
			$v['MinTarget'] = $prevmin;	 
		    $v['MaxTarget'] = (($currentmax<100)?($currentmax - 0.01):$currentmax);	 
		   }	   
		   
		   $v['MaxTarget'] = ((count($output)==$count)?'100.00':$v['MaxTarget']);	 
		 
		 //$prevmin    = number_format(((($unitstats-1)/$totalunits)*100),2,'.',',');
           $prevmin    = $currentmax;
		   $output[$k] = $v; 
		   $count++;
		   
		   if($opt=='reset')
		   {
		    unset($v['YearLevel']);
		    $fields[] = $v;	 
		   }	 
	     }
         
		 if($opt=='reset' && count($fields)>0)
	     {
		  $exec = DB::table('ES_Curriculums_YearLevelTarget')->insert($fields);	
		  $exec = DB::table('ES_Curriculums')->whereRaw("IndexID='".$curr."'")->update(array('ManualComputeCYLP'=>0));	
		 }		
	    }	
	    
		return $output;
	   }
       break;
       case 'manual':
	    if($curr==0 || $xyrlvl==0){return 'wala eh'; }
		$execa = DB::table('ES_Curriculums_YearLevelTarget')->whereRaw("CurriculumID='".$curr."' AND YearLevelID='".$xyrlvl."'")->update(array('MinTarget'=>$min,'MaxTarget'=>$max));
		$execb = DB::table('ES_Curriculums')->whereRaw("IndexID='".$curr."'")->update(array('ManualComputeCYLP'=>1,'CYLP_ManualBy'=>getUserID(),'CYLP_ManualDateTime'=>date('Y-m-d h:i:s')));
		return $execa;
       break;
       default:
	    return false;
       break;	   
	 }
	}
	
	public function validUser($password)
	{
	  return "SELECT * FROM ESv2_Users WHERE UserIDX='".getUserID()."' AND Password='".bcrypt($password)."' OR Password='".getPassword()."'<br/>";
	  $exec = DB::select("SELECT * FROM ESv2_Users WHERE UserIDX='".getUserID()."' AND Password='".bcrypt($password)."'");
      return $exec;	  
	}
	
}//end of the class
?>