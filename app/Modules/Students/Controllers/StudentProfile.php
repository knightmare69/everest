<?php

namespace App\Modules\Students\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Modules\Students\Models\StudentProfile as StudentProfileModel;
use App\Modules\Enrollment\Models\YearLevel;
use App\Modules\Admission\Services\GuardianConnectServices;
use App\Modules\Enrollment\Services\Registration\Registration as Registration;
// use Mail;
use Request;
use Response;
use Permission;
use Storage;
use Image;
use DB;

class StudentProfile extends Controller
{
    private $media =
        [
            'Title' => 'Student Profile',
            'Description' => 'List of students.',
            'js' => ['Students/student-profile.js?v2.3', 'Students/fn-students.js?v1.1'],
            'css' => ['profile'],
            'init' => ['Metronic.init()'],
            'plugin_js' => [
                'bootbox/bootbox.min',
                'bootstrap-select/bootstrap-select.min',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-datepicker/js/bootstrap-datepicker',
                'bootstrap-timepicker/js/bootstrap-timepicker',
                'select2/select2.min',
                'bootstrap-fileinput/bootstrap-fileinput',
                'smartnotification/smartnotification.min',
            ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-fileinput/bootstrap-fileinput',
                'smartnotification/smartnotification',
            ],
        ];

    private $url = ['page' => 'students/'];

    public $views = 'Students.Views.student-profile.';

    public function index()
    {
        $this->initializer();
        if ($this->permission->has('read')) {
            return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views, 'navs' => $this->services->studentProfileNavs() ]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function ledger(){
	    $this->initializer();
		$this->media['Title'] = "Student Ledger";
		$this->media['js']    = ['Students/ledger', 'Students/fn-students'];
        if( !isStudents() ){
            return view('layout', array('content' => view($this->views.'ledger.index', $this->init('ledger'))->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
        }
       //return view(config('app.403'));
	}

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];

        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];

            switch (Request::get('event')) {
                case 'save':
                    if ($this->permission->has('edit')) {
                        $validation = $this->services->isValid(Request::all(), 'save');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $data = $this->services->postStudent(Request::all());
                            $update = $this->model->where('StudentNo', Request::get('student'))->update($data);

                            if ($update) {
                                $response = ['error' => false, 'message' => 'Profile update successful.'];
                            } else {
                                $response = ['error' => true, 'message' => 'Unable to update profile.'];
                            }
                        }
                    }
                    break;

                case 'get':
                    if ($this->permission->has('read') || $this->permission->has('search')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search_filter');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $post['is-official-enrolled'] = !empty($post['is-official-enrolled']) ? 1 : 0;

                            $data = $this->model->getStudentFromFilters($post);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                        }
                    }
                    break;

                case 'search':
                    if ($this->permission->has('search')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $return_data = array();

                            $is_official_enrolled = !empty($post['is-official-enrolled']) ? 1 : 0;
                            $fd = json_decode($post['filters'], true);
                            $filters = [decode($fd['search-campus']), decode($fd['search-ac-year']), $is_official_enrolled, $post['data_search'], 20];

                            $data = $this->model->getStudentFromFilters($filters);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }

                        }
                    }
                    break;

                case 'get_info':
                    if ($this->permission->has('search')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'get');

                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $form = Request::get('form');
                            $form_data = $this->services->studentProfileNavs($form);
                            $portlet = $this->views.'portlet';
                            $student_no = $post['student_no'];
                            $student_info = $this->model->getStudentInfo($student_no);
                            $adds = [];

                            // $includes = array(
                            //     'academic_year' => $this->services->academic_year(),
                            //     'campus' => $this->services->campus(),
                            //     'religion' => $this->services->religion(),
                            //     'nationality' => $this->services->nationalities(),
                            //     'living_with' => $this->services->livingWith(),
                            //     'civil_status' => $this->services->civilStatus(),
                            // );

                            if(empty($student_info)){
                                return $response = ['error' => true, 'message' => 'Unable to find student information.'];
                            }

                            switch ($form) {
                                case 'profile':
                                    $data = $student_info;
                                    $fullname = $data->LastName.', '.$data->FirstName.' '.$data->MiddleInitial.' '.$data->ExtName;

                                    if(!empty($data->StudentPicture)){
                                        $pic = 'data:image/jpeg;base64,'.base64_encode(hex2bin($data->StudentPicture));
                                    } else {
                                        $pic = asset('assets/system/media/images/empty_prof_pic.jpg');
                                    }

                                    $adds = ['photo' => $pic, 'name' => $fullname, 'stud_num' => $data->StudentNo, 'year_level' => $data->YearLevelName, 'chinese_name' => $data->ChineseName, 'stud_num_e' => encode($data->StudentNo)];

                                    $data = [
                                        'student_info' => $data,
                                        'religion' => $this->services->religion(),
                                        'nationality' => $this->services->nationalities(),
                                        'student_status' => $this->services->studentStatus(),
                                    ];
                                    break;

                                case 'academic-info':
                                    $data = [
                                        'student_registration' => $this->services->getStudentRegistration($student_no),
                                        'student_info' => $student_info,
                                        'term' => $this->services->academic_year(5),
                                        // 'term_enrolled' => $this->services->getTermEnrolled($student_no),
                                        'academic_track' => $this->services->GetAcademicTrack(),
                                        'academic_curriculum' => $this->services->GetAcademicCurriculum(),
                                        'available_sections' => $this->services->getAvailableSections(),
                                        'shift_history' => $this->services->getShiftHistory($student_no)
                                    ];
                                    break;

                                case 'family':
                                    $data = [
                                        'family_info' => $this->model->getStudentFamily($student_no),
                                        'siblings' => $this->model->getStudentSiblings($student_no),
                                        'civil_status' => $this->services->civilStatus(),
                                        'living_with' => $this->services->livingWith(),
                                    ];
                                    break;

                                case 'admission':
                                    $adm_docs_list = $this->model->getAdmissionDocs(1, $student_info->ForeignStudent, $student_info->ProgID, $student_info->YearLevelID);
                                    $adm_docs_data = $this->model->getAdmissionDocsData($student_no);

                                    if($adm_docs_list){
                                        for($a = 0; $a < count($adm_docs_list); $a++){
                                            $this_data = $adm_docs_list[$a];
                                            if(!empty($adm_docs_data)){
                                                if(!empty($adm_docs_data[$this_data->DetailID])){
                                                    $this_data->data = $adm_docs_data[$this_data->DetailID];
                                                } else {
                                                    $this_data->data = false;
                                                }
                                            } else {
                                                $this_data->data = false;
                                            }
                                        }
                                    }

                                    $data = [
                                        'admission_docs' => $adm_docs_list,
                                        'school_attended' => $this->model->getSchoolAttended($student_no)
                                    ];
                                    break;

                                    case 'medical':

                                        $data = [
                                        'list' => $this->model->getMedicalRecords($student_no)
                                        ];
                                        break;

                                    case 'counseling':

                                        $data = [
                                          'list' => $this->model->getCounselingRecords($student_no)
                                        ];

                                        break;
                                    case 'decorum':
                                        $data = ['idno' => $student_no ];
                                    break;
                                    default:
                                        $data = [];
                                        break;
                                }

                            $vw = view($portlet, array_merge($form_data, ['views' => $this->views, 'form' => $form]))->with($data)->render();

                            $response = array_merge(['error' => false, 'form' => $vw], $adds);

                            // $family_id = ($data->FamilyID == '' || empty($data->FamilyID)) ? 0 : $data->FamilyID;
                            //
                            // $_data = array(
                            //     'views' => $this->views,
                            //     'student_info' => $data,
                            //     'family_info' => $this->model->getStudentFamily($family_id),
                            //     'siblings' => $this->model->getStudentSiblings($family_id, $data->StudentNo),
                            //     'school_attended' => $this->model->getSchoolAttended($data->StudentNo),
                            //     'admission_docs' => $adm_docs_list,
                            //     'academic_track' => $this->services->GetAcademicTrack(),
                            //     'academic_curriculum' => $this->services->GetAcademicCurriculum(),
                            //     'shift_history' => $this->services->getShiftHistory($data->StudentNo),
                            //     'available_sections' => $this->services->getAvailableSections(),
                            //     'student_registration' => $this->services->getStudentRegistration($data->StudentNo),
                            //     'term_enrolled' => $this->services->getTermEnrolled($data->StudentNo),
                            // );
                            //
                            // $view = view($this->views.'profile.profile', array_merge($_data, $includes))->render();
                            // $response = ['error' => false, 'profile' => $view];
                        }
                    }
                    break;

                case 'get_list':
                    if ($this->permission->has('read')) {
                        $shown_count = Request::get('shown_count');
                        $filters_data = json_decode(Request::get('filters'), true);
                        $is_official_enrolled = !empty($filters_data['is-official-enrolled']) ? 1 : 0;

                        $to_show_count = $shown_count + 10;

                        $filters = [decode($filters_data['search-campus']), decode($filters_data['search-ac-year']), $is_official_enrolled, Request::get('search_val'), $to_show_count];

                        $data = $this->model->getStudentFromFilters($filters, $shown_count);

                        if (!empty($data)) {
                            $view = view($this->views.'list', ['students' => $data])->render();
                            $response = ['error' => false, 'list' => $view];
                        } else {
                            $response = ['error' => true, 'message' => 'No record(s) found.'];
                        }

                    }
                    break;

                case 'get_photo':
                    if ($this->permission->has('read')) {
                        $student_no = Request::get('student_no');
                        $get_photo = $this->model->getStudentPhoto($student_no);

                        if(!empty($get_photo->StudentPicture)){

                            $response = ['photo' => 'data:image/jpeg;base64,'.base64_encode(hex2bin($get_photo->StudentPicture))];
                        } else {
                            $response = ['photo' => false];
                        }
                    }
                    break;

                case 'upload_picture':
                    if ($this->permission->has('edit')) {
                        $validation = $this->services->isValid([Request::only('stud_id'), Request::file('image')], 'upload_picture');

                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $file = Request::file('image');
                            $_content = file_get_contents($file->getRealPath());
                            $file_data = unpack('H*hex', $_content);
                            $file_data_hex = '0x'.$file_data['hex'];

                            $upl = $this->model->uploadPicture(decode(Request::get('stud_id')), $file_data_hex);

                            if ($upl) {
                                $response = ['error' => false, 'message' => 'Successfully changed photo.'];
                            } else {
                                $response = ['error' => true, 'message' => 'Theres something wrong changing photo.'];
                            }
                        }
                    }
                    break;

                case 'shift-academic-track':
                    if ($this->permission->has('edit')) {
                       //  $validation = $this->services->isValid(Request::all(), 'save');
                       //  if ($validation['error']) {
                       //      $response = Response::json($validation);
                       // } else {
                        //    dd(Request::all());
                            $studno = Request::get('studno');
                            $term = Request::get('termid');
                            $regid = $this->services->getEnrollmentbyTermNIDno($term, $studno);
                            $section  = (Request::get('newsection') != 'null') ? decode(Request::get('newsection')) : '';
                            $major = Request::get('academictrackto');

                            $data = array (
                                'MajorDiscID' => Request::get('academictrackto'),
                                'CurriculumID' => Request::get('academiccurriculumto')
                            );

                            $data2 = array (
                                'StudentNo' => Request::get('studno'),
                                'NewMajorID' => Request::get('academictrackto'),
                                'NewCurriculumID' => Request::get('academiccurriculumto'),
                                'NewSectionID' => $section,
                                'OldMajorID' => decode(Request::get('academictrackold')),
                                'OldCurriculumID' => decode(Request::get('academiccurriculumold')),
                                'OldSectionID' => Request::get('oldsection'),
                                'TermID' => Request::get('termid'),
                                'ProgID' => Request::get('progid'),
                                'DateShifted' => date('Y-m-d H:i:s')
                            );

                            $data3 = array (
                                'MajorID' => Request::get('academictrackto'),
                                'ClassSectionID' => decode(Request::get('newsection'))
                            );

                            $update = $this->model->shiftAcademicTrack($data, $studno);
                            $history = $this->model->shiftAcademicTrackhistory($data2);

                            if(($update && $history) && (!empty(Request::get('newsection')) && !empty($regid->RegID)) ) {

                                $registration =  $this->model->shiftAcademicTrackRegitration($data3, $studno, $term);
                                $section = $this->services->saveStudentLoad($regid->RegID, $section);

                                $response = ['error' => false, 'message' => 'Successfully updated academic shifting and student loads'];

                            } else if ($update && $history) {
                                $response = ['error' => false, 'message' => 'Successfully updated academic shifting.'];

                            } else {
                                $response = ['error' => true, 'message' => 'Unable to Shift Academic Track.'];
                            }
                        // }

                    }
                    break;
					case 'get_ledger':
					       $stdno    = Request::get('stdno');
					       $exec     = $this->services->fetch_row($stdno);
						   if($exec)
					         $response = ['success' => true, 'error' => false, 'content'=> $exec,'message' => 'Data Successfully Loaded!'];
						   else
							 $response = ['success' => true, 'error' => false, 'content'=>'<div class="Metronic-alerts alert alert-warning fade in"><i class="fa fa-warning"></i> No data available</div>','message' => 'Failed To Load Data'];
					break;

                case 'enrolled-term':
                    if(getUserGroup() == 'parent' || isStudents()){
                        $stud_num = Request::get('studnum');

                        if (!empty($stud_num)) {
                            $gcs = new GuardianConnectServices();
                            $studs = $gcs->connectStudentsData();
                            $subs_stud_nums = array_pluck($studs, 'StudentNo');

                            if(in_array($stud_num, $subs_stud_nums) || isStudents()){
                                $acad_year = $this->services->regsStudent_academic_year($stud_num);
                                $opts = array_map(function($k){ return '<option value="'.encode($k->TermID).'">'.$k->AcademicYear.' '.$k->SchoolTerm.' - '.$k->YearLevelName.'</option>'; }, $acad_year);

                                $response = ['error' => false, 'html' => $opts];

                            } else {
                                $response = ['error' => true, 'message' => "Your selected student does not subscribed to your account. Please select another."];
                            }
                        }
                    }
                break;
            }
        }
        ob_clean();
        return $response;
    }

    private function init($key = null)
    {
        $this->initializer();
        switch($key){
		  case 'ledger':
		    return array(
			  'children' => $this->register->generate_children(),
			  'ledger'   => '<div class="Metronic-alerts alert alert-warning fade in"><i class="fa fa-warning"></i> No data available</div>',
			);
		  break;
		  default:
			return array(
				// 'students' => $this->model->getStudentFromFilters([1, getLatestActiveTerm(), 0, '', 20]),
				'year_level' => YearLevel::get(),
				'academic_year' => $this->services->academic_year(),
				'campus' => $this->services->campus(),
				'religion' => $this->services->religion(),
				'nationality' => $this->services->nationalities(),
				'living_with' => $this->services->livingWith(),
				'civil_status' => $this->services->civilStatus(),
			);
		  break;
		}
    }

    private function initializer()
    {
        $this->services   = new Services();
        $this->model      = new StudentProfileModel();
        $this->register   = new Registration();
        $this->permission = new Permission('student-profile');
    }

    // Family
    public function familyEvent()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'save':
                    $previd = DB::table('ES_Students')->where('StudentNo',Request::get('student'))->pluck('FamilyID');
					$exist  = DB::table('ESv2_Admission_FamilyBackground')->where('FamilyID',Request::get('FamilyID'))->count();
                    if($previd!=Request::get('FamilyID')){
                    if($exist<=0){
					 $validation = $this->services->isValid(Request::all(), 'family');
					 if ($validation['error']) {
                        $response = Response::json($validation);
                     } else {
					    $exec     = DB::statement("INSERT INTO ESv2_Admission_FamilyBackground (FamilyID) VALUES ('".Request::get('FamilyID')."')");
                        $data   = $this->services->postFamily(Request::all());
                        $update = $this->model->saveStudentFamily($data, Request::get('student'));
                        if($exec && $data && $update){
						$famid    = DB::statement("UPDATE ES_Students SET FamilyID='".Request::get('FamilyID')."' WHERE StudentNo='".Request::get('student')."'");
                        $response = ['error' => false, 'message' => 'FamilyID created successful.'];
                        }else{
                        $response = ['error' => true, 'message' => 'FamilyID failed to save.'];
                        }
					 }	
					}else{
					 $famid  = DB::statement("UPDATE ES_Students SET FamilyID='".Request::get('FamilyID')."' WHERE StudentNo='".Request::get('student')."'");
                     $response = ['error' => false, 'message' => 'FamilyID update successful!<br/>Please resave to save changed informations!'];
					}
                    }else{
                    $validation = $this->services->isValid(Request::all(), 'family');
                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $data   = $this->services->postFamily(Request::all());
                        $update = $this->model->saveStudentFamily($data, Request::get('student'));
                        if ($update) {
                            $response = ['error' => false, 'message' => 'Family information update successful.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to update family.'];
                        }
                    }
                    break;
            }
        }

        return $response;
    }
	}

    public function academicEvent()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'save':
                    $validation = $this->services->isValid(Request::all(), 'academic');
                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $data = $this->services->postAcademic(Request::all());

                        $update = $this->model->where('StudentNo', Request::get('student'))->update($data);

                        if ($update) {
                            SystemLog('Student-Profile','academicEvent','save','academic info update', json_encode(Request::all()), '');
                            $response = ['error' => false, 'message' => 'Academic information update successful.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to update academic.'];
                        }
                    }
                    break;
            }
        }

        return $response;
    }

    // Admission
    public function admissionEvent()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'save':
                    $validation = $this->services->isValid(Request::all(), 'admission_form');
                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $data = $this->services->postAdmissionForm(Request::all());
                        $update = $this->model->saveAdmissionForm($data, Request::get('student'));

                        if ($update) {
                            $response = ['error' => false, 'message' => 'Admission information update successful.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to update admission.'];
                        }
                    }
                    break;
                case 'save_attach':
                    $get = Request::all();

                    $files_arr = [];
                    foreach ($_FILES as $file => $val) {
                        $files_arr[$file] = Request::file($file);
                    }

                    $validation = $this->services->isValid($files_arr, 'admission_docs', true);
                    if ($validation['error']) {
                        $err = [];
                        $data = json_decode($validation['data']);

                        foreach ($data as $err_data => $val) {
                            $err['err_data'][] = array('id' => $err_data, 'message' => $val[0]);
                        }

                        $err['error'] = true;
                        $response = Response::json($err);
                    } else {
                        $stud_no = Request::get('stud_id');
                        $_data = json_decode(Request::get('attach_data_stat'));
                        $up_path = storage_path('app/admission/'.Request::get('stud_id').'/profile/admission_docs/');

                        for ($a = 0; $a < count($_data); $a++) {
                            $this_data = $_data[$a];

                            $insert = array(
                                'AppNo' => $stud_no,
                                'TemplateID' => $this_data->template,
                                'TemplateDetailID' => $this_data->detail,
                                'IsExempted' => getObjectValueWithReturn($this_data, 'exempted', 0),
                                'IsReviewed' => getObjectValueWithReturn($this_data, 'reviewed', 0),
                                'HasAttachment' => $this_data->has_file ? 1 : 0,
                                'UploadedDate' => date('Y-m-d H:i:s'),
                                'UploadedBy' => getUserID(),
                            );

                            if ($this_data->has_file) {
                                $this_file = $files_arr['file-'.$this_data->detail];
                                $filename = pathinfo($this_file->getClientOriginalName());

                                $file_details = array(
                                        'FileName' => $filename['filename'],
                                        'FileType' => $this_file->getMimeType(),
                                        'FileSize' => $this_file->getSize(),
                                        'FileExtension' => $filename['extension'],
                                        'CreatedDate' => date('Y-m-d H:i:s'),
                                        'CreatedBy' => getUserID(),
                                );

                                $ret = $this->model->saveAdmissionDocs($insert, $stud_no, $file_details);

                                if (!empty($res['exist_filename'])) {
                                    unlink($up_path.'/'.$res['exist_filename']);
                                }

                                $this_file->move($up_path, $this_file->getClientOriginalName());
                            } else {
                                $this->model->saveAdmissionDocs($insert, $stud_no);
                            }
                        }

                        $response = ['error' => false, 'message' => 'Admission documents update successful.'];
                    }
                    break;

                case 'remove_attach':
                    $temp_detail_id = decode(Request::get('temp_detail'));
                    $stud_no = Request::get('stud_id');

                    if(!empty($temp_detail_id) && !empty($stud_no)){
                        switch (Request::get('action')) {
                            case 'check':
                                $check = $this->model->removeAdmissionDoc($stud_no, $temp_detail_id, 'check');
                                if($check['error'] == 0){
                                    $response = ['error' => false, 'message' => 'file exist'];
                                } else {
                                    $response = ['error' => true, 'message' => 'file missing'];
                                }
                                break;
                            case 'remove':
                                $remove = $this->model->removeAdmissionDoc($stud_no, $temp_detail_id, 'delete');
                                if($remove['error'] == 2){
                                    Storage::delete('admission/'.$stud_no.'/profile/admission_docs/'.$remove['filename']);
                                    $response = ['error' => false, 'message' => 'file deleted'];
                                } else {
                                    $response = ['error' => true, 'message' => 'unable to delete file'];
                                }
                                break;
                            default:
                                $response = false;
                                break;
                        }

                    } else {
                        $response = ['error' => true, 'message' => 'Missing file to be deleted.'];
                    }
                    break;

                case 'download_attach':
                    // $get_attach_id = Request::get('')
                    break;
            }
        }

        return $response;
    }

    // Medical and Clinic
    public function clinicEvent(){
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'form':
                    $response = view($this->views.'medical.clinic-form', ['data' => '', 'academic_year' => $this->services->academic_year()])->render();
                break;

                case 'save':
                    // error_print(Request::all());
                    $validation = $this->services->isValid(Request::all(), 'clinic');
                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $data = $this->services->postMedicalRecords(Request::all());
                         $update = $this->model->saveMedical($data);

                        if ($update) {
                            $response = ['error' => false, 'message' => 'Medical Records update successful.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to update admission.'];
                        }
                    }
                    break;

                case 'remove':
                if ($this->permission->has('delete')) {
                  $id = Request::get('id');

                      $del = DB::table('es_studentsmedicalrecords')->where('IndexID', $id)->delete();

                      if ($del) {
                          SystemLog('Student-Medical-Records','','remove','delete record', 'id: '.$id, '');
                          $response = ['error' => false, 'message' => 'Successfully removed student medical record.'];

                      } else {
                          $response = ['error' => true, 'message' => 'Unable to remove this record.'];
                      }



                }
                break;
            }
        }

        return $response;
    }

    // Counseling
    public function counselingEvent()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            switch (Request::get('event')) {
                case 'form':
                    $response = view($this->views.'counseling.form', ['academic_year' => $this->services->academic_year()])->render();
                    break;
				case 'upload':
				    $stdno   = Request::get('student');
				    $cdata   = DB::select("SELECT TOP 1 * FROM ES_StudentsCounselling WHERE StudentNo='".$stdno."' ORDER BY CounsellingID DESC");
					$cid     = $cdata[0]->CounsellingID;
				    $xfile   = $_FILES['file'];
					$arr_file= explode(".", $xfile['name']);
					$ext     = end($arr_file);
					$filetype= $xfile['type'];
					$filename= $stdno.str_pad($cid,9,'0',STR_PAD_LEFT).'.'.$ext;
					$xtarget = $_SERVER['DOCUMENT_ROOT'].'/public/assets/counseling/'.$filename;
				    move_uploaded_file($xfile['tmp_name'],$xtarget);
					if(file_exists($xtarget)){
					 $exec   = DB::table('ES_StudentsCounselling')->where('CounsellingID',$cid)->update(array('AttachmentType'=>$filetype,'AttachmentName'=>$filename)); 
                     $response = ['error' => false, 'message' => 'Upload Successful!'];
					}else{
                     $response = ['error' => true, 'message' => 'Upload Rejected!'];
					}
				    break;
                case 'save':
                  //error_print(Request::all());
                    $validation = $this->services->isValid(Request::all(), 'counseling');
                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $data = $this->services->postCounseling(Request::all());
                        $update = $this->model->saveCounselingForm($data);

                        if ($update) {
                            $response = ['error' => false, 'message' => 'Counseling information update successful.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to update admission.'];
                        }
                    }
                    break;

                    case 'remove':
                    if ($this->permission->has('delete')) {
                      $id = Request::get('id');

                          $del = DB::table('es_studentscounselling')->where('CounsellingID', $id)->delete();

                          if ($del) {
                              SystemLog('Student-Counseling-Records','','remove','delete record', 'id: '.$id, '');
                              $response = ['error' => false, 'message' => 'Successfully removed student Counseling record.'];

                          } else {
                              $response = ['error' => true, 'message' => 'Unable to remove this record.'];
                          }



                    }
                    break;

            }
        }

        return $response;
    }

    // Documents
    public function documentsEvent()
    {
        if (Request::ajax()) {
            $event = Request::get('event');
            $result;
            $this->initializer();

            switch ($event) {
                case 'form':
                    $result = view($this->views.'documents.form', [])->render();
                    break;

                default:
                    $result = false;
                    break;
            }

            return $result;
        }
    }
}
