<?php

namespace App\Modules\Students\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Modules\Students\Models\StudentProfile as StudentProfileModel;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As grade_model; 

// use Mail;
use Request;
use Response;
use Permission;
use Storage;
use Image;

use DB;

class StudentGrades extends Controller
{
    private $media =
        [
            'Title' => 'Student Grades',
            'Description' => 'List of students.',
            'js' => ['Students/grades'],
            'init' => ['ME.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'select2/select2.min',
                            'bootstrap-fileinput/bootstrap-fileinput',
                ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-fileinput/bootstrap-fileinput',
            ],
        ];

    private $url = ['page' => 'students/grades/'];

    public $views = 'Students.Views.grades.';

    public function index()
    {
        $this->initializer();        
        return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));        
    }
    
    public function survey(){
        $response = ['error' => true, 'message' => 'No Event Selected'];
    }
    
    
    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            $post = Request::all();
            
            switch (Request::get('event')) {
                case 'save':
                    if ($this->permission->has('save')) {
                        $validation = $this->services->isValid(Request::all(), 'save');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $data = $this->services->postStudent(Request::all());
                            $update = $this->model->where('StudentNo', Request::get('student'))->update($data);

                            if ($update) {
                                $response = ['error' => false, 'message' => 'Profile update successful.'];
                            } else {
                                $response = ['error' => true, 'message' => 'Unable to update profile.'];
                            }
                        }
                    }
                    break;

                case 'get':
                    if ($this->permission->has('read')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search_filter');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $is_official_enrolled = !empty($post['is-official-enrolled']) ? 1 : 0;

                            $filters = [decode($post['search-campus']), decode($post['search-ac-year']), $is_official_enrolled, '', 20];

                            $data = $this->model->getStudentFromFilters($filters);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                        }
                    }
                    break;

                case 'search':
                    if ($this->permission->has('search')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $return_data = array();

                            $is_official_enrolled = !empty($post['is-official-enrolled']) ? 1 : 0;
                            $fd = json_decode($post['filters'], true);
                            $filters = [decode($fd['search-campus']), decode($fd['search-ac-year']), $is_official_enrolled, $post['data_search'], 20];

                            $data = $this->model->getStudentFromFilters($filters);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }

                        }
                    }
                    break;

                case 'get_profile':
                    if ($this->permission->has('read')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'get');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $includes = array(
                                'academic_year' => $this->services->academic_year(),
                                'campus' => $this->services->campus(),
                                'religion' => $this->services->religion(),
                                'nationality' => $this->services->nationalities(),
                                'living_with' => $this->services->livingWith(),
                                'civil_status' => $this->services->civilStatus(),
                            );

                            $data = $this->model->getStudentInfo($post['student_no']);
                            $data->Age = $this->services->getAge($data->DateAdmitted, $data->DateOfBirth);

                            $adm_docs_list = $this->model->getAdmissionDocs(1, $data->ForeignStudent, $data->ProgID, $data->YearLevelID);
                            $adm_docs_data = $this->model->getAdmissionDocsData($data->StudentNo);

                            if($adm_docs_list){
                                for($a = 0; $a < count($adm_docs_list); $a++){
                                    $this_data = $adm_docs_list[$a];
                                    if(!empty($adm_docs_data)){
                                        if(!empty($adm_docs_data[$this_data->DetailID])){
                                            $this_data->data = $adm_docs_data[$this_data->DetailID];
                                        } else {
                                            $this_data->data = false;
                                        }
                                    } else {
                                        $this_data->data = false;
                                    }
                                }
                            }

                            $family_id = ($data->FamilyID == '' || empty($data->FamilyID)) ? 0 : $data->FamilyID;

                            $_data = array(
                                'views' => $this->views,
                                'student_info' => $data,
                                'family_info' => $this->model->getStudentFamily($family_id),
                                'siblings' => $this->model->getStudentSiblings($family_id, $data->StudentNo),
                                'school_attended' => $this->model->getSchoolAttended($data->StudentNo),
                                'admission_docs' => $adm_docs_list,
                            );

                            $view = view($this->views.'profile.profile', array_merge($_data, $includes))->render();
                            $response = ['error' => false, 'profile' => $view];
                        }
                    }
                    break;

                case 'grades':
                    $student_no = (getUserGroup() == 'parent') ? $post['studnum'] : getUserID2();

                    $filters = [ 'TermID' => decode($post['term']), 'StudentNo' => $student_no,];
                    $r = DB::table('es_permanentrecord_master')->selectRaw("*, dbo.fn_AcademicYearTerm(TermID) As AyTerm")->where($filters)->first();
                    $data = $this->grade->gradeView3(decode($post['term']),$student_no)->get();
                    if(!empty($data)){
                        $view = view($this->views.'table', ['term' => decode($post['term']), 'stud' => $student_no, 'grades' => $data, 'views'=> $this->views, 'reg'=> $r ])->render();
                        $response = ['error' => false, 'list' => $view,  ];
                    } else {
					    $tmpinfo= DB::SELECT("SELECT RegID,y.YearLevelID,y.YearLevelCode,y.YearLevelName 
												FROM ES_Registrations as r
										  INNER JOIN ESv2_YearLevel as y ON r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID  
											   WHERE r.TermID='".decode($post['term'])."' AND r.StudentNo='".$student_no."'");
						if($tmpinfo && count($tmpinfo)>0){
						   $regid = $tmpinfo[0]->RegID;
						   $yrlvl = $tmpinfo[0]->YearLevelID;						   
						   $link = url('assets\kinder\default.pdf').'?t='.date('Ymdis');
						   $content = '<div class="row"><div class="col-sm-12">';
						   $content.= '<div data-jay="{{ $progclass}}" data-lynesp="{{$period}}"> 
											<object type="text/html" data="'.$link.'#view=FitH&toolbar=0" width="98%" height="600px" style="overflow:auto;border:1px ridge black"></object>
									   </div>';
						   $content.= '</div></div>';	
						   $response = ['error' => false, 'list' => $content];
						}else{
						   $response = ['error' => true, 'message' => 'No record(s) found.'];
						}
                    }
                    SystemLog('Student-Portal','','Viewing of Grades','report card','Term: ' . decode($post['term']) . ' StudentNo: ' . getUserID2()  ,'' );
                    break;

                case 'get_photo':
                    $student_no = Request::get('student_no');
                    $get_photo = $this->model->getStudentPhoto($student_no);

                    if(!empty($get_photo->StudentPicture)){

                        $response = ['photo' => 'data:image/jpeg;base64,'.base64_encode(hex2bin($get_photo->StudentPicture))];
                    } else {
                        $response = ['photo' => false];
                    }
                    break;

                case 'upload_picture':
                    $validation = $this->services->isValid([Request::only('stud_id'), Request::file('image')], 'upload_picture');

                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $file = Request::file('image');
                        $_content = file_get_contents($file->getRealPath());
                        $file_data = unpack('H*hex', $_content);
                        $file_data_hex = '0x'.$file_data['hex'];

                        $upl = $this->model->uploadPicture(decode(Request::get('stud_id')), $file_data_hex);

                        if ($upl) {
                            $response = ['error' => false, 'message' => 'Successfully changed photo.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Theres something wrong changing photo.'];
                        }
                    }
                    break;
            }
        }
        ob_clean();
        return $response;
    }

    public function surveyEvent()
    {
        // $response = ['error' => true, 'message' => 'No Event Selected'];
        // if (Request::ajax()) {
            $this->initializer();
        //     $response = ['error' => true, 'message' => 'Permission Denied!'];

        //     if ($this->permission->has('read')) {
                // error_print(Request::all());
                // die();
                $term = decode(Request::get('term'));
                $period = Request::get('period');
                $req_registrar_rep = !empty(Request::get('gradecopy')) ? Request::get('gradecopy') : '';
                $req_conference = !empty(Request::get('sched')) ? Request::get('sched') : '';

                // $validation = $this->services->isValid(Request::all(), 'save');
                // if ($validation['error']) {
                //     $response = Response::json($validation);
                // } else {
                    // $data = $this->services->postStudent(Request::all());
                    $data = [
                        'TermID' => $term,
                        'StudentNo' => getUserID2(),
                        'RequestDate' => date('Y-m-d H:i:s'),
                        'PeriodID' => $period
                    ];

                    $save = $this->model->saveGS_Request($data, $req_registrar_rep, $req_conference);

                    if ($save['req_reg'] == 'duplicate' && $save['req_conf'] == 'duplicate') {
                        $response = ['error' => false, 'message' => 'You have already sent both request.'];
                    } else if($save['req_reg'] == 'save' && $save['req_conf'] == 'duplicate') {
                        $response = ['error' => false, 'message' => "Successfully sent request for official report card.<br>You've already sent a conference request."];
                    } else if($save['req_reg'] == 'duplicate' && $save['req_conf'] == 'saved') {
                         $response = ['error' => false, 'message' => "Already sent request for official report card.<br>Successfully sent a conference request."];
                    } else if($save['req_reg'] == 'saved'){
                        $response = ['error' => false, 'message' => 'Successfully sent request for official report card.'];
                    } else if($save['req_reg'] == 'duplicate'){
                        $response = ['error' => true, 'message' => 'Already sent request for official report card'];
                    }  else if($save['req_conf'] == 'saved'){
                        $response = ['error' => false, 'message' => 'Successfully sent request for conference.'];
                    } else if($save['req_conf'] == 'duplicate'){
                        $response = ['error' => true, 'message' => 'Already sent request for conference.'];
                    } else {
                        $response = ['error' => true, 'message' => 'Unable to proceed.'];
                    }
                // }
            // }
                    // break;
        // }
        
        return $response;
    }

    private function init($key = null)
    {
        $this->initializer();
        $stud  = getUserID2();
        
        return array(
            'students' => $this->model->getStudentFromFilters([1, getLatestActiveTerm(), 0, '', 20]),
            'academic_year' => $this->services->academic_yearby_student($stud),
            'campus' => $this->services->campus(),
            'religion' => $this->services->religion(),
            'nationality' => $this->services->nationalities(),
            'living_with' => $this->services->livingWith(),
            'civil_status' => $this->services->civilStatus(),
        );
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new StudentProfileModel();
        $this->grade = new grade_model();
        $this->permission = new Permission('student-profile');
    }  
}