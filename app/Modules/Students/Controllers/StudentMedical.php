<?php

namespace App\Modules\Students\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Modules\Students\Models\StudentProfile as StudentProfileModel;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As grade_model;

// use Mail;
use Request;
use Response;
use Permission;
use Storage;
use Image;
use DB;

class StudentMedical extends Controller
{
    private $media =
        [
            'Title' => 'Student Medical',
            'Description' => 'List of students.',
            'js' => ['Students/medical.js'],
            'init' => ['ME.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'select2/select2.min',
                            'bootstrap-fileinput/bootstrap-fileinput',
                ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-fileinput/bootstrap-fileinput',
            ],
        ];

    private $url = ['page' => 'students/medical-student/'];

    public $views = 'Students.Views.medical.';

    public function index()
    {
        $this->initializer();
        return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
    }




    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            $post = Request::all();

            switch (Request::get('event')) {





                case 'medical':

                    $student_no = (getUserGroup() == 'parent') ? $post['studnum'] : getUserID2();

                    $get = DB::table('es_studentsmedicalrecords')->select('*')->where('StudentNo', $student_no)->get();

                    $data = $get;


                        $view = view($this->views.'table', ['stud' => $student_no, 'data' => $data, 'views'=> $this->views])->render();
                        $response = ['error' => false, 'list' => $view, 'data'=> $data ];



                    SystemLog('Student-Portal','','Viewing of AMedical Records','student attendance','Term: ' .  $student_no . ' StudentNo: ' . $student_no  ,'' );
                    break;




            }
        }
        ob_clean();
        return $response;
    }




    private function init($key = null)
    {
        $this->initializer();
        $stud  = getUserID2();

        return array(
            // 'students' => $this->model->getStudentFromFilters([1, getLatestActiveTerm(), 0, '', 20]),
            'academic_year' => $this->services->academic_yearby_student($stud),

        );
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new StudentProfileModel();
        $this->grade = new grade_model();
        $this->permission = new Permission('student-profile');
    }
}
