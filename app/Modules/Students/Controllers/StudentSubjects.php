<?php

namespace App\Modules\Students\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Cashier\Services\CashierServices as tServices;

// use Mail;
use Request;
use Response;
use Permission;
use Storage;
use Image;
use DB;
class StudentSubjects extends Controller
{
    private $media =
        [
            'Title' => 'Subjects Enrolled',
            'Description' => 'List of student subjects.',
            'js' => ['Students/subjects.js?v=1.0.0'],
            'init' => ['MOD.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'select2/select2.min',
                            'bootstrap-fileinput/bootstrap-fileinput',
                ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-fileinput/bootstrap-fileinput',
            ],
        ];

    private $url = ['page' => 'students/subjects/'];

    public $views = 'Students.Views.subjects.';

    public function index()
    {
        $this->initializer();
        return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            $post = Request::all();

            switch (Request::get('event')) {
                case 'get-subjects':

                    $reg = (Request::get('reg'));

               	    $data = DB::table('es_registrationdetails as rd')
                            ->leftJoin('es_classschedules as cs','rd.ScheduleID','=','cs.ScheduleID')
                            ->leftJoin('es_classsections as sec','cs.SectionID','=','sec.SectionID')
                            ->leftJoin('es_subjects as s','cs.SubjectID','=','s.SubjectID')
                            ->leftJoin('es_registrations as r','rd.RegID','=','r.RegID')
                            ->selectRaw("rd.ReferenceID AS ReferenceID,rd.RegID AS RegID,
                            	rd.RegTagID AS RegTagID,
                            	rd.RefNo AS RefNo,
                            	rd.CSTargetScheduleID AS CSTargetScheduleID,
                            	rd.WithdrawnSchedID AS WithdrawnSchedID,
                            	cs.ScheduleID AS ScheduleID,
                            	cs.SectionID AS SectionID,
                            	sec.SectionName AS SectionName,
                            	s.SubjectID AS SubjectID,
                            	s.SubjectCode AS SubjectCode,
                            	s.SubjectTitle AS SubjectTitle,
                            	s.LabUnits AS LabUnits,
                            	s.AcadUnits AS AcadUnits,
                            	s.CreditUnits AS CreditUnit,
                            	sec.ProgramID AS ProgramID,
                            	sec.CollegeID AS CollegeID,
                            	r.ProgID AS RegProgID,
                            	cs.Sched_1 AS Sched_1 ")
                                ->where('r.RegID', $reg)
                                ->get();

                    $vw  = view('Accounting.Views.assessment.subjects', ['subj' => $data ])->render();

                    $response = Response::json(['success' => true,'data' => $vw,'error'   => false,'message' => '']);

                break;
            }
        }
        return $response;
    }

    private function init($key = null)
    {
        $this->initializer();
        $stud  = getUserID2();
        
        $term = tServices::getAssessment($stud,'1');
        return array(
            // 'students' => $this->model->getStudentFromFilters([1, getLatestActiveTerm(), 0, '', 20]),
            //'academic_year' => $this->services->academic_yearby_student($stud),
            'term' => $term
        );
    }
    
    private function initializer()
    {
        $this->permission = new Permission('student-profile');
    }
}
