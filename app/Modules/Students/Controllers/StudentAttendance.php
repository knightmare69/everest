<?php

namespace App\Modules\Students\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Modules\Students\Models\StudentProfile as StudentProfileModel;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As grade_model;

// use Mail;
use Request;
use Response;
use Permission;
use Storage;
use Image;
use DB;

class StudentAttendance extends Controller
{
    private $media =
        [
            'Title' => 'Student Attendance',
            'Description' => 'List of students.',
            'js' => ['Students/attendance.js'],
            'init' => ['ME.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'select2/select2.min',
                            'bootstrap-fileinput/bootstrap-fileinput',
                ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-fileinput/bootstrap-fileinput',
            ],
        ];

    private $url = ['page' => 'students/attendance/'];

    public $views = 'Students.Views.attendance.';

    public function index()
    {
        $this->initializer();
        return view('layout', array('content' => view($this->views.'index', $this->init())->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
    }




    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            $post = Request::all();

            switch (Request::get('event')) {





                case 'attendance':

                    $student_no = (getUserGroup() == 'parent') ? $post['studnum'] : getUserID2();
                    $term = decode($post['term']);

                    $filters = [ 'TermID' => decode($post['term']), 'StudentNo' => $student_no,];
                    $data = $this->getProfileData($student_no, $term);

                    if (!empty($data)) {
                        $view = view($this->views.'table', ['term' => decode($post['term']), 'stud' => $student_no, 'data' => $data, 'views'=> $this->views])->render();
                        $response = ['error' => false, 'list' => $view, 'data'=> $data ];
                    } else {
                        $response = ['error' => true, 'message' => 'No record(s) found.'];
                    }
                    SystemLog('Student-Portal','','Viewing of Attendance and Discipline','student attendance','Term: ' . decode($post['term']) . ' StudentNo: ' . $student_no  ,'' );
                    break;




            }
        }
        ob_clean();
        return $response;
    }


    private function getProfileData($studno, $term)
    {
      $StudentNo = $studno;
      $data['Profile'] =
        DB::table('es_students')
          ->select([
              DB::raw("CONCAT(LastName,', ',FirstName,' ',MiddleInitial) as Name"),
              DB::raw("CONVERT(DateofBirth,DATE) as DateofBirth"),
              'StudentNo','Gender',
            ])
          ->where('StudentNo',$StudentNo)
          ->first();

      $data['Absenses'] =
        DB::table('ES_GS_MeritDemerits as a')
          ->select([
              'Days',
              DB::raw('fldPK as AttendanceKey'),'PeriodID','IsExcused','Demerit','Remarks',
              // DB::raw("DATEDIFF(day,Date,DateEnd) as Days"),
              DB::raw("convert(Date,char(11)) as Date"),
              DB::raw("convert(DateEnd,char(11)) as DateEnd"),

            ])
          ->where("StudentNo",$StudentNo)
          ->where('Type','1')
          ->where('TermID',$term)
          // ->where('SectionID',Request::get("Section"))
          ->get();

      $data['Tardiness'] =
        DB::table('ES_GS_MeritDemerits as a')
          ->select([
              'Days',
              DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','Merit','Demerit','Remarks',
              DB::raw("convert(Date,char(11)) as Date"),
              DB::raw("convert(DateEnd,char(11)) as DateEnd"),

            ])
          ->where("StudentNo",$StudentNo)
          ->where('Type','2')
          ->where('TermID',$term)
          // ->where('SectionID',Request::get("Section"))
          ->get();

      $data['Merits'] =
        DB::table('ES_GS_MeritDemerits as a')
          ->select([
              DB::raw('fldPK as AttendanceKey'),'PeriodID','Merit','Remarks',
              DB::raw("convert(Date,char(11)) as Date"),
            ])
          ->where("StudentNo",$StudentNo)
          ->where('Type','3')
          ->where('TermID',$term)
          // ->where('SectionID',Request::get("Section"))
          ->get();

      $data['LostFound'] =
        DB::table('ES_GS_MeritDemerits as a')
          ->select([
              DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','Merit','Demerit','Remarks',
              DB::raw("convert(Date,char(11)) as Date"),
            ])
          ->where("StudentNo",$StudentNo)
          ->where('Type','4')
          ->where('TermID',$term)
          // ->where('SectionID',Request::get("Section"))
          ->get();

      $data['Offenses'] =
        DB::table('ES_GS_MeritDemerits as a')
          ->select([
              DB::raw('fldPK as AttendanceKey'),'PeriodID','Particulars','IsExcused','Days','Demerit','Remarks',
              DB::raw("convert(Date,char(11)) as Date"),
              DB::raw("convert(DateEnd,char(11)) as DateEnd"),
            ])
          ->where("StudentNo",$StudentNo)
          ->where('Type','5')
          ->where('TermID',$term)
          // ->where('SectionID',Request::get("Section"))
          ->get();

      return $data;
    }

    private function init($key = null)
    {
        $this->initializer();
        $stud  = getUserID2();

        return array(
            // 'students' => $this->model->getStudentFromFilters([1, getLatestActiveTerm(), 0, '', 20]),
            'academic_year' => $this->services->academic_yearby_student($stud),
        );
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new StudentProfileModel();
        $this->grade = new grade_model();
        $this->permission = new Permission('student-profile');
    }
}
