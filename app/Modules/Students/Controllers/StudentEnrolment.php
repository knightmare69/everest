<?php

namespace App\Modules\Students\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Modules\Students\Models\StudentProfile as StudentProfileModel;
use App\Modules\ClassRecords\Models\ReportCardDetails_model As grade_model;
use App\Libraries\CrystalReport as xpdf;


// use Mail;
use Request;
use Response;
use Permission;
use Storage;
use Image;
use DB;

class StudentEnrolment extends Controller
{
    private $media =
        [
            'Title' => 'Student Enrolment',
            'Description' => 'Online Enrollment',
            'js' => ['Students/enrolment'],
            'init' => ['ME.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'bootstrap-select/bootstrap-select.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'bootstrap-datepicker/js/bootstrap-datepicker',
                            'select2/select2.min',
                            'bootstrap-fileinput/bootstrap-fileinput',
                            'bootstrap-wizard/jquery.bootstrap.wizard.min',
                            'SmartNotification/SmartNotification.min',
                            'jquery-validation/js/jquery.validate.min',

                ],
            'plugin_css' => [
                'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-fileinput/bootstrap-fileinput',
                'SmartNotification/SmartNotification'
            ],
        ];

    private $url = ['page' => 'students/enrollment/'];

    public $views = 'Students.Views.enrolment.';

    public function index()
    {
        $this->initializer();
        return view('layout', array('content' => view($this->views.'index' )->with(['views' => $this->views,'section'=>0]), 'url' => $this->url, 'media' => $this->media));
    }

    public function print_data()
    {
        $this->initializer();
        $this->xpdf = new xpdf;

        $key = decode(Request::get('reg'));
        
        if($key == '0'){
            return view(config('app.404'));
            die();
        }
        
        $reg = $this->services->getEnrollment($key);


        $school = env('APP_TITLE','COMPANY ABC');
        $address = 'Alabang Hills Village, Alabang, Muntinlupa City';

        $subreport1 = array(
            'file' => 'dsr_subjects',
            'query' => "EXEC ES_rptRegAssessment_EnrolledSubjects '".$reg->RegID."'",
        );

        /*
        $subreport2 = array(
            'file' => 'AssessedFees',
            'query' => "EXEC ES_rptRegAssessment_Accounts '".$reg->RegID."','0'",
        );
        */
        $subreport2 = array(
            'file' => 'AssessedFees',
            'query' => "EXEC ES_GetScheduleOfFees_r3 '".$reg->CampusID."', '".$reg->TermID."', '".$reg->ProgID."','2','".$reg->RegID."'", 
        );
        
        $subreport3 = array(
            'file' => 'Company_Logo',
            'query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'"
        );
 		                            
        //$this->xpdf->filename = 'preregistration.rpt';
        $this->xpdf->filename = 'registration_form2.rpt';

        #EXEC dbo.ES_rptRegAssessment '80396', 'San Beda College Alabang', '', 'Alabang Hills Village, Alabang, Muntinlupa City', 'Alabang Campus', 'admin',  'STUDENT/CASHIER'

        //$this->xpdf->query = "EXEC ES_rptRegAssessment '".$reg->RegID."','".$school."','".$address."','','".$reg->CampusID."','admin','STUDENT/CASHIER'";
        $this->xpdf->query="EXEC ES_GetStudentRegistrationGSHS 'San Beda College Alabang', 'Alabang Hills Village, Alabang, Muntinlupa City', '',  '".$reg->CampusID."', '".$reg->TermID."', '".$reg->ProgID."', '2','".$reg->RegID."', 'admin', '1', '0', '0', '0'";

        $result = DB::insert("INSERT INTO ES_RegistrationCopy(StudentNo,RegID,CopyID,CopyName) SELECT StudentNo,RegID,1,'Student Copy' FROM ES_Registrations WHERE RegID = '".$reg->RegID."' AND RegID NOT IN (SELECT RegID FROM ES_RegistrationCopy)",[]);

        $subrpt = array(
            'subreport1' => $subreport1,
            'subreport2' => $subreport2,
        //    'subreport3' => $subreport3 
        );

            
		                  
        $this->xpdf->SubReport = $subrpt;
        $data = $this->xpdf->generate();

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="pre-registration.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        header('Page Title: Pre-registration form');
        @readfile($data['file']);
        die();
    }


    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];
            $post = Request::all();

            switch (Request::get('event')) {
                case 'enrolment':
                    //if ($this->permission->has('save')) {
                        //$validation = $this->services->isValid(Request::all(), 'save');
                        //if ($validation['error']) {
                         //   $response = Response::json($validation);
                        //} else {
                            $update = $this->services->saveEnrolment(Request::all());
                            //$update = $this->model->where('StudentNo', Request::get('student'))->update($data);

                            if ($update) {
                                $response = ['error' => false, 'message' => 'Successfully saved registration','reg'=>$update];                                
                            } else {
                                $response = ['error' => true, 'message' => 'Unable to save registration. Either Class Section is full, missing schedule found or your enrollment already validated.'];
                            }
                        //}
                    //}
                    break;

                case 'get':
                    if ($this->permission->has('read')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search_filter');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $is_official_enrolled = !empty($post['is-official-enrolled']) ? 1 : 0;

                            $filters = [decode($post['search-campus']), decode($post['search-ac-year']), $is_official_enrolled, '', 20];

                            $data = $this->model->getStudentFromFilters($filters);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                        }
                    }
                    break;

                case 'search':
                    if ($this->permission->has('search')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $return_data = array();

                            $is_official_enrolled = !empty($post['is-official-enrolled']) ? 1 : 0;
                            $fd = json_decode($post['filters'], true);
                            $filters = [decode($fd['search-campus']), decode($fd['search-ac-year']), $is_official_enrolled, $post['data_search'], 20];

                            $data = $this->model->getStudentFromFilters($filters);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }

                        }
                    }
                    break;

                case 'get_profile':
                    if ($this->permission->has('read')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'get');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $includes = array(
                                'academic_year' => $this->services->academic_year(),
                                'campus' => $this->services->campus(),
                                'religion' => $this->services->religion(),
                                'nationality' => $this->services->nationalities(),
                                'living_with' => $this->services->livingWith(),
                                'civil_status' => $this->services->civilStatus(),
                            );

                            $data = $this->model->getStudentInfo($post['student_no']);
                            $data->Age = $this->services->getAge($data->DateAdmitted, $data->DateOfBirth);

                            $adm_docs_list = $this->model->getAdmissionDocs(1, $data->ForeignStudent, $data->ProgID, $data->YearLevelID);
                            $adm_docs_data = $this->model->getAdmissionDocsData($data->StudentNo);

                            if($adm_docs_list){
                                for($a = 0; $a < count($adm_docs_list); $a++){
                                    $this_data = $adm_docs_list[$a];
                                    if(!empty($adm_docs_data)){
                                        if(!empty($adm_docs_data[$this_data->DetailID])){
                                            $this_data->data = $adm_docs_data[$this_data->DetailID];
                                        } else {
                                            $this_data->data = false;
                                        }
                                    } else {
                                        $this_data->data = false;
                                    }
                                }
                            }

                            $family_id = ($data->FamilyID == '' || empty($data->FamilyID)) ? 0 : $data->FamilyID;
                            $_data = array(
                                'views' => $this->views,
                                'student_info' => $data,
                                'family_info' => $this->model->getStudentFamily($family_id),
                                'siblings' => $this->model->getStudentSiblings($family_id, $data->StudentNo),
                                'school_attended' => $this->model->getSchoolAttended($data->StudentNo),
                                'admission_docs' => $adm_docs_list,
                            );

                            $view = view($this->views.'profile.profile', array_merge($_data, $includes))->render();
                            $response = ['error' => false, 'profile' => $view];
                        }
                    }
                    break;

                case 'schedules':

                    $section =  decode($post['section']);
                    $view = view($this->views.'advising.subjects', ['section' => $section])->render();
                    $response = ['error' => false, 'list' => $view,  ];

                    break;

                case 'get_photo':
                    $student_no = Request::get('student_no');
                    $get_photo = $this->model->getStudentPhoto($student_no);

                    if(!empty($get_photo->StudentPicture)){

                        $response = ['photo' => 'data:image/jpeg;base64,'.base64_encode(hex2bin($get_photo->StudentPicture))];
                    } else {
                        $response = ['photo' => false];
                    }
                    break;

                case 'upload_picture':
                    $validation = $this->services->isValid([Request::only('stud_id'), Request::file('image')], 'upload_picture');

                    if ($validation['error']) {
                        $response = Response::json($validation);
                    } else {
                        $file = Request::file('image');
                        $_content = file_get_contents($file->getRealPath());
                        $file_data = unpack('H*hex', $_content);
                        $file_data_hex = '0x'.$file_data['hex'];

                        $upl = $this->model->uploadPicture(decode(Request::get('stud_id')), $file_data_hex);

                        if ($upl) {
                            $response = ['error' => false, 'message' => 'Successfully changed photo.'];
                        } else {
                            $response = ['error' => true, 'message' => 'Theres something wrong changing photo.'];
                        }
                    }
                    break;
            }
        }
        ob_clean();
        return $response;
    }

    private function init($key = null)
    {
        $this->initializer();
        $stud  = getUserID2();
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new StudentProfileModel();
        $this->grade = new grade_model();
        $this->permission = new Permission('student-profile');
    }
}