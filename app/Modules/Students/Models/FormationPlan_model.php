<?php 

namespace App\Modules\Students\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class FormationPlan_model extends Model {

	protected $table='es_studentformationplan';
	public $primaryKey ='RefID';

	protected $fillable  = array(
    	                       
          'StudentNo'
          ,'MentorID'
          ,'Easy'
          ,'Hard'
          ,'Future'
          ,'SpiritualStudent'
          ,'SpiritualMentor'
          ,'HumanStudent'
          ,'HumanMentor'
          ,'IntellectualStudent'
          ,'IntellectualMentor'
          ,'ApostolicStudent'
          ,'ApostolicMentor'
          ,'OtherStudent'
          ,'OtherMentor'
          ,'UserID'
          ,'DateCreated'
          ,'Modifiedby'
          ,'DateModified'
	);

	public $timestamps = false;

   
    
    
}