<?php

namespace App\Modules\Students\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class StudentProfile extends Model
{
    protected $table = 'ES_Students';
    protected $table_family = 'ESv2_Admission_FamilyBackground';
    protected $table_medicalrecords = 'es_studentsmedicalrecords';
    // protected $table_admission = 'ESv2_Admission';
    protected $table_counseling = 'ES_StudentsCounselling';
    protected $table_adm_docs = 'ESv2_Admission_RequiredDocs';
    // private $view = 'vw_K12_Admission';
    protected $primaryKey = 'StudentNo';

    protected $table_gs_student_req = 'ES_GS_StudentRequests';

    protected $fillable = array(
        'StudentNo',
        'FirstName',
        'Middlename',
        'MiddleInitial',
        'LastName',
        'ExtName',
        'Fullname',
        'PlaceOfBirth',
        'DateOfBirth',
        'ChineseName',
        'Gender',
        'TermID',
        'YearLevelID',
        'ProgID',
        'MajorDiscID',
        'CurriculumID',
        'CampusID',
        'LRN',
        'StatusID',
        'OtherStatusID',
        'DateAdmitted',
        'StudentPicture',
        'LRN',
        'Elem_School',
        'Elem_Address',
        'Elem_InclDates',
        'HS_School',
        'HS_Address',
        'HS_InclDates',
		'CampusID',
        'ProgID',
        'ReligionID',
        'NationalityID',
        'Res_Address',
        'Res_Street',
        'Res_Barangay',
        'Res_TownCity',
        'Res_Province',
        'StudPreNumbered',
        'Note',
        'Email',
        'TelNo',
        'PassportNumber',
        'SSP_Number',
        'VisaStatus',
        'Validity_of_Stay'
    );

    public $timestamps = false;

    public function getStudentFromFilters($filters, $orig_total_count = 0)
    {
        // $prog_id = decode($filters['search-prog']);
        // $year_level_id = decode($filters['search-level']);
        // $term_id = decode($filters['search-ac-year']);
		$filters['student-search-text'] = ((array_key_exists('student-search-text',$filters))?$filters['student-search-text'] : ''); 
        $search_text = $filters['student-search-text'];
        $allowed_programs = getUserProgramAccess();

        // $get = DB::select('exec sp_K12_StudentLists ?, ?, ?, ?, ?', $filters);

        // if($prog_id == 29){
        //     if ($year_level_id == 5 || $year_level_id == 1) {
        //         $yl = [5,1];
        //     } else if ($year_level_id == 6 || $year_level_id == 2) {
        //         $yl = [6,2];
        //     }
        // } else {
        //     $yl = [$year_level_id];
        // }

        try {
            $g = DB::table($this->table.' as S')->select('S.StudentNo', 'S.Fullname', 'S.FirstName', 'S.Middlename', 'S.LastName', 'S.MiddleInitial', 'S.ExtName', 'S.Gender', 'S.DateOfBirth', DB::raw("dbo.fn_Age(S.DateOfBirth, GETDATE()) AS Age, S.FamilyID, S.YearLevelID, (CASE WHEN (SELECT TOP 1 YearLevelID FROM ES_registrations WHERE StudentNo=s.studentno) IS NOT NULL THEN (SELECT TOP 1 dbo.fn_K12_YearLevel3(YearLevelID, ProgID) FROM ES_registrations WHERE StudentNo=s.studentno ORDER BY TermID desc) ELSE dbo.fn_K12_YearLevel3(S.YearLevelID, S.ProgID) end) as YearLevelName, S.ProgID"));
                    // ->whereIn('S.ProgID', $allowed_programs);
                    // ->join('ES_Registrations as R', 'S.StudentNo', '=', 'R.StudentNo')
                    // ->where(['S.CampusID' => 1, 'R.TermID' => $term_id, 'S.ProgID' => $prog_id])
                    // ->whereIn('S.YearLevelID', $yl);

            if(!empty($search_text)){
                $g = $g->where(function ($q) use ($search_text) {
                    $s = '%'.$search_text.'%';

                    $q->where('S.StudentNo', '=', $search_text)
                        ->orWhere('S.Fullname', 'like', $s)
                        ->orWhere('S.FirstName', 'like', $s)
                        ->orWhere('S.LastName', 'like', $s)
                        ->orWhere('S.Middlename', 'like', $s);
                });
            }

            $r = $g->orderBy('S.LastName', 'ASC')->get();

            return $r;

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function _fields()
    {
        $allowed_programs = getUserProgramAccess();

        $g = DB::table($this->table.' as S')
                ->leftJoin('es_programs as p','p.ProgID','=','s.ProgID')
                ->select('S.StudentNo', 'S.Fullname', 'S.FirstName', 'S.Middlename', 'S.LastName', 'S.MiddleInitial', 'S.ExtName', 'S.Gender',
                'S.DateOfBirth', DB::raw('fn_Age(S.DateOfBirth, CURDATE()) AS Age'), 'S.FamilyID', 'S.YearLevelID',
                DB::raw('Email, TelNo, fn_YearLevel(S.YearLevelID) as YearLevelName '),
                DB::raw(' (
                    select cs.SectionName from es_classsections as cs
                    left join es_registrations as regs on regs.ClassSectionID = cs.SectionID
                    where regs.TermID =   (select reg.TermID from es_registrations as reg where reg.StudentNo = S.StudentNo order by reg.TermID desc limit 1 )
                    and regs.StudentNo = S.StudentNo LIMIT 1
                    
                     ) as SectionName, p.ProgName As Program,
                        fn_CollegeCode(p.CollegeID) As Department, p.ProgClass As ClassCode '), 'S.ProgID')
                // ->selectRaw('total_count = 1')
                ->whereIn('S.ProgID', $allowed_programs);

        return $g;
    }

    public static function scopeStudent($query, $student_no)
    {
        return $query->where('StudentNo', $student_no);
    }


    public function getStudentPhoto($student_no)
    {
        $photo = $this->select('StudentPicture')->where('StudentNo', $student_no)->first();
        return $photo;
    }

    public function studentsSearch($search_string = null, $limit = '15')
    {

        //if (!empty($search_string)) {
            $get = $this->selectRaw("StudentNo, LastName, FirstName, FamilyID,YearLevelID,  dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel  ")
                    ->whereRaw(" (LastName <> '' AND FirstName <> '') AND (Fullname like '%".$search_string."%' OR StudentNo Like '%". $search_string ."%')")
                    ->whereIn('ProgID', getUserProgramAccess())
                    ->limit($limit)
                    ->get();

            return $get;
        //} else { return false; }
    }

    public function getStudentInfo($stud_no, $term = 0 )
    {
        $get = DB::table($this->table.' as S')
                ->select('S.StudentNo', 'LastName', 'FirstName', 'Middlename', 'StudentPicture', 'MiddleInitial', 'ExtName', 'DateOfBirth', 'PlaceOfBirth', 'ReligionID', 'NationalityID', 'FamilyID', 'ForeignStudent', 'S.ProgID', 'MajorDiscID','DateAdmitted', 'CurriculumID', 'S.YearLevelID', 'ChineseName', 'S.Gender', 'S.StatusID', 'S.Res_Address', 'S.Res_Street', 'S.Res_Barangay', 'S.Res_TownCity',
                DB::raw("(CASE WHEN (SELECT TOP 1 YearLevelID FROM ES_registrations WHERE StudentNo='".$stud_no."') IS NOT NULL THEN (SELECT TOP 1 dbo.fn_K12_YearLevel3(YearLevelID, ProgID) FROM ES_registrations WHERE StudentNo='".$stud_no."' ORDER BY TermID desc) ELSE dbo.fn_K12_YearLevel3(S.YearLevelID, S.ProgID) end) as YearLevelName, dbo.fn_MajorName(MajorDiscID) As Tracks,  dbo.fn_CurriculumCode(CurriculumID) As Curriculum, dbo.fn_ProgramCollegeID(S.ProgID) As CollegeID, dbo.fn_IsForeign(NationalityID) AS [IsForeign], dbo.fn_K12IsElder(S.StudentNo) AS IsEldest , dbo.fn_RegStudentStatus(S.TermID, '{$term}' ) As Status, dbo.fn_Age(S.DateOfBirth, GETDATE()) AS Age"))
                // ->join('ES_Registrations as R', 'S.StudentNo', '=', 'R.StudentNo')
                ->where('S.StudentNo', $stud_no)
                ->first();

        return $get;
    }
    
    public function getStudentRegInfo($reg_id)
    {
        
        $get = DB::table($this->table.' as S')
                ->leftJoin('es_registrations as r','s.StudentNo','=','r.StudentNo')
                ->leftJoin('es_programs as p','r.ProgID','=','p.ProgID')                
                ->selectRaw("s.*, fn_YearLevel(r.YearLevelID) as YearLevelName, fn_MajorName(r.MajorID) As Tracks,
                            fn_CurriculumCode(CurriculumID) As Curriculum, p.CollegeID As CollegeID, fn_IsForeign(NationalityID) AS IsForeign, fn_K12IsElder(S.StudentNo) AS IsEldest ,
                            fn_Nationality(NationalityID) As Nationality, p.ProgCode,  p.ProgName As Program, p.ProgClass,
                            fn_CollegeName(p.CollegeID) As CollegeName, fn_AcademicYearTerm(r.TermID) As AYTerm,      
                            fn_TemplateCode(r.TableofFeeID) As FeesTemplate, r.TableofFeeID As RegTemplateID ,                     
                            fn_RegStudentStatus(S.TermID, r.TermID ) As Status, fn_Age(S.DateOfBirth, CURDATE()) AS Age                            
                            ")
                ->where('r.RegID', $reg_id)
                ->first();
                
        return $get;
    }

    public function getStudentSiblings($student_no)
    {
        $family_id = $this->where('StudentNo', $student_no)->pluck('FamilyID');

        if(!empty($family_id)){
            $filters = [$family_id, $student_no];
            $get = DB::select('exec sp_Students_SiblingsInfo ?, ?', $filters);

            $res = !empty($get) ? $get : false;

            return $res;

        } else {
            return false;
        }

    }

    public function getStudentFamily($student_no)
    {
        $family_id = $this->where('StudentNo', $student_no)->pluck('FamilyID');
        //var_dump($family_id);
        //dd($family_id);
        if(!empty($family_id) || $family_id != ''){
            $get = DB::table($this->table_family)->where('FamilyID', $family_id)->first();


            if (empty($get) || is_null($get))
            {
                $get = DB::table($this->table)->select('Father as Father_Name', 'Father_Occupation', 'Father_Company', 'Father_CompanyAddress', 'Father_TelNo', 'Father_Email', 'Mother as Mother_Name', 'Mother_Occupation', 'Mother_Company', 'Mother_CompanyAddress', 'Mother_TelNo', 'Mother_Email', 'Res_address', 'Res_Street', 'Res_Barangay', 'Res_TownCity', 'Guardian as Guardian_Name', 'FamilyID')->where('StudentNo', $student_no)->first();

                // var_dump($get);    
                // die();
            }

        } else {
            $get = DB::table($this->table)->select('Father as Father_Name', 'Father_Occupation', 'Father_Company', 'Father_CompanyAddress', 'Father_TelNo', 'Father_Email', 'Mother as Mother_Name', 'Mother_Occupation', 'Mother_Company', 'Mother_CompanyAddress', 'Mother_TelNo', 'Mother_Email', 'Res_address', 'Res_Street', 'Res_Barangay', 'Res_TownCity', 'Guardian as Guardian_Name', 'FamilyID')->where('StudentNo', $student_no)->first();

        }

        return $get;
    }
    public function getMedicalRecords($student_no)
    {
      $get = $get = DB::table($this->table_medicalrecords)->select('*')->where('StudentNo', $student_no)->get();

      return $get;

    }
    public function getCounselingRecords($student_no)
    {
      $get = $get = DB::table($this->table_counseling)->select('*')->where('StudentNo', $student_no)->get();

      return $get;

    }

    public function getSchoolAttended($student_no)
    {
        $get = $this->select('LRN', 'Elem_School', 'Elem_Address', 'Elem_InclDates', 'ELem_AwardHonor', 'HS_School', 'HS_Address', 'HS_InclDates', 'HS_AwardHonor')->find($student_no);

        return $get;
    }

    public function uploadPicture($student_no, $picture_data_string)
    {
        $upload = DB::update("update ES_Students set StudentPicture = CONVERT(image, $picture_data_string) where StudentNo = '".$student_no."' ");

        return $upload;
    }

    public function saveStudentFamily($post, $student_no){
        $family_id = $this->where('StudentNo', $student_no)->pluck('FamilyID');

        if(!empty($family_id)){
            $save = DB::table($this->table_family)->where('FamilyID', $family_id)->update($post);
        } else {
		
        }
        return true;
    }

    // Admission
    public function getAdmissionDocs($app_type, $is_foreign, $prog_class_id, $year_level_id)
    {
        $filters = [$app_type, $is_foreign, $prog_class_id, $year_level_id];
        $get = DB::select('exec sp_K12_RequirementsList ?, ?, ?, ?', $filters);

        if (!empty($get)) {
            return $get;
        } else {
            return false;
        }
    }

    public function getAdmissionDocsData($student_no)
    {
        try {

            $get = DB::table($this->table_adm_docs)->select('EntryID', 'TemplateDetailID', 'IsExempted', 'HasAttachment', 'IsReviewed')->where('AppNo', $student_no)->get();
            $entry_ids = array_pluck($get, 'EntryID');

            $attachments = DB::connection('sqlsrvAttachment')->table($this->table_adm_docs)->select('ID', 'EntryID', 'FileName', 'FileExtension')->whereIn('EntryID', $entry_ids)->get();

            $attach_entry_ids = array_pluck($attachments, 'EntryID');
            $new_att_arr = array_combine($attach_entry_ids, $attachments);

        if(!empty($get)){

            for($a = 0; $a < count($get); $a++){
                $doc_data = $get[$a];
                $attach_detail = false;

                if($doc_data->HasAttachment){
                    $attach_detail = !empty($new_att_arr[$doc_data->EntryID]) ? $new_att_arr[$doc_data->EntryID] : false;
                }

                $doc_data->attachment = $attach_detail;
                $get[$doc_data->TemplateDetailID] = $doc_data;
                unset($get[$a]);
            }

            return $get;

        } else {
            return false;
        }

        } catch (Exception $e) {
            err_log($e->getMessage());
        }

    }

    public function saveAdmissionForm($post, $student_no)
    {
        $update = $this->where('StudentNo', $student_no)->update($post);

        return $update;
    }

    public function saveAdmissionDocs($post, $student_no, $attach_detail = array())
    {
        $ret = array();
        $find = DB::table($this->table_adm_docs)->select('EntryID')->where(array_slice($post, 0, 3))->first();

        if (!empty($attach_detail)) {
            if (!empty($find)) {
                DB::table($this->table_adm_docs)->where('EntryID', $find->EntryID)->update($post);
                $attach_detail['EntryID'] = $find->EntryID;
            } else {
                $attach_detail['EntryID'] = DB::table($this->table_adm_docs)->insertGetID($post);
            }

            $adm_doc_attach = DB::connection('sqlsrvAttachment')->table($this->table_adm_docs);
            $find2 = $adm_doc_attach->where('EntryID', $attach_detail['EntryID'])->select('FileName', 'FileExtension')->first();

            if (!empty($find2)) {
                if ($find2->FileName.$find2->FileExtension != $attach_detail['FileName'].$attach_detail['FileExtension']) {
                    $ret['exist_filename'] = $find2->FileName.$find2->FileExtension;
                    $adm_doc_attach->where('EntryID', $attach_detail['EntryID'])->update($attach_detail);
                }
            } else {
                $adm_doc_attach->insert($attach_detail);
            }
        } else {
            if (!empty($find)) {
                $ret = DB::table($this->table_adm_docs)->where('EntryID', $find->EntryID)->update($post);
            } else {
                $ret = DB::table($this->table_adm_docs)->insert($post);
            }
        }

        return $ret;
    }

    public function removeAdmissionDoc($student_no, $template_detail_id, $action)
    {
        $find_doc = DB::table($this->table_adm_docs)->select('EntryID', 'HasAttachment')->where(['AppNo' => $student_no, 'TemplateDetailID' => $template_detail_id])->first();

        if(!empty($find_doc)){
            if($find_doc->HasAttachment){

                if($action == 'check'){
                    return ['error' => 0, 'message' => 'file exist'];
                } else if($action == 'delete') {
                    $filename = DB::connection('sqlsrvAttachment')->table($this->table_adm_docs)->select('FileName', 'FileExtension')->where('EntryID', $find_doc->EntryID)->first();
                    DB::table($this->table_adm_docs)->where('EntryID', $find_doc->EntryID)->delete();
                    DB::connection('sqlsrvAttachment')->table($this->table_adm_docs)->where('EntryID', $find_doc->EntryID)->delete();
                    return ['error' => 2, 'message' => 'deleted file', 'filename' => $filename->FileName.'.'.$filename->FileExtension];
                }
            } else {
                return ['error' => 1, 'message' => 'no file'];
            }
        } else {
            return false;
        }
    }

    // End

    public function saveCounselingForm($post, $counseling_id = 0)
    {
        if (!empty($counseling_id)) {
            // $save =
        } else {
            $save = DB::table($this->table_counseling)->insert($post);
        }

        return $save;
    }

    public function saveMedical($post)
    {
        $save = DB::table($this->table_medicalrecords)->insert($post);
        return $save;

    }

    public function getConductGrade($term, $student ){
        $data = DB::table("ES_GS_Conduct")
                    ->select(DB::raw(" ConductID, PeriodID , dbo.[fn_GSLetterGrade]( 2 ,AVG( CAST( RTRIM(LTRIM(RTRIM(REPLACE(REPLACE(Score, CHAR(13), ''), CHAR(10), '')))) AS FLOAT)) ) AS Score  "))
                    ->where(['StudentNo' => $student, 'TermID' => $term  ])
                    ->groupBy(["ConductID","PeriodID"]);

        return $data;
    }

    public function termConfiguration($term){
        $data = DB::table("ES_GS_TermConfig")
                    ->select("*")
                    ->where('TermID', $term );
                    //->whereRaw(" GETDATE() BETWEEN publishFrom AND publishto ");

        return $data;
    }

    public function isAllowedToView($term){
        $data = DB::table("ES_GS_TermConfig")
                    ->select(DB::raw(" COUNT(*) AS [Allowed]   "))
                    ->where('TermID', $term )
                    ->whereRaw(" GETDATE() BETWEEN publishFrom AND publishto ");
        return $data;
    }

    public function shiftAcademicTrack($data, $studno)
    {
        $update = DB::table("ES_Students")->where('StudentNo', $studno)->update($data);

        return $update;
    }

    public function shiftAcademicTrackRegitration($data, $studno, $term)
    {
        $registration = DB::table("ES_Registrations")
        ->where('StudentNo', $studno)
        ->where('TermID', $term)
        ->update($data);

        return $registration;
    }

    public function shiftAcademicTrackHistory($data2)
    {
        $history = DB::table("ESv2_ShiftingHistory")->insert($data2);

        return $history;
    }

    public function saveGS_Request($post, $registrar, $conference)
    {
        $return = [];

        $find = DB::table($this->table_gs_student_req)->where([
            'TermID' => $post['TermID'],
            'StudentNo' => $post['StudentNo']
        ]);

        if(!empty($registrar)){
            $reg_find = $find->where('RequestTypeID', 1)->first();

            if(!empty($reg_find)){
                $return['req_reg'] = 'duplicate';
            } else {
                $post['RequestTypeID'] = 1;
                DB::table($this->table_gs_student_req)->insert($post);
                $return['req_reg'] = 'saved';
            }
        } else {
             $return['req_reg'] = '';
        }

        if(!empty($conference)){
            $conf_find = $find->where('RequestTypeID', 2)->first();

            if(!empty($conf_find)){
                $return['req_conf'] = 'duplicate';
            } else {
                $post['RequestTypeID'] = 2;
                DB::table($this->table_gs_student_req)->insert($post);
                $return['req_conf'] = 'saved';
            }
        } else {
            $return['req_conf'] = '';
        }

        return $return;
    }

    public static function getParentDirectoryFaculty()
    {
        $userid=getUserID();

        $data = DB::table('ES_ClassSchedules as cs')
                ->selectRaw("s.StudentNo,
                    dbo.fn_StudentName4(s.StudentNo) AS StudentName,
                    dbo.fn_EmployeeName(cs.FacultyID) AS FacultyName,
                    dbo.fn_SubjectTitle(cs.SubjectID) AS Subject,
                    (SELECT Email FROM dbo.HR_Employees WHERE cs.FacultyID=employeeid) AS FacultyEmail")
                ->join('ES_RegistrationDetails as rd','rd.ScheduleID','=','cs.ScheduleID')  
                ->join('ES_Registrations as r','r.RegID','=','rd.RegID')
                ->join('ES_Students as s','s.StudentNo','=','r.StudentNo')
                ->join('ESv2_Users as u','u.FamilyID','=','s.FamilyID')
              //->whereRaw("r.ValidationDate is not null")
                ->where('r.IsWithdrawal',0)
                ->where('u.UserIDX',$userid)
              //->whereRaw("cs.FacultyID<>'' ")
              //->whereRaw("CS.FacultyID IN (SELECT employeeid FROM dbo.HR_Employees)")
                ->orderBy('StudentName','ASC')
                ->orderBy('Subject','ASC')
                ->get();


        return $data;       
  
    }

    public static function getParentDirectoryAdmin()
    {
        $userid=getUserID();

        $data = DB::table('ESv2_users as u')
                ->selectRaw("dbo.fn_EmployeeName(e.employeeid) as AdminName,u.email as Email,dbo.fn_PositionTitle(PositionID) as Position")
                ->join('HR_Employees as e','e.employeeid','=','u.FacultyID')  
                ->where('DepartmentID',1009)
                ->get();

        return $data;       
  
    }
}
