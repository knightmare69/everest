<?php
namespace App\Modules\Students\Models;

use illuminate\Database\Eloquent\Model;
use DB;

Class RecordAttachment extends Model {

	public $table='es_recordattachment';
	protected $primaryKey ='EntryID';

	protected $fillable  = array(
    	'StudentNo'
       ,'Date'
       ,'Particular'
       ,'Attachment'
       ,'Data'
       ,'Remarks'
       ,'GroupID'    /* 0 = Student Attachment, 1 = Medical Records, 2 = Counseling */
       ,'RefID'
       ,'Ext'
	);

	public $timestamps = false;

    public function scopeRef($query, $ref){
		return $query->where('RefID',$ref);
    }
}