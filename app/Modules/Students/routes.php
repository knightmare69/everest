<?php
    Route::group(['prefix' => 'students', 'middleware' => 'auth'], function () {
        Route::get('/', 'StudentProfile@index');
        Route::post('event', 'StudentProfile@event');

        Route::group(['prefix' => 'medical'], function () {
            // Route::get('/', 'Medical@index');
            Route::post('clinic/event', 'StudentProfile@clinicEvent');
        });

        Route::post('admission/event', 'StudentProfile@admissionEvent');
        Route::get('admission/event/download', 'StudentProfile@admissionDocDownload');

        Route::post('family/event', 'StudentProfile@familyEvent');

        Route::post('counseling/event', 'StudentProfile@counselingEvent');

        Route::post('documents/event', 'StudentProfile@documentsEvent');

        Route::post('academic/event', 'StudentProfile@academicEvent');

        Route::group(['prefix' => 'grades'], function () {
            Route::get('/', 'StudentGrades@index');
            Route::post('event', 'StudentGrades@event');
        });

        Route::group(['prefix' => 'subjects'], function () {
            Route::get('/', 'StudentSubjects@index');
            Route::post('/event', 'StudentSubjects@event');
        });
        
        Route::group(['prefix' => 'ledger'], function () {
            Route::get('/', 'StudentProfile@ledger');
            Route::post('event', 'StudentProfile@event');
        });
        Route::group(['prefix' => 'attendance'], function () {
            Route::get('/', 'StudentAttendance@index');
            Route::post('event', 'StudentAttendance@event');
        });
        Route::group(['prefix' => 'medical-student'], function () {
            Route::get('/', 'StudentMedical@index');
            Route::post('event', 'StudentMedical@event');
        });
        Route::group(['prefix' => 'counseling-student'], function () {
            Route::get('/', 'StudentCounseling@index');
            Route::post('event', 'StudentCounseling@event');
        });

        Route::group(['prefix' => 'evaluation'], function () {
            Route::get('/', 'StudentEvaluation@index');
            Route::post('event', 'StudentEvaluation@event');
        });
		
        Route::group(['prefix' => 'survey'], function () {        
            Route::post('/', 'StudentGrades@surveyEvent');
        });
        
        Route::group(['prefix' => 'enrollment'], function () {
            Route::get('/', 'StudentEnrolment@index');
            Route::get('print', 'StudentEnrolment@print_data');
            Route::post('event', 'StudentEnrolment@event');                    
            
        });

        Route::group(['prefix' => 'report-card'], function () {
            Route::get('/', 'StudentReportCard@index');
            Route::post('event', 'StudentReportCard@event');
            Route::get('print', 'StudentReportCard@print');
        });

        Route::group(['prefix' => 'leave'], function () {
            Route::get('/', 'StudentLeave@index');
            Route::post('event', 'StudentLeave@event');
        });

        Route::group(['prefix' => 'document-promissory'], function () {
            Route::get('/', 'StudentPromissoryDocument@index');
            Route::post('event', 'StudentPromissoryDocument@event');
        });

        Route::group(['prefix' => 'balance-promissory'], function () {
            Route::get('/', 'StudentPromissoryBalance@index');
            Route::post('event', 'StudentPromissoryBalance@event');
            Route::group(['prefix' => 'guarantorConfirmation'], function () {
                Route::get('/','StudentPromissoryBalance@guarantorConfirmation');
            });
        });
        
        
    });
