<?php

namespace App\Modules\Students\Services\StudentProfile;

use Validator;

class Validation
{
    public function studentSearch($post)
    {
        $validator = validator::make(['search' => getObjectValue($post, 'data_search')], ['search' => 'required|string']);
        return $validator;
    }

    public function studentGet($post)
    {
        $validator = validator::make(['student' => $post['student_no']], ['student' => 'required|alpha_num|min:1']);
        return $validator;
    }

    public function studentSearchFilter($post)
    {
        $validator = validator::make([
                'campus' => decode(getObjectValue($post, 'search-campus')),
                // 'academic-year' => decode(getObjectValue($post, 'search-ac-year')),
                // 'year-level' => decode(getObjectValue($post, 'search-level')),
                // 'program' => decode(getObjectValue($post, 'search-prog')),
                'searched_student(s)' => getObjectValue($post, 'student-search-text'),
            ], [
                //'campus' => 'required|int',
                // 'academic-year' => 'required|int',
                // 'year-level' => 'required|int',
                // 'program' => 'required|int',
                'searched_student(s)' => 'required|string|min:2',
            ]
        );

        return $validator;
    }

    public function studentPhoto($stud_no, $photo)
    {
        $max_size = 5000; // size in KB

        $validator = validator::make([
                'stud-id' => decode($stud_no),
                'photo' => $photo
            ], [
                'stud-id' => 'required|int',
                'photo' => 'required|max:'.$max_size.'|mimes:jpeg,png'
            ]
        );

        return $validator;
    }

    public function studentSave($post)
    {
        $data = array(
            'first_name' => getObjectValue($post, 'prof-first-name'),
            'middle_name' => getObjectValue($post, 'prof-mid-name'),
            'last_name' => getObjectValue($post, 'prof-last-name'),
            'place_of_birth' => getObjectValue($post, 'prof-dob-place'),
            'date_of_birth' => getObjectValue($post, 'prof-bdate'),
            'religion' => decode(getObjectValue($post, 'prof-religion')),
            'nationality' => decode(getObjectValue($post, 'prof-nationality')),
            'student_status' => decode(getObjectValue($post, 'prof-stud-stat')),
            'gender' => decode(getObjectValue($post, 'prof-stud-gender')),    
            // 'note' => getObjectValue($post, 'prof-note')
        );

        $rules = array(
            'first_name' => 'required|string',
            'middle_name' => 'string',
            'last_name' => 'required|string',
            'place_of_birth' => 'string',
            'date_of_birth' => 'required|date',
            'religion' => 'required|int',
            'nationality' => 'required|int',
            'student_status' => 'required|int',
            'gender' => 'string',
            // 'note' => 'string'
        );

        $validator = validator::make($data, $rules);
        return $validator;
    }

    public function familySave($post)
    {
        $data = array(
            'guardian_fullname' => getObjectValue($post, 'guardian-full-name'),
            'guardian_living_with' => decode(getObjectValue($post, 'guardian-living-with')),
            'guardian_living_with_other' => getObjectValue($post, 'guardian-living-with-other'),
            'guardian_parent_marital' => decode(getObjectValue($post, 'guardian-parent-marital')),
            'guardian_resident' => getObjectValue($post, 'guardian-addr-res'),
            'guardian_street' => getObjectValue($post, 'guardian-addr-st'),
            'guardian_barangay' => getObjectValue($post, 'guardian-addr-brgy'),
            'guardian_city' => getObjectValue($post, 'guardian-addr-city'),
            'guardian_province' => getObjectValue($post, 'guardian-addr-prov'),
            'guardian_zip' => getObjectValue($post, 'guardian-addr-zip'),
            'guardian_telephone' => getObjectValue($post, 'guardian-addr-tel'),
            'guardian_mobile' => getObjectValue($post, 'guardian-addr-mob'),
            'guardian_email' => getObjectValue($post, 'guardian-addr-email'),

            'father_fullname' => getObjectValue($post, 'father-full-name'),
            'father_date_of_birth' => getObjectValue($post, 'father-dob'),
            'father_occupation' => getObjectValue($post, 'father-ocptn'),
            'father_company' => getObjectValue($post, 'father-company'),
            'father_business_address' => getObjectValue($post, 'father-business-addr'),
            'father_business_contact' => getObjectValue($post, 'father-business-contact'),
            'father_email' => getObjectValue($post, 'father-email'),
            'father_contact_number' => getObjectValue($post, 'father-contact-no'),
            'father_educational_attainment' => getObjectValue($post, 'father-educ-attain'),
            'father_school_attended' => getObjectValue($post, 'father-school-attended'),

            'mother_fullname' => getObjectValue($post, 'mother-full-name'),
            'mother_date_of_birth' => getObjectValue($post, 'mother-dob'),
            'mother_occupation' => getObjectValue($post, 'mother-ocptn'),
            'mother_company' => getObjectValue($post, 'mother-company'),
            'mother_business_address' => getObjectValue($post, 'mother-business-addr'),
            'mother_business_contact' => getObjectValue($post, 'mother-business-contact'),
            'mother_email' => getObjectValue($post, 'mother-email'),
            'mother_contact_number' => getObjectValue($post, 'mother-contact-no'),
            'mother_educational_attainment' => getObjectValue($post, 'mother-educ-attain'),
            'mother_school_attended' => getObjectValue($post, 'mother-school-attended'),
        );

        $rules = array(
            'guardian_fullname' => 'required|string',
            'guardian_living_with' => 'required|int',
            'guardian_living_with_other' => 'string',
            'guardian_parent_marital' => 'required|int',
            'guardian_resident' => 'string',
          //'guardian_street' => 'required|string',
            'guardian_barangay' => 'string',
            'guardian_city' => 'required|string',
            'guardian_telephone' => 'string',
            'guardian_mobile' => 'required',
            'guardian_email' => 'required|email',

            'father_fullname' => 'required|string',
            'father_date_of_birth' => 'date',
          //'father_occupation' => 'required|string',
          //'father_company' => 'string',
          //'father_business_address' => 'required|string',
          //'father_business_contact' => 'required|string',
            'father_email' => 'required|email',
            'father_contact_number' => 'string',
          //'father_educational_attainment' => 'string',
          //'father_school_attended' => 'string',

            'mother_fullname' => 'required|string',
            'mother_date_of_birth' => 'date',
          //'mother_occupation' => 'required|string',
          //'mother_company' => 'string',
          //'mother_business_address' => 'required|string',
          //'mother_business_contact' => 'required|string',
            'mother_email' => 'required|email',
            'mother_contact_number' => 'string',
          //'mother_educational_attainment' => 'string',
          //'mother_school_attended' => 'string',
        );

        $validator = validator::make($data, $rules);
        return $validator;
    }

    public function academicSaveForm($post)
    {
        $data = array(
            'date_admitted' => getObjectValue($post, 'acad-admit-date'),
            'academic_year' => decode(getObjectValue($post, 'acad-year')),
            'year_level' => decode(getObjectValue($post, 'acad-level')),
            'academic_program' => decode(getObjectValue($post, 'acad-program')),
            'major_study' => decode(getObjectValue($post, 'acad-maj-study')),
            'curriculum_code' => decode(getObjectValue($post, 'acad-curr-code')),
            'campus' => decode(getObjectValue($post, 'acad-campus')),
        );

        $rules = array(
            'date_admitted' => 'date',
            'academic_year' => 'integer',
            'year_level' => 'integer',
            'academic_program' => 'integer',
            'major_study' => 'integer',
            'curriculum_code' => 'integer',
            'campus' => 'integer',
        );

        $validator = validator::make($data, $rules);
        return $validator;
    }

    public function admissionSaveForm($post)
    {
        $data = array(
            'lrn' => getObjectValue($post, 'adm-lrn'),
            'elementary_school_name' => getObjectValue($post, 'adm-elem-school-name'),
            'elementary_school_address' => getObjectValue($post, 'adm-elem-school-adr'),
            'elementary_school_inclusive_date' => getObjectValue($post, 'adm-elem-school-inclusive-date'),
            'elementary_school_grade_level' => getObjectValue($post, 'adm-elem-school-grade-level'),
            'elementary_school_award_honor' => getObjectValue($post, 'adm-elem-school-award-honor'),

            'high_school_name' => getObjectValue($post, 'adm-hs-school-name'),
            'high_school_address' => getObjectValue($post, 'adm-hs-school-adr'),
            'high_school_inclusive_date' => getObjectValue($post, 'adm-hs-school-inclusive-date'),
            'high_school_grade_level' => getObjectValue($post, 'adm-hs-school-grade-level'),
            'high_school_award_honor' => getObjectValue($post, 'adm-hs-school-award-honor'),
        );

        $rules = array(
            'lrn' => 'string',
            'elementary_school_name' => 'string',
            'elementary_school_address' => 'string',
            'elementary_school_inclusive_date' => 'date',
            'elementary_school_grade_level' => 'string',
            'elementary_school_award_honor' => 'string',

            'high_school_name' => 'string',
            'high_school_address' => 'string',
            'high_school_inclusive_date' => 'date',
            'high_school_grade_level' => 'string',
            'high_school_award_honor' => 'string',
        );

        $validator = validator::make($data, $rules);
        return $validator;
    }

    public function admissionSaveDocs($files)
    {
        $max_size = 5000; // size in KB
        $rules = array();

        foreach($files as $file => $val){
            $rules[$file] = 'max:'.$max_size.'|mimes:png,jpeg,bmp,pdf,doc,docx';
        }

        $custom_msg = ['mimes' => 'must be a valid document.', 'max' => 'size should not be greater than '.$max_size.'KB.'];

        $validator = validator::make($files, $rules, $custom_msg);

        return $validator;

    }
    public function counselingForm($post)
    {
        $data = array(
            'date' => getObjectValue($post, 'couns-date'),
            'school_year' => decode(getObjectValue($post, 'couns-ac-year')),
            'counselor' => getObjectValue($post, 'couns-name'),
            'summary' => getObjectValue($post, 'couns-summary'),
            'action' => getObjectValue($post, 'couns-action'),
        );

        $rules = array(
            'date' => 'required|date',
            'school_year' => 'required|int',
            'counselor' => 'required|string',
            'summary' => 'required|string',
            'action' => 'required',
        );

        $validator = validator::make($data, $rules);
        return $validator;
    }
    public function clinicForm($post)
    {
        $data = array(
            'date' => getObjectValue($post, 'cf-date-entry'),
            'school_year' => decode(getObjectValue($post, 'cf-ac-year')),
            'complaint' => getObjectValue($post, 'cf-complaint'),
            'diagnosis' => getObjectValue($post, 'cf-diagnosis'),
            'treatment' => getObjectValue($post, 'cf-treatment'),
            'laboratory' => getObjectValue($post, 'cf-laboratory'),
            'physician' => getObjectValue($post, 'cf-nurse-phys'),

        );

        $rules = array(
            'date' => 'required|date',
            'school_year' => 'required|int',
            'physician' => 'required|string',
            'complaint' => 'required|string',
        );

        $validator = validator::make($data, $rules);
        return $validator;
    }

}
