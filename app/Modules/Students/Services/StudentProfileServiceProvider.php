<?php

namespace App\Modules\Students\Services;

use App\Modules\Students\Services\StudentProfile\Validation;
use App\Modules\Setup\Models\GradingSystemSetup As mSetup;
use App\Modules\Enrollment\Models\Registration As mReg;
use App\Modules\Students\Models\StudentProfile as mStudent;
use App\Modules\Setup\Models\City as City;
use App\Modules\Setup\Models\Configurations as config;
use App\Modules\Enrollment\Services\Registration\Advising as advise;
use App\Modules\Accounting\Services\AccountingServiceProvider as sFinance;

use DB;

use DateTime;

class StudentProfileServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'save', $raw_err = false)
    {
        switch ($action) {
            case 'save':
                $validate = $this->validation->studentSave($post);
                break;
            case 'get':
                $validate = $this->validation->studentGet($post);
                break;
            case 'search':
                $validate = $this->validation->studentSearch($post);
                break;
            case 'search_filter':
                $validate = $this->validation->studentSearchFilter($post);
                break;
            case 'upload_picture':
                $validate = $this->validation->studentPhoto($post[0]['stud_id'], $post[1]);
                break;
            case 'family':
                $validate = $this->validation->familySave($post);
                break;
            case 'admission_form':
                $validate = $this->validation->admissionSaveForm($post);
                break;
            case 'admission_docs':
                $validate = $this->validation->admissionSaveDocs($post);
                break;
            case 'counseling':
                $validate = $this->validation->counselingForm($post);
                break;
            case 'clinic':
                $validate = $this->validation->clinicForm($post);
                break;
            case 'academic':
                $validate = $this->validation->academicSaveForm($post);
                break;
        }

        if ($validate->fails()) {
            if($raw_err){
                // $return_this = $validate;
                $return_this = ['error' => true, 'data' => $validate->errors()];
            } else {
                $return_this = ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else {
            $return_this = ['error' => false, 'message' => ''];
        }

        return $return_this;
    }

    public function studentProfileNavs($find = '')
    {
        $profnavs = [
            'profile' => [
                'icon' => 'icon-user', 'label' => 'Profile', 'content_dir' => 'profile.form'
             ],
            'academic-info' => [
                'icon' => 'icon-directions', 'label' => 'Academic Info',
                'tabs' => [
                    ['label' => 'General', 'name' => 'acad-general'],
                    ['label' => 'Shifting', 'name' => 'acad-shifting'],
                ],
                'content_dir' => ['academic.general', 'academic.shifting']
                // 'icon' => 'icon-directions', 'label' => 'Academic Info', 'content_dir' => 'academic-shifting'
            ],
            'family' => [
                'icon' => 'icon-users', 'label' => 'Family',
                'tabs' => [
                    ['label' => 'Guardian', 'name' => 'guardian'],
                    ['label' => 'Father', 'name' => 'father'],
                    ['label' => 'Mother', 'name' => 'mother'],
                    ['label' => 'Siblings', 'name' => 'Siblings'],
                    ['label' => 'Emergency', 'name' => 'emergency'],
                ],
                'content_dir' => ['family-info.guardian-form', 'family-info.father-form', 'family-info.mother-form', 'family-info.siblings-form', 'family-info.emergency-form', 'family-info.fetchers-form']
            ],
            'admission' => [
                'icon' => 'icon-graduation', 'label' => 'Admission',
                'tabs' => [
                    ['label' => 'Last School Attended', 'name' => 'adm-school-attnd'],
                    ['label' => 'Documents', 'name' => 'adm-documents-tab'],
                ],
                'content_dir' => ['admission.school-attended-form', 'admission.documents-form']
            ],
            'medical' => [
                'icon' => 'icon-heart', 'label' => 'Medical',
                'tabs' => [
                    ['label' => 'Clinic Records', 'name' => 'med-clinic'],
                  //['label' => 'Records', 'name' => 'med-medical'],
                ],
                'content_dir' => ['medical.clinic-records','medical.medical-form',]
            ],
            'counseling' => ['icon' => 'icon-info', 'label' => 'Counseling', 'content_dir' => 'counseling.list'],
            'documents' => ['icon' => 'icon-layers', 'label' => 'Documents', 'content_dir' => 'documents.list'],
        ];

        $r = !empty($find) ? $profnavs[$find] : $profnavs;
        return $r;

    }

    public function postTesting($post)
    {
        return [
            'TestingActualSched' => date('Y-m-d', strtotime(getObjectValue($post, 'act-sched-date'))),
			'TestingSignedDate' => date('Y-m-d', strtotime(getObjectValue($post, 'signed-date'))),
			'TestingSignedBy' => getUserID(),
            'FinalTestExamResultID' => decode(getObjectValue($post, 'result')),
            'TestingExamRemarks' => getObjectValue($post, 'remarks'),
        ];
    }

    public function postStudent($post)
    {    
        $mi = ((trim(getObjectValue($post, 'prof-mid-name')==''))?'':(ucfirst(getObjectValue($post, 'prof-mid-name')[0])));
		$fnme = trim(strtoupper(getObjectValue($post, 'prof-last-name')).', '.ucwords(getObjectValue($post, 'prof-first-name')).' '.$mi);
        return [
		    'Fullname'  => $fnme,
            'FirstName' => ucwords(getObjectValue($post, 'prof-first-name')),
            'Middlename' => ucwords(getObjectValue($post, 'prof-mid-name')),
            'MiddleInitial' => $mi,
            'Gender' => ucfirst(getObjectValue($post, 'prof-stud-gender')),
            'LastName' => ucwords(getObjectValue($post, 'prof-last-name')),
            'PlaceOfBirth' => getObjectValue($post, 'prof-dob-place'),
            'DateOfBirth' => date('Y-m-d', strtotime(getObjectValue($post, 'prof-bdate'))),
            'ReligionID' => decode(getObjectValue($post, 'prof-religion')),
            'NationalityID' => decode(getObjectValue($post, 'prof-nationality')),
            'StatusID' => decode(getObjectValue($post, 'prof-stud-stat')),
            'ChineseName' => getObjectValue($post, 'prof-ch-name'),
            'Res_Address' => getObjectValue($post, 'prof-addr-res'),
            'Res_Street' => getObjectValue($post, 'prof-addr-st'),
            'Res_Barangay' => getObjectValue($post, 'prof-addr-brgy'),
            'Res_TownCity' => getObjectValue($post, 'prof-addr-city'),
            // 'note' => getObjectValue($post, 'prof-note')
        ];
    }

    public function postFamily($post)
    {
        $cid = getObjectValue($post, 'guardian-addr-city');
        $city = City::where('CityID', $cid)->pluck('City');
		
        $address_full = getObjectValue($post, 'guardian-addr-res').', '.getObjectValue($post, 'guardian-addr-st').', '.getObjectValue($post, 'guardian-addr-brgy').', '.$city;

        return [
            'Guardian_Name' => getObjectValue($post, 'guardian-full-name'),
            'Guardian_ParentMaritalID' => decode(getObjectValue($post, 'guardian-parent-marital')),
            'Guardian_LivingWith' => decode(getObjectValue($post, 'guardian-living-with')),
            'Guardian_RelationshipOthers' => getObjectValue($post, 'guardian-living-with-other'),
            'Guardian_AddressFull' => $address_full,
            'Guardian_Address' => getObjectValue($post, 'guardian-addr-res'),
            'Guardian_Street' => getObjectValue($post, 'guardian-addr-st'),
            'Guardian_Barangay' => getObjectValue($post, 'guardian-addr-brgy'),
            'Guardian_TownCity' => $city,
            'Guardian_CityID' => $cid,
            'Guardian_TelNo' => getObjectValue($post, 'guardian-addr-tel'),
            'Guardian_Mobile' => getObjectValue($post, 'guardian-addr-mob'),
            'Guardian_Email' => getObjectValue($post, 'guardian-addr-email'),

            'Father_Name' => getObjectValue($post, 'father-full-name'),
            'Father_BirthDate' => getObjectValue($post, 'father-dob'),
            'Father_Occupation' => getObjectValue($post, 'father-ocptn'),
            'Father_Company' => getObjectValue($post, 'father-company'),
            'Father_CompanyAddress' => getObjectValue($post, 'father-business-addr'),
            'Father_CompanyPhone' => getObjectValue($post, 'father-business-contact'),
            'Father_Email' => getObjectValue($post, 'father-email'),
            'Father_TelNo' => getObjectValue($post, 'father-contact-no'),
            'Father_EducAttainment' => getObjectValue($post, 'father-educ-attain'),
            'Father_SchoolAttended' => getObjectValue($post, 'father-school-attended'),

            'Mother_Name' => getObjectValue($post, 'mother-full-name'),
            'Mother_BirthDate' => getObjectValue($post, 'mother-dob'),
            'Mother_Occupation' => getObjectValue($post, 'mother-ocptn'),
            'Mother_Company' => getObjectValue($post, 'mother-company'),
            'Mother_CompanyAddress' => getObjectValue($post, 'mother-business-addr'),
            'Mother_CompanyPhone' => getObjectValue($post, 'mother-business-contact'),
            'Mother_Email' => getObjectValue($post, 'mother-email'),
            'Mother_TelNo' => getObjectValue($post, 'mother-contact-no'),
            'Mother_EducAttainment' => getObjectValue($post, 'mother-educ-attain'),
            'Mother_SchoolAttended' => getObjectValue($post, 'mother-school-attended'),
        ];
    }

    public function postAdmissionForm($post)
    {
        // Fields used are based on ES_Students
        return [
            'LRN' => getObjectValue($post, 'adm-lrn'),
            'Elem_School' => getObjectValue($post, 'adm-elem-school-name'),
            'Elem_Address' => getObjectValue($post, 'adm-elem-school-adr'),
            'Elem_InclDates' => getObjectValue($post, 'adm-elem-school-inclusive-date'),
            // 'elementary_school_grade_level' => getObjectValue($post, 'adm-elem-school-grade-level'),
            // 'elementary_school_award_honor' => getObjectValue($post, 'adm-elem-school-award-honor'),

            'HS_School' => getObjectValue($post, 'adm-hs-school-name'),
            'HS_Address' => getObjectValue($post, 'adm-hs-school-adr'),
            'HS_InclDates' => getObjectValue($post, 'adm-hs-school-inclusive-date'),
            // 'high_school_grade_level' => getObjectValue($post, 'adm-hs-school-grade-level'),
            // 'high_school_award_honor' => getObjectValue($post, 'adm-hs-school-award-honor'),
        ];
    }

    public function postCounseling($post)
    {
        $data = [
            'StudentNo' => getObjectValue($post, 'student'),
            'CounsellingDate' => getObjectValue($post, 'couns-date'),
            'TermID' => decode(getObjectValue($post, 'couns-ac-year')),
            'CounselorID' => getObjectValue($post, 'couns-name'),
            'Summary' => getObjectValue($post, 'couns-summary'),
            'Action' => getObjectValue($post, 'couns-action'),
        ];

        if(!empty($post['couns_id'])){
            $data['LastModifiedBy'] = getUserID();
            $data['LastModifiedDate'] = date('Y-m-d H:i:s');
        }
        return $data;
    }
    public function postMedicalRecords($post)
    {
        $data = [
            'StudentNo' => getObjectValue($post, 'student'),
            'Date' =>date('Y-m-d', strtotime(getObjectValue($post, 'cf-date-entry'))),
            'TermID' => decode(getObjectValue($post, 'cf-ac-year')),
            'Complaint' => getObjectValue($post, 'cf-complaint'),
            'Diagnosis' => getObjectValue($post, 'cf-diagnosis'),
            'Treatment' => getObjectValue($post, 'cf-treatment'),
            'Laboratory' => getObjectValue($post, 'cf-laboratory'),
            'Physician' => getObjectValue($post, 'cf-nurse-phys'),
            'TimeIn'=>getObjectValue($post, 'cf-timein'),
            'TimeOut'=>getObjectValue($post, 'cf-timeout'),
            'Supplies'=>getObjectValue($post, 'cf-supplies'),
        ];
        
        if($data['TimeIn']=='' || $data['TimeIn']==false){$data['TimeIn']='00:00 AM';}
        if($data['TimeOut']=='' || $data['TimeOut']==false){$data['TimeOut']='17:00 PM';}

        // if(!empty($post['couns_id'])){
        //     $data['LastModifiedBy'] = getUserID();
        //     $data['LastModifiedDate'] = date('Y-m-d H:i:s');
        // }
        return $data;
    }

    public function regsStudent_academic_year($student_no)
    {

        $school = DB::table('es_institution')->value("ReportName");
        $get = DB::table('es_registrations as R')
                ->select(['T.TermID', 'T.AcademicYear', 'T.SchoolTerm', 'y.YearLevelName'])
                ->join('es_ayterm as T', 'T.TermID', '=', 'R.TermID')
                ->leftjoin('esv2_yearlevel as y', function($join){
													$join->on('y.YLID_OldValue', '=', 'r.YearLevelID');
													$join->on('y.ProgID', '=', 'r.ProgID');
												  })
                ->where('R.StudentNo', $student_no)->orderBy('R.TermID', 'DESC');

    	if($school == 'hchs'){
        $get =  $get->where('Active_OnlineEnrolment', 1);
        }
        return $get->get();
    }

	public function academic_year($limit = 0)
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm')->where(['Hidden' => 0])->orderBy('AcademicYear', 'DESC')->limit($limit)->get();
        return $get;
    }

    public function academic_yearby_student($studno)
    {
        $get = DB::table('ES_PermanentRecord_Master As g ')
                ->select(['t.TermID','t.AcademicYear','t.SchoolTerm'])
                ->leftJoin('ES_AYTerm As t','t.TermID','=','g.TermID')
                ->where('StudentNo', $studno )->orderBy('t.AcademicYear', 'DESC')->get();
        return $get;
    }

    public function campus()
    {
        $get = DB::table('ES_Campus')->select('CampusID', 'Acronym', 'ShortName')->get();
        return $get;
    }

    public function religion()
    {
        $get = DB::table('ES_Religions')->get();
        return $get;
    }

    public function nationalities()
    {
        $get = DB::table('Nationalities')->select('NationalityID', 'Nationality')->get();
        return $get;
    }

    public function livingWith()
    {
        // $get = DB::table('ESv2_Child_LivingWith')->get();
        $get = [
            1 => 'Both Parents',
			2 => 'Father',
			3 => 'Mother',
			4 => 'Guardian',
			5 => 'Others'
		];

        return $get;
    }

    public function civilStatus()
    {
        $get = DB::table('CivilStatus')->get();
        return $get;
    }

    public function getAge($admit_date, $birth_date)
    {
        $_date = new DateTime(date('Y-m-d', strtotime($admit_date)));
        $_date2 = new DateTime(date('Y-m-d', strtotime($birth_date)));
        $diff = $_date->diff($_date2);
        $age = $diff->y.'.'.$diff->m;
        return $age;
    }

    public function getAcademicYear()
    {
        $setup = new mSetup();
        $get = DB::table('ES_AYTerm')->selectRaw("TermID, AcademicYear + ' ' + SchoolTerm AS AYTerm, SchoolTerm ")
            ->where('TermID',$setup->SHSCurrentTerm())
            ->first();

        return $get;
    }

    public function studentStatus()
    {
        $g = DB::table('ES_StudentStatus')->get();
        return $g;
    }

    public function getStudentRegularLoad(){

        /* TEMP :: For this 2nd semester only */
        $level = 2;
        $yearterm = 21;

        $subjects = DB::table('ES_Students as s')
            ->leftJoin('ES_CurriculumDetails as cd','cd.CurriculumID','=','s.CurriculumID')
            ->leftJoin('ES_Subjects as sub','cd.SubjectID','=','sub.SubjectID')
            ->leftJoin('ES_YearTerms as yt','cd.YearTermID','=','yt.IndexID')
            ->selectRaw("sub.SubjectID, sub.SubjectCode, sub.SubjectTitle, sub.AcadUnits, sub.LectHrs")
            ->where('StudentNo',getUserID2())
            ->where('yt.YearLevelID',$level)
            ->where('yt.YearStatus',$yearterm)
            ->get();

        return $subjects;
    }

    public function getClassSections($idno){

        $setup = new mSetup();
        $sinfo = new mStudent();
        $student = $sinfo->getStudentInfo($idno);
        //$level = 2;

        switch($student->YearLevelID){
            case 5 : $level = 1; break;
            case 6 : $level = 2; break;
        }

        if($student->YearLevelID == '5') $level = 1;
        if($student->YearLevelID == '6') $level = 2;

        $where = " dbo.fn_curriculumMajorID(CurriculumID)  = '".$student->MajorDiscID."'";

        if($student->MajorDiscID == '605'){
             $where .= " AND CurriculumID = '{$student->CurriculumID}'";
        }

        $subjects = DB::table('ES_ClassSections as s')
            ->select("*")
            ->where('TermID',$setup->SHSCurrentTerm())
            ->where('ProgramID',$student->ProgID)
            ->where('YearLevelID',$level)
            ->whereRaw($where)
            ->get();

        return $subjects;
    }

    public function getClassSchedules($sectionid){

        $subjects = DB::table('ES_ClassSchedules as cs')
            ->select("*", DB::raw(" dbo.fn_SectionName(SectionID) As SectionName,
                                    dbo.fn_RoomName(Room1_ID) AS Room1,
                                    dbo.fn_RoomName(Room2_ID) AS Room2,
                                    dbo.fn_EmployeeName(FacultyID) AS FacultyName,
                                    dbo.fn_EmployeeName(FacultyID_2) AS FacultyName2,
                                    dbo.fn_SubjectLectureHours(SubjectID) as LecHrs
                                    "))
            ->where('SectionID',$sectionid)
            ->get();

        return $subjects;
    }

    public function getEnrollment($reg_id){
        $reg = new mReg();
        $where = array(
            'RegID' => $reg_id,
        );

        return $reg->where($where)->first();
    }

    public function getEnrollmentbyTermNIDno($term, $idno){

        $reg = new mReg();

        $where = array(
            'TermID' => $term,
            'StudentNo' => $idno,
        );

        return $reg->where($where)->first();
    }

    Public function saveStudentLoad($regID, $section, $exempt=false){
        $schedules = $this->getClassSchedules($section);
        if($exempt){
          $tmpsplit  = explode("|",$exempt);    
          $schedules = $this->getClassSchedulesWithEx($section,$tmpsplit);
        }       
        $i=1;

        DB::statement("DELETE ES_RegistrationDetails where RegID = '". $regID ."'");
        $hrs = 0;
        foreach($schedules As $r){
            $data= array(
                'RegID' => $regID,
                'ScheduleID' => $r->ScheduleID,
                'RegTagID' => '0',
                'SeqNo' => $i,
            );

            $hrs += floatval($r->LecHrs);

            $result = DB::table('ES_RegistrationDetails')->insert($data);
            $i++;
        }

        /*Update Lecture Hrs*/
        DB::statement("Update ES_Registrations set TotalLecUnits = {$hrs} where RegID = '". $regID ."'");

    }

    public function saveEnrolment($post){
        $setup = new mSetup();
        $reg = new mReg();
        $stud =  new mStudent();
        $student = $stud->getStudentInfo(getUserID2());

        $term = $setup->SHSCurrentTerm();
        $section = decode(getObjectValue($post, 'sections'));
        $book = (getObjectValue($post, 'book'));
        $is_full = $this->sectionIsFull($term,$section);

        if($is_full){
            return false;
            die();
        }

        $option = '';
        $status = '1';

        if($book == '0'){
            $option = " AND t.TemplateCode like '%TEXTBOOK%'";
        }else{
            $option = " AND t.TemplateCode like '%EBOOK%'";
        }

        if($student->MajorDiscID == '605'){
            if (strpos( strtolower($student->Curriculum) , 'ict') !== false) {
                $option .= " AND t.TemplateCode like '%ICT%'";
            }else{
                $option .= " AND t.TemplateCode like '%REG%'";
            }
        }

        if($student->IsEldest == '1'){
            $option .= " AND t.TemplateCode like '%POF%'";
        }

        if( strtolower($student->Status) == 'old'){
            $status = '2';
        }

        $fee_id = $this->getFeesTemplate($term, $student->ProgID, $student->MajorDiscID, $student->YearLevelID, $student->IsForeign, $status , $option);

        $where = array(
            'StudentNo' => $student->StudentNo,
            'TermID' =>  $term,
        );

        $data = array(
            'StudentNo' => $student->StudentNo,
            'TermID' =>  $term,
            'CampusID' => '1',
            'ClassSectionID' => $section ,
            'RegDate' => systemDate(),
            'CollegeID' => $student->CollegeID,
            'ProgID' => $student->ProgID,
            'MajorID' => $student->MajorDiscID,
            'YearLevelID' => $student->YearLevelID,
            'IsOnlineEnrolment' => 1,
            'IsRegularStudent' => 1,
            'TableofFeeID' => $fee_id

        );

        $exist = $reg->where($where)->count();

        if($exist){

            $isvalidated = $reg->where($where)
                            ->whereRaw("ValidationDate is not null")
                            ->count();

            if($isvalidated > 0){
                $res = 0;
            }else{
                $res = $reg->where($where)->update($data);
            }

        }else{
            $res = $reg->create($data);
        }

        if($res){
            $enrollment = $reg->where($where)->first();
            $res = array(
                'id' => $enrollment->RegID,
                'date' => $enrollment->RegDate,
                'status' => 'registered',
                'key' => encode($enrollment->RegID),
            );
            $output = $this->saveStudentLoad($enrollment->RegID, $enrollment->ClassSectionID);
            SystemLog('Student-Portal','','online-registration','online-registration','Term: ' . $enrollment->TermID . 'RegID:'. $enrollment->RegID .' StudentNo: ' . $enrollment->StudentNo  ,'' );
        }

        return $res;
    }

    public function getTotalEnrolledbySection($term, $section){

        $reg = new mReg();
        $where = array( 'TermID' => $term, 'ClassSectionID' => $section,);

        return $reg->where($where)->count();
    }

    public function sectionIsFull($term_id, $section_id){

        $total = intval($this->getTotalEnrolledbySection($term_id, $section_id)) + 1;
        $section = DB::table('ES_ClassSections as s')
            ->select("*")
            ->where('TermID',$term_id)
            ->where('SectionID',$section_id)
            ->first();


        if($total <= $section->Limit  ){

            return false;
        }else{
            return true;
        }
    }

    public function GetAcademicTrack()
    {
        $setup = new mSetup();

        $get = DB::table('ES_DisciplineMajors as dm')
                ->selectRaw("dm.IndexID, dm.MajorDiscDesc")
                ->leftJoin('ES_ProgramMajors as pm','dm.IndexID','=','pm.MajorDiscID')
                ->where('pm.ProgID', $setup->IsSHS())
                ->get();
        return $get;
    }

    public function GetAcademicCurriculum()
    {
        $setup = new mSetup();
        $get = DB::table('ES_Curriculums')
                ->select("*")
                ->where('ProgramID', $setup->IsSHS())
                ->get();
        return $get;
    }

    public function getAvailableSections()
    {
        $setup = new mSetup();

        $get = DB::table('ES_ClassSections')
                ->select("*")
                ->where('ProgramID', $setup->IsSHS())
                ->where('TermID',$setup->SHSCurrentTerm())
                ->get();
        return $get;

    }

    public function getStudentRegistration($stud_no)
    {
         $setup = new mSetup();
        $get = DB::table('ES_Registrations')
                ->select('*')
                ->where('StudentNo', $stud_no)
                ->where('TermID',$setup->SHSCurrentTerm())
                ->first();

        return $get;
    }

    public function getTermEnrolled($studentno)
    {
        $setup = new mSetup();

        $get = DB::table('ES_registrations as r')
                ->leftJoin('ES_AYTerm As t','t.TermID','=','r.TermID')
                ->selectraw("r.TermID,t.AcademicYear,t.SchoolTerm,ProgID")
                ->where('ProgID', $setup->IsSHS())
                ->where('r.StudentNo',$studentno)
                ->get();
        return $get;

    }

	public static function registerWithBalance($filters = array())
    {
        $get = DB::table('ES_RegisterWithBalance')->where($filters)->get();
        return $get;
    }

    public function getShiftHistory($studentno)
    {
        $setup = new mSetup();

        $get = DB::table('ESv2_ShiftingHistory')
                ->select("*", DB::raw(" dbo.fn_MajorName(OldMajorID) As OldMajorName,
                                    dbo.fn_CurriculumDesc(OldCurriculumID) AS OldCurriculumName,
                                    dbo.fn_SectionName(OldSectionID) AS OldSectionName,
                                    dbo.fn_MajorName(NewMajorID) As NewMajorName,
                                    dbo.fn_CurriculumDesc(NewCurriculumID) AS NewCurriculumName,
                                    dbo.fn_SectionName(NewSectionID) AS NewSectionName,
                                    CONVERT(VARCHAR, DateShifted, 100) AS ShiftDate
                                    "))
                ->where('StudentNo',$studentno)
                ->get();
        return $get;

    }

    public function getFeesTemplate($term, $prog, $maj, $yr , $foreign, $status = 0, $options = '' ){

        $term_summer = DB::table('ES_AYTerm')->where('TermID', $term)->pluck('SchoolTerm');
        
        if (strpos( strtolower(trim($term_summer)), 'summer') !== false) {
            $options = '';
        }

        $return = DB::select("SELECT t.TemplateID, t.TemplateCode, t.TemplateDesc FROM dbo.ES_TableofFees t LEFT JOIN dbo.ES_TableofFee_DistributionList d ON t.TemplateID=d.TemplateID
                              WHERE t.InActive = 0 AND t.TermID = '{$term}' AND d.ProgID = '{$prog}' AND d.MajorID = '{$maj}' AND d.YearLevelID = '{$yr}' AND t.ForForeign = '{$foreign}' AND t.StudentStatus = '{$status}' ". $options );

        return $return[0]->TemplateID;
    }

    public function IsPromoted($term, $idno){

        $return = DB::select("SELECT dbo.fn_Promoted(RegID) As Promoted FROM ES_Registrations WHERE TermID = {$term} AND StudentNo = '{$idno}'");
        if(count($return) > 0 ){
            return $return[0]->Promoted;
        }else{
            return 0;
        }
    }

    public function Accountability($idno){
        $return = DB::select("SELECT TOP 1 SA.Reason As Accountabilities FROM ES_StudentAccountabilities SA WHERE SA.StudentNo = '{$idno}' AND Cleared = 0 ");
        if(count($return) > 0 ){
            return $return[0]->Accountabilities;
        }else{
            return '';
        }
    }
    
    public function getClassSchedulesWithEx($sectionid,$exempt=false){
        $subjects = DB::table('ES_ClassSchedules as cs')
            ->select("*", DB::raw(" dbo.fn_SectionName(SectionID) As SectionName,
                                    dbo.fn_RoomName(Room1_ID) AS Room1,
                                    dbo.fn_RoomName(Room2_ID) AS Room2,
                                    dbo.fn_EmployeeName(FacultyID) AS FacultyName,
                                    dbo.fn_EmployeeName(FacultyID_2) AS FacultyName2,
                                    dbo.fn_SubjectLectureHours(SubjectID) as LecHrs
                                    "))
            ->where('SectionID',$sectionid);
        if($exempt){
          $subjects  = $subjects->whereNotIn('SubjectID', $exempt)
                                ->get();
        }else{
          $subjects = $subjects->get();     
        }   

        return $subjects;
    }
	
    function call_SP($param='')
    {
      if($param!='')
	  {
		$query  = "EXEC dbo.sp_studentledger @StudentNo ='".$param."';";
	  //$query  = "EXEC dbo.sp_subsidiaryledger @StudentNo ='".$param."';";
		$result = DB::select($query);
		return $result;	
	  }  
	  return false;
    }
 
    public function fetch_row($param='2016200578')
    {
      $row = $this->call_SP($param);
      if($row)
		return $this->generate_tablev1($row);
	  else
		return false;
    }
  
	function generate_tablev1($row) //sp_studentledger
	{
		$xdisplay ="<table class='table table-bordered'>
					<thead><tr>
							   <th align='center'><small>AY TERM</small></th>
							   <th>DATE</th>
							   <th>TRANS. CODE</th>
							   <th>REF. NO.</th>
							   <th>DEBIT</th>
							   <th>CREDIT</th>
							   <th>BALANCE</th>
							   <th>REMARKS</th>
							   <th>POSTED</th>
							   <th>DATE POSTED</th>
							</tr></thead><tbody><tr><td colspan='10' class='text-center'> <i class='fa fa-warning fa-2x txt-color-yellow'></i> Nothing to display!. No Record is Found.</td></tr></tbody></table>";
	  if($row && $row!='')
	  {
	   $xrow_span = 1;
	   
	   $totaldebit = 0.00;
	   $totalcredit = 0.00;
	   $totalbalance = 0.00;
	   $initial = '';
	   
	   $xgroupdetail = '';
	   $xdisplay = "<table class='table table-bordered'>";
	   $xdisplay .="<thead><tr>
							   <th align='center'><small>AY TERM</small></th>
							   <th>DATE</th>
							   <th>TRANS. CODE</th>
							   <th>REF. NO.</th>
							   <th>DEBIT</th>
							   <th>CREDIT</th>
							   <th>BALANCE</th>
							   <th>REMARKS</th>
							   <th>POSTED</th>
							   <th>DATE POSTED</th>
							</tr></thead><tbody>";
	   
	   $xfcss = $this->colorcoding('Footer');						
	   foreach($row as $rs)
	   {
		$xayterm  = property_exists($rs,'AcademicYearTerm')? $rs->AcademicYearTerm : '';
		$xtransdate = property_exists($rs,'TransDate')? $rs->TransDate : '';
		$xtransid     = property_exists($rs,'TransID')? $rs->TransID : '';
		$xrefno    = property_exists($rs,'Referenceno')? $rs->Referenceno : '';
		$xdebit   = property_exists($rs,'Debit')? $rs->Debit : '0.00';
		if($xdebit==0.00){$xdebit='0.00';}
		$xcredit   = property_exists($rs,'Credit')? $rs->Credit : '0.00';
		if($xcredit==0.00){$xcredit='0.00';}
		$xbalance  = property_exists($rs,'Balance')? $rs->Balance : '0.00';
		if($xbalance==0.00){$xbalance='0.00';}
		$xremarks  = property_exists($rs,'Remarks')? $rs->Remarks : '';
		$xposted  = property_exists($rs,'Posted')? $rs->Posted : 0;
		$xchkposted  = ($xposted==1)? "<input type='checkbox' onclick='return false;' onchange='return false;' checked/>" : "<input type='checkbox' onclick='return false;' onchange='return false;' />";
		$xdatepost  = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
		$xnonledger  = property_exists($rs,'NonLedger')? $rs->NonLedger : 0;
		
		if($xposted== 1 and $xnonledger==0)
		{$xcss = $this->colorcoding('Posted');}
		elseif(($xposted== 1 and $xnonledger==1)||($xposted== 0 and $xnonledger==1))
		{$xcss = $this->colorcoding('NonLedger');
		 $xremarks  = $xremarks.' <b>(Non-Legder)</b>';
		}
		else
		{$xcss = '';}
		
		if(($initial=='' || ($initial != '' && $initial != $xayterm))&& $xtransid!='')
		{
		 if($initial != '' && $initial != $xayterm)
		 {
		  $totalbalance = $totaldebit - $totalcredit;
		  $totalbalance = round($totalbalance, 2);
		  if($totalbalance<0)
		  {
		   $totalbalance = $totalbalance*-1;
		   $totalbalance = '('.$this->PutDecimal($totalbalance).')';
		  }
		  else
		  {
		   $totalbalance = $this->PutDecimal($totalbalance);
		  }
		  $xrow_span = $xrow_span + $xcount;
		  $xgroupdetail.=  "<tr ".$xfcss.">
							 <td colspan=4><center> *** Ending Balance for <b>".$initial."</b>***</center></td>
							 <td>Php</td>
							 <td><b>".$totalbalance."</b></td>
							 <td>Term Balance:<b>".$totalbalance."</b></td>
							 <td></td>
							 <td></td>
						  </tr>";
		 $xdisplay .= str_replace("(xmarker)","rowspan='".$xrow_span."'",$xgroupdetail);
		 $xrow_span = 1;
		 }
		$initial = $xayterm;
		
		if($xposted==1)
		{
		$totaldebit = $totaldebit + $xdebit;
		$totalcredit = $totalcredit + $xcredit;
		$totalbalance = $totaldebit - $totalcredit;
		$totalbalance = round($totalbalance, 2);
		 if($totalbalance<0)
		 {
		  $totalbalance = $totalbalance*-1;
		  $totalbalance = '('.$this->PutDecimal($totalbalance).')';
		 }
		 else
		 {
		  $totalbalance = $this->PutDecimal($totalbalance);
		 }
		}
		$xcount = 1;
		
		$xtrim_ayterm = str_replace(" ","<br>",$xayterm);
		
		$xgroupdetail = "<tr ".$xcss.">
							 <td (xmarker) class='bg-color-white' align='center' style='alignment-adjust: central;' ><b>".$xtrim_ayterm."</b></td>
							 <td>".$xtransdate."</td>
							 <td>".$xtransid."</td>
							 <td>".$xrefno."</td>
							 <td>".$this->PutDecimal($xdebit)."</td>
							 <td>".$this->PutDecimal($xcredit)."</td>
							 <td>".$totalbalance."</td>
							 <td>".$xremarks."</td>
							 <td><center>".$xchkposted."</center></td>
							 <td>".$xdatepost."</td>
					   </tr>";
		}
		elseif($initial != '' && $initial == $xayterm && $xtransid!='')
		{
		
		if($xposted==1)
		{
		$totaldebit = $totaldebit + $xdebit;
		$totalcredit = $totalcredit + $xcredit;
		$totalbalance = $totaldebit - $totalcredit;
		$totalbalance = round($totalbalance, 2);
		 if($totalbalance<0)
		 {
		  $totalbalance = $totalbalance*-1;
		  $totalbalance = '('.$this->PutDecimal($totalbalance).')';
		 }
		 else
		 {
		  $totalbalance = $this->PutDecimal($totalbalance);
		 }
		}
		$xcount = $xcount + 1;
		
		$xgroupdetail.= "<tr ".$xcss.">
							 <td>".$xtransdate."</td>
							 <td>".$xtransid."</td>
							 <td>".$xrefno."</td>
							 <td>".$this->PutDecimal($xdebit)."</td>
							 <td>".$this->PutDecimal($xcredit)."</td>
							 <td>".$totalbalance."</td>
							 <td>".$xremarks."</td>
							 <td><center>".$xchkposted."</center></td>
							 <td>".$xdatepost."</td>
					   </tr>";
		}
	   }
	   if($xgroupdetail!='')
	   {
		 $totalbalance = $totaldebit - $totalcredit;
		 $totalbalance = round($totalbalance, 2);
		 if($totalbalance<0)
		 {
		  $totalbalance = $totalbalance*-1;
		  $totalbalance = '('.$this->PutDecimal($totalbalance).')';
		 }
		 else
		 {
		  $totalbalance = $this->PutDecimal($totalbalance);
		 }
		 $xrow_span = $xrow_span + $xcount;
		 $xgroupdetail.=  "<tr ".$xfcss.">
							 <td colspan=4><center> *** Ending Balance for <b>".$initial."</b>***</center></td>
							 <td>Php</td>
							 <td><b>".$totalbalance."</b></td>
							 <td>Term Balance:<b>".$totalbalance."</b></td>
							 <td></td>
							 <td></td>
						  </tr>";
		 $xdisplay .= str_replace("(xmarker)","rowspan='".$xrow_span."'",$xgroupdetail);
		 $xrow_span = 1;
		 $xgroupdetail='';
	   }
	   $xdisplay .="<tbody></table>";
	  }
	  return $xdisplay;
	 }
	 
	 
	 
	 function putDecimal($value) 
	 {
	  return number_format($value, 2, '.', '');
	 }
	 
	 function colorcoding($value)
	 {
	  if($value == '' || $value == ' ' || $value=='Blank')
	  {
	   return "class='bg-color-white'";
	  }
	  elseif($value == 'Footer' || $value == 'footer')
	  {
	   return "class='bg-color-blueLight' style='font-size:8pt;'";
	  }
	  elseif($value == 'Posted' || $value == 'posted')
	  {
	   return "style='background-color:#F3F9FF;'";
	  }
	  elseif($value == 'NonLedger' || $value == 'nonledger')
	  {
	   return "style='background-color:#FF8080;'";
	  }
	 }
}
