<div class="form-actions right">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="button" class="btn blue input-sm" id="counseling-showform">New</button>
            <button type="button" class="btn red input-sm" id="counseling-delete">Delete</button>
        </div>
    </div>
</div>

<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="counseling-records">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="table-responsive"> -->
                        <table id="counseling_tbl" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead class="flip-content">
                                <tr>
                                    <th>Date</th>
                                    <th>Counselor</th>
                                    <th>Summary</th>
                                    <th>Action</th>
                                    <th>Has Attachment</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if(!empty($list))
                                @foreach($list as $l)
                                  <?php
                                   $path  = $_SERVER['DOCUMENT_ROOT'].'/public/assets/counseling/'.$l->AttachmentName;
                                   $uri   = url('assets/counseling/'.$l->AttachmentName);
                                   if($l->AttachmentName!='')
                                   $exist = @file_exists($path); 
                                   else
                                   $exist = false;
                                  ?>
                                  <tr data-id="{{$l->CounsellingID}}">
                                    <td>{{date('F j, Y', strtotime($l->CounsellingDate))}}</td>
                                    <td>{{$l->CounselorID}}</td>
                                    <td>{{$l->Summary}}</td>
                                    <td>{{$l->Action}}</td>
                                    <td><?php echo (($exist) ? ('<a href="'.$uri.'" target="_blank">Yes</a>'):'No');?></td>

                                  </tr>
                                @endforeach
                              @endif

                            </tbody>
                        </table>
                    <!-- </div> -->
                </div>
            </div>
        </form>
    </div>
</div>
