<form class="counseling-form" id="counseling-form" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12">
            @include('errors.event')
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Counseling Date</label>
                <div class="input-icon">
                    <i class="icon-calendar"></i>
                    <input type="text" class="form-control datepicker-on-modal" name="couns-date" id="couns-date">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">School Year</label>
                <select name="couns-ac-year" id="couns-ac-year" value="" class="form-control select2">
                    @if(!empty($academic_year))
                        @foreach($academic_year as $ayt)
                        <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear }}</option>
                        @endforeach
                    @else
                    <option value="">No selection.</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Counselor</label>
                <div class="input-icon">
                    <i class="icon-users"></i>
                    <input type="text" class="form-control" name="couns-name" id="couns-name">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Summary</label>
                <textarea name="couns-summary" id="couns-summary" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Action</label>
                <textarea name="couns-action" id="couns-action" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label class="control-label">Attachment</label>
                <input type="file" name="couns-summary" id="couns-attach" class="form-control"/>
            </div>
        </div>
    </div>
</form>
