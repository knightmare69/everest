<?php $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;    ?>
<div id="academic-shifting" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <div class="portlet-title">
                <div class="caption">
                    <h3><i class="fa fa-exchange"></i> Shifting of Academic Track</h3>
                </div>
                </div>
            </div>
            <div class="modal-body">
            <div class="portlet box light">
                    <table border="0" class="table table-condensed">
                        <tr>
                        <td align="center" colspan="2"  > <label> <b> Current </b> </label>
                        </td>
                        </tr>
                        <tr align="left">
                        @if(!empty($student_registration))
                            <td hidden="">
                            <span id="shift-current-section"  class="shift-current-section" data-id-section="{{ $student_registration->ClassSectionID }}">{{ $student_registration->ClassSectionID }}</span>
                            </td>
                         @endif
                        </tr>
                        @if(!empty($student_info))
                        <tr>
                            <td hidden=""><span id="shift-studnum"  class="shift-studnum">{{ $student_info->StudentNo }}</span></td>
                        </tr>
                        <tr>
                            <div class="form-group">
                            <td align="right">Academic Track :</td>
                            <td align="left">
                            <input disabled="" id="current-academic-track" placeholder="Disabled" data-id="{{ encode($student_info->MajorDiscID) }}" name="current-academic-track" value="{{ $student_info->Tracks }}" class="form-control">
                            </td>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group">
                            <td align="right">Curriculum :</td>
                            <td align="left">
                            <input disabled="" id="current-academic-curriculum" placeholder="Disabled" data-id="{{ encode($student_info->CurriculumID) }}" name="current-academic-curriculum" value="{{ $student_info->Curriculum }}" class="form-control">
                            </td>
                            </div>
                        </tr>
                        @endif
                        @if(!empty($student_registration))
                        <tr>
                            <td hidden=""><span id="shift-current-section"  class="shift-current-section">{{ $student_registration->ClassSectionID }}</span></td>
                        </div>
                            </td>
                        </tr>
                        @endif
                        <tr>
                        <td>
                        <br><br>
                        </td>
                        </tr>
                        <tr >
                            <td align="center" class="bg-blue-madison" colspan="2"><label><b>Shift To</b></label></td>
                        </tr>
                        <tr>
						<div class="form-group">
                            <td align="right">School Year:</td>
                            <td align="left">
                            <select name="school-term" id="school-term" class="form-control select2 input-sm school-term">
                                    <option value="0"></option>
                                    @if(!empty($term_enrolled))
                                    @foreach($term_enrolled as $te)
                                    <option value="{{ $te->TermID }}" data-prog-id="{{ $te->ProgID }}">{{ $te->AcademicYear }} {{ $te->SchoolTerm }}</option>
                                    @endforeach
                                    @endif
                                </select>

                                </td>
                         </div>
                        </tr>
                        <tr>
                            <div class="form-group">
                            <td align="right">Academic Track :</td>
                            <td align="left">
							<select name="academic-track" id="academic-track" class="form-control select2 input-sm academic-track">
                                    <option value="0"></option>
                                    @if(!empty($academic_track))
                                    @foreach($academic_track as $at)
                                    <option value="{{ $at->IndexID }}">{{ $at->MajorDiscDesc }}</option>
                                    @endforeach
                                    @endif
                                </select>

								</td>
							</div>
                        </tr>

                        <tr>
                            <div class="form-group">
                            <td align="right">Curriculum :</td>
                            <td align="left">
							<select id="academic-curriculum" name="academic-curriculum" class="form-control select2 input-sm academic-curriculum">
                                    <option value="0"></option>
                                    @if(!empty($academic_curriculum))
                                    @foreach($academic_curriculum as $ac)
                                    <option data-parent-id="{{ $ac->MajorID }}" class="curriculum" value="{{ $ac->IndexID }}">{{ $ac->CurriculumDesc }}</option>
                                    @endforeach
                                    @endif
                                </select>
							</td>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group">
                            <td align="right">Section :</td>
                            <td align="left">
                            <select id="available-sections" name="available-sections" class="form-control select2 input-sm available-sections">
                                    <option value="0"></option>
                                    @if(!empty($available_sections))
                                    @foreach($available_sections as $as)
                                    <?php
                                    $total = $service->getTotalEnrolledbySection($as->TermID, $as->SectionID);
                                    ?>
                                     @if($total < $as->Limit )
                                    <option data-term-id="{{$as->TermID}}" data-curr-id="{{ $as->CurriculumID }}" class="sections" value="{{ encode($as->SectionID) }}">{{ $as->SectionName }}</option>
                                    @endif
                                    @endforeach
                                    @endif
                                </select>
                            </td>
                            </div>
                        </tr>
                    </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default shift-academic-track">Shift Academic Track</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </div>
                    <div class="portlet box grey">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-comments"></i>Shifting History
                            </div>
                            <!-- <div class="tools">
                                <a href="javascript:;" class="collapse" data-original-title="" title="">
                                </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
                                </a>
                                <a href="javascript:;" class="reload" data-original-title="" title="">
                                </a>
                                <a href="javascript:;" class="remove" data-original-title="" title="">
                                </a>
                            </div> -->
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>
                                         #
                                    </th>
                                    <th>
                                         Date
                                    </th>
                                    <th>
                                         Former Major Track
                                    </th>
                                    <th>
                                         Former Curriculum
                                    </th>
                                    <th>
                                         Former Section
                                    </th>
                                    <th>
                                         New Major Track
                                    </th>
                                    <th>
                                         New Curriculum
                                    </th>
                                    <th>
                                         New Section
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @if(!empty($shift_history))
                                    @foreach($shift_history as $sh)
                                <tr>
                                    <td> {{ $i }} </td>
                                    <td> {{ $sh->ShiftDate}} </td>
                                    <td> {{ $sh->OldMajorName}} </td>
                                    <td> {{ $sh->OldCurriculumName}} </td>
                                    <td> {{ $sh->OldSectionName}} </td>
                                    <td> {{ $sh->NewMajorName}} </td>
                                    <td> {{ $sh->NewCurriculumName}} </td>
                                    <td> {{ $sh->NewSectionName}} </td>
                                </tr>
                                    <?php $i++; ?>
                                    @endforeach
                                    @endif
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>

    </div>
</div>
