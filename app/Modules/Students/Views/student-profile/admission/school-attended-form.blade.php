<?php //error_print($school_attended);
$_this = !empty($school_attended) ? $school_attended : array(); ?>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="school-attended-form" id="school-attended-form">
            <div class="row form-horizontal">
                <div class="col-md-12">
                    <label class="control-label col-md-5">Learning Reference Number</label>
                    <div class="col-md-7"><input type="text" class="form-control" name="adm-lrn" id="adm-lrn" value="{{ getObjectValue($_this, 'LRN') }}"></div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Elementary</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">School name</label>
                        <div class="input-icon">
                            <i class="fa fa-institution"></i>
                            <input type="text" class="form-control" name="adm-elem-school-name" id="adm-elem-school-name" value="{{ getObjectValue($_this, 'Elem_School') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Address</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" class="form-control" name="adm-elem-school-addr" id="adm-elem-school-addr" value="{{ getObjectValue($_this, 'Elem_Address') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Inclusive Dates</label>
                        <div class="input-icon">
                            <i class="icon-calendar"></i>
                            <input type="text" class="form-control datepicker" name="adm-elem-school-inclusive-date" id="adm-elem-school-inclusive-date" value="{{ getObjectValue($_this, 'Elem_InclDates') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Grade Level</label>
                        <div class="input-icon">
                            <i class="icon-puzzle"></i>
                            <input type="text" class="form-control" name="adm-elem-school-grade-level" id="adm-elem-school-grade-level">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">GWA</label>
                        <div class="input-icon">
                            <i class="icon-pencil"></i>
                            <input type="text" class="form-control" name="adm-elem-school-grade-level" id="adm-elem-school-grade-level">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Award/Honor</label>
                        <div class="input-icon">
                            <i class="icon-trophy"></i>
                            <input type="text" class="form-control" name="adm-elem-school-award-honor" id="adm-elem-school-award-honor" value="{{ getObjectValue($_this, 'ELem_AwardHonor') }}">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">High School</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">School name</label>
                        <div class="input-icon">
                            <i class="fa fa-institution"></i>
                            <input type="text" class="form-control" name="adm-hs-school-name" id="adm-hs-school-name" value="{{ getObjectValue($_this, 'HS_School') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Address</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" class="form-control" name="adm-hs-school-addr" id="adm-hs-school-addr" value="{{ getObjectValue($_this, 'HS_Address') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Inclusive Dates</label>
                        <div class="input-icon">
                            <i class="icon-calendar"></i>
                            <input type="text" class="form-control" name="adm-hs-school-inclusive-date" id="adm-hs-school-inclusive-date" value="{{ getObjectValue($_this, 'HS_InclDates') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Grade Level</label>
                        <div class="input-icon">
                            <i class="icon-puzzle"></i>
                            <input type="text" class="form-control" name="adm-hs-school-grade-level" id="adm-hs-school-grade-level">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">GWA</label>
                        <div class="input-icon">
                            <i class="icon-pencil"></i>
                            <input type="text" class="form-control" name="adm-hs-school-grade-level" id="adm-hs-school-grade-level">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Award/Honor</label>
                        <div class="input-icon">
                            <i class="icon-trophy"></i>
                            <input type="text" class="form-control" name="adm-hs-school-award-honor" id="adm-hs-school-award-honor" value="{{ getObjectValue($_this, 'HS_AwardHonor') }}">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
