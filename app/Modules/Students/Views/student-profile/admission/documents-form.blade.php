<?php

// error_print($admission_docs);
// die();
// error_print($admission_docs_info);

?>
<div class="table-responsive">
    <table class="table" id="adm-documents-table">
        <thead class="flip-content">
            <tr>
                <th width="35%">Name</th>
                <th width="10%" class="text-center">Reviewed?</th>
                <th width="10%" class="text-center">Exempted?</th>
                <th width="10%" class="text-center">Required?</th>
                <th width="10%" class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($admission_docs))
            @foreach($admission_docs as $_this)
            <tr data-id="{{ $_this->DetailID }}" data-temp="{{ $_this->TemplateID }}" data-label="{{ $_this->DocDesc }}">
                <td><span class="adm-doc-descp">{{ $_this->DocDesc }}</span><br>
                    <span class="adm-upl-filename text-muted">
                        @if(!empty($_this->data->HasAttachment) && !empty($_this->data->attachment))
                        <?php $filename = $_this->data->attachment->FileName.'.'.$_this->data->attachment->FileExtension;?>
                            {{ '('.$filename.')' }}
                        @endif
                    </span>
                </td>
                <td class="text-center">
                    <label for=""><input type="checkbox" name="adm-doc-reviewed" class="adm-doc-reviewed" {{ getObjectValue($_this->data, 'IsReviewed') ? 'checked' : '' }}></label>
                </td>
                <td class="text-center">
                    <label for=""><input type="checkbox" name="adm-doc-exempted" class="adm-doc-exempted" {{ getObjectValue($_this->data, 'IsExempted') ? 'checked' : '' }}></label>
                </td>
                <td class="text-center">
                    @if($_this->IsRequired == 0)
                    <p class="text-danger"> No </p>
                    @else
                    <p class="text-success"> Yes </p>
                    @endif
                </td>
                <td class="text-center">
                    <div class="form-inline">
                        <a href="#" class="btn btn-xs green adm-btn-file-new">
                            <i class="fa fa-upload"></i>
                        </a>
                        <a href="#" class="btn btn-xs blue adm-btn-file-dl">
                            <i class="fa fa-download"></i>
                        </a>
                        <a href="#" class="btn btn-xs red adm-btn-file-remove">
                            <i class="fa fa-trash"></i>
                        </a>
                        <input type="file" class="adm-upload-file hide" name="adm-upload-file" id="adm-upload-file" data-id={{ encode($_this->DetailID) }}>
                    </div>
                </td>
            </tr>
            @endforeach
            @else
            <td colspan="6" class="text-center">No available document(s).</td>
            @endif
        </tbody>
    </table>
</div>
