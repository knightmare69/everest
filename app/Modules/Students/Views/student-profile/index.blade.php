<?php

$permission = new App\Modules\Security\Services\Permission('student-profile');
// error_print($students);

?>
<style media="screen">
    .item-details{
        width: 100%;
    }
    .item-details a{
        width: 76%;
        white-space: nowrap;
        overflow: hidden;
        display: block;
        text-overflow: ellipsis;
        margin-right: none;
    }
    .item-details span{
        display: block;
    }
    .student-list{
        overflow-y: scroll;
        max-height: 300px;
    }
    .general-item-list > .item > .item-head > .item-details > .item-label{
        color: #000000;
    }
    .general-item-list > .item > .item-head > .item-details > .item-name{
        margin-right: 0;
    }
    textarea{
        resize: none;
    }
    .div-blocker{
        z-index: 200;
        position: absolute;
        width: 100%;
        height: 100%;
        background: #000;
        opacity: 0.1;
        display: inline-block;
    }
</style>
<div class="row">
    <div class="col-md-12">
        @include($views.'search')
    </div>
</div>
<div class="row">
    <!-- new profile -->
    <div class="col-md-12">
        <div class="profile-sidebar" style="position: relative">
            <div class="div-blocker"></div>
            <div class="portlet light profile-sidebar-portlet bordered" style="margin-bottom: 0">
            <?php $img = asset('assets/admin/layout/empty_prof_pic.jpg'); ?>
                <div class="profile-userpic">
                    <img src="{{ $img }}" class="img-responsive center-block" alt="" style="border:1px solid #ddd" id="profile-photo"/>
				</div>
                <div class="profile-usertitle">
					<div class="profile-usertitle-name">
						 <span id="full-name">[Student Name]</span><br><span id="ch-name"></span>
					</div>
					<div class="profile-usertitle-job">
						 [Student Number]
					</div>
                    <div class="text-muted bold profile-yl">
                        [Year Level]
                    </div>
				</div>
                <div class="profile-userbuttons">
                    <form id="profile-picture-form" enctype="multipart/form-data">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="">
                                <button class="btn btn-primary btn-circle input-sm btn-file">
                                    <span class="fileinput-new"> Change Photo </span>
                                    <span class="fileinput-exists"> Change Photo </span>
                                    <input type="file" name="profile-upload-photo" id="profile-upload-photo" data-id="<?= isset($student_info) ?  encode($student_info->StudentNo) : ''?> ">
                                </button>
                                <button class="btn btn-success btn-circle fileinput-exists input-sm" id="profile-upload-photo-btn"> Save</button>
                            </div>
                            <div class="">
                                <p style="margin: 5px 0">
                                    <span class="fileinput-filename" style="width: 160px;
                                    white-space: nowrap;
                                    overflow: hidden;

                                    text-overflow: ellipsis;
                                    margin-right: none;
                                    "></span>
                                    &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput"></a>
                                </p>
                            </div>
                        </div>
                    </form>
				</div>
                <div class="profile-usermenu margin-top-10" style="padding-bottom: 0">
					<ul class="nav" id="student-prof-navs">
                        @foreach($navs as $k => $n)
                        @if($permission->has($k))
                        <li>
                            <a data-form="{{ $k }}"><i class="{{ $n['icon'] }}"></i>{{ $n['label'] }}</a>
                        </li>
                        @endif
                        @endforeach
					</ul>
				</div>
            </div>
        </div>
        <!-- Content -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12" id="form-stack-new" style="position:relative">
                    <div class="div-blocker"></div>
                    @include($views.'portlet', $navs['profile'])
                </div>
            </div>
        </div>
        <!-- End -->
    </div>
    <!-- End -->

</div>
