<div class="portlet light bordered" style="margin-bottom: 0">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="{{ $icon }}"></i>
            <span class="caption-subject font-blue-madison bold uppercase">{{ $label }}</span>
        </div>
        @if(!empty($tabs))
            <ul class="nav nav-tabs">
                @foreach($tabs as $k => $t)
                <li class="{{ $k == 0 ? 'active' : '' }}"><a href="#{{ $t['name'] }}-tab" data-toggle="tab">{{ $t['label'] }}</a></li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="portlet-body">
        @if(!empty($tabs))
        <div class="tab-content">
            @foreach($tabs as $k => $t)
            <div class="tab-pane fade {{ $k == 0 ? 'in active' : '' }}" id="{{ $t['name'] }}-tab">
                @include($views.$content_dir[$k])
            </div>
            @endforeach
            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        @if(!in_array($form, ['medical', 'academic-info']))
                        <button type="button" class="btn default">Cancel</button>
                        <button type="button" class="btn green" id="{{ $form }}-save-btn">Save</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @else
            @include($views.$content_dir)
        @endif
    </div>
</div>
