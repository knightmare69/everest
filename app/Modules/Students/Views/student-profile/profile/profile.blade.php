<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-3">
            <div class="profile-holder">
                <div class="profile-pic-holder">
                    <?php $img = !empty($student_info->StudentPicture) ? 'data:image/jpeg;base64,'.base64_encode(hex2bin($student_info->StudentPicture)) : asset('assets/system/media/images/empty_prof_pic.jpg') ?>
                    <img src="{{ $img }}" class="img-responsive center-block" alt="" style="border:1px solid #ddd" id="profile-photo"/>
                </div>
            </div>
            <hr>
            <div class="profile-info text-center">
                <h3><strong class="font-green-sharp">{{ $student_info->StudentNo or '' }}</strong><br><span class="text-muted">{{ $student_info->YearLevelName or '' }}</span></h3>
            </div>
            <hr>
            <div class="text-center">
                <div class="row">
                    <div class="col-md-12">
                        @include('errors.event')
                    </div>
                </div>
                <form id="profile-picture-form" enctype="multipart/form-data">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="margin-bottom-10">
                            <span class="btn btn-primary input-sm btn-file">
                                <span class="fileinput-new"><i class="icon-picture"></i> Change Photo </span>
                                <span class="fileinput-exists"><i class="icon-picture"></i> Change Photo </span>
                                <input type="file" name="profile-upload-photo" id="profile-upload-photo" data-id="<?= isset($student_info) ?  encode($student_info->StudentNo) : ''?> ">
                            </span>
                            <span class="btn btn-success fileinput-exists input-sm" id="profile-upload-photo-btn"><i class="fa fa-check"></i> Save</span>
                        </div>
                        <div>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#academic-shifting"><i class="fa fa-exchange"></i> Shift Academic Track</button>
                        </div>
                            @include($views.'academic-shifting')
                        <div class="">
                            <p>
                                <span class="fileinput-filename" style="width: 160px;
                                white-space: nowrap;
                                overflow: hidden;

                                text-overflow: ellipsis;
                                margin-right: none;
                                "></span>
                                &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput"></a>
                            </p>
                        </div>
                    </div>
                </form>
                <!-- <span class="text-muted" id="filename-holder"></span> <button type="button" id=""></button> -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-form-holder portlet-body form">
                @include($views.'includes')
            </div>
        </div>
    </div>
</div>