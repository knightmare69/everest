<?php //unset($student_info->StudentPicture); error_print($student_info);
     $student_info = isset($student_info) ? $student_info : [];
     $religion_id = getObjectValue($student_info, 'ReligionID');
     $nationality_id = getObjectValue($student_info, 'NationalityID');
     $status_id = getObjectValue($student_info, 'StatusID');
?>
<form class="profile-form" id="profile-form" role="form">
    <div class="portlet light bg-inverse">
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-12">
                    @include('errors.event')
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label">
                            <div>
                                <label><input type="radio" name="stud-num-rad" class="stud-num-rad" value="auto-num" checked> Automatic</label>
                                <label><input type="radio" name="stud-num-rad" class="stud-num-rad" value="pre-num"> Pre-Numbered</label>
                            </div>
                        </label>
                        <div class="input-group">
							<span class="input-group-addon">Student No.</span>
							<input type="text" name="prof-stud-num" id="prof-stud-num" class="form-control" value="<?= isset($student_info) ? getObjectValue($student_info, 'StudentNo') : '' ?>" disabled>
						</div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label">Student Status</label>
                        <select class="form-control select2 select2-profile" name="prof-stud-stat" id="prof-stud-stat" placeholder="Please select status">
                            @if(!empty($student_status))
                                <option value=""></option>
                                @foreach($student_status as $ss)
                                <?php $checked = ($status_id == $ss->StatusID) ? 'selected' : ''; ?>
                                <option value="{{ encode($ss->StatusID) }}" {{ $checked }}>{{ $ss->StatusName }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">First Name</label>
                        <input type="text" name="prof-first-name" id="prof-first-name" class="form-control" value="<?= isset($student_info) ? getObjectValue($student_info, 'FirstName') : '' ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Middle Name</label>
                        <input type="text" name="prof-mid-name" id="prof-mid-name" class="form-control" value="<?= isset($student_info) ? getObjectValue($student_info, 'Middlename') : '' ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Last Name</label>
                        <input type="text" name="prof-last-name" id="prof-last-name" class="form-control" value="<?=  isset($student_info) ? getObjectValue($student_info, 'LastName') : '' ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label">Chinese Name</label>
                        <input type="text" name="prof-ch-name" id="prof-ch-name" class="form-control" value="<?= isset($student_info) ? getObjectValue($student_info, 'ChineseName') : '' ?>" {{ env('CHINESE_CHAR') != 1 ? 'disabled' : '' }}>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label">Gender</label>
                        <?php $gender = !empty($student_info) ? getObjectValue($student_info, 'Gender') : ''; ?>
                        <select class="form-control select2 select2-profile" name="prof-stud-gender" id="prof-stud-gender" placeholder="Please select gender">
                            <option value=""></option>
                            <option value="M" {{ $gender == 'M' ? 'selected' : '' }}>Male</option>
                            <option value="F" {{ $gender == 'F' ? 'selected' : '' }}>Female</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label">Place of birth</label>
                        <div class="input-icon">
                            <i class="icon-map"></i>
                            <input type="text" name="prof-dob-place" id="prof-dob-place" class="form-control " value="<?=  isset($student_info) ?  getObjectValue($student_info,'PlaceOfBirth') : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Date of birth</label>
                        <div class="input-icon">
                            <i class="icon-calendar"></i>
                            <input type="text" class="form-control datepicker " name="prof-bdate" id="prof-bdate" value="{{ !empty($student_info->DateOfBirth) ? date('m/d/Y', strtotime($student_info->DateOfBirth)) : '' }}">
                            <input type="text" id="prof-admit-date" class="hide" value="{{ !empty($student_info->DateOfBirth) ? date('m/d/Y', strtotime($student_info->DateAdmitted)) : '' }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Age</label>
                        <input type="number" disabled="" id="prof-age" class="form-control " value="<?= !empty($student_info->Age) ? number_format($student_info->Age, 2) : '' ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Religion</label>
                        <select class="form-control select2 select2-profile" name="prof-religion" id="prof-religion">
                            @if(!empty($religion))
                                @foreach($religion as $rel)
                                <?php $checked = ($religion_id == $rel->ReligionID) ? 'selected' : ''; ?>
                                <option value="{{ encode($rel->ReligionID) }}" {{ $checked }}>{{ $rel->Religion }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Nationality</label>
                        <select class="form-control select2 select2-profile" name="prof-nationality" id="prof-nationality">
                            @if(!empty($nationality))
                                @foreach($nationality as $nal)
                                <?php $checked = ($nationality_id == $nal->NationalityID) ? 'selected' : ''; ?>
                                <option value="{{ encode($nal->NationalityID) }}" {{ $checked }}>{{ $nal->Nationality }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Home Address</h4>
					<?php
					  $famid = getObjectValue($student_info, 'FamilyID');
					  if($famid!=''){
					    $tmpfam = DB::table('ESv2_Admission_FamilyBackground')->where(array('FamilyID'=>$famid))->get();
						if($tmpfam && count($tmpfam)>0){
							$student_info->Res_Address = $tmpfam[0]->Guardian_Address;
							$student_info->Res_Street  = $tmpfam[0]->Guardian_Street;
							$student_info->Res_Barangay= $tmpfam[0]->Guardian_Barangay;
							$student_info->Res_TownCity= $tmpfam[0]->Guardian_TownCity;
							$student_info->Res_Province= $tmpfam[0]->Guardian_Province;
							$student_info->Res_ZipCode = $tmpfam[0]->Guardian_ZipCode;
						}
					  }
					?>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="prof-addr-res" id="prof-addr-res" class="form-control" value="{{ getObjectValue($student_info, 'Res_Address') }}">
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="control-label">Street</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="prof-addr-st" id="prof-addr-st" class="form-control" value="{{ getObjectValue($student_info, 'Res_Street') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Barangay</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="prof-addr-brgy" id="prof-addr-brgy" class="form-control" value="{{ getObjectValue($student_info, 'Res_Barangay') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">City</label>
                        <select class="select2 form-control select2-profile" name="prof-addr-city" id="prof-addr-city">
                        <?php $selected = '';?>
						@foreach(App\Modules\Setup\Models\City::get() as $city)
                            <?php $selected = trim(strtolower(getObjectValue($student_info,'Res_TownCity'))) == trim(strtolower($city->City)) ? 'selected' : '';  ?>
                            <option {{ $selected }} value="{{ $city->City }}">{{ $city->City }}</option>
                        @endforeach
						    <?php
							if($selected =='' && trim(strtolower(getObjectValue($student_info,'Res_TownCity')))!=''){
							  echo '<option value="'.trim(strtolower(getObjectValue($student_info,'Res_TownCity'))).'" selected>'.trim(strtolower(getObjectValue($student_info,'Res_TownCity'))).'</option>';
							}elseif($selected =='' && trim(strtolower(getObjectValue($student_info,'Res_TownCity')))==''){
							  echo '<option value="" selected disabled>- Select City -</option>';
							}
							?>
                        </select>
                    </div>
				</div>
				<div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Province</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="prof-addr-prov" id="prof-addr-prov" class="form-control" value="{{ getObjectValue($student_info, 'Res_Province') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Zip Code</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="prof-addr-zip" id="prof-addr-zip" class="form-control" value="{{ getObjectValue($student_info, 'Res_ZipCode') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Note</label>
                        <textarea name="prof-note" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions right">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
                <button type="button" class="btn default">Cancel</button>
				<button type="button" class="btn green" id="prof-save-btn">Save</button>
			</div>
		</div>
	</div>
</form>
