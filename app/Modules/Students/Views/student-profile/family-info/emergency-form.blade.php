<?php 
$_this = !empty($family_info) ? $family_info : array();
if(empty($family_info) && isParent()){
	$tmpfam = DB::SELECT("SELECT * FROM ESv2_Admission_FamilyBackground WHERE FamilyID='".getFamilyID()."'");
	if($tmpfam && count($tmpfam)>0){
	  $_this = $tmpfam[0];
	}
}
?>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="emergency-form" id="emergency-form">

            <div class="row">
                <div class="">
                    <h4 class="form-section">List all persons to be contacted in the event of any emergency when none of the parents can be reached.  Note:  Please use only local contacts.</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">NAME OF EMERGENCY CONTACT 1 <span class="required">*</span></label>
                        <input type="text" name="emergencya-name" id="emergencya-name" class="form-control guarddata" value="{{getObjectValue($_this,'EmergencyContact_Name')}}" required/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label">Relationship to student: <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="fa fa-users"></i>
                            <input type="text" class="form-control guarddata" name="emergencya-relate" id="emergencya-relate" value="{{getObjectValue($_this,'EmergencyContact_Relationship')}}" required/>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label">Contact No. <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="fa fa-phone"></i>
                            <input type="text" class="form-control guarddata" name="emergencya-contact" id="emergencya-contact" value="{{getObjectValue($_this,'EmergencyContact_Mobile')}}" required/>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="row"><br/></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">NAME OF EMERGENCY CONTACT 2 <span class="required">*</span></label>
                        <input type="text" name="emergencyb-name" id="emergencyb-name" class="form-control guarddata" value="{{getObjectValue($_this,'EmergencyContactB_Name')}}" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label">Relationship to student: <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="fa fa-users"></i>
                            <input type="text" class="form-control guarddata" name="emergencyb-relate" id="emergencyb-relate" value="{{getObjectValue($_this,'EmergencyContactB_Relationship')}}" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label">Contact No. <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="fa fa-phone"></i>
                            <input type="text" class="form-control guarddata" name="emergencyb-contact" id="emergencyb-contact" value="{{getObjectValue($_this,'EmergencyContactB_Mobile')}}" required>
                        </div>
                    </div>
                </div>
            </div>
			
			<br>
        </form>
    </div>
</div>
