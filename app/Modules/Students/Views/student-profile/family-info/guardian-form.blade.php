<?php //error_print($family_info);
$_this = !empty($family_info) ? $family_info : array();
?>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="guardian-form" id="guardian-form">
            <div class="row">
                <div class="col-md-12">
                    @include('errors.event')
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Guardian Information</h4>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Family ID</label>
                        <div class="form-group">
                            <input type="text" name="FamilyID" id="FamilyID" class="form-control" value="{{ getObjectValue($_this, 'FamilyID') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Full Name</label>
                        <input type="text" name="guardian-full-name" id="guardian-full-name" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Name') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Living with</label>
                        <select class="select2 form-control select2-profile" style="margin-bottom: 5px;" name="guardian-living-with" id="guardian-living-with">
                            @if(!empty($living_with))
                                @foreach($living_with as $k => $lw)
                                <?php $checked = (!empty($_this->Guardian_LivingWith) ? ($_this->Guardian_LivingWith == $k) ? 'checked' : '' : ''); ?>
                                <option value="{{ encode($k) }}" {{ $checked }}>{{ $lw }}</option>
                                @endforeach
                            @endif
                        </select>
                        <input type="text" class="form-control  hide" name="guardian-living-with-other" id="guardian-living-with-other" placeholder="Please specify other." />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Parent Marital</label>
                        <select name="guardian-parent-marital" id="guardian-parent-marital" class="select2 form-control select2-profile">
                            @if(!empty($civil_status))
                                @foreach($civil_status as $cl)
                                <?php 
								$checked = ''; 
								if(!empty($_this->Guardian_ParentMaritalID)){ 
								$checked = ($_this->Guardian_ParentMaritalID == $cl->StatusID) ? 'selected' : ''; 
								}
								?>
                                <option value="{{ encode($cl->StatusID) }}" {{ $checked }}>{{ $cl->CivilDesc }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Home Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-addr-res" id="guardian-addr-res" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Address') }}">
                        </div>
                    </div>
					<!--
                    <div class="form-group">
                        <label class="control-label">Street</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-addr-st" id="guardian-addr-st" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Street') }}">
                        </div>
                    </div>
					-->
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Barangay</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-addr-brgy" id="guardian-addr-brgy" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Barangay') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Province</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-addr-prov" id="guardian-addr-prov" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Province') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">City</label>
                        <select class="select2 form-control select2-profile" name="guardian-addr-city" id="guardian-addr-city">
                        <option value='' selected disabled> --Select a City-- </option>
                        @foreach(App\Modules\Setup\Models\City::get() as $city)
                            <option {{ getObjectValue($_this,'Guardian_CityID') == $city->CityID ? 'selected' : '' }} value="{{ $city->CityID }}">{{ $city->City }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Zip Code</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-addr-zip" id="guardian-addr-zip" class="form-control" value="{{ getObjectValue($_this, 'Guardian_ZipCode') }}">
                        </div>
                    </div>
				</div>	
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Billing Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-bill-res" id="guardian-bill-res" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Billing_Address') }}">
                        </div>
                    </div>
					<!--
                    <div class="form-group">
                        <label class="control-label">Street</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-addr-st" id="guardian-addr-st" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Street') }}">
                        </div>
                    </div>
					-->
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Barangay</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-bill-brgy" id="guardian-bill-brgy" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Billing_Barangay') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Province</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-bill-prov" id="guardian-bill-prov" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Billing_Province') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">City</label>
                        <select class="select2 form-control select2-profile" name="guardian-bill-city" id="guardian-bill-city">
                        <option value='' selected disabled> --Select a City-- </option>
                        @foreach(App\Modules\Setup\Models\City::get() as $city)
                            <option {{ getObjectValue($_this,'Guardian_Billing_CityID') == $city->CityID ? 'selected' : '' }} value="{{ $city->CityID }}">{{ $city->City }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Zip Code</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" name="guardian-bill-zip" id="guardian-bill-zip" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Billing_ZipCode') }}">
                        </div>
                    </div>
				</div>	
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Contact Information</h4>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Telephone</label>
                        <div class="input-icon">
                            <i class="fa fa-phone"></i>
                            <input type="text" name="guardian-addr-tel" id="guardian-addr-tel" class="form-control" value="{{ getObjectValue($_this, 'Guardian_TelNo') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Mobile</label>
                        <div class="input-icon">
                            <i class="fa fa-mobile"></i>
                            <input type="text" name="guardian-addr-mob" id="guardian-addr-mob" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Mobile') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <div class="input-icon">
                            <i class="fa fa-envelope"></i>
                            <input type="text" name="guardian-addr-email" id="guardian-addr-email" class="form-control" value="{{ getObjectValue($_this, 'Guardian_Email') }}">
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
