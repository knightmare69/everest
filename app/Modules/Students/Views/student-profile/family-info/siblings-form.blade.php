<?php //error_print($siblings); ?>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="flip-content">
            <tr>
                <th>Full name</th>
                <th>Date of Birth</th>
                <th>Age</th>
                <th>Gender</th>
                <th>School Attended</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($siblings))
                @foreach($siblings as $_this)
                    @if($_this->IsMe != 1)
                    <tr data-id="{{ $_this->IDX }}">
                        <td>{{ ucwords(strtolower($_this->Fullname)) }}</td>
                        <td>{{ date('M. d, Y', strtotime($_this->DateOfBirth)) }}</td>
                        <td>{{ round($_this->Age, 2) }}</td>
                        <td>{{ ($_this->Gender == 'M') ?'Male' : 'Female' }}</td>
                        <td>{{ $_this->SchoolAttendend }}</td>
                    </tr>
                    @elseif($_this->IsMe == 1 && count($siblings) == 1)
                    <tr>
                        <td colspan="5">
                            <p class="text-center no-margin">Nothing to show.</p>
                        </td>
                    </tr>
                    @endif
                @endforeach
            @else
            <tr>
                <td colspan="5">
                    <p class="text-center no-margin">Nothing to show.</p>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
