<?php 
$_this = !empty($family_info) ? $family_info : array();
if(empty($family_info) && isParent()){
	$tmpfam = DB::SELECT("SELECT * FROM ESv2_Admission_FamilyBackground WHERE FamilyID='".getFamilyID()."'");
	if($tmpfam && count($tmpfam)>0){
	  $_this = $tmpfam[0];
	}
}
?>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="father-form" id="father-form">

            <div class="row">
                <div class="">
                    <h4 class="form-section">Father Information</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Full Name <span class="required">*</span></label>
                        <input type="text" name="father-full-name" id="guardian-full-name" class="form-control guarddata" value="{{ getObjectValue($_this, 'Father_Name') }}" required/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Date of Birth <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="icon-calendar"></i>
                            <input type="date" class="form-control guarddata datepicker" name="father-dob" id="father-dob" data-date-format="yyyy-mm-dd" value="{{ !empty($_this->Father_BirthDate) ? date('Y-m-d', strtotime($_this->Father_BirthDate)) : '' }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label">Occupation</label>
                        <div class="input-icon">
                            <i class="icon-briefcase"></i>
                            <input type="text" class="form-control guarddata" name="father-ocptn" id="father-ocptn" value="{{ getObjectValue($_this, 'Father_Occupation') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="control-label">Company</label>
                        <div class="input-icon">
                            <i class="fa fa-building"></i>
                            <input type="text" class="form-control guarddata" name="father-company" id="father-company" value="{{ getObjectValue($_this, 'Father_Company') }}">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="">
                    <h4 class="form-section">Contact Information</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="" class="control-label">Business Address</label>
                        <div class="input-icon">
                            <i class="icon-pointer"></i>
                            <input type="text" class="form-control guarddata" name="father-business-addr" id="father-business-addr" value="{{ getObjectValue($_this, 'Father_CompanyAddress') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="" class="control-label">Business Contact</label>
                    <div class="input-icon">
                        <i class="fa fa-phone"></i>
                        <input type="text" class="form-control guarddata" name="father-business-contact" id="father-business-contact" value="{{ getObjectValue($_this, 'Father_CompanyPhone') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Email <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="fa fa-envelope"></i>
                            <input type="text" name="father-email" id="father-email" class="form-control guarddata" value="{{ getObjectValue($_this, 'Father_Email') }}" required/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="control-label">Contact No. <span class="required">*</span></label>
                        <div class="input-icon">
                            <i class="fa fa-phone"></i>
                            <input type="text" name="father-contact-no" id="father-contact-no" class="form-control guarddata" value="{{ getObjectValue($_this, 'Father_Mobile') }}" required/>
                        </div>
                    </div>
                </div>
				<div class="form-group col-sm-12">
					<div class="col-sm-12">
						<label class="control-label">I am authorizing Everest Academy Manila to include this mobile no. in the homeroom class Viber group.</label><br/>
						<label><input type="radio" id="IncludeGroupa" name="IncludeGroupFather"  value="1" {{ ((getObjectValue($_this,'IncludeViberGroupFather')=='1')?'checked':'') }} required/> Yes</label>
						<label><input type="radio" id="IncludeGroupb" name="IncludeGroupFather"  value="0" {{ ((getObjectValue($_this,'IncludeViberGroupFather')=='0')?'checked':'') }}/> No</label>
						<span id="IncludeGroup-error" class="help-block" style="display: none;">This field is required.</span>
					</div>
				</div>
            </div>
            <br>
            <div class="row">
                <div class="">
                    <h4 class="form-section">Other Information</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Educational Attainment</label>
                        <div class="input-icon">
                            <i class="icon-graduation"></i>
                            <input type="text" name="father-educ-attain" id="father-educ-attain" class="form-control guarddata" value="{{ getObjectValue($_this, 'Father_EducAttainment') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">School Attended</label>
                        <div class="input-icon">
                            <i class="fa fa-bank"></i>
                            <input type="text" name="father-school-attended" id="father-school-attended" class="form-control guarddata" value="{{ getObjectValue($_this, 'Father_SchoolAttended') }}">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
