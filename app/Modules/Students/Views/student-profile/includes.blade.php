<style >
.nav-tabs {
    white-space: nowrap;
    overflow-x: auto;
    overflow-y: hidden;
}
.tab-main-nav > ul{
    /*padding-bottom: 5px;*/
}
.nav-tabs > li {
    float: none;
    display: inline-block;
}
</style>
<div class="tabbable-line tab-main-nav">
	<ul class="nav nav-tabs nav-tabs-main" >
        <li class="active">
            <a href="#stud-info-tab" data-toggle="tab">Profile Info</a>
        </li>
		<li>
			<a href="#family-info-tab" data-toggle="tab">Family Info</a>
		</li>
		<li>
			<a href="#admission-tab" data-toggle="tab">Admission</a>
		</li>
		<li>
			<a href="#medical-tab" data-toggle="tab">Medical</a>
		</li>
		<li>
			<a href="#counseling-tab" data-toggle="tab">Counseling</a>
		</li>
		<li>
			<a href="#docu-tab" data-toggle="tab">Documents</a>
		</li>
		<!-- <li>
			<a href="#tab_15_3" data-toggle="tab">Enrollment</a>
		</li>
		<li>
			<a href="#tab_15_3" data-toggle="tab">Financial</a>
		</li>
		<li>
			<a href="#tab_15_3" data-toggle="tab">Grade</a>
		</li> -->
	</ul>
</div>
<div class="tabbable-line">
    <div class="tab-content">
        <div class="tab-pane fade in active" id="stud-info-tab">
            @include($views.'profile.form')
        </div>
		<div class="tab-pane fade" id="family-info-tab">
			<div class="tabbable-line nav-tabs-sub">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#guardian-tab" data-toggle="tab">Guardian</a>
                    </li>
                    <li>
                        <a href="#father-tab" data-toggle="tab">Father</a>
                    </li>
                    <li>
                        <a href="#mother-tab" data-toggle="tab">Mother</a>
                    </li>
                    <li>
                        <a href="#siblings-tab" data-toggle="tab">Siblings</a>
                    </li>
                    <li>
                        <a href="#fetchers-tab" data-toggle="tab">Fetchers / Parents</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="guardian-tab">
                        @include($views.'family-info.guardian-form')
                    </div>
                    <div class="tab-pane fade" id="father-tab">
                        @include($views.'family-info.father-form')
                    </div>
                    <div class="tab-pane fade" id="mother-tab">
                        @include($views.'family-info.mother-form')
                    </div>
                    <div class="tab-pane fade" id="siblings-tab">
                        @include($views.'family-info.siblings-form')
                    </div>
                    <div class="tab-pane fade" id="fetchers-tab">
                        @include($views.'family-info.fetchers-form')
                    </div>
                    <div class="form-actions right">
                		<div class="row">
                			<div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn default">Cancel</button>
                				<button type="button" class="btn green" id="family-save-btn">Save</button>
                			</div>
                		</div>
                	</div>
                </div>
			</div>
		</div>
		<div class="tab-pane fade" id="admission-tab">
            <div class="tabbable-line nav-tabs-sub">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#adm-school-attnd-tab" data-toggle="tab">Last School Attended</a>
                    </li>
                    <li>
                        <a href="#adm-documents-tab" data-toggle="tab">Documents</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="adm-school-attnd-tab">
                        @include($views.'admission.school-attended-form')
                    </div>
                    <div class="tab-pane fade" id="adm-documents-tab">
                        @include($views.'admission.documents-form')
                    </div>
                </div>
                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="button" class="btn default">Cancel</button>
                            <button type="button" class="btn green" id="admission-save-btn">Save</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<div class="tab-pane fade" id="medical-tab">
            <div class="tabbable-line nav-tabs-sub">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#med-clinic-tab" data-toggle="tab">Clinic Records</a>
                    </li>
                    <li class="">
                        <a href="#med-medical-tab" data-toggle="tab">Records</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="med-clinic-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right form-group">
                                    <button type="button" class="btn green input-sm" id="med-clinic-showform">New</button>
                                </div>
                            </div>
                        </div>
                        @include($views.'medical.clinic-records')
                    </div>
                    <div class="tab-pane fade" id="med-medical-tab">
                        @include($views.'medical.medical-form')
                    </div>
                </div>
                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="button" class="btn default">Cancel</button>
                            <button type="button" class="btn green">Save</button>
                        </div>
                    </div>
                </div>
            </div>

		</div>
		<div class="tab-pane fade" id="counseling-tab">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right form-group">
                        <button type="button" class="btn green input-sm" id="counseling-showform">New</button>
                    </div>
                </div>
            </div>
            @include($views.'counseling.list')
		</div>
		<div class="tab-pane fade" id="docu-tab">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right form-group">
                        <button type="button" class="btn green input-sm" id="docu-showform">New</button>
                    </div>
                </div>
            </div>
			@include($views.'documents.list')
		</div>
	</div>
</div>
