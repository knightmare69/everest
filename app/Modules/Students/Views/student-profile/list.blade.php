<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="students-table-res">
    <thead>
        <tr>
            <th width="30px">Pic.</th>
            <th width="85px">Student No.</th>
            <th>Full Name</th>
            <th>Year Level</th>
            <th width="50px">Gender</th>
            <th width="30px">Age</th>
        </tr>
    </thead>
    <tbody>
    @if(!empty($students))
        @foreach($students as $_this)
        <tr>
            <td align="center"><img src="{{ url() .'/general/getPupilPhoto?Idno='. encode($_this->StudentNo) }}" class="img bordered"  height="25" width="25" alt="Photo" style="border: 1px solid #ddd;"></td>
            <td>{{ $_this->StudentNo }}</td>
            <td><a href="#" class="student-select" data-id="{{ $_this->StudentNo }}">{{ $_this->LastName.', '.$_this->FirstName.' '.$_this->MiddleInitial.' '.$_this->ExtName }}</a></td>
            <td>{{ $_this->YearLevelName }}</td>
            <td>{{ ($_this->Gender == 'M') ? 'Male' : 'Female'  }}</td>
            <td>{{ number_format($_this->Age, 2) }}</td>
        </tr>
        @endforeach
    @endif
    </tbody>
</table>
