<?php $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;    ?>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-6">
                <h4 class="form-section">Current Track</h4>
                <div class="form-group">
                    <label class="control-label">Academic Track</label>
                    <input type="text" class="form-control" data-id="{{ !empty($student_info->MajorDiscID) ? encode($student_info->MajorDiscID) : '' }}" value="{{ getObjectValue($student_info, 'Tracks') }}" disabled id="current-academic-track" name="current-academic-track">
                </div>
                <div class="form-group">
                    <label class="control-label">Curriculum</label>
                    <input type="text" class="form-control" data-id="{{ !empty($student_info->CurriculumID) ? encode($student_info->CurriculumID) : '' }}" value="{{ getObjectValue($student_info, 'Curriculum') }}" disabled id="current-academic-curriculum" name="current-academic-curriculum">
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="form-section">Shift To</h4>
                <form class="academic-shifting-form" id="academic-shifting">
                    <div class="form-group">
                        <label class="control-label">School Year</label>
                        <select name="school-term" id="school-term" class="form-control select2 input-sm school-term">
                            <option value="0"></option>
                            @if(!empty($term))
                                @foreach($term as $t)
                                <option value="{{ $t->TermID }}" >{{ $t->AcademicYear.' '.$t->SchoolTerm }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Academic Track</label>
                        <select name="academic-track" id="academic-track" class="form-control select2 input-sm academic-track">
                            <option value="0"></option>
                            @if(!empty($academic_track))
                                @foreach($academic_track as $at)
                                <option value="{{ $at->IndexID }}">{{ $at->MajorDiscDesc }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Curriculum</label>
                        <select id="academic-curriculum" name="academic-curriculum" class="form-control select2 input-sm academic-curriculum">
                            <option value="0"></option>
                            @if(!empty($academic_curriculum))
                                @foreach($academic_curriculum as $ac)
                                <option data-parent-id="{{ $ac->MajorID }}" class="curriculum" value="{{ $ac->IndexID }}">{{ $ac->CurriculumCode }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Section</label>
                        <select id="available-sections" name="available-sections" class="form-control select2 input-sm available-sections">
                            <option value="0"></option>
                            @if(!empty($available_sections))
                                @foreach($available_sections as $as)
                                <?php $total = $service->getTotalEnrolledbySection($as->TermID, $as->SectionID); ?>
                                    @if($total < $as->Limit )
                                    <option data-term-id="{{$as->TermID}}" data-curr-id="{{ $as->CurriculumID }}" class="sections" value="{{ encode($as->SectionID) }}">{{ $as->SectionName }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-actions right">
                		<div class="row">
                			<div class="col-md-offset-3 col-md-9">
                				<button type="button" class="btn green shift-academic-track">Shift Academic Track</button>
                			</div>
                		</div>
                	</div>
                    <input type="hidden" name="stud-prog-id" id="stud-prog-id" value="{{ !empty($student_info->ProgID) ? $student_info->ProgID : 0 }}">
                    <input type="hidden" name="shift-current-section" id="shift-current-section" value="{{ !empty($student_registration->ClassSectionID) ? $student_registration->ClassSectionID : 0 }}">
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><i class="icon-bubbles"></i> Shifting History</h3></div>
            <div class="panel-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Former Major Track</th>
                                <th>Former Curriculum</th>
                                <th>Former Section</th>
                                <th>New Major Track</th>
                                <th>New Curriculum</th>
                                <th>New Section</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @if(!empty($shift_history))
                            @foreach($shift_history as $sh)
                            <tr>
                                <td> {{ $i }} </td>
                                <td> {{ date('M d, Y h:i A', strtotime($sh->ShiftDate)) }} </td>
                                <td> {{ $sh->OldMajorName}} </td>
                                <td> {{ $sh->OldCurriculumName}} </td>
                                <td> {{ $sh->OldSectionName}} </td>
                                <td> {{ $sh->NewMajorName}} </td>
                                <td> {{ $sh->NewCurriculumName}} </td>
                                <td> {{ $sh->NewSectionName}} </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
