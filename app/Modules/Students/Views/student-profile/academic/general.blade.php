<?php $_this = !empty($family_info) ? $family_info : array();?>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="acad-general-form" id="acad-general-form">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Date Admitted</label>
                        <div class="input-icon">
                            <i class="icon-calendar"></i>
                            <input type="text" class="form-control datepicker " name="acad-admit-date" id="acad-admit-date" value="" placeholder="MM/DD/YYYY">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Year Level</label>
                        <select name="acad-year-lvl" id="acad-year-lvl" class="select2 form-control select2-profile" placeholder="- Select -">
                            <option value=""></option>
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' - '.$ayt->SchoolTerm }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Session</label>
                        <select name="acad-session" id="acad-session" class="select2 form-control select2-profile" placeholder="- Select -">
                            <option value=""></option>
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' - '.$ayt->SchoolTerm }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <!--  -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Academic Program</label>
                        <select name="acad-program" id="acad-program" class="select2 form-control select2-profile" placeholder="- Select -">
                            <option value=""></option>
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' - '.$ayt->SchoolTerm }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Major Study</label>
                        <select name="acad-maj-study" id="acad-maj-study" class="select2 form-control select2-profile" placeholder="- Select -">
                            <option value=""></option>
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' - '.$ayt->SchoolTerm }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <!--  -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Curriculum Code</label>
                        <select name="acad-program" id="acad-program" class="select2 form-control select2-profile" placeholder="- Select -">
                            <option value=""></option>
                            @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' - '.$ayt->SchoolTerm }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Table of Fees</label>
                        <div class="input-group">
							<input type="text" class="form-control">
							<span class="input-group-btn">
							    <button class="btn blue" type="button"><i class="fa fa-search"></i> Find</button>
							</span>
						</div>
                    </div>
                </div>
            </div>
            <!--  -->
        </form>
    </div>
</div>
<div class="form-actions right">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="button" class="btn default">Cancel</button>
            <button type="button" class="btn green" id="acad-general-save-btn">Save</button>
        </div>
    </div>
</div>
