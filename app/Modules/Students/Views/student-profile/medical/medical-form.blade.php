<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="medical-form">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="form-section">Medical Examination</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="table-responsive"> -->
                        <table class="table table-bordered table-responsive datatables-no-search">
                            <thead class="flip-content">
                                <tr>
                                    <th>Name</th>
                                    <th>Comment/Results</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    <!-- </div> -->
                </div>
            </div>

        </form>
    </div>
</div>
