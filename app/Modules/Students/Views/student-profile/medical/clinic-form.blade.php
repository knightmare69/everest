<div class="">
    <?php
	  $data    = ((isset($data))?$data:array());
	  $studno  = ((@array_key_exists('studno',$data) && $data['studno']!=0)?($data['studno']):false);
	  $cid     = 0;
	  
	  $termid  = 0;
	  $tdate   = date('m/d/Y');
	  $tstart  = date('h:i A');
	  $tend    = date('h:i A');
	  $doctor  = '';
	  $compl   = '';
	  $diag    = '';
	  $treat   = '';
	  $category= '';
	  $supply  = '';
	  if($studno){
	   $cid  = ((@array_key_exists('cid',$data) && $data['cid']!=0)?($data['cid']):0);
	   $exec = DB::select("SELECT * FROM ES_StudentsMedicalRecords WHERE StudentNo='".$studno."' AND IndexID='".$cid."'");
	   if($exec && count($exec)>0){
	      $indxid  = $exec[0]->IndexID;
		  $termid  = $exec[0]->TermID;
		  $tdate   = date('m/d/Y',strtotime($exec[0]->Date));
		  $tstart  = $exec[0]->TimeIn;
		  $tend    = $exec[0]->TimeOut;
		  $doctor  = $exec[0]->Physician;
		  $compl   = $exec[0]->Complaint;
		  $diag    = $exec[0]->Diagnosis;
		  $treat   = $exec[0]->Treatment;
		  $category= $exec[0]->Category;
		  $supply  = $exec[0]->Supplies;
	   }
	  }
	?>
    <form id="clinic_records" class="clinic-records">
	    <?php
		if(isset($indxid)){
		  echo '<input type="hidden" id="indexid" name="indexid" value="'.$indxid.'"/>';
		}
		?>
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                  <label class="control-label">School Year</label>
                  <select name="cf-ac-year" id="cf-ac-year" value="" class="form-control select2">
                      @if(!empty($academic_year))
                      @foreach($academic_year as $ayt)
                      <option value="{{ encode($ayt->TermID) }}" <?php echo ((substr($ayt->AcademicYear,0,4)==date('Y') || $ayt->TermID==$termid)?'selected':'');?>>{{ $ayt->AcademicYear  }}- {{$ayt->SchoolTerm}}</option>
                      @endforeach
                      @else
                      <option value="">No selection.</option>
                      @endif
                  </select>
              </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Date Entry</label>
                    <div class="input-icon">
                        <i class="icon-calendar"></i>
                        <input type="text" class="form-control datepicker-on-modal" name="cf-date-entry" id="cf-date-entry" value="{{ date('m/d/Y',strtotime($tdate))}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Time In And Out</label>
                    <div class="input-icon">
                        <i class="icon-clock"></i>
                        <input type="text" class="form-control" name="cf-timein" id="cf-timein" value="{{ date('H:i A',strtotime($tstart))}}">
                    </div>
                    <div class="input-icon">
                        <i class="icon-logout"></i>
                        <input type="text" class="form-control" name="cf-timeout" id="cf-timeout" value="{{ date('H:i A',strtotime($tend))}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="conrol-label">Nurse / Physician</label>
                    <div class="input-icon">
                        <i class="fa fa-user-md"></i>
                        <input type="text" name="cf-nurse-phys" id="cf-nurse-phys" class="form-control" value="{{$doctor}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="control-label">Complaint</label>
                    <textarea name="cf-complaint" id="cf-complaint" class="form-control">{{$compl}}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="control-label">Diagnosis</label>
                    <textarea name="cf-diagnosis" id="cf-diagnosis" class="form-control">{{$diag}}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="control-label">Category</label>
					<?php
					  $cat_rs = DB::SELECT("SELECT DISTINCT c.* FROM (SELECT DISTINCT Category FROM ES_StudentsMedicalRecords WHERE Category<>'' AND Category IS NOT NULL
					                         UNION ALL
											 SELECT CategoryName FROM ES_StudentsMedicalRecords_Category WHERE Inactive<>1) as c");
					  if($cat_rs && count($cat_rs)>0){
					    echo '<select data-target="cf-laboratory" id="sel-laboratory" class="form-control">';
					    echo '<option value="" selected disabled> - Select one - </option>';
					    echo '<option value="-1">Not in the list</option>';
					    foreach($cat_rs as $rs){
					    echo '<option value="'.$rs->Category.'" '.(($rs->Category==$category)?'selected':'').'>'.$rs->Category.'</option>';
					    }
					    echo '</select>';
					  }
					?>
                    <input type="<?php echo (($cat_rs && count($cat_rs)>0)?'hidden':'text');?>" name="cf-laboratory" id="cf-laboratory" class="form-control" value="{{$category}}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
				    <label for="" class="control-label">Medicine/Supplies Used</label>
					<?php
					  $supp_rs = DB::SELECT("SELECT DISTINCT Supplies FROM ES_StudentsMedicalRecords WHERE Supplies<>'' AND Supplies IS NOT NULL");
					  if($supp_rs && count($supp_rs)>0){
					    echo '<select data-target="cf-supplies" id="sel-supplies" class="form-control">';
					    echo '<option value="" selected disabled> - Select one - </option>';
					    echo '<option value="-1">Not in the list</option>';
					    foreach($supp_rs as $rs){
					    echo '<option value="'.$rs->Supplies.'" '.(($rs->Supplies==$supply)?'selected':'').'>'.$rs->Supplies.'</option>';
					    }
					    echo '</select>';
					  }
					?>
					<input type="<?php echo (($supp_rs && count($supp_rs)>0)?'hidden':'text');?>" name="cf-supplies" id="cf-supplies" class="form-control" value="{{$supply}}"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="" class="control-label">Treatment</label>
                    <textarea name="cf-treatment" id="cf-treatment" class="form-control">{{$treat}}</textarea>
                </div>
            </div>
		</div>	
    </form>
</div>
