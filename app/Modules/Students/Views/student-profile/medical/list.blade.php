<div class="table-responsive" id="medicalrecord_list">
    <table id="medicalrecords_tbl" class="table table-striped table-bordered table-hover dataTable no-footer">
        <thead class="flip-content">
            <tr>
                <th></th>
                <th>Date</th>
                <th>Complaint</th>
                <th>Category</th>
                <th>Diagnosis</th>
                <th>Physician</th>
            </tr>
        </thead>
        <tbody>
          @if(!empty($list))
            @foreach($list as $l)
              <tr data-id="{{$l->IndexID}}">
                <td width="50px;"><i class="fa fa-search text-info btn-mview"></i>
				    <i class="fa fa-edit text-warning btn-medit"></i>
				    <i class="fa fa-trash-o text-danger btn-mdelete"></i></td>
                <td>{{date('Y-m-d', strtotime($l->Date))}}<br/><small>{{date('h:i A', strtotime($l->TimeIn)).' to '.date('h:i A', strtotime($l->TimeOut))}}</small></td>
                <td>{{$l->Complaint}}</td>
                <td>{{$l->Category}}</td>
                <td>{{$l->Diagnosis}}</td>
                <td>{{$l->Physician}}</td>
              </tr>
			  <tr class="td-details hidden" data-id="{{$l->IndexID}}">
				<td class="text-center"><i class="fa fa-times btn-mhide"></i></td>
				<td colspan="7">
				    <div class="col-sm-12">
						<label><b>Physician: </b>{{$l->Physician}}</label>
					</div>
				    <div class="col-sm-12">
						<label><b>Complaint: </b>{{$l->Complaint}}</label>
					</div>
				    <div class="col-sm-6">
						<label><b>Category: </b>{{$l->Diagnosis}}</label>
					</div>
				    <div class="col-sm-6">
						<label><b>Diagnosis: </b>{{$l->Diagnosis}}</label>
					</div>
				    <div class="col-sm-6">
						<label><b>Treatment: </b>{{$l->Treatment}}</label>
					</div>
				    <div class="col-sm-6">
						<label><b>Supplies: </b>{{$l->Supplies}}</label>
					</div>
				</td>
			  </tr>
            @endforeach
          @endif

        </tbody>
    </table>
 </div>
