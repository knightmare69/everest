<div class="form-actions right">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="button" class="btn blue input-sm" id="med-clinic-showform">New</button>
            <button type="button" class="btn red input-sm" id="med-clinic-delete">Delete</button>
        </div>
    </div>
</div>
<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="clinic-records">
            <div class="row">
                <div class="col-md-12">
                    @include($views.'medical.list')
                </div>
            </div>

        </form>
    </div>
</div>
