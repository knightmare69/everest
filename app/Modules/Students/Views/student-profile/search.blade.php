<div class="well well-sm" style="margin-bottom: 5px;">
    <form class="form-inline right" id="search-form">
        <div class="form-group">
            <select class="form-control select2 input-sm" name="search-campus" id="search-campus">
                @if(!empty($campus))
                    @foreach($campus as $_campus)
                    <option value="{{ encode($_campus->CampusID) }}">{{ $_campus->ShortName }}</option>
                    @endforeach
                @else
                <option value="">No selection.</option>
                @endif
            </select>
        </div>
        <div class="form-group hide">
            <select name="search-ac-year" id="search-ac-year" class="form-control select2 input-sm" placeholder="Select School Year / Semester">
                <option value=""></option>
                @if(!empty($academic_year))
                    @foreach($academic_year as $ayt)
                    <option value="{{ encode($ayt->TermID) }}">{{ $ayt->AcademicYear.' - '.$ayt->SchoolTerm }}</option>
                    @endforeach
                    <option value="0">All</option>
                @else
                    <option value="">No selection.</option>
                @endif
            </select>
        </div>
        <div class="form-group hide">
            <select name="search-level" id="search-level" class="form-control select2 input-sm" placeholder="Select Year Level">
                <option value=""></option>
                @if(!empty($year_level))
                    @foreach($year_level as $yl)
                    <?php $semestral = ($yl->ProgID == 29) ? 1 : 0; ?>
                    <option value="{{ encode($yl->YLID_OldValue) }}" data-prog="{{encode($yl->ProgID)}}" data-by-sem="{{ $semestral }}">{{ $yl->YearLevelName }}</option>
                    @endforeach
                @else
                    <option value="">No selection.</option>
                @endif
            </select>
        </div>
        <div class="form-group input-large">
            <input type="text" class="form-control" id="student-search-text" name="student-search-text" placeholder="Student Name or Student Number" style="width: 100%">
        </div>
        <div class="form-group hide">
            <label><input type="checkbox" name="is-official-enrolled" id="is-official-enroled"> Officially enrolled students only?</label>
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-primary btn-sm" id="btn-search"><i class="fa fa-search"></i> Search</button>
            <i class="fa fa-refresh fa-spin hide" id="search-spinner" style="margin-left: 5px;"></i>
        </div>
        <div class="form-group hide">
            <button type="button" class="btn green btn-sm" id="btn-new"><i class="fa fa-pencil-square-o"></i> New</button>
        </div>
    </form>
</div>
