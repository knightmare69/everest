<div class="portlet purple box dvlist">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-list"></i> Student Ledger
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
			  <label class="control-label col-sm-5 col-md-3 col-lg-2 text-right">StudentName:</label>
			  <div class="col-sm-7 col-md-6 col-lg-5">
				 <?php
				 if(isset($children) && $children){
					echo '<select class="form-control" id="studno" name="studno">';
					echo '<option value="">Select...</option>';
						foreach($children as $c){
						  echo '<option value="'.$c->StudentNo.'" data-yrlvl="'.$c->YearLevelID.'">'.$c->xFullName.'</option>';
						}
					echo '</select>';
				 }else if(isset($filter) && $filter){
					echo '<input type="hidden" class="form-control" id="studno" name="studno"/>';
					echo '<input type="text" id="studname" name="studname" placeholder="Search for student here"/>'; 
				 }else{
					echo '<input type="hidden" class="form-control" id="studno" name="studno"/>';
				 }
				 ?>		 
				 <span class="help-block"></span>
			  </div>
			</div>
			<div class="col-md-12">
			  <label class="control-label col-sm-5 col-md-3 col-lg-2 text-right">Grade Level:</label>
			  <br/><hr/>
			</div>
			<div class="col-md-12 stdledger">
			  <?php echo $ledger;?>
			</div>
		</div>
	</div>
</div>	