<form class="docu-form">
    <div class="row">
        <div class="col-md-12">
            @include('errors.event')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Folder name</label>
                <div class="input-icon">
                    <i class="icon-folder-alt"></i>
                    <input type="text" class="form-control" name="docu-folder-name" id="docu-folder-name">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Reference Date</label>
                <div class="input-icon">
                    <i class="icon-calendar"></i>
                    <input type="text" class="form-control datepicker-on-modal" name="docu-date" id="docu-date">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Particular</label>
                <textarea name="docu-particular" id="docu-particular" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Remarks</label>
                <textarea name="docu-remarks" id="docu-remarks" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section">Attachments</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead class="flip-content">
                        <tr>
                            <th width="80%">Name</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>
                                <div class="">
                                    <a href="return: false" class="btn btn-xs blue">
                                        <i class="fa fa-download"></i>
                                    </a>
                                    <!-- <a href="return: false" class="btn btn-xs green">
                                        <i class="fa fa-upload"></i>
                                    </a> -->

                                    <a href="return: false" class="btn btn-xs red">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    <input type="file" class="docu-upload-file hide" name="docu-upload-file" id="docu-upload-file">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
