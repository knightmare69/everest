<div class="portlet light bg-inverse">
    <div class="portlet-body form">
        <form class="docu-records">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="table-responsive"> -->
                        <table class="table table-bordered table-responsive datatables">
                            <thead class="flip-content">
                                <tr>
                                    <th>Folder Name</th>
                                    <th>Date</th>
                                    <th>Particular</th>
                                    <th>Remarks</th>
                                    <th>Has Attachment</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    <!-- </div> -->
                </div>
            </div>
        </form>
    </div>
</div>
<div class="form-actions right">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="button" class="btn blue input-sm" id="docu-showform">New</button>
        </div>
    </div>
</div>
