<?php
    $mTerm = new \App\Modules\Setup\Models\AcademicYearTerm; 
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
    $term = $service->getAcademicYear();
    $prev_term = $mTerm->whereRaw("TermID <> '". $term->TermID ."' AND SchoolTerm = '".($term->SchoolTerm == '2nd Semester' ? '1st Semester' : '2nd Semester')."'")->orderBy('AcademicYear', 'Desc')->first();
    $stud = getUserID2();
    $model = new \App\Modules\Students\Models\StudentProfile;
    $config = $model->termConfiguration($term->TermID);
    $info = $model->getStudentInfo($stud);
    $reg = $service->getEnrollmentbyTermNIDno($term->TermID,$info->StudentNo );
    $status = "Not yet register";
    $promoted = $service->IsPromoted($prev_term->TermID,$info->StudentNo );
    $allow = true;
    
    if(!empty($reg)){
        if( ($reg->RegID) != '' ){
            $status = "Registered";
        }
        if( $reg->ValidationDate != ''){
            $status = "Validated";
        }
    }
    
    if($promoted == '0'){
        $status = "Not yet Promoted";
        $allow = true;
    }
    
    
    //$bal = $service->getOutstandingBalance($stud);
    $bal = getOutstandingBalance('1', $stud, systemDate(),$term->TermID );
    if($bal > 0 ) $allow = false;
    
?>
@if($config->count() == 0 )
    <div class="alert alert-danger display-hide" style="display: block;">
	   <button data-close="alert" class="close"></button>
	   <i class="fa fa-lg fa-warning"></i> <b>Attention!</b> Enrollment Dates was not properly configured!. Try again later.
    </div>
@else
    <?php
        $setup = $config->first();
        $date1=date_create($setup->PublishFrom  );
        $date2=date_create($setup->PublishTo );
        $date3=date_create(systemDate());
        $diff=date_diff($date1,$date3);
        $diff1=date_diff($date2,$date3);
        
        $alloToEnroll = '0';
        if( in_array($info->MajorDiscID, explode(',',$setup->AllowedToEnroll) ) ){
            $alloToEnroll = '1';
        }
        $ts1 = strtotime($setup->PublishFrom);
        $ts2 = strtotime($setup->PublishTo);
        $ts3 = strtotime(systemDate());
        
        $seconds_diff = $ts3 - $ts1;        
        $seconds_diff1 = $ts3 - $ts2;                
    ?>
    
    @if(intval($seconds_diff) < 0  || intval($seconds_diff1) > 0 || $alloToEnroll == '0' )
        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-yellow-crusta">
								<i class="icon-share font-yellow-crusta"></i>
								<span class="caption-subject bold uppercase"> Enrollment Schedule </span>
								<span class="caption-helper"></span>
							</div>
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="#" onclick="location.reload();">
								    <i class="icon-refresh"></i>
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<div class="slimScrollDiv">
								<h4 class="bold">Attention</h4>
								<p>
									 Enrollment date start : <?= $date1->format('m-d-Y') ?> 08:00 AM
								</p>
								<p>
									 Enrollment date end : <?= $date2->format('m-d-Y') ?> 05:00 PM
								</p>
							</div>
                            <div class="slimScrollDiv">
								<h4 class="bold">Allowed To Enroll</h4>
								 <table id="tbltrack" class="table table-bordered table-condensed">
                                    @foreach($service->GetAcademicTrack() as $at)                        
                                        <tr>
                                            
                                            <?php
                                                $checked = in_array($at->IndexID, explode(',',$setup->AllowedToEnroll) );
                                                                                                 
                                            ?>
                                            
                                            <td class="autofit">
                                                @if($checked == '1')
                                                    <i class="fa fa-check fa-fw fg-lg font-green"></i>
                                                @endif                                                 
                                            </td>
                                            <td>{{ $at->MajorDiscDesc }}</td>
                                        </tr>                        
                                    @endforeach                                             
                                </table>
							</div>
                            
						</div>
					</div>
                    
            </div> 
        </div>                                     
        
    @else
        <div class="row">
        	<div class="col-md-12">
        		<div class="portlet box blue" id="form_wizard_1">
        			<div class="portlet-title">
        				<div class="caption">
        					<i class="fa fa-gift"></i> Enrollment Procedure - <span class="step-title">
        					Step 1 of 4 </span>
        				</div>
        				<div class="tools hidden-xs">
        					<a href="javascript:;" class="collapse"></a>
        					<a href="#portlet-config" data-toggle="modal" class="config"></a>
        					<a href="javascript:;" class="reload"></a>
        					<a href="javascript:;" class="remove"></a>
        				</div>
        			</div>
        			<div class="portlet-body form">
                  
                                    
        				<form action="#" class="form-horizontal" id="submit_form" method="POST">
        					<div class="form-wizard">
        						<div class="form-body">
        							<ul class="nav nav-pills nav-justified steps">
        								<li>
        									<a href="#tab1" data-toggle="tab" class="step">
        									   <span class="number"> 1 </span>
        									   <span class="desc"><i class="fa fa-check"></i> Enrollment Setup </span>
        									</a>
        								</li>
        								<li>
        									<a href="#tab2" data-toggle="tab" class="step">
        									<span class="number">
        									2 </span>
        									<span class="desc">
        									<i class="fa fa-check"></i> Advising </span>
        									</a>
        								</li>
        								<li>
        									<a href="#tab3" data-toggle="tab" class="step active">
        									<span class="number">
        									3 </span>
        									<span class="desc">
        									<i class="fa fa-check"></i> Confirmation </span>
        									</a>
        								</li>
        								<li>
        									<a href="#tab4" data-toggle="tab" class="step">
        									<span class="number">
        									4 </span>
        									<span class="desc">
        									<i class="fa fa-check"></i> Pre-Registration </span>
        									</a>
        								</li>
        							</ul>
        							<div id="bar" class="progress progress-striped" role="progressbar">
        								<div class="progress-bar progress-bar-success">
        								</div>
        							</div>
        							<div class="tab-content">
                                          @if($promoted=='0')
                                    <div class="form-group">
                                        <label class="control-label col-md-3">&nbsp;</label> 
                                        <div class="col-md-6">                                                                                
                                        <div class="alert alert-danger alert-dismissable">
                                            <i class="fa fa-warning"></i> Alert! Unable to proceed enrollment. Not yet promoted. Thank you!
                                        </div>
                                        </div>                                    
                                    </div>
                                    @endif
                                    
        								<div class="alert alert-danger display-none">
        									<button class="close" data-dismiss="alert"></button>
        									You have some form errors. Please check below.
        								</div>
        								<div class="alert alert-success display-none">
        									<button class="close" data-dismiss="alert"></button>
        									Your form validation is successful!
        								</div>
        								<div class="tab-pane active" id="tab1">
        									@include($views.'tab1')
        								</div>
        								<div class="tab-pane" id="tab2">
        									<h3 class="block">Select your schedules</h3>
        									@include($views.'tab2')
        								</div>
        								<div class="tab-pane" id="tab3">
        									@include($views.'tab3')
        								</div>
                                         <div class="tab-pane" id="tab4">
        									<h3 class="block">Successfully registered!</h3>
                                            <div class="form-group">
                                            	<label class="control-label col-md-3"> Academic Year &amp; Term <span class="required"> * </span></label>
                                            	<div class="col-md-4">
                                            		<input type="text" class="form-control" onkeypress="return false;"  name="yearterm" value="{{ $term->AYTerm }}"/>
                                            		<span class="help-block"></span>
                                            	</div>
                                            </div>
                                            <div class="form-group">
                                            	<label class="control-label col-md-3">Campus <span class="required">
                                            	* </span>
                                            	</label>
                                            	<div class="col-md-4">
                                            		<input type="text" class="form-control" onkeypress="return false;"  name="campus" value="San Beda College Alabang" />
                                            		<span class="help-block"></span>
                                            	</div>
                                            </div>
        									<div class="form-group">
        										<label class="control-label col-md-3">Registration ID<span class="required">
        										* </span>
        										</label>
        										<div class="col-md-4">
        											<input type="text" class="form-control" onkeypress="return false;" data-key="<?= !empty($reg) ? encode($reg->RegID) : '' ?>" id="regid" value="<?= !empty($reg) ? ($reg->RegID) : '' ?>" />
        											<span class="help-block"></span>
        										</div>
        									</div>
        									<div class="form-group">
        										<label class="control-label col-md-3">Registered Date <span class="required">
        										* </span>
        										</label>
        										<div class="col-md-4">
        											<input type="text" class="form-control" onkeypress="return false;"  id="regdate" value="{{$reg->RegDate or ''}}" />
        											<span class="help-block">
        											</span>
        										</div>
        									</div>
        									<div class="form-group">
        										<label class="control-label col-md-3">Status <span class="required">
        										* </span>
        										</label>
        										<div class="col-md-4">
        											<input type="text" class="form-control" onkeypress="return false;"  id="regstatus" value="{{$status}}" />
        											<span class="help-block">
        											</span>
        										</div>
        									</div>
                                        </div>
        							</div>
                                    @if($bal>0)
                                    <div class="form-group">
                                        <label class="control-label col-md-3">&nbsp;</label> 
                                        <div class="col-md-6">                                                                                
                                        <div class="alert alert-danger alert-dismissable">
                                            <i class="fa fa-warning"></i> Alert! Unable to proceed enrollment due to your outstanding balance. Please settle your account before proceeding to enrollment. Thank you!
                                        </div>
                                        </div>                                    
                                    </div>
                                    @endif                                                                        
                                    @if($allow && $promoted == '1')
                                   	<div class="form-actions">
            							<div class="row">
            								<div class="col-md-offset-3 col-md-9">
            									<a href="javascript:;" class="btn default button-previous">
            									<i class="m-icon-swapleft"></i> Back </a>
            									<a href="javascript:;" class="btn blue button-next">
            									   Continue <i class="m-icon-swapright m-icon-white"></i>
            									</a>
            									<a href="javascript:;" id="btnsubmit" class="btn green button-submit">
            									Print <i class="m-icon-swapright m-icon-white"></i>
            									</a>
            								</div>
            							</div>
            						</div>
                                    @endif                                    
        						</div>                                       					
        					</div>
        				</form>
        			</div>
        		</div>
        	</div>
        </div>
    @endif
@endif