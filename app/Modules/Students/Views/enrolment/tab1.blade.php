<?php
    
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
    
    $i=1;
    $term = $service->getAcademicYear(); 
    
    $stud = getUserID2();

    $model = new \App\Modules\Students\Models\StudentProfile;
    $info = $model->getStudentInfo($stud);
    $reg = $service->getEnrollmentbyTermNIDno($term->TermID,$info->StudentNo );
        
    $status = "Not yet registered";
    if( !empty($reg)){            
        if( $reg->RegID != ''){
            $status = "Registered";
        }
        
        if( $reg->ValidationDate != ''){
            $status = "Validated";
        }
    }
               
?>
<div class="form-group">
	<label class="control-label col-md-3"> Academic Year &amp; Term <span class="required"> * </span></label>
	<div class="col-md-4">
		<input type="text" class="form-control" onkeypress="return false;"  name="yearterm" value="{{ $term->AYTerm }}"/>
		<span class="help-block"></span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Campus <span class="required">
	* </span>
	</label>
	<div class="col-md-4">
		<input type="text" class="form-control" onkeypress="return false;"  name="campus" value="San Beda College Alabang" />
		<span class="help-block"></span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Department <span class="required"> * </span></label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="department" onkeypress="return false;" value="Senior Highschool Department" />
		<span class="help-block"> </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Academic Track <span class="required"> * </span></label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="track" onkeypress="return false;" value="{{$info->Tracks}}" />
		<span class="help-block"> </span>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Curriculum <span class="required"> * </span></label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="curriculum" onkeypress="return false;" value="{{$info->Curriculum}}" />
		<span class="help-block"> </span>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Current Grade Level <span class="required"> * </span></label>
	<div class="col-md-4">
		<input type="text" class="form-control" name="yearlevel" onkeypress="return false;" value="{{$info->YearLevelName}}" />
		<span class="help-block"> </span>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Status <span class="required"> * </span></label>
	<div class="col-md-4">
		<input type="text" class="form-control" id="retention" name="retention" onkeypress="return false;" value="{{$status}}" />
		<span class="help-block"></span>
	</div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Outstanding Balance <span class="required">
    * </span>
    </label>
    <div class="col-md-4">
    	<input type="text" class="form-control" onkeypress="return false;"  id="balance" value="{{number_format($bal,2)}}" />
    	<span class="help-block">
    	</span>
    </div>
</div>