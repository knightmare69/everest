<?php 
    //$stud = getUserID2();
    //$service = new \App\Modules\Students\Services\StudentProfileServiceProvider;    
    $sections = $service->getClassSections($stud);
?>

<div class="form-group">
	<label class="control-label col-md-3"> Class Section </label>
	<div class="col-md-4">
		<select name="sections" id="sections" class="form-control">
			<option value=""></option>
            @foreach($sections as $r)
                <?php
                    $total = $service->getTotalEnrolledbySection($r->TermID, $r->SectionID);                    
                ?>
                @if($total < $r->Limit )
                <option value="{{ encode($r->SectionID) }}">{{$r->SectionName . ' ('. $total .'/'. $r->Limit   .')'}}</option>
                @endif
                
			@endforeach
		</select>
	</div>
</div>
<div id="schedules">                                    
    @include($views.'.advising.subjects')
</div>
<hr />
<div class="form-group">        
    <label class="control-label col-md-3"> Select Option </label>
    <div class="col-sm-4">
        <div class="input-group">
			<div class="icheck-list">			
               <label>
                    <input type="radio" value="0" class="icheck" name="book" data-title="TextBook (Php 3,362.00) " /> TextBook <b class="red">(Php 3,362.00)</b>
               </label>
               <label class="">
                    <input type="radio" value="1" class="icheck" name="book" data-title="E-Book (Php 3,160.00)" checked="" /> E-Book <b class="red">(Php 3,160.00)</b>
               </label>    			
            </div>
        </div>                                                  
	</div>			                    
</div>							