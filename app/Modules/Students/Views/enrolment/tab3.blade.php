<h3 class="block">Confirm your enrollment</h3>
<h4 class="form-section">credentials</h4>
<div class="form-group">
	<label class="control-label col-md-3">Academic Year &amp; Term:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="yearterm">
		</p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Campus:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="campus">
		</p>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Department:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="department">
		</p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Track:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="track">
		</p>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-md-3">Curriculum:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="curriculum">
		</p>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Year Level:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="yearlevel">
		</p>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Retention:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="retention">
		</p>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3">Section:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="sections">
		</p>
	</div>
</div>

<h4 class="form-section">Registered Subject/s</h4>

<div class="form-group" >
	<div class="col-md-12" id="enrolledsubj">
    
    </div>
</div>

<h4 class="form-section">Option</h4>

<div class="form-group" >
    <label class="control-label col-md-3">Book Option:</label>
	<div class="col-md-4">
		<p class="form-control-static" data-display="book">
		</p>
	</div>
</div>
