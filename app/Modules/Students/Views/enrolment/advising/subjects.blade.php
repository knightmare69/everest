<?php
    global $data;
     
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
    $subjects = $service->getStudentRegularLoad();
    
    $data = $service->getClassSchedules($section);    
    $i=1;    
    
    function getSchedID($subj){
        $output = '' ;
        global $data;
        foreach($data As $s){
            
            if($subj == $s->SubjectID){
                $output = $s;
                break;
            }
            
        }   
        return $output; 
    }
    
?>
<table class="table table-bordered table-condensed table-striped " >
        <thead>
            <tr>
                <th>#</th>
                <th>Code</th>
                <th>Descriptive</th>
                <th class="text-center">Hour(s)</th>
                <th class="text-center">Section</th>
                <th class="text-center">Schedule</th>
                <th class="text-center">Room</th>
                <th class="text-center">Faculty</th>
                <th class="hidden">Action</th>
            </tr>        
        </thead>
        <tbody>
            @foreach($subjects AS $g)
            <?php $sched = getSchedID( $g->SubjectID); ?>
            <tr data-id="{{ encode($g->SubjectID) }}"  data-sched="{{$sched->ScheduleID or ''}}"  >
                <td class="autofit">{{$i}}.</td>
                <td class="autofit">{{$g->SubjectCode}}</td>
                <td class="autofit">{{$g->SubjectTitle}}</td>            
                <td class="autofit text-center ">{{$g->LectHrs}}</td>
                <td class="autofit text-center ">{{$sched->SectionName or ''}}</td>
                <td class="schedule">
                    {{$sched->Sched_1 or ''}}
                    @if( !empty($sched->Sched_2))
                    <br /> {{ $sched->Sched_2 or ''}}
                    @endif 
                     @if( !empty($sched->Sched_3))
                    <br /> {{ $sched->Sched_3 or ''}}
                    @endif                                         
                </td>
                <td class="room">
                    {{$sched->Room1 or ''}}
                    @if( !empty($sched->Room2))
                    <br /> {{ $sched->Room2 or ''}}
                    @endif
                    
                </td>
                <td class="faculty">
                    {{$sched->FacultyName or ''}}
                    @if( !empty($sched->FacultyName2)  && $sched->FacultyName != $sched->FacultyName2 )
                    <br /> {{ $sched->FacultyName2 or ''}}
                    @endif
                
                </td>
                <th class="autofit hidden">
                    <a href="javascript:void(0);">Select</a> | <a href="javascript:void(0);">Remove</a>
                </th>
                
            </tr>
            <?php $i++; ?>
            @endforeach
        </tbody>
    </table>