<?php $yearterm = 0 ; ?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div id="policy" class="portlet light" >
            <div class="portlet-title">
        		<div class="caption"><i class="fa fa-list"></i>
        			<span class="caption-subject bold uppercase">Subjects Enrolled</span>
        			<span class="caption-helper hidden-xs">use this module to view your subject...</span>
        		</div>
                <div class="actions">
                
                    <div class="portlet-input input-inline input-medium">
                        <select class="form-control input-sm" name="term" id="term">
                            @if(!empty($term))
                            @foreach($term as $t)
                                <option  value="{{encode($t->RegID)}}" data-term="{{encode($t->TermID)}}" data-reg="{{$t->RegID}}" data-code="{{$t->TemplateCode}}" data-rdate="" data-odate="{{$t->ValidationDate}}"> {{ $t->Term  }} </option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <a class="btn btn-default btn-sm" id="btnref" href="javascript:;"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="subjects" class="tabpane" style="display: none;">
                    <div class="table-scrollable ">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>