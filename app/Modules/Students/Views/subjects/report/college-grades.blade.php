<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Copy of Grades</title>
    <style>
    *,
    *::before,
    *::after {
      margin: 0;
      padding: 0;
      box-sizing: inherit;
    }

    :root {
      --height: 297mm;
      --width: 210mm;
    }

    html {
      box-sizing: border-box;
      font-family: Arial, Helvetica, sans-serif;
    }

    body {
      text-align: left;
      background: #ccc;
      color: #2a2a2a;
      line-height: 1.5;
    }

    hr {
      margin: 10px 0;
      border: 0;
      border-top: 2px solid #c0c0c0;
      box-sizing: content-box;
      height: 0;
      overflow: visible;
    }

    @font-face {
      font-family: "CloisterBlack";
      src: url("fonts/CloisterBlack.ttf");
    }

    .container {
      max-width: var(--width);
      height: var(--height);
      margin: 20px auto;
      background: #fff;
      padding: 20px 40px;
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
      flex: 1;
      position: relative;

    }

    .instname {
      /* font-family: 'Old English Text MT'; */
      font-family: 'Helvetica';
      font-weight: 1000;
      text-transform : uppercase;
    }

    .header {
      display: flex;
    }

    .header-left,
    .header-right {
      flex: 0 0 200px;
    }

    .header-left {
      text-align: center;
    }

    .header-left img {
      width: auto;
      height: 80px;
    }

    .header-center {
      flex: 1;
      text-align: center;
      margin-top: 20px;
    }

    .header-center p {
      font-size: 10px;
      margin-bottom: 10px;
    }


    .header-right {
      text-align: right;
    }

    .header-right h6:nth-last-child(3) {
      font-size: 12px;
      color: #d12218;
      margin-bottom: 20px;
    }

    .header-right h6:nth-last-child(2) {
      font-size: 10px;
    }

    .header-right img {
      max-width: 40px;
      height: auto;
    }

    .section-student-info {
     /* background: #f3f3f3;*/
      /*border: 2px solid #808080;*/
      padding: 5px 0px;
      /*box-shadow: 5px 5px 1px 1px rgba(128, 128, 128, 0.4);*/
    }

    .section-student-fees {
      margin-top: 10px;
      position: relative;
            /*Copy this to background image*/

      /*background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 1)), url("{{url("assets/admin/layout/img/logo.png")}}");*/
      background-repeat: no-repeat;
      background-position: center;
      background-size: 50%;
      /* up to here*/
    }

    .section-student-fees p {
      font-size: 8px;
      font-weight: 600;
      margin-top: 15px;
    }

    .section-student-fees p span {
      padding-right: 5px;
    }

    .section-student-notice {
      position: relative;
      margin-top: 10px;
    }

    .section-student-notice::before {
      position: absolute;
      content: "";
      top: -8px;
      left: -40px;
      width: calc(100% + 80px);
      min-height: 1px;
      border-top: 1px dashed #2a2a2a;
    }
    .section-student-notice p {
      font-size: 8px;
      font-weight: 600;
      font-style: italic;
    }

    .section-student-notice h6 {
      margin: 8px 0;
    }

    .section-student-notice ol li {
      font-size: 8px;
      font-weight: 500;
      margin-left: 8px;
    }

    .section-student-notice ul li {
      list-style-type: none;
      margin-left: -1px;
      display: flex;
      align-items: center;
    }

    .section-student-notice ul li label {
      width: 90px;
      position: relative;
    }

    .section-student-notice ul li label::before {
      content: ":";
      position: absolute;
      top: 0;
      right: 0;
    }

    .section-student-notice ul li input {
      height: 10px;
      border: 0;
      border-bottom: 1px solid rgba(111, 111, 111, 0.8);
      margin-left: 5px;
      width: 280px;
    }

    .section-header {
      display: flex;
      align-items: center;
    }

    .section-header h6:first-child {
      font-size: 11px;
    }

    .section-header h6:last-child {
        font-size: 9px;
        margin-left: 0;
        letter-spacing: 20px;
        border: 1px solid black;
        padding-left: 102px;
        padding-right: 100px;
    }

    .section-footer {
      display: flex;
      margin-top: 5px;
    }

    .table {
      border-collapse: collapse;
      width: 100%;
    }

    .table tr td {
      font-size: 8px;
      vertical-align: middle;
      font-weight: 500;
    }

    .table-group {
    flex: 3;
    border: 1px solid black;
    padding: 5px;
    }

    .box {
      width: 15px;
      height: 15px;
      border: 1px solid #2a2a2a;
      margin-right: 5px;
    }

    .input-group {
      margin-top: 5px;
      display: flex;
      align-items: flex-end;
    }

    .input-group label {
      font-size: 8px;
      font-weight: 600;
      width: 50px;
    }

    .input-group input {
      border: 0;
      border-bottom: 1px solid #2a2a2a;
      width: 100%;
    }

    .input-group + .checkbox {
      margin-top: 10px;
    }
    
    .section-student-Load{
        margin-top: 10px;
        border: 1px solid black;
    }
    

    .footer { margin-top: 30px; }

    /* Helper class */
    .u-d-flex { display: flex; }
    .u-align-center { align-items: center; }
    .u-align-end { align-items: flex-end; }
    .u-justify-center { justify-content: center; }
    .u-justify-between { justify-content: space-between; }
    .u-text-center { text-align: center; }
    .u-text-right { text-align: right !important; }
    .font-xs { font-size: 9px; }

    
    .pad-r5{ padding-right: 5px; }
    .pad-r10{ padding-right: 10px; }

    .table-student-subjects th {
      font-size: 10px;
      text-align: center;
    }

    #img-logo{height: 50px;}

    @page {
      size: letter;
      margin: 10;
    }

    @media print {
      .container {
        margin: auto;
        width: auto;
        height: auto;
        box-shadow: none;
        page-break-after: always;
      }

      body {
        -webkit-print-color-adjust: exact !important;
      }

      html,
      body {
        height: 100%;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden;
      }

    }


    </style>
    {{-- <link rel="stylesheet" href="style.css" /> --}}

  </head>

<?php

$page_title = "Pre-Assessment Form";
$totalUnit=0;
$totalLab=0;

$temp = 0;

    $data = DB::table("es_permanentrecord_master AS r")
            ->leftJoin("es_students as s", "s.StudentNo", "=", "r.StudentNo")
            ->leftJoin("es_ayterm as y", "y.TermID", "=", "r.TermID")
            ->selectRaw("s.*,r.TermID, r.YearLevelID, r.RegID,
                        fn_Age(s.DateofBirth,now()) as Age,
                        fn_Nationality(s.NationalityID) as Nationality,
                        fn_Religion(s.ReligionID) as Religion,
                        concat(s.Res_Address, ' ',s.Res_Street, s.Res_Barangay, ' ', s.Res_TownCity ) as Address,
                        fn_RegStudentStatus(s.TermID,r.TermID) as RegStatus, fn_YearLevel(r.YearLevelID) as YearLevel,
                        r.ProgramID, r.StudentNo, fn_StudentName(r.StudentNo) As StudentName, fn_AcademicYearTerm(r.TermID) As Term,
                        y.SchoolTerm As SchoolTerm, r.Final_Average,
                        fn_ProgramName(s.ProgID) As Program, fn_MajorName(s.MajorDiscID) As Major ")
            ->where(['r.TermID' => $term, 'r.StudentNo'=> $idno])->first();

    $jdata= $data;

    $subj = DB::table("es_permanentrecord_details AS d")
            ->where(['d.TermID' => $term, 'd.StudentNo'=> $idno])
            ->get();

    $header = DB::table("es_institution")->first();
    $campus = DB::table("es_campus")->where('CampusID',1)->first();
    
    $title = $header->Report_InstitutionName;
    $address = $header->Report_CompleteAddress;

    $trimester= '';

    switch($data->SchoolTerm){
        case '1st trimester': $trimester = 1; break;
        case '2nd trimester': $trimester = 2; break;
        case '3rd trimester': $trimester = 3; break;
    }

    $status= 'Regular';
    $gwa = $data->Final_Average;
    
    $registrar = $campus->Registrar;
    $registrar_position = 'Registrar';
    

?>
  <body>
    <div class="container">
      <header style="font-family: 'Century Gothic'; " class="">
        <table style="width: 100%;">
            <tr>
                <td>
                    <img id="img-logo" src="{{url("assets/system/img-reports/mol-report-header.png")}}" height="" alt="logo" />
                </td>
                <td>
                 <table style="width: 100%;">
                    <tr>
                        <td style="font-weight: bold; text-align: right;">OFFICE OF REGISTRATION AND RECORDS</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 7pt; text-align: right;">{{$address}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 7pt; text-align: right;">4114 &bull; Tel. Nos. 046 4814800 &bull; <a href="mailto:registrar@mmma.edu.ph?Subject=Inquiry">registrar@mmma.edu.ph</a></td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>

      </header>

      <div class="header-center">
        <h4>COPY OF GRADES</h4>
      </div>

      <section class="section-student-info">
        <table class="table-font-small table">
          <tbody>
            <tr>
              <td width="120">STUDENT NO.:</td>
              <td width="400"><strong>{{$data->StudentNo}}</strong></td>

            <!--   <td width="40">STATUS:</td>
              <td><strong>{{$data->RegStatus}}</strong></td>
 -->
              <td width="80">Academic Year:</td>
              <td><strong>{{$data->Term}}</strong></td>
            </tr>

            <tr>
              <td>Student Name::</td>
              <td><strong> {{$data->StudentName}}</strong></td>

              <td width="35">Trimester:</td>
              <td>{{$trimester}}</td>
            </tr>

            <!-- <tr>
              <td>College:</td>
              <td><strong> {{$data->College}}</strong></td>
              <td width="35">Year Level:</td>
              <td><strong>{{$data->YearLevel}}</strong></td>
            </tr> -->

            <tr>
              <td>Program:</td>
              <td><strong> {{$data->Program}}</strong></td>
              <td width="35">Status: </td>
              <td><strong>{{$status}}</strong></td>
            </tr>

            <tr>
              <td>Level:</td>
              <td><strong>{{$data->YearLevel}}</strong></td>
              <td width="35">GWA</td>
              <td class="bold">{{$gwa}}</td>
            </tr>
          </tbody>
        </table>
      </section>

      <section class="section-student-Load" >

          <table class="table table-student-subjects" style="border-bottom: 1px solid black; ">
            <thead>
                <tr>
                 <th colspan="6" style="letter-spacing: 20px; ">
                    COPY OF GRADES
                    </th>
                </tr>
            </thead>
          </table>

          <table class="table table-student-subjects" style="margin: 5px;">
            <thead>
                <tr>
                    <th width="60" >CODE</th>
                    <th width="250" >COURSE TITLE</th>
                    <th width="130" >Credit Units</th>
                    <th width="200">FINAL GRADE</th>
                    <th width="90">REMARKS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($subj as $s)
                <tr>
                    <td width="60">{{$s->SubjectCode}}</td>
                    <td width="250">{{$s->SubjectTitle}}</td>
                    <td width="130" class="u-text-center">{{number_format($s->SubjectCreditUnits,2)}}</td>
                    <td width="200" class="u-text-center">{{$s->Final_Average}}</td>
                    <td width="90" class="u-text-center">{{$s->Final_Remarks}}</td>
                </tr>
                <?php
                	$totalUnit += tofloat($s->AcadUnits);
                	$totalLab += tofloat($s->LabUnits);
                ?>
                @endforeach
            </tbody>
            <tfoot>
                
            </tfoot>
          </table>
      </section>

      <section class="section-student-fees">
        <table class="" style="width: 100%; margin-top: 100px;">
            <tr>
                <td width="365"></td>
                <td width="365" class="u-text-center"></td>
            </tr>
            <tr>
                <td width="365"></td>
                <td width="365" class="u-text-center"><h5>{{$registrar}}</h5></td>
            </tr>
            <tr>
                <td width="365"></td>
                <td width="365" class="u-text-center"><h6>{{$registrar_position}}</h6></td>
            </tr>
            <tr>
                <td width="365">&nbsp;</td>
                <td width="365" class="u-text-center"></td>
            </tr>
            <tr>
                <td width="365">&nbsp;</td>
                <td width="365" class="u-text-center"></td>
            </tr>
            <tr>
                <td width="730" colspan="2" class="font-xs">Remarks: This is a computer generated Copy of Grades. no need for original signature.</td>
                
            </tr>
        </table>
            

      </section>



      <footer class="footer">
        <h6>MMMA-ORRFM-005 rev.01</h6>
      </footer>
    </div>
  </body>
</html>
