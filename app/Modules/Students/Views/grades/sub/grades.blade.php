<?php 
    $i=1;
    
    $model = ((isset($model))? (new \App\Modules\Students\Models\StudentProfile):$model);
    $dbcon = new \App\Modules\ClassRecords\Models\eClassRecord_model;
    $mevent = new \App\Modules\ClassRecords\Models\GradeEvent_model;
    $mconduct = new \App\Modules\ClassRecords\Models\Conduct\Conduct_model;
    
	$cnrow = array();
	$rs    = $dbcon->getConductNarrative($term, $stud);
	if($rs && count($rs)>0){
	$cnrow = $rs[0];
	}
	
	$regid='';
	$yrlvl=4;
	$tmpinfo= DB::SELECT("SELECT RegID,y.YearLevelID,y.YearLevelCode,y.YearLevelName 
                            FROM ES_Registrations as r
                      INNER JOIN ESv2_YearLevel as y ON r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID  
                           WHERE r.TermID='".$term."' AND r.StudentNo='".$stud."'");
    if($tmpinfo && count($tmpinfo)>0){
	   $regid = $tmpinfo[0]->RegID;
	   $yrlvl = $tmpinfo[0]->YearLevelID;
	}
    	
	
	$tmpi = 1;
	$tmpe = 2;
	$tmpc = 6;
	$gpa  = 0;
	
	$gpa1 = 0;
	$gpa2 = 0;
	$gpa3 = 0;
	$gpa4 = 0;
    
    if($yrlvl==1){
	   $link = url('assets\kinder\default.pdf').'?t='.date('Ymdis');
       echo '<div class="col-sm-12">';
	   echo '<div data-jay="{{ $progclass}}" data-lynesp="{{$period}}"> 
				<object type="text/html" data="'.$link.'#view=FitH&toolbar=0" width="98%" height="600px" style="overflow:auto;border:1px ridge black"></object>
            </div>';
       echo '</div>';	   
	}else{
?>
<div class="col-sm-12">
<div class="table-responsive">
<table class="table table-bordered table-condensed table-striped table-hover " style="cursor: pointer;">
    <thead>
        <tr>
            <th <?php echo (($shs == 0)?'rowspan="2"':'');?>>#</th>
            <th <?php echo (($shs == 0)?'rowspan="2"':'');?>>Code</th>
            <th <?php echo (($shs == 0)?'rowspan="2"':'');?>>Descriptive</th>
            <th class="text-center hidden" <?php echo (($shs == 0)?'rowspan="2"':'');?>>Units</th>
            <th <?php echo (($shs == 0)?'colspan="2"':'');?> class="text-center">1st</th>
            <th <?php echo (($shs == 0)?'colspan="2"':'');?> class="text-center">2nd</th>
            @if($shs == 0)
            <th <?php echo (($shs == 0)?'colspan="2"':'');?> class="text-center">3rd</th>
            <th <?php echo (($shs == 0)?'colspan="2"':'');?> class="text-center">4th</th>
            @endif
            <th class="text-center" <?php echo (($shs == 0)?'rowspan="2"':'');?>>Final</th>
            
            @if($shs == 1)
            <th class="text-center">Remedial</th>
            <th class="text-center">RFG</th>
            @endif
            
            <th class="text-center" <?php echo (($shs == 0)?'rowspan="2"':'');?>>Remarks</th>
        </tr>
        @if($shs == 0)
        <tr>
            <th class="text-center">Acad</th>
            <th class="text-center">Effort</th>
            <th class="text-center">Acad</th>
            <th class="text-center">Effort</th>
            <th class="text-center">Acad</th>
            <th class="text-center">Effort</th>
            <th class="text-center">Acad</th>
            <th class="text-center">Effort</th>
        </tr>
        @endif		
    </thead>
    <tbody>
        @foreach($grades AS $g)
        <?php		        
	          //$gpa = $g->general_average;
				$trs = DB::SELECT("SELECT ISNULL((SELECT TOP 1 LetterGradeID FROM ESv2_Gradesheet_Setup WHERE TermID=s.TermID AND YearLevelID=s.YearLevelID AND ProgID=s.ProgramID),1) as TemplateID 
										 ,ISNULL((SELECT TOP 1 TransmutationID FROM ESv2_Gradesheet_Setup WHERE TermID=s.TermID AND YearLevelID=s.YearLevelID AND ProgID=s.ProgramID),1) as TransmutationID
										 ,(SELECT Value FROM ES_GS_Setup WHERE SetupID=2) as EffortGrade
									   FROM ES_ClassSchedules as cs INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID WHERE ScheduleID='".$g->ScheduleID."'");
				  
				//echo print_r($trs);
				$tmpc = 6;
				if($trs && count($trs)>0){
					$tmpi = $trs[0]->TemplateID;
					$tmpe = $trs[0]->TransmutationID;
					$tmpc = $trs[0]->EffortGrade;
				}
				
                $css = "";
                if( strtolower($g->Final_Remarks) == 'failed'){
                    $css = "text-danger";
                }
				
				if($g->AverageA=='' &&
				   $g->AverageB=='' &&
				   $g->AverageC=='' &&
				   $g->AverageD==''){
					continue;
				}
				
				if($g->AverageA != ''){
                  $transmutea = $dbcon->gradeRemarks($tmpi, $g->AverageA);
                  $gps        = $transmutea->GradePoint;
				  
				  if($g->GPA1!=''){
					$gpa1   += $g->GPA1;
				  }else{				  
					if($gps<=0){
						$gpa1    += ($g->Weight * $g->AverageA);
					}else{
						$gpa1    += ($g->Weight * $gps);
					}
				  }
				  $g->AverageA= $transmutea->LetterGrade;
                }
				if($g->AverageB != ''){
                  $transmuteb  = $dbcon->gradeRemarks($tmpi, $g->AverageB);
                  $gps        = $transmuteb->GradePoint;
				  if($g->GPA2!=''){
					$gpa2   += $g->GPA2;
				  }else{				  
					if($gps<=0){
						$gpa2    += ($g->Weight * $g->AverageB);
					}else{
						$gpa2    += ($g->Weight * $gps);
					}
				  }
                  $g->AverageB = $transmuteb->LetterGrade;
                }
				if($g->AverageC != ''){
                  $transmutec  = $dbcon->gradeRemarks($tmpi, $g->AverageC);
                  $gps        = $transmutec->GradePoint;
				  if($g->GPA3!=''){
					$gpa3   += $g->GPA3;
				  }else{				  
					if($gps<=0){
						$gpa3    += ($g->Weight * $g->AverageC);
					}else{
						$gpa3    += ($g->Weight * $gps);
					}
				  }
                  $g->AverageC = $transmutea->LetterGrade;
                }
				if($g->AverageD != ''){
                  $transmuted  = $dbcon->gradeRemarks($tmpi, $g->AverageD);
                  $gps        = $transmuted->GradePoint;
				  if($g->GPA4!=''){
					$gpa4   += $g->GPA4;
				  }else{				  
					if($gps<=0){
						$gpa4    += ($g->Weight * $g->AverageD);
					}else{
						$gpa4    += ($g->Weight * $gps);
					}
				  }
                  $g->AverageD = $transmuted->LetterGrade;
                }
				
				if($g->Final_Average != ''){
                  $trans_ave  = $dbcon->gradeRemarks($tmpi, $g->Final_Average);
                  $fave       = $trans_ave->LetterGrade;
				  $rave       = 0;
                  $gps        = $trans_ave->GradePoint;
				  
				  if($gps<=0){
					$rave     = ($g->Weight * $g->Final_Average);
				  }else{
					$rave     = ($g->Weight * $gps);
				  }
				  
				  $g->Final_Average = $fave;
				  $gpa              = $g->general_average;
                }
				
				if($g->ConductA != ''){
                  $transmutea  = $dbcon->gradeRemarks($tmpc, $g->ConductA);
                  $g->ConductA = $transmutea->LetterGrade;
                }
				if($g->ConductB != ''){
                  $transmuteb  = $dbcon->gradeRemarks($tmpc, $g->ConductB);
                  $g->ConductB = $transmuteb->LetterGrade;
                }
				if($g->ConductC != ''){
                  $transmutec  = $dbcon->gradeRemarks($tmpc, $g->ConductC);
                  $g->ConductC = $transmutec->LetterGrade;
                }
				if($g->ConductD != ''){
                  $transmuted  = $dbcon->gradeRemarks($tmpc, $g->ConductD);
                  $g->ConductD = $transmuted->LetterGrade;
                }
        ?>
        <tr class="{{$css}}" data-idx="{{ encode($g->GradeIDX) }}" data-code="{{$g->SubjectCode}}" data-title="{{$g->SubjectTitle}}">
            <td class="autofit font-xs">{{$i}}.</td>
            <td class="autofit font-xs bold code">
                @if($g->ParentSubjectID > 0 )
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
                {{$g->SubjectCode}}
            </td>
            <td class="title">
                @if($g->ParentSubjectID > 0 )
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
                {{ $g->SubjectTitle}}
            </td>
            <td class="units font-xs text-center bold hidden">{{$g->CreditUnit}}</td>
            <td class="text-center avea bold ">{{ $g->AverageA }}</td>
            <td class="text-center effa bold ">{{ $g->ConductA }}</td>
			
            <td class="text-center aveb bold ">{{ $g->AverageB }}</td>
            <td class="text-center effb bold ">{{ $g->ConductB }}</td>
            
            @if($shs == 0)
                <td class="text-center avec bold ">{{ $g->AverageC }}</td>
                <td class="text-center effc bold ">{{ $g->ConductC }}</td>
                
				<td class="text-center aved bold ">{{ $g->AverageD }}</td>  
                <td class="text-center effd bold ">{{ $g->ConductD }}</td>  
            @else
                <td class="bold font-blue-madison text-center">{{$g->TotalAverageAB}}</td>
                <td class="bold font-blue-madison avec text-center">{{$g->AverageC}}</td>      
            @endif               
            
            <td class="bold font-blue-madison text-center">{{$g->Final_Average}}</td>                        
            <td class="bold font-blue-madison text-center">{{$g->Final_Remarks}}</td>
            
        </tr>
        <?php $i++;?>
        @endforeach
		
        <?php 
		if($gpa>0){
		  $gpa  = number_format($gpa,2,'.','');
		  $tgpa = $dbcon->gradeRemarks($tmpi, $gpa);
		  $lgpa = $tgpa->LetterGrade;
		  $rgpa = $tgpa->Remark;
		  
		  echo '<tr>
		          <td colspan="11" class="text-right"><b>GENERAL AVERAGE</b></td>
				  <td class="text-center"><b>'.$lgpa.'</b></td>
				  <td class="text-center"><b>'.$rgpa.'</b></td>
		       </tr>';
		}
		?>
    </tbody>
</table>
</div>
</div>

<div class="col-sm-6">
<table class="table table-bordered table-condense">
	<thead>
		<tr>
			<th>SOCIAL SKILLS</th>
			<th>1st</th>
			<th>2nd</th>
			<th>3rd</th>
			<th>4th</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Acts Responsibly</td>
			<td><?php echo getObjectValue($cnrow,'ConductA1');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductA2');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductA3');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductA4');?></td>
		</tr>
		<tr>
			<td>Extends Consideration, respect for, and service to others</td>
			<td><?php echo getObjectValue($cnrow,'ConductB1');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductB2');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductB3');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductB4');?></td>
		</tr>
		<tr>
			<td>Exhibits leadership in character and virtue</td>
			<td><?php echo getObjectValue($cnrow,'ConductC1');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductC2');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductC3');?></td>
			<td><?php echo getObjectValue($cnrow,'ConductC4');?></td>
		</tr>
	</tbody>
</table>

<table class="table table-bordered table-condense">
	<thead>
		<tr>
			<th>Attendance</th>
			<th>1st</th>
			<th>2nd</th>
			<th>3rd</th>
			<th>4th</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Days Absent</td>
			<td><?php echo getObjectValue($cnrow,'AbsentQ1');?></td>
			<td><?php echo getObjectValue($cnrow,'AbsentQ2');?></td>
			<td><?php echo getObjectValue($cnrow,'AbsentQ3');?></td>
			<td><?php echo getObjectValue($cnrow,'AbsentQ4');?></td>
		</tr>
		<tr>
			<td>Days Tardy</td>
			<td><?php echo getObjectValue($cnrow,'TardyQ1');?></td>
			<td><?php echo getObjectValue($cnrow,'TardyQ2');?></td>
			<td><?php echo getObjectValue($cnrow,'TardyQ3');?></td>
			<td><?php echo getObjectValue($cnrow,'TardyQ4');?></td>
		</tr>
	</tbody>
</table>
</div>

<?php 
    $data= array();
	$cdta= '';
	$prv = '';
	$i   = 0;
	$gsi = DB::SELECT("SELECT * FROM ES_GS_GradingSystem_Details WHERE TemplateID='".$tmpi."' ORDER BY Min DESC");
	$gsc = DB::SELECT("SELECT * FROM ES_GS_GradingSystem_Details WHERE TemplateID='".$tmpc."' ORDER BY Min DESC");
	
	if($gsc && count($gsc)>0){
		foreach($gsc as $c){
		    if($c->Max>0){
			   $cdta .= (($cdta!='')?', ':'').$c->LetterGrade.' - '.$c->Description;
			}
		}
	}
	
	if($gsi && count($gsi)>0){
		foreach($gsi as $r){
		  if($r->LetterGrade!=$prv){
		    $prv = $r->LetterGrade;
			$i++;
			$data[$i] = array('min'=>number_format($r->Min,0,'.',',')
			                 ,'max'=>number_format($r->Max,0,'.',',')
			                 ,'grd'=>($r->LetterGrade)
			                 ,'dsc'=>($r->Description));
		  }else{
		    $data[$i]['min']=number_format($r->Min,0,'.',',');
		  }
		}
?>
<div class="col-sm-6">
  <div class="table-responsive <?php echo (($yrlvl<4)?'hidden':'');?>">
    <?php
	    $gpa1= (($gpa1>0)?number_format($gpa1,3,'.',''):'');
	    $gpa2= (($gpa2>0)?number_format($gpa2,3,'.',''):'');
	    $gpa3= (($gpa3>0)?number_format($gpa3,3,'.',''):'');
	    $gpa4= (($gpa4>0)?number_format($gpa4,3,'.',''):'');
		
		if($gpa1!='' && $gpa2!='' && $gpa3!='' && $gpa4!=''){
		$gpa = ($gpa1+$gpa2+$gpa3+$gpa4)/4;
	    $gpa = number_format($gpa,2,'.','');
		}else
		$gpa = '';
	?>
	<table class="table table-bordered">
	    <thead>
			<th class="text-center">Q1</th>
			<th class="text-center">Q2</th>
			<th class="text-center">Q3</th>
			<th class="text-center">Q4</th>
			<th class="text-center">Final</th>
		</thead>
		<tbody>
			<tr>
				<td class="text-center"><?php echo $gpa1;?></td>
				<td class="text-center"><?php echo $gpa2;?></td>
				<td class="text-center"><?php echo $gpa3;?></td>
				<td class="text-center"><?php echo $gpa4;?></td>
				<td class="text-center"><?php echo $gpa;?></td>
			</tr>
		</tbody>
	</table>
  </div>	
  <div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<th colspan="<?php echo ((count($data)<=8)?2:3);?>" class="text-center">Grade Scale</th>
		</thead>
		<tbody>
			<?php
			if(count($data)<=8){
				foreach($data as $k=>$v){
					echo '<tr><td class="text-right" width="48%" style="border-right:0px !important;">'.$v['min'].'-'.$v['max'].'</td>
							  <td> = '.$v['grd'].(($v['grd']!=$v['dsc'])?(" (".$v['dsc'].")"):'').'</td></tr>';
				}
			}else{
			    $j = 1;
			    for($i=1;$i<=count($data);$i++){
				    $v = $data[$i];
					$cspan = '';
				    if($j==1){echo '<tr>';}
					if($i==count($data)){
					   if($j==1)
					   $cspan = 'colspan="3"'; 
					   elseif($j==2)
					   $cspan = 'colspan="2"';
					}
					echo '<td class="text-center" '.$cspan.'>'.$v['min'].'-'.$v['max'].' = '.$v['grd'].'</td>';
				    if($j==3 || $i==count($data)){
					   $j=0;
					   echo '</tr>';
					}
					$j++;
				}
			}
			?>
		</tbody>
		<tfoot>
		    <tr><td colspan="<?php echo ((count($data)<=8)?2:3);?>" class="text-center"></td></tr>
			<tr><td colspan="<?php echo ((count($data)<=8)?2:3);?>" class="text-center"><?php echo $cdta;?></td></tr>
		</tfoot>
	</table>
  </div>
</div>
<?php
    }
?>

<div class="col-sm-12">
<table class="table table-bordered table-condense">
	<thead>
		<th class="text-center">Narrative/Comments</th>
	</thead>
	<tbody>
		<tr><td><strong>1st Semester:</strong><br/><?php echo getObjectValue($cnrow,'Narrative2');?></td></tr>
		<tr><td><strong>2nd Semester:</strong><br/><?php echo getObjectValue($cnrow,'Narrative4');?></td></tr>
	</tbody>
</table>
</div>

<?php
	}
?>