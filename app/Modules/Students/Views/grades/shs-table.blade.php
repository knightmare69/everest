<table class="table table-bordered table-condensed table-striped ">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Descriptive</th>
            <th>Section</th>
            <th class="text-center">1st</th>
            <th class="text-center">2nd</th>
            <th class="text-center">Final</th>
            <th class="text-center">RFG</th>
            <th class="text-center">Remarks</th>
        </tr>
    </thead>
    <tbody>
        @foreach($grades AS $g)

        @if($g->SubjectID != 3958 && $g->SubjectID != 3959  )
        <?php

            $danger1 = "";
            $danger2 = "";
            $danger3 = "";

            if(($g->AverageA != '') && ( floatval($g->AverageA) < 75 )){
                $danger1 = "text-danger";
            }

            if(($g->AverageB != '') && ( floatval($g->AverageB) < 75 )){
                $danger2 = "text-danger";
            }

            if(round(floatval($g->Final_Average)) < 75  && getObjectValue($conf,'Period1') == 1 && getObjectValue($conf,'Period2') == 1 ){
                $danger3 = "text-danger";
            }

            if(!$g->IsNonAcademic ){
                $ave += round(floatval($g->Final_Average));
                $cnt++;
                $avxe .= $g->Final_Average.',';
            }
        ?>

        <tr>
            <td class="autofit">{{$i}}.</td>
            <td class="autofit">{{$g->SubjectCode}}</td>
            <td>{{$g->SubjectTitle}}</td>
            <td>{{$g->SectionName}}</td>
            <td class="bold text-center {{$danger1}} ">
                @if(getObjectValue($conf,'FirstTerm') == 1)
                {{$g->AverageA}}
                @endif
            </td>
            <td class="bold text-center {{$danger2}}">
                @if( ( getObjectValue($conf,'SecondTerm') == 1 || getUserName() == 'student101'))
                    @if($g->IsPassFail != '1')
                        {{$g->AverageB}}
                    @endif
                @endif
            </td>
            <td class="bold text-center autofit">


                @if( (getObjectValue($conf,'SecondTerm') == 1 && getObjectValue($conf,'FirstTerm') == 1 ) || getUserName() == 'student101' )
                    @if($g->AverageC != '')
                        @if($g->IsPassFail == '1')
                          <?=  number_format(floatval($g->TotalAverageAB),0) > 75 ? 'Passed' : 'Failed'  ?>
                        @else
                          <?php echo number_format(floatval($g->TotalAverageAB),0) ?>
                        @endif

                    @else
                        @if($g->IsPassFail == '1')
                           <?=  number_format(floatval($g->AverageB),0) > 75 ? 'Passed' : 'Failed'  ?>
                        @else
                        @if($g->AverageB != ''  && $g->AverageA != '')
                          <?php echo number_format(floatval($g->Final_Average),0) ?>
                          @endif
                        @endif
                    @endif

                @endif

                @if(getObjectValue($conf,'SecondTerm') == 1 && getObjectValue($conf,'FirstTerm') == 1 && $g->SubjectID == 3894 )
                    @if($g->AverageB == 100)
                        Passed
                    @else
                        Failed
                    @endif

                @endif

            </td>
            <td class="bold text-center {{$danger3}}">
                @if( ( (getObjectValue($conf,'SecondTerm') == 1 && getObjectValue($conf,'FirstTerm') == 1 ) && $g->SubjectID != 3894 ) || getUserName() == 'student101' )
                    @if($g->IsPassFail == '1')

                    @else
                      @if($g->AverageC != '')
                      <?php echo number_format(floatval($g->Final_Average),0) ?>
                      @endif
                    @endif
                @endif
            </td>
            <td class="bold {{$danger3}}">
                @if((getObjectValue($conf,'SecondTerm') == 1 && getObjectValue($conf,'FirstTerm') == 1 ) && $g->SubjectID != 3894 )
                @if($g->AverageB != ''  && $g->AverageA != '')
                    {{$g->Final_Remarks}}
                @endif
                @endif
            </td>
        </tr>
        <?php $i++; ?>
        @endif
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td class="text-right bold" colspan="6">
                Average :
            </td>
            <td class="bold text-center font-lg">
            @if((getObjectValue($conf,'SecondTerm') == 1 && getObjectValue($conf,'FirstTerm') == 1 ) )
                {{round($g->general_average, 2)}}
            @endif
            </td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>
