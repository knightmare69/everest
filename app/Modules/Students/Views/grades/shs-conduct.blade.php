<h3>Conduct Grade </h3>
<table class="table table-condensed table-striped table-bordered">

        <tr>
            <th rowspan="2" class="text-center">CORE VALUES</th>
            <th rowspan="2" class="text-center">BEHAVIOR STATEMENT</th>
            <th colspan="2" class="text-center">TERM</th>
        </tr>
        <tr>
            <th class="text-center">1st Term</th>
            <th class="text-center">2nd Term</th>
        </tr>
    </thead>
    <tbody>

    <tr>
        <td rowspan="2" class="bold text-center">Maka-Diyos</td>
        <td> Expresses one's spiritual beliefs while respecting spiritaul beliefs of others	 </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(1,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if( getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(1,12); ?>
            @endif
        </td>
    </tr>
    <tr>

        <td>Shows adherence to ethical principles by upholding truth</td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(2,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(2,12); ?>
            @endif
        </td>
    </tr>
    <tr>
        <td rowspan="2" class="bold text-center">
            Makatao
        </td>

        <td> Is sensitive to individual, social, and cultural differences </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(3,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if( getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(3,12); ?>
            @endif
        </td>
    </tr>
    <tr>
        <td>Demonstrate contribution towards solidarity</td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(4,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(4,12); ?>
            @endif
        </td>

    </tr>
    <tr>
        <td class="bold text-center" >
            Makakalikasan
        </td>

        <td> Cares for the environment and utilizes resources wisely, judiciously, and economically </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(5,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(5,12); ?>
            @endif
        </td>
    </tr>
    <tr>
        <td rowspan="2" class="bold text-center">
            Makabansa
        </td>

        <td> Demonstrate pride in being a Filipino; exercises the rights and responsibilities of a Filipino citizen. </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(6,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(6,12); ?>
            @endif
        </td>
    </tr>
    <tr>
        <td>Demonstrates appropriate behavior in carrying out activities in the school, community, and country</td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'FirstTerm') == 1)
            <?= getConduct(7,11); ?>
            @endif
        </td>
        <td class="text-center bold">
            @if(getObjectValue($conf,'SecondTerm') == 1)
            <?= getConduct(7,12); ?>
            @endif
        </td>
    </tr>
    </tbody>
</table>
