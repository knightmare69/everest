<?php $yearterm = 0 ; ?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div id="policy" class="portlet light" >
            <div class="portlet-title">
        		<div class="caption"><i class="fa fa-table"></i>
        			<span class="caption-subject bold uppercase"> Report of Grades</span>
        			<span class="caption-helper hidden-xs">use this module to view your grade...</span>
        		</div>
                <div class="actions">
                    @if(getUserGroup() == 'parent')
                    <?php
                    $gcs = new App\Modules\Admission\Services\GuardianConnectServices;
                    // $sps = new App\Modules\Students\Services\StudentProfileServiceProvider;
        
                    $s = $gcs->connectStudentsData();
                    ?>
                    <div class="form-inline">
                        <div class="form-group">
                            <select class="form-control input-sm" name="students" id="students">
                                <option value="" selected disabled> - Select Student - </option>
                                @if(!empty($s))
                                    @foreach($s as $r)
                                    <option value="{{ $r->StudentNo }}">{{ $r->LastName.', '.$r->FirstName.' '.$r->MiddleInitial.' '.$r->ExtName }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
						    <div class="input-group">
                            <select class="form-control input-sm" name="term" id="acad-year-term">
                                <option value="-1"> - Select Academic Year - </option>
                            </select>
							<span class="input-group-addon" id="btnref" style="background:white!important;">
								<i class="fa fa-refresh"></i>
							</span>
							</div>
                        </div>
                    </div>
                    @else
                        <div class="portlet-input input-inline input-medium">
						    <div class="input-group">
                            <select class="form-control input-sm" name="term" id="acad-year-term">
                                <option value="-1"> - Select Academic Year - </option>
                                @if(!empty($academic_year))
                                @foreach($academic_year as $ayt)
                                    <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>
                                @endforeach
                                @endif
                            </select>
							<span class="input-group-addon" id="btnref" style="background:white!important;">
								<i class="fa fa-refresh"></i>
							</span>
							</div>
                        </div>
                    @endif
                    <a class="btn btn-default btn-sm hidden" id="btnref" href="javascript:;"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
            <div class="portlet-body" id="grades"></div>
        </div>
    </div>
</div>