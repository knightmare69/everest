<?php

function getGRade($grade){
	
	switch ($grade) {
		case 1: return 'B' ;break;
		case 2: return 'D' ;break;
    case 3: return 'C' ;break;
    case 0: return 'N' ;break;
    case null: return ''; break;
		
		default:
			return '';
			break;
	}

}
 $rs = DB::select(DB::raw("call sp_k12_Checklist_ReportCard('".$term."','".$stud."','". $section ."','".$yr."','0')"));

$data_ = json_decode(json_encode($rs), true);
$data = array();
$data = array_group_by( $data_, "SectionName", "StudentNo","ClassID","SubClassID","Criteria" );
$period =1 ;
?>

@foreach($data as $a => $b)
<?php
$studentno = '';
$classid = '';
$subclassid ='';
$criteria = '';
?>
@foreach($b as $c => $d)

<?php $x=0; 

$studentno = $c;?>
@foreach ($d as $e => $f)
<?php $x =$e;
$classid = $e;
 ?>
@endforeach
@foreach ($f as $subclass => $g)
<?php 
$subclassid = $subclass;
 ?>
@endforeach
@foreach ($g as $cri => $h)
<?php 
$criteria = $cri;
 ?>
@endforeach

<table class="table table-bordered table-condensed table-striped ">
    <thead>


        <tr class="text-center">
            <th rowspan="2">
                <h2>DEVELOPMENT SKILLS</h2>
            </th>
            <th colspan="4" class="font-sm height-20">
                EVALUATION PERIODS
            </th>
        </tr>

        <tr class="text-center">
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
        </tr>
    </thead>
    <tbody>
        @foreach($d as $e => $f)
        <?php  $classname= '';  ?>
        @foreach($f as $g => $h)
        @foreach($h as $i => $j)
        <?php  $classname= $j[0]['ClassName'];  ?>
        @endforeach
        @endforeach


        <tr>
            <td style="padding-left:2px; font-weight: bold;" colspan="5" class="pl-sm">
                {{$classname}}
            </td>
        </tr>
        @foreach($f as $g => $h)
        <?php  $subclassname= '';  ?>
        @foreach($h as $i => $j)
        <?php  $subclassname= $j[0]['SubClass'];  ?>
        @endforeach
        @if($g <> 0)
            <tr>
                <td  style="padding-left:7px; font-weight: bold;" colspan="5" class="pl-md">
                    {{$subclassname}}
                </td>
            </tr>
            @endif
            @foreach($h as $i => $j)
            <?php
									//   sdd($j);
									  ?>
            <tr>
                <td  style="padding-left:12px;" class="pl-lg">
                    {{$j[0]['Criteria']}}
                </td>
                <td class="text-center">{{1 <= $period ?  getGRade($j[0]['AverageA']) : ''}}</td>
                <td class="text-center">{{2 <= $period ?  getGRade($j[0]['AverageB']) : ''}}</td>
                <td class="text-center">{{3 <= $period ?  getGRade($j[0]['AverageC']) : ''}}</td>
                <td class="text-center">{{4 <= $period ?  getGRade($j[0]['AverageD']) : ''}}</td>
            </tr>


            <?php   $i++; ?>

            @endforeach


            @endforeach
            @endforeach







    </tbody>


</table>
<hr>
<table class="table table-marks" style="margin-bottom:30px; margin-top: 30px;">
        <thead>
            <tr class="height-20">
              <th class="text-right autofit">
                <u>Marking</u>
                </th>
                <th></th>
                <th class="">
                <u>Non-Numerical Rating</u>
                </th>
            </tr>
          </thead>
          <tr>
              <td class="text-right">B</td>
              <td class="text-center">-</td>
              <td>	Beginning</td>
          </tr>
          <tr>
              <td class="text-right">D</td>
              <td class="text-center">-</td>
              <td>Developing</td>
          </tr>
          <tr>
              <td class="text-right">C</td>
              <td class="autofit text-center">-</td>
              <td>	Consistent</td>
          </tr>
     
     
     
      <tr>
          <td class="text-right">N</td>
          <td class="text-center">-</td>
          <td>		Not Yet Observed	</td>
      </tr>
    </table>

@endforeach
@endforeach