<?php
    $decimalPoint = 0;
    $periods = getPeriods(50, $term);
    $conf = $model->termConfiguration($term, 0, $progclass)->first();
    $danger = array(
        1 => ''
       ,2 => '' 
       ,3 => ''
       ,4 => ''
       ,5 => ''
    );
    #temp: as long as there are no modification on the viewing of period policy
    $perAllow = array(
         1 => getObjectValue($conf,'Period1')
        ,2 => getObjectValue($conf,'Period2')
        ,3 => getObjectValue($conf,'Period3')
        ,4 => getObjectValue($conf,'Period4')
        ,5 => 1
    );
    $period_columns = 0;
      
?>
<div class="hidden-xs">
<table class="table table-bordered table-condensed table-striped ">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Descriptive</th>
            <th class="text-center">Units</th>
            <th>Section</th>
            @foreach($periods as $p)
                @if($p->HideInGradeView == '0')
                <th class="text-center" data-id="{{encode($p->PeriodID)}}">{{$p->Description1}}</th>
                <?php $period_columns++; ?>
                @endif                
            @endforeach
            @if($school != 'USTP')        
            <th class="text-center">Final Average</th>
            <th class="text-center">Equivalent Grade</th>
            @endif
            <th class="text-center">Remarks</th>
        </tr>
    </thead>
    <tbody>

        @foreach($grades AS $g)

        <?php
             $danger = array(
                1 => ''
               ,2 => '' 
               ,3 => ''
               ,4 => ''
               ,5 => ''
            );
            $dangerfinal = "";
            $isAllPosted = false;
            $dateposted = $g->DatePosted;
            
            if($g->AverageA != '' && $g->AverageB != '' && $g->AverageC != '' && $g->AverageD != ''){
                $isAllPosted = true;
            }

            if(($g->AverageA != '') && ( floatval($g->AverageA) < 75 ) && $school != 'USTP'){
                $danger[1] = "text-danger";
            }
            

            if(($g->AverageB != '') && ( floatval($g->AverageB) < 75 ) && $school != 'USTP'){
                $danger[2] = "text-danger";
            }
            if(($g->AverageC != '') && ( floatval($g->AverageC) < 75 )){
                $danger[3] = "text-danger";
            }
            if(($g->AverageD != '') && ( floatval($g->AverageD) < 75 )  && $school != 'USTP'){
                $danger[4] = "text-danger";
            }

            if(round(floatval($g->Final_Average)) < 75  && $period1 == 1 && $period2==1 && $period3  == 1  && $period4  == 1 ){
                $dangerfinal = "text-danger";
            }

            if(!$g->IsNonAcademic ){
                $ave += round(floatval($g->Final_Average));
                $cnt++;
                $avxe .= $g->Final_Average.',';
            }
        ?>

        <tr>
            <td class="autofit">{{$i}}.</td>
            <td class="autofit">{{$g->SubjectCode}}</td>
            <td>{{$g->SubjectTitle}}</td>
            <td class="text-center">{{$g->CreditUnit}}</td>
            <td>{{$g->SectionName}}</td>
            @foreach($periods as $p)
                @if($p->HideInGradeView == '0')
                    <?php 
                    if( $g->{'Average'.$p->LetterLabel} == 'CRD') {
                        $danger[$p->NumberLabel] = '';
                    }
                    ?>
                <td data-d="{{$perAllow[$p->NumberLabel]}}" dd-pof="{{$g->IsPassFail}}" class="bold text-center {{$danger[$p->NumberLabel]}} ">                    
                    @if( ( $perAllow[$p->NumberLabel] == 1 && $g->IsPassFail != '1')  || getUserName() == 'student101'  )                    
                          {{$g->{'Average'.$p->LetterLabel} > 9  ? number_format($g->{'Average'.$p->LetterLabel}, $decimalPoint) : $g->{'Average'.$p->LetterLabel}  }}                    
                    @endif                     
                </td>
                @endif
            @endforeach  
            <!-- 
            <td class="bold text-center {{$danger1}} ">
                @if($period1 == 1 )
                @if($g->IsPassFail != '1')
                  {{$g->AverageA}}
                @endif
                @endif
            </td>
            <td class="bold text-center {{$danger2}}">
                @if( ( $period2 == 1 || getUserName() == 'student'))
                @if($g->IsPassFail != '1')
                  {{$g->AverageB}}
                @endif
                @endif
            </td>
            <td class="bold text-center {{$danger3}} ">
                @if($period3 == 1)
                {{$g->AverageC}}
                @endif
            </td>
            <td class="bold text-center {{$danger4}}">
                @if( ( $period4 == 1 || getUserName() == 'student'))
                {{$g->AverageD}}
                @endif
            </td>
            -->

            @if($school != 'USTP')    
            <td class="bold text-center autofit">
                @if( ($period1 == 1 && $period2 == 1 && $period3  == 1  && $period4  == 1  && $isAllPosted) || getUserName() == 'student' || $dateposted != '' )
                   {{ $g->Final_Average != '' ?  $g->Final_Average > 9 ? number_format($g->Final_Average, $decimalPoint) :  $g->Final_Average : '' }}
                @endif
            </td>
            <td  data-posted="{{$dateposted}}" class="bold text-center {{$g->Final_Remarks == 'Failed' ? 'text-danger': ''}}">
              @if(  ($period1 == 1 && $period2 == 1 && $period3  == 1  && $period4  == 1 && $isAllPosted ) || getUserName() == 'student' || $dateposted != '' )
              {{($g->Final_Equivalent > 9 ? number_format($g->Final_Equivalent, $decimalPoint) : ($g->Final_Equivalent))}} 
              @endif
            </td>
            @endif
            <td data-posted="{{$dateposted}}" class="bold {{$g->Final_Remarks == 'Failed' ? 'text-danger': ''}}">
              @if( ($period1 == 1 && $period2 == 1 && $period3  == 1  && $period4  == 1 && $isAllPosted ) || getUserName() == 'student'  ||  $dateposted != '')
                    {{$g->Final_Remarks}}                    
              @else
                    Not Yet Posted
              @endif
            </td>
        </tr>
        <?php $i++; ?>

        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td class="text-right bold" colspan="5">
                Average :
            </td>
            <?php
                for($x=1;$x<$period_columns;$x++){
                    echo "<td></td>";
                }
            ?>
            <td class="bold text-center font-lg">
            @if( ($period1 == 1 && $period2 == 1 && $period3  == 1  && $period4  == 1 ) || getUserName() == 'student' )
                {{-- {{round($g->general_average, 2)}} --}}
            @endif
            </td>
            @if($school != 'USTP')    
            <td></td>
            <td></td>
            @endif
            <td></td>
        </tr>
    </tfoot>
</table>
</div>
<div class="hidden-sm hidden-md hidden-lg">
    <ul class="list-group">
    @foreach($grades AS $g)
        <li class="list-group-item">
             Subject : <strong>{{$g->SubjectCode}}</strong> <br />
			 <strong>{{$g->SubjectTitle}}</strong> <br />
             Grade : <strong><?=  number_format(floatval($g->Final_Average),2) ?></strong> &emsp; Remarks : <b class="<?= strtolower($g->Final_Remarks)=='passed'?'text-success':'text-danger' ?>">{{$g->Final_Remarks}}</b>
        </li>
    @endforeach
    </ul>
</div>
<hr />
{{-- <a href="{{url('/'). "/registrar/report-card/" . "print?snum=". $stud. "&report-type=report-of-grades&pclass=".encode($progclass)."&period=24&academic-term=".encode($term)}}" target="_blank" class="btn btn-sm btn-default" data-mode="cog-college" ><i class="fa fa-print"></i> Print</a> --}}