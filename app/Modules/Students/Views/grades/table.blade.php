<?php
    global $conduct;
    $i=1;
    $school = DB::table('es_institution')->value("ReportName");

    $model = new \App\Modules\Students\Models\StudentProfile;
    $conf = $model->termConfiguration($term)->first();

    $stud_account = DB::table('es_studentaccountabilities')->where('StudentNo', $stud)->where('TermID', $term)->where('Cleared', 0);
    $stud_allow = DB::table('es_allowtoviewgrades')->where('StudentNo', $stud)->where('TermID', $term)->count();

    $reg = DB::table('es_permanentrecord_master as r')
                ->leftJoin('es_programs as p', 'p.ProgID', '=', 'r.ProgramID')
                ->selectRaw("r.*, p.ProgID,p.ProgName as Program, p.ProgClass")
                ->where('r.StudentNo', $stud)->where('r.TermID', $term)->first();
                
    $section = isset($reg) ? $reg->ClassSectionID : '';
    $prog = isset($reg) ? $reg->Program : '';
    $progclass = isset($reg) ? $reg->ProgClass :  '';
    $yr = isset($reg) ? $reg->YearLevelID : '';
    $conf = $model->termConfiguration($term, 0, $progclass)->first();
    $shs  = 0;
    // dd($conf);

    $period1 = getObjectValue($conf,'Period1');
    $period2 = getObjectValue($conf,'Period2');
    $period3 = getObjectValue($conf,'Period3');
    $period4 = getObjectValue($conf,'Period4');
    $accountabilities = array();
    
    if( $stud_account->count() > 0){
        $accountabilities = $stud_account->get();
    }

    $conduct = $model->getConductGrade($term, $stud)->orderBy('PeriodID','asc')->get();

    function getConduct($idx, $per){
        global $conduct;
        $output='';

        foreach($conduct as $r){
            if ($r->ConductID == $idx && $r->PeriodID == $per ){
                $output = $r->Score;
                break;
            }
        }
        return $output;
    }


    $progid = getObjectValue($reg, 'ProgID');

    $ave = 0;
    $avxe = '';
    $cnt = 0;
    
    $activated = true;

    $termBalance = getOutstandingBalance(1,$stud,systemDate(),0,$term);
    $outstandingBalance = getOutstandingBalance(1 , $stud, systemDate());

?>
<style type="text/css">
    #form_survey div.form-body{
        padding: 10px !important;
    }
    #form_survey div.form-actions{
        background-color: #f5f5f5;
    }
</style>


@if($period1 == 0 )
    <div class="alert alert-warning display-hide" style="display: block;">
	   <button data-close="alert" class="close"></button>
	   <i class="fa fa-lg fa-warning"></i> <b>Attention!</b> Grades are not available at this moment. Please get in touch with the Registrar at registrar@everestmanila.edu.ph.
    </div>
@endif

@if($outstandingBalance > 0  )
    @if($termBalance > 0  )
        <?php $activated = false; ?>
        <div class="alert alert-danger hide" style="display: block;">
    	   <button data-close="alert" class="close"></button>
    	   <i class="fa fa-lg fa-warning"></i><b>Attention!</b> You have remaining balance. <b>PHP {{number_format($outstandingBalance,2)}}</b>
        </div>
    @endif
@endif

@foreach ($accountabilities as $a)
    <div class="alert alert-warning display-hide" style="display: block;">
        <button data-close="alert" class="close"></button>
        <i class="fa fa-lg fa-warning"></i> <b>Accountabilities</b> {{$a->Reason}}
    </div>   
@endforeach

<?php 
$activated = true;
if($stud_account->count() > 0 ){
	$activated = false;
}else{
	$activated = true;
}	
?>

@if($activated)
	<div class="alert alert-danger display-hide" style="display: block;">
	    <button data-close="alert" class="close"></button>
	    <i class="fa fa-lg fa-warning"></i> <b>Warning!</b> Falsification, alteration or tampering of the report generated
	    by the system is considered a major and a criminal offense, hence, these are punishable by expulsion or dismissal
	    from the Institution.
	</div>
	<div class="alert alert-info " style="display: block;">
	    <button data-close="alert" class="close"></button>
	    <i class="fa fa-lg fa-info"></i> <b>Note!</b> If you wish to have a copy of the official report card kindly send an e-mail to registrar@everestmanila.edu.ph. Thank you!
	</div>
        
    
    @if($progclass < 30)

        @if($period1 != 0 || $period2 != 0 )                        
            <?php
            /** Temporary Condtion
             * activated by jayz 05272020
            $period =  $progclass == 21 ? 12: 4;
            if(getUserName() == 'parent101' || getUserName() == 'parent102' || getUserName() == 'parent102' ||  getUserName() == 'parent104' || 'activated' == 'activated'){
                $link = url("/")."/registrar/report-card/print?programs=".encode($reg->ProgID)."&report-type=report-card&academic-term=".encode($term)."&period=".encode($period)."&year-level=".encode($yr)."&section=".encode($section)."&snum=".$stud.'&pclass='.encode($progclass);
            }
			
            <div class="hidden" data-jay="{{ $progclass}}" data-lynesp="{{$period}}"> 
				<object type="text/html" data="{{$link}}#view=FitH&toolbar=0" width="98%" height="600px" style="overflow:auto;border:5px ridge blue"></object>
            </div>
			*/
			?>
			
            <div class="row">
				<div class="col-sm-12">
					@include($views.'sub.grades')
				</div>
			</div>
        @endif

    @else

        @if($progclass == '21')
            @include($views.'shs-table')
        @elseif($progclass == '20')
            @include($views.'gs-jhs-table')           
        @elseif($prog == 'Pre-School')
            @include($views.'kinder-table')
        @else
            @include($views.'college-table')
        @endif
    
    @endif
@else
	<div class="alert alert-danger">
		<i class="fa fa-lg fa-warning"></i> <b>Attention!</b> Grades are not available at this moment. Please get in touch with the Registrar at registrar@everestmanila.edu.ph.
	</div>
@endif
<hr />

@if(floatval($progclass) <= 21)
    @if(getUserGroup() != 'parent')
    <div class="row">
        @include('Home.Views.students.survey')
    </div>
    @endif
@endif