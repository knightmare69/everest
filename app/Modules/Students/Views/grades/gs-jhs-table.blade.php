<table class="table table-bordered table-condensed table-striped ">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Descriptive</th>
            <th>Section</th>
            <th class="text-center">1st</th>
            <th class="text-center">2nd</th>
            <th class="text-center">3rd</th>
            <th class="text-center">4th</th>
            <th class="text-center">Final</th>
            <th class="text-center">RFG</th>
            <th class="text-center">Remarks</th>
        </tr>
    </thead>
    <tbody>

        @foreach($grades AS $g)

        @if($g->SubjectID != 3958 && $g->SubjectID != 3959  )
        <?php

            $danger1 = "";
            $danger2 = "";
            $danger3 = "";
            $danger4 = "";
            $dangerfinal = "";

            if(($g->AverageA != '') && ( floatval($g->AverageA) < 75 )){
                $danger1 = "text-danger";
            }

            if(($g->AverageB != '') && ( floatval($g->AverageB) < 75 )){
                $danger2 = "text-danger";
            }
            if(($g->AverageC != '') && ( floatval($g->AverageC) < 75 )){
                $danger3 = "text-danger";
            }
            if(($g->AverageD != '') && ( floatval($g->AverageD) < 75 )){
                $danger4 = "text-danger";
            }

            if(round(floatval($g->Final_Average)) < 75  && getObjectValue($conf,'SecondTerm') == 1 && getObjectValue($conf,'FirstTerm') == 1 ){
                $dangerfinal = "text-danger";
            }

            if(!$g->IsNonAcademic ){
                $ave += round(floatval($g->Final_Average));
                $cnt++;
                $avxe .= $g->Final_Average.',';
            }
        ?>

        <tr>
            <td class="autofit">{{$i}}.</td>
            <td class="autofit">{{$g->SubjectCode}}</td>
            <td>{{$g->SubjectTitle}}</td>
            <td>{{$g->SectionName}}</td>
            <td class="bold text-center {{$danger1}} ">
                @if(getObjectValue($conf,'FirstTerm') == 1 )
                @if($g->IsPassFail != '1')
                  {{$g->AverageA}}
                @endif
                @endif
            </td>
            <td class="bold text-center {{$danger2}}">
                @if( ( getObjectValue($conf,'SecondTerm') == 1 || getUserName() == 'student'))
                @if($g->IsPassFail != '1')
                  {{$g->AverageB}}
                @endif
                @endif
            </td>
            <td class="bold text-center {{$danger3}} ">
                @if(getObjectValue($conf,'SecondTerm') == 1)
                {{$g->AverageC}}
                @endif
            </td>
            <td class="bold text-center {{$danger4}}">
                @if( ( getObjectValue($conf,'SecondTerm') == 1 || getUserName() == 'student'))
                {{$g->AverageD}}
                @endif
            </td>
            <td class="bold text-center autofit">

                @if( (getObjectValue($conf,'FourthTerm') == 1 && getObjectValue($conf,'ThirdTerm') == 1 && getObjectValue($conf,'SecondTerm') == 1 &&  getObjectValue($conf,'FirstTerm') == 1 ) || getUserName() == 'student' )
                        @if($g->IsPassFail == '1')
                           <?=  number_format(floatval($g->AverageB),0) > 75 ? 'Passed' : 'Failed'  ?>
                        @else
                          <?php echo number_format(floatval($g->Final_Average),0) ?>
                        @endif
                @endif
            </td>
            <td class="bold text-center {{$dangerfinal}}">
              @if( (getObjectValue($conf,'FourthTerm') == 1 && getObjectValue($conf,'ThirdTerm') == 1 && getObjectValue($conf,'SecondTerm') == 1 &&  getObjectValue($conf,'FirstTerm') == 1 ) || getUserName() == 'student' )

                    @if($g->IsPassFail == '1')

                    @else
                      @if($g->AverageC != '')
                      <?php echo number_format(floatval($g->Final_Average),0) ?>
                      @endif
                    @endif
                @endif
            </td>
            <td class="bold {{$dangerfinal}}">
              @if( (getObjectValue($conf,'FourthTerm') == 1 && getObjectValue($conf,'ThirdTerm') == 1 && getObjectValue($conf,'SecondTerm') == 1 &&  getObjectValue($conf,'FirstTerm') == 1 ) || getUserName() == 'student' )
                    {{$g->Final_Remarks}}
                @endif
            </td>
        </tr>
        <?php $i++; ?>
        @endif
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td class="text-right bold" colspan="8">
                Average :
            </td>
            <td class="bold text-center font-lg">
              @if( (getObjectValue($conf,'FourthTerm') == 1 && getObjectValue($conf,'ThirdTerm') == 1 && getObjectValue($conf,'SecondTerm') == 1 &&  getObjectValue($conf,'FirstTerm') == 1 ) || getUserName() == 'student' )
                {{round($g->general_average, 2)}}
            @endif
            </td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>
