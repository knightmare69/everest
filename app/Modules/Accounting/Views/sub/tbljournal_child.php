<?php
$payment  = new App\Modules\Accounting\Services\Assessment\Assessment;
$detail   = ((isset($detail))?$detail:array());
$assess   = ((isset($assess))?$assess:array());
$otherfee = array();
$reginfo  = array();
$validdate= '';
$templnme = '';
$tscheme  = 0;

$termid   = ((@array_key_exists('termid',$details))?$details['termid']:0);
$studno   = ((@array_key_exists('studno',$details))?$details['studno']:0);
$regid    = ((@array_key_exists('regid',$details))?$details['regid']:0);
$templid  = ((@array_key_exists('feesid',$details))?$details['feesid']:0);

$tassess    = 0;
$tnetassess = 0;
$tdiscount  = 0;
$tonnet     = 0;
$tpaymenta  = 0;
$tpaymentb  = 0;
$tpaymentc  = 0;
$tpaymentd  = 0;
$tpaydmcm   = 0;

if($termid!=0 && $studno!=0){
$reginfo  = DB::select("SELECT * FROM ES_Registrations WHERE TermID='".$termid."' AND StudentNo='".$studno."'");
if($reginfo && count($reginfo)>0){
  $validdate = $reginfo[0]->ValidationDate;
}

$otherfee = DB::select("SELECT a.AcctCode,a.AcctName,j.* 
                          FROM ES_Journals as j INNER JOIN ES_Accounts as a ON j.AccountID=a.AcctID 
						 WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND TransID=1 AND j.AccountID NOT IN (SELECT DISTINCT AccountID FROM ES_TableofFee_Details)");
}

if($templid>0){
  $tmpfee   = DB::select("SELECT * FROM ES_TableOfFees WHERE TemplateID='".$templid."'");
  $templnme = (($tmpfee && count($tmpfee)>0)?($tmpfee[0]->TemplateCode):'');
  $tscheme  = (($tmpfee && count($tmpfee)>0)?($tmpfee[0]->PaymentScheme):0);
}elseif($templid=='-1'){
  $templnme = 'Custom/Monthly';
  $tscheme  = 3; 
}

/*
echo '<tr class="info">
		 <td colspan="29">
		   <button class="btn btn-xs green '.(($validdate=='')?'btn-tblfee':'disabled').'"><i class="fa fa-table"></i> Template:<b class="templname" data-id="'.$templid.'">'.$templnme.'</b></button>
		 </td>
      </tr>'; 
*/ 
if($assess && count($assess)>0){
 //echo '<tr><td class="text-center" colspan="29"><pre>';
 //var_dump($assess);
 //echo '</pre></td></tr>';  
 echo '<thead><th colspan="2"></th>
			  <th>Assessment</th>
			  <th>Discount</th>
			  <th class="hidden">On Net</th>
			  <th>Net</th>
			  <th>Upon Enrollment</th>
			  <th class="'.(($tscheme<1)?'hidden':'').'">2nd Payment</th>
			  <th class="'.(($tscheme<2)?'hidden':'').'">3rd Payment</th>
			  <th class="'.(($tscheme<2)?'hidden':'').'">4th Payment</th>
			  <th colspan="6" class="hidden"></th></thead>';
			  
 foreach($assess as $k=>$r){
  $idno               = $r['IDNo'];
  $reserve            = $payment->get_reservation($r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  $r['ActualPayment'] = $payment->get_actualpayment($r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  $r['CreditMemo']    = $payment->get_debitcredit($r['IDNo'],$r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  
  if($tscheme != 3){
	  $netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);
	  $balance = $netass - $r['ActualPayment'];	
	  if($r['AccountID']=='111' || $r['AccountID']=='110'){
		$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);  
		$balance = $netass - $r['ActualPayment'];	
	  }
  }else{
	$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']+$r['5thPayment']+$r['6thPayment']+$r['7thPayment']+$r['8thPayment']+$r['9thPayment']+$r['10thPayment']);
	$balance = $netass - $r['ActualPayment'];	
  }
  
  if($r['AccountID']=='110' || $r['AccountID']=='111'){
    $r['1stPayment']    = $r['1stPayment'];  
	$r['ActualPayment'] = (($r['ActualPayment']>0)?$r['ActualPayment']:($r['ActualPayment']+$reserve));
	$balance            = $balance - $reserve;
  }
  
  if($r['CreditMemo']>0){
	$balance            = $balance - $r['CreditMemo'];   
  }
  
  $r['Discount']+= $r['FinancialAid'];
  $tassess      += $r['AssessFee'];
  $tdiscount    += $r['Discount'];
  $tonnet       += $r['FinancialAid'];
  $tnetassess   += $netass;
  $tpaymenta    += $r['1stPayment'];
  $tpaymentb    += $r['2ndPayment'];
  $tpaymentc    += $r['3rdPayment'];
  $tpaymentd    += $r['4thPayment'];
  
  echo '<tr data-entryid="'.$r['EntryID'].'" data-refno="'.$r['ReferenceNo'].'" data-acctid="'.$r['AccountID'].'" data-idno="'.$r['IDNo'].'" data-scheme="'.$tscheme.'">
		   <td style="font-size:9px !important;" data-acctcode="'.$r['AcctCode'].'">'.$r['AcctCode'].'</td>
		   <td style="font-size:9px !important;">'.$r['AcctName'].'</td>
           <td class="text-right" style="font-size:9px !important;" data-assess="'.$r['AssessFee'].'">'.number_format($r['AssessFee'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-discount="'.number_format($r['Discount'],2,'.',',').'">'.number_format($r['Discount'],2,'.',',').'</td>
		   <td class="text-right hidden" style="font-size:9px !important;" data-financial="'.number_format($r['FinancialAid'],2,'.',',').'">'.number_format($r['FinancialAid'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-netassess="'.number_format($netass,2,'.','').'">'.number_format($netass,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenta="'.number_format($r['1stPayment'],2,'.','').'"><b>'.number_format($r['1stPayment'],2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<1)?'hidden':'').'" style="font-size:9px !important;" data-paymentb="'.number_format($r['2ndPayment'],2,'.','').'"><b>'.number_format($r['2ndPayment'],2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:9px !important;" data-paymentc="'.number_format($r['3rdPayment'],2,'.','').'"><b>'.number_format($r['3rdPayment'],2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:9px !important;" data-paymentd="'.number_format($r['4thPayment'],2,'.','').'"><b>'.number_format($r['4thPayment'],2,'.',',').'</b></td>
		   <td class="text-right hidden" colspan="6"></td>
		</tr>'; 	
 }
 echo '<tr class="warning">
		   <td colspan="2">TOTAL:</td>
           <td class="text-right" style="font-size:10px !important;" data-assess="'.$tassess.'">'.number_format($tassess,2,'.',',').'</td>
		   <td class="text-right" style="font-size:10px !important;" data-discount="'.number_format($tdiscount,2,'.',',').'">'.number_format($tdiscount,2,'.',',').'</td>
		   <td class="text-right hidden" style="font-size:10px !important;" data-financial="'.number_format($tonnet,2,'.',',').'">'.number_format($tonnet,2,'.',',').'</td>
		   <td class="text-right" style="font-size:11px !important;" data-netassess="'.number_format($tnetassess,2,'.','').'">'.number_format($tnetassess,2,'.',',').'</td>
		   <td class="text-right danger" style="font-size:11px !important;" data-paymenta="'.number_format($tpaymenta,2,'.','').'"><b>'.number_format($tpaymenta,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<1)?'hidden':'').'" style="font-size:11px !important;" data-paymentb="'.number_format($tpaymentb,2,'.','').'"><b>'.number_format($tpaymentb,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;" data-paymentc="'.number_format($tpaymentc,2,'.','').'"><b>'.number_format($tpaymentc,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;" data-paymentd="'.number_format($tpaymentd,2,'.','').'"><b>'.number_format($tpaymentd,2,'.',',').'</b></td>
		   <td class="text-right hidden" colspan="6"></td>
	  </tr>';

$discount   = '';
$less_pay   = '';
$pay_exec   = DB::select("SELECT (SUM(ActualPayment)+SUM(CreditMemo)) as LessPayment FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."'");
$disc_exec  = DB::select("SELECT ISNULL(gt.ShortName,sp.ProvName) as DiscountName
							,ot.Percentage 
						FROM ES_OtherTransactions as ot
				  INNER JOIN ES_SchoProviders as sp ON ot.SchoProviderID=sp.SchoProviderID
				   LEFT JOIN ES_SchoGrantTemplates as gt ON ot.GrantTemplateID=gt.GrantTemplateID
					   WHERE ot.TermID='".$termid."' AND ot.IDNo='".$studno."'");
$less_exec  = DB::select("SELECT ISNULL(Particulars,o.Particular) as Description,SUM(Credit) as TotalAmount 
						    FROM ES_Journals as j 
						   INNER JOIN ES_Registrations as r ON j.TermID=r.TermID AND j.IDNo=r.StudentNo
						   INNER JOIN ES_OfficialReceipts as o ON j.ReferenceNo=o.ORNo
						   WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND TransID=20 
						     AND j.Credit>0 AND ISNULL(j.NonLedger,0)<>1 AND ISNULL(TransRefNo,'') LIKE CONCAT(r.RegID,';%') 
						   GROUP BY j.TermID,j.IDNo,Particulars,o.Particular
						  UNION ALL
						  SELECT dc.Explanation as Description,SUM(Credit) as TotalAmount 
						    FROM ES_Journals as j
					  INNER JOIN ES_DebitCreditMemo as dc ON j.ReferenceNo=dc.RefNo
                      INNER JOIN ES_Registrations as r ON j.TermID=r.TermID AND j.IDNo=r.StudentNo
						   WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND TransID=60 AND ISNULL(j.UserID,'')<>'discount'  
						GROUP BY dc.Explanation");					 
						
if($disc_exec && count($disc_exec)>0){
  foreach($disc_exec as $d){
	$discount .= $d->DiscountName.',';
  }
  echo '<tr class=""><td colspan="16"></td></tr>
		<tr class="info">
			<td colspan="2">Discount(s):</td>
			<td colspan="14">'.$discount.'</td>
		</tr>';
}

if($less_exec && count($less_exec)>0){
  foreach($less_exec as $l){
    $less_pay .= '<tr class="warning">
				   <td colspan="2">'.strtoupper($l->Description).'</td>
				   <td colspan="3" class="text-right"></td>
				   <td class="text-right" style="font-size:11px !important;">'.number_format($l->TotalAmount,2).'</td>
				   <td class="text-right '.(($tscheme<1)?'hidden':'').'" style="font-size:11px !important;"></td>
				   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"></td>
				   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"></td>
				   <td colspan="11" class="hidden"></td>
				  </tr>';
  }
}

if($pay_exec && count($pay_exec)>0){
  $tmppay    = $pay_exec[0]->LessPayment;
  $tpaydmcm  = ((@floatval($tmppay))?floatval($tmppay):0);
  $xpaymenta = ($tpaymenta-$tpaydmcm);
  if($tpaymentb>0){
  $dpaymenta = (($xpaymenta<0)?$tpaymenta:($tpaydmcm));
  }else{
  $dpaymenta = ($tpaydmcm);
  }
  
  $xpaymentb = (($tpaymenta+$tpaymentb)-$tpaydmcm);
  if($tpaymentc>0){
    $dpaymentb = (($xpaymentb<0)?$tpaymentb:(($xpaymenta<0)?($xpaymenta*-1):0));
  }else{
    $dpaymentb = ($tpaydmcm-$tpaymenta);
  }
  
  $xpaymentc = (($tpaymenta+$tpaymentb+$tpaymentc)-$tpaydmcm);
  $dpaymentc = (($xpaymentc<0)?$tpaymentc:(($xpaymentb<0)?($xpaymentb*-1):0));
  $xpaymentd = (($tpaymenta+$tpaymentb+$tpaymentc+$tpaymentd)-$tpaydmcm);
  $dpaymentd = (($xpaymentd<0)?($xpaymentc*-1):(($xpaymentc<0)?($xpaymentc*-1):0));
  
  $tpaymenta = $tpaymenta - $dpaymenta;
  $cpaymenta = (($dpaymenta<=0)?'':'');
  $tpaymentb = $tpaymentb - $dpaymentb;
  $cpaymentb = (($dpaymentb<=0)?'hidden':'');
  $tpaymentc = $tpaymentc - $dpaymentc;
  $cpaymentc = (($dpaymentc<=0)?'hidden':'');
  $tpaymentd = $tpaymentd - $dpaymentd;
  $cpaymentd = (($dpaymentc<0 || $dpaymentd<=0)?'hidden':'');
  
  if($tpaydmcm>0){
  echo '<tr class=""><td colspan="16"></td></tr>';
  if($less_pay==''){
  echo '<tr class="warning">
		   <td colspan="2">Less Payment Received:</td>
		   <td colspan="3" class="text-right"></td>
		   <td class="text-right" style="font-size:11px !important;">'.number_format($dpaymenta,2).'</td>
		   <td class="text-right '.(($tscheme<1)?'hidden':'').'" style="font-size:11px !important;"><b class="'.$cpaymentb.'">'.number_format($dpaymentb,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"><b class="'.$cpaymentb.'">'.number_format($dpaymentc,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"><b class="'.$cpaymentb.'">'.number_format($dpaymentd,2,'.',',').'</b></td>
		   <td colspan="11" class="hidden"></td>
	    </tr>';
  }else{
  echo '<tr class="warning">
		   <td colspan="2">Less:</td>
		   <td colspan="3" class="text-right"></td>
		   <td class="text-right" style="font-size:11px !important;"></td>
		   <td class="text-right '.(($tscheme<1)?'hidden':'').'" style="font-size:11px !important;"></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"></td>
		   <td colspan="11" class="hidden"></td>
	    </tr>';
  echo  $less_pay;
  }		
  echo '<tr class="warning">
		   <td colspan="5"></td>
		   <td class="text-right danger" style="font-size:11px !important;"><b class="'.$cpaymenta.'">'.number_format($tpaymenta,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<1)?'hidden':'').'" style="font-size:11px !important;"><b class="'.$cpaymentb.'">'.number_format($tpaymentb,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"><b class="'.$cpaymentc.'">'.number_format($tpaymentc,2,'.',',').'</b></td>
		   <td class="text-right '.(($tscheme<2)?'hidden':'').'" style="font-size:11px !important;"><b class="'.$cpaymentc.' '.$dpaymentd.'">'.number_format($tpaymentd,2,'.',',').'</b></td>
		   <td colspan="7" class="hidden"></td>
	    </tr>';	  
  }		
}

}else{
 echo '<tr><td class="text-center" colspan="16"><i class="fa fa-warning"></i> No Data Available</td></tr>';
}

?>

