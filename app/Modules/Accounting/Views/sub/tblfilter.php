<?php
$type = ((isset($type))?$type:1);
switch($type){	
 case '1':
 case 'student':
	if($list){
		 echo '<table class="table table-bordered table-condense" style="white-space:nowrap;">
				<thead>
				  <th>StudentNo</th>
				  <th>Last Name</th>
				  <th>First Name</th>
				  <th>Middle Name</th>
				  <th>FamilyID</th>
				  <th>Level</th>
				  <th>Program</th>
				  <th>Siblings</th>
			   </thead>
			   <tbody>';
		foreach($list as $r){
		 echo  '<tr data-list="'.$r->StudentNo.'" data-name="'.$r->LastName.', '.$r->FirstName.' '.$r->Middlename.'" 
					data-progid="'.$r->ProgID.'" data-progid="'.$r->ProgID.'" data-progname="'.$r->ProgName.'"
					data-yrlvlid="'.$r->YearLevelID.'" data-yrlvl="'.$r->YearLevelName.'">
				  <td>'.$r->StudentNo.'</td>
				  <td>'.$r->LastName.'</td>
				  <td>'.$r->FirstName.'</td>
				  <td>'.$r->Middlename.'</td>
				  <td>'.$r->FamilyID.'</td>
				  <td>'.$r->YearLevelName.'</td>
				  <td>'.$r->ProgName.'</td>
				  <td>'.getTotalSiblings($r->StudentNo).'</td>
				</tr>';
		}
		 echo '</tbody></table>';
	}else{
	  echo '<table class="table table-bordered table-condense">
				<thead>
					<th>Last Name</th>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Program</th>
					<th>Level</th>
				</thead>
			</table>';	
	} 
 break; 
 case 'scholarship':
    if($list){
		echo '<table class="table table-bordered table-condense" style="white-space:nowrap;">
				<thead>
				  <th>Code</th>
				  <th>Provider</th>
				  <th>Schort Name</th>
				  <th>Has Grant Template</th>
			   </thead>
			   <tbody>';
		foreach($list as $r){
		 $chck = DB::table("ES_SchoGrantTemplates")->where("SchoProviderID",$r->SchoProviderID);
		 $count= (($chck && count($chck->get())>0)?"Yes":"No");
		 echo  '<tr data-list="'.$r->SchoProviderID.'" data-code="'.$r->ProvCode.'" data-name="'.$r->ProvName.'">
				  <td>'.$r->ProvCode.'</td>
				  <td>'.$r->ProvName.'</td>
				  <td>'.$r->ProvShort.'</td>
				  <td>'.$count.'</td>
				</tr>';
		}
		echo '</tbody></table>';
	}else{
	  echo '<table class="table table-bordered table-condense">
				<thead>
					<th>Code</th>
					<th>Provider</th>
					<th>Short Name</th>
				</thead>
			</table>';	
	} 
 break;
}
?>