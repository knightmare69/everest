<?php
$payment  = new App\Modules\Accounting\Services\Assessment\Assessment;
$detail   = ((isset($detail))?$detail:array());
$assess   = ((isset($assess))?$assess:array());
$otherfee = array();
$reginfo  = array();
$validdate= '';
$templnme = '';
$tscheme  = 0;

$termid   = ((@array_key_exists('termid',$details))?$details['termid']:0);
$studno   = ((@array_key_exists('studno',$details))?$details['studno']:0);
$regid    = ((@array_key_exists('regid',$details))?$details['regid']:0);
$templid  = ((@array_key_exists('feesid',$details))?$details['feesid']:0);

if($termid!=0 && $studno!=0){
$reginfo  = DB::select("SELECT * FROM ES_Registrations WHERE TermID='".$termid."' AND StudentNo='".$studno."'");
if($reginfo && count($reginfo)>0){
  $validdate = $reginfo[0]->ValidationDate;
}

$otherfee = DB::select("SELECT a.AcctCode,a.AcctName,j.* 
                          FROM ES_Journals as j INNER JOIN ES_Accounts as a ON j.AccountID=a.AcctID 
						 WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND TransID=1 AND j.AccountID NOT IN (SELECT DISTINCT AccountID FROM ES_TableofFee_Details)");
}

if($templid>0){
  $tmpfee   = DB::select("SELECT * FROM ES_TableOfFees WHERE TemplateID='".$templid."'");
  $templnme = (($tmpfee && count($tmpfee)>0)?($tmpfee[0]->TemplateCode):'');
  $tscheme  = (($tmpfee && count($tmpfee)>0)?($tmpfee[0]->PaymentScheme):0);
}elseif($templid=='-1'){
  $templnme = 'Custom/Monthly';
  $tscheme  = 3; 
}

echo '<tr class="info">
		 <td colspan="7">
		   <button class="btn btn-xs green '.(($validdate=='')?'btn-tblfee':'disabled').'"><i class="fa fa-table"></i> Template:<b class="templname" data-id="'.$templid.'">'.$templnme.'</b></button>
		 </td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td></td>
		 <td colspan="18"></td>
      </tr>';  
if($assess && count($assess)>0){	
 foreach($assess as $k=>$r){
  $idno               = $r['IDNo'];
  $reserve            = $payment->get_reservation($r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  $r['ActualPayment'] = $payment->get_actualpayment($r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  $r['CreditMemo']    = $payment->get_debitcredit($r['IDNo'],$r['EntryID'],$r['ReferenceNo'],$r['AccountID']);
  
  if($tscheme != 3){
	  $netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);
	  $balance = $netass - $r['ActualPayment'];	
	  if($r['AccountID']=='111' || $r['AccountID']=='110'){
		$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);  
		$balance = $netass - $r['ActualPayment'];	
	  }
  }else{
	$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']+$r['5thPayment']+$r['6thPayment']+$r['7thPayment']+$r['8thPayment']+$r['9thPayment']+$r['10thPayment']);
	$balance = $netass - $r['ActualPayment'];	
  }
  
  if($r['AccountID']=='110' || $r['AccountID']=='111'){
    $r['1stPayment']    = $r['1stPayment'];  
	$r['ActualPayment'] = (($r['ActualPayment']>0)?$r['ActualPayment']:($r['ActualPayment']+$reserve));
	$balance            = $balance - $reserve;
  }
  
  if($r['CreditMemo']>0){
	$balance            = $balance - $r['CreditMemo'];   
  }
  
  echo '<tr data-entryid="'.$r['EntryID'].'" data-refno="'.$r['ReferenceNo'].'" data-acctid="'.$r['AccountID'].'" data-idno="'.$r['IDNo'].'" data-scheme="'.$tscheme.'">
		   <td style="font-size:9px !important;" data-acctcode="'.$r['AcctCode'].'">'.$r['AcctCode'].'</td>
		   <td style="font-size:9px !important;">'.$r['AcctName'].'</td>
           <td class="text-right" style="font-size:9px !important;" data-assess="'.number_format($r['AssessFee'],2,'.','').'">'.number_format($r['AssessFee'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-discount="'.number_format($r['Discount'],2,'.','').'">'.number_format($r['Discount'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-financial="'.number_format($r['FinancialAid'],2,'.','').'">'.number_format($r['FinancialAid'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-netassess="'.number_format($netass,2,'.','').'">'.number_format($netass,2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'':'tdedit').'" style="font-size:9px !important;" data-pointer="data-paymenta" data-paymenta="'.number_format($r['1stPayment'],2,'.','').'">'.number_format($r['1stPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<1)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymentb" data-paymentb="'.number_format($r['2ndPayment'],2,'.','').'">'.number_format($r['2ndPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<2)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymentc" data-paymentc="'.number_format($r['3rdPayment'],2,'.','').'">'.number_format($r['3rdPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<2)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymentd" data-paymentd="'.number_format($r['4thPayment'],2,'.','').'">'.number_format($r['4thPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymente" data-paymente="'.number_format($r['5thPayment'],2,'.','').'">'.number_format($r['5thPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymentf" data-paymentf="'.number_format($r['6thPayment'],2,'.','').'">'.number_format($r['6thPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymentg" data-paymentg="'.number_format($r['7thPayment'],2,'.','').'">'.number_format($r['7thPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymenth" data-paymenth="'.number_format($r['8thPayment'],2,'.','').'">'.number_format($r['8thPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'thide':(($tscheme<3)?'':'tdedit')).'" style="font-size:9px !important;" data-pointer="data-paymenti" data-paymenti="'.number_format($r['9thPayment'],2,'.','').'">'.number_format($r['9thPayment'],2,'.',',').'</td>
		   <td class="text-right '.(($tscheme<3)?'thide':(($tscheme<3)?'':'tdedit')).'" style="width:1px !important;font-size:9px !important;" data-paymentj="'.number_format($r['10thPayment'],2,'.','').'">'.number_format($r['10thPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paydiscount="'.number_format($r['PaymentDiscount'],2,'.','').'">'.number_format($r['PaymentDiscount'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-payment="'.number_format($r['ActualPayment'],2,'.','').'">'.number_format($r['ActualPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-creditmemo="'.number_format($r['CreditMemo'],2,'.','').'">'.number_format($r['CreditMemo'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-refund="'.number_format($r['Refund'],2,'.','').'">'.number_format($r['Refund'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-balance="'.number_format($balance,2,'.','').'">'.number_format($balance,2,'.',',').'</td>
		   <td style="font-size:9px !important;">'.$r['Remarks'].'</td>
		   <td style="font-size:9px !important;">'.$r['DMCMRefNo'].'</td>
		   <td style="font-size:9px !important;">'.$r['EntryID'].'</td>
		   <td style="font-size:9px !important;"><i class="fa '.(($r['InstallmentExcluded']>0)?'fa-check-square-o':'fa-square-o').'"></i></td>
		   <td style="font-size:9px !important;"><i class="fa '.(($r['SpecialFee']>0)?'fa-check-square-o':'fa-square-o').'"></i></td>
		   <td style="font-size:9px !important;">'.$r['SeqNo'].'</td>
		</tr>'; 	
 }
}else{
 echo '<tr><td class="text-center" colspan="29"><i class="fa fa-warning"></i> No Data Available</td></tr>';
}

echo '<tr class="warning">
		 <td colspan="29">
		   <button class="btn btn-xs btn-primary btn-add-other"><i class="fa fa-plus"></i> Add</button>
		   <span>Other fees</span>
		 </td>
      </tr>';
if($otherfee && count($otherfee)>0){
  foreach($otherfee as $o){
  $btn        = '<i class="fa fa-lock text-warning"></i>';
  if($o->DMCMRefNo!=''){
    $tmpcmemo = DB::SELECT("SELECT SUM(Credit) as Credit FROM ES_Journals 
    	                     WHERE TransID=60 AND TermID='".$o->TermID."' AND IDNo='".$o->IDNo."' AND ReferenceNo='".$o->DMCMRefNo."'");
    if($tmpcmemo && count($tmpcmemo)>0){
        $o->CreditMemo = $tmpcmemo[0]->Credit;	
    }
  }
  
  $actualpay  = $o->ActualPayment;
  $creditmemo = $o->CreditMemo;
  
  if($actualpay==0 && $creditmemo==0){
    $btn      = '<i class="fa fa-times text-danger btn-remove-other" data-id="'.$o->EntryID.'"></i>';
  }
  
  echo '<tr data-entryid="'.$o->EntryID.'" data-refno="'.$o->ReferenceNo.'" data-acctid="'.$o->AccountID.'" data-idno="'.$o->IDNo.'">
		   <td style="font-size:9px !important;" data-acctcode="'.$o->AcctCode.'">'.$o->AcctCode.'</td>
		   <td style="font-size:9px !important;">'.$btn.'&nbsp;'.$o->AcctName.'</td>
           <td class="text-right" style="font-size:9px !important;" data-assess="'.$o->{'Assess Fee'}.'">'.number_format($o->{'Assess Fee'},2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-discount="'.$o->Discount.'">'.number_format($o->Discount,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-financial="'.$o->FinancialAidExternal.'">'.number_format($o->FinancialAidExternal,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-netassess="'.number_format($o->Debit,2,'.','').'">'.number_format($o->Debit,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenta="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentb="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentc="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentd="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymente="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentf="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentg="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenth="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymenti="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paymentj="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-paydiscount="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-payment="'.number_format($o->ActualPayment,2,'.','').'">'.number_format($o->ActualPayment,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-creditmemo="'.number_format($o->CreditMemo,2,'.','').'">'.number_format($o->CreditMemo,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-refund="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td class="text-right" style="font-size:9px !important;" data-balance="'.number_format(0,2,'.','').'">'.number_format(0,2,'.',',').'</td>
		   <td style="font-size:9px !important;">'.$o->Remarks.'</td>
		   <td style="font-size:9px !important;">'.$o->DMCMRefNo.'</td>
		   <td style="font-size:9px !important;">'.$o->EntryID.'</td>
		   <td style="font-size:9px !important;"><i class="fa '.(($o->InstallmentExcluded>0)?'fa-check-square-o':'fa-square-o').'"></i></td>
		   <td style="font-size:9px !important;"><i class="fa '.(($o->SpecialFee>0)?'fa-check-square-o':'fa-square-o').'"></i></td>
		   <td style="font-size:9px !important;">0</td>
		</tr>';
 }	
}else{
 echo '<tr><td class="text-center" colspan="7"><i class="fa fa-warning"></i> No Data Available</td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td></td>
		   <td colspan="18"></td></tr>';
}	  
?>

