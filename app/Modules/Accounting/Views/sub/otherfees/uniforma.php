<div class="col-sm-12">
   <table class="table table-bordered table-condense">
	  <thead>
		<th colspan="2">Description</th>
		<th>Price</th>
		<th>Quantity</th>
		<th>Amount</th>
	  </thead>
	  <tbody>
		 <?php 
		  $test = DB::select("SELECT * FROM ES_Accounts WHERE AcctID IN ('1008','1009','1010','1011','1012','1013','1014','1015','1016','1017','1018','1019','1020')");
		  $unia  = array();
		  if($test && count($test)>0){
		   foreach($test as $s){
		    $unia[$s->AcctID]=$s->DefaultAmount;
		   }
		  }
		 ?>
		 <tr><td colspan="5">Boy's Regular Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1008" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">White Shirt</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1008],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1009" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">Gray Pants(with garter)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1009],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1010" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">Gray Shorts</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1010],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">Girl's Regular Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1011" data-grdmin="1" data-grdmax="6">
			 <td width="5%"></td>
			 <td width="60%">LS PeterPan White Shirt(Kinder to Gr5 only)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1011],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1012" data-grdmin="7" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">MS White Blouse(Gr6 to Gr8 only)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1012],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1013" data-grdmin="1" data-grdmax="6">
			 <td width="5%"></td>
			 <td width="60%">Plaid Jumper(Kinder to Gr5 only)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1013],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1014" data-grdmin="7" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">Plaid Skirt(Gr6 to Gr8 only)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1014],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">PE Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1015" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">PE Shirt</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1015],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1016" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">PE Pants</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1016],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1017" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">PE Shorts</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1017],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1018" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">PE Vest</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1018],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">Outerwear</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1019" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">Zip-up Sweater(Boys)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1019],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1020" data-grdmin="1" data-grdmax="9">
			 <td width="5%"></td>
			 <td width="60%">Button-down Cardigan(Girls)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="<?php echo number_format($unia[1020],2,".","");?>"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
	  </tbody>
   </table>
</div>
