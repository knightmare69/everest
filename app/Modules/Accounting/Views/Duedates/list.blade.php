@for($i=1;$i<=10; $i++)
<tr data-val="{{$i}}">
    <td class="bold">Due Date {{$i}}</td>
    <td>
        @if($i == 1)
        <input value="" class="input-no-border date-picker form-control " placeholder="Enter Date" data-due="1{{$i}}" />
        @endif
    </td>
    <td>
        @if($i < 3)
        <input value="" class="input-no-border date-picker form-control " data-due="2{{$i}}" placeholder="Enter Date"  />
        @endif
    </td>
    <td>
        @if($i < 5)
        <input value="" class="input-no-border date-picker form-control " data-due="3{{$i}}" placeholder="Enter Date"  />
        @endif
    </td>
    <td><input value="" class="input-no-border date-picker form-control " data-due="4{{$i}}" placeholder="Enter Date"  /></td>
</tr>
@endfor      