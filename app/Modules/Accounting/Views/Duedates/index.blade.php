<?php
    $at = getAYTerm();
    
    echo json_encode($data);
    global $data1;
    $data1 = $data;
    
    function get_value($scheme, $due){
        global $data1;
            
        $return = '';
        $col = array(
             1 => 'FirstPaymentDate'            
            ,2 => 'SecondPaymentDate'            
            ,3 => 'ThirdPaymentDate'
            ,4 => 'FourthPaymentDate'            
            ,5 => 'FifthPaymentDate'            
            ,6 => 'SixthPaymentDate'            
            ,7 => 'SeventhPaymentDate'
            ,8 => 'EighthPaymentDate'            
            ,9 => 'NinthPaymentDate'            
            ,10 => 'TenthPaymentDate'            
        );
        
        foreach($data1 as $d){
            if($d->PaymentSchemeID == $scheme){
                $return =  getObjectValue($data1, $col[$due]);        
                return $return;
            }
        }
        
        return $return;
    }
    
?>

<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Due Dates Setup
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
			</a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row">
            <div class="col-sm-12 " style="padding-bottom: 10px;">               
                <div class="portlet-input input-inline input-medium">
                    <select class="select2 form-control" name="term" id="term">
                        <option value="0" >- Select AY Term -</option>
                        @foreach($at as $r)
                        <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AYTerm }}</option>
                        @endforeach
                    </select>
    			</div>
                <div class="portlet-input input-inline">
                    <button type="button" class="btn btn-sm btn-default " data-menu="add"><i class="fa fa-plus"></i> <span class="hidden-xs">Reset</span> </button>
                    <button type="button" class="btn btn-sm btn-success " data-menu="save"><i class="fa fa-save"></i> <span class="hidden-xs">Save</span> </button>
                    
                    <button type="button" class="btn btn-sm btn-default " data-menu="refresh"><i class="fa fa-refresh"></i> <span class="hidden-xs">Refresh</span> </button>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="bold text-center">CASH</th>
                            <th class="bold text-center">Semi-Annual</th>
                            <th class="bold text-center">Quarterly</th>
                            <th class="bold text-center">Monthly</th>                                                        
                        </tr>
                    </thead>
                    <tbody>
                        @for($i=1;$i<=10; $i++)
                            <tr data-val="{{$i}}">
                                <td class="bold">Due Date {{$i}}</td>
                                <td>
                                    @if($i == 1)
                                    <input value="<?= get_value(1, $i); ?>" class="input-no-border date-picker form-control " placeholder="Enter Date" data-due="1{{$i}}" />
                                    @endif
                                </td>
                                <td>
                                    @if($i < 3)
                                    <input value="<?= get_value(2, $i); ?>" class="input-no-border date-picker form-control " data-due="2{{$i}}" placeholder="Enter Date"  />
                                    @endif
                                </td>
                                <td>
                                    @if($i < 5)
                                    <input value="<?= get_value(3, $i); ?>" class="input-no-border date-picker form-control " data-due="3{{$i}}" placeholder="Enter Date"  />
                                    @endif
                                </td>
                                <td><input value="<?= get_value(4, $i); ?>" class="input-no-border date-picker form-control " data-due="4{{$i}}" placeholder="Enter Date"  /></td>
                            </tr>
                            @endfor                                           
                    </tbody>
                </table>
            </div>
        </div>        		
	</div>
</div>