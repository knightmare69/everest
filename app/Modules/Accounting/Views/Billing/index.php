<style>
    #tblassess td{
        padding: 4px !important;
        font-size:12px !important;
    }

    #tblassess th{
        font-size:12px !important;
    }

    .totaloutbalance{
        font-weight:  bolder !important;
    }
    .profile{
        float: left;
        width: 140px;
        margin-right: 10px;
    }
    .profile-content{
        overflow: hidden;
    }
    .totalrow td{
        font-size:12px !important;
    }
</style>
<div class="portlet blue box dvlist tabbable tabs-left" style="font-size:10px;">
	<div class="portlet-title">
	 <ul class="nav nav-tabs pull-left">
	   <li class="active"><a href="#tab_1" data-toggle="tab">Student</a></li>
	   <li class=""><a href="#tab_2" data-toggle="tab">Employee</a></li>
	   <li class=""><a href="#tab_3" data-toggle="tab">Scho. Provider</a></li>
	   <li class=""><a href="#tab_4" data-toggle="tab">Other Payor</a></li>
	 </ul>
	 <div class="pull-right" style="padding-top:8px;">
	    
	      <button class="btn btn-xs btn-default btn-permit"><i class="fa fa-table"></i> DR/CR Memo</button>
          <button class="btn btn-xs btn-default btn-permit"><i class="fa fa-table"></i> Allow to Enroll</button>
          <button class="btn btn-sm btn-warning btn-schedule"><i class="fa fa-table"></i> Penalty</button>
		
	 </div>
	</div>
	<div class="portlet-body">
	 <div class="row">
	   <div class="col-sm-12">
	    <div class="profile">
            <img class="entryphoto" width="140" height="170" src="<?php echo url('assets/system/media/images/no-image.png').'?'.rand(0,9999);?>"/>
		</div>
        <div class="profile-content">
        <div class="tab-content" >
            <div class="tab-pane fade active in" id="tab_1">

            <div class="col-sm-2 col-sm-offset-1">StudentNo : <b class="studno" data-stats="1"></b></div>
            <div class="col-sm-3">YearLevel : <b class="yrlvl"></b></div>
            <div class="col-sm-6 ">Acad Program : <b class="prog"></b></div>

            <div class="row static-info">
        		<div class="col-md-1 name bold text-right"><span class="autofit">Full Name :</span></div>
        		<div class="col-md-8 value">
        		<div class="input-group" style="margin-bottom: 5px;">
                    <input type="text" class="form-control uppercase input-sm bold" id="studname" name="studname" data-button=".btnfilter"/>
                	<span class="input-group-addon btnfilter" data-source="student" data-parameter="#studname" data-id=".studno"><i class="fa fa-search"></i> Search   </span>
                  </div>
        		</div>
        	</div>
                                           
            <div class="row static-info">
                <div class="col-md-4">
                    <div class="row static-info">
                        <div class="col-md-4 name bold "><span class="autofit">Transaction :</span></div>
                		<div class="col-md-8 value">
                			<select id="txn" name="txn" class="form-control input-sm bold input-medium ">
                                <option value="1">Enrollment</option>
                                <option value="2">Add/Drop/Change</option>
                                <option value="15">Other Assessment</option>
                            </select>
                		</div>                                                                    
                    </div>
                    <div class="row static-info">
                        <div class="col-md-4 name bold ">
                			 <span class="autofit">Reference :</span>
                		</div>
           	            <div class="col-md-8 form-inline value">
                            <select class="input-sm form-control input-small bold" id="refno" name="refno"></select>
                            <button type="button" class="btn btn-sm btn-primary btn-refno-refresh ">Refresh</button>
      		            </div>
                    </div>                  
                    <div class="row static-info">
                        <div class="col-md-4 name bold"></div>
                		<div class="col-md-8 value">
                            <button class="btn btn-sm btn-primary btn-new" type="button"><i class="fa fa-file"></i> New Transaction </button>
                            <button class="btn btn-sm btn-danger btn-purge" type="button"><i class="fa fa-times"></i> Purge </button>
                		</div>        
        	       </div>            
                </div>        		                
               	<div class="col-md-8">
        			 
                      <div class="row static-info">
                        <div class="col-md-6 value">
                        <ul class="list-group" style="margin-bottom: 0px;">
								<li class="list-group-item">
									 Academic Year/Term : <b class="termid font-blue-madison bold" data-termid="0"></b>
								</li>
                                <li class="list-group-item">
									 Registration Date : <b class="regdate font-blue-madison bold"></b>									
								</li>
                                
								<li class="list-group-item">
									 Validation Date : <b class="validate font-blue-madison bold"></b>
								</li>
															
							</ul>
                </div>
                <div class="col-md-6 value">
                <ul class="list-group" style="margin-bottom: 0px;">
								<li class="list-group-item">
									 Remarks : <b class="remarks bold font-blue-madison"></b>
								</li>
								<li class="list-group-item">
									 Template : <b class="templname bold font-red-flamingo " data-id="0"></b>
								</li>
													
							</ul>
                </div>
            </div>
                     
                     
        		</div>
        	</div>

            

              </div>
        </div>

        </div>

	   </div>
     </div>
     <div class="well" style="padding:5px;background-color:#f9f9f9;">
	   	 <ul class="nav nav-tabs ">
           <li class="active"><a href="#tab_a1" data-toggle="tab">Assessment</a></li>
           <li class=""><a href="#tab_a2" data-toggle="tab">Discounts</a></li>
           <li class=""><a href="#tab_a3" data-toggle="tab">Other Fees</a></li>
           <li class=""><a href="#tab_a4" data-toggle="tab">Enrollment</a></li>
            <div class="pull-right">
                <button class="btn btn-sm btn-default btn-print-assess"><i class="fa fa-print"></i> Print Assessment</button>
                <button class="btn btn-sm btn-default btn-ledger"><i class="fa fa-table"></i> Ledger</button>
                <button class="btn btn-sm btn-default btn-print-soa"><i class="fa fa-table"></i> S.O.A</button>

                <button class="btn btn-sm btn-success btn-tblfee"><i class="fa fa-table"></i> Table Of Fees</button>

                <button class="btn btn-sm btn-info btn-permit"><i class="fa fa-list"></i> Permit</button>
                <button class="btn btn-sm btn-info btn-permit"><i class="fa fa-list"></i> Clearance</button>
                <button class="btn btn-sm btn-warning btn-schedule"><i class="fa fa-table"></i> Schedule of Payment</button>                

            </div>
		 </ul>
         <div class="tab-content">
           <div class="tab-pane fade active in" id="tab_a1">
                <div class="table-responsive">
			    <table class="table table-bordered table-strife table-condensed table-hover" id="tblassess" style="cursor: pointer; margin-bottom: 0px;">
				 <thead>
                    <th >#</th>
				   <th >CODE</th>
				   <th >ACCOUNT</th>
				   <th >ASSESSED</th>
				   <th class="text-center">FINANCIAL<br>AID</th>
				   <th class="text-center" >BILL TO<br>PROVIDER</th>
				   <th class="text-center bold" >NET ASSESS</th>
				   <th class="text-center bold">1ST<br>INSTMNT</th>
				   <th class="text-center bold">2ND<br>INSTMNT</th>
				   <th class="text-center bold">3RD<br>INSTMNT</th>
				   <th class="text-center bold">4TH<br>INSTMNT</th>
                   <th class="text-center bold">5TH<br>INSTMNT</th>
				   <th class="text-center bold bg-yellow-gold" >PAYMENT<br>DISCOUNT</th>
				   <th class="text-center bold bg-yellow-gold" >ACTUAL<br>PAYMENT</th>
				   <th class="text-center bold bg-yellow-gold" >CREDIT<br>MEMO</th>
				   <th class="text-center bold bg-yellow-gold" >DEBIT<br>REFUND</th>
				   <th class="text-center bold bg-yellow-gold" >BALANCE</th>
				   <th >REMARKS</th>
				   <th >DMCM<br>REFNO</th>
				   <th >SEQ</th>
				 </thead>
				 <tbody>
				  <tr>
					<td class="text-center" colspan="22"><i class="fa fa-warning"></i> No Data Available</td>
				  </tr>
				 </tbody>
				 <tfoot class="bg-blue-madison totalrow bold font-lg">
				   <td colspan="3" style="text-align:right;font-size:10px !important;">TOTAL:</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
                   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td class="text-right totalsum">0.00</td>
				   <td></td>
				   <td></td>
				   <td></td>
				 </tfoot>
			   </table>
			   </div>
		   </div>
           <div class="tab-pane fade" id="tab_a2">
		    <div class="row">
			  <div class="col-sm-12">
			     <button type="button" class="btn btn-xs btn-success btn-discount-new"><i class="fa fa-file"></i> New</button>
			     <button type="button" class="btn btn-xs btn-warning btn-discount-save"><i class="fa fa-save"></i> Save</button>
			     <button type="button" class="btn btn-xs btn-danger btn-discount-delete"><i class="fa fa-trash-o"></i> Delete</button>
			     <button type="button" class="btn btn-xs btn-info btn-discount-refresh"><i class="fa fa-refresh"></i> Refresh</button>
				 <br/><br/>
			  </div>
			  <div class="col-sm-12">
			    <div class="col-sm-2">
				  Provider:
				  <select class="form-control" id="provider" name="provider">
				    <option value="1">Discount</option>
				    <option value="1">Scholarship</option>
				    <option value="2">Guarantor</option>
				  </select>
				</div>
			    <div class="col-sm-4">
				  <br/>
				  <div class="input-group">
				    <input type="hidden" id="scholprovid" name="scholprovid" />
					<input type="text" class="form-control" id="scholprovider" name="scholprovider" data-id="0" data-button=".btnfilter"/>
					<span class="input-group-addon btnfilter" data-source="scholarship" data-parameter="#scholprovider" data-id="#scholprovid"><i class="fa fa-search"></i></span>
				  </div>
				</div>
			    <div class="col-sm-6">
				  <br/>
				  <select class="form-control" id="schoOption" name="schoOption">
						<option value="1">By Account</option>
						<option value="2">Grant Template</option>
						<option value="3">Custom</option>
				  </select>
				</div>
			  </div>
			  <div class="col-sm-12">
				<div class="col-sm-6">
				  <br/>
				  <select class="form-control" id="application" name="application">
				    <option value="0">Half/Full Scholar On Tuition</option>
				    <option value="1">Half/Full Scholar On All Accounts</option>
				    <option value="2">Allotted Amount</option>
				    <option value="3">Remaining Balance</option>
				  </select>
				  <select class="form-control hidden" id="template" name="template">
				    <option value="-1">-SELECT ONE-</option>
				  </select>
				</div>
			    <div class="col-sm-6">
				  <br/>
				  <div class="input-group">
				   <input type="text" class="form-control numberonly text-align-right" id="scholamount" name="scholamount" value="0.00"/>
				   <span class="input-group-btn">
				    <div class="btn-group">
				     <button id="scholamttype" class="btn btn-success dropdown-toggle" type="button" data-value="0" data-toggle="dropdown" aria-expanded="true">Perc.</button>
					 <ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(0);" data-btntarget="#scholamttype" data-label="Perc." data-value="0">Percentage</a></li>
						<li><a href="javascript:void(0);" data-btntarget="#scholamttype" data-label="Amt." data-value="1">Amount</a></li>
					 </ul>
					</div>
				   </span>
				  </div>
				  <div class="optgrant text-align-right hidden">
				    <div class="col-sm-6 radio-list">
						<label class="radio-inline"><div class="radio" id="opttemplateb"><input name="optgranttempl" id="optgrantbyrate" value="0" type="radio" checked></div> Rate</label>
						<label class="radio-inline"><div class="radio" id="opttemplatea"><input name="optgranttempl" id="optgrantbyamount" value="1" type="radio"></div> Amount</label>
					</div>
                    <div class="col-sm-6">
					    <input type="text" class="form-control text-right numberonly" id="customdisc" name="customdisc" value="0.00"/>
                    </div>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-12" style="margin-bottom:10px;">
				<div class="col-sm-6">
				  <br/>
				  <div class="checkbox-list">
				    <label class="checkbox-inline"><input type="checkbox" id="applyNet" name="applyNet" checked/> Apply To Net</label>
				    <label class="checkbox-inline"><input type="checkbox" id="applyTotal" name="applyTotal"/> Apply To Total</label>
				  </div>
				</div>
				<div class="col-sm-6">
				  <br/>
				  <div class="optgrant hidden">
				  </div>
				</div>
			  </div>
			</div>
		    <div class="table-responsive">
			  <table class="table table-bordered table-condense table-hover" id="tbldiscount">
			    <thead>
				   <th>Type</th>
				   <th>Provider</th>
				   <th>Template</th>
				   <th>Total Discount</th>
				   <th>Percentage</th>
				   <th>Apply On Net</th>
				   <th>Remarks</th>
				</thead>
				<tbody>
				</tbody>
			  </table>
		   </div>
		 </div>
         <div class="tab-pane fade dvothertxn" id="tab_a3">
			<div class="row">
			    <div class="col-sm-12">
				  <button class="btn btn-xs btn-warning btn-other-save"><i class="fa fa-save"></i> Save</button>
				  <button class="btn btn-xs btn-danger btn-other-clear"><i class="fa fa-refresh"></i> Clear</button>
				  <br/><br/>
				</div>
				<div class="col-sm-12">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_1_1" data-toggle="tab" aria-expanded="true">
							Car Sticker </a>
						</li>
						<li class="">
							<a href="#tab_1_2" data-toggle="tab" aria-expanded="false">
							Uniform </a>
						</li>
						<li class="">
							<a href="#tab_1_3" data-toggle="tab" aria-expanded="false">
							Summer Program </a>
						</li>
						<li class="" data-grdmin="9" data-grdmax="9">
							<a href="#tab_1_4" data-toggle="tab" aria-expanded="false">
							Grad.Package </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="tab_1_1"  data-target="sticker"  data-grdmin="1" data-grdmax="13">

                          <?php echo (string)view($views.'sub.otherfees.carstick',array());?>

						</div>
						<div class="tab-pane fade" id="tab_1_2" data-target="uniform" data-grdmin="1" data-grdmax="13">
						  <div class="" data-grdmin="1" data-grdmax="9"><?php echo (string)view($views.'sub.otherfees.uniforma',array());?></div>
						  <div class="" data-grdmin="10" data-grdmax="13"><?php echo (string)view($views.'sub.otherfees.uniformb',array());?></div>
						</div>
					    <div class="tab-pane fade" id="tab_1_3" data-target="summer" data-grdmin="1" data-grdmax="13">
                          <?php echo (string)view($views.'sub.otherfees.summer',array());?>
					    </div>
					    <div class="tab-pane fade" id="tab_1_4" data-target="graduate"  data-grdmin="9" data-grdmax="9">
                          <?php echo (string)view($views.'sub.otherfees.graduate',array());?>
					    </div>
					</div>
				</div>
				<div class="col-sm-12">
				  <div class="table-responsive">
					<table class="table table-bordered table-condense">
					  <tbody>
						<tr><td>Total Car Sticker:</td><td class="text-right" data-source="sticker">0.00</td></tr>
						<tr><td>Total Uniform:</td><td class="text-right" data-source="uniform">0.00</td></tr>
						<tr><td>Total Summer Program:</td><td class="text-right" data-source="summer">0.00</td></tr>
						<tr><td>Total Grad. Package:</td><td class="text-right" data-target="graduate">0.00</td></tr>
					  </tbody>
					</table>
				  </div>
				</div>
			</div>
         </div>
     </div>
	 </div>
	 <div class="portlet-footer">
		<div class="row" style="text-align:right">
		  <div class="col-sm-3 col-md-2 col-md-offset-4">
            <div class="well well-sm bg-grey">
            <span><h4 class="netassess">0.00</h4> NET ASSESSMENT<span>
            </div>
            </div>
		  <div class="col-sm-3 col-md-2">
             <div class="well well-sm bg-green">
                <span><h4 class="totalpayment">0.00</h4> TOTAL PAYMENT<span></div>
             </div>

		  <div class="col-sm-3 col-md-2">
            <div class="well well-sm bg-yellow-casablanca">
            <span><h4 class="totalbalance">0.00</h4> TOTAL BALANCE<span>
            </div>

          </div>
            <div class="col-sm-3 col-md-2">
                <div class="well well-sm bg-purple">
                    <span class=""><h4 class="totaloutbalance">0.00</h4> Outstanding Balance</span>
                </div>
            </div>
		</div>

	 </div>
</div>
<div id="modal_template" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select a template</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="radio-list" style="margin-bottom:10px;">
				        <br/>
						<label class="radio-inline"><div class="radio" id="optschemea"><input name="optscheme" id="optschemea" value="-1" type="radio" checked></div> All</label>
						<label class="radio-inline"><div class="radio" id="optschemeb"><input name="optscheme" id="optschemeb" value="0" type="radio"></div> Full Payment</label>
						<label class="radio-inline"><div class="radio" id="optschemec"><input name="optscheme" id="optschemec" value="1" type="radio"></div> Semestral</label>
						<label class="radio-inline"><div class="radio" id="optschemed"><input name="optscheme" id="optschemed" value="2" type="radio"></div> Quarterly</label>
						<label class="radio-inline"><div class="radio" id="optschemee"><input name="optscheme" id="optschemee" value="3" type="radio"></div> Monthly</label>
				  </div>
				  <div class="table-responsive modal_template_list" style="max-height:400px; overflow:scroll;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>TemplateID</th>
						    <th>TemplateCode</th>
						</thead>
						<tbody>
						 <tr><td class="text-center" colspan="2"><i class="fa fa-warning"></i> No data available</td></tr>
						</tbody>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-templ-select">Select</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal_filter" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select from the list</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="input-group">
				    <input class="form-control" id="txt-modal-search" data-button=".btn-modal-search" placeholder="StudentNo, LastName, Firstname" type="text">
					<span class="input-group-addon btn-modal-search" data-source="#txt-modal-search"><i class="fa fa-search"></i></span>
				  </div>
				  <div class="table-responsive modal_list" style="max-height:400px; overflow:scroll;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>Last Name</th>
						    <th>First Name</th>
						    <th>Middle Name</th>
						    <th>Program</th>
						    <th>Level</th>
						</thead>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-modal-select">Select</button>
				</div>
			</div>
		</div>
	</div>
</div>

