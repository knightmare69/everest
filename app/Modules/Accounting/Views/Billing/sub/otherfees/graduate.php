<div class="col-sm-12">
  <div class="table-responsive">
	<table class="table table-bordered table-condense">
	  <thead>
		<th colspan="3">Description</th>
		<th>Price</th>
		<th>Quantity</th>
		<th>Amount</th>
	  </thead>
	  <tbody>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="0">
		  <td colspan="3"><b>Blazer Package/Gala Uniform Package</b></td>
		  <td class="component text-right" data-price="html">0.00</td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
		<tr class="formula-row" data-operate="multiply" data-otxn-acctid="0">
		  <td colspan="3"><b>Graduation Photos</b></td>
		  <td class="component text-right" data-price="html">0.00</td>
		  <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
		  <td class="formula-total text-right" data-amt="html">0.00</td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td colspan="2">Graduation Portrait Pictures</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td width="10px;"></td>
		  <td>One(1) 8R Individual Shot</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td width="10px;"></td>
		  <td>One(1) 5R Individual Shot</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td width="10px;"></td>
		  <td>Four(4) Wallet Size</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td colspan="2">Graduation Day Pictures</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td width="10px;"></td>
		  <td>Two(2) 8R Photo Of Child On Stage</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
		<tr>
		  <td width="10px;"></td>
		  <td width="10px;"></td>
		  <td>One(1) 8R Family Picture</td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
	 </tbody>
	</table>
 </div>							
</div>