<div class="col-sm-12">
   <table class="table table-bordered table-condense">
	  <thead>
		<th colspan="2">Description</th>
		<th>Price</th>
		<th>Quantity</th>
		<th>Amount</th>
	  </thead>
	  <tbody>
		 <tr><td colspan="5">Boy's Regular Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1025" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS White Polo</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="350.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1026" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Gray Pants</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="700.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">Girl's Regular Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1027" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Polo Jack Blouse</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="350.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1028" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Plaid Skirt</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="1300.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">PE Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1015" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS PE Shirt</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="285.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1016" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS PE Pants</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="315.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1017" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS PE Shorts</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="240.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1018" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">Vest Reversible</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="180.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">Outerwear</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1019" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">Zip-up Sweater(Boys)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="385.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1020" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">Button-down Cardigan(Girls)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="360.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr><td colspan="5">Gala Uniform</td></tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1034" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Gala Uniform</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="4300.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1035" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Blazer Only</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="3800.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1036" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Necktie(Boys)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="400.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1037" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Knee High Socks(Girls)</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="200.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
		 <tr class="formula-row" data-operate="multiply" data-otxn-acctid="1038" data-grdmin="10" data-grdmax="13">
			 <td width="5%"></td>
			 <td width="60%">HS Everest Pins</td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-price="value" value="120.00"></td>
			 <td width="5%"><input type="text" class="component text-right numberonly" style="border:none;" data-qty="value" value="0"></td>
			 <td class="formula-total text-right" data-amt="html">0.00</td>
		 </tr>
	  </tbody>
   </table>
</div>
