  <div class="col-sm-12 formula-row" data-operate="multiply" data-otxn-acctid="1007">
	<div class="col-sm-4 col-md-3" style="margin-bottom:2px;">
		<label class="control-label col-md-4">Price:</label>
		<div class="col-md-8">
		 <label class="form-control component numberonly" data-price="html">150.00</label>
		</div>
	</div>
	<div class="col-sm-4 col-md-4" style="margin-bottom:2px;">
		<label class="control-label col-md-4">Quantity:</label>
		<div class="col-md-8">
		 <input type="text" class="form-control component numberonly" data-qty="value" value="0"/>
		</div>
	</div>
	<div class="col-sm-4 col-md-5" style="margin-bottom:2px;">
		<label class="control-label col-md-4">Amount Due:</label>
		<div class="col-md-8">
		 <label class="form-control tb-total formula-total numberonly" data-amt="html">0.00</label>
		</div>
	</div>
  </div>