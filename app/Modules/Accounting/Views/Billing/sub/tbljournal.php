<?php
$detail  = ((isset($detail))?$detail:array());
$i=0;  
foreach($assess as $k=>$r){
  $netass  = $r['AssessFee'] - ($r['Discount']+$r['FinancialAid']);
  //$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']) - ($r['Discount']+$r['FinancialAid']);
  $balance = $netass - $r['ActualPayment'];	
  if($r['AccountID']!='111' && $r['AccountID']!='110'){
    $r['1stPayment'] = $netass;  
  }else{
	$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);  
    $balance = $netass - $r['ActualPayment'];	
  }
  $i++;
  ?>
  
  <tr data-entryid="<?=$r['EntryID']?>" data-acctid="<?=$r['AccountID']?>" data-splfee="<?=$r['SpecialFee']?>" data-installment="<?=$r['InstallmentExcluded']?>">
           <td ><?=$i?>.</td>
		   <td  data-acctcode="<?=$r['AcctCode']?>"><?=$r['AcctCode']?></td>
		   <td ><?=$r['AcctName']?></td>
           <td class="text-right autofit bold"  data-assess="<?=$r['AssessFee']?>"><?=number_format($r['AssessFee'],2,'.',',')?></td>
		   <td class="text-right autofit"  data-discount="<?=$r['Discount']?>"><?=number_format($r['Discount'],2,'.',',')?></td>
		   <td class="text-right"  data-financial="<?=$r['FinancialAid']?>"><?=number_format($r['FinancialAid'],2,'.',',')?></td>
		   <td class="text-right bold"  data-netassess="<?=$netass?>"><?=number_format($netass,2,'.',',')?></td>
		   <td class="text-right autofit"  data-paymenta="<?= $r['1stPayment'] ?>"><?=number_format($r['1stPayment'],2,'.',',')?></td>
		   <td class="text-right autofit"  data-paymentb="<?= $r['2ndPayment'] ?>"><?=number_format($r['2ndPayment'],2,'.',',')?></td>
		   <td class="text-right autofit"  data-paymentc="<?= $r['3rdPayment'] ?>"><?=number_format($r['3rdPayment'],2,'.',',')?></td>
		   <td class="text-right autofit"  data-paymentd="<?= $r['4thPayment'] ?>"><?=number_format($r['4thPayment'],2,'.',',')?></td>
           <td class="text-right autofit"  data-paymente="<?= $r['5thPayment'] ?>"><?=number_format($r['5thPayment'],2,'.',',')?></td>
		   <td class="text-right autofit"  data-paydiscount="<?= $r['PaymentDiscount'] ?>"><?=number_format($r['PaymentDiscount'],2,'.',',')?></td>
		   <td class="text-right bold  "  data-payment="<?= $r['ActualPayment'] ?>"><?=number_format($r['ActualPayment'],2,'.',',')?></td>
		   <td class="text-right"  data-creditmemo="<?= $r['CreditMemo'] ?>"><?=number_format($r['CreditMemo'],2,'.',',')?></td>
		   <td class="text-right"  data-refund="<?= $r['Refund'] ?>"><?=number_format($r['Refund'],2,'.',',')?></td>
		   <td class="text-right bold bg-yellow-casablanca"  data-balance="<?=$balance?>"><?=number_format($balance,2,'.',',')?></td>
		   <td ><?=$r['Remarks']?></td>
		   <td ><?=$r['DMCMRefNo']?></td>		   		   		   
		   <td ><?=$r['SeqNo']?></td>
	</tr>	
<?php } ?>