<div class="col-sm-12">
<div class="table-responsive xinstruction">
<table id="tblassess" class="table table-bordered" style="margin-bottom:15px;">
<thead>
	<th class="text-center">Description</th>
	<th class="text-center">AssessFee</th>
	<th class="text-center">Discount</th>
	<th class="text-center">Amount Due</th>
	<th class="text-center">2nd Payment</th>
	<th class="text-center">3rd Payment</th>
	<th class="text-center">4th Payment</th>
</thead>
<tbody>
<?php
$detail  = ((isset($detail))?$detail:array());  
foreach($assess as $k=>$r){
//$netass  = $r['AssessFee'] - ($r['Discount']+$r['FinancialAid']);
  $netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']) - ($r['Discount']+$r['FinancialAid']);
  $balance = $netass - $r['ActualPayment'];	
  if($r['AccountID']!='111' && $r['AccountID']!='110'){
    $r['1stPayment'] = $netass;  
  }else{
	$netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);  
    $balance = $netass - $r['ActualPayment'];	
  }
  
  echo '<tr data-entryid="'.$r['EntryID'].'" data-acctid="'.$r['AccountID'].'">
		   <td style="font-size:16px !important;">'.$r['AcctName'].'</td>
           <td class="text-right" style="font-size:12px !important;" data-assess="'.$r['AssessFee'].'">'.number_format($r['AssessFee'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:12px !important;" data-discount="'.($r['Discount']+$r['FinancialAid']).'">'.number_format(($r['Discount']+$r['FinancialAid']),2,'.',',').'</td>
		   <td class="text-right" style="font-size:12px !important;" data-paymenta="'.number_format($r['1stPayment'],2,'.','').'">'.number_format($r['1stPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:12px !important;" data-paymentb="'.number_format($r['2ndPayment'],2,'.','').'">'.number_format($r['2ndPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:12px !important;" data-paymentc="'.number_format($r['3rdPayment'],2,'.','').'">'.number_format($r['3rdPayment'],2,'.',',').'</td>
		   <td class="text-right" style="font-size:12px !important;" data-paymentd="'.number_format($r['4thPayment'],2,'.','').'">'.number_format($r['4thPayment'],2,'.',',').'</td>
		</tr>'; 	
}
?>
</tbody>
<tfoot>
 <tr class="totalrow">
   <td><strong>TOTAL:</strong></td>
   <td class="text-right">0.00</td>
   <td class="text-right">0.00</td>
   <td class="text-right amountdue">0.00</td>
   <td class="text-right">0.00</td>
   <td class="text-right">0.00</td>
   <td class="text-right">0.00</td>
 </tr>
 <tr><td colspan="7"></td></tr>
 <tr>
  <td colspan="7" style="padding:0px;">   
	<table class="table table-bordered table-condense" style="margin-bottom:0px;">
	  <tbody>
		<tr class="warning"><td>Total Amount Due:</td><td class="text-right" data-source="amountdue">0.00</td></tr>  
		<tr class=""><td colspan="2"></td></tr>  
		<tr class="hidden"><td>Total Car Sticker:</td><td class="text-right" data-source="sticker">0.00</td></tr>  
		<tr class="hidden"><td>Total Uniform:</td><td class="text-right" data-source="uniform">0.00</td></tr>
		<tr class="hidden"><td>Total Summer Program:</td><td class="text-right" data-source="summer">0.00</td></tr>
		<tr class="hidden"><td>Total Grad. Package:</td><td class="text-right" data-target="graduate">0.00</td></tr>
	  </tbody>
	</table>
  </td>
 </tr>
</tfoot>
</table>
</div>
<div class="col-sm-12">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab_1_1" data-toggle="tab" aria-expanded="true">
			Car Sticker </a>
		</li>
		<li class="">
			<a href="#tab_1_2" data-toggle="tab" aria-expanded="false">
			Uniform </a>
		</li>
		<li class="">
			<a href="#tab_1_3" data-toggle="tab" aria-expanded="false">
			Summer Program </a>
		</li>
		<li class="" data-grdmin="9" data-grdmax="9">
			<a href="#tab_1_4" data-toggle="tab" aria-expanded="false">
			Grad.Package </a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade active in" id="tab_1_1"  data-target="sticker"  data-grdmin="1" data-grdmax="13">
		  <?php echo (string)view('Accounting.Views.sub.otherfees.carstick',array());?>
		</div>
		<div class="tab-pane fade" id="tab_1_2" data-target="uniform" data-grdmin="1" data-grdmax="13">
		  <div class="" data-grdmin="1" data-grdmax="9"><?php echo (string)view('Accounting.Views.sub.otherfees.uniforma',array());?></div>
		  <div class="" data-grdmin="10" data-grdmax="13"><?php echo (string)view('Accounting.Views.sub.otherfees.uniformb',array());?></div>
		</div>
		<div class="tab-pane fade" id="tab_1_3" data-target="summer" data-grdmin="1" data-grdmax="13">
		  <?php echo (string)view('Accounting.Views.sub.otherfees.summer',array());?>
		</div>	
		<div class="tab-pane fade" id="tab_1_4" data-target="graduate"  data-grdmin="9" data-grdmax="9">
		  <?php echo (string)view('Accounting.Views.sub.otherfees.graduate',array());?>
		</div>					
	</div>  
</div>
</div>