<?php
   if(isset($disc)){
    foreach($disc as $d){
	  echo '<tr data-id="'.$d->RefNo.'" data-refno="'.$d->RefNo.'" data-provider="'.$d->SchoProviderID.'" data-grant="'.$d->GrantTemplateID.'">
			  <td width="100px;"><button class="btn btn-sm btn-danger btndelete"><i class="fa fa-trash-o"></i> Delete</button></td>
			  <td>'.$d->ProvCode.'</td>
			  <td>'.$d->ShortName.'</td>
			  <td class="text-center">'.(($d->TotalDiscount==0)?($d->Percentage.'%'):($d->TotalDiscount)).'</td>
			  <td class="text-right"><i class="fa '.(($d->ApplyOnNet==1)?'fa-check-square-o':'fa-square-o').'"></i></td>
			  <td>'.$d->Remarks.'</td>
			</tr>';
	}
   }else{
	echo '<tr data-id="0">
		    <td colspan="7" class="text-center">No data available</td>
		  </tr>';
   }
?>

