<div class="row">
<div class="col-sm-12">
<div class="portlet red box dvlist tabbable tabs-left" style="font-size:10px;">
	<div class="portlet-title">
	 <div class="pull-right" style="padding-top:8px;">
	    <button class="btn btn-sm btn-success btnsave"><i class="fa fa-save"></i> Save</button>
	    <button class="btn btn-sm btn-danger btnremove"><i class="fa fa-times"></i> Remove</button>
	    <button class="btn btn-sm btn-default btnprint hidden"><i class="fa fa-print"></i> Print</button>
	 </div>  
	</div>
	<div class="portlet-body">
	 <div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				<label class="col-sm-4" style="padding-left:1px !important;">Student:<b class="stdno"></b></label>
				<label class="col-sm-4">Level:<b class="yrlvl"></b></label>
				<label class="col-sm-4">Program:<b class="program"></b></label>
				<div class="col-sm-12 input-group btnfilter" data-source="student" data-parameter=".stdname" data-id=".stdname">
				   <label class="col-sm-12 form-control stdname"></label>
				   <span class="input-group-addon btnfilter" data-source="student" data-parameter=".stdname" data-id=".stdname">
				     <i class="fa fa-search"></i>
				   </span>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>Reg ID:</label>
				<label class="form-control regid"></label>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>Reg Date:</label>
				<label class="form-control regdt"></label>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>No. of Siblings:</label>
				<label class="form-control sibling"></label>
			</div>
		</div>
		<div class="col-sm-12">
		<div class="col-sm-12">
		    <div class="row">
			  <div class="row">
			    <div class="col-sm-2">
				  Provider:
				  <select class="form-control" id="provider" name="provider">
				    <option value="1">Discount</option>
				    <option value="1">Scholarship</option>
				    <option value="2">Guarantor</option>
				  </select>
				</div>
			    <div class="col-sm-4">
				  <br>
				  <div class="input-group">
				    <input type="hidden" id="scholprovid" name="scholprovid">
					<input type="text" class="form-control" id="scholprovider" name="scholprovider" data-id="6" data-button=".btnfilter">
					<span class="input-group-addon btnfilter" data-source="scholarship" data-parameter="#scholprovider" data-id="#scholprovid"><i class="fa fa-search"></i></span>
				  </div>
				</div>
			    <div class="col-sm-6">
				  <br>
				  <select class="form-control" id="schoOption" name="schoOption">
						<option value="1">By Account</option>
						<option value="2">Grant Template</option>
						<option value="3">Custom</option>
				  </select>
				</div>
			  </div>
			  <div class="row">
				<div class="col-sm-6">
				  <br>
				  <select class="form-control hidden" id="application" name="application">
				    <option value="0">Half/Full Scholar On Tuition</option>
				    <option value="1">Half/Full Scholar On All Accounts</option>
				    <option value="2">Allotted Amount</option>
				    <option value="3">Remaining Balance</option>
				  </select>
				  <select class="form-control" id="template" name="template">
				    <option value="-1">-SELECT ONE-</option>
				  <option value="1">Sibling 02</option><option value="3">Sibling 03</option><option value="4">Sibling 04</option><option value="5">Sibling 05</option></select>
				</div>	
			    <div class="col-sm-6">
				  <br>
				  <div class="input-group hidden"> 
				   <input type="text" class="form-control numberonly text-align-right" id="scholamount" name="scholamount" value="0.00">
				   <span class="input-group-btn">
				    <div class="btn-group">
				     <button id="scholamttype" class="btn btn-success dropdown-toggle" type="button" data-value="0" data-toggle="dropdown" aria-expanded="true">Perc.</button>
					 <ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(0);" data-btntarget="#scholamttype" data-label="Perc." data-value="0">Percentage</a></li>
						<li><a href="javascript:void(0);" data-btntarget="#scholamttype" data-label="Amt." data-value="1">Amount</a></li>
					 </ul>						
					</div>
				   </span>
				  </div>
				  <div class="optgrant text-align-right">
				    <div class="col-sm-6 radio-list">
						<label class="radio-inline"><div class="radio" id="uniform-optgrantbyrate"><input name="optgranttempl" id="optgrantbyrate" value="0" type="radio" checked=""></div> Rate</label>
						<label class="radio-inline"><div class="radio" id="uniform-optgrantbyamount"><input name="optgranttempl" id="optgrantbyamount" value="1" type="radio"></div> Amount</label>
					</div>
                    <div class="col-sm-6">
					    <input type="text" class="form-control text-right numberonly" id="customdisc" name="customdisc" value="0.00">
                    </div>					
				  </div>
				</div>
			  </div>
			  <div class="col-sm-12" style="margin-bottom:10px;">
				<div class="col-sm-6">
				  <br>
				  <div class="checkbox-list">
				    <label class="checkbox-inline"><div class="checker" id="uniform-applyNet"><span class="checked"><input type="checkbox" id="applyNet" name="applyNet" checked=""></span></div> Apply To Net</label>
				    <label class="checkbox-inline"><div class="checker" id="uniform-applyTotal"><span><input type="checkbox" id="applyTotal" name="applyTotal"></span></div> Apply To Total</label>
				  </div>	 
				</div>
				<div class="col-sm-6">
				  <br>
				  <div class="optgrant">
				  </div>	 
				</div>
			  </div>
			</div>  
		</div>
		<div class="col-sm-12" style="padding:1px !important;">
		    <div class="table-responsive" style="max-height:600px !important;overflow:auto !important;">
				<table class="table table-bordered table-condense" id="tbldiscount">
					<thead>
						<th width="100px;"><button class="btn btn-sm btn-info btnrefresh"><i class="fa fa-refresh"></i></button>
						                  <button class="btn btn-sm green btnadd"><i class="fa fa-plus"></i></button></th>
						<th class="text-center">Provider</th>	
						<th class="text-center">Template</th>	
						<th class="text-center">Amt/Percent</th>	
						<th class="text-center">Apply On</th>	
						<th class="text-center">Remarks</th>	
					</thead>
					<tbody>
						@include($rviews.'sub.discount')
					</tbody>
				</table>
			</div>
		</div>
	 </div>
	</div>
</div>
</div>
	
</div>
<div id="modal_filter" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select from the list</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="input-group">
				    <input class="form-control" id="txt-modal-search" data-button=".btn-modal-search" placeholder="StudentNo, LastName, Firstname" type="text">
					<span class="input-group-addon btn-modal-search" data-source="#txt-modal-search"><i class="fa fa-search"></i></span>
				  </div>
				  <div class="table-responsive modal_list" style="max-height:360px !important; overflow:auto;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>Acct Code</th>
						    <th>Acct Name</th>
						    <th>Amount</th>
						</thead>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-modal-select">Select</button>
				</div>
			</div>
		</div>
	</div>	
</div>