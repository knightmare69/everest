<?php
    $totalb= 0;
    $termb = 0;
    $termc = 0;
	$prvtrm= 0;
?>
<style>#ledger_div td{font-size:10px !important;padding:1px !important;}#ledger_div th{font-size:12px !important;}</style>
@if(!empty($data))
        <table id="student_ledger_tbl" class="table table-striped table-bordered table-condensed table-hover font-md" style="cursor: pointer;" >
        <thead>
        <tr>
        {{-- <th style="text-align:center;">Academic Year and Term </th> --}}
        <th>Trans Date</th>
        <th>Code</th>
        <th colspan="2">Ref #</th>
        <th class="text-center">Debit</th>
        <th class="text-center">Credit</th>
        <th class="text-center">Balance</th>
        <th>Remarks</th>
        <th class="autofit">Posted</th>
        <th>DatePosted</th>
        </tr>
        </thead>
        <tbody>


    <?php
       $data_ = json_decode(json_encode($data), true);
        $new = array();
        foreach ($data_ as $item) {
            if (!isset($new[$item['AcademicYearTerm']])) {
                $new[$item['AcademicYearTerm']] = array();
            }
            $new[$item['AcademicYearTerm']][] = $item;
        };

        foreach ($new as $key => $val) {

            $totald= 0;

     ?>
     <tr >
         <td   style:"text-align:left;" colspan="10" >
             <span class="caption-subject font-red-sunglo bold uppercase" > {{($key)}} </span>
         </td>
     </tr>
        <?php
        foreach ($val as $d) {
		    /*if ($d['TransCode']=='DMCM' && $d['ReferenceNo']==''){
			   $d['NonLedger'] = 1;
			}*/
			
            $credit = ($d['FinancialAid'] > 0 ?  $d['FinancialAid'] : $d['Credit']);
            $non_ledger_row = 0;
            $non_ledger = 0;
            $debit_ = 0;
            $cbalance = ($d['NonLedger'] > 0 ? 0  :($d['AssessFee'] - $credit)) ;
            $dbalance = ($d['Debit'] -  $d['Credit']);

            $remarks = "";

            if($d['TransID'] == '20'){
                $remarks = $d['Remarks'].':'.$d['ORPaymentFor'].' ['.$d['ORRefNo'].']';
            }else{
                $remarks = $d['Remarks'];
            }

            if ($d['NonLedger'] > 0) {
                $non_ledger_row  =  $d['Credit'] -  $d['NonLedger'];
                $cbalance = $cbalance - $non_ledger_row;
                $non_ledger =  $d['NonLedger'] - 0.00;
                $dbalance = $dbalance  + $non_ledger;
            }
            if ($d['TransCode']=='DMCM' &&  $d['Debit'] > 0) {
                $debit_ =  $d['Debit'] - 0.00;
                $cbalance = $cbalance + $debit_;
            }
            // if ($d['TransCode']=='DMCM' &&  $d['Credit'] > 0) {
            //     $credit_ =  $d['Credit'] - 0.00;
            //     $cbalance = $cbalance - $credit_;
            //       $dbalance = $dbalance - $credit_;
            // }

            if ($d['PostedDate'] == null && $d['Posted'] == 0) {
                $f = $d['Credit'] - 0.00;
                $cbalance = $cbalance + $f;
                $dbalance = $dbalance  + $f;
            }
            if($d['TransCode'] == 'REG' && $prvtrm!=$d['ReferenceNo']){
			$prvtrm=$d['ReferenceNo'];
            ?>
            <script>
                zz +=' <option data-refnum="{{$d['ReferenceNo']}}" value="{{$d['TermID']}}">{{$d['ReferenceNo']}}</option>';
            </script>
            <?php
            }
            $totalb += $cbalance;
            $totald += $dbalance;
            $transdate = $d['TransDate'];
            $posteddate = ($d['PostedDate'] == null ? null :$d['PostedDate']);
            $posteddate = ($posteddate == null ? null :$posteddate) ;
            $repeat = 1;
            $repeat2 = 1 ;
            $colorcode = '';
            if ($d['TransCode']=='DMCM') {
                $colorcode = 'yellow-saffron';
				if($d['ReferenceNo']=='' || $d['ReferenceNo']==0){
					$d['NonLedger'] = $d['Credit'];
				}
            } elseif ($d['TransCode'] == 'REG') {
                $colorcode = 'blue-madison';
            } elseif ($d['TransCode'] == 'OR') {
                $colorcode = 'green-jungle';
            }
            if ($d['FinancialAid'] > 0 || $non_ledger_row > 0) {
                $repeat = 2;
            }

            for ($j = 0; $j < $repeat; $j++) {
			    if($d['TransCode']=='DMCM'){
				  $btn = '<a href="'.url('accounting/memo').'" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>';
				}elseif($d['TransCode']=='REG'){
				  $btn = '<a href="'.url('accounting').'" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>';
				}else{
				  $btn = '<button class="btn btn-xs btn-info btnmodify"><i class="fa fa-edit"></i></button>';
				}
                ?>
                <tr data-termid="{{$d['TermID']}}" data-posted="{{$d['Posted']}}" data-or_refno="{{$d['ORRefNo']}}" data-dmcmrefno="{{$d['DMCMRefNo']}}" data-transdate="{{$d['TransDate']}}"
                data-sname="{{$info->LastName.', '.$info->FirstName.' '.$info->Middlename}}" data-studno="{{$info->StudentNo}}" data-code="{{$d['TransCode']}}" data-ledger="{{$d['NonLedger']}}" data-ref="{{$d['ReferenceNo']}}"
                class="print_assessment {{($d['NonLedger'] > 0  ? ($j == 0 ? 'danger': '')  : "")}}"  >

                    <td style="width:160px;" class="highlight autofit"><span class="todo-tasklist-date">{{date('Y-m-d',strtotime($transdate))}} </span></td>
                    <td class="autofit"><center><button style="font-size: 1em;width:56px;" type="button" class="btn btn-circle {{($j==0 && $d['NonLedger'] > 0 ?'green-seagreen':$colorcode)}} btn-xs"> {{$d['TransCode']}} </button></td>
                    <td width="10px"><?php echo $btn;?></td>
					<td class="autofit" ><a href="javascript:void();" >{{$d['ReferenceNo'] }}</a></td>
                    <td class="bold " style="text-align:right;">{{number_format(($j==0 ? ($d['TransCode']=='DMCM' && $d['Debit'] > 0 ?$d['Debit'] :($d['AssessFee'])):0 ),2,'.',',')}}</td>
                    <td class="bold " style="text-align:right;">{{number_format(($j==0 && $d['NonLedger'] == 0 ? $d['Credit'] :($d['NonLedger'] > 0  ? ($j == 0 ? $d['NonLedger'] : $non_ledger_row ) :$d['FinancialAid'])),2,'.',',')}}</td>
                    <td class="bold " style="text-align:right;">{{number_format(($j==0 && $d['FinancialAid'] > 0 ?$d['AssessFee'] :$totalb ),2,'.',',')}} </td>
                    <td>{{$remarks}}</td>
                    <td class="text-center">
                        <div class="icheckbox_flat-green {{($d['NonLedger'] > 0  ? ($j == 1 &&  $d['Posted'] == 1? 'checked':'' ) : 'checked')}}">
                            <input {{($d['Posted'] === 1 ? 'checked' : '')}} type="checkbox"  style="position: absolute; opacity: 0;" class="icheck" >
                        </div>
                    </td>
                    <td class="autofit" style="width:160px;"><span class="todo-tasklist-date">{{($d['NonLedger'] > 0  ? ( $j == 0 ?'*Non-Ledger*' : ($posteddate == null ? '':(date('Y-m-d',strtotime($posteddate)))) ) : (date('Y-m-d',strtotime($posteddate))))}} </span></td>
                </tr>
            <?php
            }
            $termb = $totalb;
            $termc = $totald;
            ?>
            </tr>
            <?php
        } ?>
        <tr class="bg-grey" >
            <td colspan="5" class="text-right bold" >
                *** Ending Balance for  {{($key === 'null' ? '' : $key) }} ***
            </td>
            <td class="text-right bold">
                PHP
            </td>
            <td class="text-right bold">
            {{number_format($termb,2,'.',',')}}
            </td>
            <?php
             // $termc += $termb;
               ?>
            <td class="bold text-left font-red-sunglo">{{('Term Balance: '.number_format($termc,2,'.',',')  )}}
            </td>
            <td></td>
            <td></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
@endif
