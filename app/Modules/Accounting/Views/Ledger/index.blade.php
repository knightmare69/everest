<?php
// error_print($progs);
// die();
///$info = json_decode(json_encode($info), true);
?>
<script>
var zz= '';
</script>

<style >

body {
    color: #333333;
    font-family: "Open Sans", sans-serif;
    padding: 0px !important;
    margin: 0px !important;
    font-size: 11px;
    direction: ltr;
}
.font-red-sunglo {
    color: #e26a6a !important;
    font-size: 1.4em;
    
    vertical-align: middle;
    margin: auto;
}
.todo-tasklist-date {
    color: #637b89 !important;
    /* margin-right: 12px; */
}
.bold-letter{
    font-weight: bold;
}

.input-group .input-group-addon {
    border-color: #e5e5e5;
    background: #1853ad;
    min-width: 39px;
}
.input-group .input-group-addon > i {
    color: #ffffff;
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption"> <i class="fa fa-codepen"></i>Student Ledger </div>
                <div id="print_btn" class="actions">
                    @if(!@empty ($info))
                        <a  class="btn btn-info btn-sm" id="modify_or" data-info="">
                        <i class="fa fa-plus"></i> Modify </a>
                        <a  class="btn green btn-sm" id="refresh_ledger" data-info="">
                        <i class="fa fa-refresh"></i> Refresh </a>
                        <a href="#" class="btn default btn-sm" id="print_ledger_btn" data-studnumber="{{$info->StudentNo}}">
                        <i class="fa fa-print"></i> Print </a>
                    @endif
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                <div class="row">
                    <div class="col-lg-12">
                    @if(empty($info))
                        <form  id="search-form">
                            <div class="form-body">
                                <div class="tab-content">
                                    <div class="col-sm-3 bold">Student No:  <b class="student_no bold" data-stats="1"></b></div>
                                    <div class="col-sm-3">YearLevel: <b class="yrlvl"></b></div>
                                    <div class="col-sm-4 col-lg-5">Acad Program: <b class="prog"></b></div>
                                                    {{-- <div class="col-sm-4 col-md-2 col-lg-1"><label></label></div> --}}
                                                    <div class="col-sm-8 col-md-10 col-lg-11">
                                                        <div class="input-group">
                                                            <input value=""  type="text" class="form-control bold font-lg" id="student-search-text" name="student-search-text" placeholder="Search Student Name">
                                                            <span id="btn_search" class="input-group-addon btn btn-primary btn-sm" ><i class="fa fa-search"></i> Search</span>
                                                        </div>
                                                        <p></p>
                                                    </div>
                                                </div>
                                                <div class="form-group hide">
                                                    <select class="form-control select2 input-sm" name="search-campus" id="search-campus">
                                                        @if(!empty($campus))
                                                            @foreach($campus as $_campus)
                                                                <option value="{{ encode($_campus->CampusID) }}">{{ $_campus->ShortName }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">No selection.</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    @else
                                        <form   id="search-form">
                                            <div class="form-body">
                                                <div class="tab-content">

                                                    <div class="col-sm-3">Student No:  {{$info->StudentNo}}  <b class="student_no" data-stats="1"></b></div>
                                                    <div class="col-sm-3">YearLevel: {{$info->YearLevelName}} <b class="yrlvl"></b></div>
                                                    <div class="col-sm-4 col-lg-5">Acad Program: {{$info->Program}} <b class="prog"></b></div>
                                                    {{-- <div class="col-sm-4 col-md-2 col-lg-1"><label></label></div> --}}
                                                    <div class="col-sm-8 col-md-10 col-lg-11">
                                                        <div class="input-group">
                                                            <input value="{{$info->LastName.', '.$info->FirstName.' '.$info->Middlename}}"  type="text" class="form-control font-lg bold" id="student-search-text" name="student-search-text" placeholder="Search Student Name">
                                                            <span id="btn_search" class="input-group-addon btn btn-primary btn-sm" ><i class="fa fa-search"></i> Search</span>

                                                        </div>
                                                        <p></p>
                                                    </div>
                                                </div>
                                                <div class="form-group hide">
                                                    <select class="form-control select2 input-sm" name="search-campus" id="search-campus">
                                                        @if(!empty($campus))
                                                            @foreach($campus as $_campus)
                                                                <option value="{{ encode($_campus->CampusID) }}">{{ $_campus->ShortName }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">No selection.</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    @endif
                                </div>

                    <div class="col-lg-12">
                        <div id="ledger_div" class="col-sm-12">
                            @include($views.'ledgertbl')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include($views.'modifyor')