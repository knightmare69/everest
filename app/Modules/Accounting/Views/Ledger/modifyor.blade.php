<style>
.small_font{
    font-size: 0.9em;
}

.center_div {
    position: fixed;
    width: 100px;
    height: 200px;
    top: 65%;
    left: 50%;
    margin-top: -100px;
    margin-left: -50px;
    text-align: center;
}
.hasFound{
    background-color: blue !important;
}
</style>

<div id="modify_or_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" style="width:75%;">
        <!--Begin mODAL CONTENT -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modifying and Mapping References</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-6">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-codepen"></i>Official Receipt
                                    </div>
                                    <div class="tools">

                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <p>Selected OR Number</p>
                                                <strong><p style="font-size:1.5em;color:red;" id="or_no"> </p></strong>
                                            </div>
                                            <div class="col-lg-8 ">
                                                <div class="checkbox">
                                                    <label class="small_font">
                                                      <input type="checkbox" id="post_payment">Post Payment?</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="small_font">
                                                      <input type="checkbox" id="checkUpdateOR">Update OR Trans/Ref upon save?</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="small_font">
                                                      <input type="checkbox" id="checkDistributeTerms">Update AY Term for each Account?</label>
                                                </div>

                                            </div>
                                            <div id="or_div_tbl">

                                        </div>


                                        <p id="or_total">Total</p>
                                    </div>




							</div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>



                        <div class="col-md-6">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-info"></i>Assessment/Billing
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">


                                    <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-inline" role="form">
                                            <p></p>
                                            <div class="form-group ">
                                                <label class="sr-only" for="exampleInputEmail2">Select</label>
                                                <select class="form-control small_font" id="assess_type">
                                                    <option>Enrollment</option>
                                                    <option>Add/Drop/Change Subject</option>
                                                    <option>Transcript of Record</option>
                                                    <option>Thesis</option>
                                                    <option>Graduation</option>
                                                    <option>Admission/Testing</option>
                                                    <option>Other Assessment</option>
                                                    <option>Beginning Balance</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                <select class="form-control small_font" id="reg_no" onchange="orChange()">
                                                    <option>101527</option>
                                                    <option>1234233</option>
                                                    <option>1234</option>
                                                    <option>324</option>
                                                    <option>54233</option>
                                                </select>
                                            </div>
                                            {{-- <div class="checkbox">
                                                <label>
                                                <input type="checkbox"> Remember me </label>
                                            </div> --}}
                                            <button type="button" class="btn btn-default small_font">Refresh</button>
                                        </form>
                                        <hr>

                                        <div style="margin-top:27px" id="assessment_div_tbl">



                                    </div>


                                        <p id="assess_total">Total</p>

                                    </div>
                                </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>
                    <div class="center_div">

                        <button style="width:68px;" id="map" class="btn btn-xs green">
															Map
                                                        </button>

                        <p></p>
                        <button style="width:68px;" id="unmap" class="btn btn-xs red">
															Unmap
                                                        </button>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div class="checkbox pull-left">
                    <label >
                      <input type="checkbox" id="checknonledger">Set all accounts to NON-LEDGER upon save?</label>
                </div>
                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                <button id="update_selected_or" type="button" class="btn green  mr-auto">Update</button>
            </div>
        </div>

        <!--END mODAL CONTENT -->
    </div>
</div>
