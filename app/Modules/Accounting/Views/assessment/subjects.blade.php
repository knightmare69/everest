<?php $i=1;
    $status = array(
        0 => 'Regular Load'
       ,1 => 'Added Subject'
       ,2 => 'Changed Subject'
       ,3 => 'Dropped Subject' 
       ,4 => 'Withdrawn'
    );
?>
<table class="table table-bordered table-condensed table-striped table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Description</th>
            <th>Unit</th>
            <th>Section</th>
            <th>Schedule</th>
            <th>Teacher</th>
            <th>Status</th>
        </tr>        
    </thead>
    <tbody>
        @foreach($subj as $r)
        <tr>
            <td><?php echo $i; ++$i; ?></td>
            <td>{{$r->SubjectCode}}</td>
            <td>{{$r->SubjectTitle}}</td>
            <td>{{$r->CreditUnit}}</td>
            <td>{{$r->SectionName}}</td>
            <td>{{$r->Sched_1}}</td>
            <td> </td>
            <td>{{$status[$r->RegTagID]}}</td>
        </tr>
        @endforeach
    </tbody>
</table>