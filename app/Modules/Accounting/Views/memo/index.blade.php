<?php
    $service = new \App\Modules\Students\Services\StudentProfileServiceProvider;
    $at = getAYTerm();
    $term = DB::select("SELECT TOP 1 * FROM ES_AYTerm WHERE IsCurrentTerm=1 ORDER BY AcademicYear DESC")[0]->TermID;
	
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-graduation-cap"></i>
			<span class="caption-subject bold uppercase"> Student Debit/Credit Memo </span>
			<span class="caption-helper">Use this module to adjust student finance record...</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline  input-medium">
	           <input type="text" class="form-control input-circle filter-table input-sm " data-table="records-table" />
			</div>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="well well-sm">
            <div class="portlet-input input-inline input-medium">
                <select class="select2 form-control" name="term" id="term">
                    <option value="0" >- Select AY Term -</option>
                    @if(!empty($at))
                        @foreach($at as $r)
                        <option value="{{ encode($r->TermID) }}" <?php echo (($r->TermID==$term)?'selected':'');?>>{{ $r->AYTerm }}</option>
                        @endforeach
                    @endif
                </select>
			</div>
            <a href="{{url('accounting/memo/create')}}" class="btn btn-sm btn-primary " ><i class="fa fa-plus"></i> Create New </a>
            <button type="button" class="btn btn-sm btn-danger " data-menu="void"><i class="fa fa-ban"></i> Void </button>
            <button type="button" class="btn btn-sm btn-default " data-menu="refresh"><i class="fa fa-refresh"></i> Refresh </button>
        </div>
        <div class="" id="grdList">

            @include($views.'list',[ 'tm' => $term])
        </div>
	</div>
</div>
