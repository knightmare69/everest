<?php

    $j = 1;
    $x = 0;
    // $total = 0;
    $balance = 0;
    $page = isset($page) ? $page : 1;
    // if($tm == '0'){
    //     $fterm = 0;
    //     foreach( getAYTerm() as $r){
    //         if($fterm == '0'){
    //             $fterm = $r->TermID;
    //         }
    //
    //         if($r->Isactive == '1'){
    //             $tm = $r->TermID;
    //         }
    //     }
    //
    //
    // }

    // $data = App\Modules\Accounting\Models\DebitCreditMemo::selectRaw("*, fn_StudentName(IDNo) As FullName ")->where('TermID',$tm)->get();

?>
<div class="" style="padding-bottom: 5px;">
<div class="btn-group btn-group-circle">
    <?php
        $add = 0;
        $max =  $total / 50;
        // if( $total % 50 <= 4) $add = 1;
        $max = (floor($max) + 1);

        $url = url('enrollment/advising?v='. rand() .'&t='.encode($term));
    ?>
    <a class="btn btn-default" href="{{$url.'&v=1'}}">&LessLess; First</a>
    @for($i =1 ; $i<= $max; $i++ )
        <a class="btn btn-default <?= $page == $i ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a>
	@endfor
    <a class="btn btn-default" href="{{$url.'&v='.$max}}">Last &GreaterGreater;</a>
</div>
</div>

<table class="table table-striped table-bordered table-hover table_scroll" data-term="<?= $term ?>" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Ref#</th>
            <th>Date</th>
            <th>Explanation</th>
            <th>Posted</th>
            <th>Void</th>
            <th>Type</th>
            <th>ID #</th>
            <th>Student Name</th>
            <th>TransCode</th>
            <th>Trans #</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <?php
            $balance = number_format( getOutstandingBalance('1',$r->StudentNo) , 4 , ".", ",") ;
            $majorid = $r->MajorID == 0 ? $r->TrackID : $r->MajorID ;
        ?>
        <tr data-id="{{ ($r->RefNo) }}" data-idno="{{ encode($r->IDNo) }}" class="<?= $r->IsVoid == '1' ? 'warning':'' ?>"  >
            <td class="autofit">
                <input type="checkbox" class="chk-child" >
            </td>
            <td class="font-xs autofit bold">{{$j}}.</td>
            <td class="advised">
                <a href="javascript:void();" class="options" data-menu="edit">{{$r->RefNo}}</a>
            </td>
            <td>{{ $r->RefDate }}</td>
            <td class="">{{ $r->Explanation }} </td>
            <td>{{ $r->PostedDate }}</td>
            <td class="autofit"><?= $r->IsVoid == '1' ? '<i class="fa fa-check"></i>':'' ?></td>
            <td class="autofit">Student</td>
            <td class="autofit text-center ">{{ $r->IDNo }}</td>
            <td class="acct">{{$r->FullName}}</td>
            <td>{{$r->TransType}}</td>
            <td>{{$r->TransNo}}</td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>
<div class="">
    <div class="btn-group btn-group-circle">
        <a class="btn btn-default" href="{{$url.'&v=1'}}">&LessLess; First</a>
        @for($i =1 ; $i<= $max; $i++ )
            <a class="btn btn-default <?= $page == $i ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a>
    	@endfor
        <a class="btn btn-default" href="{{$url.'&v='.$max}}">Last &GreaterGreater;</a>
    </div>
</div>
