<style>
.profile{
    float: left;
    width: 150px;
    margin-right: 20px;
}
.content{
    overflow: hidden;
}
</style>
<div class="portlet light hidden-print" style="margin-bottom: 0px;" id="main_module">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-money"></i>
			<span class="caption-subject bold uppercase"> Debit/Credit Memo </span>
			<span class="caption-helper hidden-xs">Use this module to adjust student fee/s.</span>
        </div>

		<div class="actions  " style="padding-bottom: 6px !important;">
            <div class="portlet-input hide input-inline input-medium ">
                <div class="input-group  ">
	             <input type="text" class="form-control input-sm " id="orsearch" placeholder="Search O.R"  />
                 <span class="input-group-btn ">
			     <button class="btn btn-default btn-sm" style="height: 28px;" data-menu="orsearch" type="button">Go!</button>
			     </span>
               </div>
            </div>
            <a data-menu="new" class="btn btn-xs btn-primary hidden-print margin-bottom-5">Create New</a>
            <a data-menu="void" class="btn btn-xs btn-danger hidden-print margin-bottom-5">Void</a>
            <a data-menu="open" href="<?php echo url('accounting/memo');?>" class="btn btn-xs btn-info hidden-print margin-bottom-5">List</a>
		</div>
	</div>
	<div class="portlet-body">
        @include($views.'create.profile')
        <div class="row">
            <div class="col-sm-5">
                <div id="current_balance" style="margin-bottom: 5px;">
        		  @include($views.'create.assess')
                </div>            
            </div>
            <div class="col-sm-7">
                <div id="transaction_details" style="margin-bottom: 5px;">
        		  @include($views.'create.txn')
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <a data-menu="soa" class="btn btn-md green hidden-print margin-bottom-5">S.O.A</a>
                <a id="ledger" href="{{url('accounting/studentledger' )}}" class="btn btn-md blue hidden-print margin-bottom-5">Ledger</a>
            </div>
            <div class="col-xs-4">
                
            </div>
            <div class="col-xs-3 text-right invoice-block">
                <a class="btn btn-lg blue hidden-print margin-bottom-5" disabled="disabled" onclick="javascript:window.print();">Print <i class="fa fa-print"></i></a>
                <a class="btn btn-lg green hidden-print margin-bottom-5" data-menu="save">Save <i class="fa fa-check"></i></a>
            </div>
        </div>
	</div>
</div>
@include($views.'create.add')
