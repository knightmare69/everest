<?php
    $rows= 30;
    $i=0;
?>
<div class="table-scrollable" style="height: 250px; overflow-y: scroll; margin-bottom: 0px !important;" >
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="tbltxn" style="cursor: pointer; ">
    <thead>
        <tr>
            <th width="20" class="autofit">#</th>
            <th width="75" class="text-center">CODE</th>
            <th width="165" class="text-center">ACCOUNT NAME</th>
            <th width="80"  class="text-center">DR.CODE</th>
            <th width="75" class="text-center">DEBIT</th>
            <th width="75" class="text-center">CREDIT</th>
            <th width="200" class="text-center">REMARKS</th>
        </tr>
    </thead>
    <tbody class="">
        @for($i=1;$i<=30; $i++)
            <tr data-id="" data-ref="" data-trn="" >
                <td width="20" class="bold seq font-xs">{{$i}}.</td>
                <td width="75" class="bold code font-xs"></td>
                <td width="165" class="bold name font-xs"></td>
                <td width="80" class="bold dr font-xs"></td>
                <td width="75" class="bold debit font-xs text-right"><input type="text" value="" class="input-no-border text-right numberonly " /> </td>
                <td width="75" class="bold credit font-xs text-right"><input type="text" value="" class="input-no-border text-right numberonly" /></td>
                <td width="200" class="bold rmks font-xs text-right"></td>
            </tr>
        @endfor
    </tbody>
</table>
</div>
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="tbltxn_footer" style="cursor: pointer;">
    <tfoot class="">
        <tr>
            <td width="332" class="bold font-xs">
				<a href="javascript:void(0);" class="btn btn-xs btn-success btn-add"><i class="fa fa-plus"></i> Add</a>
				<a href="javascript:void(0);" class="btn btn-xs btn-danger btn-remove"><i class="fa fa-times"></i> Remove</a>
				<span class="pull-right">Total :</span>
			</td>
            <td width="73" ></td>
            <td width="73"></td>
            <td width="200"></td>
        </tr>
    </tfoot>    
</table>
