 <?php // error_print($event); 
 
$accounts = \App\Modules\Accounting\Models\ChartOfAccounts::orderBy('AcctName','ASC')->get();
                                
 ?>
 <div id="modal_account" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select an account</h4>
			</div>
			<div class="modal-body">			                        			
 <form class="form-horizontal" id="add_form" name="add_form" action="?" >
    <div class="form-body">
        <div class="form-group hidden">
    		<label class="col-md-3 control-label">Fee</label>
    		<div class="col-md-6">
    			<select id="acct_txn" name="acct_txn" class="form-control" data-live-search="true">
                    <option value="-1"> - Select Transaction - </option>
                    <option value="1">Enrollment</option>
                    <!-- <option value="2">Add/Drop/Change</option> -->
                    <option value="15" selected="">Other Assessment</option>
                </select>
    		</div>
    	</div>
        
        <div class="form-group">
    		<label class="col-md-3 control-label">Fee</label>
    		<div class="col-md-6">
    			<select id="acct" name="acct" class="form-control  bs-select " data-live-search="true">
                    <option value="-1"> - Select Account - </option>
                    @if(!empty($accounts))
                        @foreach($accounts AS $c)
                            <?php if($c->Inactive!=1){ ?>
							<option value="{{ $c->AcctID }}" data-code="{{$c->AcctCode}}" data-name="{{$c->AcctName}}" data-amt="{{$c->DefaultAmount}}" > {{ $c->AcctName.' ('.$c->AcctCode.')'}} </option>
							<?php } ?>
                        @endforeach
                    @endif
                </select>
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Debit</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter Amount" name="acct_dbt" id="acct_dbt" maxlength="9" class="form-control text-right numberonly" value="0.00" >
    		</div>
    	</div>
        <div class="form-group">
    		<label class="col-md-3 control-label">Credit</label>
    		<div class="col-md-6">
    			<input type="text" placeholder="Enter Amount" name="acct_cdt" id="acct_cdt" maxlength="9" class="form-control text-right numberonly" value="0.00" >
    		</div>
    	</div>

        <div class="form-group">
    		<label class="col-md-3 control-label">Remarks</label>
    		<div class="col-md-6">
    			<textarea class="form-control except" style="resize: none !important;" id="acct_desc" name="acct_desc"></textarea>
    		</div>
    	</div>
    </div>
</form>

	<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary" data-menu="add-row-proceed">Add Row</button>
                   <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" >Cancel</button>
                   
				</div>
			</div>
		</div>
	</div>
</div>