@foreach($data as $d)
<option value="{{encode($d->RegID)}}" 
    data-remarks="{{$d->Remarks}}" 
    data-term="{{$d->Term}}" 
    data-tid="{{encode($d->TermID)}}"
    data-date="{{$d->RegDate}}" 
    data-feeid="{{$d->ScheduleFeeID}}"
    data-validation="{{$d->ValidationDate}}"
    data-assess="{{$d->AssessedFees}}" 
    data-balance="{{$d->TotalBalance}}"
    >{{$d->RegID}}</option>
@endforeach