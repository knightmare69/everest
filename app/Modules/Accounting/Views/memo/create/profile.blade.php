<div class="row">
    <div class="col-sm-8">
        <div class="profile">
            <img id="stud_photo" style="border: 1px solid;" class="entryphoto pull-left" width="150" height="180" src="<?php echo url('assets/system/media/images/no-image.png').'?'.rand(0,9999);?>"/>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        <div class="input-group-btn">
            				<button id="ptype" type="button" data-id="1" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="bold">Student</span> <i class="fa fa-angle-down"></i></button>
            				<ul class="dropdown-menu">
            					<li> <a href="javascript:void();" class="idtype" data-type="1"> Students </a></li>
                                <li> <a href="javascript:void();" class="idtype" data-type="2"> Employee </a></li>
                                <li> <a href="javascript:void();" class="idtype" data-type="3"> Scho. Provider </a></li>
                                <li> <a href="javascript:void();" class="idtype" data-type="4" > Applicant </a> </li>
            					<li> <a href="javascript:void();" class="idtype" data-type="5"> Other Payer </a> </li>
            					<li class="divider"></li><li><a href="javascript:void();">Cancel </a></li>
            				</ul>
            			</div>
                        <input type="text" id="txtname" name="txtname" class="form-control bold font-blue-madison" value="" placeholder="Search by IDNo, Name" />
                        <span class="input-group-btn">
            			 <button class="btn blue" type="button" data-menu="search">Search</button>
            			</span>    
                    </div>
                    <div class="help-block">
                        <strong class="font-blue-sharp font-xs">ID #:</strong> <strong id="tidno" class="font-blue-sharp font-xs">ID Number</strong> |
                        <strong class="font-blue-sharp font-xs">Level :</strong> <strong id="tyr" class="font-blue-sharp font-xs">Year Level</strong> |
                        <strong class="font-blue-sharp font-xs">Department :</strong> <strong id="tdept" class="font-blue-sharp font-xs">&nbsp;</strong> |
                        <strong class="font-blue-sharp font-xs"> Nationality : </strong> <strong id="tnat" class="font-blue-sharp font-xs">&nbsp;</strong>
            		</div>
                </div>
            </div>
            <hr style="margin-top: 5px;margin-bottom: 5px;">
            <div class="row">
                <div class="col-sm-5">
                    <div class="row static-info">
                        <div class="col-md-12 name"><strong class="font-purple-plum">Assessment / Billing for Adjustment</strong></div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name bold text-right"> Transaction : </div>
                        <div class="col-md-7 value">
                        	<select id="txn" name="txn" class="form-control input-sm ">
                                <option value="1">Enrollment</option>
                                <option value="2">Add/Drop/Change</option>
                                <option value="15">Other Assessment</option>
                            </select>
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name text-right bold"> Reference # : </div>
                        <div class="col-md-7 value form-inline">
                        	<select id="refs" name="refs" class="form-control input-sm  input-xsmall " style="padding-left: 5px; padding-right: 5px;">
                                <option>- select -</option>
                            </select>
                            <button type="button" class="btn btn-default btn-xs " data-menu="ref-refresh"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="row static-info">
                        <div class="col-md-12">
                            <div class="well well-sm well-light" style="margin-bottom: 0px;">
                                <strong class="font-purple-plum">Transaction Details</strong>
                                <table class="">
                                    <tr><td class="font-xs bold font-blue-madison">A.Y. Term : </td><td class="term bold font-xs"> </td></tr>
                                    <tr><td class="font-xs bold font-blue-madison">Reg. Date :</td><td class="date bold font-xs"></td></tr>
                                    <tr><td class="font-xs bold font-blue-madison">Validation Date :</td><td class="validation bold font-xs"></td></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 util-btn-margin-bottom-5">
    <div class="well well-light well-sm " style="margin-bottom: 1px;">
    	<div class="well-title">
    		<div class="caption">
    			<i class="fa fa-memo font-green-sharp"></i>
    			<span class="caption-subject font-green-sharp bold ">MEMO Reference</span>
    			<span class="caption-helper"></span>
    		</div>
    		<div class="actions">
    		
    		</div>
    	</div>
    	<div style="border-top: 1px solid silver;" >
    	  <form id="frm_reference"  class="form-horizontal">
            <div class="form-body" style="padding-top: 5px;">
    				<div class="form-group" style="margin-bottom: 5px !important;">
    					<label class="col-md-4 control-label bold font-xs">Reference No</label>
    					<div class="col-md-8">
    					   <label class="form-control text-danger input-sm font-xs bold" data-value="0" id="dmcm_no">[Auto Number]</label>             
    					</div>
    				</div>
    				<div class="form-group" style="margin-bottom: 5px !important;">
    					<label class="col-md-4 control-label bold font-xs">Ref Date</label>
    					<div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <input value="<?= systemDate('m/d/Y'); ?>" type="text" id="txdate" class="form-control input-sm font-xs date-picker bold" placeholder="Enter Date">
                                </div>
                                <div class="col-md-6">                                                                
                                    <input type="text" class="form-control timepicker input-sm font-xs timepicker-default" value="" id="txtime"  placeholder="Date/Time" />
                                </div>
                            </div>                                             						
    					</div>
    				</div>
    				<div class="form-group" style="margin-bottom: 5px !important;">
    					<label class="col-md-4 control-label bold font-xs">Explanation</label>
    					<div class="col-md-8">
    					<textarea class="form-control input-sm font-xs bold" id="explanation" style="resize: none !important; " rows="3" ></textarea>
    					</div>
    				</div>    				
    			</div>                
          </form>
    	</div>
    </div>                    
  </div>
</div>