      <?php
$i=0;
$data = isset($data) ? $data : array();
$total = array(
    'assess' => 0
   ,'aid' => 0
   ,'ext' => 0
   ,'dsc' => 0
   ,'net' => 0
   ,'pay' => 0
   ,'bal' => 0
);
?>
<div class="table-scrollable" style="height: 250px; overflow-y: scroll; margin-bottom: 0px !important;" >
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="tblcurrent" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit">#</th>
            <th class="text-center">Code</th>
            <th class="text-center">Account Name</th>
            <th class="text-center">Balance</th>
            <th class="text-center">ASSESSED</th>
        </tr>
    </thead>
    <tbody>
    @if(!empty($data))
    
<?php
    foreach($data as $r) {
  $netass  = $r->{'Assess Fee'} - ($r->Discount + $r->FinancialAidExternal);
  $balance = $netass - $r->ActualPayment - $r->CreditMemo ;
    $total['assess'] += $r->{'Assess Fee'};
    $total['aid'] += $r->Discount;
    $total['ext'] += $r->FinancialAidExternal;
    $total['dsc'] += $r->PaymentDiscount;
    $total['net'] += $netass;
    $total['bal'] += $balance;
    $total['pay'] += $r->ActualPayment;
  $i++;
  ?>

  <tr data-ref="<?= $r->EntryID ?>"
      data-id="<?= $r->AccountID ?>" 
      data-code="<?=$r->AcctCode?>"
      data-name="<?=$r->AcctName?>"
      data-bal="<?=$balance?>"
      data-trn="<?= $r->ReferenceNo.';'.$r->EntryID.';1' ?>" 
      data-memoref="<?=$r->DMCMRefNo?>"
      data-ext="<?=$r->FinancialAidExternal?>">
           <td class="font-xs autofit" ><?=$i?>.</td>
		   <td class="" ><?=$r->AcctCode?></td>
           <td class="" ><?=$r->AcctName?></td>
           <td class="text-right bold danger " data-balance="<?=$balance?>"><?=number_format($balance,2,'.',',')?></td>
           <td class="text-right bold"  data-netassess="<?=$netass?>"><?=number_format($netass,2,'.',',')?></td>
	</tr>
    <?php } ?>
    @else
    <tr>
        <td colspan="5"> No Assessment at this moment... </td>
    </tr>
    @endif
    </tbody>
</table>
</div>
<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="tbltxn_footer" style="cursor: pointer;">
    <tfoot class="">
        <tr>
            <td width="332" class="bold font-xs text-right">Total : </td>
            <td width="75" class="current_total bold " ><?= number_format($total['bal'],2) ?></td>            
            <td width="200" class="text-right">
               <a href="javascript:void(0)" title="Click here to Credit all balances"> Credit All Balance </a> 
            </td>
        </tr>
    </tfoot>
</table>
