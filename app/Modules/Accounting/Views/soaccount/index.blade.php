<div class="col-sm-12">
<div class="portlet blue box dvchild">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i> Student Enrollment Info
		</div>
		<div class="tools">
		  <button class="btn btn-sm btn-info btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
		  <button class="btn btn-sm btn-warning btn-assess hidden"><i class="fa fa-print"></i> Assessment</button>
		  <button class="btn btn-sm btn-success btn-soa hidden"><i class="fa fa-print"></i> S.O.A</button>
		</div>
	</div>
	<div class="portlet-body" id="list">
		<div class="row" style="margin-bottom:1px;">
		 <div class="col-sm-12">
			<?php
			  $isupdated = isFamilyUpdated();
			  if($isupdated==0){
				echo '<div class="alert alert-danger"><i class="fa fa-warning"></i> You need to update your family information in order to proceed. Kindly click <a href="'.url('guardian').'">My Profile</a>.</div>';
			  }
			?>
		 </div>
		 <div class="col-sm-12">
		     <div class="form-group">
				<label>Student Name:<b class="studno"></b></label>
				<select class="form-control" id="studname" name="studname">
					<option value="-1" selected disabled> - Select one - </option>
					<?php
					  $stdcnt = 0;
					  $select = Request::get('studno');
					  $exec   = DB::select("SELECT * FROM ES_Students WHERE StatusID<7 AND FamilyID='".getFamilyID()."' AND StudentNo IN (SELECT StudentNo FROM ES_Registrations WHERE TermID='".getActiveTerm()."') ORDER BY StudentNo DESC");
					  if($exec && count($exec)>0){
					    foreach($exec as $c){
						  echo '<option value="'.$c->StudentNo.'" '.(($select && $c->StudentNo==$select)?'selected':'').'>'.$c->Fullname.'</option>';
						  $stdcnt++;
						}
					  }
					?>
				</select>
			 </div>
		 </div>
		 <div class="col-sm-3 col-md-2 col-md-offset-3">
			  <label class="">Student No:&nbsp;&nbsp;<b class="xstdno"></b></label>
			  <label class="hidden">RegID:&nbsp;&nbsp;<b class="regid"></b></label>
		 </div>
		 <div class="col-sm-3 col-md-2">
		 	  <label>Level:&nbsp;&nbsp;<b class="yrlvl"></b></label>
		 </div>
		 <div class="col-sm-3 col-md-2">
		 	  <label>Status:&nbsp;&nbsp;<b class="sstats"></b></label>
		 </div>
		 <div class="col-sm-3 col-md-3">
		 	  <label>Academic Year:&nbsp;&nbsp;<b class="term" data-id="0"></b></label>
		 </div>
		</div>
	</div>
</div>
</div>

<?php
$aclass  = (($epayment && $epayment!='' && ($stdcnt>1))?'hidden':'');
$bclass  = (($epayment && $epayment!='' && ($stdcnt>1))?'':'hidden');
$pclass  = (($epayment && $epayment!='')?'':'hidden');
$pclass  = (($stdcnt>1)?$pclass:' hidden');
?>
<div class="col-sm-12 dvncart <?php echo $aclass;?>">
	<div class="row">
	<div class="col-sm-6">
	<div class="portlet yellow box dvassess">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-table"></i> Assessment
			</div>
			<div class="tools">
			</div>
		</div>
		<div class="portlet-body" id="list">
			<div class="row" style="margin-bottom:1px;">
			 <div class="col-sm-12">
			   <div class="table-responsive tbl-assess"></div>
			 </div>
			</div>
		</div>
	</div>
	</div>
	<div class="col-sm-6">
	<div class="portlet red box dvduedate">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-calendar"></i> Payment Due Dates
			</div>
			<div class="tools">
			</div>
		</div>
		<div class="portlet-body" id="list">
			<div class="row" style="margin-bottom:1px;">
			 <div class="col-sm-12">
			   <div class="table-responsive tbl-duedate"></div>
			 </div>
			</div>
		</div>
	</div>
	</div>
	</div>
</div>


<div class="col-sm-12 col-md-5 dvwcart <?php echo $bclass;?>">
<div class="portlet red box dvduedate">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-calendar"></i> Payment Due Dates
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body" id="list">
		<div class="row" style="margin-bottom:1px;">
		 <div class="col-sm-12">
		   <div class="table-responsive tbl-duedate"></div>
		 </div>
		</div>
	</div>
</div>

<div class="portlet yellow box dvassess">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-table"></i> Assessment
		</div>
		<div class="tools">
		</div>
	</div>
	<div class="portlet-body" id="list">
		<div class="row" style="margin-bottom:1px;">
		 <div class="col-sm-12">
		   <div class="table-responsive tbl-assess"></div>
		 </div>
		</div>
	</div>
</div>
</div>


<div class="col-sm-12 col-md-7 dvpayment">
<div class="portlet purple box dvcart <?php echo $pclass;?>">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-money"></i> Payment Summary
		</div>
		<div class="tools">
		  <button class="btn btn-sm btn-danger btnepayment"><i class="fa fa-money"></i> Make Payment</button>
		</div>
	</div>
	<div class="portlet-body" id="list">
		<table class="table table-bordered table-condense" id="epayment">
			<thead>
				<th></th>
				<th>Student(s)</th>
				<th>Amount</th>
			</thead>
			<tbody>
				<?php echo (($epayment && $epayment!='')?$epayment:'');?>
			</tbody>
		</table>
	</div>
</div>
</div>
