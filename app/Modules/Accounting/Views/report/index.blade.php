<div class="row">
    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i>Report Settings
                </div>
            </div>
            <div class="portlet-body form form_wrapper">
                <form class="horizontal-form" id="report-form" action="#" method="POST" onsubmit="return false;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">SELECT CAMPUS</label>
                                    <select class="select2 form-control" name="campus" id="campus">
                                        <option value="" disabled selected> - SELECT ONE - </option>
										<?php 
										  $i = 0;
										  foreach($campus as $c){
											  echo '<option value="'.$c->CampusID.'" '.(($i==0)?'selected':'').'>'.$c->ShortName.'</option>';
											  $i++;
										  }
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">ACADEMIC YEAR</label>
                                    <select class="select2 form-control" name="ayterm" id="ayterm">
                                        <option value="" disabled selected> - SELECT ONE - </option>
										<?php 
										  foreach($ayterm as $a){
											  echo '<option value="'.$a->TermID.'" '.(($a->Active_OnlineEnrolment==1)?'selected':'').'>'.$a->AcademicYear.' '.$a->SchoolTerm.'</option>';
										  }
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
								    <hr/>
                                    <label class="control-label">Report Title</label>
                                    <select class="select2 form-control" name="report" id="report">
                                        <option value="" disabled selected> - SELECT ONE - </option>
										<?php 
										  foreach($reports as $r=>$v){
											  echo '<option value="'.$r.'" >'.$v['name'].'</option>';
										  }
										?>
                                    </select>
									<hr/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Program</label>
                                    <select class="select2 form-control" name="program" id="program">
                                        <option value="" selected>All</option>
										<?php 
										  foreach($programs as $p){
											  echo '<option value="'.$p->ProgID.'">'.$p->ProgName.'</option>';
										  }
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Yearlevel</label>
                                    <select class="select2 form-control" name="yrlvl" id="yrlvl">
                                        <option value="" selected>All</option>
										<?php 
										  foreach($yrlvl as $y){
											  echo '<option value="'.$y->YLID_OldValue.'" data-prog="'.$y->ProgID.'">'.$y->YearLevelName.'</option>';
										  }
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">DATE AS OF...</label>
                                    <input type="text" class="form-control datepicker" name="asof" id="asof" data-dateFormat="mm/dd/yy" value="<?php echo date('m/d/Y');?>"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
								    <hr/>
                                    <label class="control-label">Include...</label>
									<div class="checkbox-list">
									  <label class="checkbox-inline">
									     <input type="checkbox" class="form-control" name="backaccount" id="backaccount"/><i></i>
									     Back Account
									  </label>
									  <label class="checkbox-inline">
									     <input type="checkbox" class="form-control" name="baonly" id="baonly"/><i></i>
									     Only
									  </label>
									  <label class="checkbox">
									     <input type="checkbox" class="form-control" name="zerobal" id="zerobal"/><i></i>
									     Zero Balance Or Fully Paid
									  </label>
									  <label class="checkbox">
									     <input type="checkbox" class="form-control" name="notvalid" id="notvalid"/><i></i>
									     Not Yet Validated
									  </label>
                                </div>
                            </div>
                        </div>
					</div>
					<div class="form-actions right">
						<button class="btn pull-left default hidden" style="margin-left: 10px;" id="btnsetup" type="button"><i class="fa fa-cog"></i> Setup </button>
						<div class="btn-group">
						    <button class="btn green dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-download"></i> Export <i class="fa fa-angle-down"></i></button>
						    <ul class="dropdown-menu">
							<li><a class="pexport" data-typeid="27" href="javascript:void(0);">Whole Report</a></li>
							<li><a class="pexport" data-typeid="28" href="javascript:void(0);">Data Only</a></li>
							</ul>
						</div>
						<button class="btn blue btn_action btn_save" type="button" id="print-report"><i class="fa fa-print"></i> Print Report</button>
					</div>
                </form>
            </div>
        </div>
    </div>							
</div>