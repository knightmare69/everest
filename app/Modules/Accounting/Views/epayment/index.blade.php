<?php 
$_mid       = "000000180620562FEDE7";//"00000009052092F0B682";//"000000010620519C3D0B";//
$cert       = "C4CDED743CD8E94890B673866883F61B";//"F22D593D5402073A22CE0AB361D5AA3A";//"88CF75D44333E07B371CA6EACDECF5A0";//<-- your merchant key
$purl       = "https://ptiapps.paynamics.net/webpayment/Default.aspx";//"https://testpti.payserv.net/webpayment/Default.aspx"//;
$_ipaddress = "122.2.22.163";

function get_details($regid){   
	$qry    = "SELECT g.GroupID
					,g.GroupName
					,SUM(j.Debit) as Debit
					,SUM(j.[1st Payment]) as First
					,(SELECT TOP 1 FirstPaymentDueDate FROM ES_Registrations) as FirstDueDate
					,SUM(j.[2nd Payment]) as Second
					,(SELECT TOP 1 SecondPaymentDueDate FROM ES_Registrations) as SecondDueDate
					,SUM(j.[3rd Payment]) as Third
					,(SELECT TOP 1 ThirdPaymentDueDate FROM ES_Registrations) as ThirdDueDate
					,SUM(j.[4th Payment]) as Fourth
					,(SELECT TOP 1 FourthPaymentDueDate FROM ES_Registrations) as FourthDueDate
					,SUM(j.Debit - (j.ActualPayment + CreditMemo)) as Balance
					,SUM(j.ActualPayment) as Payment
					,SUM(j.CreditMemo) as CreditMemo
					,fe.PaymentScheme
				FROM ES_Journals as j
		INNER JOIN ES_Registrations as r ON j.IDNo=r.StudentNo AND j.TermID=r.TermID AND j.TransID=1
		INNER JOIN ES_TableOfFees as fe ON r.TableofFeeID=fe.TemplateID
		INNER JOIN ES_Accounts as a ON j.AccountID=a.AcctID
		INNER JOIN ES_AccountGroups as g ON a.GroupID=g.GroupID  
				WHERE r.RegID='".$regid."' 
			GROUP BY g.GroupID,g.GroupSort,g.GroupName,fe.PaymentScheme";
	$exec     = DB::select($qry);
	if($exec && count($exec)>0){
	   return $exec; 
	}else{
	   return false;
	}
}

function get_edetails($payid){   
	$qry    = " SELECT  pd.StudentNo as GroupID
					,s.Fullname as GroupName
					,pd.TotalAmount as Debit
					,pd.TotalAmount as First
					,GETDATE() as FirstDueDate
					,pd.TotalAmount as Second
					,GETDATE() as SecondDueDate
					,pd.TotalAmount as Third
					,GETDATE() as ThirdDueDate
					,pd.TotalAmount as Fourth
					,GETDATE() as FourthDueDate
					,pd.TotalAmount as Balance
					,0 as Payment
					,0 as CreditMemo
					,0 as PaymentScheme
				FROM ESv2_EPayment_Details as pd
			   INNER JOIN ES_Students as s ON pd.StudentNo=s.StudentNo       
			   WHERE pd.PaymentID=".$payid;
	$exec     = DB::select($qry);
	if($exec && count($exec)>0){
	   return $exec; 
	}else{
	   return false;
	}
}

function get_channel_amt($total=0,$pchannel=''){
    $tmpamt = 28;
	if($total>0){
	  switch($pchannel){
	    case 'cc':
		  return (($total*0.04256)+22.40);	
		break;
	    case '7connect':
	    case '7eleven':
		  return ($total*0.0392);	
		break;
	    case 'bdootc':
	    case 'bdoobp':
		  return 39;	
		break;
	    case 'bpionline':
		  $tmpval = ($total*0.0224);
		  return (($tmpval>28)?$tmpval:28);	
		break;
	    default:
		  return 28;
		break;
	  }
	}else{
	  return $tmpamt;
	}
}

function generate_fees($details,$payoption,$_ptype){
    $schedule  = array(1=>'First',2=>'Second',3=>'Third',4=>'Fourth');
    $tmpamt    = 0;
    $total     = 0;
	$tblfee    = '';
	$strxml    = '';
	$i         = 1;
	foreach($details as $rs){
		if($payoption==0){
		   $tmpamt = $rs->Balance;
		}else{
		   $tmpbal = 0; 
		   foreach($schedule as $k=>$v){
			if($k<=$payoption){
			  $tmpbal += $rs->{$v};
			}
		   }
		   $tmpamt = $tmpbal - ($rs->Payment + $rs->CreditMemo);
		}
		 
		if($tmpamt>0){
		  $tmplabl = (($rs->GroupID=='9999')?'Convenience Fee':($rs->GroupName));
		  $strxml .="<Items><itemname>".$tmplabl."</itemname><quantity>1</quantity><amount>".$tmpamt."</amount></Items>"; // pls change this value to the preferred item to be seen by customer. (eg. Room Detail (itemname - Beach Villa, 1 Room, 2 Adults       quantity - 0       amount - 10)) NOTE : total amount of item/s should be equal to the amount passed in amount xml node below.   
		  $tblfee .='<tr><td width="10px;">'.$i.'</td>
		                <td>'.$tmplabl.'</td>
		                <td class="text-right">'.number_format($tmpamt,2).'</td></tr>'; 
		  $i++;	
          $total += $tmpamt;		  
		} 
	}
	/*	
	if(isset($_ptype) && $_ptype!='' && $total>0){
	  $tmplabl= 'Convenience Fee';
	  $tmpamt = get_channel_amt($total,$_ptype); ;
	  $strxml.="<Items><itemname>".$tmplabl."</itemname><quantity>1</quantity><amount>".$tmpamt."</amount></Items>"; // pls change this value to the preferred item to be seen by customer. (eg. Room Detail (itemname - Beach Villa, 1 Room, 2 Adults       quantity - 0       amount - 10)) NOTE : total amount of item/s should be equal to the amount passed in amount xml node below. 
	  $tblfee.='<tr><td width="10px;">'.$i.'</td>
			        <td>'.$tmplabl.'</td>
			        <td class="text-right">'.number_format($tmpamt,2).'</td></tr>'; 
	  $i++;	
	  $total += $tmpamt;		  
	}
    */
    return array($total,$tblfee,$strxml); 
}

$studentinfo = ((isset($studentinfo))?$studentinfo:array()); 
$regid       = ((@isset($regid) && $regid!='')?$regid:0);
$payoption   = ((@isset($schedid) && $schedid!='')?$schedid:0);
$_ptype      = ((@isset($gateid) && $gateid!='')?$gateid:'sale');
$arr_method  = array('cc'        =>array('label'=>'Visa/Master Card','fee'=>'(t*0.038)+20','vat'=>'(t*0.04256)+22.40')
				  //,'gc'        =>array('label'=>'GCash','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'7eleven'   =>array('label'=>'7-Eleven/7Connect','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'ecpay'     =>array('label'=>'ECPAY','fee'=>'25','vat'=>'f+(f*0.12)')
				    ,'bn'        =>array('label'=>'Bancnet IPG','fee'=>'25','vat'=>'f+(f*0.12)')
					,'bdootc'    =>array('label'=>'BDO Over the Counter','fee'=>'25','vat'=>'f+(f*0.12)')
					,'bdoobp'    =>array('label'=>'BDO Online Bills Payment','fee'=>'25','vat'=>'f+(f*0.12)')
				    ,'bpionline' =>array('label'=>'BPI Direct Debit','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'BDOONLINE' =>array('label'=>'BDO Online Direct Payment','fee'=>'25','vat'=>'f+(f*0.12)')
					,'hsbinstall'=>array('label'=>'HSBC INSTALLMENT','fee'=>'25','vat'=>'f+(f*0.12)')
					,'pnbotc'    =>array('label'=>'PNB Over the Counter','fee'=>'25','vat'=>'f+(f*0.12)')
					,'pnbobp'    =>array('label'=>'PNB Online Bills Payment','fee'=>'25','vat'=>'f+(f*0.12)')
				    ,'sbcobp'    =>array('label'=>'Security Bank Online Bills Payment','fee'=>'25','vat'=>'f+(f*0.12)')
					,'ucpbotc'   =>array('label'=>'UCPB Over the Counter','fee'=>'25','vat'=>'f+(f*0.12)')
					,'ucpbobp'   =>array('label'=>'UCPB Online Bills Payment','fee'=>'25','vat'=>'f+(f*0.12)')
				    ,'ubponline' =>array('label'=>'Unionbank Online','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'ubpobp'    =>array('label'=>'Unionbank Online Bills Payment','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'ML'        =>array('label'=>'MLhuillier','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'ceb'       =>array('label'=>'Cebuana','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'sm'        =>array('label'=>'SM Payment','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'truemoney' =>array('label'=>'True Money','fee'=>'25','vat'=>'f+(f*0.12)')
				  //,'DA5'       =>array('label'=>'Direct Agent 5','fee'=>'25','vat'=>'f+(f*0.12)')
				);
$studno = '';				
?>
<div id="content">
 <div class="row">
 <div class="col-sm-12 col-md-12">
 <!-- <link rel="stylesheet" href="<?php echo url('assets/resources/style.css');?>">
      <link rel="stylesheet" href="<?php echo url('assets/resources/css?family=Montserrat');?>"> -->
	
	<div class="">
		<?php
			$_requestid = substr(uniqid(), 0, 13);
		    if(isset($payid) && $payid!=0){
		      $fdetails   = get_edetails($payid);
			  $update_pay = DB::table('ESv2_EPayment')->where(array('EntryID'=>$payid))->update(array('RequestID'=>$_requestid));
			}else{ 
		      $fdetails = get_details($regid);
			  $studno   = ('?studno='.$studentinfo->StudentNo);
			}
			
			list($xtotal,$tblfee,$xmlfee) = generate_fees($fdetails,$payoption,$_ptype);
			if(!isset($payid) || $payid<=0){
			    $reginfo  = DB::select("SELECT * FROM ES_Registrations WHERE RegID=".$regid);
			    $termid   = $reginfo[0]->TermID;
			    $pcheck   = DB::select("SELECT * FROM ESv2_EPayment WHERE UserID='".getUserID()."' AND StatusID=0");
				if($pcheck && count($pcheck)>0){
			    $update_pay = DB::table('ESv2_EPayment')->where(array('EntryID'=>$pcheck[0]->EntryID))->update(array('RequestID'=>$_requestid,'TotalAmount'=>$xtotal)); 
				}else{
				$pdata    = array('UserID'     => getUserID(),
								 'TermID'     => $termid,
								 'RequestID'  => $_requestid,
								 'StatusID'   => '0',
								 'TotalAmount'=> $xtotal,
							     'DateCreated'=>date('Y-m-d H:i:s'),
						   );
			    $pentryid = DB::table('ESv2_EPayment')->insertGetId($pdata);
				$ddta = array('PaymentID'  =>$pentryid,
							  'StudentNo'  =>$reginfo[0]->StudentNo,
							  'RegID'      =>$regid,
							  'ScheduleID' =>$payoption,
                              'TotalAmount'=>$xtotal,);
				$detailid = DB::table('ESv2_EPayment_Details')->insert($ddta);
				}
			}else{
			    $pcheck   = DB::select("SELECT * FROM ESv2_EPayment WHERE UserID='".getUserID()."' AND StatusID=0");
				if($pcheck && count($pcheck)>0){
			    $update_pay = DB::table('ESv2_EPayment')->where(array('EntryID'=>$pcheck[0]->EntryID))->update(array('RequestID'=>$_requestid,'TotalAmount'=>$xtotal)); 
				}
			}
			
			$_noturl    = url('epayment/notify/'.$regid); // url where response is posted
			$_resurl    = url(); //url of merchant landing page
			if($payoption==0){
			 $_resurl    = url('epayment/process/'.$regid); //url of merchant landing page
			}else{
			 $_resurl    = url('epayment/process/'.$regid); //url of merchant landing page
			}
			
			$_cancelurl = url('epayment/cancel');
	        $_ptype     = ((@isset($_ptype))?$_ptype:'sale');
			$_fname     = $studentinfo->FirstName; // kindly set this to first name of the cutomer
			$_fname     = (($_fname=='')?'Not Applicable':$_fname);
			$_mname     = $studentinfo->MiddleName; // kindly set this to middle name of the cutomer
			$_mname     = ((trim($_mname)=='')?'.':$_mname);
			$_lname     = $studentinfo->LastName; // kindly set this to last name of the cutomer
			$_lname     = (($_lname=='')?'Not Specified':$_lname);
			
			if(isset($payid) && $payid!=0){
			 $_lname     = $studentinfo->LastName;
			 $_lname     = (($_lname=='')?'Not Specified':$_lname);
			 $_fname     = $studentinfo->Fullname;
			 $_fname     = str_replace($_lname.',','',$_fname);
			 $_fname     = str_replace($_lname,'',$_fname); 
			 $_mname     = '.';
			 $studentinfo->Fullname = $_lname.', '.$_fname;
			}else{
			
			}
			
			$_addr1     = $studentinfo->Res_Address; // kindly set this to address1 of the cutomer
			$_addr1     = ((trim($_addr1)=='')?'.':$_addr1);
			$_addr2     = trim($studentinfo->Res_Address2);// kindly set this to address2 of the cutomer
			$_addr2     = ((trim($_addr2)=='')?'.':$_addr2);
			$_city      = $studentinfo->Res_TownCity; // kindly set this to city of the cutomer
			$_city      = ((trim($_city)=='')?'.':$_city);
			$_state     = $studentinfo->Res_Province; // kindly set this to state of the cutomer
			$_state     = ((trim($_state)=='' || $_state=='NA')?'.':$_state);
			$_country   = getObjectValue($studentinfo,'Res_Country');
			$_country   = (($_country=='')?"PH":$_country); // kindly set this to country of the cutomer ex. PH
			$_zip       = $studentinfo->Res_ZipCode; // kindly set this to zip/postal of the cutomer
			$_zip       = (($_zip=='' || $_zip=='NA')?'1005':$_zip);
			$_sec3d     = "try3d"; // 
		  //$_email     = 'jhe69samson@gmail.com';
		    $_email     = $studentinfo->StudentEmail; // kindly set this to email of the cutomer
		  //$_email     = ((trim($_email)=='')?'jhe69samson@gmail.com':$_email);
			$_phone     = $studentinfo->StudentContact; // set to this ID number
			$_mobile    = $studentinfo->StudentContact; // kindly set this to mobile number of the cutomer
			if(($_phone=='' || $_phone=='-') && ($_mobile!='' && $_mobile!='-')){
				$_phone = $_mobile;
			}
			
			if($_phone=='' || $_phone=='-'){
				$_phone = '027356011';
			}
			
			$_clientip  = $_SERVER['REMOTE_ADDR'];
			$t_amount   = $xtotal; // value of payment
			$_amount    = number_format($t_amount,2, '.', ''); 
			$_currency  = "PHP"; //PHP or USD
			$forSign    = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;
			
			$_sign      = hash("sha512", $forSign.$cert);
			$xmlstr     = "";
			$strxml     = "";
		?>
		<div class="col-sm-12">
			<div class="info-details">
				<div class="row">
				  <div class="form-group col-sm-12">
					<label for="name-holder">Name</label>
					<input id="fname" name="txtfname" type="text" class="form-control" placeholder="First Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo $studentinfo->Fullname;?>" disabled>
				  </div>
				  
				  <div class="form-group col-sm-12">
					<label for="name-holder">Address</label>
					<input id="address1" name="txtaddress1" type="text" class="form-control" placeholder="Address 1" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_addr1.' '.$_addr2.' '.$_city.' '.$_state.' '.$_country.' '.$_zip;?>" disabled>
				  </div>
				  
				  <div class="form-group col-sm-6">
					<label for="name-holder">Email</label>
					<input id="email" name="txtemail" type="text" class="form-control" placeholder="Email" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_email;?>" disabled>
				  </div>
				  
				  <div class="form-group col-sm-6">
					<label for="name-holder">Mobile Number</label>
					<input id="number" name="txtnumber" type="text" class="form-control" placeholder="Mobile Number" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_mobile;?>" disabled>
				  </div>
				  
				</div>
			 </div>
		</div>
		<div class="col-md-12 col-lg-12" style="height:100%;">
			<div class="summary">
				<h4><strong>Select Payment Channel:</strong></h4>
				<select class="form-control" id="pchannel" name="pchannel">
					<option value="">- Please choose -</option>
					<?php foreach($arr_method as $k=>$v){
						echo '<option value="'.encode($k).'" '.(($_ptype==$k)?'selected':'').'>'.$v['label'].'</option>'; 
					}?>
                </select>				
			</div>	
		</div>
		<div class="col-md-12 col-lg-12" style="height:100%;margin-top:10px;">
			<div class="summary">
				<h4><b>Breakdown:</b></h4>
				<div class="table-responsive">
				   <table class="table table-condense">
					  <tbody>
						<?php echo $tblfee;?>
					  </tbody>
				   </table>
				</div>
				<div class="">
				<div class="summary-item"><strong><span class="price pull-right">PHP : <?php echo number_format($t_amount,2); ?></span></strong></div>
				</div>
				<?php
					$strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
					$strxml = $strxml . "<Request>";
					$strxml = $strxml . "<orders>";
					$strxml = $strxml . "<items>";
					if(@isset($xmlfee)){
					  $strxml .= $xmlfee;
					}else{
					  $strxml = $strxml . "<Items>";
					  $strxml = $strxml . "<itemname>item 1</itemname><quantity>1</quantity><amount>".$_amount."</amount>"; // pls change this value to the preferred item to be seen by customer. (eg. Room Detail (itemname - Beach Villa, 1 Room, 2 Adults       quantity - 0       amount - 10)) NOTE : total amount of item/s should be equal to the amount passed in amount xml node below. 
					  $strxml = $strxml . "</Items>";
					}
					$strxml = $strxml . "</items>";
					$strxml = $strxml . "</orders>";
					$strxml = $strxml . "<mid>" . $_mid . "</mid>";
					$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
					$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
					$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
					$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
					$strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
					$strxml = $strxml . "<mtac_url>".url('epayment/terms')."</mtac_url>"; // pls set this to the url where your terms and conditions are hosted
					$strxml = $strxml . "<descriptor_note>''</descriptor_note>"; // pls set this to the descriptor of the merchant ""
					$strxml = $strxml . "<fname>" . $_fname . "</fname>";
					$strxml = $strxml . "<lname>" . $_lname . "</lname>";
					$strxml = $strxml . "<mname>" . $_mname . "</mname>";
					$strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
					$strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
					$strxml = $strxml . "<city>" . $_city . "</city>";
					$strxml = $strxml . "<state>" . $_state . "</state>";
					$strxml = $strxml . "<country>" . $_country . "</country>";
					$strxml = $strxml . "<zip>" . $_zip . "</zip>";
					$strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
					$strxml = $strxml . "<trxtype>sale</trxtype>";
					$strxml = $strxml . "<email>" . $_email . "</email>";
					$strxml = $strxml . "<phone>" . $_phone . "</phone>";
					$strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
					$strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
					$strxml = $strxml . "<amount>" . $_amount . "</amount>";
					$strxml = $strxml . "<currency>" . $_currency . "</currency>";
					$strxml = $strxml . "<mlogo_url>".url('assets/img/logo-dark.png')."</mlogo_url>";// pls set this to the url where your logo is hosted
					if($_ptype!='' && $_ptype!=='sale'){
					$strxml = $strxml . "<pmethod>".$_ptype."</pmethod>";
					}else{
					$strxml = $strxml . "<pmethod>sale</pmethod>";
					}
					$strxml = $strxml . "<signature>" . $_sign . "</signature>";
					$strxml = $strxml . "</Request>";
					$b64string =  base64_encode($strxml);
				?>
				<div class="submit_form">
				<form name="form1" method="post" action="<?php echo $purl;?>">
					<input type="hidden" name="paymentrequest" id="paymentrequest" value="<?php echo $b64string ?>" style="width:800px; padding: 10px;">
					<?php 
					  echo '<div class="col-sm-12"><hr/></div>';
					  echo '<div class="col-sm-2"><a href="'.url('accounting/soaccounts'.$studno).'" class="btn btn-lg btn-default btn-block"><i class="fa fa-arrow-left"></i> Back</a></div>';
					  echo '<div class="col-sm-10">';
					  if($_amount>0){
					    echo '<input type="'.(($_ptype=='sale')?'button':'submit').'" class="btn btn-primary btn-lg btn-block btnsubmit" value="Pay Now">';
					  }else{
					    echo '<input type="button" class="btn btn-default btn-lg btn-block" disabled value="Nothing to pay">';
					  }
					  echo '</div>';
					?>
				</form>
				</div>
			</div>
		</div>
	 
	</div>

		
 </div>
 </div>	 
</div>

<script src="<?php echo url('assets/resources/jquery-3.2.1.min.js');?>"></script>
<script src="<?php echo url('assets/resources/bootstrap.min.js');?>""></script>
<script type="text/javascript">
    var tid = '<?php echo Request::get('tid');?>';
    var pid = '<?php echo Request::get('pid');?>';
    $(document).ready(function(){
		$('body').on('change','#pchannel',function(){
		   var gid = $(this).val();
		   if(tid!=''){
		   window.location.href=base_url+'epayment?tid='+tid+'&gid='+gid;
		   }
		   
		   if(pid!=''){
		   window.location.href=base_url+'epayment?pid='+pid+'&gid='+gid;
		   }
		});
		
		$('body').on('click','.btnsubmit',function(){
			if($(this).is('[type="button"]')){
				msgbox('error','Select a payment channel first!!!');
			}else{
				msgbox('warning','Redirecting! Please Wait!');
			}
		});
	});
</script>