@foreach($data as $d)
<tr data-prog='{{$d->ProgID}}' data-major='{{$d->MajorID}}' data-level='{{$d->YearLevelID}}' >
    <td><input type="checkbox" /> </td>
    <td>{{$d->Program}}</td>
    <td>{{$d->Major}}</td>
    <td> 
        @if( isSHS($d->ProgID) == '1' )
          @if($d->YearLevelID == 5)
          G11
          @else
          G12
          @endif
        @else          
          G{{$d->YearLevelID}} 
        @endif   
    </td>
</tr>
@endforeach