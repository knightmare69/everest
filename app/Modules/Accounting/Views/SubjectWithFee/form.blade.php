<?php
    $model = new \App\Modules\Setup\Models\Programs;
    $data =  $model->orderby('ProgName','ASC')->get();
    $status= array( 0 => 'All', 1 => 'New', 2 => 'Old' );

?>
<div id="template_modal" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> <h4 class="modal-title">Template Info</h4>
    	</div>
        <div class="modal-body">
        	<div class="row">
                <div class="col-md-12">
                    @include('errors.event')
                </div>
            </div>                
            <form action="#" class="form-horizontal" id="form_template" name="form_template">
                    <fieldset>
                        <div class="form-group" style="margin-bottom: 8px;">
                            <div class="col-md-12">
        						<label class="control-label">Transaction Type</label>
                                <select id="type" name="type" class="form-control input-sm">
                                    <option value="1" selected="">ENROLLMENT</option>
                                    <option value="2">ADD/DROP/CHANGE</option>
                                    <option value="15">OTHER TRANSACTIONS</option>
                                </select>
                            </div>
    					</div>
                        <div class="form-group" style="margin-bottom: 8px;">
                            <div class="col-sm-8 col-sm-12">
                                <label class="control-label">Template Code</label>
    				            <input type="text" id="code" name="code" class="form-control input-sm ">
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <label class="control-label">&nbsp;</label>
                                <label class="checkbox"><input type="checkbox" id="foreign" name="foreign" class="zeckbox"> For Foreign </label>
                            </div>
                        </div>
    					<div class="form-group" style="margin-bottom: 8px;">
                            <div class="col-sm-12">
    						  <label class="control-label">Description</label>
                              <input type="text" id="desc" name="desc" class="form-control input-sm ">
                            </div>
    					</div>
                    <div class="form-group" style="margin-bottom: 8px;">
                        <div class="col-md-4 col-sm-12">

    						<label class="control-label">Gender</label>
                            <select id="gender" name="gender" class="form-control input-sm  ">
                                <option value="0" selected="">ALL</option>
                                <option value="1">MALE ONLY</option>
                                <option value="2">FEMALE ONLY</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-12">
    						<label class="control-label">Applies to which student?</label>
                            <select id="status" name="status" class="form-control input-sm">
                                <?php $z = 0;?>
                                @foreach($status as $s)
                                <option value="{{$z}}" >{{$s}} Students <?= $z > 0 ? 'Only' : '' ?> </option>
                                <?php $z++; ?>
                                @endforeach
                            </select>
                            </div>
                             <div class="col-md-4 col-sm-12">

    						    <label class="control-label">Currency</label>
                                <select id="curr" name="curr" class="form-control input-sm">
                                    <option value="1">PHP</option>
                                    <option value="2">USD</option>
                                </select>
                            </div>
                            
                     </div>

                     <div class="form-group">
                        <div class="col-md-6 col-md-12">
    						<label class="control-label">Payment Options</label>
                            <select id="scheme" name="scheme" class="form-control">
                                <option value="0">CASH</option>
                                <option value="1">SEMESTRAL</option>
                                <option value="2">QUARTERLY/INSTALLMENT</option>
                                <option value="3">MONTHLY</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-12">

    						<label class="control-label">Payment Schedule</label>
                            <select id="option" name="option" class="form-control select2me input-sm">
                                <option>- select -</option>
                                <?php $option = getPaymentOptions();  ?>
                                @foreach($option as $o)
                                <option value="{{$o->PaymentOptionID}}" >{{$o->PayOption}}</option>
                                @endforeach
                            </select>
                            </div>
                           
    					</div>
                    <div class="form-group">
                        <div class="col-sm-12">
						  <label class="control-label">Remarks</label>
                          <input type="text" id="remarks" name="remarks" class="form-control input-sm not-required ">
                        </div>
					</div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
    							<div class="icheck-inline">
                                    <label class="checkbox"><input type="checkbox" id="inactive" name="inactive" class="zeckbox"> Inactive </label>
    							</div>
    						</div>
                        </div>
					</div>

                    <input type="hidden" id="idx" name="idx" value="0" />
                    <input type="hidden" id="mterm" name="term" value="0" />
                    <input type="hidden" id="mcampus" name="campus" value="1" />
                    
                </fieldset>
            </form>
    	</div>
    	<div class="modal-footer">
    		<button class="btn" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
    		<button class="btn green btn-primary" data-menu="save" data-mode="template" type="button">Save Form</button>
    	</div>
    </div>
</div>
</div>