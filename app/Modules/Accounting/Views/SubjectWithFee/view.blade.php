<?php

    $template = \App\Modules\Accounting\Models\TableOfFees::where('TemplateID', $ref)->first();
    $status = get_template_status();
    $session = get_template_session();

?>
<style>
    .fees-table{
        table-layout: fixed;
        border-collapse: collapse;
    }
</style>
<div class="portlet light" style="margin-bottom: 0px; ">
	<div class="portlet-title">
		<div class="caption font-blue-madison"><i class="fa fa-cubes"></i>
			<span class="caption-subject bold font-blue-madison uppercase"><?= isset($template) ? $template->TemplateCode . ' | '. $template->TemplateDesc : '' ?></span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline  input-medium">
	           <input type="text" class="form-control input-circle  filter-table input-sm " data-table="records-table" />
			</div>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
            <a data-original-title="Return to list" title="Return to list" class="btn btn-circle btn-icon-only btn-default" href="{{url('accounting/fees-template?t='. encode($term) )}}"><i class="fa fa-list"></i></a>
		</div>
	</div>
	<div class="portlet-body" >
        <div id="left_container" >
               <button class="btn btn-primary btn-sm" data-menu="add" data-mode="fees" type="button"><i class="fa fa-file"></i> Reset </button>
               <button class="btn btn-info btn-sm" data-menu="save" data-mode="fees" type="button"><i class="fa fa-save"></i> Save </button>
               <hr style="margin-bottom: 5px !important; margin-top: 5px !important;" />
               <div id="templatecontainer">
                    <form role="form" action="#" id="formFee">
                        <div class="form-group">
    						<label class="control-label">Account Name</label>
                            <select id="acct" name="acct" class="form-control select2me" data-col="acct">
                                <option value="-1">- select -</option>
                                @foreach(\App\Modules\Accounting\Models\ChartOfAccounts::orderBy('AcctName','ASC')->get() as $a)
                                <option value="{{$a->AcctID}}">{{$a->AcctName.' ['.$a->AcctCode.']'}}</option>
                                @endforeach
                            </select>
    					</div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
            						<label class="control-label">Amount</label>
                                    <div class="input-group">
        								<div class="input-group-btn">
        									<button type="button" class="btn default" tabindex="-1">PHP</button>
        									<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
        									   <i class="fa fa-angle-down"></i>
        									</button>
        									<ul class="dropdown-menu currency-sel" role="menu">
        										<li><a href="#" data-value="1" class="active"  > PHP </a></li>
        										<li><a href="#" data-value="2"> USD </a></li>
        										<li class="divider"></li>
        										<li><a href="#">Cancel </a></li>
        									</ul>
        								</div>
        								<input type="text" class="form-control numberonly text-right" name="amt" id="amt" data-col="amt" value="0.00"/>
                                        <input type="hidden" class="form-control" name="curr" id="curr" data-col="curr" value="1"/>
        							</div>
            					</div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                            		<label class="control-label">Sequence</label>
                                    <input type="text" class="form-control" name="seqno" id="seqno" data-col="seq" value="0"/>
                            	</div>
                            </div>
                        </div>
                        <div class="form-group">
    						<label class="control-label">Applies to</label>
                            <select id="status" name="status" class="form-control " data-col="status" >
                                <option value="-1">- select -</option>
                                <?php $z = 0;?>
                                @foreach($status as $s)
                                <option value="{{$z}}" >{{$s}} Students <?= $z > 0 ? 'Only' : '' ?> </option>
                                <?php $z++; ?>
                                @endforeach
                            </select>
    					</div>
                        <div class="form-group">
    						<label class="control-label">Session Mode</label>
                            <select id="session" name="session" class="form-control " data-col="session" >
                                <option value="-1">- select -</option>
                                <?php $z = 0;?>
                                @foreach($session as $s)
                                <option value="{{$z}}" >{{$s}}</option>
                                <?php $z++; ?>
                                @endforeach
                            </select>
    					</div>
                        <div class="form-group">
    						<label class="control-label">Remarks</label>
                            <textarea rows="2" name="remarks" class="form-control not-required " data-col="remarks" style="resize: none;"></textarea>
    					</div>
                        <div class="form-group">
    						<label class="control-label">Year Level</label>
                            <select id="level" name="level" class="form-control " data-col="level" >
                                <option value="0"> Applies to All </option>
                                @foreach(get_Yearlevel() as $y)
                                <option value="{{$y->YLID_OldValue}}" >{{$y->YearLevelName}}</option>
                                @endforeach
                            </select>
    					</div>
                        <div class="form-group">
    						<label class="control-label">Special Condition</label>
                            <select id="condition" name="condition" class="form-control select2me not-required" multiple="" data-col="level" >
                                <option value="0"> Applies to All </option>
                                @foreach(get_Yearlevel() as $y)
                                <option value="{{$y->YLID_OldValue}}" >{{$y->YearLevelName}}</option>
                                @endforeach
                            </select>
    					</div>
                        <input type="hidden" id="idx" name="idx" data-col="id" value="0" />
                        <input type="hidden" id="template" name="template" data-col="template" value="{{encode($ref)}}" />
                    </form>
               </div>
        </div>
        <div id="right_container">
            <button class="btn btn-danger btn-sm" data-menu="delete" data-mode="fees" type="button"><i class="fa fa-times"></i>  Delete Selected </button>
            <button class="btn btn-info btn-sm" data-menu="custom" data-mode="fees" type="button"><i class="fa fa-calendar"></i>  Custom Installment </button>
            <a data-original-title="Return to list" title="Return to list" class="btn pull-right btn-sm btn-default" href="{{url('accounting/fees-template?t='. encode($term) )}}"><i class="fa fa-list"></i> Return </a>
            <hr style="margin-bottom: 5px !important; margin-top: 5px !important;" />
            <div id="fee-section">
                @include($views.'fees')
            </div>
        </div>
	</div>
</div>