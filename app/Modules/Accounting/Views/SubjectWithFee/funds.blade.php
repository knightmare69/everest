<?php
    $rs = DB::select("SELECT * FROM es_accountgroups");
    $i =1;
?>
<div class="portlet box blue-hoki">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Fund / Groups
		</div>
		<div class="actions">
			<a href="#" class="btn btn-default btn-sm">
			<i class="fa fa-pencil"></i> Edit </a>
			<a href="#" class="btn btn-default btn-sm">
			<i class="fa fa-plus"></i> Add </a>
		</div>
	</div>
	<div class="portlet-body">		
			<table class="table table-hover table-striped table-condensed table-bordered" id="tblfund" style="cursor: pointer;">
			<thead>
			<tr>
				<th> # </th>
				<th> Code </th>
				<th> Name / Description </th>
				<th> Short Name </th>
			</tr>
			</thead>
			<tbody>
			 @foreach($rs as $r)
                <tr>
                    <td class="autofit">{{$i}}</td>
                    <td class="autofit">{{$r->GroupCode}}</td>
                    <td class="autofit">{{$r->GroupName}}</td>
                    <td class="autofit">{{$r->GroupShort}}</td>
                </tr>
                <?php $i++; ?>
             @endforeach
			</tbody>
			</table>
	
	</div>
</div>