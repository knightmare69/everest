<?php
    $j=1;
    
    $status= array( 0 => 'All Students', 1 => 'New Student Only', 2 => 'Old Student Only' );

    $scheme = array(
        0 => 'Cash',
        1 => 'Semestral',
        2 => 'Quarterly',
        3 => 'Monthly',
    );
                                        
    function getPrograms($template){
        $programs = new \App\Modules\Accounting\Models\TableOfFees_Programs;            
        $rs = $programs::selectRaw("ProgID, fn_ProgramCode(ProgID) As Code, fn_MajorName(MajorID) As Major, YearLevelID As Yr ")->where('TemplateID', $template )->get();
        $output = "";
        
        foreach($rs as $r){
            if( isSHS($r->ProgID) =='1' ){
                $output .=  ($output != '' ? ', ' : '') . $r->Code . ' ' . $r->Major . ' ' .  ( $r->Yr == 5 ? 'G11' : 'G12')  ;
            }else{
                $output .=  ($output != '' ? ', ' : '') . $r->Code . ' ' . $r->Major . ' ' . $r->Yr ;
            }
            
        }
                    
        return $output;
    }
?>                                    
<div class="table-scrollable"> 
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table" style="cursor: pointer;">
    <thead>
        <tr>            
            <th class="autofit">#</th>
            <th>Template Name</th>
            <th>Program Applies To</th>
            <th>Foreign</th>
            <th>Currency</th>
            <th>Applies to</th>
            <th>Payment <br /> Scheme</th>
            <th>User </th>
            <th>Date Modified</th>

        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
        @foreach($data as $r)
        <tr data-id="{{ encode($r->TemplateID) }}" data-campus="<?= encode($r->CampusID)?>" data-curr="<?= ($r->CurrencyID)?>" data-status="{{$r->StudentStatus}}" data-scheme="{{$r->PaymentScheme}}" >
            <td class="font-xs autofit bold">{{$j}}.</td>
            <td class="">
                <a href="javascript:void();" class="options" data-menu="view">{{$r->TemplateDesc}}</a>
                <br /><small class="font-xs bold tcode">{{ $r->TemplateCode }}</small>
            </td>
            <td>
                <a class="pull-right btn btn-xs btn-circle" data-menu="setup" href="javascript:void(0);" title="Setup Academic Program"><i class="fa fa-cog"></i> </a>
                <?= getPrograms($r->TemplateID); ?>
            </td>
            <td class="autofit text-center"><?= $r->ForForeign == 1 ? '<i class="fa fa-check"></i>': '' ?></td>
            <td class="autofit text-center">{{$r->Curr}}</td>
            <td class="autofit">{{$status[$r->StudentStatus]}}</td>
            <td class="autofit">{{$scheme[$r->PaymentScheme] }}</td>
            <td class="autofit ">{{$r->ModifiedBy}}</td>
            <td class="autofit">{{$r->LastModified}}</td>
        </tr>
        <?php $j++; ?>
        @endforeach
        @endif
    </tbody>
</table>
</div>

<ul class="pagination pagination-sm">
	<li><a  href="{{$url.'&v=1'}}" ><i class="fa fa-angle-left"></i></a></li>
    @for($i =1 ; $i<= $max; $i++ )        
    <li><a class="<?= $page == $i || $i =='0' ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a></li>    
    @endfor	
	<li><a href="{{$url.'&v='.$max}}"><i class="fa fa-angle-right"></i></a></li>
</ul>