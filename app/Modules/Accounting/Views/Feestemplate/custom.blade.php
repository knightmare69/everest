<?php

    $j = 1;
    $total = array(
        '0' => 0
       ,'1' => 0
       ,'2' => 0
       ,'3' => 0
       ,'4' => 0
       ,'5' => 0
       ,'6' => 0
       ,'7' => 0
       ,'8' => 0
       ,'9' => 0
       ,'10' => 0
       ,'11' => 0
    );
    $bal=0;
    $option = 0;

    $model = new \App\Modules\Accounting\Models\TableOfFees_Details;
    $data = $model->selectRaw("AccountID, a.AcctCode,a.AcctName, a.ShortName, Amount, TemplateID , IndexID, CurrencyID, Remarks,
            dbo.fn_CurrencyCode(CurrencyID) As Curr, es_tableoffee_details.Inactive, ChargeMode, SeqNo, [Option], Option_Session, Option_Yr,
            [1stPayment], [2ndPayment], [3rdPayment], [4thPayment], [5thPayment], [6thPayment], [7thPayment], [8thPayment],[9thPayment],[10thPayment]
            ")
            ->leftJoin("es_accounts as a","a.AcctID",'=','es_tableoffee_details.AccountID')
            ->where("es_tableoffee_details.TemplateID",$ref)
            ->orderBy('SeqNo','asc')
            ->get();
    $dues = DB::table('es_tableoffees_dates')->where('TemplateID', $ref)->first();

    $status = get_template_status();
    $session = get_template_session();

?>
<style>
  .input-no-border{
	 width:100% !important;
	 height:100% !important;
	 border:none !important;
	 border-color:transparent !important;
	 outline:none !important;
	 box-shadow:transparent;
	 background: transparent;
	 background-color: transparent !important;
	 padding-top:0px !important;
	 font-size: 12px !important;
  }
</style>
<div  style="overflow-x:hidden;overflow-y:scroll;" id="custom_header">
<table class="table table-striped table-bordered table-hover table-condensed table_scroll fees-table" style="cursor: pointer; margin-bottom: 0px; ">
    <thead class="text-center">
        <tr class="text-center">
            <th class="text-center" width="28">#</th>
            <th width="80" class="text-center"><div style="width: 80px !important;">Code</div></th>
            <th width="100" >Account Name</th>
            <th width="50" class="text-center">Curr</th>
            <th width="80" class="text-center">Amount</th>
            <th width="110" class="text-center">1st<br />Payment</th>
            <th width="110" class="text-center">2nd<br />Payment</th>
            <th width="110" class="text-center">3rd<br />Payment</th>
            <th width="110" class="text-center">4th<br />Payment</th>
            <th width="110" class="text-center">5th<br />Payment</th>
            <th width="110" class="text-center">6th<br />Payment</th>
            <th width="110" class="text-center">7th<br />Payment</th>
            <th width="110" class="text-center">8th<br />Payment</th>
            <th width="110" class="text-center">9th<br />Payment</th>
            <th width="110" class="text-center">10th<br />Payment</th>
            <th width="100" class="text-center">Balance</th>
        </tr>
    </thead>
</table>
</div>
<div class="table-scrollable" id="custom_box" style="height:450px; overflow-y:  scroll; margin-top: 0 !important; margin-bottom: 0px !important;">
    <table class="table table-striped table-bordered table-hover table-condensed fees-table " id="customtable" style="cursor: pointer; margin-bottom: 0px !important;">
        <tbody>
            @foreach($data as $r)
                <?php
                    $option = $r->Option != '' ? $r->Option : '0';
                    $bal = floatval($r->Amount) - floatval($r['1stPayment']) - floatval($r['2ndPayment']) - $r['3rdPayment'] - $r['4thPayment']- $r['5thPayment']
                                      - floatval($r['6thPayment']) - $r['7thPayment']- $r['8thPayment'] - $r['9thPayment']- $r['10thPayment']
                    ;

                    $bal = round($bal,2);
                    $total[11] += $bal;
                ?>
            <tr data-id="{{ encode($r->IndexID) }}" data-template="<?= encode($r->TemplateID)?>" data-acct="{{$r->AccountID}}" data-curr="<?= ($r->CurrencyID)?>" data-status="{{$r->Option}}" data-session="{{$r->Option_Session}}" data-scheme="{{$r->PaymentScheme}}" data-amt="{{number_format($r->Amount,2)}}" data-seq="{{ $r->SeqNo}}" data-remarks="{{ $r->Remarks}}" data-level="{{ $r->Option_Yr}}" >
                <td width="28" class="font-xs bold"><div style="width: 28px !important;">{{$j}}.</div></td>
                <td width="80" class=""><div style="width: 80px !important;">{{ $r->AcctCode }}</div></td>
                <td width="100" ><a href="javascript:void(0)" class="options" data-menu="edit" data-mode="fees">{{ $r->AcctName }}</a><br />{{ $r->ShortName }} </td>
                <td width="50" class="text-center">{{ $r->Curr }}</td>
                <td width="80" class=" assess text-right bold">{{ number_format($r->Amount,2) }}</td>
                <td width="110" class=" editable text-right " data-col="1" tabindex="1{{$j}}" >{{ number_format($r['1stPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="2" tabindex="2{{$j}}" >{{ number_format($r['2ndPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="3" tabindex="3{{$j}}" >{{ number_format($r['3rdPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="4" tabindex="4{{$j}}" >{{ number_format($r['4thPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="5" tabindex="5{{$j}}" >{{ number_format($r['5thPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="6" tabindex="6{{$j}}" >{{ number_format($r['6thPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="7" tabindex="7{{$j}}" >{{ number_format($r['7thPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="8" tabindex="8{{$j}}" >{{ number_format($r['8thPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="9" tabindex="9{{$j}}" >{{ number_format($r['9thPayment'],2) }}</td>
                <td width="110" class=" editable text-right " data-col="10" tabindex="10{{$j}}" >{{ number_format($r['10thPayment'],2) }}</td>
                <td width="80" class=" text-right cbalance <?= $bal !=0 ? 'danger':'' ?> ">{{number_format($bal,2)}}</td>

            </tr>
            <?php $j++;
                  $total[0] += $r->Amount;
                  $total[1] += $r['1stPayment'];
                  $total[2] += $r['2ndPayment'];
                  $total[3] += $r['3rdPayment'];
                  $total[4] += $r['4thPayment'];
                  $total[5] += $r['5thPayment'];
                  $total[6] += $r['6thPayment'];
                  $total[7] += $r['7thPayment'];
                  $total[8] += $r['8thPayment'];
                  $total[9] += $r['9thPayment'];
                  $total[10] += $r['10thPayment'];
            ?>
            @endforeach
        </tbody>
         <tfoot>
            <tr class="total"  >
                <td width="28"><div style="width: 28px !important;"></div></td>
                <td width="70" class=""><div style="width: 70px !important;"></div></td>

                <td class="" width="32%" ><div  class="bold text-right">Total:</div></td>
                <td width="50" class="text-center"></td>
                <td width="80" class="text-right bold ">{{ number_format($total[0],2) }}</td>
                <td width="110" class="text-right bold " data-col="1" >{{ number_format($total[1],2) }}</td>
                <td width="110" class="text-right bold " data-col="2">{{ number_format($total[2],2) }}</td>
                <td width="110" class="text-right bold " data-col="3">{{ number_format($total[3],2) }}</td>
                <td width="110" class="text-right bold " data-col="4">{{ number_format($total[4],2) }}</td>
                <td width="110" class="text-right bold " data-col="5">{{ number_format($total[5],2) }}</td>
                <td width="110" class="text-right bold " data-col="6">{{ number_format($total[6],2) }}</td>
                <td width="110" class="text-right bold " data-col="7">{{ number_format($total[7],2) }}</td>
                <td width="110" class="text-right bold " data-col="8">{{ number_format($total[8],2) }}</td>
                <td width="110" class="text-right bold " data-col="9">{{ number_format($total[9],2) }}</td>
                <td width="110" class="text-right bold " data-col="10">{{ number_format($total[10],2) }}</td>
                <td width="80" class=" text-right bold ">{{number_format($total[11],2)}}</td>
            </tr>
             <tr data-due="{{getObjectValue($dues,'IndexID')}}" >
                <td width="28"><div style="width: 28px !important;"></div></td>
                <td width="70" class=""><div style="width: 70px !important;"></div></td>
                <td class="" width="32%" ><div  class="bold text-right">Due Dates:</div></td>
                <td width="50" class="text-center"></td>
                <td width="80" class="text-center bold">{{setDateFormat( getObjectValue($dues,'Date1'),'yyyy-mm-dd','mm/dd/yyyy')}}</td>
                <td width="110" class="text-center bold"><input data-id="1" value="{{setDateFormat(getObjectValue($dues,'Date1'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="2" value="{{setDateFormat(getObjectValue($dues,'Date2'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="3" value="{{setDateFormat(getObjectValue($dues,'Date3'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="4" value="{{setDateFormat(getObjectValue($dues,'Date4'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold  form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="5" value="{{setDateFormat(getObjectValue($dues,'Date5'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold  form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="6" value="{{setDateFormat(getObjectValue($dues,'Date6'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold  form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="7" value="{{setDateFormat(getObjectValue($dues,'Date7'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold  form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="8" value="{{setDateFormat(getObjectValue($dues,'Date8'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="9" value="{{setDateFormat(getObjectValue($dues,'Date9'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border bold  form-control date-picker" /> </td>
                <td width="110" class="text-center bold"><input data-id="10" value="{{setDateFormat(getObjectValue($dues,'Date10'),'yyyy-mm-dd','mm/dd/yyyy')}}" class="input-no-border  bold form-control date-picker" /> </td>
                <td width="80" class=" text-right bold"></td>
            </tr>
        </tfoot>
    </table>
    <label id='tbleditor' style='display: none;' class=' input table-editor' data-table="customtable">
        <input id="txteditor" value="0" class="form-control input-sm text-right numberonly " maxlength="9" style="border: none;" />
    </label>
</div>
