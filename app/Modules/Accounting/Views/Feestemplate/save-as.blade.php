<?php
    $model = new \App\Modules\Setup\Models\Programs;
    $data =  $model->orderby('ProgName','ASC')->get();
    $status= array( 0 => 'All', 1 => 'New', 2 => 'Old' );
    $campus = get_campus();

?>
<div id="saveas_modal" class="modal fade" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> <h4 class="modal-title">Save As Template </h4>
    	</div>
        <div class="modal-body">
        	<div class="row">
                <div class="col-md-12">
                    @include('errors.event')
                </div>
            </div>                
            <form action="#" class="form-horizontal" id="form_saveas" name="form_saveas">
                    <fieldset>          
                    
                        <div class="form-group" style="margin-bottom: 8px;">
                            <label class="control-label col-md-4 col-lg-4">Save As From</label>
                            <div class="col-md-8 col-lg-8">                                
                                <span id="from" class="form-control input-sm"></span>    				            
                            </div>                           
                        </div>
                        <hr />
                        
                        <div class="form-group">
                            <label class=" col-sm-4 control-label">Academic Year Term </label>
                            <div class="col-sm-8">
                            <select class="select2 form-control" name="term">
                                <option value="0" selected="" >- Select AY Term -</option>
                                @foreach($at as $r)
                                <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ ($r->TermID) }}">{{ $r->AYTerm }}</option>
                                @endforeach
                            </select>
                            </div>                                                        
                        </div>   
                        <div class="form-group">
                            <label class=" col-sm-4 control-label">Campus Name </label>
                            <div class="col-sm-8">
                            <select class="form-control" name="campus">
                                <option value="0" selected="" >- Select Campus -</option>
                                @foreach($campus as $r)
                                <option <?= $term== $r->CampusID ? 'selected' : '' ?>  value="{{ ($r->CampusID) }}">{{ $r->CampusName }}</option>
                                @endforeach
                            </select>
                            </div>                                                        
                        </div>   
                        
                                   
                        <div class="form-group" style="margin-bottom: 8px;">
                            <label class="control-label col-md-4 col-lg-4">Template Code</label>
                            <div class="col-md-8 col-lg-8">                                
    				            <input type="text" id="code" name="code" class="form-control input-sm ">
                            </div>
                           
                        </div>
                       	<div class="form-group" style="margin-bottom: 8px;">
                            <label class="control-label col-sm-4 col-lg-4">Description</label>
                            <div class="col-sm-8 col-lg-8">    						  
                              <input type="text" id="desc" name="desc" class="form-control input-sm ">
                            </div>
    					</div>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Remarks</label>
                            <div class="col-sm-8">
    						  <textarea name="remarks" class="form-control not-required" rows="2" style="resize: none;"></textarea>                              
                            </div>
    					</div>
                    
                        <div class="form-group">
                            <label class="control-label col-lg-4">&nbsp;</label>
                            <div class="col-md-8 col-sm-12 col-lg-8">
                                
                                <label class="checkbox"><input type="checkbox" id="foreign" name="foreign" class="zeckbox"> For Foreign </label>
                            </div>                            
                        </div>
    				
                        <input type="hidden" id="saveas_idx" name="idx" value="0" />
                    
                </fieldset>
            </form>
    	</div>
    	<div class="modal-footer">
    		<button class="btn" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
    		<button class="btn green btn-primary" data-menu="saveas_save" data-mode="template" type="button">Save Form</button>
    	</div>
    </div>
</div>
</div>