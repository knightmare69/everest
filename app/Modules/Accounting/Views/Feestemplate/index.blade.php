<?php
    $at = getAYTerm();
    $add = 0;
        $max =  $total / 50;
        if( $total % 50 <= 4) $add = 1;
        $max = floor($max) + $add;

        $url = url('accounting/fees-template?v='. rand() .'&t='.encode($term).'&f='.($filter));
?>
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-cubes"></i>
			<span class="caption-subject bold uppercase"> Fees Setup </span>
			<span class="caption-helper hidden-xs">Use this module to setup Fees Template for enrollment and other transaction(s).</span>
		</div>
		<div class="actions hidden-xs">
            <div class="portlet-input input-inline  input-medium">
	           <input type="text" class="form-control input-circle filter-table input-sm " data-table="records-table" />
			</div>
            <a href="javascript:location.reload();" class="btn btn-circle btn-icon-only btn-default " data-original-title="" title="Refresh"><i class="fa fa-refresh"></i></a>
			<a href="javascript:void();" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
    <div class="row">
        <div class="col-sm-12">
               
                <div class="portlet-input input-inline input-medium">
                    <select class="select2 form-control" name="term" id="term">
                        <option value="0" >- Select AY Term -</option>
                        @foreach($at as $r)
                        <option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AYTerm }}</option>
                        @endforeach
                    </select>
    			</div>
                <div class="portlet-input input-inline">
                    <button type="button" class="btn btn-sm btn-primary " data-menu="add"><i class="fa fa-plus"></i> <span class="hidden-xs">Create New</span> </button>
                    <button type="button" class="btn btn-sm btn-success " data-menu="edit"><i class="fa fa-edit"></i> <span class="hidden-xs">Edit</span> </button>
                    
                    <button type="button" class="btn btn-sm btn-danger " data-menu="remove"><i class="fa fa-trash"></i> <span class="hidden-xs">Remove</span> </button>
                    <button type="button" class="btn btn-default btn-sm" data-menu="print2"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span> </button>
        
                    <button type="button" class="btn btn-sm btn-default " data-menu="refresh"><i class="fa fa-refresh"></i> <span class="hidden-xs">Refresh</span> </button>
                    <button type="button" class="btn btn-sm btn-default  " data-menu="saveAs"><i class="fa fa-save"></i> <span class="hidden-xs">Save As</span> </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
             
                    <ul class="pagination pagination-sm hide">
                    	<li><a  href="{{$url.'&v=1'}}" ><i class="fa fa-angle-left"></i></a></li>
                        @for($i =1 ; $i<= $max; $i++ )
                            <li><a class="<?= $page == $i || $i =='0' ? 'active' : '' ?>" href="{{$url.'&v='.$i}}">{{$i}}</a></li>
                        @endfor
                    	<li>
                    		<a href="{{$url.'&v='.$max}}"><i class="fa fa-angle-right"></i></a>
                    	</li>
                    </ul>
                
                <div class="scroller " id="grdList">
                    @include($views.'list')
                </div>
            </div>
        </div>
	</div>
</div>
@include($views.'form')
@include($views.'modal')
@include($views.'save-as')