<div id="setup_modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog" ><div class="modal-content">
    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h4 class="modal-title">Setup Academic Programs</h4></div>
    <div class="modal-body">
        
        <form role="form" class="form-horizontal" action="?" id="frm_programs" >
            <fieldset>
                <div class="form-group" style="margin-bottom: 8px;">
                    <div class="col-md-12">
                        <label class="control-label">Academic Programs</label>
                        <select id="prog" name="prog" class="form-control select2me">
                        <option value="0">- Select -</option>
                        @foreach(get_programs() as $p)
                        <option value="{{$p->ProgID}}">{{$p->ProgName}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 8px;">
                    <div class="col-md-12">
                        <label class="control-label">Major / Strands </label>
                        <select id="major" name="major" class="form-control select2me" multiple="">
                        <option> - Select </option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 8px;">
                    <div class="col-md-12">
                        <label class="control-label">Year Level </label>
                        <select id="level" name="level" class="form-control select2me" multiple="">
                        @foreach(get_Yearlevel() as $y)
                        <option value="{{$y->YearLevelID}}">{{$y->YearLevelName}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>                
            </fieldset>
        </form>
        
        <button class="btn btn-sm btn-primary" data-menu="add-row"  type="button"> Add Row </button>
        <button class="btn btn-sm btn-danger" data-menu="rem-row"  type="button"> Remove selected </button>
        
        <hr style="margin-top: 10px; margin-bottom: 10px;" />
               
        <table id="tblprograms" class="table table-striped table-bordered table-hover table_scroll dataTable table-condensed no-footer" style="cursor: pointer; margin-bottom: 0px !important;">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Programs</th>
                    <th>Strand/Track</th>
                    <th>Year Level</th>                    
                </tr>
            </thead>
            <tbody>
                            
            </tbody>
           
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn btn-success" data-menu="save-programs" type="button"> Save </button>
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button"><i class="fa fa-times"></i> Close</button>
    </div></div></div>
</div>