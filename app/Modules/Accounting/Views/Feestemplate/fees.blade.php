<?php
    
    $j = 1;
    $total =0;
    $option = 0;
    
    $model = new \App\Modules\Accounting\Models\TableOfFees_Details;    
    $data = $model->selectRaw("AccountID, a.AcctCode,a.AcctName, a.ShortName, Amount, TemplateID , IndexID, CurrencyID, Remarks,
            dbo.fn_CurrencyCode(CurrencyID) As Curr, es_tableoffee_details.Inactive, ChargeMode, SeqNo, [Option], Option_Session, Option_Yr ")
            ->leftJoin("es_accounts as a","a.AcctID",'=','es_tableoffee_details.AccountID')
            ->where("es_tableoffee_details.TemplateID",$ref)
            ->orderBy('SeqNo','asc')
            ->get();
            
    $status = get_template_status();    
    $session = get_template_session();
    
?>
<div  style="  overflow-y:  scroll;">
<table class="table table-striped table-bordered table-hover table-condensed table_scroll fees-table" style="cursor: pointer; margin-bottom: 0px; ">
    <thead>
        <tr>
            <th width="35"><input type="checkbox" class="chk-header"></th>
            <th class="text-center" width="3%">#</th>
            <th class="" width="32%">Account Description</th>
            <th width="18%"  >Short Name</th>
            <th width="40" class="text-center">Curr</th>
            <th width="80" class="text-center">Amount</th>
            <th width="15%">Applies<br />Student</th>
            <th width="17%">Remarks</th>
            <th width="35">Seq</th>
            <th width="60" class="text-center">Inactive</th>                                               
        </tr>
    </thead>
</table>
</div>
<div class="table-scrollable" id="feestemplate" style="height:450px; overflow-y:  scroll; margin-top: 0 !important; margin-bottom: 0px !important;">
    <table class="table table-striped table-bordered table-hover table-condensed fees-table " id="records-table" style="cursor: pointer; margin-bottom: 0px !important;">
        <tbody>
            @foreach($data as $r)
			<?php                      
				$option  = $r->Option != '' ? $r->Option : '0';       
				$ptempid = $r->TemplateID;
				$ptermid = $r->TermID;
				$pscheme = $r->PaymentScheme;
				$pcheck  = DB::SELECT("SELECT * FROM ES_TableofFees WHERE TemplateID='".$ptempid."'");
			?>
            <tr data-id="{{ encode($r->IndexID) }}" data-template="<?= encode($r->TemplateID)?>" data-acct="{{$r->AccountID}}" data-curr="<?= ($r->CurrencyID)?>" data-status="{{$r->Option}}" data-session="{{$r->Option_Session}}" data-scheme="{{$r->PaymentScheme}}" data-amt="{{number_format($r->Amount,2)}}" data-seq="{{ $r->SeqNo}}" data-remarks="{{ $r->Remarks}}" data-level="{{ $r->Option_Yr}}" >
                <td width="35"><input type="checkbox" class="chk-child" ></td>
                <td width="3%" class="font-xs  bold">{{$j}}.</td>
                <td class="" width="32%" ><a href="javascript:void(0)" class="options" data-menu="edit" data-mode="fees">{{ $r->AcctName }}</a> <br /> <small title="Accout Code" class="xs-font">{{ $r->AcctCode }}</small> </td>
                <td class="" width="18%" >{{ $r->ShortName }}</td>
                <td width="40" class="text-center">{{ $r->Curr }}</td>
                <td width="80" class=" text-right">{{ number_format($r->Amount,2) }}</td>

                <td width="15%" class="">{{ get_template_status($option) }} Students <?= $option > 0 ? 'Only' : '' ?></td>
                <td width="17%">{{ $r->Remarks }}</td>
                <td width="35" class="text-center">{{ $r->SeqNo }}</td>
                <td width="60" class="text-center"><input type="checkbox"  class="form-control in_active" <?= $r->Inactive == '1' ? 'checked=""' : '' ?>  /></td>
            </tr>
            <?php $j++; $total += $r->Amount; ?>
            @endforeach
        </tbody>
    </table>
</div>
<div  style="  overflow-y:  scroll;">
    <table class="table table-striped table-bordered table-hover table-condensed fees-table " style="cursor: pointer; margin-bottom: 0px !important;">
        <tfoot>
            <tr  >
                <td width="35"></td>
                <td colspan="5" width="58%" class="text-right">Total : </td>
                <td width="80" class="bold text-right">{{ number_format($total,2) }}</td>
                <td width="15%" class=""></td>
                <td width="17%"></td>
                <td width="35" class=""></td>
                <td width="60" class="text-center"></td>
            </tr>
        </tfoot>
    </table>
</div>