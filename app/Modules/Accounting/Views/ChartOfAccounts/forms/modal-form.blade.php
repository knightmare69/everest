<form class="acct-form-modal" role="form" id="acct-modal-form" data-form-type="{{ $form_type }}">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
					<label class="control-label">Code</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-info"></i></span>
                        <input type="text" class="form-control" name="code" id="code">
                    </div>
                    <span class="help-block"></span>
				</div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Short Name</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-language"></i></span>
                        <input type="text" class="form-control" name="sname" id="sname">
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
					<label class="control-label">Name</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-language"></i></span>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <span class="help-block"></span>
				</div>
            </div>
        </div>
    </div>
</form>
