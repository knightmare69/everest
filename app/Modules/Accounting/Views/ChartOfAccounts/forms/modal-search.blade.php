<div class="form">
    <form class="form-horizontal" role="form" id="search-form">
        <div class="form-body">
            {!! $add_content or '' !!}
            @forelse($search as $s)
            <div class="form-group">
                <label class="col-md-3 control-label">{{ $s['label'] }}</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <input type="text" class="form-control" name="{{ $s['id'] }}" id="{{ $s['id'] }}">
                        <span class="input-group-addon">
                            <input type="checkbox" name="dis-{{ $s['id'] }}" id="dis-{{ $s['id'] }}">
                        </span>
                    </div>
                </div>
            </div>
            @empty
            <p class="text-center">No search option(s) available.</p>
            @endforelse
        </div>
    </form>
</div>
