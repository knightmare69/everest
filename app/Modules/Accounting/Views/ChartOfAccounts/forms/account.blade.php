<?php

$pcode = !empty($data->ParentAcctID) ? App\Modules\Accounting\Models\ChartOfAccounts::find($data->ParentAcctID)->AcctCode : '';
$fgcode = !empty($data->GroupID) ? App\Modules\Accounting\Models\AccFundGroup::find($data->GroupID)->GroupCode : '';
$sfcode = !empty($data->SubFundID) ? App\Modules\Accounting\Models\SubFund::find($data->SubFundID)->SubGroupCode : '';
$sucode = !empty($data->SubUnitID) ? App\Modules\Accounting\Models\SubUnits::find($data->SubUnitID)->SubUnitCode : '';
$dept_code = !empty($data->DeptID) ? App\Modules\Setup\Models\Department::find($data->DeptID)->DeptName : '';
$clcode = !empty($data->ClassID) ? App\Modules\Accounting\Models\AccClassif::find($data->ClassID)->ClassCode : '';
$tcode = !empty($data->CategoryID) ? App\Modules\Accounting\Models\AccTypes::find($data->CategoryID)->CategoryName : '';

$ao = isset($data->AcctOption) ? $data->AcctOption : '';
$apo = isset($data->PaymentOption) ? $data->PaymentOption : '';

?>
<form class="" role="form" id="charts-acc-form" data-form-type="account" data-id="{{ !empty($data->AcctID) ? encode($data->AcctID) : 0 }}">
    <div class="form-body">
        <h3 class="form-section font-grey-cascade">
            Account Info
            <label style="float: right;">
                Inactive<input type="checkbox" name="acc-inactive" id="acc-inactive" {{ !empty($data->Inactive) ? 'checked' : '' }}>
            </label>
        </h3>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
					<label class="control-label">Account Code</label>
                    <input type="text" class="form-control" name="acc-code" id="acc-code" value="{{ !empty($data->AcctCode) ? $data->AcctCode : '' }}">
                    <span class="help-block"></span>
				</div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
					<label class="control-label">Parent Account</label>
                    <div class="input-group">
						<input type="text" class="form-control inp-with-attr" name="par-acc" id="par-acc" value="{{ $pcode }}" data-id="{{ !empty($data->ParentAcctID) ? encode($data->ParentAcctID) : '' }}" readonly>
						<span class="input-group-btn">
						    <button class="btn blue search-show-modal" type="button" data-search="par-acc"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
            </div>
            <div class="col-md-4">
                <label class="control-label">Default Amount</label>
                <div class="input-group">
                    <input type="text" class="form-control numberonly" value="{{ !empty($data->DefaultAmount) ? number_format($data->DefaultAmount, 2) : '0.00' }}" name="acc-amount" id="acc-amount" placeholder="0.00">
                    <span class="input-group-addon">
                        <input type="checkbox" class="tooltips" data-container="body" data-placement="top" data-original-title="Total Amount only in Proof List Reports & Daily Cash Receipt Book" name="acc-def-amount" id="acc-def-amount" {{ !empty($data->TotalAmountOnly) ? 'checked' : '' }}>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Account Name</label>
                    <input type="text" class="form-control" name="acc-name" id="acc-name" value="{{ !empty($data->AcctName) ? $data->AcctName : '' }}">
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Short Name</label>
                    <input type="text" class="form-control" name="acc-sname" id="acc-sname" value="{{ !empty($data->ShortName) ? $data->ShortName : '' }}">
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Fund / Group</label>
                    <div class="input-group">
                        <input type="text" class="form-control inp-with-attr" name="acc-group" id="acc-group" value="{{ $fgcode }}" data-id="{{ !empty($data->GroupID) ? encode($data->GroupID) : '' }}" readonly>
						<span class="input-group-btn">
						    <button class="btn blue search-show-modal" type="button" data-search="fund-group"><i class="fa fa-search"></i></button>
						</span>
					</div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Sub Fund</label>
                    <div class="input-group">
                        <input type="text" class="form-control inp-with-attr" name="acc-sub-fund" id="acc-sub-fund" value="{{ $sfcode }}" data-id="{{ !empty($data->SubFundID) ? encode($data->SubFundID) : '' }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn blue search-show-modal" type="button" data-search="subfund"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Sub Unit</label>
                    <div class="input-group">
                        <input type="text" class="form-control inp-with-attr" name="acc-unit" id="acc-unit" value="{{ $sucode }}" data-id="{{ !empty($data->SubUnitID) ? encode($data->SubUnitID) : '' }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn blue search-show-modal" type="button" data-search="subunit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Resource Center / Department</label>
                    <div class="input-group">
                        <input type="text" class="form-control inp-with-attr" name="acc-dept" id="acc-dept" value="{{ $dept_code }}" data-id="{{ !empty($data->DeptID) ? encode($data->DeptID) : '' }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn blue search-show-modal" type="button" data-search="src-dept"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Classification</label>
                    <div class="input-group">
                        <input type="text" class="form-control inp-with-attr" name="acc-classif" id="acc-classif" value="{{ $clcode }}" data-id="{{ !empty($data->ClassID) ? encode($data->ClassID) : '' }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn blue search-show-modal" type="button" data-search="acc-classif"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Type</label>
                    <div class="input-group">
                        <input type="text" class="form-control inp-with-attr" name="acc-type" id="acc-type" value="{{ $tcode }}" data-id="{{ !empty($data->CategoryID) ? encode($data->CategoryID) : '' }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn blue search-show-modal" type="button" data-search="acc-type"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Account Options</label>
                    <select class="select2me form-control select2-offscreen" name="acc-options" id="acc-options">
                        @forelse($acc_opt as $k => $a)
                        <option value="{{ encode($a->AcctOptionID) }}" {{ $a->AcctOptionID == $ao ? 'selected' : '' }} >{{ $a->AcctOptionName }}</option>
                        @empty
                        <option value="">Not Available</option>
                        @endforelse
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Payment Options</label>
                    <select class="select2me form-control select2-offscreen" name="acc-pay-options" id="acc-pay-options">
                        @forelse($pay_opt as $p)
                        <option value="{{ encode($p->PaymentOptionID) }}" {{ $p->PaymentOptionID == $apo ? 'selected' : '' }} >{{ $p->PaymentOptionName }}</option>
                        @empty
                        <option value="">Not Available</option>
                        @endforelse
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
		<div class="row">
			<div class="col-md-12">
                <div class="right">
                    <button type="button" class="btn default" id="cancel-acct-form">Back</button>
                    <button type="submit" class="btn green" id="submit-acct-form">Submit</button>
                </div>
			</div>
		</div>
	</div>
</form>
