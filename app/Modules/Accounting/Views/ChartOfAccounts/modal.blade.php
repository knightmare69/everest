<?php $a = ['par-acc' => 'account', 'fund-group' => 'group', 'subfund' => 'sfund', 'subunit' => 'sunits', 'src-dept' => 'dept', 'acc-classif' => 'classif', 'acc-type' => 'types']; ?>
<div class="tabbable-line">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-rec" data-toggle="tab">Record(s)</a>
        </li>
        @if($with_form)
        <li>
            <a href="#tab-form" data-toggle="tab">Form <span id="acct-form-lbl-actn">({{ $action }})</span></a>
        </li>
        @endif
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="tab-rec">
            @include($views.'tbls.'.$a[$form])
        </div>
        @if($with_form)
        <div class="tab-pane fade" id="tab-form">
            @include($views.'forms.modal-form', ['form_type' => $a[$form]])
            <div class="form-actions">
        		<div class="row">
        			<div class="col-md-12">
                        <div class="right">
                            <!-- <button type="button" class="btn default">Cancel</button> -->
                            <button type="submit" class="btn green" id="modal-submit-btn">Submit</button>
                        </div>
        			</div>
        		</div>
        	</div>
        </div>
        @endif
    </div>
</div>
