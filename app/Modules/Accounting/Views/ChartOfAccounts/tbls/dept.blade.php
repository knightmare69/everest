<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="dept-res-tbl">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
            @foreach($data as $k => $d)
            <tr>
                <td>{{ $k + 1 }}</td>
                <td><a href="#" class="a-acct" data-id="{{ encode($d->DeptID) }}">{{ $d->DeptCode }}</a></td>
                <td>{{ $d->DeptName }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
