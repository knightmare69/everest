<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="subfund-res-tbl">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
            @foreach($data as $k => $d)
            <tr>
                <td>{{ $k + 1 }}</td>
                <td><a href="#" class="a-acct" data-id="{{ encode($d->SubGroupID) }}">{{ $d->SubGroupCode }}</a></td>
                <td>{{ $d->SubGroupName }}</td>
                <td>{{ $d->SubGroupShort }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
