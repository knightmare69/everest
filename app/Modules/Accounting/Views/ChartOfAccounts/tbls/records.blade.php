<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="cot-res-table">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
            <th>Parent Account Code</th>
            <th>Amount</th>
            <th>Fund/Group</th>
            <th>Sub Fund</th>
            <th>Sub Unit</th>
            <th>Res. Center / Dept.</th>
            <th>Classification</th>
            <th>Type</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
