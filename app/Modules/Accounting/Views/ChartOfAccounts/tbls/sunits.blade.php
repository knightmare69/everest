<table class="table table-striped table-bordered table-hover table_scroll dataTable" id="subunits-res-tbl">
    <thead>
        <tr>
            <th>#</th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($data))
            @foreach($data as $k => $d)
            <tr>
                <td>{{ $k + 1 }}</td>
                <td><a href="#" class="a-acct" data-id="{{ encode($d->SubUnitID) }}">{{ $d->SubUnitCode }}</a></td>
                <td>{{ $d->SubUnitName }}</td>
                <td>{{ $d->SubUnitShort }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
