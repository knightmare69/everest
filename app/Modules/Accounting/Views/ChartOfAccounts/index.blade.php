<style >
    .dropdown-menu li.active a{
        background-color: #2977f7 !important;
        color: #fff;
    }
    .dropdown-menu li.active:hover a{
        color: #fff;
    }
    .dataTables_filter .form-control {
        margin-left: 0;
    }
</style>
<div class="row" id="acc-tbl-rec-holder">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption"> <i class="fa fa-table"></i> Chart of Accounts</div>
            </div>
            <div class="portlet-body">
                @include($views.'tbls.records')
            </div>
        </div>
    </div>
</div>
<div id="rep-inpt-search" class="hide">
    <div class="btn-group btn-group-solid" style="float: right">
		<a class="btn blue btn-acc-search" data-toggle="modal" href="#search-modal"><i class="fa fa-search"></i> Search</a>
        <a class="btn green-haze btn-tblr-refresh"><i class="fa fa-refresh"></i> Refresh</a>
		<a class="btn green btn-acc-new"><i class="fa fa-file"></i> New</a>
	</div>
</div>
<div class="modal fade bs-modal-sm" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-bg">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Search Options</h4>
			</div>
			<div class="modal-body">
                <div class="alert alert-info" style="margin-bottom:0">
					Tick the corresponding checkbox to find specific value.
				</div>
                @include($views.'forms.modal-search')
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="button" class="btn blue" id="find-search-opt">Find</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="row hide" id="acc-form-holder" style="margin-top: 40px;">
    <div class="col-md-8">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-archive"></i> Chart of Accounts Form
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body form" id="form-holder"></div>
        </div>
    </div>
</div>
