<?php $data = isset($data) ? $data : array(); ?>
<!-- BEGIN FORM-->
<form class="horizontal-form" id="form-discount" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Search Student <small><i class="text-danger">*</i></small></label>
                    <div class="input-group">
                        <input type="text" class="form-control select2" name="reg" id="reg">
                        <span class="input-group-addon cursor-pointer" id="showDiscounts">
                            <i class="fa fa-file"></i> Show Discounts
                        </span>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Transaction <small><i class="text-danger">*</i></small></label>
                    <select class="form-control" name="transactiion" id="transactiion">
                        <option value="1">Enrollment</option>
                        <option value="2">Add/Drop/Change Subject</option>
                        <option value="3">Beginning Balance</option>
                    </select>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Campus <small><i class="text-danger">*</i></small></label>
                    <select class="form-control" name="campus" id="campus">
                        @foreach($campus as $row)
                        <option value="{{ $row->CampusID }}">{{ $row->ShortName }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label">Date Entry</label>
                <div class="input-group date form_datetime" data-date="{{ date('m/d/Y H:i') }}">
                    <input type="text" size="16" readonly name="date_entry" id="date_entry" class="form-control" value="{{ date('m/d/Y H:i') }}">
                    <span class="input-group-btn">
                    <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                    <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
                <!-- /input-group -->
            </div>
            <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Academic Year <small><i class="text-danger">*</i></small></label>
                    <select class="form-control" name="acadyear" id="acadyear">
                        @foreach($terms as $row)
                        <option {{ $row->Active_OnlineEnrolment == '1' ? 'selected' : '' }} value="{{ $row->TermID }}">{{ $row->AcademicYear.' '.$row->SchoolTerm }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Provider <small><i class="text-danger">*</i></small></label>
                    <select class="form-control" name="provider_type" id="provider_type">
                        <option value="">-- SELECT --</option>
                        <option value="1">Discount</option>
                        <option value="2">Gurantor</option>
                    </select>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">&nbsp;</label>
                   <div class="input-group">
                        <input type="text" placeholder='Search Provider' readonly name="provider_name" id="provider_name" class="form-control custom-input-sm" value="">
                        <input type="hidden" placeholder='' readonly name="provider" id="provider" class="form-control custom-input-sm" value="">
                        <span class="input-group-addon cursor-pointer" id="searchProvider">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">
                        <input type="checkbox" class="form-control" name="applied_net_assessed" id="applied_net_assessed">
                        Applies on net assessed
                    </label>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group" style="margin-bottom: 5px;">
                        <input type="text" placeholder='0.0' name="applied_amount" id="applied_amount" class="form-control custom-input-sm not-required" value="">
                        <span class="input-group-addon">
                            %
                        </span>
                    </div>
                    <label><input type="radio" class="form-control" name="applied_type" id="amount"> AMOUNT</label>
                    <label><input type="radio" class="form-control" name="applied_type" id="rate"> RATE (%)</label>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        <small><i class="text-danger">*</i></small> Scholarship by Account
                        <input type="checkbox" class="form-control" name="scholar_account" id="scholar_account">
                    </label>
                    <select class="form-control" name="scholarship" id="scholarship">
                        <option value="">-- SELECT --</option>
                        <option value="1">Half/Full Scholar on Tuition Fee</option>
                        <option value="2">Half/Full Scholar in all Accounts</option>
                        <option value="3">Alloted Amount</option>
                        <option value="4">Remaining Balance</option>
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">
                        Scholars Grant Template
                        <input type="checkbox" class="form-control" name="scholar_grant" id="scholar_grant">
                    </label>
                    <select class="form-control not-required" name="template" id="template">
                        <option value="">-- SELECT --</option>
                    </select>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label><input type="radio" class="form-control" name="bill_to" id="amount"> Auto Credit </label>
                    <label><input type="radio" class="form-control" name="bill_to" id="rate"> Bill To</label>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
    <hr style="margin-top: 3px; margin-bottom: 10px;" />
    <div class="form-actions right  ">
        <button class="btn default" type="reset">Reset</button>
        <button class="btn blue btn_action btn-save-discount" type="button"><i class="fa fa-check"></i> Save</button>
    </div>
</form>
<!-- END FORM-->