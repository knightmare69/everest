<table class="table table-striped table-bordered table-condensed table-hover font-xs ">
	<thead>
		<tr>
			<th>Action</th>
			<th>TYPE</th>
			<th>PROVIDER NAME</th>
			<th>TOTAL DISCOUNT</th>
			<th>PAYMENT</th>
			<th>CREDITED</th>
			<th>REMARKS</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr data-id="{{ $row->RefNo }}">
			<td><a href="javascript:;" class="btn btn-discount-remove btn-danger btn-sm">Delete</a></td>
			<td>Scho.Provider</td>
			<td>{{ $row->ProvName }}</td>
			<td>{{ $row->TotalDiscount }}</td>
			<td>0.0</td>
			<td></td>
			<td></td>
		</tr>
		@endforeach
	</tbody>
	@if(count($data) <= 0)
	<tfoot>
		<tr>
			<td colspan="7">No records found.</td>
		</tr>
	</tfoot>
	@endif
</table>