<?php $table = isset($table) ? $table : array(); ?>
<table class="table table-striped table-bordered table-hover table_scroll">
    <thead>
        <tr>
            <th><input type="checkbox" class="chk-header"></th>
            <th>Name</th>
            <th>Description</th>
            <th width="10">Status</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->GroupID) }}"  data-status="{{ $row->Inactive }}" class="without-bg">
            <td class="autofit"><input type="checkbox" class="chk-child"></td>
            <td><a href="javascript:void(0)" class="a_select">{{ $row->GroupName  }}</a></td>
            <td>{{ $row->GroupDesc }}</td>
            <td class="center"><?= !$row->Inactive ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
        </tr>
    @endforeach
    </tbody>
</table>