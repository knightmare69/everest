<div class="portlet blue box dvlist tabbable tabs-left" style="font-size:10px;">
	<div class="portlet-title">
	 <ul class="nav nav-tabs pull-left">
	   <li class="active"><a href="#tab_1" data-toggle="tab">Student</a></li>
	   <li class=""><a href="#tab_2" data-toggle="tab">Employee</a></li>
	   <li class=""><a href="#tab_3" data-toggle="tab">Scho. Provider</a></li>
	   <li class=""><a href="#tab_4" data-toggle="tab">Other Payor</a></li>
	 </ul>
	 <div class="pull-right" style="padding-top:8px;">
	    <div class="btn-group">
	      <button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-print"></i> Print</button>
		  <ul class="dropdown-menu pull-right" role="menu">
			<li><a href="javascript:;" class="btn-print-assess"><i class="fa fa-file"></i> Assessment</a></li>
			<li><a href="javascript:;" class="btn-print-soa"><i class="fa fa-file"></i> S.O.A</a></li>
		  </ul>											
		</div>
	 </div>  
	</div>
	<div class="portlet-body">
	 <div class="row">
	   <div class="col-sm-3 col-md-2">
	    <div style="border:1px solid black;">
	     <img class="entryphoto" width="100%;" height="200%;" src="<?php echo url('assets/system/media/images/no-image.png').'?'.rand(0,9999);?>"/>
		</div>
	   </div>
	   <div class="col-sm-9 col-md-10">
         <div class="tab-content">
           <div class="tab-pane fade active in" id="tab_1">
		    <div class="col-sm-3 col-sm-offset-2 col-lg-offset-1">StudentNo:<b class="studno" data-stats="1"></b></div>
		    <div class="col-sm-3">YearLevel:<b class="yrlvl"></b></div>
		    <div class="col-sm-4 col-lg-5">Acad Program:<b class="prog"></b></div>
		    <div class="col-sm-4 col-md-2 col-lg-1"><label>Name:</label></div>
			<div class="col-sm-8 col-md-10 col-lg-11">
			  <div class="input-group">
			    <input type="text" class="form-control" id="studname" name="studname" data-button=".btnfilter"/>
				<span class="input-group-addon btnfilter" data-source="student" data-parameter="#studname" data-id=".studno"><i class="fa fa-search"></i></span>
			  </div>
			</div>
		   </div>
         </div>
		 <div class="col-sm-12"><br/></div>
		 <div class="col-sm-6">
		   <div class="well">
		     <div class="row">			  
			   <div class="form-group">
				 <label class="col-sm-3" style="font-size:10px;">Transaction:</label>
				 <div class="col-sm-9">
					<select class="form-control input-sm" id="transtype" name="transtype">
					   <option value="1">Enrollment</option>
					</select>
				 </div>
			   </div>
			   <div class="form-group">
				 <label class="col-sm-3" style="font-size:10px;">Reference:</label>
				 <div class="col-sm-6">
					<select class="form-control input-sm" id="refno" name="refno">
					</select>
				 </div>
				 <button type="button" class="btn btn-sm btn-primary btn-refno-refresh">Refresh</button>
			   </div> 
			 </div>
		   </div>
		 </div>
		 <div class="col-sm-6">
		   <div class="well">
		     <div class="row">
			   <div class="col-sm-12"><label style="font-size:9px;">AY TERM:<b class="termid" data-termid="0"></b></label></div>
			   <div class="col-sm-12"><label style="font-size:9px;">REG DATE:<b class="regdate"></b></label></div>
			   <div class="col-sm-12"><label style="font-size:9px;">VALIDATED:<b class="validate"></b></label></div>
			   <div class="col-sm-12"><label style="font-size:9px;">REMARKS:<b class="remarks"></b></label></div>
			 </div>
		   </div>
		 </div>
		 <div class="col-sm-12 hidden" style="padding:2px;">
		     <button class="btn btn-xs btn-default btn-print-assess"><i class="fa fa-print"></i> Print Assessment</button>
		 </div>
	   </div>
     </div>
     <div class="well" style="padding:5px;background-color:#f9f9f9;">
	   	 <ul class="nav nav-tabs">
           <li class="active"><a href="#tab_a1" data-toggle="tab">Assessment</a></li>
           <li class=""><a href="#tab_a2" data-toggle="tab">Discounts</a></li>
           <li class=""><a href="#tab_a3" data-toggle="tab">Other Fees</a></li>
           <li class=""><a href="#tab_a4" data-toggle="tab">Enrollment</a></li>
		 </ul>  
         <div class="tab-content">
           <div class="tab-pane fade active in" id="tab_a1">
			   <div class="table-responsive">
			   <table class="table table-bordered table-condense table-hover" id="tblassess" style="white-space:nowrap;">
				 <thead>
				   <th style="font-size:9px !important;">CODE</th>
				   <th style="font-size:9px !important;">ACCOUNT</th>
				   <th style="font-size:9px !important;">ASSESSED</th>
				   <th style="font-size:9px !important;">DISCOUNT</th>
				   <th style="font-size:9px !important;">DISCOUNT ON NET</th>
				   <th style="font-size:9px !important;">NET ASSESS</th>
				   <th style="font-size:9px !important;">1ST INSTMNT</th>
				   <th style="font-size:9px !important;">2NR INSTMNT</th>
				   <th style="font-size:9px !important;">3RD INSTMNT</th>
				   <th style="font-size:9px !important;">4TH INSTMNT</th>
				   <th style="font-size:9px !important;">PAYMENT DISCOUNT</th>
				   <th style="font-size:9px !important;">ACTUAL PAYMENT</th>
				   <th style="font-size:9px !important;">CREDIT MEMO</th>
				   <th style="font-size:9px !important;">DEBIT REFUND</th>
				   <th style="font-size:9px !important;">BALANCE</th>
				   <th style="font-size:9px !important;">REMARKS</th>
				   <th style="font-size:9px !important;">DMCM REFNO</th>
				   <th style="font-size:9px !important;">ENTRY ID</th>
				   <th style="font-size:9px !important;">INSTALLMENT</th>
				   <th style="font-size:9px !important;">SPCL. FEE</th>
				   <th style="font-size:9px !important;">SEQ. NO</th>
				   <th style="font-size:9px !important;">5TH INSTMNT</th>
				   <th style="font-size:9px !important;">6TH INSTMNT</th>
				   <th style="font-size:9px !important;">7TH INSTMNT</th>
				   <th style="font-size:9px !important;">8TH INSTMNT</th>
				   <th style="font-size:9px !important;">9TH INSTMNT</th>
				   <th style="font-size:9px !important;">10TH INSTMNT</th>
				 </thead>
				 <tbody>
				  <tr>
					<td class="text-center" colspan="21"><i class="fa fa-warning"></i> No Data Available</td>
				  </tr>
				 </tbody>
				 <tfoot class="info totalrow">
				   <td colspan="2" style="text-align:right;font-size:10px !important;">TOTAL:</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td class="text-right" style="font-size:10px !important;" class="totalsum">0.00</td>
				   <td style="font-size:10px !important;"></td>
				   <td style="font-size:10px !important;"></td>
				   <td style="font-size:10px !important;"></td>
				   <td style="font-size:10px !important;"></td>
				   <td style="font-size:10px !important;"></td>
				   <td style="font-size:10px !important;"></td>
				 </tfoot>
			   </table>
			   </div>
			   <div class="alert alert-warning" style="margin-bottom:2px;">
			     Template:<b class="templname" data-id="0"></b>
			   </div>
			   <div class="">
				<button class="btn btn-xs btn-primary"><i class="fa fa-folder"></i> File Location</button>
				<button class="btn btn-xs btn-success btn-tblfee"><i class="fa fa-table"></i> Table Of Fees</button>
				<button class="btn btn-xs btn-warning btn-balance"><i class="fa fa-bar-chart-o"></i> Outstanding Balance</button>
				<button class="btn btn-xs btn-warning btn-recalc"><i class="fa fa-tachometer"></i> Recalculate</button>
			   </div>
			   <div class="row" style="text-align:right">
				  <div class="col-sm-3 col-md-2 col-md-offset-4"><span><h4 class="netassess">0.00</h4> NET ASSESSMENT<span></div>
				  <div class="col-sm-3 col-md-2"><span><h4 class="totalpayment">0.00</h4> TOTAL PAYMENT<span></div>
				  <div class="col-sm-3 col-md-2"><span><h4 class="totalbalance">0.00</h4> TOTAL BALANCE<span></div>
				  <div class="col-sm-3 col-md-2"><span><h4 class="totaloutbalance">0.00</h4> Outstanding Balance</span></div>
			   </div>
		   </div>
           <div class="tab-pane fade" id="tab_a2">
		    <div class="row">
			  <div class="col-sm-12">
			     <button type="button" class="btn btn-xs btn-success btn-discount-new"><i class="fa fa-file"></i> New</button>
			     <button type="button" class="btn btn-xs btn-warning btn-discount-save"><i class="fa fa-save"></i> Save</button>
			     <button type="button" class="btn btn-xs btn-danger btn-discount-delete"><i class="fa fa-trash-o"></i> Delete</button>
			     <button type="button" class="btn btn-xs btn-info btn-discount-refresh"><i class="fa fa-refresh"></i> Refresh</button>
				 <br/><br/>
			  </div>
			  <div class="col-sm-12">
			    <div class="col-sm-2">
				  Provider:
				  <select class="form-control" id="provider" name="provider">
				    <option value="1">Discount</option>
				    <option value="1">Scholarship</option>
				    <option value="2">Guarantor</option>
				  </select>
				</div>
			    <div class="col-sm-4">
				  <br/>
				  <div class="input-group">
				    <input type="hidden" id="scholprovid" name="scholprovid" />
					<input type="text" class="form-control" id="scholprovider" name="scholprovider" data-id="0" data-button=".btnfilter"/>
					<span class="input-group-addon btnfilter" data-source="scholarship" data-parameter="#scholprovider" data-id="#scholprovid"><i class="fa fa-search"></i></span>
				  </div>
				</div>
			    <div class="col-sm-6">
				  <br/>
				  <select class="form-control" id="schoOption" name="schoOption">
						<option value="1">By Account</option>
						<option value="2">Grant Template</option>
						<option value="3">Custom</option>
				  </select>
				</div>
			  </div>	
			  <div class="col-sm-12">
				<div class="col-sm-6">
				  <br/>
				  <select class="form-control" id="application" name="application">
				    <option value="0">Half/Full Scholar On Tuition</option>
				    <option value="1">Half/Full Scholar On All Accounts</option>
				    <option value="2">Allotted Amount</option>
				    <option value="3">Remaining Balance</option>
				  </select>
				  <select class="form-control hidden" id="template" name="template">
				    <option value="-1">-SELECT ONE-</option>
				  </select>
				</div>	
			    <div class="col-sm-6">
				  <br/>
				  <div class="input-group"> 
				   <input type="text" class="form-control numberonly text-align-right" id="scholamount" name="scholamount" value="0.00"/>
				   <span class="input-group-btn">
				    <div class="btn-group">
				     <button id="scholamttype" class="btn btn-success dropdown-toggle" type="button" data-value="0" data-toggle="dropdown" aria-expanded="true">Perc.</button>
					 <ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(0);" data-btntarget="#scholamttype" data-label="Perc." data-value="0">Percentage</a></li>
						<li><a href="javascript:void(0);" data-btntarget="#scholamttype" data-label="Amt." data-value="1">Amount</a></li>
					 </ul>						
					</div>
				   </span>
				  </div>
				  <div class="optgrant text-align-right hidden">
				    <div class="col-sm-6 radio-list">
						<label class="radio-inline"><div class="radio" id="opttemplateb"><input name="optgranttempl" id="optgrantbyrate" value="0" type="radio" checked></div> Rate</label>
						<label class="radio-inline"><div class="radio" id="opttemplatea"><input name="optgranttempl" id="optgrantbyamount" value="1" type="radio"></div> Amount</label>
					</div>
                    <div class="col-sm-6">
					    <input type="text" class="form-control text-right numberonly" id="customdisc" name="customdisc" value="0.00"/>
                    </div>					
				  </div>
				</div>
			  </div>
			  <div class="col-sm-12" style="margin-bottom:10px;">
				<div class="col-sm-6">
				  <br/>
				  <div class="checkbox-list">
				    <label class="checkbox-inline"><input type="checkbox" id="applyNet" name="applyNet" checked/> Apply To Net</label>
				    <label class="checkbox-inline"><input type="checkbox" id="applyTotal" name="applyTotal"/> Apply To Total</label>
				  </div>	 
				</div>
				<div class="col-sm-6">
				  <br/>
				  <div class="optgrant hidden">
				  </div>	 
				</div>
			  </div>	
			</div>
		    <div class="table-responsive">
			  <table class="table table-bordered table-condense table-hover" id="tbldiscount">
			    <thead> 
				   <th>Type</th>
				   <th>Provider</th>
				   <th>Template</th>
				   <th>Total Discount</th>
				   <th>Percentage</th>
				   <th>Apply On Net</th>
				   <th>Remarks</th>
				</thead>
				<tbody>
				</tbody>
			  </table>
		   </div>
		 </div>
         <div class="tab-pane fade dvothertxn" id="tab_a3">
			<div class="row">
			    <div class="col-sm-12">
				  <button class="btn btn-xs btn-warning btn-other-save"><i class="fa fa-save"></i> Save</button>
				  <button class="btn btn-xs btn-danger btn-other-clear"><i class="fa fa-refresh"></i> Clear</button>
				  <br/><br/>
				</div>
				<div class="col-sm-12">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_1_1" data-toggle="tab" aria-expanded="true">
							Car Sticker </a>
						</li>
						<li class="">
							<a href="#tab_1_2" data-toggle="tab" aria-expanded="false">
							Uniform </a>
						</li>
						<li class="">
							<a href="#tab_1_3" data-toggle="tab" aria-expanded="false">
							Summer Program </a>
						</li>
						<li class="" data-grdmin="9" data-grdmax="9">
							<a href="#tab_1_4" data-toggle="tab" aria-expanded="false">
							Grad.Package </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="tab_1_1"  data-target="sticker"  data-grdmin="1" data-grdmax="13">
						  <?php echo (string)view('Accounting.Views.sub.otherfees.carstick',array());?>
						</div>
						<div class="tab-pane fade" id="tab_1_2" data-target="uniform" data-grdmin="1" data-grdmax="13">
						  <div class="" data-grdmin="1" data-grdmax="9"><?php echo (string)view('Accounting.Views.sub.otherfees.uniforma',array());?></div>
						  <div class="" data-grdmin="10" data-grdmax="13"><?php echo (string)view('Accounting.Views.sub.otherfees.uniformb',array());?></div>
						</div>
					    <div class="tab-pane fade" id="tab_1_3" data-target="summer" data-grdmin="1" data-grdmax="13">
						  <?php echo (string)view('Accounting.Views.sub.otherfees.summer',array());?>
					    </div>	
					    <div class="tab-pane fade" id="tab_1_4" data-target="graduate"  data-grdmin="9" data-grdmax="9">
					      <?php echo (string)view('Accounting.Views.sub.otherfees.graduate',array());?>
					    </div>					
					</div>  
				</div>
				<div class="col-sm-12">
				  <div class="table-responsive">
					<table class="table table-bordered table-condense">
					  <tbody>
						<tr><td>Total Car Sticker:</td><td class="text-right" data-source="sticker">0.00</td></tr>  
						<tr><td>Total Uniform:</td><td class="text-right" data-source="uniform">0.00</td></tr>
						<tr><td>Total Summer Program:</td><td class="text-right" data-source="summer">0.00</td></tr>
						<tr><td>Total Grad. Package:</td><td class="text-right" data-target="graduate">0.00</td></tr>
					  </tbody>
					</table>
				  </div>
				</div>	
			</div>		   
         </div>		   
     </div>	 
	 </div>
	 <div class="portlet-footer">
	 </div>
</div>
<div id="modal_template" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select a template</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="radio-list" style="margin-bottom:10px;">
				        <br/>
						<label class="radio-inline"><div class="radio" id="optschemea"><input name="optscheme" id="optschemea" value="-1" type="radio" checked></div> All</label>
						<label class="radio-inline"><div class="radio" id="optschemeb"><input name="optscheme" id="optschemeb" value="0" type="radio"></div> Full Payment</label>
						<label class="radio-inline"><div class="radio" id="optschemec"><input name="optscheme" id="optschemec" value="1" type="radio"></div> Semestral</label>
						<label class="radio-inline"><div class="radio" id="optschemed"><input name="optscheme" id="optschemed" value="2" type="radio"></div> Quarterly</label>
						<label class="radio-inline"><div class="radio" id="optschemee"><input name="optscheme" id="optschemee" value="3" type="radio"></div> Monthly</label>
				  </div>
				  <div class="table-responsive modal_template_list" style="max-height:400px; overflow:scroll;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>TemplateID</th>
						    <th>TemplateCode</th>
						</thead>
						<tbody>
						 <tr><td class="text-center" colspan="2"><i class="fa fa-warning"></i> No data available</td></tr>
						</tbody>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-templ-select">Select</button>
				</div>
			</div>
		</div>
	</div>	
</div>
<div id="modal_filter" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false" data-type="student">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Select from the list</h4>
			</div>
			<div class="modal-body">
				<div class="portlet-body form">
				  <div class="input-group">
				    <input class="form-control" id="txt-modal-search" data-button=".btn-modal-search" placeholder="StudentNo, LastName, Firstname" type="text">
					<span class="input-group-addon btn-modal-search" data-source="#txt-modal-search"><i class="fa fa-search"></i></span>
				  </div>
				  <div class="table-responsive modal_list" style="max-height:400px; overflow:scroll;">
				     <table class="table table-bordered table-condense">
					    <thead>
						    <th>Last Name</th>
						    <th>First Name</th>
						    <th>Middle Name</th>
						    <th>Program</th>
						    <th>Level</th>
						</thead>
					 </table>
				  </div>
				</div>
				<div class="modal-footer">
				   <button type="button" class="btn btn-sm btn-primary btn-modal-select">Select</button>
				</div>
			</div>
		</div>
	</div>	
</div>