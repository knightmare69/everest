<?php

namespace App\Modules\Accounting\Services\Validation;

use Validator;

class FeesTemplateValidation
{
    public function fee_details($post)
    {
        
        $validator = validator::make(
            [
                'TemplateID' => getObjectValue($post, 'template'),
                'AccountID' => getObjectValue($post, 'acct'),
                'Amount' => getObjectValue($post, 'amt')
            ],
            [
                'TemplateID' => 'required|string',
                'AccountID' => 'required|integer',
                'Amount' => 'required|numeric'
            ]
        );

        return $validator;
    }

    public function template($post)
    {
         $validator = validator::make(
            [
                'TermID' => getObjectValue($post, 'term')
                //,'campus' => getObjectValue($post, 'campus')
                ,'TemplateCode' => getObjectValue($post, 'code')
                ,'TemplateDesc' => getObjectValue($post, 'desc')
            ],
            [
                'TermID' => 'required|string'
               //,'campus' => 'required|integer'
               ,'TemplateCode' => 'required|string'
               ,'TemplateDesc' => 'required|string'
            ]
        );

        return $validator;
    }
    
    public function saveAs($post)
    {
         $validator = validator::make(
            [
                'TermID' => getObjectValue($post, 'term')
                ,'campus' => getObjectValue($post, 'campus')
                ,'TemplateCode' => getObjectValue($post, 'code')
                ,'TemplateDesc' => getObjectValue($post, 'desc')
            ],
            [
                'TermID' => 'required|string'
               ,'campus' => 'required|string'
               ,'TemplateCode' => 'required|string|unique:ES_TableofFees'
               ,'TemplateDesc' => 'required|string'
            ]
        );

        return $validator;
    }
    
    #dmcm validation
    public function dmcm_master($post)
    {
         $validator = validator::make(
            [
                'IDNo' => getObjectValue($post, 'idno')
                ,'Explanation' => getObjectValue($post, 'explanation')               
            ],
            [
                'IDNo' => 'required|string'
               ,'Explanation' => 'required|string'
            ]
        );

        return $validator;
    }
        
}
