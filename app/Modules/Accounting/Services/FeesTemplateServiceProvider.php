<?php

namespace App\Modules\Accounting\Services;
use App\Modules\Accounting\Services\Validation\FeesTemplateValidation;
use App\Modules\Accounting\Models\TableOfFees as tModel;
use App\Modules\Accounting\Models\TableOfFees_Details as fModel;
use App\Modules\Accounting\Services\AccountingServiceProvider as sFinance;

use DB;
use Request;

class FeesTemplateServiceProvider
{
    public function __construct()
	{
		$this->validation = new FeesTemplateValidation;
	}

    public function saveAs(){
        $p  = Request::all();

        $old_id = decode($p['idx']);
        $old = tModel::where('TemplateID', $old_id )->first();

        $data= array(
               'TemplateCode' => getObjectValue($p,'code')
              ,'TemplateDesc' => getObjectValue($p,'desc')
              ,'TemplateRemarks' => getObjectValue($p,'remarks')
              ,'TermID' => (getObjectValue($p,'term'))
              ,'CampusID' => getObjectValueWithReturn($p,'campus','1')
              ,'LastModified' => systemDate()
              ,'Modifiedby' => getUserID()
              ,'ForForeign' => getObjectValue($p,'foreign')
              ,'TransType' => $old->TransType
              ,'CurrencyID'  => $old->CurrencyID
              ,'StudentStatus' => $old->StudentStatus
              ,'StudentGender' => $old->StudentGender
              ,'PaymentScheme' => $old->PaymentScheme
              ,'PaymentOption' => $old->PaymentOption
              ,'TotalAmount' => $old->TotalAmount
        );

        tModel::create($data);

        $new_template = tModel::where(['TemplateCode'=> getObjectValue($p,'code'), 'TermID' => getObjectValue($p,'term') ])->first();

        /*Save as OLD FEES to New*/
        $qry = "Insert into es_tableoffee_details (TemplateID, [AccountID],[Amount],[Remarks],[ChargeMode],[Inactive],[CurrencyID],[Succeeding],[SeqNo],[Option],[Option_Session],[Option_ProgID],[Option_Yr],
                [1stPayment],[2ndPayment],[3rdPayment],[4thPayment],[5thPayment],[6thPayment],[7thPayment],[8thPayment],[9thPayment],[10thPayment],
                [NumPayment],[CustomPaymentType] ) select ".$new_template->TemplateID.",[AccountID],[Amount],[Remarks],[ChargeMode],[Inactive],[CurrencyID],[Succeeding],[SeqNo],[Option],[Option_Session],[Option_ProgID],[Option_Yr],
                [1stPayment],[2ndPayment],[3rdPayment],[4thPayment],[5thPayment],[6thPayment],[7thPayment],[8thPayment],[9thPayment],[10thPayment],
                [NumPayment],[CustomPaymentType]
                FROM es_tableoffee_details where TemplateID = '".$old_id."'";

        DB::statement($qry);

        /*Save as OLD Program Distribution to New*/
        $qry = "Insert into es_tableoffee_distributionlist (TemplateID,ProgID,YearLevelID,MajorID) select ".$new_template->TemplateID.",ProgID,YearLevelID,MajorID
                FROM es_tableoffee_distributionlist where TemplateID = '".$old_id."'";
        DB::statement($qry);

        return true;

    }

    public function recompute_totalfees($tempate){

        $rs = fModel::selectRaw("SUM(Amount) As Total
								,SUM([1stPayment]) AS Col1
								,SUM([2ndPayment]) AS Col2
								,SUM([3rdPayment]) AS Col3
								,SUM([4thPayment]) AS Col4
								,SUM([5thPayment]) AS Col5
								,SUM([6thPayment]) AS Col6
								,SUM([7thPayment]) AS Col7
								,SUM([8thPayment]) AS Col8
								,SUM([9thPayment]) AS Col9
								,SUM([10thPayment]) AS Col10
							")
                    ->where('TemplateID', $tempate)
                    ->first();


        $data = array(
                'TotalAmount'=> $rs->Total
               ,'Total1stPayment'=> $rs->Col1
               ,'Total2ndPayment'=> $rs->Col2
               ,'Total3rdPayment'=> $rs->Col3
               ,'Total4thPayment'=> $rs->Col4
               ,'Total5thPayment'=> $rs->Col5
               ,'Total6thPayment'=> $rs->Col6
               ,'Total7thPayment'=> $rs->Col7
               ,'Total8thPayment'=> $rs->Col8
               ,'Total9thPayment'=> $rs->Col9
               ,'Total10thPayment'=> $rs->Col10
        );

        tModel::where('TemplateID', $tempate)->update($data);
        return true;
    }

   	public function isValid($post,$action = 'template')
	{
	    if($action =='template'){
	       $validate = $this->validation->template($post);
	    }elseif($action =='fees'){
           $validate = $this->validation->fee_details($post);
        }elseif($action =='save-as'){
           $validate = $this->validation->saveAs($post);
	    }

    	if ($validate->fails())
    	{
    		return ['error'=>true,'message'=>getErrorMessages($validate->errors()->getMessages())];
    	}
		return ['error'=>false,'message'=> ''];
	}

   	public function set_template_data($post)
	{

		return [
		       'TemplateCode' => getObjectValue($post,'code')
              ,'TemplateDesc' => getObjectValue($post,'desc')
              ,'TemplateRemarks' => getObjectValue($post,'remarks')
              ,'TermID' => decode(getObjectValue($post,'term'))
              ,'CampusID' => getObjectValueWithReturn($post,'campus','1')
              ,'LastModified' => systemDate()
              ,'Modifiedby' => getUserID()
              ,'ForForeign' => getObjectValue($post,'foreign')
              ,'TransType' => getObjectValue($post,'type')
              ,'CurrencyID'  => getObjectValue($post,'curr')
              ,'StudentStatus' => getObjectValue($post,'status')
              ,'StudentGender' => getObjectValue($post,'gender')
              ,'PaymentScheme' => getObjectValue($post,'scheme')
              ,'PaymentOption' => getObjectValue($post,'option')
			];
	}

    public function set_fees_data($post)
	{
	    $fee_id = decode(getObjectValue($post,'template'));
        $acctid = getObjectValue($post,'acct');
        $template = tModel::where('TemplateID', $fee_id)->first();
        $accounts = getAccountInfo($acctid);
        $curr = getObjectValueWithReturn($post,'curr','1');
        $numpay=1;
        $inc = 1;
        if(IsTFAccountID($accounts->AcctOption) || $inc == 1 ){
            switch($template->PaymentScheme){
                #SEMESTRAL
                case '1':  $custom =  sFinance::computeInstallment(1, getObjectValueWithReturn($post,'amt',0),1); break;
                #QUATERLY
                case '2':  $custom =  sFinance::computeInstallment(2, getObjectValueWithReturn($post,'amt',0),1); break;
                #Monthly
                case '3':  $custom =  sFinance::computeInstallment(3, getObjectValueWithReturn($post,'amt',0),1); break;
                default:
                    $custom =  sFinance::computeInstallment(0, getObjectValueWithReturn($post,'amt',0),1); break;
                break;

            }
        }else{
            $custom =  sFinance::computeInstallment(0, getObjectValueWithReturn($post,'amt',0),1);
        }

		return [
              'TemplateID' => $fee_id
              ,'AccountID' => getObjectValue($post,'acct')
              ,'Amount' => getObjectValueWithReturn($post,'amt',0)
              ,'CurrencyID'  => ($curr == '0' ? 1 : $curr )
              ,'Option' => getObjectValue($post,'status')
              ,'Option_Session' => getObjectValue($post,'session')
              ,'Remarks' => getObjectValue($post,'remarks')
              ,'Inactive' => 0
              ,'SeqNo' => getObjectValue($post,'seqno')
              ,'1stPayment' => $custom['1']
              ,'2ndPayment' => $custom['2']
              ,'3rdPayment' => $custom['3']
               ,'4thPayment' => $custom['4']
               ,'5thPayment' => $custom['5']
               ,'6thPayment' => $custom['6']
               ,'7thPayment' => $custom['7']
               ,'8thPayment' => $custom['8']
               ,'9thPayment' => $custom['9']
               ,'10thPayment' => $custom['10']
               ,'NumPayment' => $numpay
			];
	}



}
