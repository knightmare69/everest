<?php

namespace App\Modules\Accounting\Services;
use App\Modules\Accounting\Models\DebitCreditMemo as Model;
use App\Modules\Accounting\Services\AccountingServiceProvider as sFinance;
use App\Modules\Accounting\Services\Validation\FeesTemplateValidation;

use Request;
use DB;

class DMCMServiceProvider
{
    public function __construct()
	{
		$this->validation = new FeesTemplateValidation;
	}
    
    public function saveRecord(){
        $p = Request::all();
        $i = 0;
		$r = 0;
        $data = self::set_master_data($p);
        $data['RefNo'] = DB::select("SELECT TOP 1 (RefNo+1) as IDX FROM ES_DebitCreditMemo ORDER By RefNo DESC")[0]->IDX;
        $r             = $data['RefNo'];
		
		$idx = Model::create($data)->RefNo;
        
        $details = $p['details'];
        foreach($details as $d){
            
            $data = self::set_details_data($idx , $d, $i);  
            $data['ReferenceNo'] = $r;		
            //dd($data);			
            DB::table('es_journals')->insert($data);   
            
            #Insert entry for Debit
            /*
            if( floatVal(getObjectValueWithReturn($d,'debit',0)) > 0 ){                            
                $data = self::set_assess_data( $idx , $d, $i);
                DB::table('es_journals')->insert($data);            
            }
            */
                       
            $i++;
        }                
        
        #Recompute payment                    
		if(decode(getObjectValue($p,'reg'))!=''){
        sFinance::recomputeTotalPayment(getObjectValue($p,'txn') , decode(getObjectValue($p,'reg')), getObjectValue($p,'type'), decode(getObjectValue($p,'idno')), systemDate());
        }            
        return $idx;
    }
    
    
    
   	public function set_master_data($post)
	{

		return [
	       'RefDate' => systemDate()
          ,'IDType' => getObjectValue($post,'type')
          ,'IDNo' => decode(getObjectValue($post,'idno'))
          ,'TermID' => decode(getObjectValue($post,'term'))
          ,'CampusID' => getObjectValueWithReturn($post,'campus','1')
          ,'CreatedDate' => systemDate()
          ,'CreatedBy' => getUserID()
          ,'IsVoid' => 0
          ,'PostedDate' => systemDate()
          ,'Posted' => 1
          ,'TransType' => getObjectValue($post,'txn')
          ,'CurrencyID' => getObjectValueWithReturn($post,'curr','1')
          ,'TransNo' => decode(getObjectValue($post,'reg'))
          ,'Explanation' => getObjectValue($post,'explanation')
		];
	}
    
    public function set_details_data($idx, $post, $seq )
	{
 
        $p  = Request::all();
        $credit = getObjectValueWithReturn($post,'credit',0);
        if( floatval($credit) > 0 ){
            $trn = getObjectValue($post,'trn');
        }else{
            //$trn = '';
            $trn = getObjectValue($post,'trn');
        }
        
	   return  array(
           'ServerDate' => systemDate()
          ,'TransDate' => systemDate()
          ,'TermID' => decode(getObjectValue($p,'term'))
          ,'CampusID' => getObjectValueWithReturn($p,'campus','1')
          ,'TransID' => 60
          ,'ReferenceNo' => $idx           
          ,'IDType' => '1'
          ,'IDNo' => decode(getObjectValue($p,'idno'))
          ,'Payor' => getObjectValue($p,'name')
          ,'AccountID' => trimmed(getObjectValue($post,'id'))
          ,'CurrencyID' => getObjectValueWithReturn($post,'curr','1')
          ,'Debit' => getObjectValueWithReturn($post,'debit',0)
          ,'Credit' => $credit
          ,'Remarks' => ''
          ,'UserID' => getUserID()
          ,'DateModified' => systemDate()
          ,'ModifiedBy' => getUserID()
          ,'TransRefNo' => $trn
          ,'TransType' => getObjectValue($p,'txn')
          ,'DMCode' => getObjectValue($post,'dmcode')
          ,'SeqNo' => $seq
        ); 
	}
    
    public function set_assess_data($idx, $post, $seq )
	{
 
       $p  = Request::all();
              
	   return  array(
           'ServerDate' => systemDate()
          ,'TransDate' => systemDate()
          ,'TermID' => decode(getObjectValue($p,'term'))
          ,'CampusID' => getObjectValueWithReturn($p,'campus','1')
          ,'TransID' => getObjectValueWithReturn($p,'txn','1')
          ,'ReferenceNo' => decode(getObjectValue($p,'reg'))         
          ,'IDType' => getObjectValueWithReturn($p,'type','1')
          ,'IDNo' => decode(getObjectValue($p,'idno'))
          ,'Payor' => getObjectValue($p,'name')
          ,'AccountID' => trimmed(getObjectValue($post,'id'))
          ,'CurrencyID' => getObjectValueWithReturn($post,'curr','1')
          ,'Debit' => getObjectValueWithReturn($post,'debit',0)
          ,'Credit' => 0
          ,'Remarks' => ''
          ,'Assess Fee' => getObjectValueWithReturn($post,'debit',0)
          ,'1st Payment' => getObjectValueWithReturn($post,'debit',0)
          ,'UserID' => getUserID()
          ,'DateModified' => systemDate()
          ,'ModifiedBy' => getUserID()
          ,'DMCMRefNo' => $idx
          ,'SeqNo' => $seq
        ); 
	}
    

    
}