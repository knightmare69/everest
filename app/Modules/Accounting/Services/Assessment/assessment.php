<?php
namespace App\Modules\Accounting\Services\Assessment;
use DateTime;
use DB;
Class assessment{
  public $assess     = array();
  public $peracct    = array();
  public $djournal   = array();
  public $dcmemo     = array();
  public $discount   = array('Amount' => 0,'Percentage' => 0); 
  public $onnet      = array('Amount' => 0,'Percentage' => 0); 
  public $netasses   = 0;
  public $scheme     = 0;
  
  public function get_filter($type=1,$args=''){
	  switch($type){
        case 'student':
		  $qry  = "	SELECT s.StudentNo
				  ,s.LastName
				  ,s.FirstName
				  ,s.Middlename
				  ,s.MiddleInitial
				  ,s.ExtName
				  ,r.YearLevelID
				  ,y.YearLevelName
				  ,r.ProgID
				  ,p.ProgName
                  ,r.ValidatingOfficerID
                  ,r.ValidationDate		
                  ,s.FamilyID			  
			  FROM ES_Students as s 
		INNER JOIN ES_Registrations as r on r.StudentNo=s.StudentNo and r.termid=dbo.fn_getStudentLatestTerm(s.StudentNo)
		INNER JOIN ES_Programs as p ON r.ProgID=p.ProgID 
		INNER JOIN ESv2_YearLevel as y ON r.YearLevelID=y.YLID_OldValue AND p.ProgID=y.ProgID ".
		(($args!='')?"WHERE (s.StudentNo LIKE '%".$args."%' OR LastName LIKE'%".$args."%' OR FirstName LIKE'%".$args."%' OR s.FamilyID LIKE'%".$args."%')":"");
		$qry .= (($args!='')?(" AND s.StatusID<7"):(" WHERE s.StatusID<7"));
		$qry .= " ORDER BY s.LastName,s.FamilyID,r.YearLevelID,s.FirstName";
		break;
		case 'scholarship':
		    $qry = "SELECT * FROM ES_SchoProviders WHERE ProvCode LIKE '%".$args."%' OR ProvName LIKE '%".$args."%'";
		break;
	  }
	  $exec = DB::select($qry);
	  return $exec;
  }
  
  public function save_studentdiscount($post){
  	 $post['discount'] = ((array_key_exists('discount',$post))?floatval($post['discount']):0);
  	 $post['perc']     = ((array_key_exists('perc',$post))?floatval($post['perc']):0);
	 $data = array(
	             'TermID'           => ((array_key_exists('termid',$post))?$post['termid']:0)
	            ,'CampusID'         => 1
	            ,'TransType'        => 1
	            ,'IDType'           => ((array_key_exists('idtype',$post))?$post['idtype']:1)
	            ,'IDNo'             => ((array_key_exists('studno',$post))?$post['studno']:'')
	            ,'RegID'            => ((array_key_exists('regid',$post))?$post['regid']:'')
	            ,'SchoProviderID'   => ((array_key_exists('provid',$post))?$post['provid']:0)
	            ,'SchoProviderType' => ((array_key_exists('provtype',$post))?$post['provtype']:1)
	            ,'SchoOptionID'     => ((array_key_exists('option',$post))?$post['option']:1)
	            ,'SchoAccounts'     => ((array_key_exists('schoacct',$post))?$post['schoacct']:0)
	            ,'GrantTemplateID'  => ((array_key_exists('grant',$post))?$post['grant']:0)
	            ,'SchoFundType'     => ((array_key_exists('fund',$post))?$post['fund']:0)
	            ,'TotalDiscount'    => ((array_key_exists('discount',$post))?$post['discount']:0)
	            ,'Percentage'       => ((array_key_exists('perc',$post))?$post['perc']:0)
	            ,'ApplyOnNet'       => ((array_key_exists('onnet',$post))?$post['onnet']:0)	   
				,'AssessedBy'       =>  getUserID()       
				,'AssessedDate'     =>  date('Y-m-d')       
			 ); 		 
	 $exec = DB::table('ES_OtherTransactions')->insert($data);		 
	 return $data;
  }
  
  public function get_givendiscount($termid='1',$studno='',$regid=''){
	$qry  = "SELECT o.RefNo 
				   ,o.TransType
				   ,o.CampusID
				   ,o.TermID
				   ,o.TblFeeID
				   ,o.IDType
				   ,o.IDNo
				   ,o.SchoProviderType
				   ,o.SchoProviderID
				   ,p.ProvCode
				   ,p.ProvName
				   ,o.GrantTemplateID
				   ,t.TemplateCode
				   ,t.ShortName
				   ,o.ApplyOnNet
				   ,o.TotalDiscount
				   ,o.Percentage
				   ,o.Remarks
			   FROM ES_OtherTransactions as o
		 INNER JOIN ES_SchoProviders as p ON o.SchoProviderID=p.SchoProviderID
		  LEFT JOIN ES_SchoGrantTemplates as t ON o.GrantTemplateID=t.GrantTemplateID AND p.SchoProviderID=t.SchoProviderID
			  WHERE o.TermID>='".$termid."' AND o.IDNo='".$studno."'";  
	$exec = DB::select($qry);
	return $exec;
  }
  
  public function get_schoproviderinfo($xid){
	$qry  = "SELECT * FROM ES_SchoProviders WHERE SchoProviderID='".$xid."'";  
	$exec = DB::select($qry);
	return $exec;
  }
  
  public function get_schogranttemplate($xid){
	$qry = "SELECT * FROM ES_SchoGrantTemplates WHERE SchoProviderID='".$xid."' AND Inactive=0";
	$exec = DB::select($qry);
	return $exec;   
  }
  
  public function get_studentinfo($stdno){
	  $result = array();
	  $qry = "SELECT s.StudentNo
				    ,s.LastName
				    ,s.Firstname
				    ,s.Middlename
				    ,s.ProgID
				    ,p.ProgName
				    ,s.YearLevelID
				    ,y.YearLevelID as xYearLevelID
				    ,y.YearLevelName
				    ,r.RegID
				    ,r.TermID
				    ,r.TableofFeeID
				    ,f.TemplateCode
				    ,f.TemplateDesc
		            ,f.PaymentOption
		            ,f.PaymentScheme
				    ,r.RegDate
					,r.ValidatingOfficerID
				    ,r.ValidationDate
				    ,r.Remarks
				    ,CONCAT(t.AcademicYear,' ',t.SchoolTerm) as AYTerm
				    ,(CASE WHEN r.TermID=s.TermID AND s.AppNo<>'' THEN 0 ELSE 1 END) as StudentStatus	
			   FROM ES_Students as s 
		  LEFT JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo
		  LEFT JOIN ES_Programs as p ON ISNULL(r.ProgID,s.ProgID)=p.ProgID
		  LEFT JOIN ESv2_YearLevel as y ON ISNULL(r.YearLevelID,s.YearLevelID)=y.YLID_OldValue AND ISNULL(r.ProgID,s.ProgID)=y.ProgID
		  LEFT JOIN ES_AYTerm as t ON r.TermID=t.TermID
		  LEFT JOIN ES_TableofFees as f ON r.TableofFeeID =f.TemplateID
			  WHERE s.StudentNo='".$stdno."'
		   ORDER BY s.StudentNo,ISNULL(r.RegID,0) DESC";
	  $exec = DB::select($qry);
	  return $exec;
  }
  
  public function get_studentjournal($termid,$studno,$refno,$option=1){
	  $qry  = "SELECT j.EntryID
				  ,j.ServerDate
				  ,j.TransDate
				  ,j.TermID
				  ,j.CampusID
				  ,j.IDNo
				  ,j.TransID
				  ,j.ReferenceNo
				  ,j.[Description]
				  ,j.Particulars
				  ,j.Payor
				  ,j.AccountID
				  ,a.AcctCode
				  ,a.AcctName
				  ,a.AcctOption
				  ,a.CategoryID
				  ,j.CheckNo
				  ,j.Debit
				  ,j.Credit
				  ,j.Remarks
				  ,j.[Assess Fee]
				  ,j.Discount
                  ,j.FinancialAidExternal
				  ,j.[1st Payment]
				  ,j.[2nd Payment]
				  ,j.[3rd Payment]
				  ,j.[4th Payment]
				  ,j.[5th Payment]
				  ,j.[6th Payment]
				  ,j.[7th Payment]
				  ,j.[8th Payment]
				  ,j.[9th Payment]
				  ,j.[10th Payment] 
				  ,(ISNULL([1st Payment],0)+ISNULL([2nd Payment],0)+ISNULL([3rd Payment],0)+ISNULL([4th Payment],0)) as AllPayment
				  ,j.PaymentDiscount 
				  ,j.ActualPayment
				  ,j.Refund
				  ,j.DMCMRefNo
				  ,j.TransType
				  ,j.TransRefNo
				  ,j.CreditMemo
				  ,j.SeqNo
				  ,j.NonLedger
				  ,j.Deferred
	              ,j.InstallmentExcluded
				  ,j.SpecialFee
			  FROM ES_Journals as j
			INNER JOIN ES_Accounts as a ON j.AccountID=a.AcctID
			 WHERE TermID='".$termid."'
			   AND j.AccountID IN (111,110,1002,1003,1004)
			   AND ReferenceNo='".$refno."'
			   AND IDNo='".$studno."' 
			   AND IDType=1
			ORDER BY a.AcctID";
	  $exec = DB::select($qry);
	  if($option==1){
	    return $exec;
	  }elseif($option==2){
	    foreach($exec as $r){
		  $xntryid = $r->EntryID;
		  $xrefno  = $r->ReferenceNo;
		  $acctid  = $r->AccountID;	  
		  $disc    = DB::SELECT("SELECT SUM(Credit) as Credit FROM ES_Journals WHERE TermID='".$r->TermID."' AND IDNo='".$r->IDNo."' AND AccountID='".$r->AccountID."' AND TransID=60 AND Description='Total Discount'");
		  $disc    = (($disc && count($disc)>0)?($disc[0]->Credit):0);
		  $faid    = DB::SELECT("SELECT SUM(Credit) as Credit FROM ES_Journals WHERE TermID='".$r->TermID."' AND IDNo='".$r->IDNo."' AND AccountID='".$r->AccountID."' AND TransID=60 AND Description IN (SELECT ProvName FROM ES_SchoProviders)");
		  $faid    = (($faid && count($faid)>0)?($faid[0]->Credit):0);
		  if($acctid==110 || $acctid==111){
			$reserve = $this->get_reservation($xntryid,$xrefno,$acctid);
		    if( ( (($r->{'Assess Fee'}-$disc)-$faid)>$r->AllPayment ) && $r->ActualPayment>$reserve){
				$recompute = DB::statement("UPDATE ES_Journals SET [1st Payment]=(".$reserve."+[1st Payment]) WHERE EntryID='".$xntryid."'");
			}
		    $recompute = $this->recalc_actualpayment($termid,($xrefno.";%"),$studno,$acctid);
		  }else{
		    $recompute = $this->recalc_actualpayment($termid,($xrefno.";".$xntryid.";%"),$studno,$acctid);
		  }
		  
		  $redcmemo  = $this->get_debitcredit($studno,$xntryid,$xrefno,$acctid);
		  
		  $data                        = array();
		  $data['EntryID']             = $r->EntryID;
		  $data['ServerDate']          = $r->ServerDate;
		  $data['TransDate']           = $r->TransDate;
		  $data['TermID']              = $r->TermID;
		  $data['CampusID']            = $r->CampusID;
		  $data['TransID']             = $r->TransID;
		  $data['ReferenceNo']         = $refno;//$r->ReferenceNo;
		  $data['IDNo']                = $studno;//$r->ReferenceNo;
		  $data['Description']         = $r->Description;
		  $data['Particular']          = $r->Particulars;
		  $data['Payor']               = $r->Payor;
		  $data['SeqNo']               = $r->SeqNo;
		  $data['AccountID']           = $r->AccountID;
		  $data['AcctCode']            = $r->AcctCode;
		  $data['AcctName']            = $r->AcctName;
		  $data['AcctOption']          = $r->AcctOption;
		  $data['CategoryID']          = $r->CategoryID;
		  $data['CheckNo']             = $r->CheckNo;
		  $data['Debit']               = $r->Debit;
		  $data['Credit']              = $r->Credit;
		  $data['Remarks']             = $r->Remarks;
		  $data['AssessFee']           = $r->{'Assess Fee'};
		  $data['Discount']            = $disc;
		  $data['FinancialAid']        = $faid;
		  $data['1stPayment']          = $r->{'1st Payment'};
		  $data['2ndPayment']          = $r->{'2nd Payment'};
		  $data['3rdPayment']          = $r->{'3rd Payment'};
		  $data['4thPayment']          = $r->{'4th Payment'};
		  $data['5thPayment']          = $r->{'5th Payment'};
		  $data['6thPayment']          = $r->{'6th Payment'};
		  $data['7thPayment']          = $r->{'7th Payment'};
		  $data['8thPayment']          = $r->{'8th Payment'};
		  $data['9thPayment']          = $r->{'9th Payment'};
		  $data['10thPayment']         = $r->{'10th Payment'};
		  $data['PaymentDiscount']     = $r->PaymentDiscount;
		  $data['ActualPayment']       = $r->ActualPayment;
		  $data['Refund']              = $r->Refund;
		  $data['DMCMRefNo']           = $r->DMCMRefNo;
		  $data['TransRefNo']          = $r->TransRefNo;
		  $data['CreditMemo']          = $r->CreditMemo;
		  $data['NonLedger']           = $r->NonLedger;
		  $data['Deferred']            = $r->Deferred;
		  $data['InstallmentExcluded'] = $r->InstallmentExcluded;
		  $data['SpecialFee']          = $r->SpecialFee;
		  $this->assess[$acctid]       = $data;
	    }
	    return $this->assess;
	  }
  }
  
  public function get_discountdetail($termid,$studno){
	 $qry = "SELECT o.RefNo
				   ,o.TransType
				   ,o.CampusID
				   ,o.TermID
				   ,o.IDType
				   ,o.IDNo
				   ,o.SchoProviderID
				   ,p.ProvCode
				   ,p.ProvName 
				   ,o.GrantTemplateID
				   ,t.TemplateCode
				   ,t.ShortName
				   ,o.SchoAccounts
				   ,d.AcctID
				   ,o.SchoFundType
				   ,d.TypeUsed
				   ,ISNULL((CASE WHEN o.TotalDiscount>0 THEN o.TotalDiscount ELSE d.Amount END),0) as TotalDiscount
				   ,ISNULL((CASE WHEN o.Percentage>0 AND (ISNULL(d.TypeUsed,0)=0 AND ISNULL(d.Amount,0)<100) THEN o.Percentage ELSE d.Amount END),0) as TotalPercentage
				   ,o.ApplyOnNet
			   FROM ES_OtherTransactions as o
		 INNER JOIN ES_SchoProviders as p ON o.SchoProviderID=p.SchoProviderID
		  LEFT JOIN ES_SchoGrantTemplates as t ON o.GrantTemplateID=t.GrantTemplateID AND o.SchoProviderID=t.SchoProviderID
		  LEFT JOIN ES_SchoGrantTemplate_Details as d ON o.GrantTemplateID=d.GrantTemplateID
			  WHERE IDNo='".$studno."' AND o.TermID='".$termid."'";
	 $exec = DB::select($qry);
     return $exec;	 
  }
  
  public function get_studenttemplate($templid){
	  $qry  = "SELECT d.IndexID as EntryID
					 ,GETDATE() as ServerDate
					 ,GETDATE() as TransDate
					 ,f.TermID
					 ,f.CampusID
					 ,1 as TransID
					 ,1 as ReferenceNo
					 ,'' as [Description]
					 ,'' as Particulars
					 ,'' as Payor
					 ,d.AccountID
					 ,a.AcctCode
					 ,a.AcctName
					 ,a.AcctOption
					 ,a.CategoryID
					 ,'' as CheckNo
					 ,'0.00' as Debit
					 ,Amount as Credit
					 ,'' as Remarks
					 ,Amount as [Assess Fee]
					 ,'0.00' as Discount
					 ,'0.00' as FinancialAidExternal
					 ,[1stPayment] as [1st Payment]		  
					 ,[2ndPayment] as [2nd Payment]		  
					 ,[3rdPayment] as [3rd Payment]		  
					 ,[4thPayment] as [4th Payment]
					 ,[5thPayment] as [5th Payment]
					 ,[6thPayment] as [6th Payment]
					 ,[7thPayment] as [7th Payment]
					 ,[8thPayment] as [8th Payment]
					 ,[9thPayment] as [9th Payment]
					 ,[10thPayment] as [10th Payment]
					 ,'0.00' as PaymentDiscount
					 ,'0.00' as ActualPayment
					 ,'0.00' as Refund
					 ,'0.00' as DMCMRefNo
					 ,'0.00' as TransType
					 ,'0.00' as TransRefNo
					 ,'0.00' as CreditMemo
					 ,d.SeqNo
					 ,'0' as NonLedger
					 ,'0' as Deferred
					 ,'0' as InstallmentExcluded
					 ,'0' as SpecialFee
				  FROM ES_TableofFees as f
			INNER JOIN ES_TableofFee_Details as d ON f.TemplateID=d.TemplateID
			INNER JOIN ES_Accounts as a ON d.AccountID=a.AcctID
				 WHERE f.TemplateID='".$templid."'
		      ORDER BY a.AcctID";
	  $exec = DB::select($qry);
	  return $exec;
  }
  
  public function update_tableoffees($termid,$refno,$templid,$scheme,$payno){
	 $qry  = "UPDATE ES_Registrations SET TableofFeeID='".$templid."',NumberofPayments='".$payno."' WHERE TermID='".$termid."' AND RegID='".$refno."'
	          UPDATE r SET r.FirstPaymentDueDate   = r.RegDate
						  ,r.SecondPaymentDueDate  = ISNULL(f.Date2,d.SecondPaymentDate)
						  ,r.ThirdPaymentDueDate   = ISNULL(f.Date3,d.ThirdPaymentDate)
						  ,r.FourthPaymentDueDate  = ISNULL(f.Date4,d.FourthPaymentDate)
						  ,r.FifthPaymentDueDate   = d.FifthPaymentDate
						  ,r.SixthPaymentDueDate   = d.SixthPaymentDate
						  ,r.SeventhPaymentDueDate = d.SeventhPaymentDate
						  ,r.EightPaymentDueDate   = d.EighthPaymentDate
						  ,r.NinethPaymentDueDate  = d.NinthPaymentDate
						  ,r.TenthPaymentDueDate   = d.TenthPaymentDate
				  FROM ES_Registrations as r
			INNER JOIN ES_RegistrationConfig_Dates as d ON r.TermID=d.TermID and d.NoOfPayments='".$payno."' AND d.PaymentSchemeID='".$scheme."'
			INNER JOIN ES_TableOfFees_Dates as f ON r.TableofFeeID=f.TemplateID
				WHERE r.TermID='".$termid."' AND r.RegID='".$refno."'";
     $exec = DB::statement($qry);
     return true;			 
  }
  
  public function update_regassess($termid,$studno,$refno){
	 $qry = "UPDATE ES_OtherTransactions SET RegID='".$refno."' WHERE TermID='".$termid."' AND IDNo='".$studno."'
	         UPDATE r 
				SET r.AssessedFees              = j.TotalAssessment
				   ,r.TotalAssessment           = j.TotalAssessment
				   ,r.TotalBalance              = j.TotalBalance
				   ,r.TotalDiscount             = j.TotalDiscount
				   ,r.TotalFinancialAid         = j.TotalFinancialAid
				   ,r.TotalFinancialAidExternal = j.TotalFinancialAidExternal
				   ,r.TotalNetAssessed          = j.TotalNetAssessed
				   ,r.TotalPayment              = j.TotalPayment
			   FROM ES_Registrations as r 
		 INNER JOIN (SELECT TermID
						   ,IDNo
						   ,ReferenceNo
						   ,SUM([Assess Fee]) as TotalAssessment
						   ,SUM((([1st Payment]+[2nd Payment]+[3rd Payment]+[4th Payment])-(Discount+FinancialAidExternal))-ActualPayment) as TotalBalance
						   ,SUM(Discount) as TotalDiscount
						   ,SUM(FinancialAidExternal) as TotalFinancialAid
						   ,SUM(FinancialAidExternal) as TotalFinancialAidExternal
						   ,SUM(([1st Payment]+[2nd Payment]+[3rd Payment]+[4th Payment])-(Discount+FinancialAidExternal)) as TotalNetAssessed
						   ,SUM(ActualPayment) as TotalPayment
					   FROM ES_Journals
					  WHERE TermID = '".$termid."' AND IDNo='".$studno."'
				   GROUP BY TermID,IDNo,ReferenceNo) as j ON r.StudentNo=j.IDNo AND r.TermID=j.TermID AND CONVERT(VARCHAR(250),r.RegID)=j.ReferenceNo 
			  WHERE r.StudentNo='".$studno."' AND r.TermID='".$termid."' AND r.RegID='".$refno."'"; 
     $exec = DB::statement($qry);
     return true;			 
  }
  
  
  public function save_template($termid,$studno,$refno,$templid,$data){
	  $accts  = '';	  
	  $scheme = $this->scheme;
	  foreach($data as $r){
		 $penalty = 0;
		 $accts  .= (($accts=='')?'':',').$r['AccountID'];
		 $netass  = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']) - ($r['Discount']+$r['FinancialAid']);
		 if($r['10thPayment']==0){
			 if($r['AccountID']!='111' && $r['AccountID']!='110'){
			 //$r['1stPayment'] = (($r['2ndPayment']==0)?$netass:($r['1stPayment']- ($r['Discount']+$r['FinancialAid'])));  
			   $discount        = ($r['Discount'] + $r['FinancialAid']);
			 }else{
				$netass         = ($r['1stPayment']+$r['2ndPayment']+$r['3rdPayment']+$r['4thPayment']);  
				$penalty        = ($netass - ($r['AssessFee'] - $r['Discount']));
				if($penalty>0){
				 $r['AssessFee'] = $r['AssessFee']+$penalty+$r['FinancialAid'];
				 $discount       = ($r['Discount']+$r['FinancialAid']);
				}else{
				 $discount       = ($r['Discount'] - $penalty);
				}
			 }
		 }else{
			$discount        = ($r['Discount'] + $r['FinancialAid']); 
		 }
         /*
		  Save Total Discount to creditmemo column
		  Save Details to CreditMemo Table 
         */		 
		 $exec = DB::statement("INSERT INTO ES_Journals(ServerDate
		                                               ,TransDate
													   ,TermID
													   ,CampusID
													   ,TransID
													   ,ReferenceNo
													   ,[Description]
													   ,AccountID
													   ,IDType
													   ,IDNo
													   ,Debit
													   ,[Assess Fee]
													   ,Discount
													   ,PaymentDiscount
													   ,FinancialAidExternal
													   ,[CreditMemo]
													   ,[1st Payment]
													   ,[2nd Payment]
													   ,[3rd Payment]
													   ,[4th Payment]
													   ,[5th Payment]
													   ,[6th Payment]
													   ,[7th Payment]
													   ,[8th Payment]
													   ,[9th Payment]
													   ,[10th Payment]
													   ,ActualPayment)
								                   SELECT GETDATE() as ServerDate
														 ,RegDate
														 ,TermID
														 ,CampusID
														 ,1 as TransID
														 ,RegID
														 ,'' as [Description]
														 ,'".$r['AccountID']."' as AccountID
														 ,1 as IDType
														 ,StudentNo
														 ,'".$r['AssessFee']."' as Debit
														 ,'".$r['AssessFee']."' as [Assess Fee]
														 ,'0' as Discount
														 ,'0' as FinancialAidExternal
														 ,'0' as CreditMemo
														 ,'".$discount."' as PaymentDiscount
														 ,'".$r['1stPayment']."' as [1st Payment]
														 ,'".$r['2ndPayment']."' as [2nd Payment]
														 ,'".$r['3rdPayment']."' as [3rd Payment]
														 ,'".$r['4thPayment']."' as [4th Payment]
														 ,'".$r['5thPayment']."' as [5th Payment]
														 ,'".$r['6thPayment']."' as [6th Payment]
														 ,'".$r['7thPayment']."' as [7th Payment]
														 ,'".$r['8thPayment']."' as [8th Payment]
														 ,'".$r['9thPayment']."' as [9th Payment]
														 ,'".$r['10thPayment']."' as [10th Payment]
														 ,'".$r['ActualPayment']."' as [ActualPayment] 
													 FROM ES_Registrations 
													WHERE TermID='".$termid."' 
													  AND StudentNo='".$studno."' 
													  AND '".$r['AccountID']."' NOT IN (SELECT AccountID FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."' AND ReferenceNo='".$refno."')
													  
					            UPDATE ES_Journals SET Debit           = '".$r['AssessFee']."'
								                      ,[Assess Fee]    = '".$r['AssessFee']."'
													  ,CreditMemo      = '0'
													  ,PaymentDiscount = '".$discount."'
													  ,[1st Payment]   =  ".$r['1stPayment']."
													  ,[2nd Payment]   = '".$r['2ndPayment']."'
													  ,[3rd Payment]   = '".$r['3rdPayment']."'
													  ,[4th Payment]   = '".$r['4thPayment']."'
													  ,[5th Payment]   = '".$r['5thPayment']."'
													  ,[6th Payment]   = '".$r['6thPayment']."'
													  ,[7th Payment]   = '".$r['7thPayment']."'
													  ,[8th Payment]   = '".$r['8thPayment']."'
													  ,[9th Payment]   = '".$r['9thPayment']."'
													  ,[10th Payment]  = '".$r['10thPayment']."'
								                 WHERE AccountID   = '".$r['AccountID']."' 
												   AND TransID     = 1
												   AND DMCMRefNo   = ''
												   AND TermID      = '".$termid."' 
												   AND IDNo        = '".$studno."' 
												   AND ReferenceNo = '".$refno."'");	 
	  }
	  
	  if($accts!=''){
		$exec = DB::statement("DELETE FROM ES_Journals WHERE TermID = '".$termid."' 
	  	                                               AND IDNo   = '".$studno."' 
	  	                                               AND TransID=1
												       AND DMCMRefNo   = ''
	  	                                               AND AccountID IN (111,110,1002,1003,1004)
													   AND AccountID NOT IN (".$accts.")"); 
													  
	  }else{
		$exec = DB::statement("DELETE FROM ES_Journals WHERE TermID = '".$termid."' 
	  	                                               AND IDNo   = '".$studno."' 
	  	                                               AND TransID=1
												       AND DMCMRefNo   = ''
	  	                                               AND AccountID IN (111,110,1002,1003,1004)"); 
													  
	  }
	  
	  $exec = DB::statement("UPDATE x SET x.TransRefNo=CONCAT(j.ReferenceNo,';',j.EntryID,';',1),x.TransType=1,x.NonLedger=0
									FROM ES_Journals j
							  INNER JOIN ES_Journals x ON j.TermID=x.TermID AND j.IDNo=x.IDNo AND x.TransID=20 AND x.AccountID=1005 AND x.NonLedger=0
								   WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID=1 AND j.AccountID IN (110,111)
							
							UPDATE j SET j.ActualPayment = (ISNULL((SELECT SUM(Credit) FROM ES_Journals 
																	WHERE TermID=j.TermID 
																	  AND IDNo=j.IDNo 
																	  AND TransID=20 
																	  AND TransRefNo LIKE CONCAT(j.ReferenceNo,';',j.EntryID,';%')
					                                                  AND AccountID<>'1005' 
																	  AND Credit>0),0) - ISNULL((SELECT SUM(Debit) FROM ES_Journals 
																	WHERE TermID=j.TermID 
																	  AND IDNo=j.IDNo 
																	  AND TransID=60 
																	  AND TransRefNo LIKE CONCAT(j.ReferenceNo,';',j.EntryID,';%') 
																	  AND Debit>0),0))
									FROM ES_Journals j
								   WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID=1 AND j.DMCMRefNo='' AND j.AccountID IN (110,111,1002,1003,1004)
						    
							UPDATE j SET j.ActualPayment  = (CASE WHEN j.ActualPayment>0 AND j.[2nd Payment]>0 THEN j.ActualPayment ELSE (j.ActualPayment + x.Credit) END)
							            ,j.PaymentDiscount= (CASE WHEN j.ActualPayment>0 THEN j.PaymentDiscount ELSE j.PaymentDiscount END)
									 FROM ES_Journals j
							   INNER JOIN ES_Journals x ON j.TermID=x.TermID AND j.IDNo=x.IDNo AND x.TransID=20 AND x.AccountID IN (1005,1049) AND x.NonLedger=0
								    WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID=1 AND j.DMCMRefNo='' AND j.AccountID IN (110,111)");
  /**/
    $execb = DB::statement("UPDATE j SET j.[1st Payment] = (CASE WHEN j.[2nd Payment]>0 THEN 
	                                                                j.[1st Payment]-ISNULL((SELECT SUM(x.Credit) 
							                                                                  FROM ES_Journals x WHERE x.TermID='".$termid."' AND x.IDNo='".$studno."' 
																							   AND x.TransID=20 AND x.AccountID IN (1005,1049) AND x.NonLedger=0
																							   AND (Particulars LIKE '%Reserv%' OR Description LIKE '%Reserv%')),0) 
                                                            ELSE j.[1st Payment] END)
									 FROM ES_Journals j
							       WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID=1 AND j.DMCMRefNo='' AND j.AccountID IN (110,111)");	
  								   
	  return true;
  }
  
  public function exec_assessment($termid,$studno,$refno=0,$templid=0){
    $untilc   = 0;
	$divisor  = 1;
	$jcount   = 0;
	$journal  = $this->get_studentjournal($termid,$studno,$refno);
    $discount = $this->get_discountdetail($termid,$studno);	
	if($templid>0){
	 $journal = $this->get_studenttemplate($templid);	
	}
	if($journal){
	foreach($journal as $r){
      $acctid                      = $r->AccountID;	  
	  $data                        = array();
	  $data['EntryID']             = $r->EntryID;
	  $data['ServerDate']          = $r->ServerDate;
	  $data['TransDate']           = $r->TransDate;
	  $data['TermID']              = $r->TermID;
	  $data['CampusID']            = $r->CampusID;
	  $data['TransID']             = $r->TransID;
	  $data['ReferenceNo']         = $refno;//$r->ReferenceNo;
	  $data['IDNo']                = $studno;//$r->ReferenceNo;
	  $data['Description']         = $r->Description;
	  $data['Particular']          = $r->Particulars;
	  $data['Payor']               = $r->Payor;
	  $data['SeqNo']               = $r->SeqNo;
	  $data['AccountID']           = $r->AccountID;
	  $data['AcctCode']            = $r->AcctCode;
	  $data['AcctName']            = $r->AcctName;
	  $data['AcctOption']          = $r->AcctOption;
	  $data['CategoryID']          = $r->CategoryID;
	  $data['CheckNo']             = $r->CheckNo;
	  $data['Debit']               = $r->Debit;
	  $data['Credit']              = $r->Credit;
	  $data['Remarks']             = $r->Remarks;
	  $data['AssessFee']           = $r->{'Assess Fee'};
	  $data['Discount']            = 0;
	  $data['FinancialAid']        = 0;
	  
	  if($r->AccountID=='111' ||$r->AccountID=='110')
	   $data['1stPayment']          = $r->{'1st Payment'};
      else
	   $data['1stPayment']          = (($r->{'2nd Payment'}==0)?($r->{'Assess Fee'}):($r->{'1st Payment'}));//$r->{'Assess Fee'};
        
	  $data['2ndPayment']          = $r->{'2nd Payment'};
	  $data['3rdPayment']          = $r->{'3rd Payment'};
	  $data['4thPayment']          = $r->{'4th Payment'};
	  $data['5thPayment']          = $r->{'5th Payment'};
	  $data['6thPayment']          = $r->{'6th Payment'};
	  $data['7thPayment']          = $r->{'7th Payment'};
	  $data['8thPayment']          = $r->{'8th Payment'};
	  $data['9thPayment']          = $r->{'9th Payment'};
	  $data['10thPayment']         = $r->{'10th Payment'};
	  $data['PaymentDiscount']     = $r->PaymentDiscount;
	  $data['ActualPayment']       = $r->ActualPayment;
	  $data['Refund']              = $r->Refund;
	  $data['DMCMRefNo']           = $r->DMCMRefNo;
	  $data['TransRefNo']          = $r->TransRefNo;
	  $data['CreditMemo']          = $r->CreditMemo;
	  $data['NonLedger']           = $r->NonLedger;
	  $data['Deferred']            = $r->Deferred;
	  $data['InstallmentExcluded'] = $r->InstallmentExcluded;
	  $data['SpecialFee']          = $r->SpecialFee;
	  $this->assess[$acctid]       = $data;
	  if($acctid=='111' || $acctid=='110'){
	    if($data['10thPayment']>0){
		  $this->scheme = 3;
          $divisor      = 10;		  
	    }else if($data['4thPayment']>0){
		  $this->scheme = 2;
          $divisor      = 4;		  
	    }else if($data['3rdPayment']>0){
		  $this->scheme = 2;
          $divisor      = 4;		  
		  $untilc       = 3; 
	    }else if($data['2ndPayment']>0){
		  $this->scheme = 1;  
		  $divisor      = 2;  
	    }
	  }
	}
	
	foreach($discount as $r){
	  $acctid   = $r->AcctID;
      $amount   = $r->TotalDiscount;
      $percent  = $r->TotalPercentage;
	  if($r->GrantTemplateID>0){
		if($r->TypeUsed==0){
		  $amount  = 0;
          $percent = (($percent>0)?$percent:$r->TotalDiscount);		  
		}else{
		  $amount  = (($amount>0)?$amount:$r->TotalDiscount);
		  $percent = 0;
		}  
	  }else{
		  
	  }
	  
      $data                      = array();
	  $data['Description']       = $r->ProvName;
	  if($acctid!='0' && $acctid!=''){
		 $applyamt               = ((array_key_exists($acctid,$this->peracct))?$this->peracct[$acctid]['Amount']:0);  
		 $applyperc              = ((array_key_exists($acctid,$this->peracct))?$this->peracct[$acctid]['Percentage']:0);  
		 $data['AcctID']         = $acctid;
		 $data['Amount']         = $applyamt+$amount; 
		 $data['Percentage']     = $applyperc+$percent; 
		 $this->peracct[$acctid] = $data;
	  }else{ 
		 if($r->ApplyOnNet==1){
		  $applyamt                      = $this->onnet['Amount'];
		  $applyperc                     = $this->onnet['Percentage'];
		  $this->onnet['Description']    = $r->ProvName;
		  $this->onnet['Amount']         = $applyamt+$amount;
		  $this->onnet['Percentage']     = $applyperc+$percent;	 
		 }else{
		  $applyamt                      = $this->discount['Amount'];
		  $applyperc                     = $this->discount['Percentage'];
		  $this->discount['Description'] = $r->ProvName;
		  $this->discount['Amount']      = $applyamt+$amount;
		  $this->discount['Percentage']  = $applyperc+$percent;
		 } 
	  }	  
	}
	
	foreach($this->peracct as $d){
	   $acctid = $d['AcctID'];
       $totaldisc = 0;
       if(array_key_exists($acctid,$this->assess)){
		  $assessamt = $this->assess[$acctid]['AssessFee'];
		  $percent   = (($d['Percentage']<=100)? $d['Percentage'] : 100);
		  $percent   = (($percent>0)? ($percent/100) : 0 );
          $amount    = $d['Amount'];		  
		  
		  $totaldisc = ($assessamt*$percent);
		  $totaldisc = $totaldisc + $amount;
		  $this->assess[$acctid]['Discount']   += $totaldisc;
		//$this->assess[$k]['PaymentDiscount'] += $this->assess[$k]['Discount'];
		  $ddata     = array();
		  $ddata['TermID']      = $this->assess[$acctid]['TermID'];
		  $ddata['IDNo']        = $studno;
		  $ddata['IDType']      = 1;
		  $ddata['AccountID']   = $acctid;
		  $ddata['TransType']   = 1;
		  $ddata['TransID']     = 60;
		  $ddata['Description'] = 'Total Discount';
		  $ddata['Credit']      = $totaldisc;  
		  array_push($this->djournal,$ddata);
	   }	
	}
	
	$this->netasses = 0;
	foreach($this->assess as $k=>$a){
	    $assessamt       = $a['AssessFee'];
        $discount        = $a['Discount'];
        $financial       = $a['FinancialAid'];
      //$this->netasses += $assessamt - ($discount+$financial);		
		if($k=='111' || $k=='110'){
		  $this->netasses += ($assessamt - ($discount+$financial))/$divisor;
		}else{
			  $this->netasses += $assessamt - ($discount+$financial);	
			  $tmpass = ($assessamt - ($discount+$financial));
		      if($a['2ndPayment']>0){
				  $tmpval = (($divisor>1)?($tmpass/$divisor):$tmpass);
				  $this->assess[$k]['1stPayment'] = $tmpval;
				  if($divisor>=2){
				  $this->assess[$k]['2ndPayment'] = $tmpval;
				  }
				  if($divisor>=4){
				  $this->assess[$k]['3rdPayment'] = $tmpval;
				  $this->assess[$k]['4thPayment'] = (($untilc>0)?0:$tmpval);
				  }
			  }else{
			      $this->assess[$k]['1stPayment'] = $tmpass;
			  }  
		}
		$jcount++;
	}
	
	if($this->netasses>0){
		$totaldisc      = $this->onnet['Amount'];
		$percent        = (($this->onnet['Percentage']<=100)?$this->onnet['Percentage']:100);
		$percent        = (($percent>0)?($percent/100):0) * $this->netasses;
		$totaldisc      = $totaldisc+$percent;
		
		if($totaldisc>0){
		  $totaldisc = $totaldisc/$jcount;
          foreach($this->assess as $k=>$a){
			$this->assess[$k]['FinancialAid']    += $totaldisc;  
		  //$this->assess[$k]['PaymentDiscount'] += $this->assess[$k]['Discount'];
		    $ddata                                = array();
			$ddata['TermID']                      = $this->assess[$k]['TermID'];
			$ddata['IDNo']                        = $studno;
			$ddata['IDType']                      = 1;
			$ddata['AccountID']                   = $k;
			$ddata['TransType']                   = 1;
			$ddata['TransID']                     = 60;
			$ddata['Description']                 = $this->onnet['Description'];
			$ddata['Credit']                      = $totaldisc;  
			array_push($this->djournal,$ddata);
		  }	  
		}
	}
	
	if($this->scheme==3){
	   $totaltuition = 0;
       $overalldisc  = 0;
       $overallfinan = 0;
       $mtuition     = 0;
       $ntuition     = 0;
       foreach($this->assess as $k=>$data){
		   $totaltuition += $data['AssessFee'];
		   $overalldisc  += $data['Discount'];
		   $overallfinan += $data['FinancialAid'];
	   }
       $mtuition = $totaltuition -($overalldisc+$overallfinan);
	 //$mtuition = (($mtuition*0.008)*$divisor)+$mtuition;
       $ntuition = $mtuition/$divisor;
	   foreach($this->assess as $k=>$data){
	       $mtuition = ($data['AssessFee']-($data['Discount']+$data['FinancialAid']))/$divisor;   
		   $this->assess[$k]['1stPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['2ndPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['3rdPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['4thPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['5thPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['6thPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['7thPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['8thPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['9thPayment']  = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
		   $this->assess[$k]['10thPayment'] = $mtuition;//$ntuition * ($this->assess[$k]['AssessFee']/$totaltuition);
	   }	 
	}else{
		$tuitionid   = '111';	
		if(array_key_exists('111',$this->assess))
		 $tuitionid  = '111';	
		else if(array_key_exists('110',$this->assess))
		 $tuitionid  = '110';  
		
		if($this->scheme>0){
		  $tuition   = $this->assess[$tuitionid]['AssessFee'];	
		  $discount  = $this->assess[$tuitionid]['Discount'];	
		  $financial = $this->assess[$tuitionid]['FinancialAid'];	
		  
		  $paymenta  = ($tuition-$discount)/$divisor;
		  if($this->scheme==2){
		    if($untilc>0){
			$tmpay   = ((($paymenta * 0.008)*8)*3)+($paymenta*4);
			$paymentb= ($tmpay/3);
			$this->assess[$tuitionid]['1stPayment']=$paymentb-$financial;	
			$this->assess[$tuitionid]['2ndPayment']=$paymentb;	
			$this->assess[$tuitionid]['3rdPayment']=$paymentb;	
			$this->assess[$tuitionid]['4thPayment']=0;	
			}else{
			$paymentb = (($paymenta * 0.008) *8)+$paymenta;  
			$this->assess[$tuitionid]['1stPayment']=$paymenta-$financial;	
			$this->assess[$tuitionid]['2ndPayment']=$paymentb;	
			$this->assess[$tuitionid]['3rdPayment']=$paymentb;	
			$this->assess[$tuitionid]['4thPayment']=$paymentb;	
			}
		  }else if($this->scheme==1){
			$paymentb = (($paymenta * 0.008) *6)+$paymenta;  
			$this->assess[$tuitionid]['1stPayment']=$paymenta-$financial;	
			$this->assess[$tuitionid]['2ndPayment']=$paymentb;
			$this->assess[$tuitionid]['3rdPayment']=0;	
			$this->assess[$tuitionid]['4thPayment']=0;	
		  }
		}else{
		  $tuition   = $this->assess[$tuitionid]['AssessFee'];	
		  $discount  = $this->assess[$tuitionid]['Discount'];	
		  $financial = $this->assess[$tuitionid]['FinancialAid'];	
		  
		  $paymenta  = ($tuition-$discount)/$divisor;
		  $this->assess[$tuitionid]['1stPayment']=$paymenta-$financial;	
		  $this->assess[$tuitionid]['2ndPayment']=0;	
		  $this->assess[$tuitionid]['3rdPayment']=0;	
		  $this->assess[$tuitionid]['4thPayment']=0;	
		}
	}
	
	foreach($this->assess as $k=>$a){
	    $assessamt       = $a['AssessFee'];
        $discount        = $a['Discount'];
        $financial       = $a['FinancialAid'];
      //$this->netasses += $assessamt - ($discount+$financial);		
		if($k!='111' && $k!='110'){
		  $tmpass = ($assessamt - ($discount+$financial));
		  if($a['2ndPayment']>0){
			  $tmpval = (($divisor>1)?($tmpass/$divisor):$tmpass);
			  $tmpval = (($untilc>0)?($tmpass/$untilc):$tmpval);
			  
			  $this->assess[$k]['1stPayment'] = $tmpval;
			  if($divisor>=2){
			  $this->assess[$k]['2ndPayment'] = $tmpval;
			  }
			  if($divisor>=4){
			  $this->assess[$k]['3rdPayment'] = $tmpval;
			  $this->assess[$k]['4thPayment'] = (($untilc>0)?0:$tmpval);
			  }
		  }else{
			  $this->assess[$k]['1stPayment'] = $tmpass;
		  }	  
		}
	}
	
	if(count($this->djournal)>0){
	  foreach($this->djournal as $r){
		$desc = $r['Description'];
        $dcdata = array();
        $dcdata['RefDate']     = date('Y-m-d H:i:s');
        $dcdata['CreatedDate'] = date('Y-m-d H:i:s');		
        $dcdata['CreatedBy']   = 'admin';		
        $dcdata['TermID']      = $termid;		
        $dcdata['IDType']      = 1;		
        $dcdata['IDNo']        = $studno;		
        $dcdata['TransNo']     = $refno;		
        $dcdata['TransType']   = 1;		
		$dcdata['Explanation'] = $desc;		
        $dcdata['Posted']      = 1;		
        $dcdata['PostedDate']  = date('Y-m-d H:i:s');		
		$this->dcmemo[$desc]   = $dcdata;
      }	
	}
	  
	 if($refno!=0 && $refno!=''){
	  $save = $this->save_template($termid,$studno,$refno,$templid,$this->assess);
	  $save = $this->save_debitcredit($termid,$studno,$refno,$this->dcmemo);
	  $save = $this->save_dcmemo($termid,$studno,$refno,$this->djournal);
	  if($templid>0){
	  $exec = $this->update_tableoffees($termid,$refno,$templid,$this->scheme,$divisor);
	  $exec = $this->update_regassess($termid,$studno,$refno);
	  }
	 }
	}
	return $this->assess;
  }

  function save_debitcredit($termid,$studno,$refno,$arrdata){
	 $refno   = DB::table('ES_DebitCreditMemo')->orderBy('RefNo', 'desc')->pluck('RefNo');
	 $refno   = (($refno=='')?1:($refno+1));
	 $explain = '';
	 $transno = '';
	 foreach($arrdata as $r){
	   $explain .= (($explain=='')?'':',')."'".$r['Explanation']."'";
	   $transno  = $r['TransNo'];
	   $exec     = DB::statement("INSERT INTO ES_DebitCreditMemo(RefNo,RefDate,TermID,IDType,IDNo,CampusID,Explanation,TransType,TransNo,Posted,PostedDate,CreatedBy,CreatedDate,SchoProviderID)
                                                       SELECT '".$refno."'
													         ,'".$r['RefDate']."'
													         ,'".$r['TermID']."'
															 ,'".$r['IDType']."'
															 ,'".$r['IDNo']."'
															 ,'1'
															 ,'".$r['Explanation']."'
															 ,'".$r['TransType']."'
															 ,'".$r['TransNo']."'
															 ,'".$r['Posted']."'
															 ,'".$r['PostedDate']."'
															 ,'".$r['CreatedBy']."'
															 ,'".$r['CreatedDate']."'
															 ,'1' as SchoProviderID
														WHERE '".$r['Explanation']."' NOT IN (SELECT Explanation FROM ES_DebitCreditMemo WHERE  TermID='".$termid."' AND IDNo='".$studno."' AND TransNo='".$r['TransNo']."')");		
														
		$refno++;													 
	 }
	 
     if($explain!=''){
	    $qry   = "DELETE FROM ES_DebitCreditMemo WHERE (TransNo='".$transno."' AND IDNo='".$studno."' AND (Explanation='Total Discount' OR Explanation IN (SELECT ProvName FROM ES_SchoProviders))) AND Explanation NOT IN (".$explain.")";	 
        $exec  = DB::statement($qry);
	 }
	 
	 return true;
  }
  
  function save_dcmemo($termid,$studno,$refno,$arrdata){
	 $dcmemo = '';
	 $accts  = '';
	 if(count($arrdata)>0){
	     foreach($arrdata as $r){
		  $dcmemo .= (($dcmemo=='')?'':',')."'".$r['Description']."'";
		  $accts  .= (($accts=='')?'':',').$r['AccountID'];
		  $qry     = "INSERT INTO ES_Journals(ServerDate,TransDate,TransType,TermID,CampusID,TransID,ReferenceNo,DMCMRefNo,[Description],AccountID,IDType,IDNo,Credit,TransRefNo)
							 SELECT TOP 1
									j.ServerDate
								   ,j.TransDate
								   ,j.TransType
								   ,j.TermID
								   ,j.CampusID
								   ,60 as TransID
								   ,m.RefNo as ReferenceNo
								   ,j.ReferenceNo as DMCMRefNo
								   ,m.Explanation as [Description] 
								   ,j.AccountID as AccountID
								   ,j.IDType
								   ,j.IDNo
								   ,".$r['Credit']." as Credit
								   ,CONCAT(j.ReferenceNo,';',j.EntryID,';',0) as TransRefNo
							   FROM ES_Journals as j
						 INNER JOIN ES_DebitCreditMemo as m ON j.TermID=m.TermID AND j.IDNo=m.IDNo AND j.IDType=m.IDType AND j.ReferenceNo=m.TransNo
							  WHERE j.TermID      = '".$termid."' 
								AND j.IDNo        = '".$studno."' 
								AND m.Explanation = '".$r['Description']."' 
								AND j.AccountID   = '".$r['AccountID']."' 
								AND j.TransID     = 1
								AND '".$r['AccountID']."' NOT IN (SELECT AccountID FROM ES_Journals WHERE TransID=60 AND Description='".$r['Description']."' AND TermID = '".$termid."' AND IDNo = '".$studno."')
						 
						 UPDATE ES_Journals SET Credit     = '".$r['Credit']."'
											   ,TransRefNo = (SELECT TOP 1 CONCAT(ReferenceNo,';',EntryID,';',0) FROM ES_Journals WHERE TransID=1 AND AccountID='".$r['AccountID']."' AND TermID = '".$termid."' AND IDNo = '".$studno."')					 
											   ,UserID     = 'discount'
									  WHERE TransID=60 AND AccountID='".$r['AccountID']."' AND Description='".$r['Description']."' AND TermID = '".$termid."' AND IDNo = '".$studno."'
						 
						 UPDATE j SET CreditMemo=ISNULL((SELECT SUM(Credit) FROM ES_Journals WHERE TermID=j.TermID AND TransID=60 AND TransRefNo LIKE CONCAT(j.ReferenceNo,';',j.EntryID,';%')),0)
								 FROM ES_Journals as j
								WHERE j.TransID=1 AND j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID='".$r['AccountID']."'";
		  $exec  = DB::statement($qry);		
		 }
	 }else{
		  $qry  = "UPDATE j SET CreditMemo=ISNULL((SELECT SUM(Credit) FROM ES_Journals WHERE TermID=j.TermID AND TransID=60 AND TransRefNo LIKE CONCAT(j.ReferenceNo,';',j.EntryID,';%')),0)
					   FROM ES_Journals as j
					  WHERE j.TransID=1 AND j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID='1'";
		  $exec  = DB::statement($qry);	
	 }
     /**/ 
	 if($dcmemo!='' && $accts!=''){
	   $exec   = DB::statement("DELETE FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."' AND TransID=60 AND ReferenceNo NOT IN (SELECT RefNo FROM ES_DebitCreditMemo WHERE IDNo='".$studno."' AND TransNo='".$refno."')
	                            DELETE FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."' AND TransID=60 AND (Description='Total Discount' OR  Description IN (SELECT ProvName FROM ES_SchoProviders)) AND AccountID NOT IN (SELECT AccountID FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."' AND TransID=1)");
	 }
     return true;	 
  }
  
  function get_templatefee($term=1,$yrlvl=1,$progid=5,$scheme=-1){
	  $qry = "SELECT t.TemplateID
	                ,t.TemplateCode 
                FROM ES_TableofFees as t 
          INNER JOIN ES_TableofFee_DistributionList as l ON t.TemplateID=l.TemplateID
               WHERE t.TermID        = '".$term."' 
			     AND l.YearLevelID   = '".$yrlvl."'
 				 AND l.ProgID        = '".$progid."'
			     AND t.PaymentScheme = ".(($scheme>-1)?"'".$scheme."'":"t.PaymentScheme"); 
	  $exec = DB::select($qry);
	  return $exec;
  }
  
  public function load_othertxn($termid,$studno,$refno){
	$data = array();
	$exec = DB::select("SELECT AccountID,Debit,Remarks FROM ES_Journals WHERE TransType='16' AND IDNo='".$studno."' AND TermID='".$termid."'");
    foreach($exec as $r){
		$tmparr               = array();
		$tmpremark            = $r->Remarks;
		
		$tmpsplit            = explode("x",$tmpremark);
		$acctid              = $r->AccountID;
		$tmparr['AccountID'] = $r->AccountID;
		$tmparr['Amount']    = $r->Debit;
		$tmparr['Price']     = $tmpsplit[0];
		$tmparr['Count']     = $tmpsplit[1];
		$data[$acctid]       = $tmparr;
		
	}
	return $data;	
  }
  
  public function save_othertxn($termid,$studno,$refno,$accts){							 	  
	  foreach($accts as $r){
	     $remark = ((@array_key_exists('desc',$r))?($r['desc']):'');
		 $exec = DB::statement("INSERT INTO ES_Journals(ServerDate,TransDate,TermID,CampusID,TransID,TransType,ReferenceNo,[Description],AccountID,IDType,IDNo,Debit,[Assess Fee],[1st Payment],Remarks)
								                   SELECT GETDATE() as ServerDate
														 ,GETDATE() as TransDate
														 ,ISNULL(r.TermID,'".$termid."') as TermID
														 ,ISNULL(r.CampusID,s.CampusID) as CampusID
														 ,1 as TransID
														 ,16 as TransType
														 ,ISNULL(r.RegID,0) as RegID
														 ,'".$remark."' as [Description]
														 ,'".$r['acctid']."' as AccountID
														 ,1 as IDType
														 ,s.StudentNo
														 ,'".$r['amt']."' as Debit
														 ,'".$r['amt']."' as [Assess Fee]
														 ,'".$r['amt']."' as [1st Payment]
														 ,'".$r['price']."x".$r['qty']."' as [Remarks] 
													 FROM ES_Students as s
											    LEFT JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo AND r.TermID='".$termid."'
													WHERE s.StudentNo='".$studno."'");
	  }
	  return true;
  }
  
  public function rem_othertxn($termid,$studno,$refno){
	  $exec = DB::statement("DELETE FROM ES_Journals WHERE TermID ='".$termid."' 
	  	                                               AND IDNo   ='".$studno."'
													   AND ActualPayment=0
													   AND CreditMemo=0
	  	                                               AND TransID NOT IN (20,60) 
	  	                                               AND AccountID NOT IN (111,110,1002,1003,1004)");
	  return true;
  }
  
  public function exec_penalties($termid,$studno,$regid,$txndate=''){
	 $paysched = array(0=>'First',1=>'Second',2=>'Third',3=>'Fourth');
	 $txndate  = (($txndate=='' && strtotime($txndate)==false)?date('Y-m-d'):$txndate);
	 $pen_arr  = array();
	 $pen_perc = 0.03;
	 $tpayment = 0;
	 $payment  = 0;
	 $tpenalty = 0;
	 $totaldue = 0;
	 $resvc = DB::select("SELECT ISNULL(Credit,0) as Credit FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."' AND TransID=20 AND AccountID=1005 AND NonLedger=0");
	 $exec  = DB::select("SELECT r.RegID
			 				    ,SUM(ISNULL([1st Payment],AssessedFees)) as FirstPayment
			 				    ,r.RegDate as FirstPaymentDueDate
							    ,SUM([2nd Payment]) as SecondPayment
							    ,r.SecondPaymentDueDate 
							    ,SUM([3rd Payment]) as ThirdPayment
							    ,r.ThirdPaymentDueDate
							    ,SUM([4th Payment]) as FourthPayment
							    ,r.FourthPaymentDueDate
							    ,SUM(j.ActualPayment) + 
							     ISNULL((SELECT SUM(Credit) 
							               FROM ES_Journals 
							              WHERE TransID=60 AND TermID='".$termid."' AND [Description] NOT IN ('Total Discount','Early Bird Discount') 
							                AND TransRefNo LIKE CONCAT(r.RegID,';%')),0) as ActualPayment 
						   FROM ES_Journals as j
					 INNER JOIN ES_Registrations as r ON j.TermID=r.TermID AND j.IDNo=r.StudentNo AND j.ReferenceNo=r.RegID 
						  WHERE j.TermID='".$termid."' AND j.IDNo='".$studno."' AND j.TransID=1 AND j.AccountID NOT IN (1071)
					   GROUP BY r.RegID,r.RegDate,r.FirstPaymentDueDate,r.SecondPaymentDueDate,r.ThirdPaymentDueDate,r.FourthPaymentDueDate");
    if($exec && count($exec)>0){
		$keyid = str_replace('-','',$txndate).str_pad($regid,6,'0',STR_PAD_LEFT);
		
		foreach($exec as $r){
		  if($resvc && count($resvc)>0 && $r->{'ThirdPayment'}>0){
			$r->ActualPayment =  $r->ActualPayment;// + $resvc[0]->Credit;
		  }	
		  $tpayment   = $r->ActualPayment;	
		  $payment    = $r->ActualPayment;	
		  foreach($paysched as $k=>$s){
			  $totaldue += $r->{$s.'Payment'};
			  if($r->{$s.'Payment'}>0){
				$pen_arr[$k] = array('KeyID'=>$keyid,'Scheme'=>$r->{$s.'Payment'},'Payment'=>(($payment>0)?$payment:0),'CutDate'=>$r->{$s.'PaymentDueDate'});   	
				//20200303 - Binago nanaman duedate lang muna pero di magpepenalty
			    //$cutdate   = (($r->{$s.'PaymentDueDate'}=='')?0:strtotime($r->{$s.'PaymentDueDate'}.' -14 days'));
				$cutdate   = (($r->{$s.'PaymentDueDate'}=='')?0:strtotime($r->{$s.'PaymentDueDate'}.' +30 days'));
				$checkdate = strtotime($txndate);
				$interval  = (($cutdate==0)?0:round((($checkdate - $cutdate)/ (60 * 60 * 24*30)),0));
				if($payment>=$r->{$s.'Payment'}){
				  $payment = $payment-$r->{$s.'Payment'};
				  $pen_arr[$k]['Excess']            = $payment;
				  $pen_arr[$k]['Balance']           = 0.00;
				  $pen_arr[$k]['Penalty']           = 0.00;
				  $pen_arr[$k]['PenaltyMonthCount'] = 0.00;
				}else{
				  $balance   = $r->{$s.'Payment'}-$pen_arr[$k]['Payment'];
				  $month     = (($checkdate>$cutdate)?$interval:0);
				  $penalty   = ((env('WAVE_PENALTY',0)==1)?0:(($balance*$pen_perc)*$month));
				  $tpenalty += $penalty;
				  $pen_arr[$k]['Excess']            = 0.00;
				  $pen_arr[$k]['Balance']           = $balance;
				  $pen_arr[$k]['Penalty']           = $penalty;
				  $pen_arr[$k]['PenaltyMonthCount'] = $month;
				  $payment = $payment - ($penalty+$r->{$s.'Payment'});
				}  
			  }
		 }
		}
		
        if(count($pen_arr)>0){
		  $del = DB::statement("DELETE FROM ES_SOA_GSHS_Penalties WHERE RegID='".$regid."'");

		  foreach($pen_arr as $k=>$data){
			 if($data['Payment']<0.0001){
			   $data['Payment'] = 0;
			 }		  	 
			 if($data['Excess']<0.0001){
			   $data['Excess'] = 0;
			 }		
			 if($data['Balance']>0){
                //$data['Balance']+= $resvc[0]->Credit;
			 }  	 
			 $data['Balance']        = round($data['Balance'],4);
			 $data['Penalty']        = round($data['Penalty'],4);
			 $data['SeqNo']          = $k+1;
			 $data['RegID']          = $regid;
			 $data['TotalAmountDue'] = round($totaldue,4);
			 $data['TotalPenalty']   = round($tpenalty,4);
             $exec = DB::table('ES_SOA_GSHS_Penalties')->insert($data);			 
		  }
		  if($tpenalty>0){
		   $count = DB::select("SELECT COUNT(ReferenceNo) as Items FROM ES_Journals WHERE TransID=1 AND ReferenceNo='".$regid."' AND AccountID='1071' AND (ActualPayment=0 AND CreditMemo=0)");  
		   $count = (($count && count($count)>0)?$count[0]->Items:0);
		   $query = ""; 
			if($count==0){
		      $query = "INSERT INTO ES_Journals(ServerDate
								   ,TransDate
								   ,TermID
								   ,CampusID
								   ,TransID
								   ,ReferenceNo
								   ,[Description]
								   ,AccountID
								   ,IDType
								   ,IDNo
								   ,Debit
								   ,[Assess Fee])
							 SELECT GETDATE() as ServerDate
								   ,GETDATE() as TransDate
								   ,r.TermID
								   ,'1' as CampusID
								   ,'1' as TransID
								   ,r.RegID
								   ,'Penalty For Over Due Date' as [Description]
								   ,'1071' as AccountID
								   ,'1' as IDType
								   ,r.StudentNo
								   ,SUM(p.Penalty) as Debit 
								   ,SUM(p.Penalty) as [Assess Fee] 
							   FROM ES_Registrations as r
						 INNER JOIN ES_SOA_GSHS_Penalties as p ON CONVERT(VARCHAR(50),r.RegID) = CONVERT(VARCHAR(50),p.RegID) AND p.CutDate<=GETDATE() 
							  WHERE r.RegID='".$regid."' AND p.Penalty>0
						   GROUP BY r.StudentNo,r.RegID,r.TermID";
		      $exec = DB::statement($query); 		   
		   }else{
			 $query = "UPDATE ES_Journals SET Debit = (SELECT SUM(Penalty) FROM ES_SOA_GSHS_Penalties WHERE RegID='".$regid."') 
                                             ,[Assess Fee] = (SELECT SUM(Penalty) FROM ES_SOA_GSHS_Penalties WHERE RegID='".$regid."') 
                                        WHERE ReferenceNo='".$regid."' AND TransID=1 AND AccountID='1071' AND (ActualPayment=0 AND CreditMemo=0)";   
		       $exec = DB::statement($query); 		   
		   }
		   //die($count);
          }
		}
        return $keyid;		
	}
	return false;
  }
  
  function get_reservation($entryid,$refno,$acctid){
	$payment = DB::select("SELECT SUM(Credit) as ActualPayment FROM ES_Journals WHERE TransID=20 AND ([Description] LIKE 'Reserv%' OR [Particulars] LIKE 'Reserv%') AND TransRefNo LIKE '".$refno.";%' AND NonLedger=0");
    return (($payment && count($payment)==1)? $payment[0]->ActualPayment:'0.00');	  
  }
  
  function get_debitcredit($studno,$entryid,$refno,$acctid,$resave=0){
	 If($acctid=='110' || $acctid=='111'){
	 $dmcm = DB::select("SELECT SUM(Credit) as CM, SUM(Debit) as DM 
                                    FROM ES_DebitCreditMemo as dc
                              INNER JOIN ES_Journals as j ON CONVERT(VARCHAR(400),dc.RefNo)=j.ReferenceNo AND j.TransID=60
                                   WHERE dc.IDNo='".$studno."' AND dc.TransNo='".$refno."' 
								     AND j.TransRefNo NOT LIKE (SELECT TOP 1 CONCAT(ReferenceNo,';',EntryID,';%') FROM ES_Journals WHERE TransID=1 AND IDNo='".$studno."' AND ReferenceNo='".$refno."' AND AccountID<>'".$acctid."') 
								     AND (j.Description<>'Total Discount' AND j.Description NOT IN (SELECT ProvName FROM ES_SchoProviders)) ");
							       //AND j.TransRefNo LIKE (SELECT TOP 1 CONCAT(ReferenceNo,';%') FROM ES_Journals WHERE TransID=1 AND IDNo='".$studno."' AND ReferenceNo='".$refno."' AND AccountID='".$acctid."') 
								     		 
	 }else{
	 $dmcm = DB::select("SELECT SUM(Credit) as CM, SUM(Debit) as DM 
                                    FROM ES_DebitCreditMemo as dc
                              INNER JOIN ES_Journals as j ON CONVERT(VARCHAR(400),dc.RefNo)=j.ReferenceNo AND j.TransID=60
                                   WHERE dc.IDNo='".$studno."' AND dc.TransNo='".$refno."' 
								     AND j.TransRefNo LIKE (SELECT TOP 1 CONCAT(ReferenceNo,';',EntryID,';%') FROM ES_Journals WHERE TransID=1 AND IDNo='".$studno."' AND ReferenceNo='".$refno."' AND AccountID='".$acctid."') 
								     AND (j.Description<>'Total Discount' AND j.Description NOT IN (SELECT ProvName FROM ES_SchoProviders)) ");
	 }
	 
	 if($dmcm && count($dmcm)==1){
	    $tmpdb = $dmcm[0]->DM;
	    $tmpcb = $dmcm[0]->CM;
		/**/
		if($tmpdb>0){
		  $dm_exec = DB::statement("UPDATE ES_Journals SET Refund=".$tmpdb." WHERE TransID=1 AND IDNo='".$studno."' AND ReferenceNo='".$refno."' AND AccountID='".$acctid."'");
		}
		if($tmpcb>0){
		  $dm_exec = DB::statement("UPDATE ES_Journals SET CreditMemo=".$tmpcb." WHERE TransID=1 AND IDNo='".$studno."' AND ReferenceNo='".$refno."' AND AccountID='".$acctid."'");
		}
		
	 }
	 
     return (($dmcm && count($dmcm)==1)?$dmcm[0]->CM : '0.00');	 
  }
  
  function get_actualpayment($entryid,$refno,$acctid){
    if($acctid=='110' || $acctid=='111'){
	  $payment = DB::select("SELECT SUM(Credit) as ActualPayment FROM ES_Journals WHERE TransID=20 AND ((ReferenceNo='".$refno."' AND AccountID='".$acctid."') OR (TransRefNo LIKE '".$refno.";%' AND AccountID NOT IN (1002,1003,1004)))");
	}else{
      $payment = DB::select("SELECT SUM(Credit) as ActualPayment FROM ES_Journals WHERE TransID=20 AND ((ReferenceNo='".$refno."' AND AccountID='".$acctid."') OR (TransRefNo LIKE '".$refno.";".$entryid.";%'))");
	}
	
	return (($payment && count($payment)==1)? $payment[0]->ActualPayment:'0.00');	
  }
  
  public function recalc_actualpayment($termid,$refno,$studno,$acctid){
    $crd = 0;
	if($acctid==110 || $acctid==111){
    $qry = " WHERE TransID=20 AND TermID='".$termid."' 
			   AND ((IDNo='".$studno."' AND AccountID='".$acctid."') 
			     OR (AccountID NOT IN (1002,1003,1004) AND TransRefNo LIKE '".$refno."'))";
	}else{
	$qry = " WHERE TransID=20 AND TermID='".$termid."' 
			   AND ((IDNo='".$studno."' AND AccountID='".$acctid."') 
			     OR (AccountID<>'".$acctid."' AND TransRefNo LIKE '".$refno."'))";
	}
	
	$exec = DB::select("SELECT SUM(Credit) as Credit FROM ES_Journals ".$qry);
	if($exec && count($exec)>0){
	   $crd    = $exec[0]->Credit;
       $recalc = DB::statement("UPDATE ES_Journals SET ActualPayment='".$crd."' WHERE TransID=1 AND TermID='".$termid."' AND IDNo='".$studno."' AND AccountID='".$acctid."'"); 	   
	   $mapped = DB::statement("UPDATE ES_Journals SET TransRefNo=(SELECT TOP 1 CONCAT(ReferenceNo,';',EntryID,';1') FROM ES_Journals WHERE TermID='".$termid."' AND IDNo='".$studno."' AND AccountID='".$acctid."' AND TransID=1) ".$qry." AND ISNULL(Credit,0)>0"); 
	}
    return $crd;	
  }
  
  public function recalc_creditmemo($termid,$studno,$acctid){
  
  }
  
  function child_seltemplate($termid=0,$studno='',$scheme=0,$yrlvl=0,$major=0){
	 $chk = DB::SELECT("SELECT (CASE WHEN s.Gender='M' THEN 1 WHEN s.Gender='F' THEN 2 ELSE 0 END) as Gender
	                          ,(SELECT TOP 1 ISNULL(COUNT(*),0) FROM ES_Admission WHERE AppNo=s.AppNo AND TermID='".$termid."') as SStatus
		                      ,ForeignStudent as IsForeign
		                      ,dbo.fn_K12IsElder(s.StudentNo) as IsElder
		                      ,(CASE WHEN r.TableofFeeID IS NULL THEN 1 ELSE r.TableofFeeID END) as TemplateID
	                      FROM ES_Students as s 
	           LEFT OUTER JOIN ES_Registrations as r ON r.StudentNo=s.StudentNo AND r.TermID='".$termid."'
	                     WHERE s.StudentNo='".$studno."'");
	 $gender ='M';
	 $foreign=0;
	 $sstats =0;
	 $elder  =0;
	 if($chk && count($chk)>0){
	    foreach($chk as $rs){
		   
		}
		
	    $selrs = DB::SELECT("SELECT tb.*
							   FROM ES_TableofFees AS tb
					LEFT OUTER JOIN ES_TableofFee_DistributionList as dl ON tb.TemplateID=dl.TemplateID
					LEFT OUTER JOIN ES_RegistrationConfig_Dates as d on d.TermID=tb.TermID AND d.PaymentSchemeID=0 AND d.Classification=0
					LEFT OUTER JOIN ES_Registrations as r ON r.StudentNo='2017939' AND r.TermID=1001
							  WHERE ((tb.TemplateID=0) OR 
									 (tb.TermID = 1001 AND 
									 tb.PaymentScheme=0 AND 
									 dl.YearLevelID=9 AND 
									 (tb.ForForeign = 0 OR tb.ForForeign = 0) AND
									 (tb.StudentGender=1 OR tb.StudentGender=0) AND
									 (tb.StudentStatus=0 OR tb.StudentStatus=0) AND 
									 tb.InActive=0 AND 0=0))");
	 
	 }				 
  }
  
  function recalc_assessment($idno='',$refno=0){
	$res    = false;
	$reginfo= false;
	if($idno!='' && $idno!=0){
		if($refno==0){
			$reginfo = DB::SELECT("SELECT TOP 1 * FROM ES_Registrations WHERE StudentNo='".$idno."' ORDER BY RegID DESC");
		}else{
			$reginfo = DB::SELECT("SELECT TOP 1 * FROM ES_Registrations WHERE RegID='".$refno."' ORDER BY RegID DESC");
		}
		
		if($reginfo && count($reginfo)>0){
		  $termid = $reginfo[0]->TermID;
		  $templid= $reginfo[0]->TableofFeeID;
		  $studno = $idno;
		  $execa  = $this->exec_assessment($termid,$studno,$refno,$templid);
		  $execb  = $this->get_studentjournal($termid,$studno,$refno,2);
		  $res    = true;
		}
		$resched = $this->update_payschedule($idno);
	}
	return $res;
  }
  
  function update_payschedule($studno=''){
    $reginfo= DB::SELECT("SELECT TOP 1 * FROM ES_Registrations WHERE StudentNo='".$studno."' ORDER BY RegID DESC");
	$regid  = (($reginfo && count($reginfo)>0)?($reginfo[0]->RegID):0);
	$tempid = (($reginfo && count($reginfo)>0)?($reginfo[0]->TableofFeeID):0);
    $pdates = DB::SELECT("SELECT Date1,Date2,Date3,Date4 FROM ES_TableOfFees_Dates WHERE TemplateID='".$tempid."'
						  UNION ALL
						  SELECT FirstPaymentDate,SecondPaymentDate,ThirdPaymentDate,FourthPaymentDate FROM ES_RegistrationConfig_Dates as cd 
						   INNER JOIN ES_TableofFees as t ON t.TermID=cd.TermID AND t.PaymentScheme=cd.PaymentSchemeID WHERE t.TemplateID='".$tempid."'");
    if($pdates && count($pdates)>0){
	  $tfields                        = array();
	  $tfields['FirstPaymentDueDate'] = $pdates[0]->Date1;
	  $tfields['SecondPaymentDueDate']= $pdates[0]->Date2;
	  $tfields['ThirdPaymentDueDate'] = $pdates[0]->Date3;
	  $tfields['FourthPaymentDueDate']= $pdates[0]->Date4;
	  $query   = DB::table('ES_Registrations')->where('RegID', $regid)->update($tfields);
    }
    return true; 
  }

}
?>