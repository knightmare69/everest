<?php

namespace App\Modules\Accounting\Services;

use Validator;

class ChartOfAccountsSP
{
    private function decode_inputs($arr, $exclude_arr_keys)
    {
        $dk = array_diff_key($arr, array_flip($exclude_arr_keys));
        $nd = array_map(function ($v) { return decode($v); }, $dk);
        return array_merge($arr, $nd);
    }

    public function _validate($d, $form)
    {

        switch ($form) {
            case 'group':
            case 'sfund':
            case 'sunits':
            case 'classif':
            case 'types':
                $rules = [
                    'code' => 'required|string',
                    'sname' => 'required|string',
                    'name' => 'required|string',
                ];
                break;

            case 'account':
                $d = $this->decode_inputs($d, ['acc-code', 'acc-amount', 'acc-name', 'acc-sname']);
                $exl_upd = !empty($d['id']) ? ','.$d['id'].',AcctID' : '';

                $rules = [
                    'acc-code' => 'required|unique:es_accounts,AcctCode'.$exl_upd.'|string',
                    'par-acc' => 'integer',
                    'acc-amount' => 'required|regex:/^\d*(\.\d{1,2})?$/',
                    'acc-name' => 'required|string',
                    'acc-sname' => 'required|string',
                    'acc-group' => 'required|integer',
                    'acc-sub-fund' => 'required|integer',
                    'acc-unit' => 'required|integer',
                    'acc-dept' => 'required|integer',
                    'acc-classif' => 'required|integer',
                    'acc-type' => 'required|integer',
                    'acc-options' => 'required|integer',
                    'acc-pay-options' => 'required|integer',
                ];
                break;

            default:
                $rules = [];
                break;
        }

        $custom_attribs = [];

        $v = Validator::make($d, $rules)->addCustomAttributes($custom_attribs);
        return ['messages' => $v->errors()->all(), 'keys' => $v->errors()->keys()];;
    }

    public function db_fields($d, $form)
    {
        $a = [];

        switch ($form) {
            case 'group':
                $r = ['GroupCode', 'GroupShort', 'GroupName'];
                $a = ['OR_PrintInGroup' => 1];
                break;

            case 'sfund':
                $r = ['SubGroupCode', 'SubGroupShort', 'SubGroupName'];
                break;

            case 'sunits':
                $r = ['SubUnitCode', 'SubUnitShort', 'SubUnitName'];
                break;

            case 'classif':
                $r = ['ClassCode', 'ClassShort', 'ClassName'];
                $a = ['OR_PrintInGroup' => 1];
                break;

            case 'types':
                $r = ['CategoryCode', 'CategoryShort', 'CategoryName'];
                break;

            case 'account':
                $d = $this->decode_inputs($d, ['acc-code', 'acc-amount', 'acc-name', 'acc-sname']);

                $r = ['AcctCode', 'ParentAcctID', 'DefaultAmount', 'AcctName', 'ShortName', 'GroupID', 'SubFundID', 'SubUnitID', 'DeptID', 'ClassID', 'CategoryID', 'AcctOption', 'PaymentOption'];

                if (!empty($d['id'])) {
                    $a = ['ModifiedBy' => getUserFacultyID(), 'ModifiedDate' => systemDate()];
                } else {
                    $a = ['CreatedBy' => getUserFacultyID(), 'CreatedDate' => systemDate()];
                }

                $a['Inactive'] = !empty($d['acc-inactive']) ? 1 : 0;
                $a['TotalAmountOnly'] = !empty($d['acc-def-amount']) ? 1 : 0;

                unset($d['id'], $d['acc-inactive'], $d['acc-def-amount']);
                // $a = ['SortOrder', 'GLID', 'GLID_Setup_Debit', 'GLID_Collection_Credit', 'PayorTypeID', 'ReqAYTermID', 'ReqDeptID', 'ReqYrLevelID'];
                break;

            default:
                $r = [];
                break;
        }

        $r = array_merge(array_combine($r, $d), $a);
        return $r;

    }
}
