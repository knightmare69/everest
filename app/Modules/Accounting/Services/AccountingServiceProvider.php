<?php

namespace App\Modules\Accounting\Services;

use App\Modules\Enrollment\Models\Registration as mReg;
use App\Modules\Accounting\Models\TableOfFees as tModel;
use App\Modules\Accounting\Models\TableOfFees_Details as mFeesDetails;
use App\Modules\Accounting\Models\TableOfFees_Dues as mFeesDues;
use App\Modules\Students\Models\StudentProfile as mStudent;
use App\Modules\Cashier\Models\Journals as mJournal;
use App\Modules\Cashier\Models\OtherTransactions as mOthertrans;
use App\Modules\Cashier\Models\PaymentOptions as mPayoption;

use DB;
use Request;

class AccountingServiceProvider
{
    public static function validateTxn($txn, $ref, $type, $idno, $orno ){
        switch($txn){
            case '1': self::validateEnrollment($ref, $orno); break;
            case '2': self::validateADC($ref); break;
            default : self::validateOtherTxn($ref,$type, $idno ); break;
        }
    }

    public static function validateEnrollment($ref, $orno){
       $model = new mReg();
       $rs  = $model->where('RegID', $ref)->where('TableofFeeID','<>', '')->first();
       if(!empty($rs)){
         if( $rs->ValidationDate == ''){
            $data = array( 'ValidationDate' => systemDate(), 'ValidatingOfficerID' => getUserID() , 'ORNo' => $orno );
            $model->where('RegID', $ref)->update($data);

         }
       }
       return true;
    }

    public static function validateADC($ref){

    }

    public static function validateOtherTxn($ref, $type, $idno )
    {

    }

    public static function computeInstallment($opt, $amount, $scheme = 0){

        $m1st = 0;$m2nd = 0;$m3rd = 0;$m4th = 0;$m5th = 0;$m6th = 0;$m7th = 0;$m8th = 0;$m9th = 0;$m10th = 0;
        if($scheme ==0){
            $option = mPayoption::where('PaymentOptionID', $opt)->first();
            if ( floatval($option['1stPayment']) > 0 ){ $m1st = floatval($amount) * ( floatval($option['1stPayment']) / 100) ; }
            if ( floatval($option['2ndPayment']) > 0 ){ $m2nd = floatval($amount) * ( floatval($option['2ndPayment']) / 100) ; }
            if ( floatval($option['3rdPayment']) > 0 ){ $m3rd = floatval($amount) * ( floatval($option['3rdPayment']) / 100) ; }
            if ( floatval($option['4thPayment']) > 0 ){ $m4th = floatval($amount) * ( floatval($option['4thPayment']) / 100) ; }
            if ( floatval($option['5thPayment']) > 0 ){ $m5th = floatval($amount) * ( floatval($option['5thPayment']) / 100) ; }
            if ( floatval($option['6thPayment']) > 0 ){ $m6th = floatval($amount) * ( floatval($option['6thPayment']) / 100) ; }
            if ( floatval($option['7thPayment']) > 0 ){ $m7th = floatval($amount) * ( floatval($option['7thPayment']) / 100) ; }
            if ( floatval($option['8thPayment']) > 0 ){ $m8th = floatval($amount) * ( floatval($option['8thPayment']) / 100) ; }
            if ( floatval($option['9thPayment']) > 0 ){ $m9th = floatval($amount) * ( floatval($option['9thPayment']) / 100) ; }
            if ( floatval($option['10thPayment']) > 0 ){ $m10th = floatval($amount) * ( floatval($option['10thPayment']) / 100) ; }
        }else{

            if ($opt == 0){
                $m1st = floatval($amount) ;
            }

            if ($opt == 1){
                $m1st = floatval($amount) / 2 ;
                $m2nd = floatval($amount) - floatval($m1st);
            }

            if ($opt == 2){
                $m1st = floatval($amount) / 4 ;
                $m2nd = $m1st;
                $m3rd = $m1st;
                $m4th = floatval($amount) - floatval($m1st) - floatval($m2nd) - floatval($m3rd);

                /* custom for IJA Quarterly */
                // $m1st = (floatval($amount) * 30 ) / 100 ;
                // $m2nd = (floatval($amount) * (70/3) ) / 100 ;
                // $m3rd = $m2nd;
                // $m4th =  floatval($amount) - floatval($m1st) - floatval($m2nd) - floatval($m3rd);
                  /* custom for IJA Quarterly */


            }

            if ($opt == 3){
                $m1st = floatval($amount) / 8 ;
                $m2nd = $m1st;
                $m3rd = $m1st;
                $m4th = $m1st;
                $m5th = $m1st;
                $m6th = $m1st;
                $m7th = $m1st;
                $m8th = floatval($amount) - (floatval($m1st) * 7);

                /* custom for IJA Monthly */
                // $m1st = (floatval($amount) * 30 ) / 100 ; // 1st Payment 30%
                // $m2nd = (floatval($amount) * (70/9) ) / 100 ;
                // $m3rd = $m2nd;
                // $m4th = $m2nd;
                // $m5th = $m2nd;
                // $m6th = $m2nd;
                // $m7th = $m2nd;
                // $m8th = $m2nd;
                // $m9th = $m2nd;
                //
                // $m10th =  floatval($amount) - floatval($m1st) - (floatval($m2nd) * 8);
                /* custom for IJA MonthLy */
            }
        }
        $data = array('1' => $m1st,'2' => $m2nd,'3' => $m3rd,'4' => $m4th,'5' => $m5th,'6' => $m6th,'7' => $m7th,'8' => $m8th,'9' => $m9th ,'10' => $m10th);
        return $data;
    }

    public static function createEnrollmentFees($ref,  $fee)
    {
        $mStud = new mStudent();
        $mJournal = new mJournal();
        
        $template = tModel::where('TemplateID', $fee)->first();
        
        $reg = mReg::selectRaw("*, fn_StudentName(StudentNo) As FullName")->where('RegID', $ref)->first();
        $student = $mStud->getStudentInfo($reg->StudentNo);
        
        $status = 1; #default to old student option.
        if( strtolower($student->Status) == 'new'){
            $status = '2';
        }
        
        $fees = mFeesDetails::Accdetails()->selectRaw("es_tableoffee_details.*, a.PaymentOption, a.AcctOption ")
                ->where('TemplateID', $fee )->where('es_tableoffee_details.Inactive','0')
                ->whereRaw("Option In (0,". $status .")")     
                ->get();
    

        #Clear Journals for Enrollment
        $mJournal->where('TransID','1')->where('ReferenceNo',$ref)->delete();
        
        $i=1;
        
        foreach($fees as $f){

            if($template->PaymentScheme == '6'){
                $installment = self::computeInstallment($f->PaymentOption ,$f->Amount);
            }else{
                $installment = array(
                     1 => $f['1stPayment']
                    ,2 => $f['2ndPayment']
                    ,3 => $f['3rdPayment']
                    ,4 => $f['4thPayment']
                    ,5 => $f['5thPayment']
                    ,6 => $f['6thPayment']
                    ,7 => $f['7thPayment']
                    ,8 => $f['8thPayment']
                    ,9 => $f['9thPayment']
                    ,10 => $f['10thPayment']
                );
            }


            $data = array(
                'ServerDate' => systemDate()
                ,'TransDate' => systemDate()
                ,'TermID' => $reg->TermID
                ,'CampusID' => $reg->CampusID
                ,'TransID' => 1
                ,'ReferenceNo' => $ref
                ,'IDType' => '1'
                ,'IDNo' => $reg->StudentNo
                ,'Payor' => $reg->FullName
                ,'AccountID' => $f->AccountID
                ,'CurrencyID' => 0
                ,'Debit' => $f->Amount
                ,'Remarks' => ''
                ,'UserID' => getUserID()
                ,'Assess Fee' => $f->Amount
                ,'ADCRefund' => 0
                ,'FinancialAidExternal' => 0
                ,'Adjustment' => 0
                ,'1st Payment' => $installment['1']
                ,'2nd Payment'  => $installment['2']
                ,'3rd Payment'  => $installment['3']
                ,'4th Payment'  => $installment['4']
                ,'5th Payment' => $installment['5']
                ,'6th Payment' => $installment['6']
                ,'7th Payment' => $installment['7']
                ,'8th Payment' => $installment['8']
                ,'9th Payment' => $installment['9']
                ,'10th Payment' => $installment['10']
                ,'SeqNo' => $i
                ,'SeqNo2' => $i
                ,'DeptID' => $reg->ProgID
                ,'DeptCode' => ''
                ,'YrLvlID' => $reg->YearLevelID
                ,'YrLvlName' => ''
            );

            $i++;

            $mJournal->create($data);
        }

         return true;
    }

    public static function createAssessment($term, $idno, $txn, $ref,  $fee)
    {
        $ret = array('err' => false, 'msg' => 'success');
        $fees = tModel::where('TemplateID', $fee )->first();

        /*Validation of Fees Template*/
        if ( empty($fee) ) {
            $ret = array(
                'err' => true, 'msg' => 'invalid fees template.'
            );
            return $ret;
        }

        switch($txn){
            #Enrollment
            case '1':

                $due = mFeesDues::where('TemplateID', $fee)->first();

                if( getObjectValueWithReturn($due, 'Date1','') ==''){
                    $ret = array(
                        'err' => true, 'msg' => 'Due dates required, Please check due dates.'
                    );
                    return $ret;
                }

                self::createEnrollmentFees($ref,  $fee);

                if($due){
                    $data= array(
                        'TableofFeeID' => $fee
                       ,'FirstPaymentDueDate' => getObjectValueWithReturn($due, 'Date1','NULL')
                       ,'SecondPaymentDueDate' => getObjectValueWithReturn($due, 'Date2','NULL')
                       ,'ThirdPaymentDueDate' => getObjectValueWithReturn($due, 'Date3','NULL')
                       ,'FourthPaymentDueDate' => getObjectValueWithReturn($due, 'Date4','NULL')
                       ,'FifthPaymentDueDate' => getObjectValueWithReturn($due, 'Date5','NULL')
                       ,'SixthPaymentDueDate' => getObjectValueWithReturn($due, 'Date6','NULL')
                       ,'SeventhPaymentDueDate' => getObjectValueWithReturn($due, 'Date7','NULL')
                       ,'EightPaymentDueDate' => getObjectValueWithReturn($due, 'Date8','NULL')
                       ,'NinethPaymentDueDate' => getObjectValueWithReturn($due, 'Date9','NULL')
                       ,'TenthPaymentDueDate' => getObjectValueWithReturn($due, 'Date10','NULL')
                    );
                }else{
                    $data= array(
                        'TableofFeeID' => $fee
                    );
                }


                mReg::where('RegID', $ref)->update($data);

            break;

            #Add/Drop/Change
            case '2': break;

        }

        return $ret;
    }

    public static function purgeAssessment($txn, $ref)
    {
        $ret = array('err' => false, 'msg' => 'success');
        switch($txn)
        {
            case '1':
            case '2':
            case '15':

                $model = new mOthertrans();
                $journal = new mJournal();

                $rs = $model->where('RefNo', $ref)->first();
                if($rs)
                {

                    $journal
                        ->where('TransID', $rs->TransType)
                        ->where('Referenceno', $rs->RefNo)
                        ->where('IDNO', $rs->IDNo)
                        ->where('IDType', $rs->IDType)
                        ->delete();

                    $model->where('RefNo', $ref)->delete();

                    self::recomputeStudentDiscount($rs->RegID);

                }

                $ret = array('err' => false, 'msg' => 'record successfully deleted', 'rs'=>$rs);

            break;
        }

        return $ret;


    }

    public static function createOtherTxn($post, $data)
    {

        $model = new mOthertrans();

        $forms = [
             'TermID' => getObjectValue($post,'TermID')
            ,'CampusID' => '1'
            ,'Date' => systemDate()
            ,'IDType' => getObjectValue($post,'IDType')
            ,'IDNo' => getObjectValue($post,'IDNo')
            ,'TransType' => '1'
            ,'RegID' => getObjectValue($post,'RegID')
            ,'AssessedBy' => getUserID()
            ,'AssessedDate' => systemDate()
            ,'TotalNetAssessed' => getObjectValueWithReturn($post,'Total',0)
            ,'Percentage' => getObjectValue($post,'Percentage')
            ,'SchoOptionID' => getObjectValue($post,'SchoOptions')
            ,'SchoAccounts' => getObjectValue($post,'SchoAccouns')
            ,'GrantTemplateID' => getObjectValue($post,'TemplateID')
            ,'ApplyOnNet' => getObjectValue($post,'IsNet') == '1' ? '1' : '0'
            ,'ApplyOnTotal' => getObjectValue($post,'IsNet') == '0' ? '1' : '0'
        ];


        $rs = $model->create($forms);

        #err_log($idx);

        $journal = new mJournal();
        $i = 0;
        //err_log($data);
        foreach($data as $d){

            $list = array(
                'ServerDate' => systemDate()
                ,'TransDate' => systemDate()
                ,'TermID' => $rs->TermID
                ,'CampusID' => $rs->CampusID
                ,'TransID' => 1
                ,'ReferenceNo' => $rs->RefNo
                ,'IDType' => $rs->IDType
                ,'IDNo' => $rs->IDNo
                ,'Payor' => ''
                ,'AccountID' => $d['id']
                ,'CurrencyID' => 0
                ,'Debit' => $d['amt']
                ,'Remarks' => ''
                ,'UserID' => getUserID()
                ,'Assess Fee' => $d['amt']
                ,'1st Payment' => $d['amt']
                ,'SeqNo' => $i
            );

            $journal->create($list);
            $i++;
        }

        self::recomputeStudentDiscount(getObjectValue($post,'RegID'));

        return 1;
    }

    public static function IsOneReceipt($pORNo , $pTransType , $pTransRefNo, $mTermID) {

        $mTermID = 0;

        $rs = DB::select("SELECT fn_IsOneOfficialReceipt('{$pORNo}', '{$pTransType}', '{$pTransRefNo}') as IsOne,
                          fn_GetAcademicYearTermID('{$pTransType}', '{$pTransRefNo}') as ReturnTermID ");

        foreach($rs as $r){
            $mTermID = ( trimmed($r->IsOne) ? trimmed($r->ReturnTermID) : 0);
        }

        return $mTermID;
    }

    public static function getFeesTemplatebyStudent($term, $idno){

        $mStud = new mStudent();

        $student = $mStud->getStudentInfo($idno);
        $reg = mReg::where(['TermID'=> $term, 'StudentNo' => $idno])->first();

        $status = 1;
        if( strtolower($student->Status) == 'old'){
            $status = '2';
        }

        $foreign = 0;
        $prog = 0;
        $major =0 ;
        $level = 0;

        $template = 0 ;
        $model = new tModel();

        $status = $status == 'New' ? '1':'2';
        $foreign = $student->ForeignStudent == '' ? 0 : $student->ForeignStudent;
        
        $where = [
            'TermID' => $term,
            'ForForeign' => $foreign,
            'Inactive' => 0,
            'YearLevelID' => $reg->YearLevelID
        ];

        $rs = $model->Distribution($reg->ProgID, $reg->MajorID);
        $rs = $rs
                ->selectRaw("*, fn_CurrencyCode(CurrencyID) As Curr ")
                ->where($where)
                ->whereIn('StudentStatus',['0',$status])
                ->get();

        return $rs;
   }

    public static function RecomputeTotalPayment($type, $refno, $idtype, $idno, $asof, $updateterm=0){

        $model = new mReg();

        $h = 0;
        $i = 0;
        $strDQL = "";
        $strDML = "";
        $strSQLWHERE = "";
        $nNonLedger = 0;     /* Compute Total Non Ledger */
        $nActualPayment = 0;  /* Compute Total Actual Payment */

        $TotalRecord = 0 ;
        $mTransRefNo = "";
        $mDebitRefund = 0;
        $mActualPayment = 0;
        $mPaymentDiscount = 0;
        $sTransID= 0 ;
        $sTransType = "";
        $sTransRefNo = "";
        $sDMCMRefNo = "";
        $sEntryID = "";
        $sAccountID = "";
        $sPaymentOption = "";

        $m_TransType = 0;
        $m_TransRefNo = "";
        $lngAffected  = 0;

        If ($updateterm == 1)
        {
            /* Update TermID for OR Entries */
            $vTermID =0;
            $rs = DB::table("vw_Payment_and_Adjustment")
                    ->selectRaw("ReferenceNo")
                    ->whereRaw("TransID = '20' AND TransType = '{$type}' AND TRN_RefNo = '{$refno}'")
                    ->groupBy('ReferenceNo')
                    ->get();


            /* need to debug... zero debit / credit in ledger */
            $i = 1;
            foreach($rs as $r){

                /* Check if one OR - Update Also the TermID in Debit side */
                $vTermID = self::IsOneReceipt(trimmed($r->ReferenceNo), $type, $refno, $vTermID);
                If ( $vTermID ){
                    DB::statement("UPDATE es_journals SET TermID = '{$vTermID}' WHERE TransID = '20' AND ReferenceNo = '{$r->ReferenceNo}'");
                }else{
                    #for review : 2018-06-30 15:14H
                    /* ORIG
                    DB::statement("UPDATE es_journals
                                    SET TermID = fn_GetAcademicYearTermID(TransType, REPLACE(SUBSTRING(TransRefNo, 1, CHARINDEX(';', TransRefNo, 1)), ';', '') )
                                   WHERE TransID = '20' AND ReferenceNo = '{$r->ReferenceNo}' AND Credit > 0 AND TransType = '{$type}'
                                   AND LEFT(TransRefNo," . strlen($refno) . ") = '{$refno}'");
                                   */
                    DB::statement("UPDATE es_journals
                                    SET TermID = fn_GetAcademicYearTermID(TransType, '{$r->ReferenceNo}' )
                                   WHERE TransID = '20' AND ReferenceNo = '{$r->ReferenceNo}' AND Credit > 0 AND TransType = '{$type}'
                                   AND LEFT(TransRefNo," . strlen($refno) . ") = '{$refno}'");


                }
            }
        }

        /* Delete Credit Memo on the Current Transaction (previous error entry) */
        DB::statement("DELETE FROM es_journals WHERE (TransID = '{$type}' AND Referenceno = '{$refno}' AND IDType = '{$idtype}' AND IDNo = '{$idno}') and (Credit > 0) AND (DMCMRefNo <> '')");

        /* Reset Entries */
        DB::statement("UPDATE es_journals SET ActualPayment = 0, PaymentDiscount = 0, CreditMemo = 0, Refund = 0, ADCRefund = 0, Adjustment = 0  WHERE TransID = '{$type}' and Referenceno = '{$refno}' and IDType = '{$idtype}' AND IDNo = '{$idno}'");

        /* Update Payment/Adjustment per account */
        $strDQL = "SELECT TransID, TRN_EntryID, Sum(Debit) as Debit, Sum(Credit) as Credit, SUM(CashierDiscount) AS CashierDiscount, DMCode
                   FROM vw_payment_and_adjustment
                   WHERE (TransType = '{$type}' AND TRN_RefNo = '{$refno}') AND DMCMPosted = 1 GROUP BY  TransID, TRN_EntryID, DMCode";

        $rs = DB::select($strDQL);

        foreach($rs as $r){
            /* official receipts */
            if ( trimmed($r->TransID) == 20 ) {
                    DB::statement("UPDATE es_journals SET PaymentDiscount = (PaymentDiscount + " . $r->CashierDiscount . "), ActualPayment = (ActualPayment + " . trimmed($r->Credit) . ") WHERE EntryID = '{$r->TRN_EntryID}'");
            } elseif( trimmed($r->TransID) == 60  ){
                #debit/credit memo and Add/Drop
                If ($r->DMCode == "Refund") {
                    DB::statement("UPDATE es_journals SET Refund = (Refund + " . $r->Debit . ") WHERE EntryID = '{$r->TRN_EntryID}'");
                } else {
                    DB::statement( "UPDATE es_journals SET CreditMemo = (CreditMemo + " . $r->Credit . ") WHERE EntryID = '{$r->TRN_EntryID}'");
                }

            } elseif( trimmed($r->TransID) == 2  ){
                DB::statement( "UPDATE es_journals SET CreditMemo = (CreditMemo + " . $r->Credit . "), ADCRefund = (ADCRefund + " .$r->Credit. ") WHERE EntryID = '{$r->TRN_EntryID}'");
            }
        }

        #UPDATE Totals

        if ($type == '1') #Enrollment
        {
                #Dim cStudent As cls_Student

                #Set cStudent = New cls_Student
                #cStudent.SetEnrollment_RegNo pRefNo

                $cStudent  = $model->where('RegID', $refno)->first();

                $strDML = "UPDATE es_registrations
                           SET     TotalPreviousBalance = " . getOutstandingBalance($idtype, $idno, $asof, $cStudent->TermID) . ",
                                   TotalAssessment = dbo.fn_TotalOrigAssessment('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalFinancialAid = dbo.fn_TotalFinancialAid('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalNetAssessed = dbo.fn_TotalNetAssessed('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalPayment = dbo.fn_TotalPayment_r2('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalDiscount = dbo.fn_TotalPaymentDiscount('{$type}', '{$refno}', '{$idtype}', '{$idno}', '" . $asof . "'),
                                   TotalNonLedger = " . $nNonLedger . ",
                                   TotalDebitMemo = dbo.fn_TotalDebitMemo('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalCreditMemo = dbo.fn_TotalCreditMemo('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalBalance = ((TotalNetAssessed+TotalDebitMemo) - (TotalPayment+TotalDiscount+TotalCreditMemo)),
                                   TotalFinancialAidExternal = dbo.fn_TotalFinancialAidExternal('{$type}', '{$refno}', '{$idtype}', '{$idno}')
                           WHERE RegID = '{$refno}'";

                #run update
                DB::statement($strDML);
        }
        elseif($type == '2') #ADDCHANGEDROP
        {
                $strDML = "UPDATE es_addchangedropsubjects
                         SET       TotalNetAssessed = dbo.fn_TotalNetAssessed('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalPayment = dbo.fn_TotalPayment_r2('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalDiscount = dbo.fn_TotalPaymentDiscount('{$type}', '{$refno}', '{$idtype}', '{$idno}', '" . $asof . "'),
                                   TotalNonLedger = " . $nNonLedger . ",
                                   TotalDebitMemo = dbo.fn_TotalDebitMemo('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalCreditMemo = dbo.fn_TotalCreditMemo('{$type}', '{$refno}', '{$idtype}', '{$idno}')
                         WHERE RefID = '{$refno}'";


                DB::statement($strDML);
        }
        elseif ( in_array($type, array(0,3,4,5,6,15)) ) #TRANS_GRADUATION, TRANS_ADMISSION, TRANS_OTHERBILLING, TRANS_THESIS, TRANS_TRANSCRIPT, TRANS_BEGBALANCE
        {

                $strDML = "UPDATE es_othertransactions
                         SET       TotalNetAssessed = dbo.fn_TotalNetAssessed('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalPayment = dbo.fn_TotalPayment_r2('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalDiscount = dbo.fn_TotalPaymentDiscount('{$type}', '{$refno}', '{$idtype}', '{$idno}', '" . $asof . "'),
                                   TotalNonLedger = " . $nNonLedger . ",
                                   TotalDebitMemo = dbo.fn_TotalDebitMemo('{$type}', '{$refno}', '{$idtype}', '{$idno}'),
                                   TotalCreditMemo = dbo.fn_TotalCreditMemo('{$type}', '{$refno}', '{$idtype}', '{$idno}')
                         WHERE RefNo = '{$refno}'";


                DB::statement($strDML);
        }

        #Call LogTransaction("RecomputeTotalPayment()", "Recompute Transactions", "TransID:={$type}; ReferenceNo:={$refno}; IDType:={$idtype};IDNo:=" . pIDNo)

    }

    public static function recomputeStudentDiscount($reg_id)
    {
        $rs = DB::table("es_othertransactions as o")
                ->leftJoin('es_journals as j', 'o.RefNo','=','j.ReferenceNo')
                ->selectRaw("j.AccountID, Sum(Debit) As Amt")
                ->whereRaw("o.RegID = '{$reg_id}' and j.TransID = o.TransType")
                ->groupBy('j.AccountID')
                ->get();

        #err_log($rs);

        $strDML = "UPDATE es_journals set Discount = 0, Debit = `Assess Fee` , `1st Payment` = case when `2nd Payment` = 0 then `Assess Fee` else `2nd payment` end   where TransID = '1' AND ReferenceNo ='{$reg_id}'";
        DB::statement($strDML);

        foreach($rs as $r)
        {
            $strDML = "UPDATE es_journals set Discount = '{$r->Amt}', Debit = `Assess Fee` - {$r->Amt} , `1st Payment` = case when `2nd Payment` = 0 then debit else `2nd payment` - {$r->Amt} end   where TransID = '1' AND ReferenceNo ='{$reg_id}' AND AccountID = '{$r->AccountID}'";
            DB::statement($strDML);

        }

        return true;
    }



}
