<?php

namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Registrar\Models\TermConfig as tModel;
use App\Modules\Students\Models\StudentProfile As mStudents;
use App\Modules\Cashier\Services\CashierServices as Services;
use App\Modules\Accounting\Services\Assessment\assessment as assess;
use App\Modules\Accounting\Services\AccountingServiceProvider as spAssess;
use App\Modules\Accounting\Services\DMCMServiceProvider as spDMCM;
use App\Modules\Accounting\Models\DebitCreditMemo as DMCM;

use Permission;
use Request;
use Response;
use DB;

class DebitCreditMemo extends Controller
{
    protected $ModuleName = 'debitcreditmemo';

    private $media = [
            'Title' => 'Adjustments',
            'Description' => 'Debit/Credit Memo',
            'js' => ['Accounting/memo.js?v=1.0.3'],
            'css' => ['profile'],
            'init' => ['Memo.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                           ,'bootstrap-datepicker/js/bootstrap-datepicker'
                           ,'bootstrap-timepicker/js/bootstrap-timepicker.min'

                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification'
                ,'bootstrap-datepicker/css/datepicker3'
                ,'bootstrap-timepicker/css/bootstrap-timepicker.min'

            ],
            'closeSidebar' => true
        ];

    private $url = ['page' => 'accounting/memo'];

    private $views = 'Accounting.Views.memo.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();

        /*
        if (!$this->permission->has('read')) {
            return view(config('app.403'));
            die();
        }
        */



        $_incl = [
             'views' => $this->views
            ,'total' => 0

        ];

        SystemLog( $this->media['Title'] ,'','Page View','page-view','','' );

        return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
 	}

    public function create()
 	{
 		$this->initializer();
        $term = array();
        $stud = array();
        $assess = array();
        $balance= 0 ;
        $idno = decode(Request::get('idno'));

        if( $idno != ''){
            $stud = $this->model
                    ->where('StudentNo', $idno)
                    ->selectRaw("StudentNo, AppNo, LastName, FirstName, ProgID, YearLevelID, fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel, CurriculumID, fn_CurriculumCode(CurriculumID) AS CurriculumCode , fn_MajorName(MajorDiscID) As Major ")
                    ->first();
            $term = Services::getAssessment($idno,'1');
            $m_assess = new assess();
            $assess = $m_assess->get_studentjournal($term[0]->TermID,$idno,$term[0]->RegID);
            $balance = number_format( getOutstandingBalance('1',$idno, systemDate()), 2 , ".", ",") ;
        }

        $_incl = [
             'views' => $this->views
            ,'idno' => $idno
            ,'data' => $stud
            ,'balance' => $balance
            ,'term' => $term
            ,'assess' => $assess
            ,'total' => 0

        ];

        SystemLog( $this->media['Title'] ,'','Page Create View','page-create-view','','' );

        return view('layout',array('content'=>view($this->views.'create.form')->with($_incl),'url'=>$this->url,'media'=>$this->media));

    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event'))
            {
                case 'save-record':

                    $dmcm   = new spDMCM();
                    $refno  =  $dmcm->saveRecord();
					$idno   = ((getObjectValue($post,'idno')!='')?0:decode(getObjectValue($post,'idno')));
					$regid  = 0;
					$recalc = $this->assess->recalc_assessment($idno,$regid);
                    SystemLog( $this->media['Title'] ,'','Save DMCM','create new dmcm','IDType: 1, IDNo: '. decode(getObjectValue($post,'idno')) .' Name: '. getObjectValue($post,'name')  ,'' );

                    $response = successSave(['ref' => $refno]);

                break;

                case 'void':

                    $ref = (Request::get('ref'));
                    $rs = DB::table('es_debitcreditmemo')->where('RefNo', $ref)->first();

                    #deleted details
                    DB::table('es_journals')
                            ->where('TransID','60')
                            ->where('Referenceno', $ref)
                            ->delete();

                    $data = [
                        'IsVoid' => '1'
                        ,'Explanation' => '*** Void ****'
                        ,'VoidDate' => systemDate()
                        ,'Posted' => '0'
                        ,'ModifiedBy' => getUserID()
                        ,'ModifiedDate' => systemDate()
                    ];

                    #update to void info.
                    DB::table('es_debitcreditmemo')->where('RefNo', $ref)->update($data);

                    #Recompute payment
                    $mAssess = new spAssess();
                    $mAssess::recomputeTotalPayment($rs->TransType, $rs->TransNo, $rs->IDType, $rs->IDNo, systemDate());


                    SystemLog( $this->media['Title'] ,'','Void DMCM','user void dmcm entry',
                    'RefNo:'. $ref .' '.
                    'IDNo:'. $rs->IDNo .' '.
                    'Explanation :'. $rs->Explanation .' '.
                    'TransType :'. $rs->TransType .' '.
                    'TransNo :'. $rs->TransNo
                    ,'' );

                    $response = successDelete();

                break;

                case 'get-subjects':

                    $reg = (Request::get('reg'));

               	    $data = DB::table('vw_studentload')->where('RegID', $reg)->get();

                    $vw  = view($this->views.'subjects', ['subj' => $data ])->render();

                    $response = Response::json(['success' => true,'data' => $vw,'error'   => false,'message' => '']);

                break;

                case 'assessment':

                    $reg = (Request::get('refno'));
                    $term = decode(Request::get('term'));
                    $idno = decode(Request::get('idno'));

                    $vw  = $this->getAssessment($term,$idno,$reg);
                    #err_log($data);
                    #$vw  = view($this->views.'create.assess', ['data' => $data ])->render();
                    
                    $response = Response::json(['success' => true,'view' => $vw,'error'   => false,'message' => '']);

                break;

                case 'list':
				    ini_set('max_execution_time', 300);
                    $term = decode(Request::get('term'));
					if($term==0){
					  $tmpid = DB::SELECT('SELECT TermID FROM  ES_AYTerm WHERE IsCurrentTerm=1');
					  $term  = (($tmpid && count($tmpid)>0)?($tmpid[0]->TermID):0);
					}
					
                    $datas =array(
                     'data'=>  DMCM::selectRaw("TOP 500 *, dbo.fn_StudentName(IDNo) As FullName ")->where('TermID',$term)->orderBy('RefNo','DESC')->get(),
                     'term' => $term,
                     'total' => 0,
                    );

                    $vw  = view($this->views.'list', $datas)->render();
                    $response = Response::json(['success' => true,'data' => $vw,'error'   => false,'message' => '']);

                break;


                case  'get-refs':

                    $idno = decode($post['idno']);
                    $type = $post['type'];
                    $txn = $post['txn'];
                    $all = $post['all'];

                    if($idno != '' ){

                        $obalance = getOutstandingBalance($type , $idno, systemDate() );

                        $data =  $this->services->setPayer($type, $idno, $txn);
                        $refs =  view($this->views.'create.refs',['data' =>  $data['refs']])->render();

                        $data['details']['obalance'] = $obalance;
                        $data['details']['regid'] = (($data['refs'] && count($data['refs'])>0)?($data['refs'][0]->RegID):'');
                        $data['details']['all'] = $all ;

                        $idx = (($data['refs'] && count($data['refs'])>0)?($data['refs'][0]->RegID):'');

                        $assessment = $this->getAssessment((($data['refs'] && count($data['refs'])>0)?($data['refs'][0]->TermID):0),$idno,$idx);



                        unset($data['details']);
                        unset($data['refs']);

                        $response = ['error'=>false,'message'=>'success', 'data' => $refs,'assessment' => $assessment ,
                        'idx' => $idx,
                        'ref' => $data, 'obalance' => number_format($obalance,2) ];

                    }else{
                        $response = ['error'=>true,'message'=>'no selected payer'];
                    }

                break;

                default: return response('Unauthorized.', 401); break;
            }
        }
        ob_clean();
        return $response;
    }

    private function getAssessment($term, $idno, $regid ){
        $m_assess = new assess();
        $assess = $m_assess->get_studentjournal($term,$idno,$regid);
        return view($this->views.'create.assess',  ['data' => $assess ] )->render();
    }

    private function initializer()
    {
        $this->model = new mStudents();
        $this->permission = new Permission($this->ModuleName);
        $this->services = new Services();
        $this->assess = new assess();
    }
}
