<?php
namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Services\Assessment\assessment as assess;

use DB;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\pdf\TCPDF as tpdf;
use App\Libraries\pdf\TCPDF;
use Response;
use Request;
use Input;
use Permission;

class Reports extends Controller
{
	protected $ModuleName = 'Accounting Reports';

    private $media = [
            'Title' => 'Accounting Reports',
            'Description' => 'Welcome To Accounting Reports!',
            'js' => ['accounting/report'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'accounting/reports'];

    private $views = 'Accounting.Views.Report.';
	
	
	private $rparam  = array('campus'     =>'CampusID'
	                        ,'college'    =>'CollegeID'
							,'ayterm'     =>'TermID'
							,'program'    =>'ProgID'
							,'yrlvl'      =>'YearLevelID'
							,'asof'       =>'DateAsOf'
							,'backaccount'=>'IncludeBackAccount'
							,'baonly'     =>'BackAccountOnly'
							,'zerobal'    =>'IncludeZeroBalance'
							,'notvalid'   =>'IncludeNotValidated');
	
	private $rpdefault= array('campus'    =>1
	                        ,'college'    =>0
							,'ayterm'     =>1
							,'program'    =>0
							,'yrlvl'      =>0
							,'asof'       =>"'01/01/2000'"
							,'backaccount'=>1
							,'baonly'     =>0
							,'zerobal'    =>0
							,'notvalid'   =>0
							,'bycollege'  =>0
							,'byprogram'  =>0);
							
	private $reports = array(
	                     'studdetail' => array('name'=>'Student Details','sp'=>'rpt_StudentDetails','file'=>'Accounting/StudentDetails.rpt','logo'=>1),
	                     'payledger'  => array('name'=>'Student Payment Ledger','sp'=>'rpt_StudentDetails','file'=>'Accounting/StudentPaymentLedger.rpt','logo'=>1),
	                     'unpaid'     => array('name'=>'Student Unpaid Fees','sp'=>'rpt_StudentDetails','file'=>'Accounting/StudentUnpaidFees.rpt','logo'=>1),
	                  );
	
	public function index(){
		$this->initializer();
		//if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
        //}
        //return view(config('app.403'));
	}
	
	public function print_report(){
        $response = ['error' => true, 'message' => 'No Event Selected'];
		$report   = Request::get('report');
		$get      = Request::all();
		$param    = '';
		
		foreach($this->rparam as $k=>$v){
		   if(array_key_exists($k,$get) && $get[$k]!=''){
			   if($k=='notvalid' || $k=='zerobal' || $k=='baonly' || $k=='backaccount'){
				   $param .= (($param!='')?',':'').'@'.$v.'=1';
			   }else{
			       $param .= (($param!='')?',':'').'@'.$v."='".$get[$k]."'";
			   }
		   }else{
		       $param .= (($param!='')?',':'').'@'.$v."=".$this->rpdefault[$k];
		   }
		}
		
		if(array_key_exists($report,$this->reports)){
			$this->initializer();
			$this->xpdf->filename = $this->reports[$report]['file'];
		    $this->xpdf->query    = "EXEC ".$this->reports[$report]['sp']." ".$param;
		    $subrpt = array(
                           'subreport1' => array('file' =>'Company_Logo','query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'")
		                  ); 
		    $this->xpdf->SubReport=$subrpt;				   
		    $data = $this->xpdf->generate();
		    
		    header('Content-type: application/pdf');
		    header('Content-Disposition: inline; filename="' . $data['filename'] . '"');
		    header('Content-Transfer-Encoding: binary');
		    header('Accept-Ranges: bytes');
		    @readfile($data['file']);
		}else{
			echo "Report Doesn't Exist!";
		}
	}
	
	public function export_report(){
        $response = ['error' => true, 'message' => 'No Event Selected'];
		$report   = Request::get('report');
		$typeid   = Request::get('typeid');
		$get      = Request::all();
		$param    = '';
		
		foreach($get as $g=>$v){
		   if(array_key_exists($g,$this->rparam) && $v!=''){
			   $param .= (($param!='')?',':'').'@'.$this->rparam[$g]."='".$v."'";
		   }
		}
		
		if(array_key_exists($report,$this->reports)){
			$this->initializer();
			$this->xpdf->formatType = $typeid;
			$this->xpdf->extension  = 'xls';
			$this->xpdf->filename   = $this->reports[$report]['file'];
		    $this->xpdf->query      = "EXEC ".$this->reports[$report]['sp']." ".$param;
		    $subrpt = array(
                           'subreport1' => array('file' =>'Company_Logo','query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'")
		                  ); 
		    $this->xpdf->SubReport=$subrpt;				   
		    $data = $this->xpdf->generate();
			
			$data['filename']  = str_replace('rpt','xls',$data['filename']);

		    $xls = url('assets/system/reports/'.$data['filename']);
		    header("Location:".$xls);
		    exit();
		    
		  //$data['filename']  = str_replace('rpt','xls',$data['filename']);
		  //header('Content-type: application/vnd.ms-excel');
		  //header('Content-Disposition: inline; filename="' . $data['filename'] . '"');
		  //header('Content-Transfer-Encoding: binary');
		  //header('Accept-Ranges: bytes');
		  //@readfile($data['file']);
		}else{
			echo "Report Doesn't Exist!";
		}
	}
	
    private function initializer(){
	   ini_set('max_execution_time', 300);
	   $this->assess = new assess;
	   $this->tpdf   = new tpdf;
	   $this->xpdf   = new xpdf;
    }
	
	public function init($apage='assess'){
		$this->initializer();
		$data = array(
		   'ayterm'   => DB::select("SELECT * FROM ES_AYTerm"),
		   'campus'   => DB::select("SELECT * FROM ES_Campus"),
		   'reports'  => $this->reports,
		   'programs' => DB::select("SELECT * FROM ES_Programs"),
		   'yrlvl'    => DB::select("SELECT * FROM ESv2_Yearlevel"),
		   
		);
		return $data;
	}
}
?>