<?php
namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Services\Assessment\assessment as assess;
use App\Modules\Accounting\Models\StudentDiscount as dmodel;

use DB;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;

class StudentDiscount extends Controller{
   private $media = ['Title'       => 'Student Discount',
					 'Description' => 'Student Discount',
					 'js'          => ['Accounting/studentdiscount'],
					 'plugin_js'   => ['bootbox/bootbox.min',
									   'bootstrap-select/bootstrap-select.min',
									   'datatables/media/js/jquery.dataTables.min',
									   'datatables/extensions/TableTools/js/dataTables.tableTools.min',
									   'datatables/extensions/Scroller/js/dataTables.scroller.min',
									   'datatables/plugins/bootstrap/dataTables.bootstrap',
									   'jquery-validation/js/jquery.validate.min',
									   'jquery-validation/js/additional-methods.min',
									   'bootstrap-wizard/jquery.bootstrap.wizard.min',
									   'SmartNotification/SmartNotification.min'
									],
				  /* 'init'        => ['SECURITY.init()'], */
					 'plugin_css'  => ['bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification',],	
					];
    
   private $url =  [
        'page'  => 'accounting/student-discount/',
        'form' => 'form',
    ];
    
   public $r_view = 'Accounting.Views.StudentDiscount.';
   
   public function index(){
	    $this->initializer();
		//if ($this->permission->has('read')) {
		ini_set('memory_limit', '-1');
        return view('layout',array('content'=>view($this->r_view.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
        //}
		//return view(config('app.403'));
   }
   
   public function initializer(){
     $this->dmodel = new dmodel;
     $this->assess = new assess;
   }
   
   public function init(){
		return array(
		        'rviews'  =>$this->r_view
		       ,'students'=>$this->dmodel->getStudentList()   
		    );
   }
   
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event')){
				case 'discount':
                    $termid     = 0;
                    $refno      = Request::get('refno');
                    $studno     = Request::get('studno');
					$disc       = $this->assess->get_givendiscount($termid,$studno,$refno);
					$discount   = (string)view($this->r_view.'sub.discount',array('disc'=>$disc));
				    $response   = Response::json(array('success'=>true,'content'=>$discount));
				break;
			}
            return $response;
        }
        return $response;			
	}			
}
?>