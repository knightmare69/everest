<?php

namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Enrollment\Models\mRegistrationConf as Model;

use Permission;
use Request;
use Response;
use DB;

class DueDateSetup extends Controller
{
    protected $ModuleName = 'due-date-setup';

    private $media = [
            'Title' => 'Due Date Setup',
            'Description' => 'Welcome to fees due dates setup.',
            'js' => ['Accounting/dues'],            
            'init' => ['MOD.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                            ,'bootstrap-datepicker/js/bootstrap-datepicker'
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification'
                ,'bootstrap-datepicker/css/datepicker'
            ],
        ];

    private $url = ['page' => 'accounting/due-date-setup'];

    private $views = 'Accounting.Views.Duedates.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));

            $_incl = [
                'views' => $this->views,
                'data' => $this->getData($term),
                'term' => $term,
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($term){

        ini_set('max_execution_time', 120);

        $ret = null;
        if($term != ''){
                                       
             $rs = DB::table('es_ayterm As a')             
                    ->leftJoin('es_registrationconfig_dates as d','a.TermID','=','d.TermID')
                    ->selectRaw("a.TermID as `key`, d.* ")
                    ->where('a.TermID', $term)
                    ->where('d.Classification','0'); 
           

            $ret = $rs->get();
        }

        return $ret;
    }

   
    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                
                case 'save-template':
                    
                   	if ($this->permission->has('add')) {
						$validation = $this->services->isValid(Request::all());
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
                            $index = Request::get('idx');
                            $data = $this->services->set_template_data(Request::all());
                            if($index == ''){
                                
                                $this->model->create($data);                                
                                SystemLog( $this->media['Title'] ,'Feestemplate','event','Create Template',json_encode($data),'' );
                                
                            }else{
                                
                                $this->model->where('TemplateID', decode( $index)) ->update($data);                                
                                SystemLog( $this->media['Title'] ,'Feestemplate','event','Update Template',json_encode($data),'' );
                                
                            }
														
							$response = ['error'=>false,'message'=>'Successfully Save!'];
						}
					}

                break;

                case 'delete-template':

                    $template = decode(Request::get('template'));

                    $rs = $this->model->select(["TemplateCode"])->where(['TemplateID' => $template ])->first();                    
                    
                    $code = $rs->TemplateCode;
                    
                    DB::statement("DELETE FROM es_tableoffees WHERE TemplateID = '{$template}'");
                    DB::statement("DELETE FROM es_tableoffee_distributionlist WHERE TemplateID = '{$template}'");
                    DB::statement("DELETE FROM es_tableoffee_details WHERE TemplateID = '{$template}'");
                    
                    SystemLog( $this->media['Title'] , 'Fees template', 'event', 'Delete Fee', 'ID:'. $template . ', Template:'. $code , 'Delete Template ');                                                                   

                    $response = successDelete();

                break;                                             

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->model = new model();
        $this->permission = new Permission($this->ModuleName);
    }
}