<?php
namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Services\Assessment\assessment as assess;

use DB;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;

class Accounting extends Controller{
   private $media = [ 'Title' => 'Assessment/Billing',
        'Description'   => 'Accounting',
		'closeSidebar'  => true,
        'js'            => ['Accounting/assessment'],
        'plugin_js'     => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
							'jquery-validation/js/jquery.validate.min',
							'jquery-validation/js/additional-methods.min',
							'bootstrap-wizard/jquery.bootstrap.wizard.min',
                            'SmartNotification/SmartNotification.min'
                           ],
	 /* 'init'          => ['SECURITY.init()'], */
        'plugin_css'    => ['bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification',],	
    ];
    
    private $url =  [
        'page'  => 'accounting/',
        'form' => 'form',
    ];
    
	public $r_view = 'Accounting.Views.';
	
    public function index()
    {
		$this->initializer();
		//if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->r_view.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
        //}
        return view(config('app.403'));
    }
	
	public function recompute(){
		$this->initializer();
		die();
	}
	
	public function soa(){
		$this->initializer();
		die();
	}
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax()){
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event')){
				case 'seltemplate':
				  $content = '';
				  $termid  = Request::get('termid');
                  $studno  = Request::get('studno');
                  $progid  = Request::get('progid');
                  $yrlvl   = Request::get('yrlvl');
				  $exec    = DB::select("SELECT  t.*,
												 (CASE WHEN t.PaymentScheme=0 THEN 'Full Payment' WHEN t.PaymentScheme=1 THEN 'Semestral' ELSE 'Quarterly' END) AS SchemeName
											FROM ES_TableofFees as t
									  INNER JOIN ES_TableofFee_DistributionList as d ON t.TemplateID=d.TemplateID
									  INNER JOIN ESv2_YearLevel as y ON d.YearLevelID=y.YLID_OldValue AND d.ProgID=y.ProgID
										   WHERE t.TermID                = '".$termid."'
											 AND RIGHT(t.TemplateCode,3) = (SELECT (CASE WHEN TermID='".$termid."' AND AppNo<>'' THEN 'NEW' ELSE 'OLD' END) as Status FROM ES_Students WHERE StudentNo='".$studno."')
											 AND y.YearLevelID           = '".$yrlvl."'
										ORDER BY t.PaymentScheme");
				  foreach($exec as $r){
					 $content .= '<option value="'.$r->TemplateID.'" data-scheme="'.$r->PaymentScheme.'">'.$r->SchemeName.'</option>'; 
				  }						
				  $response = Response::json(array('success'=>true,'content'=>$content));						
				break;
                case 'filter':
				    $type     = Request::get('type');
                    $args     = Request::get('args');
					$exec     = $this->assess->get_filter($type,$args);
					$content  = (string)view($this->r_view.'sub.tblfilter',array('type'=>$type,'list'=>$exec));	
                    $response = Response::json(array('success'=>true,'content'=>$content));
                break;
				case 'reginfo':
                    $args       = Request::get('args');
					$exec       = $this->assess->get_studentinfo($args);
					$check      = $this->auto_sibling($args);
					$content    = "";
					foreach($exec as $r){
					  $content .= '<option value="'.$r->RegID.'" data-termid     = "'.$r->TermID.'" 
					                                             data-ayterm     = "'.$r->AYTerm.'" 
					                                             data-regdate    = "'.(($r->RegDate<>'')?date('m/d/Y',strtotime($r->RegDate)):'').'"
					                                             data-validator  = "'.(($r->ValidatingOfficerID<>'')?$r->ValidatingOfficerID:'').'"
					                                             data-validate   = "'.(($r->ValidationDate<>'')?date('m/d/Y',strtotime($r->ValidationDate)):'').'"
					                                             data-templateid = "'.(($r->TableofFeeID<>'')?$r->TableofFeeID:'').'"
																 data-template   = "'.(($r->TemplateCode<>'')?$r->TemplateCode:'').'"
																 data-studstats  = "'.$r->StudentStatus.'"
																 data-yrlvlid    = "'.$r->YearLevelID.'"
																 data-xyrlvlid   = "'.$r->xYearLevelID.'"
																 data-yrlvl      = "'.$r->YearLevelName.'"
																 data-progid     = "'.$r->ProgID.'"
																 data-progname   = "'.$r->ProgName.'"
																 data-studpic    = "'.url() .'/general/getPupilPhoto?Idno='. encode($r->StudentNo).'">'.$r->RegID.'</option>';	
					}
				    $response = Response::json(array('success'=>true,'content'=>$content,'record'=>$exec,'discount'=>$check));
                break;
				case 'assessment':
                    $termid     = Request::get('termid');
                    $refno      = Request::get('refno');
                    $studno     = Request::get('studno');
                    $templid    = Request::get('templid');
					$exec       = $this->assess->exec_assessment($termid,$studno,$refno,$templid);//$this->assess->get_studentjournal($termid,$studno,$refno);
					$disc       = $this->assess->get_givendiscount();
					$otxn       = $this->assess->load_othertxn($termid,$studno,$refno);
					$content    = (string)view($this->r_view.'sub.tblassessment',array('assess'=>$exec));
                    $discount   = (string)view($this->r_view.'sub.tbldiscount',array('disc'=>$disc));
				    $response   = Response::json(array('success'=>true,'content'=>$content,'discount'=>$discount,'othertxn'=>$otxn));					
				break;
				case 'journal':
                    $termid     = Request::get('termid');
                    $refno      = Request::get('refno');
                    $studno     = Request::get('studno');
                    $templid    = Request::get('templid');
					$exec       = $this->assess->exec_assessment($termid,$studno,$refno,$templid);//$this->assess->get_studentjournal($termid,$studno,$refno);
					$disc       = $this->assess->get_givendiscount();
					$otxn       = $this->assess->load_othertxn($termid,$studno,$refno);
                    $content    = (string)view($this->r_view.'sub.tbljournal',array('assess'=>$exec,'details'=>array('termid'=>$termid,'studno'=>$studno,'regid'=>$refno,'feesid'=>$templid)));
                    $discount   = (string)view($this->r_view.'sub.tbldiscount',array('disc'=>$disc));
					
					$response   = Response::json(array('success'=>true,'content'=>$content,'discount'=>$discount,'othertxn'=>$otxn));					
				break;
				case 'templatelist':
                    $termid     = Request::get('term');
                    $yrlvl      = Request::get('yrlvl');
                    $progid     = Request::get('progid');
                    $scheme     = Request::get('scheme');
				    $exec       = $this->assess->get_templatefee($termid,$yrlvl,$progid,$scheme);
					$content    = "";
				    if($exec){
						foreach($exec as $r){
						  $content .= '<tr data-list="'.$r->TemplateID.'" data-code="'.$r->TemplateCode.'">
						                <td>'.$r->TemplateID.'</td>  
						                <td>'.$r->TemplateCode.'</td>  
						               </tr>'; 	
						}
					}else
                      $content  = '';	
				  
				    $response = Response::json(array('success'=>true,'content'=>$content));	
				break;
				case 'template':
                    $termid     = Request::get('termid');
                    $refno      = Request::get('refno');
                    $studno     = Request::get('studno');
					$templid    = Request::get('templid');
				    $exec       = $this->assess->exec_assessment($termid,$studno,$refno,$templid);//$this->assess->get_studenttemplate($refno);
					if($exec){
					  $content  = (string)view($this->r_view.'sub.tbljournal',array('assess'=>$exec));
                    }else
                      $content  = '';	
				  
				    $response = Response::json(array('success'=>true,'content'=>$content));					
				break;
				case 'discount':
                    $termid     = Request::get('termid');
                    $refno      = Request::get('refno');
                    $studno     = Request::get('studno');
					$disc       = $this->assess->get_givendiscount($termid,$studno,$refno);
					$discount   = (string)view($this->r_view.'sub.tbldiscount',array('disc'=>$disc));
				    $response   = Response::json(array('success'=>true,'content'=>$discount));	
				break;
				case 'schoprovider':
				    $refno    = Request::get('provid');
				    $content  = array();	
					$scholar  = $this->assess->get_schoproviderinfo($refno);
					$template = $this->assess->get_schogranttemplate($refno);
					$opttempl = '';
					foreach($template as $t){
					  $opttempl .= '<option value="'.$t->GrantTemplateID.'">'.$t->TemplateCode.'</option>';	
					}
				    $response = Response::json(array('success'=>true,'content'=>$content,'template'=>$opttempl));			
				break;
				case 'savediscount':
				    $xpost    = Request::all();
				    $exec     = $this->assess->save_studentdiscount($xpost);
					$response = Response::json(array('success'=>true,'content'=>$exec));	
				break;
				case 'rem_discount':
				    $refno      = Request::get('refno');
					$exec       = DB::statement("DELETE FROM ES_OtherTransactions WHERE RefNo='".$refno."'");
					$response   = Response::json(array('success'=>true,'content'=>'Delete Successfully'));					
				break;
				case 'other_acct':
				    $xpost    = Request::all();
					$termid   = Request::get('termid');
                    $studno   = Request::get('studno');
					$refno    = Request::get('regid');
                    $accts    = Request::get('other');
				    $exec     = $this->assess->save_othertxn($termid,$studno,$refno,$accts);
					$response   = Response::json(array('success'=>true,'content'=>'Successfully Saved'));					
				break;
				case 'other_rem':
				    $xpost    = Request::all();
					$entryid  = Request::get('entryid');
				    $exec     = DB::statement("DELETE FROM ES_Journals WHERE EntryID='".$entryid."' AND (ActualPayment=0 OR CreditMemo=0)");
					$response = Response::json(array('success'=>true,'content'=>'Successfully Saved'));					
				break;
				case 'other_load':
				    $xpost    = Request::all();
					$termid   = Request::get('termid');
                    $studno   = Request::get('studno');
					$refno    = Request::get('regid');
				    $exec     = $this->assess->load_othertxn($termid,$studno,$refno);
					$response   = Response::json(array('success'=>true,'content'=>''));					
				break;
				case 'other_clear':
				    $xpost    = Request::all();
					$termid   = Request::get('termid');
                    $studno   = Request::get('studno');
					$refno    = Request::get('regid');
				    $exec     = $this->assess->rem_othertxn($termid,$studno,$refno);
					$response = Response::json(array('success'=>true,'content'=>''));					
				break;
			}
            return $response;
        }
        return $response;			
	}

	public function botfunc(){
	  ini_set('max_execution_time', 3600);
	  $start_time = microtime(true);
	  $success = false;
	  $studno   = Request::get('studno');
	  $slist   = DB::select("SELECT r.* 
	                           FROM ES_Registrations as r
						 INNER JOIN ES_AYTerm as a ON r.TermID=a.TermID
							  WHERE a.Active_OnlineEnrolment=1 
							    AND r.TableofFeeID>0 
								".(($studno!='' && $studno)?(" AND r.StudentNo='".$studno."'"):"")."
						   ORDER BY RegID DESC");
	  if($slist && count($slist)>0){
	   foreach($slist as $r){		
          $this->initializer();			   
		  $termid  = $r->TermID;
		  $refno   = $r->RegID;
		  $studno  = $r->StudentNo;
		  $templid = $r->TableofFeeID;
		  $exec    = $this->assess->exec_assessment($termid,$studno,$refno,$templid);
	      if($exec){
		    $success = true;
		    echo 'StudentNo:'.$studno.'<br/>';
		  }else{
		    echo 'FailedNo:'.$studno.'<br/>';
		  }
	   }
	  
	  }
	  $end_time = microtime(true);
      $exec_time = ($end_time - $start_time); 
  
      echo "<br/><br/> Execution time of script = ".$exec_time."s <br/>";
	  die(($success)?'Done':'Done with error');
	}
	
    private function initializer()
    {
	   ini_set('max_execution_time', 300);
		$this->assess = new assess;
    }
	
	public function init($apage='assess')
	{
		$this->initializer();
		$data = array();
		return $data;
	}	
	
	private function auto_sibling($stdno=''){
	  if($stdno!=''){
	    $exec = DB::SELECT("SELECT TOP 1 *,'".getTotalSiblingsX($stdno)."' as SiblingCount FROM ES_Registrations WHERE StudentNo='".$stdno."' ORDER BY RegID DESC");
		if($exec && count($exec)>0){
		  $rs     = $exec[0];
		  $termid = $rs->TermID;
		  $regid  = $rs->RegID;
		  $count  = $rs->SiblingCount;
		  $grant  = 1;
		  $percent= 10;
		  $check  = DB::select("SELECT * FROM ES_OtherTransactions WHERE TermID='".$termid."' AND IDNo='".$stdno."' AND SchoProviderID=6");
	    //if($count>1 && count($check)<=0){
		  if($rs->ValidationDate=='' && $count>1 && count($check)<=0){
		    switch($count){
			   case 2:
			      $grant  = 1;
		          $percent= 10;
			   break;
			   case 3:
			      $grant  = 3;
		          $percent= 15;
			   break;
			   case 4:
			      $grant  = 4;
		          $percent= 20;
			   break;
			   case 5:
			      $grant  = 5;
		          $percent= 25;
			   break;
			}
			$data = array(
	             'TermID'           => $termid
	            ,'CampusID'         => 1
	            ,'TransType'        => 1
	            ,'IDType'           => 1
	            ,'IDNo'             => $stdno
	            ,'RegID'            => $regid
	            ,'SchoProviderID'   => 6
	            ,'SchoProviderType' => 1
	            ,'SchoOptionID'     => 2
	            ,'SchoAccounts'     => 0
	            ,'GrantTemplateID'  => $grant
	            ,'SchoFundType'     => 0
	            ,'TotalDiscount'    => 0
	            ,'Percentage'       => $percent
	            ,'ApplyOnNet'       => 0	   
				,'AssessedBy'       =>  getUserID()       
				,'AssessedDate'     =>  date('Y-m-d')       
			 );			
	        $exec = DB::table('ES_OtherTransactions')->insert($data); 
			return $exec;
		  }else{
		    return -1;
		  }
		}
	  }else{
	    return 0;
	  }
	}
	
}
?>