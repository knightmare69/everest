<?php

namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Models\TableOfFees as tModel;
use App\Modules\Accounting\Models\TableOfFees_Details as fModel;
use App\Modules\Accounting\Models\TableOfFees_Programs as pModel;

use App\Modules\Accounting\Services\FeesTemplateServiceProvider as services;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF;

use Permission;
use Request;
use Response;
use DB;

class SubjectWithFee extends Controller
{
    protected $ModuleName = 'subject-with-fee';

    private $media = [
            'Title' => 'Subject with Fees',
            'Description' => 'Welcome to subject with fees',
            'js' => ['Accounting/subject-with-fee'],
            'css' => [''],
            'init' => ['MOD.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',                            
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                            'smartnotification/smartnotification.min'
                        ],

            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
                'smartnotification/smartnotification',
            ],
        ];

    private $url = ['page' => 'accounting/subject-with-fee'];

    private $views = 'Accounting.Views.SubjectWithFee.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {

            $term = decode(Request::get('t'));
            $f = Request::get('f');
            $v = Request::get('v');

            $_incl = [
                'views' => $this->views,
                'data' => $this->getData('list',$term,$f,($v == 'undefined' ? 1 : $v)),
                'total' => $this->getData('total',$term,$f),
                'term' => $term,
                'filter'=>$f,
                'page'=> ($v == 'undefined' ? 1 : $v)
            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));

 	}

    private function getData($mode,$term, $filter='', $page=1){

        ini_set('max_execution_time', 120);

        $whereRaw = "";

        $take = 50;
        $skip = ($page == 1 ? 0 : ($take * $page) );
        $rs = null;
        if($term != ''){

            $whereRaw = "a.TermID = {$term} ";
           // $whereRaw .= " AND r.ProgID IN (".implode(getUserProgramAccess(),',') .")";

            $query = "SELECT a.*, fn_CurrencyCode(a.CurrencyID) As Curr 
                      FROM  es_tableoffees a                        
                      WHERE " . $whereRaw . " Order By TemplateCode ASC
                      OFFSET     {$skip} ROWS
    					 FETCH NEXT {$take}  ROWS ONLY
                      " ;
            //err_log($query);
            if($mode == 'total'){
                $rs =  DB::table('es_tableoffees As a')                        
                        ->whereRaw($whereRaw)
                        ->count();
            }else{
                $rs = DB::select($query);
            }
        }

        return $rs;
    }

    public function view(){
        $this->initializer();
        if ($this->permission->has('read')) {
            
            $term = decode(Request::get('t'));
            $ref = decode(Request::get('ref'));            

            $_incl = [
                'views' => $this->views
                ,'term' => $term                               
                ,'ref' => ($ref)
            ];
            
            SystemLog( $this->media['Title'] ,'','Page View','page-view','','' );
            return view('layout',array('content'=>view($this->views.'view')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {

                case 'save-template':
                    
                   	if ($this->permission->has('add')) {
						$validation = $this->services->isValid(Request::all());
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
                            $index = Request::get('idx');
                            $data = $this->services->set_template_data(Request::all());
                            if($index == ''){
                                
                                $this->model->create($data);                                
                                SystemLog( $this->media['Title'] ,'Feestemplate','event','Create Template',json_encode($data),'' );
                                
                            }else{
                                
                                $this->model->where('TemplateID', $index)->update($data);                                
                                SystemLog( $this->media['Title'] ,'Feestemplate','event','Update Template',json_encode($data),'' );
                                
                            }
														
							$response = ['error'=>false,'message'=>'Successfully Save!'];
						}
					}

                break;

                case 'delete-template':

                    $template = decode(Request::get('template'));

                    $rs = $this->mFee->selectRaw("dbo.fn_TemplateCode(TemplateID) As TemplateCode, dbo.fn_AccountName(AccountID) As AccountName ")->where(['IndexID' => $template ])->first();                    
                    
                    DB::statement("DELETE ES_TableofFees WHERE TemplateID = '{$template}'");
                    DB::statement("DELETE ES_TableofFee_DistributionList WHERE TemplateID = '{$template}'");
                    DB::statement("DELETE ES_TableofFee_Details WHERE TemplateID = '{$template}'");
                    
                    SystemLog( $this->media['Title'] , 'Fees template', 'event', 'Delete Fee', 'ID:'. $template . ', Template:'. $rs->TemplateCode , 'Delete Template ');                                                                   

                    $response = successDelete();

                break;
                
                case 'save-fee':

                    if($this->permission->has('read')){
                        
                        $validation = $this->services->isValid(Request::all(),'fees');
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
						  
                            $index = Request::get('idx');
                            $ref = decode( Request::get('template') ) ;
                            
                            $data = $this->services->set_fees_data(Request::all());
                                    
                            if( $index == '' || $index == '0' ){
                                
                                $this->mFee->create($data);                                
                                SystemLog( $this->media['Title'] ,'Fees template','event','Create fee',json_encode($data),'' );
                                
                            }else{
                                
                                $this->mFee->where('IndexID', decode($index) )->update($data);                                
                                SystemLog( $this->media['Title'] ,'Fees template','event','Update fee',json_encode($data),'' );
                                
                            }
                            
						    $table = view($this->views.'fees', ['ref'=> $ref ] )->render();
							$response = ['error'=>false,'message'=>'Successfully Save!', 'fees' => $table ];
						}

                    }


                break;

                 case 'delete-fee':

                    $list = Request::get('ids');

                    foreach($list as $r){
                        
                        $ref = decode($r);
                        
                        $rs = $this->mFee->selectRaw("dbo.fn_TemplateCode(TemplateID) As TemplateCode, dbo.fn_AccountName(AccountID) As AccountName ")->where(['IndexID' => $ref ])->first();
                        
                        DB::statement("DELETE es_tableoffee_details WHERE IndexID = '{$ref}'");
                        SystemLog( $this->media['Title'] , 'Fees template', 'event', 'Delete Fee', 'RefID:'. $ref . ', Template:'. $rs->TemplateCode .', Account:'. $rs->AccountName.', Amount:'. $rs->Amount , 'Delete Template Fee');                                                                   
                    }

                    $response = successDelete();

                break;
                
                case  'get-parameters':
                
                    $prog = Request::get('prog');
                    $majors = get_majors($prog);
                    
                    $vw_majors = "<option value='0'> - Select -</option>";
                    
                    foreach($majors as $m){
                        $vw_majors .= "<option value='".$m->MajorID."'>".$m->MajorName."</option>";
                    }
                                        
                    $level = get_Yearlevel(0,$prog);
                    $vw_level = "<option value='0'> - Select -</option>";
                    
                    foreach($level as $l){
                        $vw_level .= "<option value='".$l->YLID_OldValue."'>".$l->YearLevelName."</option>";
                    }
                    
                    $response = ['error'=>false,'message'=>'success', 'major' => $vw_majors, 'level' => $vw_level ];
                                        
                break;
                
                case 'save-programs':
                    
                    $model = new pModel();
                    $template = decode(Request::get('template'));
                    $list = Request::get('data');
                    
                    $rs = $this->model->where('TemplateID', $template)->first();

                    $model->where('TemplateID',$template)->delete();

                    foreach($list as $r){
                        
                        $prog = ($r['prog']);
                        $maj = explode( "," , $r['maj'] ) ;
                        $level = explode( "," , $r['level'] );
                        
                        foreach($maj as $m){
                            
                            foreach($level as $l){
                                $data = array(                        
                                   'TemplateID'=> $template
                                  ,'ProgID' => $prog
                                  ,'YearLevelID' => $l
                                  ,'MajorID'             => $m                 
                                );
                                                                  
                                $model->create($data);                                
                            }
                                                                                    
                        }               
                                                                                                                           
                        SystemLog( $this->media['Title'] , 'Fees template', 'event', 'Save Template Program', 'Template:'. $rs->TemplateCode , 'Save Template Programs');                                                                   
                    }

                    $response = successSave();
                
                break;
                
                case 'get-programs': 
                
                    $model = new pModel();
                    $template = decode(Request::get('template'));
                    $rs = $model->selectRaw("dbo.fn_ProgramName(ProgID) As Program, dbo.fn_MajorName(MajorID) As Major, YearLevelID, ProgID, MajorID ")->where('TemplateID', $template)->get();
                    
                    $table = view($this->views.'programs', ['data'=> $rs ] )->render();
                    $response = ['error'=>false,'message'=>'success', 'table' => $table];
                    
                break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    
    private function initializer()
    {

        $this->model = new tModel();
        $this->mFee = new fModel();
        $this->permission = new Permission($this->ModuleName);
        $this->services = new services;
        $this->xpdf = new xpdf;
    }
}
