<?php
namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Services\Assessment\assessment as assess;

use DB;
use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;

class SOAccounts extends Controller{
   private $media = [ 'Title' => 'Online Payment',
        'Description'   => 'Online Payment',
		'closeSidebar'  => true,
        'js'            => ['Accounting/soaccounts'],
        'plugin_js'     => ['bootbox/bootbox.min',
							'bootstrap-select/bootstrap-select.min',
	                        'datatables/media/js/jquery.dataTables.min',
				            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
				            'datatables/extensions/Scroller/js/dataTables.scroller.min',
				            'datatables/plugins/bootstrap/dataTables.bootstrap',
							'jquery-validation/js/jquery.validate.min',
							'jquery-validation/js/additional-methods.min',
							'bootstrap-wizard/jquery.bootstrap.wizard.min',
                            'SmartNotification/SmartNotification.min'
                           ],
	 /* 'init'          => ['SECURITY.init()'], */
        'plugin_css'    => ['bootstrap-select/bootstrap-select.min','SmartNotification/SmartNotification',],	
    ];
    
    private $url =  [
        'page' => 'accounting/',
        'form' => 'form',
    ];
    
	public $r_view = 'Accounting.Views.soaccount.';
	
    public function index(){
		$this->initializer();
		//if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->r_view.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
        //}
        return view(config('app.403'));
    }
	
	public function recompute(){
		$this->initializer();
		die();
	}
	
	public function soa(){
		$this->initializer();
		die();
	}
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax()){
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event')){
                case 'filter':
				    $type     = Request::get('type');
                    $args     = Request::get('args');
					$exec     = $this->assess->get_filter($type,$args);
					$content  = (string)view($this->r_view.'sub.tblfilter',array('type'=>$type,'list'=>$exec));	
                    $response = Response::json(array('success'=>true,'content'=>$content));
                break;
				case 'reginfo':
                    $studno   = Request::get('studno');
				    $reginfo  = DB::select("SELECT TOP 1 r.*,(CASE WHEN r.TermID=s.TermID THEN '0' ELSE '1' END) as StatusID
					                                    ,y.YearLevelCode,y.YearLevelName,ay.AcademicYear,ay.SchoolTerm  
													FROM ES_Registrations as r
											  INNER JOIN ES_Students as s on r.StudentNo=s.StudentNo
											  INNER JOIN ESv2_YearLevel as y On r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID
											  INNER JOIN ES_AYTerm as ay ON r.TermID=ay.TermID
												   WHERE r.Studentno='".$studno."'
												ORDER BY r.RegID DESC");
					$content  = (($reginfo && count($reginfo)>0)?($reginfo[0]):array());
                    if($reginfo && count($reginfo)>0){
					  $idno   = $studno;
					  $regid  = $reginfo[0]->RegID;
					  $recalc = $this->assess->recalc_assessment($idno,$regid);
					}
					$response = Response::json(array('success'=>true,'content'=>$content));
				break;
				case 'assessment':
                    $termid  = Request::get('termid');
                    $studno  = Request::get('studno');
                    $templid = Request::get('templid');
					if($templid=='' || $termid=='' || $studno==''){
					  $content = '<div class="alert alert-warning"><i class="fa fa-warning"></i> No assessment yet.</div>';
					}else{
					  $assess  = DB::select("SELECT j.*,a.AcctCode,a.AcctName
	                                               ,(SELECT SUM(Credit) FROM ES_Journals WHERE TransID=60 AND [Description]='Total Discount' AND TransRefNo LIKE CONCAT(j.ReferenceNo,';',j.EntryID,';%')) as TotalDiscount 
	                                               ,(SELECT SUM(Credit) FROM ES_Journals WHERE TransID=60 AND [Description] IN (SELECT ProvName FROM ES_SchoProviders) AND TransRefNo LIKE CONCAT(j.ReferenceNo,';',j.EntryID,';%')) as EarlyBird 
                                               FROM ES_Journals as j
                                         INNER JOIN ES_TableofFee_Details as td ON j.AccountID=td.AccountID
                                         INNER JOIN ES_Accounts as a ON j.AccountID=a.AcctID
                                              WHERE TermID='".$termid."' AND IDNo='".$studno."' AND j.TransID=1 AND td.TemplateID='".$templid."'
										   ORDER BY j.AccountID");				   
					  if($assess && count($assess)>0){
					    $tbody     ='';
						$thead     ='';
						$tassess   =0;
						$tdiscount =0;
						$tonnet    =0;
						$tnetassess=0;
						foreach($assess as $rs){
						  $assess     = $rs->{'Assess Fee'};
						  $netassess  = $rs->{'1st Payment'}+$rs->{'2nd Payment'}+$rs->{'3rd Payment'}+$rs->{'4th Payment'};
						  $paymenta   = $rs->{'1st Payment'};
						  $paymentb   = $rs->{'2nd Payment'};
						  $paymentc   = $rs->{'3rd Payment'};
						  $paymentd   = $rs->{'4th Payment'};
						  $tassess   += $assess;
						  $odiscount  = floatval($rs->TotalDiscount)+floatval($rs->EarlyBird);
						  $tdiscount += $rs->TotalDiscount+$rs->EarlyBird;
						  $tonnet    += $rs->EarlyBird;
						  $tnetassess+= $netassess;
						  $thead     = '<tr>
						                  <th class="text-center">Accounts</th>
						                  <th class="text-center">Assessment</th>
						                  <th class="text-center">Discount</th>
						                  <th class="text-center hidden">On Net</th>
						                  <th class="text-center">Net</th>
										</tr>';
						  $tbody    .='<tr>
									    <td>'.$rs->AcctName.'</td>
									    <td class="text-right">'.number_format($assess,2,'.',',').'</td>
									    <td class="text-right">'.number_format(($odiscount),2,'.',',').'</td>
									    <td class="text-right hidden">'.number_format(($rs->EarlyBird),2,'.',',').'</td>
									    <td class="text-right">'.number_format(($netassess),2,'.',',').'</td>
						              </tr>';
						}
						$tfoot       = '<tr>
						                  <td class=""><b>TOTAL:</b></th>
						                  <td class="text-right">'.number_format($tassess,2,'.',',').'</td>
						                  <td class="text-right">'.number_format($tdiscount,2,'.',',').'</td>
						                  <td class="text-right hidden">'.number_format($tonnet,2,'.',',').'</td>
						                  <td class="text-right">'.number_format($tnetassess,2,'.',',').'</td>
										</tr>';
						  
					    $content='<table class="table table-bordered table-condense">
									<thead>'.$thead.'</thead>
									<tbody>'.$tbody.'</tbody>
									<tfoot>'.$tfoot.'</tfoot>
								  </table>';
					  }else{
					    $content = '<div class="alert alert-warning"><i class="fa fa-warning"></i> No assessment yet.</div>';
					  }
                    }
					$response = Response::json(array('success'=>true,'content'=>$content));
				break;
				case 'duedate':
                    $regid  = Request::get('regid');                    
					$qry    = "SELECT g.GroupID
									,g.GroupName
									,SUM(j.Debit) as Debit
									,SUM(j.[1st Payment]) as First
									,(SELECT TOP 1 ISNULL(FirstPaymentDueDate,RegDate) FROM ES_Registrations WHERE RegID='".$regid."') as FirstDueDate
									,SUM(j.[2nd Payment]) as Second
									,(SELECT TOP 1 SecondPaymentDueDate FROM ES_Registrations WHERE RegID='".$regid."') as SecondDueDate
									,SUM(j.[3rd Payment]) as Third
									,(SELECT TOP 1 ThirdPaymentDueDate FROM ES_Registrations WHERE RegID='".$regid."') as ThirdDueDate
									,SUM(j.[4th Payment]) as Fourth
									,(SELECT TOP 1 FourthPaymentDueDate FROM ES_Registrations WHERE RegID='".$regid."') as FourthDueDate
									,SUM(j.Debit - (j.ActualPayment + CreditMemo)) as Balance
									,SUM(j.ActualPayment) as Payment
									,SUM(j.CreditMemo) as CreditMemo
									,fe.PaymentScheme
								FROM ES_Journals as j
						INNER JOIN ES_Registrations as r ON j.IDNo=r.StudentNo AND j.TermID=r.TermID AND j.TransID=1
						INNER JOIN ES_TableOfFees as fe ON r.TableofFeeID=fe.TemplateID
						INNER JOIN ES_Accounts as a ON j.AccountID=a.AcctID
						INNER JOIN ES_AccountGroups as g ON a.GroupID=g.GroupID  
								WHERE r.RegID='".$regid."' 
							GROUP BY g.GroupID,g.GroupSort,g.GroupName,fe.PaymentScheme";
					$exec     = DB::select($qry);	
					$content  = '<div class="alert alert-warning"><i class="fa fa-warning"></i> No Data Available.</div>';
					if($exec && count($exec)>0){
						$assest = array(1=>0,2=>0,3=>0,4=>0);
						$duedate= array(1=>'',2=>'',3=>'',4=>'');
						$payment= 0;
						$total  = 0;
						$tmpval = 0;
						$i=1;
						
					    $content ='<table class="table table-bordered table-condense">
								  <thead>
									<th class="text-center">Due Date</th>
									<th class="text-center">Amount</th>
									<th class="text-center">Balance</th>
									<th></th>
								  </thead>
								  <tbody>';
						 
						foreach($exec as $rs){
						  if($i==1){
							$duedate[1]='Upon Enrollment';//((strtotime($rs->FirstDueDate) && $rs->FirstDueDate!='')?date('Y-m-d',strtotime($rs->FirstDueDate)):'Upon Enrollment'); 
							$duedate[2]=((strtotime($rs->SecondDueDate) && $rs->SecondDueDate!='')?date('Y-m-d',strtotime($rs->SecondDueDate)):'Date not set');
							$duedate[3]=((strtotime($rs->ThirdDueDate) && $rs->ThirdDueDate!='')?date('Y-m-d',strtotime($rs->ThirdDueDate)):'Date not set');
							$duedate[4]=((strtotime($rs->FourthDueDate) && $rs->FourthDueDate!='')?date('Y-m-d',strtotime($rs->FourthDueDate)):'Date not set'); 
						  }
						  
						  $payment   += $rs->Payment+$rs->CreditMemo; 
						  $assest[1] += $rs->First;
						  $assest[2] += $rs->Second;
						  $assest[3] += $rs->Third;
						  $assest[4] += $rs->Fourth;
						  $i++;
						}
					
						$tmpval   = $payment;
						$displayb = 0; 
						foreach($assest as $k=>$v){
						   if($assest[$k]>0){
							$ispaid  = 0;
							$total  += $v;
							$tmpval  = ($tmpval - $v);
							$payid   = 0;
							$pcheck  = DB::SELECT("SELECT * FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID WHERE pd.RegID='".$regid."' AND pd.ScheduleID=".$k." ORDER BY DetailID DESC");
							if($pcheck && count($pcheck)>0){
							  $payid    = $pcheck[0]->EntryID;
							  $tmpstats = $pcheck[0]->StatusID;
							  if($tmpstats==1){
							    $ispaid = 1;
							  }elseif($tmpstats=='-2'){
								$ispaid = '-1';
							  }
							}
							
							$width = '150px;';
							if($payment<$total && $ispaid==0){
							  if($displayb==0){
							    $cart     = '<a class="btn btn-sm btn-info btnacart" data-regid="'.$regid.'" data-payment="'.$k.'" data-balance="'.($tmpval*-1).'" href="javascript:void(0);"><i class="fa fa-plus"></i></a>';
							    $btn      = $cart.'<a class="btn btn-sm btn-danger" href="'.url('epayment?tid='.base64_encode($regid.':'.$k)).'"><i class="fa fa-money"></i> Pay Now</a>';
							    $displayb = 1;
							  }else{
							    $btn      = '<a class="btn btn-sm btn-warning" href="javascript:void(0);"><i class="fa fa-calendar"></i> Not Yet Due Date</a>';
							  }
							}elseif($payment<$total && $ispaid!=0){
							  if($ispaid==1){
							    $btn     = '<a class="btn btn-sm btn-info" href="javascript:void(0);"><i class="fa fa-refresh fa-spin"></i> For Validation</a>';
								$tmpval += $v; 
							  }else{
							    $btn      = '<a class="btn btn-sm btn-danger btnrevert" data-payment="'.$payid.'" href="javascript:void(0);"><i class="fa fa-undo"></i> Cancel</a>
											 <a class="btn btn-sm btn-info" href="javascript:void(0);"><i class="fa fa-refresh fa-spin"></i> Pending</a>';
								$displayb = 1;
								$tmpval  += $v; 
							  }
							  $width = '200px;';
							}else{
							  $btn   = '<a class="btn btn-sm btn-success" href="javascript:void(0);"><i class="fa fa-lock"></i> Paid</a>';
							  $width = '100px;';
							}
							
						    $isupdated = isFamilyUpdated();
						    if($isupdated==0){
							  $btn = '<a class="btn btn-sm btn-default btn-disabled" href="'.url('guardian').'"><i class="fa fa-lock"></i> Locked</a>';
						    }
							
							$content.= '<tr><td>'.$duedate[$k].'</td>
										    <td class="text-right">'.number_format($v,2).'</td>
										    <td class="text-right">'.(($tmpval>0)?'0.00':number_format(($tmpval*-1),2)).'</td>
										    <td width="'.$width.'">'.$btn.'</td></tr>';
						   }
						}
                        $content.='</tbody></table>';
					}
					
					$response = Response::json(array('success'=>true,'content'=>$content));
				break;
				case 'delcart':
				    $p       = Request::all();
					$payid   = getObjectValue($p,'payid');  
					$detid   = getObjectValue($p,'detailid');  
					$exec    = DB::table('ESv2_EPayment_Details')->where(array('DetailID'=>$detid))->delete();
					$check   = DB::select("SELECT * FROM ESv2_EPayment_Details WHERE PaymentID='".$payid."'");
					if(!$check || count($check)<=0){
					  $del   = DB::statement("DELETE FROM ESv2_EPayment WHERE EntryID='".$payid."'");
					}
					
					if($exec){
						$response = Response::json(array('success'=>true,'content'=>$this->load_epayment(),'message'=>'Successful'));
					}else{
					    $response = Response::json(array('success'=>true,'content'=>$this->load_epayment(),'message'=>'Failed to remove'));
					}
				break;
				case 'addcart':
				    $result  = array('success'=>false,'content'=>'','message'=>'Failed to execute');
					$p       = Request::all();
					$studno  = ''; 
					$termid  = getActiveTerm();
					$regid   = getObjectValue($p,'regid'); 
					$payid   = getObjectValue($p,'payid'); 
					$balance = getObjectValue($p,'balance'); 
					$entryid = 0;
					$content = '';
					$reginfo = DB::select("SELECT * FROM ES_Registrations WHERE RegID='".$regid."'");
					$checkin = DB::select("SELECT * FROM ESv2_EPayment WHERE UserID='".getUserID()."' AND StatusID=0");
					$checkdt = DB::select("SELECT * FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID WHERE UserID='".getUserID()."' AND pd.RegID='".$regid."' AND StatusID=0");
					if($reginfo && count($reginfo)>0){
					   $studno = $reginfo[0]->StudentNo;
					}
					
					if($checkin && count($checkin)>0){
					   $entryid = $checkin[0]->EntryID; 
					}else{
					   $data    = array('UserID'     => getUserID(),
										'TermID'     => $termid,
										'StatusID'   => 0,
										'TotalAmount'=> 0,
										'DateCreated'=> date('Y-m-d H:i:s'),
					               );
					   $entryid = DB::table('ESv2_EPayment')->insertGetId($data);
                    }
					
					if($entryid!=0){
						if(count($checkdt)<=0){
							$ddta = array('PaymentID'  =>$entryid,
										  'StudentNo'  =>$studno,
										  'RegID'      =>$regid,
										  'ScheduleID' =>$payid,
										  'TotalAmount'=>$balance,
							          );
					        $detailid = DB::table('ESv2_EPayment_Details')->insert($ddta);
							if($detailid){
								$result['success']=true;
								$result['message']='Successful';
								$result['content']=$this->load_epayment();
							}
						}else{
							$result['message']='Already added';
						}
					}else{
						$result['message']='Failed to execute';
					}
					
					$response = Response::json($result);
                break;				
			}
            return $response;
        }
        return $response;			
	}
	
	private function load_epayment($limit=0){
		$result  = '';
		$checkdt = DB::select("SELECT *,(SELECT TOP 1 Fullname FROM ES_Students WHERE StudentNo=pd.StudentNo) as StudentName FROM ESv2_EPayment as p INNER JOIN ESv2_EPayment_Details as pd ON p.EntryID=pd.PaymentID WHERE UserID='".getUserID()."' AND StatusID=0");
		if($checkdt && count($checkdt)>$limit){
			$epayid  = $checkdt[0]->EntryID;
			$balance = 0;
			foreach($checkdt as $r){
			  $balance+=$r->TotalAmount;
			  $result .='<tr data-id="'.$r->DetailID.'">
							<td width="10px;"><a href="javascript:void(0);" class="btn btn-sm btn-danger btnrcart"><i class="fa fa-times"></i></a></td>
							<td>'.$r->StudentName.' ('.$r->StudentNo.')</td>
							<td class="text-right">'.number_format($r->TotalAmount,2).'</td>
						 </tr>';
			}
			
			if($balance>0){
			  $result .='<tr data-epayment="'.$epayid.'">
							<td colspan="2">TOTAL:</td>
							<td class="text-right"><strong>'.number_format($balance,2).'</strong></td>
						 </tr>';
			}
		}else{
			$cleara = DB::statement("DELETE FROM ESv2_EPayment_Details WHERE PaymentID IN (SELECT EntryID FROM ESv2_EPayment WHERE UserID='".getUserID()."' AND StatusID=0)");
			$clearb = DB::statement("DELETE FROM ESv2_EPayment WHERE UserID='".getUserID()."' AND StatusID=0");
		}
		
		return $result;
	}
	
	private function initializer(){
	   ini_set('max_execution_time', 300);
		$this->assess = new assess;
    }
	
	public function init($apage='assess'){
		$this->initializer();
		$data = array('epayment'=>$this->load_epayment(1),);
		return $data;
	}	
	
}
?>