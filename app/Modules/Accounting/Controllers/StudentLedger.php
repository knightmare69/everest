<?php

namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
// use App\Modules\Setup\Services\FacultyAssignServiceProvider as Services;
use App\Modules\Accounting\Models\Reports as Rep_Model;
use App\Modules\Accounting\Services\Assessment\assessment as assess;
use App\Libraries\CrystalReport as xpdf;
use App\Libraries\PDF;
use App\Modules\Students\Services\StudentProfileServiceProvider as Services;
use App\Modules\Enrollment\Models\YearLevel;
use App\Modules\Accounting\Models\Ledger  as LedgerModel;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Permission;
use Request;
use Response;

class StudentLedger extends Controller
{
    protected $ModuleName = 'account-ledger';

    private $media = [
            'Title' => 'Student Ledger',
            'Description' => 'Welcome To Student Ledger!',
		    'closeSidebar'  => true,
            'js' => ['Accounting/studentledger', 'Accounting/printledger','Accounting/mapping_modifying_or'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'select2/select2',
            ],
        ];

    private $url = ['page' => 'accounting/studentledger/'];

    private $views = 'Accounting.Views.Ledger.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $_incl = [
                'views' => $this->views,
                //'at' => $this->model->AcademicTerm(),
                //'progs' => $this->model->Programs()
            ];

            return view('layout', array('content' => view($this->views.'index', $this->init())->with($_incl, ['views' => $this->views, 'navs' => $this->services->studentProfileNavs() ]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function ledger(){
        $this->initializer();
        if ($this->permission->has('read')) {
        //    return 'jay';

            if(Request::get('idno') != 'undefined'){
                $student_no   = decode(Request::get('idno'));
				$check_journ  = $this->recalc($student_no); 
                $student_info = $this->model->getStudentInfo($student_no);
                $studentledger= $this->model->getStudentLedger($student_no);
            }else{
                $student_no = '';
                $studentledger = null;
                $student_info = null;
            }

            $_incl = [
                'views' => $this->views,
                'idno' => $student_no,
                'data' => $studentledger,
                'info' => $student_info,

            ];

            return view('layout',array('content'=>view($this->views.'index')->with($_incl),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
    }


    public function reports()
    {
        if (getUserID() == 'admin') {
            $this->initializer();

            return view('layout', array('content' => view($this->views.'index')->with($_incl), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }



    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];

        if (Request::ajax()) {
            $this->initializer();
            $response = ['error' => true, 'message' => 'Permission Denied!'];

            switch (Request::get('event')) {

                case 'get':
                    if ($this->permission->has('read')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search_filter');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $post['is-official-enrolled'] = !empty($post['is-official-enrolled']) ? 1 : 0;

                            $data = $this->model->getStudentFromFilters($post);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                        }
                    }
                    break;

                 case 'ay_terms':
                        if ($this->permission->has('search')) {
                            if (isset($_POST['student_no'])) {
                                $student_no = $_POST['student_no'];
                                $ay_term = $this->model->ayTerm($student_no);



                                $response = ['terms' => $ay_term];
                            }
                        }
                break;
                case 'get_students':
                 if ($this->permission->has('read')) {
                     if (isset($_POST['student_no'])) {
                         $student_no = $_POST['student_no'];
                         $student_info = $this->model->getStudentInfo($student_no);
                         if (!empty($student_info->StudentPicture)) {
                             $pic = 'data:image/jpeg;base64,'.base64_encode(hex2bin($student_info->StudentPicture));
                         } else {
                             $pic = asset('assets/system/media/images/empty_prof_pic.jpg');
                         }

                         $response = ['img'=> $pic, 'error' => false, 'list' => $student_info];
                     }
                 }

                break;

                case 'get_student_ledger':
                if ($this->permission->has('read')) {
                    if (isset($_POST['student_no'])) {
                        $student_no = $_POST['student_no'];
                        $student_info = $this->model->getStudentInfo($student_no);
                        $studentledger = $this->model->getStudentLedger($student_no);
                        $response = ['info'=> $student_info, 'error' => false, 'ledger' => $studentledger];

                    }
                }

                break;
                case 'refresh_ledger':
                if ($this->permission->has('read')) {
                    if (isset($_POST['student_no'])) {
                            $student_no = $_POST['student_no'];
                            $studentledger = $this->model->getStudentLedger($student_no);
                            if ($studentledger){
                                    $response = ['ledger'=>$studentledger, 'error' => false, 'message' => 'Successfully Retrieved'];
                            }else{
                                 $response = [ 'error' => true, 'message' => 'Error'];
                            }



                    }

                }
                break;

                case 'update_or':
                    if ($this->permission->has('read')) {
                        if (isset($_POST['post_payment'])) {
                            $post_payment = $_POST['post_payment'];
                            $checknonledger = $_POST['checknonledger'];
                            $termid = $_POST['termid'];
                            $or_no = $_POST['or_no'];


                            if($checknonledger == 1 ){
                                $term_id = $_POST['selected_termid'];
                                $entryid = $_POST['entryid_arr'];
                                $this->model->setNonledgerAll($term_id,$entryid);
                            }

                           $this->model->updateAYTerm($termid,$or_no);
                           $forposting = $this->model->updateORReceipts($or_no,$post_payment);



                           if($forposting){
                               $response = ['error' => false, 'forposting' => $forposting, 'message' => 'OR Successfully Updated'];
                           }else{
                               $response = [ 'error' => true, 'message' => 'Error'];
                           }


                        }

                    }
                break;


                case 'referencing_or':
                if ($this->permission->has('read')) {

                    $new_type= $_POST['new_type'];
                    $termid = $_POST['termid'];
                    $new_refno =$_POST['new_refno'];
                    $new_entryid= $_POST['new_entryid'];
                    $entryid= $_POST['entryid'];
                    $trnenrtyid= $_POST['trnenrtyid'];
                    $trnrefno= $_POST['trnrefno'];
                    $new_trnidtext =  ($_POST['new_trnidtext'] > 0  ? $_POST['new_trnidtext']  :  $_POST['new_trnidtext']==='#Non-Ledger#' ? $_POST['new_trnidtext']:0  );
                    $checkDistributeTerms = $_POST["checkDistributeTerms"];
                    //print_r($new_trnidtext);
                    //die;


                    if($new_trnidtext === 0){
                        if($trnrefno != ""){
                            if($checkDistributeTerms == 1){
                                if($this->model->checkedDistributeTerms($entryid)){
                                    $response = ['error' => true, 'message' => 'Success'];
                                }


                            }else{
                                if($this->model->uncheckedDistributeTerms($entryid, $termid)){
                                    $response = ['error' => true, 'message' => 'Success 2'];
                                }

                            }

                        }

                    }else if($new_trnidtext === '#Non-Ledger#'){
                        if($this->model->setNonledger($entryid)){

                        $response = ['error' => false, 'message' => 'Successfully set to Non LEdger'];
}


                    }else{
                        $mTransType = $new_type;
                        $mTransRefNo = $new_refno;
                        if($checkDistributeTerms == 1){
                            $response = ['error' => true, 'message' => 'Permission Denied!'];

                        }else{
                            if($this->model->setTransref($new_type,$new_refno,$new_entryid,$entryid)){
                                $response = ['error' => false, 'message' => 'Successfully Mapped Reference'];

                            }
                        }


                    }


                }

                break;


                case 'get_or_details':
                if ($this->permission->has('read')) {
                    if (isset($_POST['or_no'])) {
                        $or_no = $_POST['or_no'];
                        $or_refno = $_POST['or_refno'];
                        $or_details = $this->model->get_or_details($or_no);
                        $assess_details = $this->model->get_assess_details($or_refno);
                        $response = ['assess'=> $assess_details ,'or'=> $or_details, 'error'=> false];

                    }

                }

                break;

                case 'on_or_change':
                if ($this->permission->has('read')) {
                    if (isset($_POST['or_refno'])) {
                        $or_refno = $_POST['or_refno'];
                        $assess_details = $this->model->get_assess_details($or_refno);
                        $response = ['assess'=> $assess_details ,'error'=> false];

                    }

                }
                break;

                case 'search':
                    if ($this->permission->has('search')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'search');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $return_data = array();

                            $is_official_enrolled = !empty($post['is-official-enrolled']) ? 1 : 0;
                            $fd = json_decode($post['filters'], true);
                            $filters = [decode($fd['search-campus']), decode($fd['search-ac-year']), $is_official_enrolled, $post['data_search'], 20];

                            $data = $this->model->getStudentFromFilters($filters);

                            if (!empty($data)) {
                                $view = view($this->views.'list', ['students' => $data])->render();
                                $response = ['error' => false, 'list' => $view];
                            } else {
                                $response = ['error' => true, 'message' => 'No result(s) found.'];
                            }
                        }
                    }
                    break;

                case 'get_info':
                    if ($this->permission->has('read')) {
                        $post = Request::all();

                        $validation = $this->services->isValid(Request::all(), 'get');

                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $form = Request::get('form');
                            $form_data = $this->services->studentProfileNavs($form);
                            $portlet = $this->views.'portlet';
                            $student_no = $post['student_no'];
                            $student_info = $this->model->getStudentInfo($student_no);
                            $adds = [];

                            if (empty($student_info)) {
                                return $response = ['error' => true, 'message' => 'Unable to find student information.'];
                            }

                            switch ($form) {
                                case 'profile':
                                    $data = $student_info;
                                    $fullname = $data->LastName.', '.$data->FirstName.' '.$data->MiddleInitial.' '.$data->ExtName;

                                    if (!empty($data->StudentPicture)) {
                                        $pic = 'data:image/jpeg;base64,'.base64_encode(hex2bin($data->StudentPicture));
                                    } else {
                                        $pic = asset('assets/system/media/images/empty_prof_pic.jpg');
                                    }

                                    $adds = ['photo' => $pic, 'name' => $fullname, 'stud_num' => $data->StudentNo, 'year_level' => $data->YearLevelName, 'chinese_name' => $data->ChineseName, 'stud_num_e' => encode($data->StudentNo)];

                                    $data = [
                                        'student_info' => $data,
                                        'religion' => $this->services->religion(),
                                        'nationality' => $this->services->nationalities(),
                                        'student_status' => $this->services->studentStatus(),
                                    ];
                                    break;

                                default:
                                    $data = [];
                                    break;
                            }

                            $vw = view($portlet, array_merge($form_data, ['views' => $this->views, 'form' => $form]))->with($data)->render();

                            $response = array_merge(['error' => false, 'form' => $vw], $adds);
                        }
                    }
                    break;

                case 'get_list':
                    if ($this->permission->has('read')) {
                        $shown_count = Request::get('shown_count');
                        $filters_data = json_decode(Request::get('filters'), true);
                        $is_official_enrolled = !empty($filters_data['is-official-enrolled']) ? 1 : 0;

                        $to_show_count = $shown_count + 10;

                        $filters = [decode($filters_data['search-campus']), decode($filters_data['search-ac-year']), $is_official_enrolled, Request::get('search_val'), $to_show_count];

                        $data = $this->model->getStudentFromFilters($filters, $shown_count);

                        if (!empty($data)) {
                            $view = view($this->views.'list', ['students' => $data])->render();
                            $response = ['error' => false, 'list' => $view];
                        } else {
                            $response = ['error' => true, 'message' => 'No record(s) found.'];
                        }
                    }
                    break;

                case 'get_photo':
                    if ($this->permission->has('read')) {
                        $student_no = Request::get('student_no');
                        $get_photo = $this->model->getStudentPhoto($student_no);

                        if (!empty($get_photo->StudentPicture)) {
                            $response = ['photo' => 'data:image/jpeg;base64,'.base64_encode(hex2bin($get_photo->StudentPicture))];
                        } else {
                            $response = ['photo' => false];
                        }
                    }
                    break;
            }
        }
        ob_clean();
        return $response;
    }



    public function print_report()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];

        if ($this->permission->has('print')) {

            if( Request::get('studnumber')){
                $studnumber = Request::get('studnumber');
                $configs = array($studnumber);

                $this->proc_report('all_ledger', $configs);

            }
            $ref = Request::get('ref');
            $trans_code = Request::get('code');
            $ledger = Request::get('ledger');
            $stud_name = Request::get('studname');
            $stud_no = Request::get('studno');
            $transdate = Request::get('transdate');
            $dmcmrefno =  Request::get('dmcmrefno');

            if ($trans_code ==='DMCM'){
                //die('DMCM');
                $configs = array($ref,$stud_name,$dmcmrefno,$stud_no);
                $this->proc_report('dmcm', $configs);
            }else if($trans_code ==='REG'){
                //die('Ledger');
                $configs = array($ref,$stud_no,$stud_name,$transdate);
                $this->proc_report('ledger', $configs);
            }else if ($trans_code ==='OR'){
                $configs = array($ref);
                $this->proc_report('non-ledger', $configs);
            }
        }
        return $response;
    }

    private function proc_report($for, $configs = array(), $dl_name = '', $prog_id = null)
    {
        $this->initializer();
        $subrpt = array(
            'subreport1' => array(
                'file' => 'Company_Logo',
                'query' => "call ES_rptReportLogo('San Beda College Alabang', 'Republic', 'Muntinlupa City', '-1')",
            ),
        );

        if ($for == 'non-ledger') {
            $this->xpdf->filename = 'certificate_of_payment.rpt';
            $this->xpdf->query = "call ES_rptCertificateOfPayment('{$configs['0']}', 'San Beda College Alabang', '', 'Alabang Hills Village, Alabang, Muntinlupa City', 'Alabang Campus')";
            $subrpt = array(
                'subreport1' => array(
                    'file' => 'Check',
                    'query' => "call ES_rptCertificateOfPayment_Checks('{$configs['0']}')",
                )
            );

        } else if ($for == 'ledger') {
            //$configs = array($ref,$stud_no,$stud_name,$transdate);
            $this->xpdf->filename = 'assessed_fees.rpt';
            $this->xpdf->query = "call ES_rptAssessment('1', '{$configs['0']}', '1', '{$configs['1']}', '{$configs['2']}',  '{$configs['3']}', 0, 'San Beda College Alabang', '', 'Alabang Hills Village, Alabang, Muntinlupa City', 'Alabang Campus')";
        } else if ($for == 'dmcm') {
            $this->xpdf->filename = 'debitcredit_memo.rpt';
            $this->xpdf->query = "call ES_rptDebitCreditMemo('{$configs['0']}', 'San Beda College Alabang', '', 'Alabang Hills Village, Alabang, Muntinlupa City', 'Alabang Campus')";
            $subrpt = array(
                'subreport1' => array(
                    'file' => 'AdjustedEntries',
                    'query' => "call ES_DebitCreditTransactions('1', '{$configs['2']}', '1', '{$configs['3']}')",
                )
            );
        } else if ($for == 'all_ledger') {
            $this->xpdf->filename = 'student_ledger_r2.rpt';
            $this->xpdf->query = "call ES_rptStudentLedger_r3('{$configs['0']}', 'admin')";
        }  else if ($for == 'soa') {
            $this->xpdf->filename = 'finance/soa shs.rpt';
            $this->xpdf->query = 'call rpt_soa_shs ('.implode(', ', $configs).')';
            $term_id = decode(Request::get('academic-term'));
            $prog_id = decode(Request::get('programs'));
            $yl_id = decode(Request::get('year-level'));
            $idno = Request::get('snum');
            $subrpt = array(
                'subreport1' => array(
                    'file' => 'Company_Logo',
                    'query' => "call ES_rptReportLogo('San Beda College Alabang', 'Republic', 'Muntinlupa City', '-1')",
                ),
                'subreport2' => array(
                    'file' => 'subject enrolled',
                    'query' => "call ES_rptGetStudentRegistration_Subjects (@CampusID = 1, @TermID = {$term_id}, @FilterBy = 0, @Bulk = 0, @ByLevel = {$yl_id}, @CollegeID = 0, @ProgramID = {$prog_id}, @YearLevelID = {$yl_id}, @StudentNo = '{$idno}')",
                ),
                'subreport3' => array(
                    'file' => 'assessment',
                    'query' => "call rpt_soa_assessment(@CampusID = 1, @TermID = {$term_id}, @FilterBy = 0, @Bulk = 0, @ByLevel = {$yl_id}, @CollegeID = 0, @ProgramID = {$prog_id}, @YearLevelID = {$yl_id}, @StudentNo = '{$idno}',  @DateAsOF = '". systemDate()."')",
                ),
                'subreport4' => array(
                    'file' => 'payment',
                    'query' => "call ES_rptStatementofAccount_OfficialReceipts_College( @CampusID = 1, @TermID = {$term_id}, @FilterBy = 0, @Bulk = 0, @ByLevel = {$yl_id}, @CollegeID = 0, @ProgramID = {$prog_id}, @YearLevelID = {$yl_id}, @StudentNo = '{$idno}',  @DateAsOF = '". systemDate()."')",
                ),

                'subreport5' => array(
                    'file' => 'SOA_Letter',
                    'query' => 'call rpt_soa_shs('.implode(', ', $configs).')'
                ),
            );
        } else {
            die('Unable to find report type!');
        }

        if($for == 'ledger' || $for == '' ||$for == 'grade-correction-history' ) {
             $data = $this->xpdf->generate();
        }
        else
        {
            $this->xpdf->SubReport = $subrpt;
            $data = $this->xpdf->generate();
        }

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename='.$dl_name.'.pdf');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($data['file']);
        die();
    }

    private function init($key = null)
    {
        $this->initializer();

        return array(
            // 'students' => $this->model->getStudentFromFilters([1, getLatestActiveTerm(), 0, '', 20]),
            'year_level' => YearLevel::get(),
            'academic_year' => $this->services->academic_year(),
            'campus' => $this->services->campus(),
            'religion' => $this->services->religion(),
            'nationality' => $this->services->nationalities(),
            'living_with' => $this->services->livingWith(),
            'civil_status' => $this->services->civilStatus(),
        );
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new LedgerModel();
        $this->rep_model = new Rep_Model();
        $this->permission = new Permission($this->ModuleName);
        $this->xpdf = new xpdf();

    }
	
	private function recalc($stdno=''){
	   if($stdno!='' && $stdno!=false){
	    $this->assess = new assess(); 
	    $reginfo= DB::SELECT("SELECT TOP 1 * FROM ES_Registrations WHERE StudentNo='".$stdno."' AND ISNULL(TableofFeeID,0)>0 ORDER BY RegID DESC");
		if($reginfo && count($reginfo)>0){
			$termid = $reginfo[0]->TermID;
			$refno  = $reginfo[0]->RegID;
			$templid= $reginfo[0]->TableofFeeID;
			$studno = $stdno;
			$execa  = $this->assess->exec_assessment($termid,$studno,$refno,$templid);
			$execb  = $this->assess->get_studentjournal($termid,$studno,$refno,2);
		}
	   }
	   return true;
	}
}
