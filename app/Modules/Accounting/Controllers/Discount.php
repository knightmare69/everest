<?php 

namespace App\Modules\Setup\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Setup\Services\DiscountServiceProvider as Services;
use App\Modules\Setup\Models\Group as GroupModel;
use Request;
use Response;
use Permission;

class Discount extends Controller {
	use Services;
	private $media =
		[
			'Title'=> 'Discount',
			'Description'=> 'Manage',
			'js'		=> ['setup/discount'],
			'init'		=> ['Discount.init()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'bootstrap-datepicker/js/bootstrap-datepicker',
				'bootstrap-timepicker/js/bootstrap-timepicker.min',
				'bootstrap-daterangepicker/moment.min',
				'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
                'datatables/media/js/jquery.dataTables.min',
	            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
	            'datatables/extensions/Scroller/js/dataTables.scroller.min',
	            'datatables/plugins/bootstrap/dataTables.bootstrap'
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
				'bootstrap-datetimepicker/css/bootstrap-datetimepicker.min',
				'bootstrap-timepicker/css/bootstrap-timepicker.min'
			]
		];

	private $url = [ 'page' => 'setup/discount/','form'=> '.form_crud' ];

	public $views = 'Setup.Views.Discount.';

 	public function index()
 	{
 		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index',$this->init())->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}

	private function init($key = null) 
	{
		$this->initializer();
		return array(
			'table' => $key == null ?  $this->model->all() : array(),
			'campus' => $this->getCampus(),
			'terms' => $this->getTerms()
		);
	
	}

	private function initializer()
	{
		$this->model = new GroupModel;
		$this->permission = new Permission('group');
	}
}