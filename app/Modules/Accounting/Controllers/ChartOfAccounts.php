<?php

namespace App\Modules\Accounting\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Models\ChartOfAccounts as Model;
use App\Modules\Accounting\Models\AccOptions;
use App\Modules\Accounting\Models\PayOptions;

use App\Modules\Accounting\Models\AccClassif;
use App\Modules\Accounting\Models\AccFundGroup;
use App\Modules\Accounting\Models\AccTypes;
use App\Modules\Accounting\Models\SubFund;
use App\Modules\Accounting\Models\SubUnits;

use App\Modules\Setup\Models\Department;

use App\Modules\Accounting\Services\ChartOfAccountsSP as Services;

use Response;
use Illuminate\Http\Request;
use Permission;

class ChartOfAccounts extends Controller
{
    private $module_name = 'accounts-chart';

    private $media = [
        'Title' => 'Chart of Accounts',
        'Description' => 'Welcome To Chart of Accounts',
        'js' => ['Accounting/accounts-chart'],
        'plugin_js' => [
            'bootbox/bootbox.min',
            'datatables/media/js/jquery.dataTables.min',
            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
            'datatables/extensions/Scroller/js/dataTables.scroller.min',
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2.min',
        ],
        'plugin_css' => [
            'datatables/plugins/bootstrap/dataTables.bootstrap',
            'select2/select2',
        ],
    ];

    private $url = ['page' => 'accounting/accounts-chart/'];

    private $views = 'Accounting.Views.ChartOfAccounts.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }

        if ($this->permission->has('read')) {

            $search_by = [
                ['label' => 'Code', 'id' => 'code'],
                ['label' => 'Name', 'id' => 'name'],
                ['label' => 'Fund/Group', 'id' => 'fund-group'],
            ];

            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'search' => $search_by]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event(Request $req)
    {
        if ($req->ajax()) {
            switch ($req->get('e')) {
                case 'form':
                    if ($this->permission->has('read')){
                        $form = $req->get('form');
                        $has_form = true;

                        if ($form == 'account') {
                            $title = 'Account Info';
                            $data = !empty($req->get('id')) ? Model::find(decode($req->get('id'))) : [];
                            $a_opt = AccOptions::orderBy('SeqNo', 'ASC')->get();
                            $p_opt = PayOptions::orderBy('SeqNo', 'ASC')->get();

                            $tbl = view($this->views.'.forms.account', ['data' => $data, 'acc_opt' => $a_opt, 'pay_opt' => $p_opt])->render();

                        } else {
                            switch ($form) {
                                case 'par-acc':
                                    $data = Model::get();
                                    $title = 'Parent Account';
                                    $has_form = false;
                                    break;

                                case 'fund-group':
                                    $data = AccFundGroup::get();
                                    $title = 'Fund / Group';
                                    break;

                                case 'subfund':
                                    $data = SubFund::get();
                                    $title = 'Sub Fund';
                                    break;

                                case 'subunit':
                                    $data = SubUnits::get();
                                    $title = 'Sub Unit';
                                    break;

                                case 'src-dept':
                                    $data = Department::get();
                                    $title = 'Resource Center / Department';
                                    $has_form = false;
                                    break;

                                case 'acc-classif':
                                    $data = AccClassif::get();
                                    $title = 'Classification';
                                    break;

                                case 'acc-type':
                                    $data = AccTypes::get();
                                    $title = 'Type';
                                    break;

                                default:
                                    $data = '';
                                    $title = '';
                                    break;
                            }

                            $tbl = view($this->views.'modal', ['data' => $data, 'form' => $form, 'with_form' => $has_form, 'views' => $this->views, 'action' => 'New'])->render();
                        }

                        $response = ['error' => false, 'message' => 'Form found.', 'title' => $title, 'content' => $tbl];
                    }
                    break;

                case 'save-form':
                    if ($this->permission->has('add')) {
                        $form = $req->get('form');
                        $d = $req->get('data');
                        parse_str($d, $nd);

                        $v = $this->services->_validate($nd, $form);

                        if (!empty($v['messages'])) {
                            $response = ['error' => true, 'message' => 'Please check required fields.', 'err_list' => $v];

                        } else {
                            $df = $this->services->db_fields($nd, $form);

                            switch ($form) {
                                case 'account':
                                    if (!empty($nd['id'])) {
                                        $s = Model::where('AcctID', decode($nd['id']))->update($df);
                                    } else {
                                        $s = Model::insert($df);
                                    }
                                    break;

                                case 'group':
                                    $s = AccFundGroup::insert($df);
                                    break;

                                case 'sfund':
                                    $s = SubFund::insert($df);
                                    break;

                                case 'sunits':
                                    $s = SubUnits::insert($df);
                                    break;

                                case 'classif':
                                    $s = AccClassif::insert($df);
                                    break;

                                case 'types':
                                    $s = AccTypes::insert($df);
                                    break;

                                default:
                                    $s = false;
                                    break;
                            }

                            $response = ['error' => false, 'message' => 'Saving complete.'];

                        }
                    }
                    break;

                case 'records':
                    if($this->permission->has('read')) {
                        $r = $req->all();
                        $search_opt = parse_str($req->get('search_opt'), $so);
                        $so = array_filter($so);

                        $sort_col_index = $r['order'][0]['column'];
						$sort = $r['order'][0]['dir'];

                        $colum_sorts = [1 => 'a.AcctCode', 2 => 'a.AcctName', 3 => 'AcctParentCode', 4 => 'AcctParentCode', 5 => 'a.DefaultAmount',  6 => 'g.GroupCode', 7 => 'sg.SubGroupCode', 8 => 'su.SubUnitCode', 9 => 'su.SubUnitCode', 10 => 'd.DeptName', 11 => 'cl.ClassCode', 12 => 'c.CategoryName'];

                        $sorting = (empty($sort_col_index) ? 'a.AcctID DESC': $colum_sorts[$sort_col_index].' '.$sort);

                        // if (!empty($r['search']['value'])) {
                        $sf = ['code' => 'a.AcctCode', 'name' => 'a.AcctName', 'fund-group' => 'g.GroupCode'];

						// $v = $r['search']['value'];
                        // $st = $sf[$r['search_by']];

                        // $data = $this->model->_fields()->selectRaw('total_count = COUNT(*) OVER()')
                        $data = $this->model->_fields();
						// ->where($st, 'like', '%'.$v.'%')

                        foreach($so as $key => $sv) {
                            if(array_key_exists($key, $sf)){
                                $operator = !empty($so['dis-'.$key]) ? '=' : 'like';
                                $v = ($operator == 'like') ? '%'.$sv.'%' : $sv;
                                $data->where($sf[$key], $operator, $v);
                            }
                        }

                        $total_count = $data->count();

                        $data->orderByRaw($sorting)
						->skip($r['start'])
                        ->take($r['length']);

						$data = $data->get();

						// } else {
						// 	$data = $this->model->_fields()->selectRaw('total_count = COUNT(*) OVER()')
						// 	->orderByRaw($sorting)
						// 	->skip($r['start'])
						// 	->take($r['length'])
						// 	->get();
                        //
						// }

	                    $response = $this->formatDataToDisplay($total_count, $data, $r);
                    }
                    break;

                default:
                    $response = ['error' => true, 'message' => 'Event not found.'];
                    break;
            }

            return $response;

        } else {
            return http_response_code(403);
        }
    }

    private function formatDataToDisplay($total_count, $data, $settings)
    {
        $return_arr = [];
        $start = $settings['start'] + 1;

        if(!empty($data)){
            $return_arr = array(
                'recordsTotal' => $total_count,
                'recordsFiltered' => $total_count,
            );

            for($a = 0; $a < count($data); $a++){

                $_this = $data[$a];
                $cur_count = $start + $a;

                $return_arr['data'][] = array(
					// 'DT_RowClass' => 'without-bg',
					// 'DT_RowData' => ['id' => encode($_this->UserIDX)],
                    $cur_count,
                    '<a href="#" class="res-a-acct" data-id="'.encode($_this->AcctID).'">'.$_this->AcctCode.'</a>',
					$_this->AcctName,
					$_this->ShortName,
					$_this->AcctParentCode,
					number_format($_this->DefaultAmount, 2),
					$_this->GroupCode,
					$_this->SubGroupCode,
					$_this->SubUnitCode,
					$_this->DeptName,
					$_this->ClassCode,
					$_this->CategoryName,
                );
            }

        } else {
            $return_arr = array(
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => []
            );
        }

        return $return_arr;
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new Model();
        $this->permission = new Permission($this->module_name);
    }
}
