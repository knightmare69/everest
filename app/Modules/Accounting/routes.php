<?php
Route::group(['prefix' => 'accounting','middleware'=>'auth'], function () {
		    Route::get('/','Accounting@index');
			Route::get('/txn','Accounting@txn');
		    Route::post('/txn','Accounting@txn');
		    Route::get('/pdf','Accounting@pdf');
			Route::get('/dmcm','DMCM@index');
			Route::get('/reports','Reports@index');
			Route::get('/print','Reports@print_report');		
			Route::get('/export','Reports@export_report');		
			Route::get('/test','Accounting@test');
			
	Route::group(['prefix' => 'soaccounts', 'middleware' => 'auth'], function (){
		Route::get('/', 'SOAccounts@index');
		Route::get('/txn', 'SOAccounts@txn');
		Route::post('/txn', 'SOAccounts@txn');
    });
	
	Route::group(['prefix' => 'accounting-reports', 'middleware' => 'auth'], function (){
        Route::get('/', 'Reports@index');
        Route::post('event', 'Reports@event');
		Route::get('print', 'Reports@print_report');
        Route::get('export', 'Reports@export');
    });	
			
	Route::group(['prefix' => 'student-discount', 'middleware' => 'auth'], function (){
        Route::get('/', 'StudentDiscount@index');
        Route::post('txn', 'StudentDiscount@txn');
    });

    Route::group(['prefix' => 'accounts-chart', 'middleware' => 'auth'], function () {
        Route::get('/', 'ChartOfAccounts@index');
        Route::post('event', 'ChartOfAccounts@event');
    });

    Route::group(['prefix' => 'fees-template', 'middleware' => 'auth'], function () {
        Route::get('/', 'Feestemplate@index');
        Route::get('/view', 'Feestemplate@view');
        Route::post('event', 'Feestemplate@event');
		Route::get('print', 'Feestemplate@print_report');
        Route::get('export', 'Feestemplate@export');
    });

    Route::group(['prefix' => 'studentledger', 'middleware' => 'auth'], function () {
        Route::get('/', 'StudentLedger@index');
        Route::post('event', 'StudentLedger@event');
        Route::get('/ledger', 'StudentLedger@ledger');
    	Route::get('print', 'StudentLedger@print_report');
        Route::get('export', 'StudentLedger@export');

    });

    /*Created : ARS 2018-01-31 */
    Route::group(['prefix' => 'subject-with-fee', 'middleware' => 'auth'], function () {
        Route::get('/', 'SubjectWithFee@index');
        Route::post('event', 'SubjectWithFee@event');
    });

    /*Created : ARS 2018-03-07 16:11H */
    Route::group(['prefix' => 'due-date-setup', 'middleware' => 'auth'], function () {
        Route::get('/', 'DueDateSetup@index');
        Route::post('event', 'DueDateSetup@event');
    });

    /*Created : ARS 2018-01-31 */
    Route::group(['prefix' => 'assessment', 'middleware' => 'auth'], function () {
        Route::get('/', 'Assessment@index');
        Route::post('event', 'Assessment@event');
        Route::get('soa', 'Assessment@soa');
        Route::get('assessment', 'Assessment@assessment');
    });

    /*Created : ARS 2018-06-27 17:10H */
    Route::group(['prefix' => 'memo', 'middleware' => 'auth'], function () {
        Route::get('/', 'DebitCreditMemo@index');
        Route::get('/create', 'DebitCreditMemo@create');
        Route::post('event', 'DebitCreditMemo@event');

    });	
});

Route::group(['prefix' => 'epayment'], function (){
		Route::get('/','Epayment@index');
		Route::post('/txn','Epayment@txn');
		Route::get('/process/{regid}','Epayment@process');
		Route::post('/process/{regid}','Epayment@process');
		Route::get('/nofity/{regid}','Epayment@notify');
		Route::post('/notify/{regid}','Epayment@notify');
		Route::get('/cancel','Epayment@cancel');
		Route::post('/cancel','Epayment@cancel');
		Route::get('/terms','Epayment@terms');
});

Route::get('recalc','Accounting@botfunc');
?>