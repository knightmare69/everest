<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class TableOfFees_Programs extends Model
{
    protected $table = 'es_tableoffee_distributionlist';
    protected $primaryKey = 'IndexID';
    
   	protected $fillable  = array(
	   'TemplateID'
      ,'ProgID'
      ,'YearLevelID'
      ,'MajorID'    
	);

	public $timestamps = false;
    
    
}