<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class AccFundGroup extends Model
{
    protected $table = 'es_accountgroups';
    protected $primaryKey = 'GroupID';

    protected $fillable  = ['GroupCode', 'GroupName', 'GroupShort', 'GroupSort', 'OR_PrintInGroup'];

    public $timestamps = false;
}
