<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class SubFund extends Model
{
    protected $table = 'es_accountsubgroups';
    protected $primaryKey = 'SubGroupID';

    protected $fillable  = ['SubGroupCode', 'SubGroupName', 'SubGroupShort', 'SeqNo'];

    public $timestamps = false;
}
