<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class ChartOfAccounts extends Model
{
    protected $table = 'es_accounts';
    protected $primaryKey = 'AcctID';

    protected $fillable  = ['AcctCode', 'AcctName', 'ShortName', 'CategoryID', 'GroupID', 'ClassID', 'Inactive', 'AcctOption', 'PaymentOption', 'SortOrder', 'GLID', 'SubFundID', 'SubUnitID', 'DeptID', 'TotalAmountOnly', 'GLID_Setup_Debit', 'GLID_Collection_Credit', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', 'DefaultAmount', 'PayorTypeID', 'ReqAYTermID', 'ReqDeptID', 'ReqYrLevelID', 'ParentAcctID'];

    public $timestamps = false;

    public function _fields()
    {
        $f = array_map(function($a) { return 'a.'.$a;}, $this->fillable);
        $nf = ['c.CategoryName', 'g.GroupCode', 'g.GroupName', 'g.GroupShort', 'cl.ClassCode', 'cl.ClassName', 'cl.ClassShort', 'ao.AcctOptionName', 'ao.AcctOptionDesc', 'po.PaymentOptionName', 'po.PaymentDescription', 'sg.SubGroupCode', 'sg.SubGroupName', 'sg.SubGroupShort', 'su.SubUnitCode', 'su.SubUnitName', 'su.SubUnitShort', 'd.DeptName', 'a.AcctID'];

        $sk = array_merge($f, $nf, [DB::raw('dbo.fn_AccountCode(ParentAcctID) as AcctParentCode')]);

        $s = DB::table($this->table.' as a')->select($sk)
                ->leftJoin('es_accountscategory as c', 'c.CategoryID', '=', 'a.CategoryID')
                ->leftJoin('es_accountgroups as g', 'g.GroupID', '=', 'a.GroupID')
                ->leftJoin('es_accountsclass as cl', 'cl.ClassID', '=', 'a.ClassID')
                ->join('es_accountoptions as ao', 'ao.AcctOptionID', '=', 'a.AcctOption')
                ->join('es_paymentoptions as po', 'po.PaymentOptionID', '=', 'a.PaymentOption')
                ->leftJoin('es_accountsubgroups as sg', 'sg.SubGroupID', '=', 'a.SubFundID')
                ->leftJoin('es_accountsubunits as su', 'su.SubUnitID', '=', 'a.SubUnitID')
                ->leftJoin('es_departments as d', 'd.DeptID', '=', 'a.DeptID');

        return $s;
    }
}
