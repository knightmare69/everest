<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class AccClassif extends Model
{
    protected $table = 'es_accountsclass';
    protected $primaryKey = 'ClassID';

    protected $fillable  = ['ClassCode', 'ClassName', 'ClassShort', 'ClassSort', 'OR_PrintInGroup'];

    public $timestamps = false;
}
