<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class TableOfFees_Dues extends Model
{
    protected $table = 'es_tableoffees_dates';
    protected $primaryKey = 'IndexID';
    
   	protected $fillable  = array(
	   'TemplateID'
      ,'Date1'
      ,'Date2'
      ,'Date3'
      ,'Date4'
      ,'Date5'
      ,'Date6'
      ,'Date7'
      ,'Date8'
      ,'Date9'    
      ,'Date10'
	);

	public $timestamps = false;
    
    
}