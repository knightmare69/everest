<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class AccOptions extends Model
{
    protected $table = 'es_accountoptions';
    protected $primaryKey = 'AcctOptionID';

    protected $fillable = ['AcctOptionName', 'AcctOptionDesc', 'SeqNo', 'Inactive', 'WithSucceedingAmount', 'BaseCaption', 'SucceedingCaption', 'SeqNo2', 'RegActive', 'ADCActive', 'ShortName'];

    public $timestamps = false;
}
