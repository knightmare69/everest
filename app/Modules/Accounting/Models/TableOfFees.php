<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class TableOfFees extends Model
{
    protected $table = 'es_tableoffees';
    protected $primaryKey = 'TemplateID';
    
   	protected $fillable  = array(
	   'TemplateCode'
      ,'TemplateDesc'
      ,'TemplateRemarks'
      ,'TermID'
      ,'CampusID'
      ,'LastModified'
      ,'Modifiedby'
      ,'ForForeign'
      ,'TransType'
      ,'CurrencyID' 
      ,'StudentStatus'
      ,'StudentGender'
      ,'PaymentScheme'
      ,'PaymentOption'
       ,'LockedBy'
       ,'LockedDate'
       ,'PracticumOnly'
       ,'Total1stPayment'
       ,'Total2ndPayment'
       ,'Total3rdPayment'
       ,'Total4thPayment'
       ,'Total5thPayment'
       ,'Total6thPayment'
       ,'Total7thPayment'
       ,'Total8thPayment'
       ,'Total9thPayment'
       ,'Total10thPayment'
       ,'TotalNumPayment'
       ,'TotalAmount'
       ,'TotalNumMonth'
       ,'InActive'
	);

	public $timestamps = false;
    
    public function scopeDistribution($query, $prog, $major){
        return $query->leftJoin("es_tableoffee_distributionlist as p","p.TemplateID","=","es_tableoffees.TemplateID")
                ->where(["ProgID"=>$prog, "MajorID"=>$major]);
    }
    
    
}