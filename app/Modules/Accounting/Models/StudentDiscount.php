<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class StudentDiscount extends Model{
    protected $table = 'ESv2_StudentDiscounts';
    protected $primaryKey = 'IndexID';
    
   	protected $fillable  = array();

	public $timestamps = false;
    
    public function getStudentList($filter='',$offset=0,$limit=100){
	  $qry  = "SELECT * FROM ES_Students WHERE StudentNO NOT IN (SELECT StudentNo FROM ES_GraduateStudents) ORDER BY LastName,FirstName OFFSET ".$offset." ROWS FETCH NEXT ".$limit." ROWS ONLY";
	  $exec = DB::select($qry);
	  return (($exec)?$exec:false);
	}
    
    
}