<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class SubUnits extends Model
{
    protected $table = 'es_accountsubunits';
    protected $primaryKey = 'SubUnitID';

    protected $fillable  = ['SubUnitCode', 'SubUnitName', 'SubUnitShort', 'SeqNo'];

    public $timestamps = false;
}
