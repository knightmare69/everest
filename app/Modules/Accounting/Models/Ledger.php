<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Ledger extends Model
{
    protected $table = 'es_students';
    protected $table_family = 'esv2_admission_familybackground';
    // protected $table_admission = 'esv2_admission';
    protected $table_counseling = 'es_studentscounselling';
    protected $table_adm_docs = 'esv2_admission_requireddocs';
    // private $view = 'vw_K12_Admission';
    protected $primaryKey = 'StudentNo';

    protected $table_gs_student_req = 'es_gs_studentrequests';

    protected $fillable = array(
        'StudentPicture',
        'LRN',
        'Elem_School',
        'Elem_Address',
        'Elem_InclDates',
        'HS_School',
        'HS_Address',
        'HS_InclDates',
    );

    public $timestamps = false;

    public function getStudentFromFilters($filters, $orig_total_count = 0)
    {

        $filter = isset($filters['search-prog']) ? $filters['search-prog'] : 0 ;

        $prog_id = decode($filter);
        $year_level_id = decode(isset($filters['search-level']) ? $filters['search-level'] : 0 );
        $term_id = decode(isset($filters['search-ac-year']) ? $filters['search-ac-year'] : 0 );
        $search_text = $filters['student-search-text'];
        $allowed_programs = getUserProgramAccess();
        try {
            $g = DB::table($this->table.' as S')->select('S.StudentNo', 'S.Fullname', 'S.FirstName', 'S.Middlename', 'S.LastName', 'S.MiddleInitial', 'S.ExtName', 'S.Gender', 'S.DateOfBirth', DB::raw('dbo.fn_Age(S.DateOfBirth, GETDATE()) AS Age'), 'S.FamilyID', 'S.YearLevelID', DB::raw('dbo.fn_K12_YearLevel3(S.YearLevelID, S.ProgID) as YearLevelName'), 'S.ProgID')
                    ->whereIn('S.ProgID', $allowed_programs);
                    // ->join('es_registrations as R', 'S.StudentNo', '=', 'R.StudentNo')
                    // ->where(['S.CampusID' => 1, 'R.TermID' => $term_id, 'S.ProgID' => $prog_id])
                    // ->whereIn('S.YearLevelID', $yl);
            if(!empty($search_text)){
                $g = $g->where(function ($q) use ($search_text) {
                    $s = '%'.$search_text.'%';

                    $q->where('S.StudentNo', '=', $search_text)
                        ->orWhere('S.Fullname', 'like', $s)
                        ->orWhere('S.FirstName', 'like', $s)
                        ->orWhere('S.LastName', 'like', $s)
                        ->orWhere('S.Middlename', 'like', $s);
                });
            }
            $r = $g->orderBy('S.LastName', 'ASC')->get();

            return $r;

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
    public function getStudentLedger($id){
            $data = DB::select(DB::raw("EXEC sp_accountledger 0,'".$id."'"));
            return $data;

    }
    public function getStudentPhoto($student_no)
    {
        $photo = $this->select('StudentPicture')->where('StudentNo', $student_no)->first();
        return $photo;
    }

    public function updateORReceipts($or,$forposting)
    {
        $update  = DB::table('es_officialreceipts')
                        ->where('ORNo',$or)
                        ->update(['ForPosting' => $forposting]);
        return $update;
        //"UPDATE es_officialreceipts SET ForPosting = '" & IIf(chkPostPayment.value = checked, 0, 1) & "' WHERE ORNo = '" & DEFAULT_ORNO &
    }

    public function studentsSearch($search_string = null, $limit = '15')
    {

        //if (!empty($search_string)) {
            $get = $this->selectRaw("StudentNo, LastName, FirstName, FamilyID,YearLevelID,  dbo.fn_K12_YearLevel3(YearLevelID, ProgID) As YearLevel  ")
                    ->whereRaw(" (LastName <> '' AND FirstName <> '') AND (Fullname like '%".$search_string."%' OR StudentNo Like '%". $search_string ."%')")
                    ->whereIn('ProgID', getUserProgramAccess())
                    ->limit($limit)
                    ->get();
            return $get;
        //} else { return false; }
    }
    public function get_or_details($no){
        $get = DB::table('es_journals as J')
                    ->leftJoin('es_accounts as A','A.AcctID','=','J.AccountID')
                    ->select('A.AcctName','A.ShortName', 'J.Credit','J.TransRefNo', 'J.TransType', 'J.AccountID','J.EntryID')
                    ->where('ReferenceNo', $no)
                    ->where('TransID', 20)
                    ->where('Credit','>', 0)
                    ->get();

            return $get;
    }
    public function get_assess_details($no){
        $get = DB::table('es_journals as J')
                    ->leftJoin('es_accounts as A','A.AcctID','=','J.AccountID')
                //    ->selectRaw('[J].[Assess Fee] AS AssessFee')
                    ->select('J.TransID','A.AcctName','A.ShortName', 'J.Credit','J.TransRefNo', 'J.TransType', 'J.AccountID','J.EntryID','J.Debit', 'J.ActualPayment','J.ReferenceNo',
                    DB::raw("J.[1st Payment] as first,J.[2nd Payment] as second, J.[3rd Payment] as third,J.[4th Payment] as fourth,J.[5th Payment] as fifth"  ))
                    ->where('ReferenceNo', $no)
                    ->where('TransID', 1)
                    //->where('Credit','>', 0)
                    ->get();

            return $get;
    }

    public function setNonledger($entryid){
    //$get =   DB::statement("UPDATE es_journals SET NonLedger = 1, TransType = 0, TransRefNo = '' WHERE EntryID = '".$entryid."'");
    $get =   DB::table('es_journals')
                ->where('EntryID', $entryid)
                ->update(['NonLedger'=> 1, 'TransType'=> 0, 'TransRefNo'=> '']);
    return $get;
    }

    public function setNonledgerAll($termid,$entryid){
        //"UPDATE es_journals SET TermID = '" & TermID & "', NonLedger = 1, TransType = 0, TransRefNo = '' WHERE EntryID = '" & Val(fgLeft.TextMatrix(i, COL_L_ENTRYID)) & "'"
        $get =   DB::table('es_journals')
                    ->wherein('EntryID', $entryid)
                    ->update(['NonLedger'=> 1, 'TransType'=> 0, 'TransRefNo'=> '', 'TermID' => $termid]);
        return $get;

    }

    public function updateAYTerm($termid,$or_no){
         $tmpcheck = DB::SELECT("SELECT * FROM ES_Registrations WHERE TermID='".$termid."' AND StudentNo IN (SELECT PayorID FROM ES_OfficialReceipts WHERE ORNo='".$or_no."')");
		 $data     = ['TermID' => $termid];
		 if($tmpcheck && count($tmpcheck)>0){
		   $reginfo = $tmpcheck[0];
		 //$sltxn   = DB::statement("UPDATE ES_Journals SET TransRefNo='".$reginfo->RegID.";' WHERE TransID='20' AND ReferenceNo='".$or_no."' AND TransRefno NOT LIKE '".$reginfo->RegID.";%'");
		   $ortxn   = DB::statement("UPDATE ES_OfficialReceipts SET RefNo='".$reginfo->RegID."' WHERE ORNo='".$or_no."'");
		 }
		 
         $get =   DB::table('es_journals')
                     ->where('TransID', 20)
                     ->where('ReferenceNo',$or_no)
                     //->where('Debit','>',0)
                     ->update($data);
         return $get;
    }
	
    public function setTransref($new_type,$new_refno,$new_entryid,$entryid){
        $transrefno = $new_refno.';'.$new_entryid.';1';
    //    die($transrefno);
        //$get =   DB::statement("UPDATE es_journals SET NonLedger = 0, TransType = '".$new_type."', TransRefNo = '".$or_refno.";".$new_entryid.";1' WHERE EntryID = '".$entryid."' ");
        $get =   DB::table('es_journals')
                    ->where('EntryID', $entryid)
                    ->update(['NonLedger'=> 0, 'TransType'=> $new_type, 'TransRefNo'=>$transrefno]);
        return $get;
    }
    public function checkedDistributeTerms($entry_id){

        $get =   DB::table('es_journals')
                    ->where('EntryID', $entry_id)
                    ->update([ 'TermID'=> DB::raw("dbo.fn_GetAcademicYearTermID(TransType, REPLACE(SUBSTRING(TransRefNo, 1, CHARINDEX(';', TransRefNo, 1)), ';', '') )")]);
        return $get;

    }
    public function uncheckedDistributeTerms($entry_id, $termid){

        $get =   DB::table('es_journals')
                    ->where('EntryID', $entry_id)
                    ->update([ 'TermID'=> $termid]);
        return $get;

    }
    public function getStudentInfo($stud_no, $term = 0 )
    {
        $get = DB::table($this->table.' as S')
                ->select('S.StudentNo', 'LastName', 'FirstName', 'Middlename', 'MiddleInitial', 'ExtName', 'DateOfBirth', 'PlaceOfBirth', 'ReligionID', 'NationalityID', 'FamilyID', 'ForeignStudent', 'S.TermID', 'S.ProgID', 'MajorDiscID','DateAdmitted', 'CurriculumID', 'S.YearLevelID', 'ChineseName', 'S.Gender', 'S.StatusID', 'S.Res_Address', 'S.Res_Street', 'S.Res_Barangay', 'S.Res_TownCity', 'S.TblFeesID',
                DB::raw("dbo.fn_K12_YearLevel3(S.YearLevelID, S.ProgID) as YearLevelName, dbo.fn_MajorName(MajorDiscID) As Tracks,  dbo.fn_CurriculumCode(CurriculumID) As Curriculum,(select progname from es_programs where progid = S.Progid) as Program,(select CollegeName from es_colleges where collegeid = dbo.fn_ProgramCollegeID(S.ProgID)) As College, dbo.fn_IsForeign(NationalityID) AS IsForeign, dbo.fn_K12IsElder(S.StudentNo) AS IsEldest , dbo.fn_RegStudentStatus(S.TermID, '{$term}' ) As Status, dbo.fn_Age(S.DateOfBirth, GETDATE()) AS Age"), 'StudentPicture')
                // ->join('es_registrations as R', 'S.StudentNo', '=', 'R.StudentNo')
                ->where('S.StudentNo', $stud_no)
                ->first();

        return $get;
    }



    public function ayTerm($stud_no){
        //  $get = DB::table('es_ayterm')
        //         ->select('TermID', 'AcademicYear', 'SchoolTerm')
        //         //-
        //         ->orderBy('AcademicYear', 'DESC');
        // return $get;vf

     $get = DB::table('es_ayterm')->select('TermID', 'AcademicYear', 'SchoolTerm')->where(['Hidden' => 0])
     ->whereIn('TermID',[DB::raw("select termid from es_registrations where studentno ='{$stud_no}'")])
     ->orderBy('AcademicYear', 'DESC')->limit(0)->get();
        return $get;

    }

}
