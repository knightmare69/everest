<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class TableOfFees_Details extends Model
{
    protected $table = 'es_tableoffee_details';
    protected $primaryKey = 'IndexID';
    
   	protected $fillable  = array(
	   'TemplateID'
      ,'AccountID'
      ,'Amount'
      ,'Remarks'
      ,'ChargeMode'
      ,'Inactive'
      ,'CurrencyID' 
      ,'Succeeding'
      ,'SeqNo'
      ,'Option'
      ,'Option_Session'
      ,'Option_ProgID'
      ,'Option_Yr'
      ,'PracticumOnly'
       ,'1stPayment'
       ,'2ndPayment'
       ,'3rdPayment'
       ,'4thPayment'
       ,'5thPayment'
       ,'6thPayment'
       ,'7thPayment'
       ,'8thPayment'
       ,'9thPayment'
       ,'10thPayment'
       ,'NumPayment'
       ,'CustomPaymentType'       
	);

	public $timestamps = false;
    
    
    public function scopeAccdetails($query){
        return $query->leftJoin("es_accounts as a","a.AcctID","=","es_tableoffee_details.AccountID");                
    }
    
    
}