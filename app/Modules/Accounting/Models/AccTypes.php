<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class AccTypes extends Model
{
    protected $table = 'es_accountscategory';
    protected $primaryKey = 'CategoryID';

    protected $fillable  = ['CategoryCode', 'CategoryName', 'CategoryShort'];

    public $timestamps = false;
}
