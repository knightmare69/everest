<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class DebitCreditMemo extends Model
{
    protected $table = 'es_debitcreditmemo';
    protected $primaryKey = 'RefNo';

    protected $fillable  = 
        [  'RefNo'
		 , 'RefDate'
         , 'IDType'
         , 'IDNo'
         , 'Posted'
         , 'PostedDate'
         , 'CreatedBy'
         , 'CreatedDate'
         , 'ModifiedBy'
         , 'ModifiedDate'
         , 'IsVoid'
         ,'VoidDate'
         ,'TransType'
         ,'TransNo'
         ,'CampusID'
         ,'TermID'
         ,'BulkRefNo'
         ,'AutoAdjustFromTransType'
         ,'AutoAdjustTransNo'
         ,'Explanation'    
        ];

    public $timestamps = false;
}
?>  