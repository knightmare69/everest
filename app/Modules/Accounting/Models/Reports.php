<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Reports extends Model
{
    public static function AcademicTerm()
    {
        $get = DB::table('es_ayterm')->select('TermID', 'AcademicYear', 'SchoolTerm')
        // ->where('SchoolTerm', 'School Year')
        ->orderBy('AcademicYear', 'DESC')->get();

        return $get;
    }

    public function Programs()
    {
        $get = DB::table('es_programs')->select('ProgID', 'ProgName', 'ProgShortName', 'ProgClass')
        ->whereIn('ProgShortName', ['Grade School', 'High School', 'Senior High School'])
                // ->whereNotIn('ProgClass', [50, 80, 60])
                ->orderBy('ProgClass', 'ASC')->get();

        return $get;
    }

    public function getYearLevel($prog_class)
    {
        if($prog_class >= 50){
            $at = DB::table('vw_yearlevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->whereIn('YearLevel', ['Grade 11', 'Grade 12'])
                ->orderBy('SeqNo', 'ASC')->get();
        } else {
            $at = DB::table('vw_yearlevel')->select('YearLevelID', 'YearLevel', 'ProgClass')
                ->where('ProgClass', $prog_class)
                ->orderBy('SeqNo', 'ASC')->get();
        }

        return $at;
    }

    public function getSections($term_id, $prog_id, $yl_id)
    {
        if($prog_id == '29'){
            $yl_id = $yl_id == '5' ? [5, 1] : [$yl_id];
            $yl_id = $yl_id == '6' ? [6, 2] : [$yl_id];

        } else {
            $yl_id = [$yl_id];
        }

        $sec = DB::table('es_classsections')->select('SectionID', 'SectionName')
                ->where(['TermID' => $term_id, 'ProgramID' => $prog_id])
                ->whereIn('YearLevelID', $yl_id)
                ->orderBy('SectionName', 'ASC')->get();

        return $sec;
    }

    public function getStudent($find_str, $term, $program, $year_level, $section_id)
    {
        $student = DB::table('es_registrations as r')
                    ->select('s.StudentNo', 's.FirstName', 's.LastName', 's.MiddleName', 's.MiddleInitial')
                    ->join('es_students as s', 'r.StudentNo', '=', 's.StudentNo')
                    ->where(['r.ProgID' => $program, 'r.YearLevelID' => $year_level, 'r.TermID' => $term])
                    ->where(function($query) use ($find_str){
                        $query->where('r.StudentNo', '=', $find_str)
                        ->orWhere('s.LastName', 'like', '%'.$find_str.'%')
                        ->orWhere('s.FirstName', 'like', '%'.$find_str.'%');
                    });

        if($section_id != 0){
            $student->where(['r.ClassSectionID' => $section_id]);
        }

        return $student->get();
    }

    public function getStudentSectionID($student_no, $term_id, $prog_id, $year_level_id)
    {
        $get = DB::table('es_registrations')->select('ClassSectionID')
                ->where(['StudentNo' => $student_no, 'TermID' => $term_id, 'ProgID' => $prog_id, 'YearLevelID' => $year_level_id])->first();

        $ret = !empty($get->ClassSectionID) ? $get->ClassSectionID : 0;
        return $ret;
    }

    public function getProjections($term, $prog){

        $get = DB::select("SELECT IFNULL(p.IndexID,0) AS IndexID, ProgClass, {$term} AS TermID , 1 AS CampusID, y.ProgID, y.YearLevelID, y.YearLevelName AS YearLevel,
                        fn_ProgramCollegeID(y.ProgID) AS CollegeID, m.MajorDiscID AS MajorID, fn_MajorName(m.MajorDiscID) AS Major,
                        IFNULL(p.NoSections,0) As NoSections,
                        IFNULL(p.NoStudents,0) As NoStudents
                        FROM es_yearlevel y
                        LEFT JOIN es_programmajors m ON y.ProgID=m.ProgID
                        LEFT JOIN ES_EnrollmentProjections p ON p.ProgID = y.ProgID AND p.MajorID = m.MajorDiscID AND p.YearLevelID=y.YearLevelID
                        WHERE y.ProgID = {$prog}");

        return $get;

    }

    public function setProjections($term, $post){

        $prog = decode(getObjectValue($post,'prog'));

        $whre = array(
            'TermID' => $term,
            'ProgID' => $prog,
            'MajorID' => getObjectValue($post,'major'),
            'YearLevelID' => getObjectValue($post,'level'),

        );

        $data = array(
            'TermID' => $term,
            'CampusID' => 1,
            'ProgID' => $prog,
            'MajorID' => getObjectValue($post,'major'),
            'YearLevelID' => getObjectValue($post,'level'),
            'NoSections' => getObjectValue($post,'sec'),
            'NoStudents' => getObjectValue($post,'stu'),
        );

        $ret = DB::table('ES_EnrollmentProjections')->where($whre)->count();

        if($ret > 0 ){
            $ret = DB::table('ES_EnrollmentProjections')->where($whre)->update($data);
        }else{
            $ret = DB::table('ES_EnrollmentProjections')->insert($data);
        }

        return $ret;
    }
}
