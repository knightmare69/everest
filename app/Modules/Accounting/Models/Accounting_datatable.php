<?php

namespace App\Modules\Accounting\Models;

use illuminate\Database\Eloquent\Model;
use DB;
use Permission;
use Request;
use Response;

class Accounting_datatable extends Model
{
    public function AcademicTerm()
    {
        $get = DB::table('ES_AYTerm')->select('TermID', 'AcademicYear', 'SchoolTerm','Active_OnlineEnrolment')
        // ->where('SchoolTerm', 'School Year')
        ->where('Hidden','0')        
        ->orderBy('AcademicYear', 'DESC')
        ->orderBy('SchoolTerm', 'DESC')
        ->get();

        return $get;
    }

public function StudentWithoutDiscounts($termid){
		$notin = DB::table('ES_OtherTransactions')->select('regid')->get();
		$collect = json_decode(json_encode((array) $notin), true);
		//print_r($collect);
		$get = DB::table('ES_registrations')->select('regid')->wherenotin('regid',$collect)->where('termid',$termid)->count();
		return $get;

}

public function StudentWithDiscounts($termid){
		$notin = DB::table('ES_OtherTransactions')->select('regid')->get();
		$collect = json_decode(json_encode((array) $notin), true);
		//print_r($collect);
		$get = DB::table('ES_registrations')->select('regid')->whereIn('regid',$collect)->where('termid',$termid)->count();
		return $get;

}


public function StudentDiscountsTotal(){
	$get= DB::select('EXEC sp_students_with_without_discounts');
	return $get;
}

public function DiscountsinPhp(){
	$get= DB::select('EXEC sp_total_discounts_php');
	return $get;
}

public function TuitionFeeDiscounts(){
	$get= DB::select('EXEC sp_tuition_fee_discounts');
	return $get;
}

public function Reservation_Payments(){
	$get= DB::select('EXEC sp_reservation_and_payments');
	return $get;
}





}
