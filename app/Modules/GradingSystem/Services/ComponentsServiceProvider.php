<?php

namespace App\Modules\GradingSystem\Services;

use App\Modules\GradingSystem\Services\Components\Validation;
use DB;

class ComponentsServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'save')
    {
        if ($action == 'save') {
            $validate = $this->validation->validateDefault($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }
        return ['error' => false, 'message' => ''];
    }

    public function postComponentNew($post)
    {
        $return = [];

        foreach ($post as $val) {
            $return[] = array(
                'CompCode' => getObjectValue($val, 'code'),
                'CompDescription' => getObjectValue($val, 'description'),
                'Projection' => getObjectValue($val, 'projection'),
                'SortOrder' => 0,
                'LastModifiedBy' => getUserID(),
                'LastModifiedDate' => date('Y-m-d H:i:s')
            );
        }

        return $return;
    }

    public function postComponentEdit($post)
    {
        return array(
            'CompCode' => getObjectValue($post, 'code'),
            'CompDescription' => getObjectValue($post, 'description'),
            'Projection' => getObjectValue($post, 'projection'),
            'SortOrder' => 0,
            'LastModifiedBy' => getUserID(),
            'LastModifiedDate' => date('Y-m-d H:i:s')
        );
    }
}
