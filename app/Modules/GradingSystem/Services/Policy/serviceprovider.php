<?php

namespace App\Modules\GradingSystem\Services\Policy;
use Validator;

class serviceprovider
{
    public function __construct()
    {
 
    }

    public function validateDefault($post)
    {
        
        $validator = validator::make(
			['policy'		=>  getObjectValue($post,'policy')],			
			['policy'		=> 'required']
		);
		
        return $validator;                
    }
    
    public function isValid($post, $action = 'save')
    {
        $result = ['error' => false, 'message' => ''];
        switch($action){
            case 'save': 
            
                $validate = $this->validateDefault($post);
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }
            
            break;
            
            case 'component-insert':
            
             
            break;
        }
        
        return $result;
    }

    public function postPolicy($post)
    {
                
        $return = array(
            'Policy' => getObjectValue($post, 'policy'),
            'PolicyDescription' => getObjectValue($post, 'desc'),  
            'ProgID' => getObjectValue($post, 'program'),
            'IsClub' => isset($post['club'])?  1 : 0 ,
            'IsConduct' => isset($post['conduct'])?  1 : 0 ,
            'IsZeroBased' => isset($post['zerobased'])?  1 : 0 ,
            'Inactive' => isset($post['inactive'])?  1 : 0 ,
            
            'Createdby' => getUserID(),
            'DateCreated' => systemDate()
        );
                
        return $return;
    }
}
