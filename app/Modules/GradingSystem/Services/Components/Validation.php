<?php

namespace App\Modules\GradingSystem\Services\Components;

use Validator;

class Validation
{
    public function validateDefault($post)
    {
        $data = [];
        $rules = [];

        if(!empty($post['collect']['new'])){            
            foreach ($post['collect']['new'] as $key => $new) {
                $data['new_code_'.$key] = getObjectValue($new, 'code');
                $rules['new_code_'.$key] = 'required|string';

                $data['new_description_'.$key] = getObjectValue($new, 'description');
                $rules['new_description_'.$key] = 'required|string';
            }
        }

        if(!empty($post['collect']['update'])){
            foreach ($post['collect']['update'] as $key => $update) {
                $data['code_'.$key] = getObjectValue($update, 'code');
                $rules['code_'.$key] = 'required|string';

                $data['description_'.$key] = getObjectValue($update, 'description');
                $rules['description_'.$key] = 'required|string';
            }
        }

        $validator = validator::make($data, $rules);

        return $validator;
    }
}
