<?php

namespace App\Modules\GradingSystem\Services;

use App\Modules\GradingSystem\Services\Areas\Validation;
use DB;

class AreasServiceProvider
{
    public function __construct()
    {
        $this->validation = new Validation();
    }

    public function isValid($post, $action = 'save')
    {
        if ($action == 'save') {
            $validate = $this->validation->validateDefault($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        } else if($action == 'search-coordinator') {
            $validate = $this->validation->validateSearchCoord($post);
            if ($validate->fails()) {
                return ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
            }
        }
        return ['error' => false, 'message' => ''];
    }

    // public function postAreasNew($post)
    // {
    //     $return = [];
    //
    //     foreach ($post as $val) {
    //         $return[] = array(
    //             'SubjectAreaName' => getObjectValue($val, 'name'),
    //             'ShortName' => getObjectValue($val, 'short_name'),
    //             'CoordinatorID' => getObjectValue($val, 'coordinator'),
    //         );
    //     }
    //
    //     return $return;
    // }

    public function postAreas($post)
    {
        return array(
            'SubjectAreaID' => getObjectValue($post, 'subj_id'),
            'SubjectAreaName' => getObjectValue($post, 'name'),
            'ShortName' => getObjectValue($post, 'short_name'),
            'CoordinatorID' => getObjectValue($post, 'coordinator'),
        );
    }

}
