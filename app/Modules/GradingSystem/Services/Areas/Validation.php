<?php

namespace App\Modules\GradingSystem\Services\Areas;

use Validator;

class Validation
{
    public function validateSearchCoord($post)
    {
        $validator = validator::make(
            ['coordinator_name' => getObjectValue($post, 'search-coord')],
            ['coordinator_name' => 'required|string']
        );

        return $validator;
    }

    public function validateDefault($post)
    {
        $data = [];
        $rules = [];

        if(!empty($post['collect']['new'])){
            foreach ($post['collect']['new'] as $key => $new) {
                $data['new_name_'.$key] = getObjectValue($new, 'name');
                $rules['new_name_'.$key] = 'required|string';

                $data['new_short_name_'.$key] = getObjectValue($new, 'short_name');
                $rules['new_short_name_'.$key] = 'required|string';

                $data['new_coordinator_'.$key] = getObjectValue($new, 'coordinator');
                $rules['new_coordinator_'.$key] = 'string';
            }
        }

        if(!empty($post['collect']['update'])){
            foreach ($post['collect']['update'] as $key => $update) {
                $data['name_'.$key] = getObjectValue($update, 'name');
                $rules['name_'.$key] = 'required|string';

                $data['short_name_'.$key] = getObjectValue($update, 'short_name');
                $rules['short_name_'.$key] = 'required|string';

                $data['coordinator_'.$key] = getObjectValue($update, 'coordinator');
                $rules['coordinator_'.$key] = 'string';
            }
        }

        $validator = validator::make($data, $rules);

        return $validator;
    }
}
