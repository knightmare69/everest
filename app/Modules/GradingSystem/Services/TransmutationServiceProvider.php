<?php

namespace App\Modules\GradingSystem\Services;
use Validator;

class TransmutationServiceProvider
{
    public function __construct()
    {
 
    }

    public function validateDefault($post)
    {
        
        $validator = validator::make(
			['templatename'		=>  getObjectValue($post,'templatename')],			
			['templatename'		=> 'required']
		);
		
        return $validator;                
    }
    
     public function validateGrade($post)
    {
        
        $validator = validator::make(
			['min'		=>  getObjectValue($post,'min'),
             'template'   =>  getObjectValue($post,'template'),
           
            ],			
			['template'	=> 'required', 'min' => 'required']
		);
		
        return $validator;                
    }
    
    public function isGradeValid($post){
        $result = ['error' => false, 'message' => ''];            
        $validate = $this->validateGrade($post);
        if ($validate->fails()) {
            $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
        }
        return $result;
    }
    
    public function isValid($post, $action = 'save')
    {
        $result = ['error' => false, 'message' => ''];
        switch($action){
            case 'save': 
            
                $validate = $this->validateDefault($post);
                if ($validate->fails()) {
                    $result =  ['error' => true, 'message' => getErrorMessages($validate->errors()->getMessages())];
                }
            
            break;
            
            case 'component-insert':
            
             
            break;
        }
        
        return $result;
    }

    public function postTemplate($post)
    {
                
        $return = array(
            'TemplateName' => getObjectValue($post, 'templatename'),
            'Description' => getObjectValue($post, 'desc'),  
            'Inactive' => getObjectValue($post, 'inactive'),
        );
                
        return $return;
    }
    
    public function postGrade($post)
    {                
        $return = array(
            'TemplateID' => decode(getObjectValue($post, 'template')),
            'Min' => getObjectValue($post, 'min'),
            'Max' => getObjectValue($post, 'max'),
            'TransmutedGrade' => getObjectValue($post, 'grade'),
            'Inactive' => getObjectValueWithReturn($post, 'inactive','0'),
        );
                
        return $return;
    }
    
}
