<div class="row"><div class="col-md-12 col-sm-12">
<div class="portlet box green-haze" id="try">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-cubes"></i> List of Components </div>
        <div class="actions">            
            <div class="btn-group">                      		            
				<a class="btn btn-sm btn-default" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
					<i class="fa fa-reorder"></i>
				</a>
				<ul class="dropdown-menu pull-right">
                	<li>
						<a class="font-blue" href="javascript:;" id="comp-edit">
						<i class="fa fa-pencil font-blue"></i> Edit </a>
					</li>
					<li>
						<a class="font-red" href="javascript:;" id="comp-remove">
						<i class="fa fa-trash-o font-red"></i> Remove </a>
					</li>
                    <li class="divider"></li>
                    <li>
						<a class="font-green" href="javascript:;" id="comp-save">
						<i class="fa fa-check font-green"></i> Save </a>
					</li>
                    <li>
						<a href="javascript:;" id="comp-cancel">
						<i class="fa fa-times"></i> Cancel </a>
					</li>
				</ul>
			</div>
        </div>
    </div>
    <div class="portlet-body" >
        <a href="javascript:;" class="btn btn-primary btn-sm" id="comp-add-row"><i class="fa fa-cube"></i> Add Row </a>
        <hr style=" margin-bottom: 3px; margin-top: 3px;" />
        <div class="table_wrapper" id="component-result-holder" >
            @include($views.'table')
        </div>        
    </div>
</div>
</div></div>