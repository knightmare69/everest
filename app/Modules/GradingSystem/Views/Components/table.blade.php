<table class="table table-striped table-bordered table-condensed table-hover table_scroll" id="records-table">
    <thead>
        <tr>
            <th width="5%">
                <label><input type="checkbox" id="check-parent"></label>
            </th>
            <th>Code</th>
            <th>Component Name</th>
            <th>Projection</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr data-id="0">
            <td>
                <label><input type="checkbox" class="check-child" data-id="0"></label>
            </td>
            <td><input type="text" class="form-control code"></td>
            <td><input type="text" class="form-control description"></td>
            <td><input type="text" class="form-control projection numberonly " maxlength="2" ></td>
            <td></td>
        </tr>
        @if(!empty($data))
            @foreach($data as $_this)
            <tr data-id="{{ encode($_this->fldPK) }}">
                <td>
                    <label><input type="checkbox" class="check-child"></label>
                </td>
                <td>
                    <p>{{ $_this->CompCode }}</p>
                    <input type="text" class="form-control code hide" value="{{ $_this->CompCode }}">
                </td>
                <td>
                    <p>{{ $_this->CompDescription }}</p>
                    <input type="text" class="form-control description hide" value="{{ $_this->CompDescription }}">
                </td>
                <td>
                    <p>{{ $_this->Projection }}</p>
                    <input type="text" class="form-control projection hide" value="{{ $_this->Projection }}">
                </td>
                <td></td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>