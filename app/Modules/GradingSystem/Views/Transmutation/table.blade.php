<?php       
$TemplateID = ( isset($TemplateID) ? $TemplateID : 0 );
$data = App\Modules\GradingSystem\Models\Transmutation\Transtable_model::where('TemplateID',$TemplateID)->orderBy('Max','DESC')->get();     
?>
<table id="tblgrades" class="table table-condensed  table-hover" style="cursor: pointer !important;">
    <thead>
        <th>#</th>
        <th>Initial Grade</th>                        
        <th class="">Transmuted Grade</th>
        <th class="text-center ">Action</th>
    </thead>
    <tbody>
    @for($i=0; $i < count($data) ; $i++ )
        <tr data-template="<?= encode($data[$i]->TemplateID) ?>" data-min="<?= $data[$i]->Min ?>" data-max="<?= $data[$i]->Max ?>"
            data-grade="<?= $data[$i]->TransmutedGrade ?>"
            data-id="<?= encode($data[$i]->IndexID) ?>">
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="bold text-primary"><?= ($data[$i]->Min == $data[$i]->Max ? number_format($data[$i]->Min,2) :  number_format($data[$i]->Min,2) . ' - '. number_format( $data[$i]->Max,2)   ) ?></td>			    
            <td class="bold "><?= $data[$i]->TransmutedGrade  ?> </a></td>
            <td class="autofit"><a class="edit" data-menu="edit" data-mode="grades"  href="javascript:void(0)">Edit</a></td> 
        </tr> 
    @endfor        
    </tbody>
</table>	                    	
