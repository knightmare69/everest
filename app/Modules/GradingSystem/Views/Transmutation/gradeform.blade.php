<?php
?>
<div id="grade_modal" class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Tranmutation Info</h4>
			</div>
			<div class="modal-body">
				<form action="#" class="form-horizontal" id="form_grade" name="form_grade">
                    <fieldset>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Min</label>
                        <div class="col-md-8">                                    						
				            <input type="text" id="min" name="min" class="form-control input-small numberonly"  data-allow="173">
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Max</label>
                        <div class="col-md-8">                                    						
				            <input type="text" id="max" name="max" class="form-control input-small numberonly"  data-allow="173">
                        </div>                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Transmuted Grade</label>
                        <div class="col-md-8">                                    						
				            <input type="text" id="grade" name="grade" class="form-control input-small">
                        </div>                        
                    </div>				
                    		
                    <div class="form-group">
			            <label class="col-md-4 control-label">&nbsp; </label>
                        <div class="col-md-8">
                            <div class="input-group">
    							<div class="icheck-inline">								
                                    <label class="checkbox"><input type="checkbox" id="inactive" name="inactive" class="zeckbox"> Inactive </label>
    							</div>
    						</div>
                        </div>
					</div>
                    <input type="hidden" id="idx" name="idx" value="0" />
                    <input type="hidden" id="template" name="template" value="0" />
                    </fieldset>
                    
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
				<button class="btn btn-success" data-menu="save" data-mode="grades" type="button">Save Form</button>
			</div>
		</div>
	</div>
</div>