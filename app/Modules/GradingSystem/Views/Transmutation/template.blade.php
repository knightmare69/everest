<?php $table = App\Modules\GradingSystem\Models\Transmutation\Transmutation_model::get(); ?>
<table id="tbltemplate" class="table table-condensed table-hover" style="cursor: pointer !important;">
    <thead>
        <th>#</th>
        <th>Template Name</th>
        <th>Status</th>
    </thead>
    <tbody>
    @for($i=0; $i < count($table) ; $i++ )
        <tr data-id ="<?= encode($table[$i]->TranstblID) ?>" data-name="<?= $table[$i]->TemplateName ?>" >
            <td class="autofit font-xs">{{$i + 1}}.</td>
            <td class="name" ><?= $table[$i]->TemplateName ?></td>		    
            <td class="text-center state"><?= $table[$i]->Inactive ==1 ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : ""; ?></td>
        </tr>
    @endfor
    </tbody>
</table>