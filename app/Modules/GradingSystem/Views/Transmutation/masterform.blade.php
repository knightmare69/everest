<div id="template_modal" class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Form Template</h4>
			</div>
			<div class="modal-body">
				<form action="#" class="form-horizontal" id="form_template" name="form_template">
					<div class="form-group">
						<label class="control-label col-md-4">Name</label>
						<div class="col-md-8">
							<div class="input-group input-medium">
								<input type="text" id="templatename" name="templatename" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4">Description</label>
						<div class="col-md-8">
							<div class="input-group input-medium" >
								<textarea id="desc" name="desc" style="resize: none !important;" class="form-control input-sm"></textarea>
							</div>
							<!-- /input-group -->
						</div>
					</div>				
                    <div class="form-group">
						<label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                        <div class="input-group">
							<div class="icheck-inline">								
                                <label class="checkbox"><input type="checkbox" id="inactive" name="inactive" class="zeckbox"> Inactive </label>
							</div>
						</div>
                        </div>
					</div>
                    <input type="hidden" id="idx" name="idx" value="0" />
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
				<button class="btn green btn-primary" data-menu="save" data-mode="template" type="button">Save Form</button>
			</div>
		</div>
	</div>
</div>