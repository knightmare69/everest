<div class="row"><div class="col-sm-12">
<div class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption"> 
            <i class="fa fa-table"></i>
            <span class="caption-subject bold "> Transmutation Table </span><br />
            <span class="caption-helper"> Use this module to configure transmutation table...</span>
        </div>
		<div class="tools">				                                        
            <a href="javascript:;" class="collapse"></a>				
			<a href="javascript:;" class="reload"></a>
		</div>
	</div>
	<div class="portlet-body clearfix">
        <div class="row">
            <div id="left_container" >
                <div id="template-container" class="well well-light well-sm" style="overflow: auto; height: 550px;">
                   <button class="btn btn-primary btn-sm" data-menu="create" data-mode="template" type="button"><i class="fa fa-file"></i> Create </button>
                   <button class="btn btn-info btn-sm" data-menu="edit" data-mode="template" type="button"><i class="fa fa-edit"></i> Edit </button>
                   <button class="btn btn-danger btn-sm" data-menu="delete" data-mode="template" type="button"><i class="fa fa-times"></i> Delete </button>
                   <hr style="margin-bottom: 5px !important; margin-top: 5px !important;" />
                   <div id="templatecontainer">
                    @include($views.'template')
                   </div>                   
                                      
                </div>               
            </div>
            <div id="right_container">
                <div class="well well-light well-sm" style="overflow: auto; height: 550px; margin-bottom: 0px;">
                    
                    <div class="text-right">
                    <h5 class="pull-left bold text-success" style="margin-bottom: 8px;">Transmutation Table</h5>
                   <button class="btn btn-success btn-sm" data-menu="create" data-mode="grades" type="button"><i class="fa fa-file"></i> New </button>                   
                   <button class="btn btn-warning btn-sm" data-menu="delete" data-mode="grades" type="button"><i class="fa fa-times"></i> Remove </button>
                        <hr style="margin-bottom: 5px !important; margin-top: 5px !important;" />
                    </div>
                   
                   <div id="gradecontainer">
                    @include($views.'table')
                   </div>                
                </div>            
            </div>
        </div>            
	</div>
</div></div></div>
@include($views.'masterform')
@include($views.'detailform')