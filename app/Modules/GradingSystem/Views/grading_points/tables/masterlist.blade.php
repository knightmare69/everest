<?php
	$data = \App\Modules\GradingSystem\Models\Grading\Points::data();

	$data = $data->where('ProgClassCode',decode(Request::get('code')));
?>
<table class="table table-hover" id="TableMasterList">
	<thead>
		<tr class="center bg-grey-cascade">
			<th class="sm-font upper-letter center cs-td-valign" width="5">#</th>
			<th class="sm-font upper-letter center cs-td-valign" width="20">Point</th>
			<th class="sm-font upper-letter left cs-td-valign" width="30">Equivalence</th>
			<th class="sm-font upper-letter left cs-td-valign" width="50">Description</th>
			<th class="sm-font upper-letter center cs-td-valign" width="50">Grade Remark</th>
			<th class="sm-font upper-letter center cs-td-valign" width="30">Equiv. Grade</th>
			<th class="sm-font upper-letter center cs-td-valign" width="30">Equiv. Grade (GWA)</th>
			<th class="sm-font upper-letter center cs-td-valign" width="30">Not Credited Char.</th>
			<th class="sm-font upper-letter center cs-td-valign" width="30">Credit the unit</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Include summary failures</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Disqualify scholarship/Academic Honor</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Hide fr. Faculty</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Compute GPA/GWA</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Deducted to credit Units (GPA Computation)</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Grades periods</th>
			<th class="sm-font upper-letter center cs-td-valign" width="10">Hide Evaluation form</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; ?>
		@foreach($data->get() as $row)
		<tr class="center cs-tbody-td cursor-pointer rowSel" data-id="{{ encode($row->GradeID) }}">
			<td class="bold">{{ $i++ }}</td>
			<td>{{ $row->Format == '1' ? $row->Grade : $row->MinGrade.' - '.$row->MaxGrade }}</td>
			<td class="left">{{ $row->Equivalence }}</td>
			<td class="left">{{ trimLength($row->GradeDescription,100,'...') }}</td>
			<td>{{ $row->Remarks }}</td>
			<td>{{ $row->EquivGrade }}</td>
			<td>{{ $row->EquivGradeGWA }}</td>
			<td>{{ $row->NotCreditedChar }}</td>
			<td><label class="label label-{{ $row->CreditTheUnit ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->ComputeInSummaryofFailures ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->SchoDisqualify ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->HideInFaculty ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->ComputeGWA ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->DeductedToEnrolledUnits ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->GradesFollowingPeriods ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
			<td><label class="label label-{{ $row->HideInEvaluation ? 'success' : 'default' }}"><i class="fa fa-check"></i></label></td>
		</tr>
		@endforeach
		@if($data->count() > 0)
		<tr class="cs-row-color">
			<td colspan="16" class="left">Total records(s): <b>({{ $data->count() }})</b></td>
		</tr>
		@endif
		@if($data->count() <= 0)
		<tfoot>
			<tr class="cs-row-color">
				<td colspan="16" class="center bold">No result found.</td>
			</tr>
		</tfoot>
		@endif
	</tbody>
</table>