<style>

	#TableMasterList > tbody > tr:hover,.row-selected {
		background-color: #e08283 !important;
		color: #fff !important;
	}

	.cs-row-color {
		background-color: #e7e7e7  !important;
	}

	.cs-row-color:hover {
		background-color: #fff  !important;
	}
	.cs-table-no-border {
		border-top: 0 none !important;
	}

	.cs-tbody-tr-no-border td {
		border: 0px none !important;
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
	}

	.cs-tbody-tr-border-bottom {
		border-top: 0px none !important;
		border-left: 0px none !important;
		border-right: 0px none !important;
		border-bottom: 0px none !important;
		/*border-bottom: 1px solid #e5e5e5 !important;*/
	}

	.no-border-top {
		border-top: 0px none !important;
	}

	.cs-td-valign {
		vertical-align: middle !important;
	}

	.cs-input-xs {
		width: 60px !important;
	}

	#GradingSettings {
		background-color: #e7e7e7 !important;
	}

	#GradingSettings li{
		float: left;
		list-style: none;
	}

	#GradingSettings li:first-child{
		margin-left: -41px !important;
		padding-left: 0 !important;
	}

	#GradingSettings li a{
		color: #777 !important;
		padding: 10px;
		border-radius: 4px;
		text-decoration: none;

	}
	#GradingSettings li.active a, #GradingSettings > li a:hover {
		background: #428bca none !important;
		color: #fff !important;
	}
	

</style>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-file"></i> Grading Points
		</div>
		<div class="actions btn-set">
			<button class="btn green" id="BtnCreate"><i class="fa fa-file"></i> Create</button>
			<button class="btn red" id="BtnDelete"><i class="fa fa-trash"></i> Delete</button>
			<div class="btn-group">
				<a data-toggle="dropdown" href="#" class="btn yellow dropdown-toggle">
				<i class="fa fa-share"></i> More <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#">
						Print </a>
					</li>
					<li class="devider"></li>
					<li>
						<a href="#">
						Export to CSV </a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<ul id="GradingSettings">
					@foreach(App\Modules\GradingSystem\Models\Grading\Settings::orderBy('Classification','asc')->get() as $row)
					<li class="{{ $row->IndexID == 1 ? 'active' : '' }}" data-code="{{ encode($row->Classification) }}">
						<a href="javascript:;" class="aSettings">{{ $row->Remarks }}</a>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="clearfix margin-top-10"></div>
		<div id="TableWrapper">
		@include($views.'tables.masterlist')
		</div>
	</div>
</div>