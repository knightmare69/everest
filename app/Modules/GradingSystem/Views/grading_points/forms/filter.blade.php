<form class="horizontal-form" id="FormMoreFilter" action="#" method="POST">
    <div class="row">
        <div class="form-group">
             <div class="col-md-4">
                <label class="control-label bold">Course Code</label>
                <input type="text" placeholder="Course Code"  class="form-control input-xs" name="CourseCode" id="CourseCode" value="{{ Request::get('CourseCode') }}">
             </div>
             <!--/span-->
             <div class="col-md-8">
                <label class="control-label bold">Course Title</label>
                <input type="text" placeholder="Course Title" class="form-control input-xs" name="CourseTitle" id="CourseTitle" value="{{ Request::get('CourseTitle') }}">
             </div>
             <!--/span-->
        </div>
        <div class="form-group">
             <div class="col-md-4">
                <label class="control-label bold">Parent Code</label>
                <input type="text" placeholder="Course Code"  class="form-control input-xs" name="ParentCode" id="ParentCode" value="{{ Request::get('ParentCode') }}">
             </div>
             <!--/span-->
             <div class="col-md-8">
                <label class="control-label bold">Course Level</label>
                <select class="form-control" name="CourseLevel" id="CourseLevel">
                    <option value="">All</option>
                    @foreach(App\Modules\Setup\Models\ProgramClass::where('ClassCode','<=',21)->get() as $row)
                    <option {{ Request::get('CourseLevel') == $row->ClassCode ? 'selected' : '' }} value="{{ $row->ClassCode }}">{{ $row->ClassDesc }}</option>
                    @endforeach
                </select>
             </div>
             <!--/span-->
        </div>
    </div>
</form>