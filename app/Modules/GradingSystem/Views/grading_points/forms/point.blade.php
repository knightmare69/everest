<?php
    global $data;
    $data = \App\Modules\GradingSystem\Models\Grading\Points::limit(1)->where('GradeID',decode(Request::get('key')))->first();

    function isCheck($isCheck = false) {
        return $isCheck ? 'checked' : '';
    }

    function isSel($a,$b) {
        return $a == $b ? 'selected' : '';
    }
?>
<!-- BEGIN FORM-->
<form class="horizontal-form form_crud" action="#" method="POST">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @include('errors.event')
            </div>
        </div>
        <div class="row">
             <div class="col-md-12">
                    <label class="control-label bold">
                        Format
                    </label>
                    <select class="form-control" name="FormatType" id="FormatType">
                        <option value="1" {{ isFormatOne() ? 'selected' : '' }}>Format 1</option>
                        <option value="2" {{ !isFormatOne() ? 'selected' : '' }}>Format 2</option>
                    </select>
                </div>
            <div class="col-md-6 margin-top-10">
                <div class="col-md-12">
                    <label class="control-label bold">Information</label>
                </div>
                <div class="form-group format-one {{ (!isFormatOne()) ? 'hide' : '' }}">
                    <div class="col-md-6">
                        <label class="control-label ">Grade Point <small><i class="text-danger">*</i></small></label>
                        <input type="text" placeholder="0" class="form-control input-xs" name="Grade" id="Grade" value="{{ getObjectValue($data,'Grade') }}">
                    </div>
                </div>
                <div class="form-group format-two {{ (isFormatOne()) ? 'hide' : '' }}">
                    <div class="col-md-12">
                        <label class="control-label ">Grade Point <small><i class="text-danger">*</i></small></label>
                    </div>
                     <div class="col-md-6">
                        <label class="control-label ">Min</label>
                        <input type="text" placeholder="0" class="form-control input-xs" name="MinGrade" id="MinGrade" value="{{ getObjectValue($data,'MinGrade') }}">
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <label class="control-label ">Max</label>
                        <input type="text" placeholder="0" class="form-control input-xs" name="MaxGrade" id="MaxGrade" value="{{ getObjectValue($data,'MaxGrade') }}">
                     </div>
                     <!--/span-->
                </div>
                <div class="form-group">
                     <div class="col-md-12">
                        <label class="control-label ">Equivalence <small><i class="text-danger">*</i></small></label>
                        <input type="text" class="form-control" name="Equivalence" id="Equivalence" value="{{ getObjectValue($data,'Equivalence') }}">
                     </div>
                </div>

                <div class="form-group">
                     <div class="col-md-12">
                        <label class="control-label ">Description</label>
                         <select class="form-control" name="GradDescID" id="GradDescID">
                            <option value="">- SELECT -</option>
                            @foreach(App\Modules\GradingSystem\Models\Grading\Description::get() as $row)
                            <option {{ isSel(getObjectValue($data,'GradDescID'),$row->GradeDescID)  }}  value="{{ encode($row->GradeDescID) }}">{{ ucfirst($row->GradeDescription) }}</option>
                            @endforeach
                        </select>
                     </div>
                </div>

                <div class="form-group">
                     <div class="col-md-12">
                        <label class="control-label ">Remarks</label>
                         <select class="form-control" name="FinalRemarkID" id="FinalRemarkID">
                            <option value="">- SELECT -</option>
                            @foreach(App\Modules\GradingSystem\Models\Grading\Remarks::get() as $row)
                            <option {{ isSel(getObjectValue($data,'FinalRemarkID'),$row->RemarkID)  }} value="{{ encode($row->RemarkID) }}">{{ ucfirst($row->Remarks) }}</option>
                            @endforeach
                        </select>
                     </div>
                </div>
            </div>
            <div class="col-md-6 margin-top-10">
                <div class="col-md-12">
                    <label class="control-label bold">Numerical Equivalent</label>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label ">Equivalent Grade</label>
                        <input type="text" placeholder="" class="form-control input-xs not-required" name="EquivGrade" id="EquivGrade" value="{{ getObjectValue($data,'EquivGrade') }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label ">Equivalent Grade (GWA)</label>
                        <input type="text" placeholder="" class="form-control input-xs not-required" name="EquivGradeGWA" id="EquivGradeGWA" value="{{ getObjectValue($data,'EquivGradeGWA') }}">
                    </div>
                </div>
                 <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label ">Not Credited Character</label>
                        <input type="text" placeholder="Character" class="form-control input-xs not-required" name="NotCreditedChar" id="NotCreditedChar" value="{{ getObjectValue($data,'NotCreditedChar') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table margin-top-10">
                    <tbody>
                        <tr class="cs-tbody-tr-no-border">
                            <td  class="" colspan="2">SETTINGS</td>
                        </tr>
                        <tr class="cs-tbody-tr-no-border">
                            <td  class="upper-letter xs-font">
                                <input type="checkbox" class="form-control" name="CreditTheUnit" id="CreditTheUnit" {{ isCheck(getObjectValue($data,'CreditTheUnit')) }}>
                                <label class="xs-font" for="CreditTheUnit">Credit the unit</label>
                            </td>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="ComputeGWA" id="ComputeGWA"  {{ isCheck(getObjectValue($data,'ComputeGWA')) }}>
                                <label class="xs-font" for="ComputeGWA">Compute GPA/GWA</label>
                            </td>
                        </tr>
                        <tr>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="ComputeInSummaryofFailures" id="ComputeInSummaryofFailures"  {{ isCheck(getObjectValue($data,'ComputeInSummaryofFailures')) }}>
                                <label class="xs-font" for="ComputeInSummaryofFailures">Include summary failures</label>
                            </td>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="DeductedToEnrolledUnits" id="DeductedToEnrolledUnits"  {{ isCheck(getObjectValue($data,'DeductedToEnrolledUnits')) }}>
                                <label class="xs-font" for="DeductedToEnrolledUnits">Deducted to credit Units (GPA Computation)</label>
                            </td>
                        </tr>
                        <tr>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="SchoDisqualify" id="SchoDisqualify"  {{ isCheck(getObjectValue($data,'SchoDisqualify')) }}>
                                <label class="xs-font" for="SchoDisqualify">Disqualify scholarship/Academic Honor</label>
                            </td>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="GradesFollowingPeriods" id="GradesFollowingPeriods"  {{ isCheck(getObjectValue($data,'GradesFollowingPeriods')) }}>
                                <label class="xs-font" for="GradesFollowingPeriods">Grades also for the following periods</label>
                            </td>
                        </tr>
                        <tr>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="HideInFaculty" id="HideInFaculty"  {{ isCheck(getObjectValue($data,'HideInFaculty')) }}>
                                <label class="xs-font" for="HideInFaculty">Hide this grade from Faculty Grade encoding</label>
                            </td>
                            <td  class="upper-letter xs-font">
                                <input type="checkbox"  class="form-control" name="HideInEvaluation" id="HideInEvaluation"  {{ isCheck(getObjectValue($data,'HideInEvaluation')) }}>
                                <label class="xs-font" for="HideInEvaluation">Hide this grade from Evaluation form</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->