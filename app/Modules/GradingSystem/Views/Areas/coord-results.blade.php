<?php $i=1;?>
@if(!empty($coordinator))
<div class="scrollbox" style="height: 400px; overflow: auto;">
<table class="table table-striped table-condensed table-hover table_scroll dataTable no-footer" id="table-coord-res">
    <thead>
        <tr>
            <th>#</th>
            <th>ID #</th>
            <th>Name</th>
            <th class="autofit" >Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($coordinator as $_this)
        <tr  >
            <td class="autofit">{{$i}}</td>
            <td class="autofit">{{ $_this->EmployeeID }}</td>
            <td>
                <a href="javascript:void(0);" >{{ ucwords(strtolower($_this->CoordinatorName)) }}</a>                
            </td>
            <td width="20%" class="text-center">
                
                <button data-id="{{ $_this->EmployeeID }}" data-name="{{ ucwords(strtolower($_this->CoordinatorName)) }}" class="btn green btn-xs btn-coord-select-btn">
                    <i class="fa fa-check"></i> Select
                </button>
            </td>
        </tr>
        <?php ++$i; ?>
        @endforeach
    </tbody>
</table>
</div>
@endif
