<?php
// error_print($data);
?>
<table class="table table-striped table-bordered table-hover table_scroll" id="records-table">
    <thead>
        <tr>
            <th width="5%">
                <label><input type="checkbox" id="check-parent"></label>
            </th>
            <th width="10%">Code</th>
            <th>Name / Description</th>
            <th>Short Name</th>
            <th width="30%">Coordinator</th>
        </tr>
    </thead>
    <tbody>
        <tr data-id="0">
            <td>
                <label><input type="checkbox" class="check-child" data-id="0"></label>
            </td>
            <td width="5%"><input type="text" class="form-control subj-id"></td>
            <td><input type="text" class="form-control name"></td>
            <td><input type="text" class="form-control short-name"></td>
            <td width="30%">
                <div class="input-group">
                    <input type="text" class="form-control coordinator">
                    <div class="input-group-btn">
                        <button type="button" class="coord-search-btn btn btn-default"><i class="fa fa-search"></i></button>
                        <button type="button" class="coord-search-cancel-btn btn btn-default text-danger"><i class="fa fa-times"></i></button>
                    </div>
                </div>
            </td>
        </tr>
        @if(!empty($data))
            @foreach($data as $_this)
            <tr data-id="{{ encode($_this->SubjectAreaID) }}">
                <td>
                    <label><input type="checkbox" class="check-child"></label>
                </td>
                <td width="10%">
                    <p>{{ $_this->SubjectAreaID }}</p>
                    <input type="text" class="form-control subj-id hide" value="{{ $_this->SubjectAreaID }}">
                </td>
                <td>
                    <p>{{ $_this->SubjectAreaName }}</p>
                    <input type="text" class="form-control name hide" value="{{ $_this->SubjectAreaName }}">
                </td>
                <td>
                    <p>{{ $_this->ShortName }}</p>
                    <input type="text" class="form-control short-name hide" value="{{ $_this->ShortName }}">
                </td>
                <td>
                    <p>{{ $_this->CoordinatorName }}</p>
                    <div class="input-group hide">
                        <input type="text" class="form-control coordinator" value="{{ $_this->CoordinatorName }}" data-id="{{ $_this->CoordinatorID }}" >
                        <span class="input-group-btn">
                            <button type="button" class="coord-search-btn btn btn-default" ><i class="fa fa-search"></i></button>
                            <button type="button" class="coord-search-cancel-btn btn btn-default text-danger"><i class="fa fa-times"></i></button>
                        </span>
                    </div>
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
