<?php //error_print($data)?>
<style media="screen">
    .table .btn{
        margin-right: 0;
    }
    .coord-search-cancel-btn i.fa-times{
        color: #f3565d;
    }
    #records-table_filter{
        text-align: right;
    }
    #records-table{
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light" id="try">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-cubes"></i> List of Subject Area</div>
                <div class="actions">
                    <div class="btn-group">
						<a class="btn btn-sm btn-default" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
    						<i class="fa fa-reorder"></i>
						</a>
						<ul class="dropdown-menu pull-right">
                            <li>
								<a href="javascript:;" id="sub-ar-add-row">
								<i class="fa fa-cube"></i> Add Row </a>
							</li>
							<li>
								<a class="font-blue" href="javascript:;" id="sub-ar-edit">
								<i class="fa fa-pencil font-blue"></i> Edit </a>
							</li>
							<li>
								<a class="font-red" href="javascript:;" id="sub-ar-remove">
								<i class="fa fa-trash-o font-red"></i> Remove </a>
							</li>
                            <li class="divider"></li>
                            <li>
								<a class="font-green" href="javascript:;" id="sub-ar-save">
								<i class="fa fa-check font-green"></i> Save </a>
							</li>
                            <li>
								<a href="javascript:;" id="sub-ar-cancel">
								<i class="fa fa-times"></i> Cancel </a>
							</li>
						</ul>
					</div>
                </div>
            </div>
            <div class="portlet-body table_wrapper" id="sub-ar-result-holder">
                @include($views.'table')
            </div>
        </div>
    </div>
</div>
