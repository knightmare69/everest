<?php
    $progs = getUserProgramAccess();
    
    $model = new \App\Modules\Setup\Models\Programs;
    $data =  $model->whereIn('ProgID',$progs)->orderby('ProgName','ASC')->get();    
?>
<div id="form_modal1" class="modal fade" role="dialog" aria-hidden="true">    
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Create Policy</h4>
			</div>
			<div class="modal-body">
				<form action="#" class="form-horizontal" id="form_createpolicy" name="form_createpolicy">
                    
					<div class="form-group">
						<label class="control-label col-md-4">Policy</label>
						<div class="col-md-8">
							<div class="input-group input-medium">
								<input type="text" id="policy" name="policy" class="form-control">
							</div>							
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4">Description</label>
						<div class="col-md-8">
							<div class="input-group input-medium" >
								<textarea id="desc" name="desc" style="resize: none !important;" class="form-control input-sm"></textarea>
							</div>
							<!-- /input-group -->
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4">Academic Program</label>
						<div class="col-md-8">
							<div class="input-group input-medium" >
								<select id="program" name="program" class="form-control input-sm">
                                    <option value="-1" selected="" disabled="" > - select academic program - </option>
                                    @if(!empty($data))
                                        <option value="0"> - All Available Program/s - </option>
                                        @foreach($data as $r)
                                            <option value="{{ $r->ProgID }}"> {{ $r->ProgName }} </option>                                                                                
                                        @endforeach
                                    @else
                                        <option value="0"> - All Programs - </option>
                                    @endif                                                                        
                                </select>								
							</div>
							<!-- /input-group -->
						</div>
					</div>
                    <div class="form-group">
						<label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                        <div class="input-group">
							<div class="">
                                <label class="checkbox"><input type="checkbox" id="zerobased" name="zerobased" class="zeckbox"> Zero Based / No Transmutation </label>
								<label class="checkbox"><input type="checkbox" id="conduct" name="conduct" class="zeckbox"> Required Conduct </label>    
                                <label class="checkbox"><input type="checkbox" id="club" name="club" class="zeckbox"> Club Subject </label>    
                                <label class="checkbox"><input type="checkbox" id="inactive" name="inactive" class="zeckbox"> Inactive </label>                                			
							</div>
						</div>
                        </div>						
					</div>                                       
                    <input type="hidden" id="idx" name="idx" value="0" />                                 
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
				<button class="btn green btn-primary" data-menu="save-policy" type="button">Save Form</button>
			</div>
		</div>
	</div>
</div>