<div id="components" class="portlet light" style="margin-bottom: 0px; ">
    <div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cubes"></i>
			<span class="caption-subject bold uppercase">Components</span>
			<span class="caption-helper"></span>
		</div>
		<div class="actions">
            <button class="btn btn-default" type="button" data-menu="add-component" > <i class="fa fa-plus"></i> Add </button>			
            <a class="btn btn-default" href="javascript:void(0);" data-menu="delete-component" ><i class="fa fa-trash-o"></i> Delete </a>			            			
		</div>
	</div>
	<div class="portlet-body">
        <h4 class="text-success" > POLICY : <span id="policycomponent" data-id=""> [No Policy] </span> </h4>
        <hr style="margin-bottom: 0px; margin-top: 0px;" />
        <div class="scroller" style="height: 550px;">
            @include($views.'component.td')            
        </div>        	
	</div>
</div>