<?php 
    $components = isset($components) ? $components : array();
    $total = 0; 
?>
<div class="table-responsive">
<table class="table table-striped table-hover table_scroll" id="tblpolicycomponent" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>                    
            <th>Code</th>
            <th>Component</th>
            <th>Percentage</th>
            <th>Parent</th>
            <th class="autofit">Seq</th>
            <th class="autofit">Ref#</th>            
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($components as $row)
        <tr data-id="{{ encode($row->fldPK) }}" 
            data-component="{{ encode($row->ComponentID) }}" 
            data-policy="{{ encode($row->PolicyID) }}"  data-code="{{ $row->CompCode }}" data-name="{{ $row->Caption }}" class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td class="autofit font-xs">{{$i}}.</td>            
            <td><a href="javascript:void(0)" class="a_edit">{{ $row->CompCode }}</a></td>
            <td>{{ $row->Caption }}</td>
            <td class="text-center">{{ $row->Percentage }}%</td>
            <td>{{$row->ParentID}}</td>
            <td>{{ $row->SortOrder }}</td>
            <td>{{ $row->ComponentID }}</td>            
        </tr>
        <?php $total+= $row->Percentage;  ++$i; ?>
    @endforeach
    </tbody>
    @if($total>0)
    <tfoot>
        <tr>
            <td colspan="4"> Total </td>
            <td class="text-center">{{$total}}%</td>
            <td></td>
            <td></td>            
        </tr>
    </tfoot>
    @endif
</table>
</div>