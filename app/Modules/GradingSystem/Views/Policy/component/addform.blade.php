<?php
    $model = new \App\Modules\GradingSystem\Models\Components;
    $data =  $model->get();
    $i=1;
?>
<div id="component_modal" class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Insert Component</h4>
			</div>
			<div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="portlet-input input-inline input-small">
            				<div class="input-icon right"><i class="icon-magnifier"></i>
            					<input type="text" placeholder="search..." class="form-control input-circle filter-table" data-table="tblnewcomponents">
            				</div>
            			</div>            
                    </div>
                </div>
                <hr style="margin-bottom: 3px; margin-top: 3px;" />
                <div class="row">
                <div class="col-sm-12">                                            
                <div class="scroller" style="height: 400px;">
                    <div class="table-responsive">
                    
                    <table class="table table-striped table-condensed table-hover" id="tblnewcomponents">
                        <thead>
                            <tr>
                                <th  class="autofit">
                                    <label><input type="checkbox" class="chk-header"></label>
                                </th>
                                <th>Ref.#</th>
                                <th>Code</th>
                                <th>Component</th>
                                <th>Percentage</th>
                                <th>Parent</th>
                                <th>Cols</th>
                                <th>Seq No</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($data))
                                @foreach($data as $_this)
                                <tr data-id="{{ encode($_this->fldPK) }}" data-code="{{ $_this->CompCode }}" data-name="{{ $_this->CompDescription }}" class="without-bg">
                                    <td><input type="checkbox" class="chk-child"></td>
                                    <td class="autofit font-xs">{{ $_this->fldPK }}.</td>
                                    <td class="code">{{ $_this->CompCode }}</td>
                                    <td class="desc">{{ $_this->CompDescription }}</td>
                                    <td class="editable"><input type="text" class="form-control numberonly input-sm input-xsmall component-percentage" /></td>
                                    <td class="editable"><input type="text" class="form-control numberonly input-sm input-xsmall component-parent" /></td>
                                    <td class="editable"><input type="text" class="form-control numberonly input-sm input-xsmall component-cols" /></td>                                    
                                    <td class="editable"><input type="text" class="form-control numberonly input-sm input-xsmall component-seq" /></td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No record found!</td>
                                </tr>
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right bold">Total</td>
                                <td id="totalpercentage">0</td>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
                </div>
                </div>
			</div>
			<div class="modal-footer">
                <label class="pull-left">
                    <input type="checkbox" class="component-selectonly" /> Selected Only
                </label>
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
				<button class="btn green btn-primary" data-menu="save-component" type="button"> Save List </button>
			</div>
		</div>
	</div>
</div>
