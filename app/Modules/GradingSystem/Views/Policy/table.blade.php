<?php $table = isset($table) ? $table : array(); ?>
<table class="table table-striped table-hover table_scroll" id="tblpolicy" style="cursor: pointer;">
    <thead>
        <tr>
            <th class="autofit"><input type="checkbox" class="chk-header"></th>
            <th class="autofit">#</th>
            <th>Policy</th>
            <th class="autofit">Zero Based</th>
            <th class="autofit">Conduct</th>
            <th class="autofit">Club</th>
            <th class="autofit">Inactive</th>
            <th class="autofit"></th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($table as $row)
        <tr data-id="{{ encode($row->PolicyID) }}"
            data-name="{{ $row->Policy }}"  data-desc="{{ $row->PolicyDescription }}" data-club="{{$row->IsClub}}" data-status="{{ $row->Inactive }}" data-program="{{$row->ProgID}}" data-default="{{$row->IsDefault}}"
            data-conduct="{{$row->IsConduct}}" data-zerobased="{{$row->IsZeroBased}}"        
            class="without-bg">
            <td><input type="checkbox" class="chk-child"></td>
            <td class="autofit font-xs">{{$i}}.</td>
            <td>
                <a href="javascript:void(0)" class="view-component">{{ $row->Policy }}</a> <br />
                <i>{{ $row->PolicyDescription }}</i>
            </td>
            <td class="center"><?= $row->IsZeroBased==1 ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
            <td class="center"><?= $row->IsConduct==1 ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
            <td class="center"><?= $row->IsClub==1 ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
            <td class="center"><?= $row->Inactive==1 ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : "<span class='label label-default label-round'><i class='fa fa-check'></i></span>"; ?></td>
            <td><a href="javascript:void(0)" class="a_select">Edit</a></td>
        </tr>
        <?php ++$i; ?>
    @endforeach
    </tbody>
</table>