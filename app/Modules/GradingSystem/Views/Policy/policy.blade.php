<div id="policy" class="portlet light" style="margin-bottom: 0px;">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-wrench"></i>
			<span class="caption-subject bold uppercase"> Policy Setup</span>
			<span class="caption-helper">grading policy...</span>
		</div>
		<div class="actions">
            <div class="portlet-input input-inline input-small">
				<div class="input-icon right">
					<i class="icon-magnifier"></i>
					<input type="text" placeholder="search..." class="form-control input-circle filter-table" data-table="tblpolicy">
				</div>
			</div>
                                
			<a class="btn btn-circle btn-default" href="javascript:void(0);" data-menu="add-policy"><i class="fa fa-plus"></i> Add </a>
            <button type="button" class="btn btn-circle btn-default" data-menu="delete-policy"> <i class="fa fa-times"></i> Delete </button>            
			<a href="#" class="btn btn-circle btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="scroller" style="height: 550px;">
            @include($views.'table')            
        </div>        	
	</div>
</div>