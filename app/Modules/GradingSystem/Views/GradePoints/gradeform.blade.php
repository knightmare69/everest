<?php
    $model = new \App\Modules\Setup\Models\Programs;
    $data =  $model->orderby('ProgName','ASC')->get();
?>
<div id="grade_modal" class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Grade Info</h4>
			</div>
			<div class="modal-body">
				<form action="#" class="form-horizontal" id="form_grade" name="form_grade">
                    <fieldset>
                    <div class="form-group">                    
                        <div class="col-sm-6">                            
    						<label class="control-label">Min</label>        						
   							<input type="text" id="min" name="min" class="form-control">
      					</div>                        
                        <div class="col-sm-6">                            
    						<label class="control-label">Max</label>        						
   							<input type="text" id="max" name="max" class="form-control">
      					</div>                                                                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Letter</label>        						
				            <input type="text" id="letter" name="letter" class="form-control">
                        </div>
                        
                    </div>
					                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Grade Point</label>        						
				            <input type="text" id="gpoint" name="gpoint" class="form-control">
                        </div>
                        
                    </div>
					<div class="form-group">
                        <div class="col-sm-12">                                                
						  <label class="control-label">Description</label>					
                          <input type="text" id="desc" name="desc" class="form-control">                          													  						
                        </div>                        						
					</div>		
                    <div class="form-group">
                        <div class="col-sm-12">                                                
						  <label class="control-label">Remarks</label>					
                          <input type="text" id="remarks" name="remarks" class="form-control">                          													  						
                        </div>                        						
					</div>	
                    		
                    <div class="form-group">						
                        <div class="col-md-12">
                            <div class="input-group">
    							<div class="icheck-inline">								
                                    <label class="checkbox"><input type="checkbox" id="inactive" name="inactive" class="zeckbox"> Inactive </label>
    							</div>
    						</div>
                        </div>
					</div>
                    <input type="hidden" id="idx" name="idx" value="0" />
                    <input type="hidden" id="template" name="template" value="0" />
                    </fieldset>
                    
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" type="button">Close</button>
				<button class="btn green btn-primary" data-menu="save" data-mode="grades" type="button">Save Form</button>
			</div>
		</div>
	</div>
</div>