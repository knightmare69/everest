<?php       
    $TemplateID = ( isset($TemplateID) ? $TemplateID : 0 );
    $data = App\Modules\GradingSystem\Models\Grading\Points::where('TemplateID',$TemplateID)->get();     
?>
	<table id="tblgrades" class="table table-condensed  table-hover" style="cursor: pointer !important;">
        <thead>
            <th>#</th>
            <th>Min</th>
            <th>Max</th>
            <th>Letter Grade</th>
            <th class="">Description</th>
			<th class="text-center ">Grade Point</th>
            <th class="text-center ">Remarks</th>
            <th class="autofit">Inactive</th>
            <th class="text-center ">Action</th>
        </thead>
        <tbody>
        @for($i=0; $i < count($data) ; $i++ )
            <tr data-template="<?= encode($data[$i]->TemplateID) ?>" data-min="<?= $data[$i]->Min ?>" data-max="<?= $data[$i]->Max ?>"
                data-letter="<?= $data[$i]->LetterGrade ?>" data-desc="<?= $data[$i]->Description ?>" data-point="<?= $data[$i]->GradePoint ?>"
                data-remarks="<?= $data[$i]->Remark ?>" data-id="<?= encode($data[$i]->IndexID) ?>"
            >
                <td class="autofit font-xs">{{$i + 1}}.</td>
                <td class="bold text-primary"><?= $data[$i]->Min ?></td>
			    <td class="bold text-primary"><?= $data[$i]->Max  ?></a></td>
                <td class="bold "><?= $data[$i]->LetterGrade  ?> </a></td>
                <td class="autofit "><?= $data[$i]->Description ?></td>
				<td class="text-center"><?= number_format($data[$i]->GradePoint,2) ?></td>	
                <td class="text-center"><?= $data[$i]->Remark ?></td>	
                <td class="text-center state"><?= $data[$i]->Inactive ==1 ? "<span class='label label-success label-round'><i class='fa fa-check' ></i></span>" : ""; ?></td>
                <td class="autofit"><a class="edit" data-menu="edit" data-mode="grades"  href="javascript:void(0)">Edit</a></td> 
            </tr> 
        @endfor        
        </tbody>
    </table>	                    	
   