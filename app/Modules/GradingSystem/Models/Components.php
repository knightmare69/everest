<?php

namespace App\Modules\GradingSystem\Models;

use illuminate\Database\Eloquent\Model;

class Components extends Model
{
    // protected $table = 'ES_GradingComponents';
    protected $table = 'ES_GS_GradeComponents';
    protected $primaryKey = 'fldPK';

    protected $fillable = ['CompCode', 'CompDescription','Projection' ,'LastModifiedBy', 'LastModifiedDate'];
    public $timestamps = false;
    
    public function scopeProjection($query, $compid)
    {
        return $query->where('fldPK',$compid)->pluck('Projection');
    }
    
    
}
