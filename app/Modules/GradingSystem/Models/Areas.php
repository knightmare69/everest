<?php

namespace App\Modules\GradingSystem\Models;

use illuminate\Database\Eloquent\Model;
use DB;

class Areas extends Model
{
    protected $table = 'ES_SubjectAreas';
    protected $primaryKey = 'SubjectAreaID';

    protected $fillable = ['SubjectAreaName', 'ShortName', 'CoordinatorID'];
    public $timestamps = false;

    public function getAreas()
    {
        $data = DB::table($this->table)
                    ->select('SubjectAreaID', 'SubjectAreaName', 'ShortName', 'CoordinatorID', DB::raw('dbo.fn_EmployeeName3(CoordinatorID) as CoordinatorName'))->get();
        return $data;
    }

    public function getCoordinator($string_search)
    {
        $find = DB::table('HR_Employees')
                ->select('EmployeeID', DB::raw('dbo.fn_EmployeeName3(EmployeeID) as CoordinatorName'))
                ->where('LastName', 'like', '%'.$string_search.'%')
                ->get();
        return $find;
    }

    public function check_duplicate($area_id)
    {
        $find = $this->find($area_id);

        if(!empty($find->exists)){
            return count($find);
        } else {
            return false;
        }
    }
}
