<?php 

namespace App\Modules\GradingSystem\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Policy_model extends Model {

	protected $table='ES_GS_GradingPolicy';
    public $component = 'ES_GS_PolicyComponents';
    
	public $primaryKey ='PolicyID';

	protected $fillable  = array(	    
        'TermID'
        ,'Policy'
        ,'PolicyDescription'
        ,'ProgID'
        ,'IsZeroBased'
        ,'IsConduct'
        ,'IsDefault'
        ,'IsClub'
        ,'IsHomeRoom'
        ,'Inactive'        
	);
    
	public $timestamps = false;
    
   	public static function get_view()
	{
		return
		DB::table('ES_GS_GradingPolicy')
			->select([
				'PolicyID'
                ,'TermID'
                ,'Policy'
                ,'PolicyDescription'
                ,'ProgID'
                ,'IsHomeRoom'
                ,'IsDefault'
                ,'IsClub'
                ,'IsConduct'
                ,'IsZeroBased'
				,DB::raw("dbo.fn_ProgramName(ProgID) AS 'Program'")
                ,'Inactive'
			])
			->orderBy('Policy','asc')->get();
	}
    
    public static function get_components($policy){
        
        $data = array();
        
        try {
            $data = DB::table('ES_GS_PolicyComponents')
                ->select("*")
                ->where('PolicyID',$policy)
                ->orderBy('ParentID','asc')                      
                ->orderBy('SortOrder','asc')
                ->get();    
            
        } catch ( Illuminate\Database\QueryException $e) {
            var_dump($e->errorInfo);
        }

        return $data;
		        
    }
    
    public function erase_policy($policy){
        
        $component = DB::table('ES_GS_GradingPolicy')->where('PolicyID', $policy)->delete();
        $result = $this->destroy($policy);
        return $result;
    }
    
    public function erase_policycomponent($key){
        
        $result = DB::table('ES_GS_PolicyComponents')->where('fldPK', $key)->delete();        
        return $result;
    }
    
    
    public function append_policycomponent($p){        
        $data = array(
            'PolicyID' => decode($p['policy']), 
            'ComponentID' => decode($p['component']),
            'CompCode' => $p['code'],
            'Caption' => $p['name'],
            'Percentage' => $p['percent'],
            'ParentID' => $p['parent'],
            'SortOrder' => $p['seq'],
        );
        return DB::table('ES_GS_PolicyComponents')->insert($data);
    }
    
    public function setAsParent_policycomponent($p){
        $where = array(
            'PolicyID' => decode($p['policy']),      
            'ComponentID' => $p['parent'],   
        );
        
        $data = array(
            'HasChild' => 1,
        );

        return DB::table('ES_GS_PolicyComponents')->where($where)->update($data);
    }
    
}