<?php 
namespace App\Modules\GradingSystem\Models\Transmutation;

use illuminate\Database\Eloquent\Model;

Class Transmutation_model extends Model {

    protected $table='ES_GS_Transmutation';
	protected $primaryKey ='TranstblID';

	protected $fillable  = array(
      'TemplateName'
      ,'Description'
      ,'Inactive'
	);
    
    public $timestamps = false;
    
}