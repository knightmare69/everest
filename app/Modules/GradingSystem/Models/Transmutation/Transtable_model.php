<?php 
namespace App\Modules\GradingSystem\Models\Transmutation;

use illuminate\Database\Eloquent\Model;
use DB;

Class Transtable_model extends Model {

    protected $table='ES_GS_Transmutation_Table';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
      'TemplateID'
      ,'Min'
      ,'Max'
      ,'TransmutedGrade'
	);
    
    public $timestamps = false;
    
    public function scopeTransmuteID($query,$value)
    {   
       return $query->whereRaw("TemplateID='2' AND ? BETWEEN [Min] AND [Max]",array($value))->pluck('TransmutedGrade');
    }
}