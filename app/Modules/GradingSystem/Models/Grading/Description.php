<?php 
namespace App\Modules\GradingSystem\Models\Grading;

use illuminate\Database\Eloquent\Model;

Class Description extends Model {

	public $table='ES_GradeDescription';
	protected $primaryKey ='GradeDescID';

	protected $fillable  = array(
			'GradeDescCode',
			'GradeDescription',
			'LastModifiedBy',
			'LastModifiedDate',
			'SortOrder',
	);

	public $timestamps = false;
}