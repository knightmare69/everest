<?php 
namespace App\Modules\GradingSystem\Models\Grading;

use illuminate\Database\Eloquent\Model;

Class Settings extends Model {

    protected $table='ES_GS_GradingSystem';
	protected $primaryKey ='TemplateID';

	protected $fillable  = array(
      'TemplateName'
      ,'Description'
      ,'Inactive'
	);
    
    public $timestamps = false;
    
}