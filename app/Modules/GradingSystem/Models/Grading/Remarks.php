<?php 
namespace App\Modules\GradingSystem\Models\Grading;

use illuminate\Database\Eloquent\Model;

Class Remarks extends Model {

	public $table='ES_GradeFinalRemark';
	protected $primaryKey ='RemarkID';

	protected $fillable  = array(
			'RemarkCode',
			'Remarks',
			'CreditTheUnit',
			'ComputeGWA',
			'SortOrder',
			'LastModifiedBy',
			'LastModifiedDate',
			'AllowToCredit',
			'FailedGradeUnit',
	);

	public $timestamps = false;
}