<?php 
namespace App\Modules\GradingSystem\Models\Grading;

use illuminate\Database\Eloquent\Model;
use DB;

Class Points extends Model {

	protected $table='ES_GS_GradingSystem_Details';
	protected $primaryKey ='IndexID';

	protected $fillable  = array(
      'TemplateID'
      ,'Min'
      ,'Max'
      ,'LetterGrade'
      ,'Description'
      ,'Remark'
      ,'Inactive'
	  ,'GradePoint'
	);

	public $timestamps = false;
}