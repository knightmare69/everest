<?php

namespace App\Modules\GradingSystem\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\GradingSystem\Services\ComponentsServiceProvider as Services;
use App\Modules\GradingSystem\Models\Components as ComponentsModel;

use Permission;
use Request;
use Response;

class Components extends Controller
{
    protected $ModuleName = 'grading-system-components';

    private $media =
        [
            'Title' => 'Subject Components',
            'Description' => 'Welcome To Subject Component!',
            'js' => ['GradingSystem/components'],
            'init' => [],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
            ],
        ];

    private $url = ['page' => 'grading-system/components/'];

    private $views = 'GradingSystem.Views.Components.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $_data = $this->model->orderBy('CompDescription', 'asc')->get();

            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'data' => $_data]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'save':
                    if ($this->permission->has('edit') && $this->permission->has('add')) {
                        $validation = $this->services->isValid(Request::all(), 'save');
                        if ($validation['error']) {
                            $response = Response::json($validation);

                        } else {
                            $components = Request::get('collect');

                            if(!empty($components)){

                                if(!empty($components['new'])){
                                    $data = $this->services->postComponentNew($components['new']);
                                    $this->model->insert($data);
                                }

                                if(!empty($components['update'])){
                                    foreach ($components['update'] as $update) {
                                        $data = $this->services->postComponentEdit($update);
                                        $this->model->where('fldPK', decode($update['comp']))->update($data);
                                    }
                                }

                                $_data = $this->model->orderBy('CompDescription', 'asc')->get();
                                $table = view($this->views.'table', ['data' => $_data])->render();

                                $response = ['error' => false, 'message' => 'Succesfully saved components.', 'table' => $table];
                            } else {
                                $response = ['error' => true, 'message' => 'Missing components!'];
                            }
                        }
                    }
                    break;

                case 'delete':
                    if ($this->permission->has('delete')){
                        $ids = Request::get('comps');
                        foreach($ids as $key => $_this){ $ids[$key] = decode($_this); }
                        $del = $this->model->whereIn('fldPK', $ids)->delete();

                        if($del){
                            $response = ['error' => false, 'message' => 'Successfully deleted component(s).'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to delete component(s).'];
                        }
                    }
                    break;
                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new ComponentsModel();
        $this->permission = new Permission($this->ModuleName);
    }
}