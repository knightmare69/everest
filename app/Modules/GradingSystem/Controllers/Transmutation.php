<?php

namespace App\Modules\GradingSystem\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\GradingSystem\Services\TransmutationServiceProvider as service;
use App\Modules\GradingSystem\Models\Transmutation\Transmutation_model as Model;
use App\Modules\GradingSystem\Models\Transmutation\Transtable_model as Model_Grades;

use Permission;
use Request;
use Response;

class Transmutation extends Controller
{
    protected $ModuleName = 'transmutation';

    private $media =
        [
            'Title' => 'Transmutation',
            'Description' => 'Welcome To Transmutation Table!',
            'js' => ['GradingSystem/transmutation'],
            'init' => ['ME.init()'],
            'plugin_js' => [
                            'bootbox/bootbox.min',
                            'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                        ],
            'plugin_css' => [
                'datatables/plugins/bootstrap/dataTables.bootstrap',
            ],
        ];

    private $url = ['page' => 'grading-system/transmutation/'];

    private $views = 'GradingSystem.Views.Transmutation.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $_data = $this->model->get();

            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'data' => $_data]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    
    private function gradetable($template){
        $data = array( 'TemplateID' => $template );
        return view($this->views.'table',$data)->render();
    }
    
 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'grades':
					if ($this->permission->has('read')) {
					    $id = decode(Request::get('idx'));
						$response = ['error' => false,'content'=> $this->gradetable($id)];
					}
				break;
			
				case 'delete-grade':
					if ($this->permission->has('delete')) {
						if (!$this->isUsed(decode(Request::get('ids')))) {
						    $template = decode(Request::get('tem'));
                            
						 	$status = $this->model_grade->where('IndexID',decode(Request::get('ids')))->delete();                            
                            if($status){
                                $response = ['error'=>false,'message'=>'Successfully deleted!','table'=>$this->gradetable($template)];    
                                SystemLog('Grading System','','','delete-template', 'Template =' . Request::get('name') ,'record successfully deleted!' );
                            }else{
                                $response = errorDelete();    
                            }                                                        							                                                        
						} else {
							$response = ['error' => true,'message' => 'This grading system template was already used. Deletion Failed!'];
						}
					}
				break;
				case 'save-grade':
					if ($this->permission->has('add')) {
						$key = decode(Request::get('idx'));
                        $rights = ($key == '0' || $key == '' ? 'add' : 'edit' ) ;                    
                        $template = decode(Request::get('template'));
                        
    					if ($this->permission->has($rights)) {
    						$validation = $this->service->isGradeValid(Request::all());
    						if ($validation['error']) {
    							$response = Response::json($validation);
    						} else {
                                $data = $this->service->postGrade(Request::all());
                                $this->model_grade->updateOrCreate(array('IndexID'=> $key ),$data);                            							
    			                $response = ['error'=>false,'message'=>'Successfully Save!','table'=>$this->gradetable($template)];
                                SystemLog('Grading System','','','save-template', json_encode($data) ,'record successfully saved!' );                                
    						}
    					}     
					}
				break;
                case 'save-template': 
                    if ($this->permission->has('add')) {
						
                        $key = Request::get('idx');
                        $rights = ($key == '0' || $key == '' ? 'add' : 'edit' ) ;                    
                                            
    					if ($this->permission->has($rights)) {
    						$validation = $this->service->isValid(Request::all());
    						if ($validation['error']) {
    							$response = Response::json($validation);
    						} else {
                                $data = $this->service->postTemplate(Request::all());
                                $this->model->updateOrCreate(array('TranstblID'=> decode($key) ),$data);                            							
    			                $response = ['error'=>false,'message'=>'Successfully Save!','table'=>$this->templatetable()];
                                SystemLog('Grading System','','','save-template', json_encode($data) ,'record successfully saved!' );
                                
    						}
    					}                    													
					}
                break;
               	case 'delete-template':
					if ($this->permission->has('delete')) {
						if (!$this->isUsed(decode(Request::get('ids')))) {
						 	$status = $this->model->where('TranstblID',decode(Request::get('ids')))->delete();                            
                            if($status){
                                $response = ['error'=>false,'message'=>'Successfully deleted!','table'=>$this->templatetable()];    
                                SystemLog('Grading System','','','delete-template', 'Template =' . Request::get('name') ,'record successfully deleted!' );
                            }else{
                                $response = errorDelete();    
                            }                                                        							                                                        
						} else {
							$response = ['error' => true,'message' => 'This grading system template was already used. Deletion Failed!'];
						}
					}
				break;
                
                
			}
		}
		return $response;
	}
    
    private function templatetable(){
		return view($this->views.'template')->render();
	}
    
   
    
    
	private function isUsed($key)
	{	
		//$count = DB::select("select dbo.fn_IsSubjectLinked('".$key."') as total");
		//$count = isset($count[0]) ? $count[0]->total > 0 ? true : false : false;
        $count = 0;
		if ($count > 0) {
			return true;
		}
		return false;
	}


   
   	
    private function initializer()
 	{
 		$this->permission = new Permission($this->ModuleName);
 		$this->model = new Model;
        $this->model_grade = new Model_Grades;
        $this->service = new service();
 	}
    
}
