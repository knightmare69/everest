<?php

namespace App\Modules\GradingSystem\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\GradingSystem\Services\AreasServiceProvider as Services;
use App\Modules\GradingSystem\Models\Areas as AreasModel;

use Permission;
use Request;
use Response;

class Areas extends Controller
{
    protected $ModuleName = 'grading-system-areas';

    private $media = [ 'Title' => 'Subject Areas', 'Description' => 'Welcome To Subject Areas!',
            'js' => ['GradingSystem/areas'],
            'init' => [],
            'plugin_js' => ['bootbox/bootbox.min', 'datatables/media/js/jquery.dataTables.min',
                            'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                            'datatables/extensions/Scroller/js/dataTables.scroller.min',
                            'datatables/plugins/bootstrap/dataTables.bootstrap',
                            'select2/select2.min',],
            'plugin_css' => [ 'datatables/plugins/bootstrap/dataTables.bootstrap', 'select2/select2',],
        ];

    private $url = ['page' => 'grading-system/areas/'];

    private $views = 'GradingSystem.Views.Areas.';

    public function __construct()
    {
        $this->initializer();
    }

    public function index()
    {
        if (isParent()) {
            return redirect('/security/validation');
        }
        $this->initializer();
        if ($this->permission->has('read')) {
            $_data = $this->model->getAreas();
            return view('layout', array('content' => view($this->views.'index')->with(['views' => $this->views, 'data' => $_data]), 'url' => $this->url, 'media' => $this->media));
        }

        return view(config('app.403'));
    }

    public function event()
    {
        $response = ['error' => true, 'message' => 'No Event Selected'];
        if (Request::ajax()) {
            $this->initializer();
            $response = Response::json(['error' => true, 'message' => 'Permission Denied!']);

            switch (Request::get('event')) {
                case 'save':
                    if ($this->permission->has('edit') && $this->permission->has('add')) {

                        $validation = $this->services->isValid(Request::all(), 'save');
                        if ($validation['error']) {
                            $response = Response::json($validation);
                        } else {
                            $areas = Request::get('collect');

                            if(!empty($areas)){

                                if(!empty($areas['new'])){
                                    foreach ($areas['new'] as $new) {
                                        $has_duplicate = $this->model->check_duplicate($new['subj_id']);

                                        if($has_duplicate == false){
                                            $data = $this->services->postAreas($new);
                                            $this->model->insert($data);

                                        } else {
                                            $response = ['error' => true, 'message' => $new['name'].' had duplicate.',];
                                            return $response;
                                            break;

                                        }
                                    }
                                }

                                if(!empty($areas['update'])){
                                    foreach ($areas['update'] as $update) {
                                        $has_duplicate = $this->model->check_duplicate($update['subj_id']);
                                        if($has_duplicate == false || $has_duplicate == 1){
                                            $data = $this->services->postAreas($update);
                                            $this->model->where('SubjectAreaID', decode($update['area']))->update($data);
                                        } else {
                                            $response = ['error' => true, 'message' => $new['name'].' had duplicate.',];
                                            return $response;
                                            break;
                                        }
                                    }
                                }

                                $_data = $this->model->getAreas();
                                $table = view($this->views.'table', ['data' => $_data])->render();
                                $response = ['error' => false, 'message' => 'Succesfully saved subject areas.', 'table' => $table];
                            } else {
                                $response = ['error' => true, 'message' => 'Missing subject areas!'];
                            }
                        }
                    }
                    break;

                case 'search-coordinator':
                    if ($this->permission->has('search')) {
                         $validation = $this->services->isValid(Request::all(), 'search-coordinator');
                         $search = Request::get('search-coord');
                         $find = $this->model->getCoordinator($search);

                         if(!empty($find)){
                             $vw_res = view($this->views.'coord-results', ['coordinator' => $find])->render();
                             $response = ['error' => false, 'message' => count($find).' result(s) found.', 'html' => $vw_res];
                         } else {
                             $response = ['error' => true, 'message' => 'No coordinator found.'];
                         }
                    }
                    break;

                case 'delete':
                    if ($this->permission->has('delete')){
                        $ids = Request::get('sub-areas');
                        foreach($ids as $key => $_this){ $ids[$key] = decode($_this); }
                        $del = $this->model->whereIn('SubjectAreaID', $ids)->delete();

                        if($del){
                            $response = ['error' => false, 'message' => 'Successfully deleted subject area(s).'];
                        } else {
                            $response = ['error' => true, 'message' => 'Unable to delete subject area(s).'];
                        }
                    }
                    break;

                default:
                    return response('Unauthorized.', 401);
                    break;
            }
        }

        return $response;
    }

    private function initializer()
    {
        $this->services = new Services();
        $this->model = new AreasModel();
        $this->permission = new Permission($this->ModuleName);
    }
}