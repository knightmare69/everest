<?php namespace App\Modules\GradingSystem\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\GradingSystem\Models\Policy_model As dbcon;
use App\Modules\GradingSystem\Services\Policy\serviceprovider as service;

use Permission;
use Request;

class PolicySetup extends Controller{
    
    protected $ModuleName = 'policy-setup';
    
	private $media = [ 'Title'=> 'Policy Setup',
    	               'Description'=> 'Welcome To Policy Setup!',
    	               'js'		=> ['GradingSystem/policy'],
    	               'init'		=> ['ME.init()'], 
    	               'plugin_js'	=> ['bootbox/bootbox.min',
                                        'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
                                        'bootstrap-select/bootstrap-select.min',],
                       'plugin_css' => ['jstree/dist/themes/default/style.min',
                                        'bootstrap-select/bootstrap-select.min',                                        
                                        'select2/select2',
                                        'jquery-multi-select/css/multi-select'],  
                     ];

	private $url = [ 'page' => 'grading-system/policy-setup/' ];

	private $views = 'GradingSystem.Views.Policy.';

	function __construct(){
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {
                                    
            $data = array(
                'table'=> $this->dbcon->get_view(),
            );
            
            return view('layout',array('content'=>view($this->views.'index',$data )->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}
    
    public function request_service(){
        $response = ['error'=>true,'message'=>'No request '];
		if (Request::ajax())
		{
		      
			$this->initializer();
			$response = ['error'=>true,'message'=>'Permission Denied!'];
			switch(Request::get('event'))
			{
			     case 'insert-component': 
                    /*
                    $validation = $this->service->isValid(Request::all());
                	if ($validation['error']) {
						$response = Response::json($validation);
					} else {
    				   		
					}
                    */
                     $ids = Request::get('details');               
                     $parent = [];
                                              
                        foreach($ids as $r){						      
                            $this->dbcon->append_policycomponent($r);
                            if($r['parent'] != "0" || $r['parent'] != '' ){
                                //array_push($parent,$r['parent']);
                                $this->dbcon->setAsParent_policycomponent($r);
                            }
                        }
                        
                        //err_log(json_encode($parent));
                        
    			        $response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog('Policy Setup','PolicySetup','request_service','insert-component','id:'. json_encode($ids) ,'record successfully inserted!' );
                                                                                                           
                 break;
			     case 'delete-component': 
                    
                    if ($this->permission->has('delete')) {
						$ids = Request::get('ids');
						foreach($ids as $r){						      
							 $res = $this->dbcon->erase_policycomponent(decode($r['id']));                             
                             if($res){
                                SystemLog('Policy Setup','PolicySetup','request_service','delete-component','info:'. json_encode($r) ,'record successfully deleted!' );
                             }
						}
						$response = ['error'=> false, 'message' => 'Successfully deleted'];
                                                                                                                        
					}
                 
                 break;
                 
                 case 'component':
                    
                    $policy_id = decode( Request::get('idx') ) ;                    
                    $data = view($this->views.'component.td',['components'=>$this->dbcon->get_components($policy_id)])->render();                     
                    // $data = $this->dbcon->get_students($schedule_id);
                     
                    $response = ['error'=>false,'message'=>'record successfully loaded','content'=> $data ];
                 break;
				case 'save-policy':
                
                    $key = Request::get('idx');
                    $rights = ($key == '0' || $key == '' ? 'add' : 'edit' ) ;                    
                                        
					if ($this->permission->has($rights)) {
						$validation = $this->service->isValid(Request::all());
						if ($validation['error']) {
							$response = Response::json($validation);
						} else {
                            $data = $this->service->postPolicy(Request::all());
                            $this->dbcon->updateOrCreate(array('PolicyID'=> decode($key) ),$data);                            							
			                $response = ['error'=>false,'message'=>'Successfully Save!','table'=> $this->policytable()];
						}
					}
					break;
				
				case 'delete':
					if ($this->permission->has('delete')) {
						$ids = Request::get('ids');
						foreach($ids as $id){						      
							 $this->dbcon->erase_policy(decode($id));
						}
						$response = ['error'=> false,'message'=>'Successfully Deleted'];
                        SystemLog('Policy Setup','PolicySetup','request_service','delete','id:'. json_encode($ids) ,'record successfully deleted!' );                                                                                                
					}
					break;
			
				case 'edit':
						$response = view($this->views.'forms.form',['data'=>$this->model->find(decode(Request::get('id')))]);
					break;
			}
		}
		return $response;
    }
    
    private function policytable(){
		return view($this->views.'table',['table'=>$this->dbcon->get_view()])->render();
	}
    

 	private function initializer()
 	{
 		$this->permission = new Permission($this->ModuleName);
        $this->dbcon = new dbcon;
        $this->service = new service();
        
 	}
}