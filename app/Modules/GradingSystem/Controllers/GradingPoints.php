<?php namespace App\Modules\GradingSystem\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\GradingSystem\Models\Grading\Settings as Model;
use App\Modules\GradingSystem\Models\Grading\Points as Model_Grades;
use App\Modules\GradingSystem\Services\GradePointServiceProvider as service;
use Permission;
use Request;
use Response;
use DB;

class GradingPoints extends Controller{
	private $media = [
			'Title'=> 'Grading Points',
			'Description'=> '',
			'js'		=> ['GradingSystem/gradepoints'],
			'init'		=> ['ME.init()'],
			'plugin_js'	=> ['bootbox/bootbox.min']
		];

	private $url = [ 'page' => 'grading-system/grade-points/' ];

	private $views = 'GradingSystem.Views.GradePoints.';

	public function __construct()
	{
		$this->initializer();
	}

 	public function index()
 	{
 		if (isParent()) {
 			return redirect('/security/validation');
 		}
  		$this->initializer();
        if ($this->permission->has('read')) {
            return view('layout',array('content'=>view($this->views.'index')->with(['views'=>$this->views]),'url'=>$this->url,'media'=>$this->media));
        }
        return view(config('app.403'));
 	}
    
    private function gradetable($template){
        $data = array( 'TemplateID' => $template );
        return view($this->views.'gradepoint',$data)->render();
    }
    
 	public function event()
	{
		$response = noEvent();
		if (Request::ajax())
		{
			$response = permissionDenied();
			switch(Request::get('event'))
			{
				case 'grades':
					if ($this->permission->has('read')) {
					    $id = decode(Request::get('idx'));
						$response = ['error' => false,'content'=> $this->gradetable($id)];
					}
				break;
			
				case 'delete-grade':
					if ($this->permission->has('delete')) {
						if (!$this->isUsed(decode(Request::get('ids')))) {
						    $template = decode(Request::get('tem'));
                            
						 	$status = $this->model_grade->where('IndexID',decode(Request::get('ids')))->delete();                            
                            if($status){
                                $response = ['error'=>false,'message'=>'Successfully deleted!','table'=>$this->gradetable($template)];    
                                SystemLog('Grading System','','','delete-template', 'Template =' . Request::get('name') ,'record successfully deleted!' );
                            }else{
                                $response = errorDelete();    
                            }                                                        							                                                        
						} else {
							$response = ['error' => true,'message' => 'This grading system template was already used. Deletion Failed!'];
						}
					}
				break;
				case 'save-grade':
					if ($this->permission->has('add')) {
						$key = decode(Request::get('idx'));
                        $rights = ($key == '0' || $key == '' ? 'add' : 'edit' ) ;                    
                        $template = decode(Request::get('template'));
                        
    					if ($this->permission->has($rights)) {
    						$validation = $this->service->isGradeValid(Request::all());
    						if ($validation['error']) {
    							$response = Response::json($validation);
    						} else {
                                $data = $this->service->postGrade(Request::all());
                                $this->model_grade->updateOrCreate(array('IndexID'=> $key ),$data);                            							
    			                $response = ['error'=>false,'message'=>'Successfully Save!','table'=>$this->gradetable($template)];
                                SystemLog('Grading System','','','save-template', json_encode($data) ,'record successfully saved!' );                                
    						}
    					}     
					}
				break;
                case 'save-template': 
                    if ($this->permission->has('add')) {
						
                        $key = Request::get('idx');
                        $rights = ($key == '0' || $key == '' ? 'add' : 'edit' ) ;                    
                                            
    					if ($this->permission->has($rights)) {
    						$validation = $this->service->isValid(Request::all());
    						if ($validation['error']) {
    							$response = Response::json($validation);
    						} else {
                                $data = $this->service->postTemplate(Request::all());
                                $this->model->updateOrCreate(array('TemplateID'=> decode($key) ),$data);                            							
    			                $response = ['error'=>false,'message'=>'Successfully Save!','table'=>$this->templatetable()];
                                SystemLog('Grading System','','','save-template', json_encode($data) ,'record successfully saved!' );
                                
    						}
    					}                    													
					}
                break;
               	case 'delete-template':
					if ($this->permission->has('delete')) {
						if (!$this->isUsed(decode(Request::get('ids')))) {
						 	$status = $this->model->where('TemplateID',decode(Request::get('ids')))->delete();                            
                            if($status){
                                $response = ['error'=>false,'message'=>'Successfully deleted!','table'=>$this->templatetable()];    
                                SystemLog('Grading System','','','delete-template', 'Template =' . Request::get('name') ,'record successfully deleted!' );
                            }else{
                                $response = errorDelete();    
                            }                                                        							                                                        
						} else {
							$response = ['error' => true,'message' => 'This grading system template was already used. Deletion Failed!'];
						}
					}
				break;
                
                
			}
		}
		return $response;
	}
    
    private function templatetable(){
		return view($this->views.'template')->render();
	}
    
   
    
    
	private function isUsed($key)
	{	
		//$count = DB::select("select dbo.fn_IsSubjectLinked('".$key."') as total");
		//$count = isset($count[0]) ? $count[0]->total > 0 ? true : false : false;
        $count = 0;
		if ($count > 0) {
			return true;
		}
		return false;
	}



 	private function initializer()
 	{
 		$this->permission = new Permission('grade-points');
 		$this->model = new Model;
        $this->model_grade = new Model_Grades;
        $this->service = new service();
 	}
}
/*

select * from modules

insert into modules(name,url,slug,sort)
values('Registrar','#','registrar','2')

select * from pages

insert into pages(name,slug,url,module_id,sort)
values('Subjects','registrar-subject','/registrar/subject',1004,1)

*/