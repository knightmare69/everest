<?php
	Route::group(['prefix' => 'grading-system','middleware'=>'auth'], function () {		
	    Route::group(['prefix' => 'components','middleware'=>'auth'], function () {
		    Route::get('/','Components@index');
		    Route::post('event','Components@event');
		});  
        Route::group(['prefix' => 'grade-points','middleware'=>'auth'], function () {
		    Route::get('/','GradePoints@index');
		    Route::post('event','GradePoints@event');
		});
        Route::group(['prefix' => 'transmutation','middleware'=>'auth'], function () {
		    Route::get('/','Transmutation@index');
		    Route::post('event','Transmutation@event');
		});
        Route::group(['prefix' => 'policy-setup','middleware'=>'auth'], function () {
		    Route::get('/','PolicySetup@index');
		    Route::post('thisRequest','PolicySetup@request_service');
		});
        
        Route::group(['prefix' => 'areas', 'middleware' => 'auth'], function () {
            Route::get('/', 'Areas@index');
            Route::post('event', 'Areas@event');
        });
        
        Route::group(['prefix' => 'grade-points','middleware' => 'auth'], function () {
		    Route::get('/','GradingPoints@index');
		    Route::post('/event','GradingPoints@event');
		});
                              
	});
?>