<?php

namespace App\Modules\Home\Controllers;

use App\Http\Controllers\Controller;
use Permission;
use Request;
use Response;

use App\Modules\Setup\Models\GradingSystemSetup As setup;
use App\Modules\Security\Models\Auditrails as AuditrailsModel;
use App\Modules\Students\Models\StudentProfile as student_model;
use App\Modules\Security\Models\Users\User as user_model;
use App\Modules\Enrollment\Models\Registration as RegsModel;
use App\Modules\Academics\Models\FacultyAssignments as FA_Model;
use App\Modules\Academics\Services\FacultyAssignServiceProvider as FA_Services;
use App\Modules\Admission\Services\GuardianConnectServices;
use App\Modules\Accounting\Models\Accounting_datatable as acc_table;
use DB;

class Home extends Controller 
{
    private $media =
        [
            'Title' => 'Home',
            'Description' => 'Welcome To Dashboard!',
            'js' => ['Home/index', 'Home/students','Home/accounting'],
            'init' => ['PrintID.init()'],
            'plugin_js' => ['bootbox/bootbox.min', 'jquery.blockui.min', 'bootstrap-datepicker/js/bootstrap-datepicker'],
            'plugin_css' => ['bootstrap-datepicker/css/datepicker'],
        ];

    private $url = ['page' => 'home/'];
    private $views = 'Home.Views.';

    public function __construct(){
        $this->initializer();
    }

    public function index()
    {
        if (isParent() && getSessionData('inquired')!=0){
            return redirect('/security/validation');
        }
        $this->initializer();
         
        $params = [
            'c_term' => $this->setup->CurrentTermID(),
            'at' => $this->accounting_table->AcademicTerm(),
            'discount' => array(),//$this->accounting_table->StudentWithDiscounts($this->setup->CurrentTermID()),
            'wodiscount' => array(),//$this->accounting_table->StudentWithoutDiscounts($this->setup->CurrentTermID()),
            'discounttotal'=> array(),//$this->accounting_table->StudentDiscountsTotal(),
            'phpdiscount' => array(),//$this->accounting_table->DiscountsinPhp(),
            'tuitionfeediscounts' => array(),//$this->accounting_table->TuitionFeeDiscounts(),
            'reservations_payments' => array(),//$this->accounting_table->Reservation_Payments(),
            'term' =>  decode(Request::get('t')),
            'audit' => $this->audit->orderBy('created_date', 'desc')->limit(10)->get(),
            'parent_directory_faculty' => $this->student->getParentDirectoryFaculty(),
            'parent_directory_admin' => $this->student->getParentDirectoryAdmin(),
        ];

        return view('layout', array('content' => view($this->views.'index', $params)->with(['views' => $this->views]), 'url' => $this->url, 'media' => $this->media));
    }
	
	public function widget(){
		$result = ['error' => true, 'message' => 'Ooops, Theres an error occured'];
		if (Request::ajax()){
		    $event = Request::get('event');
			switch($event){
				case 'attrition':
					$sql = "SELECT (SELECT TOP 1 CONCAT(AcademicYear,'',SchoolTerm) FROM ES_AYTerm WHERE TermID='".getActiveTerm()."') as AcademicTerm,
								   y.YearLevelID,y.YearLevelCode,y.YearLevelName,y.ProgID,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND ValidationDate IS NOT NULL AND TermID='".getActiveTerm()."')  as Total,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND ValidationDate IS NOT NULL AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='M'))  as NewBoys,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND ValidationDate IS NOT NULL AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='F'))  as NewGirls,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND ValidationDate IS NOT NULL AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='M'))  as OldBoys,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND ValidationDate IS NOT NULL AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='F'))  as OldGirls,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=1 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE Gender='M'))  as WithBoys,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=1 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE Gender='F'))  as WithGirls
							  FROM ESv2_YearLevel as y";
					$rs  = DB::select($sql);
					if($rs && count($rs)>0){
					  $result['error']  = false;
					  $result['message']= 'Load Widget Successfully';
					  $result['success']= true;
					  $result['content']= (string)view('Home.Views.widgets.Accounting.attritionrates',array('rs_attr'=>$rs));
					}
					break;
				case 'reservations_payment':
					$sql = "SELECT y.YearLevelID,y.YearLevelName,y.YearLevelCode,y.ProgID,
								  (SELECT COUNT(s.StudentNo) 
									FROM ES_Students as s 
							INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo
								WHERE r.TermID='".getActiveTerm()."' AND s.Gender='M'
								 AND  s.StudentNo IN (SELECT IDNo FROM ES_Journals 
															 WHERE TermID=r.TermID AND TransID=20 AND NonLedger=0 
									   AND (Particulars LIKE '%Reserv%' AND AccountID IN (1005,1121)))
								 AND ISNULL(r.YearLevelID,s.YearLevelID)=y.YLID_OldValue AND ISNULL(r.ProgID,s.ProgID)=y.ProgID) as ReservedBoys,
								  (SELECT COUNT(s.StudentNo) 
									FROM ES_Students as s 
							INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo
								WHERE r.TermID='".getActiveTerm()."' AND s.Gender='F'
								 AND  s.StudentNo IN (SELECT IDNo FROM ES_Journals 
															 WHERE TermID=r.TermID AND TransID=20 AND NonLedger=0 
									   AND (Particulars LIKE '%Reserv%' AND AccountID IN (1005,1121)))
								 AND ISNULL(r.YearLevelID,s.YearLevelID)=y.YLID_OldValue AND ISNULL(r.ProgID,s.ProgID)=y.ProgID) as ReservedGirls,				  
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND ValidationDate IS NOT NULL AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='M'))  as NewBoys,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND ValidationDate IS NOT NULL AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='F'))  as NewGirls,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND ValidationDate IS NOT NULL AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='M'))  as OldBoys,
								  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND ValidationDate IS NOT NULL AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='F'))  as OldGirls 
							  FROM ESv2_YearLevel as y";
					$rs  = DB::select($sql);	
					if($rs && count($rs)>0){
					  $result['error']  = false;
					  $result['message']= 'Load Widget Successfully';
					  $result['success']= true;
					  $result['content']= (string)view('Home.Views.widgets.Accounting.reservations_payments',array('r_rpayment'=>$rs));
					}
					break;
				case 'student_discount':
					$rs = $this->accounting_table->StudentDiscountsTotal();
					if($rs && count($rs)>0){
					  $result['error']  = false;
					  $result['message']= 'Load Widget Successfully';
					  $result['success']= true;
					  $result['content']= (string)view('Home.Views.widgets.Accounting.studentswithdiscounts',array('discounttotal'=>$rs));
					}
                    break;		
                case 'phpdiscount':
                    $rs = $this->accounting_table->DiscountsinPhp();
					if($rs && count($rs)>0){
					  $result['error']  = false;
					  $result['message']= 'Load Widget Successfully';
					  $result['success']= true;
					  $result['content']= (string)view('Home.Views.widgets.Accounting.discountsinphp',array('phpdiscount'=>$rs));
					}
					break;
				default:
					$result['message']='Nothing to execute';
					break;
			}
		}
		return $result;
	}

    public function studentactivation()
    {
        $result = ['error' => true, 'message' => 'Ooops, Theres an error occured'];

        if (Request::ajax()) {
            $this->initializer();
            $post = Request::all();
            
            $where = [
                'StudentNo' => getObjectValue($post,'studno'),
                'DateOfBirth' => setDateFormat(getObjectValue($post,'dob'),'mm/dd/yyyy','yyyy-mm-dd'),
            ];
            
            $IsExists = $this->student->where($where)->count();
            
            if($IsExists){
                $result = ['error' =>false, 'message' => 'Student Successfully validated!' ];
                $this->user->where('UserIDX', getUserID() )->update(['UserID'=> getObjectValue($post,'studno')] );
                
            }else{
                $result = ['error' =>true, 'message' => 'Information not exists!' ];
            }                                    
        }
        
        return $result;
    }

 	private function initializer()
 	{
 		$this->permission = new Permission('dashboard');
        $this->audit = new AuditrailsModel;
        $this->student = new student_model();
        $this->user = new user_model();
        $this->setup = new setup();
        $this->accounting_table = new acc_table();
 	}
}