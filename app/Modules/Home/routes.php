<?php
	Route::group(['prefix' => '/','middleware'=>'auth'], function () {
	    Route::get('/','Home@index');
	});
	Route::group(['prefix' => 'home','middleware'=>'auth'], function () {
	    Route::get('/','Home@index');
		Route::post('widget','Home@widget');
	});
    
    Route::group(['prefix' => 'activate','middleware'=>'auth'], function () {
	    Route::post('students','Home@studentactivation');
	});
    
?>