<div class="row">
	<div class="col-sm-8">
	@if(!isParentOrStudents())
		@include('Home.Views.widgets.schedule')
	@else
		@include('Home.Views.widgets.posting')
	@endif    
	</div>    
	<div class="col-sm-4">
		@include('Home.Views.widgets.activities')
	</div>
	{{-- admin dashboard--}}

	@if(isParentOrStudents())
	<div class="col-sm-12">
	   @include('Home.Views.widgets.parent_directory') 
	</div>
	@endif

	{{-- Accounting Dashboard --}}
	@if(isAccounting())
	<div class="col-md-12">

		<ul class="nav nav-pills">
			<li class="active">
				<a href="#tab_2_1" data-toggle="tab">
					Discounts Demographics</a>
			</li>
			<li>
				<a href="#tab_2_2" data-toggle="tab">
				Reservation and Payments </a>
			</li>
			<li>
				<a href="#tab_2_3" data-toggle="tab">
				Attrition Rates </a>
			</li>
			<li>
				<a href="#tab_2_4" data-toggle="tab">
				Year End Enrollment Profile </a>
			</li>
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane fade active in" id="tab_2_1">
			<div class="col-md-6">
				 @include('Home.Views.widgets.Accounting.studentswithdiscounts')
			</div>
			<div class="col-sm-6">
				@include('Home.Views.widgets.Accounting.discountsinphp') 
			</div>
            <div class="col-sm-12">
                @include('Home.Views.widgets.Accounting.tuitionfeediscounts') 
            </div>			
		</div>
		<div class="tab-pane fade" id="tab_2_2">
			<div class="col-md-12">
				 @include('Home.Views.widgets.Accounting.reservations_payments') 
			</div>
		</div>
		<div class="tab-pane fade" id="tab_2_3">
			<div class="col-md-12">
				 @include('Home.Views.widgets.Accounting.attritionrates') 
			</div>
		</div>	
		<div class="tab-pane fade" id="tab_2_4">
			<div class="col-md-12">
				<h1>Year End Enrollment Profile</h1>
			</div>
		</div>							
	</div>
@endif
{{-- Accounting Dashboard --}} 
</div>