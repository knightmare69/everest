<div class="col-sm-12">    
    <div class="portlet box yellow">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-child"></i> Student Account Activation
			</div>
			<div class="tools">
				<a class="reload" href="javascript:;" data-original-title="" title="">
				</a>
			</div>
		</div>
		<div class="portlet-body form">
            
			<!-- BEGIN FORM-->
			<form class="form-horizontal" id="form_activation" action="#" novalidate="novalidate">
				<div class="form-body">
			        <h2>Welcome Guest!</h2>
                    <p>Please fill up the following field to activate your account as a student.</p>
                    <hr />
            		<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
					</div>
					<div class="alert alert-success display-hide">
						<button data-close="alert" class="close"></button>
						Your form validation is successful!
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Student No <span class="required" aria-required="true"> * </span>
						</label>
						<div class="col-md-4">
						  <input type="text" name="studno" id="studno" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Date Of Birth <span class="required" aria-required="true">
						* </span>
						</label>
						<div class="col-md-4">
								<input type="text" name="dob" class="form-control datepicker ">
						
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Verification Code <span class="required" aria-required="true">
						* </span>
						</label>
						<div class="col-md-4">
								<input type="text" name="vcode" class="form-control" placeholder="Found on your email registration">
						
							<span class="help-block">Found on your email registration</span>
						</div>
					</div>				
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn green" id="btnactivate" type="button">Submit</button>
							<button class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>

</div>  