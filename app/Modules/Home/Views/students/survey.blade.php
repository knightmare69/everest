<?php 

$stud = getUserID2();
$academic_year = new App\Modules\Students\Services\StudentProfileServiceProvider();
$academic_year = $academic_year->academic_yearby_student($stud);

?>
<div class="col-sm-12">    
    <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-child"></i> Student Request Form
			</div>
			<div class="tools">
				<a class="reload" href="javascript:;" data-original-title="" title="">
				</a>
			</div>
		</div>
		<div class="portlet-body form">
            
			<!-- BEGIN FORM-->
			<form class="form-horizontal" id="form_survey" action="#" novalidate="novalidate">
				<div class="form-body">			        
                    <p>Kindly tick your desired request for school action</p>
                    <hr />
            		
					<div class="form-group">
						
						  <div class="col-md-offset-3 col-md-7">
							<div class="checkbox">
								
								<label><input type="checkbox" id="gradecopy" name="gradecopy" ></label> Official copy of the report card from the <b>"Registrar's Office"</b> 
							</div>
						            
						</div>
					</div>
					
                    	<div class="form-group">
						
						  <div class="col-md-offset-3 col-md-7">
							<div class="checkbox">
								<label><input type="checkbox" id="sched" name="sched" ></label> Schedule of <b>Parent-Faculty Conference (with Class Adviser &amp; Subject Teachers)</b>
							</div>
						            
						</div>
					</div>
                    
					<div class="form-group">
						<label class="control-label col-md-3">Period <span class="required" aria-required="true">
						* </span>
						</label>
						<div class="col-md-4">
                            <select class="form-control" name="period" id="period">
                                <option value="1">1st Quarter</option>
                                <option value="2">2nd Quarter</option>
                                <option value="3">3rd Quarter</option>
                                <option value="4">4th Quarter</option>
                            </select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Academic Year <span class="required" aria-required="true">
						* </span>
						</label>
						<div class="col-md-4">
                            <select class="form-control" name="term" id="term">
                                @if(!empty($academic_year))
				                    @foreach($academic_year as $ayt)
				                        @if($ayt->SchoolTerm != 'School Year' )                        
				                        <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>
				                        @endif
				                    @endforeach
				                @endif
                            </select>
						</div>
					</div>

				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn green" id="btnrequest" type="button">Submit</button>
							<button class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>

</div>  