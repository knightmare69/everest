	<?php
	/*
	$sql = "SELECT y.YearLevelID,y.YearLevelName,y.YearLevelCode,y.ProgID,
				  (SELECT COUNT(j.IDNo) 
					 FROM ES_Journals as j
			   INNER JOIN ES_Students as s ON j.IDNo=s.StudentNo AND s.Gender='M'
			   INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo AND ISNULL(r.TermID,s.TermID)=j.TermID
											   AND ISNULL(r.YearLevelID,s.YearLevelID)=y.YLID_OldValue AND ISNULL(r.ProgID,s.ProgID)=y.ProgID 
					WHERE j.TermID='".getActiveTerm()."' AND j.TransID=20
					  AND j.Particulars LIKE '%Reserv%' AND AccountID=1005) as ReservedBoys,
				  (SELECT COUNT(j.IDNo) 
					 FROM ES_Journals as j
			   INNER JOIN ES_Students as s ON j.IDNo=s.StudentNo AND s.Gender='F'
			   INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo AND ISNULL(r.TermID,s.TermID)=j.TermID
											   AND ISNULL(r.YearLevelID,s.YearLevelID)=y.YLID_OldValue AND ISNULL(r.ProgID,s.ProgID)=y.ProgID 
					WHERE j.TermID='".getActiveTerm()."' AND j.TransID=20
					  AND j.Particulars LIKE '%Reserv%' AND AccountID=1005) as ReservedGirls,				  
				  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='M'))  as NewBoys,
				  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='F'))  as NewGirls,
				  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='M'))  as OldBoys,
				  (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='F'))  as OldGirls 
			  FROM ESv2_YearLevel as y";
	$rs  = DB::select($sql);	
    */
    $rs = ((isset($r_rpayment))?$r_rpayment:array());	
	?>
	<div class="portlet box green dvrpayment">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Summary of Reserved and Enrolled Students
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table  table-bordered ">
								<thead>
								<tr>
									<th></th>
									<th colspan="3">
										RESERVED
									</th>
									<th colspan="3">
										ENROLLED
									</th>
								</tr>
								<tr>
									<th></th>
									<th>BOYS</th>
									<th>GIRLS</th>
									<th>TOTAL</th>
									<th>BOYS</th>
									<th>GIRLS</th>
									<th>TOTAL</th>
								</tr>
								</thead>
								<tbody>
								<?php 
								   $rboys = 0;
								   $rgirl = 0;
								   $eboys = 0;
								   $egirl = 0;
								?>
								@foreach($rs as $r)
								<tr>
                                <?php 
								   $rboys += $r->ReservedBoys;
								   $rgirl += $r->ReservedGirls;
								   $eboys += ($r->NewBoys + $r->OldBoys);
								   $egirl += ($r->NewGirls + $r->OldGirls);
								?>								
									<td>				
									{{$r->YearLevelName}}	
									</td>
									<td>{{$r->ReservedBoys}}</td>
									<td>{{$r->ReservedGirls}}</td>
									<td>{{$r->ReservedGirls+ $r->ReservedBoys}}</td>
									<td>{{$r->NewBoys + $r->OldBoys}}</td>
									<td>{{$r->NewGirls + $r->OldGirls}}</td>
									<td>{{$r->NewGirls + $r->OldGirls + $r->NewBoys + $r->OldBoys}}</td>
								</tr>
								@endforeach

								</tbody>
								<tfoot>
									<tr>
										<td>TOTAL</td>
										<td>{{$rboys}}</td>
										<td>{{$rgirl}}</td>
										<td>{{$rboys+ $rgirl}}</td>
										<td>{{$eboys}}</td>
										<td>{{$egirl}}</td>
										<td>{{$eboys + $egirl}}</td>
									</tr>
								</tfoot>
								</table>
							</div>
						</div>
					</div>