<?php
/*
$sql = "SELECT (SELECT TOP 1 CONCAT(AcademicYear,'',SchoolTerm) FROM ES_AYTerm WHERE TermID='".getActiveTerm()."') as AcademicTerm,
               y.YearLevelID,y.YearLevelCode,y.YearLevelName,y.ProgID,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."')  as Total,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='M'))  as NewBoys,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE TermID='".getActiveTerm()."' AND Gender='F'))  as NewGirls,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='M'))  as OldBoys,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=0 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE (TermID<>'".getActiveTerm()."' OR TermID IS NULL) AND Gender='F'))  as OldGirls,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=1 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE Gender='M'))  as WithBoys,
              (SELECT COUNT(*) FROM ES_Registrations WHERE YearLevelID=y.YLID_OldValue AND ProgID=y.ProgID AND IsWithdrawal=1 AND TermID='".getActiveTerm()."' AND StudentNo IN (SELECT StudentNo FROM ES_Students WHERE Gender='F'))  as WithGirls
          FROM ESv2_YearLevel as y";
$rs  = DB::select($sql);		  
*/
$rs = ((isset($rs_attr))?$rs_attr:array());
?>
<div class="portlet box blue dvattrition">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i> Attrition Rates <?php echo (($rs && count($rs)>0)?(' of '.$rs[0]->AcademicTerm):'');?>
							</div>
							<div class="tools">
								<i class="fa fa-file-excel-o" onclick="tableToExcel('attrition', 'name', 'export.xls')"></i>
								<a href="javascript:;" class="reload"></a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="attrition">
								<thead>
								<tr>
									<th rowspan="2" class="text-center">
									  GradeLevel	
									</th>
									<th colspan="4" class="text-center">
										 Boys
									</th>
									<th colspan="4" class="text-center">
										 Girls
									</th>
									<th colspan="4" class="text-center">
										 Total
									</th>
									<th colspan="2" rowspan="2" class="text-center">
										 Per Department
									</th>
								</tr>
								<tr>
								  <th class="text-center">New</th>
								  <th class="text-center">Existing</th>
								  <th class="text-center">Withdrawn</th>
								  <th class="text-center">SubTotals</th>
								  
								  <th class="text-center">New</th>
								  <th class="text-center">Existing</th>
								  <th class="text-center">Withdrawn</th>
								  <th class="text-center">SubTotals</th>
								  
								  <th class="text-center">New</th>
								  <th class="text-center">Existing</th>
								  <th class="text-center">Withdrawn</th>
								  <th class="text-center">SubTotals</th>
								</tr>
								</thead>
								<tbody>
								  <?php
								    if($rs && count($rs)>0){
									  $prevdept = 0;
									  $deptlbl  = '';
									  $newboys  = 0;
									  $oldboys  = 0;
									  $newgirls = 0;
									  $oldgirls = 0;
									  $withboys = 0;
									  $withgirls= 0;
									  foreach($rs as $r){
									    $newboys  += $r->NewBoys;
									    $oldboys  += $r->OldBoys;
									    $newgirls += $r->NewGirls;
									    $oldgirls += $r->OldGirls;
									    $withboys += $r->WithBoys;
									    $withgirls+= $r->WithGirls;
										$prevdept += $r->NewBoys+$r->OldBoys+$r->NewGirls+$r->OldGirls;
										if($r->YearLevelCode=='G5'){
										  $deptlbl='Lower School';
										}elseif($r->YearLevelCode=='G8'){
										  $deptlbl='Middle School';
										}elseif($r->YearLevelCode=='G12'){
										  $deptlbl='Upper School';
										}else{
										  $deptlbl='';
										}
									    echo '<tr>
										        <td>'.$r->YearLevelName.'</td>
										        <td>'.$r->NewBoys.'</td>
										        <td>'.$r->OldBoys.'</td>
										        <td>'.$r->WithBoys.'</td>
										        <td>'.($r->NewBoys + $r->OldBoys).'</td>
										        <td>'.$r->NewGirls.'</td>
										        <td>'.$r->OldGirls.'</td>
										        <td>'.$r->WithGirls.'</td>
										        <td>'.($r->NewGirls + $r->OldGirls).'</td>
										        <td>'.($r->NewBoys + $r->NewGirls).'</td>
										        <td>'.($r->OldBoys + $r->OldGirls).'</td>
										        <td>'.($r->WithBoys + $r->WithGirls).'</td>
										        <td>'.(($r->NewGirls + $r->NewBoys) + ($r->OldGirls + $r->OldBoys)).'</td>
										        <td>'.(($deptlbl!='')?$deptlbl:'').'</td>
										        <td>'.(($deptlbl!='')?$prevdept:'').'</td>
											 </tr>';
									    if($deptlbl!=''){
										  $prevdept=0;
										  $deptlbl='';
										}
									  }
									 echo '<tr>
											<td>TOTAL</td>
											<td>'.$newboys.'</td>
											<td>'.$oldboys.'</td>
											<td>'.$withboys.'</td>
											<td>'.($newboys + $oldboys).'</td>
											<td>'.$newgirls.'</td>
											<td>'.$oldgirls.'</td>
											<td>'.$withgirls.'</td>
											<td>'.($newgirls + $oldgirls).'</td>
											<td>'.($newboys + $newgirls).'</td>
											<td>'.($oldboys + $oldgirls).'</td>
											<td>'.($withboys + $withgirls).'</td>
											<td>'.(($newgirls + $newboys) + ($oldgirls + $oldboys)).'</td>
											<td></td>
											<td></td>
										 </tr>';
									  
									}
								  ?>
								</tbody>
								</table>
							</div>
						</div>
					</div>