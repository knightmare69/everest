<div class="portlet box green dvdiscrevenue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-book"></i>% of Discount vs Total Revenue (in Php million) 
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
			</a>
			<a href="javascript:;" class="reload" data-original-title="" title="">
			</a>
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-scrollable">
			<table class="table table-bordered">
			<thead>
			<tr>
				<th colspan="2">
					 
				</th >
				@foreach($phpdiscount as $z)
				<th colspan="2">{{$z->SchoolYear}}</th >
				@endforeach
			</tr>
			</thead>
			<tbody>
			<tr>
			<td  colspan="2" >		 
				</td>
				@foreach($phpdiscount as $z)
				<td >
					 No.
				</td>
				<td>
					%
				</td>
				@endforeach
	
			</tr>
			<tr>
			<td  colspan="2" >
				Discount in Php
				</td>

				@foreach($phpdiscount as $z)
				<td>
					<?php echo number_format($z->TotalDiscount, 2,'.',','); ?>  
				</td>
				<td>
					<?php
					if ($z->TotalRevenue == 0){
						echo $z->TotalRevenue;
					}else{
						echo round((($z->TotalDiscount / $z->TotalRevenue) * 100),2) ; 
					}
					?>										 
				</td>
				@endforeach		
			</tr>
			<tr>
			<td  colspan="2" >
					Total Revenue
				</td>
				@foreach($phpdiscount as $z)
				<td>
					<?php echo number_format($z->TotalRevenue, 2,'.',','); ?>  
				</td>
				<td>
						<?php
					if ($z->TotalRevenue == 0){
						echo $z->TotalRevenue;
					}else{
						echo "100" ; 
					}
					?>
				</td>
				@endforeach			
			</tr>
			<tr>
			<td  colspan="2" >
					Net Revenue 
				</td>
				@foreach($phpdiscount as $z)
				<td>
					<?php echo number_format(($z->TotalRevenue- $z->TotalDiscount), 2,'.',','); ?>  
				</td>
				<td>
					<?php
					if ($z->TotalRevenue == 0){
						echo $z->TotalRevenue;
					}else{
						echo round((($z->TotalRevenue- $z->TotalDiscount)/ $z->TotalRevenue) * 100,2);
					}
					?>
				</td>
				@endforeach	
				
			</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
					