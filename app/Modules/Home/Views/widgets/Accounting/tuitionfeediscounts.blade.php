
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-book"></i>Tuition Fee Discounts
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
								</a>
								<a href="javascript:;" class="reload" data-original-title="" title="">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th colspan="2">
										 
									</th >
									@foreach($tuitionfeediscounts as $z)
									<th colspan="2">
									
								<div class="portlet-input input-inline">									
											<select class="select2 form-control" name="academic-term" id="academic-term1">
												<option value="0" ><p><?php echo  $z->SchoolYear; ?></p></option>
												@if(!empty($at))                        
												@foreach($at as $r)
												<option <?= $term== $r->TermID ? 'selected' : '' ?>  value="{{ encode($r->TermID) }}">{{ $r->AcademicYear.' - '.$r->SchoolTerm }}</option>
												@endforeach
												@endif
											</select>				
										</div> 	
										
									</th >
									@endforeach
								</tr>
								</thead>
								<tbody>
								<tr>
								<td  colspan="2" >		 
									</td>
									@foreach($tuitionfeediscounts as $z)
									<th >
										 No.
									</th>
									<th>
										Percentage(%)
									</th>
									@endforeach
						
								</tr>
								<tr>
								<td  colspan="2" >
									Family Discounts
									</td>

									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->FamilyDiscounts; ?>  
									</td>
									<td>
									<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->FamilyDiscounts / $z->total) * 100),2) ; 
										}
										?>	
																 
									</td>
									@endforeach		
								</tr>
								<tr>
								<td  colspan="2" >
										Sibling Discounts
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->SiblingDiscounts; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->SiblingDiscounts / $z->total) * 100),2) ; 
										}
										?>	
									</td>
									@endforeach			
								</tr>
								<tr>
								<td  colspan="2" >
										Tuition Assistance
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->TuitionAssistances; ?>  
									</td>
									<td>
									<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->TuitionAssistances / $z->total) * 100),2) ; 
										}
										?>	
										
									</td>
									@endforeach	
									
								</tr>
								<tr>
								<td  colspan="2" >
										HS Scholarships
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->Scholarships; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->Scholarships / $z->total) * 100),2) ; 
										}
										?>	
									</td>
									@endforeach	
									
								</tr>
								<tr>
								<td  colspan="2" >
										EB Discounts
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->EBDiscounts; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->EBDiscounts / $z->total) * 100),2) ; 
										}
										?>	
									</td>
									@endforeach	
									
								</tr>
								<tr>
								<td  colspan="2" >
										Early Bird Discounts
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->EarlyBirds; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->EarlyBirds / $z->total) * 100),2) ; 
										}
										?>	
									</td>
									@endforeach	
									
								</tr>
								<tr>
								<td  colspan="2" >
										Integer Awards
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->IntegerAwards; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->IntegerAwards / $z->total) * 100),2) ; 
										}
										?>	
									</td>
									@endforeach	
									
								</tr>
								<tr>
								<td  colspan="2" >
										Special Grants
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->SpecialGrants; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo round((($z->SpecialGrants / $z->total) * 100),2) ; 
										}
										?>	
									</td>
									@endforeach	
									
								</tr>
								<tr>
								<td  colspan="2" >
										Total Discounts
									</td>
									@foreach($tuitionfeediscounts as $z)
									<td>
										<?php echo $z->total; ?>  
									</td>
									<td>
										<?php
										if ($z->total == 0){
											echo $z->total;
										}else{
											echo "100%" ; 
										}
										?>	
									</td>
									@endforeach	
									
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					