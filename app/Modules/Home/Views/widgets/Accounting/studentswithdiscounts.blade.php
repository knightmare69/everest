	<?php
	 $discounttotal = ((isset($discounttotal))?$discounttotal:array());
	?>
	<div class="portlet box red dvstuddiscount">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-book"></i>Number of Students with Discounts 
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse" data-original-title="" title="">
				</a>
				<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
				</a>
				<a href="javascript:;" class="reload" data-original-title="" title="">
				</a>
				
			</div>
		</div>
		<div class="portlet-body">
			<div class="table-scrollable">
				<table class="table table-bordered">
				<thead>
				<tr>
					<th colspan="2">
						 
					</th>
					@foreach($discounttotal as $z)
					<th colspan="2"><?php echo $z->SchoolYear;?></th >
					@endforeach
				</tr>
				</thead>
				<tbody>
				<tr>
				<td  colspan="2" >		 
					</td>
					@foreach($discounttotal as $z)
					<th >
						 No.
					</th>
					<th>
						Percentage(%)
					</th>
					@endforeach
		
				</tr>
				<tr>
				<td  colspan="2" >
						<b>No. of students w/ discounts </b>
					</td>

					@foreach($discounttotal as $z)
					<td>
						<?php echo $z->WithDiscount; ?>  
					</td>
					<td>
						<?php
						if ($z->Total == 0){
							echo $z->Total;
						}else{
							echo round((($z->WithDiscount / $z->Total) * 100),2) . ' %'; 
						}
						?>										 
					</td>
					@endforeach		
				</tr>
				<tr>
				<td  colspan="2" >
						<b>No. of students w/o discounts</b> 
					</td>
					@foreach($discounttotal as $z)
					<td>
						<?php echo $z->WithoutDiscount; ?>  
					</td>
					<td>
							<?php
						if ($z->Total == 0){
							echo $z->Total;
						}else{
							echo round((($z->WithoutDiscount / $z->Total) * 100),2). ' %' ; 
						}
						?>
					</td>
					@endforeach			
				</tr>
				<tr>
				<td  colspan="2" >
						<b>Total Students </b>
					</td>
					@foreach($discounttotal as $z)
					<td>
						<?php echo $z->Total; ?>  
					</td>
					<td>
						<?php
						if ($z->Total == 0){
							echo $z->Total;
						}else{
							echo '100%';
						}
						?>
					</td>
					@endforeach	
					
				</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div>
					