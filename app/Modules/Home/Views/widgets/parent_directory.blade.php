<div class="portlet box green-haze hidden" id="parentdir">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-group"></i>Parent Directory
		</div>
		<!-- <div class="tools">
			<a href="javascript:;" class="collapse" data-original-title="" title="">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
			</a>
			<a href="javascript:;" class="reload" data-original-title="" title="">
			</a>
			<a href="javascript:;" class="remove" data-original-title="" title="">
			</a>
		</div> -->
	</div>
	<div class="portlet-body">
	<div class="caption">
		<h4><b>Faculties Assigned To:</b></h4>
	</div>
	<div class="panel-group accordion scrollable" id="accordion2">
		<?php $prevstudno=''?>
		@forelse($parent_directory_faculty as $p)
			@if($prevstudno!=$p->StudentNo)
				<?php
                   $class     = ''; 				
				   $prevstudno= $p->StudentNo;
				   $reginfo   = DB::select("SELECT TOP 1 r.YearLevelID,r.ProgID,y.YearLevelName,
				   	                                     r.ClassSectionID,sec.SectionName
				   	                                     ,dbo.fn_EmployeeName(sec.AdviserID) AS FacultyName 
				   	                                     ,(SELECT Email FROM dbo.HR_Employees WHERE EmployeeID=sec.AdviserID) AS FacultyEmail 
													FROM ES_Registrations as r
											  INNER JOIN ESv2_YearLevel as y on r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID
											  INNER JOIN ES_ClassSections as sec ON r.ClassSectionID=sec.SectionID
													WHERE r.StudentNo='".$p->StudentNo."' AND r.TermID='".getCurrentTerm()."' ORDER BY RegID DESC");
				    $additional ='';
				    $adviser ='';
                    if($reginfo && count($reginfo)>0){
                      $adviser ="<tr><td>".$reginfo[0]->FacultyName."</td>
                                     <td><a href='mailto:".$reginfo[0]->FacultyEmail."'>".$reginfo[0]->FacultyEmail."</a></td>
                                     <td>Adviser/Homeroom</td></tr>";
					  $additional = "(".$reginfo[0]->YearLevelName." - ".$reginfo[0]->SectionName." - <small>Adviser:".$reginfo[0]->FacultyName."</small>)";
					}else{
					  $additional = '';
					  $class      = 'noadviser';
					}
				?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title accstdno <?php echo $class;?>">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#{{$p->StudentNo}}" aria-expanded="false"><?php echo $p->StudentName.' '.$additional;?></a>
							</h4>
						</div>
						<div id="{{$p->StudentNo}}" class="panel-collapse collapse hidden" aria-expanded="false" style="height: 0px;">
							<div class="panel-body">
								<table class="table table-hover" width="80%">
									<thead>
										<tr>
											<th> Faculty Name </th>
											<th> Email </th>
											<th> Subject </th>
										</tr>
									</thead>
									<tbody>
										<?php echo $adviser;?>
										@foreach($parent_directory_faculty as $p2) 
										@if($prevstudno==$p2->StudentNo)
											<tr>
												@if (strlen($p2->FacultyName)<1)
												<td> N/A </td>
												@else
												<td> {{$p2->FacultyName}} </td>
												@endif
												@if (strlen($p2->FacultyEmail)<1)
												<td> N/A </td>
												@else
												<td> <a href="mailto:{{$p2->FacultyEmail}}"> {{$p2->FacultyEmail}} </a> </td>
												@endif
												<td> {{$p2->Subject}} </td>
											</tr>	
										@endif	
										@endforeach	
									</tbody>
								</table>
							</div>
						</div>
					</div>
				@endif
			@empty 
		</div>
		<p>No Data to Show.</p>
		@endforelse
		<div class="hidden">
			<h4><b>Administrators:</b></h4>
				<div class="panel-body">
					
						<table class="table table-hover" width="80%">
							<thead>
								<tr>
									<th> Administrator Name </th>
									<th> Email </th>
									<th> Position </th>
								</tr>
							</thead>
							
							<tbody>
								@forelse($parent_directory_admin as $a)
								<tr>
									<td> {{$a->AdminName}} </td>
									@if (strlen($a->Email)<1)
									<td> N/A </td>
									@else
									<td> <a href="mailto:{{$a->Email}}"> {{$a->Email}} </a> </td>
									@endif
									<td> {{$a->Position}} </td>
								</tr>
								@empty 
								<p>No Data to Show.</p>
								@endforelse
							</tbody>
						</table>
					
				</div>				
		</div>
	</div>
</div>
