<!-- Begin: life time stats -->
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-thumb-tack"></i>Overview
		</div>
		<div class="tools">
			<a class="collapse" href="javascript:;" data-original-title="" title="">
			</a>
			<a class="config" data-toggle="modal" href="#portlet-config" data-original-title="" title="">
			</a>
			<a class="reload" href="javascript:;" data-original-title="" title="">
			</a>
			<a class="remove" href="javascript:;" data-original-title="" title="">
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="tabbable-line">
			<ul class="nav nav-tabs">
				<li class="active">
					<a data-toggle="tab" href="#overview_1" aria-expanded="true">
					Current School Year </a>
				</li>
				<li class="hide">
					<a data-toggle="tab" href="#overview_2" aria-expanded="false">
					Statistics </a>
				</li>
				<li class="hide">
					<a data-toggle="tab" href="#overview_3" aria-expanded="false">
					New Customers </a>
				</li>
				<li class="dropdown hide">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
					Orders <i class="fa fa-angle-down"></i>
					</a>
					<ul role="menu" class="dropdown-menu">
						<li>
							<a data-toggle="tab" tabindex="-1" href="#overview_4">
							Latest 10 Orders </a>
						</li>
						<li>
							<a data-toggle="tab" tabindex="-1" href="#overview_4">
							Pending Orders </a>
						</li>
						<li>
							<a data-toggle="tab" tabindex="-1" href="#overview_4">
							Completed Orders </a>
						</li>
						<li>
							<a data-toggle="tab" tabindex="-1" href="#overview_4">
							Rejected Orders </a>
						</li>
					</ul>
				</li>
			</ul>
			<div class="tab-content">
				<div id="overview_1" class="tab-pane active">
                    <?php
                        $idno = getUserFacultyID();              
                        $table = App\Modules\ClassRecords\Models\Schedules_model::View($c_term, $idno )->get() ;
                    ?>
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">

					<div class="table-responsive <?php echo $c_term;?>">
						<table class="table table-striped table-hover table-bordered">
						<thead>
						<tr>
                            <th>#</th>
                            <th>Section Name</th>
                            <th>Subject Code</th>
							<th>Subject Name</th>
							<th class="text-center"> Male </th>
							<th class="text-center"> Female </th>
							<th> </th>
						</tr>
						</thead>
						<tbody>
                        @for($i=0; $i < count($table) ; $i++ )
                        <?php
                            $male = 0;
                            $female = 0;

                            if( $table[$i]->FacultybyGender ) {
                                if ( $table[$i]->FacultyID == $idno ) {
                                    $male = $table[$i]->TotalMale;
                                }else if ($table[$i]->FacultyID_2 == $idno){
                                    $female = $table[$i]->TotalFemale;
                                }
                            }else{
                                $male = $table[$i]->TotalMale;
                                $female = $table[$i]->TotalFemale;
                            }

                        ?>
                    	<tr data-id="<?= $table[$i]->ScheduleID ?>" data-section="<?= encode($table[$i]->SectionID) ?>" data-polid="<?= encode($table[$i]->PolicyID) ?>" >
                    		<td class="autofit">{{ $i + 1 }}.</td>
                            <td><?= $table[$i]->SectionName ?></td>
                    		<td><?= $table[$i]->SubjectCode ?></td>
                            <td class="autofit">
                                <?= $table[$i]->SubjectTitle ?>
                            </td>
                            <td class="text-center"> <?= $male ?> </td>
                            <td class="text-center"><?= $female ?> </td>
                    		<td>
                                <a href="{{ url('class-record/eclass-records?t='. encode($table[$i]->TermID) . '&s='.$table[$i]->ScheduleID   ) }}" class="btn btn-default btn-xs "> Grade Sheet  </a>
                            </td>
                    	</tr>
                        @endfor
						</tbody>
						</table>
					</div>
                    </div>
				</div>
				<div id="overview_2" class="tab-pane">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-bordered">
						<thead>
						<tr>
							<th>
								 Product Name
							</th>
							<th>
								 Price
							</th>
							<th>
								 Views
							</th>
							<th>
							</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<a href="#">
								Metronic - Responsive Admin + Frontend Theme </a>
							</td>
							<td>
								 $20.00
							</td>
							<td>
								 11190
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Regatta Luca 3 in 1 Jacket </a>
							</td>
							<td>
								 $25.50
							</td>
							<td>
								 1245
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Apple iPhone 4s - 16GB - Black </a>
							</td>
							<td>
								 $625.50
							</td>
							<td>
								 809
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Samsung Galaxy S III SGH-I747 - 16GB </a>
							</td>
							<td>
								 $915.50
							</td>
							<td>
								 6709
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Motorola Droid 4 XT894 - 16GB - Black </a>
							</td>
							<td>
								 $878.50
							</td>
							<td>
								 784
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Samsung Galaxy Note 3 </a>
							</td>
							<td>
								 $925.50
							</td>
							<td>
								 21245
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Inoval Digital Pen </a>
							</td>
							<td>
								 $125.50
							</td>
							<td>
								 1245
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						</tbody>
						</table>
					</div>
				</div>
				<div id="overview_3" class="tab-pane">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-bordered">
						<thead>
						<tr>
							<th>
								 Customer Name
							</th>
							<th>
								 Total Orders
							</th>
							<th>
								 Total Amount
							</th>
							<th>
							</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<a href="#">
								David Wilson </a>
							</td>
							<td>
								 3
							</td>
							<td>
								 $625.50
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Amanda Nilson </a>
							</td>
							<td>
								 4
							</td>
							<td>
								 $12625.50
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Jhon Doe </a>
							</td>
							<td>
								 2
							</td>
							<td>
								 $125.00
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Bill Chang </a>
							</td>
							<td>
								 45
							</td>
							<td>
								 $12,125.70
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Paul Strong </a>
							</td>
							<td>
								 1
							</td>
							<td>
								 $890.85
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Jane Hilson </a>
							</td>
							<td>
								 5
							</td>
							<td>
								 $239.85
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Patrick Walker </a>
							</td>
							<td>
								 2
							</td>
							<td>
								 $1239.85
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						</tbody>
						</table>
					</div>
				</div>
				<div id="overview_4" class="tab-pane">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-bordered">
						<thead>
						<tr>
							<th>
								 Customer Name
							</th>
							<th>
								 Date
							</th>
							<th>
								 Amount
							</th>
							<th>
								 Status
							</th>
							<th>
							</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<a href="#">
								David Wilson </a>
							</td>
							<td>
								 3 Jan, 2013
							</td>
							<td>
								 $625.50
							</td>
							<td>
								<span class="label label-sm label-warning">
								Pending </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Amanda Nilson </a>
							</td>
							<td>
								 13 Feb, 2013
							</td>
							<td>
								 $12625.50
							</td>
							<td>
								<span class="label label-sm label-warning">
								Pending </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Jhon Doe </a>
							</td>
							<td>
								 20 Mar, 2013
							</td>
							<td>
								 $125.00
							</td>
							<td>
								<span class="label label-sm label-success">
								Success </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Bill Chang </a>
							</td>
							<td>
								 29 May, 2013
							</td>
							<td>
								 $12,125.70
							</td>
							<td>
								<span class="label label-sm label-info">
								In Process </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Paul Strong </a>
							</td>
							<td>
								 1 Jun, 2013
							</td>
							<td>
								 $890.85
							</td>
							<td>
								<span class="label label-sm label-success">
								Success </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Jane Hilson </a>
							</td>
							<td>
								 5 Aug, 2013
							</td>
							<td>
								 $239.85
							</td>
							<td>
								<span class="label label-sm label-danger">
								Canceled </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="#">
								Patrick Walker </a>
							</td>
							<td>
								 6 Aug, 2013
							</td>
							<td>
								 $1239.85
							</td>
							<td>
								<span class="label label-sm label-success">
								Success </span>
							</td>
							<td>
								<a class="btn default btn-xs green-stripe" href="#">
								View </a>
							</td>
						</tr>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
