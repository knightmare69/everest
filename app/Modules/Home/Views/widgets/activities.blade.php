<?php
 $bldg = new \App\Modules\Setup\Models\Buildings;
 $room = new \App\Modules\Setup\Models\Rooms;
 $calendar = "SELECT * FROM ESv2_Events 
               WHERE ((StartDate BETWEEN '".date('Y-m-d',strtotime('-4 days'))." 00:00:00' AND '".date('Y-m-d', strtotime('+5 days'))." 23:59:59') 
			      OR (EndDate BETWEEN '".date('Y-m-d')." 00:00:00' AND '".date('Y-m-d')." 23:59:59'))
			ORDER BY  StartDate DESC";
 $exec     = DB::select($calendar);  
?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-bell-o"></i>Recent Activities
		</div>
		<div class="actions hidden">
			<div class="btn-group">
				<a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#" class="btn btn-sm btn-default dropdown-toggle">
				Filter By <i class="fa fa-angle-down"></i>
				</a>
				<div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
					<label><div class="checker"><span><input type="checkbox"></span></div> Finance</label>
					<label><div class="checker"><span class="checked"><input type="checkbox" checked=""></span></div> Membership</label>
					<label><div class="checker"><span><input type="checkbox"></span></div> Customer Support</label>
					<label><div class="checker"><span class="checked"><input type="checkbox" checked=""></span></div> HR</label>
					<label><div class="checker"><span><input type="checkbox"></span></div> System</label>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		<div class="" style="overflow:hidden; overflow-y:auto; width:auto; max-height:300px;">
            <div>
			<ul class="feeds">
			    <?php
				 foreach($exec as $r){
				   $typeid= $r->TypeID;
                   switch($typeid){
					 case '0':
					 case '3':
					 case '4':
					 case '5':
					    $color = ((date('Y-m-d')==date('Y-m-d',strtotime($r->StartDate)))?'label-warning':'label-info');
					    $icon  = 'fa-bell-o';
						if($typeid=='3'){
						  $icon  = 'fa-bank';	
						}else if($typeid=='4'){
						  $icon  = 'fa-users';	
						}else if($typeid=='5'){
						  $icon  = 'fa-calendar';	
						  $color = 'label-danger';
						}
						
						if(isParentOrStudents() && $r->NotifyParents==0){continue;}
						echo '<li data-eventid="'.$r->EntryID.'">
								<div class="col1">
									<div class="cont">
										<div class="cont-col1">
											<div class="label label-sm '.$color.'">
												<i class="fa '.$icon.'"></i>
											</div>
										</div>
										<div class="cont-col2">
											<div class="desc">
											 <strong>'.$r->Title.'</strong><br/>
											 <i>'.$r->Description.'</i>
											</div>
										</div>
									</div>
								</div>
								<div class="col2">
									<div class="date">'.date('Y-m-d',strtotime($r->StartDate)).'</div>
								</div>
							</li>';
                      break;					 
				   }				   
				 }
				?>
			</ul>
		    </div>
		</div>
		<div class="scroller-footer">
			<div class="btn-arrow-link pull-right">
				<a href="#">See All Records</a>
				<i class="icon-arrow-right"></i>
			</div>
		</div>
	</div>
</div>
<div id="modal_event" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<span class="pull-right">
			<button type="button" class="btn btn-sm btn-default btn-cancel" data-dismiss="modal" aria-hidden="true">Cancel</button>
			</span>
			<h4 class="modal-title"><strong class="action">Create</strong> Events</h4>
			</div>
			<div class="modal-body" style="overflow-y:auto;overflow-x:hidden;max-height:480px;">
				<div class="portlet-body form">
				 <form class="form-horizontal" id="eventform" role="form" onsubmit="return false;"> 
                   <div class="form-body">
				    <div class="form-group hidden">
					 
					</div>
					<input type="hidden" id="xid" name="xid" value="new"/>
				    <div class="form-group hidden">
				        <label class="col-sm-3 col-md-2 control-label">Type:</label>
				        <div class="col-sm-9 col-md-10">
						   <select class="form-control" id="type" name="type">
						     <option value="0" data-period="0" data-room="0" data-reserve="0" data-attach="1">Bulletin</option>
							 <option value="1" data-period="1" data-room="1" data-reserve="0" data-attach="0">Room Reservation</option>
			                 <option value="2" data-period="1" data-room="1" data-reserve="0" data-attach="1">Personal Event</option>
			                 <option value="3" data-period="1" data-room="1" data-reserve="1" data-attach="1">School Event</option>
						     <option value="4" data-period="1" data-room="1" data-reserve="1" data-attach="1">Parent Teacher</option>
						     <option value="5" data-period="0" data-room="0" data-reserve="0" data-attach="0">Holiday</option>';
						   </select>
						</div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Title:</label>
				        <div class="col-sm-9 col-md-10"><input type="text" class="form-control" id="title" name="title"/></div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Description:</label>
				        <div class="col-sm-9 col-md-10"><input type="text" class="form-control" id="desc" name="desc"/></div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Notes:</label>
				        <div class="col-sm-9 col-md-10"><textarea class="form-control" id="note" name="note"></textarea></div>
				    </div>
					<div class="form-group dvroom hidden">
					    <input type="hidden" id="bldg" name="bldg" value="-1"/>
				        <label class="col-sm-3 col-md-2 control-label">Room:</label>
				        <div class="col-sm-9 col-md-10">
						   <select class="form-control" id="room" name="room">
						     <option value="-1" selected disabled>- No Room -</option>
							 <?php 
							   foreach($room->where('IsReservable','1')->get() as $r){
								 echo '<option value="'.$r->RoomID.'" data-bldg="'.$r->BldgID.'">'.$bldg->BuildingName($r->BldgID).' - '.$r->RoomName.'</option>';  
							   }
							 ?>
						   </select>
						</div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Start:</label>
				        <div class="col-sm-5 col-md-6"><input type="text" class="form-control datepicker" id="startd" name="startd"/></div>
				        <div class="col-sm-4 col-md-4 dvperiod"><input type="text" class="form-control timepicker" id="startt" name="startt"/></div>
				    </div>
					<div class="form-group dvperiod ">
				        <label class="col-sm-3 col-md-2 control-label">End:</label>
				        <div class="col-sm-5 col-md-6"><input type="text" class="form-control datepicker" id="endd" name="endd"/></div>
				        <div class="col-sm-4 col-md-4"><input type="text" class="form-control timepicker" id="endt" name="endt"/></div>
					</div>
					<div class="form-group dvattachform hidden">
				        <hr style="margin-bottom:5px;"/>
						   <strong>Attachments:</strong>
						<hr style="margin-top:5px;"/>
				        <label class="col-sm-3 col-md-2 control-label">Attach:</label>
				        <div class="col-sm-9 col-md-10">
						  <div class="table-responsive" style="max-height:200px;overflow-y:auto;overflow-x:;">
						    <table id="tblattachment" class="table table-bordered table-condense">
							  <thead>
							    <th width="20%">Options
								</th>
							    <th>Filename</th>
							  </thead>
							  <tbody></tbody>
							</table>
						  </div>
						</div>
				    </div>
				   </div>	
				 </form>
				</div>
			</div>
        </div>
    </div>
</div>	