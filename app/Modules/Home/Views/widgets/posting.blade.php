<div class="portlet box green-haze" id="recBulletin">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-bookmark-o"></i>Recent Bulletin
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-sm-12"><br/></div>
			<div class="col-sm-12">
				<?php 
				  $indicator = '';
				  $content   = '';
				  $ecount    = 0;
				  $xcount    = 0;
				  $eventi    = 0;
				  $sdate     = date("Y-m-d", strtotime('monday this week'));//
				  $edate     = date('Y-m-d',strtotime('+21 days'));//date("Y-m-d", strtotime('monday this week'));
				  $exec      = DB::SELECT("SELECT * FROM ESv2_Event_Attachment as ea
								       INNER JOIN ESv2_Events as e ON ea.EventID=e.EntryID
								            WHERE TypeID=0 AND StartDate BETWEEN '".$sdate."' AND '".$edate."'
								         ORDER BY StartDate DESC");
				  if($exec && count($exec)>0){
				    foreach($exec as $r){
					  if($ecount>3){continue;}
					  if($eventi!=$r->EventID){
					    $eventi=$r->EventID;
						$ecount++;
					  }else{
					    $eventi=$r->EventID;
					  }
					  $fpath = str_pad($r->EventID, 8, "0", STR_PAD_LEFT); 
					  $dpath = 'assets/upload/';
					  $item  = '<embed src="'.url('assets/dataprivacy.pdf?t='.date('Ymdis').'#toolbar=0&navpanes=0').'" width="98%" height="480px;"></embed>';
					  if(strpos($r->Filename,'.pdf')>0){
					     $item = '<embed class="item-content" src="'.url($dpath.$fpath."/".$r->Filename.'?t='.date('Ymdis').'#toolbar=0&navpanes=0').'" width="98%" height="480px;"></embed>';
					  }else{
					     $item = '<div style="overflow:auto;height:480px!important;text-align:center!important;" class="item-content">
						               <img src="'.url($dpath.$fpath."/".$r->Filename.'?t='.date('Ymdis')).'" class="img-responsive" style="display: inline-block !important;"/>
								  </div>';
					  }
					  
					  $indicator .= '<li data-target="#myCarousel" data-slide-to="'.$xcount.'" class="'.(($xcount==0)?'active':'').'"></li>';
					  $content   .= '<div class="item '.(($xcount==0)?'active':'').'">'.$item.'
										<div class="carousel-caption" data-eventid="'.$r->EventID.'">
											<h4><a href="javascript:void(0);">'.$r->Title.'</a></h4>
											<p>'.$r->Description.'</p>
										</div>
									</div>';
					  $xcount++;
					}
				  }else{
				    $content='<div class="alert alert-warning">
					            <i class=""></i> Nothing to display.
							  </div>';
				  }
				?>
				<div id="myCarousel" class="carousel image-carousel slide">
				    <div class="col-sm-12 <?php echo (($indicator=='')?'hidden':'');?>">
					<!-- Carousel nav -->
					<a class="pull-left" href="#myCarousel" data-slide="prev">
					<i class="fa fa-arrow-left fa-2x"></i>
					</a>
					<a class="pull-right" href="#myCarousel" data-slide="next">
					<i class="fa fa-arrow-right fa-2x"></i>
					</a>
					<ol class="carousel-indicators"><?php echo $indicator;?></ol>
					</div>
					<div class="carousel-inner text-center">
						<?php echo $content;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>