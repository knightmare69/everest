<?php
 $islock = false;
 $onlist = array();
 foreach($trdata as $r){
	   $empname = strtoupper($r->LastName.', '.$r->FirstName);
	   $subjid  = $r->SubjectID;
	   $subject = $r->SubjectTitle;
	   $pdate   = $r->DatePosted;
	   $rdate   = date('Y-m-d',strtotime($r->ReserveDate));
	   $xstart  = (($r->ReserveDate=='')?date('h:i A',strtotime($r->TimeStart)):'');
	   $xend    = (($r->ReserveDate=='')?date('h:i A',strtotime($r->TimeEnd)):'');
	   $rtime   = (($r->ReserveDate=='')? '--:-- -- - --:-- --' : (date('h:i A',strtotime($r->TimeStart)).' - '.date('h:i A',strtotime($r->TimeEnd))));
	   $islock  = (($pdate!='')?true:$islock);
	   $btn     = '<div class="btn-group btn-sm" data-start="'.$xstart.'" data-end="'.$xend.'">
					<button type="button" class="btn btn-info btn-schedule dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
				        <b class="btn-label">'.$rtime.'</b> <i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu scrollable-menu slot-container" role="menu" aria-labelledby="btnGroupVerticalDrop1">
						<li class="slot-loading">
							<a href="javascript.void(0);">
							  <i class="fa fa-refresh fa-spin"></i>Getting List...
							</a>
						</li>
					</ul>
				   </div>';
	
	if($islock){
	   $btn	    = '<label class="text-center">'.$rtime.'</label>';
	}	
	
	if(array_key_exists($r->EmployeeID,$onlist)){
	    $onlist[$r->EmployeeID]['subject'] .= ', '.$r->SubjectTitle;	
	}else{
		$data = array('empname'   => $empname
		             ,'subjid'    => $subjid
					 ,'subject'   => $subject
					 ,'startdate' => $r->StartDate
					 ,'btn'       => $btn);
		$onlist[$r->EmployeeID] = $data;
	}
 }
 
 foreach($onlist as $k=>$d){
    echo '<tr data-id="'.$k.'" data-subject="'.$d['subjid'].'">
	         <td>'.$d['empname'].'</td>
	         <td>'.$d['subject'].'</td>
	         <td class="sched_date">'.date('Y-m-d',strtotime($d['startdate'])).'</td>
	         <td>'.$d['btn'].'</td>
	      <tr>';	
	 
 }
?>