	<div class="col-sm-12">
		<div class="portlet box green" data-condition="" data-notify="">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-calendar"></i> Schedules</div>
				<div class="tools">
				 <button type="button" class="btn btn-primary btn-print"><i class="fa fa-print"></i></button>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
		             <div class="col-sm-12">
					    <div class="table-responsive">
						   <table class="table table-condense table-bordered">
						      <thead class="text-center">
							     <th>Date</th>
							     <th>Time</th>
							     <th>Student Name</th>
							     <th>Grade Level</th>
							  </thead>
							  <tbody>
							    <?php
								  if(isset($table) && count($table)>0){
									 foreach($table as $r){
										 echo '<tr data="'.$r->StudentNo.'">
										         <td width="10%">'.date('d/m/Y',strtotime($r->ReserveDate)).'</td>
										         <td width="20%">'.date('h:i A',strtotime($r->TimeStart)).' - '.date('h:i A',strtotime($r->TimeEnd)).'</td>
										         <td>'.$r->Fullname.'</td>
										         <td>'.$r->YearLevelName.'</td>
										      </tr>';
									 } 
								  }else{
								    echo '<tr><td colspan="4" class="text-center"><i class="fa fa-warning"></i> No data to load yet.</td></tr>';
								  } ?>
							  </tbody>
						   </table>
						</div>
                     </div>					 
				</div>
			</div>
        </div>
    </div>	