<div class="col-sm-12">
  <p>You have successfully booked your schedule. This email will serve as confirmation to your schedule.
</div>
<div class="col-sm-12">
  <div class="table-responsive">
     <table class="table table-bordered table-condense" id="tblschedule">
		  <thead>
			 <th>Faculty</th>
			 <th>Subject</th>
			 <th>Date</th>
			 <th>Time</th>
		  </thead>
		  <tbody>
		    <?php
			 $islock = false;
		     foreach($trdata as $r){
			    $empname = $r->LastName.', '.$r->FirstName;
			    $pdate   = $r->DatePosted;
			    $rdate   = date('Y-m-d',strtotime($r->ReserveDate));
			    $xstart  = (($r->ReserveDate=='')?date('h:i A',strtotime($r->TimeStart)):'');
			    $xend    = (($r->ReserveDate=='')?date('h:i A',strtotime($r->TimeEnd)):'');
			    $rtime   = (($r->ReserveDate=='')? '--:-- -- - --:-- --' : (date('h:i A',strtotime($r->TimeStart)).' - '.date('h:i A',strtotime($r->TimeEnd))));
			    $btn	    = '<label class="text-center">'.$rtime.'</label>';	
				if($r->ReserveDate==''){continue;}
				echo '<tr data-id="'.$r->EmployeeID.'">
						 <td>'.$empname.'</td>
						 <td>'.$r->SubjectTitle.'</td>
						 <td class="sched_date">'.date('Y-m-d',strtotime($r->StartDate)).'</td>
						 <td>'.$btn.'</td>
					  <tr>';	
			 }
			?> 
		  </tbody>
	 </table>
  </div>
</div>
<br/>
<br/>