<div class="row">
  <div class="col-sm-12">
     <div class="portlet box calendar blue">
			<div class="portlet-title">
				<div class="caption"></div>
				<div class="tools">
					<button class="btn btn-xs btn-warning btn-fprint"><i class="fa fa-print"></i> Print</button>
					<button class="btn btn-xs btn-info btn-reload"><i class="fa fa-refresh"></i> Refresh</button>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
					   <div class="form-group">
					      <label class="">Event:</label>
						  <select class="form-control" id="event" name="event">
						     <option value="-1"> - SELECT ONE - </option>
							 <?php
							 if($events){
								 echo $events;
							 }
							 ?>
						  </select>
					   </div>
					</div>
					<div class="col-md-12">
					   <div class="table-responsive">
					      <table id="tbschedule" class="table table-bordered table-condense">
						    <thead>
							   <th>Date</th>
							   <th>Time</th>
							   <th>Students</th>
							   <th>Parent</th>
							</thead>
							<tbody></tbody>
						  </table>
					   </div>
					</div>
				</div>
				<!-- END CALENDAR PORTLET-->
			</div>
		</div>
  </div>
</div>