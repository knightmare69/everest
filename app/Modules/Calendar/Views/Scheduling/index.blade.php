	<div class="col-sm-6 col-md-4">
		<div class="portlet box green" data-condition="" data-notify="">
			<div class="portlet-title">
				<div class="caption"></div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<div class="row">
		             <div class="col-sm-12">
					    <div class="form-group hidden">
						   <label>Schedule</label>
						   <label id="schedule" name="schedule" class="form-control"><?php echo ((isset($eventid))?$eventid:'');?></label>
						</div>
					    <div class="form-group">
						   <label>Student:</label>
						   <select id="student" name="student" class="form-control">
						      <option value="-1" selected disabled>- SELECT ONE STUDENT -</option>
						      <?php
							    foreach($students as $r){
									echo '<option value="'.$r->RegID.'" data-studno="'.$r->StudentNo.'" data-program="'.$r->ProgName.'" data-yrlvl="'.$r->YearLevelName.'">'.$r->LastName.', '.$r->FirstName.'</option>';
								}
							  ?>
						   </select>
						</div>
					    <div class="form-group">
						   <label>Program:</label>
						   <label id="program" name="program" class="form-control"></label>
						</div>
					    <div class="form-group">
						   <label>YearLevel:</label>
						   <label id="yrlvl" name="yrlvl" class="form-control"></label>
						</div>
                     </div>					 
				</div>
			</div>
        </div>
    </div>		
	<div class="col-sm-6 col-md-8">
		<div class="portlet box blue" data-condition="" data-notify="">
			<div class="portlet-title">
				<div class="caption"></div>
				<div class="tools">
				   @if(isParentOrStudents()==false)
				   <button class="btn btn-xs btn-warning btn-unlock hidden pull-right"><i class="fa fa-unlock"></i> Unlock</button>
				   <button class="btn btn-xs btn-info btn-refresh pull-right"><i class="fa fa-refresh"></i> Refresh</button>
			       @endif
				   <button class="btn btn-xs btn-success btn-submit hidden pull-right"><i class="fa fa-lock"></i> Submit</button>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
		             <div class="col-sm-12">
					    <div class="table-responsive" style="height:150% !important;">
						   <table class="table table-bordered table-condense" id="tblschedule">
						      <thead>
							     <th>Faculty</th>
							     <th>Subject</th>
							     <th>Date</th>
							     <th>Time</th>
							  </thead>
							  <tbody>
							  </tbody>
						   </table>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>
                                   <br/>

						</div>
                     </div>					 
				</div>
			</div>
        </div>
    </div>		
    <!-- <style type="text/css">
	   	/* responsive */
	@media only screen and (min-width: 300px) and (max-width: 479px){ 
		.table-responsive {
	   		padding-bottom: 70% !important;
	   	}
	}

	@media only screen and (min-width: 480px) and (max-width: 767px){ 
		.table-responsive {
	   		padding-bottom: 70% !important;
	   	}
	}

	@media only screen and (min-width: 768px) and (max-width: 991px){
		.table-responsive {
	   		padding-bottom: 70% !important;
	   	}
	}

	@media only screen and (min-width: 992px) and (max-width: 1999px){
		.table-responsive {
	   		padding-bottom: 0% !important;
	   	}
	}

	@media only screen and (min-width: 1024px) and (min-height: 768px){
		.table-responsive {
	   		padding-bottom: 0% !important;
	   	}
	}
	</style> -->