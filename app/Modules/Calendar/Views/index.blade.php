<?php
  $bldg = new \App\Modules\Setup\Models\Buildings;
  $room = new \App\Modules\Setup\Models\Rooms;
?>
<style>
 .fc-month-view .fc-time{
	display:none;  
  }
</style>
<div class="row" data-current-date="<?php echo date('m/d/Y');?>" data-current-time="<?php echo date('h:i A');?>">
	<div class="col-md-12">
		<div class="portlet box calendar <?php echo $color;?>" data-condition="<?php echo $show;?>" data-notify="">
			<div class="portlet-title">
				<div class="caption">
					<button class="btn btn-xs btn-info btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
					<button class="btn btn-xs green <?php echo (($permission->has('add')==false)?'hidden':'btn-add');?>"><i class="fa fa-plus"></i> Create  <i class="icon-angle-down"></i></button>
					<div class="<?php echo (($permission->has('read')==false || $color!='red-pink' || isParentOrStudents())?'hidden':'btn-group');?>">
						<button class="btn btn-xs yellow dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
						  Show: <b class="btnselected">All</b>
						  <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li class="schoolview" data-text="All"><a href="javascript:void(0);">All</a></li>
							<li class="schoolview" data-text="School Event"><a href="javascript:void(0);">School Event</a></li>
							<li class="schoolview" data-text="Internal Event"><a href="javascript:void(0);">Internal Calendar</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-3 col-sm-12 hidden">
						<!-- BEGIN DRAGGABLE EVENTS PORTLET-->
						<h3 class="event-form-title">Draggable Events</h3>
						<div id="external-events">
							<form class="inline-form">
								<input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title"/><br/>
								<a href="javascript:;" id="event_add" class="btn default">
								Add Event </a>
							</form>
							<hr/>
							<div id="event_box">
							</div>
							<label for="drop-remove">
							<input type="checkbox" id="drop-remove"/>remove after drop </label>
							<hr class="visible-xs"/>
						</div>
						<!-- END DRAGGABLE EVENTS PORTLET-->
					</div>
					<div class="col-md-12 col-sm-12">
					    <div id="calendar" class="has-toolbar"></div>
					</div>
				</div>
				<!-- END CALENDAR PORTLET-->
			</div>
		</div>
	</div>
</div>
<div id="modal_event" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<span class="pull-right">
			<button type="button" class="btn btn-sm green btn-reserve hidden"><i class="fa fa-list"></i> Schedule</button>
			<button type="button" class="btn btn-sm btn-danger <?php echo (($permission->has('delete')==false)?'hidden':'btn-delete hidden');?>"><i class="fa fa-trash-o"></i> Delete</button>
			<button type="button" class="btn btn-sm btn-warning <?php echo (($permission->has('edit')==false)?'hidden':'btn-save hidden');?>"><i class="fa fa-save"></i> Save</button>
			<button type="button" class="btn btn-sm btn-default btn-cancel" data-dismiss="modal" aria-hidden="true">Cancel</button>
			</span>
			<h4 class="modal-title"><strong class="action">Create</strong> Events&nbsp;&nbsp; <small class="creator"></small></h4>
			</div>
			<div class="modal-body" style="overflow-y:auto;overflow-x:hidden;max-height:480px;">
				<div class="portlet-body form">
				 <form class="form-horizontal" id="eventform" role="form" onsubmit="return false;"> 
                   <div class="form-body">
				    <div class="form-group hidden">
					 
					</div>
					<input type="hidden" id="xid" name="xid" value="new"/>
				    <div class="form-group dvtype">
				        <label class="col-sm-3 col-md-2 control-label">Type:</label>
				        <div class="col-sm-9 col-md-10">
						   <select class="form-control" id="type" name="type"><?php echo $dropdown;?></select>
						</div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Title:</label>
				        <div class="col-sm-9 col-md-10"><input type="text" class="form-control" id="title" name="title"/></div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Description:</label>
				        <div class="col-sm-9 col-md-10"><input type="text" class="form-control" id="desc" name="desc"/></div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Notes:</label>
				        <div class="col-sm-9 col-md-10"><textarea class="form-control" id="note" name="note"></textarea></div>
				    </div>
					<div class="form-group">
				        <label class="col-sm-3 col-md-2 control-label">Start:</label>
				        <div class="col-sm-5 col-md-6"><input type="text" class="form-control datepicker" id="startd" name="startd"/></div>
				        <div class="col-sm-4 col-md-4 dvperiod"><input type="text" class="form-control timepicker" id="startt" name="startt"/></div>
				    </div>
					<div class="form-group dvperiod ">
				        <label class="col-sm-3 col-md-2 control-label">End:</label>
				        <div class="col-sm-5 col-md-6"><input type="text" class="form-control datepicker" id="endd" name="endd"/></div>
				        <div class="col-sm-4 col-md-4"><input type="text" class="form-control timepicker" id="endt" name="endt"/></div>
					</div>
					<div class="form-group dvroom">
					    <input type="hidden" id="bldg" name="bldg" value="-1"/>
				        <label class="col-sm-3 col-md-2 control-label">Room:</label>
				        <div class="col-sm-9 col-md-10">
						   <select class="form-control" id="room" name="room">
						     <option value="-1" selected disabled>- No Room -</option>
							 <?php 
							   foreach($room->where('IsReservable','1')->get() as $r){
								 echo '<option value="'.$r->RoomID.'" data-bldg="'.$r->BldgID.'">'.$bldg->BuildingName($r->BldgID).' - '.$r->RoomName.'</option>';  
							   }
							 ?>
						   </select>
						</div>
				    </div>
				    <?php if(!isParentOrStudents()){ ?>
					<div class="form-group dvnotify hidden">
					 <label class="col-sm-3 col-md-2 control-label">Notify:</label>
					 <div class="checkbox-list col-sm-9 col-md-10">
					  <label class="checkbox-inline"><input type="checkbox" id="notifyparent" name="notifyparent" value="1" checked>Parent</label>
					  <label class="checkbox-inline"><input type="checkbox" id="notifyfaculty" name="notifyfaculty" value="1" checked> Faculty</label>
					  <label class="checkbox-inline"><input type="checkbox" id="notifyemployee" name="notifyemployee" value="1" checked> Staff</label>
					 </div>				
					</div>
					<div class="form-group dvoption hidden" tabindex="-1">
					 <label class="col-sm-3 col-md-2 control-label">Option:</label>
					 <div class="checkbox-list col-sm-9 col-md-9">
					  <label class="hidden"><input type="hidden" id="ispublic" name="ispublic" value="1" checked> Is Public</label>
					  <label class="checkbox"><input type="checkbox" id="enotify" name="enotify" value="1"> Email Notification</label>
					  <label class="checkbox dvreserve"><input type="checkbox" id="withreserve" name="withreserve" value="1"> Scheduling</label>
					  <label class="checkbox dvattach"><input type="checkbox" id="withattach" name="withattach" value="1"> Attachment</label>
					 </div>				
					</div>
				    <?php } ?>
					<div class="dvreservebtn hidden">
					  <?php
					  if(isParentOrStudents()){
						  echo '<a href="javascript:void(0);" class="btn btn-info btn-schedule pull-right">Schedule For The Event</a>'; 
					  }else{
						  echo '<a href="javascript:void(0);" class="btn btn-xs btn-warning btn-rview pull-right"><i class="fa fa-calendar"></i> View Reserv.</a>';
					  }
					  ?>
					</div>
					<div class="dvreserveform hidden">
						<div class="form-group">
							<hr style="margin-bottom:5px;"/>
							   <strong>Reservation:</strong>
							   <span class="pull-right">
							     <div class="btn-group">
									<button id="btnGroupVerticalDrop5" type="button" class="btn btn-xs btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-print"></i> View <i class="fa fa-angle-down"></i></button>
									<ul class="dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop5">
										<li><a href="javascript:;" class="btn-vwbook"> Booking </a></li>
										<li><a href="javascript:;" class="btn-vwsched"> Faculty Sched </a></li>
									</ul>
								 </div>
							     <button class="btn btn-xs btn-danger btn-rexempt"><i class="fa fa-gear"></i> Manage Exempt. & Breaks</button>
							     <button class="btn btn-xs btn-info btn-remail hidden"><i class="fa fa-envelope"></i> Email All</button>
							   </span>
							   <br/>
							<hr style="margin-top:5px;"/>
							<label class="col-sm-3 col-md-2 control-label">Start Date:</label>
							<div class="col-sm-5 col-md-6"><input type="text" class="form-control datepicker" id="rstartd" name="rstartd"/></div>
							<div class="col-sm-4 col-md-4">
							  <select class="form-control numberonly" id="rduration" name="rduration" placeholder="Duration">
								<option value="600">10 Mins<option>
								<option value="900">15 Mins<option>
								<option value="1200">20 Mins<option>
								<option value="1500">25 Mins<option>
								<option value="1800">30 Mins<option>
								<option value="3600">1 Hour<option>
							  </select>
							</div>
						</div>
						<div class="form-group" tabindex="-1">
							<label class="col-sm-3 col-md-2 control-label">Time:</label>
							<div class="col-sm-4 col-md-5"><input type="text" class="form-control timepicker" id="rstartt" name="rstartt" placeholder="Start"/></div>
							<div class="col-sm-5 col-md-5"><input type="text" class="form-control timepicker" id="rendt" name="rendt" placeholder="End"/></div>
						</div>
					</div>
					<div class="form-group dvattachform hidden" tabindex="-1">
				        <hr style="margin-bottom:5px;"/>
						   <strong>Attachments:</strong>
						<hr style="margin-top:5px;"/>
				        <label class="col-sm-3 col-md-2 control-label">Attach:</label>
				        <div class="col-sm-9 col-md-10">
						  <div class="table-responsive" style="max-height:200px;overflow-y:auto;overflow-x:;">
						    <table id="tblattachment" class="table table-bordered table-condense">
							  <thead>
							    <th width="20%">
							      <?php if(!isParentOrStudents()){ ?>	
								  <input type="file" class="form-control hidden" id="attach" name="attach"/>
								  <button type="button" class="btn btn-sm green btn-attach"><i class="fa fa-paperclip"></i> Attach</button>
								  <?php
								  }else{
								  	echo 'Options';
								  }
								  ?>
								</th>
							    <th>Filename</th>
							  </thead>
							  <tbody></tbody>
							</table>
						  </div>
						</div>
				    </div>
					<div class="form-group dvattendee hidden" tabindex="-1">
					   <div class="col-sm-12">  
							<div class="table-responsive">
								<table id="tblattendee" class="table table-bordered table-condense"  style="white-space:nowrap;">
									<thead>
									   <th width="5%"><button class="btn btn-xs btn-success btn-add-attend"><i class="fa fa-plus"></i></button>  
													  <button class="btn btn-xs btn-danger btn-del-attend"><i class="fa fa-times"></i></button></th>
									   <th>FullName</th>
									</thead>
									<tbody>
									   <tr>
										  <td colspan="2" class="text-center">No attendee yet.</td>
									   </tr>
									</tbody>
								</table>
							</div>
					   </div>
					</div>
				   </div>	
				 </form>
				</div>
			</div>
        </div>
    </div>
</div>	
<div id="modal_filter" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
			<span class="pull-right">
			   <button type="button" class="btn btn-sm btn-default btn-rcancel" data-dismiss="modal" aria-="true">Cancel</button>
			</span>
			<h4 class="modal-title"><strong class="">Select Attendee</strong></h4>
			</div>
			<div class="modal-body">
			   <div class="row">
			      <div class="col-sm-12">
					<div class="input-group">
						<input class="form-control" type="text" id="empfilter" name="empfilter" placeholder="Lastname,FirstName">
						<span class="input-group-addon btn-emp-filter"><i class="fa fa-search"></i></span>
					</div>
				  </div>
				  <div class="col-sm-12">
				    <div class="table-responsive" style="overflow-y:auto;overflow-x:hidden;max-height:480px;">
					    <table id="tblempfilter" class="table table-bordered table-condense table-select" style="white-space:nowrap;">
						   <thead>
						       <th>EmployeeID</th>
						       <th>Fullname</th>
						       <th>Position</th>
						   </thead>
						   <tbody>
						   </tbody>
						</table>
					</div>
				  </div>
			   </div>
			</div>
		    <div class="modal-footer">
			  <button class="btn btn-primary btn-filter-select"><i class="fa fa-check"></i> Select</button>
		    </div>
		</div>
	</div>
</div>	

<div id="modal_reexempt" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<span class="pull-right">
			 <button type="button" class="btn btn-sm btn-default btn-rcancel" data-dismiss="modal" aria-="true">Cancel</button>
			</span>
			<h4 class="modal-title"><strong class="">Exemption And Breaks</strong></h4>
			</div>
			<div class="modal-body" style="overflow-y:auto;overflow-x:;max-height:480px;">
				<div class="portlet-body form">
				  <table id="tblrexemption" class="table table-bordered table-condense">
				    <thead>
					   <tr>
						   <th><button class="btn btn-sm btn-primary btn-rrefresh"><i class="fa fa-refresh"></i> Refresh</button></th>
						   <th>Faculty/Activity</th>
						   <th>Date</th>
						   <th>TimeStart</th>
						   <th>TimeEnd</th>
					   </tr>
					   <tr class="exemptform">
						   <th><button class="btn btn-sm btn-warning btn-rsave"><i class="fa fa-save"></i></button>
						       <button class="btn btn-sm btn-default btn-rnegate"><i class="fa fa-times"></i></button></th>
						   <th><select class="form-control" id="entityid">
						          <option value='-1' data-standard="1" selected disabled> - SELECT ATLEAST ONE - </option>
						          <option value='1' data-standard="1">LUNCH BREAK</option>
						          <option value='2' data-standard="1">AM BREAK</option>
						          <option value='3' data-standard="1">PM BREAK</option>
						       </select></th>
						   <th><input type="text" id="xdate" name="xdate" class="form-control datepicker"/></th>
						   <th><input type="text" id="xstart" name="xstart" class="form-control timepicker"/></th>
						   <th><input type="text" id="xend" name="xend" class="form-control timepicker"/></th>
					   </tr>
					</thead>
				    <tbody>
                    </tbody>					
				  </table>
                </div>
            </div>
        </div>
    </div>
</div>	