<?php
Route::group(['prefix' => 'calendar','middleware'=>'auth'], function () {
		    Route::get('/','Calendar@index');
		    Route::get('/school','Calendar@school');
		    Route::get('/mycalendar','Calendar@personal');
		    Route::get('/room','Calendar@room');
			Route::get('/txn','Calendar@txn');
		    Route::get('/booking','Calendar@booking');
		    Route::post('/txn','Calendar@txn');
});

Route::group(['prefix' => 'scheduling','middleware'=>'auth'], function () {
		    Route::get('/','Scheduling@index');
			Route::get('/faculty','Scheduling@faculty');
			Route::get('/print','Scheduling@print_data');
			Route::post('/faculty','Scheduling@faculty');
			Route::get('/txn','Scheduling@txn');
		    Route::post('/txn','Scheduling@txn');
});
?>