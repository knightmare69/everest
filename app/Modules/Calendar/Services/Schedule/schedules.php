<?php
namespace App\Modules\Calendar\Services\Schedule;

use DateTime;
use DB;
Class schedules
{
  public $eventColor = array('0'=>'#ffab00','1'=>'#0000ff','2'=>'#ce2929','3'=>'#008000','4'=>'#671663','5'=>'#ed3b3b');	
  function load_event($start,$end,$filter=''){
	$result = array();
	$exec   = DB::select("SELECT * FROM ESv2_Events WHERE ".(($filter!='')?$filter:'')." ((StartDate BETWEEN '".$start."' AND '".$end."') OR (EndDate BETWEEN '".$start."' AND '".$end."'))");  
	foreach($exec as $r){
		$tmpdata = array();
		$tmpdata['id']       = $r->EntryID;
		$tmpdata['url']      = $r->EntryID;
		$tmpdata['title']    = $r->Title;
		$tmpdata['start']    = $r->StartDate;
		$tmpdata['end']      = $r->EndDate;
		$tmpdata['start']   .= ' '.(($r->StartTime!='')?$r->StartTime:'00:00:00');
		$tmpdata['end']     .= ' '.(($r->EndTime!='')?$r->EndTime:'23:59:59');
		$tmpdata['color']    = (($r->TypeID=='3' && $r->NotifyParents==false)?'#008060':$this->eventColor[$r->TypeID]);
		array_push($result,$tmpdata);
	}
	return $result;
  }
  
  function info_event($xid){
	$exec   = DB::select("SELECT TOP 1 e.*
	                                  ,(CASE WHEN e.UserID='".getUserID()."' THEN 0 ELSE 1 END) as IsLock
	                                  ,u.Fullname 
							FROM ESv2_Events as e
					   LEFT JOIN ESv2_Users as u ON e.UserID=u.UserIDX  	 
						   WHERE EntryID='".$xid."'");  
	if($exec){
	   $r = $exec[0];
       if($r->StartDate!=''){
        $r->StartDate = date('m/d/Y',strtotime($r->StartDate));  
	   }
       if($r->EndDate!=''){
        $r->EndDate = date('m/d/Y',strtotime($r->EndDate));  
	   }
	   
       if($r->StartTime!=''){
        $r->StartTime = date('h:i A',strtotime($r->StartTime));  
	   }
	   
       if($r->EndTime!=''){
        $r->EndTime = date('h:i A',strtotime($r->EndTime));  
	   }
	   return $r;
    }else
	   return array();  
  }
  
  function load_attachment($xid){
	$exec    = DB::select("SELECT * FROM ESv2_Event_Attachment WHERE EventID='".$xid."'");  
	$attach  = '';
	if($exec){
	  foreach($exec as $r){
	   $dirpath  = str_replace('index.php','',$_SERVER['SCRIPT_FILENAME']);
	   $pathname = url(str_replace($dirpath,'',$r->Extname));	  
	   $attach  .= '<tr data-id="'.$r->EntryID.'" data-arrid="-1" data-filename="'.$pathname.'">
		             <td width="20%"><button class="btn btn-xs red btn-aremove"><i class="fa fa-trash-o"></i></button>
					                 <button class="btn btn-xs blue btn-aview" data-view="'.$pathname.'"><i class="fa fa-file-o"></i></button></td>
		             <td data-view="'.$pathname.'">'.$r->Filename.'</td>
		            </tr>';	  
	  }
      return $attach;	  
	}else{
	  return $attach;	  	
	}  
  }
  
  function save_event($xid,$args=false){
	$data = array();
	if($args){
	  $data['UserID']      = getUserID();
	  $data['Title']       = ((array_key_exists('title',$args))?$args['title']:'');	
	  $data['TypeID']      = ((array_key_exists('type',$args))?$args['type']:'');	
	  $data['Description'] = ((array_key_exists('desc',$args))?$args['desc']:'');	
	  $data['Notes']       = ((array_key_exists('note',$args))?$args['note']:'');	
	  $data['StartDate']   = ((array_key_exists('startd',$args))?date('Y-m-d',strtotime($args['startd'])):date('Y-m-d'));	
	  $data['EndDate']     = ((array_key_exists('endd',$args))?date('Y-m-d',strtotime($args['endd'])):$data['StartDate']);	
	  
	  if(array_key_exists('bldg',$args) && $args['bldg']!=''){
		$data['BuildingID'] = $args['bldg'];  
	  }
	  if(array_key_exists('room',$args) && $args['room']!=''){
		$data['RoomID'] = $args['room'];  
	  }
	  
	  if(array_key_exists('startt',$args) && $args['startt']!=''){
		$data['StartTime'] = date('H:i:s',strtotime($args['startt']));  
	  }
	  if(array_key_exists('endt',$args) && $args['endt']!=''){
		$data['EndTime']   = date('H:i:s',strtotime($args['endt']));  
	  }
	  
	  if(array_key_exists('ispublic',$args)){
		$data['IsPublic'] = true;   
	  }
	  if(array_key_exists('withreserve',$args)){
		$data['Reservation'] = true;   
		$data['RStartDate']  = date('Y-m-d',strtotime($args['rstartd']));
		$data['RDuration']   = (($args['rduration']=='')?300:$args['rduration']);
		$data['REndDate']    = date('Y-m-d',strtotime($args['rstartd']));
		$data['RStartTime']  = date('H:i:s',strtotime($args['rstartt']));
		$data['REndTime']    = date('H:i:s',strtotime($args['rendt']));
	  }
	  
	  if(array_key_exists('notifyparent',$args)){
		$data['NotifyParents'] = true;   
	  }else{
		$data['NotifyParents'] = false;     
	  }
	  
	  if(array_key_exists('notifyfaculty',$args)){
		$data['NotifyFaculty'] = true;   
	  }else{
		$data['NotifyFaculty'] = false;     
	  }
	  
	  if(array_key_exists('notifyemployee',$args)){
		$data['NotifyEmployees'] = true;   
	  }else{
		$data['NotifyEmployees'] = false;     
	  }
	  
	  if($data['TypeID']=='0'){
		$data['NotifyParents'] = true;   
	    $data['NotifyFaculty'] = true;   
	    $data['NotifyEmployees'] = true;   
	  }
	  
      if($xid=='new'){
		 $xid  = DB::table('ESv2_Events')->insertGetId($data);
         $exec = true;		 
	  }else{
		 $exec = DB::table('ESv2_Events')->where('EntryID',$xid)->update($data);  
	  }
	}
	return (($exec)?$xid:false);
  }
  
  function drop_event($xid,$args=false){
	return true;  
  }
  
  function get_attendee($xid){
	  $content = '';
	  $qry     = "SELECT e.*
	                    ,u.UserIDX
						,p.PositionDesc
					FROM HR_Employees as e
			  INNER JOIN HR_PositionTitles as p ON e.PosnTitleID = p.PosnTitleID 	  
			   LEFT JOIN ESv2_Users as u ON e.EmployeeID=u.FacultyID
			   LEFT JOIN ESv2_UsersGroup as g ON u.UsersGroupID=g.GroupID and g.GroupCode<>'parent'
				   WHERE CONVERT(VARCHAR(250),e.EmployeeID) IN (SELECT CONVERT(VARCHAR(250),UserID) FROM ESv2_Event_Attendee WHERE EventID='".$xid."')
					OR CONVERT(VARCHAR(250),u.UserIDX) IN (SELECT CONVERT(VARCHAR(250),UserID) FROM ESv2_Event_Attendee WHERE EventID='".$xid."')";
	  $exec   = DB::select($qry);
      if($exec){
		$content   = '';
		foreach($exec as $r){
		  $uid     = (($r->UserIDX=='')?$r->EmployeeID:$r->UserIDX);
		  $tmpname = $r->LastName.', '.$r->FirstName.' '.substr($r->MiddleName,0,1).'.';
		  $content.= '<tr data-id="'.$uid.'"><td><button class="btn btn-xs btn-danger btn-rem-attendee"><i class="fa fa-times"></i> Remove</button></td><td>'.$tmpname.'</td></tr>';
		}
	  }
      return $content;	  
  }
  
  public function get_filter($type='employees',$args=''){
	  $qry = '';
	  switch($type){
		case 'employees':
		  $qry  = "	SELECT e.*
		                  ,u.UserIDX
						  ,p.PositionDesc
                      FROM HR_Employees as e
				INNER JOIN HR_PositionTitles as p ON e.PosnTitleID = p.PosnTitleID 	  
                 LEFT JOIN ESv2_Users as u ON e.EmployeeID=u.FacultyID
                 LEFT JOIN ESv2_UsersGroup as g ON u.UsersGroupID=g.GroupID and g.GroupCode<>'parent' ".
		         (($args!='')?"WHERE e.EmployeeID LIKE '%".$args."%' OR e.LastName LIKE'%".$args."%' OR e.FirstName LIKE'%".$args."%'":"");
		break;
	  }
	  
	  if($qry!=''){
	     $exec = DB::select($qry);
	     return $exec;
	  }else{
		 return false; 
	  }
   }
  
  public function get_rooms($opt='1',$startdate="",$starttime='',$enddate="",$endtime=""){
	  $bldg       = new \App\Modules\Setup\Models\Buildings;
	  $startdate  = ((strtotime($startdate)==false || $startdate=="")?date('Y-m-d'):date('Y-m-d',strtotime($startdate)));
	  $starttime  = ((strtotime($starttime)==false || $starttime=="")?date('H:i:s'):date('H:i:s',strtotime($starttime)));
	  $enddate    = ((strtotime($enddate)==false || $enddate=="")?date('Y-m-d'):date('Y-m-d',strtotime($enddate)));
	  $endtime    = ((strtotime($endtime)==false || $endtime=="")?date('H:i:s'):date('H:i:s',strtotime($endtime)));
	  $qry = "SELECT * 
	            FROM ES_Rooms
			   WHERE RoomID NOT IN (
				         SELECT RoomID
				           FROM ESv2_Events 
				          WHERE RoomID IS NOT NULL 
				            AND (CAST(StartDate as DATETIME) + CAST(StartTime AS DATETIME) BETWEEN '".$startdate." ".$starttime."' AND '".$enddate." ".$endtime."')
				            AND (CAST(EndDate as DATETIME) + CAST(EndTime AS DATETIME) BETWEEN '".$startdate." ".$starttime."' AND '".$enddate." ".$endtime."')
				        ) AND IsReservable=1";
      $exec = DB::select($qry);
	  $rs   = ''; 
      switch($opt){
		  case '1':
		     foreach($exec as $r){
				 $rs .= '<option value="'.$r->RoomID.'" data-bldg="'.$r->BldgID.'">'.$bldg->BuildingName($r->BldgID).' - '.$r->RoomName.'</option>';
			 }
		  break;
	  }
	  
      return $rs;	  
  }
  
  public function get_students(){
	$qry = "SELECT s.StudentNo,s.LastName,s.FirstName,s.Middlename,s.MiddleInitial,y.YearLevelID,y.YearLevelName,p.ProgID,p.ProgName,r.RegID
		      FROM ES_Students as s
		INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo AND r.TermID=(SELECT TOP 1 TermID FROM ES_AYTerm WHERE Active_OnlineEnrolment=1 ORDER BY AcademicYear DESC) 
		INNER JOIN ESv2_YearLevel as y ON r.YearLevelID=y.YLID_OldValue AND r.ProgID=y.ProgID
		INNER JOIN ES_Programs as p ON r.ProgID = p.ProgID
			 WHERE FamilyID='".getFamilyID()."'";
    $exec = DB::SELECT($qry);			 
    return $exec;
  }
  
  public function get_studreserve(){
	  
  }
  
  public function get_faculty($schedule,$regid){
	 $qry = "SELECT rd.RegID
				   ,rd.ReferenceID
				   ,e.EmployeeID 
				   ,e.LastName
				   ,e.FirstName
				   ,e.MiddleName
				   ,s.SubjectID
				   ,s.SubjectCode
				   ,s.SubjectTitle
				   ,n.StartDate
				   ,a.ReserveDate
				   ,a.TimeStart
				   ,a.TimeEnd
				   ,a.DatePosted
			   FROM ES_RegistrationDetails as rd
		 INNER JOIN ES_ClassSchedules as cs ON rd.ScheduleID=cs.ScheduleID
		 INNER JOIN ES_Subjects as s ON cs.SubjectID=s.SubjectID
		 INNER JOIN HR_Employees as e ON cs.FacultyID=e.EmployeeID
		 INNER JOIN ESv2_Events as n ON n.EntryID='".$schedule."'
		  LEFT JOIN ESv2_Event_Reservation as a ON cs.FacultyID=a.FacultyID AND rd.RegID=a.RegID AND a.EventID=n.EntryID AND a.SubjectID=cs.SubjectID AND a.UserID='".getUserID()."'
			  WHERE rd.RegID='".$regid."'
			  UNION ALL
             SELECT rd.RegID
				   ,'1' as ReferenceID
				   ,e.EmployeeID 
				   ,e.LastName
				   ,e.FirstName
				   ,e.MiddleName
				   ,'1' as SubjectID
				   ,'Homeroom' as SubjectCode
				   ,'Homeroom' as SubjectTitle
				   ,n.StartDate
				   ,a.ReserveDate
				   ,a.TimeStart
				   ,a.TimeEnd
				   ,a.DatePosted
			   FROM ES_Registrations as rd
         INNER JOIN ES_ClassSections as sec ON rd.ClassSectionID=sec.SectionID
		 INNER JOIN HR_Employees as e ON sec.AdviserID=e.EmployeeID
		 INNER JOIN ESv2_Events as n ON n.EntryID='".$schedule."'
		  LEFT JOIN ESv2_Event_Reservation as a ON sec.AdviserID=a.FacultyID AND a.SubjectID=1 AND rd.RegID=a.RegID AND a.EventID=n.EntryID AND a.UserID='".getUserID()."'
			  WHERE rd.RegID='".$regid."'
              UNION ALL
             SELECT r.RegID
				   ,'2' as ReferenceID
				   ,e.EmployeeID 
				   ,e.LastName
				   ,e.FirstName
				   ,e.MiddleName
				   ,'2' as SubjectID
				   ,'Mentoring' as SubjectCode
				   ,'Mentoring' as SubjectTitle
				   ,n.StartDate
				   ,a.ReserveDate
				   ,a.TimeStart
				   ,a.TimeEnd
				   ,a.DatePosted
			   FROM ES_Registrations as r
         INNER JOIN ESv2_Mentoring_Students as m ON r.StudentNo=m.StudentNo AND r.TermID=m.TermID
		 INNER JOIN HR_Employees as e ON m.FacultyID=e.EmployeeID
		 INNER JOIN ESv2_Events as n ON n.EntryID='".$schedule."'
		  LEFT JOIN ESv2_Event_Reservation as a ON m.FacultyID=a.FacultyID AND a.SubjectID=2 AND r.RegID=a.RegID AND a.EventID=n.EntryID AND a.UserID='".getUserID()."'
			  WHERE r.RegID='".$regid."'"; 
	$exec = DB::SELECT($qry);			 
     return $exec;		  
  }
  
  public function get_available($schedule,$facid){
	  $res      = '<li class="slot-data" data-start="" data-end=""><a href="javascript:void(0);">--:-- -- - --:-- --</a></li>';
	  $qry      = "SELECT RegID,TimeStart,TimeEnd FROM ESv2_Event_Reservation WHERE EventID='".$schedule."' AND FacultyID='".$facid."'
		           UNION ALL
			       SELECT RegID,TimeStart,TimeEnd FROM ESv2_Event_Reservation WHERE EventID='".$schedule."' AND UserID='".getUserID()."'
				   UNION ALL
				   SELECT RegID,TimeStart,TimeEnd FROM ESv2_Event_Reservation WHERE EventID='".$schedule."' AND FacultyID IN ('1','2','3')";
	  $rs       = DB::SELECT($qry);
	  $event    = DB::SELECT("SELECT TOP 1 StartDate,RStartTime,REndTime,RDuration FROM ESv2_Events WHERE EntryID='".$schedule."'")[0];
	  $startt   = strtotime($event->RStartTime);
	  $endt     = strtotime($event->REndTime);
	  $interval = $event->RDuration;
	  for($i=$startt;$i<$endt;$i+=$interval){
		  $taken   = false;
		  $tmplast = $i+$interval;
		  foreach($rs as $r){
			  $xstart = strtotime($r->TimeStart);
			  $xend   = strtotime($r->TimeEnd);
			  
			  if($i>=$xstart && $i<$xend){
				  $taken = true;
				  break;
			  }
			  
			  if($tmplast>$xstart && $tmplast<=$xend){
				  $taken = true;
				  break;
			  }
		  }
		  if($taken){
			 continue; 
		  }else{
			 $res .= '<li class="slot-data" data-start="'.date('h:i A',$i).'" data-end="'.date('h:i A',$i+$interval).'"><a href="javascript:void(0);">'.date('h:i A',$i).' - '.date('h:i A',$i+$interval).'</a></li>'; 
		  }
	  }
	  return $res;
  }
  
  public function save_schedule($eventid,$studno,$faculty,$subject,$rdate,$tstart,$tend){
	  if($studno!='0'){
	    $checking =  DB::select("SELECT COUNT(*) as IsExist
								  FROM (
								    SELECT EntryID FROM ESv2_Event_Reservation WHERE EventID='".$eventid."' AND FacultyID='".$faculty."' AND ('".$tstart."' BETWEEN TimeStart AND TimeEnd) AND ('".$tend."' BETWEEN TimeStart AND TimeEnd)
								    UNION ALL
								    SELECT EntryID FROM ESv2_Event_Reservation WHERE EventID='".$eventid."' AND UserID='".getUserID()."' AND ('".$tstart."' BETWEEN TimeStart AND TimeEnd) AND ('".$tend."' BETWEEN TimeStart AND TimeEnd)
								  ) as a");
      }else{
	    $checking = false;	  
	  }
	  if($checking && $checking[0]->IsExist>0){
		  return false;
	  }else{
		$exec  = DB::statement("DELETE FROM ESv2_Event_Reservation WHERE EventID='".$eventid."' AND FacultyID='".$faculty."' AND RegID='".$studno."' AND SubjectID='".$subject."' AND UserID='".getUserID()."' AND RegID<>'0'
		                        INSERT INTO ESv2_Event_Reservation(EventID,UserID,RegID,FacultyID,SubjectID,ReserveDate,TimeStart,TimeEnd) 
								VALUES ('".$eventid."','".getUserID()."','".$studno."','".$faculty."','".$subject."','".$rdate."','".$tstart."','".$tend."')");  
		return $exec;						
	  }								
  }
  
  public function get_employees(){
	  $content = '';
	  $query   = DB::SELECT("SELECT e.EmployeeID
							       ,e.LastName
							       ,e.FirstName 
						       FROM HR_Employees e
					      LEFT JOIN ES_Faculty as f ON CONVERT(VARCHAR(250),e.EmployeeID)=CONVERT(VARCHAR(250),f.EmployeeID)
						      WHERE e.Inactive=0");
	  foreach($query as $r){
		 $content .= '<option value="'.$r->EmployeeID.'" data-standard="0">'.$r->LastName.', '.$r->FirstName.'</option>';
	  }					
      return $content;	  
  }
  
  public function get_exemptions($eventid){
	  $content = '';
	  $exec    = DB::SELECT("SELECT r.*
                                   ,e.LastName
                                   ,e.FirstName								   
	                           FROM ESv2_Event_Reservation as r 
					      LEFT JOIN HR_Employees as e ON r.FacultyID=e.EmployeeID
						      WHERE r.RegID='0'");
	  foreach($exec as $r){
		  $empid    = $r->FacultyID;
		  $empname  = $r->LastName.', '.$r->FirstName;
		  switch($empid){
			 case "1":
			    $empname = 'LUNCH BREAK';
             break;			 
			 case "2":
			    $empname = 'AM BREAK';
             break;			 
			 case "3":
			    $empname = 'PM BREAK';
             break;			 
		  }
		  
		  $date    = date('Y-m-d',strtotime($r->ReserveDate));
		  $tstart  = date('h:i A',strtotime($r->TimeStart));
		  $tend    = date('h:i A',strtotime($r->TimeEnd));
		  $content.= '<tr data-id="'.encode($r->EntryID).'">
		                <td><button class="btn btn-sm btn-danger btn-rremove" data-id="'.$r->EntryID.'"><i class="fa fa-trash-o"></i> Remove</button></td>
					    <td>'.$empname.'</td>
					    <td>'.$date.'</td>
					    <td>'.$tstart.'</td>
					    <td>'.$tend.'</td>
					  </tr>';
	  }						  
	  return $content;					   
  }
  
  public function get_sched_student($eventid){
	  $qry = "SELECT r.FacultyID
				    ,ReserveDate 
				    ,TimeStart
				    ,TimeEnd
				    ,r.RegID
				    ,reg.StudentNo
				    ,s.LastName
				    ,s.FirstName
				    ,s.Middlename
				    ,s.MiddleInitial
				    ,s.Fullname
				    ,y.YearLevelName
			    FROM ESv2_Event_Reservation as r
		  INNER JOIN ES_Registrations as reg ON r.RegID=reg.RegID
		  INNER JOIN ES_Students as s ON reg.StudentNo=s.StudentNo
		  INNER JOIN ESv2_Users as u ON r.FacultyID=u.FacultyID
		  INNER JOIN ESv2_YearLevel as y ON reg.ProgID=y.ProgID AND reg.YearLevelID = y.YLID_OldValue
			   WHERE r.EventID = '".$eventid."'
			     AND u.UserIDX = '".getUserID()."'
			ORDER BY ReserveDate,TimeStart";
	  $query   = DB::SELECT($qry);	
	  return $query;
  }
  
  /** For exporting to ical **
  public function generateString() {
	  $created = new DateTime();
	  $content = '';

	  $content = "BEGIN:VEVENT\r\n"
			   . "UID:{$this->uid}\r\n"
			   . "DTSTART:{$this->formatDate($this->start)}\r\n"
			   . "DTEND:{$this->formatDate($this->end)}\r\n"
			   . "DTSTAMP:{$this->formatDate($this->start)}\r\n"
			   . "CREATED:{$this->formatDate($created)}\r\n"
			   . "DESCRIPTION:{$this->formatValue($this->description)}\r\n"
			   . "LAST-MODIFIED:{$this->formatDate($this->start)}\r\n"
			   . "LOCATION:{$this->location}\r\n"
			   . "SUMMARY:{$this->formatValue($this->summary)}\r\n"
			   . "SEQUENCE:0\r\n"
			   . "STATUS:CONFIRMED\r\n"
			   . "TRANSP:OPAQUE\r\n"
			   . "END:VEVENT\r\n";
	  return $content;
  }
 
  public function generateString() {
	  $content = "BEGIN:VCALENDAR\r\n"
				 . "VERSION:2.0\r\n"
				 . "PRODID:-//" . $this->author . "//NONSGML//EN\r\n"
				 . "X-WR-CALNAME:" . $this->title . "\r\n"
				 . "CALSCALE:GREGORIAN\r\n";

	  foreach($this->events as $event) {
		$content .= $event->generateString();
	  }
	  $content .= "END:VCALENDAR";
	  return $content;
  }
  */
}
?>