<?php
namespace App\Modules\Calendar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Calendar\Services\Schedule\schedules as svc;

use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;
use DB;

class Calendar extends Controller
{
	
   private $media = [ 'Title'       => 'Calendar',
                      'Description' => 'Calendar Management',
                      'js'          => ['Calendar/calendar'],
		              'init'        => ['Calendar.init()'],
					  'plugin_js'   => ['bootbox/bootbox.min',
					                    'bootstrap-timepicker/js/bootstrap-timepicker.min',
										'bootstrap-select/bootstrap-select.min',
										'datatables/media/js/jquery.dataTables.min',
										'datatables/extensions/TableTools/js/dataTables.tableTools.min',
										'datatables/extensions/Scroller/js/dataTables.scroller.min',
										'datatables/plugins/bootstrap/dataTables.bootstrap',
										'jquery-validation/js/jquery.validate.min',
										'jquery-validation/js/additional-methods.min',
										'bootstrap-wizard/jquery.bootstrap.wizard.min',
										'SmartNotification/SmartNotification.min',
										'moment.min',
										'fullcalendar/fullcalendar.min',
									   ],
					  'plugin_css'  => ['bootstrap-select/bootstrap-select.min'
									   ,'SmartNotification/SmartNotification'
									   ,'bootstrap-datepicker/css/datepicker'
									   ,'bootstrap-timepicker/css/bootstrap-timepicker'
									   ,'fullcalendar/fullcalendar.min'
									   ],
    ];
    
    private $url =  [
        'page'  => 'calendar/',
        'form' => 'form',
    ];
    
	public $r_view   = 'Calendar.Views.';
	public $showtype = '3,4,5'; 
	
    public function index()
    {
		$this->initializer();
		if($this->permission->has('read')){
		  return view('layout',array('content'=>view($this->r_view.'index',$this->init()),'url'=>$this->url,'media'=>$this->media));
		}else
		  return view(config('app.403'));
    }
	
    public function school()
    {
		$this->showtype = '3,4,5,0';
		$this->initializer();
	    $this->permission = new Permission('school');
		if($this->permission->has('read')){
		  return view('layout',array('content'=>view($this->r_view.'index',$this->init('school')),'url'=>$this->url,'media'=>$this->media));
		}else
		  return view(config('app.403'));
    }
	
    public function personal()
    {
		$this->showtype = '2';
		$this->initializer();
		$this->permission = new Permission('mycalendar');
		if($this->permission->has('read')){
		  return view('layout',array('content'=>view($this->r_view.'index',$this->init('mycalendar')),'url'=>$this->url,'media'=>$this->media));
		}else
		  return view(config('app.403'));
    }
    public function room()
    {
		$this->showtype = '1';
		$this->initializer();
		$this->permission = new Permission('room');
		if($this->permission->has('read')){
		  return view('layout',array('content'=>view($this->r_view.'index',$this->init('room')),'url'=>$this->url,'media'=>$this->media));
        }else
		  return view(config('app.403'));
    }
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event'))
            {
				case 'filter':
				    $txttype   = Request::get('type');
				    $txtfilter = Request::get('filter');
					$exec      = $this->svc->get_filter('employees',$txtfilter);
					$content   = '';
					if($exec){
					   $xid        = '';
					   foreach($exec as $r){
						   if($xid!=$r->EmployeeID){
						     $xid      = $r->EmployeeID;
						     $uid      = $r->UserIDX;
						     $content .= '<tr class="'.(($uid=='')?'danger':'').'" data-id="'.$r->EmployeeID.'" data-uid="'.$r->UserIDX.'">
						                    <td>'.$r->EmployeeID.'</td>
						                    <td class="data-name">'.$r->LastName.', '.$r->FirstName.'</td>
						                    <td>'.$r->PositionDesc.'</td>
						                </tr>';
						  }				
					   }	
					}
					$response = Response::json(array('success'=>true,'content'=>$content));
				break;
                case 'info':
				    $xid      = Request::get('xid');
				    $content  = $this->svc->info_event($xid);
				    $attach   = $this->svc->load_attachment($xid);
					$attend   = $this->svc->get_attendee($xid);
					$url      = url('calendar/school');
					
					if(count($content)>0 && $content->UserID!=getUserID()){
					  $content->IsLock = (($this->permission->has('edit'))?0:1);	
					}
					
					if($content->Reservation==1){
						$url                 = ((isParentOrStudents() || isfaculty())?url('scheduling?event='.encode($xid)):url('scheduling/faculty'));
						$content->RStartDate = date('Y-m-d',strtotime($content->RStartDate));
						$content->RStartTime = date('h:i A',strtotime($content->RStartTime));
						$content->REndTime   = date('h:i A',strtotime($content->REndTime));
					}
					$response = Response::json(array('success'=>true,'content'=>$content,'attach'=>$attach,'attend'=>$attend,'xid'=>$url));
				  break;
                case 'remove':
				    $xid      = Request::get('xid');
				    $attend   = DB::table('ESv2_Event_Attendee')->where('EventID',$xid)->delete();
				    $attachs  = DB::table('ESv2_Event_Attachment')->where('EventID',$xid)->delete();
				    $content  = DB::table('ESv2_Events')->where('EntryID',$xid)->delete();
					$response = Response::json(array('success'=>true,'content'=>$content));
				  break;
				case 'save':
				    $xid  = Request::get('xid');
				    $data = Request::all();
				    $exec = $this->svc->save_event($xid,$data);
					$response = Response::json(array('success'=>true,'content'=>'Success','xid'=>$exec));
				  break;
				case 'event':
				    $start    = Request::get('start');
				    $end      = Request::get('end');
					$cond     = Request::get('condition');
					$cond   = (($cond && $cond!='')?$cond.' AND':'');	
					
					$data     = $this->svc->load_event($start,$end,$cond);
					$data     = json_encode($data);
					$data     = json_decode($data);
				    $response = Response::json($data);
				  break;
				case 'room':
				    $startdate = Request::get('startd');
					$starttime = Request::get('startt');
					$enddate   = Request::get('endd');
					$endtime   = Request::get('endt');
				    $room      = $this->svc->get_rooms('1',$startdate,$starttime,$enddate,$endtime); 
					$response  = Response::json(array('success'=>true,'content'=>$room));
				  break;
				case 'delattach':
				    $xid      = Request::get('aid');
				    $content  = DB::table('ESv2_Event_Attachment')->where('EntryID',$xid)->delete();
				    $response = Response::json(array('success'=>true,'content'=>$content));
				   break;	
				case 'attendee':
				     $xid       = Request::get('xid');
				     $uid       = Request::get('uid');
				     $xdel      = DB::statement("DELETE FROM ESv2_Event_Attendee WHERE EventID='".$xid."'");
					 if(strpos($uid,'|')!==false){
				       $tmpval  = explode('|',$uid);
					   foreach($tmpval as $x){
						  $exec = DB::statement("INSERT INTO ESv2_Event_Attendee(EventID,UserID) VALUES('".$xid."','".$x."')");
					   }
					 }else{
					   $exec    = DB::statement("INSERT INTO ESv2_Event_Attendee(EventID,UserID) VALUES('".$xid."','".$uid."')");
					 }
				     $response = Response::json(array('success'=>true,'content'=>$exec));
                   break;				
				case 'attachment':
				    $xid         = Request::get('xid');
				    $pathname    = str_replace('index.php','assets/upload/',$_SERVER['SCRIPT_FILENAME']).str_pad($xid,8,'0',STR_PAD_LEFT);
					if(!file_exists($pathname)){mkdir($pathname);}
				    $fileName    = $_FILES['file']['name'];
                    $fileType    = $_FILES['file']['type'];
                    $fileError   = $_FILES['file']['error'];
                    $fileContent = file_get_contents($_FILES['file']['tmp_name']);
					file_put_contents($pathname.'/'.$fileName,$fileContent);
				    $exec = DB::table('ESv2_Event_Attachment')->insert(array('EventID'=>$xid,'Filename'=>$fileName,'Extname'=>$pathname.'/'.$fileName));
				    $response = Response::json(array('filename'=>$fileName));
                  break;				
				  case 'exemption':
				    $eventid  = Request::get('schedule');
				    $content  = $this->svc->get_exemptions($eventid);
					$employee = $this->svc->get_employees();
					$response = Response::json(array('success'=>true,'content'=>$content,'employee'=>$employee));
				  break;				
				  case 'remexempt':
				    $entryid  = Request::get('entryid');
				    $exec     = DB::statement("DELETE FROM ESv2_Event_Reservation WHERE EntryID='".$entryid."'");
					$response = Response::json(array('success'=>$exec,'message'=>(($exec)?'Success':'Failed')));
				  break;
				  case 'email':
				    $response ='';
				  break;
			}
            return $response;
        }
        return $response;			
	}
	public function booking($eid=0,$m='b'){
	   $this->initializer();
	   $eid    = Request::get('event');
	   $m      = ((Request::get('mode'))?(Request::get('mode')):'b');
	   $order  = (($m=='f')?" ORDER BY emp.LastName,r.FacultyID,r.TimeStart":"");
	   $result = DB::select("SELECT e.EntryID
								   ,e.Title
								   ,e.StartDate
								   ,e.EndDate
								   ,r.UserID
								   ,u.Email
								   ,u.FamilyID
								   ,u.FullName
								   ,r.RegID
								   ,s.Fullname as StudentName
								   ,r.FacultyID
								   ,subj.SubjectCode
								   ,emp.LastName
								   ,emp.FirstName
								   ,r.ReserveDate
								   ,r.TimeStart
								   ,r.TimeEnd
								   ,r.DatePosted 
							  FROM ESv2_Events as e 
						INNER JOIN ESv2_Event_Reservation as r ON e.EntryID=r.EventID
						INNER JOIN ESv2_Users as u ON r.UserID=u.UserIDX
						INNER JOIN ES_Registrations as reg ON r.RegID=reg.RegID
						INNER JOIN ES_Students as s ON reg.StudentNo=s.StudentNo
						INNER JOIN HR_Employees as emp ON r.FacultyID=emp.EmployeeID
						INNER JOIN ES_Subjects as subj ON r.SubjectID=subj.SubjectID
							 WHERE e.EntryID='".$eid."' ".$order);						 
	   $xtbl   ='';
	   $xtdt   ='';
	   $xtitle ='';
	   $xprev  ='';
	   $xregid ='';
	   $i      = 1;
	   foreach($result as $rs){
	     $xtitle = $rs->Title;
		 if($m==='f'){
	      if($xprev!=$rs->FacultyID){
		  if($xprev!=''){
		  $xtdt   .='<tr><td colspan="3">&nbsp;</td></tr>'; 
		  }
		  $xtdt   .='<tr><td colspan="3">Faculty: <b>'.$rs->LastName.','.$rs->FirstName.'</b></td></tr>'; 
		  $xprev   =$rs->FacultyID;
		  $i       = 1;
		  }
	      $xtdt   .='<tr><td>'.$i.'. '.$rs->FullName.' ('.$rs->Email.')'.'</td>
		                 <td>'.$rs->StudentName.'('.$rs->RegID.')</td>
						 <td>'.date('Y-m-d H:i A',strtotime($rs->ReserveDate.' '.$rs->TimeStart)).' - '.date('H:i A',strtotime($rs->ReserveDate.' '.$rs->TimeEnd)).'</td></tr>';
		  $i++;
		 }else{
	     if($xprev!=$rs->UserID){
		 if($xprev!=''){
		 $xtdt   .='<tr><td colspan="2">&nbsp;</td></tr>'; 
		 }
		 $xtdt   .='<tr><td colspan="2">'.$i.'. Parent: <b>'.$rs->FullName.'('.$rs->Email.')</b></td></tr>'; 
		 $xprev   =$rs->UserID;
		 $xregid  ='';
		 $i++;
		 }
	     if($xregid!=$rs->RegID){
		 if($xregid!=''){
		 $xtdt   .='<tr><td colspan="2">&nbsp;</td></tr>'; 
		 }
		 $xtdt   .='<tr><td colspan="2">Student: <i>'.$rs->StudentName.'('.$rs->RegID.')</i></td></tr>'; 
		 $xregid  =$rs->RegID;
		 }
		 $xtdt   .='<tr><td>'.$rs->LastName.','.$rs->FirstName.'</td>
		                <td>'.date('Y-m-d H:i A',strtotime($rs->ReserveDate.' '.$rs->TimeStart)).' - '.date('H:i A',strtotime($rs->ReserveDate.' '.$rs->TimeEnd)).'</td></tr>';
		 }				
	   }
	   echo '<h4>'.$xtitle.'</h4>';
	   echo '<table><tbody>'.$xtdt.'</tbody></table>';
	}

    private function initializer($apage='school')
    {
	   ini_set('max_execution_time', 300);
	   $this->svc        = new svc;
	   $this->permission = new Permission($apage);
	}
	
	public function init($apage='assess')
	{
		$this->initializer($apage);
		$dropdown = '';
		$show     = '';
		$color    = 'green-meadow';
		switch($this->showtype){
		  case '0':
		    $show     = "TypeID IN (".$this->showtype.")";
		    $dropdown = '<option value="0" data-period="0" data-room="0" data-reserve="0" data-attach="1">Bulletin</option>';
		   break;
		  case '1':
		    $color    = 'blue';
			if($this->permission->has('edit') && $this->permission->has('delete')){
		     $show    = "TypeID = '".$this->showtype."'";
			}else{
		     $show    = "TypeID = '".$this->showtype."' AND (UserID='".getUserID()."' OR EntryID IN (SELECT EventID FROM ESv2_Event_Attendee WHERE UserID='".getUserID()."')) ";
			}
		    $dropdown = '<option value="1" data-period="1" data-room="1" data-reserve="0" data-attach="0">Room Reservation</option>';
		   break;
		  case '2':
		    $color    = 'green-meadow';
		    $show     = "(TypeID IN (".$this->showtype.") AND UserID='".getUserID()."') ";
		    $dropdown = '<option value="2" data-period="1" data-room="1" data-reserve="0" data-attach="1">Personal Event</option>';
		   break;
		  default:
		    $color    = 'red-pink';
			$show     = ((isParentOrStudents())?($show." AND NotifyParents=1 "):$show);
			$show     = ((isParentOrStudents()==false && isfaculty()==true )?($show." AND (NotifyFaculty=1 OR (NotifyFaculty=0 AND UserID='".getUserID()."')) "):$show);
			$show     = ((isParentOrStudents()==false && isfaculty()==false)?($show." AND (NotifyEmployees=1 OR (NotifyEmployees=0 AND UserID='".getUserID()."')) "):$show);
            $show     = "TypeID IN (".$this->showtype.") ".$show." ";
			$dropdown = '<option value="0" data-period="0" data-room="0" data-reserve="0" data-attach="1">Bulletin</option>
			             <option value="3" data-period="1" data-room="1" data-reserve="1" data-attach="1">School Event</option>
						 <option value="4" data-period="1" data-room="1" data-reserve="1" data-attach="1">Parent Teacher</option>
						 <option value="5" data-period="0" data-room="0" data-reserve="0" data-attach="0">Holiday</option>';
			break;		  
		}
		$data = array('show'       => $show,
		              'dropdown'   => $dropdown,
					  'color'      => $color,
					  'permission' => $this->permission
					  );
		return $data;
	}	
	
}
?>