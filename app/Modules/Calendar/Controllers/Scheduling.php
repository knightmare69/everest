<?php
namespace App\Modules\Calendar\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Calendar\Services\Schedule\schedules as svc;
use App\Modules\Webmail\Services\Emailer as mailer;

use App\Libraries\CrystalReport as xpdf;
use Response;
use Request;
use Input;
use Permission;
use DB;
use Config;

class Scheduling extends Controller
{
     private $media = [ 'Title'     => 'Scheduling',
                      'Description' => 'Scheduling Management',
                      'js'          => ['Calendar/scheduling'],
		              'init'        => [],
					  'plugin_js'   => ['bootbox/bootbox.min',
					                    'bootstrap-timepicker/js/bootstrap-timepicker.min',
										'bootstrap-select/bootstrap-select.min',
										'datatables/media/js/jquery.dataTables.min',
										'datatables/extensions/TableTools/js/dataTables.tableTools.min',
										'datatables/extensions/Scroller/js/dataTables.scroller.min',
										'datatables/plugins/bootstrap/dataTables.bootstrap',
										'jquery-validation/js/jquery.validate.min',
										'jquery-validation/js/additional-methods.min',
										'bootstrap-wizard/jquery.bootstrap.wizard.min',
										'SmartNotification/SmartNotification.min',
										'moment.min',
										'fullcalendar/fullcalendar.min',
									   ],
					  'plugin_css'  => ['bootstrap-select/bootstrap-select.min'
									   ,'SmartNotification/SmartNotification'
									   ,'bootstrap-datepicker/css/datepicker'
									   ,'bootstrap-timepicker/css/bootstrap-timepicker'
									   ,'fullcalendar/fullcalendar.min'
									   ],
    ];
    
    private $url =  [
        'page'  => 'scheduling/',
        'form' => 'form',
    ];
    
	public $r_view   = 'Calendar.Views.Scheduling.';
	
    public function index()
    {
		$this->initializer();
		$udate   = env('PTC_OPEN');
		$ddate   = env('PTC_CLOSE');
		$cdate   = date('Y-m-d h:i A');
		$xdate   = date('Y-m-d h:i A',strtotime($udate));
		$edate   = date('Y-m-d h:i A',strtotime($ddate));
		$event   = Request::get('event');
		$isopen  = DB::select("SELECT Count(*) as Items FROM ESv2_Events WHERE CONVERT(DATE,RStartDate)>=GETDATE() AND EntryID='".decode($event)."'");
		if($this->permission->has('read')){
		   if((strtotime($cdate)<strtotime($xdate)) && (strtotime($cdate)>strtotime($edate))){
			   die('Parent-Teacher Conference Booking Is Open By <b>'.env('PTC_OPEN').'</b> to <b>'.env('PTC_CLOSE').'</b>');
		   }
		   
		   if(isfaculty()==false){
		     return view('layout',array('content'=>view($this->r_view.'index',$this->init('reserve')),'url'=>$this->url,'media'=>$this->media));
		   }else{
			 return view('layout',array('content'=>view($this->r_view.'students',$this->init('faculty')),'url'=>$this->url,'media'=>$this->media));  
		   }
		}else
		   return view(config('app.403'));
    }
	
	public function txn(){
		ini_set('max_execution_time', 60);
        $response = 'No Event Selected';
        if (Request::ajax())
        {
            $this->initializer();
            $response = ['success'=>false,'error'=>true,'message'=>'Permission Denied!'];
            switch(Request::get('event'))
            {
				case 'faculty':
				    $schedule = Request::get('sched');
				    $regid    = Request::get('regid');
					$trdata   = $this->svc->get_faculty($schedule,$regid);
					$content  = (string)view($this->r_view.'faculty',array('trdata'=>$trdata));
					$response = Response::json(array('success'=>true,'content'=>$content,'regid'=>$regid));
				break;
				case 'optsched':
				    $schedule  = Request::get('sched');
				    $facultyid = Request::get('faculty');
					$content   = $this->svc->get_available($schedule,$facultyid);
					$response  = Response::json(array('success'=>true,'content'=>$content));
				break;
				case 'savesched':
				    $schedule  = Request::get('sched');
				    $studno    = Request::get('studno');
				    $faculty   = Request::get('faculty');
				    $subject   = Request::get('subject');
					$rdate     = Request::get('rdate');
				    $tstart    = Request::get('start');
				    $tend      = Request::get('end');
					if($tstart==''){
					  $exec      = DB::STATEMENT("DELETE FROM ESv2_Event_Reservation WHERE UserID='".getUserID()."' AND FacultyID='".$faculty."' AND SubjectID='".$subject."'");	
					}else{
					  $exec      = $this->svc->save_schedule($schedule,$studno,$faculty,$subject,$rdate,$tstart,$tend);
					}
					$content   = $this->svc->get_exemptions($schedule);
					$response  = Response::json(array('success'=>$exec,'message'=>'Successfully Save','content'=>$content));
				break;
				case 'locksched':
				    $schedule  = Request::get('sched');
				    $studno    = Request::get('studno');
				    $option    = Request::get('option');
					if($option==1){
					   $email    = DB::select("SELECT Email FROM ESv2_Users WHERE UserIDX='".getUserID()."'")[0]->Email;
                            //$exec     = DB::statement("UPDATE ESv2_Event_Reservation SET DatePosted='".date('Y-m-d H:i:s')."' WHERE EventID='".$schedule."' AND RegID='".$studno."'");

					   $trdata   = $this->svc->get_faculty($schedule,$studno);
					   $content  = (string)view($this->r_view.'email',array('trdata'=>$trdata));
					   $data     = array('to'     => $email
									   ,'from'    => array('from'=>'ptcnoreply@everestmanila.edu.ph','label'=>'noreply')
									   ,'subject' => 'Parent-Teacher Conference Confirmation'
									   ,'message' => $content
									   ,'attach'  => array()
					                   );
					   
					   Config::set('mail.host', 'smtp.googlemail.com');
					   Config::set('mail.port', '465');
					   Config::set('mail.username', 'ptcnoreply@everestmanila.edu.ph');
					   Config::set('mail.password', 'ptc2020!!');
					   $exec   = $this->mailer->sendEmail($data);
					}else
					   $exec   = DB::statement("UPDATE ESv2_Event_Reservation SET DatePosted=NULL WHERE EventID='".$schedule."' AND RegID='".$studno."'");
				   
					$response  = Response::json(array('success'=>$exec,'message'=>'Successfully Locked'));
				break;
				case 'fac_schedule':
				    $schedid  = Request::get('schedule');
					$content  = $this->svc->get_faculty_schedule($schedid);
					$response = Response::json(array('success'=>true,'content'=>$content,'message'=>'Successfully Locked'));
				break;
            }
            return $response;
        }
        return $response;			
	}
	
	public function print_data(){
	      $this->initializer();
		  
          $eventid = Request::get('schedule');
		  $userid  = getUserID();
		  
          $this->xpdf->filename='ptc_reservation.rpt';
		  $this->xpdf->query="EXEC rpt_ptc_reservation '".$eventid."','".$userid."'";
		  $subrpt = array(
                           'subreport1' => array('file' =>'Company_Logo','query' => "EXEC ES_rptReportLogo '". env('APP_TITLE','COMPANY ABC'). "', 'Republic', 'Muntinlupa City', '-1'")
		                 ); 
						 
		  $this->xpdf->SubReport=$subrpt;				   
		  $data = $this->xpdf->generate();
		  
		  header('Content-type: application/pdf');
          header('Content-Disposition: inline; filename="' . $data['filename'] . '"');
          header('Content-Transfer-Encoding: binary');
          header('Accept-Ranges: bytes');
          @readfile($data['file']);	
	}
	
    private function initializer($apage='school')
    {
	   ini_set('max_execution_time', 300);
	   $this->svc        = new svc;
	   $this->mailer     = new mailer;
	   $this->permission = new Permission($apage);
	   $this->xpdf       = new xpdf;
	}
	
	public function init($apage='assess')
	{
	   $this->initializer();
	   $event = Request::get('event');
	   if($apage=='reserve'){
		   if($event=='' || $event==false || ( base64_encode(base64_decode($event)) !== $event)){
			   header('Location: '.url('calendar/school'));
			   die('Invalid Event! Make sure you load scheduling from Calendar Module!');
		   }
		   $data  = array(
						  'eventid'    => decode($event)   
						 ,'permission' => $this->permission
						 ,'students'   => $this->svc->get_students()
						);
	   }else if($apage=='faculty'){
		   if($event=='' || $event==false || ( base64_encode(base64_decode($event)) !== $event)){
			   header('Location: '.url('calendar/school'));
			   die('Invalid Event! Make sure you load scheduling from Calendar Module!');
		   }
		   $data = array(
		                  'eventid'    => decode($event)
						 ,'permission' => $this->permission
						 ,'table'      => $this->svc->get_sched_student(decode($event))
		                );
	   }else{
		   $data = array(
		                 'events' => $this->svc->get_eventlist()
						,'trdata' => $this->svc->get_faculty_schedule(0)
						 );  
	   }			
	   return $data;
	}	
}
?>