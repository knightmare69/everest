<?php
namespace App\Modules\OnlinePayment\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\OnlinePayment\Services\PaymentRecords as PaymentRecords;
use App\Modules\OnlinePayment\Models\Portals as Portals;
use App\Modules\OnlinePayment\Models\OnlinePaymentRecords as TransactionRecords;
use App\Modules\OnlinePayment\Models\OnlinePaymentTxUsed as TxUsed;

use Request;
use DB;

class OnlinePayment extends Controller
{
    private $media =
		[
			'Title'=> 'Online Payment',
			'Description'=> 'Welcome Parent/Guardian!',
			'js'		=> ['OnlinePayment/records.js'],
			'plugin_js'	=> [
				'bootbox/bootbox.min',
                'bootstrap-select/bootstrap-select.min',
                'datatables/media/js/jquery.dataTables.min',
                'datatables/extensions/TableTools/js/dataTables.tableTools.min',
                'datatables/extensions/Scroller/js/dataTables.scroller.min',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
                'bootstrap-datepicker/js/bootstrap-datepicker',
                'select2/select2.min',
                'bootstrap-fileinput/bootstrap-fileinput',
			],
			'plugin_css' => [
				'bootstrap-datepicker/css/datepicker',
                'select2/select2',
                'datatables/plugins/bootstrap/dataTables.bootstrap',
				'bootstrap-fileinput/bootstrap-fileinput',
				'online-payment/styles'
			]
        ];
    
    private $url = [ 'page' => 'online-payment/' ];
        
    private $views = 'OnlinePayment.Views.';
    
    public function index()
 	{
		$this->initializer();
		return view('layout',array(
			'content'=>view($this->views.'index')->with(['views'=>$this->views, 'payments' => $this->payment_records->getRecords()]),
			'url'=>$this->url,
			'media'=>$this->media,
		));
	}

	public function event()
	{
		$response = ['error' => true, 'message' => 'No Event Selected'];
		$this->initializer();
		if (Request::ajax()) {
			$records = array();
			$search = Request::has('search')?Request::get('search'):array();
			$page = Request::has('page')?Request::get('page'):1;
			$idno = decode(Request::get('id_number'));
			$portal = Request::get('portal');
			$txno = Request::get('transaction_number');
			$decode_tx = Request::has('decode')?Request::get('decode'):false;
			$or_number = Request::has('orno')?Request::get('orno'):'';
			
			switch(Request::get('event'))
			{		
				case 'portal-list':
					$portals = $this->payment_portals->get();
					
					return array('error' => false, 'message' => 'Successfully get lists of online payment portals', 'data' => $portals);
				break;

				case 'cashier-verify-transaction':
					
					$info = Request::all();
						
					$merchant_info = $this->payment_portals
									->where('name','like','%'.$info['portal'].'%')
									->first();

					return json_encode(array('data' => $merchant_info, 'count' => count($merchant_info)));

				break;

				case 'transaction-id':
					$select = array(
						'_id',
						'transaction_code'
					);
					
					$model = $this->transaction_records
							->select($select)
							->where('payment_gateway','like','%'.$portal.'%')
							->where('is_validated','=',1)
							->where('student_id','=',$idno)
							->skip($start)->take(10);

					if (isset($search) && !empty($search) && $search != '' && isset($portal) && !empty($portal) && $portal != '') {
						$start = 10 * ($page-1);
						$records = $model->where('transaction_code','like','%'. $search .'%')
									->get();
					}
					
					$records = $model->where('is_code_used','=',false)
								->get();
								
					$more = false;
					if (count($records) > 10){
						unset($records[count($records)-1]);
						$more = true;
					}
					
					if(!empty($records)){
						foreach($records as &$info){
							$info->id = encode($info->_id);
							unset($info->_id);
		
							$info->text = $info->transaction_code;
							unset($info->transaction_code);
						}
					}

					return json_encode(array('results' => $records,'pagination' => array('more' => $more)));

				break;
				
				case 'transaction-information':

					$txno = $decode_tx ? decode($txno) : $txno;

					$select = array(
						DB::raw('online_payment_tx_records.amount as tx_amount'),
						'online_payment_tx_records.transaction_code',
						'online_payment_tx_records.payment_status',
						'online_payment_tx_records.request_status',
						'online_payment_tx_records.user_fullname',
						'online_payment_tx_records.student_name',
						'online_payment_tx_records.student_id',
						'online_payment_tx_records.student_term',
						DB::raw('online_payment_charge_records.ptc_amount as ptc_charge'),
						DB::raw('online_payment_charge_records.school_amount as school_charge')
					);

					$records = $this->transaction_records->select($select)
							->join('online_payment_charge_records','online_payment_tx_records.charge_id','=','online_payment_charge_records._id')
							->where('online_payment_tx_records._id', $txno)
							->where('online_payment_tx_records.student_id','=', $idno)
							->get();

					foreach($records as $info)
					{
						$info->tx_amount = $info->tx_amount - ($info->ptc_charge + $info->school_charge);
						unset($info->ptc_charge);
						unset($info->school_charge);
					}

					return json_encode(array('results' => $records));
					
				break;

				case 'use-transaction-code':
					$txno = $decode_tx ? decode($txno) : $txno;
					$cashier = getFullNameByID(getUserID());
					
					$query = $this->transaction_records
						->where('_id',$txno)
						->where('student_id',$idno);
					
					$query->update(array(
						'is_code_used' => true
					));

					$data = $query->first();
					
					$info = array(
						'id' => $data->_id,
						'or_number' => $or_number,
						'cashier_name' => $cashier
					);
					
					$this->transaction_used->fill($info);

                    if($this->transaction_used->save()){
						return array('error' => false, 'message' => 'Transaction code used successfully!', 'affectedRows' => $data);
                    }

				break;
				
				case 'verify-transaction':
						$ch = curl_init();

						curl_setopt($ch, CURLOPT_URL, Request::get('url'));
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

						$data = curl_exec($ch);

						$info = curl_getinfo($ch);

						$pending_codes = array('P','R','K','V','A');
						$error_codes = array('F','U');

						curl_close($ch);

						if($data)
						{
							$validated = 0;
							
							if(in_array($data,$pending_codes))
							{
								$validated = 2;
							}
							else if(in_array($data,$error_codes))
							{
								$validated = -1;
							}
							else
							{
								$validated = 1;
							}
							
							$txno_checking = $this->transaction_records
											->where('student_id', '=',$idno)
											->where('transaction_code','=',$txno)
											->where('is_code_used','=',false);

							$tx_data = $txno_checking->get()->toArray();

							if(count($tx_data) == 1)
							{
								$txno_checking->update(['is_validated' => $validated,'request_status' => $data]);
							}
						}
						return $data ? array('error' => false, 'response_code' => $info['http_code'], 'message' => 'Verified successfully', 'status' => $data, 'records' => $tx_data) : array('error' => true, 'response_code' => $info['http_code'], 'message' => 'Online Portal cannot reached, please check your internet connection or contact the administrator', 'status' => 'Offline');
				break;

				default:
					return $response;
				break;
			}
		}

		return $response;
	}

	public function initializer()
	{
		$this->payment_records = new PaymentRecords;
		$this->payment_portals = new Portals();
		$this->transaction_records = new TransactionRecords();
		$this->transaction_used = new TxUsed();
	}
}