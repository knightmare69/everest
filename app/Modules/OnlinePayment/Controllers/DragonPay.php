<?php
namespace App\Modules\OnlinePayment\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Students\Services\StudentProfileServiceProvider as StudentServices;
use App\Modules\Admission\Services\GuardianConnectServices as Services;
use App\Modules\Students\Services\StudentProfileServiceProvider as StudentProfileService;
use App\Modules\OnlinePayment\Models\OnlinePaymentRecords as OnlinePaymentRecords;
use App\Modules\OnlinePayment\Models\OnlinePaymentTxGenerator as OnlinePaymentTxGenerator;
use App\Modules\OnlinePayment\Models\ChargeRecords as CRecords;
use App\Modules\OnlinePayment\Models\ChargeSetup as CSetup;
use App\Modules\OnlinePayment\Models\Portals as Portals;
use Request;
use DB;
class DragonPay extends Controller 
{
    private $media =
		[
			'Title'=> 'Online Payment',
			'Description'=> 'Welcome Parent/Guardian!',
			'js'		=> ['OnlinePayment/payment.js'],
			'init'		=> ['ONLINE_PAYMENT.init()'],
			'plugin_js'	=> [
				'bootbox/bootbox.min','jquery-validation/js/jquery.validate.min',
				'select2/select2.min',
				'jquery-multi-select/js/jquery.multi-select',
                'bootstrap-select/bootstrap-select.min',
				'smartnotification/smartnotification.min'
			],
			'plugin_css' => [
				'bootstrap-select/bootstrap-select.min',
				'select2/select2',
				'jquery-multi-select/css/multi-select',
                'smartnotification/smartnotification',
				'online-payment/styles'
			]
        ];
    
	private $url = [ 'page' => 'dragonpay/' ];
	private $url_m = [ 'page' => 'dragonpay/message'];
        
	private $views = 'OnlinePayment.Views.dragonpay.';
	
	public function index()
 	{
		$this->initializer();
		return view('layout',
				array(
					'content' => view(
						$this->views.'index',
						$this->init()
					)->with(array(
						'views'=>$this->views,
						'students'=>$this->services->connectStudentsData()
						)
					),
				'url'=>$this->url,
				'media'=>$this->media,
				));
	}
	
	public function event()
	{
		$response = array('error' => true, 'message' => 'No Event Selected');

		if (Request::ajax()) 
		{
			$this->initializer();

			$response['message'] = Request::get('event');
			$post = Request::all();
			$event = Request::get('event');
			// $response = ['error' => true, 'message' => Request::get('event')];

			switch($event)
			{
				case 'billing':
					$student_id = getUserGroup() == 'parent' ? $post['studnum'] : getUserID2();
					$academic_term = decode($post['term']);
					
					$select = array(
						'reg.RegID',
						'reg.RegDate',
						'reg.StudentNo',
						't.TermID',
						't.AcademicYear',
						't.SchoolTerm',
						DB::raw("fn_StudentName(reg.StudentNo) as Name"),
						DB::raw("fn_K12_YearLevel(reg.YearLevelID) as YearLevel")
					);

					$ay_term = DB::table('es_registrations as reg')
						->leftJoin('es_ayterm as t','t.TermID','=','reg.TermID')
						->select($select)
						->where('reg.StudentNo', $student_id)
						->where('reg.TermID', $academic_term)
						->orderBy('RegDate', 'desc')
						->first();
					
					$register_id = $ay_term->RegID;

					// set schedule payment
					setPaymentSchedule($register_id, $student_id, 'jmc');

					$select = array(
						'1stPayment',
						'2ndPayment',
						'3rdPayment',
						'4thPayment',
						'5thPayment',
						'6thPayment',
						'7thPayment',
						'8thPayment',
						'9thPayment',
						'10thPayment'
					);

					// query : get schedule payment
					$sched = DB::table('es_registration_installment')
								->select($select)
								->where('RegID', $register_id)
								->first();

					// assign schedule payment to object
					$schedule_amount = new \stdClass();

					foreach ($sched as $key => $value) {
						$schedule_amount->FullPayment += $value;
						$schedule_amount->$key = $value;
					}
					// get student details
					$student_details = array(
						'school_term' => $ay_term->SchoolTerm,
						'academic_year' => $ay_term->AcademicYear,
						'student_no' => $student_id,
						'name' => $ay_term->Name,
						'year_level' => $ay_term->YearLevel,
						'active_charge_setup' => $this->charge_setup->where('status','=',1)->first()
					);

					// initial render for payment list before append to html
					$payment_schedule_view = view( $this->views.'payment-list', 
								array(
									'sched' => $schedule_amount,
									'views'=> $this->views
								))->render();
					// initial render for receipt page before append to html
					$transaction_view = view( $this->views.'receipt', 
								array(
									// 'reg' => $r,
									'views'=> $this->views
								))->render();
					// response html
					$response = array(
									'error' => false, 
									'list' => $payment_schedule_view, 
									'transaction' => $transaction_view, 
									'student_details' => $student_details,
								);
					break;
						
				case 'enrolled-term':
					
					if(getUserGroup() == 'parent')
					{
                        $stud_num = Request::get('studnum');

						if (!empty($stud_num))
						{
                            $studs = $this->services->connectStudentsData();
							
							$subs_stud_nums = array_pluck($studs, 'StudentNo');

							if(in_array($stud_num, $subs_stud_nums))
							{
                                $acad_year = $this->studentProfileService->regsStudent_academic_year($stud_num);
                                // $opts = array_map(function($k){ return '<option value="'.encode($k->TermID).'">'.$k->AcademicYear.' '.$k->SchoolTerm.'</option>'; }, $acad_year);
								$response = ['error' => false, 'data' => encode($acad_year[0]->TermID)];
							} 
							else 
							{
                                $response = ['error' => true, 'message' => "Your selected student does not subscribed to your account. Please select another."];
                            }
                        }
					}
					
					break;

				case 'unused-transaction':

					$tx_avail = false;
					$tx_codes = $this->transaction_id->orderBy('_id', 'asc')->get();
					$tx_request = $this->transaction_details->get();
					if($tx_request->count() < 1)
					{	// help resolve null/empty data in table
						$id = $this->transaction_id->insertGetId(array('transaction_code' => "OPR-"));
						$response = ['error' => false,'unused' => true, 'message' => 'Found ID that is Unused', 'tx_id' => 1];
					}
					else
					{
						foreach($tx_codes as $code)
						{
							// check if id created is used
							$tx_used = $this->transaction_details->where('transaction_id', $code->_id)->count();
							if($tx_used == 0)
							{
								$tx_avail = true;
								$response = array('error' => false,'unused' => true, 'message' => 'Found ID that is Unused', 'tx_id' => $code->_id);
								break 2;
							}
						}
					}

					$tx_avail ? $response = ['error' => false,'unused' => false, 'message' => 'No ID that is Unused'] : $response;
					
					break;

				case 'get-transaction':

					$tx_code = Request::get('code');
					$id = $this->transaction_id->insertGetId(array('transaction_code' => $tx_code));
					$response = array('error' => false, 'tx_id' => $id);

				break;

				case 'pay-request':

					$u_data = Request::get('u_data');
					$charge_data = Request::get('charges_info');
					$fields = array(
						'txnid' => array(
							'label' => 'Transaction ID',
							'type' => 'text',
							'attributes' => array(),
							'filter' => FILTER_SANITIZE_STRING,
							'filter_flags' => array(FILTER_FLAG_STRIP_LOW),
						),
						'amount' => array(
							'label' => 'Amount',
							'type' => 'number',
							'attributes' => array('step="0.01"'),
							'filter' => FILTER_SANITIZE_NUMBER_FLOAT,
							'filter_flags' => array(FILTER_FLAG_ALLOW_THOUSAND, FILTER_FLAG_ALLOW_FRACTION),
						),
						'description' => array(
							'label' => 'Description',
							'type' => 'text',
							'attributes' => array(),
							'filter' => FILTER_SANITIZE_STRING,
							'filter_flags' => array(FILTER_FLAG_STRIP_LOW),
						),
						'email' => array(
							'label' => 'Email',
							'type' => 'email',
							'attributes' => array(),
							'filter' => FILTER_SANITIZE_EMAIL,
							'filter_flags' => array(),
						),
					);

					$portal_payment = $this->portals->where('name','like','DragonPay%')->first();
					$urlDragonpay = $portal_payment['is_production'] > 0 ? $portal_payment['merchant_production_url'] . 'Pay.aspx?' : $portal_payment['merchant_test_url'] . 'Pay.aspx?';
							
					$parameters = array(
						"merchantid" => $portal_payment['merchant_name'],
						"txnid" => $post['txnid'],
						"amount" => $post['amount'],
						"ccy" => "PHP",
						"description" => $post['description'],
						"email" =>  $post['email'],
					);
						
					foreach ($fields as $key => $value) 
					{
						if (isset($_POST[$key])) 
						{
							$parameters[$key] = filter_input(
								INPUT_POST,
								$key,
								$value['filter'],
								array_reduce($value['filter_flags'], function ($a, $b) {
								return $a | $b;
								}, 0)
							);
						}
					}
					$chargeId = $this->charge_records->insertGetId(array(
						'transaction_code' => $post['txnid'],
						'charge_setup' => $charge_data['charge_setup'],
						'ptc_amount' => $charge_data['ptc_amount'],
						'school_amount' => $charge_data['school_amount']
					));
					
					$parameters['amount'] = number_format($parameters['amount'],2,'.','');
					$parameters['key'] = $portal_payment['is_production'] > 0 ? $portal_payment['merchant_production_password'] : $portal_payment['merchant_test_password'];

					$digest_string = implode(':', $parameters);
					unset($parameters['key']);
					$parameters['digest'] = sha1($digest_string);
					
					$urlDragonpay .= http_build_query($parameters, '', '&');
					
					$this->transaction_details->insert(array(
						'transaction_id' => $post['txid'],
						'transaction_code' => $post['txnid'],
						'charge_id' => $chargeId,
						'amount' => $parameters['amount'],
						'description' => $parameters['description'],
						'email' => $parameters['email'],
						'student_name' => $u_data['name'],
						'student_term' => $u_data['academic_year']. ' ' . $u_data['school_term'],
						'student_id' => getUserGroup() == 'parent' ? $u_data['student_no'] : getUserID2(),
						'user_id' => getUserID2(),
						'user_fullname' => getUserFullName(),
						'user_group' => getUserGroup(),
						'payment_status' => 'On Transaction',
						'payment_gateway' => 'DragonPay',
						'is_validated' => -1
					));
					
					$response = ['error' => false, 'html' => $urlDragonpay];

				break;
			}
		}

		ob_clean();

        return $response;
	}

	public function message()
	{
		if(Request::isMethod('get'))
		{
			$this->initializer();
			$get_request = Request::all();

			$dp_message = array(
				"txnid" => $get_request['txnid'],
				"referenceno" => $get_request['refno'],
				"status" => $get_request['status'],
				"message" => $get_request['message'],
				"digest" => $get_request['digest'],
			);

			$transact = $this->transaction_details->where('request_digest','like',$dp_message['digest']."%")->count();
			if($transact == 1 && isset($dp_message['referenceno']))
			{
				unset($this->media['js']);
				unset($this->media['init']);

				return view('layout',array(
					'content'=>view($this->views.'message')->with(['views'=>$this->views,'postback'=>$dp_message]),
					'url'=>$this->url_m,
					'media'=>$this->media,
				));
			}
			else
			{
				return view(config('app.403'));
			}
		}
	}

	public function postback_message(){

		if(Request::isMethod('post'))
		{
			$this->initializer();
			$get_request = Request::all();

			$dp_message = array(
				"txnid" => $get_request['txnid'],
				"referenceno" => $get_request['refno'],
				"status" => $get_request['status'],
				"message" => $get_request['message'],
				"digest" => $get_request['digest']
			);

			if(isset($dp_message['digest']))
			{
				$transact = $this->transaction_details->where('transaction_code','like',$dp_message['txnid']."%")->count();
				
				if($transact == 1 && isset($dp_message['txnid']))
				{
					$this->transaction_details
					->where('transaction_code','like',$dp_message['txnid']."%")
					->update(
						array(
							'payment_status' => 'Request Success',
							'reference_transaction' => $dp_message['referenceno'],
							'request_status' => $dp_message['status'],
							'request_message' => $dp_message['message'],
							'request_digest' => $dp_message['digest'],
							'is_validated' => 0
						));

					echo 'result=OK';
				}
				else
				{
					echo 'result=FAIL_DIGEST_MATCH';
				}
			}
			else
			{
				view(config('app.403'));
			}
		}
		else
		{
			view(config('app.403'));
		}
	}

	private function init()
    {
		$stud  = getUserID2();

        return array(
            'academic_year' => $this->student->academic_yearby_student($stud),
		);
	}
	
	private function initializer()
	{
		$this->student = new StudentServices;
		$this->services = new Services;
		$this->transaction_details = new OnlinePaymentRecords();
		$this->transaction_id = new OnlinePaymentTxGenerator();
		$this->charge_records = new CRecords();
		$this->charge_setup = new CSetup();
		$this->studentProfileService = new StudentProfileService;
		$this->portals = new Portals();
	}
}