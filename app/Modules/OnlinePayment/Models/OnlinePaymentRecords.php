<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class OnlinePaymentRecords extends Model {

    protected $table='online_payment_tx_records';

    public $timestamps = false;
    
    protected $primaryKey ='_id';
}