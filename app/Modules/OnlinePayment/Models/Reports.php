<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Reports extends Model {

    protected $table='online_payment_reports';
    
    public $timestamps = false;
    
    protected $primaryKey ='id';

    protected $fillable = array(
        'reports_name',
        'payment_portal',
        'date_from',
        'date_to',
        'collection_for');
}