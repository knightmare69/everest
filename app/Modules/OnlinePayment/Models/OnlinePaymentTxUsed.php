<?php

namespace App\Modules\OnlinePayment\Models;

use illuminate\Database\Eloquent\Model;

class OnlinePaymentTxUsed extends Model
{
    protected $table = 'online_payment_tx_used';
    public $timestamps = false;

    protected $fillable = array(
        'id',
        'or_number',
        'cashier_name'
    );

}