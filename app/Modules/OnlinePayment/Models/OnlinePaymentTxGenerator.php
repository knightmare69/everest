<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class OnlinePaymentTxGenerator extends Model {

    protected $table='online_payment_tx_generator';

    public $timestamps = true;
    
    protected $primaryKey ='_id';

    protected $fillable = array('trasanction_code');

}