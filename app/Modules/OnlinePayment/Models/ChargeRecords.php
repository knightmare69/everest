<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class ChargeRecords extends Model {

    protected $table='online_payment_charge_records';

    public $timestamps = false;
    
    protected $primaryKey ='_id';

    protected $fillable = array(
        'transaction_code',
        'charge_setup',
        'amount');
}