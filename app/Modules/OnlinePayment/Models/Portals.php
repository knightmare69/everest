<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class Portals extends Model {

    protected $table='online_payment_portal';

    public $timestamps = false;
    
    protected $primaryKey ='id';

    protected $fillable = array(
        'name',
        'merchant_name',
        'merchant_password',
        'merchant_test_url');
}