<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class ChargeSetup extends Model {

    protected $table='online_payment_charge_fee';

    public $timestamps = false;
    
    protected $primaryKey ='_id';

}