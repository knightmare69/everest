<?php 

namespace App\Modules\OnlinePayment\Models;
use illuminate\Database\Eloquent\Model;
use DB;

Class PaymentCollected extends Model {

    protected $table='online_payment_collected';

    public $timestamps = false;

    protected $fillable = array(
        'report_id',
        'charge_id'
    );
}