<?php
	Route::group(['prefix' => 'online-payment', 'middleware'=>'auth'], function () {

		Route::get('/', 'OnlinePayment@index');
		Route::post('event', 'OnlinePayment@event');

		Route::group(['prefix' => 'dragonpay', 'middleware'=>'auth'], function () {
			Route::get('/', 'DragonPay@index');
			Route::post('event', 'DragonPay@event');
			Route::get('/message', 'DragonPay@message');
			
		});
	});

	Route::group(['prefix' => 'online-payment'], function () {
		Route::group(['prefix' => 'dragonpay'], function () {
			Route::post('/postback_msg', 'DragonPay@postback_message');
		});
	});
?>
