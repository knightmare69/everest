<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-money"></i> Transaction Records
        </div>
        <div class="tools">
        </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="onlinepayment_rectable">
        <thead>
          <tr>
              <th>Date</th>
              <th>Transaction Code</th>
              <th>Student Name</th>
              <th>Amount</th>
              <th>Payed By</th>
              <th>Payment Status</th>
          </tr>
        </thead>
        <tbody>
          @if(!empty($payments))
            @foreach($payments as $p)
              <tr data-id="{{$p->_id}}">
                <td>{{date('F j, Y', strtotime($p->date_added))}}</td>
                <td>{{$p->transaction_code}}</td>
                <td>{{$p->student_name}}</td>
                <td>&#8369; {{number_format($p->amount,2)}}</td>
                <td>{{$p->user_fullname}}</td>
                <td>{{$p->payment_status}}</td>
              </tr>
            @endforeach
          @endif
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>