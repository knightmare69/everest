<div class="receipt content portlet light">
  <div class="row">
      <div class="receipt col-md-12 col-lg-8">
          <h3 class="title">Payment Information</h3>
          <form role="form">
              <input type="hidden" class="studentid" name="studentid" value="{{getObjectValue($reg,'StudentNo')}}">
              <input type="hidden" class="studentname" name="studentname" value="{{getObjectValue($reg,'Name')}}">
              <div class="form-group">
                <label for="txnid">Transaction ID:</label>
                <input type="text" class="form-control" id="txnid" name="txnid" readonly>
              </div>
              <div class="form-group">
                  <label for="amount">Amount:</label>
                  <input type="text" class="form-control" id="amount" name="amount" readonly>
              </div>
              <div class="form-group">
                  <label for="description">Description:</label>
                  <textarea rows="4" cols="50" class="form-control" id="description" name="description" readonly>
    
                  </textarea>
              </div>
              <div class="form-group">
                  <label for="email">Email:</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="samplemerchant@email.com">
                  <span id="email-error" style="color: red;padding: 0px 5px 5px 7px;display:none;">Please enter valid email address!</span>
                  <span id="email-valid" style="color: #28a745;display:none;padding: 0px 5px 5px 7px;">Email is valid</span>
              </div>
              
              
          </form>
      </div>
      <div class="receipt col-md-12 col-lg-4">
            <h1>Total Payment</h1>
            <h2 class="summary-item">PHP : <span id="total-payment-receipt"> </span></h2>
            <!--For Payment-->
            <div class="summary-item"><span class="text"></span><span class="price"></span></div>
  
            <button type="button" id="submit-payment" class="btn btn-primary btn-lg btn-block" style="margin-top: 50px;" disabled>
                Pay Now
            </button>
          <img src="{{ url("/") .'/assets/global/img/dragon-logo.png' }}"  alt="Photo">
      </div>
  </div>
</div>