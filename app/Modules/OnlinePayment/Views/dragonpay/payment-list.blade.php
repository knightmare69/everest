<?php
    // $scheme = array(
    //     0 => 'Full Payment',
    //     1 => '1st Payment',
    //     2 => '2nd Payment',
    //     3 => '3rd Payment',
    //     4 => '4th Payment',
    //     5 => '5th Payment',
    //     6 => '6th Payment',
    //     7 => '7th Payment',
    //     8 => '8th Payment',
    //     9 => '9th Payment',
    //     10 => '10th Payment',
    // );

    // $total=array(
    //     0 => '0',
    //     1 => '0',
    //     2 => '0',
    //     3 => '0',
    //     4 => '0',
    //     5 => '0',
    //     6 => '0',
    //     7 => '0',
    //     8 => '0',
    //     9 => '0',
    //     10 => '0',
    //     11 => '0'
    // );

    // foreach($sched as $r){
    //     $total[1] = floatval($total[1]) + $r->{'1st Payment'};  //6500
    //     $total[2] = floatval($total[2]) + $r->{'2nd Payment'};  //
    //     $total[3] = floatval($total[3]) + $r->{'3rd Payment'};
    //     $total[4] = floatval($total[4]) + $r->{'4th Payment'};
    //     $total[5] = floatval($total[5]) + $r->{'5th Payment'};
    //     $total[6] = floatval($total[6]) + $r->{'6th Payment'};
    //     $total[7] = floatval($total[7]) + $r->{'7th Payment'};
    //     $total[8] = floatval($total[8]) + $r->{'8th Payment'};
    //     $total[9] = floatval($total[9]) + $r->{'9th Payment'};
    //     $total[10] = floatval($total[10]) + $r->{'10th Payment'};
    //     $total[11] = floatval($total[11]) + $r->{'ActualPayment'};

    //     $full = floatval($r->Debit) - floatval($r->{
    //                  'ActualPayment'});
    //              $total[0] = floatval($total[0]) + $full;
    // }
// ?>

<div class="payment-options">
    @foreach ($sched as $key => $value)
        @if($value > 1)
            @if($key == 0)
            <label class="form-check-label">
                <input type="checkbox" class="payment_list full-payment" id="{{$key}}" name="payment-list[]" value="{{$value}}">
                <span>{{$key}}</span>
                <h4>&#8369; {{number_format($value)}}</h4>
            </label>
            @else
            <label class="form-check-label">
                <input type="checkbox" class="payment_list divided-payment" id="{{$key}}" name="payment-list[]" value="{{$value}}">
                <span>{{$key}}</span>
                <h4>&#8369; {{number_format($value)}}</h4>
            </label>
            @endif
        @elseif($value < 1)
            <label class="form-check-label disable-payment">
                <input type="checkbox" class="payment_list disable-payment" disabled>
                <span>{{$key}}</span>
                <h4>&#8369; {{number_format($value)}}</h4>
            </label>
        @endif
    @endforeach
    <div class="summary">
        <h4>Convenience Fee</h4>
        <h4>&#8369; <span class="convenience-fee" id="convenience-fee">0.00</span></h4>
    </div>
    <hr>
    <div class="summary">
        <h4>Total Payment</h4>
        <h4>&#8369; <span class="price" id="total_payment">0.00</span></h4>
    </div>
</div>


