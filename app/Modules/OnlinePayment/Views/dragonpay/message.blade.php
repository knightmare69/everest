<div class=row>

    <div class="col-md-12 col-sm-12">

        <div id="policy" class="portlet light">

            <div class="portlet-body">
                
                <div class="text-center mb-4" style="margin-top: 30px;">
                    @if($postback["status"] == 'S')
                        <i class="fa fa-check-circle fa-5x text-success"></i>
                        <h3><strong>Success! <br><br></strong>{{ $postback["message"] }}<br><br>You can directly close the browser</h3>
                    @elseif($postback["status"] == 'P')
                        <i class="fa fa-info-circle fa-5x text-info"></i>
                        <h3><strong>Info! <br><br></strong>{{ $postback["message"] }}<br><br>You can directly close the browser</h3>
                    @elseif($postback["status"] == 'F')
                        <i class="fa fa-exclamation-circle fa-5x text-danger"></i>
                        <h3><strong>Warning! <br><br></strong>{{ $postback["message"] }}<br><br>You can directly close the browser</h3>
                    @else
                        <i class="fas fa-exclamation-circle fa-5x text-danger"></i>
                        <h3><strong>"Something went wrong. Please contact the admin of the system"</strong></h3>
                    @endif
                </div>
                
            </div>
        </div>
    </div>
</div>