<div class="row">
    <div class="col-md-12 col-sm-12">
        <div id="policy" class="portlet light">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-money"></i>
                    <span class="caption-subject bold uppercase">Online Payment</span>
                    <span class="caption-helper">use this module to pay your student(s) bill using DragonPay</span>
                </div>
            </div> 
        </div>
    </div>
</div>
<div class="portlet light" id="receipt-payment">
    <ul class="stepper">
        <li class="step step-active">
            @if(getUserGroup() == 'parent')
                <div class="step-title">Select Student</div>
            @else
                <div class="step-title">Select Academic Year</div>
            @endif
        </li>
        <li class="step">
            <div class="step-title">Payment Selection</div>
        </li>
        <li class="step">
            <div class="step-title">Check Receipt</div>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-content-tab tab-active">
            <div class="form-group has-success">
                @if(getUserGroup() == 'parent')
                    <select class="form-control input-sm" name="students" id="students">
                        <option value="" selected disabled> - Select Student - </option>
                            @if(!empty($students))
                                @foreach($students as $r)
                                    <option value="{{ $r->StudentNo }}">{{ $r->LastName.', '.$r->FirstName.' '.$r->MiddleInitial.' '.$r->ExtName }}</option>
                                @endforeach
                            @endif
                    </select>
                @else
                    <select class="form-control input-sm" name="term" id="acad-year-term">
                        <option value="-1"> - Select Current Enrolled Academic Year - </option>
                        @if(!empty($academic_year))
                            @foreach($academic_year as $ayt)            
                                <option value="{{encode($ayt->TermID)}}"> {{ $ayt->AcademicYear . ' ' . $ayt->SchoolTerm  }} </option>
                            @endforeach
                        @endif
                    </select>
                @endif            
            </div>
            <div class="form-actions right">
                @if(getUserGroup() == 'parent')
                    <button type="button" id="step-1-btn" disabled class="btn green mt-ladda-btn ladda-button btn-outline" data-style="slide-right" data-spinner-color="#333">
                        <i class="icon-arrow-right"></i> Next
                    </button>
                @else
                    <input type="hidden" name="current-user" id="current-user" value="{{getUserFullName()}}">
                    <button type="button" id="step-1-btn-student" disabled class="btn green mt-ladda-btn ladda-button btn-outline" data-style="slide-right" data-spinner-color="#333">
                            <i class="icon-arrow-right"></i> Next
                    </button>
                @endif
            </div>
        </div>
        <div class="step-content-tab">
            <div class="form-group has-success">
                <div class="row">
                    <div class="col-md-8">
                        <div class="portlet-body" id="billing">
        
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet box green" id="student-details">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-child"></i> Student Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="form-group">
                                    <label for="student-name">Student Name</label>
                                    <input type="text" disabled class="form-control" id="student-name">
                                </div>
                                <div class="form-group">
                                    <label for="student-number">Student Number</label>
                                    <input type="text" disabled class="form-control" id="student-number">
                                </div>
                                <div class="form-group">
                                    <label for="school-term">School Term</label>
                                    <input type="text" disabled class="form-control" id="school-term">
                                </div>
                                <div class="form-group">
                                    <label for="school-year">School Year</label>
                                    <input type="text" disabled class="form-control" id="school-year">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-action step-content-tab-footer">
                <button type="button" class="btn blue ladda-button btn-outline btn-back" data-style="slide-left" data-spinner-color="#333">
                    <i class="icon-arrow-left"></i> Back
                </button>

                <button type="button" id="submit-online-payment" disabled class="btn green mt-ladda-btn ladda-button btn-outline" data-style="slide-right" data-spinner-color="#333">
                    <i class="icon-arrow-right"></i> Next
                </button>
            </div>
        </div>

        <div class="step-content-tab">
            <div class="form-group has-success">
                <div class="portlet-body" id="transaction">
            
                </div>
            </div>
                
            <div class="form-action">
                <button type="button" class="btn blue ladda-button btn-outline btn-back" data-style="slide-left" data-spinner-color="#333">
                    <i class="icon-arrow-left"></i> Back
                </button>
            </div>
        </div>
    </div>
</div>