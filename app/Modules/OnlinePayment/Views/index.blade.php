<div class="row">
    <div class="col-md-12 col-sm-12">
        <div id="policy" class="portlet light">
            <div class="portlet-body">
                <h3 class="payment-title"><b>Choose a payment method</b> <small>(Click one of the options below)</small> </h3>

                @if(getUserGroup() == 'parent' || getUserGroup() == 'students')
                    <div class="row">
                        <div class="col-md-3">
                            <div class="payment-method">
                                <div id="create-online-payment">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <img class="img-responsive" src="{{ url("/") .'/assets/global/img/dragon-logo.png' }}"  alt="dragonpay">
                                </div>
                            </div>
                            <h5 class="text-center">Pay with Dragonpay</h5>
                        </div>
                        <div class="col-md-3">
                            <div class="payment-method payment-disable">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <img class="img-responsive" src="{{ url("/") .'/assets/global/img/paynamics.png' }}"  alt="paynamics">
                            </div>
                            <h5 class="text-center">Pay with Paynamics <small>(Available Soon)</small></h5>
                        </div>
                        <div class="col-md-3">
                            <div class="payment-method payment-disable">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <img class="img-responsive" src="{{ url("/") .'/assets/global/img/paymaya.webp' }}"  alt="paymaya">
                            </div>
                            <h5 class="text-center">Pay with Paymaya <small>(Available Soon)</small></h5>
                        </div>
                        <div class="col-md-3">
                            <div class="payment-method payment-disable">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <img class="img-responsive" src="{{ url("/") .'/assets/global/img/landbank.png' }}"  alt="landbank">
                            </div>
                            <h5 class="text-center">Pay with Landbank <small>(Available Soon)</small></h5>
                        </div>
                    </div>
                @endif

                @include($views.'table')
            </div>
        </div>
    </div>
</div>