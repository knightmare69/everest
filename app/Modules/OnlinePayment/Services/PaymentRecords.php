<?php

namespace App\Modules\OnlinePayment\Services;

use App\Modules\OnlinePayment\Models\OnlinePaymentRecords as Records;

class PaymentRecords
{
    public function __construct()
    {
        
    }

    public function getRecords()
    {
        return Records::where(
                            array('user_id' => getUserID2()),
                            array('user_group' => getUserGroup()))
                            ->orderBy('date_added','desc')
                            ->get();
    }
}