<!-- BEGIN HEADER INNER -->
<div class="page-header-inner">
	<!-- BEGIN LOGO -->
	<div class="page-logo" style="top: 0px;">
		<a href="{{ url() }}">
		  <img height="50" width="50" style="margin-top: 0px; " src="<?= asset('assets/admin/layout/img/logo.png'); ?>" alt="logo" class="logo-default"/>          
		</a>
        
        <h3 class="autofit pull-left" style="margin-top: 10px !important;">
            <a class="institution-title hidden-phone hidden-xs hidden-sm" href="{{ url() }}">
		     <img height="30" width="300" style="margin-top: 0px; " src="<?= asset('assets/admin/layout/img/logo.old.png'); ?>" alt="logo" class="logo-default"/>          
		    </a>
        </h3>
                                
		<div class="menu-toggler sidebar-toggler hide">
			<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
		</div>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN RESPONSIVE MENU TOGGLER -->
	<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
	</a>
	<!-- END RESPONSIVE MENU TOGGLER -->
	<!-- BEGIN TOP NAVIGATION MENU -->
	<div class="top-menu">
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
			<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="icon-bell"></i>
				<span class="badge badge-default" id="HeaderPendingNotificationTotal">
				0 </span>
				</a>
				<ul class="dropdown-menu">
					<li class="external">
						<h3><span class="bold" id="PendingNotificationTotal">0 pending</span> notifications</h3>
						<a href="javascript:void(0)">view all</a>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" id="HeaderPendingNotification" style="height: 250px;" data-handle-color="#637283">
						</ul>
					</li>
				</ul>
			</li>
			<!-- END NOTIFICATION DROPDOWN -->
			<!-- BEGIN INBOX DROPDOWN -->
			<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
			<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="icon-envelope-open"></i>
				<span class="badge badge-default" id="HeaderUnreadMessagesTotal">
				0 </span>
				</a>
				<ul class="dropdown-menu">
					<li class="external">
						<h3>You have <span class="bold" id="UnreadMessagesTotal">0 New</span> Messages</h3>
						<a href="javascript:void(0)">view all</a>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" id="HeaderUnreadMessages" style="height: 275px;" data-handle-color="#637283">
						</ul>
					</li>
				</ul>
			</li>
			<!-- END INBOX DROPDOWN -->
			<!-- BEGIN TODO DROPDOWN -->
			<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
			<!-- <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="icon-calendar"></i>
				<span class="badge badge-default">
				0 </span>
				</a>
				<ul class="dropdown-menu extended tasks">
					<li class="external">
						<h3>You have <span class="bold">0 pending</span> tasks</h3>
						<a href="page_todo.html">view all</a>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
						</ul>
					</li>
				</ul>
			</li> -->
			<!-- 
					changed by: Lendell Garcellano
					changed date: 7/11/2017 
					Removed Tasks on Header Content as per sir Mikael
			-->

			<!-- END TODO DROPDOWN -->
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
			<li class="dropdown dropdown-user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<img alt="" class="img-circle" src="{{ url('general/getUserPhoto?date='.date('ymdhms')) }}"/>
				<span class="username username-hide-on-mobile">
				{{ ucfirst(strtolower(getUserName())) }} </span>
				<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-default">
					<li>
						<a href="{{ url('profile') }}">
						<i class="icon-user"></i> My Profile </a>
					</li>
					<?php
					if(!isParent()){
					?>
					<li>
						<a href="{{ url('calendar/mycalendar') }}">
						<i class="icon-calendar"></i> My Calendar </a>
					</li>
					<li>
						<a href="{{ url('email') }}">
						<i class="icon-envelope-open"></i> My Inbox </a>
					</li>
					<?php
					}
					?>
					<li class="divider">
					</li>
					<!-- <li>
						<a href="extra_lock.html">
						<i class="icon-lock"></i> Lock Screen </a>
					</li> -->
					<!-- 
					changed by: Lendell Garcellano
					changed date: 7/11/2017 
					Removed Lock Screen on Header Content as per sir Mikael
					-->
					<li>
						<a href="{{ url('logout') }}">
						<i class="icon-key"></i> Log Out </a>
					</li>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
			<!-- BEGIN QUICK SIDEBAR TOGGLER -->
			<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
			<!-- <li class="dropdown dropdown-quick-sidebar-toggler">
				<a href="javascript:;" class="dropdown-toggle"><i class="icon-logout"></i> </a>
			</li> -->
			<!-- END QUICK SIDEBAR TOGGLER -->
		</ul>
	</div>
	<!-- END TOP NAVIGATION MENU -->
</div>
<!-- END HEADER INNER -->