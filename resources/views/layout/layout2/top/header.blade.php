<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="System Base design" name="description">
<meta content="Jeremiah James Samson" name="author">
<meta content="Jose Lendell Nico O. Garcellano" name="author">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>{{ $media['Title']  }}</title>
<?php //<title>{{ getControllerName() ? ucfirst(explode('?',getControllerName())[0]) : 'K12 System'  }}</title>  ?>