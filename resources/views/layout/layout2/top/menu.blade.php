<div class="page-header-menu">
	<div class="container">
		<!-- BEGIN HEADER SEARCH BOX -->
		<form class="search-form" action="?" method="GET">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Search" name="query">
				<span class="input-group-btn">
				<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
				</span>
			</div>
		</form>
		<!-- END HEADER SEARCH BOX -->
		<!-- BEGIN MEGA MENU -->
		<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
		<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
		<div class="hor-menu ">
			<ul class="nav navbar-nav">
				<li class="active">
					<a href="{{ url() }}">Dashboard</a>
				</li>
				<li>
					<a href="{{ url('sales') }}">Sales</a>
				</li>
				<li>
					<a href="{{ url('expenses') }}">Expenses</a>
				</li>
				<li>
					<a href="{{ url('inventory') }}">Inventory</a>
				</li>
				<li>
					<a href="{{ url('stockmaster') }}">StockMaster</a>
				</li>
				<li>
					<a href="{{ url('reports') }}">Reports</a>
				</li>
				<li class="menu-dropdown classic-menu-dropdown ">
					<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Setup <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{ url('setup/branch') }}">
							<i class="icon-briefcase"></i>
							Branch </a>
						</li>
						<li>
							<a href="{{ url('setup/department') }}">
							<i class="icon-wallet"></i>
							Department </a>
						</li>
						<li>
							<a href="{{ url('setup/group') }}">
							<i class="icon-wallet"></i>
							Group </a>
						</li>
						<li>
							<a href="{{ url('setup/level') }}">
							<i class="icon-wallet"></i>
							Level </a>
						</li>
						<li>
							<a href="{{ url('setup/position') }}">
							<i class="icon-wallet"></i>
							Position </a>
						</li>
					</ul>
				</li>

				<li class="menu-dropdown classic-menu-dropdown ">
					<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Security <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{ url('security/users') }}">
							<i class="fa fa-user"></i>
							Users </a>
						</li>
						<li>
							<a href="{{ url('security/access') }}">
							<i class="fa fa-gears"></i>
							Access </a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- END MEGA MENU -->
	</div>
</div>