<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
<link href="<?= asset('assets/global/plugins/font-awesome/css/font-awesome.min.css?t='.date('YmdHis')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css?t='.date('YmdHis')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap/css/bootstrap.min.css?t='.date('YmdHis')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/uniform/css/uniform.default.css?t='.date('YmdHis')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.css?t='.date('YmdHis')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?= asset('assets/global/plugins/bootstrap-datepicker/css/datepicker.css?t='.date('YmdHis')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css?t='.date('YmdHis')); ?>"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<?php

$plugin_css = isset($media['plugin_css']) ? $media['plugin_css'] : array();
$admin_css = isset($media['admin_css']) ? $media['admin_css'] : array();
$css_data = isset($media['css']) ? $media['css'] : array();

if (isset($plugin_css))
{
	if ( count($plugin_css) > 0 )
	{
		foreach($plugin_css as $css)
		{        
		    echo "<link href='".asset('assets/global/plugins/').'/'.$css.".css?t=".date('YmdHis')."' rel='stylesheet' type='text/css'/>";          
		}
	}
}
if (isset($admin_css))
{
	if ( count($admin_css) > 0 )
	{
		foreach($admin_css as $css)
		{        
		    echo "<link href='".asset('assets/admin/pages/css/').'/'.$css.".css?t=".date('YmdHis')."' rel='stylesheet' type='text/css'/>";          
		}
	}
}
if (isset($css_data))
{
	if ( count($css_data) > 0 )
	{
		foreach($css_data as $cs)
		{        
		    echo "<link href='".asset('assets/system/css/').'/'.$cs.".css?t=".date('YmdHis')."' rel='stylesheet' type='text/css'/>";          
		}
	}
}
?>
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="<?= asset('assets/admin/pages/css/tasks.css'); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css'); ?>' stylesheet instead of 'components.css'); ?>' in the below style tag -->
<link href="<?= asset('assets/global/css/components-rounded.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/css/plugins.css?t=20190808'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/layout.css?t=20190808'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/themes/light2.css?t=20190808'); ?>" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?= asset('assets/admin/layout/css/custom.css?t='.date('YmdHis')); ?>" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/system/css/custom.css?t='.date('YmdHis')) }}" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>