<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?= asset('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/excanvas.min.js'); ?>"></script> 
<![endif]-->
<script src="<?= asset('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js'); ?>"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php
$plugin_js = isset($media['plugin_js']) ? $media['plugin_js'] : array();
if ( count($plugin_js) > 0 ){
	foreach($plugin_js as $script){        
	  	echo "<script type='text/javascript' src='" . asset('assets/global/plugins/' . $script .'.js') . "'></script>\n";          
	}
}
?>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= asset('assets/global/scripts/metronic.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/quick-sidebar.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/demo.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/pages/scripts/index.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/pages/scripts/tasks.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN SYSTEM SCRIPTS -->
<script src="<?= asset('assets/system/scripts/general.js?t='.date('YmdHis')); ?>"></script>
<script src="<?= asset('assets/system/scripts/common.js?t='.date('YmdHis')); ?>"></script>
<script src="<?= asset('assets/system/scripts/fn_jquery.js?t='.date('YmdHis')); ?>"></script>
<script src="<?= asset('assets/system/scripts/jquery_helper.js?t='.date('YmdHis')); ?>"></script>
<script src="<?= asset('assets/system/scripts/autoload.js?t='.date('YmdHis')); ?>"></script>
<script src="<?= asset('assets/system/scripts/notification.js?t='.date('YmdHis')); ?>"></script>
<!-- END SYSTEM SCRIPTS -->
<!-- BEGIN PAGE SCRIPTS -->
@include('layout.layout2.bottom.custom_scripts')
<!-- END PAGE SCRIPTS -->