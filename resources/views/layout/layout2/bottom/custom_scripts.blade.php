<?php
$js = isset($media['js']) ? $media['js'] : array();
$admin_js = isset($media['admin_js']) ? $media['admin_js'] : array();
$init = isset($media['init']) ? $media['init'] : array();
if ( count($js) > 0 ){
    foreach($js as $script){
        if($script <> '') {
            $script = strpos($script, '.js') !== false ? $script : $script.'.js';
            echo "<script type='text/javascript' src='" . asset('assets/system/scripts/' . $script) . "?stamp=".rand(0,9999)."'></script>";
        }
    }
}

if ( count($admin_js) > 0 )
{
    foreach($admin_js as $script){
        if($script <> '') echo "<script type='text/javascript' src='" . asset('assets/admin/pages/scripts/' . $script.'.js' ) ."?stamp=".rand(0,9999). "'></script>";
    }
}
?>
<script>
jQuery(document).ready(function() {
	<?php
        if ( count($init) > 0 )
        {
		  	foreach($init as $fn){
  		  		echo $fn.';';
            }
	 	}
	?>
});
</script>
