<div class="page-footer">
	<div class="page-footer-inner">
		 2017 &copy; Prince Technologies Corporation. ver. 1.0.72
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
@include('extras.modals')