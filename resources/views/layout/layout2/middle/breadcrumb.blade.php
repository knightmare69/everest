<div class="page-bar">
	<?php
		$url =  str_replace(url(),'','http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$url = explode('/',$url);
	?>
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url() }}">Home</a>
		</li>
		@for($i=0;$i<count($url);$i++)
			@if ($i <> (count($url)-1) )
			<li>
				<a href="#">{{ ucfirst($url[$i]) }}
					<i class="fa fa-angle-right"></i>
				</a>
			</li>
			@else
			<li>
				<a href="#">{{ explode('?',ucfirst($url[$i]))[0] }}</a>
			</li>
			@endif
		@endfor
	</ul>
	<div class="page-toolbar ">
		<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height grey-salt">			
			<span class="thin uppercase visible-lg-inline-block"> <i class="fa fa-cogs fa-fw"></i> </span>			
		</div>
	</div>
</div>