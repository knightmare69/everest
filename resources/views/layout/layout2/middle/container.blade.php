<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
@include('layout.layout2.top.header')
@include('layout.layout2.top.css')
<link rel="shortcut icon" href="{{ url('favicon.ico') }}">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  
page-quick-sidebar-over-content page-style-square page-boxed page-header-fixed page-sidebar-fixed
page-header-fixed page-quick-sidebar-over-content page-sidebar-fixed page-sidebar-closed-hide-logo

page-header-fixed page-quick-sidebar-over-content page-sidebar-closed

-->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square {{ isset($media['closeSidebar']) ? 'page-sidebar-closed'  : '' }}">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER TOP -->
	@include('layout.layout2.top.header_content')
	<!-- END HEADER TOP -->
</div>
<!-- END HEADER -->

<div class="clearfix">
</div>

<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
    <?php if(isParentOrStudents()){  
	         if(getSessionData('inquired')!=1 || chkAppEntry()!=false){?>
                @include('layout.layout2.left.parent') 
    <?php
	         }
	      }else{ ?>
	         @include('layout.layout2.left.sidebar')
    <?php } ?>
    
	<!-- END SIDEBAR -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content <?php echo (getSessionData('inquired')==1 && chkAppEntry()==false)?' no-sidebar':'';?>">
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			@include('layout.layout2.middle.page_header')
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT INNER -->
			<?php echo isset($content) ? $content : ''; ?>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
	@include('layout.layout2.left.quick_sidebar')
</div>
<!-- END PAGE CONTAINER -->

<!-- BEGIN PRE-FOOTER -->
@include('layout.layout2.bottom.footer')
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN LOAD JQUERY LIBRARY -->
<script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js') }} before bootstrap.min.js') }} to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}" type="text/javascript"></script>
<!-- END LOAD JQUERY LIBRARY -->

<!-- BEGIN AUTOLOAD VAR -->
<script>
var base_url, page_url, form, user_group;
jQuery(document).ready(function() {    
  	base_url = "<?php echo url(); ?>/";
  	page_url = "<?php echo isset($url['page']) ? $url['page'] : ''; ?>";
  	form = "<?php echo isset($url['form']) ? $url['form'] : '';?>";
  	user_group = "<?php echo strtolower(getUserGroup()); ?>";
  	is_parent = "<?php echo isParent(); ?>";
});
</script>
<!-- END AUTOLOAD VAR -->

<!--BEGIN SCRIPTS -->
@include('layout.layout2.bottom.scripts')
<!--ENDS SCRIPTS -->

<script>
jQuery(document).ready(function() {    
   	Metronic.init(); // init metronic core componets
   	Layout.init(); // init layout
   	QuickSidebar.init(); // init quick sidebar
    Demo.init(); // init demo features 
   	Index.init();   
   	Index.initDashboardDaterange();
   	Index.initJQVMAP(); // init index page's custom scripts
  //Index.initCalendar(); // init index page's custom scripts
   	Index.initCharts(); // init index page's custom scripts
   	Index.initChat();
   	Index.initMiniCharts();
   	Tasks.initDashboardWidget();   	
});
</script>
<!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>