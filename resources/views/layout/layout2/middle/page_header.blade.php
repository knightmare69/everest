<h3 class="page-title hide">
{{ isset($media['Title']) ? ucfirst($media['Title']) : '' }} <small>{{ isset($media['Description']) ? ucfirst($media['Description']) : '' }}</small>
</h3>
@include('layout.layout2.middle.breadcrumb')