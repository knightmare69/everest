<?php
    function isActive(array $page,$prefix = '') {
        $controller = explode('?',str_replace(url(),'',getUrlSegment()))[0];
        foreach($page as $key => $value){
            if ( $controller === $prefix.strtolower($value) ) return 'active ';
        }
        return '';
    }
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu {{ isset($media['closeSidebar']) ? 'page-sidebar-menu-closed'  : '' }}" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			<li class="sidebar-search-wrapper">
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
				<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<form class="sidebar-search hide" action="?" method="POST">
					<a href="javascript:;" class="remove">
					<i class="icon-close"></i>
					</a>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
						<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
						</span>
					</div>
				</form>
				<!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
            <br />

            @if(!isParent())
			<li class="start {{ isActive(array('')) }}">
				<a href="{{ url() }}"> <i class="icon-home"></i> <span class="title">Dashboard</span> <span class="selected"></span></a>
			</li>
			@endif
            <li class="start">
				<a href="javascript:void(0);"> <i class="icon-calendar"></i> <span class="title">Calendar</span> <span class="arrow"></span></a>
				<ul class="sub-menu">
					<li class="{{ isActive(array('school'),'calendar/') }}"><a href="{{ url('calendar/school') }}"><i class="icon-calendar"></i> School Calendar</a></li>
					<li class="{{ isActive(array('personal'),'calendar/') }}"><a href="{{ url('calendar/mycalendar') }}"><i class="icon-calendar"></i> My Calendar</a></li>
					<li class="{{ isActive(array('room'),'calendar/') }}"><a href="{{ url('calendar/room') }}"><i class="icon-calendar"></i> Room Reservation</a></li>
					<li class="{{ isActive(array('bulletin'),'calendar/') }} hidden"><a href="{{ url('calendar/bulletin') }}"><i class="icon-calendar"></i> Bulletin</a></li>
				</ul>
			</li>
			
            @if(isPermissionHas('guardian','read') && isParent() )
			<li class="{{ isActive(array('guardian')) }}">
				<a href="{{ url('guardian') }}">
					<i class="icon-wallet"></i>
					<span class="title">My Profile</span>
					<span class=""></span>
				</a>
			</li>
			<li class="{{ isActive(array('guardian-connect')) }}">
				<a href="javascript:;" id="GuardianConnect">
					<i class="icon-wallet"></i>
					<span class="title">Guardian Connect</span>
					<span class=""></span>
				</a>
			</li>
			@endif

			<?php
				$apply = isPermissionHas('admission-apply','read');
				$quick = isPermissionHas('admission-quick','read');
				$listing = isPermissionHas('admission-listing','read');
				$approval = isPermissionHas('admission-approval','read');
				$panels = isPermissionHas('admission-panels','read');
				$tour = isPermissionHas('admission-tour','read');
			?>

            @if($apply || $listing || $panels || $tour)
			<li class="{{ isActive(array('apply','edit','listing','testing','medical','quick','schedule/inquiry','tour/schedule','exam/schedule'),'admission/') }}">
				<a href="javascript:void(0)">
				    <i class="fa fa-ticket"></i>
				    <span class="title">Admission</span>
				    <span class="arrow "></span>
				</a>
				@if($apply || $listing || $panels || $tour)
				<ul class="sub-menu">
					@if($apply && isPermissionHas('admission-apply','add'))
					<li class="{{ isActive(array('apply'),'admission/') }}">
						<a href="{{ url('admission/redirecToApply') }}"><i class="icon-wallet"></i> Create Application </a>
					</li>
					@endif

					@if($quick && isPermissionHas('admission-quick','add'))
					<li class="{{ isActive(array('quick'),'admission/') }}">
						<a href="{{ url('admission/quick') }}"> <i class="icon-wallet"></i> Quick Application </a>
					</li>
					@endif

                    @if($apply && isPermissionHas('admission-apply','add'))
					<li class="{{ isActive(array('reservation'),'admission/') }}">
						<a href="{{ url('admission/reservation') }}"> <i class="fa fa-user"></i> Reservation </a>
					</li>
					@endif
                    
                    @if(!isParent())
    					@if($listing)
    					<li class="{{ isActive(array('listing'),'admission/') }}">
    						<a href="{{ url('admission/listing') }}">
    						<i class="icon-wallet"></i>
    						Application List </a>
    					</li>
    					@endif
                    
                    @if(isPermissionHas('examinee-testing','read'))   					
					<li class="{{ isActive(array('testing'),'examinees/') }}">
						<a href="{{ url('examinees/testing') }}">
						<i class="fa fa fa-flask"></i>
						Examinees for Testing </a>
					</li>
    				@endif
                    @if(isPermissionHas('examinee-medical','read')) 
					<li class="{{ isActive(array('medical'),'examinees/') }}">
						<a href="{{ url('examinees/medical') }}">
						<i class="fa fa-ambulance"></i>
						Examinees for Medical </a>
					</li>
    				@endif
					@endif
                   
                    @if(isPermissionHas('admission-apply','add') || isPermissionHas('admission-quick','add'))
                    <li>
        				<a href="javascript:;"> <i class="fa fa-calendar"></i> <span class="title">Inquiry</span> <span class="arrow "></span></a>
        				<ul class="sub-menu">
        					<li class="{{ isActive(array('schedule/inquiry'),'admission/') }}">
								<a href="{{ url('admission/schedule/inquiry') }}"> <i class="icon-wallet"></i> Walk-in Inquiry</a>
							</li>
							<li class="{{ isActive(array('schedule/inquiry/list'),'admission/') }}">
								<a href="{{ url('admission/schedule/inquiry/list') }}"> <i class="icon-wallet"></i> Inquiry List</a>
							</li>
        				</ul>
        		    </li>
        		    @endif
                    <li>
        				<a href="javascript:;"> <i class="fa fa-calendar"></i> <span class="title">Schedules</span> <span class="arrow "></span></a>
        				<ul class="sub-menu">
        					<li class="{{ isActive(array('setup/testing')) }}">
			                     <a href="{{ url('setup/testing') }}"><i class="icon-calendar"></i> Testing  </a>
                            </li>
					        <li class="{{ isActive(array('setup/medical')) }}">
                                <a href="{{ url('setup/medical') }}"><i class="fa fa-medkit"></i> Medical  </a>
        					</li>
        					<li class="{{ isActive(array('tour/schedule'),'admission/') }}">
								<a href="{{ url('admission/tour/schedule') }}"> <i class="icon-wallet"></i> Tour/Interview</a>
							</li>
							<li class="{{ isActive(array('exam/schedule'),'admission/') }}">
								<a href="{{ url('admission/exam/schedule') }}"> <i class="icon-wallet"></i> Exam Schedule</a>
							</li>
        				</ul>
        			</li>
        			@if($panels)
        			<li class="{{ isActive(array('panels'),'admission/') }}">
						<a href="{{ url('admission/panels') }}">
						<i class="fa fa fa-flask"></i>
						Admissions Commitee </a>
					</li>
					@endif
					@if(isPermissionHas('admission-listing','print') || isPermissionHas('admission-listing','export'))
					<li>
        				<a href="{{ url('admission/analytics') }}"> <i class="fa fa-bar-chart-o"></i> <span class="title">Analytics</span></a>
        		    </li>
        		    @endif
				</ul>
				@endif
			</li>
			@endif
            
			@if( !isParent() )
			<li class="">
				<a href="javascript:;"><i class="fa fa-file-text"></i> <span class="title">Accounting</span><span class="arrow "></span></a>
				<ul class="sub-menu">
				    @if(isPermissionHas('accounts-chart','read'))
                	<li><a href="{{ url('accounting\accounts-chart') }}"><i class="fa fa-table"></i> Chart Of Accounts </a></li>
				    @endif

                    @if(isPermissionHas('feestemplate','read'))
                    <li><a href="{{ url('accounting/fees-template') }}"><i class="fa fa-list"></i> Fees Setup </a></li>
                    @endif

                    @if(isPermissionHas('subject-with-fee','read'))
                    <li><a href="{{ url('accounting/subject-with-fee') }}"><i class="fa fa-list"></i> Subject with Fees </a></li>
                    @endif

                    @if(isPermissionHas('due-date-setup','read'))
                    <li><a href="{{ url('accounting/due-date-setup') }}"><i class="fa fa-calendar"></i> Due Date Setup </a></li>
                    @endif
					@if(isPermissionHas('accounting','read'))
                	<li><a href="{{ url('accounting') }}"><i class="fa fa-table"></i> Assessment/Billing </a></li>
				    @endif
				    
				    @if(isPermissionHas('account-ledger','read'))
                	<li><a href="{{ url('accounting\studentledger') }}"><i class="fa fa-table"></i> Student Legder </a></li>
				    @endif
				    
				    
				    @if(isPermissionHas('account-ledger','read'))
                   	<li class="">
        				<a href="javascript:;"><i class="fa fa-money"></i> <span class="title">Debit/Credit Memo </span><span class="arrow "></span></a>
        				<ul class="sub-menu">

                        	<li><a href="{{ url('accounting/memo/create') }}"><i class="fa fa-file"></i> New Entry </a></li>
                            <li><a href="{{ url('accounting/memo') }}"><i class="fa fa-list"></i> List of Transaction/s </a></li>
                        </ul>
        			</li>
        			
        			<li><a href="{{ url('accounting\reports') }}"><i class="fa fa-print"></i> Reports </a></li>
				    @endif
                </ul>
			</li>
		    @endif

            @if( !isParent() && isPermissionHas('cashiering','read'))
            <!-- Cashier -->
			<li class="">
				<a href="javascript:;"><i class="fa fa-money"></i> <span class="title">Cashier</span><span class="arrow "></span></a>
				<ul class="sub-menu">

                    @if(isPermissionHas('cashiering','read'))
                	<li><a href="{{ url('cashier/cashiering') }}"><i class="fa fa-money"></i> Cashiering </a></li>
                	@endif

                    @if(isPermissionHas('official-receipts','read'))
                    <li><a href="{{ url('cashier/official-receipts') }}"><i class="fa fa-list"></i> Official Receipts </a></li>
                    @endif
					
                    @if(isPermissionHas('cashiering','read'))
                	<li><a href="{{ url('cashier/online-transactions') }}"><i class="fa fa-th-list"></i> Online Transactions </a></li>
                	@endif

                </ul>
			</li>
		    @endif

            @if( !isParent() ) <?php  //&& isPermissionHas('registrar','read')  ?>
            <li class="">
				<a href="javascript:;"><i class="fa fa-file-text"></i> <span class="title">Registrar</span><span class="arrow "></span></a>
				<ul class="sub-menu">

                    @if(isPermissionHas('student-profile','read'))
					<li><a href="{{ url('students') }}"><i class="icon-users"></i> Student Profile </a></li>
                    @endif

                    @if(isPermissionHas('registrar-subjects','read'))
					<li><a href="{{ url('registrar/subjects') }}"><i class="fa fa-book"></i> Subjects </a></li>
                    @endif

                    @if(isPermissionHas('registrar-curriculum','read'))
					<li><a href="{{ url('registrar/curriculum') }}"><i class="fa fa-th-list "></i> Curriculum </a></li>
                    @endif

                    @if(isPermissionHas('transcript','read'))
					<li><a href="{{ url('registrar/transcript') }}"><i class="fa fa-graduation-cap "></i> Transcript of Record </a></li>
                    @endif

                    @if(isPermissionHas('correction-of-grades','read'))
					<li><a href="{{ url('registrar/correction-of-grades') }}"><i class="fa fa-star "></i> Correction of Grades </a></li>
                    @endif

                    @if(isPermissionHas('grading-sheet-inventory','read'))
                    <li><a href="{{ url('registrar/grading-sheet-inventory') }}"><i class="fa fa-bar-chart-o"></i> <span class="font-xs"> Inventory of Grading Sheet </span></a></li>
                    @endif

                    @if(isPermissionHas('school-policy','read'))
					<li><a href="{{ url('registrar/school-policy') }}"><i class="fa fa-th-list "></i> School Policy  </a></li>
                    @endif

                    @if(isPermissionHas('student-accountability','read'))
					<li><a href="{{ url('registrar/accountability') }}"><i class="fa fa-th-list "></i> Accountability  </a></li>
                    @endif

                    @if(isPermissionHas('request-report-card','read'))
					<li class="" ><a href="{{ url('registrar/request-report-card') }}" class=""><i class="fa fa-download "></i> <span class="font-xs">Request for Report Card </span> </a></li>
                    @endif
                    
                    @if(isPermissionHas('report-card','read'))
					<li><a href="{{ url('registrar/report-card') }}"><i class="fa fa-book "></i> Reports </a></li>
                    @endif

				</ul>
			</li>
			@endif

            <!-- Enrollment -->
			<li class="{{ isActive(array('enrollment','enrollment/children','enrollment/registered','enrollment/sectioning')) }}">
				<a href="javascript:;"><i class="fa fa-child"></i> <span class="title">Enrollment</span><span class="arrow "></span></a>
				<ul class="sub-menu">

                    @if(isPermissionHas('promotions','read'))
					<li class="{{ isActive(array('promotions')) }}" data-permission="{{isPermissionHas('promotions','read')}}">
						<a href="{{ url('enrollment/promotions') }}"><i class="fa fa-graduation-cap"></i> Student Promotions </a>
					</li>
					@endif

					@if(isPermissionHas('enrollment','read'))
					<li class="{{ isActive(array('enrollment')) }}" data-permission="{{isPermissionHas('enrollment','read')}}">
						<a href="{{ url('enrollment') }}"><i class="fa fa-file"></i> Enrollment Proper </a>
					</li>
					@endif

					@if(isPermissionHas('enrollment-child','read'))
					<li class="{{ isActive(array('enrollment/children')) }} hidden"><a href="{{ url('enrollment/children') }}"><i class="fa fa-users"></i> Children Enrollment </a></li>
					@endif
                    
                    @if(isPermissionHas('enrollment','read'))
					<li class="{{ isActive(array('enrollment')) }}">
                        <a href="{{ url('enrollment/advising') }}"><i class="fa fa-table"></i> Advising </a>
                    </li>
					@endif

                    @if(isPermissionHas('addropchange','read'))
					<li class="{{ isActive(array('addropchange')) }}">
                        <a href="{{ url('add-drop-change') }}"><i class="fa fa-cubes"></i> Add/Drop/Change </a>
                    </li>
					@endif

					@if(isPermissionHas('enrollment-registered','read'))
					<li class="{{ isActive(array('enrollment/registered')) }}">
						<a href="{{ url('enrollment/registered') }}"> <i class="fa fa-table"></i> List of Registered</a>
					</li>
					@endif

					@if(isPermissionHas('enrollment-registered','read'))
					<li class="{{ isActive(array('enrollment/sectioning')) }}">
						<a href="{{ url('enrollment/sectioning') }}"><i class="fa fa-users"></i>Class Sectioning </a>
					</li>
					@endif
					@if(isPermissionHas('enrollment-registered','read'))
					<li class="{{ isActive(array('manual-class')) }}">
						<a href="{{ url('manual-class') }}"><i class="fa fa-calendar"></i>Manual Sectioning </a>
					</li>
					@endif
				</ul>
			</li>

			@if(!isParentOrStudents())
            <!-- Academics -->
            <li>
				<a href="javascript:;"><i class="fa fa-building"></i><span class="title">Academics</span> <span class="arrow "></span></a>
				<ul class="sub-menu">
                    @if(isPermissionHas('faculty-assignment','read'))
                    <li><a href="{{ url('academics/faculty-assignment') }}"><i class="fa fa-pencil"></i> Faculty Assigments </a></li>
                    @endif
                    @if(isPermissionHas('club-organization','read'))
   	                <li><a href="{{ url('academics/club-organization') }}"><i class="fa fa-sitemap"></i> Club/Organization </a></li>
                    @endif

                    @if(isPermissionHas('conduct-setup','read'))
   	                <li><a href="{{ url('academics/conduct-setup') }}"><i class="fa fa-meh-o"></i> Conduct </a></li>
                    @endif
					
                    @if(isPermissionHas('summary-of-grades','read'))
                    <li><a href="{{ url('academics/summary-of-grades') }}"><i class="fa fa-calculator"></i> Summary of Grades </a></li>
                    @endif
					
                    @if(isPermissionHas('faculty-management','read'))
   	                <li><a href="{{ url('academics/faculty-management') }}"><i class="fa fa-user-md"></i> Faculty Management </a></li>
                    @endif
                    
                    @if(isPermissionHas('academic-setup','read'))
					<li><a href="{{ url('academics/setup') }}"><i class="fa fa-wrench "></i> Academic Setup </a></li>
                    @endif

                    @if(isPermissionHas('academic-reports','read'))
					<li><a href="{{ url('academics/reports') }}"><i class="fa fa-book "></i> List of reports </a></li>
                    @endif

				</ul>
			</li>

            <li>
				<a href="javascript:;"><i class="fa fa-superscript"></i><span class="title">Class Record</span> <span class="arrow "></span></a>
				<ul class="sub-menu">

                    @if(isPermissionHas('schedules','read'))
					<li><a href="{{ url('class-record/schedules') }}"> <i class="fa fa-calendar"></i> My Schedules </a></li>
                    @endif

                    @if(isPermissionHas('eclass-records','read'))
					<li><a href="{{ url('class-record/eclass-records') }}"><i class="fa fa-star"></i> eClass Records</a></li>
                    @endif
                    @if(isPermissionHas('enarrative','read'))
					<li><a href="{{ url('class-record/enarrative') }}"><i class="fa fa-star"></i> Conduct and Narratives</a></li>
                    @endif

                    @if(isPermissionHas('checklist','read'))
					<li><a href="{{ url('class-record/checklist') }}"><i class="fa fa-check-square-o"></i> Kindergarten</a></li>
					@endif
					
                    @if(isPermissionHas('attendance','read'))
    	            <li><a href="{{ url('class-record/student-attendance') }}"> <i class="fa fa-book"></i> Attendance</a></li>
                    <li><a href="{{ url('class-record/discipline') }}"> <i class="fa fa-meh-o"></i> Discipline\Offenses</a></li>
                    @endif

                    <!-- @if(isPermissionHas('student-conduct','read'))
                    <li><a href="{{ url('class-record/student-conduct') }}"> <i class="fa fa-meh-o "></i> Student Conduct </a></li>
	
                    Requested to be hidden by: Cristina Suntay
                    Email date: 9/27/2017

                    @endif -->

                    @if(isPermissionHas('student-clubs','read'))
                    <li><a href="{{ url('class-record/student-clubs') }}"> <i class="fa fa-users "></i> Student Clubs </a></li>
                    @endif

                    @if(isPermissionHas('review-records','read'))
					<li><a href="{{ url('class-record/review') }}"><i class="fa fa-star"></i>  Reviewer</a></li>
                    @endif

                    @if(isPermissionHas('remedial','read'))
					<li><a href="{{ url('class-record/remedial') }}"><i class="fa fa-gavel "></i> Remedial</a></li>
                    @endif

                    @if(isPermissionHas('mentoring','read'))
					<li><a href="{{ url('class-record/mentoring') }}"><i class="fa fa-user "></i> Mentoring</a></li>
                    @endif

                    @if(isPermissionHas('subject-formations','read'))
                    <li><a href="{{ url('class-record/subject-formations') }}"> <i class="fa fa-sliders"></i> Subject Formations </a></li>
                    @endif

				</ul>
			</li>
            <!-- Added : ARS 201606081316H -->
            <li>
				<a href="javascript:;"> <i class="fa fa-puzzle-piece"></i><span class="title">Grading System</span><span class="arrow "></span></a>
				<ul class="sub-menu">

                    @if(isPermissionHas('grading-system-setup','read'))
                    <li><a href="{{ url('setup/grade-oldsetup') }}"><i class="fa fa-wrench"></i> Grading System Setup </a></li>
                    <li><a href="{{ url('setup/grade-setup') }}"><i class="fa fa-wrench"></i> GS Setup  Per Level</a></li>
                    @endif

                    @if(isPermissionHas('grading-system-areas','read'))
                    <li><a href="{{ url('grading-system/areas') }}"><i class="fa fa-list"></i> Subject Areas </a></li>
                    @endif

                    @if(isPermissionHas('policy-setup','read'))
                    <li><a href="{{ url('grading-system/policy-setup') }}"> <i class="fa fa-cog"></i> Policy Setup</a></li>
                    @endif
                    @if(isPermissionHas('grading-system-components','read'))
                    <li><a href="{{ url('grading-system/components') }}"> <i class="fa fa-cubes"></i> Components </a></li>
                    @endif
                    @if(isPermissionHas('grade-points','read'))
					<li><a href="{{ url('grading-system/grade-points') }}"> <i class="fa fa-trophy"></i> Grade Points </a></li>
                    @endif

                    @if(isPermissionHas('transmutation','read'))
                    <li><a href="{{ url('grading-system/transmutation') }}"> <i class="fa fa-filter"></i> Transmutation </a></li>
                    @endif
				</ul>
			</li>
            @if(!isParent())
			<li class="{{ isActive(array('reports')) }}">
				<a href="{{ url('reports') }}"> <i class="fa fa-print"></i> <span class="title">Reports</span> <span class=""></span></a>
			</li>
			@endif
			<li>
				<a href="javascript:;"><i class="icon-wrench"></i> <span class="title">Setup Manager</span> <span class="arrow "></span></a>
				<ul class="sub-menu">
                    @if(isPermissionHas('academic-yearterm','read'))
                    <li><a href="{{ url('setup/academic-yearterm') }}"><i class="fa fa-edit"></i> Academic Year &amp; Term </a></li>
                    @endif

				    @if(isPermissionHas('colleges','read'))
                    <li><a href="{{ url('setup/college') }}"><i class="fa fa-building"></i> College </a></li>
                    @endif

                    @if(isPermissionHas('department','read'))
					<li><a href="{{ url('setup/department') }}"><i class="fa fa-building"></i> Department </a></li>
                    @endif
                    @if(isPermissionHas('position','read'))
					<li><a href="{{ url('setup/position') }}"><i class="fa fa-user-md"></i> Position Title</a></li>
                    @endif

				    @if(isPermissionHas('programs','read'))
                    <li><a href="{{ url('setup/programs') }}"><i class="fa fa-edit"></i> Programs/Class/Major </a></li>
                    @endif
					
				    @if(isPermissionHas('rooms-buildings','read'))
                    <li><a href="{{ url('setup/rooms-buildings') }}"><i class="fa fa-building"></i> Buildings And Rooms </a></li>
                    @endif

				    @if(isPermissionHas('regconfig','read'))
                    <li><a href="{{ url('setup/regconfig') }}"><i class="fa fa-cog"></i> Reg. Configuration </a></li>
                    @endif
				</ul>
			</li>
			<li>
				<a href="javascript:;"><i class="icon-lock"></i><span class="title">Security</span><span class="arrow "></span></a>
				<ul class="sub-menu">
                    @if(isPermissionHas('users','read'))
					<li><a href="{{ url('security/users') }}"><i class="fa fa-user"></i> Users </a></li>
                    @endif

                    @if(isPermissionHas('group','read'))
					<li><a href="{{ url('setup/group') }}"><i class="fa fa-users"></i> User Groups </a></li>
                    @endif

                    @if(isPermissionHas('access','read'))
					<li><a href="{{ url('security/access') }}"><i class="fa fa-gears"></i> Access Rights </a></li>
                    @endif

                    @if(isPermissionHas('pswd-config','read'))
                    <li><a href="{{ url('security/password') }}"> <i class="fa fa-lock"></i> Password Config </a></li>
                    @endif

                    @if(isPermissionHas('audit-trails','read'))
                    <li><a href="{{ url('security/logs') }}"><i class="fa fa-history"></i> Audit Trails </a></li>
                    @endif
				</ul>
			</li>
			@endif
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
