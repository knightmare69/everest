<?php
    function isActive(array $page,$prefix = '') {
        $controller = explode('?',str_replace(url(),'',getUrlSegment()))[0];
        foreach($page as $key => $value){
            if ( $controller === $prefix.strtolower($value) ) return 'active ';
        }
        return '';
    }
?><div class="page-sidebar-wrapper <?php echo (getSessionData('inquired')==1 && count(chkAppEntry())<=0)?'hide':'';?>">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu {{ isset($media['closeSidebar']) ? 'page-sidebar-menu-closed'  : '' }}" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			<li class="sidebar-search-wrapper">
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
				<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<form class="sidebar-search hide" action="?" method="POST">
					<a href="javascript:;" class="remove">
					<i class="icon-close"></i>
					</a>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
						<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
						</span>
					</div>
				</form>
				<!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
            <br />			
            
			<li class="start {{ isActive(array('')) }} <?php echo (getSessionData('inquired')==1)?'hide':'';?>">
				<a href="{{ url() }}"> <i class="icon-home"></i> <span class="title">Dashboard</span> <span class="selected"></span></a>
			</li>
			<li class="start <?php echo (getSessionData('inquired')==1)?'hide':'';?>">
				<a href="{{ url('calendar') }}"> <i class="icon-calendar"></i> <span class="title">Calendar</span> <span class="arrow"></span></a>
				<ul class="sub-menu">
					<li class="{{ isActive(array('school'),'calendar/') }}"><a href="{{ url('calendar/school') }}"><i class="icon-calendar"></i> School Calendar</a></li>
				</ul>
			</li>               
			
            @if( !isStudents() )                        
            <li class="{{ isActive(array('guardian')) }}">
				<a href="{{ url('guardian') }}">
					<i class="icon-user"></i>
					<span class="title">My Profile</span>
					<span class=""></span>
				</a>
			</li>
            @endif
			
			<li class="hidden">
			    <a href="{{ url('students') }}"><i class="icon-users"></i> Student Profile </a>
			</li>
			
            @if(isPermissionHas('inbox','read'))
            <li class="">
				<a href="{{ url('profile/inbox') }}"> <i class="fa fa-envelope-o"></i> <span class="title">Inbox</span></a>
			</li>
            @endif
            
            @if( isStudents() &&  getUserID2() != '' || getUserGroup() == 'parent' )            
            
             
              <li class="{{ isActive(array('students','students/evaluation', 'students/counseling-student','students/medical-student','students/grades','students/attendance','students/meritdemerits')) }}  <?php echo (getSessionData('inquired')==1)?'hide':'';?>">
                <a href="javascript:;"><i class="fa fa-user"></i> <span class="title">Student</span><span class="arrow "></span></a>
                <ul class="sub-menu">
				  <li class="hidden">
					<a href="{{url('students/subjects')}}"> <i class="fa fa-list"></i> <span class="title">Subjects Enrolled</span></a>
				  </li>
                  <li class="">
                    <a href="{{url('students/grades')}}"> <i class="fa fa-table"></i> <span class="title">Grades</span></a>
				  </li>
				  
				  <li class="hidden">
                    <a href="{{url('students/evaluation')}}"> <i class="fa fa-table"></i> <span class="title">Evaluation</span></a>
                  </li>
                  <li class="hidden">
                    <a href="{{url('students/medical-student')}}"> <i class="icon-heart"></i> <span class="title">Medical</span></a>
                  </li>
                  <li class="hidden">
                    <a href="{{url('students/counseling-student')}}"> <i class="icon-info"></i> <span class="title">Counseling</span></a>
                  </li>
                  <li class="hidden">
                    <a href="{{url('students/dtr')}}"> <i class="icon-calendar"></i> <span class="title">DTR</span></a>
                  </li>
                  <li class="hidden">
                    <a href="{{url('students/attendance')}}"> <i class="fa fa-book"></i> <span class="title">Attendance & Discipline</span></a>
                  </li>
                </ul>
			  </li>
            
            @endif
                           			            
			<?php
				$quick   = 0;//isPermissionHas('admission-quick','read');
				$apply   = isPermissionHas('admission-apply','read');
				$listing = isPermissionHas('admission-listing','read');
			?>
            @if($apply || $listing)
			<li class="">
				<a href="javascript:void(0)">
				<i class="fa fa-ticket"></i>
				<span class="title">Admission</span>
				<span class="arrow "></span>
				</a>
				@if($apply || $listing)
				<ul class="sub-menu" data-apply="{{$apply}}" data-list="{{$listing}}">
					@if($apply)
					<li class="{{ isActive(array('apply'),'admission/') }}">
						<a href="{{ url('admission/redirecToApply') }}">
						<i class="icon-wallet"></i>
						Create Application </a>
					</li>
					@endif
                    
					@if($quick)
					<li class="{{ isActive(array('quick'),'admission/') }}">
						<a href="{{ url('admission/quick') }}">
						<i class="icon-wallet"></i>
						Quick Application </a>
					</li>
					@endif
                
                	<li class="{{ isActive(array('listing'),'admission/') }}">
						<a href="{{ url('admission/listing') }}">
						<i class="icon-wallet"></i>
						Application List </a>
					</li>
					
				</ul>
				@endif
			</li>
			@endif
            
			@if(isPermissionHas('enrollment-child','read') && isPermissionHas('enrollment','read')==false)
			<li class="{{ isActive(array('enrollment/children')) }} <?php echo (getSessionData('inquired')==1)?'hide':'';?>">
				<a href="{{ url('enrollment/children') }}">
				<i class="fa fa-users"></i>
				<span class="title">Enrollment
				<span class=""></span></span></a>
			</li>
			<li class="<?php echo (getSessionData('inquired')==1)?'hide':'';?>">
				<a href="{{ url('accounting/soaccounts') }}">
				<i class="fa fa-money"></i>
				<span class="title">Online Payment
				<span class=""></span></span></a>
			</li>
			@endif
			
            <li class="hidden">
				<a href="{{url('students/grades')}}"> <i class="fa fa-table"></i> <span class="title">Student Grades</span></a>
			</li>
            <li class="hidden">
				<a href="{{url('students/ledger')}}"> <i class="fa fa-list"></i> <span class="title">Student Ledger</span></a>
			</li>
            
            @if( !isStudents() )            
			<li class="hidden"><a href="{{ url('enrollment/children') }}"><i class="fa fa-folder-o "></i> Enrollment </a></li>
            <li class="hidden">
				<a href="{{url('students/enrollment')}}"> <i class="fa fa-child"></i> <span class="title">Enrollment</span></a>
			</li>
            @endif     
            
			<li class="hidden">
				<a href="{{ url('subscription') }}"> <i class="fa fa-child"></i> <span class="title">Subscription</span></a>
			</li>
            <li class="hidden">
				<a href="{{ url('help') }}"> <i class="fa fa-question-circle"></i> <span class="title">Help</span></a>
			</li>
            
           
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>