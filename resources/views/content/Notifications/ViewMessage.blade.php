<?php
	$services = new  App\Notifications\Services;

	$data = $services->getUserNofifications()
				->select(['i.Subject','i.Content'])
				->where('to.ToID',decode(Request::get('ToID')))
				->where('i.ID',decode(Request::get('InboxID')))
				->first();
?>
<table class="table">
	<tbody>
		<tr>
			<td><b>Subject:&nbsp;</b>{{ $data->Subject }}</td>
		</tr>
		<tr>
			<td style="height: 300px !important;">
				<b>Content:</b><br />
				<div class="bg-grey" style="width: 100%;height: 100%">
					<div class="col-md-12 margin-top-10">
					{{ $data->Content }}
					</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>