@foreach($data as $row)
<li>
	<a href="javascript:;" class="aSelUnreadMessage" data-toid="{{ encode($row->ToID) }}" data-inboxid="{{ encode($row->InboxID) }}">
	<span class="photo">
	<img alt="" class="img-circle" src="{{ url('general/getUserPhoto?UserID='.encode($row->UserIDFrom)) }}">
	</span>
	<span class="subject">
	<span class="from">
	{{ ucfirst(strtolower($row->FullName)) }} </span>
	<span class="time">{{ DateDiff($row->InboxDate,date('Y-m-d')) <= 0 ? 'Just Now' :  setDate($row->InboxDate,'D H:i A') }} </span>
	</span>
	<span class="message">
	{{ ucfirst(strtolower($row->Subject)) }} </span>
	</a>
</li>
@endforeach