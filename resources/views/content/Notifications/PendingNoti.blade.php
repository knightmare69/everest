@foreach($data as $row)
<li>
	<a href="javascript:;" class="aSelPendingNoti" data-toid="{{ encode($row->ToID) }}" data-inboxid="{{ encode($row->InboxID) }}">
	<span class="time">{{ DateDiff($row->InboxDate,date('Y-m-d')) <= 0 ? 'Just Now' : setDate($row->InboxDate,'D H:i A') }} </span>
	<span class="details">
	{{ ucfirst(strtolower($row->Subject)) }} </span>
	</a>
</li>
@endforeach