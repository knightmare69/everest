<style>
	.border-bottom { border-bottom: 1px solid #000 }
    .border-total { border-bottom: 2px solid #000; border-top: 1px solid #000; border-bottom-style:double;  }
    .border-header { border-bottom: 1px solid #000; border-top: 1px solid #000; }
    .border-bottom tr > th{ border-bottom: 1px solid #000 }
    .border-top { border-top: 1px solid #000 }
    .border-top tr > td{ border-top: 1px solid #000  }
	.border { border: 0.5px solid #000 }
	.right { text-align: right; }
	.bold { font-weight: bold; }
	.center { text-align: center; }
	.left { text-align: left; }
	.valign { vertical-align: middle; }
	.bg { background-color: #EDEBE0; }
    .bg-grey {background-color: #e5e5e5 !important;}
	.pad-5 { padding: 10px; }
    .autofit{width:1%;white-space:nowrap !important;}
    .font-xs{ font-size: 8px; }
    .font-sm{ font-size: 10px; }
    .font-md{ font-size: 12px; }
    .pull-left { left: auto; }
    .ucase { text-transform: uppercase !important; }
    .info { font-size: 13px; }
     .header-report{font-family: "Old English Text MT", "Arial Narrow", Arial, sans-serif;}

    body{
         width: 210mm; height: 297mm;
        /*width: 279.4mm; height: 215.9mm;*/
    }
		.red{
			color : red;
		}
		.blue{
			color : blue;
		}
</style>

<?php
    $ref = Request::get('ref');
    $stud_no = Request::get('studno');
    $dmcmrefno = Request::get('dmcmrefno');


    $header =  DB::table("es_institution")->first();
    $republic = 'Republic of the Philippines';
    $institution_name = $header->Report_InstitutionName;
    $campus_address = $header->Report_CompleteAddress;

    $rs = DB::table(DB::raw("es_financialtransactions FT"))
        ->selectRaw("
		now() AS DatePrinted,
		fn_AcademicYearTerm(J.TermID) AS AYTerm,
		J.ReferenceNo,
		DMCM.RefDate,
		DMCM.IDNo,
		fn_StudentName2(DMCM.IDNo)AS Name,
		DMCM.Explanation,
		DMCM.Posted,
		DMCM.PostedDate,
		DMCM.TransNo,
		A.AcctCode,
		A.AcctName,
		J.Debit,
		J.Credit,
		J.Remarks,
		FT.TransName AS FTTransName")
		->join(DB::raw('es_debitcreditmemo DMCM'),'FT.TransID','=','DMCM.TransType','right outer')
		->join(DB::raw('es_journals J'),'DMCM.RefNo','=','J.ReferenceNo','right outer')
		->join(DB::raw('es_accounts A'),'J.AccountID','=','A.AcctID','right outer')
        ->whereRaw(" J.TransID = '60' AND J.ReferenceNo = '{$ref}' ")
        ->get();

    $subreport = DB::table(DB::raw("es_accountsclass AC"))
    	->selectRaw("
    	A.AcctCode, A.AcctName, AG.GroupCode, AG.GroupName,
    	SUM(CASE TransID WHEN '60' THEN 0 ELSE J.Debit END) AS Debit, SUM(CASE TransID WHEN '60' THEN 0 ELSE J.Credit END)
        AS Credit, SUM(CASE TransID WHEN '20' THEN J.PaymentDiscount ELSE 0 END) AS PaymentDiscount")
		->join(DB::raw('es_accounts A'),'AC.ClassID','=','A.ClassID','left outer')
		->join(DB::raw('es_accountgroups AG'),'A.GroupID','=','AG.GroupID','right outer')
		->join(DB::raw('es_journals J'),'A.AcctID','=','J.AccountID','left outer')
		->join(DB::raw('es_debitcreditmemo DMCM'),'J.DMCMRefNo','=','DMCM.RefNo','left outer')
		->join(DB::raw('es_officialreceipts OFR'),'J.ReferenceNo','=','OFR.ORNo','left outer')
		->whereRaw(" J.TransID = '20' AND J.IDType = '1' AND J.IDNo = '{$stud_no}' AND OFR.TransType = '1' AND OFR.RefNo = '{$dmcmrefno}' OR J.TransID = '1' AND J.IDType = '1' AND J.IDNo = '{$stud_no}' AND J.ReferenceNo = '{$dmcmrefno}' ")
		->groupby("J.AccountID")
		->groupby("A.AcctCode")
		->groupby("A.AcctName")
		->groupby(DB::raw("ifnull(DMCM.Posted, 1)"))
		->groupby("AG.GroupCode")
		->groupby("AG.GroupName")
		->groupby("AG.GroupShort")
		->groupby("AC.ClassCode")
		->groupby("AC.ClassName")
		->groupby("AC.ClassShort")

		->orderby(DB::raw("MIN(J.EntryID)"))
		->get();

		// $subreport = DB::table(DB::raw("( select A.AcctCode, A.AcctName, AG.GroupCode, AG.GroupName,
	  //   	SUM(CASE TransID WHEN '60' THEN 0 ELSE J.Debit END) AS Debit, SUM(CASE TransID WHEN '60' THEN 0 ELSE J.Credit END)
	  //       AS Credit, SUM(CASE TransID WHEN '20' THEN J.PaymentDiscount ELSE 0 END) AS PaymentDiscount, ifnull(DMCM.Posted, 1) AS hav_cndtion, J.EntryID AS ord_cndtion
		// 	from es_accountsclass AC
		// 	LEFT OUTER JOIN es_accounts A ON AC.ClassID = A.ClassID
		// 	LEFT OUTER JOIN es_accountgroups AG ON A.GroupID = AG.GroupID
		// 	RIGHT OUTER JOIN es_journals J
		// 	LEFT OUTER JOIN es_debitcreditmemo DMCM ON J.DMCMRefNo = DMCM.RefNo ON A.AcctID = J.AccountID
	  //       LEFT OUTER JOIN es_officialreceipts OFR ON J.ReferenceNo = OFR.ORNo
		// 	WHEREJ.TransID = '20' AND J.IDType = '1' AND J.IDNo = '{$stud_no}' AND OFR.TransType = '1' AND OFR.RefNo = '{$dmcmrefno}' OR J.TransID = '1' AND J.IDType = '1' AND J.IDNo = '{$stud_no}' AND J.ReferenceNo = '{$dmcmrefno}'
		// 	GROUP BY J.AccountID, A.AcctCode, A.AcctName, ifnull(DMCM.Posted, 1), AG.GroupCode, AG.GroupName, AG.GroupShort, AC.ClassCode, AC.ClassName, AC.ClassShort
		// 	ORDER BY MIN(J.EntryID) ) tbl"))
		// ->selectRaw("*")
		// ->whereRaw("tbl.cndtion = 1")
		// ->orderby(DB::raw("MIN(tbl.ord_cndtion)"))
		// ->get();


    $page_title = "DEBIT / CREDIT MEMO";
    $header = DB::table("es_institution")->first();
    $title = $header->Report_InstitutionName;
    $address = $header->Report_CompleteAddress;

    $date = systemDate();
    $temp = "";
    $total = array(
         0 => '0'
        ,1 => '0'
        ,2 => '0'
        ,3 => '0'
        ,4 => '0'
        ,5 => '0'
    );

	$data_ = json_decode(json_encode($rs), true);
 	$data = array();
 	$data2= array();

    foreach ($data_ as $c) {
		array_push($data2, $c);
	}
	$data = array_group_by($data2, "TransNo");

    foreach ($data2 as $i) {
		$explanation = $i['Explanation'];
		$trans_name = $i['FTTransName'];
		$trans_no = $i['TransNo'];
		$ayterm = $i['AYTerm'];
		$ref_date = $i['RefDate'];
		$posted_date = $i['PostedDate'];
		$posted = $i['Posted'] == false ? 'UNPOSTED' : 'POSTED';
		$name = $i['Name'].' ('.$i['IDNo'].') ';
		$ref_no = str_replace(",","",substr("000000",10-strlen(str_replace(".00","",$i['ReferenceNo']))).str_replace(".00","",$i['ReferenceNo']));
    };

    $sub_data_ = json_decode(json_encode($subreport), true);
 	$sub_data = array();
 	$sub_data2= array();

    foreach ($sub_data_ as $c) {
		array_push($sub_data2, $c);
	}
	$sub_data = array_group_by($sub_data2, "GroupName");

?>


{{-- HEADER BEGIN --}}
<title>{{$page_title}}</title>
<table style="width: 100%;">
	<tr>
        <td class="center">{{$republic}}</td>
    </tr>
    <tr>
        <td class="center bold">{{$institution_name}}</td>
    </tr>
    <tr>
        <td class="info center">{{$campus_address}}</td>
    </tr>
    <tr>
        <td class="center bold">&nbsp;</td>
    </tr>
    <tr>
        <td class="center bold" style="text-decoration: underline;">{{$page_title}}</td>
    </tr>

</table>
{{-- HEADER END --}}

<table style="width: 100%;">
	<tr>
        <td class="info bold">Name:</td>
        <td width="400px"style="font-weight: bold;font-size: 15px;color: maroon;">{{$name}}</td>
        <td style="font-size: 15px;font-weight: bold;">Ref. No.:</td>
        <td style="font-weight: bold;font-size: 15px;color: maroon;">{{$ref_no}}</td>
    </tr>
    <tr>
        <td class="info bold">College:</td>
        <td width="400px"></td>
        <td class="info bold">Ref. Date:</td>
        <td class="info bold">{{date_format(date_create($ref_date),"m/d/Y h:i A")}}</td>
    </tr>
    <tr>
        <td class="info bold">Program:</td>
        <td width="400px"></td>
        <td style="font-size: 15px;font-weight: bold;">Status:</td>
        <td style="font-weight: bold;font-size: 15px;text-decoration: underline;">{{$posted}}</td>
    </tr>
    <tr>
        <td class="info bold"></td>
        <td width="400px"></td>
        <td class="info bold">Posted Date:</td>
        <td class="info bold">{{date_format(date_create($posted_date),"m/d/Y h:i A")}}</td>
    </tr>
</table>
<table style="width: 100%;">
	<tr>
        <td colspan="3" class="info bold" style="text-decoration: underline;">Adjust Transaction</td>
    </tr>
    <tr>
        <td colspan='3' class="info bold" style="padding-left: 25px;"><pre>Transaction:<span style="color: maroon;">  {{$trans_name}}</span>   Reference No.:<span style="color: maroon;">  {{$trans_no}}</span>   A.Y. Term:<span style="color: maroon;">  {{$ayterm}}</span></pre></td>
    </tr>
    <tr>
        <td class="info bold" style="text-decoration: underline;">Explanation:</td>
    </tr>
     <tr>
        <td colspan='3' class="info bold" style="padding-left: 25px;">{{$explanation}}</td>
    </tr>
</table>

<br />
<br />
<table width="100%" class="font-md">
    <thead>
        <tr border="1">
            <th class="border-bottom border-top">CODE</th>
            <th class="border-bottom border-top">ACCOUNT NAME</th>
            <th class="border-bottom border-top">DEBIT</th>
            <th class="border-bottom border-top">CREDIT</th>
            <th class="border-bottom border-top">REMARKS</th>
        </tr>
    </thead>
    <tbody>
		<?php $debit = 0; $credit = 0; ?>
		@foreach($data as $d => $v)
			@foreach($v as $c)
			<?php
				$debit = $debit + $c['Debit'];
				$credit = $credit + $c['Credit'];
			?>
			<tr nobr="true" >
				<td class="center">{{$c['AcctCode']}}</td>
				<td class="center">{{$c['AcctName']}}</td>
				<td class="right">{{$c['Debit'] <> 0 ? number_format($c['Debit'],2) : '0.00'}}</td>
				<td class="right">{{$c['Credit'] <> 0 ? number_format($c['Credit'],2) : '0.00'}}</td>
				<td class="right">{{$c['Remarks']}}</td>
			</tr>
			@endforeach
		@endforeach
		<tr>
        	<td colspan='5' class="border-top"></td>
    	</tr>
		<tr>
			<td></td>
			<td class="right"><b>TOTAL:</b></td>
			<td class="right"><b>{{$debit <> 0 ? number_format($debit,2): '0.00'}}</b></td>
			<td class="right"><b>{{$credit <> 0 ? number_format($credit,2): '0.00'}}</b></td>
		</tr>
		<tr>
			<td colspan='5' style="border-top-style:double;"></td>
		</tr>
    </tbody>
</table>
<br />
<table style="width: 100%;padding-left: 50px;padding-right: 50px;" class="font-md">
	<tr>
		<td colspan="5" style="border-bottom: 3px solid;border-color: maroon;"><b>CURRENT TRANSACTIONS:<b/></td>
	</tr>
	<tr>
		<td colspan="5"></td>
	</tr>
    <tr>
        <td class="center" style="color: dodgerblue;"><b>CODE</b></td>
        <td class="center" style="color: dodgerblue;"><b>ACCOUNT NAME</b></td>
        <td class="center" style="color: dodgerblue;"><b>DEBIT</b></td>
        <td class="center" style="color: dodgerblue;"><b>CREDIT</b></td>
        <td class="center" style="color: dodgerblue;"><b>BALANCE</b></td>
    </tr>
   	<tr>
		<td colspan="5" class="border-bottom"></td>
	</tr>
	<?php $gt_debit = 0; $gt_credit = 0; $gt_balance = 0; ?>
	@foreach($sub_data as $d => $v)
		<tr>
			<td colspan='5'><b>{{$d}}</b></td>
		</tr>
		<?php $debit = 0; $credit = 0; $balance = 0; ?>
		@foreach($v as $c)
			<?php
				$debit = $debit + $c['Debit'];
				$credit = $credit + $c['Credit'] + $c['PaymentDiscount'];
				$balance = $balance + ($c['Debit'] - ($c['Credit'] + $c['PaymentDiscount']));
			?>
			<tr nobr="true">
				<td style="padding-left: 30px;">{{$c['AcctCode']}}</td>
				<td>{{$c['AcctName']}}</td>
				<td class="right">{{$c['Debit'] <> 0 ? number_format($c['Debit'],2) : '-'}}</td>
				<td class="right">{{$c['Credit'] + $c['PaymentDiscount'] <> 0 ? number_format($c['Credit'] + $c['PaymentDiscount'],2)  : '-'}}</td>
				<td class="right">{{$c['Debit'] - ($c['Credit'] + $c['PaymentDiscount']) <> 0 ? number_format($c['Debit'] - ($c['Credit'] + $c['PaymentDiscount']),2) : '-'}}</td>
			</tr>
		@endforeach
		<tr>
			<td></td>
			<td class="right" style="font-style: italic;"><b>Sub-Total:</b></td>
			<td class="border-bottom right"><b>{{$debit <> 0 ? number_format($debit,2) : '0.00'}}</b></td>
			<td class="border-bottom right"><b>{{$credit <> 0 ? number_format($credit,2) : '0.00'}}</b></td>
			<td class="border-bottom right"><b>{{$balance <> 0 ? number_format($balance,2) : '0.00'}}</b></td>
		</tr>
		<?php
			$gt_debit = $gt_debit + $debit;
			$gt_credit = $gt_credit + $credit ;
			$gt_balance = $gt_balance + $balance;
		?>
	@endforeach
	<tr>
		<td></td>
		<td  class="right" style="font-style: italic;"><b>Grand Total:</b></td>
		<td style="border-bottom-style:double;" class="right"><b>{{$gt_debit <> 0 ? number_format($gt_debit,2) : '0.00'}}</b></td>
		<td style="border-bottom-style:double;" class="right"><b>{{$gt_credit <> 0 ? number_format($gt_credit,2) : '0.00'}}</b></td>
		<td style="border-bottom-style:double;" class="right"><b>{{$gt_balance <> 0 ? number_format($gt_balance,2) : '0.00'}}</b></td>
	</tr>
	<tr>
        <td colspan="5" class="center bold">&nbsp;</td>
    </tr>
	<tr>
		<td colspan="5" style="border-bottom: 3px solid;border-color: maroon;"></td>
	</tr>
</table>
<script>
    window.onload=window.print();
</script>
