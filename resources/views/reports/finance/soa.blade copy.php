<style>
        .columns {
          margin-left: -15px;
          margin-right: -15px
        }

        .columns::before,
        .columns::after {
          content: "";
          display: table
        }

        .columns::after {
          clear: both
        }

       
        .column-jay{
          float: left;
          width: 100%;
          min-height: 1px;
          /* padding-left: 15px; */
          padding-right: 5px
        }
        .column-esp{
          float: left;
          width: 100%;
          min-height: 1px;
          padding-left: 5px;
          /* padding-right: 15px */
        }

        .column-12 {
          width: 100%
        }

        .column-offset-12 {
          margin-left: 100%
        }

        .column-11 {
          width: 91.66667%
        }

        .column-offset-11 {
          margin-left: 91.66667%
        }

        .column-10 {
          width: 83.33333%
        }

        .column-offset-10 {
          margin-left: 83.33333%
        }

        .column-9 {
          width: 75%
        }

        .column-offset-9 {
          margin-left: 75%
        }

        .column-8 {
          width: 66.66667%
        }

        .column-offset-8 {
          margin-left: 66.66667%
        }

        .column-7 {
          width: 58.33333%
        }

        .column-offset-7 {
          margin-left: 58.33333%
        }

        .column-6 {
          width: 50%
        }

        .column-offset-6 {
          margin-left: 50%
        }

        .column-5 {
          width: 41.66667%
        }

        .column-offset-5 {
          margin-left: 41.66667%
        }

        .column-4 {
          width: 33.33333%
        }

        .column-offset-4 {
          margin-left: 33.33333%
        }

        .column-3 {
          width: 25%
        }

        .column-offset-3 {
          margin-left: 25%
        }

        .column-2 {
          width: 16.66667%
        }

        .column-offset-2 {
          margin-left: 16.66667%
        }

        .column-1 {
          width: 8.33333%
        }

        .column-offset-1 {
          margin-left: 8.33333%
        }

        .table {
          width: 100%;
          /* table-layout: fixed */
        }

        *,
        *::before,
        *::after {
          margin: 0;
          padding: 0;
          -webkit-box-sizing: inherit;
          box-sizing: inherit
        }

        html {
          -webkit-box-sizing: border-box;
          box-sizing: border-box;
          font-family: Arial, Helvetica, sans-serif
        }

        body {
          text-align: left;
          background: #ccc;
          color: #212529;
          line-height: 1.5;
          font-size: 10px
        }

        .container {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
          -ms-flex-direction: column;
          flex-direction: column;
          margin: 20px auto;
          background: #fff;
          -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
          box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
          color: #212529;
          position: relative;
          width: 100%
        }

        :root {
          line-sizing: normal
        }

        :root {
          -ms-text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric;
          text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric
        }
        .autofit {
		width: 1%;
		white-space: nowrap !important;
	}

        img,
        video,
        canvas,
        audio,
        iframe,
        embed,
        object {
          display: block
        }

        img,
        video {
          max-width: 100%;
          height: auto
        }

        img {
          vertical-align: middle;
          border-style: none
        }

        article,
        aside,
        figcaption,
        figure,
        footer,
        header,
        hgroup,
        main,
        nav,
        section {
          display: block
        }

        b,
        strong {
          font-weight: bolder
        }

        table {
          border-collapse: collapse
        }

        .text {
          font-weight: 400
        }

        .text-title {
          margin-bottom: 10px
        }

        .text-paragraph {
          text-indent: 35px;
          text-align: justify
        }

        .header-top {
          position: relative
        }

        .header-top__logo {
          position: absolute;
          top: 0;
          left: 0;
          width: 100px;
          height: 100px
        }

        .header-center {
          margin-top: 20px;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
          -ms-flex-direction: column;
          flex-direction: column
        }

        .header-center__logo {
          margin-bottom: 20px;
          width: 200px;
          height: 200px
        }

        .container {
          max-width: 4.12in;
          height: 5.5in;
          padding: 10px 10px
        }
        /* .container-portrait {
          max-width: 4.25in;
          height: 5.5in;
       
        } */

       

        .table-header td {
          font-weight: 700
        }

        .table-header tr:nth-child(1) td:first-child {
          width: 65px
        }

        .table-header tr:nth-child(1) td:last-child {
          width: 130px
        }

        .table-signature {
          margin-top: 30px;
          border-top: 5px double #212529;
          border-bottom: 5px double #212529
        }

        .table-signature th:first-child {
          width: 130px
        }

        .table-signature td {
          height: 20px;
          vertical-align: bottom
        }

        .table-signature td:nth-child(2) {
          border-bottom: 1px solid #212529
        }

        .table-info {
          margin-bottom: 10px
        }

        .table-info th:nth-child(1) {
          width: 30px
        }

        .table-info th:nth-child(2) {
          width: 40px
        }

        .table-info th:nth-child(3) {
          width: 20px
        }

        .table-info th:nth-child(6) {
          width: 25px
        }

        .table-info th:nth-child(7) {
          width: 40px
        }

        .table-info th:nth-child(8) {
          width: 25px
        }

        .table-info th:nth-child(9) {
          width: 40px
        }

        .table-info tr:nth-child(2) td {
          font-size: 9px
        }

        .table-grades thead tr:nth-child(2)~tr th {
          border: 1px solid #212529
        }

        .table-grades tr:nth-child(1) th:nth-child(1) {
          width: 150px
        }

        .table-grades tr:nth-child(1) th:nth-child(2),
        .table-grades tr:nth-child(1) th:nth-child(3),
        .table-grades tr:nth-child(1) th:nth-child(4),
        .table-grades tr:nth-child(1) th:nth-child(5) {
          width: 35px
        }

        .table-grades tr:nth-child(1) th:nth-child(5)~th {
          width: 50px
        }

        .table-grades tbody:nth-of-type(1) tr:not(:last-of-type) td {
          border: 1px solid #212529
        }

        .parent-subj {
          padding-left: 20px !important;
          font-weight: normal !important;
        }

        .table-grades tbody:nth-of-type(1) td:first-child {
          padding-left: 10px
        }

        .table-grades tbody:nth-of-type(1) td:not(:first-child) {
          text-align: center
        }

        .table-grades tbody:nth-of-type(2) {
          font-style: italic;
          font-weight: 700
        }

        .table-grades tbody:nth-of-type(2) td:first-child {
          padding-left: 10px
        }

        .table-grades tbody:nth-of-type(2) td:not(:first-child) {
          text-align: center
        }

        .table-manners {
          margin-top: 10px
        }

        .table-manners th,
        .table-manners td {
          border: 1px solid #212529
        }

        .table-manners th {
          font-size: 12px
        }

        .table-manners th:nth-child(1) {
          width: 100px
        }

        .table-manners th:nth-child(2)~th {
          width: 30px
        }

        .table-marks {
          margin-top: 20px
        }

        .table-marks th:nth-child(1) {
          width: 130px
        }

        .table-marks th:nth-child(2) {
          width: 40px
        }

        .table-marks td:first-child {
          padding-right: 15px
        }

        .table-marks td:last-child {
          padding-left: 10px
        }

        .table-attendance {
          margin-top: 15px
        }

        .table-attendance tr:first-child~tr th {
          border: 1px solid #212529
        }

        .table-attendance th:first-child~th:not(:last-child) {
          width: 20px
        }

        .table-attendance th:last-child {
          width: 35px
        }

        .table-attendance td {
          border: 1px solid #212529
        }

        .front-title {
          margin-top: 10px
        }

        .front-logo {
          width: 160px;
          height: 160px;
          margin: 10px auto 15px
        }

        .front-subtitle {
          margin-bottom: 15px
        }

        .front-subtitle .subtitle {
          font-size: 13px;
          margin-bottom: 30px
        }

        .front-subtitle .department {
          font-size: 15px
        }

        .height-20 {
          height: 15px
        }

        .height-25 {
          height: 25px
        }

        .height-30 {
          height: 19px
        }

        .height-35 {
          height: 35px
        }

        .height-40 {
          height: 40px
        }

        .height-50 {
          height: 50px
        }

        .text-center {
          text-align: center
        }

        .text-right {
          text-align: right
        }

        .vertical-bottom {
          vertical-align: bottom
        }

        .vertical-top {
          vertical-align: top
        }

        .border {
          border: 1px solid #212529
        }

        .border-bottom {
          border-bottom: 1px solid #212529;
          font-size: 9pt;
          font-weight: bold;

        }

        .border-bottom2x {
          border-bottom: 2px solid #212529
        }

        .border-bottom-double {
          border-bottom: 5px double #212529
        }

        .pl-lg {
          padding-left: 15px
        }

        .px-md {
          padding-left: 5px;
          padding-right: 5px
        }

        @page {
          size: a4;
          margin: 0
        }

        @media print {
          .container {
            margin: auto;
            width: auto;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            page-break-inside:avoid;
            /* page-break-after: always */
          }

          body {
            -webkit-print-color-adjust: exact !important
          }

          /* html,
          body {
            height: 100%
          } */
        }
/*# sourceMappingURL=grade-school.min.css.map */

.center {
		text-align: center !important;
		vertical-align: middle !important;
	}

  table td, th{
       font-size: 7pt !important;
     }
     .font-5{
       font-size: 9pt !important;
     }

.autofit {
    width: 1%;
    white-space: nowrap !important;
}


.header {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end
    }

    .header__logo {
      width: 60px;
      height: auto
    }

    .header__text {
      -webkit-box-flex: 1;
      -ms-flex: 1;
      flex: 1
    }

    .header__text-top {
      line-height: 1;
    
      padding-left: 8px
    }

    .header__text-top h1 {
      font-weight: 500;
      font-size: 25px
    }

    .header__text-top p {
      font-weight: 600;
      font-size: 13px
    }

    .header__text-center {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      padding: 0 8px 3px
    }

    .header__text-center ul {
      list-style-type: none;
      font-family: "tonic";
      font-weight: 600
    }

    .header__text-bottom {
      border-top: 2px solid #2a2a2a;
      text-align: right
    }

    .header__text-bottom p {
      font-size: 14px;
      font-family: "tonic";
      font-weight: 600
    }

    .header__list-left {
      font-size: 11px
    }

    .header__list-left li {
      line-height: 1
    }

    .header__list-right {
      font-size: 13px;
      -ms-flex-item-align: end;
      align-self: flex-end;
      text-align: right
    }

    .header__list-right li {
      line-height: 1.2
    }
    
    .main__header {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      margin-bottom: 5px
    }

    .main__header-left {
      text-align: left;
      width: 360px
    }

    .main__header-left .input-group label {
      width: 55px
    }

    .main__header-left .input-group input {
      -webkit-box-flex: 1;
      -ms-flex: 1;
      flex: 1
    }

    .main__header-right {
      text-align: right
    }

    .main__header-right .input-group label {
      padding-right: 4px;
      width: 275px
    }

    .main__header-right .input-group input {
      width: 200px
    }

    
    .input-group {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      font-size: 10px
    }

    .input-group:not(:last-child) {
      margin-bottom: 3px
    }

    .input-group input {
      border: 0;
      border-bottom: 1px solid #2a2a2a;
      font-size: inherit;
      font-weight: 600
    }


    </style>
<style>
	.full-border {
		border-bottom: .5px solid #000;
		border-left: .5px solid #000;
		border-right: .5px solid #000;
		border-top: .5px solid #000;
	}

	.border-bottom {
		border-bottom: .5px solid #000
	}

	.border-left {
		border-left: .5px solid #000
	}

	.border-right {
		border-right: .5px solid #000
	}

	.border-total {
		border-bottom: 2px solid #000;
		border-top: 1px solid #000;
		border-bottom-style: double;
	}

	.border-header {
		border-bottom: 1px solid #000;
		border-top: 1px solid #000;
	}

	.border-bottom tr>th {
		border-bottom: 1px solid #000
	}

	.border-top {
		border-top: .5px solid #000
	}

	.border-top tr>td {
		border-top: 1px solid #000
	}

	.border {
		border: 0.5px solid #000
	}

	.right {
		text-align: right;
	}

	.bold {
		font-weight: bold;
	}

	.center {
		text-align: center;
		vertical-align: middle !important;
	}

	.left {
		text-align: left;
	}

	.valign {
		vertical-align: middle;
	}

	.bg {
		background-color: #EDEBE0;
	}

	.bg-grey {
		background-color: #e5e5e5 !important;
	}

	.bg-blue {
		background-color: #adceee !important;
	}

	.bg-yellow {
		background-color: #ecedef !important;
	}

	.pad-5 {
		padding: 10px;
	}

	.autofit {
		width: 1%;
		white-space: nowrap !important;
	}

	.font-xs {
		font-size: 8px;
	}

	.font-sm {
		font-size: 10px;
	}

	.font-md {}

	.pull-left {
		left: auto;
	}

	.ucase {
		text-transform: uppercase !important;
	}

	.rotate-90 {
		-ms-transform: rotate(-90deg);
		/* IE 9 */
		-webkit-transform: rotate(-90deg);
		/* Safari 3-8 */
		transform: rotate(-90deg);
	}

	.indent {
		text-indent: 45px;
	}

	.indent-1 {
		text-indent: 5px;
	}

	#rotate {
		-moz-transform: rotate(-90.0deg);
		/* FF3.5+ */
		-o-transform: rotate(-90.0deg);
		/* Opera 10.5 */
		-webkit-transform: rotate(-90.0deg);
		/* Saf3.1+, Chrome */
		filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);
		/* IE6,IE7 */
		-ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)";
		/* IE8 */
	}

	.border-cut {
		border-left: 2px dashed;
	}

	table.rotate-table-grid {
		box-sizing: border-box;
		border-collapse: collapse;
	}

	.rotate-table-grid tr,
	.rotate-table-grid td,
	.rotate-table-grid th {
		/* border: 1px solid #ddd; */
		position: relative;
		padding: 10px;
	}

	.pad-xs {
		padding: 1px;
	}

	.rotate-table-grid th span {
		transform-origin: 0 50%;
		transform: rotate(-90deg);
		white-space: nowrap;
		display: block;
		position: absolute;
		bottom: 0;
		left: 50%;
	}
	.rotate-table-grid td {

		transform: rotate(-90deg);
		white-space: nowrap;

		position: absolute;

	}

	table {
		border-spacing: 0;
		font-size: 8pt;
	}

	td {
		padding: 3px;
	}

	body {
		width: 210mm;
		height: 148.5mm;
		font-size: 9pt;
		/* width: 279.4mm; height: 215.9mm; */
		/* font-family: "Times New Roman", Times, serif; */
	}

	.red {
		color: red;
	}

	.blue {
		color: blue;
	}

	* {
		box-sizing: border-box;
	}

	.stamp {
	  /* transform: rotate(12deg); */
		color: #555;
		font-size: 2rem;
		font-weight: 700;
		border: 0.25rem solid #555;
		display: inline-block;
		padding: 0.25rem 1rem;
		text-transform: uppercase;
		border-radius: 1rem;
		font-family: 'Courier';
		-webkit-mask-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/8399/grunge.png');
	  -webkit-mask-size: 944px 604px;
	  mix-blend-mode: multiply;
	}



	.is-approved {
		color: #0A9928;
		border: 0.5rem solid #0A9928;
		-webkit-mask-position: 13rem 6rem;
		/* transform: rotate(-14deg); */
	  border-radius: 0;
	}
	body{
		font-family: "Century Gothic";
		font-size: 8pt;
	}

	@mediaprint {

		body {
			-webkit-print-color-adjust: exact;
			-webkit-mask-image: exact;
		}
		.stamp {
		  /* transform: rotate(12deg); */
			color: #555;
			font-size: 2rem;
			font-weight: 700;
			border: 0.25rem solid #555;
			display: inline-block;
			padding: 0.25rem 1rem;
			text-transform: uppercase;
			border-radius: 1rem;
			font-family: 'Courier';
			-webkit-mask-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/8399/grunge.png');
		  -webkit-mask-size: 944px 604px;
		  mix-blend-mode: multiply;
		}



		.is-approved {
			color: #0A9928;
			border: 0.5rem solid #0A9928;
			-webkit-mask-position: 13rem 6rem;
			/* transform: rotate(-14deg); */
		  border-radius: 0;
		}
	}
</style>
<title>S.O.A.</title>

<?php
use App\Modules\Accounting\Services\Assessment\assessment as assess;
    $id = decode(Request::get('ref'));
		$term = decode(Request::get('term'));
		$studentno = Request::get('studno');
    $temp = 0;
		$total_balance= 0;
		$tuitionfee_bal =0;
		$bal =  DB::select(DB::raw("call  ES_GetOutstandingBalanceSummary('1','".$studentno."','1')"));
		foreach ($bal as $key => $value) {
				$total_balance += $value->Balance;
		}

		$studentinfo = DB::table('es_students as s')
												->leftJoin('es_registrations as r','r.StudentNo','=','s.StudentNo')
												->selectRaw('r.*, s.*,
														fn_AcademicYear(r.TermID) AS SchoolYear,
														fn_StudentName(r.StudentNo) As StudentName, fn_SectionName(ClassSectionID) As Section ,
						                            fn_AcademicYearTerm(r.TermID) As Term, fn_ProgramName(r.ProgID) As Program, fn_MajorName(r.MajorID) As Major,
						                            fn_K12_YearLevel(r.YearLevelID) As YearLevel,fn_TemplateCode(TableofFeeID) As Template
												')
												->where('r.StudentNo', $studentno)
												->where('r.RegID', $reg)
												->first();
		$assess = new assess;
		$keyid = $assess->exec_penalties($term, $studentno, $reg, date('Y-m-d'));
		$penalties = DB::table('ES_SOA_GSHS_Penalties as P')
								->selectRaw("P.EntryID,
            P.RegID,
            P.SeqNo,
            P.Checked,
            P.CutDate,
            P.PenaltyMonthCount,
            P.Scheme,
            P.Payment,
            P.Excess,
            P.Balance,
            P.Penalty,
            P.TotalAmountDue,
            P.TotalPenalty,
            P.TotalBalanceToDate,
            IFNULL(( SELECT SchoProviderType
                     FROM   ES_Registrations R
                     WHERE  R.RegID = P.RegID
                   ), 0) AS SchoProviderType,
            IFNULL(( SELECT
                        Debit-Credit
              FROM      ES_SOA_GSHS
              WHERE     KeyID = KeyID
                        AND StudentNo = (select StudentNo from es_registrations where RegID = P.RegID limit 1)
                        AND TransID = '0' LIMIT 1   ), 0) AS BeginningBalance
							")
								->where('KeyID',$keyid)
								->get();

		$assessment = DB::select(DB::raw("call  ES_rptRegAssessment_Accounts_r2('".$reg."','".$studentno."','".$term."')"));


    $date = systemDate();
    $template = "";
    $provider = "";
		$page_title =" STATEMENT OF ACCOUNT";
?>
@if(strtotime($studentinfo->ValidationDate) > 100 && 1 == 2)
<p class="stamp is-approved" style="
top: 0in;
left: 3.6in;
position: fixed;
text-align: center;
">Enrolled</p>
<p class="stamp is-approved" style="
top: 0in;
left: 6.4in;
position: fixed;
text-align: center;
">Enrolled</p>
@endif
<div style="">

 <table style="width: 100%;">
	 <tr>
		 <td><img src="{{url('general/getThemePhoto?tid=1&field=report_header')}}" style="height:50px; margin-left: 25px;" ></td>
	 </tr>
 </table>

 {{-- <table style="margin-top: 10px;float:right; width:30%;">
	 <tr>
		 <td>
			 &nbsp;
			 <svg style="display:none;width:122px;height:40px;" id="barcode"></svg>
		 </td>
	 </tr>
 </table> --}}
 @foreach  ($penalties as $key => $v)
 <?php
	 $totalPenalty = $v->TotalPenalty;
	 ?>

 @endforeach



 <div style=" font-size: 5pt;">

	 <table style="width:100%;">
		 <table style="float: left;width:70%;">
			 <tr>
				 <td class="bold" style="font-size: 9pt;"> Statement of Account - S.Y. {{$studentinfo->Term}}</td>
			 </tr>
			 <tr>
				 <td class="bold" style="font-size: 9pt;"> {{$studentinfo->Program}}</td>
			 </tr>
		 </table>
		 @if($studentinfo->StudentNo == '')
		 <table style="float:right;width:30%;">
			 <tr>
				 <td class="stamp"> OFFICIALLY ENROLLED</td>
			 </tr>
		 </table>
	 @endif
	 </table>

	 <table style="float:left; width:50%;">
		 <tr>
			 <td class="pad-xs"> Student No:</td>
			 <td class="pad-xs">{{$studentinfo->StudentNo}}</td>
		 </tr>
		 <tr>
			 <td class="pad-xs">Student Name</td>
			 <td class="pad-xs">{{$studentinfo->StudentName}} {{$studentinfo->ChineseName}} </td>
		 </tr>
		 <tr>
			 <td class="pad-xs">Year Level: </td>
			 <td class="pad-xs" class="">{{$studentinfo->YearLevel}}</td>
		 </tr>
		 {{-- <tr>
				 <td class="pad-xs">Department :  </td>
				 <td class="pad-xs" class="">{{$studentinfo->Program}} </td>
		 </tr> --}}
		 <tr>
			 <td class="pad-xs">Section : </td>
			 <td class="pad-xs" class="">{{$studentinfo->Section}} </td>
		 </tr>
	 </table>

	 <table style=" margin-top: 10px; float:right; width:50%;">
		 <tr>
			 <td class="pad-xs"> Date as of:</td>
			 <td class="pad-xs" class="">{{date("F j, Y, g:i a")}}</td>
		 </tr>
		 <tr>
			 <td class="pad-xs">Registration ID:</td>
			 <td class="pad-xs" class="">{{$studentinfo->RegID}} </td>
		 </tr>
		 <tr>
			 <td class="pad-xs">Payment Scheme: </td>
			 <td class="pad-xs" class="">{{$studentinfo->Template}}</td>
		 </tr>
		 <tr>
			 <td class="pad-xs">Scholarship : </td>
			 <td class="pad-xs" class=""></td>
		 </tr>
	 </table>
 </div>

 <table style="width:5.2in;">
 </table>

 <div class='border-bottom' style="height: 255px; width:100%;">
	 <table id style=" height: 240px;margin-top: 10px; float:left; width:35%;">
		 <tr>
			 <th class="border-top" colspan="2">SCHEDULE OF FEES</th>
		 </tr>
		 <tr>
			 <th class="border-top" style="width: 60%"> Accounts</th>
			 <th class="border-top border-right" style="width: 40%" class="">Amount</th>
		 </tr>
		 <?php $totalAssess = 0;
				 $paymentCreditMemo = 0;
				 $balance = 0
					 ?>
		 @foreach  ($assessment as $key => $value)

			 @if($value->TransID == 1)
		 <?php $totalAssess += ($value->AssessFee  );
					 $paymentCreditMemo += $value->ActualPayment +$value->CreditMemo +$value->Discount;
					 $balance += $value->Balance;	?>
		 <tr>
			 <td>{{$value->AcctName}}</td>
			 <td class="border-right  right">{{number_format($value->AssessFee,2)}} </td>
		 </tr>
		 @endif

		 @endforeach
		 <tr>
			 <td class="border-top bold">
				 Assessed Fees
			 </td>
			 <td class="border-right border-top right bold">
				 {{number_format($totalAssess,2)}}
			 </td>
		 </tr>
		 <tr>
			 <td class="">
				 (Payments / Cr. Memo)
			 </td>
			 <td class=" border-right right">
				 P {{number_format($paymentCreditMemo,2)}}
			 </td>
		 </tr>
		 {{-- <tr>
			 <td class="">
			 Penalty
			 </td>
			 <td class=" border-right right">
				 P {{number_format($totalPenalty,2)}}
			 </td>
		 </tr> --}}
		 <tr>
			 <td class="border-top border-bottom bold">
				 Amount Payable
			 </td>
			 <td class=" border-top border-bottom border-right right bold">
				 P {{number_format($balance,2)}}
			 </td>
		 </tr>
		 {{-- <tr>
			 <td class="border-right" colspan="2">
				 <table style="width:221px; margin-top: 10px; margin-bottom:20px;">
					 <tr>
						 <td style="font-size:.8em" class="border">
							 Late Payment Fee of P250.00 is charged
							 for every payment made afer the due
						 </td>
					 </tr>
				 </table>
			 </td>
		 </tr> --}}
	 </table>

	 <table class="border-bottom" style="margin-top: 10px;float:left; width:60%;">
		 <tr>
			 <th class="border-top" colspan="4">SCHEDULE OF PAYMENT</th>
		 </tr>
		 <tr>
			 <th class="border-top"> DATE</th>
			 <th class="border-top">AMOUNT DUE</th>
			 <th class="border-top"> PAYMENT/ADJ</th>
			 {{-- <th class="border-top">PENALTY</th> --}}
			 <th class="border-top">BALANCE</th>
		 </tr>
		 <?php
			 $scheme_t = 0;
			 $payment_t =0;
			 $balance_t =0;
			 $penalty_t = 0;
					 ?>
		 @foreach  ($penalties as $key => $v)
		 <?php
			 $scheme_t += $v->Scheme;
			 $payment_t += $v->Payment;
			 $balance_t += $v->Balance;
			 $penalty_t +=$v->Penalty;
			 ?>
		 <tr>
			 <td class="autofit">{{isset($v->CutDate) ? setDate($v->CutDate,'F j,Y'):''}}</td>
			 <td class="right">{{number_format($v->Scheme,2)}} </td>
			 <td class="right">{{number_format($v->Payment,2)}}</td>
			 {{-- <td class="right">{{number_format($v->Penalty,2)}}</td> --}}
			 <td class="right">{{number_format($v->Balance,2)}}</td>
		 </tr>
		 @endforeach
		 <tr>
			 <td class="border-top border-bottom bold">TOTALS</td>
			 <td class="border-top border-bottom bold right">{{number_format($scheme_t,2)}} </td>
			 <td class="border-top border-bottom bold right">{{number_format($payment_t,2)}}</td>
			 {{-- <td class="border-top border-bottom bold right">{{number_format($penalty_t,2)}}</td> --}}
			 <td class="border-top border-bottom bold right">{{number_format($balance_t,2)}}</td>
		 </tr>
	 </table>
 </div>
 <table  style="font-size : 7pt;width:100%; border-spacing:3">
	 <tr>
		 <td colspan="2">Validation : {{ strtotime($studentinfo->ValidationDate) > 100 ? date("F j, Y, g:i a", strtotime($studentinfo->ValidationDate)) : ''}}</td>
	 </tr>
	 <tr>
		 <td style="width:50%">Generated by:</td>
		 <td style="width:50%">Conformity</td>
	 </tr>
	 <tr>
		 <td class="center border-bottom">{{getUserFullName()}}</td>
		 <td class="center border-bottom"></td>
	 </tr>
	 <tr>
		 <td></td>
		 <td class="center">SIGNATURE OVER PRINTED NAME / DATE</td>
	 </tr>
 </table>



</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>

<script>
	window.onload = window.print();
	var barcode = <?php echo $studentno.$reg; ?>;
	JsBarcode("#barcode", barcode);
	// $(function() {
	// 	var header_height = 0;
	// 	$('.rotate-table-grid th span').each(function() {
	// 		if ($(this).outerWidth() > header_height) header_height = $(this).outerWidth();
	// 	});
	// 	$('.rotate-table-grid th').height(440);
	// });

</script>
