<!DOCTYPE html>
<style>
.text-left {
  text-align: left
}

.text-center {
  text-align: center
}

.text-right {
  text-align: right
}

.border {
  border: 1px solid #2a2a2a
}

.border-bottom {
  border-bottom: 1px solid #2a2a2a
}

*,
*::before,
*::after {
  margin: 0;
  padding: 0;
  -webkit-box-sizing: inherit;
  box-sizing: inherit
}

html {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  font-family: Arial, Helvetica, sans-serif
}

body {
  text-align: left;
  background: #ccc;
  color: #2a2a2a;
  line-height: 1.5
}

.container {
  width: 100%;
  margin: 20px auto;
  background: #fff;
  -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  color: #2a2a2a
}

.input-group {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex
}

.input-label {
  white-space: nowrap
}

.input-control {
  width: 100%
}

.input-border-bottom {
  border: 0;
  border-bottom: 1px solid #2a2a2a
}

.table {
  width: 100%
}

:root {
  line-sizing: normal
}

:root {
  -ms-text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric;
  text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric
}

img,
video,
canvas,
audio,
iframe,
embed,
object {
  display: block
}

img,
video {
  max-width: 100%;
  height: auto
}

img {
  vertical-align: middle;
  border-style: none
}

article,
aside,
figcaption,
figure,
footer,
header,
hgroup,
main,
nav,
section {
  display: block
}

b,
strong {
  font-weight: bolder
}

table {
  border-collapse: collapse
}

body {
  font-size: 14px
}

.header {
  position: relative;
  margin-bottom: 10px
}

.header img {
  width: 400px;
  height: auto;
  margin: auto
}

.header p {
  font-size: 9px;
  position: absolute;
  top: 77%;
  left: 34%;
  font-weight: 600;
  letter-spacing: 1px
}

.container {
  max-width: 8.3in;
  height: auto;
  min-height: 11.7in;
  padding: 10px 20px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column
}

.table {
  font-size: 10px
}

.table-student {
  margin-top: 15px;
  margin-bottom: 5px
}

.table-student td {
  font-weight: 700;
  padding: 2px 10px
}

.table-student td:nth-child(1) {
  width: 40px
}

.table-student td:nth-child(2) {
  width: 100px
}

.table-student td:nth-child(4) {
  width: 80px
}

.table-student td:nth-child(5) {
  width: 210px
}

.table-school-year th:nth-child(1) {
  width: 140px
}

.table-school-year th:nth-child(2) {
  width: 50px
}

.table-school-year th:nth-child(3) {
  width: 100px
}

.table-school-year th:nth-child(4) {
  width: 80px
}

.table-school-year th:nth-child(5) {
  width: 80px
}

.table-school-year th:nth-child(6) {
  width: 80px
}

.table-school-year th:nth-child(8) {
  width: 80px
}

.table-school-year td {
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap
}

.table-school-year td span {
  display: block;
  position: relative
}

.table-school-year td span::before {
  content: "";
  position: absolute;
  top: 0;
  right: 0;
  height: 1px;
  width: 90%;
  background: #2a2a2a
}

.table-school-year tfoot td {
  padding-top: 3px;
  padding-bottom: 3px
}

.table-school-year tfoot td span {
  padding-top: 3px;
  padding-bottom: 3px
}

.table-school-year tfoot td:first-child {
  padding-right: 40px
}

.table-school-year tfoot td:last-child {
  padding-left: 10px
}

.footer {
  font-size: 10px;
  margin-top: auto;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between
}

.pl-10 {
  padding-left: 10px
}

.pr-10 {
  padding-right: 10px
}

.h-30 {
  height: 30px
}

.v-bottom {
  vertical-align: bottom
}

.border-top {
  border-top: 1px solid #2a2a2a
}

.border-x {
  border-top: 1px solid #2a2a2a;
  border-bottom: 1px solid #2a2a2a
}

.color-red {
  color: #a00
}

.bg-red {
  background: #faa
}

@page {
  size: a4;
  margin: 0
}

@media print {
  .container {
    margin: auto;
    width: auto;
    height: auto;
    padding-top: 0;
    padding-bottom: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    page-break-after: always
  }
  .footer {
    position: absolute;
    bottom: 10px;
    left: 20px;
    right: 20px
  }
  body {
    -webkit-print-color-adjust: exact !important
  }
  html,
  body {
    height: 100%
  }
}
</style>

<?php 
    

    $term = decode(Request::get('academic-term'));
    $campus = decode(Request::get('campus'));
    $progid = decode(Request::get('programs'));
    $sect = decode(Request::get('section'));
    $yl = decode(Request::get('year-level'));

    $studentno = (Request::get('studnumber'));


    $page_title = "Report of Grades";
    $header = DB::table("es_institution")->first();
    
    //$rs = DB::select("EXEC ES_rptStudentLedger 0,'$studentno'");
    $rs = DB::select("EXEC ES_rptStudentLedger");
    $totalb=0;
    $totald = 0;


  ?>
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Student Ledger</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <body>
    <div class="container">
        <center>
        
        <img height="100" width="100" style="width: 75px; float: left; position: absolute;  margin-left: 50px;" src="{{url('general/getThemePhoto?tid=1&field=header_logo')}}" class="header-logo" />
        <h3>{{$header->Report_InstitutionName}}</h3>
        <p>{{$header->Report_CompleteAddress}}</p>
        </center>

      <main class="main">
        <h3 class="text-center">
          <u>STUDENT LEDGER</u>
        </h3>

        <table class="table table-student">
          <tbody>
            <tr>
              <td></td>
              <td>STUDENT NO.:</td>
              <td>{{$rs[0]->StudentNo}}</td>
              <td>COLLEGE:</td>
              <td>{{$rs[0]->CollegeCode}}</td>
            </tr>

            <tr>
              <td></td>
              <td>FULLNAME:</td>
              <td>{{$rs[0]->Fullname}}</td>
              <td>PROGRAM:</td>
              <td>{{$rs[0]->ProgCode}}</td>
            </tr>

            <tr>
              <td></td>
              <td>YEAR LEVEL:</td>
              <td colspan="3">{{$rs[0]->YearLevelID}}</td>
            </tr>
          </tbody>
        </table>

        <table class="table table-school-year">
          <thead>
            <tr class="border-x">
              <th class="text-center">
                TRANS.
                <br />
                DATE
              </th>
              <th class="text-center">
                TRANS.
                <br />
                CODE
              </th>
              <th class="text-center">REFERENCE NO.</th>
              <th class="text-right">DEBIT</th>
              <th class="text-right">CREDIT</th>
              <th class="text-right">BALANCE</th>
              <th class="pl-10">REMARKS</th>
              <th class="text-center">
                DATE
                <br />
                POSTED
              </th>
            </tr>
          </thead>

          <tbody>



        <?php   
                $rs= json_decode(json_encode($rs), true);
                $rs = array_group_by( $rs, "AcademicYearTerm");

                // dd($rs);
          ?>

        @foreach($rs as $sterm => $r)
        
      
            <tr class="h-30">
              <td colspan="8" class="v-bottom">
                <strong>
                  <u>{{$sterm}}</u>
                </strong>
              </td>
            </tr>

           <?php 

              $totaldebit = 0;
              $totalcredit = 0;

            ?>

        
          @foreach($r as $key => $a)

            <?php 

            $credit = ($a['FinancialAid'] > 0 ?  $a['FinancialAid'] : $a['Credit']);
            $non_ledger_row = 0;
            $non_ledger = 0;
            $debit_ = 0;
            $cbalance = ($a['NonLedger'] > 0 ? 0  :($a['AssessFee'] - $credit)) ;
            $dbalance = ($a['Debit'] -  $a['Credit']);

            $posteddate = strtotime($a['PostedDate']);
            
            
          

            if($a['TransID'] == '20'){
                $remarks = $a['Remarks'].':'.$a['OR PaymentFor'].' ['.$a['ORRefNo'].']';
            }else{
                $remarks = $a['Remarks'];
            }


            if ($a['NonLedger'] > 0) {
                $non_ledger_row  =  $a['Credit'] -  $a['NonLedger'];
                $cbalance = $cbalance - $non_ledger_row;
                $non_ledger =  $a['NonLedger'] - 0.00;
                $dbalance = $dbalance  + $non_ledger;
            }
            if ($a['TransCode']=='DMCM' &&  $a['Debit'] > 0) {
                $debit_ =  $a['Debit'] - 0.00;
                $cbalance = $cbalance + $debit_;
            }

              if ($a['PostedDate'] == null && $a['Posted'] == 0) {
                $f = $a['Credit'] - 0.00;
                $cbalance = $cbalance + $f;
                $dbalance = $dbalance  + $f;
            }


            $totalb += $cbalance;
            $totald += $dbalance;
            $transdate = $a['TransDate'];
            $totaldebit += $a['Debit'];
            $totalcredit += $a['Credit'];

            
             ?>


            @if($a['NonLedger'] <> 0)
              <tr class="bg-red">
            @endif  
              
                <td class="text-center">{{$a['TransDate']}}</td>
                <td>{{$a['TransCode']}}</td>
                <td class="text-center">{{$a['ReferenceNo']}}</td>
                <td class="text-right">{{number_format($a['Debit'],2,'.',',')}}</td>
                <td class="text-right">{{number_format($a['Credit'],2,'.',',')}}</td>
                <td class="text-right">{{number_format(($a['FinancialAid'] > 0 ?$a['AssessFee'] :$totalb ),2,'.',',')}} </td>
                <td class="pl-10">{{$a['Remarks']}}</td>

                
                <td class="text-center">
                @if($a['NonLedger'] <> 0)
                      -              
                @else 
                    {{date('Y-m-j H:i',$posteddate)}}
                 @endif
                </td>
            

              </tr>

          

          @endforeach

            <?php


            
            $termb = $totalb;
            $termc = $totald;
            
            ?>

            <tr>
              <td colspan="3" class="text-right">
                <strong>
                  <small>
                    *** Ending Balance for {{$sterm}} ***
                  </small>
                </strong>
              </td>
              <td class="text-right">
                <strong>
                  <span> {{number_format($totaldebit,2,'.',',')}}</span>
                </strong>
              </td>
              <td class="text-right">
                <strong>
                  <span>{{number_format($totalcredit,2,'.',',')}}</span>
                </strong>
              </td>
              <td class="text-right">
                <strong>
                  <span> {{number_format($termb,2,'.',',')}}</span>
                </strong>
              </td>
              <td class="text-right color-red">
                <strong>
                  Term Balance:
                </strong>
              </td>
              <td class="pl-10">
                <strong> {{number_format($termb,2,'.',',')}}</strong>
              </td>
            </tr>

      @endforeach

         </table>
      </main>

      <footer class="footer">
          <p>Print Info: {{date("F j, Y, g:i a")}} </p>
          <p>Page 1 of 1</p>
      </footer>
    </div>
  </body>
</html>
