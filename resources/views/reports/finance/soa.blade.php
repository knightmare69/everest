
<style>

  .header__logo {
    width: 35px;
    height: auto;
}
  .text-left {
  text-align: left
}

.text-center {
  text-align: center
}

.text-right {
  text-align: right
}

.border {
  border: 1px solid #2a2a2a
}

.border-bottom {
  border-bottom: 1px solid #2a2a2a
}

*,
*::before,
*::after {
  margin: 0;
  padding: 0;
  -webkit-box-sizing: inherit;
  box-sizing: inherit
}

html {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  font-family: Arial, Helvetica, sans-serif
}

body {
  text-align: left;
  background: #ccc;
  color: #2a2a2a;
  line-height: 1.5
}

.container {
  width: 100%;
  margin: 20px auto;
  background: #fff;
  -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  color: #2a2a2a
}

.input-group {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex
}

.input-label {
  white-space: nowrap
}

.input-control {
  width: 100%
}

.input-border-bottom {
  border: 0;
  border-bottom: 1px solid #2a2a2a
}

.table {
  width: 100%
}

:root {
  line-sizing: normal
}

:root {
  -ms-text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric;
  text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric
}

img,
video,
canvas,
audio,
iframe,
embed,
object {
  display: block;
}

img,
video {
  max-width: 100%;
  height: auto
}

img {
  vertical-align: middle;
  border-style: none
}

article,
aside,
figcaption,
figure,
footer,
header,
hgroup,
main,
nav,
section {
  display: block
}

b,
strong {
  font-weight: bolder
}

table {
  border-collapse: collapse
}

body {
  font-size: 14px
}

.container {
  max-width: 215.9mm;
  height: 355.6mm;
  padding: 10px 30px
}

.header {
  position: relative;
  margin-bottom: 20px
}

.header img {
  width: 50px;
  height: auto;
  margin: auto
}

.header p {
  font-size: 9px;
  /*position: absolute;*/
  top: 77%;
  left: 34%;
  font-weight: 600;
  letter-spacing: 1px
}

.main-header {
  margin-bottom: 10px;
  position: relative
}

.main-title {
  font-family: "Old English Text MT", "Arial Narrow", Arial, sans-serif;
  font-weight: 500
  text-align: center;
}

.main-subtitle p{
  font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;
  font-size: 10px
}

.main-subtitle span {
  font-size: 12px;
  position: absolute;
  bottom: 0;
  right: 30px
}

.table {
  font-size: 9px
}

.table-student th:nth-child(1) {
  width: 90px
}

.table-student th:nth-child(3) {
  width: 80px
}

.table-student th:nth-child(4) {
  width: 60px
}

.table-student th:nth-child(5) {
  width: 130px
}

.table-student th:nth-child(6) {
  width: 130px
}

.table-student td {
  height: 20px
}

.table-courses {
  width: 58.33333%;
  margin-right: 10px
}

.table-courses td:nth-child(1) {
  width: 50px;
  padding-left: 5px
}

.table-courses td:nth-child(2) {
  padding-left: 5px
}

.table-courses td:nth-child(3) {
  width: 30px
}

.table-courses td:nth-child(4) {
  width: 5px
}

.table-courses td:nth-child(5) {
  width: 30px
}

.table-assessment td:nth-child(1) {
  padding-left: 10px
}

.table-payment td:nth-child(1) {
  width: 130px;
  padding-left: 40px
}

.table-payment td:nth-child(2) {
  width: 150px
}

.table-payment td:nth-child(3) {
  text-align: right;
  width: 70px
}

.table-payment td:nth-child(4) {
  text-align: center;
  width: 50px
}

.table-payment td:nth-child(5) {
  text-align: center;
  width: 140px
}

.table-total td {
  padding-right: 5px
}

.table-total td:nth-child(1) {
  width: 160px
}

.table-total td:nth-child(2) {
  width: 100px
}

.table-total td:nth-child(4) {
  width: 100px
}

.table-total td:nth-child(5) {
  width: 100px
}

.table-total td:nth-child(6) {
  width: 90px
}

.table-signature td:nth-child(2) {
  width: 150px
}

.border-top {
  border-top: 1px solid #2a2a2a
}

.border-bottom2x {
  border-bottom: 2px solid #2a2a2a
}

.border-bottom-double5x {
  border-bottom: 5px double #2a2a2a
}

.v-top {
  vertical-align: top
}

.v-bottom {
  vertical-align: bottom
}

.d-flex {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex
}

.w-5 {
  width: 5px
}

.w-50 {
  width: 50px
}

.pr-5 {
  padding-right: 5px
}

.pl-5 {
  padding-left: 5px
}

.h-5 {
  height: 10px
}

.h-30 {
  height: 30px
}

.letter-spacing-5 {
  letter-spacing: 5px
}

.clearfix {
  overflow: auto;
  position: absolute;
  padding-left: 170px;
}

@page {
  size: letter;
  margin: 0
}

@media print {
  .container {
    margin: auto;
    width: auto;
    height: auto;
    padding-bottom: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    page-break-after: always
  }
  body {
    -webkit-print-color-adjust: exact !important
  }
  html,
  body {
    height: 100%
  }
}
</style>
<title>S.O.A.</title>

<?php
use App\Modules\Accounting\Services\Assessment\assessment as assess;
    $id = decode(Request::get('ref'));
		$term = decode(Request::get('term'));
		$studentno = Request::get('studno');
    $regid = decode(Request::get('regid'));
    $temp = 0;
		$total_balance= 0;
		$tuitionfee_bal =0;
		$bal =  DB::select(DB::raw("call  ES_GetOutstandingBalanceSummary('1','".$studentno."','1')"));
		foreach ($bal as $key => $value) {
				$total_balance += $value->Balance;
		}

		$studentinfo = DB::table('es_students as s')
												->leftJoin('es_registrations as r','r.StudentNo','=','s.StudentNo')
												->selectRaw('r.*, s.*,
														fn_AcademicYear(r.TermID) AS SchoolYear,
														fn_StudentName4(r.StudentNo) As StudentName, fn_SectionName(ClassSectionID) As Section ,
						                            fn_AcademicYearTerm(r.TermID) As Term, fn_ProgramName(r.ProgID) As Program, fn_MajorName(r.MajorID) As Major,
						                            fn_K12_YearLevel(r.YearLevelID) As YearLevel,fn_TemplateCode(TableofFeeID) As Template,
                                        fn_ScholarProviderName (r.SchoProviderID) AS ScholarName
												')
												->where('r.StudentNo', $studentno)
												->where('r.RegID', $reg)
												->first();
		$assess = new assess;
		$keyid = $assess->exec_penalties($term, $studentno, $reg, date('Y-m-d'));
		$penalties = DB::table('ES_SOA_GSHS_Penalties as P')
								->selectRaw("P.EntryID,
            P.RegID,
            P.SeqNo,
            P.Checked,
            P.CutDate,
            P.PenaltyMonthCount,
            P.Scheme,
            P.Payment,
            P.Excess,
            P.Balance,
            P.Penalty,
            P.TotalAmountDue,
            P.TotalPenalty,
            P.TotalBalanceToDate,
            IFNULL(( SELECT SchoProviderType
                     FROM   ES_Registrations R
                     WHERE  R.RegID = P.RegID
                   ), 0) AS SchoProviderType,
            IFNULL(( SELECT
                        Debit-Credit
              FROM      ES_SOA_GSHS
              WHERE     KeyID = KeyID
                        AND StudentNo = (select StudentNo from es_registrations where RegID = P.RegID limit 1)
                        AND TransID = '0' LIMIT 1   ), 0) AS BeginningBalance
							")
								->where('KeyID',$keyid)
								->get();




    $payment = DB::table('ES_Journals as J')
              ->selectRaw(" J.TransID, SUM( J.Credit ) AS Payment,
                            ( CASE TransID WHEN 20 THEN ReferenceNo ELSE 'CM #' + ReferenceNo END ) AS ORNumber,
                            J.TransDate AS DateofPayment,
                            BC.CheckNo,
                            BC.CheckDate,
                            BC.CheckBank,
                            SUM( BC.CheckAmount ) AS CheckAmount ")
              ->LeftJoin("ES_BankChecks AS BC","BC.ORNo", "=", "J.ReferenceNo")
              ->whereIn('J.TransID',[20,60])
              ->where('J.IDType', 1)
              ->where('J.IDNo', $studentno)
              ->where('J.TermID', $term)
              ->whereRaw('IFNULL(Case WHEN TransID = 60 THEN (SELECT POsted from ES_DebitCreditMemo WHERE RefNo = J.ReferenceNo) ELSE 1 END,0) > 0  ')
              ->groupBy(['TransID', "ReferenceNo" , "J.TransDate", "BC.CheckNo","BC.CheckDate","BC.CheckBank"])
              ->limit(100)
              ->orderBy('DateofPayment','ASC')
              ->get();           

		


    $date = systemDate();
    $template = "";
    $provider = "";

		$page_title =" STATEMENT OF ACCOUNT";
    $header = DB::table("es_institution")->first();
    $title = $header->Report_InstitutionName;
    $address = $header->Report_CompleteAddress;


?>



 <body>
    <div class="container">
   
       <header class="header" style="margin-bottom: 15px; margin-top: 10px">
          <!-- Header logo -->
          <div class="clearfix">
            <img src="{{url('general/getThemePhoto?tid=1&field=header_logo')}}" alt="Logo" />
       
          </div>
      
          <!-- Header text group -->

          <div class="main-title text-center">
              <div class="header__title--top">
                  <h2>{{$title}} </h2>
                 
                </div>

                   <div class="main-subtitle">
                     <p> {{$address}} </p>
                  </div>
           </div>
        </header>


    

      <main class="main">
        <div class="main-header">
          <h2 class="main-subtitle text-center">STATEMENT OF ACCOUNT</h2>
          <h5 class="main-subtitle text-center">
            As of {{date("F j, Y")}}
            <span>Reg. # {{$regid}}</span>
          </h5>
        </div>

        <table class="table table-student text-center">
          <thead>
            <tr class="border-top border-bottom2x">
              <th>Student #</th>
              <th>Student Name</th>
              <th>Program</th>
              <th>Yr. Level</th>
              <th>Class Section</th>
              <th>Academic Yr. & Term</th>
            </tr>
          </thead>
          <tbody>
            <tr class="border-bottom-double5x">
              <td>{{$studentinfo->StudentNo}}</td>
              <td>{{$studentinfo->StudentName}}</td>
              <td>{{$studentinfo->Program}}</td>
              <td>{{$studentinfo->YearLevel}}</td>
              <td>{{$studentinfo->Section}}</td>
              <td>{{$studentinfo->SchoolYear}}</td>
            </tr>
          </tbody>
        </table>

        <div class="d-flex">
          <table class="table table-courses">
            <thead>
              <tr class="text-center h-30">
                <th colspan="5" class="border-bottom2x">ENROLLED COURSES</th>
              </tr>
              <tr class="text-center v-top">
                <th rowspan="2" class="border-bottom-double5x">
                  Code
                </th>
                <th rowspan="2" class="border-bottom-double5x">
                  Descriptive Title
                </th>
                <th colspan="3" class="letter-spacing-5">
                  UNIT
                </th>
              </tr>

              <tr class="text-center border-bottom-double5x">
                <th>Lec</th>
                <th></th>
                <th>Lab</th>
              </tr>
            </thead>
            <tbody>

              <!-- STUDENT REGISTERED SUBJECTS -->
              <?php
                $subj = DB::select("call ES_GetStudentRegistration_Subjects({$regid})");
                $totalcreditunit = 0;
                $totallab = 0;
                $totallec = 0;
              ?>

               @foreach ($subj as $key => $v)
                <?php
                $totalcreditunit += $v->CreditUnits;
                $totallab += $v->LabUnits;
                $totallec += $v->LectHrs;
                 ?>

                 <tr>
                  <td>{{$v->SubjectCode}}</td>
                  <td>{{$v->SubjectTitle}}</td>
                  <td class="text-center">{{$v->LectHrs}}</td>
                  <td></td>
                  <td class="text-center">{{$v->LabUnits}}</td>
                </tr>
               @endforeach  

            </tbody>
            <tfoot>
              <tr class="h-30 v-bottom">
                <td colspan="5">
                  <i>Note: Subject marked with "*" is Special Subject.</i>
                </td>
              </tr>
            </tfoot>
          </table>

          <!-- ASSESSMENT HERE -->
          <?php  $assessment = DB::select(DB::raw("call  ES_rptRegAssessment_Accounts_r2('".$reg."','".$studentno."','".$term."')")); 
            $discount = 0;
            $net = 0;
            $subtotal = 0;
            $subtotal2 = 0;
            $subtotalpayment = 0;
            $subtotalbal = 0;
            $totalassessfee = 0;
            $totalfinaid = 0;
            $totalnet = 0;
            $totalpayment = 0;
            $totalbal = 0;
            $prelim = 0;
            $midterm = 0;
            $semifinal = 0;
            $final = 0;
            $sembal = 0;



          ?>

          <table class="table table-assessment">
            <thead>
              <tr>
                <th colspan="9" class="text-center h-30 border-bottom2x letter-spacing-5">
                  <h3>ASSESSMENT</h3>
                </th>
              </tr>
              <tr class="border-bottom-double5x">
                <th>Account Name</th>
                <th class="text-right pr-5">Assessed</th>
                <th colspan="2" class="text-center">
                  Financial
                  <br />
                  Aid
                </th>
                <th colspan="2" class="text-center">
                  Net
                  <br />
                  Assessed
                </th>
                <th colspan="2" class="text-center">
                  Payment
                </th>
                <th class="text-center">
                  Balance
                </th>
              </tr>
              <tr>
                <th colspan="9">
                  <u>
                    ENROLLMENT
                  </u>
                </th>
              </tr>
            </thead>
            <tbody>

              @foreach($assessment as $key => $r)

              <?php 
                $discount = $r->Discount + $r->CreditMemo;
                $net = $r->AssessFee - $discount;
                $subtotal += $r->AssessFee;
                $subtotal2 += $net;
                $subtotalpayment += $r->ActualPayment;
                $subtotalbal += $r->Balance;

                $totalassessfee = $subtotal;
                $totalfinaid += $r->Discount;
                $totalnet = $subtotal2;
                $totalpayment = $subtotalpayment;
                $totalbal = $subtotalbal;

                $prelim = ($totalnet - $totalpayment)/4;
                $sembal = $totalnet - $totalpayment;

            

               ?>


               <tr>
                  <td> {{$r->AcctName}}</td>
                  <td class="text-right w-50 pr-5">{{number_format($r->AssessFee,2,'.',',')}}</td>
                  <td class="w-5"></td>
                  <td class="text-right w-50 pr-5">{{number_format($r->Discount,2,'.',',') > 0 ? number_format($r->Discount,2,'.',',') : '-'}}</td>
                  <td class="w-5"></td>
                  <td class="text-right w-50 pr-5">{{$net > 0 ? number_format($net,2,'.',',') : '-'}}</td>
                  <td class="w-5"></td>
                  <td class="text-right w-50 pr-5">{{(number_format($r->ActualPayment,2,'.',',')) > 0 ? (number_format($r->ActualPayment,2,'.',','))  : '-' }}</td>
                  <td class="text-right w-50 pr-5">{{(number_format($r->Balance,2,'.',',')) > 0 ? (number_format($r->Balance,2,'.',','))  : '-' }}</td>
              </tr> 
              @endforeach
              
            </tbody>
            <tfoot>
              <tr class="text-right h-30 v-top">
                <td class="pr-5">
                  <strong>
                    <i>
                      SUB TOTAL:
                    </i>
                  </strong>
                </td>
                <td class="pr-5 border-top border-bottom">
                  {{number_format($subtotal,2,'.',',')}}
                </td>
                <td></td>
                <td class="pr-5 border-top border-bottom">
                  {{number_format($totalfinaid,2,'.',',') > 0 ? number_format($totalfinaid,2,'.',',') : '-'}}
                </td>
                <td></td>
                <td class="pr-5 border-top border-bottom">
                {{number_format($subtotal2,2,'.',',')}}
                </td>
                <td></td>
                <td class="pr-5 border-top border-bottom">
                {{number_format($subtotalpayment,2,'.',',')}}
                </td>
                <td class="pr-5 border-top border-bottom">
                 {{number_format($subtotalbal,2,'.',',')}}
                </td>
              </tr>

              <tr class="text-right h-30 v-top">
                <td class="pr-5">
                  <strong>
                    <i>
                      TOTAL:
                    </i>
                  </strong>
                </td>
                <td class="pr-5 border-bottom-double5x">
                    {{number_format($totalassessfee,2,'.',',')}}
                </td>
                <td></td>
                <td class="pr-5">
                  -
                </td>
                <td></td>
                <td class="pr-5">
                    {{number_format($totalnet,2,'.',',')}}
                </td>
                <td></td>
                <td class="pr-5">
                  {{number_format($totalpayment,2,'.',',')}}
                </td>
                <td class="pr-5">
                  {{number_format($totalbal,2,'.',',')}}
                </td>
              </tr>

              <tr class="h-30">
                <td class="text-right pr-5">
                  <strong>
                    SCHOLARSHIP:
                  </strong>
                </td>
                <td colspan="8" class="pl-5">{{$studentinfo->ScholarName}}</td>
              </tr>
            </tfoot>
          </table>
        </div>

        <table class="table table-payment">
          <thead>
            <tr>
              <th colspan="6" class="text-center border-bottom">
                PAYMENT & INFORMATION
              </th>
            </tr>
            <tr class="border-bottom2x">
              <th class="text-center">O.R Number</th>
              <th>Date of Payment</th>
              <th class="text-right">Amount</th>
              <th class="text-center">Mode</th>
              <th class="text-center">Check #</th>
              <th>Bank-Branch</th>
            </tr>
          </thead>
          <tbody class="border-bottom2x">

          @foreach($payment as $key => $p) 
 
            <tr>
              <td>{{$p->ORNumber}}</td>
              <td>{{$p->DateofPayment}}</td>
              <td>{{number_format($p->Payment,2,'.',',')}}</td>
              <td>@if (is_null($p->CheckNo))
                      CASH
                   @else CHECK
                   @endif
              </td>
              <td>{{$p->CheckNo}}</td>
              <td>{{$p->CheckBank}}</td>
            </tr>

            @endforeach
          </tbody>
        </table>

        <table class="table table-total">
          <tbody class="text-right">
            <tr>
              <td>
                <strong>Total Assessment:</strong>
              </td>
              <td>{{number_format($totalassessfee,2,'.',',')}}</td>
              <td>
                <strong>
                  Total Payment:
                </strong>
              </td>
              <td> {{number_format($totalpayment,2,'.',',')}}</td>
              <td>
                <strong>
                  Pre-lim
                </strong>
              </td>
              <td>{{number_format($prelim,2,'.',',')}}</td>
            </tr>

            <tr>
              <td>
                <strong>Total Financial Aid:</strong>
              </td>
              <td class="border-bottom"> {{number_format($totalfinaid,2,'.',',') > 0 ? number_format($totalfinaid,2,'.',',') : '-'}} </td>
              <td>
                <strong>
                  Total Discount:
                </strong>
              </td>
              <td>{{$studentinfo->TotalDiscount}}</td>
              <td>
                <strong>
                  Midterm
                </strong>
              </td>
              <td>{{number_format($prelim,2,'.',',')}}</td>
            </tr>

            <tr>
              <td>
                <strong>Net Assessed:</strong>
              </td>
              <td class="border-bottom-double5x"> {{number_format($totalnet,2,'.',',')}}</td>
              <td>
                <strong>
                  Total Non-Ledger Account:
                </strong>
              </td>
              <td>{{$studentinfo->TotalNonLedger}}</td>
              <td>
                <strong>
                  Semi-final
                </strong>
              </td>
              <td>{{number_format($prelim,2,'.',',')}}</td>
            </tr>

            <tr>
              <td colspan="3">
                <strong>
                  Total Forwarded Balance:
                </strong>
              </td>
              <td class="border-bottom-none">{{number_format($studentinfo->TotalPreviousBalance,2,'.',',')}}</td>
              <td>
                <strong>
                  Final
                </strong>
              </td>
              <td class="border-bottom">{{number_format($prelim,2,'.',',')}}</td>
            </tr>

            <tr>
              <td colspan="3">
              <!--   <strong>
                  Outstanding Balance:
                </strong> -->
              </td>
              <td class="border-bottom-double5x-none"></td>
              <td> <strong>
                  Outstanding Balance:
                </strong></td>
              <td class="border-bottom-double5x">{{number_format($sembal,2,'.',',')}}</td>
            </tr>

            <tr class="h-5">
              <td class="border-bottom2x" colspan="6"></td>
            </tr>
          </tbody>
        </table>

        <table class="table table-signature">
          <tbody>
            <tr class="h-30 v-top">
              <td>Prepared By:</td>
              <td></td>
              <td>Certified True and Correct:</td>
            </tr>

            <tr class="text-center">
              <td class="border-bottom">
               {{getUserFullName()}}
              </td>
              <td></td>
              <td class="border-bottom"></td>
            </tr>

            <tr class="text-center">
              <td>Assessment</td>
              <td></td>
              <td></td>
            </tr>

            <tr>
              <td>Date Printed:{{ date("F j, Y, g:i a")}}</td>
              <td></td>
              <td class="text-right">Page 1 of 1</td>
            </tr>
          </tbody>
        </table>
      </main>
    </div>
  </body>

  <script>
    //window.onload = window.print();
  </script>