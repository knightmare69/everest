<style>
	.border-bottom { border-bottom: 1px solid #000 }
    .border-total { border-bottom: 2px solid #000; border-top: 1px solid #000; border-bottom-style:double;  }
    .border-header { border-bottom: 1px solid #000; border-top: 1px solid #000; }
    .border-bottom tr > th{ border-bottom: 1px solid #000 }
    .border-top { border-top: 1px solid #000 }
    .border-top tr > td{ border-top: 1px solid #000  }
	.border { border: 0.5px solid #000 }
	.right { text-align: right; }
	.bold { font-weight: bold; }
	.center { text-align: center; }
	.left { text-align: left; }
	.valign { vertical-align: middle; }
	.bg { background-color: #EDEBE0; }
    .bg-grey {background-color: #e5e5e5 !important;}
	.pad-5 { padding: 10px; }
    .autofit{width:1%;white-space:nowrap !important;}
    .font-xs{ font-size: 8px; }
    .font-sm{ font-size: 10px; }
    .font-md{ font-size: 12px; }
    .pull-left { left: auto; }
    .ucase { text-transform: uppercase !important; }
    .info { font-size: 13px; }

    body{
         width: 210mm; height: 297mm;
        /*width: 279.4mm; height: 215.9mm;*/
    }
		.red{
			color : red;
		}
		.blue{
			color : blue;
		}
</style>

<?php


    $header =  DB::table("es_institution")->first();
    $republic = 'Republic of the Philippines';
    $institution_name = $header->Report_InstitutionName;
    $campus_address = $header->Report_CompleteAddress;

    $ref = Request::get('ref');
    $stud_name = Request::get('studname');
    $stud_no = Request::get('studno');
    $transdate = Request::get('transdate');

    $rs = DB::table(DB::raw("es_financialtransactions FT"))
        ->selectRaw("
        now() AS DatePrinted,
		'' AS ComputerName,
		fn_AcademicYearTerm(J.TermID)AS AcademicYearTerm,
		FT.TransCode,
		FT.TransName,
		J.ReferenceNo,
		J.IDNo,
		'{$stud_name}' AS PayorName,
		J.`TransDate`, A.AcctCode,
		A.AcctName,
		A.ShortName,
		J.Debit,
		J.`1st Payment`, J.`2nd Payment`, J.`3rd Payment`, J.`4th Payment`, J.`5th Payment`,(
			J.Debit -(
				J.ActualPayment + J.CreditMemo +(
					CASE
					WHEN J.transID = 0
					AND J.Credit <> 0 THEN
						J.Credit
					ELSE
						0
					END
				)
			)
		)AS Balance,
		(
			J.ActualPayment + J.CreditMemo +(
				CASE
				WHEN J.transID = 0
				AND J.Credit <> 0 THEN
					J.Credit
				ELSE
					0
				END
			)
		)AS Credit,
		J.Discount AS FinancialAid,
		J.`Assess Fee`, J.PaymentDiscount,
		J.`CreditMemo`, A.AcctOption,
		AG.GroupID,
		AG.GroupCode,
		AG.GroupName,
		AG.GroupShort,
		AC.ClassCode,
		AC.ClassName,
		AC.ClassShort,
		AC.`ClassSort`, J.Remarks AS Remarks,
		J.EntryID,
		J.AccountID,
		J.DMCMRefNo,
		J.Credit AS CreditSide")
		->join(DB::raw('es_journals J'),'FT.TransID','=','J.TransID','right outer')
		->join(DB::raw('es_accounts A'),'J.AccountID','=','A.AcctID','right outer')
		->join(DB::raw('es_accountsclass AC'),'AC.ClassID','=','A.ClassID','left outer')
		->join(DB::raw('es_accountgroups AG'), 'A.GroupID','=','AG.GroupID', 'left outer')
        ->whereRaw(" J.TransID = 1 AND J.ReferenceNo = '{$ref}'
				AND J.IDType = 1
				AND J.IDNo = '{$stud_no}'
				AND J.TransDate <= '{$transdate}'
				AND J.Debit <> 0
				AND J.DMCMRefNo= '' ")
           ->orderby("J.SeqNo")
           ->orderby("J.EntryID")
           ->get();

    $page_title = "ASSESSMENT";
    $header = DB::table("es_institution")->first();
    $title = $header->Report_InstitutionName;
    $address = $header->Report_CompleteAddress;

    $date = systemDate();
    $temp = "";
    $total = array(
         0 => '0'
        ,1 => '0'
        ,2 => '0'
        ,3 => '0'
        ,4 => '0'
        ,5 => '0'
    );

	$data_ = json_decode(json_encode($rs), true);
 	$data = array();
 	$data2= array();

    foreach ($data_ as $c) {
		array_push($data2, $c);
	}
	$data = array_group_by($data2, "GroupName");

    foreach ($data2 as $i) {
		$term = $i['AcademicYearTerm'];
		$id_no = $i['IDNo'];
		$payor_name = $i['PayorName'];
		$ref_no = $i['ReferenceNo'];
		$trans_date = $i['TransDate'];
		$trans_name = $i['TransName'];
    };
?>


{{-- HEADER BEGIN --}}
<title>{{$page_title}}</title>
<table style="width: 100%;">
	<tr>
        <td class="center">{{$republic}}</td>
    </tr>
    <tr>
        <td class="center bold">{{$institution_name}}</td>
    </tr>
    <tr>
        <td class="info center">{{$campus_address}}</td>
    </tr>
    <tr>
        <td class="center bold">&nbsp;</td>
    </tr>
    <tr>
        <td class="center bold">{{$page_title}}</td>
    </tr>
    <tr>
        <td class="info center" style="font-weight: bold;text-decoration: underline;">{{$term}} </td>
    </tr>
</table>
{{-- HEADER END --}}

<table style="width: 100%;">
	<tr>
        <td class="info bold">ID No:</td>
        <td class="info">{{$id_no}}</td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td class="info bold">Name:</td>
        <td class="info">{{$payor_name}}</td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td class="info bold">Reference No:</td>
        <td class="info">{{$ref_no}}</td>
        <td class="info bold">Date:</td>
        <td class="info">{{date_format(date_create($trans_date),"F d, Y h:i:s A")}}</td>
    </tr>
    <tr>
        <td class="info bold">Transaction:</td>
        <td class="info">{{$trans_name}}</td>
        <td colspan="2"></td>
    </tr>
</table>


<br />
<br />
<table width="100%" class="font-md">
    <thead>
        <tr border="1">
            <th class="border-bottom border-top">CODE</th>
            <th class="border-bottom border-top">ACCOUNT NAME</th>
            <th class="border-bottom border-top">REMARKS</th>
            <th class="border-bottom border-top">ASSESTMENT</th>
            <th class="border-bottom border-top">FINANCIAL AID</th>
            <th class="border-bottom border-top">NET ASSESSED</th>
        </tr>
    </thead>
    <tbody>
		<?php $gt_assess_fee = 0; $gt_financial_aid = 0; $gt_debit = 0; ?>
			@foreach($data as $d => $v)
			<tr>
				<td  colspan= '12'><b>	{{$d}}</b></td>
			</tr>
			<?php $assess_fee = 0; $financial_aid = 0; $debit = 0; ?>
			@foreach($v as $c)
				<?php 
					$assess_fee = $assess_fee + $c['Assess Fee']; 
					$financial_aid = $financial_aid + $c['FinancialAid']; 
					$debit = $debit + $c['Debit']; 
				?>
				<tr nobr="true" >
					<td>{{$c['AcctCode']}}</td>
					<td>{{$c['AcctName']}}</td>
					<td class="center">{{$c['Remarks']}}</td>
					<td class="right">{{$c['Assess Fee'] <> 0 ? number_format($c['Assess Fee'],2) : '-'}}</td>
					<td class="right">{{$c['FinancialAid'] <> 0 ? number_format($c['FinancialAid'],2) : '-'}}</td>
					<td class="right">{{$c['Debit'] <> 0 ? number_format($c['Debit'],2) : '-'}}</td>
				</tr>
			@endforeach
			<tr>
				<td colspan='2'></td>
				<td class="center" style="font-style: italic;">	<b>Sub-total:</b></td>
				<td style="border-top: 1px solid;" class="right"><b>{{$assess_fee <> 0 ? number_format($assess_fee,2): '-'}}</b></td>
				<td style="border-top: 1px solid;" class="right"><b>{{$financial_aid <> 0 ? number_format($financial_aid,2): '-'}}</b></td>
				<td style="border-top: 1px solid;" class="right"><b>{{$debit <> 0 ? number_format($debit,2): '-'}}</b></td>
			</tr>
			<?php 
				$gt_assess_fee = $gt_assess_fee + $assess_fee; 
				$gt_financial_aid = $gt_financial_aid + $financial_aid; 
				$gt_debit = $gt_debit + $debit; 
			?>
		@endforeach
		<tr>
			<td colspan= '2'></td>
			<td  class="center" style="font-style: italic;"><b>Grand Total:</b></td>
			<td style="border-top: 1px solid;border-bottom-style:double;" class="right"><b>{{$gt_assess_fee <> 0 ? number_format($gt_assess_fee,2): '-'}}</b></td>
			<td style="border-top: 1px solid;border-bottom-style:double;" class="right"><b>{{$gt_financial_aid <> 0 ? number_format($gt_financial_aid,2): '-'}}</b></td>
			<td style="border-top: 1px solid;border-bottom-style:double;" class="right"><b>{{$gt_debit <> 0 ? number_format($gt_debit,2): '-'}}</b></td>
		</tr>
    </tbody>
</table>
<script>
    window.onload=window.print();
</script>


