<style>
	.border-bottom { border-bottom: 1px solid #000 }
    .border-total { border-bottom: 2px solid #000; border-top: 1px solid #000; border-bottom-style:double;  }
    .border-header { border-bottom: 1px solid #000; border-top: 1px solid #000; }
    .border-bottom tr > th{ border-bottom: 1px solid #000 }
    .border-top { border-top: 1px solid #000 }
    .border-top tr > td{ border-top: 1px solid #000  }
	.border { border: 0.5px solid #000 }
	.right { text-align: right; }
	.bold { font-weight: bold; }
	.center { text-align: center; }
	.left { text-align: left; }
	.valign { vertical-align: middle; }
	.bg { background-color: #EDEBE0; }
    .bg-grey {background-color: #e5e5e5 !important;}
	.pad-5 { padding: 10px; }
    .autofit{width:1%;white-space:nowrap !important;}
    .font-xs{ font-size: 8px; }
    .font-sm{ font-size: 10px; }
    .font-md{ font-size: 12px; }
    .pull-left { left: auto; }
    .ucase { text-transform: uppercase !important; }
    .info { font-size: 13px; }
    .header-report{font-family: "Calibri";}

    body{
         width: 210mm; height: 297mm;
        /*width: 279.4mm; height: 215.9mm;*/
    }
		.red{
			color : red;
		}
		.blue{
			color : blue;
		}
</style>

<?php
    $header =  DB::table("es_institution")->first();
    $republic = 'Republic of the Philippines';
    $institution_name = $header->Report_InstitutionName;
    $campus_address = $header->Report_CompleteAddress;

    $ref = Request::get('ref');

    $rs = DB::table(DB::raw("es_accountsclass AC"))
        ->selectRaw("
		'Everest Academy Manila' AS InstitutionName,
		'' AS Republic,
		'Philippine' AS CampusAddress,
		'Main Campus' AS Campus,
		GETDATE() AS DatePrinted,
		'' AS ComputerName,
		J.Credit,
		A.AcctCode,
		A.AcctName,
		dbo.fn_EmployeeName(OFR.CashierID)AS CashierName,
		FT.TransName,
		dbo.fn_AcademicYearTerm(J.TermID)AS AcademicYearTerm,
		OFR.ORNo,
		OFR.[Date],
		OFR.PayorName,
		OFR.TransType,
		OFR.RefNo,
		OFR.CashReceive,
		OFR.CheckReceive,
		OFR.CardReceive,
		OFR.CHANGE,
		OFR.CardTypeID,
		OFR.CardTypeName,
		OFR.CardApprovalNo,
		OFR.CardTypeID2,
		OFR.CardTypeName2,
		OFR.CardApprovalNo2,
		OFR.AmountinWords,
        OFR.CreatedBy")
		->join(DB::raw('es_accounts A'),'AC.ClassID','=','A.ClassID','right outer')
		->join(DB::raw('es_accountgroups AG'),'A.GroupID','=','AG.GroupID','left outer')
		->join(DB::raw('es_journals J'),'A.AcctID','=','J.AccountID','left outer')
		->join(DB::raw('es_officialreceipts OFR'),'J.ReferenceNo','=','OFR.ORNo','left outer')
		->join(DB::raw('es_financialtransactions FT'),'OFR.TransType','=','FT.TransID','right outer')
        ->whereRaw("J.TransID = 20
			AND J.Credit > 0
			AND OFR.ORNo = '{$ref}'")
        ->get();

    $page_title = "CERTIFICATE OF PAYMENT";
    $header = DB::table("es_institution")->first();
    $title = $header->Report_InstitutionName;
    $address = $header->Report_CompleteAddress;

    $date = systemDate();
    $temp = "";
    $total = array(
         0 => '0'
        ,1 => '0'
        ,2 => '0'
        ,3 => '0'
        ,4 => '0'
        ,5 => '0'
    );

	$data_ = json_decode(json_encode($rs), true);
 	$data = array();
 	$data2= array();

    foreach ($data_ as $c) {
		array_push($data2, $c);
	}
	$data = array_group_by($data2, "ORNo");

    foreach ($data2 as $i) {
		$or_date = $i['Date'];
		$ref_no = '';
		$ref_no = $i['TransName'].'  #'.str_replace(",","",substr("0000000000",10-strlen(str_replace(".00","",$i['RefNo']))).str_replace(".00","",$i['RefNo']));
		$ay_term = $i['AcademicYearTerm'];
		$payor_name = $i['PayorName'];
		$amount_in_words = $i['AmountinWords'];
		$cash_receive = $i['CashReceive'];
		$check_receive = $i['CheckReceive'];
		$change = $i['CHANGE'];
		$card_receive = $i['CardReceive'];
		$card_info = '';
		$card_info = $i['CardTypeID'] == 0 ? '' : $i['CardTypeName'].'-'.$i['CardApprovalNo'];
		$card_info .= $i['CardTypeID2'] <> 0 ? ' / '.$i['CardTypeName2'].'-'.$i['CardApprovalNo2'] : '';
		
    };
 
    $cashier_name= getFullNameByID($rs[0]->CreatedBy);
?>

{{-- HEADER BEGIN --}}
<title>{{$page_title}}</title>
<table style="width: 100%;">
	<tr>
        <td class="center">{{$republic}}</td>
    </tr>
    <tr>
        <td class="center bold header-report">{{$institution_name}}</td>
    </tr>
    <tr>
        <td class="info center">{{$campus_address}}</td>
    </tr>
    <tr>
        <td class="center bold">&nbsp;</td>
    </tr>
    <tr>
        <td class="center bold">{{$page_title}}</td>
    </tr>
</table>
{{-- HEADER END --}}
<br />
<table style="width: 100%;">
	<tr> 
        <td colspan="2" class="info bold" style="color: #800000;">{{date_format(date_create($or_date),"m/d/Y h:i A")}}</td>
        <td style="font-weight: bold;font-size: 18px;">No.</td>
        <td style="font-weight: bold;font-size: 18px;color: #800000;">{{$ref}}</td>
    </tr>
    <tr>
        <td class="info bold"><pre>Payor:			{{$payor_name}}</pre></td>
        <td colspan="3" class="info"></td>
    </tr>
    <tr>
        <td class="info bold"><pre>Payment For:		{{$ref_no}}</pre></td>
        <td colspan="3" class="info"></td>
    </tr>
    <tr>
        <td class="info bold"><pre>			{{$ay_term}}</pre></td>
        <td colspan="3" class="info"></td>
    </tr>
</table>

<br />
<table width="100%" class="font-md">
    <thead>
        <tr bgcolor="#D3D3D3" border="1">
            <th width="5%" class="border-top">ACCOUNT NAME</th>
            <th width="2.5%" class="border-top">CODE</th>
            <th width="2.5%" class="border-top">AMOUNT</th>
        </tr>
    </thead>
    <tbody>
	<?php $gt_credit = 0; ?>
	@foreach($data as $d => $v)
		@foreach($v as $c)
		<tr nobr="true" >
			<td>{{$c['AcctName']}}</td>
			<td class="center">{{$c['AcctCode']}}</td>
			<td class="right">{{$c['Credit'] <> 0 ? number_format($c['Credit'],2) : '-'}}</td>
		</tr>
		<?php $gt_credit = $gt_credit + $c['Credit']; ?>
		@endforeach
	@endforeach
		<tr>
			<td></td>
			<td class="center"><b>TOTAL:</b></td>
			<td style="border-top: 1px solid;border-bottom-style:double;" class="right"><b>{{$gt_credit <> 0 ? number_format($gt_credit,2): '-'}}</b></td>
		</tr>
		<tr>
        	<td colspan='3' class="center bold">&nbsp;</td>
    	</tr>
		<tr>
			<td colspan='3' class="center">*** {{$amount_in_words}} ***</td>
		</tr>
		<tr>
        	<td colspan='3' class="center bold">&nbsp;</td>
    	</tr>
    	<tr>
        	<td colspan='3' class="center bold border-top" bgcolor="#D3D3D3">PAYMENT INFORMATION</td>
    	</tr>
    </tbody>
</table>
<table style="width: 50%;" class="font-md">
	<tr>
        <td><b>Cash:</b></td>
        <td class="right"><b>{{number_format($cash_receive,2)}}</b></td>
        <td class="center"><pre>	<b>Change:</b></pre></td>
        <td class="right"><b>{{number_format($change,2)}}</b></td>
    </tr>
    <tr>
        <td><b>Check:</b></td>
        <td class="right"><b>{{number_format($check_receive,2)}}</b></td>
        <td class="center"><pre>	<b>&nbsp;</b></pre></td>
        <td></td>
    </tr>
     <tr>
        <td><b>Card:</b></td>
        <td class="right"><b>{{number_format($card_receive,2)}}</b></td>
        <td colspan='2' class="left"><pre>	<b>{{$card_info}}</b></pre></td>
    </tr>
</table>
<table style="width: 100%;" class="font-md">
 	<tr>
        <td colspan='3' class="border-top"></td>
    </tr>
    <tr>
        <td colspan='3'class="center bold">&nbsp;</td>
    </tr>
 	<tr>
        <td colspan='3'class="center bold">&nbsp;</td>
    </tr>
    <tr>
        <td class="center border-bottom">{{$cashier_name}}</td>
        <td class="center bold"><pre>&nbsp;		</pre></td>
        <td class="center border-bottom"></td>
    </tr>
     <tr>
        <td class="center"><b>Collecting Officer</b></td>
        <td class="center bold"><pre>&nbsp;		</pre></td>
        <td class="center"><pre><b> Certified By </pre></b></td>
    </tr>
</table>

<script>
    window.onload=window.print();
</script>