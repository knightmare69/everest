<style>
    .border-bottom { border-bottom: 1px solid #000 }
    .border { border: 0.5px solid #000 }
    .right { text-align: right; }
    .bold { font-weight: bold; }
    .center { text-align: center; }
    .left { text-align: left; }
    .valign { vertical-align: middle; }
    .bg { background-color: #EDEBE0; }
    .pad-5 { padding: 10px; }
    .font-large { font-size: larger; }
    .lborder{
        border-right-width:0.1px;
        border-bottom-width: 0.1px;
     }
</style>
<?php



    $teacher = "";

    $j=0;
    $i=0;
    $temp = 0;
    $total = 0;

    $model = new \App\Modules\ClassRecords\Models\ReportCardDetails_model;
    $dbcon = new \App\Modules\ClassRecords\Models\eClassRecord_model;
    $mevent = new \App\Modules\ClassRecords\Models\GradeEvent_model;
    $mconduct = new \App\Modules\ClassRecords\Models\Conduct\Conduct_model;

    //$sched = $msched->selectRaw("*, dbo.fn_EmployeeName3(FacultyID) As Teacher,dbo.fn_EmployeeName3(FacultyID_2) As Teacher2, dbo.fn_SubjectCode(SubjectID) As Code, dbo.fn_SubjectTitle(SubjectID) As Subject, dbo.fn_SectionName(SectionID) As Section ")->where('ScheduleID', $_id )->first();

    $comp = $dbcon->get_components($_id,$pid);

    $students = DB::table("es_registrations AS r")
            ->leftJoin("ES_RegistrationDetails as d", 'r.RegID','=','d.RegID')
            ->selectRaw("r.StudentNo, dbo.fn_StudentName2(r.StudentNo) As StudentName, dbo.fn_AcademicYearTerm(r.TermID) As Term, dbo.fn_ProgramName(r.ProgID) As Program, dbo.fn_studentgender(r.studentno) as Gender, dbo.fn_MajorName(r.MajorID) As Major ")
            ->where('d.ScheduleID',$_id)
            ->whereRaw(" r.validationdate is not null and r.iswithdrawal = 0")
            // ->orderBy('Gender','desc')
            ->orderBy('StudentName','asc')
            ->get();



        
    $teacher =  $sched->Teacher;


    $cols = array(
        '17' => 10,
        '15' => 5,
        '18' => 2,
        '22' => 5,
        '21' => 5
    );


    $arr_event = [];

    #Initialized Events
    foreach($comp as $c){

        $temp = $c->ComponentID;

        $arr_event[$temp]  = array(
            'total' => '0',
            'evnt' => array(),
            'cols' => isset($cols[$temp]) ? $cols[$temp] :  $c->Cols,
        );


        $events = $mevent->where('ScheduleID', $_id)
            ->where('GradingPeriod', $pid)
            ->where('ComponentID', $temp)
            // ->where('Gender', $gen_id)
            ->get();

        foreach ($events as $e) {
            $arr_event[$temp]['evnt'][$j] = array(
                'EventID' => $e->EventID,
                'EventName' => '',//$e->EventName,
                'Description' => '',//$e->EventDescription,
                'EventDate' =>  ($e->EventDate != '' ?  date('m-d', strtotime($e->EventDate)) : '[Date]' ),
                'TotalItems' => $e->TotalItems,
                'Seq' => $e->SortOrder,
            );
            $total += intval($e->TotalItems);
            $j++;
        }

        $arr_event[$temp]['total'] = $total;

        $j =0 ;
        $total = 0;

    }

    //err_log($arr_event)  ;


    $name = 'asdasd';
    $title = ENV('APP_TITLE');
    $address = ENV('APP_ADDRESS');
    $term = !isset($students[0]->Term) ? '' : $students[0]->Term ;
    $date = systemDate();

    $pgrade = "PeriodGrade1";

    if($pid == '11'){
        $pgrade = "PeriodGrade1";
    }else if($pid == '12'){
        $pgrade = "PeriodGrade2";
    }else{
        $pgrade = "PeriodGrade".$pid;
    }

    $average = "AverageA";
    $quarter = "1st Quarter";
    switch($pid){
        case '1':
        case '11':
            $average = "AverageA";
        break;
        case '2':
        case '12':
            $average = "AverageB";
            $quarter = "2nd Quarter";
        break;
        case '3':
            $average = "AverageC";
            $quarter = "3rd Quarter";
        break;
        case '4':
            $average = "AverageD";
            $quarter = "4th Quarter";
        break;
    }

?>

<table style="width:100%;">
    <tbody>
        <tr>

            <td colspan="2" width="350">
            <img height="auto" width="auto" style="margin-top: 0px; " src="{{public_path('assets/system/media/images/report_title.png')}}" alt="logo" class="logo-default">

            </td>

            <td class="" width="750">
                <table>
                    <tr>
                        <td class="right">Section : </td>
                        <td> {{$sched->Section}}</td>
                    </tr>
                    <tr>
                        <td class="right">Code : </td>
                        <td> {{$sched->Code}}</td>
                    </tr>
                    <tr>
                        <td class="right">Subject Title : </td>
                        <td> {{$sched->Subject}}</td>
                    </tr>
                    <tr>
                        <td class="right">Teacher : </td>
                        <td> {{$teacher}}</td>
                    </tr>
                    <tr>
                        <td class="right">Period : </td>
                        <td> {{$quarter}}</td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td width="100"></td>
            <td class="bold font-large">CLASS RECORD</td>
            <td style="text-align: right;"></td>
        </tr>
        <tr>
        <td></td>
            <td>{{$term}}</td>
            <td style="text-align: right;"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </tbody>
</table>
<hr />
<br />
<table border="1">
   <tr nobr="true">
      <td width="100%" ><table class="center" id="tblmain" style="width:100%;" border="0"  >
    <thead>
        <tr>
            <th colspan="5" width="238" class="lborder" ></th>
            @foreach($comp as $c)
                <th width="{{ (( $c->count ) * 20) + 70 }}" colspan="{{($c->count + 3)}}" class="left lborder bold" >{{' '.$c->Caption}}</th>
            @endforeach
            <th class="lborder" width="30">Conduct</th>
        </tr>

        <tr>
            <th width="15" rowspan="2" class="lborder">#</th>
            <th width="133" rowspan="2" class="lborder">Student Name </th>
            <th width="30" rowspan="2" class="lborder">Raw</th>
            <th width="30" rowspan="2" class="lborder">Final</th>
            <th width="30" rowspan="2" class="lborder">Desc</th>
            @foreach($comp as $c)
           
                <th width="25" class="lborder">WT</th>
                <th width="25" class="lborder">PS</th>
                <th width="20" class="lborder">Total</th>
                @for($j=0;$j<$c->count; ++$j )
                    <th width="20" class="lborder">{{ $arr_event[$c->ComponentID]['evnt'][$j]['EventName'] or $j+1 }}</th>
                @endfor
            @endforeach

            <th width="30" rowspan="2" class="lborder">Effort</th>
<!--             <th width="30" rowspan="2" class="lborder">Acts Responsibly</th>
            <th width="30" rowspan="2" class="lborder">Extends consideration,
respect for,
and service to others</th>
            <th width="30" rowspan="2" class="lborder">Exhibits leadership
in character and virtue</th> -->


        </tr>
        <tr>
            @foreach($comp as $c)
                <th width="25" class="lborder">{{$c->Percentage}}%</th>
                <th width="25" class="lborder">100</th>
                <th width="20" class="lborder">{{$arr_event[$c->ComponentID]['total']}}</th>
                @for($j=0;$j< $c->count; ++$j )
                    <th width="20" class="lborder">{{ $arr_event[$c->ComponentID]['evnt'][$j]['TotalItems'] or '' }}</th>
                   
                @endfor
            @endforeach
        </tr>

    </thead>
    <tbody>
    <?php
      $tmpi= 1;
      $tmpe= 2;
      $trs = DB::SELECT("SELECT ISNULL((SELECT TOP 1 LetterGradeID FROM ESv2_Gradesheet_Setup WHERE TermID=s.TermID AND YearLevelID=s.YearLevelID AND ProgID=s.ProgramID),1) as TemplateID 
                               ,ISNULL((SELECT TOP 1 TransmutationID FROM ESv2_Gradesheet_Setup WHERE TermID=s.TermID AND YearLevelID=s.YearLevelID AND ProgID=s.ProgramID),1) as TransmutationID
                           FROM ES_ClassSchedules as cs INNER JOIN ES_ClassSections as s ON cs.SectionID=s.SectionID WHERE ScheduleID='".$_id."'");
      
      if($trs && count($trs)>0){
        $tmpi = $trs[0]->TemplateID;
        $tmpe = $trs[0]->TransmutationID;
      }
    ?>
    @foreach($students as $r)
        <?php
            $where = array(
                 'StudentNo' => $r->StudentNo
                ,'ScheduleID' => $_id
            );
            $i++;
            $rs = $model->where($where)->get();
            $grade = "";
            $ave = "";
            $rem = "";

            foreach($rs as $z){
                $grade = $z->$pgrade;
                $ave = $z->$average;
            }

            if($ave != ''){
                $transmute = $dbcon->gradeRemarks($tmpi, $ave);
                $rem = $transmute->LetterGrade;
            }


        ?>
    <tr nobr="true">
        <td width="15" class="lborder">{{$i}}.</td>
        <td class="left lborder" width="133">{{' '.trimmed( $r->StudentName ) }}</td>
        <td width="30" class="lborder">{{$grade}}</td>
        <td width="30" class="lborder">{{$ave}}</td>
        <td width="30" class="lborder" >{{$rem}}</td>

            <?php

                foreach($comp as $c) :

                $items = $arr_event[$c->ComponentID]['total'];
                $total = 0 ;
                $ps = 0;
                $wt = 0;
                $tdcol = "";

                for($j=0;$j < $c->count ; ++$j ){

                        $eventid = isset($arr_event[$c->ComponentID]['evnt'][$j]['EventID']) ? $arr_event[$c->ComponentID]['evnt'][$j]['EventID'] : 0 ;
                        $rc = '';

                        //err_log($eventid);
                        if($eventid != '0'){
                            $score =  $dbcon->Score($eventid, $r->StudentNo)->first();
                            $total +=  isset($score) ? $score->RawScore : 0 ;
                            $rc = !isset($score->RawScore)? '' : number_format($score->RawScore) ;
                        }

                        $tdcol .= '<td width="20" class="lborder center" >'. $rc . '</td>';

                   }


                       if($items>0) $ps  = (($total / $items)*100);
                       $percentx = $dbcon->transmuteGrade($tmpe, $ps);
                       $ps = $percentx->TransmutedGrade;
                       $wt = number_format($ps * ($c->Percentage / 100) ,2);
                     //err_log($tdcol);
                ?>
                <td width="25" class="lborder bold">{{$wt}}</td>
                <td width="25" class="lborder">{{number_format($ps,2)}}</td>
                <td width="20" class="lborder">{{$total}}</td>
                <?php

                echo $tdcol;

                endforeach ;

                $totalconduct = 0 ;

                for($l=1; $l<=1;++$l){
                    $score =  $mconduct->Score( $_id, $l , $pid , $r->StudentNo)->first();
                    if($score['Score'] > 0 ) {
                        $totalconduct  = $totalconduct + floatval($score['Score']);
                    }

                    echo '<td width="30" class="lborder" >'.$score['Score'].'</td>';

                }

                $cletter= '';
                if($totalconduct > 0){
                    $totalconduct = number_format($totalconduct / 4,2) ;
                    $conductgrade = $mconduct->ConductGrade('2', $totalconduct);
                    $cletter = trimmed($conductgrade->LetterGrade);

                }

                ?>

                <!-- <td width="20" class="lborder" >{{$totalconduct}}</td>
                <td width="20" class="lborder" >{{$cletter}}</td> -->

    </tr>
    @endforeach
    </tbody>

</table>
</td>
</tr>
</table>