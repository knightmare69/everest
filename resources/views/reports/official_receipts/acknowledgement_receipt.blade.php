<style>
    /**/
    body{ width: 800; height: 450; }
	.border-bottom { border-bottom: 1px solid #000 }
	.border { border: 0.5px solid #000 }
	.right { text-align: right; }
	.bold { font-weight: bold; }
	.center { text-align: center; }
	.left { text-align: left; }
	.valign { vertical-align: middle; }
	.bg { background-color: #EDEBE0; }
	.pad-5 { padding: 10px; }
    .fxd { position: fixed; }
</style>
<?php

    $id = (Request::get('or'));
    $temp = 0;

    $details = DB::table('es_journals as j')
                ->leftJoin('es_accounts as a','j.accountid','=','a.acctid')
                ->whereRaw("TransID = 20 AND ReferenceNo = '{$id}'")
                ->selectRaw("j.*, a.AcctCode, a.AcctName, a.ShortName ")
                ->get();

    $data = DB::table("es_officialreceipts AS a")
                ->selectRaw("a.*")
                ->where('ORNo',$id)->first();

	$checks = DB::table("es_bankchecks")->where('ORNo',$id)->get();

    $title = ENV('APP_TITLE');
    $address = ENV('APP_ADDRESS');
    if( !isset($data->ORNo) ){
        echo 'no receipt to view.';
        die();
    }
    $term = !isset($data->Term) ? '' : $data->Term ;
    $date = date('m/d/Y',strtotime($data->Date));
	$time = date('H:i A',strtotime($data->Date));
    $template = "";

    $total = 0;
	function twipToPixel($twip)
	{
		return $twip * 0.066666667;
	}
	$left = 7698;
	$top = 1394;
	#echo 'left : ' . twipToPixel($left).' Top: '. twipToPixel($top);


?>
{{-- <div style="height: 550px;">
<img src="{{url("assets/system/media/images/report_header.jpg")}}" style="margin: 0 auto;  width: 476px;  display: block;margin-bottom: 10px;" >
<table width="100%">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td  style="text-align: right;"><b>No:</b> {{$data->ORNo}}</td>
</tr>
<tr>
<td  style="text-align:center;"colspan="5"><b>ACKNOWEDGEMENT RECEIPT</b></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td  style="text-align: right;"><b>Date:</b> {{$date}}</td>
</tr>
<tr>
<td colspan="2"><b>Name:</b> {{$data->PayorName}} </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="2"><b>Year Level:</b> </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<table style="float: left;font-size:0.9em;" width="40%">
<tbody>

  @foreach($details as $d)
  <tr>
  	<td style="padding-left: 60px;">{{$d->ShortName}}</td>
  	<td  style="text-align: right;">{{number_format($d->Credit,2)}}</td>
  </tr>
  @endforeach
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<table style="float: right;font-size:0.9em;" width="30%">
<tbody>
<tr>
<td style="text-align: left;">Cash</td>
<td style="text-align: right;">{{number_format($data->CashReceive,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="text-align: left;">Check</td>
<td style="text-align: right;">{{number_format($data->CheckReceive,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td  style="text-align: left;border-bottom: 2px solid #000000;">Credit</td>
<td style="text-align: right;border-bottom: 2px solid #000000;">{{number_format($data->CardReceive,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td  style="text-align: left;">Total</td>
<td style="text-align: right;">{{number_format($data->AmountDue,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td  style="text-align: left;">Change</td>
<td style="text-align: right;">{{number_format($data->Change,2)}}</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<label class="fxd" style="top: 44%; left: 80%; border-top: 2px solid #000000;" ><b>Authorized Signature</b></label>
<label class="fxd" style="top: 94%; left: 80%; border-top: 2px solid #000000;" ><b>Authorized Signature</b></label>
<p class="fxd" style="top: 48%; " ><b >- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </b></p>
</div>

<div>
  <img src="{{url("assets/system/media/images/report_header.jpg")}}" style="margin: 0 auto;  width: 476px;  display: block;margin-bottom: 10px;" >
<table width="100%">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td  style="text-align: right;"><b>No:</b> {{$data->ORNo}}</td>
</tr>
<tr>
<td  style="text-align:center;"colspan="5"><b>ACKNOWEDGEMENT RECEIPT</b></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td  style="text-align: right;"><b>Date:</b> {{$date}}</td>
</tr>
<tr>
<td colspan="2"><b>Name:</b> {{$data->PayorName}} </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="2"><b>Year Level:</b> </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<table style="float: left;font-size:0.9em;" width="49%">
<tbody>

  @foreach($details as $d)
  <tr>
  	<td>{{$d->ShortName}}</td>
  	<td  style="text-align: right;">{{number_format($d->Credit,2)}}</td>
  </tr>
  @endforeach
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<table style="float: right;font-size:0.9em;" width="49%">
<tbody>
<tr>
<td>&nbsp;Cash</td>
<td style="text-align: right;">{{number_format($data->CashReceive,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;Check</td>
<td style="text-align: right;">{{number_format($data->CheckReceive,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Credit</td>
<td style="text-align: right;">{{number_format($data->CardReceive,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Total</td>
<td style="text-align: right;">{{number_format($data->AmountDue,2)}}</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Change</td>
<td style="text-align: right;">{{number_format($data->Change,2)}}</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div> --}}
<img src="{{url("assets/system/media/images/report_header.jpg")}}" class="fxd" style="top: 5; left: 150;width: 450px;" >
<img src="{{url("assets/system/media/images/report_header.jpg")}}" class="fxd" style="top: 570; left: 150;width: 450px;" >
<label class="fxd" style="top: 116; left: 300; width: 450px; " ><b>ACKNOWLEDGEMENT RECEIPT</b></label>
<label class="fxd" style="top: 666; left: 300; width: 450px; " ><b>ACKNOWLEDGEMENT RECEIPT</b></label>

<label class="fxd" style="top: 158; left: 651.53; width: 600px; " ><b>No.</b> {{$data->ORNo}}</label>
<label class="fxd" style="top: 178; left: 601.53; width: 600px; " ><b>Date:</b> {{$date.' '.$time}}</label>
<label class="fxd" style="top: 698; left: 651.53; width: 600px; " ><b>No.</b> {{$data->ORNo}}</label>
<label class="fxd" style="top: 718; left: 601.53; width: 600px; " ><b>Date:</b> {{$date.' '.$time}}</label>


{{-- <label class="fxd" style="top: 500; left: 641; border-top: 2px solid #000000;" ><b>Authorized Signature</b></label>
<label class="fxd" style="top: 1040; left: 641; border-top: 2px solid #000000;" ><b>Authorized Signature</b></label> --}}


<label class="fxd" style="top: 450; left: 36.4;" >Cashier: </label>
<label class="fxd" style="top: 470; left: 86.4;  " > <b>{{strtoupper(getFullNameByID($data->CreatedBy))}} </b></label>
<label class="fxd" style="top: 490; left: 86.4; border-top: 2px solid #000000; text-align :center;"  >Signature over Printed Name </label>
<label class="fxd" style="top: 9090; left: 36.4;" >Cashier: </label>
<label class="fxd" style="top: 9110; left: 86.4;  " > <b>{{strtoupper(getFullNameByID($data->CreatedBy))}} </b></label>
<label class="fxd" style="top: 9130; left: 86.4; border-top: 2px solid #000000; text-align :center;"  >Signature over Printed Name </label>
{{-- <label class="fxd" style="top: 1030; left: 36.4;" ><b>Cashier:</b> {{getFullNameByID($data->CreatedBy)}}</label> --}}

<label class="fxd" style="top: 413; left: 390; width: 600px;text-align: left " ><b>Amount in Words:</b></label>
<label class="fxd" style="top: 433; left: 410; width: 400px;text-align: left;font-size:1em; font-family: 'Courier New' " >{{$data->AmountinWords}}</label>
{{-- <label class="fxd" style="top: 388; left: 530; width: 60px; " >{{$date}}</label> --}}

<label class="fxd" style="top: 955; left: 390; width: 600px;text-align: left " ><b>Amount in Words:</b></label>
<label class="fxd" style="top: 975; left: 410; width: 400px;text-align: left;font-size:1em;font-family: 'Courier New' " >{{$data->AmountinWords}}</label>
{{-- <label class="fxd" style="top: 930; left: 530; width: 60px; " >{{$date}}</label> --}}

<p class="fxd" style="top: 512; text-align: center;" >- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

<label class="fxd" style="top: 178; left: 36.4; width: 300px; "><b>Name:</b> {{$data->PayorName }}</label>
<label class="fxd" style="top: 718; left: 36.4; width: 300px; "><b>Name:</b> {{$data->PayorName }}</label>


<label class="fxd " style="top: 230; left: 500; width: 60px; " >Cash :</label>
<label class="fxd right" style="top: 230; left: 626.07; width: 60px; " >{{number_format($data->CashReceive,2)}}</label>
<label class="fxd " style="top: 7700; left: 500; width: 60px; " >Cash :</label>
<label class="fxd right" style="top: 770; left: 626.07; width: 60px; " >{{number_format($data->CashReceive,2)}}</label>


<label class="fxd " style="top: 244.2; left: 500; width: 60px; " >Check :</label>
<label class="fxd right" style="top: 244.2; left: 626.07; width: 60px; " >{{number_format($data->CheckReceive,2)}}</label>
<label class="fxd " style="top: 786.2; left: 500; width: 60px; " >Check :</label>
<label class="fxd right" style="top: 786.2; left: 626.07; width: 60px; " >{{number_format($data->CheckReceive,2)}}</label>

{{-- <label class="fxd " style="top: 262.2; left: 500; width: 160px; " >Credit Card :</label>
<label class="fxd right" style="top: 262.2; left: 626.07; width: 60px; " >{{number_format($data->CardReceive,2)}}</label>
<label class="fxd " style="top: 804.2; left: 500; width: 160px; " >Credit Card :</label>
<label class="fxd right" style="top: 804.2; left: 626.07; width: 60px; " >{{number_format($data->CardReceive,2)}}</label> --}}


<label class="fxd " style="top: 280.2; left: 500; width: 160px; border-top: solid 1px" ><b>Total :</label>
<label class="fxd right" style="top: 280.2; left: 626.07; width: 60px; border-top: solid 1px" >{{number_format($data->AmountDue,2)}}</b></label>
<label class="fxd " style="top: 822.2; left: 500; width: 160px;border-top: solid 1px " ><b>Total :</label>
<label class="fxd right" style="top: 822.2; left: 626.07; width: 60px;border-top: solid 1px " >{{number_format($data->AmountDue,2)}}</b></label>


<label class="fxd " style="top: 298.2; left: 500; width: 160px; " >Change :</label>
<label class="fxd right" style="top: 298.2; left: 626.07; width: 60px; " >{{number_format($data->Change,2)}}</label>

<label class="fxd " style="top: 840.2; left: 500; width: 160px; " >Change :</label>
<label class="fxd right" style="top: 840.2; left: 626.07; width: 60px; " >{{number_format($data->Change,2)}}</label>










<label class="fxd" style="top: 158.20; left: 52.73; width: 600px; " >{{$term}}</label>
{{-- <label class="fxd" style="top: 268.20; left: 529.20; width: 600px; " >{{getUserNameByID($data->CreatedBy)}}</label> --}}

{{-- <!-- OR Details --> --}}
<table class="fxd" style="top: 230px; left: 36.40; width: 330px; font-size: 0.9em">
  <?php
    $or_total = 0;
   ?>
@foreach($details as $d)
<tr>
	<td width='240px'>{{$d->ShortName}}</td>
	<td style="text-align: right;">{{number_format($d->Credit,2)}}</td>
</tr>
<?php
  $or_total = $or_total + $d->Credit;
 ?>
@endforeach
<tr>
  <td style="text-align: right;font-size: 1em"><b>Total:</b></td>
	<td style="text-align: right;font-size: 1em"><b>{{number_format($or_total,2)}}</b></td>
</tr>
</table>
<table class="fxd" style="top: 770px; left: 36.40; width: 330px;font-size: 0.9em;">

@foreach($details as $d)
<tr>
	<td width='240px'>{{$d->ShortName}}</td>
	<td style="text-align: right;">{{number_format($d->Credit,2)}}</td>

</tr>
@endforeach
<tr>
  <td style="text-align: right;font-size: 1em"><b>Total:</b></td>
	<td style="text-align: right;font-size: 1em"><b>{{number_format($or_total,2)}}</b></td>
</tr>

</table>

<!-- Check Details -->
<table class="fxd" style="top: 318; left: 390.20; width: 410px;font-size:0.9em;font-family: 'Courier New' ">
@foreach($checks as $c)
<?php
$checkdate = date('m/d/Y',strtotime($c->CheckDate));
?>
<tr>
	<td>{{$c->CheckNo}}</td>
	<td>{{$c->CheckBank}}</td>
  <td>{{$checkdate}}</td>
  <td>{{number_format($c->CheckAmount,2)}}</td>
</tr>
@endforeach
</table>
<!-- Check Details -->
<table class="fxd" style="top: 860; left: 390.20; width: 410px;font-size:0.9em;font-family: 'Courier New' ">
@foreach($checks as $c)
<?php
$checkdate = date('m/d/Y',strtotime($c->CheckDate));
?>
<tr>
	<td>{{$c->CheckNo}}</td>
	<td>{{$c->CheckBank}}</td>
  <td>{{$checkdate}}</td>
  <td>{{number_format($c->CheckAmount,2)}}</td>
</tr>
@endforeach
</table>
