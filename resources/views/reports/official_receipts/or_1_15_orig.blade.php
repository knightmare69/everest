<style>
    /**/
    body{ width: 800; height: 450; }
	.border-bottom { border-bottom: 1px solid #000 }
	.border { border: 0.5px solid #000 }
	.right { text-align: right; }
	.bold { font-weight: bold; }
	.center { text-align: center; }
	.left { text-align: left; }
	.valign { vertical-align: middle; }
	.bg { background-color: #EDEBE0; }
	.pad-5 { padding: 10px; }
    .fxd { position: fixed; }
</style>
<?php

    $id = (Request::get('or'));
    $temp = 0;

    $details = DB::table('es_journals as j')
                ->leftJoin('es_accounts as a','j.accountid','=','a.acctid')
                ->whereRaw("TransID = 20 AND ReferenceNo = '{$id}'")
                ->selectRaw("j.*, a.AcctCode, a.AcctName, a.ShortName ")
                ->get();

    $data = DB::table("es_officialreceipts AS a")
                ->selectRaw("a.*")
                ->where('ORNo',$id)->first();

	$checks = DB::table("es_bankchecks")->where('ORNo',$id)->get();

    $title = ENV('APP_TITLE');
    $address = ENV('APP_ADDRESS');
    if( !isset($data->ORNo) ){
        echo 'no receipt to view.';
        die();
    }
    $term = !isset($data->Term) ? '' : $data->Term ;
    $date = date('m/d/Y',strtotime($data->Date));
	$time = date('H:i A',strtotime($data->Date));
    $template = "";

    $total = 0;
	function twipToPixel($twip)
	{
		return $twip * 0.066666667;
	}
	$left = 7698;
	$top = 1394;
	#echo 'left : ' . twipToPixel($left).' Top: '. twipToPixel($top);


?>

<label class="fxd" style="top: 116; left: 36.4; width: 300px; ">{{$data->PayorID . ' ' . $data->PayorName }}</label>


<label class="fxd " style="top: 134.27; left: 500; width: 60px; " >Cash :</label>
<label class="fxd right" style="top: 134.27; left: 626.07; width: 60px; " >{{number_format($data->CashReceive,2)}}</label>

<label class="fxd " style="top: 118.07; left: 500; width: 60px; " >Check :</label>
<label class="fxd right" style="top: 118.07; left: 626.07; width: 60px; " >{{number_format($data->CheckReceive,2)}}</label>

<label class="fxd " style="top: 152.47; left: 500; width: 160px; " >Credit Card :</label>
<label class="fxd right" style="top: 152.47; left: 626.07; width: 60px; " >{{number_format($data->CardReceive,2)}}</label>

<label class="fxd " style="top: 172.53; left: 500; width: 160px; " >Total :</label>
<label class="fxd right" style="top: 172.53; left: 626.07; width: 60px; " >{{number_format($data->AmountDue,2)}}</label>

<label class="fxd right" style="top: 170.53; left: 500; width: 190px; border-top: solid 1px " ></label>
<label class="fxd " style="top: 189.93; left: 500; width: 160px; " >Change :</label>
<label class="fxd right" style="top: 189.93; left: 626.07; width: 60px; " >{{number_format($data->Change,2)}}</label>

<label class="fxd" style="top: 257.2; left: 47.6; width: 600px; " >{{$data->AmountinWords}}</label>
<label class="fxd" style="top: 43.13; left: 399.80; width: 600px; " >{{$data->ORNo}}</label>
<label class="fxd" style="top: 43.13; left: 641.53; width: 600px; " >{{$date.' '.$time}}</label>
<label class="fxd" style="top: 286; left: 264.47; width: 160px; " >{{$time}}</label>
<label class="fxd" style="top: 286; left: 188.53; width: 60px; " >{{$date}}</label>

<label class="fxd" style="top: 158.20; left: 52.73; width: 600px; " >{{$term}}</label>
<label class="fxd" style="top: 268.20; left: 529.20; width: 600px; " >{{getUserNameByID($data->CreatedBy)}}</label>

<!-- OR Details -->
<table class="fxd" style="top: 178.27; left: 36.40; width: 400px;">
@foreach($details as $d)
<tr>
	<td>{{$d->ShortName}}</td>
	<td>{{number_format($d->Credit,2)}}</td>
</tr>
@endforeach
</table>

<!-- Check Details -->
<table class="fxd" style="top: 92.93; left: 513.20; width: 300px;">
@foreach($checks as $c)
<?php
$checkdate = date('m/d/Y',strtotime($c->CheckDate));
?>
<tr>
	<td>{{$c->CheckNo}}</td>
	<td>{{$checkdate}}</td>
	<td>{{$c->CheckBank}}</td>
</tr>
@endforeach
</table>
