<style>
    /**/
    body{ 
        width: 7in; height: 5in;
        font-size: 7.5pt;
		font-weight: bold;
        font-family: 'Arial';
        margin:0;
    }
    table{
        font-size: 7pt !important;
        font-family: 'Arial';
		font-weight: bold;
    }
    label{
        font-family: 'Arial';
		font-weight: bold;
    }
	
    .medium{
	    font-size:8pt !important;
	}
    .enlarge{
	    font-size:9pt !important;
	}
	
    .border-bottom { border-bottom: 1px solid #000 }
    .border { border: 0.5px solid #000 }
    .right { text-align: right; }
    .bold { font-weight: bold; }
    .center { text-align: center; }
    .left { text-align: left; }
    .valign { vertical-align: middle; }
    .bg { background-color: #EDEBE0; }
    .pad-5 { padding: 10px; }
    .fxd { position: fixed; }

    @page{
        margin: 0;
        size: A4;
    }
</style>
<pre>
<?php
    function numberTowords($num = false){
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
    }

    $id = (Request::get('or'));
    $temp = 0;

    $details = DB::table('es_journals as j')
                ->leftJoin('es_accounts as a','j.accountid','=','a.acctid')
                ->whereRaw("TransID = 20 AND ReferenceNo = '{$id}'")
                ->selectRaw("j.*, a.AcctCode, a.AcctName, a.ShortName ")
                ->get();

    $data = DB::table("es_officialreceipts AS a")
            ->leftJoin('es_students as s','s.StudentNo', '=' ,'a.PayorID' )
            ->leftJoin('es_registrations as r','r.RegID', '=' ,'a.RefNo' )
            ->leftJoin('esv2_admission_familybackground as f','s.FamilyID', '=' ,'f.FamilyID' )
    // ->leftJoin('es_registrations as r',function($join)
    // {
    //   $join->on('r.StudentNo','=','s.StudentNo');
    //   $join->where('r.RegID', '=','a.RefNo');
    // })

    ->selectRaw("a.*,
      dbo.fn_K12_YearLevel(r.YearLevelID) as YearLevel,
      concat(f.Guardian_Address, ' ',f.Guardian_Barangay, ' ', f.Guardian_TownCity,' ',f.Guardian_ZipCode) as Address
    ")
    ->where('a.ORNo',$id)->first();



    $checks = DB::SELECT("SELECT * FROM ES_BankChecks WHERE ORNo='".$id."'");
    $uname  = (($data)?(DB::SELECT("SELECT TOP 1 FullName FROM ESv2_Users WHERE UserIDX='".intval($data->CreatedBy)."' OR FacultyID='".$data->CreatedBy."'")):false);
    if($uname && count($uname)>0){
       $data->CreatedBy = $uname[0]->FullName;
    }

    $title = ENV('APP_TITLE');
    $address = ENV('APP_ADDRESS');
    if( !isset($data->ORNo) ){
        echo 'no receipt to view.';
        die();
    }
    $term = !isset($data->Term) ? '' : $data->Term ;
    $date = date('m/d/Y',strtotime($data->Date));
    $time = date('H:i A',strtotime($data->Date));
    $template = "";
    $particular = $details[0]->Particulars;
    $particular = (($particular=='')?($data->Particular):$particular);
    
    if($data->AmountinWords==''){
        $data->AmountinWords = numberTowords(number_format($data->AmountDue, 2));
    }


    $total = 0;
    function twipToPixel($twip){
        return $twip * 0.066666667;
    }
    $left = 7698;
    $top = 1394;
    #echo 'left : ' . twipToPixel($left).' Top: '. twipToPixel($top);

?>
<label class="fxd" style="top: 2.32in; left: 7in;  ">{{$data->ORNo}}</label>
<label class="fxd" style="top: 2.44in; left: 6.8in;  ">{{$date}}</label>

<label class="fxd enlarge bold" style="top: 2.74in; left: 4.1in;">{{$data->PayorName}}</label>
<label class="fxd medium bold" style="top: 3.2in; left: 3.03in;  ">{{$data->Address}}</label>
<label class="fxd medium bold" style="top: 3.87in; left: 3.9in;  ">{{strtoupper($data->AmountinWords)}}</label>

<label class="fxd enlarge bold right" style="top: 4.1in; left: 6.83in;  ">{{number_format($data->AmountDue, 2)}}</label>
<label class="fxd" style="top:  4.46in; left: 4.6in;  ">{{strtoupper($particular)}}</label>
<label class="fxd" style="top:  4.98in; left: 6.22in;  ">{{$data->CreatedBy}}</label>
<label class="fxd" style="top: 1.96in; left: 2.09in;  "></label>
<label class="fxd" style="top: 1.96in; left: 1.19in;  "></label>


<!-- OR Details -->
<table class="fxd" style="top: 1.64in; left: 0.04in; width: 195px; font-size:10pt;">
@foreach($details as $d)
<tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td class="right enlarge">{{number_format($d->Credit,2)}}</td>
</tr>
@endforeach
</table>
<table class="fxd" style="top: 3.6in; left: 0.03in; width: 195px; font-size:11pt;">
<tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td class="right enlarge">{{number_format($data->AmountDue,2)}}</td>
</tr>
</table>

<!-- Check Details -->
<table class="fxd" style="top: 4.2in; left: 0.6in; width: 195px; font-size:10pt;">
@foreach($checks as $c)
<?php
$checkdate = date('m/d/Y',strtotime($c->CheckDate));
?>
<tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>{{$c->CheckNo}}<br/>
        {{$c->CheckBank}}<br/></td>
</tr>
@endforeach
</table>


<script>
    window.onload = window.print();
</script>