<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Consolidated Conduct</title>
    <style>
    .text-center {
      text-align: center
    }

    .text-right {
      text-align: right
    }

    .vertical-middle {
      vertical-align: middle
    }

    .vertical-top {
      vertical-align: top
    }

    .vertical-bottom {
      vertical-align: bottom
    }

    .border {
      border: 1px solid #2a2a2a
    }

    *,
    *::before,
    *::after {
      margin: 0;
      padding: 0;
      -webkit-box-sizing: inherit;
      box-sizing: inherit
    }

    html {
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
      font-family: Arial, Helvetica, sans-serif
    }

    body {
      text-align: left;
      background: #ccc;
      color: #2a2a2a;
      line-height: 1.5
    }

    .input-group {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      font-size: 10px
    }

    .input-group:not(:last-child) {
      margin-bottom: 3px
    }

    .input-group input {
      border: 0;
      border-bottom: 1px solid #2a2a2a;
      font-size: inherit;
      font-weight: 600
    }

    @font-face {
      font-family: "Goudy Old Style";
      src: url("../css/fonts/Goudy Old Style.ttf")
    }

    @font-face {
      font-family: "tonic";
      src: url("../css/fonts/tonic.ttf")
    }

    .container {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column;
      margin: 20px auto;
      background: #fff;
      -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23)
    }

    .table {
      width: 100%
    }

    :root {
      line-sizing: normal
    }

    :root {
      -ms-text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric;
      text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric
    }

    img,
    video,
    canvas,
    audio,
    iframe,
    embed,
    object {
      display: block
    }

    img,
    video {
      max-width: 100%;
      height: auto
    }

    img {
      vertical-align: middle;
      border-style: none
    }

    article,
    aside,
    figcaption,
    figure,
    footer,
    header,
    hgroup,
    main,
    nav,
    section {
      display: block
    }

    b,
    strong {
      font-weight: bolder
    }

    table {
      border-collapse: collapse
    }

    .header {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end
    }

    .header__logo {
    margin-top: 10px;
    width: 85px;
    height: auto;
    }

    .header__text {
      width: 400px
    }

    .header__text-top {
      line-height: 1;
      font-family: "Goudy Old Style";
      padding-left: 8px
    }

    .header__text-top h1 {
      font-weight: 500;
      font-size: 20px
    }

    .header__text-top p {
      font-weight: 600;
      font-size: 10px
    }

    .header__text-center {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      padding: 0 8px 3px
    }

    .header__text-center ul {
      list-style-type: none;
      font-family: "tonic";
      font-weight: 600
    }

    .header__text-bottom {
      border-top: 2px solid #2a2a2a;
      text-align: right
    }

    .header__text-bottom p {
      font-size: 10px;
      font-family: "tonic";
      font-weight: 600
    }

    .header__right {
      margin-left: auto
    }

    .header__right h6 {
      font-style: italic;
      font-weight: 500;
      font-size: 8px
    }

    .header__list-left {
      font-size: 8px
    }

    .header__list-left li {
      line-height: 1
    }

    .header__list-right {
      font-size: 10px;
      -ms-flex-item-align: end;
      align-self: flex-end;
      text-align: right
    }

    .header__list-right li {
      line-height: 1.2
    }

    .container {
      padding: 20px;
      max-width: 11.7in;
      min-height: 8.3in
    }

    .main h2 {
      text-align: center;
      font-family: Calibri, "Trebuchet MS", sans-serif;
      font-size: 18px
    }

    .main__header {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      margin: 10px 0
    }

    .main__header-left {
      text-align: left;
      width: 260px
    }

    .main__header-left .input-group label {
      width: 55px
    }

    .main__header-left .input-group input {
      -webkit-box-flex: 1;
      -ms-flex: 1;
      flex: 1
    }

    .main__header-right {
      text-align: right
    }

    .main__header-right .input-group label {
      padding-right: 4px;
      width: 175px
    }

    .main__header-right .input-group input {
      width: 200px
    }

    .rotated-header {
      text-align: center;
      vertical-align: bottom;
      white-space: nowrap;
      position: relative
    }

    .rotated-header>div {
      position: absolute;
      top: 95%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%) rotate(-90deg);
      transform: translate(-50%, -50%) rotate(-90deg);
      max-width: 1px
    }

    .pr-sm {
      padding-right: 5px
    }

    .pl-sm {
      padding-left: 5px
    }

    th {
      font-weight: 400
    }

    th,
    td {
      font-size: 7px
    }

    .footer {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end;
      margin-top: auto
    }

    .footer>div:not(:last-child) {
      width: 200px;
      text-align: center
    }

    .footer>div:last-child {
      margin-left: auto
    }

    .footer__adviser--1 {
      line-height: 1
    }

    .footer__adviser--1 h6 {
      font-size: 10px
    }

    .footer__adviser--1 small {
      font-size: 9px;
      font-style: italic
    }

    .footer__adviser--2 {
      line-height: 1
    }

    .footer__adviser--2 h6 {
      font-size: 10px
    }

    .footer__adviser--2 small {
      font-size: 9px;
      font-style: italic
    }

    .footer__adviser--3 {
      line-height: 1
    }

    .footer__adviser--3 h6 {
      font-size: 10px
    }

    .footer__adviser--3 small {
      font-size: 9px;
      font-style: italic
    }

    .footer__signature {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center
    }

    .footer__signature .input-group label {
      width: 55px;
      font-size: 9px;
      font-style: italic
    }

    @page {
      size: a4 landscape;
      margin: 0
    }

    @media print {
      .container {
        margin: auto;
        width: auto;
        height: auto;
        padding-bottom: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        page-break-after: always
      }

      /* .footer {
        position: absolute;
        bottom: 20px;
        left: 20px;
        right: 20px
      } */

      body {
        -webkit-print-color-adjust: exact !important
      }

      /* html,
      body {
        height: 100%;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden
      } */
    }

    .width-20 {
      width: 20px
    }

    .width-25 {
      width: 25px
    }

    .width-35 {
      width: 35px
    }

    .height-15 {
      height: 15px
    }

    .height-25 {
      height: 25px
    }

    .height-60 {
      height: 60px
    }

    .height-100 {
      height: 100px
    }

    .bold{
      font-weight: bold;
    }

    /*# sourceMappingURL=jhs-cc.min.css.map */
.bg-green{ background-color: #dff0d8;}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.jfixed{
  position: fixed;
  top: 0;
  left: 0;
}

.font-green{
  color: green;
}
.font-blue{
  color:darkblue
}
.font-red{
  color: red !important;
}
    </style>
    <?php
    $bgGreen ='background-color: #dff0d8;';
    $width20 ='width:20px;';
    $rotatedHeader = 'text-align: center;
      vertical-align: bottom;
      white-space: nowrap;
      position: relative';
    $rotatedHeaderDiv = ' position: absolute;
      top: 95%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%) rotate(-90deg);
      transform: translate(-50%, -50%) rotate(-90deg);
      max-width: 1px';
    ?>
  </head>
  <body>
    
<?php

function getPrefect($level, $type){
  $dept = new App\Modules\Setup\Models\Department;
  //0 head
  //1 assisstant
 
  $whereRaw = '';
  if(in_array($level,['Kinder', 'Grade 1', 'Grade 2', 'Grade 3'])){
    $whereRaw .= "DeptCode = 'K-G3'";

  }else if (in_array($level,['Grade 4', 'Grade 5', 'Grade 6'])){
    $whereRaw  .= "DeptCode = 'G4-G6'";
  }else if (in_array($level,['Grade 7', 'Grade 8'])){
    $whereRaw  .= "DeptCode = 'G7-G8'";
  }else if (in_array($level,['Grade 9', 'Grade 10'])){
    $whereRaw  .= "DeptCode = 'G9-G10'";
  }else if ($level == 'Grade 11'){
    $whereRaw  .= "DeptCode = 'G11'";
  }else if ($level == 'Grade 12'){
    $whereRaw  .= "DeptCode = 'G12'";
  }
   $data = DB::table('es_departments')
    ->whereRaw($whereRaw)
    ->first();
    if($type == 0){
      if(isset($data)){
        return $dept->getEmployee($data->DeptHead_EmployeeID);
      }
     
    }else{
      if(isset($data)){
        return $dept->getEmployee($data->DeptVice_EmployeeID);
      }
    
    }
   return '';
    
}

function lettergrade($grade){
	if($grade != '' ){
		$gs = getSetting(1);
	$gs_ = DB::table('es_gs_transmutation_table')->where('TemplateID', 5)
	->whereRaw(" (".$grade." BETWEEN Min AND Max )")
	->first();
  if(isset($gs_)){
    return $gs_->TransmutedGrade;
  }

	}
	return '';

}
$term_id = decode(Request::get('academic-term'));
$camp = decode(Request::get('campus'));    
$prog = decode(Request::get('programs'));
$sect = decode(Request::get('section'));
$yl = decode(Request::get('year-level'));
$period = decode(Request::get('period'));

$page_title = "Conduct Consolidated Grades";    
$header = DB::table("es_institution")->first();

$per = DB::table('es_gs_gradingperiods')->where('PeriodID', $period)->first();

$period_desc = getObjectvalue($per, 'Description1');
  
$whereRaw = "r.TermID = '{$term_id}' AND ValidationDate IS NOT NULL AND IsWithdrawal = 0 ";

if($prog != '0'){
    $whereRaw .= " AND r.ProgID = '{$prog}'";
}
if($yl != '0'){
    $whereRaw .= " AND r.YearLevelID= '{$yl}'";
}

if($sect != ''){
    $whereRaw .= " AND r.ClassSectionID = '{$sect}'";
}        

$rs = DB::table("es_registrations AS r")                
            ->selectRaw("r.*, fn_StudentName(r.StudentNo) As Name, fn_StudentGender(r.StudentNo) As Gender, fn_AcademicYear(r.TermID) As Term, fn_ProgramName(r.ProgID) As Program, fn_K12_YearLevel(r.YearLevelID) As level, 
            fn_SectionName(ClassSectionID) As Section,
    fn_SectionAdviser(ClassSectionID) as Adviser,
    fn_EmployeeName6((select AdviserID from es_classsections where SectionID =ClassSectionID limit 1)) as Homeroom,
    fn_Merit( r.StudentNo, r.TermID, '". $period ."', 5) As Absent , fn_Merit( r.StudentNo, r.TermID, '". $period ."', 6) As Tardiness   ")                
            ->whereRaw($whereRaw)                
            //->orderBy('Gender', 'DESC')
            ->orderBy('Name', 'ASC')
            ->get();



$title = $header->Report_InstitutionName;
$address = $header->Report_CompleteAddress;

$date = systemDate();

$rs = json_decode(json_encode($rs), true);
$rs = array_group_by($rs, 'ClassSectionID');
foreach($rs as $v=>  $t){
  $whereRaw = "
        case when fn_ProgramName((select ProgramID from es_classsections where SectionID = cs.SectionID limit 1)) = 'Junior High School' then  
         
        case when  fn_IsParentSubject(SubjectID) > 0 then 1=1 else 
        (select IsExclConductCompute from es_permanentrecord_details where SubjectID= cs.SubjectID limit 1) = 0  or
        (select IsNonConduct from es_subjects where SubjectID= cs.SubjectID limit 1) = 0
        end

        else 
        (select ShowOnReport from es_permanentrecord_details where SubjectID= cs.SubjectID limit 1) = 1  
        end
        ";
$whereRaw ="(select ShowOnReport from es_permanentrecord_details where SubjectID= cs.SubjectID limit 1) = 1  ";  
$subj = DB::table("es_classSchedules as cs")
        ->selectRaw("cs.*, 
        (select ShowOnReport from es_permanentrecord_details where SubjectID= cs.SubjectID limit 1) as ShowOnReport,
        (select SubjParentID from es_subjects where SubjectID = cs.SubjectID limit 1) as ParentID,
        (select IsNonConduct from es_subjects where SubjectID = cs.SubjectID limit 1) as isNonConduct,
        fn_SubjectSortOrder2(SubjectID, (select CurriculumID from es_classsections where SectionID = cs.SectionID limit 1)) as SortOrder,
        fn_IsParentSubject2(SubjectID, SectionID) as isParent,
        fn_EmployeeName6(cs.FacultyID) as Teacher,
        (select IsExclConductCompute from es_permanentrecord_details where SubjectID= cs.SubjectID limit 1) as IsExclConductCompute,
        (select ConductUnit from es_permanentrecord_details where SubjectID= cs.SubjectID limit 1) as ConductUnit,
        fn_SubjectTitle(SubjectID) As Subj ")        
        ->where("SectionID", $v)
        // ->whereRaw($whereRaw)
        ->orderBy("SortOrder")
        ->get();
$temp = "";
$section = "";
$program = "";
$level ="";
$homeroom_teacher="";

$total = array(
     0 => '0'
    ,1 => '0'
    ,2 => '0'        
    ,3 => '0'
    ,4 => '0'
    ,5 => '0'
);
$total_merit=0;
$total_demerit=0;
$average=0;
$overall_finalaverage= 0;
$finalaverage = 0;

$data = array();
$i=0;
$term	='';
 
$att = DB::table('ES_PermanentRecord_SchoolDays')->where('TermID', $term_id)->where('SchoolID',0)->first();
// dd($subj);
foreach($t as $r){
    
    $term = $r['Term'];
    $section = $r['Section'];
    $program = $r['Program'];
$adviser = $r['Adviser'];
$level= $r['level'];
$homeroom_teacher =$r['Homeroom'];
    
    $data[$r['StudentNo']]=array(
        'idno' => $r['StudentNo']
       ,'name' => $r['Name']
       ,'level' => $r['level']
       ,'progid' => $r['ProgID']
       ,'term' => $r['TermID']
       ,'dept' => $r['Program']
       ,'regdate' => setDate($r['RegDate'], 'm-d-Y')
       ,'receipt' => $r['ORNo']
       ,'valdate' => setDate($r['ValidationDate'], 'm-d-Y')
       ,'sec' => $r['Section']
       ,'sec_id' => $r['ClassSectionID']
       ,'absent' => $r['Absent']
       ,'late' => $r['Tardiness']
    );
} 
$eval =  new App\Modules\Registrar\Services\EvaluationServices;
foreach ($data as $key => $v) {
  $eval->computeQuarterAve( $v['term'], $v['idno'], $period );
  # code...
}
$jSubjectCounter=0;
foreach ($subj as $s) {
  if($s->ShowOnReport == 1){
    $jSubjectCounter++;
  }
}
$i=1; 
?>
    <div class="container">
        <a id="dlink"  style="display:none;"></a>
        
        <input  title="{{$section}}-Consolidated Conduct - {{$period_desc}}" class="jfixed button no-print" type="button" onclick="tableToExcel('table', 'name', '{{$section}}-Classs Records - {{$period_desc}}.xls')" value="Export to Excel">
          {{-- <input  title="{{$section}}-Consolidated Conduct - {{$period_desc}}" class="jfixed button no-print" type="button" onclick="fnExcelReport('table');" value="Export to Excel"> --}}
      
      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ HEADER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <header class="header">
        <!------------------------------ Header logo ------------------------------>
        <div class="header__logo">
          <img src='{{url("assets/admin/layout/img/logo.png")}}' alt="Logo" />
        </div>
        <!------------------------------ Header text group ------------------------------>
        <div class="header__text">
          <div class="header__text-top">
            <h1>SAN BEDA UNIVERSITY</h1>
            <p>Integrated Basic Education Department</p>
          </div>
          <div class="header__text-center">
            <ul class="header__list-left">
              <li>Manila: 638 Mediola St., San Miguel, Manila</li>
              <li>Rizal: Havila Main Road, Brgy, San Juan, Taytay, Rizal</li>
              <li>
                Telefax: (Manila) 735-6011 to 15 / (Rizal) 660-9667 to 68
              </li>
              <li>www.sanbedarizal.edu.ph</li>
            </ul>
            <ul class="header__list-right">
              <li>PREFECT OF STUDENT AFFAIRS</li>
            <li>{{$program}}</li>
            </ul>
          </div>
          <div class="header__text-bottom">
            <p>Ut In Omnibus Glorificetur Deus</p>
          </div>
        </div>
      </header>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ MAIN
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <main class="main">
      <h2>{{$program == 'Grade School' || $level =='Kinder' ? 'CONSOLIDATED CONDUCT AND MERIT SYSTEM ON DEPORTMENT': 'CONSOLIDATED CONDUCT'}}</h2>
        <!------------------------------ Main header ------------------------------>
        <div class="main__header">
          <!------------------------------ Main header left ------------------------------>
          <div class="main__header-left">
            <div class="input-group">
              <label>Grade&Sec.</label>
              <input type="text" value="{{$section}}" />
            </div>
            <div class="input-group">
              <label>Adviser:</label>
              <input type="text" value="{{$adviser}}" />
            </div>
          </div>
          <!------------------------------ Main header right ------------------------------>
          <div class="main__header-right">
            <div class="input-group">
              <label>Quarter:</label>
              <input type="text" value="{{$period_desc}}" />
            </div>
            <div class="input-group">
              <label>Academic Year:</label>
              <input type="text" value="{{$term}}" />
            </div>
          </div>
        </div>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ TABLE
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <table id="table" class="table">
          <tbody>
            <tr class="text-center">
              <th class="width-25"></th>
            <th colspan="{{($level == 'Kinder' ? 1: 0) +  3 + $jSubjectCounter}}"></th>
              <td class="width-20" rowspan="3"></td>
              <td colspan="10" class="border">
                <h3>DEPORTMENT</h3>
              </td>
              <td class="width-20" rowspan="3"></td>
            <td rowspan="2" colspan="{{$program == 'Grade School' ? 1: 2}}" class="border">
                <h3>FINAL GRADE</h3>
              </td>
              <td class="width-20" rowspan="3"></td>
            <td colspan="{{($level == 'Kinder' ? 3: 1) + 3}}" class="border">
                <h3>ATTENDANCE</h3>
              </td>
            </tr>
            <tr class="height-15 text-center">
              <td></td>
            <td colspan="{{$jSubjectCounter + 1}}" class="border"></td>
              
              <td class="border"></td>
            <td rowspan="2" class="width-20 border " style="{{$bgGreen.' '.$width20.' '.$rotatedHeader}} "> 
            <div style="{{$rotatedHeaderDiv}}">
                  <h3>CONDUCT AVERAGE</h3>
                </div>
              </td>
              @if ($level == 'Kinder')
              <td rowspan="2" class="width-20 border rotated-header ">
                  <div>
                    <h3>EQUIVALENT GRADE</h3>
                  </div>
                </td>
              @endif
              

              <td colspan="5" class="border">MERITS</td>
              <td colspan="5" class="border">DEMERITS</td>

              <td class="width-25 border rotated-header" rowspan="2">
                <div>
                  Days of School
                </div>
              </td>
              <td class="width-25 border rotated-header" rowspan="2">
                <div>
                  Days Present
                </div>
              </td>
              <td class="width-25 border rotated-header" rowspan="2">
                <div>
                  Excused Absences
                </div>
              </td>
              @if($level == 'Kinder')
              <td class="width-25 border rotated-header" rowspan="2">
                  <div>
                      Unexcused Absences
                  </div>
                </td>
                <td class="width-25 border rotated-header" rowspan="2">
                    <div>
                        Excused Tardiness
                    </div>
                  </td>
                  <td class="width-25 border rotated-header" rowspan="2">
                      <div>
                        Unexcused Tardiness
                      </div>
                    </td>
              @else
              <td class="width-25 border rotated-header" rowspan="2">
                <div>
                  Frequency of
                  <br />
                  Tardiness
                </div>
              </td>
              @endif
              <td class="width-25" rowspan="2"></td>
              <td class="width-25 border rotated-header" rowspan="2">
                <div>Activity Grade</div>
              </td>
            </tr>
            <tr class="height-100">
              <td></td>
              <td class="text-center border">
                <h3>NAME OF STUDENT</h3>
              </td>
              <?php
              $studentCntr= 0;
              $homeroom_average = 0;
              $overall_average = 0;
              ?>
              @foreach ($subj as $s)
                <?php
              
                    ${'subjectAve'.$s->SubjectID} = 0;       
                ?>
              @endforeach
              @foreach($subj as $s)
              @if ($s->ShowOnReport == 1)
            <th data-id="{{$s->SubjectID}}" class="width-25 border rotated-header">
                <div>
                  <?php
                 $newtext = wordwrap(ucwords(strtolower(substr($s->Subj,0,40))), 27, "<br />");
                  ?>
                    <?= $newtext ?>
                  @if ($s->IsExclConductCompute == 0)
                      <span class="font-red">* </span>
                  @endif
                </div>
              </th>
              @endif
              @endforeach
              <td class="width-25 border rotated-header">
                <div>
                  Homeroom Conduct
                  <br />
                  Grade
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Meritorious Deeds/Benedictine
                  <br />
                  Hallmarks
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Family Council Assembly
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  No Infraction
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Attendance in Special School
                  <br />
                  Activities
                </div>
              </td>
              <td class="width-25 border rotated-header bg-green">
                <div>
                  <h3>TOTAL MERITS</h3>
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Unexcused Absences
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Unexcused Tardiness
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Improper Uniform/No ID
                </div>
              </td>
              <td class="width-25 border rotated-header">
                <div>
                  Violation/s of Code of
                  <br />
                </div>
              </td>
              <td class="width-25 border rotated-header bg-green">
                <div>
                  <h3>TOTAL DEMERITS</h3>
                </div>
              </td>
              <td class="width-35 border rotated-header">
                <div>
                  <h3>CONDUCT GRADE</h3>
                  (CONDUCT AVE + MERITS) -
                  <br />
                  DEMERITS
                </div>
              </td>
              @if ($program == 'Grade School')
                  @else
                  <td class="width-35 border rotated-header">
                      <div>
                        <h3>EQUIVALENT GRADE</h3>
                      </div>
                    </td>
              @endif
              
            </tr>
          {{-- </thead>
          <tbody> --}}
            <tr class="height-15 text-center">
              <td></td>
              <td></td>
              
            @foreach($subj as $s)
            @if ($s->ShowOnReport == 1)
                
           
            @if ($s->IsExclConductCompute == 0)
            <td  class="border bold font-blue">{{number_format($s->ConductUnit,2)}} </td>
            @else
            <td  class="border bold font-blue"></td>
            @endif
            @endif
           
          
           
            @endforeach
            <td  class="border bold font-blue">1.00</td>
            <td  class="border bold font-blue"></td>
              
            </tr>
           
          
            @foreach($data as $d => $v)
            <tr>
                <?php $studentCntr++; ?>
            <td class="text-center">{{$i}}.</td>
              <td class="pl-sm border">
                <span> {{$v['name']}} </span>
              </td>
              <?php $parentid = array();
            
              $parent_cntr = 0;  ?>
              @foreach($subj as $s)
                <?php
                if($s->isParent > 0){
                        array_push($parentid, $s->SubjectID );
                        $parent_cntr++;
                    }
                ?>
              @endforeach
              <?php 
              foreach( $parentid as $l => $k){
                ${'grade'.$k.'jayz'} = 0;
    
              }
              // dd($subj);
            ?>
              @foreach($subj as $s)
                <?php
                if(in_array($s->ParentID, $parentid) && $s->isNonConduct == 0){
                  ${'grade'.$s->ParentID.'jayz'} += getConductAverage(0, $period, $v['idno'], $s->ScheduleID, 1 ); ;
                }
                ?>
              @endforeach
                <?php 
                  $sujCntr = 0;
                ?>
              @foreach($subj as $s)
              <?php
                    
                    
									
                    // $sc = 'ConductA';
                    //  switch($period){
                    //             case 1:case 11: $sc = 'ConductA';break;
                    //             case 2:case 12: $sc =  'ConductB';break;
                    //             case 3:  $sc =  'ConductC';break;
                    //             case 4: $sc =  'ConductD';break;
                    //         }
                    //     $grade = "";
                    //     $get = DB::table("es_gs_conduct as c")
                    //             ->selectRaw("Score")
                    //             ->where("StudentNo", $v['idno']  )
                    //             ->where("ScheduleID", $s->ScheduleID )
                    //             ->where("PeriodID",$period)
                    //             ->first();
                    //     $get = DB::table('es_permanentrecord_details')
                    //             ->select([DB::raw("
                    //         ".$sc.
                    //         " AS Score ")])
                    //             ->where("ScheduleID", $s->ScheduleID )
                    //             ->where("StudentNo", $v['idno']  )
                    //             ->first();
                        
                    //     if( !empty($get)) {
                    //         //$grade = $period == '11'?  $get->ConductA : $get->ConductB  ;
                    //         $grade = $get->Score ;
                    //         $average+=$grade;
                    //     }

                   
                    if($s->isParent > 0){
                      // $parent_cntr++;
                        $grade =  (${'grade'.$s->SubjectID.'jayz'} / $s->isParent ) ;
                        $grade = number_format($grade, 2);
                        $average+=$grade;
                        $sujCntr++;
                    }else{
                        $grade = getConductAverage(0, $period, $v['idno'], $s->ScheduleID, 1 ); 
                        if($s->ParentID > 0 && getProgramName($v['progid']) == 'Junior High School' ){
                          //  $average+=$grade;
                          //  $sujCntr++;
                        }else{
                          $average+=$grade;
                          $sujCntr++;
                        }
                       
                    }
                   
                  $per = 'A';
                    switch($period){
                                case 1:case 11: $per="A"; break;
                                case 2:case 12: $per="B"; break;
                                case 3: $per="C"; break;
                                case 4: $per="D"; break;
                            }

                    DB::table('es_permanentrecord_details')->where('ScheduleID',$s->ScheduleID)->where('StudentNo',$v['idno'])->update(['Conduct'.$per => $grade]);           
                    ${'subjectAve'.$s->SubjectID} += $grade;
                    $con_posted = true;
                $con_grades = DB::table('es_gs_conduct')->where('ScheduleID', $s->ScheduleID)->where('StudentNo', $v['idno'])->where('PeriodID', $period)->get();
                // dd($con_grades);

                foreach ($con_grades as $key => $v_) {
                    if($v_->DatePosted == null){
                        $con_posted = false;
                    }
                }                      
                    ?>
                @if ($s->ShowOnReport == 1)
              <td class="{{$grade > 74 ? '': 'font-red'}} text-center border {{$s->isParent > 0 ? 'font-green': ''}}">{{ $con_posted ? $grade: '' }}</td>
              @endif
              @endforeach
              <?php 
                 $con_posted = true;
                $con_grades = DB::table('es_gs_conduct')->where('SectionID', $v['sec_id'])->where('StudentNo', $v['idno'])->where('PeriodID', $period)->get();

                foreach ($con_grades as $key => $v_) {
                    if($v_->DatePosted == null){
                        $con_posted = false;
                    }
                }
                 
                    $home_room = getConductAverage(0, $period, $v['idno'], $v['sec_id'], 0 );                  
                    $average+=$home_room;
                    $homeroom_average += $home_room;
                    
                    $average = $average / ( ($sujCntr) + 1 );
                ?>
              <td class="text-center border">{{$con_posted ? $home_room: ''}}</td>
              <?php
              $sc = 'A';
                     switch($period){
                                case 1:case 11: $sc = 'A';break;
                                case 2:case 12: $sc =  'B';break;
                                case 3:  $sc =  'C';break;
                                case 4: $sc =  'D';break;
                            }
              ?>
              <?php

              $pm = DB::table('es_permanentrecord_master')->where('TermID', $v['term'])->where('StudentNo', $v['idno'])->first();
              $qtrAve = getObjectvalue($pm, 'Conduct'.$sc.'_Average');
              $average =$qtrAve ;
              $overall_average += number_format($qtrAve, 2);


              ?>
              <td class="text-center border bg-green">{{number_format($qtrAve,2) }}</td>
              @if ($level == 'Kinder')
              <td class="text-center border ">{{lettergrade($qtrAve) }}</td>
              @endif

              <td></td>
              @for($c=1; $c<5; $c++)
              <?php
              $con_score=0;
              $conduct = DB::table("ES_Deportment_list")
                  ->where('deportmentid', $c)
                  ->where("StudentNo", $v['idno']  )
                  ->where("SectionID",$v['sec_id'] )
                  ->where("PeriodID",$period)
                  ->first();
              if( !empty($conduct)) {
                  $con_score = $conduct->Score;
                  $total_merit+=$con_score;
              }
          ?>
          
          <td class="text-center border merit{{$c}}">{{$con_score ==  '' ? 0 : $con_score}}</td>         
              @endfor                 
              <td class="text-center border bg-green merit_total">{{$total_merit}}</td>
              <?php
                $tardy_jay = 0;
                $absent_jay = 0;
              ?>
              @for($c=5; $c<9; $c++)
              <?php
              
                    $con_score=0;
                    $conduct = DB::table("ES_Deportment_list")
                        ->where('deportmentid', $c)
                        ->where("StudentNo", $v['idno']  )
                        ->where("SectionID",$v['sec_id'] )
                        ->where("PeriodID",$period)
                        ->first();
                    if( !empty($conduct)) {
                        $con_score = $conduct->Score;
                        $total_demerit+=$con_score;
                        if($c == 5){
                          $absent_jay += $con_score / .5;
                        }else if($c == 6){
                          $tardy_jay += $con_score / .5;
                        }
                    }
                ?>
              <td class="text-center border merit{{$c}}">{{$con_score ==  '' ? 0 : $con_score}}</td>         
              @endfor                 
              <td class="text-center border bg-green demerit_total">{{$total_demerit}}</td>
             

              <td></td>
              <?php
               $meritj = 0;
              $casej = DB::table('es_gs_meritdemerits')
            ->selectRaw("SUM(Merit) as Merit")
            ->where('StudentNo', $v['idno'] )
            ->where('TermID', $term_id)
            ->where('PeriodID', $period)
            ->where('Type', 5)
            ->first();

            $meritj = $casej ? $casej->Merit : 0; 


              $case = DB::table('es_gs_meritdemerits')
                  ->where('StudentNo', $v['idno'] )
                  ->where('TermID',$term_id )
                  ->where('PeriodID',$period)
                  ->where('Type', 5)
                  ->count();
              $final_conduct = $average +  $total_merit - $total_demerit;
              $final_conduct = $case > 0 ? (65+$meritj) : $final_conduct; 
              $finalaverage+= $final_conduct;
				?>

              <td class="text-center border"><?= number_format( $final_conduct,2);  ?></td>
              @if ($program == 'Grade School')
              @else
              <td class="text-center border">{{lettergrade($final_conduct)}}</td>
              @endif
             

              <td></td>
              <?php
				      	// $att->TOTAL = $period == '11'? 55:47;
                    $present = isset($att->TOTAL) ?  $att->TOTAL : 52 ;
                    $absent_ex = DB::table("ES_GS_MeritDemerits")
                                ->selectRaw("sum(`Days`) As Days")
                                ->where("StudentNo", $v['idno']  )
                                ->where("TermID", $term_id  )                                
                                ->where('IsExcused','1') 
                                ->where("PeriodID",$period)
                                ->where('Type', '1')                                
                                ->first();
                    $absent_unex = DB::table("ES_GS_MeritDemerits")
                    ->selectRaw("sum(`Days`) As Days")
                    ->where("StudentNo", $v['idno']  )
                    ->where("TermID", $term_id  )  
                    ->where("PeriodID",$period)                              
                    ->where('IsExcused','0') 
                    ->where('Type', '1')                                
                    ->first();
                    $tardy_ex = DB::table("ES_GS_MeritDemerits")
                                ->selectRaw("sum(`Days`) As Days")
                                ->where("StudentNo", $v['idno']  )
                                ->where("TermID", $term_id  )                                
                                ->where('IsExcused','1') 
                                ->where("PeriodID",$period)
                                ->where('Type', '2')                                
                                ->first();
                    $tardy_unex = DB::table("ES_GS_MeritDemerits")
                    ->selectRaw("sum(`Days`) As Days")
                    ->where("StudentNo", $v['idno']  )
                    ->where("TermID", $term_id  ) 
                    ->where("PeriodID",$period)                               
                    ->where('IsExcused','0') 
                    ->where('Type', '2')                                
                    ->first();
                    $tardy_jz = 'TardinessA';
                    $absent_jz ='AbsentA';
                    switch($period){
                        case 1: case 11: $tardy_jz="TardinessA"; $absent_jz="AbsentA";  break;
                        case 2: case 12: $tardy_jz="TardinessB"; $absent_jz="AbsentB"; break;
                        case 3: $tardy_jz="TardinessC"; $absent_jz="AbsentC"; break;
                        case 4: $tardy_jz="TardinessD"; $absent_jz="AbsentD"; break;
                    }
					$att_ex = DB::table('ES_PermanentRecord_Master')
							->select([DB::raw("
							".$absent_jz." AS Absent,
							".$tardy_jz." AS Late")])
							->where("StudentNo", $v['idno']  )
                            ->where("TermID", $term_id  )->first();  
                    if( !empty($absent_ex)) {
                        $present =  isset($att_ex)  ? $present - (getObjectvalue($att_ex,'Absent') + $absent_jay ) : 0;                        
                    }
                ?>

              <td class="text-center border">{{isset($att->TOTAL) ?  $att->TOTAL : 52 }}</td>
                  <td class="text-center border">{{$present}}</td>
              <td class="text-center border">{{getObjectvalue($att_ex,'Absent')}}</td>
              @if ($level== 'Kinder')
                  <td class="text-center border">{{$v['absent'] > 0 ? number_format($v['absent'],0) : ''}}</td>
              <td class="text-center border">{{getObjectvalue($att_ex,'Late')}}</td>
              <td class="text-center border">{{$v['late'] > 0 ? number_format($v['late'],0) : ''}}</td>
              @else
              <td class="text-center border">{{getObjectvalue($att_ex,'Late')}}</td>
              @endif
             

              <td></td>

              <td class="text-center border"></td>
            </tr>
            <?php
            $total[0]++;
                     
               $i++;
               $average=0;
               $total_merit=0;
               $total_demerit=0;
       
           ?>
            @endforeach

         

          

         

       
            <tr class="height-15">
              <td></td>
              <td>
                <strong>AVERAGE</strong>
              </td>
              @foreach ($subj as $s)
              <?php
              $totalave = ${'subjectAve'.$s->SubjectID} / $studentCntr;
              // $overall_average+= $totalave;
              // $subjectCntr++;
            ?>
            @if ($s->ShowOnReport == 1)
              <td class="font-blue text-center">
                  <strong>{{number_format($totalave,2)}} </strong>
                </td>
              @endif
              @endforeach
              
 
              <td class="font-blue text-center"> 
                <strong>{{number_format($homeroom_average /$studentCntr , 2)}} </strong>
              </td>
              <td class="font-blue text-center" >
                <strong>{{number_format($overall_average / $studentCntr, 2)}}</strong>
              </td>
            <td colspan="{{($level =='Kinder' ? 3: 0) + 10 + ($program == 'Grade School' ? 1: 2)}}"></td>
              <td  class="font-blue text-center">
                <strong>{{number_format($finalaverage / $studentCntr, 2)}}</strong>
              </td>
            </tr>
          {{-- </tbody> --}}
          {{-- <tfoot> --}}
            <tr>
              <td></td>
              <td colspan="{{($level =='Kinder' ? 3: 0) + 32 +($program == 'Grade School' ? 1: 2)}}" class="text-right">
                <i
>CONSOLIDATED CONDUCT({{$section}}|{{$period_desc}}|AY
                  {{$term}})</i
                >
              </td>
            </tr>
            <tr class="height-60">
              <td></td>
              <td></td>
              @foreach ($subj as $s)
              @if ($s->ShowOnReport == 1)
              @if($s->isParent > 0)
              <td></td>
              @else
              <td class="border rotated-header">
                  <div>
                  {{$s->Teacher}}
                  </div>
              </td>
             
              @endif
              @endif
              @endforeach
            
              <td class="border rotated-header">
                <div>
                 {{$homeroom_teacher}}
                </div>
              </td>
              <td></td>
              <td colspan="{{($level =='Kinder' ? 3: 0) + 18 +($program == 'Grade School' ? 1: 2)}}"></td>
              <td class="border rotated-header">
                <div>
                 
                </div>
              </td>
            </tr>
            <tr class="height-25">
              <td></td>
              <td class="text-right vertical-bottom pr-sm">
                <i>Please sign if there are NO CORRECTIONS</i>
              </td>
              @foreach ($subj as $s)
              @if ($s->ShowOnReport == 1)
              @if ($s->isParent > 0)
              <td></td>
              @else
              <td class="text-center border"></td>
              @endif
              @endif
              @endforeach
             
 
              <td class="text-center border"></td>
              <td></td>
              <td colspan="{{($level =='Kinder' ? 3: 0) + 18+ ($program == 'Grade School' ? 1: 2)}}"></td>
              <td class="text-center border"></td>
            </tr>
          </tbody>
          {{-- </tfoot> --}}
        </table>
      </main>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <div class="footer__adviser--1">
          <h6>{{(strtoupper($adviser))}}</h6>
          <small>Adviser</small>
        </div>
        <div class="footer__adviser--2">
        <h6>{{(strtoupper(getPrefect($level, 1)))}}</h6>
          <small>Assistant Prefect of Student Affairs (Discipline)</small>
        </div>
        <div class="footer__adviser--3">
          <h6>{{(strtoupper(getPrefect($level, 0)))}}</h6>
          <small>Prefect of Student Affairs, {{$program}} </small>
        </div>
        <div class="footer__signature">
          <div class="input-group">
            <label>Printed on:</label>
          <input type="text" value="{{date('M j, Y H:m:s')}}" />
          </div>
          <div class="input-group">
            <label>by:</label>
          <input type="text" value="{{getUserFullName()}}" />
          </div>
          <div class="input-group">
            <label>Version:</label>
            <input type="text" />
          </div>
        </div>
      </footer>
    </div>

    <?php
}
    ?>
  </body>
</html>

<style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    </style>
    {{-- <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      var tableToExcel = (function () {
          var uri = 'data:application/vnd.ms-excel;base64,'
              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
              return function (table, name) {
              if (!table.nodeType) table = document.getElementById(table)
              var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

              var blob = new Blob([format(template, ctx)]);
              var blobURL = window.URL.createObjectURL(blob);

              if (ifIE()) {
                  csvData = table.innerHTML;
                  if (window.navigator.msSaveBlob) {
                      var blob = new Blob([format(template, ctx)], {
                          type: "text/html"
                      });
                      navigator.msSaveBlob(blob, '' + name + '.xls');
                  }
              }
              else
              window.location.href = uri + base64(format(template, ctx))
          }
      })()

     function ifIE() {
          var isIE11 = navigator.userAgent.indexOf(".NET CLR") > -1;
          var isIE11orLess = isIE11 || navigator.appVersion.indexOf("MSIE") != -1;
          return isIE11orLess;
      }
  </script> --}}
    <script>   
    function fnExcelReport(id_table) {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById(id_table); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
      
            var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name, filename) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
    
                document.getElementById("dlink").href = uri + base64(format(template, ctx));
                document.getElementById("dlink").download = filename;
                document.getElementById("dlink").click();
    
            }
        })()
    
        
    
    </script>
