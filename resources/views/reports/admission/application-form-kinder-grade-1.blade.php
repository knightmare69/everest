<?php 
  $appno = decode(Request::get('AppNo'));
  $indent = str_repeat('&nbsp;', 3);

  $admission_data = App\Modules\Admission\Models\Admission::where('AppNo',$appno)->get();
  $admission_data = isset($admission_data[0]) ? $admission_data[0] : [];  

  $family_data = App\Modules\Admission\Models\FatherBackground::where('FamilyID',getGuardianFamilyID($appno))->get(); 
  $family_data = isset($family_data[0]) ? $family_data[0] : [];  

  $ay = App\Modules\Setup\Models\AcademicYearTerm::where('TermID', getObjectValue($admission_data,'TermID'))->get();
  $ay = isset($ay[0]) ? $ay[0] : [];

  $status = explode(',',getObjectValue($admission_data,'Family_HealthStatus')); 
  $arr = [];
  foreach($status as $stats) {
    $arr[] = $stats;
  }

  $siblings = App\Modules\Admission\Models\Siblings::where('FamilyID',getGuardianFamilyID($appno))->get();
  $siblings = isset($siblings) ? $siblings : [];

  $references = App\Modules\Admission\Models\References::where('AppNo',$appno)->get();
  $references = isset($references) ? $references : [];

  function getAnswerByID($key){
    $qns_answer = App\Modules\Admission\Models\QuestionaireAnswer::where(['app_no'=>decode(Request::get('AppNo')),'question_id'=>$key])->get();
    return isset($qns_answer[0]) ? $qns_answer[0]->answer : '';
  }

 ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Application Form (Kinder-Grade 1)</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="{{url("assets/css/application-form.min.css")}}" />
  </head>
  <style>
  .checkbox-check::after {
    content: '\2714';
    position: absolute;
    left: -12px;
  }
  </style>
  <body>
    <!--
      !WARNING: All the codes below are hard coded in CSS, Please be careful when modifying the html structure.
    <!-->
    <div class="container">
      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ HEADER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <header class="header">
        <!------------------------------ Header logo ------------------------------>
        <div class="header__logo">
          <img src="{{url("assets/img/logo.jpg")}}" alt="logo" /> 
        </div>

        <!------------------------------ Header title ------------------------------>
        <div class="header__title">
          <h3>APPLICATION FORM (Kinder - Grade 1)</h3>
          <small>*Please inform school for any subsequent changes</small>
        </div>
      </header>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ MAIN
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <main class="main">
        <div class="main__header">
          <h4>Dear Parents,</h4>

          <p>
            Thank you for your interest in Everest Academy. The Admissions
            Committee always tries to make decisions in the best interest of
            each child. Your responses in this form will help us get to know
            your child, guide us as we evaluate his/her application and, with
            the hope that admission is offered, ensure a smooth transition into
            our school.
          </p>
        </div>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ SECTION - APPLICATION
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <section class="section-application__two d-flex">
          <div class="input-group">
            <label class="input-label mr-xs">Date of Application: </label>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{date('d', strtotime($admission_data->AppDate))}}"/>
            <input type="text" class="input-control input-border-bottom" style="width:70%; text-align:center;" value="{{date('m', strtotime($admission_data->AppDate))}}"/>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{date('Y', strtotime($admission_data->AppDate))}}"/>
            <ul class="input-list">
              <li>Day</li>
              <li>Month</li>
              <li>Year</li>
            </ul>
          </div>

          <div class="input-group">
            <label class="input-label">SY 20</label>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ substr($ay->AcademicYear, 2,2) }}"/>
          </div>

          <div class="input-group">
            <label class="input-label">to 20</label>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ substr($ay->AcademicYear,-2) }}"/>
          </div>
        </section>

        <h4>
          <i>*Please do not leave any blanks, for items that are not applicable
            kindly write NA.
          </i>
        </h4>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ SECTION - STUDENT INFORMATION
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <section class="mt-md">
          <h4 class="mb-md">
            <i>
              STUDENT INFORMATION
            </i>
          </h4>

          <!------------------------------ INPUT ROW ONE ------------------------------>
          <div class="input-row-1 d-flex">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Full Name:</label>
              <input type="text" class="input-control input-border-bottom" style="width:32%;text-align:center;" value="{{ getObjectValue($admission_data,'LastName') }}"/>
              <input type="text" class="input-control input-border-bottom" style="width:33.3%;" value="{{ getObjectValue($admission_data,'FirstName') }}"/>
              <input type="text" class="input-control input-border-bottom" style="width:33.3%;" value="{{ getObjectValue($admission_data,'MiddleName') }}"/>
              <ul class="input-list">
                <li>Last</li>
                <li>First</li>
                <li>Middle</li>
              </ul>
            </div>

            <div class="input-group">
              <label class="input-label mr-xs">Nickname:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'NickName') }}"/>
            </div>
          </div>

          <!------------------------------ INPUT ROW TWO ------------------------------>
          <div class="input-row-2 d-flex">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Date of Birth:</label>
              <input type="text" class="input-control input-border-bottom" style="width:30%;text-align:center;" value="{{date('d', strtotime($admission_data->DateOfBirth))}}"/>
              <input type="text" class="input-control input-border-bottom" style="width:20%;text-align:center;" value="{{date('m', strtotime($admission_data->DateOfBirth))}}"/>
              <input type="text" class="input-control input-border-bottom" style="width:40%;text-align:center;" value="{{date('Y', strtotime($admission_data->DateOfBirth))}}"/>
              <ul class="input-list">
                <li style="margin-right:45px;">Day</li>
                <li style="margin-right:45px;">Month</li>
                <li>Year</li>
              </ul>
            </div>

            <span class="checkbox {{ getObjectValue($admission_data,'IsAdopted') ? 'checkbox-check' : '' }}">Adopted</span>

            <div class="input-group">
              <label class="input-label">Age:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.Request::get('age') }}"/>
            </div>

            <div>
              <span class="checkbox {{ getObjectValue($admission_data,'Gender') == 'M' ? 'checkbox-check' : '' }}">Male</span>
              <span class="checkbox {{ getObjectValue($admission_data,'Gender') == 'F' ? 'checkbox-check' : '' }}">Female</span>
            </div>
          </div>

          <!------------------------------ INPUT ROW THREE ------------------------------>
          <div class="input-row-3 d-flex">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Home Address:</label>
              <input type="text" class="input-control input-border-bottom" style="width:60%; text-align:center;" value="{{ getObjectValue($admission_data,'HomeAddressUnitNo') }}"/>
              <input type="text" class="input-control input-border-bottom" style="text-align:center; width:150%;" value="{{ getObjectValue($admission_data,'HomeAddressStreet') }}"/>
              <input type="text" class="input-control input-border-bottom" value="{{ getObjectValue($admission_data,'HomeAddressBrgy') }}"/>
              <ul class="input-list">
                <li>House/Unit No</li>
                <li>Street</li>
                <li>Barangay</li>
              </ul>
            </div>
          </div>

          <!------------------------------ INPUT ROW FOUR ------------------------------>
          <div class="input-row-4 d-flex">
            <div class="input-group">
              <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ getObjectValue($admission_data,'HomeAddressCity') }}"/>
              <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ getObjectValue($admission_data,'HomeAddressZipCode') }}"/>
              <ul class="input-list">
                <li>City</li>
                <li>Zip/Postal Code</li>
              </ul>
            </div>

            <div class="input-group input-group--two">
              <label class="input-label">Home Phone:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'HomePhone') }}"/>
            </div>
          </div>

          <!------------------------------ INPUT ROW FIVE ------------------------------>
          <div class="input-row-5 d-flex mb-md">
            <div class="input-group">
              <label class="input-label mr-xs">Nationality:</label>
              @foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
                @if(getObjectValue($admission_data,'NationalityID') == $row->NationalityID)
                  <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Nationality }}"/>
                @endif
              @endforeach
            </div>

            <div class="input-group">
              <label class="input-label mr-xs">Citizenship:</label>
              @foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
                @if(getObjectValue($admission_data,'CitizenshipID') == $row->NationalityID)
                  <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Nationality }}"/>
                @endif
              @endforeach
            </div>
          </div>

          <!------------------------------ INPUT ROW SIX ------------------------------>
          <div class="input-row-6 d-flex mb-md">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Religion:</label>
              @foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
                @if(getObjectValue($admission_data,'ReligionID') == $row->ReligionID)
                  <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Religion }}"/>
                @endif
              @endforeach
            </div>
            <span>Baptized:</span>
            <span class="checkbox {{ getObjectValue($admission_data,'IsBaptized') ? 'checkbox-check' : '' }}">Yes</span>
            <span class="checkbox {{ !getObjectValue($admission_data,'IsBaptized') ? 'checkbox-check' : '' }}">No</span>
            <div class="input-group">
              <label class="input-label mr-xs">Religion Baptized in:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>
          </div>

          <!------------------------------ INPUT ROW SEVEN ------------------------------>
          <div class="input-row-7 d-flex">
            <div class="input-group stretch">
              <input type="text" class="input-control input-border-bottom" style="padding-left:40px;width:30%;" value="{{ setDateFormat(getObjectValue($admission_data,'BaptizedIn_Date'),'yyyy-mm-dd','mm/dd/yyyy') }}"/>
              <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ getObjectValue($admission_data,'BaptizedIn_Church')}}"/>
              <input type="text" class="input-control input-border-bottom" style="width:40%;" value="{{ getObjectValue($admission_data,'BaptizedIn_City') }}"/>
              <ul class="input-list">
                <li>Date</li>
                <li>Church</li>
                <li>City</li>
              </ul>
            </div>
          </div>

          <div class="w-100 mb-md">
            <div class="input-group">
              <label class="input-label mr-xs">Parish or place of worship:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'WorshipPlace') }}"/>
            </div>
          </div>

          <!------------------------------ INPUT ROW EIGHT ------------------------------>
          <div class="input-row-8 d-flex mb-md">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Address:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'WorshipAddress') }}"/>
            </div>

            <div class="input-group">
              <label class="input-label mr-xs">Phone:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'WorshipTelNo') }}"/>
            </div>
          </div>
        </section>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ SECTION - STUDENT INFORMATION
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <section>
          <h4 class="mb-md">
            <i>
              SCHOLASTIC INFORMATION
            </i>
          </h4>

          <p class="mb-md">
            Is your currently child attending any pre-school or Kindergarten
            program ? <span class="checkbox {{ getObjectValue($admission_data,'IsAttendingKdg') ? 'checkbox-check' : '' }}">Yes</span> <span class="checkbox {{ !getObjectValue($admission_data,'IsAttendingKdg') ? 'checkbox-check' : '' }}">No</span>
          </p>

          <div class="d-flex mb-md">
            <div class="input-group stretch">
              <label class="input-label mr-xs">If yes, name of school:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchool') }}"/>
            </div>

            <div class="input-group">
              <label class="input-label mr-xs">No. of years in program:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolYrsProgram') }}"/>
            </div>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Website:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolWebsite') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Address:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolAddress') }}"/>
          </div>

          <div class="d-flex mb-md">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Head of school:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolHead') }}"/>
            </div>

            <div class="input-group">
              <label class="input-label">Phone:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolContactNo') }}"/>
            </div>
          </div>

          <p>How did you come to know about Everest Academy?</p>

          <!------------------------------ INPUT ROW NINE ------------------------------>
          <div class="input-row-9 d-flex mb-md">
            <span class="checkbox {{ getAnswerByID(6) == 'Open House' ? 'checkbox-check' : '' }}">Open House</span>
            <span class="checkbox {{ getAnswerByID(6) == 'Website' ? 'checkbox-check' : '' }}">Website</span>
            <span class="checkbox {{ getAnswerByID(6) == 'Pre-school' ? 'checkbox-check' : '' }}">Pre-school</span>

            <div class="input-group stretch">
              <label class="input-label mr-xs">
                <span class="checkbox {{ getAnswerByID(6) == 'Everest family/employee' ? 'checkbox-check' : '' }}">
                  Everest family/employee
                </span>
              </label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(7) }}"/>
              <small>(please specify name)</small>
            </div>
          </div>

          <!------------------------------ INPUT ROW TEN ------------------------------>
          <div class="input-row-10 mb-md">
            <div class="input-group">
              <label class="input-label mr-xs">
                <span class="checkbox {{ getAnswerByID(6) == 'Other' ? 'checkbox-check' : '' }}">
                  Other
                </span>
              </label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(8) }}"/>
            </div>
          </div>
        </section>
      </main>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 1 of 5</h4>
      </footer>
    </div>

    <div class="container">
      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <h4 class="mb-md">
          <i>PERSONAL HISTORY</i>
        </h4>

        <p class="mb-md">
          Please list three words that would best describe your child’s
          personality.
        </p>

        <div class="d-flex mb-lg">
          <div class="input-group w-100">
            <label class="input-label ">1.</label>
            <input type="text" class="input-control input-border-bottom mr-xs" value="{{ $indent.getAnswerByID(17) }}"/>
          </div>

          <div class="input-group w-100">
            <label class="input-label">2.</label>
            <input type="text" class="input-control input-border-bottom mr-xs" value="{{ $indent.getAnswerByID(12) }}"/>
          </div>

          <div class="input-group w-100">
            <label class="input-label">3.</label>
            <input type="text" class="input-control input-border-bottom mr-xs" value="{{ $indent.getAnswerByID(13) }}"/>
          </div>
        </div>

        <p class="mt-md mb-md">
          Please rate your child’s independence level in the following self-help
          skills:
        </p>

        <p class="mb-md">
          Toilet training
          <br />
          <span class="checkbox mr-xs {{ getAnswerByID(18) == 'Fully independent' ? 'checkbox-check' : '' }}">Fully independent</span>
          <span class="checkbox mr-xs {{ getAnswerByID(18) == 'Needs some assistance (e.g. after bowel movement)' ? 'checkbox-check' : '' }}">Needs some assistance (e.g. after bowel movement)</span>
          <span class="checkbox {{ getAnswerByID(18) == 'Still needs assistance/supervision' ? 'checkbox-check' : '' }}">Still needs assistance/supervision</span>
        </p>

        <p class="mb-md">
          Eating habits
          <br />
          <span class="checkbox mr-xs {{ getAnswerByID(15) == 'Fully independent' ? 'checkbox-check' : '' }}">Fully independent</span>
          <span class="checkbox mr-xs {{ getAnswerByID(15) == 'Needs some help (e.g. cutting food)' ? 'checkbox-check' : '' }}">Needs some help (e.g. cutting food)</span>
          <span class="checkbox {{ getAnswerByID(15) == 'Still needs assistance/supervision' ? 'checkbox-check' : '' }}">Still needs assistance/supervision</span>
        </p>

        <p class="mb-md">
          Dressing / changing clothes
          <br />
          <span class="checkbox mr-xs {{ getAnswerByID(16) == 'Fully independent' ? 'checkbox-check' : '' }}">Fully independent</span>
          <span class="checkbox mr-xs {{ getAnswerByID(16) == 'Needs some help (e.g. fixing belts, buttons)' ? 'checkbox-check' : '' }}">Needs some help (e.g. fixing belts, buttons)</span>
          <span class="checkbox {{ getAnswerByID(16) == 'Still needs assistance/supervision' ? 'checkbox-check' : '' }}">Still needs assistance/supervision</span>
        </p>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What would you like to tell us about your child that will help us to
          know him/her better? We would be interested to know about any special
          talents, gifts or interests that he/she may possess, as well as areas
          in which you would like to see further development.
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(19) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What form of discipline do you use at home and how does your child
          respond to discipline?
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(20) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What are the most important qualities you are looking for in your
          child’s education?
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(21) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Why do you want your child to attend Everest Academy?
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(22) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 2 of 5</h4>
      </footer>
    </div>

    <div class="container">
      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Are there any prior circumstances which may affect your child’s
          success at Everest?
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(23) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Has your child had any behavioral / disciplinary difficulties at
          previous schools? <span class="checkbox {{ getAnswerByID(24) == 'No' ? 'checkbox-check' : '' }}">No</span> <span class="checkbox {{ getAnswerByID(24) == 'Yes' ? 'checkbox-check' : '' }}">Yes</span>. If YES, please give details.
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(25) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Please describe any illnesses, diseases, or disabilities, which either
          have affected or may limit your child’s participation in school
          activities.
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(26) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Has your child had any behavioral, psychological or educational
          evaluations?
          <span class="checkbox {{ getAnswerByID(27) == 'No' ? 'checkbox-check' : '' }}">No</span> <span class="checkbox {{ getAnswerByID(27) == 'Yes' ? 'checkbox-check' : '' }}">Yes</span>
        </p>

        <div class="input-group w-100 mb-md">
          <label class="input-label">If yes, when was your child evaluated?</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(28) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label">Who referred your child for evaluation?</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(29) }}"/>
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What is your child’s behavioral, psychological or educational need
          based on the evaluation report? </br>(e.g. ADHD, ASD, Speech Delay etc.)
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(30) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Was your child recommended to undergo intervention or therapy? <span class="checkbox {{ getAnswerByID(31) == 'No' ? 'checkbox-check' : '' }}">No</span> <span class="checkbox {{ getAnswerByID(31) == 'Yes' ? 'checkbox-check' : '' }}">Yes</span></br>
          If yes, what type of therapy did/ does your child undergo?
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(32) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What is the duration of therapy? (Write inclusive days/ weeks/ months)
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(33) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Name of specialist/ therapist</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(34) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Hospital/ Clinic name and address</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(35) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Contact number of specialist/ therapist</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(36) }}"/>
        </div>
      </section>

      <h4>
        <i>* Please submit a photocopy of your child’s complete evaluation and
          therapy report.
        </i>
      </h4>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 3 of 5</h4>
      </footer>
    </div>

    <div class="container">
      <h4 class="mb-md">
        <i>
          PARENT INFORMATION
        </i>
      </h4>

      <div class="columns">
        <div class="column-6">
          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Father's full name:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_Name') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Home address:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_Address') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Date of birth:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.setDateFormat(getObjectValue($family_data,'Father_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Civil status:</label>
            @foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
              @if(getObjectValue($family_data,'Father_MaritalID') == $row->StatusID)
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->CivilDesc  }}"/>
              @endif
            @endforeach
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Home phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_TelNo') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Mobile phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_Mobile') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Employer: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_Company') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Position: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_Occupation') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Type of business: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_BusinessType') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Work phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_CompanyPhone') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Work address: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_CompanyAddress') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">E-mail:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_Email') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Educational attainment: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Father_EducAttainment') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Religion: </label>
            @foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
              @if(getObjectValue($family_data,'Father_ReligionID') == $row->ReligionID)
                <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Religion }}"/>
              @endif
            @endforeach
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Nationality: </label>
            @foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
              @if(getObjectValue($family_data,'Father_NationalityID') == $row->NationalityID )
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Nationality }}"/>
              @endif
            @endforeach
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Citizenship: </label>
            @foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
              @if(getObjectValue($family_data,'Father_CitizenshipID') == $row->NationalityID)
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Nationality }}"/>
              @endif
            @endforeach
          </div>
        </div>

        <div class="column-6">
          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Mother's full name:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_Name') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Home address:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_Address') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Date of birth:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.setDateFormat(getObjectValue($family_data,'Mother_BirthDate'),'yyyy-mm-dd','mm/dd/yyyy') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Civil status:</label>
            @foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
              @if(getObjectValue($family_data,'Mother_MaritalID') == $row->StatusID)
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->CivilDesc  }}"/>
              @endif
            @endforeach
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Home phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_TelNo') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Mobile phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_Mobile') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Employer: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_Company') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Position: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_Occupation') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Type of business: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_BusinessType') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Work phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_CompanyPhone') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Work address: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_CompanyAddress') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">E-mail:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_Email') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Educational attainment: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Mother_EducAttainment') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Religion: </label>
            @foreach(App\Modules\Admission\Models\Admission::getReligion() as $row)
              @if(getObjectValue($family_data,'Mother_ReligionID') == $row->ReligionID)
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Religion }}"/>
              @endif
            @endforeach
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Nationality: </label>
            @foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
              @if(getObjectValue($family_data,'Mother_NationalityID') == $row->NationalityID)
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Nationality }}"/>
              @endif
            @endforeach
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Citizenship: </label>
            @foreach(App\Modules\Admission\Models\Admission::getNationality() as $row)
              @if(getObjectValue($family_data,'Mother_CitizenshipID') == $row->NationalityID)
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Nationality }}"/>
              @endif
            @endforeach
          </div>
        </div>
      </div>

      <!------------------------------ INPUT ROW TWELVE ------------------------------>
      <div class="input-row-12">
        <div class="input-group mb-md">
          <label class="input-label mr-xs">Date of Marriage:</label>
          <input type="text" class="input-control input-border-bottom" style="text-align: center; width:80px;" value="{{date('d', strtotime(getObjectValue($family_data,'FatherMother_MarriageDate')))}}"/>
          <input type="text" class="input-control input-border-bottom" style="text-align: center;" value="{{date('m', strtotime(getObjectValue($family_data,'FatherMother_MarriageDate')))}}"/>
          <input type="text" class="input-control input-border-bottom" style="text-align: center;" value="{{date('Y', strtotime(getObjectValue($family_data,'FatherMother_MarriageDate')))}}"/>
          <ul class="input-list">
            <li>Day</li>
            <li>Month</li>
            <li>Year</li>
          </ul>
        </div>
      </div>

      <p class="mb-md">
        To whom should admission correspondence be sent? 
        <span class="checkbox {{ getObjectValue($admission_data,'Family_Correspondence') == 'Father' ? 'checkbox-check' : '' }}"> Father </span> 
        <span class="checkbox {{ getObjectValue($admission_data,'Family_Correspondence') == 'Mother' ? 'checkbox-check' : '' }}"> Mother </span>
        <span class="checkbox {{ getObjectValue($admission_data,'Family_Correspondence') == 'Both' ? 'checkbox-check' : '' }}"> Both </span>
      </p>

      <div class="input-group w-100 mb-md">
        <label class="input-label">Emergency Contact Person if parents cannot be reached:</label>
        <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Name') }}"/>
      </div>

      <div class="input-group w-100 mb-md">
        <label class="input-label">Address:</label>
        <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Address') }}"/>
      </div>

      <div class="columns">
        <div class="column-6">
          <div class="input-group w-100 mb-md">
            <label class="input-label">Home phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_TelNo') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label">Mobile phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Mobile') }}"/>
          </div>
        </div>
        <div class="column-6">
          <div class="input-group w-100 mb-md">
            <label class="input-label">Work phone:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_CompanyPhone') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label">Email:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Email') }}"/>
          </div>
        </div>
      </div>

      <div class="input-group w-100 mb-md">
        <label class="input-label">Relationship to applicant: </label>
        <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Relationship') }}"/>
      </div>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 4 of 5</h4>
      </footer>
    </div>

    <div class="container">
      <section class="mb-lg">
        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Person financially responsible for tuition payments:</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'Family_PaymentResponsible') }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Address for billing:</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Guardian_Billing_AddressFull') }}"/>
        </div>
      </section>

      <section>
        <p>Please check the following if applicable:</p>

        <p>
          <span class="checkbox {{ in_array('Mother deceased', $arr) ? 'checkbox-check' : '' }}"> Mother deceased </span> 
          <span class="checkbox {{ in_array('Father deceased', $arr) ? 'checkbox-check' : '' }}"> Father deceased </span>
          <span class="checkbox {{ in_array('Parents divorced', $arr) ? 'checkbox-check' : '' }}"> Parents divorced </span>
        </p>

        <p class="mb-md">
          <span class="checkbox {{ in_array('Parents separated', $arr) ? 'checkbox-check' : '' }}"> Parents separated </span>
          <span class="checkbox {{ in_array('Mother remarried', $arr) ? 'checkbox-check' : '' }}"> Mother remarried </span>
          <span class="checkbox {{ in_array('Father remarried', $arr) ? 'checkbox-check' : '' }}"> Father remarried </span>
        </p>

        <p class="mb-md">
          If applicant does not live with both parents, please explain family
          situation:
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getObjectValue($admission_data,'Family_Situation') }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <section>
        <p class="mb-md">
          OTHER CHILDREN
        </p>

        <!------------------------------ INPUT ROW THIRTEEN ------------------------------>
        @foreach($siblings as $row)
        <div class="input-row-13 mb-lg">
          <div class="input-group w-100">
            <input type="text" class="input-control input-border-bottom" style="width:80%;" value="{{ $row->FullName }}"/>
            <input type="text" class="input-control input-border-bottom" value="{{ $row->SchoolAttended }}"/>
            <input type="text" class="input-control input-border-bottom" style="width:50%;" value="{{ setDateFormat($row->DateofBirth,'yyyy-mm-dd','mm/dd/yyyy') }}"/>
            <ul class="input-list">
              <li style="margin-right:200px;">Name</li>
              <li style="margin-right:185px;">Present School / Grade</li>
              <li>Date of Birth</li>
            </ul>
          </div>
        </div>
        @endforeach
        @if(count($siblings) < 3)
          @for($i = count($siblings); $i < 3; $i++)
          <div class="input-row-13 mb-lg">
            <div class="input-group w-100 mb-md">
              <input type="text" class="input-control input-border-bottom" style="width:80%;"/>
              <input type="text" class="input-control input-border-bottom"/>
              <input type="text" class="input-control input-border-bottom" style="width:50%;"/>
              <ul class="input-list">
                <li style="margin-right:200px;">Name</li>
                <li style="margin-right:185px;">Present School / Grade</li>
                <li>Date of Birth</li>
              </ul>
            </div>
          </div>
          @endfor
        @endif
        
        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">What types of activities do you enjoy as a family?</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'Familly_Activities') }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <section>
        <h4 class="mb-md">
          <i>REFERENCES</i>
        </h4>

        <p class="mb-md">
          Please list below the names, schools, and telephone numbers of those
          who will be completing the recommendation forms.
        </p>
        
        <div class="columns">
          <div class="column-6">
          @foreach($references as $row)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Name:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Name }}"/>
            </div>

            <div class="input-group w-100 mb-md">
              <label class="PhoneNumber-label mr-xs">School:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->School }}"/>
            </div>
          @endforeach
          @if(count($references) < 2)
            @for($i = count($references); $i < 2; $i++)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Name:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>

            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">School:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>
            @endfor
          @endif
          </div>
          <div class="column-6">
          @foreach($references as $row)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Relationship:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Relationship }}"/>
            </div>

            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Phone number:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->PhoneNumber }}"/>
            </div>
          @endforeach
          @if(count($references) < 2)
            @for($i = count($references); $i < 2; $i++)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Relationship:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>

            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Phone number:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>
            @endfor
          @endif
          </div>
        </div>

        <p class="mb-lg">
          I/We, the undersigned parent(s) or guardian(s), acknowledge that the
          information submitted in this application form is true and correct.
          I/We understand that failure to provide complete and accurate
          information of any kind on this form will void the application and
          could result in the student being permanently dropped from Everest
          Academy after being enrolled. I/We allow Everest Academy to contact
          the student’s previous school for further information about him / her,
          as needed. All information submitted is deemed confidential and will
          be treated as such by Everest Academy.
        </p>

        <div class="columns gutter-lg">
          <div class="column-6">
            <h4 class="mb-md">
              Father or Guardian
            </h4>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Printed Name</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Signature</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Date</li>
              </ul>
            </div>
          </div>
          <div class="column-6">
            <h4 class="mb-md">
              Mother or Guardian
            </h4>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Printed Name</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Signature</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Date</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 5 of 5</h4>
      </footer>
    </div>
  </body>
</html>
