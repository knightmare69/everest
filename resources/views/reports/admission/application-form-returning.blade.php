<?php 
  $appno = decode(Request::get('AppNo'));
  $indent = str_repeat('&nbsp;', 3);

  $admission_data = App\Modules\Admission\Models\Admission::where('AppNo',$appno)->get();
  $admission_data = isset($admission_data[0]) ? $admission_data[0] : [];  

  $family_data = App\Modules\Admission\Models\FatherBackground::where('FamilyID',getGuardianFamilyID($appno))->get(); 
  $family_data = isset($family_data[0]) ? $family_data[0] : [];  

  $ay = App\Modules\Setup\Models\AcademicYearTerm::where('TermID', getObjectValue($admission_data,'TermID'))->get();
  $ay = isset($ay[0]) ? $ay[0] : [];

  $last_attended = explode(',',getObjectValue($admission_data,'PresentSchoolDateAttended'));

  $sch_attended = App\Modules\Admission\Models\SchoolsAttended::where(['AppNo'=>$appno])->get();
  $sch_attended = isset($sch_attended) ? $sch_attended : []; 

  $status = explode(',',getObjectValue($admission_data,'Family_HealthStatus')); 
  $arr = [];
  foreach($status as $stats) {
    $arr[] = $stats;
  }

  $references = App\Modules\Admission\Models\References::where('AppNo',$appno)->get();
  $references = isset($references) ? $references : [];

  function getAnswerByID($key){
    $qns_answer = App\Modules\Admission\Models\QuestionaireAnswer::where(['app_no'=>decode(Request::get('AppNo')),'question_id'=>$key])->get();
    return isset($qns_answer[0]) ? $qns_answer[0]->answer : '';
  }

 ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Application Form (Returning Students)</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="{{url("assets/css/application-form.min.css")}}" />
  </head>
  <style>
  .checkbox-check::after {
    content: '\2714';
    position: absolute;
    left: -12px;
  }
  </style>
  <body>
    <!--
      !WARNING: All the codes below are hard coded in CSS, Please be careful when modifying the html structure.
    <!-->

    <div class="container">
      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ HEADER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <header class="header">
        <!------------------------------ Header logo ------------------------------>
        <div class="header__logo">
          <img src="{{url("assets/img/logo.jpg")}}" alt="logo" />
        </div>

        <!------------------------------ Header title ------------------------------>
        <div class="header__title">
          <h3>APPLICATION FORM (Returning Student)</h3>
          <small>*Please inform school for any subsequent changes</small>
        </div>
      </header>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ MAIN
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <main class="main">
        <div class="main__header">
          <h4>Dear Parents,</h4>

          <p>
            Thank you for your interest in Everest Academy. The Admissions
            Committee always tries to make decisions in the best interest of
            each child. Your responses in this form will help us get to know
            your child, guide us as we evaluate his/her application and, with
            the hope that admission is offered, ensure a smooth transition into
            our school.
          </p>
        </div>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ SECTION - APPLICATION
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <section class="section-application__two d-flex">
          <div class="input-group">
            <label class="input-label mr-xs">Date of Application: </label>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{date('d', strtotime($admission_data->AppDate))}}"/>
            <input type="text" class="input-control input-border-bottom" style="width:70%; text-align:center;" value="{{date('m', strtotime($admission_data->AppDate))}}"/>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{date('Y', strtotime($admission_data->AppDate))}}"/>
            <ul class="input-list">
              <li>Day</li>
              <li>Month</li>
              <li>Year</li>
            </ul>
          </div>

          <div class="input-group">
            <label class="input-label">SY 20</label>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ substr($ay->AcademicYear, 2,2) }}"/>
          </div>

          <div class="input-group">
            <label class="input-label">to 20</label>
            <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ substr($ay->AcademicYear,-2) }}"/>
          </div>
        </section>

        <h4>
          <i>*Please do not leave any blanks, for items that are not applicable
            kindly write NA.
          </i>
        </h4>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ SECTION - STUDENT INFORMATION
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <section class="mt-md">
          <h4 class="mb-md">
            <i>
              STUDENT INFORMATION
            </i>
          </h4>

          <!------------------------------ INPUT ROW ONE ------------------------------>
          <div class="input-row-1 d-flex">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Full Name:</label>
              <input type="text" class="input-control input-border-bottom" style="width:32%;text-align:center;" value="{{ getObjectValue($admission_data,'LastName') }}"/>
              <input type="text" class="input-control input-border-bottom" style="width:33.3%;" value="{{ getObjectValue($admission_data,'FirstName') }}"/>
              <input type="text" class="input-control input-border-bottom" style="width:33.3%;" value="{{ getObjectValue($admission_data,'MiddleName') }}"/>
              <ul class="input-list">
                <li>Last</li>
                <li>First</li>
                <li>Middle</li>
              </ul>
            </div>

            <div class="input-group">
              <label class="input-label mr-xs">Nickname:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'NickName') }}"/>
            </div>
          </div>

          <!------------------------------ INPUT ROW TWO ------------------------------>
          <div class="input-row-2 d-flex">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Date of Birth:</label>
              <input type="text" class="input-control input-border-bottom" style="width:30%;text-align:center;" value="{{date('d', strtotime($admission_data->DateOfBirth))}}"/>
              <input type="text" class="input-control input-border-bottom" style="width:20%;text-align:center;" value="{{date('m', strtotime($admission_data->DateOfBirth))}}"/>
              <input type="text" class="input-control input-border-bottom" style="width:40%;text-align:center;" value="{{date('Y', strtotime($admission_data->DateOfBirth))}}"/>
              <ul class="input-list">
                <li style="margin-right:45px;">Day</li>
                <li style="margin-right:45px;">Month</li>
                <li>Year</li>
              </ul>
            </div>

            <span class="checkbox {{ getObjectValue($admission_data,'IsAdopted') ? 'checkbox-check' : '' }}">Adopted</span>

            <div class="input-group">
              <label class="input-label">Age:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.Request::get('age') }}"/>
            </div>

            <div>
              <span class="checkbox {{ getObjectValue($admission_data,'Gender') == 'M' ? 'checkbox-check' : '' }}">Male</span>
              <span class="checkbox {{ getObjectValue($admission_data,'Gender') == 'F' ? 'checkbox-check' : '' }}">Female</span>
            </div>
          </div>

          <!------------------------------ INPUT ROW THREE ------------------------------>
          <div class="input-row-3 d-flex">
            <div class="input-group stretch">
              <label class="input-label mr-xs">Home Address:</label>
              <input type="text" class="input-control input-border-bottom" style="width:60%; text-align:center;" value="{{ getObjectValue($admission_data,'HomeAddressUnitNo') }}"/>
              <input type="text" class="input-control input-border-bottom" style="text-align:center; width:150%;" value="{{ getObjectValue($admission_data,'HomeAddressStreet') }}"/>
              <input type="text" class="input-control input-border-bottom" value="{{ getObjectValue($admission_data,'HomeAddressBrgy') }}"/>
              <ul class="input-list">
                <li>House/Unit No</li>
                <li>Street</li>
                <li>Barangay</li>
              </ul>
            </div>
          </div>

          <!------------------------------ INPUT ROW FOUR ------------------------------>
          <div class="input-row-4 d-flex">
            <div class="input-group">
              <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ getObjectValue($admission_data,'HomeAddressCity') }}"/>
              <input type="text" class="input-control input-border-bottom" style="text-align:center;" value="{{ getObjectValue($admission_data,'HomeAddressZipCode') }}"/>
              <ul class="input-list">
                <li>City</li>
                <li>Zip/Postal Code</li>
              </ul>
            </div>

            <div class="input-group input-group--two">
              <label class="input-label">Home Phone:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'HomePhone') }}"/>
            </div>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label">Emergency Contact Person if parents cannot be reached:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Name') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label">Address:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Address') }}"/>
          </div>

          <div class="columns">
            <div class="column-6">
              <div class="input-group w-100 mb-md">
                <label class="input-label">Home phone:</label>
                <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_TelNo') }}"/>
              </div>

              <div class="input-group w-100 mb-md">
                <label class="input-label">Mobile phone:</label>
                <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Mobile') }}"/>
              </div>
            </div>
            <div class="column-6">
              <div class="input-group w-100 mb-md">
                <label class="input-label">Work phone:</label>
                <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_CompanyPhone') }}"/>
              </div>

              <div class="input-group w-100 mb-md">
                <label class="input-label">Email:</label>
                <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Email') }}"/>
              </div>
            </div>
          </div>

          <div class="input-group w-100 mb-lg">
            <label class="input-label">Relationship to applicant: </label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'EmergencyContact_Relationship') }}"/>
          </div>
        </section>

        <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ SECTION - STUDENT INFORMATION
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
        <section>
          <h4 class="mb-md">
            <i>
              SCHOLASTIC INFORMATION
            </i>
          </h4>

          <div class="input-row-14 d-flex mb-md">
            <div class="input-group">
              <label class="input-label">Last grade level completed:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentGradeLevel') }}"/>
            </div>

            <div class="input-group">
              <label class="input-label">Last month/year attended:</label>
              <input type="text" class="input-control input-border-bottom" style="text-align:center" value="{{ $last_attended[0] }}"/>/
              <input type="text" class="input-control input-border-bottom" style="text-align:center" value="{{ $last_attended[1] }}"/>
            </div>
          </div>

          <div class="input-group w-100 mb-md">
            <label class="input-label mr-xs">Reason for leaving:</label>
            <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolReasonLeaving') }}"/>
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>

          <div class="input-group w-100 mb-md">
            <input type="text" class="input-control input-border-bottom" />
          </div>
        </section>
      </main>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 1 of 4</h4>
      </footer>
    </div>

    <div class="container">
      <section>
        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Reasons for re-applying:</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'PresentSchoolReasonReApplying') }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>
      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Did the applicant pass all subjects during the last grading period?
          <span class="checkbox {{ getObjectValue($admission_data,'PresentSchoolPassAllSubjects') == 'Yes' ? 'checkbox-check' : '' }}">Yes</span>
          <span class="checkbox {{ getObjectValue($admission_data,'PresentSchoolPassAllSubjects') == 'No' ? 'checkbox-check' : '' }}">No,</span>
          subject/s failing are:
        </p>

        <div class="input-group w-100">
          <input type="text" class="input-control input-border-bottom" value="{{ getObjectValue($admission_data,'PresentSchoolSubjectsFailing') }}"/>
        </div>

        <table class="table mt-md mb-md">
          <thead class="text-center">
            <tr>
              <th>Name of School/s Attended</th>
              <th>Address</th>
              <th>
                Dates Attended
                <br />
                (month-year to month-year)
              </th>
              <th>
                Grade
                <br />
                Level
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($sch_attended as $row)
            <tr>
              <td style="text-align:center">{{ $row->school }}</td>
              <td style="text-align:center">{{ $row->address }}</td>
              <td style="text-align:center">{{ str_replace("/","-",$row->fromDate).' to '.str_replace("/","-",$row->toDate) }}</td>
              <td style="text-align:center">{{ $row->yearLevel }}</td>
            </tr>
            @endforeach
            @if(count($sch_attended) < 3)
              @for($i = count($sch_attended); $i <= 3; $i++)
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              @endfor
            @endif
          </tbody>
        </table>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Please list your child’s honors, awards, outstanding achievements and
          leadership roles (team captain, class president, role in a play, etc…)
          since leaving Everest.
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(39) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Please list areas in which you would like to see further development
          now that you are re-applying.
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(40) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Has your child had any behavioral / disciplinary difficulties at
          previous schools? <span class="checkbox {{ getAnswerByID(24) == 'Yes' ? 'checkbox-check' : '' }}">Yes</span> <span class="checkbox {{ getAnswerByID(24) == 'No' ? 'checkbox-check' : '' }}">No</span> If YES, please give details
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(25) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 2 of 4</h4>
      </footer>
    </div>

    <div class="container">
      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Has your child had any behavioral, psychological or educational
          evaluations?
          <span class="checkbox {{ getAnswerByID(27) == 'No' ? 'checkbox-check' : '' }}">No</span> <span class="checkbox {{ getAnswerByID(27) == 'Yes' ? 'checkbox-check' : '' }}">Yes</span>
        </p>

        <div class="input-group w-100 mb-md">
          <label class="input-label">If yes, when was your child evaluated?</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(28) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label">Who referred your child for evaluation?</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(29) }}"/>
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What is your child’s behavioral, psychological or educational need
          based on the evaluation report? (e.g. ADHD, ASD, Speech Delay etc.)
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(30) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          Was your child recommended to undergo intervention or therapy? <span class="checkbox {{ getAnswerByID(31) == 'No' ? 'checkbox-check' : '' }}">No</span> <span class="checkbox {{ getAnswerByID(31) == 'Yes' ? 'checkbox-check' : '' }}">Yes</span>. If YES please list what type of therapy:
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(32) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <p class="mb-md">
          What is the duration of therapy? (Write inclusive days/ weeks/ months)
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getAnswerByID(33) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Name of specialist/ therapist</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(34) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Hospital/ Clinic name and address</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(35) }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label mr-xs">Contact number of specialist/ therapist</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getAnswerByID(36) }}"/>
        </div>
      </section>

      <h4 class="mb-md">
        <i>* Please submit a photocopy of your child’s complete evaluation and
          therapy report.
        </i>
      </h4>

      <!------------------------------------------------------------------------------------------------------------------------>
      <section>
        <h4 class="mb-md">
          <i>
            PARENT INFORMATION
          </i>
        </h4>

        <div class="input-group w-100 mb-md">
          <label class="input-label">Person financially responsible for tuition payments:</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($admission_data,'Family_PaymentResponsible') }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <label class="input-label">Address for billing:</label>
          <input type="text" class="input-control input-border-bottom" value="{{ $indent.getObjectValue($family_data,'Guardian_Billing_AddressFull') }}"/>
        </div>
      </section>

      <section>
        <p>Please check the following if applicable:</p>

        <p>
          <span class="checkbox {{ in_array('Mother deceased', $arr) ? 'checkbox-check' : '' }}"> Mother deceased </span> 
          <span class="checkbox {{ in_array('Father deceased', $arr) ? 'checkbox-check' : '' }}"> Father deceased </span>
          <span class="checkbox {{ in_array('Parents divorced', $arr) ? 'checkbox-check' : '' }}"> Parents divorced </span>
        </p>

        <p class="mb-md">
          <span class="checkbox {{ in_array('Parents separated', $arr) ? 'checkbox-check' : '' }}"> Parents separated </span>
          <span class="checkbox {{ in_array('Mother remarried', $arr) ? 'checkbox-check' : '' }}"> Mother remarried </span>
          <span class="checkbox {{ in_array('Father remarried', $arr) ? 'checkbox-check' : '' }}"> Father remarried </span>
        </p>

        <p class="mb-md">
          If applicant does not live with both parents, please explain family
          situation:
        </p>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" value="{{ getObjectValue($admission_data,'Family_Situation') }}"/>
        </div>

        <div class="input-group w-100 mb-md">
          <input type="text" class="input-control input-border-bottom" />
        </div>
      </section>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 3 of 4</h4>
      </footer>
    </div>

    <div class="container">
      <table class="table mb-md">
        <tbody>
          <tr class="vertical-top height-50">
            <td class="padding">Father's full name:{{ $indent.getObjectValue($family_data,'Father_Name') }}</td>
            <td class="padding">Mother’s full name:{{ $indent.getObjectValue($family_data,'Mother_Name') }}</td>
          </tr>

          <tr class="vertical-top height-50">
            <td class="padding">Home address:{{ $indent.getObjectValue($family_data,'Father_Address') }}</td>
            <td class="padding">Home address:{{ $indent.getObjectValue($family_data,'Mother_Address') }}</td>
          </tr>

          <tr>
            <td class="padding">Civil status:
            @foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
              @if(getObjectValue($family_data,'Father_MaritalID') == $row->StatusID)
              {{ $indent.$row->CivilDesc  }}
              @endif
            @endforeach
            </td>
            <td class="padding">Civil status:
            @foreach(App\Modules\Admission\Models\Admission::getCivilStatus() as $row)
              @if(getObjectValue($family_data,'Mother_MaritalID') == $row->StatusID)
              {{ $indent.$row->CivilDesc  }}
              @endif
            @endforeach
            </td>
          </tr>

          <tr>
            <td class="padding">Home phone:{{ $indent.getObjectValue($family_data,'Father_TelNo') }}</td>
            <td class="padding">Home phone:{{ $indent.getObjectValue($family_data,'Mother_TelNo') }}</td>
          </tr>

          <tr>
            <td class="padding">Mobile phone:{{ $indent.getObjectValue($family_data,'Father_Mobile') }}</td>
            <td class="padding">Mobile phone:{{ $indent.getObjectValue($family_data,'Mother_Mobile') }}</td>
          </tr>

          <tr>
            <td class="padding">Employer:{{ $indent.getObjectValue($family_data,'Father_Company') }}</td>
            <td class="padding">Employer:{{ $indent.getObjectValue($family_data,'Mother_Company') }}</td>
          </tr>

          <tr>
            <td class="padding">Position:{{ $indent.getObjectValue($family_data,'Father_Occupation') }}</td>
            <td class="padding">Position:{{ $indent.getObjectValue($family_data,'Mother_Occupation') }}</td>
          </tr>

          <tr>
            <td class="padding">Type of business:{{ $indent.getObjectValue($family_data,'Father_BusinessType') }}</td>
            <td class="padding">Type of business:{{ $indent.getObjectValue($family_data,'Mother_BusinessType') }}</td>
          </tr>

          <tr>
            <td class="padding">Work phone:{{ $indent.getObjectValue($family_data,'Father_CompanyPhone') }}</td>
            <td class="padding">Work phone:{{ $indent.getObjectValue($family_data,'Mother_CompanyPhone') }}</td>
          </tr>

          <tr class="vertical-top height-50">
            <td class="padding">Work address:{{ $indent.getObjectValue($family_data,'Father_CompanyAddress') }}</td>
            <td class="padding">Work address:{{ $indent.getObjectValue($family_data,'Mother_CompanyAddress') }}</td>
          </tr>

          <tr>
            <td class="padding">Email:{{ $indent.getObjectValue($family_data,'Father_Email') }}</td>
            <td class="padding">Email:{{ $indent.getObjectValue($family_data,'Mother_Email') }}</td>
          </tr>
        </tbody>
      </table>

      <section>
        <h4 class="mb-md">
          <i>REFERENCES</i>
        </h4>

        <p class="mb-md">
          Please list below the names, schools, and telephone numbers of those
          who will be completing the recommendation forms.
        </p>

        <div class="columns">
          <div class="column-6">
          @foreach($references as $row)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Name:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Name }}"/>
            </div>

            <div class="input-group w-100 mb-md">
              <label class="PhoneNumber-label mr-xs">School:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->School }}"/>
            </div>
          @endforeach
          @if(count($references) < 2)
            @for($i = count($references); $i < 2; $i++)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Name:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>

            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">School:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>
            @endfor
          @endif
          </div>
          <div class="column-6">
          @foreach($references as $row)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Relationship:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->Relationship }}"/>
            </div>

            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Phone number:</label>
              <input type="text" class="input-control input-border-bottom" value="{{ $indent.$row->PhoneNumber }}"/>
            </div>
          @endforeach
          @if(count($references) < 2)
            @for($i = count($references); $i < 2; $i++)
            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Relationship:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>

            <div class="input-group w-100 mb-md">
              <label class="input-label mr-xs">Phone number:</label>
              <input type="text" class="input-control input-border-bottom" />
            </div>
            @endfor
          @endif
          </div>
        </div>

        <p class="mb-lg">
          I/We, the undersigned parent(s) or guardian(s), acknowledge that the
          information submitted in this application form is true and correct.
          I/We understand that failure to provide complete and accurate
          information of any kind on this form will void the application and
          could result in the student being permanently dropped from Everest
          Academy after being enrolled. I/We allow Everest Academy to contact
          the student’s previous school for further information about him / her,
          as needed. All information submitted is deemed confidential and will
          be treated as such by Everest Academy.
        </p>

        <div class="columns gutter-lg">
          <div class="column-6">
            <h4 class="mb-md">
              Father or Guardian
            </h4>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Printed Name</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Signature</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Date</li>
              </ul>
            </div>
          </div>
          <div class="column-6">
            <h4 class="mb-md">
              Mother or Guardian
            </h4>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Printed Name</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Signature</li>
              </ul>
            </div>

            <div class="input-group w-100 mb-lg">
              <input type="text" class="input-control input-border-bottom" />

              <ul class="input-list">
                <li>Date</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ FOOTER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <footer class="footer">
        <h4>Everest Academy Application Form: G2-11 (ver.June2019)</h4>
        <h4>Page 4 of 4</h4>
      </footer>
    </div>
  </body>
</html>
