<style>
	.border-bottom { border-bottom: 1px solid #000 }
	.border { border: 0.5px solid #000 }
	.right { text-align: right; }
	.bold { font-weight: bold; }
	.center { text-align: center; }
	.left { text-align: left; }
	.valign { vertical-align: middle; }
	.bg { background-color: #EDEBE0; }
	.pad-5 { padding: 10px; }
</style>
<?php

    $id = decode(Request::get('ref'));
    $temp = 0;
    $data = DB::table("ES_AddChangeDropSubjects AS a")
                ->leftJoin("ES_Registrations as r", 'a.RegID','=','r.RegID')
                ->selectRaw("a.*, r.StudentNo, dbo.fn_StudentName(r.StudentNo) As StudentName, dbo.fn_AcademicYearTerm(r.TermID) As Term, dbo.fn_ProgramName(r.ProgID) As Program, dbo.fn_MajorName(r.MajorID) As Major ")
                ->where('RefID',$id)->first();

    $subj = DB::select("SELECT  s.SubjectID ,
                            s.SubjectCode ,
                            s.SubjectTitle ,
                            cs.SectionID ,
                            dbo.fn_SectionName(cs.SectionID) AS SectionName ,
                            cs.Sched_1 ,
                            cs.Room1_ID ,
                            dbo.fn_RoomName(cs.Room1_ID) AS RoomName ,
                            s.LectHrs AS Credit ,
                            dbo.fn_EmployeeName(FacultyID) AS Faculty ,
                            a.TransType As RegTagID ,
                            a.Target_ScheduleID AS ScheduleID
                    FROM dbo.ES_AddChangeDropDetails AS a
                    	LEFT JOIN dbo.ES_ClassSchedules cs ON a.Target_ScheduleID=cs.ScheduleID
                    	LEFT JOIN dbo.ES_Subjects s ON cs.SubjectID = s.SubjectID
                    WHERE a.TransID = '{$id}' order by RegTagID asc ");

    $items =  DB::select("SELECT a.AcctID, a.AcctName, a.AcctCode, j.Debit
                    FROM dbo.ES_JOURNALS AS j
                    	LEFT JOIN dbo.ES_Accounts a ON j.AccountID=a.AcctID
                    WHERE j.TransID = 2 AND j.ReferenceNo = '{$id}' ");

    $name = 'asdasd';
    $title = ENV('APP_TITLE');
    $address = ENV('APP_ADDRESS');
    $term = $data->Term;
    $date = systemDate();
    $template = "16-17-SHS Summer Class Program";

    $tag = array(
         0 => 'Regular'
        ,1 => 'ADDED'
        ,2 => 'CHANGED'
        ,3 => 'DROPPED'
    );
    $total = 0;
?>
<table>
    <tbody>
        <tr>
            <td><b>{{$title}}</b></td>
            <td></td>
        </tr>
        <tr>
            <td>SENIOR HIGH SCHOOL</td>
            <td style="text-align: right;">{{$date}}</td>
        </tr>
        <tr>
            <td>{{$term}}</td>
            <td style="text-align: right;">ADD/DROP/CHANGE SUBJECT</td>
        </tr>
         <tr>
            <td>REG.# {{$id}}</td>
            <td style="text-align: right;">{{$template}}</td>
        </tr>
    </tbody>
</table>
<hr />
<br />

<table class="center" style="width:100%;"  >
    <tr>
        <td colspan="2" class="left bold" >{{$data->StudentNo . ' ' . $data->StudentName }}</td>

    </tr>
    <tr>
        <td class="left">{{$data->Program}}</td>
        <td></td>
    </tr>
    <tr>
        <td class="left">&nbsp; - {{$data->Major}}</td>
        <td></td>
    </tr>
    <tr>
        <td class="border-bottom" ></td>
        <td class="border-bottom" ></td>
    </tr>
    <tr>
        <td colspan="2" >
            <table class="center" style="width:100%;" >
            <thead>
                <tr >
                    <th class="border-bottom" width="20%">CODE</th>
                    <th class="border-bottom" width="40%">SUBJECT TITLE</th>
                    <th class="border-bottom" width="20%">CREDIT</th>
                    <th class="border-bottom" width="20%">SECTION</th>
                </tr>
            </thead>
        	<tbody>
                @foreach($subj as $s)
                    @if($s->RegTagID!=$temp)
                        <?php $temp = $s->RegTagID;  ?>
                    <tr>
                        <td class="left" colspan="4"><?= $tag[$temp] ?> SUBJECT(s): <br /> ********** </td>
                    </tr>
                    @endif
            		<tr>
                    	<td>{{$s->SubjectCode}}</td>
                        <td>{{$s->SubjectTitle}} <br />
                        {{$s->Sched_1}}
                        </td>
                        <td>{{$s->Credit}}</td>
                        <td>{{$s->SectionName}}</td>
                    </tr>
                @endforeach
             </tbody>
             </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="border-bottom">
        </td>
    </tr>
    <tr>
        <td colspan="2" class="left">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="left">ASSESSMENT : <br />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="left">
         <table class="center" style="width:100%;" cellpadding="1" >
            <thead >
                <tr >
                    <th border="1" colspan="2" > PARTICULARS </th>
                    <th border="1" class="border-bottom" >AMOUNT</th>
                </tr>
            </thead>
        	<tbody>
                @foreach($items as $i)
                    <tr>
                        <td>{{$i->AcctCode}}</td>
                        <td>{{$i->AcctName}}</td>
                        <td>{{ number_format($i->Debit,2) }}</td>
                    </tr>
                    <?php $total += floatval($i->Debit) ?>
                @endforeach
             </tbody>
             <tfoot>
                <tr>
                    <td colspan="2" class="right"> Total : </td>
                    <td class="bold">{{number_format($total,2)}}</td>
                </tr>
             </tfoot>
             </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="left"></td>
    </tr>
    <tr>
        <td class="left">Endorsed by : ___________________________<br /> Coordinator</td>
        <td class="left">Approved by : ___________________________<br /> Principal</td>
    </tr>
</table>