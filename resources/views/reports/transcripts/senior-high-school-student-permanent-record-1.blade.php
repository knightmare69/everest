<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<?php
//sbu -- 2018100122
$stdno = ((isset($stdno))?$stdno:'2008087');
$ay    = DB::select("SELECT r.TermID,t.AcademicYear,t.SchoolTerm
						  ,(SELECT YearLevelCode FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelCode
						  ,(SELECT YearLevelName FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelName
						  ,r.YearLevelID,r.ProgID,r.MajorID
						  ,p.ProgCode,p.ProgName
						  ,m.MajorDiscCode,m.MajorDiscDesc
						  ,pm.AcademicA_Average,pm.AcademicB_Average,pm.AcademicC_Average,pm.AcademicD_Average,pm.Final_Average
						  ,pm.ConductA_Average,pm.ConductB_Average,pm.ConductC_Average,pm.ConductD_Average,pm.Final_Conduct
						  ,pm.TotalSubjects,pm.SchoolDays,pm.PresentDays
						  ,pa.DaysPresent
						  ,t.StartofAY,t.EndofAY
					  FROM ES_Registrations as r
				INNER JOIN ES_AYTerm as t on r.TermID=t.TermID
				INNER JOIN ES_PermanentRecord_Master as pm ON r.TermID=pm.TermID AND r.StudentNo=pm.StudentNo AND r.RegID=pm.RegID
				 LEFT JOIN ES_PermanentRecord_Attendance as pa ON r.TermID=pa.TermID AND r.StudentNo=pa.StudentNo
				INNER JOIN ES_Programs as p ON r.ProgID=p.ProgID
				 LEFT JOIN ES_DisciplineMajors as m ON r.MajorID=m.IndexID
					 WHERE r.StudentNo='".$stdno."'
				  ORDER BY t.AcademicYear,t.SchoolTerm");
				  
$rs    = DB::select("SELECT r.StudentNo,s.LRN
						   ,LastName,FirstName,Middlename,MiddleInitial,FullName
						   ,DateOfBirth,PlaceOfBirth
						   ,Father,Mother
						   ,s.Res_Address,s.Res_Street,s.Res_TownCity,s.Res_Province,s.Res_ZipCode
						   ,DateGraduated
						   ,(SELECT Nationality FROM Nationalities WHERE NationalityID=s.NationalityID) as Nationality
						   ,r.RegID,r.RegDate
						   ,r.TermID
						   ,(SELECT YearLevelCode FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelCode
						   ,(SELECT YearLevelName FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelName
						   ,(SELECT AcademicYear FROM es_ayterm WHERE TermID=r.TermID) as AcademicYear
						   ,(SELECT SchoolTerm FROM es_ayterm WHERE TermID=r.TermID) as SchoolTerm
						   ,rd.ScheduleID,rd.ScheduleID_Old,rd.RegTagID
                           ,pr.SubjectID,pr.SubjectCode,pr.SubjectTitle
                           ,pr.AverageA						   
                           ,pr.AverageB						   
                           ,pr.AverageC						   
                           ,pr.AverageD
                           ,pr.TotalAverageCD						   
					   FROM ES_Registrations as r
				 INNER JOIN ES_Students as s ON r.StudentNo=s.StudentNo
				  LEFT JOIN ES_RegistrationDetails as rd ON r.RegID=rd.RegID
                  LEFT JOIN ES_PermanentRecord_Details as pr ON r.StudentNo=pr.StudentNo AND r.RegID=pr.RegID AND rd.ScheduleID=pr.ScheduleID
					  WHERE r.StudentNo='".$stdno."'
				   ORDER BY r.RegID,pr.GradeIDX");
$stdinfo   = $rs[0];					
$termid    = $ay[0]->TermID;
$recordid  = 0; 
$profile   = url('assets/system/media/profile/'.$stdno.'/pic.jpg');
$skip_term = array(); 
?>
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Senior High School - Student Permanent Record</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo url('assets/test/css/student-permanent-record-1.min.css');?>"/>
  </head>
  <body>
    <div class="container">
      <header class="header">
        <img src="<?php echo url('assets/test/img/logo.jpg');?>" alt="logo" class="header-logo" />
        <div class="header-title">
          <h1>SAN BEDA UNIVERSITY</h1>
          <h5>RIZAL</h5>
        </div>
        <div class="header-avatar">
          <img
            src="<?php echo $profile;?>"
            alt="avatar"
          />
        </div>
      </header>

      <main class="main">
        <div class="main-header">
          <p></p>
          <h2>Office of the Registrar</h2>
          <h3>
            SENIOR HIGH SCHOOL STUDENT'S PERMANENT RECORD
          </h3>
        </div>

        <div class="columns mb-5">
          <div class="column-6">
            <div class="input-group">
              <label>Name:</label>
              <input type="text" value="<?php echo $stdinfo->FullName;?>"/>
            </div>

            <div class="input-group">
              <label>Date of Birth:</label>
              <input type="text" value="<?php echo (($stdinfo->DateOfBirth!='')?(date('m/d/Y',strtotime($stdinfo->DateOfBirth))):'');?>"/>
            </div>

            <div class="input-group">
              <label>Place of Birth:</label>
              <input type="text" value="<?php echo $stdinfo->PlaceOfBirth;?>"/>
            </div>

            <div class="input-group">
              <label>Father:</label>
              <input type="text" value="<?php echo $stdinfo->Father;?>"/>
            </div>

            <div class="input-group">
              <label>Mother:</label>
              <input type="text" value="<?php echo $stdinfo->Mother;?>"/>
            </div>
          </div>
          <div class="column-6">
            <div class="input-group">
              <label>LRN:</label>
              <input type="text" value="<?php echo $stdinfo->LRN;?>"/>
            </div>

            <div class="input-group">
              <label>Nationality:</label>
              <input type="text" value="<?php echo $stdinfo->Nationality;?>"/>
            </div>

            <div class="input-group">
              <label>Address:</label>
              <input type="text" value="<?php echo $stdinfo->Res_Address.' '.$stdinfo->Res_Street;?>"/>
            </div>
            <div class="input-group">
              <label style="color:white !important;">Address:</label>
              <input type="text" value="<?php echo $stdinfo->Res_TownCity;?>"/>
            </div>
            <div class="input-group">
              <label style="color:white !important;">Address:</label>
              <input type="text" value="<?php echo $stdinfo->Res_Province;?>"/>
            </div>
          </div>
        </div>

        <div class="columns">
          <div class="column-9">
            <div class="input-group">
              <label>Intermediate Course Completed</label>
              <input type="text" />
            </div>
          </div>

          <div class="column-3">
            <div class="input-group">
              <label>AY</label>
              <input type="text" />
            </div>
          </div>
        </div>

        <div class="columns mb-5">
          <div class="column-9">
            <div class="input-group">
              <label>Junior High School Completed</label>
              <input type="text" />
            </div>
          </div>

          <div class="column-3">
            <div class="input-group">
              <label>AY</label>
              <input type="text" />
            </div>
          </div>
        </div>
        <table class="table border-x">
          <thead class="text-center">
            <tr>
              <th colspan="6">
                <div class="columns">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Grade</label>
                      <input type="text" value="<?php echo $ay[$recordid]->YearLevelName;?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Semester</label>
                      <input type="text" value="<?php echo $ay[$recordid]->SchoolTerm;?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns mb-5">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Strand</label>
                      <input type="text" value="<?php echo $ay[$recordid]->MajorDiscDesc;?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Academic Year</label>
                      <input type="text" value="<?php echo $stdinfo->AcademicYear;?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </th>
            </tr>
            <tr>
              <th rowspan="2" colspan="2" class="border-x">SUBJECTS</th>
              <th colspan="2" class="border">
                <small>
                  Grading Period
                </small>
              </th>
              <th rowspan="2" class="border">
                SEMESTER
                <br />
                FINAL GRADE
              </th>
              <th rowspan="2" class="border-x">
                <small>
                  Action
                  <br />
                  Taken</small
                >
              </th>
            </tr>

            <tr>
              <th class="border">
                <small>1st</small>
              </th>
              <th class="border">
                <small>2nd</small>
              </th>
            </tr>
          </thead>
          <tbody><!-- First Record -->
		    <?php
			$display = 0;
			$skip    = 0;
			foreach($rs as $grades){
			 if($skip==1){
			    continue;
			 }
			 
			 if($grades->TermID==$termid && $grades->SubjectTitle!=''){
			    $avea     = (($grades->AverageA!='')?number_format(floatval($grades->AverageA),2):'');
			    $aveb     = (($grades->AverageB!='')?number_format(floatval($grades->AverageB),2):'');
			    $avec     = (($grades->AverageC!='')?number_format(floatval($grades->AverageC),2):'');
			    $aved     = (($grades->AverageD!='')?number_format(floatval($grades->AverageD),2):'');
			    $totalave = (($grades->TotalAverageCD!='')?number_format(floatval($grades->TotalAverageCD),2):'');
				echo '<tr class="h-20">
						<td colspan="2" class="border-x ">'.$grades->SubjectTitle.'</td>
						<td class="border w-60"><center>'.$avea.'</center></td>
						<td class="border w-60"><center>'.$aveb.'</center></td>
						<td class="border w-110"><center>'.$grades->TotalAverageCD.'</center></td>
						<td class="border-x w-60"></td>
					  </tr>';
				$display++;	  
			 }else{
			   if(in_array($grades->TermID,$skip_term)){
			    continue;
			   }
			   
			   if($skip==0){
				 array_push($skip_term,$termid);
			     $termid    = $grades->TermID; 
				 $skip=1;
			   }
			 }
			
			}
			
			if($display<15){
			   $space = 15-$display;
			   for($space;$space>0;$space--){
			    echo '<tr class="h-20">
						<td colspan="2" class="border-x "></td>
						<td class="border w-60"></td>
						<td class="border w-60"></td>
						<td class="border w-110"></td>
						<td class="border-x w-60"></td>
					  </tr>'; 
			   
			   }
			}
			?>

            <tr class="border-x-double">
              <td colspan="2">
                <strong>GENERAL AVERAGE</strong>
				<?php 
				  $avea     = '';
				  $aveb     = '';
				  $avec     = '';
				  $aved     = '';
				  $cona     = '';
				  $conb     = '';
				  $conc     = '';
				  $cond     = '';
				  
				  $avea     = (($recordid!=-1 && $ay[$recordid]->AcademicA_Average!='')?number_format(floatval($ay[$recordid]->AcademicA_Average),2):'');
				  $aveb     = (($recordid!=-1 && $ay[$recordid]->AcademicB_Average!='')?number_format(floatval($ay[$recordid]->AcademicB_Average),2):'');
				  $avec     = (($recordid!=-1 && $ay[$recordid]->AcademicC_Average!='')?number_format(floatval($ay[$recordid]->AcademicC_Average),2):'');
				  $aved     = (($recordid!=-1 && $ay[$recordid]->AcademicD_Average!='')?number_format(floatval($ay[$recordid]->AcademicD_Average),2):'');
				  
				  $cona     = (($recordid!=-1 && $ay[$recordid]->AcademicA_Average!='')?number_format(floatval($ay[$recordid]->AcademicA_Average),2):'');
				  $conb     = (($recordid!=-1 && $ay[$recordid]->AcademicB_Average!='')?number_format(floatval($ay[$recordid]->AcademicB_Average),2):'');
				  $conc     = (($recordid!=-1 && $ay[$recordid]->AcademicC_Average!='')?number_format(floatval($ay[$recordid]->AcademicC_Average),2):'');
				  $cond     = (($recordid!=-1 && $ay[$recordid]->AcademicD_Average!='')?number_format(floatval($ay[$recordid]->AcademicD_Average),2):'');
				   
                  $totala   = '';
                  $totalc   = '';
                  if($avea!='' && $aveb!=''){
				    $totala = number_format((($avea+$aveb)/2),2);
				  }		
				  
                  if($cona!='' && $conb!=''){
				    $totalc = number_format((($cona+$conb)/2),2);
				  }			  
			    ?>
              </td>
              <td class="border"><center><?php echo $avea;?></center></td>
              <td class="border"><center><?php echo $aveb;?></center></td>
              <td class="border"><center><?php echo $totala;?></center></td>
              <td></td>
            </tr>

            <tr>
              <td class="w-120">Days of School</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"><?php echo (($recordid!=-1)?($ay[$recordid]->DaysPresent):'');?></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Days Present</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"><?php echo (($recordid!=-1)?($ay[$recordid]->DaysPresent):'');?></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Conduct</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr class="border-x">
              <td colspan="6">
                <div class="columns">
                  <div class="column-5">
                    <strong> REMEDIAL/SUMMER CLASS</strong>
                  </div>
                  <div class="column-7">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns">
                  <div class="column-4 column-offset-5">
                    <div class="input-group">
                      <label>Conducted from</label>
                      <input type="text" />
                    </div>
                  </div>
                  <div class="column-3">
                    <div class="input-group">
                      <label>To</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr class="text-center">
              <td colspan="2" class="border-x">
                <strong>SUBJECT/S</strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    Sem Final
                    <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    REMEDIAL <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  RECOMPUTED
                  <br />
                  FINAL GRADE
                </strong>
              </td>
              <td class="border-x">
                <strong>
                  <small>
                    Action
                    <br />
                    Taken
                  </small>
                </strong>
              </td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td></td>
              <td colspan="2" class="border-bottom"></td>
              <td></td>
              <td colspan="2" class="border-bottom"></td>
            </tr>

            <tr class="text-center">
              <td></td>
              <td colspan="2">Recorder's Signature over Printed Name</td>
              <td></td>
              <td colspan="2">Date</td>
            </tr>
          </tbody>
        </table>
        <?php
		$recordid = ((count($ay)>1)?1:-1);
		?>
        <table class="table">
          <thead class="text-center">
            <tr>
              <th colspan="6">
                <div class="columns">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Grade</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->YearLevelName):'');?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Semester</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->SchoolTerm):'');?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns mb-5">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Strand</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->MajorDiscDesc):'');?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Academic Year</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->AcademicYear):'');?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </th>
            </tr>

            <tr>
              <th rowspan="2" colspan="2" class="border-x">SUBJECTS</th>
              <th colspan="2" class="border">
                <small>
                  Grading Period
                </small>
              </th>
              <th rowspan="2" class="border">
                SEMESTER
                <br />
                FINAL GRADE
              </th>
              <th rowspan="2" class="border-x">
                <small>
                  Action
                  <br />
                  Taken</small
                >
              </th>
            </tr>

            <tr>
              <th class="border">
                <small>1st</small>
              </th>
              <th class="border">
                <small>2nd</small>
              </th>
            </tr>
          </thead>
          <tbody><!-- Second Record -->
		    <?php
			$display = 0;
			$skip    = 0;
			$termid  = ((count($ay)>1)?($ay[1]->TermID):0);
			foreach($rs as $grades){
			 if($skip==1){
			    continue;
			 }else{
			    if(!in_array($grades->TermID,$skip_term)){
				   array_push($skip_term,$termid);
			    }
			 }
			 
			 if($grades->TermID==$termid && $grades->SubjectTitle!=''){
			    $avea     = (($grades->AverageA!='')?number_format(floatval($grades->AverageA),2):'');
			    $aveb     = (($grades->AverageB!='')?number_format(floatval($grades->AverageB),2):'');
			    $avec     = (($grades->AverageC!='')?number_format(floatval($grades->AverageC),2):'');
			    $aved     = (($grades->AverageD!='')?number_format(floatval($grades->AverageD),2):'');
			    $totalave = (($grades->TotalAverageCD!='')?number_format(floatval($grades->TotalAverageCD),2):'');
				echo '<tr class="h-20">
						<td colspan="2" class="border-x ">'.$grades->SubjectTitle.'</td>
						<td class="border w-60"><center>'.$avea.'</center></td>
						<td class="border w-60"><center>'.$aveb.'</center></td>
						<td class="border w-110"><center>'.$grades->TotalAverageCD.'</center></td>
						<td class="border-x w-60"></td>
					  </tr>';
				$display++;	  
			 }else{
			   if(in_array($grades->TermID,$skip_term)){
			    continue;
			   }
			   
			   if($skip==0){
				 array_push($skip_term,$termid);
			     $termid = $grades->TermID;
				 $skip=1;
			   }
			 }
			
			}
			
			if($display<15){
			   $space = 15-$display;
			   for($space;$space>0;$space--){
			    echo '<tr class="h-20">
						<td colspan="2" class="border-x "></td>
						<td class="border w-60"></td>
						<td class="border w-60"></td>
						<td class="border w-110"></td>
						<td class="border-x w-60"></td>
					  </tr>'; 
			   
			   }
			}
			?>

            <tr class="border-x-double">
              <td colspan="2">
                <strong>GENERAL AVERAGE</strong>
              </td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td></td>
            </tr>

            <tr>
              <td class="w-120">Days of School</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Days Present</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Conduct</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>
          </tbody>
        </table>
      </main>
    </div>
	
	<div class="container">
      <main class="main">

        <div class="columns mb-5">
          <div class="column-6">
            <div class="input-group">
              <label>Name:</label>
              <input type="text" value="<?php echo $stdinfo->FullName;?>"/>
            </div>
          </div>
          <div class="column-6">
            <div class="input-group">
              <label>LRN:</label>
              <input type="text" value="<?php echo $stdinfo->LRN;?>"/>
            </div>
          </div>
        </div>
		

        <table class="table border-x">
		  <tbody>

            <tr class="border-x">
              <td colspan="6">
                <div class="columns">
                  <div class="column-5">
                    <strong> REMEDIAL/SUMMER CLASS</strong>
                  </div>
                  <div class="column-7">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns">
                  <div class="column-4 column-offset-5">
                    <div class="input-group">
                      <label>Conducted from</label>
                      <input type="text" />
                    </div>
                  </div>
                  <div class="column-3">
                    <div class="input-group">
                      <label>To</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr class="text-center">
              <td colspan="2" class="border-x">
                <strong>SUBJECT/S</strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    Sem Final
                    <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    REMEDIAL <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  RECOMPUTED
                  <br />
                  FINAL GRADE
                </strong>
              </td>
              <td class="border-x">
                <strong>
                  <small>
                    Action
                    <br />
                    Taken
                  </small>
                </strong>
              </td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td></td>
              <td colspan="2" class="border-bottom"></td>
              <td></td>
              <td colspan="2" class="border-bottom"></td>
            </tr>

            <tr class="text-center">
              <td></td>
              <td colspan="2">Recorder's Signature over Printed Name</td>
              <td></td>
              <td colspan="2">Date</td>
            </tr>
		  </tbody>
		</table>
		
        <?php
		$recordid = ((count($ay)>2)?2:-1);
		?>
        <table class="table">
          <thead class="text-center">
            <tr>
              <th colspan="6">
                <div class="columns">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Grade</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->YearLevelName):'');?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Semester</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->SchoolTerm):'');?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns mb-5">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Strand</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->MajorDiscDesc):'');?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Academic Year</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->AcademicYear):'');?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </th>
            </tr>
            <tr>
              <th rowspan="2" colspan="2" class="border-x">SUBJECTS</th>
              <th colspan="2" class="border">
                <small>
                  Grading Period
                </small>
              </th>
              <th rowspan="2" class="border">
                SEMESTER
                <br />
                FINAL GRADE
              </th>
              <th rowspan="2" class="border-x">
                <small>
                  Action
                  <br />
                  Taken</small
                >
              </th>
            </tr>

            <tr>
              <th class="border">
                <small>1st</small>
              </th>
              <th class="border">
                <small>2nd</small>
              </th>
            </tr>
          </thead>
          <tbody><!-- Third Record -->
		    <?php
			$display = 0;
			$skip    = 0;
			$termid  = ((count($ay)>2)?($ay[2]->TermID):0);
			foreach($rs as $grades){
			 if($skip==1){
			    continue;
			 }else{
			    if(!in_array($grades->TermID,$skip_term)){
				   array_push($skip_term,$termid);
			    }
			 }
			 
			 if($grades->TermID==$termid && $grades->SubjectTitle!=''){
			    $avea     = (($grades->AverageA!='')?number_format(floatval($grades->AverageA),2):'');
			    $aveb     = (($grades->AverageB!='')?number_format(floatval($grades->AverageB),2):'');
			    $avec     = (($grades->AverageC!='')?number_format(floatval($grades->AverageC),2):'');
			    $aved     = (($grades->AverageD!='')?number_format(floatval($grades->AverageD),2):'');
			    $totalave = (($grades->TotalAverageCD!='')?number_format(floatval($grades->TotalAverageCD),2):'');
				echo '<tr class="h-20">
						<td colspan="2" class="border-x ">'.$grades->SubjectTitle.'</td>
						<td class="border w-60"><center>'.$avea.'</center></td>
						<td class="border w-60"><center>'.$aveb.'</center></td>
						<td class="border w-110"><center>'.$grades->TotalAverageCD.'</center></td>
						<td class="border-x w-60"></td>
					  </tr>';
				$display++;	  
			 }else{
			   if(in_array($grades->TermID,$skip_term)){
			    continue;
			   }
			   
			   if($skip==0){
				 array_push($skip_term,$termid);
			     $termid = $grades->TermID;
				 $skip=1;
			   }
			 }
			
			}
			
			if($display<15){
			   $space = 15-$display;
			   for($space;$space>0;$space--){
			    echo '<tr class="h-20">
						<td colspan="2" class="border-x "></td>
						<td class="border w-60"></td>
						<td class="border w-60"></td>
						<td class="border w-110"></td>
						<td class="border-x w-60"></td>
					  </tr>'; 
			   
			   }
			}
			?>

            <tr class="border-x-double">
              <td colspan="2">
                <strong>GENERAL AVERAGE</strong>
              </td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td></td>
            </tr>

            <tr>
              <td class="w-120">Days of School</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Days Present</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Conduct</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr class="border-x">
              <td colspan="6">
                <div class="columns">
                  <div class="column-5">
                    <strong> REMEDIAL/SUMMER CLASS</strong>
                  </div>
                  <div class="column-7">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns">
                  <div class="column-4 column-offset-5">
                    <div class="input-group">
                      <label>Conducted from</label>
                      <input type="text" />
                    </div>
                  </div>
                  <div class="column-3">
                    <div class="input-group">
                      <label>To</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr class="text-center">
              <td colspan="2" class="border-x">
                <strong>SUBJECT/S</strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    Sem Final
                    <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    REMEDIAL <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  RECOMPUTED
                  <br />
                  FINAL GRADE
                </strong>
              </td>
              <td class="border-x">
                <strong>
                  <small>
                    Action
                    <br />
                    Taken
                  </small>
                </strong>
              </td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td></td>
              <td colspan="2" class="border-bottom"></td>
              <td></td>
              <td colspan="2" class="border-bottom"></td>
            </tr>

            <tr class="text-center">
              <td></td>
              <td colspan="2">Recorder's Signature over Printed Name</td>
              <td></td>
              <td colspan="2">Date</td>
            </tr>
          </tbody>
        </table>

        <?php
		$recordid = ((count($ay)>3)?3:-1);
		?>
        <table class="table">
          <thead class="text-center">
            <tr>
              <th colspan="6">
                <div class="columns">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Grade</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->YearLevelName):'');?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Semester</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->SchoolTerm):'');?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns mb-5">
                  <div class="column-3">
                    <div class="input-group">
                      <label>Strand</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->MajorDiscDesc):'');?>"/>
                    </div>
                  </div>

                  <div class="column-3">
                    <div class="input-group">
                      <label>Academic Year</label>
                      <input type="text" value="<?php echo (($recordid!=-1)?($ay[$recordid]->AcademicYear):'');?>"/>
                    </div>
                  </div>

                  <div class="column-6">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </th>
            </tr>
            <tr>
              <th rowspan="2" colspan="2" class="border-x">SUBJECTS</th>
              <th colspan="2" class="border">
                <small>
                  Grading Period
                </small>
              </th>
              <th rowspan="2" class="border">
                SEMESTER
                <br />
                FINAL GRADE
              </th>
              <th rowspan="2" class="border-x">
                <small>
                  Action
                  <br />
                  Taken</small
                >
              </th>
            </tr>

            <tr>
              <th class="border">
                <small>1st</small>
              </th>
              <th class="border">
                <small>2nd</small>
              </th>
            </tr>
          </thead>
          <tbody><!-- Fourth Record -->
		    <?php
			$display = 0;
			$skip    = 0;
			$termid  = ((count($ay)>3)?($ay[3]->TermID):0);
			foreach($rs as $grades){
			 if($skip==1){
			    continue;
			 }else{
			    if(!in_array($grades->TermID,$skip_term)){
				   array_push($skip_term,$termid);
			    }
			 }
			 
			 if($grades->TermID==$termid && $grades->SubjectTitle!=''){
			    $avea     = (($grades->AverageA!='')?number_format(floatval($grades->AverageA),2):'');
			    $aveb     = (($grades->AverageB!='')?number_format(floatval($grades->AverageB),2):'');
			    $avec     = (($grades->AverageC!='')?number_format(floatval($grades->AverageC),2):'');
			    $aved     = (($grades->AverageD!='')?number_format(floatval($grades->AverageD),2):'');
			    $totalave = (($grades->TotalAverageCD!='')?number_format(floatval($grades->TotalAverageCD),2):'');
				echo '<tr class="h-20">
						<td colspan="2" class="border-x ">'.$grades->SubjectTitle.'</td>
						<td class="border w-60"><center>'.$avea.'</center></td>
						<td class="border w-60"><center>'.$aveb.'</center></td>
						<td class="border w-110"><center>'.$grades->TotalAverageCD.'</center></td>
						<td class="border-x w-60"></td>
					  </tr>';
				$display++;	  
			 }else{
			   if(in_array($grades->TermID,$skip_term)){
			    continue;
			   }
			   
			   if($skip==0){
				 array_push($skip_term,$termid);
			     $termid = $grades->TermID;
				 $skip=1;
			   }
			 }
			
			}
			
			if($display<15){
			   $space = 15-$display;
			   for($space;$space>0;$space--){
			    echo '<tr class="h-20">
						<td colspan="2" class="border-x "></td>
						<td class="border w-60"></td>
						<td class="border w-60"></td>
						<td class="border w-110"></td>
						<td class="border-x w-60"></td>
					  </tr>'; 
			   
			   }
			}
			?>

            <tr class="border-x-double">
              <td colspan="2">
                <strong>GENERAL AVERAGE</strong>
              </td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td></td>
            </tr>

            <tr>
              <td class="w-120">Days of School</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Days Present</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>

            <tr>
              <td class="w-120">Conduct</td>
              <td class="border-bottom"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-bottom"></td>
            </tr>
			
			

            <tr class="border-x">
              <td colspan="6">
                <div class="columns">
                  <div class="column-5">
                    <strong> REMEDIAL/SUMMER CLASS</strong>
                  </div>
                  <div class="column-7">
                    <div class="input-group">
                      <label>School</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>

                <div class="columns">
                  <div class="column-4 column-offset-5">
                    <div class="input-group">
                      <label>Conducted from</label>
                      <input type="text" />
                    </div>
                  </div>
                  <div class="column-3">
                    <div class="input-group">
                      <label>To</label>
                      <input type="text" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr class="text-center">
              <td colspan="2" class="border-x">
                <strong>SUBJECT/S</strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    Sem Final
                    <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  <small>
                    REMEDIAL <br />
                    Grade
                  </small>
                </strong>
              </td>
              <td class="border">
                <strong>
                  RECOMPUTED
                  <br />
                  FINAL GRADE
                </strong>
              </td>
              <td class="border-x">
                <strong>
                  <small>
                    Action
                    <br />
                    Taken
                  </small>
                </strong>
              </td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td colspan="2" class="border-x"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border"></td>
              <td class="border-x"></td>
            </tr>

            <tr class="h-20">
              <td></td>
              <td colspan="2" class="border-bottom"></td>
              <td></td>
              <td colspan="2" class="border-bottom"></td>
            </tr>

            <tr class="text-center">
              <td></td>
              <td colspan="2">Recorder's Signature over Printed Name</td>
              <td></td>
              <td colspan="2">Date</td>
            </tr>
          </tbody>
        </table>
      </main>
    </div>
  </body>
</html>