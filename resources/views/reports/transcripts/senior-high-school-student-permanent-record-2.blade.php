<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<?php
//sbu -- 2018100122
$stdno = ((isset($stdno))?$stdno:'2008087');
$ay    = DB::select("SELECT r.TermID,t.AcademicYear,t.SchoolTerm
						  ,(SELECT YearLevelCode FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelCode
						  ,(SELECT YearLevelName FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelName
						  ,r.RegID,r.RegDate,r.ValidationDate,r.YearLevelID,r.ProgID,r.MajorID
						  ,'' as School
						  ,p.ProgCode,p.ProgName
						  ,m.MajorDiscCode,m.MajorDiscDesc
						  ,pm.AcademicA_Average,pm.AcademicB_Average,pm.AcademicC_Average,pm.AcademicD_Average,pm.Final_Average,pm.Final_Remarks
						  ,pm.ConductA_Average,pm.ConductB_Average,pm.ConductC_Average,pm.ConductD_Average,pm.Final_Conduct
						  ,pm.TotalSubjects,pm.SchoolDays,pm.PresentDays
						  ,pa.DaysPresent
						  ,t.StartofAY,t.EndofAY
					  FROM ES_Registrations as r
				INNER JOIN ES_AYTerm as t on r.TermID=t.TermID
				INNER JOIN ES_PermanentRecord_Master as pm ON r.TermID=pm.TermID AND r.StudentNo=pm.StudentNo AND r.RegID=pm.RegID
				 LEFT JOIN ES_PermanentRecord_Attendance as pa ON r.TermID=pa.TermID AND r.StudentNo=pa.StudentNo
				INNER JOIN ES_Programs as p ON r.ProgID=p.ProgID
				 LEFT JOIN ES_DisciplineMajors as m ON r.MajorID=m.IndexID
					 WHERE r.StudentNo='".$stdno."'
				  ORDER BY t.AcademicYear,t.SchoolTerm");
				  
$rs    = DB::select("SELECT r.StudentNo,s.LRN
						   ,LastName,FirstName,Middlename,MiddleInitial,FullName
						   ,DateOfBirth,PlaceOfBirth
						   ,Father,Mother
						   ,s.Res_Address,s.Res_Street,s.Res_TownCity,s.Res_Province,s.Res_ZipCode
						   ,DateGraduated
						   ,(SELECT Nationality FROM Nationalities WHERE NationalityID=s.NationalityID) as Nationality
						   ,r.RegID,r.RegDate
						   ,r.TermID
						   ,(SELECT YearLevelCode FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelCode
						   ,(SELECT YearLevelName FROM ESv2_YearLevel WHERE ProgID=r.ProgID AND YLID_OldValue=r.YearLevelID) as YearLevelName
						   ,(SELECT AcademicYear FROM es_ayterm WHERE TermID=r.TermID) as AcademicYear
						   ,(SELECT SchoolTerm FROM es_ayterm WHERE TermID=r.TermID) as SchoolTerm
						   ,rd.ScheduleID,rd.ScheduleID_Old,rd.RegTagID
                           ,pr.SubjectID,pr.SubjectCode,pr.SubjectTitle
						   ,(SELECT SUM(LectHrs+LabHrs) FROM ES_Subjects WHERE SubjectID=pr.SubjectID) as SubjectHrs
                           ,pr.AverageA						   
                           ,pr.AverageB						   
                           ,pr.AverageC						   
                           ,pr.AverageD
                           ,pr.TotalAverageCD
                           ,pr.Final_Average
                           ,pr.Final_Remarks						   
					   FROM ES_Registrations as r
				 INNER JOIN ES_Students as s ON r.StudentNo=s.StudentNo
				  LEFT JOIN ES_RegistrationDetails as rd ON r.RegID=rd.RegID
                  LEFT JOIN ES_PermanentRecord_Details as pr ON r.StudentNo=pr.StudentNo AND r.RegID=pr.RegID AND rd.ScheduleID=pr.ScheduleID
					  WHERE r.StudentNo='".$stdno."'
				   ORDER BY r.RegID,pr.GradeIDX");
$stdinfo   = $rs[0];					
$termid    = $ay[0]->TermID;
$recordid  = 0; 
$profile   = url('assets/system/media/profile/'.$stdno.'/pic.jpg');
$skip_term = array(); 
?>
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>SENIOR HIGH SCHOOL - STUDENTS PERMANENT RECORD</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo url('assets/test/css/student-permanent-record-2.min.css');?>"/>
  </head>
  <body>
    <div class="container">
      <h4 class="text-center">SENIOR HIGH SCHOOL STUDENT'S PERMANENT RECORD</h4>

      <main class="main">
        <div class="columns mt-10 pl-5">
          <div class="column-6">
            <div class="input-group">
              <label class="w-120">
                <strong>
                  Name:
                </strong>
              </label>
              <input type="text" class="pl-30" value="<?php echo $stdinfo->FullName;?>"/>
            </div>

            <div class="input-group">
              <label class="w-120">
                <strong>
                  Date of Birth:
                </strong>
              </label>
              <input type="text" class="pl-30" value="<?php echo (($stdinfo->DateOfBirth!='')?(date('m/d/Y',strtotime($stdinfo->DateOfBirth))):'');?>"/>
            </div>

            <div class="input-group">
              <label class="w-120">
                <strong>
                  Place of Birth:
                </strong>
              </label>
              <input type="text" class="pl-30" value="<?php echo $stdinfo->PlaceOfBirth;?>"/>
            </div>

            <div class="input-group">
              <label class="w-120">
                <strong>
                  Father:
                </strong>
              </label>
              <input type="text" class="pl-30" value="<?php echo $stdinfo->Father;?>"/>
            </div>

            <div class="input-group">
              <label class="w-120">
                <strong>
                  Mother:
                </strong>
              </label>
              <input type="text" class="pl-30" value="<?php echo $stdinfo->Mother;?>"/>
            </div>
          </div>
          <div class="column-6">
            <div class="input-group">
              <label>
                <strong>LRN:</strong>
              </label>
              <input type="text" class="w-180 pl-10" value="<?php echo $stdinfo->LRN;?>"/>
            </div>

            <div class="input-group">
              <label>
                <strong>Nationality:</strong>
              </label>
              <input type="text" class="w-180 pl-10" value="<?php echo $stdinfo->Nationality;?>"/>
            </div>

            <div class="input-group">
              <label>
                <strong>
                  Region IV-A.SO.(A) No.
                </strong>
              </label>
              <input type="text" class="w-180 pl-10" />
            </div>

            <div class="input-group">
              <label>
                <strong>Date of Graduation:</strong>
              </label>
              <input type="text" class="w-180 pl-10" />
            </div>

            <div class="input-group">
              <label>
                <strong>Address:</strong>
              </label>
              <input type="text" class="w-180 pl-10"  value="<?php echo $stdinfo->Res_Address.' '.$stdinfo->Res_Street.' '.$stdinfo->Res_TownCity.' '.$stdinfo->Res_Province;?>"/>
            </div>
          </div>
        </div>

        <div class="d-flex pl-5">
          <div class="input-group flex-expand mr-10">
            <label class="w-250">
              <strong>
                Primary Course Completed:
              </strong>
            </label>
            <input type="text" class="text-center" value="<?php echo $ay[(count($ay)-1)]->ProgName.' '.$ay[(count($ay)-1)]->MajorDiscDesc;?>"/>
          </div>
          <div class="text-right">
            <small><?php echo $ay[(count($ay)-1)]->AcademicYear;?></small>
          </div>
        </div>

        <div class="d-flex pl-5">
          <div class="input-group flex-expand mr-10">
            <label class="w-250">
              <strong>
                Intermediate Course Completed:
              </strong>
            </label>
            <input type="text" class="text-center" />
          </div>
          <div class="text-right">
            <small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
          </div>
        </div>
        <?php
		  $gradesa = ((count($ay)>0)?($ay[0]):false);
		  $gradesb = ((count($ay)>1)?($ay[1]):false);
		  $gradesc = ((count($ay)>2)?($ay[2]):false);
		  $gradesd = ((count($ay)>3)?($ay[3]):false);
		?>
        <div class="columns no-gutters">
          <div class="column-6">
            <table class="table border-right">
              <thead>
                <tr class="border-top-x-double">
                  <th colspan="2" class="pl-5">
                    <?php echo (($gradesa)?($gradesa->YearLevelName):'');?> <span class="fr">Date Enrolled:</span>
                  </th>
                  <th colspan="2" class="font-light pl-10"><?php echo (($gradesa)? date('M Y',strtotime($gradesa->RegDate)):'');?></th>
                </tr>

                <tr class="border-bottom-x-double">
                  <th colspan="2" class="pl-5">
                    School:
                    <span class="font-light pl-10"><?php echo (($gradesa)? ($gradesa->School):'');?></span>
                    <span class="fr">School Year:</span>
                  </th>
                  <th colspan="2" class="text-center font-light"><?php echo (($gradesa)? ($gradesa->AcademicYear):'');?></th>
                </tr>

                <tr class="text-center">
                  <th class="border-x v-bottom font-lg text-spacing-md">
                    SUBJECTS
                  </th>
                  <th class="w-50 border font-light">
                    Final
                    <br />
                    Rating
                  </th>
                  <th class="w-50 border font-light">
                    Action
                    <br />
                    Taken
                  </th>
                  <th class="w-50 border-x font-light">
                    Hours
                  </th>
                </tr>
              </thead>

              <tbody>
			    <?php
				 $rows  =0;
				 $ga    ='';
				 $garmk ='';
				 $schday=0;
				 $prsday=0;
				 $cd    ='';
				 $cdrmk ='';
				 $totale=0;
				 if($gradesa){
				    $termid=$gradesa->TermID;
					$schday=$gradesa->SchoolDays;
					$prsday=$gradesa->DaysPresent;
					$ga    =$gradesa->Final_Average;
                    $garmk =$gradesa->Final_Remarks;					
					$cd    =$gradesa->Final_Conduct; 
					foreach($rs as $r){
					  if($r->TermID==$termid){
					    $totale=$totale+(($r->SubjectHrs!='')?floatval($r->SubjectHrs):0);
					    echo '<tr class="border-bottom-grey">
								  <td class="pl-5">'.$r->SubjectTitle.'</td>
								  <td class="text-center border-y">
									<strong>'.(($r->Final_Average!='')?number_format(floatval($r->Final_Average),2):'').'</strong>
								  </td>
								  <td class="text-center border-y">'.$r->Final_Remarks.'</td>
								  <td class="text-center">'.$r->SubjectHrs.'</td>
							  </tr>';
						$rows++;	  
					  }
					}
				 }
				 
				 if($rows<15){
				    for($rows;$rows<15;$rows++){
					   echo '<tr class="border-bottom-grey">
							  <td class="pl-5">&nbsp;</td>
							  <td class="text-center border-y">
								<strong>&nbsp;</strong>
							  </td>
							  <td class="text-center border-y">&nbsp;</td>
							  <td class="text-center">&nbsp;</td>
						    </tr>';
					}
				 }
				?>

                <tr class="border-bottom-grey">
                  <td class="pl-5">General Average</td>
                  <td class="text-center border-y">
                    <strong><?php echo $ga;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $garmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days of School</td>
                  <td class="text-center border-y">
                    <strong><?php echo $schday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days Present</td>
                  <td class="text-center border-y">
                    <strong><?php echo $prsday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Conduct</td>
                  <td class="text-center border-y">
                    <strong><?php echo $cd;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $cdrmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr>
                  <td class="pl-5 font-lg">
                    <strong>TOTAL UNITS EARNED</strong>
                  </td>
                  <td class="text-center border-y">
                    <strong></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"><?php echo $totale;?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="column-6"><!--Second -->
            <table class="table border-right">
              <thead>
                <tr class="border-top-x-double">
                  <th colspan="2" class="pl-5">
                    <?php echo (($gradesb)?($gradesb->YearLevelName):'');?> <span class="fr">Date Enrolled:</span>
                  </th>
                  <th colspan="2" class="font-light pl-10"><?php echo (($gradesb)? date('M Y',strtotime($gradesb->RegDate)):'');?></th>
                </tr>

                <tr class="border-bottom-x-double">
                  <th colspan="2" class="pl-5">
                    School:
                    <span class="font-light pl-10"><?php echo (($gradesb)? ($gradesb->School):'');?></span>
                    <span class="fr">School Year:</span>
                  </th>
                  <th colspan="2" class="text-center font-light"><?php echo (($gradesb)? ($gradesb->AcademicYear):'');?></th>
                </tr>

                <tr class="text-center">
                  <th class="border-x v-bottom font-lg text-spacing-md">
                    SUBJECTS
                  </th>
                  <th class="w-50 border font-light">
                    Final
                    <br />
                    Rating
                  </th>
                  <th class="w-50 border font-light">
                    Action
                    <br />
                    Taken
                  </th>
                  <th class="w-50 border-x font-light">
                    Hours
                  </th>
                </tr>
              </thead>

              <tbody>
			    <?php
				 $rows  =0;
				 $ga    ='';
				 $garmk ='';
				 $schday=0;
				 $prsday=0;
				 $cd    ='';
				 $cdrmk ='';
				 $totale=0;
				 if($gradesb){
				    $termid=$gradesb->TermID;
					$schday=$gradesb->SchoolDays;
					$prsday=$gradesb->DaysPresent;
					$ga    =$gradesb->Final_Average;
                    $garmk =$gradesb->Final_Remarks;					
					$cd    =$gradesb->Final_Conduct; 
					foreach($rs as $r){
					  if($r->TermID==$termid){
					    $totale=$totale+(($r->SubjectHrs!='')?floatval($r->SubjectHrs):0);
					    echo '<tr class="border-bottom-grey">
								  <td class="pl-5">'.$r->SubjectTitle.'</td>
								  <td class="text-center border-y">
									<strong>'.(($r->Final_Average!='')?number_format(floatval($r->Final_Average),2):'').'</strong>
								  </td>
								  <td class="text-center border-y">'.$r->Final_Remarks.'</td>
								  <td class="text-center">'.$r->SubjectHrs.'</td>
							  </tr>';
						$rows++;	  
					  }
					}
				 }
				 
				 if($rows<15){
				    for($rows;$rows<=15;$rows++){
					   echo '<tr class="border-bottom-grey">
							  <td class="pl-5">&nbsp;</td>
							  <td class="text-center border-y">
								<strong>&nbsp;</strong>
							  </td>
							  <td class="text-center border-y">&nbsp;</td>
							  <td class="text-center">&nbsp;</td>
						    </tr>';
					}
				 }
				?>

                <tr class="border-bottom-grey">
                  <td class="pl-5">General Average</td>
                  <td class="text-center border-y">
                    <strong><?php echo $ga;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $garmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days of School</td>
                  <td class="text-center border-y">
                    <strong><?php echo $schday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days Present</td>
                  <td class="text-center border-y">
                    <strong><?php echo $prsday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Conduct</td>
                  <td class="text-center border-y">
                    <strong><?php echo $cd;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $cdrmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr>
                  <td class="pl-5 font-lg">
                    <strong>TOTAL UNITS EARNED</strong>
                  </td>
                  <td class="text-center border-y">
                    <strong></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"><?php echo $totale;?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="columns no-gutters">
          <div class="column-6"><!--Third -->
            <table class="table border-bottom border-right">
              <thead>
                <tr class="border-top-x-double">
                  <th colspan="2" class="pl-5">
                    <?php echo (($gradesc)?($gradesc->YearLevelName):'');?> <span class="fr">Date Enrolled:</span>
                  </th>
                  <th colspan="2" class="font-light pl-10"><?php echo (($gradesc)? date('M Y',strtotime($gradesc->RegDate)):'');?></th>
                </tr>

                <tr class="border-bottom-x-double">
                  <th colspan="2" class="pl-5">
                    School:
                    <span class="font-light pl-10"><?php echo (($gradesc)? ($gradesc->School):'');?></span>
                    <span class="fr">School Year:</span>
                  </th>
                  <th colspan="2" class="text-center font-light"><?php echo (($gradesc)? ($gradesc->AcademicYear):'');?></th>
                </tr>

                <tr class="text-center">
                  <th class="border-x v-bottom font-lg text-spacing-md">
                    SUBJECTS
                  </th>
                  <th class="w-50 border font-light">
                    Final
                    <br />
                    Rating
                  </th>
                  <th class="w-50 border font-light">
                    Action
                    <br />
                    Taken
                  </th>
                  <th class="w-50 border-x font-light">
                    Hours
                  </th>
                </tr>
              </thead>

              <tbody>
			    <?php
				 $rows  =0;
				 $ga    ='';
				 $garmk ='';
				 $schday=0;
				 $prsday=0;
				 $cd    ='';
				 $cdrmk ='';
				 $totale=0;
				 if($gradesc){
				    $termid=$gradesc->TermID;
					$schday=$gradesc->SchoolDays;
					$prsday=$gradesc->DaysPresent;
					$ga    =$gradesc->Final_Average;
                    $garmk =$gradesc->Final_Remarks;					
					$cd    =$gradesc->Final_Conduct; 
					foreach($rs as $r){
					  if($r->TermID==$termid){
					    $totale=$totale+(($r->SubjectHrs!='')?floatval($r->SubjectHrs):0);
					    echo '<tr class="border-bottom-grey">
								  <td class="pl-5">'.$r->SubjectTitle.'</td>
								  <td class="text-center border-y">
									<strong>'.(($r->Final_Average!='')?number_format(floatval($r->Final_Average),2):'').'</strong>
								  </td>
								  <td class="text-center border-y">'.$r->Final_Remarks.'</td>
								  <td class="text-center">'.$r->SubjectHrs.'</td>
							  </tr>';
						$rows++;	  
					  }
					}
				 }
				 
				 if($rows<15){
				    for($rows;$rows<=15;$rows++){
					   echo '<tr class="border-bottom-grey">
							  <td class="pl-5">&nbsp;</td>
							  <td class="text-center border-y">
								<strong>&nbsp;</strong>
							  </td>
							  <td class="text-center border-y">&nbsp;</td>
							  <td class="text-center">&nbsp;</td>
						    </tr>';
					}
				 }
				?>

                <tr class="border-bottom-grey">
                  <td class="pl-5">General Average</td>
                  <td class="text-center border-y">
                    <strong><?php echo $ga;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $garmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days of School</td>
                  <td class="text-center border-y">
                    <strong><?php echo $schday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days Present</td>
                  <td class="text-center border-y">
                    <strong><?php echo $prsday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Conduct</td>
                  <td class="text-center border-y">
                    <strong><?php echo $cd;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $cdrmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr>
                  <td class="pl-5 font-lg">
                    <strong>TOTAL UNITS EARNED</strong>
                  </td>
                  <td class="text-center border-y">
                    <strong></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"><?php echo $totale;?></td>
                </tr>
              </tbody>
            </table>
          </div>
		  
          <div class="column-6"><!--Fourth-->
            <table class="table border-bottom border-right">
              <thead>
                <tr class="border-top-x-double">
                  <th colspan="2" class="pl-5">
                    <?php echo (($gradesd)?($gradesd->YearLevelName):'');?> <span class="fr">Date Enrolled:</span>
                  </th>
                  <th colspan="2" class="font-light pl-10"><?php echo (($gradesd)? date('M Y',strtotime($gradesd->RegDate)):'');?></th>
                </tr>

                <tr class="border-bottom-x-double">
                  <th colspan="2" class="pl-5">
                    School:
                    <span class="font-light pl-10"><?php echo (($gradesd)? ($gradesd->School):'');?></span>
                    <span class="fr">School Year:</span>
                  </th>
                  <th colspan="2" class="text-center font-light"><?php echo (($gradesd)? ($gradesd->AcademicYear):'');?></th>
                </tr>

                <tr class="text-center">
                  <th class="border-x v-bottom font-lg text-spacing-md">
                    SUBJECTS
                  </th>
                  <th class="w-50 border font-light">
                    Final
                    <br />
                    Rating
                  </th>
                  <th class="w-50 border font-light">
                    Action
                    <br />
                    Taken
                  </th>
                  <th class="w-50 border-x font-light">
                    Hours
                  </th>
                </tr>
              </thead>

              <tbody>
			    <?php
				 $rows  =0;
				 $ga    ='';
				 $garmk ='';
				 $schday=0;
				 $prsday=0;
				 $cd    ='';
				 $cdrmk ='';
				 $totale=0;
				 if($gradesd){
				    $termid=$gradesd->TermID;
					$schday=$gradesd->SchoolDays;
					$prsday=$gradesd->DaysPresent;
					$ga    =$gradesd->Final_Average;
                    $garmk =$gradesd->Final_Remarks;					
					$cd    =$gradesd->Final_Conduct; 
					foreach($rs as $r){
					  if($r->TermID==$termid){
					    $totale=$totale+(($r->SubjectHrs!='')?floatval($r->SubjectHrs):0);
					    echo '<tr class="border-bottom-grey">
								  <td class="pl-5">'.$r->SubjectTitle.'</td>
								  <td class="text-center border-y">
									<strong>'.(($r->Final_Average!='')?number_format(floatval($r->Final_Average),2):'').'</strong>
								  </td>
								  <td class="text-center border-y">'.$r->Final_Remarks.'</td>
								  <td class="text-center">'.$r->SubjectHrs.'</td>
							  </tr>';
						$rows++;	  
					  }
					}
				 }
				 
				 if($rows<15){
				    for($rows;$rows<=15;$rows++){
					   echo '<tr class="border-bottom-grey">
							  <td class="pl-5">&nbsp;</td>
							  <td class="text-center border-y">
								<strong>&nbsp;</strong>
							  </td>
							  <td class="text-center border-y">&nbsp;</td>
							  <td class="text-center">&nbsp;</td>
						    </tr>';
					}
				 }
				?>

                <tr class="border-bottom-grey">
                  <td class="pl-5">General Average</td>
                  <td class="text-center border-y">
                    <strong><?php echo $ga;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $garmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days of School</td>
                  <td class="text-center border-y">
                    <strong><?php echo $schday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Days Present</td>
                  <td class="text-center border-y">
                    <strong><?php echo $prsday;?></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"></td>
                </tr>

                <tr class="border-bottom-grey">
                  <td class="pl-5">Conduct</td>
                  <td class="text-center border-y">
                    <strong><?php echo $cd;?></strong>
                  </td>
                  <td class="text-center border-y"><?php echo $cdrmk;?></td>
                  <td class="text-center"></td>
                </tr>

                <tr>
                  <td class="pl-5 font-lg">
                    <strong>TOTAL UNITS EARNED</strong>
                  </td>
                  <td class="text-center border-y">
                    <strong></strong>
                  </td>
                  <td class="text-center border-y"></td>
                  <td class="text-center"><?php echo $totale;?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </main>

      <footer class="footer">
        <h2 class="text-center">CERTIFICATION</h2>
        <p>
          I certify that this is a true record of
          <span><?php echo (($stdinfo)?($stdinfo->FullName):'');?></span>

          This student is eligible on this <span><?php echo date('d');?></span> day of
          <span><?php echo date('M Y');?></span> for admission to <span>COLLEGE</span>

          as a/an regular/irregular student. He/She has no money or property
          responsibility in this school.
        </p>

        <div class="columns no-gutters">
          <div class="column-4">
            <h3>This record is valid only for:</h3>
            <small>(Not Valid Without the College Seal)</small>
          </div>
          <div class="column-6">
            <h3></h3>
          </div>
        </div>

        <div class="columns mt-10">
          <div class="column-6">
            <div class="columns mt-10">
              <div class="column-8">
                <div class="input-group">
                  <label class="mr-10">
                    <strong>Prepared by:</strong>
                  </label>
                  <input type="text" class="text-center" />
                </div>
              </div>
              <div class="column-4">
                <div class="input-group">
                  <label class="mr-10">Date: </label>
                  <input type="text" class="text-center" />
                </div>
              </div>
            </div>

            <div class="columns mt-10">
              <div class="column-8">
                <div class="input-group">
                  <label class="mr-10">
                    <strong>Checked by:</strong>
                  </label>
                  <input type="text" class="text-center" />
                </div>
              </div>
              <div class="column-4">
                <div class="input-group">
                  <label class="mr-10">Date: </label>
                  <input type="text" class="text-center" />
                </div>
              </div>
            </div>
          </div>
          <div class="column-6 text-center">
            MARIA OLIVA S. REYES
            <br />
            Registrar
          </div>
        </div>

        <p class="text-center mt-10">That in all things God may be glorified</p>
      </footer>
    </div>
  </body>
</html>