<!DOCTYPE html>
<?php
	 $req    = Request::all();
	 $term   = getObjectValue($req,'term');
	 $term   = ((trim($term)=='')?0:decode($term));
	 $campus = getObjectValue($req,'campus');
	 $campus = ((trim($campus)=='')?0:decode($campus));
	 $progid = getObjectValue($req,'program');
	 $asof   = getObjectValue($req,'asof');
	 $asof   = ((trim($asof)=='')?date('Y-m-d 23:59:59'):date('Y-m-d 23:59:59',strtotime($asof).""));
?>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Reserved Students</title>
    <style>
    .text-center {
      text-align: center
    }

    .text-right {
      text-align: right
    }

    .vertical-middle {
      vertical-align: middle
    }

    .vertical-top {
      vertical-align: top
    }

    .vertical-bottom {
      vertical-align: bottom
    }

    .border {
      border: 1px solid #2a2a2a
    }

    *,
    *::before,
    *::after {
      margin: 0;
      padding: 0;
      -webkit-box-sizing: inherit;
      box-sizing: inherit
    }

    html {
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
      font-family: Arial, Helvetica, sans-serif
    }

    body {
      text-align: left;
      background: #ccc;
      color: #2a2a2a;
      line-height: 1.5
    }

    .input-group {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      font-size: 10px
    }

    .input-group:not(:last-child) {
      margin-bottom: 3px
    }

    .input-group input {
      border: 0;
      border-bottom: 1px solid #2a2a2a;
      font-size: inherit;
      font-weight: 600
    }

    @font-face {
      font-family: "Goudy Old Style";
      src: url("../css/fonts/Goudy Old Style.ttf")
    }

    @font-face {
      font-family: "tonic";
      src: url("../css/fonts/tonic.ttf")
    }

    .container {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column;
      margin: 20px auto;
      background: #fff;
      -webkit-box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23)
    }

    .table {
      width: 100%
    }

    :root {
      line-sizing: normal
    }

    :root {
      -ms-text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric;
      text-spacing: trim-start allow-end trim-adjacent ideograph-alpha ideograph-numeric
    }

    img,
    video,
    canvas,
    audio,
    iframe,
    embed,
    object {
      display: block
    }

    img,
    video {
      max-width: 100%;
      height: auto
    }

    img {
      vertical-align: middle;
      border-style: none
    }

    article,
    aside,
    figcaption,
    figure,
    footer,
    header,
    hgroup,
    main,
    nav,
    section {
      display: block
    }

    b,
    strong {
      font-weight: bolder
    }

    table {
      border-collapse: collapse
    }

    .header {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end
    }

    .header__logo {
    margin-top: 10px;
    width: 85px;
    height: auto;
    }

    .header__text {
      width: 400px
    }

    .header__text-top {
      line-height: 1;
      font-family: "Goudy Old Style";
      padding-left: 8px
    }

    .header__text-top h1 {
      font-weight: 500;
      font-size: 20px
    }

    .header__text-top p {
      font-weight: 600;
      font-size: 10px
    }

    .header__text-center {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      padding: 0 8px 3px
    }

    .header__text-center ul {
      list-style-type: none;
      font-family: "tonic";
      font-weight: 600
    }

    .header__text-bottom {
      border-top: 2px solid #2a2a2a;
      text-align: right
    }

    .header__text-bottom p {
      font-size: 10px;
      font-family: "tonic";
      font-weight: 600
    }

    .header__right {
      margin-left: auto
    }

    .header__right h6 {
      font-style: italic;
      font-weight: 500;
      font-size: 8px
    }

    .header__list-left {
      font-size: 8px
    }

    .header__list-left li {
      line-height: 1
    }

    .header__list-right {
      font-size: 10px;
      -ms-flex-item-align: end;
      align-self: flex-end;
      text-align: right
    }

    .header__list-right li {
      line-height: 1.2
    }

    .container {
      padding: 20px;
      max-width: 11.7in;
      min-height: 8.3in
    }

    .main h2 {
      text-align: center;
      font-family: Calibri, "Trebuchet MS", sans-serif;
      font-size: 18px
    }

    .main__header {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      margin: 10px 0
    }

    .main__header-left {
      text-align: left;
      width: 260px
    }

    .main__header-left .input-group label {
      width: 55px
    }

    .main__header-left .input-group input {
      -webkit-box-flex: 1;
      -ms-flex: 1;
      flex: 1
    }

    .main__header-right {
      text-align: right
    }

    .main__header-right .input-group label {
      padding-right: 4px;
      width: 175px
    }

    .main__header-right .input-group input {
      width: 200px
    }

    .rotated-header {
      text-align: center;
      vertical-align: bottom;
      white-space: nowrap;
      position: relative
    }

    .rotated-header>div {
      position: absolute;
      top: 95%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%) rotate(-90deg);
      transform: translate(-50%, -50%) rotate(-90deg);
      max-width: 1px
    }

    .pr-sm {
      padding-right: 5px
    }

    .pl-sm {
      padding-left: 5px
    }

    th {
      font-weight: 400
    }

    th,
    td {
      font-size: 8px
    }

    .footer {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end;
      margin-top: auto
    }

    .footer>div:not(:last-child) {
      width: 200px;
      text-align: center
    }

    .footer>div:last-child {
      margin-left: auto
    }

    .footer__adviser--1 {
      line-height: 1
    }

    .footer__adviser--1 h6 {
      font-size: 10px
    }

    .footer__adviser--1 small {
      font-size: 9px;
      font-style: italic
    }

    .footer__adviser--2 {
      line-height: 1
    }

    .footer__adviser--2 h6 {
      font-size: 10px
    }

    .footer__adviser--2 small {
      font-size: 9px;
      font-style: italic
    }

    .footer__adviser--3 {
      line-height: 1
    }

    .footer__adviser--3 h6 {
      font-size: 10px
    }

    .footer__adviser--3 small {
      font-size: 9px;
      font-style: italic
    }

    .footer__signature {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center
    }

    .footer__signature .input-group label {
      width: 55px;
      font-size: 9px;
      font-style: italic
    }

    @page {
      size: a4 landscape; 
      margin: 18mm;  
    }

    @media print {
	  thead{
	    display: table-row-group;
        page-break-inside:avoid;
	  }
      tr{ page-break-inside:avoid; page-break-after:auto }
      td{ page-break-inside:avoid; page-break-after:auto }
	  
      .container {
        margin: auto;
        width: auto;
        height: auto;
        padding-bottom: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        page-break-after: always
      }

      /* .footer {
        position: absolute;
        bottom: 20px;
        left: 20px;
        right: 20px
      } */

      body {
        -webkit-print-color-adjust: exact !important
      }

      /* html,
      body {
        height: 100%;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden
      } */
    }

    .width-20 {
      width: 20px
    }

    .width-25 {
      width: 25px
    }

    .width-35 {
      width: 35px
    }

    .height-15 {
      height: 15px
    }

    .height-25 {
      height: 25px
    }

    .height-60 {
      height: 60px
    }

    .height-100 {
      height: 100px
    }

    .bold{
      font-weight: bold;
    }

    /*# sourceMappingURL=jhs-cc.min.css.map */
.bg-green{ background-color: #dff0d8;}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.button-blue {
  background-color: blue;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.jfixed{
  position: fixed;
  top: 0;
  left: 80%;
}

.font-green{
  color: green;
}
.font-blue{
  color:darkblue
}
.font-red{
  color: red !important;
}
    </style>
    <?php
    $bgGreen ='background-color: #dff0d8;';
    $width20 ='width:20px;';
    $rotatedHeader = 'text-align: center;
      vertical-align: bottom;
      white-space: nowrap;
      position: relative';
    $rotatedHeaderDiv = ' position: absolute;
      top: 95%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%) rotate(-90deg);
      transform: translate(-50%, -50%) rotate(-90deg);
      max-width: 1px';
    ?>
  </head>
  <body>
    <div class="container">
        <a id="dlink"  style="display:none;"></a>
		<span class="jfixed">
        <input  title="Online Enrollment <?php echo $asof;?>" class="button no-print" type="button" onclick="tableToExcel('table', 'name', 'Online Enrollment <?php echo $asof;?>.xls')" value="Export to Excel">
        <input  title="Online Enrollment <?php echo $asof;?>" class="button-blue no-print" type="button" onclick="window.print();" value="Print">
		</span>
       
      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ HEADER
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <header class="header">
        <!------------------------------ Header logo ------------------------------>
        <div class="header__logo">
          <img src='{{url("assets/img/logo.jpg")}}' alt="Logo" />
        </div>
        <!------------------------------ Header text group ------------------------------>
        <div class="header__text">
          <div class="header__text-top">
		    <p>Republic of the Philippines</p>
            <h1>EVEREST ACADEMY Inc.</h1>
          </div>
          <div class="header__text-center">
            <ul class="header__list-left">
              <li>3846 38th Drive, North Bonifacio Global City, Taguig City, 1634 Philippines</li>
              <li></li>
              <li></li>
              <li></li>
            </ul>
            <ul class="header__list-right">
              <li></li>
              <li></li>
            </ul>
          </div>
          <div>
		    <br/>
			<p style="font-size:10px;text-align:left;">&nbsp;&nbsp;Reserved Students</p>
          </div>
        </div>
      </header>

      <!-------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        ------ MAIN
        ---------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------->
      <main class="main">
	    <br/>
		<?php
		 $exec = DB::select("SELECT r.RegID,r.RegDate
								   ,r.StudentNo
								   ,s.LastName
								   ,s.FirstName
								   ,s.Middlename
								   ,s.Fullname 
								   ,y.YearLevelCode
								   ,y.YearLevelName
								   ,r.ValidationDate
								   ,r.ValidatingOfficerID
								   ,(SELECT TOP 1 Particulars FROM ES_Journals 
															 WHERE TermID=r.TermID AND TransID=20 AND NonLedger=0 AND IDNo=r.StudentNo
															   AND (Particulars LIKE '%Reserv%' AND AccountID IN (1005,1121))) as Particular
								  FROM ES_Students as s 
							 LEFT JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo
							INNER JOIN ESv2_YearLevel as y ON r.ProgID=y.ProgID AND r.YearLevelID=y.YLID_OldValue
								 WHERE r.TermID='1006'
								   AND s.StudentNo IN (SELECT IDNo FROM ES_Journals 
															 WHERE TermID=r.TermID AND TransID=20 AND NonLedger=0 
															   AND (Particulars LIKE '%Reserv%' AND AccountID IN (1005,1121)))
							  ORDER BY y.YearLevelID,s.LastName,s.FirstName");
		?>
		<table id="table" class="table table-condense table-bordered">
			<thead>
				<th class="border">####</th>
				<th class="border">RegID</th>
				<th class="border">RegDate</th>
				<th class="border">Student No</th>
				<th class="border">FullName</th>
				<th class="border">Grade Level</th>
				<th class="border">Particular</th>
				<th class="border">Status</th>
			</thead>
			<tbody>
				<?php
				  $i=1;
				  $j=1;
				  if($exec && count($exec)>0){
					foreach($exec as $rs){
					    echo '<tr class="">
								<td class="border">'.$i.'</td>
								<td class="border">'.$rs->RegID.'</td>
								<td class="border">'.date('m/d/Y h:i',strtotime($rs->RegDate)).'</td>
								<td class="border">'.$rs->StudentNo.'</td>
								<td class="border">'.$rs->Fullname.'</td>
								<td class="border">'.$rs->YearLevelCode.'</td>
								<td class="border">'.$rs->Particular.'</td>
								<td class="border">'.(($rs->ValidationDate=='')?'NOT YET ENROLLED':'ENROLLED').'</td>
							  </tr>';
					    $i++; 
					    $j++;
					}
				  }
				?>
			</tbody>
		</table>
      </main>
    </div>	  
    <?php
    ?>
  </body>
</html>

<style>
    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
    </style>
    {{-- <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      var tableToExcel = (function () {
          var uri = 'data:application/vnd.ms-excel;base64,'
              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
          return function (table, name) {
              if (!table.nodeType) table = document.getElementById(table)
              var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

var blob = new Blob([format(template, ctx)]);
              var blobURL = window.URL.createObjectURL(blob);

              if (ifIE()) {
                  csvData = table.innerHTML;
                  if (window.navigator.msSaveBlob) {
                      var blob = new Blob([format(template, ctx)], {
                          type: "text/html"
                      });
                      navigator.msSaveBlob(blob, '' + name + '.xls');
                  }
              }
              else
              window.location.href = uri + base64(format(template, ctx))
          }
      })()

     function ifIE() {
          var isIE11 = navigator.userAgent.indexOf(".NET CLR") > -1;
          var isIE11orLess = isIE11 || navigator.appVersion.indexOf("MSIE") != -1;
          return isIE11orLess;
      }
  </script> --}}
    <script>   
    function fnExcelReport(id_table) {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById(id_table); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
      
            var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name, filename) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
    
                document.getElementById("dlink").href = uri + base64(format(template, ctx));
                document.getElementById("dlink").download = filename;
                document.getElementById("dlink").click();
    
            }
        })()
    
        
    
    </script>
