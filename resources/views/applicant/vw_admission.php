<?php
 $session_data = $this->session->userdata('logged_in');
 $rsnat  = ((isset($rsnat))? $rsnat : array());
 $rscit  = ((isset($rscit))? $rscit : array());
 $rsprov = ((isset($rsprov))? $rsprov : array());
?>
<div id="ribbon">
	<!-- <span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Admission</li>
	</ol> -->
</div>
<div id="content">
	<div class="row">
	  <div class="col-lg-12">
		 <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-gavel"></i> <?php echo $title; ?> <span>> Application</span></h1>
		 </div>
		 <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
			<div class="well no-padding pull-right">
				<a id="btncancel" class="btn btn-sm btn-success" href="<?php echo site_url('admission/welcome'); ?>">
					Cancel Application
				</a>
			</div>
		 </div>
	  </div>
	</div><!-- /.row -->


	<!-- widget grid -->
	<section id="widget-grid" class="">
		<div class="row">
			<article class="col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false">
					<header>	<h2>Admission Wizard </h2>	</header>
					<div>
						<div class="jarviswidget-editbox"> </div>
						<div class="widget-body fuelux">
							<div class="wizard" id="admissionWizard">
								<ul class="steps">
                                    <li data-target="#step1" class="active">
									<!--<span class="badge badge-info">1</span>Step 1<span class="chevron"></span>-->
									</li>
									<li data-target="#step2" class="hidden">
										<span class="badge">2</span>Step 2<span class="chevron"></span>
									</li>
									<li data-target="#step3" class="hidden">
										<span class="badge">3</span>Step 3<span class="chevron"></span>
									</li>
									<li data-target="#step4" class="hidden">
										<span class="badge">4</span>Step 4<span class="chevron"></span>
									</li>
									<li data-target="#step5" class="hidden">
										<span class="badge">5</span>Step 5<span class="chevron"></span>
									</li>
									<li data-target="#step6" class="hidden">
										<span class="badge">6</span>Step 6<span class="chevron"></span>
									</li>
									<li data-target="#step7" class="hidden">
										<span class="badge">7</span>Step 7<span class="chevron"></span>
									</li>
								</ul>
								<div class="actions">
									<button type="button" class="btn btn-sm btn-primary btn-prev disabled">
										<i class="fa fa-arrow-left"></i>Prev
									</button>
									<button type="button" class="btn btn-sm btn-success btn-next" data-last="Finish">
										Next<i class="fa fa-arrow-right"></i>
									</button>
								</div>
							</div>

							<div class="step-content">
								<!-- <form class="smart-form" id="fuelux-wizard" method="post" novalidate="novalidate"> -->
								<?php echo form_open('admission/submit',array('id'=>'fuelux-wizard','class' => 'smart-form', 'novalidate'=>'novalidate')); ?>
						  		<div class="step-pane active" id="step1">
									<br>
									<h3><strong class="hidden">Step 1 - </strong> Applying for <b class="for_rizal hidden">INTEGRATED BASIC EDUCATION DEPARTMENT - San Beda University Rizal Campus</b></h3>
									<br>
									<fieldset>
                                        <input class="hidden" type="hidden" id="UserName" name="UserName" value="<?php echo ((isset($userinfo) && is_array($userinfo))? $userinfo['id'] : '' );?>"/>
										<div class="row">
											<section class="col col-6">
												<label>Select Department :</label>
												<label class="select">
													<select name="department" id="department" >
														<!-- <option value="0" selected="" disabled=""> - Select Campus - </option>

														<option value="college" selected>College</option>
														<option value="shs" >Senior High School</option> -->

													</select> <i></i> </label>
											</section>
											<section class="col col-3 hidden">
												<label>Select Campus :</label>
												<label class="select">
													<select name="Choice1_campusID" id="Choice1_campusID" >
														<option value="0"  disabled=""> - Select Campus - </option>
														<?php
															foreach ($rscampus as $rscampus){
															 echo "<option value='". $rscampus->CampusID ."'>". $rscampus->ShortName ."</option>";
															}
														?>
													</select> <i></i> </label>
											</section>
											<section class="col col-3 hidden">
												<label>Academic Year & Term :</label>
												<label class="select">
													<select name="TermID" id="TermID">
														<option value="0" selected="" disabled=""> - Select Academic Year & Term - </option>
														<?php
															foreach ($rsayterm as $rsayterm){
															 echo "<option value='". $rsayterm->TermID ."' selected>". $rsayterm->AcademicYearTerm."</option>";
															}
														?>
													</select> <i></i> </label>
											</section>


											<section class="col col-4">
												<label id="AppType_Label">Application Type :</label>
												<label class="select">
													<select name="ApplyTypeID" id="ApplyTypeID">

													</select> <i></i> </label>
											</section>

										<!-- </div> -->


										<!-- <div class="row"> -->
											<section class="col col-6 firstchoice">
												<label id="Choice1_Label">First Choice :</label>
												<label class="select">
													<select name="Choice1_Course" id="Choice1_Course">

													</select> <i></i> </label>
                                                                                                <input type="hidden" class="hidden" id="Choice1_CourseMajor" name="Choice1_CourseMajor" value="0">
											</section>

											<section class="col col-6 for_college">
												<label>Second Choice :</label>
												<label class="select">
													<select name="Choice2_Course" id="Choice2_Course">

													</select> <i></i> </label>
											</section>

											<section class="col col-6 hidden">
												<label>Third Choice :</label>
												<label class="select">
													<select name="Choice3_Course" id="Choice3_Course">

													</select> <i></i> </label>
											</section>
										</div>
										<div class="row">

										</div>
										<div class="row">

										</div>

									</fieldset>
								</div>
									<div class="step-pane" id="step2">
										<br>
										<h3><strong class="hidden">Step 2 -</strong> Basic Information</h3>
										<!-- wizard form starts here -->

										<fieldset>
											<!-- Basic information -->
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="LastName" id="LastName"  placeholder="Last name" value="<?php if(isset($userinfo['lastname'])) echo $userinfo['lastname']; ?>">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="FirstName" id="FirstName" placeholder="First name" value="<?php if(isset($userinfo['firstname'])) echo $userinfo['firstname']; ?>">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="MiddleName" id="MiddleName" placeholder="Middle name" value="<?php if(isset($userinfo['middlename'])) echo $userinfo['middlename']; ?>">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
													<select class="form-control" id="drp_ExtName">
													  <option value="" selected disabled>Extension name: (e.g Jr, Sr, III, IV, etc.)</option>
													  <option value="Jr">Jr</option>
													  <option value="Sr">Sr</option>
													  <option value="II">II</option>
													  <option value="III">III</option>
													  <option value="IV">IV</option>
													  <option value="V">V</option>
													  <option value="-1">Other</option>
													</select>  <!-- value="<?php if(isset($userinfo['extname'])) echo $userinfo['extname']; ?>"> -->
													<input type="hidden" name="ExtName" id="ExtName" placeholder="Extension name: (e.g Jr, Sr, III, IV, etc.)">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-2">
													<label class="select">
														<select name="Gender">
															<?php
															$gender ='';
															if(isset($userinfo['gender']))
															{$gender = trim($userinfo['gender']) ;}

															 echo  '<option value="0" ' . ($gender == ''? 'selected':'') . ' disabled>Gender</option>
																	  <option value="M" ' . ($gender == 'M'? 'selected':'') . ' >Male</option>
																     <option value="F" ' . ($gender == 'F'? 'selected':'') . ' >Female</option>' ;
															?>
														</select> <i></i> </label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="PlaceOfBirth" id="PlaceOfBirth" placeholder="Birth place" value="<?php if(isset($userinfo['birthplace'])) echo $userinfo['birthplace']; ?>">
													</label>
												</section>
												<section class="col col-3">
												    <label class="control-label">Date of Birth:</label>
													<label class="input">
														<input type="text" id="DateOfBirth" name="DateOfBirth" placeholder="Date of Birth (mm/dd/yyyy)" class="form-control"  data-format="MM/DD/YYYY" data-template="MMM/DD/YYYY" value=""/>
													</label>
												</section>
												<section class="col col-3 for_shs hidden">
													<label class="input">
														<input type="text" name="LRN" id="LRN" placeholder="Learner Reference Number (LRN)" >
													</label>
												</section>

												<section class="col col-2 for_shs hidden">
														<label>Indigenous Group?</label>
													<div class="inline-group">
														<label class="radio">
															<input type="radio" name="isIndigenous"  checked="" value="0">
															<i></i>No</label>
														<label class="radio">
															<input type="radio" name="isIndigenous"  value="1">
															<i></i>Yes</label>

													</div>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="hidden" name="Indigenous_Group" id="Indigenous_Group" placeholder="Indigenous Group" >
													</label>
												</section>

											</div>
											<div class="row">
												<!-- Civil Status -->
												<section class="col col-2">
													<label class="select">
														<select name="CivilStatusID">
															<option value="0" disabled=""> - Civil Status -</option>
															<?php
															    $res = '';
																if(isset($userinfo['cstatus']))
																{$res = trim($userinfo['cstatus']);}

																foreach ($rscs as $rscs){
                                                                                                                                 if($rscs->CivilDesc=='Deceased'){continue;}
																 echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
																}

															?>
														</select> <i></i> </label>
												</section>
												<!-- Religion -->
												<section class="col col-2">
													<label class="select">
														<select name="ReligionID" id="ReligionID">
															<option value="0" selected="" disabled="">Religion</option>
															<?php
																$selrel = '';
																if(isset($userinfo['relid']))
																{$selrel = trim($userinfo['relid']);}

																foreach ($rsreligion as $rsreligion){
																 echo "<option value='". $rsreligion->ReligionID ."'".  ($selrel == $rsreligion->ReligionID ? 'selected':'') .">". $rsreligion->ShortName."</option>";
																}
															?>
															<option value="-1">Other</option>
														</select> <i></i> 
														<input type="hidden" id="ReligionOther" placeholder="Religion"/>
													</label>
												</section>
												<!-- Nationality -->
												<section class="col col-2">
													<label class="select">
														<select name="NationalityID" id="NationalityID">
															<option value="0" selected="" disabled="">Nationality</option>
															<?php
																foreach ($rsnat as $rs){
																   echo "<option value='". $rs->NationalityID ."'>". $rs->Nationality."</option>";
																}
															?>
														</select> <i></i> </label>
												</section>

												<section class="col col-2 for_shs hidden">
													<label class="input">
														<input type="text" name="Mother_Tongue" id="Mother_Tongue" placeholder="Mother Tongue" >
													</label>
												</section>
												<section class="col col-2 for_shs hidden">
													<label class="input">
														<input type="text" name="Other_Language" id="Other_Language" placeholder="Other Language Spoken" >
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Res_Address" id="Res_Address" placeholder="Residence">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="Res_Street" id="Res_Street" placeholder="Street">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<select name="Res_TownCity" id="Res_TownCity" class="form-control" placeholder="Town">
														<option value='' selected disabled> - Select Town - </option>";
														<?php
                                                              $prevVal = '';
                                                              foreach ($rscit as $rs){
															     if($prevVal==$rs->CityName){
																  continue;
																 }else{
																  $prevVal=$rs->CityName;
																 }
														         echo "<option value='". utf8_encode($rs->CityName)."' data-prov='".$rs->ProvinceID."'>". utf8_encode($rs->CityName)."</option>";
														      }
														?>
                                                        </select>
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<select name="Res_Province" id="Res_Province" class="form-control" placeholder="Province">
														<option value='' selected disabled> - Select Province - </option>";
														<?php
                                                              foreach ($rsprov as $rs){
														         echo "<option value='". utf8_encode($rs->ProvinceName)."'  data-prov='".$rs->ProvinceID."'>". utf8_encode($rs->ProvinceName)."</option>";
														      }
														?>
                                                        </select>
													</label>
												</section>
												<section class="col col-1">
													<label class="input">
														<input type="text" class="numberonly" name="Res_ZipCode" id="Res_Zipcode" placeholder="Postal Code">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Perm_Address" id="Perm_Address" placeholder="Permanent">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="Perm_Street" id="Perm_Street" placeholder="Street">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<select name="Perm_TownCity" id="Perm_TownCity"  class="form-control" placeholder="Town">
														<option value='' selected disabled> - Select Town - </option>";
														<?php
														    $prevVal = '';
														    foreach ($rscit as $rs){
														       if($prevVal==$rs->CityName){
																  continue;
														       }else{
																  $prevVal=$rs->CityName;
														       }
														       echo "<option value='". $rs->CityName."' data-prov='".$rs->ProvinceID."'>". $rs->CityName."</option>";
														    }
														?>
                                                        </select>
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<select name="Perm_Province" id="Perm_Province" class="form-control" placeholder="Province">
														<option value='' selected disabled> - Select Province - </option>";
														<?php
                                                            foreach ($rsprov as $rs){
														      echo "<option value='". $rs->ProvinceName."'  data-prov='".$rs->ProvinceID."'>". $rs->ProvinceName."</option>";
														    }
														?>
                                                        </select>
													</label>
												</section>
												<section class="col col-1">
													<label class="input">
														<input type="text" class="numberonly" name="Perm_ZipCode" id="Perm_ZipCode" placeholder="Postal Code">
													</label>
												</section>

												<section class="col col-2">
												   <div class="inline-group">
													<label class="checkbox">
														<input value="1" type="checkbox"  id="same_as_res"><i></i>
														<small>Same as Residence Address?</small>
													</label>

												  </div>
												</section>
											</div>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" class="numberonly" name="TelNo" id="Telno" maxlength="14" placeholder="Telephone number (e.g 7356011)">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" class="numberonly" name="MobileNo" id="MobileNo" maxlength="14" placeholder="Mobile number (e.g. 09234567890)">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="Email" id="Email" placeholder="E-mail" value="<?php echo (($session_data && @array_key_exists('email',$session_data))?$session_data['email']:''); ?>">
													</label>
												</section>
											</div>
											<div class="row">
											<section class="col col-12">
												<label>	State any disability or ailment that should be taken into consideration in planning your study program and daily activities (e.g. hearing, reading speech difficulties, physical disabilities, allergies, psychological/emotional disturbances, etc.)   </label>
													<label class="input">
													<input type="text" name="Disability" id="Disability"  placeholder="e.g. hearing, reading speech difficulties, physical disabilities, allergies, psychological/emotional disturbances, etc.">
												</label>
											</section>
										</div>


											<!-- <div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="disability" id="disability" placeholder="Physical/Medical Disability">
													</label>
												</section>
                                            </div> -->
										</fieldset>
                                        <!-- <fieldset class="foreigndata hidden">
											<header style="margin-top:-10px;margin-left:-10px;">Foreign Student</header><br/>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="passport" id="passport" placeholder="Passport No.">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="dateissue" id="dateissue" placeholder="Date Isssued">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="placeissue" id="placeissue" placeholder="Place Isssued">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="visatype" id="visatype" placeholder="Type of Visa">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="visastats" id="visastats" placeholder="Visa Status">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="icardno" id="icardno" placeholder="I-Card No.">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="stayfrom" id="stayfrom" placeholder="Stay From">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="stayto" id="stayto" placeholder="Stay To">
													</label>
												</section>
												<section class="col col-3 pull-right">
													<label class="input">
														<input type="text" name="remarks" id="remarks" placeholder="Remarks">
													</label>
												</section>
											</div>
                                        </fieldset> -->
									</div>
									<div class="step-pane" id="step3">
										<br>
										<h3><strong class="hidden">Step 3 -</strong> Family Background </h3>
										<fieldset class="workdata">

											<div class="row">
												<header style="font-size:0.9em;"><strong>Father</strong></header>
												<br>

												<section class="col col-3">
													<label class="input">
														<input type="text" name="Father" id="Father"  placeholder="Father's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Father_Occupation" id="Father_Occupation"  placeholder="Father's Occupation" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" class="numberonly" name="Father_TelNo" id="Father_TelNo"  maxlength="14" placeholder="Father's Contact (e.g. 09234567890)" value="">
													</label>
												</section>

												<!-- Civil Status -->
												<section class="col col-2">
													<label class="select">
														<select name="Father_CivilStatusID">
															<option value="0" disabled=""> - Civil Status -</option>
															<?php

																foreach ($rscs1 as $rscs)
																{
																	if ($rscs->CivilDesc != 'Single'){
																	 echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
																 }
																}

															?>
														</select> <i></i> </label>
												</section>
												<section class="col col-1">
												   <div class="inline-group">
													<label class="checkbox">
														<input value="1" type="checkbox" name="Father_isOFW" id="Father_isOFW"> Is OFW
														<i></i>
													</label>

												  </div>
												</section>


                                            </div>
                                            <div class="row">
												<section class="col col-3">
													<label class="select">
														<select name="Father_Nationality" id="Father_Nationality"  placeholder="Father's Nationality">
															<option value="0" selected disabled=""> - Father's Nationality -</option>
															<?php
																foreach ($rsnat as $rsf){
																 echo "<option value='". $rsf->NationalityID ."'>". $rsf->Nationality."</option>";
																}
															?>
														</select>
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Father_Address" id="Father_Address"  placeholder="Father's Address" value="">
													</label>
													<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Father_Address"/><i></i><small>same as residence address of applicant</small></label>
													<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Father_Address"/><i></i><small>same as permanent address of applicant</small></label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Father_Email" id="Father_Email"  placeholder="Father's E-mail" value="">
													</label>
												</section>
											</div>
											<div class="row">
												<header style="font-size:0.9em;"><strong>Mother</strong></header>
												<br>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Mother" id="Mother"  placeholder="Mother's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Mother_Occupation" id="Mother_Occupation"  placeholder="Mother's Occupation" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" class="numberonly" name="Mother_TelNo" id="Mother_TelNo"  maxlength="14" placeholder="Mother's Contact (e.g. 09234567890)" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="select">
														<select name="Mother_CivilStatusID">
															<option value="0" disabled=""> - Civil Status -</option>
															<?php
																foreach ($rscs1 as $rscs){
																	if ($rscs->CivilDesc != 'Single'){
																	 echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
																    }
																}
															?>
														</select> <i></i> </label>
												</section>
												<section class="col col-1">
												   <div class="inline-group">
													<label class="checkbox">
														<input value="1" type="checkbox" name="Mother_isOFW" id="Mother_isOFW"> Is OFW
														<i></i>
													</label>

												  </div>
												</section>
											</div>
                                            <div class="row">
												<section class="col col-3">
													<label class="select">
														<select name="Mother_Nationality" id="Mother_Nationality"  placeholder="Mother's Nationality">
															<option value="0" disabled=""> - Mother's Nationality -</option>
															<?php
																foreach ($rsnat as $rsm){
																 echo "<option value='". $rsm->NationalityID ."'>". $rsm->Nationality."</option>";
																}
															?>
														</select>
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Mother_Address" id="Mother_Address"  placeholder="Mother's Address" value="">
													</label>
													<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Mother_Address"/><i></i><small>same as residence address of applicant</small></label>
													<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Mother_Address"/><i></i><small>same as permanent address of applicant</small></label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Mother_Email" id="Mother_Email"  placeholder="Mother's E-mail" value="">
													</label>
												</section>
											</div>
											<div class="row">
												<header style="font-size:0.9em;"><strong>Guardian </strong></header>
												<br>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Guardian" id="Guardian"  placeholder="Guardian's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Guardian_Address" id="Guardian_Address"  placeholder="Guardian's Address" value="">
													</label>
													<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Guardian_Address"/><i></i><small>same as residence address of applicant</small></label>
													<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Guardian_Address"/><i></i><small>same as permanent address of applicant</small></label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" class="numberonly" name="Guardian_TelNo" id="Guardian_TelNo" maxlength="14" placeholder="Guardian's Contact Number  (e.g. 09234567890)" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Guardian_Relationship" id="Guardian_Relationship"  placeholder="Relationship with Guardian" value="">
													</label>
												</section>
											</div>
											<!-- <div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="nosibling" id="nosibling"  placeholder="Number of Siblings" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="brothers" id="brothers"  placeholder="Brothers" value="">
													</label>
												</section>
                                            </div> -->
											<div class="row for_graduate">
												<header style="font-size:0.9em;"><strong>Spouse </strong>(For married applicants)</header>
												<br>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Spouse" id="Spouse"  placeholder="Spouse Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Spouse_Address" id="Spouse_Address"  placeholder="Spouse's Residence" value="">
													</label>
													<label class="checkbox" style="margin-bottom:1.5px;"><input type="checkbox" class="addr_opt" data-src="resident" data-target="Spouse_Address"/><i></i><small>same as residence address of applicant</small></label>
													<label class="checkbox"><input type="checkbox" class="addr_opt" data-src="permanent" data-target="Spouse_Address"/><i></i><small>same as permanent address of applicant</small></label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" class="numberonly" name="Spouse_TelNo" id="Spouse_TelNo" maxlength="14"  placeholder="Spouse's Contact Number  (e.g. 09234567890)" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="Spouse_Email" id="Spouse_Email"  placeholder="Spouse's E-mail" value="">
													</label>
												</section>
                                            </div>
										</fieldset>
                                        <!-- <fieldset class="relativedata">
											<header style="margin-top:-10px;margin-left:-10px;">Relative Background</header><br/>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnamea" id="rnamea" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationa" id="rrelationa" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdatea" id="rdatea" placeholder="Date">
													</label>
												</section>
										    </div>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnameb" id="rnameb" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationb" id="rrelationb" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdateb" id="rdateb" placeholder="Date">
													</label>
												</section>
										    </div>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnamec" id="rnamec" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationc" id="rrelationc" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdatec" id="rdatec" placeholder="Date">
													</label>
												</section>
										    </div>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnamed" id="rnamed" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationd" id="rrelationd" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdated" id="rdated" placeholder="Date">
													</label>
												</section>
										    </div>
										</fieldset> -->
										<br>

									</div>
									<div class="step-pane" id="step4">
									    <br/>
									    <h3><strong class="hidden">Step 4 -</strong> Educational Attainment </h3>
											<br/>
											<p>Please provide details of last school attended:</p>
										<fieldset>
											<div class="row for_shs hidden">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="JHS_School" id="JHS_School"  placeholder="School Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="JHS_Address" id="JHS_Address"  placeholder="School Address" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="JHS_Degree" id="JHS_Degree" maxlength="26"  placeholder="Academic Track" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="JHS_Graduated" id="JHS_Graduated"  placeholder="Inclusive Academic Year" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="JHS_Award" id="JHS_Award"  placeholder="Award/Honor" value="">
													</label>
												</section>

											</div>
											<div class="row for_college">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="SHS_School" id="SHS_School"  placeholder="School Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="SHS_Address" id="SHS_Address"  placeholder="School Address" value="">
													</label>
												</section>

												<section class="col col-2">
													<label class="input">
														<input type="text" name="SHS_Degree" id="SHS_Degree" maxlength="26"  placeholder="Academic Track" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="SHS_Graduated" id="SHS_Graduated"  placeholder="Inclusive Academic Year" value="">
													</label>
												</section>

												<section class="col col-2">
													<label class="input">
														<input type="text" name="SHS_Award" id="SHS_Award"  placeholder="Award/Honor" value="">
													</label>
												</section>

											</div>
											<div class="row for_graduate hidden">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="College_School" id="College_School"  placeholder="College Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="College_Address" id="College_Address"  placeholder="College Address" value="">
													</label>
												</section>

												<section class="col col-2">
													<label class="input">
														<input type="text" name="College_Degree" id="College_Degree"  placeholder="College Degree" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="College_InclDates" id="College_InclDates"  placeholder="Inclusive Academic Year" value="">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="College_Award" id="College_Award"  placeholder="Award/Honor" value="">
													</label>
												</section>
											</div>
											<!-- <div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
													</label>
												</section>
											</div> -->
										</fieldset>
										<div class="for_transferee hidden">
										<br/>
										<p>
											For applicants transferring from another Undergraduate/Medical/Law/Graduate School, provide details:
										</p>



										<fieldset>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="LastSchool_Name" id="LastSchool_Name"  placeholder="School Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="LastSchool_Address" id="LastSchool_Address"  placeholder="School Address" value="">
													</label>
												</section>

												<section class="col col-3">
													<label class="input">
														<input type="text" name="LastSchool_Degree" id="LastSchool_Degree" maxlength="26" placeholder="Program Enrolled /No. of Units taken" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="LastSchool_SY" id="LastSchool_SY"  placeholder="Inclusive Academic Year" value="">
													</label>
												</section>


											</div>
											<!-- <div class="row">
												<section class="col col-6">
													<label class="checkbox">
													    Have you been placed under probation?&nbsp
														<input type="checkbox" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="checkbox">
													    Have you ever been dismissed?&nbsp
														<input type="checkbox" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
														<i></i>
													</label>
												</section>
											</div> -->
											<!-- <div class="row">
												<section class="col col-6">
													<label class="checkbox">
													    Membership in Professional, Civil Societies, Associations, Labor Unios, etc:
														<input type="checkbox" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
														<i></i>
													</label>
												</section>
											</div> -->
										</fieldset>
									</div>
              
									</div>
									<div class="step-pane" id="step5">
										<br>
									<h3><strong class="hidden">Step 5 -</strong> Other Information</h3>
										<br>

									<fieldset>
											<div class="row">
												<section class="col col-6 college-only">
													<label>	Licensure Exam/s Passed? </label>
														<label class="input">

														<input type="text" name="lexam_passed" id="lexam_passed"  placeholder="Licensure Exam/s Passed">
													</label>
												</section>
											<section class="col col-6">
												<label>	What other schools have you applied to? </label>
													<label class="input">

													<input type="text" name="Other_School" id="Other_School"  placeholder="School Name">
												</label>
											</section>

											<section class="col col-12">
													How did you come to know San Beda University?:<br/>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbya" id="knowbya"/> from parents/siblings
														<i></i>
													</label>
												</div>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbyb" id="knowbyb"/> from my friends/classmates
														<i></i>
													</label>
												</div>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbyc" id="knowbyc"/> from my own initiative
														<i></i>
													</label>
												</div>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbyd" id="knowbyd"/> from SBU website
														<i></i>
													</label>
												</div>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbye" id="knowbye" /> from the internet/webpage
														<i></i>
													</label>
												</div>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbyf" id="knowbyf"/> from SBU brochures/poster
														<i></i>
													</label>
												</div>
												<div class="col-sm-3">
													<label class="checkbox">
														<input type="checkbox" value="1" name="knowbyg" id="knowbyg"/> from career orientation talks
														<i></i>
													</label>
												</div>
												<div class="col-sm-6">

													<div class="form-group">

														<label class="checkbox col-md-4 control-label">
															<input type="checkbox" id="knowbyh"/> others(pls. specify)

															<i></i>
														</label>
														<div class="col-md-8">
															<input class="form-control" placeholder="Others" type="hidden" name="know_others">
														</div>
													</div>

												</div>
											</section>
										</div>
									</fieldset>
									                          <fieldset class="workdata">
											<header style="margin-top:-10px;margin-left:-10px;">Work Experience(Present Employment)</header><br/>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wnamea" id="wnamea" placeholder="Company Name">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="waddra" id="waddra" placeholder="Company Address">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wpositiona" id="wpositiona" placeholder="Position">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wcontacta" id="wcontacta" maxlength='14' placeholder="Tel./Cel./Fax No.">
													</label>
												</section>
										    </div>


											<!-- <div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wnameb" id="wnameb" placeholder="Company Name">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="waddrb" id="waddrb" placeholder="Company Address">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wpositionb" id="wpositionb" placeholder="Position">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wdateempb" id="wdateempb" placeholder="Date Employed">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wcontactb" id="wcontactb" placeholder="Tel./Cel./Fax No.">
													</label>
												</section>
										    </div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wnamec" id="wnamec" placeholder="Company Name">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="waddrc" id="waddrc" placeholder="Company Address">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wpositionc" id="wpositionc" placeholder="Position">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wdateempc" id="wdateempc" placeholder="Date Employed">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wcontactc" id="wcontactc" placeholder="Tel./Cel./Fax No.">
													</label>
												</section>
										    </div> -->
										</fieldset>
										<!-- <br>
										<h3><strong class="hidden">Step 5 -</strong> Document Requirements </h3>
										<fieldset>
												<div class="row">
													<section>
														<label class="label">Birth Certificate</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="birthcert" name="birthcert" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
												<div class="row">
													<section>
														<label class="label">FORM 137</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="form137" name="form137" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
												<div class="row">
													<section>
														<label class="label">FORM 138</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="form138" name="form138" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
												<div class="row">
													<section>
														<label class="label">True Copy of Report Card</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="reportcard" name="reportcard" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
											</fieldset> -->
									</div>

									<div class="step-pane" id="step6">
									<br>
										<h3><strong class="hidden">Step 6 -</strong> By clicking the box,</h3>
									<br>
									<fieldset>
									<div class="row">
										<section>
											<div class="col-sm-12">
												<label class="checkbox">
													<input type="checkbox" id="policy1" /><i></i> I confirm that I have read, understood and accepted the Admission Policies of the Department I am applying.<br/> 
																							 I understanding that my application will be evaluated based on the policies set by the department I am applying.
														
												</label>
											</div>
										</section>
									</div>

									<div class="row">
									<section>
										<div class="col-sm-12">
											<label class="checkbox">
												<input type="checkbox"  id="policy2"/><i></i> I certify that the foregoing information and credentials that I will submit are true and complete to the best of my knowledge.<br/> 
                                                                                        I am aware that omission or falsification of any information and credentials will be considered sufficient reason for rejection of this application or for dismissal, even if already admitted. 
													
											</label>
										</div>
									</section>
									</div>

									<div class="row">
									<section>
										<div class="col-sm-12">
											<label class="checkbox">
												<input type="checkbox"  id="policy4" /> I agree and consent to the Finance Policy of San Beda University.
													<i></i>
											</label>
										</div>
									</section>
									</div>

									<div class="row">
									<section>
									<div class="col-sm-12">
										<label class="checkbox">
											<input type="checkbox"  id="policy3" /> I agree and consent to the Privacy Policy of San Beda University.
												<i></i>
										</label>
									</div>
									</section>
									</div>


									</fieldset>

                                        <!-- <fieldset class="examdata hidden">
											<div class="row">
												<section class="col col-3">
													<label class="checkbox">
														Have you taken <b class="examname">NMAT</b>?
														<input type="checkbox" name="examtaken" id="examtaken"/>
														<i></i>
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="examscore" id="examscore" placeholder="Exam Score">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="examdate" id="examdate" placeholder="Date Taken">
													</label>
												</section>
										    </div>

											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherapply" id="otherapply" placeholder="What other medical/law schools have you applied into?">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
														Have you previously enrolled to other <b class="examschool">medical/law</b> school?
														<input type="checkbox" name="otherenrolled" id="otherenrolled"/>
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherenroll" id="otherenroll" placeholder="If yes, indicate school">
													</label>
												</section>
											</div>

											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
														Have you ever been dismissed or disqualified from enrolling in that medical/law school by reason of scholastic standing or disciplinary actions?
														<input type="checkbox" name="otherdismiss" id="otherdismiss"/>
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherexplain" id="otherexplain" placeholder="Explain">
													</label>
												</section>
											</div>

											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherlicense" id="otherlicense" placeholder="Licensing Exam's Passed">
													</label>
												</section>
											</div>
										</fieldset>
										<fieldset class="hidden">
										<h1 class="text-center text-success"><i class="fa fa-check"></i> Congratulations!
										<br>
										<small>Click finish to end wizard</small></h1>
										<br>
										<br>
										<br>
										<br>
										</fieldset> -->
									</div>
									<div class="step-pane" id="step7">

										<br>



										 <div id="overview_all">
										 </div>





										<!-- <br>
											<h3><strong class="hidden">Step 7 </strong> - Finished</h3>
										<br>
										<br>
										<br>
										<h1 class="text-center text-success"><i class="fa fa-check"></i> Congratulations!
										<br>
										<small>Click finish to submit your application to San Beda University</small></h1>
										<br>
										<br>
										<br>
										<br> -->

                                        <!-- <fieldset class="examdata hidden">
											<div class="row">
												<section class="col col-3">
													<label class="checkbox">
														Have you taken <b class="examname">NMAT</b>?
														<input type="checkbox" name="examtaken" id="examtaken"/>
														<i></i>
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="examscore" id="examscore" placeholder="Exam Score">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="examdate" id="examdate" placeholder="Date Taken">
													</label>
												</section>
										    </div>

											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherapply" id="otherapply" placeholder="What other medical/law schools have you applied into?">
													</label>
												</section>
                                            </div>
											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
														Have you previously enrolled to other <b class="examschool">medical/law</b> school?
														<input type="checkbox" name="otherenrolled" id="otherenrolled"/>
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherenroll" id="otherenroll" placeholder="If yes, indicate school">
													</label>
												</section>
											</div>

											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
														Have you ever been dismissed or disqualified from enrolling in that medical/law school by reason of scholastic standing or disciplinary actions?
														<input type="checkbox" name="otherdismiss" id="otherdismiss"/>
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherexplain" id="otherexplain" placeholder="Explain">
													</label>
												</section>
											</div>

											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherlicense" id="otherlicense" placeholder="Licensing Exam's Passed">
													</label>
												</section>
											</div>
										</fieldset> -->
										<fieldset class="hidden">
										<h1 class="text-center text-success"><i class="fa fa-check"></i> Congratulations!
										<br>
										<small>Click finish to end wizard</small></h1>
										<br>
										<br>
										<br>
										<br>
										</fieldset>
									</div>

								</form>
							</div>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->

			</article>
			<!-- WIDGET END -->

		</div>

		<!-- end row -->

	</section>
	<!-- end widget grid -->

  </div>

	<!-- Modal -->
	<div  class="modal fade" id="policymodal1" tabindex="-1" role="dialog" aria-labelledby="policymodal1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Department Policy</h4>
				</div>
				<div id="scrollme" style="max-height: calc(100vh - 210px);overflow-y: auto;" class="modal-body">

					<div class="row">
						<div class="col-md-12">
<div class="for_shs">						
<h3>SENIOR HIGH SCHOOL (Grade 11)</h3>
<ul>
 	<li>Grade in any subject/quarter must not be lower than 80%</li>
 	<li>General Average and Conduct Average –at least 82 %</li>
</ul>
</div>
&nbsp;
&nbsp;
<div class="for_college">
<div class="for_CAS hidden">
<h3>COLLEGE OF ARTS AND SCIENCES</h3>
&nbsp;
<strong>For Freshmen:</strong>
<ol>
 	<li>High School Grade Average of 82 with no failing grade in any subject</li>
 	<li>Passed entrance examination and interview</li>
 	<li>Favorable recommendation from officials of the last school attended</li>
</ol>
<strong>For Transferees:</strong>
<ol>
 	<li>GPA/GWA of 82 from last school attended</li>
 	<li>No failing grade</li>
 	<li>No incomplete grade nor withdrawn subject without permission in more than 3 units per semester</li>
 	<li>Passed entrance examination and interview</li>
 	<li>Favorable recommendation from officials of the last school attended</li>
</ol>
Note:
<ul>
 	<li>Admission is limited only to those seeking enlistment in the first or second year of the desired course.</li>
 	<li>Transferee must not have been previously enrolled in two or more colleges or universities unless grades are exceptionally high.</li>
 	<li>Former San Beda-CAS student who transferred to another college/university is not allowed for re-admission.</li>
 	<li>For accreditation of courses: A subject with a grade of 82 and above, which has the same description as the subject offered under the degree program, will be credited. However, it may be repeated if deemed necessary by the Board of Admissions.</li>
</ul>
&nbsp;
&nbsp;
</div>
<div class="for_COM hidden">
<h3>COLLEGE OF NURSING</h3>
In selecting applicants for admission, the following will be reviewed:
<ol>
 	<li>Grades received in previous academic work;</li>
 	<li>Entrance exam scores;</li>
 	<li>Recommendation from the officials of the last school attended;</li>
 	<li>Interviews, essay tests, and other supplementary credentials; and</li>
 	<li>Certification of  good moral character</li>
</ol>
In addition, applicants must:
<ol>
 	<li>possess a pleasing personality;</li>
 	<li>meet the minimum height requirement (152.4cm for women and 157.48cm for men);and</li>
 	<li>be physically fit (without deformities/abnormalities)</li>
</ol>
Specifically for freshmen, a high school general average of 82 and above with no grades below 80 in Science, Math, and English is required.

For transferees, admission is limited only to those seeking enlistment in the first or second year. Those who have completed his/her first and second year college in another school but would not otherwise qualify for admission in San Beda University as a transferee may be allowed to enroll as first year student again, provided he/she signs an affidavit waiving any and all credits earned in the school he/she came from. Transferees must also meet the following requirements:
<ol>
 	<li>Must come from a Level II Accredited schools;</li>
 	<li>Should not have a failing grade in any subject taken in college;</li>
 	<li>Should have a weighted GPA not lower than 2.5 counting all the subjects taken in the school he/she was previously enrolled in and subjects to be credited in the academic program he/she is enlisting;</li>
 	<li>Should neither have withdrawn nor have an incomplete grade in more than one (1) subject per semester in the school he/she was previously enrolled;</li>
 	<li>Willing to repeat all General Education subjects with grades below 2.5;</li>
 	<li>Should have passed the initial interview and evaluation of credentials to determine qualification for entrance examination;</li>
 	<li>Must not have been previously enrolled in two or more colleges or universities;</li>
 	<li>Should have passed the entrance exam and interview; and</li>
 	<li>Should submit all other requirements as deemed necessary by the Board of Admissions</li>
</ol>
&nbsp;
&nbsp;
</div>
<div class="for_SOL hidden">
<h3>COLLEGE OF LAW</h3>
&nbsp;
<ol>
 	<li>The San Beda College of Law admits only those applicants whose credentials and personal qualifications yield evidence that they can offer an excellent academic performance and profit from the intellectual, social and spiritual opportunities offered by the College.</li>
</ol>
&nbsp;
<ol>
 	<li>An applicant for admission to the law course should have pursued and satisfactorily completed in an authorized and recognized university or college the courses of study prescribed for a bachelor’s degree in arts or sciences; and should have earned the following number of units in preparatory law course: 18 units of English, 18 units of Social Science, 6 units of Mathematics and 6 units of Filipino.</li>
</ol>
&nbsp;

<em>Note:  A bachelor’s degree holder who passed the entrance examination but lacks the required number of units in the required courses may conditionally enter the College of Law, but must satisfy these requirements before admission to Second Year Law.</em>

&nbsp;
<ol>
 	<li>All students entering the San Beda College of Law for the first time shall take an entrance examination, which consists mainly of a test of logic, comprehension, articulation, and aptitude for the law profession.  Entrance examinations are given, upon prior application, in accordance with the schedule duly announced, generally at any time between the closing of the school year and reopening of classes.</li>
</ol>
&nbsp;
<ol>
 	<li>As a matter of policy, the San Beda College of Law does not accept transferees from other schools, except in extremely meritorious cases as may be determined by the Dean.</li>
</ol>
&nbsp;
<ol>
 	<li>All new students enrolling in the College of Law or reenrolling after an absence of two consecutive semesters are under academic probation for the entire school year.  Old students may be admitted on probation in attendance, conduct or academics.  In such cases, the Committee on Admission or the Dean may set the conditions under which they are admitted on probation.</li>
</ol>
&nbsp;
&nbsp;
</div>
</div>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="agree_1" disabled>
						I AGREE
					</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->




	<!-- Modal -->
	<div  class="modal fade" id="policymodal2" tabindex="-1" role="dialog" aria-labelledby="policymodal1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Article Post</h4>
				</div>
				<div id="scrollme2" style="max-height: calc(100vh - 210px);overflow-y: auto;" class="modal-body">

					<div class="row">
						<div class="col-md-12">
							<p>Although Yasin was not happy about leaving Iraq, he soon settled into his new life in a big city called London. London was very exciting with its tall buildings and museums, and Yasin especially liked the London Planetarium and the big River Thames with all of its old bridges.

Yasin even made friends with a boy who lived next door called Andrew. All summer long Andrew and Yasin played in the park or went to the zoo with Andrew’s mum. Andrew shared his toys and his comics with Yasin and told him all about his favourite superheroes. They even built a camp in Yasin’s back garden where they would hide from the grownups.

The summer was a fun time and young Yasin soon felt quite at home in London even though it was a very big city and not nearly as sunny and hot as it was in Samarra. His English got better and better, especially with help from Andrew, although there were a lot of words that Yasin did not understand and he often felt silly because he could not speak as well as he would like.

When September finally came around and the leaves began to fall from the trees, Yasin’s father explained that it was time for his son to go to school. Yasin was seven years old so he would be going to year three of the local primary school – the same year as his friend Andrew!

Although Yasin was very nervous about going to school, his father and mother assured him that it would be a fun place where he would meet lots of new friends and learn lots of interesting new things. ‘English schools are supposed to be very good,’ said Yasin’s mother.

‘And you will learn some very interesting things and your English will get better in no time,’ assured his father.

Yasin was still not convinced, but when Andrew knocked on the door that morning with a big smile on his face saying how fun it was going to be at school, Yasin felt much better because he trusted his friend.

The two boys chatted all the way to the school gates: Andrew told Yasin about the playground and who was the best teacher and what boys were the most fun and what girls were pretty and how they often served custard for pudding at lunchtimes. Yasin did not know what custard was, but Andrew looked very excited about it so Yasin thought it must taste very good.

But when the boys got to their class, things did not go how Yasin imagined they would. The teacher told Andrew to take a seat at the front of the class as she introduced Yasin to the rest of the children. He did not like standing up in front of the class and one boy shouted out that he was a smelly foreigner. The boys and girls all laughed, and then another boy made fun of Yasin’s accent when he was asked to say his name and where he came from. ‘I can’t understand him, miss. He can’t even speak English,’ said the nasty boy.

Finally Yasin was allowed to take a seat at the back of the class, but he wished that he was next to Andrew as he felt very alone. The girl sitting beside him kept looking at him in a strange way that made Yasin uncomfortable, and during the lesson she put her hand up and asked the teacher if she could move places. Yasin did not understand what he had done to offend the girl.

When the bell went it was time to go out into the playground. All of the children closed their books and put on their coats and headed out of the door into the bright autumn sunshine. The teacher kept Yasin back for a moment and gave him a badge with his name on which she pinned to his jumper. ‘There you go,’ she said with a smile. ‘Now all of the children will be able to learn your name.’

Yasin thought that the badge looked silly, and when he went out into the playground all of the children began pointing and laughing. ‘You’ve got a girl’s name,’ said a small boy with curly blond hair.

Yasin wanted to explain that it was not a girl’s name but he was too nervous because all of the children were pointing at him and laughing. When Yasin got nervous his English was not very good and the words always got stuck in his throat. He was very sad and wanted to run out of the playground back to his mother and father and never return to school again. But just as he was about to run, he heard a familiar voice. ‘Hi Yasin.’ And when he looked up there was Andrew standing right beside him.</p>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="agree_2" disabled>
						I AGREE
					</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->




		<!-- Modal -->
		<div  class="modal fade" id="policymodal3" tabindex="-1" role="dialog" aria-labelledby="policymodal1" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="myModalLabel">Data Privacy Policy</h4>
					</div>
					<div id="scrollme3" style="max-height: calc(100vh - 210px);overflow-y: auto;" class="modal-body">
						<div class="row">
                              <?php echo $this->load->view('admission/vw_dataprivacy','',true);?>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="agree_3" disabled>
							I AGREE
						</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
