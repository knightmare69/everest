<?php
if(isset($tdata) && $tdata){
   foreach($tdata as $r){	
?>
   <tr data-id="<?php echo $r->AppNo;?>">
	  <td class="text-center"><label class="checkbox">
			<input name="checkbox-inline" type="checkbox"  data-id="<?php echo $r->AppNo;?>">
			<i></i>
		  </label></td>
	  <td width="100px;"><button class="btn btn-info btn-edit"><i class="fa fa-edit"></i></button>
	      <div class="btn-group">
		   <button class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-print"></i></button>
		   <ul class="dropdown-menu">
			  <li><a href="javascript:void(0);" class="btn-print" data-id="<?= $r->AppNo?>">Application Form</a></li>
			  <li><a href="javascript:void(0);" class="btn-permit" data-id="<?= $r->AppNo?>">Exam Permit</a></li>
		   </ul>
		  </div>
		  </td>	  
	  <td><?php echo $r->AppNo;?></td>
	  <td><?php echo $r->Fullname;?></td>
	  <td><?php echo $r->ApplicationType;?></td>
	  <td><?php echo date('m/d/Y',strtotime($r->AppDate));?></td>
	  <td><?php echo (($r->Gender=='M')?'Male':'Female');?></td>
	  <td><?php echo $r->ProgCode;?></td>
	  <td><?php echo $r->MajorDiscDesc;?></td>
	  <td><?php echo $r->AdmStatus;?></td>
   </tr>
<?php
   }
}else{
	echo '<tr><td colspan="11" class="text-center"><i class="fa fa-warning"></i> No Data To Display</td></tr>';
}
?>