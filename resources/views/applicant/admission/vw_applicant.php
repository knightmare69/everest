<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Admission</li>
		<li>Applications List</li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
	  <div class="col-xs-12 col-sm-3">
	   Campus:
	   <select class="form-control" id="campus" name="campus">
		   <option value="-1" selected disabled> - Select One - </option>
		   <?php if(isset($rscampus)){
			   foreach($rscampus as $c){
				   echo '<option value="'.$c->CampusID.'">'.$c->ShortName.'</option>';
			   }
		   }?>
	   </select>
	  </div>
	  <div class="col-xs-12 col-sm-3">
	   Acad. Year And Term:
	   <select class="form-control" id="ayterm" name="ayterm">
		   <option value="-1" selected disabled> - Select One - </option>
		   <?php if(isset($rsayterm)){
			   foreach($rsayterm as $a){
				   echo '<option value="'.$a->TermID.'">'.$a->AcademicYearTerm.'</option>';
			   }
		   }?>
	   </select>
	  </div>
	  <div class="col-xs-12 col-sm-3">
	   Application Type:
	   <select class="form-control" id="apptype" name="apptype">
		   <option value="-1" selected disabled> - Select One - </option>
		   <?php if(isset($rsapptypes)){
			   foreach($rsapptypes as $at){
				   echo '<option value="'.$at->TypeID.'">'.$at->ApplicationType.'</option>';
			   }
		   }?>
	   </select>
	  </div>
	  <div class="col-xs-12 col-sm-3">
	   <br/>
	   <div class="col-sm-8">
	   <div class="input-group">
	      <input class="form-control txtparam" type="text"/>
		  <span class="input-group-addon btnsearch">
		      <i class="fa fa-search"></i>
		  </span>
	   </div>
	   </div>
	   <div class="col-sm-4">
	     <div class="btn-group">
		    <button class="btn btn-default" dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i> Option</button>
			<ul class="dropdown-menu">
			   <li class="btnadmit"><a href="javascript:void(0);"><i class="fa fa-plus"></i> Admit</a></li>
			   <li class="btndeny"><a href="javascript:void(0);"><i class="fa fa-minus"></i> Deny</a></li>
			   <li class="btnremove"><a href="javascript:void(0);"><i class="fa fa-times"></i> Remove</a></li>
			</ul>
		 </div>
	   </div>
	  </div>
	  <div class="col-sm-12" style="margin-top:10px !important;">
	     <div class="table-responsive">
		    <table class="table table-bordered table-condense has-tickbox" id="tblapplist">
			   <thead>
			      <th class="text-center"><label class="checkbox">
					    <input name="checkbox-inline" type="checkbox">
					    <i></i>
					  </label></th>
			      <th class="text-center" width="100px;">-</th>
			      <th>AppNo</th>
			      <th>Full Name</th>
			      <th>App. Type</th>
			      <th>App. Date</th>
			      <th>Gender</th>
			      <th>Course Choice 1</th>
			      <th>Major Choice 1</th>
			      <th>Status</th>
			   </thead>
			   <tbody>
			      <?php echo $this->load->view('admission/tdata','',true);?>
			   </tbody>
			</table>
		 </div>
		 <div class="pull-right">
			<ul class="pagination">
				<li>
					<a href="javascript:void(0);"><i class="fa fa-arrow-left"></i></a>
				</li>
				<li>
					<a href="javascript:void(0);" class="pcurrent">1</a>
				</li>
				<li>
					<a href="javascript:void(0);"><i class="fa fa-arrow-right"></i></a>
				</li>
			</ul>
		 </div>
	  </div>
	</div>
	
</div>	