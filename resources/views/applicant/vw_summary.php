<br>
<?php
$apptype= '';
$choice1 = '';
$choice3 = '';
$choice2 = '';
//print_r($rschoice1);
//die();
foreach ($rsapptypes as $key) {
  if($key->TypeID == $summary["ApplyTypeID"]){
      $apptype = $key->ApplicationType;
  }
}

foreach ($rschoice1 as $key) {
  if($key->ProgID == $summary["Choice1_Course"]){
      $choice1 = $key->ProgName;
  }
  if(@is_array($summary)){
  if($key->ProgID == ((array_key_exists("Choice2_Course",$summary) && $summary["Choice2_Course"]!=$summary["Choice1_Course"])?$summary["Choice2_Course"]:'')){
      $choice2 = $key->ProgName;
  }
  if($key->ProgID == ((array_key_exists("Choice3_Course",$summary) && $summary["Choice3_Course"]!=$summary["Choice1_Course"])?$summary["Choice3_Course"]:'')){
  //    $choice3 = $key->ProgName;
  }
  }
}



 ?>

  <div class="col-xs-12 col-sm-6 col-md-2">
      <div class="panel panel-success pricing-big">
          <div class="panel-heading">
              <h3 class="panel-title">
                  Applying For</h3>
          </div>
          <div class="panel-body no-padding text-align-center">

    <div class="price-features">
      <ul class="list-unstyled text-left">
            <li><i class=" text-success"></i> <strong>Department:</strong> <?php echo $summary['department']; ?></li>
            <li><i class=" text-success"></i> <strong>Application Type:</strong> <?php  echo $apptype; ?></li>
            <li><i class=" text-success"></i><strong>First Choice: </strong><?php  echo $choice1; ?></li>
            <li class="<?php echo (($choice2=='')?'hidden':''); ?>"><i class=" text-success"></i><strong>Second Choice: </strong> <?php  echo $choice2; ?></li>
            <li class="hidden"><i class=" text-success"></i><strong>Third Choice: </strong> <?php  echo $choice3; ?></li>

          </ul>
    </div>
          </div>

      </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-md-3">
      <div class="panel panel-teal pricing-big">

          <div class="panel-heading">
              <h3 class="panel-title">
                  Personal Information</h3>
          </div>
          <div class="panel-body no-padding text-align-center">

    <div class="price-features">
      <ul class="list-unstyled text-left">
          <li><i class=" text-success"></i> <strong>Fullname: </strong> <?php  echo $summary['FirstName'] ." ".$summary['MiddleName'] ." ".$summary['LastName']; ?></li>
            <li><i class=" text-success"></i> <strong>Gender: </strong> <?php echo isset($summary['Gender']) ? $summary['Gender']:'' ; ?> </li>
            <li><i class=" text-success"></i> <strong>Date of Birth </strong> <?php echo $summary['DateOfBirth']; ?></li>
            <li><i class=" text-success"></i> <strong>Address: </strong> <?php  echo $summary['Res_Address'] ." ".$summary['Res_Street'] ." ".$summary['Res_TownCity']." ".$summary['Res_ZipCode']; ?></li>
          <li><i class=" text-success"></i> <strong>Mobile Number </strong> <?php echo $summary['MobileNo']; ?> </li>
          <li><i class=" text-success"></i><strong>Telephone </strong> <?php echo $summary['TelNo']; ?> </li>
          <li><i class=" text-success"></i><strong>Email </strong> <?php echo $summary['Email']; ?> </li>
          </ul>
    </div>
          </div>

      </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-md-3">
      <div class="panel panel-primary pricing-big">
        <img  class="ribbon" alt="">
          <div class="panel-heading">
              <h3 class="panel-title">
                  Family Background</h3>
          </div>
          <div class="panel-body no-padding text-align-center">

    <div class="price-features">
      <ul class="list-unstyled text-left">
            <li><i class=" text-success"></i> <strong>Father:</strong> <?php  echo $summary['Father']; ?></li>
            <li><i class=" text-success"></i> <strong>Father Occupation:</strong> <?php  echo $summary['Father_Occupation']; ?></li>
            <li><i class=" text-success"></i> <strong>Father Contact Number:</strong> <?php  echo $summary['Father_TelNo']; ?></li>
            <li><i class=" text-success"></i> <strong>Father Email:</strong> <?php  echo $summary['Father_Email']; ?></li>
            <li><i class=" text-success"></i> <strong>Mother:</strong> <?php  echo $summary['Mother']; ?></li>
            <li><i class=" text-success"></i> <strong>Mother Occupation:</strong> <?php  echo $summary['Mother_Occupation']; ?></li>
            <li><i class=" text-success"></i> <strong>Mother Contact Number:</strong> <?php  echo $summary['Mother_TelNo']; ?></li>
            <li><i class=" text-success"></i> <strong>Mother Email:</strong> <?php  echo $summary['Mother_Email']; ?></li>
            <li><i class=" text-success"></i> <strong>Guardian:</strong> <?php  echo $summary['Guardian']; ?></li>
            <li><i class=" text-success"></i> <strong>Guardian Contact Number:</strong> <?php  echo $summary['Guardian_TelNo']; ?></li>
            <li><i class=" text-success"></i> <strong>Relationship with Guardian:</strong> <?php  echo $summary['Guardian_Relationship']; ?></li>
            <li><i class=" text-success"></i> <strong>Spouse:</strong> <?php  echo $summary['Spouse']; ?></li>
            <li><i class=" text-success"></i> <strong>Spouse Contact Number:</strong> <?php  echo $summary['Spouse_TelNo']; ?></li>
            <li><i class=" text-success"></i> <strong>Spouse Email:</strong> <?php  echo $summary['Spouse_Email']; ?></li>

          </ul>
    </div>
          </div>

      </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-md-2">
      <div class="panel panel-darken pricing-big">

          <div class="panel-heading">
              <h3 class="panel-title">
                Educational Attainment</h3>
          </div>
          <div class="panel-body no-padding text-align-center">

    <div class="price-features">
      <ul class="list-unstyled text-left">
             <?php 
               $lastattend = '';
			   $course     = '';
                if($summary['JHS_School']<>'' && $summary['JHS_School']<>' '){
                    $lastattend = $summary['JHS_School'];
                }
                if($summary['SHS_School']<>'' && $summary['SHS_School']<>' '){
                    $lastattend = $summary['SHS_School'];
                }
                if($summary['College_School']<>'' && $summary['College_School']<>' '){
                    $lastattend = $summary['College_School'];
                }
             ?>
            <li><i class=" text-success"></i> <strong>Last School Attended:</strong> <?php  echo $lastattend; ?></li>

          </ul>
    </div>
          </div>
          <
      </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-md-2">
      <div class="panel panel-success pricing-big">

          <div class="panel-heading">
              <h3 class="panel-title">
                Other Information</h3>
          </div>
          <div class="panel-body no-padding text-align-center">

    <div class="price-features">
      <ul class="list-unstyled text-left">
          <li><i class=" text-success"></i> <strong>Licensure Exam/s Passed:</strong> <?php  echo $summary['lexam_passed']; ?></li>
            <li><i class=" text-success"></i> <strong>Other School:</strong> <?php  echo $summary['Other_School']; ?></li>
              <li class="hidden"><i class=" text-success"></i> <strong>How did you come to know San Beda University?</strong></li>
              <ul>
              <p> <?php  echo $summary['know_others']; ?></p>
            </ul>
          </ul>
    </div>
          </div>

      </div>
  </div>
