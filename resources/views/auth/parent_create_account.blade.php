<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.0
Version: 3.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-.js'); ?>"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-.js'); ?>"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>ESIMS | Create Account</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="K-12 Class Management System" name="description"/>
<meta content="Allan Robert B. Sanchez" name="author"/>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/uniform/css/uniform.default.css'); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= asset('assets/global/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/pages/css/login-soft.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/icheck/skins/all.css'); ?>" rel="stylesheet"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?= asset('assets/global/css/components.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/css/plugins.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/layout.css'); ?>" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?= asset('assets/admin/layout/css/themes/default.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/custom.css'); ?>" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/x-icon" href="{{ url('favicon.ico') }}"/>
<link rel="icon" href="{{ url('favicon.ico') }}" type="image/x-icon">

<style>
	.login .content_register {
	    width: 600px !important;
	    background-color: #fff !important;
	    color: #000 !important;
	}
	.login .content_register h3,.login .content_register p {
	    color: #000 !important;
	}
	.login .content .register-form {
	    display: block;
	}
</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<span style="font-size: 30px;color: #fff;font-weight: bold;font-family: arial">
		K12 Class Management System
	</span>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
		@if (count($errors) > 0)
    		<div class="alert alert-danger">
    			<?php echo $errors->get('Username')[0] ?>
    		</div>
    		<input type="hidden" name="invpswd" id="invpswd" value="<?= isset($errors->get('InvalidPswd')[0]) ? $errors->get('InvalidPswd')[0] : '' ?>">
    		<input type="hidden" name="_auth_token" id="_auth_token" value="<?= isset($errors->get('token')[0]) ? $errors->get('token')[0] : ''  ?>">
		@endif

	<div id="register-success" style="display: none;">
        <div class="well">
            <h2>Congratulations! You have successfully registered! </h2>
            <div class="confirmation">
            <p>Please enter your verification code found at your email</p>
            <input type="text" class="form-control" id="vcode" name="vcode" value="" />
            <div class="form-actions">
                <button type="button" class="btn btn-default " id="btnresend"> Resend Code </button>
                <button type="button" class="btn btn-success pull-right" id="btnconfirm"> Submit </button>
            </div>
			</div>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="alert alert-danger register-alert display-hide">
	</div>
	<!-- BEGIN REGISTRATION FORM -->
	<form class="register-form" action="register" method="post">
		<input type="hidden" name="ParentInquiryID" value="{{Request::get('p')}}">
        <h3>Verification</h3>
        <p>For our security purpose, input your password to secure your inquiry:</p>
        <div class="form-group hide">
			<label class="control-label visible-ie8 visible-ie9">Account Type</label>
		    <select name="acctype" id="acctype" class="form-control">
                <option value="2" selected="selected">Parent/Guardian</option>
            </select>
		</div>
        <div class="form-group hide">
			<div class="row">
				<div class="col-md-6">
					<label class="control-label visible-ie8 visible-ie9">Last Name</label>
					<div class="input-icon">
						<i class="fa fa-font"></i>
						<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="fullname" value="{{ $fname }}"/>
					</div>
				</div>
   	            <div class="col-md-6">
					<label class="control-label visible-ie8 visible-ie9">First Name</label>
					<div class="input-icon">
						<i class="fa fa-font"></i>
						<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="fname" value="{{ $lname }}"/>
					</div>
				</div>
			</div>
		</div>

    	<div class="form-group hide">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="Email" id="Email" value="{{ $email }}"/>
			</div>
		</div>
		<div class="form-group hidden">
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="Username" id="Username" minlength="6" maxlength="100" value="{{ $email }}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="upassword"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
			<div class="controls">
				<div class="input-icon">
					<i class="fa fa-check"></i>
					<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword"/>
				</div>
			</div>
		</div>
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label>
			<input type="checkbox" name="tnc"/> I agree to the 
			<a id="termservice" href="javascript:void(0);"> Terms of Service </a>
			and 
			<a id="ppolicy" href="javascript:void(0);"> Privacy Policy </a>
			</label>
			<div id="register_tnc_error">
			</div>
		</div>
		<div class="form-actions">
			<a href="<?php echo url('/');?>" id="register-back-btn" class="btn red"><i class="m-icon-swapleft"></i> Cancel </a>
			<button type="submit" id="register-submit-btn" class="btn blue pull-right"> Verify <i class="m-icon-swapright m-icon-white"></i></button>
		</div>
	</form>
	<!-- END REGISTRATION FORM -->
</div>
<?php echo view('auth.policy');?>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 Powered by : Prince Technologies Corporation
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?= asset('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/excanvas.min.js'); ?>"></script>
<![endif]-->
<script src="<?= asset('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/backstretch/jquery.backstretch.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= asset('assets/global/plugins/select2/select2.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootbox/bootbox.min.js'); ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= asset('assets/global/scripts/metronic.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/system/scripts/general.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/system/scripts/Accounts/parent-create.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
var base_url='<?= url() ?>';
jQuery(document).ready(function() {
  	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
  	Register.init();

       // init background slide images
       $.backstretch([
        "<?= asset('assets/admin/pages/media/bg/1.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/2.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/3.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/4.jpg')?>"
        ], {
          fade: 1000,
          duration: 8000
    	});
	$('#ppolicy').click(function(){
		$('#policy').modal('show');
	});	
	$('#termservice').click(function(){
		$('#terms').modal('show');
	});	
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
