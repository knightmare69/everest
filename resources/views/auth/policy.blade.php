<div id="terms" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Data Privacy Notice</h4>
			</div>
			<div class="modal-body">
			  <div class="row text-center">
				<embed src="<?php echo url('assets/dataprivacy.pdf?t='.date('Ymdis').'#toolbar=0&navpanes=0');?>" width="98%" height="480px;"></embed>
			  </div>
              <div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">OK</button>
              </div>			  
			</div>
		</div>
    </div>
</div>	

<div id="policy" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-backdrop fade in"></div>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			<h4 class="modal-title">Data Privacy Notice</h4>
			</div>
			<div class="modal-body">
			  <div class="row text-center">
				<embed src="<?php echo url('assets/dataprivacy.pdf?t='.date('Ymdis').'#toolbar=0&navpanes=0');?>" width="98%" height="480px;"></embed>
			  </div>
              <div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">OK</button>
              </div>			  
			</div>
		</div>
    </div>
</div>	