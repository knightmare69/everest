<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.0
Version: 3.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-.js'); ?>"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-.js'); ?>"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>ESIMS | Reset Password</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/uniform/css/uniform.default.css'); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= asset('assets/global/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/pages/css/login-soft.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/icheck/skins/all.css'); ?>" rel="stylesheet"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?= asset('assets/global/css/components.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/css/plugins.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/layout.css'); ?>" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?= asset('assets/admin/layout/css/themes/default.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/custom.css'); ?>" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>

<style>
	.login .content_register {
	    width: 600px !important;
	}
</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<span style="font-size: 30px;color: #fff;font-weight: bold;font-family: arial">
		Everest Academy Manila
	</span>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<h3 class="form-title">Reset Password</h3>
	<div class="alert alert-danger reset-alert display-hide">
		</div>
	<form class="form-horizontal reset-form" role="form" method="POST" action="{{ url('reset/password') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="token" value="{{ Request::get('token') }}">

		<div class="form-group">
			<label class="control-label">Password</label>
			<input type="password" class="form-control" name="password" id="password">
		</div>

		<div class="form-group">
			<label class="control-label">Confirm Password</label>
			<input type="password" class="form-control" name="password_confirmation">
		</div>

		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
					Reset Password
				</button>
			</div>
		</div>
	</form>		
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?= asset('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/excanvas.min.js'); ?>"></script> 
<![endif]-->
<script src="<?= asset('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/backstretch/jquery.backstretch.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= asset('assets/global/plugins/select2/select2.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootbox/bootbox.min.js'); ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= asset('assets/global/scripts/metronic.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/demo.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/system/scripts/general.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/system/scripts/Accounts/reset.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
var base_url='<?= url() ?>';
jQuery(document).ready(function() {     
  	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
  	Reset.init();
  	Demo.init();
       // init background slide images
       $.backstretch([
        "<?= asset('assets/admin/pages/media/bg/1.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/2.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/3.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/4.jpg')?>"
        ], {
          fade: 1000,
          duration: 8000
    	});
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>