<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.0
Version: 3.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-.js'); ?>"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-.js'); ?>"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>ESIMS | Inquiry</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="K-12 Class Management System" name="description"/>
<meta content="Allan Robert B. Sanchez" name="author"/>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/uniform/css/uniform.default.css'); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?= asset('assets/global/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/pages/css/login-soft.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/icheck/skins/all.css'); ?>" rel="stylesheet"/>
<link href="<?= asset('assets/global/plugins/bootstrap-datepicker/css/datepicker.css'); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?= asset('assets/global/css/components.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/css/plugins.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/layout.css'); ?>" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?= asset('assets/admin/layout/css/themes/default.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/admin/layout/css/custom.css'); ?>" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/x-icon" href="{{ url('favicon.ico') }}"/>
<link rel="icon" href="{{ url('favicon.ico') }}" type="image/x-icon">

<style>
	.login .content_register {
	    width: 600px !important;
	    background-color: #fff !important;
	    color: #000 !important;
	}
	.login .content_register h3,.login .content_register p {
	    color: #000 !important;
	}

	.child-form {
		width: 600px !important;
	}
	.inquiry-page {
		background-color: #fff !important;
	}

</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<span style="font-size: 30px;color: #fff;font-weight: bold;font-family: arial">
		Inquiry Form
	</span>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content inquiry-page">
	<!-- BEGIN LOGIN FORM -->
		@if (count($errors) > 0)
    		<div class="alert alert-danger">
    			<?php echo $errors->get('Username')[0] ?>
    		</div>
    		<input type="hidden" name="invpswd" id="invpswd" value="<?= isset($errors->get('InvalidPswd')[0]) ? $errors->get('InvalidPswd')[0] : '' ?>">
    		<input type="hidden" name="_auth_token" id="_auth_token" value="<?= isset($errors->get('token')[0]) ? $errors->get('token')[0] : ''  ?>">
		@endif	

	<!-- BEGIN REGISTRATION FORM -->
	<form class="inquiry-form" action="register" method="post">
		<input class="form-control placeholder-no-fix" type="hidden" placeholder="Last Name" name="type" value="{{ $type }}"/>
        <h3>Parent Information Form</h3>
		<div class="alert alert-danger register-alert display-hide">
		</div>
        <p>
			 Fill up details below:
		</p>
		<div class="form-group">
			<label class="control-label ">Last Name of the Parent/Guardian</label>
			<div class="input-icon">
				<i class="fa fa-font"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lname" value="{{ old('lname') }}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label ">First Name of the Parent/Guardian</label>
			<div class="input-icon">
				<i class="fa fa-font"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="fname" value=""/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label ">Middle Name of the Parent/Guardian</label>
			<div class="input-icon">
				<i class="fa fa-font"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Middle Name" name="mname" value=""/>
			</div>
		</div>
    	<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label ">Email of the Parent/Guardian</label>
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" id="email" value="{{ old('email') }}"/>
			</div>
		</div>

		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label ">Contact Number of the Parent/Guardian</label>
			<div class="input-icon">
				<i class="fa fa-phone"></i>
				<input class="form-control placeholder-no-fix" type="text" placeholder="Contact Number" name="contact_no" id="contact_no" value="{{ old('contact_no') }}"/>
			</div>
		</div>
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
		<div class="form-actions">
			<button type="submit" id="register-next-btn" class="btn blue col-md-12"> Next <i class="m-icon-swapright m-icon-white"></i></button>
		</div>
	</form>
	<!-- END REGISTRATION FORM -->

	<!-- BEGIN REGISTRATION FORM -->
	<form class="inquiry-app-form" style="display: none;" action="register" method="post">
		<input class="form-control placeholder-no-fix" type="hidden" placeholder="Last Name" name="type" value="{{ $type }}"/>
        <h3>Child Information Form</h3>
		<div class="alert alert-danger register-alert display-hide">
		</div>
        <p>
			 Fill up details below. You  may submit multiple applications.
		</p>
		<div class="form-group hidden">
			<div class="col-md-12">
				<label class="control-label">Request a tour?</label>
			    <input type="checkbox" class="" name="schedule_type">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<label class="control-label">Year Applying for</label>
			    <select name="schoolyear" id="schoolyear" class="form-control">
			    	<option value="" selected="">-- CHOOSE--</option>
					<?php 
					foreach($terms as $row){
					  if (substr($row->AcademicYear,0,4)>=date('Y')){
				    ?>	
					  <option {{ substr($row->AcademicYear,0,4)==date('Y') ? 'selected' : '' }} value="{{$row->TermID}}" >{{ $row->AcademicYear.' - '.$row->SchoolTerm }}</option>
			    	<?php
					 }
					}
					?>
	            </select>
			</div>
			<div class="col-md-6">
				<label class="control-label ">Year Level</label>
			    <select name="yearlevel" id="yearlevel" class="form-control">
			    	<option value="" selected="">-- CHOOSE --</option>
	                @foreach($yearlevel as $row)
	                	@if($row->YearLevelName != 'Grade 12')
	                <option data-progclass="{{$row->ProgClass}}" value="{{$row->YearLevelID}}" >{{$row->YearLevelName}}</option>
	                	@endif
	                @endforeach
	            </select>
	            <input type="hidden" name="progclass" id="progclass">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<label class="control-label">Last Name of the Child</label>
				<div class="input-icon">
					<i class="fa fa-font"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lname" value="{{ old('lname') }}"/>
				</div>
			</div>
			<div class="col-md-6">
				<label class="control-label ">First Name of the Child</label>
				<div class="input-icon">
					<i class="fa fa-font"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="fname" value=""/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<label class="control-label ">Middle Name of the Child</label>
				<div class="input-icon">
					<i class="fa fa-font"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="Middle Name" name="mname" value=""/>
				</div>
			</div>
			<div class="col-md-6">
				<label class="control-label ">Sex of the Child</label>
				<select name="gender" id="gender" class="form-control">
			    	<option value="" selected="">-- CHOOSE --</option>
	                <option value='M'>Male</option>
	               	<option value='F'>Female</option>
	            </select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<label class="control-label ">Birthdate of the Child</label>
				<div class="input-icon">
					<i class="fa fa-calendar"></i>
					<input type="text" placeholder="mm/dd/yyyy" data-date-format="mm/dd/yyyy" class="form-control date-picker" name="bdate" id="bdate" value="">
				</div>
			</div>
		</div>
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
		<div class="form-actions">
			<div class="row">
				<div class="col-md-12" style="margin-top: 10px;">
				    <br/><br/><br/>
					<div class="col-md-12">
						<button type="button" id="register-back-btn" class="btn gray col-md-4"> <i class="m-icon-swapleft m-icon-black"></i> Back</button>
						<button type="submit" id="register-submit-btn" class="btn blue col-md-4 pull-right"> Submit <i class="m-icon-swapright m-icon-white"></i></button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- END REGISTRATION FORM -->

    <div id="register-success" class="register-form">

        <div class="well">
            <messageresult></messageresult>
        </div>

        <div class="clearfix"></div>
    </div>

</div>

<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 Powered by : Prince Technologies Corporation
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?= asset('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/excanvas.min.js'); ?>"></script>
<![endif]-->
<script src="<?= asset('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/backstretch/jquery.backstretch.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= asset('assets/global/plugins/select2/select2.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootbox/bootbox.min.js'); ?>"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= asset('assets/global/scripts/metronic.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/admin/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/system/scripts/general.js'); ?>" type="text/javascript"></script>
<script src="<?= asset('assets/system/scripts/Accounts/Inquiry.js'); ?>" type="text/javascript"></script>

<style>
	.child-form {
		width: 600px !important;
	}
	.inquiry-page {
		background-color: #fff !important;
	}
	

</style>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
var base_url='<?= url() ?>';
jQuery(document).ready(function() {
  	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
  	Login.init();

       // init background slide images
       $.backstretch([
        "<?= asset('assets/admin/pages/media/bg/1.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/2.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/3.jpg')?>",
        "<?= asset('assets/admin/pages/media/bg/4.jpg')?>"
        ], {
          fade: 1000,
          duration: 8000
    	});
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
