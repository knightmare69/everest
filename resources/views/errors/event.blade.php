<div class="alert alert-success success_message hide alert-dismissable">
	<button aria-hidden="true" data-dismiss="alert" class="close hide" type="button"></button>
	<message>&nbsp;</message>
</div>
<div class="alert alert-danger error_message hide alert-dismissable">
	<button aria-hidden="true" data-dismiss="alert" class="close hide" type="button"></button>
	<message>&nbsp;</message>
</div>