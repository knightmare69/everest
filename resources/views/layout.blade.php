@if(isset($layout) && $layout != '')
	<?php $layout = 'layout.'.$layout.'.middle.container'; ?>
	@include($layout)
@else
	@include('layout.'.config('app.layout').'.middle.container')
@endif