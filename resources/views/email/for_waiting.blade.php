<table>
<tr>
      <td>
            <b style="font-size: 15px;"> </b>Dear {{ ucfirst(strtolower(getObjectValue($post,'fname'))).' '.ucfirst(strtolower(getObjectValue($post,'lname'))) }},
            <p>Thank you for your interest in Everest Academy. We regret to inform you that there are currently no seats available for your child's grade level.</p> 
            <p>Students deemed eligible for admission, while waiting for a seat, can be included in our wait pool. Priority is given to those most aligned with the school's vision and mission and not on first-come first-served basis. If you wish that your child be included in the wait pool, please click this <a href="<?=getObjectValue($post,'link')?>">link</a> to begin the application process.</p>
            <p>After completing the form, please click "Submit". You will receive an email to arrange your initial admission interview. In this interview we hope to determine if the school's vision and mission, and your goals for your child match, and if we should proceed with your child's application to the waiting pool.</p>
            <p>Please bring a printed copy of the accomplished application form to your scheduled interview.</p>
            <p>
                Thank you
            </p>
      </td>
</tr>
</table>