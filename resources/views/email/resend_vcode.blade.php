<table>
<tr>
      <td>
            <b style="font-size: 15px;">Hi!,</b> {{ getObjectValue($data,'FullName') }}
            <h4>You are requesting for the confirmation code of your account.</h4>
      
            <p>
                  To activate your account. Please click the following URL:
            </p>
            <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?= url('registration/confirm?code='.getObjectValue($data,'ConfirmNo'));?>">
                        <?= url('registration/confirm?code='.getObjectValue($data,'ConfirmNo'));?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <p>
                   If clicking the URL above does not work, copy and paste the URL into a browser window.
            </p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>