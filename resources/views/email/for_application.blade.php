<table>
<tr>
      <td>
            <b style="font-size: 15px;"> </b>Dear {{ ucfirst(strtolower(getObjectValue($post,'fname'))).' '.ucfirst(strtolower(getObjectValue($post,'lname'))) }},
            <p>Thank you for your interest in Everest Academy. Please fill out the form to begin the application process by clicking 
            <a href="<?=getObjectValue($post,'link')?>">here</a>.</p> 
            <p>After you have completed the form and submitted it on-line, you will receive an email to arrange your initial admissions interview. <!--In this interview, we hope to determine the congruence between your goals for your child and the school's vision and mission, and if we should proceed with your child’s application.</p>
            <p>Please bring a printed copy of the accomplished application form to your scheduled interview.--></p>
            <p>If clicking on the link does not work, kindly copy and paste the URL below into a browser window:</p>
             <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?=getObjectValue($post,'link')?>">
                        <?=getObjectValue($post,'link')?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <p>
                Thank you
            </p>
      </td>
</tr>
</table>


