<table>
<tr>
      <td>
            <p>Dear {{ getObjectValue($post,'FullName') . ' ' . getObjectValue($post,'fname') }}</p>
            <p>Thank you for submitting the application form for admission to Everest Academy. You will receive an email from <a href="mailto:admissions@everestmanila.edu.ph">admissions@everestmanila.edu.ph</a> to arrange your initial admissions interview. Please submit a printed copy of the accomplished application form when we return to the campus along with the other required application documents. We look forward to assisting you in the admissions process.</p>
            <!-- BEGIN: Note Panel
            <p>You can return to your application form anytime with this link:</p>
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?= url('registration/confirm?code='.getObjectValue($post,'ConfirmNo').'&app='.getObjectValue($post,'AppNo'));?>">
                                 <?= url('registration/confirm?code='.getObjectValue($post,'ConfirmNo').'&app='.getObjectValue($post,'AppNo'));?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
			<!-- BEGIN: Note Panel -->
            <p>You can print your application form anytime with this link:</p>
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?= url('admission/redirectReport?AppNo='.encode(getObjectValue($post,'AppNo')));?>">
                        <?= url('admission/redirectReport?AppNo='.encode(getObjectValue($post,'AppNo')));?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
			
            <p>If clicking the URL above does not work, copy and paste the URL into a browser window.</p>
            <p>Thank you!</p>
            <!-- END: Note Panel -->
			<div>@include('email.admission_esign')</div>
      </td>
</tr>
</table>