<table>
<tr>
      <td>
            <b style="font-size: 15px;">Hi!,</b> {{ ucfirst(strtolower(getObjectValue($post,'fname'))) }}
            <h4>Thank you for joining the K-12 Class Management System!</h4>
            <p>Here is your account details:</p>

            <p>
                  To verify your account. Please click the link below.
            </p>
            
            
            <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?=getObjectValue($post,'verification_link')?>">
                        <?=getObjectValue($post,'verification_link')?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <p>
                   If clicking the URL above does not work, copy and paste the URL into a browser window.
            </p>
            <p>
                Thank you
            </p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>