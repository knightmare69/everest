<table>
<tr>
      <td>
            <h4>Your K12 Account Details.</h4>
            <p>You have successfully reset your account. Below are your account details.</p>
            <p>
                  <b>Username:</b> {{ getObjectValue($post,'username') }} <br />
                  <b>Password:</b> {{ getObjectValue($post,'password') }}
            </p>                  
            <p>
                  Click here to login.
            </p>
            <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="http://myeverestacademy.everestmanila.com/public/auth/login">http://myeverestacademy.everestmanila.com/public/auth/login</a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>

<!-- 
                              CHANGELOG:
                              
                              Updated by: Lendell Garcellano
                              Updated date: 7/11/2017 
                              Updated Link to redirect to Everest K-12 Portal
-->