<table>
<tr>
      <td>
	   <?php 
	    $sched=date('Y-m-d H:i A');
		$msg  ='The first step as you and the school consider an application from your family is an admissions inquiry interview, which we would like to schedule on '.date('m/d/Y',strtotime($sched)).' at '.date('h:i A',strtotime($sched)).'. Kindly confirm by reply email.';
	    if(getObjectValue($post,'sched',false)!=false){
		  	foreach(getObjectValue($post,'sched',false) as $r){
				$sched = $r->BatchDate;
				$msg   = 'The first step as you and the school consider an application from your family is an admissions inquiry interview, which we would like to schedule on '.date('m/d/Y',strtotime($sched)).' at '.date('h:i A',strtotime($sched)).'. Kindly confirm by reply email.';
			}
		}else if(getObjectValue($post,'confirm',false)!=false){
			$msg ='Thank you for confirming your inquiry interview schedule. Please complete this online form to help us get to know you a little prior to meeting you.<br/br/>We look forward to seeing you then.';
		}else if(getObjectValue($post,'child',false)!=false){
			$msg ='It was a pleasure to meet you and I do hope that you were able to gather all the information you were looking for in your search for the best school for '.getObjectValue($post,'child','(name of child/ren)').'. I look forward to hopefully receiving and processing the application documents for your child.';
		}
	    ?>
		<b style="font-size: 15px;"> </b>Dear {{ ucfirst(strtolower(getObjectValue($post,'fname'))).' '.ucfirst(strtolower(getObjectValue($post,'lname'))) }},
		<p>{{$msg}}</p>
		
		<p></p>
		 <!-- BEGIN: Note Panel -->
		<p>
			Thank you
		</p>
		<!-- END: Note Panel -->
      </td>
</tr>
</table>