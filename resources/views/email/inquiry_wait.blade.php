<table>
<tr>
      <td>
            <b style="font-size: 15px;">Dear </b> {{ ucfirst(strtolower(getObjectValue($post,'fname'))).' '.ucfirst(strtolower(getObjectValue($post,'lname'))) }},
            <p>Thank you for your interest in Everest Academy. Our seats for the grade level you are inquiring about are currently full and your application will be for inclusion in the waiting pool. Should a seat become available only students in the waiting pool are considered for admission and it is therefore advisable to proceed with an application. You will need to complete the application requirements, have your child  take the entrance test and if eligible for admission your child will be placed in the waiting pool.</p> 
            <p>To proceed with an application please complete the online application form <a href="<?=getObjectValue($post,'link')?>">here</a> in order for us to verify the availability of seats in the grade level that you are interested in.</p>
            
            <p>If clicking on the link does not work, kindly copy and paste the URL below into a browser window:</p>
             <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?=getObjectValue($post,'link')?>">
                        <?=getObjectValue($post,'link')?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <p>
                Thank you
            </p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>