<table>
<tr>
      <td>
            <b style="font-size: 15px;"> </b>Dear {{ getObjectValue($post,'fullname').' '.getObjectValue($post,'fname') }},
            <p>Thank you for <?php echo ((getObjectValue($post,'Inquired','0')==1)?"your application.":"joining the K-12 Class Management System.");?></p>
            <p>Here are your account details:</p>
            <p><b>Username:</b> {{ getObjectValue($post,'Username') }}</p>
            <p><b>Password:</b> {{ getObjectValue($post,'upassword') }}</p>
            <?php if(getObjectValue($post,'Inquired','0')==0){?>
			<p>To activate your account. Please enter your verification code : {{getObjectValue($post,'code')}}</p>
            <p>You may also click the following link to activate your account.</p>
            
            <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?= url('registration/confirm?code='.getObjectValue($post,'code'));?>">
                        <?= url('registration/confirm?code='.getObjectValue($post,'code'));?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <p>If clicking the URL above does not work, copy and paste the URL into a browser window.</p>
            <?php } ?>
            <p>Thank you</p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>