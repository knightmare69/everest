<table>
<tr>
      <td>
            <b style="font-size: 15px;">Dear </b> {{ ucfirst(strtolower(getObjectValue($post,'fname'))) }},
            <p>
                  Thank you for submitting an application for <b>{{ucfirst(strtolower(getObjectValue($post,'child')))}}</b>. Your child is now included as an applicant for admission to Everest Academy Manila and will be assigned an entrance test schedule. 
            </p>    
            <p>To check your admission application list, please go to this link <a href="{{url('admission/listing')}}">{{url('admission/listing')}}</a></p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>