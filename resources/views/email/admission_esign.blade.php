<table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none">
 <tbody><tr>
  <td width="91" valign="top" style="width:68.4pt;padding:0cm 5.4pt">
	<p style="margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US">&nbsp;<img src="https://ci4.googleusercontent.com/proxy/nWd7e4sFgkHXBDu306Gfoj4SCpaTu6esnyBikfXfwZ0kT9ZTsOrPMAfAtw_wOqftzjWI5107INGEkoVhTdCGNFPmHEYtSVkPFzmpMtt9MKHh_zRgwrlJq5MpyDGlxHf7iJkTQQFUDaGPjBZ8371NjYc1LoAc8tzrhoMErXbwa14prhIzJe0XrJ3R=s0-d-e1-ft#https://drive.google.com/a/everestmanila.edu.ph/uc?id=0B8YKFkQ-_cNTT2VwVXZIMFA5bTBhYUZVVFM5QWtpVGpVc3Rz&amp;export=download" width="96" height="96" class="CToWUd"><span></span><span></span></span></p>
  </td>
  <td width="547" valign="top" style="width:410.4pt;padding:0cm 5.4pt">
	<p style="margin-bottom:0.0001pt;text-align:justify;line-height:normal"><b><span lang="EN-US" style="font-family:Optima">Admission Office</span></b></p>
	<p style="margin-top:1px;margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US" style="font-family:Optima">Everest Academy Manila</span></p>
	<p style="margin-top:1px;margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US" style="font-family:Optima">3846 38<sup>th</sup> Drive North, Bonifacio Global City,</span></p>
	<p style="margin-top:1px;margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US" style="font-family:Optima">Taguig 34 Philippines</span></p>
	<p style="margin-top:1px;margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US" style="font-family:Optima">Tel: +632-8882-5019/ 8882-4981 loc. 111</span></p>
	<p style="margin-top:1px;margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US" style="font-family:Optima">E-mail:<a href="mailto:admissions@everestmanila.edu.ph" target="_blank">admissions@everestmanila.edu.<wbr>ph</a></span></p>
	<p style="margin-top:1px;margin-bottom:0.0001pt;text-align:justify;line-height:normal"><span lang="EN-US" style="font-family:Optima">Website:<a href="http://www.everestmanila.com" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.everestmanila.com&amp;source=gmail&amp;ust=1598420790905000&amp;usg=AFQjCNFJuEB6xPVAOACiwlF2k7D-jAO7Vg">www.everestmanila.com</a></span></p>
  </td>
 </tr>
</tbody>
</table>