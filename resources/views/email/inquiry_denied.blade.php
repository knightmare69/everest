<table>
<tr>
      <td>
            <b style="font-size: 15px;"> </b>Dear {{ ucfirst(strtolower(getObjectValue($post,'fname'))).' '.ucfirst(strtolower(getObjectValue($post,'lname'))) }},
            <p>Thank you once again for your interest in Everest Academy for {{getObjectValue($post,'child','(child’s name)')}}. We always do our best to make decisions in the best interest of each child. Because of this the admissions committee has decided that we will not be offering him/her admission to {{getObjectValue($post,'yrlvl')}}  for SY{{getObjectValue($post,'ayterm')}}.</p>
            <p>It was a pleasure to meet your family and I do wish {{getObjectValue($post,'child','(child’s name)')}} the best and success in the school that you do select.</p>

            <p>
                Thank you
            </p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>