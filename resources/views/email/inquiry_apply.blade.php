<table>
<tr>
      <td>
            <b style="font-size: 15px;"> </b>Dear {{ ucfirst(strtolower(getObjectValue($post,'fname'))).' '.ucfirst(strtolower(getObjectValue($post,'lname'))) }},
            <p>Thank you for your interest in Everest Academy Manila. Please fill in the requested information to continue your inquiry by clicking 
            <a href="<?=getObjectValue($post,'link')?>">here</a> in order for us to verify the availability of seats in the grade level that you are interested in.
            </p>
            
            <p>If clicking on the link does not work, kindly copy and paste the URL below into a browser window:</p>
             <!-- BEGIN: Note Panel -->
            <table class="twelve columns" style="margin-bottom: 10px">
            <tr>
                  <td class="panel">
                        <a href="<?=getObjectValue($post,'link')?>">
                        <?=getObjectValue($post,'link')?> </a>
                  </td>
                  <td class="expander">
                  </td>
            </tr>
            </table>
            <p>
                Thank you
            </p>
            <!-- END: Note Panel -->
      </td>
</tr>
</table>