SET IDENTITY_INSERT [dbo].[page_actions] ON 
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (35, 5085, N'Clinic Visit Per Department', N'clinic-visit-per-department', 0, 1)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (36, 5085, N'Hourly Census', N'hourly-census', 0, 2)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (37, 5085, N'Top 10 Illness', N'top-10-illness', 0, 3)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (38, 5085, N'Top 5 most utilized supplies used', N'top-5-most-utilized-supplies-used', 0, 4)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (39, 5085, N'Top 5 student that visits the clinic in a month', N'top-5-student-that-visits-the-clinic-in-a-month', 0, 5)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (40, 5084, N'All subjects of  student grades', N'all-subjects-of--student-grades', 0, 1)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (41, 5084, N'Comparative class average of students', N'comparative-class-average-of-students', 0, 2)
INSERT [dbo].[page_actions] ([action_id], [page_id], [name], [slug], [is_default], [sort]) VALUES (42, 5084, N'Distribution of grades per class', N'distribution-of-grades-per-class', 0, 3)
SET IDENTITY_INSERT [dbo].[page_actions] OFF
