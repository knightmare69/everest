<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebmailUsersConfigTable extends Migration {

	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webmail_config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id',250);
            $table->string('email',200)->unique();
            $table->string('host',200);
            $table->string('server',25);
            $table->string('encryption',25);
            $table->string('port',20);
            $table->tinyInteger('validate_cert')->length(1)->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webmail_config');
    }

}
