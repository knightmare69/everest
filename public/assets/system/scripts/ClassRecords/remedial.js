var ME = {

    err : function(){
        msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
        progressBar('off');
    },

    post : function(e){

      var _checked = $('#tblstudents').find('.chk-child:checked');

      var validatepost = function($r){
            var flg = true;
            $r.find('td.remedial').each(function(){
                score = $(this).html();
                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                    return false;
                }
            });
            return flg;
      };

      var execution = function(){

            var ids = [];

            _checked.each(function(e){
                var _tr = $(this).closest('tr') ;
                var _d = _tr.data();
                var id = _tr.attr('data-idno');
                var sc = _tr.attr('data-sched');

                if(id !== 0 || id !== undefined){
                    if (validatepost(_tr)) {
                        var x = { idno : id, sched :sc, reg : _tr.attr('data-reg')  };
                        ids.push(x);
                    }
                }
            });

            if(ids.length >= 1){

                var data = { event : 'post', term : ME.$term.val(), stud : ids, code :  $('#sched').find('option:selected').text() };
                setAjaxRequest( base_url + page_url + 'event', data, function(r) {
                    if (r.error) {
                        msgbox('error','Sorry, unable to post conduct');
                    } else {
                        //showSuccess(result.message);
                        sysfnc.showAlert('.portlet-body','success',r.message,true,true,true,'check',10)
                        ME.initstudents();
                    }
                });
                progressBar('off');
            } else{
                msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
            }
        };

        if (_checked.length > 0 ) {

          $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
    			content : "Posting student remedial grade will validate the record and it will no longer editable.. <br/> Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     $.SmartMessageBox({
            			title : "<span class='text-warning'><i class='fa fa-lock'></i> Do you wish to POST the selected student(s)?</span>",
            			content : "Do you certify that all the remedial grades per student in this class record are true and correct?",
            			buttons : '[No][Yes]'
            		}, function(ButtonPressed) {
            			if (ButtonPressed === "Yes") {
            			     execution(true);
            			}
            		});
    			}else{
    			     $(this).remove();
    			}
    		});
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },

    saveConduct : function(row,col){

        var $r = $('#tblstudents tbody tr').eq(row);
        var $c = $r.find('td').eq(col);

        var data = {
            event : 'save-remedial',
            term : $('#term').val(),
            code : $.trim($('#sched').find('option:selected').html()),
            schedule : $r.attr('data-sched'),
            idno : $r.attr('data-idno'),
            regid : $r.attr('data-reg'),
            score : $c.html(),
            finl : $.trim($r.find('td.final').html()),
            rmk : $.trim($r.find('td.remarks').html()),
            idx : $r.attr('data-id'),
        };

        var mysuccess = function(e){
            if(!e.error){
                //console.log('conduct grade successfully saved!');
            }
        };
        setAjaxRequest(ME.mylink+"event",data,mysuccess,ME.error,'json',true,false);

    },

    compute : function($row){
        var score = 0;
        var i = 2;

        var f = $.trim($row.find('td.final').html());
        var r = $.trim($row.find('td.remedial').html());

        var ave = (parseFloat(f) + parseFloat(r))  / i ;

        var v = ME.validGrade(ave);
        if(v.valid){
            $row.find('td.recompute').html(ave.toFixed(2));
            $row.find('td.letter').html(v.letter);
            $row.find('td.remarks').html(v.remarks);
        }
    },

    validGrade : function(grade){

        var v = { valid : false, msg : 'invalid grade', letter : '', desc : '', remarks : '', };

         $('#tblgradesystem tbody tr').each(function(){
            var min =  strCast( $(this).find('td.min').html().trim() , 'float') ;
            var max =  strCast( $(this).find('td.max').html().trim() , 'float') ;
            if( grade >= min && grade <= max ){
                v = {
                    valid : true,
                    msg : 'grade accepted',
                    letter : $(this).find('td.letter').html().trim(),
                    desc : $(this).find('td.desc').html().trim(),
                    remarks : $(this).find('td.remarks').html().trim(),
                };
                return v;
            }

        });

        return v;

    },

    initstudents: function(e){

        var s = $('#sched').val(), t = $('#term').val();
        var data = { event : 'students', term : t,sched : s };

        var success = function(e){
            $('#studentlist-container').html(e.content);
            progressBar('off');
        };

        if(s == '' || s == '-1' ){
            this.initloads();
            alert('Please select a subject');
            return false;
        }

        if(t == '' || t == '-1'){
            alert('Please select a academic year and term');
            return false;
        }

        $('#studentlist-container').html("<h4> <i class='fa fa-spin fa-spinner'></i> <i class='font-sm'>Please wait while loading...</i>  </h4>");
        setAjaxRequest(this.mylink+"event",data,success,ME.err,'json',true);
        progressBar('off');

    },

    initplugins : function(){
        $('.bs-select').selectpicker({ iconBase: 'fa', tickIcon: 'fa-check', size : 8 });
    },

    initservices: function() {

        var $editor = $('#tbleditor');
        var $editorText = $('#tbleditor').find('input');

        $editorText.on('blur',$.proxy(this.hideactiveText,this));
        $editorText.on('focus',$.proxy(this.editorFocusUp,this));
        $editorText.on('mouseup',$.proxy(this.editorMouseUp,this));
        $editorText.on('change',$.proxy(this.editorChange,this));
        $editorText.on('paste',$.proxy(this.mypaste,this));

        $editorText.on('keypress',function(e){

            if( e.keyCode === 27) {
                var c = $(e.target).attr('data-col'), r =  $(e.target).attr('data-row');
                var $nr = $('#tblstudents tbody tr').eq(r).find('td').eq(c);
                ME.compute(r,c);
                ME.hideactiveText();
                ME.cell_focus($nr);
            }else if (e.keyCode == 13 ){
                if(!ME.IsChanged){
                    ME.editorChange(e);
                }
                ME.IsChanged=false;
            }
        });

        $('body').on('click','.btn, .reload, .clink', $.proxy(this.menu,this));
        $('body').on('change','#sched, #period', $.proxy(this.initstudents,this));
        $('body').on('click','#tblstudents tbody td', $.proxy(this.focus,this));
        $('body').on('keydown','#tblstudents tbody td', $.proxy(this.captureKey,this));
        $('body').on('change','#type, #term', $.proxy(this.term_changed,this));
        $('body').on('click','.profile-usermenu a', $.proxy(this.help,this));
    },

    mypaste: function(e){

         var r = $(e.target).attr('data-row');
         var c = $(e.target).attr('data-col');

         var pasteData = e.originalEvent.clipboardData.getData('text')
         var rows = pasteData.split("\n");

         var a = 0;
         var b = 0;


         for(var y = 0 ; y <= rows.length ; ++y){
            if(rows[y] != undefined){
                var cells = rows[y].split("\t");
                for(var x = 0 ; x <= cells.length ; ++x){
                    if( cells[x] != undefined) {

                        a = parseInt(r)+ parseInt(y);
                        b = parseInt(c)+ parseInt(x);

                        var v = ME.validGrade(cells[x])
                        if(v.valid){
                            var $c = $('#tblstudents tbody tr').eq(a).find('td').eq(b);
                            if($c.hasClass('editable')){
                                $c.html(cells[x]);
                                ME.compute($c.parent());
                                ME.saveConduct(a,b);
                            }
                        }
                    }
                }
            }
         }
         $('#tbleditor').hide();
    },

    captureKey: function(e){

        var presskey = "";
        var r = $(e.target).parent().index(), c = $(e.target).index();

        //console.log(e.keyCode);

        switch(e.keyCode){

            case 37: //left

                var nc = parseInt(c)-1;
                var $nextrow = $('#tblstudents tbody tr').eq(r).find('td').eq(nc);
                this.focus($nextrow);
                return false;
            break;

            case 38: //up
                var nr = parseInt(r)-1;
                var nc = parseInt(c)-1;
                var $nextrow = $('#tblstudents tbody tr').eq(nr).find('td').eq(c);
                this.focus($nextrow);
                return false;
            break;

            case 39: //right

                var nc = parseInt(c)+1;
                var $nextrow = $('#tblstudents tbody tr').eq(r).find('td').eq(nc);
                this.focus($nextrow);
                return false;
            break;

            case 40: //down

                var nr = parseInt(r)+1;
                var nc = parseInt(c)+1;
                var $nextrow = $('#tblstudents tbody tr').eq(nr).find('td').eq(c);
                this.focus($nextrow);
                return false;

            break;

            case 46:

                if($(e.target).hasClass('editable')){
                    $(e.target).html('');
                }
                return false;
            break;

            case 17: //ctrl
                return false;
            break;

            case 13: //Enter

            break;
            default :
                var charCode = (e.which) ? e.which : e.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                }
                presskey = String.fromCharCode(e.keyCode);

            break;
        }

        var top  = window.pageYOffset || document.documentElement.scrollTop;
        if($(e.target).hasClass('editable')){

            var $editor = $('#tbleditor');
            var $editorText = $('#tbleditor').find('input');

            var add_width = -10;
            var data = $(this).data();
            var currval = "";

            currval = $(e.target).html();

           if(presskey != '') currval = presskey;
           var pos =  $(e.target).offset();
           var h = parseInt( $(e.target).height()-1  ) ;
           var w = parseInt( $(e.target).width()+add_width ) ;

           $editorText.attr('data-col',c);
           $editorText.attr('data-row',r);

           $editorText
                .height(h)
                .width(w)
                .val('');

           $editor
                .css({'position': 'fixed' , 'top' : pos.top - top +"px",'left' : pos.left +"px",})
                .show();

           if( currval != '' && currval != undefined ) $editorText.val(currval.trim() );

           $editorText
                .focus()
                .select(false);

        } else {
            this.hideactiveText();
        }
        e.preventDefault();

    },

    editorChange: function(e){

        var $editor = $('#tbleditor');
        var $editorText = $('#tbleditor').find('input');

        var col = $editorText.attr('data-col');
        var row = $editorText.attr('data-row');
        var $r =  $('#tblstudents tbody tr').eq(row).find('td').eq(col);
        var id = $r.attr('data-id');
        var cls = $r.attr('data-class');

        var grade = strCast(e.target.value, 'float');

        var result= ME.validGrade(grade);
        if ( !result.valid){
            msgbox('warning','Ooops, Invalid grade');
            return false;
        }

        $r.html(e.target.value );

        ME.compute($r.parent());
        ME.saveConduct(row,col);

        var ud = parseInt(row)+1;
        var $nextrow = $('#tblstudents tbody tr').eq(ud).find('td').eq(col);
        this.focus($nextrow);
        this.hideactiveText();
        //e.preventDefault();
    },

    editorFocusUp: function(e){ e.target.select();},
    editorMouseUp: function(e){ return false; },
    hideactiveText:function(e){
        $('#tbleditor').hide();
        //e.preventDefault();
    },

    focus : function(e){

        $('#tblstudents tbody td').removeClass('cell-focus');
        if(e.target){
            $(e.target).addClass('cell-focus');
            $(e.target).focus();
            var $r = $(e.target).closest('tr');
        }else{
            $(e).addClass('cell-focus');
            $(e).focus();
            var $r = $(e).closest('tr');
        }



        $('#tblstudents tbody tr').removeClass('active-row');
        $r.addClass('active-row');

        $('.profile-usertitle-name').html($r.attr('data-name'));
        $('.profile-usertitle-job').html($r.attr('data-idno'));
        $('.img-responsive').attr('src', $r.find('td.pic').find('img').attr('src'));

    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'refresh':  this.initstudents();  break;
            case 'post': this.post(e); break;
        }
    },

    term_changed : function(e){

        var selected = 0;

        if(e ==undefined){
            selected = this.$term.val();
        }else{
            selected = $(e.target).val() ;
        }

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select an academic year & term','No AY Term');
            return false;
        }

        this.initloads();

    },

    initloads : function(){

        var term = $('#term').find('option:selected').text()
        //var x = term.trim().indexOf('School');
        var tid = $('#term').val();

        var data = { event : 'schedules', term : tid };
        var success = function(e){
            $('#sched').html(e.content);
            progressBar('off');
        };
        setAjaxRequest(this.mylink+"event",data,success,ME.err,'json',true);

    },

     help: function(e){

        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');

        $('#summary-container').hide();
        $('#gradingsystem-container').hide();
        $('#studentlist-container').hide();

        var data = $(e.target).data();
        switch (data.menu) {
            case 'conduct': $('#studentlist-container').show(); break;
            case 'summary': $('#summary-container').show(); break;
            case 'help': $('#gradingsystem-container').show(); break;
        }
    },

    init : function() {
        this.mylink = base_url+page_url;
        this.$term = $('#term');
        this.$section = $('#sched');
        this.$period = $('#period');
        this.initplugins();
        this.initservices();
        this.initloads();
    },
};