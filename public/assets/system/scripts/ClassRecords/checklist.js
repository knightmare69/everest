function scrollme(e){
    var x = e.scrollLeft;
    document.getElementById('mytblheader').scrollLeft =x;
    var y = e.scrollTop;
    if(document.getElementById('domaincols')){
        document.getElementById('domaincols').scrollTop =y;
    }
}

var ME = {
    init : function() {
        this.mylink = base_url+page_url;
        this.IsChanged = false;
        this.$term = $('#term');
        this.$period = $('#period');
        this.$tbl = $('#tblbody');
        this.$editor = $('#tbleditor');
        this.$editorText = this.$editor.find('.input-sm');
        this.initplugins();
        this.initservices();
        ME.init_table();
    },

    init_table : function(){

        var h = $('.myheader thead').height();
        $('#student_header_r').height(h);

        $('.myheader thead th').each(function(){
           //console.log($(this));
           var i =  $(this).index(),w = $(this).width();
           $('#tblbody tbody tr').eq(2).find('td').eq(i).find('div').width(w);
        });

        $('#tbldomain tbody tr').each(function(){
           var i =  $(this).index();
           var h = $(this).find('td').eq(0).find('div').height();
        });
    },

    initplugins : function(){
        $('.bs-select').selectpicker({ iconBase: 'fa', tickIcon: 'fa-check', size : 8 });
    },

    initservices: function() {
        $('body').find(this.$term).on('change',$.proxy(this.term_changed, this));
        $('body').find(this.$period).on('change',$.proxy(this.period_changed, this));
        
        $('body').on('click','.btn, .reload, .clink', $.proxy(this.menu,this));
        $('body').on('dblclick','#tblbody tbody td', $.proxy(this.activeText, this));
        $('body').on('keydown','#tblbody tbody td', $.proxy(this.activeText, this));
        $('body').on('click','#tblbody tbody td', function(){ ME.cell_focus($(this)); });
        this.$editorText.on('blur',$.proxy(this.hideactiveText,this));
        this.$editorText.on('focus',$.proxy(this.editorFocusUp,this));
        this.$editorText.on('mouseup',$.proxy(this.editorMouseUp,this));
        //this.$editorText.on('change',$.proxy(this.editorChange,this));
        this.$editorText.on('paste',$.proxy(this.mypaste,this));
        this.$editorText.on('keypress',function(e){

            if( e.keyCode === 27) {
                var c = $(e.target).attr('data-col'), r =  $(e.target).attr('data-row');
                var $nr = $('#tblbody tbody tr').eq(r).find('td').eq(c);
                //ME.compute(r,c);
                ME.hideactiveText();
                ME.cell_focus($nr);
            }else if (e.keyCode == 13 ){

                if(!ME.IsChanged){
                    ME.editorChange(e);
                }
                ME.IsChanged=false;
            }

        });
        $('body').on('dblclick','#tblbody tbody td', $.proxy(this.activeText, this));

    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'post': this.post(); break;
            case 'refresh':  this.term_changed();  break;
            case 'checklist': this.checklist(); break;
            case 'grading': $('#grading_modal').modal('show'); break;
            case 'print': this.print(); break;
        }
    },
    
   print : function(){    
      var t = getParameterByName('term'), s = getParameterByName('open'), p = getParameterByName('period');      
      window.open(ME.mylink+'print?sched='+s+'&period='+ p +'&term='+t ,'_blank');            
   },

   post : function(e){

      var _checked = $('#tblbody').find('.chk-child:checked');

      var validatepost = function($r){
            var flg = true;

            $r.find('td.xgrade').each(function(){
                var item = $(this).attr('data-items');
                var score = 0;

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if ( item > 0 ){
                    if(score == '' ){
                        flg = false ;
                    }
                }
            });

            return flg;
      };


      var execution = function(){
         var ids = [];
            _checked.each(function(e){
                var _tr = $(this).closest('tr') ;
                var _d = _tr.data();
                var id = _tr.attr('data-idno');

                if(id !== 0 || id !== undefined){
                    if (validatepost(_tr)) {
                        var x = {
                            idno : id,
                            reg : _d.reg,
                            yr : _d.yr,
                            subj : _d.subject,
                            grade : _tr.find('td.InitialGrade').find("div").html().trim(),
                            fgrade : _tr.find('td.TransmutedGrade').find("div").html().trim(),
                            remarks : _tr.find('td.Description').find("div").html().trim(),
                        };
                        ids.push(x);
                    }
                }
            });

            if(ids.length >= 1){

                var data = {
                    event : 'post',
                    term : ME.$term.val(),
                    sched : ME.$schedule.val(),
                    subj : ME.$schedule.find('option:selected').attr('data-subj'),
                    period : ME.$period.val(),
                    stud : ids,
                }

                setAjaxRequest(
                    base_url + page_url + 'thisRequest', data,
                    function(r) {
                        if (r.error) {
                            msgbox('error','Sorry, unable to post grades');
                        } else {
                            //showSuccess(result.message);
                            ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                            ME.refresh();
                        }
                    }
                );
                progressBar('off');
            } else{
                msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
            }
        } ;

        if (_checked.length > 0 ) {

          $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
    			content : "Posting student record will activate the class record lock features.. <br/> You are no longer able to create, edit and delete an event. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     $.SmartMessageBox({
            			title : "<span class='text-warning'><i class='fa fa-lock'></i> Do you wish to POST the selected student(s)?</span>",
            			content : "Do you certify that all the grades per student in this class record are true and correct?",
            			buttons : '[No][Yes]'
            		}, function(ButtonPressed) {
            			if (ButtonPressed === "Yes") {
            			     execution(true);
            			}
            		});
    			}else{
    			     $(this).remove();
    			}
    		});
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },



    cell_focus : function(e){
        $('#tblbody tbody td').removeClass('cell-focus');
        $(e).addClass('cell-focus');
        $(e).focus();
        /*
        var $r = $(e).closest('tr');
        $('.profile-usertitle09--0-name').html($r.attr('data-name'));
        $('.profile-usertitle-job').html($r.attr('data-student'));
        $('.img-responsive').attr('src', $r.find('td.img').find('img').attr('src'));
        */
    },

    err : function(){
        msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
        progressBar('off');
    },

    validate : function(grade){
       var v = { valid : false, msg : 'invalid conduct grade', letter : '', desc : '', remarks : '', };

        $('#tblgrading tbody tr').each(function(){
            var min =  strCast( $(this).find('td.min').html().trim() , 'float') ;
            var max =  strCast( $(this).find('td.max').html().trim() , 'float') ;
            if( grade >= min && grade <= max ){
                v = {
                    valid : true,
                    msg : 'grade accepted',
                    letter : $(this).find('td.letter').html().trim(),
                    desc : $(this).find('td.desc').html().trim(),
                    remarks : $(this).find('td.remarks').html().trim(),
                };
                return v;
            }

        });

        return v;

    },

    save_grade : function(row,col){

        var $r = $('#tblbody tbody tr').eq(row);
        var $td = $r.find('td').eq(col);
        var grade = $r.find('td').eq(col).find("div").html();

        var data = { event : 'save-grade',

            sec : $('#section').val(),
            period : this.$period.val(),
            regid : $td.attr('data-reg'),
            idno : $td.attr('data-idno'),
            chkid : $r.attr('data-idx'),
            score : grade,

        };

        var mysuccess = function(e){
            if(!e.error){

            }

        };

        if (grade != ''){
            setAjaxRequest(this.mylink+"event",data,mysuccess,ME.error,'json',true,false);
        }

    },

    erase_grade : function(row,col){

        var $r = $('#tblbody tbody tr').eq(row);

        var data = {
            event : 'erase-grade',
            chkid : $r.attr('data-idx'),
            sec : $('#section').val(),
            period : this.$period.val(),
            idno : $r.find('td').eq(col).attr('data-idno'),
            name : $r.find('td').eq(col).attr('data-name'),
        };

        var mysuccess = function(e){
            if(!e.error){

            }
        };
        setAjaxRequest(this.mylink+"event",data,mysuccess,ME.error,'json',true,false);

    },


    editorFocusUp: function(e){ e.target.select();},
    editorMouseUp: function(e){ return false; },
    hideactiveText:function(e){ this.$editor.hide(); },
    editorChange: function(e){


        ME.IsChanged = true;

        var col = this.$editorText.attr('data-col');
        var row = this.$editorText.attr('data-row');
        var $r =  $('#tblbody tbody tr').eq(row).find('td').eq(col);
        var id = $r.attr('data-id');
        var cls = $r.attr('data-class');

        var result= ME.validate(e.target.value);


        if ( !result.valid){
            msgbox('warning','Ooops, Invalid grade');
            return false;
        }

        $r.find('div').html(e.target.value);

        //ME.compute('total',row,col);
        ME.save_grade(row,col);

        var ud = parseInt(row)+1;
        var $nextrow = $('#tblbody tbody tr').eq(ud).find('td').eq(col);
        this.cell_focus($nextrow);
        this.hideactiveText();
        e.preventDefault();
    },

     activeText: function(e){

        var presskey = "";
        var ro = $(e.target).parent().index();
        var co = $(e.target).index();

        if(e.type == "keydown"){

            var gotonextcell = function(row,col){
               var $nextrow =  $('#tblbody tbody tr').eq(row).find('td').eq(col);
               ME.cell_focus($nextrow);
               return false;
            };

            if(e.keyCode === 37 || e.keyCode === 39 ){
                var lr = parseInt(co + (e.keyCode === 37 ? -1 : 1));
                return gotonextcell(ro,lr);
            }

            if(e.keyCode === 38 || e.keyCode === 40 ){
                var ud = parseInt(ro + (e.keyCode === 38 ? -1 : 1));
                return gotonextcell(ud,co);
            }

            if( e.keyCode === 46){
                if($(e.target).hasClass('editable')){
                    $(e.target).find("div").html('');
                    ME.erase_grade(ro,co);

                }
                return false;
            }

            if(e.keyCode == 27) { return false; }
        }

        var top  = window.pageYOffset || document.documentElement.scrollTop;
        if($(e.target).hasClass('editable')){

            var add_width = -10;
            var data = $(this).data();
            var currval = "";

            currval = $(e.target).find('div').html().trim();

            if(presskey != '') currval = presskey;
            var pos =  $(e.target).offset();
            var h = parseInt( $(e.target).height()-1  ) ;
            var w = parseInt( $(e.target).width()+add_width ) ;
           this.$editor.css({'position': 'fixed' , 'top' : pos.top - top +"px",'left' : pos.left +"px",});
           this.$editorText.val('');
           this.$editorText.height(h);
           this.$editorText.width(w);
           this.$editor.show();
           this.$editorText.attr('data-col',co);
           this.$editorText.attr('data-row',ro);
           if( currval != '' && currval != undefined ) this.$editorText.val(currval.trim() );
           this.$editorText.focus();
           this.$editorText.select(false);

        } else {
            this.hideactiveText();
        }

        if(e.keyCode == 13){
            e.preventDefault();
        }
    },

    checklist: function(){



    },

    initlist: function(){
        var t = ME.$term.val();
        var p = ME.$period.val();

        if(t == undefined || t == '-1'){
            msgbox('warning','Please select an academic year & term','No AY Term');
            return false;
        }

        if(p == undefined || p == '-1'){
            msgbox('warning','Please select period/quarter','No Period selected');
            return false;
        }

        var data = { event : 'schedules', term : t, period : p };
        var success = function(e){
            $('#myschedules').html(e.content);
            progressBar('off');
        }

        var err = function(){
            msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
            progressBar('off');
        }

        $('#myschedules').html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        setAjaxRequest(this.mylink+"event",data,success,err,'json',true);
    },

    term_changed : function(e){
        ME.initlist();
    },
    
    period_changed : function(e){
        ME.initlist();
    },
    

};