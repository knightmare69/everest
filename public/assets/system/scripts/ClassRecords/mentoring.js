var MOD = {
    init: function(){
        var t = localStorage.TermID;
        var term = getParameterByName('t');
        if(t != undefined && term == undefined){ location.replace(base_url+page_url+'?t='+ t); }
        this.load();
        this.reload();
    },
    add : function(){ this.loadStudents(); },
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    load : function(){

      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      $('body').on('click','.views', $.proxy(this.help,this));
      $('body').on('change', '#term', function(e){
        localStorage.TermID =  $('#term').val();
        location.replace(base_url+page_url+'?t='+ $('#term').val());
      });
      $('body').on('keypress','#searchkey', function(e){
        if(e.keyCode ==  13){ MOD.searchOnEnter(); }
      });

      $('body').on('mousedown','#tblgrades tbody td', function(e){
         MOD.closemenu();
         if(e.button == 2){
              MOD.initrclick(e);
         }
      });
    },

    help: function(e){

        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');
        
        var data = $(e.target).data();
        this.init_tab(data.menu);
                
    },
    
    init_tab: function(mo){
        
        
        $('.tabpane').hide();
        
        switch(mo){
            case 'sheet': $('#mentorsheet').show(); break;
            case 'formation': $('#formation').show(); break;
            case 'history': $('#history').show(); break;
        }
    },
    
    reload : function(e){
        var m = $('.profile-usermenu').find('li.active').find('a').attr('data-menu');
        console.log(m);
        this.init_tab(m);

    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'add': this.add(e); break;
            case 'edit': this.edit(e); break;
            case 'select': this.selectThis(e); break;
            case 'remove': this.deleteThis(e); break;
            case 'save-session': this.save_sess(e); break;
            case 'formation': this.saveThis(e); break;
            case 'print': this.print(e); break;
            case 'reload': this.reload(e); break;
            case 'cancel-menu': this.closemenu(); break;
            case 'print-eval': this.printEval(e); break;

        }
    },

   

   
    edit : function(e){
        var id =  $(e.target).closest('tr').attr('data-idno');
        var idx =  $(e.target).closest('tr').attr('data-id');
       if( id == undefined){
            msgbox('warning','No Selected student');
            return false;
       }
       location.replace(base_url+page_url+'/edit?t='+ $('#term').val()+'&idno='+ id + '&ref='+ idx );
    },

    selectThis : function(){

       var id =  $('#table-students-res').find('tr.active-row').attr('data-id');
       if( id == undefined){
            msgbox('warning','No Selected student');
            return false;
       }
       location.replace(base_url+page_url+'/new?t='+ $('#term').val()+'&idno='+ id );
    },

    searchOnEnter: function(){

       var find = $('#searchkey').val();

       setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students', 'term': $('#academic-term').val(), 'find' : find }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#event_modal .scrollbox').html(r.data);
            }
        },undefined,'json',true,false);
        progressBar('off');

    },

    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                var d = {
                    ref : $(this).attr('data-id'),
                    idno : $(this).attr('data-idno'),
                    prog : $(this).find('.level').find('option:selected').attr('data-prog'),
                    major : $(this).find('.track').find('option:selected').val(),
                    level : $(this).find('.level').val(),
                };
                ids.push(d);
           }
        });
        return ids;
    },

    saveThis: function(){

       var subj = [];
       var section = '';


        $.SmartMessageBox({
        	title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Student Formation Plan? </h2>",
        	content : "Do you want to save student formation plan? Click [YES] to proceed...",
        	buttons : '[No][Yes]'
        }, function(ButtonPressed) {
        	if (ButtonPressed === "Yes"){
           	     var data = $('#formationForm').serialize();
                setAjaxRequest( base_url + page_url + '/event?event=save-formation' ,data
        			   ,function(r)
        				{
        				  if(r.error)
        				  {
        			        msgbox('danger',r.error);
        			      }else{
        			         msgbox('success',r.message);
        			      }
        				}
                       ,function(err)
                        {
        				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
        				});
                progressBar('off');
        
         
        	}
        });
    },

    loadStudents : function(){
      var s = getParameterByName('s');
      var p = $('#period').val();

        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students', 'term': $('#academic-term').val() }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
                //sysfnc.showAlert('#grdSchedules','danger', r.message,true,true,true,'warning');
            }else{
                $('#event_modal .scrollbox').html(r.data);
                $('#event_modal').modal('show');
            }
        },undefined,'json');
        progressBar('off');
    },

    deleteThis : function(e){

        var _this = this;
        var selecteds = _this.getSelected();

        if (selecteds.length > 0 ) {
          $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove selected student/s? </h2>",
    			content : "Do you want to remove the selected student/s? Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    setAjaxRequest( base_url + page_url + '/event?event=remove' , { idno : selecteds } , function(r) {
                    if(r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    }
                    progressBar('off');
					$('#term').trigger('change');
                    });
    			}
    		});
        }else{
            alert('Please select student to delete');
        }
    },

   	save_sess: function(e){

		var termid = $('#term').val();

        var data = $('#dataForm').serialize();
        setAjaxRequest( base_url + page_url + '/event?event=save-session' ,data
			   ,function(r)
				{
				  if(r.error)
				  {
			        msgbox('danger',r.error);
			      }else{
			         msgbox('success',r.message);
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
				});
        progressBar('off');

	},

    print : function(){
        var id = $('#btnprint').attr('data-reg');

        if(id == 0 || id == undefined){
            alert('no registration found!');
            return false;
        }
        window.open(base_url+'students/enrollment/print?reg='+$('#btnprint').attr('data-reg')+'&t='+ Math.random() ,'_blank');
	 },
    initrclick: function(e){
        var pos = $(e.target).position();
        var cs = {top: pos.top, left: pos.left}

        $( "#mouse_menu_right" ).css(cs);
        $('#mouse_menu_right').show('50');
    },

   
     printEval : function(){
        window.open(base_url+'enrollment/advising/printEval?idno='+ getParameterByName('idno') +'&t='+ Math.random() ,'_blank');
	 },

};

$('document').ready(function(){
  $('body').on('click','[data-menu="refresh"]',function(){
	window.location.reload();
  });
  
});
