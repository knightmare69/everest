"use strict";

var ClassReview = {
    ajax_stack: [],
    empl_stack: [],
    empl_selected: '',
    sched_id: 0,
    is_shs: 1,
    year_level: '',
    program: 0,
    load_more_faculties: function() {
        var term_id = $('#academic-year').val(),
            _find = $('#faculty-find').val(),
            _this = this;

        setAjaxRequest(
            base_url + page_url + 'event', {
                'event': 'load-faculties',
                't': term_id,
                'f': _find,
                'e': this.empl_stack
            },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    if (result.total >= 1) {
                        $('#faculty-list').append(result.list);
                        _this.load_photos();
                    } else {
                        $('#faculty-list-get').parent().addClass('hide');
                        msgbox('error', 'No result(s) found.');
                    }
                }
            }
        )
        progressBar('off');
    },

    find_faculties: function() {
        var term_id = $('#academic-year').val(),
            _find = $('#faculty-find').val(),
            _this = this;

        if (this.ajax_stack.length >= 1) {

            for (var i = 0; i < this.ajax_stack.length; i++) {
                this.ajax_stack[i].abort();
            }

            this.ajax_stack = [];
            this.empl_stack = [];
        }

        setAjaxRequest(
            base_url + page_url + 'event', {
                'event': 'find-faculties',
                't': term_id,
                'f': _find
            },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    $('#faculty-list').empty().html(result.list);
                    var load_more = $('#faculty-list-get').parent();

                    if (result.total >= 1) {
                        load_more.removeClass('hide');

                        _this.load_photos();

                    } else {
                        load_more.addClass('hide');
                    }

                }
            }
        );
        progressBar('off');
    },

    load_photos: function() {
        var stack = this.ajax_stack,
            empl = this.empl_stack;

        $('#faculty-list .item-details a').each(function() {
            var e = $(this).attr('data-id'),
                img = $(this).prev();

            empl.push(e);

            stack.push(
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: base_url + page_url + 'event',
                    dataType: 'html',
                    data: {
                        'event': 'faculty-photo',
                        'e': e
                    },
                    type: 'POST',
                    async: true,
                    beforeSend: function() {},
                    success: function(res) {
                        img.attr('src', res);
                    },
                })
            )

        });
    },

    get_sections_with_subject: function(empl_id) {
        var term_id = $('#academic-year').val(),
            _this = this;
        this.empl_selected = empl_id;

        setAjaxRequest(
            base_url + page_url + 'event', {
                'event': 'get-faculty-sections-subjects',
                't': term_id,
                'e': empl_id
            },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    var info = $('a.faculty-names[data-id="' + empl_id + '"]'),
                        photo = info.prev('img').attr('src'),
                        name = info.text();

                    $('#faculty-selected-name').text(': ' + name);
                    $('#selected-subj-title').text('');
                    $('#myschedules').empty();
                    $('#post-grade').addClass('hide');

                    $('#sections').select2('destroy').html(result.list).select2({
                        placeholder: 'Please select section.'
                    });

                    $('#faculty-class-record').removeClass('hide');
                }
            }
        );
        progressBar('off');
    },

    get_class_records: function(sched_id) {
        var period = $('#period').val(),
            term = $('#academic-year').val(),
            _this = this,
            exclude_ibed_cr = ['Grade 1', 'Grade 7','Grade 9','Grade 10', 'Grade 11', 'Grade 12'],
            url = '', params = {};

        if((_this.year_level == 'Grade 11') || (_this.year_level == 'Grade 12') || (_this.program == 7) ){
            _this.is_shs = 1;
        } else {
            _this.is_shs = 0;
        }
            url = 'class-record/eclass-records/thisRequest';
            params = {
                'event': 'students',
                'sched_id': sched_id,
                'period': period,
                'shs': _this.is_shs,  
        }

        setAjaxRequest(
            base_url + url, params,
            function(result2) {
                if (result2.error) {
                    msgbox('error', result2.message);

                } else {
                    $('#post-grade').removeClass('hide');
                    $('#myschedules').empty().html(result2.content);

                    if(result2.ibed_table != undefined){
                        $('#myschedules table').DataTable().destroy();
                        $('#myschedules table').DataTable();
                    }
                }
            }
        );
        progressBar('off');
    },

    post: function(e){

      var _checked = $('#tblgrade').find('.chk-child:checked'), _this = this;

      var validatepost = function($r){

            var flg = true;

            $r.find('td.eventScore').each(function(){
                var item = $(this).attr('data-items');
                var score = 0;

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if ( item > 0 ){
                    if(score == '' ){
                        flg = false ;
                    }
                }
            });

            $r.find('td.conductScore').each(function(){
                var score = 0;

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                }
            });

            return flg;
      };


      var execution = function(){
         var ids = [];
                _checked.each(function(e){
                    var _tr = $(this).closest('tr') ;
                    var _d = _tr.data();
                    var id = _tr.attr('data-idno');

                    if(id !== 0 || id !== undefined){
                        if (validatepost(_tr)) {
                            var x = {
                                idno : id,
                                reg : _d.reg,
                                yr : _d.yr,
                                subj : _d.subject,
                                grade : _tr.find('td.InitialGrade').find("div").html().trim(),
                                fgrade : _tr.find('td.TransmutedGrade').find("div").html().trim(),
                                remarks : _tr.find('td.Description').find("div").html().trim(),
                                cona : _tr.find('td.conductScore[data-conduct="1"]').find("div").html(),
                                conb : _tr.find('td.conductScore[data-conduct="2"]').find("div").html(),
                                conc : _tr.find('td.conductScore[data-conduct="3"]').find("div").html(),
                                cond : _tr.find('td.conductScore[data-conduct="4"]').find("div").html(),
                            };

                            ids.push(x);
                        }
                    }
                });

                if(ids.length >= 1){

                    var term_id = $('#academic-year').val(),
                        section_id = $('#sections').val(),
                        subject_id = $('#sections option:selected').attr('data-subj'),
                        // subject_id = $('#subjects').val(),
                        period = $('#period').val();

                    var data = {
                        event : 'post',
                        term : term_id,
                        sched : _this.sched_id,
                        subj : subject_id,
                        period : period,
                        stud : ids,
                    }

                    setAjaxRequest(
                        base_url + 'class-record/eclass-records/thisRequest', data,
                        function(r) {
                            if (r.error) {
                                msgbox('error','Sorry, unable to post grades');
                            } else {
                                //showSuccess(result.message);
                                // ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                                Metronic.alert({
                                    container: '#show-message'  , // alerts parent container(by default placed after the page breadcrumbs)
                                    place: 'prepend', // append or prepent in container
                                    type: 'success',  // alert's type
                                    message: r.message,  // alert's message
                                    close: true, // make alert closable
                                    reset: true, // close all previouse alerts first
                                    focus: true, // auto scroll to the alert after shown
                                    closeInSeconds: 10, // auto close after defined seconds
                                    icon: 'check' // put icon before the message
                                });
                                _this.get_class_records(_this.sched_id);
                            }
                        }
                    );
                    progressBar('off');
                } else{
                    msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
                }

        } ;


        if (_checked.length > 0 ) {

          $.SmartMessageBox({
                title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
                content : "Posting student record will activate the class record lock features.. <br/> You are no longer able to create, edit and delete an event. Click [YES] to proceed...",
                buttons : '[No][Yes]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "Yes") {
                     $.SmartMessageBox({
                        title : "<span class='text-warning'><i class='fa fa-lock'></i> Do you wish to POST the selected student(s)?</span>",
                        content : "Do you certify that all the grades per student in this class record are true and correct?",
                        buttons : '[No][Yes]'
                    }, function(ButtonPressed) {
                        if (ButtonPressed === "Yes") {
                             execution(true);
                        }
                    });
                }else{
                     $(this).remove();
                }
            });
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },
}

function scrollme(e){
    var x = e.scrollLeft;
    document.getElementById('mytblheader').scrollLeft =x;
    var y = e.scrollTop;
    if(document.getElementById('studentcols')){
        document.getElementById('studentcols').scrollTop =y;
    }

}

$(document).ready(function() {

    // To overwrite default setup and skip init object on ME
    ME.mylink = base_url + 'class-record/eclass-records/';
    ME.$term = $('#academic-year');
    ME.$period = $('#period');
    ME.$schedule = $('#sections');
    ME.$editor = $('#tbleditor');
    ME.$editorText = $('#tbleditor .input-sm');
    ME.addlistener();
    // End

    $('body').on('click', '#show-sy-only', function(e) {
        var show_sy_only = false,
            at = $('#academic-year');

        if ($(this).is(':checked')) {
            at.children().each(function() {
                var _find = $(this).text().indexOf('School Year');

                if (_find === -1) {
                    $(this).addClass('hide');
                }
            });
        } else {
            at.children().removeClass('hide');
        }
    });

    $('#academic-year').select2({
        placeholder: 'Please select a Term.'

    }).on('change', function(e) {
        $('#faculty-class-record').addClass('hide');
        ClassReview.find_faculties();
        $('#faculty-find, #faculty-list').closest('div.row').removeClass('hide');

        var test_sy = $(this).select2('data').text, hs_opt = $('.mgs'), sh_opt = $('.shs'), summer_opt = $('.summer'), period = $('#period');

        if(test_sy.indexOf('Summer') == 12) {
            hs_opt.addClass('hide');
            sh_opt.addClass('hide');
            summer_opt.removeClass('hide');

        } else if(test_sy.indexOf('School Year') === -1){
            hs_opt.addClass('hide');
            summer_opt.addClass('hide');
            sh_opt.removeClass('hide');
            // ClassReview.is_shs = 1;
        } else {
            hs_opt.removeClass('hide');
            sh_opt.addClass('hide');
            summer_opt.addClass('hide');
            // ClassReview.is_shs = 0;
        }

        period.attr('disabled', false).select2('val', period.find('option:not(.hide)').val());

    });

    $('#period').select2({
        placeholder: 'Please select a period.'
    }).on('change', function(e) {
        var sched_id = $('#sections').val();

        if (Boolean(sched_id)) {
            ClassReview.get_class_records(sched_id.trim());
        }
    });

    $('body').on('click', '#faculty-find-btn', function(e) {
        ClassReview.find_faculties();
    });

    $('body').on('click', '#faculty-list-get', function() {
        ClassReview.load_more_faculties();
    });

    $('body').on('click', '.item-details .item-name.primary-link', function(e) {
        var emp = $(this).attr('data-id');
        $('li.item').removeClass('active');
        $(this).closest('li').addClass('active');
        $("html, body").animate({ scrollTop: 0 }, 500);
        ClassReview.get_sections_with_subject(emp);
    });

    // sections
    $('#sections').select2().on('change', function(e) {
        var sel_data = $(this).select2('data').element[0],
            _title = sel_data.attributes['data-subj-title'].value,
            _this = ClassReview,
            subj_title = $('#selected-subj-title');

        _this.sched_id = $(this).val();
        _this.year_level = sel_data.attributes['data-year-level'].value;
        _this.program = sel_data.attributes['data-prog'].value;

        subj_title.text(_title);
        _this.get_class_records($(this).val());
    });
    // End

    // Notes
    // $(document).click(function(e){
    //     // console.log(e);
    //     if(e.target.offsetParent == null || e.target.offsetParent.id !== 'div-note-msg'){
    //         $('#div-note-msg').addClass('hide');
    //     }
    // });
    //
    // $('body').on('click', '.eventScore.editable', function(e){
    //     var note_div = $('#div-note-msg'), pos_t = $(this).offset().top, pos_l = $(this).offset().left;
    //
    //     note_div.removeClass('hide').css({'top' : (pos_t - (note_div.outerHeight() + (note_div.outerHeight() / 2) - 20 )), 'left': pos_l - (note_div.outerHeight() / 2) });
    //     e.stopPropagation();
    // });
    //
    // $('body').on('click', '#note-btn-close', function(e){
    //     $('#div-note-msg').addClass('hide');
    // });
    // End

    // Posting
    $('body').on('click', '#post-grade', function(e){
        ClassReview.post(e)
    });

    $('body').on('click', '.chk-header', function(e){
        var flg = $(e.target).prop('checked');
        $('#tblgrade tbody').find('.chk-child').prop('checked', flg);
        $('#studentlist tbody').find('.chk-child').prop('checked',flg);
    });
    // End

    $('body').on('click', '#btn-refresh', function(e){
        ClassReview.get_class_records(ClassReview.sched_id);
    });
});
