var ME = {
    init : function() {        
        this.mylink = base_url+page_url;
        this.$term = $('#term');
        this.initplugins();
        this.initservices();        
    },
    
    initplugins : function(){
        $('.bs-select').selectpicker({ iconBase: 'fa', tickIcon: 'fa-check', size : 8 });
    },
    
    initservices: function() {
        $('body').find(this.$term).on('change',$.proxy(this.term_changed, this));
        $('body').on('click','.btn, .reload, .clink', $.proxy(this.menu,this));
    },
    
    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'refresh':  this.term_changed();  break;
        }
    },
    term_changed : function(e){

        var selected = 0;

        if(e ==undefined){
            selected = this.$term.val();
        }else{
            selected = $(e.target).val() ;
        }

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select an academic year & term','No AY Term');
            return false;
        }

        var data = { event : 'schedules', term : selected, };
        var success = function(e){
            $('#myschedules').html(e.content);
            progressBar('off');
        }

        var err = function(){
            msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
            progressBar('off');
        }

        $('#myschedules').html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        setAjaxRequest(this.mylink+"event",data,success,err,'json',true);
    },
    
    
};