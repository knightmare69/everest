var ME = {

    err : function(){
        msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
        progressBar('off');
    },

    initAlerts : function(container, alert, message, closable, reset, focus,icon, time ) {

        Metronic.alert({
            container: container  , // alerts parent container(by default placed after the page breadcrumbs)
            place: 'prepend', // append or prepent in container
            type: alert,  // alert's type
            message: message,  // alert's message
            close: closable, // make alert closable
            reset: reset, // close all previouse alerts first
            focus: focus, // auto scroll to the alert after shown
            closeInSeconds: time, // auto close after defined seconds
            icon: icon // put icon before the message
        });
    },


    post : function(e){

      var _checked = $('#tblstudents').find('.chk-child:checked');

      var validatepost = function($r){

            var flg = true;

            $r.find('td.clubScore').each(function(){

                score = $(this).html();
                
                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                    return false;
                }
            });

            return flg;
      };


      var execution = function(){
            
            var ids = [];
            
            _checked.each(function(e){
                var _tr = $(this).closest('tr') ;
                var _d = _tr.data();
                var id = _tr.attr('data-idno');
                var reg = _tr.attr('data-reg');
                var sched = _tr.attr('data-sched');
                var grade = _tr.find('td.final').html();     
                
                if(id !== 0 || id !== undefined){
                    if (validatepost(_tr)) {
                        var x = { idno : id, reg : reg, sched : sched, grade : grade  };
                        ids.push(x);
                    }
                }
            });

                if(ids.length >= 1){

                    var data = {
                        event : 'post',
                        term : ME.$term.val(),                        
                        club : ME.$club.val(),                    
                        period : ME.$period.val(),
                        stud : ids,
                    }

                    setAjaxRequest(
                        base_url + page_url + 'event', data,
                        function(r) {
                            if (r.error) {
                                msgbox('error','Sorry, unable to post conduct');
                            } else {
                                //showSuccess(result.message);
                                ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                                ME.initstudents();
                            }
                        }
                    );
                    progressBar('off');
                } else{
                    msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
                }
                
        } ; 
      
    
        if (_checked.length > 0 ) {
            
          $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning! Do you wish to POST the selected student(s) </span>",
    			content : "You will no longer edit/modify the grade of posted student(s) <br/> Are you sure you want to post selected student(s)?. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     $.SmartMessageBox({
            			title : "<span class='text-warning'><i class='fa fa-lock'></i> Confirmation?</span>",
            			content : "Do you certify that all the final grade per student in this class record are verified and correct?",
            			buttons : '[No][Yes]'
            		}, function(ButtonPressed) {
            			if (ButtonPressed === "Yes") {
            			     execution(true);
            			}
            		});
    			}else{
    			     $(this).remove();
    			}
    		});
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },
    
    saveClub : function(row,col){

        var $r = $('#tblstudents tbody tr').eq(row).find('td').eq(col);
        
        var data = {
            event : 'save-grade',
            term : $('#term').val(),            
            club : $('#club').val(),
            grade : $r.parent().find('td.final').html(),            
            period : $('#period').val(),
            idno : $r.parent().attr('data-idno'),
            name : $r.parent().attr('data-name'),
            score : $r.html(),
            reg : $r.parent().attr('data-reg'),
            sched : $r.parent().attr('data-sched'),
            idx : $r.attr('data-event'),
        };
    
        var mysuccess = function(e){
            if(!e.error){
                //console.log('conduct grade successfully saved!');
    
            }
        };
        setAjaxRequest(ME.mylink+"event",data,mysuccess,ME.error,'json',true,false);

    },
             
    
    compute : function($row){
        
        
        $row.find('td.clubScore').each(function(){            
            var score = strCast( $(this).html(),'float' );
            var eq = score * $(this).attr('data-percent')                        
            if(eq > 0){                             
                $row.find('td.percent[data-group='+ $(this).attr('data-group')  +']').html( eq.toFixed(2) );    
            }else{
                $row.find('td.percent[data-group='+ $(this).attr('data-group')  +']').html("0");
            }             
        });                
        
        var grade = 0;
        $row.find('td.percent').each(function(){            
            grade += strCast( $(this).html(),'float' );     
        }); 
                  
        var v = ME.validGrade(grade);
        if(v.valid){
            $row.find('td.final').html(grade.toFixed(2));
            $row.find('td.letter').html(v.letter);
            $row.find('td.remarks').html(v.desc);            
        }
        
        
    },
    
    validGrade : function(grade){
                        
        var v = { valid : false, msg : 'invalid conduct grade', letter : '', desc : '', remarks : '', };
        $('#tblgradesystem tbody tr').each(function(){
            var min =  strCast( $(this).find('td.min').html().trim() , 'float') ;
            var max =  strCast( $(this).find('td.max').html().trim() , 'float') ;
            if( grade >= min && grade <= max ){
                v = {
                    valid : true,
                    msg : 'conduct grade accepted',
                    letter : $(this).find('td.letter').html().trim(),
                    desc : $(this).find('td.desc').html().trim(),
                    remarks : $(this).find('td.remarks').html().trim(),
                };
                return v;
            }

        });

        return v;

    },
            
    initstudents: function(e){

        var s = $('#club').val(), p = $('#period').val(), t = $('#term').val(), y = $('#type').val();
        
        var data = { event : 'students', term : t,club : s, period : p, type: y };
        
        var success = function(e){
            $('#studentlist-container').html(e.content);
            progressBar('off');
        }
        
        if(s == '' || s == '-1' ){ 
            this.initloads();
            alert('Please select a club');
            return false; 
        }
        
        if(p == '' || p == '-1' ){ 
            alert('Please select a period');
            return false; 
        }
        
        if(t == '' || t == '-1'){ 
            alert('Please select a academic year and term');
            return false; 
        }
        
        $('#studentlist-container').html("<h4> <i class='fa fa-spin fa-spinner'></i> <i class='font-sm'>Please wait while loading...</i>  </h4>");
        setAjaxRequest(this.mylink+"event",data,success,ME.err,'json',true);
    },

    initplugins : function(){
        $('.bs-select').selectpicker({ iconBase: 'fa', tickIcon: 'fa-check', size : 8 });
    },

    initservices: function() {
        
        var $editor = $('#tbleditor');
        var $editorText = $('#tbleditor').find('input');
            
        $editorText.on('blur',$.proxy(this.hideactiveText,this));
        $editorText.on('focus',$.proxy(this.editorFocusUp,this));
        $editorText.on('mouseup',$.proxy(this.editorMouseUp,this));
        $editorText.on('change', function(e){
         ME.editorChange(e);
         ME.IsChanged=false;
        });
        $editorText.on('paste',$.proxy(this.mypaste,this));
        
        $editorText.on('keypress',function(e){
            
            if( e.keyCode === 27) {                  
                var c = $(e.target).attr('data-col'), r =  $(e.target).attr('data-row');                                                
                var $nr = $('#tblstudents tbody tr').eq(r).find('td').eq(c);
                ME.compute(r,c);
                ME.hideactiveText();
                ME.cell_focus($nr);                
            }else if (e.keyCode == 13 ){
                if(!ME.IsChanged){
                    ME.editorChange(e);                     
                }
                ME.IsChanged=false;
            }                                                             
        });
        
        $('body').on('click','.btn, .reload, .clink', $.proxy(this.menu,this));
        $('body').on('change','#club, #period', $.proxy(this.initstudents,this));
        $('body').on('click','#tblstudents tbody td', $.proxy(this.focus,this));
        $('body').on('keydown','#tblstudents tbody td', $.proxy(this.captureKey,this));
        $('body').on('change','#term', $.proxy(this.term_changed,this));
        $('body').on('click','.profile-usermenu a', $.proxy(this.help,this));
    },
    
    mypaste: function(e){
         
         var r = $(e.target).attr('data-row');
         var c = $(e.target).attr('data-col');
         
         var pasteData = e.originalEvent.clipboardData.getData('text')        
         var rows = pasteData.split("\n");
         
         var a = 0;
         var b = 0;
         
         
         for(var y = 0 ; y <= rows.length ; ++y){
            if(rows[y] != undefined){                                            
                var cells = rows[y].split("\t");
                for(var x = 0 ; x <= cells.length ; ++x){
                    if( cells[x] != undefined) { 
                        
                        a = parseInt(r)+ parseInt(y);
                        b = parseInt(c)+ parseInt(x);
                        
                        var v = ME.validGrade(cells[x])                        
                        if(v.valid){                            
                            var $c = $('#tblstudents tbody tr').eq(a).find('td').eq(b);                             
                            if($c.hasClass('editable')){
                                $c.html(cells[x]);                                
                                ME.compute($c.parent());
                                ME.saveClub(a,b);                                
                            }                                                                
                        }                        
                    }                            
                }            
            }                        
         }                                    
         $('#tbleditor').hide();       
    },
    
    captureKey: function(e){
        
        var presskey = "";
        var r = $(e.target).parent().index(), c = $(e.target).index();                                            
        
        //console.log(e.keyCode);
        
        switch(e.keyCode){
            
            case 37: //left
                
                var nc = parseInt(c)-1;                
                var $nextrow = $('#tblstudents tbody tr').eq(r).find('td').eq(nc);
                this.focus($nextrow);
                return false;
            break;
            
            case 38: //up
                var nr = parseInt(r)-1;
                var nc = parseInt(c)-1;                
                var $nextrow = $('#tblstudents tbody tr').eq(nr).find('td').eq(c);
                this.focus($nextrow);
                return false;
            break;
            
            case 39: //right
                
                var nc = parseInt(c)+1;                
                var $nextrow = $('#tblstudents tbody tr').eq(r).find('td').eq(nc);
                this.focus($nextrow);
                return false;
            break;
            
            case 40: //down
                            
                var nr = parseInt(r)+1;
                var nc = parseInt(c)+1;                                
                var $nextrow = $('#tblstudents tbody tr').eq(nr).find('td').eq(c);                
                this.focus($nextrow);
                return false;
                                
            break;
            
            case 46: //delete
            
                if($(e.target).hasClass('editable')){
                    $(e.target).html('');
                    ME.erase_grade(r,c);
                }
                return false;
            break;
            
            case 17: //ctrl                                        
                return false;
            break;
            
            case 13: //Enter
                
            break;
            default :
                var charCode = (e.which) ? e.which : e.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                }
                presskey = String.fromCharCode(e.keyCode);
                
            break;
        }
        
        var top  = window.pageYOffset || document.documentElement.scrollTop;
        if($(e.target).hasClass('editable')){
            
            var $editor = $('#tbleditor');
            var $editorText = $('#tbleditor').find('input');
    
            var add_width = -10;
            var data = $(this).data();
            var currval = "";

            currval = $(e.target).html();
                        
           if(presskey != '') currval = presskey;
           var pos =  $(e.target).offset();
           var h = parseInt( $(e.target).height()-1  ) ;
           var w = parseInt( $(e.target).width()+add_width ) ;
                      
           $editorText.attr('data-col',c);
           $editorText.attr('data-row',r);
           
           $editorText
                .height(h)                
                .width(w)
                .val('');
           
           $editor
                .css({'position': 'fixed' , 'top' : pos.top - top +"px",'left' : pos.left +"px",})
                .show();
                
           if( currval != '' && currval != undefined ) $editorText.val(currval.trim() );
           
           $editorText
                .focus()
                .select(false);

        } else {
            this.hideactiveText();
        }
        e.preventDefault();
        
    },
    
    editorChange: function(e){
        
        if( ME.IsChanged ) return false;
        ME.IsChanged = true;
        
        var $editor = $('#tbleditor');
        var $editorText = $('#tbleditor').find('input');
                
        var col = $editorText.attr('data-col');
        var row = $editorText.attr('data-row');
        var $r =  $('#tblstudents tbody tr').eq(row).find('td').eq(col);
        var id = $r.attr('data-id');
        var cls = $r.attr('data-class');
        
        var grade = strCast(e.target.value, 'float');                
                        
        if (isNaN(grade) || grade > 100 ){
            msgbox('warning','Ooops, Invalid club grade');
            ME.IsChanged=false;            
            return false;
        }        
                
        $r.html( $.trim(grade));   
        
        ME.compute($r.parent());        
        ME.saveClub(row,col);
                
        var ud = parseInt(row)+1;
        var $nextrow = $('#tblstudents tbody tr').eq(ud).find('td').eq(col);            
        this.focus($nextrow);
        this.hideactiveText();
        //e.preventDefault();
    },
    
    editorFocusUp: function(e){ e.target.select();},
    editorMouseUp: function(e){ return false; },
    hideactiveText:function(e){             
        $('#tbleditor').hide();
        //e.preventDefault();  
    },

    focus : function(e){        
        
        $('#tblstudents tbody td').removeClass('cell-focus');
        if(e.target){
            $(e.target).addClass('cell-focus');
            $(e.target).focus();    
            var $r = $(e.target).closest('tr');
        }else{
            $(e).addClass('cell-focus');
            $(e).focus();
            var $r = $(e).closest('tr');
        }
                                
        $('#tblstudents tbody tr').removeClass('active-row');
        $r.addClass('active-row');
        
        $('.profile-usertitle-name').html($r.attr('data-name'));
        $('.profile-usertitle-job').html($r.find('td.studno').html());
        $('.img-responsive').attr('src', $r.find('td.pic').find('img').attr('src'));
        
    },
    
    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'refresh':  this.initstudents();  break;
            case 'post': this.post(e); break;          
            case 'print': this.print(e); break;  
        }
    },
    
    print: function(){
        var t = $('#term').val(), c = $('#club').val(), p = $('#period').val();        
        window.open(ME.mylink+'print?term='+t+'&period='+ p + '&club='+ c  ,'_blank');
            
    },
        
    term_changed : function(e){

        var selected = 0;

        if(e ==undefined){
            selected = this.$term.val();
        }else{
            selected = $(e.target).val() ;
        }

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select an academic year & term','No AY Term');
            return false;
        }
                
        this.initloads();
                
    },
    
    initloads : function(){
        
        var term = $('#term').find('option:selected').text()
        var x = term.trim().indexOf('School');
        var t = $('#type').val();
        var tid = $('#term').val();
        
        var data = { event : 'schedules', term : tid , type: t,  period : x };
        var success = function(e){
            $('#club').html(e.content);
            $('#period').html(e.period);
            progressBar('off');
        }        
        setAjaxRequest(this.mylink+"event",data,success,ME.err,'json',true);
        
    },
    
     help: function(e){
        
        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');
        
        $('#summary-container').hide();
        $('#gradingsystem-container').hide(); 
        $('#studentlist-container').hide();
                
        var data = $(e.target).data();
        switch (data.menu) {
            case 'conduct': $('#studentlist-container').show(); break;
            case 'summary': $('#summary-container').show(); break;
            case 'help': $('#gradingsystem-container').show(); break;
        }        
    },
    
    erase_grade : function(row,col){

        var $r = $('#tblstudents tbody tr').eq(row);

        var data = {
            event : 'erase-grade',
            activity : $r.find('td').eq(col).attr('data-event'),
            idno : $r.attr('data-idno'),
            name : $r.attr('data-name'),
        };

        var mysuccess = function(e){
            if(!e.error){
                ME.compute($r);
            }
        };
        setAjaxRequest(this.mylink+"event",data,mysuccess,ME.error,'json',true,false);

    },
    
    initremarks : function(){
        $('#tblstudents tbody tr').each(function(){
           var g = $.trim($(this).find('td.final').html());
           
           if(g != '0' && g != '' && g != undefined ){                                    
                var v = ME.validGrade(g);
                
                if(v.valid){
                    //$(this).find('td.final').html(grade.toFixed(2));
                    $(this).find('td.letter').html(v.letter);
                    $(this).find('td.remarks').html(v.desc);            
                }                    
           }                                  
        });                    
    },
    
    init : function() {
        this.IsChanged = false;
        this.mylink = base_url+page_url;
        this.$term = $('#term');
        this.$club = $('#club');
        this.$period = $('#period');
        this.initplugins();
        this.initservices();
        this.initloads();        
    },
};