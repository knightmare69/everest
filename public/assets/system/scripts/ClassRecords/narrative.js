var ayterm;
var icount = 0;
var islock = 0;
$('document').ready(function(){
   ayterm = $('#term').val();   
   $('body').on('click','.btn-save',function(){save_narrative();});
   $('body').on('click','.btn-delete',function(){remove_narrative();});
   $('body').on('click','.btn-post,.btn-upost',function(){
	   var option = (($(this).is('.btn-post'))?1:0);
       option_narrative(option);
   });
   $('body').on('click','[data-idno]',function(){stud_profile($(this));});
   $('body').on('click','[data-menu="refresh"]',function(){load_students();});
   $('body').on('change','#section,#period',function(){load_students();});
   $('body').on('click','[data-menu]',function(){
	   var menu = $(this).attr('data-menu');
	   $(this).closest('.nav').find('.active').removeClass('active');
	   $('#mytblheader').find('.students').addClass('hidden');
	   $('#mytblheader').find('.gradesystem').addClass('hidden');
       switch(menu){
		   case 'class-record':
		     $(this).closest('li').addClass('active');
		     $('#mytblheader').find('.students').removeClass('hidden');
		   break;
		   case 'gradesys':
		     $(this).closest('li').addClass('active');
		     $('#mytblheader').find('.gradesystem').removeClass('hidden');
		   break;
	   }	   
   });
   
   $('body').on('change','.conduct',function(){
	   var tmpval = $(this).val();
	   var checki = false;
	   if(isNaN(tmpval)==false){
		   if(tmpval==''){return false;}
		   $('.gradesystem').find('[data-template]').each(function(){
			   var xmin = $(this).attr('data-min');
			   var xmax = $(this).attr('data-max');
			   if(xmin<=tmpval && xmax>=tmpval){
				   checki=true;
			   }
		   });
		   if(checki==false){
			 alert('Invalid Input'); 
		     $(this).val(''); 
		   }
	   }else{
		  alert('Invalid Input'); 
		  $(this).val(''); 
	   }
   });	
   
   $('body').on('change','[type="text"],textarea',function(){
	   $(this).addClass('modified');
   });	   
   
   $('body').on('change','#section',function(){
	   var selected = $(this).val();
	   var schedid  = $(this).find('[value="'+selected+'"]').attr('data-schedule');
	   $(this).attr('data-sched',schedid);
   });
   
   enable_term();
   enable_btn();
});

function stud_profile(tr){
	var pic   = $(tr).find('img').attr('src');
	var stdno = $(tr).attr('data-student');
	var name  = $(tr).attr('data-name');
	$('.profile-userpic').find('img').attr('src',pic);
	$('.profile-usertitle-name').html(name);
	$('.profile-usertitle-job').html(stdno);
}

function enable_term(){
	$('#section').find('[data-term!="'+ayterm+'"]').addClass('hidden');	   
    if(ayterm!==undefined && ayterm!='-1'){
	   $('#section').val(-1);
       $('#section').find('[data-term="'+ayterm+'"]').removeClass('hidden');	   
	}
	$('#period').val(-1);
}

function enable_btn(){
	icount = $('#tblnarrative').find('tr[data-idno]').length;
	islock = $('#tblnarrative').find('.fa-lock').length;
	$('.btn-save').addClass('hidden');	  
	$('.btn-delete').addClass('hidden');	  
	$('.btn-post').addClass('hidden');	  
	//$('.btn-upost').addClass('hidden');	  
	if(icount>0){
	  	if(islock==icount){
	      //$('.btn-upost').removeClass('hidden');		
		}else if(islock<=0){
		  $('.btn-save').removeClass('hidden');	  
	      $('.btn-delete').removeClass('hidden');	  
	      $('.btn-post').removeClass('hidden');	  
		}
	}
}

function load_students(){
    var section = $('#section').val();
    var period  = $('#period').val();
	if(ayterm==undefined || ayterm==-1 || section==-1 || period==-1){return false;}
	progressBar('on');
    setAjaxRequest(
		base_url+'class-record/enarrative/thisRequest'
		,{event:'list',term:ayterm,section:section,period:period}
		,function(r){
		  if(r.success){
			$('#myschedules').html(r.content);
			$('.tooltips').tooltip();
			enable_btn();
		  }
		  progressBar('off');
		 }
		,function(err)
		 {
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		 }
	); 
}

function save_narrative(){
    var section = $('#section').val();
	var sched   = $('#section').attr('data-sched'); 
    var period  = $('#period').val();
	var studno  = '';
	var narrate = '';
	var scount   = $('#tblnarrative').find('[data-idno]').length;
	if(ayterm==undefined || ayterm==-1 || section==-1 || period==-1){return false;}
	progressBar('on');
    
	$('#tblnarrative').find('[data-idno]').each(function(){
	   if($(this).find('[data-editable]').length>0){
		   var i      = 1;
		   studno     = $(this).attr('data-student');
		   conduct    = [];
           narrative  = [];
		   
           for(i=1;i<=$(this).find('.conduct').length;i++){
			 var indx = (i-1); 
			 conduct[indx] = $(this).find('#conduct'+i).val();  
			 conduct[indx] = ((conduct[indx]==undefined || conduct[indx]=='')?$(this).find('#conduct'+i).html():conduct[indx]);   
		   }
		   
		   narra = $(this).find('#narrativea').val();
		   narrb = $(this).find('#narrativeb').val();
	       narrative['0'] = ((narra==undefined || narra=='')?$(this).find('#narrativea').html():narra);
	       narrative['1'] = ((narrb==undefined || narrb=='')?$(this).find('#narrativeb').html():narrb);
		   
		   setAjaxRequest(
			  base_url+'class-record/enarrative/thisRequest'
			 ,{event:'save',term:ayterm,section:section,period:period,studno:studno,conduct:conduct,narrate:narrative}
			 ,function(r)
			  {
			   scount--;
			   if(scount<=0){
				 $('.modified').removeClass('modified');  
				 confirmEvent('<i class="fa fa-check text-success"></i> Save Successfully!',function(){return;});	
				 progressBar('off');   
			   }
			  }
			 ,function(err)
			  {
			   confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Save Data!',function(){return;});	
			   progressBar('off');
			   return false;
			  }
		   );    
	   }else{
		   progressBar('off');
	   }
	});
}

function option_narrative(opt){
    var section = $('#section').val();
    var period  = $('#period').val();
	var studno  = '';
	var narrate = '';
	var scount   = $('#tblnarrative').find('[data-idno]').length;
	if(ayterm==undefined || ayterm==-1 || section==-1 || period==-1){return false;}
	if($('.modified').length>0){
	  confirmEvent('<i class="fa fa-warning text-danger"></i> Save your changes first!',function(){return;});		
	  return false;	
	}
	progressBar('on');
    
	$('#tblnarrative').find('[data-idno]').each(function(){
		   studno    = $(this).attr('data-student');	
		   setAjaxRequest(
			  base_url+'class-record/enarrative/thisRequest'
			 ,{event:'padlock',term:ayterm,section:section,period:period,studno:studno,option:opt}
			 ,function(r)
			  {
			   scount--;
			   if(scount<=0){
				 load_students();
				 progressBar('off');   
			   }
			  }
			 ,function(err)
			  {
			   confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Save Data!',function(){return;});	
			   progressBar('off');
			   return false;
			  }
		   );  
	});
	progressBar('off');   
}

function remove_narrative(){
    var section = $('#section').val();
    var period  = $('#period').val();
	var studno  = '';
	var narrate = '';
	var scount   = $('#tblnarrative').find('[data-idno]').length;
	if(ayterm==undefined || ayterm==-1 || section==-1 || period==-1){return false;}
	progressBar('on');
    $('#tblnarrative').find('[data-idno]').each(function(){
	   progressBar('on');
       if($(this).find('#narrative').length>0){
		   trow      = $(this);
		   studno    = $(this).attr('data-student');	
		   ischeck   = $(this).find('.chk-child').is(':checked');
		   if(!ischeck){
			   progressBar('off');
               return;
		   }
		   setAjaxRequest(
			  base_url+'class-record/enarrative/thisRequest'
			 ,{event:'remove',term:ayterm,section:section,period:period,studno:studno}
			 ,function(r)
			  {
			   $('[data-student="'+studno+'"]').find('#narrative').html('');
			   $('[data-student="'+studno+'"]').find('.chk-child').prop('checked',false);
			   scount--;
			   if(scount<=0){
				 load_students();
				 progressBar('off');   
			   }
			  }
			 ,function(err)
			  {
			   confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Save Data!',function(){return;});	
			   progressBar('off');
			   return false;
			  }
		   );    
	   }
	});
	
}