var 
    isEdit = false,
    StudentNo = '',
    TermID = '',
    Term = '',
    SectionID = '',
    Section = '',
    AttendanceKey = '';

var  Me = function(){

	var handleActionButtons = function() {
		jQuery('#BtnFilter').click(function() {
			var result = ajaxRequest(base_url+page_url+'event','event=showFilter');

			if (result.error == true) {
				msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
			}

			showModal({
                name: 'basic',
                title : 'Filter',
                content: result.content,
                class: 'modal_filter',
                button: {
                	class: 'btnSearch',
                	caption: 'Search'
                }
            });
            modifyModalBtnClass('btnSearch');
		});

        jQuery('#BtnCreate').click(function() {
            var self = jQuery(this);

            if (!StudentNo) {
                return msgbox('error','Please select student first.');
            }

            var result = ajaxRequest(base_url+page_url+'event','event=showAttendanceModal');

            if (result.error == true) {
                msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
            }

            showModal({
                name: 'basic',
                title : 'Attendance',
                content: result.content,
                button: {
                    class: 'btnSaveAttendance',
                    caption: 'Save'
                }
            });
            
            isEdit = false;
            FN.datePicker();
            FN.datePickerTimeNoSeconds();
            clearSelectedAttendance();
            modifyModalBtnClass('btnSaveAttendance');
			$('.tbattend').removeClass('hidden');
			$('.tboffense').addClass('hidden');
			$('#AttendanceTabs').find('[data-type="1"]').trigger('click');
        });
		
        jQuery('#BtnOffense').click(function() {
            var self = jQuery(this);

            if (!StudentNo) {
                return msgbox('error','Please select student first.');
            }

            var result = ajaxRequest(base_url+page_url+'event','event=showAttendanceModal');

            if (result.error == true) {
                msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
            }

            showModal({
                name: 'basic',
                title : 'Discipline',
                content: result.content,
                button: {
                    class: 'btnSaveAttendance',
                    caption: 'Save'
                }
            });
            
			
            isEdit = false;
            FN.datePicker();
            FN.datePickerTimeNoSeconds();
            clearSelectedAttendance();
            modifyModalBtnClass('btnSaveAttendance');
			$('.tbattend').addClass('hidden');
			$('.tboffense').removeClass('hidden');
			$('#AttendanceTabs').find('[data-type="5"]').trigger('click');
        });
	}

	var handleFilterEvents = function() {

		//handle search
		jQuery('body').on('click','.btnSearch',function() {

			var self = jQuery(this);

			if (!isFormValid('#FormFilter')) {
				return msgbox('error','Please complete all fields');
			}

			onProcess('',self,'Searching...');
			setAjaxRequest(
				base_url+page_url+'event',
				'event=loadData&'+jQuery('body #FormFilter').serialize(),
				function(result) {
					if (result.error == false) {
						jQuery('#TableWrapper').html(result.content);
						jQuery(':checkbox').uniform();
                        setActiveFilter();
                        showActiveFilter();
                        closeModal('.modal_filter');

					} else {
						msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
					}
					onProcess('',self,'...',false);
					progressBar('off');
				},
				function(error) {
					onProcess('',self,'...',false);
					progressBar('off');
				}
			);

			
            modifyModalBtnClass('btnSearch');
		});
        
		jQuery('body').on('change','#Section',function() {
		    var select = $(this).val();
		    var yrlvl  = $(this).find('[value="'+select+'"]').attr('data-year');
			if($('#YearLevel').closest('.form-group').is('.hidden')){
			$('#YearLevel').val(yrlvl);
			$('#Section').val(select);
			}
		});
		
        jQuery('body').on('change','.adviser_section',function() {

            var self = jQuery(this);
            var adviser_section = jQuery('#adviser_section').val();
                SectionID       = jQuery('#adviser_section').val();


            // onProcess('',self,'Searching...');
            setAjaxRequest( base_url + page_url + 'event', {'event': 'loadData', 'Section': adviser_section },
                function(result) {
                    if (result.error  == false) {
                        jQuery('#TableWrapper').html(result.content);
                        jQuery(':checkbox').uniform();
                        setActiveFilter();
                        showActiveFilter();
                        closeModal('.modal_filter');
                    } else {
                        msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
                    }
                    onProcess('',self,'...',false);
                    progressBar('off');
                },
                function(error) {
                    onProcess('',self,'...',false);
                    progressBar('off');
                }
                                );
            modifyModalBtnClass('btnSearch');
        });

		//handle year level load sections
		jQuery('body').on('change','.filter',function() {

			var self = jQuery(this);

            if (!isFormValid('#FormFilter',false)) return;

			setAjaxRequest(
				base_url+page_url+'event',
				'event=loadSections&'+jQuery('body #FormFilter').serialize()+'&ProgCode='+jQuery('#YearLevel option:selected').attr('data-code'),
				function(result) {
					if (result.error == false) {
						setSections(result.data);
					} else {
						msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
					}
					progressBar('off');
				},
				function(error) {
					progressBar('off');
				},
				'json',true,false
			);

            modifyModalBtnClass('btnSearch');

            function setSections(data) {
            	var option = document.createElement('option');
            	var options = "<option value=''>- Select - </option>";
            	for(var i in data) {
            		option = document.createElement('option');
            		option.setAttribute('value',data[i].SecID)
            		option.setAttribute('data-year',data[i].YearLevelID)
            		option.appendChild(document.createTextNode(data[i].Section));

            		options += option.outerHTML;
            	}

            	jQuery('#Section').html(options);
            }

		});

        //handle attaendance saving
        jQuery('body').on('click','.btnSaveAttendance', function() {
            var self = jQuery(this);
            if (!isFormValid(getCurrentActiveForm())) {
                return msgbox('error','Please complete all required fields.');
            }

            confirmEvent("Are you sure you want to save this?", function(yes) {
                if (!yes) return;
                onProcess('',self,'Saving...');
                setAjaxRequest(
                    base_url+page_url+'event',
                    'event=save&isEdit='+isEdit+'&type='+getType()+'&StudentNo='+StudentNo+'&AttendanceKey='+AttendanceKey+'&'+jQuery(getCurrentActiveForm()).serialize()+'&TermID='+TermID+'&SectionID='+SectionID,
                    function(result){
                        onProcess('',self,'Save',false);
                        if (!isEdit) {
                            clear_value(getCurrentActiveForm());
                        }
                        closeModal('.modal_filter');
                        showProfile();
                        progressBar('off');
                    },
                    function() {
                        progressBar('off');
                        onProcess('',self,'Save',false);
                        msgbox("error","There was an error occurd");
                    }
                );
            });
        });
    
        //handle  delete attaendance
        jQuery('#BtnDelete').click(function() {
            var self = jQuery(this);
            if (!AttendanceKey) {
                return msgbox('error','Please select attendance.');
            }

            confirmEvent("Are you sure you want to delete this?", function(yes) {
                if (!yes) return;
                onProcess('',self,'Deleting...');
                setAjaxRequest(
                    base_url+page_url+'event',
                    'event=delete&AttendanceKey='+AttendanceKey,
                    function(result){
                        onProcess('',self,'Delete',false);
                        AttendanceKey = '';
                        showProfile();
                        progressBar('off');
                    },
                    function() {
                        progressBar('off');
                        onProcess('',self,'Delete',false);
                        msgbox("error","There was an error occurd");
                    }
                );
            });
        });

        jQuery('#BtnReset').click(function() {
            if (!StudentNo) {
               return resetForm();
            }
            confirmEvent("Are you sure you want to reset this?", function(yes) {
                if (!yes) return;
               resetForm();
            });
        });

	}

	var handleSelections = function() {
		//handle row selection
		jQuery('body').on('click','.rowSel', function() {
			var self = $(this), isChecked = false;

			jQuery('table tbody tr td .chk-child').prop('checked',false);

			if (self.hasClass('row-selected')) {
				self.removeClass('row-selected');
                clearProfile();
			} else {
				jQuery('.rowSel').removeClass('row-selected');
				self.addClass('row-selected');
				StudentNo = self.attr('data-studentno');
                isChecked = true;
                showProfile();
			}

			self.find('.chk-child').prop('checked',isChecked);
			jQuery(':checkbox').uniform();
		});

        //handle row selection double click
        jQuery('body').on('dblclick','.rowSel', function() {
            var self = $(this), isChecked = false;

            jQuery('table tbody tr td .chk-child').prop('checked',false);

            if (self.hasClass('row-selected')) {
                self.removeClass('row-selected');
                clearProfile();
            } else {
                jQuery('.rowSel').removeClass('row-selected');
                self.addClass('row-selected');
                StudentNo = self.attr('data-studentno');
                isChecked = true;
                showProfile();
            }

            self.find('.chk-child').prop('checked',isChecked);
            jQuery(':checkbox').uniform();
        });

		//handle attendance  double selection
		jQuery('body').on('dblclick','.attendanceSel', function() {
			var self = $(this);

            AttendanceKey = self.closest('tr').attr('data-attendancekey');

			var result = ajaxRequest(base_url+page_url+'event','event=showAttendanceModal&AttendanceKey='+AttendanceKey);

			if (result.error == true) {
				msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
			}

			showModal({
                name: 'basic',
                title : 'Discipline',
                content: result.content,
                class: 'modal_filter',
                button: {
                	class: 'btnSaveAttendance',
                	caption: 'Save'
                }
            });

            isEdit = true;
            FN.datePicker();
            FN.datePickerTimeNoSeconds();
            setActiveAttendanceTab(self);
            disableNotActiveTab();
            self.addClass('row-selected');
            modifyModalBtnClass('btnSaveAttendance');
            self.closest('table').find('.rowSel').removeClass('row-selected');
			$('#AttendanceTabs').find('[data-type="5"]').trigger('click');
			switch(AttendanceKey){
			  case '5':
			   $('.modal-title').html('Discipline');
			   $('.tbattend').addClass('hidden');
			   $('.tboffense').removeClass('hidden');
			   $('#AttendanceTabs').find('[data-type="5"]').trigger('click');
              break;			 
              default:
			   $('.tbattend').removeClass('hidden');
			   $('.tboffense').addClass('hidden');
			   $('#AttendanceTabs').find('[data-type="1"]').trigger('click');
              break;			  
			}
		});

        //handle attendance  double selection
        jQuery('body').on('click','.attendanceSel', function() {
            var self = $(this);
            AttendanceKey = self.closest('tr').attr('data-attendancekey');
            if (self.hasClass('row-selected')) {
                self.removeClass('row-selected');
            } else {
                self.closest('table').find('tr').removeClass('row-selected');
                self.addClass('row-selected');
            }
        });		
	}

	function modifyModalBtnClass(myClass) {
        var classes = ['btnSearch','btnSaveAttendance'];
        for(var i = 0;i<=classes.length;i++) {
            jQuery('.modal .btn_modal').removeClass(classes[i]);
        }
        jQuery('.modal .btn_modal').addClass(myClass);
    }

    function setActiveAttendanceTab(self) {
        jQuery('#AttendanceTabs ul li').each(function() {
            var aSelf = jQuery(this);
            if (aSelf.find('a').attr('data-type') == getAttendanceType(self)) {
                aSelf.find('a').trigger('click');
            }
        });
    }

    function disableNotActiveTab() {
        jQuery('#AttendanceTabs ul li').each(function() {
            var self = jQuery(this);
            if (!self.hasClass('active')) {
                self.find('a').attr('data-toggle','');
                self.find('a').addClass('hidden');
            }
        });
    }

    function ifNull(data) {
    	if (data == null || !data) {
    		return '';
    	}
    	return data;
    }

    function showActiveFilter() {
        jQuery('#SchoolYearTitle u').html(Term);
        jQuery('#SectionTitle u').html(Section);
    }

    function clearActiveFilter() {
        jQuery('#SchoolYearTitle u').html('School Year');
        jQuery('#SectionTitle u').html('Section');
    }

    function resetForm() {
        clearProfile();
        clearStudentsList();
        clearActiveFilter();
        clearSelectedProfile();
        clearSelectedAttendance();
    }

    function setActiveFilter() {
        if($('#adviser_section').length==0){
         Term = jQuery('#SchoolYear option:selected').text();
         TermID = jQuery('#SchoolYear option:selected').val();

         Section = jQuery('#Section option:selected').text();
         SectionID = jQuery('#Section option:selected').val();
        }else{
            TermID = jQuery('#adviser_section').find('option:selected').attr('data-term');
            Term = jQuery('#adviser_section').find('option:selected').attr('data-ay');
            SectionID = jQuery('#adviser_section option:selected').val();
            Section   = jQuery('#adviser_section option:selected').text();
        }
    }

    function getAttendanceType(self) {
        return self.closest('table').attr('data-type')
    }

    function getType() {
    	return jQuery('#AttendanceTabs ul li.active a').attr('data-type');
    }

    function getCurrentActiveForm() {
        return '#'+jQuery('#AttendanceTabs .tab-content .active form').attr('id');
    }

    function clearSelectedProfile() {
        jQuery('#TableMasterList tbody tr').removeClass('row-selected');
        jQuery('#TableMasterList tbody tr td .chk-child').prop('checked',false);
        jQuery(':checkbox').uniform();
    }

    function clearSelectedAttendance() {
        jQuery('#TableAttendanceWrapper tbody tr').removeClass('row-selected');
    }

    function clearStudentsList() {
        jQuery('#TableMasterList tfoot').html('<tr><td colspan="6" class="center bold">No Record found.</td></tr>');
    }

    function clearProfile() {
        var row = '';
        //parse student abseses
        row = '<tr class="sm-font no-record">'+
                    '<td colspan="7">No Record</td>'+
                '</tr>';
       
        jQuery('#TableAbsenses tbody').html(row);

        //parse student offenses
        row = '<tr class="sm-font no-record">'+
                    '<td colspan="5">No Record</td>'+
                '</tr>';
        
        jQuery('#TableOffenses tbody').html(row);

        //parse student merits
        row = '<tr class="sm-font no-record">'+
                    '<td colspan="4">No Record</td>'+
                '</tr>';
        
        jQuery('#TableMerits tbody').html(row);

        //parse student lost and found
        row = '<tr class="sm-font no-record">'+
                    '<td colspan="4">No Record</td>'+
                '</tr>';
        
        jQuery('#TableLostFound tbody').html(row);

        //clear info
        jQuery('#Name').text('');
        jQuery('#StudentNo').text('');
        jQuery('#Gender').text('');
        jQuery('#BirthDate').text('');

        isEdit = false;
        StudentNo = '';
        AttendanceKey = '';
        
        jQuery('#StudentPhoto').attr('src',base_url+'general/getStudentPhotoByStudentNo?StudentNo=');
    }

    function showProfile() {
    	setAjaxRequest(
    		base_url+page_url+'event',
    		'event=showProfileData&StudentNo='+StudentNo+'&SchoolYear='+TermID+'&Section='+SectionID,
    		function(result) {

    			var 
    				Profile = result.data.Profile,
    				Absenses =result.data.Absenses,
    				Offenses =result.data.Offenses,
    				Merits = result.data.Merits,
    				LostFound = result.data.LostFound,
                    Tardiness = result.data.Tardiness,
    				row = '';

    			//set up student info
    			jQuery('#Name').text(Profile.Name);
    			jQuery('#StudentNo').text(Profile.StudentNo);
    			jQuery('#Gender').text(Profile.Gender);
    			jQuery('#BirthDate').text(Profile.DateofBirth);

    			//show student photo
    			jQuery('#StudentPhoto').attr('src',base_url+'general/getStudentPhotoByStudentNo?StudentNo='+Profile.StudentNo);

    			
    			//parse student abseses
    			row = '<tr class="sm-font no-record">'+
							'<td colspan="7">No Record</td>'+
						'</tr>';

    			for(var i = 0;i<Absenses.length; i++) {                    
    				if (i <= 0) row = '';
    				row += "<tr class='cursor-pointer attendanceSel' data-attendancekey='"+Absenses[i].AttendanceKey+"'>"+
    							"<td class='sm-font'>"+Absenses[i].PeriodID+"</td>"+
    							"<td class='sm-font'>"+ifNull(Absenses[i].Date)+"</td>"+
    							"<td class='sm-font'>"+ifNull(Absenses[i].DateEnd)+"</td>"+
    							"<td class='sm-font'>"+Absenses[i].Days+"</td>"+
    							"<td class='sm-font'>"+(Absenses[i].IsExcused == '1' ? 'Yes' : 'No')+"</td>"+
    							"<td class='sm-font'>"+Absenses[i].Remarks+"</td>"+
    						"</tr>";
    			}

    			jQuery('#TableAbsenses tbody').html(row);

    			//parse student offenses
    			row = '<tr class="sm-font no-record">'+
							'<td colspan="5">No Record</td>'+
						'</tr>';
    			isfaculty = $('#adviser_section').length;
    			for(var i = 0;i<Offenses.length; i++) {
    				if (i <= 0) row = '';
					var xoption = Offenses[i].Remarks;
					if(isfaculty==0 && xoption==''){
					   xoption = '<a href="javascript:void(0);">Double-Click Here<a>';
					}else{
					     
					}
					
    				row += "<tr class='cursor-pointer attendanceSel' data-attendancekey='"+Offenses[i].AttendanceKey+"'>"+
    							"<td class='sm-font'>"+ifNull(Offenses[i].Date)+"</td>"+
                                "<td class='sm-font'>"+Offenses[i].Particulars+"</td>"+
    							"<td class='sm-font'>"+xoption+"</td>"+
    						"</tr>";
    			}

    			jQuery('#TableOffenses tbody').html(row);

    			//parse student merits
    			row = '<tr class="sm-font no-record">'+
							'<td colspan="4">No Record</td>'+
						'</tr>';
    			
    			for(var i = 0;i<Merits.length; i++) {
    				if (i <= 0) row = '';
    				row += "<tr class='cursor-pointer attendanceSel' data-attendancekey='"+Merits[i].AttendanceKey+"'>"+
    							"<td class='sm-font'>"+Merits[i].PeriodID+"</td>"+
    							"<td class='sm-font'>"+ifNull(Merits[i].Date)+"</td>"+
    							"<td class='sm-font'>"+Merits[i].Remarks+"</td>"+
    						"</tr>";
    			}

    			jQuery('#TableMerits tbody').html(row);

    			//parse student lost and found
    			row = '<tr class="sm-font no-record">'+
							'<td colspan="4">No Record</td>'+
						'</tr>';
    			
    			for(var i = 0;i<LostFound.length; i++) {
    				if (i <= 0) row = '';
    				row += "<tr class='cursor-pointer attendanceSel' data-attendancekey='"+LostFound[i].AttendanceKey+"'>"+
    							"<td class='sm-font'>"+LostFound[i].PeriodID+"</td>"+
    							"<td class='sm-font'>"+ifNull(LostFound[i].Date)+"</td>"+
    							"<td class='sm-font'>"+LostFound[i].Particulars+"</td>"+
    							"<td class='sm-font'>"+LostFound[i].Remarks+"</td>"+
    						"</tr>";
    			}

    			jQuery('#TableLostFound tbody').html(row);

                for(var i = 0;i<Tardiness.length; i++) {
                    if (i <= 0) row = '';
                    row += "<tr class='cursor-pointer attendanceSel' data-attendancekey='"+Tardiness[i].AttendanceKey+"'>"+
                                "<td class='sm-font'>"+Tardiness[i].PeriodID+"</td>"+
                                "<td class='sm-font'>"+ifNull(Tardiness[i].Date)+"</td>"+
                                "<td class='sm-font'>"+Tardiness[i].Remarks+"</td>"+
                            "</tr>";
                }

                jQuery('#TableTardiness tbody').html(row);
    		},
    		function(error) {},
    		'json',true,false
    	);
    }

	return {
		init: function() {
            clearProfile();
			handleActionButtons();
			handleFilterEvents();
			handleSelections();
			
			$('body').on('click','.btn-export',function(){
			   var xsection = SectionID;
			   var xstudno  = $('#StudentNo').text();
			   if($('.adviser_section').length>0){
			     xsection = $('.adviser_section').val();
			   }
			   
			   if(xsection==undefined || xsection=='' || xsection==-1){
			     msgbox('error','No Selected Section/Student!!');
				 return false;
			   }
			   if(xstudno==undefined || xstudno=='' || xstudno==-1){
			     msgbox('error','No Selected Section/Student!!');
				 return false;
			   }
			   
			   window.open(base_url+'class-record/discipline/export?type=5&stdno='+xstudno+'&section='+xsection,'_blank');
			});
		}
	}
}();