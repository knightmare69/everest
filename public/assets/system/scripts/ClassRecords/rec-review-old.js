var ClassReview = {
    ajax_stack: [],
    empl_stack: [],
    empl_selected: '',
    load_more_faculties: function() {
        var term_id = $('#academic-year').val(),
            _find = $('#faculty-find').val(),
            _this = this;

        setAjaxRequest(
            base_url + page_url + 'event', {
                'event': 'load-faculties',
                't': term_id,
                'f': _find,
                'e': this.empl_stack
            },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    if (result.total >= 1) {
                        $('#faculty-list').append(result.list);
                        _this.load_photos();
                    } else {
                        $('#faculty-list-get').parent().addClass('hide');
                        msgbox('error', 'No result(s) found.');
                    }
                }
            }
        )
        progressBar('off');
    },

    find_faculties: function() {
        var term_id = $('#academic-year').val(),
            _find = $('#faculty-find').val(),
            _this = this;

        if (this.ajax_stack.length >= 1) {

            for (var i = 0; i < this.ajax_stack.length; i++) {
                this.ajax_stack[i].abort();
            }

            this.ajax_stack = [];
            this.empl_stack = [];
        }

        setAjaxRequest(
            base_url + page_url + 'event', {
                'event': 'find-faculties',
                't': term_id,
                'f': _find
            },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    $('#faculty-list').empty().html(result.list);
                    var load_more = $('#faculty-list-get').parent();

                    if (result.total >= 1) {
                        load_more.removeClass('hide');

                        _this.load_photos();

                    } else {
                        load_more.addClass('hide');
                    }

                }
            }
        );
        progressBar('off');
    },

    load_photos: function() {
        var stack = this.ajax_stack,
            empl = this.empl_stack;

        $('#faculty-list .item-details a').each(function() {
            var e = $(this).attr('data-id'),
                img = $(this).prev();

            empl.push(e);

            stack.push(
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: base_url + page_url + 'event',
                    dataType: 'html',
                    data: {
                        'event': 'faculty-photo',
                        'e': e
                    },
                    type: 'POST',
                    async: true,
                    beforeSend: function() {},
                    success: function(res) {
                        img.attr('src', res);
                    },
                })
            )

        });
    },

    get_sections_with_subject: function(empl_id) {
        var term_id = $('#academic-year').val(),
            _this = this;
        this.empl_selected = empl_id;

        setAjaxRequest(
            base_url + page_url + 'event', {
                'event': 'get-faculty-sections-subjects',
                't': term_id,
                'e': empl_id
            },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    var info = $('a.faculty-names[data-id="' + empl_id + '"]'),
                        photo = info.prev('img').attr('src'),
                        name = info.text(),
                        subj_title = $('#selected-subj-title');

                    $('#faculty-selected-photo').attr('src', photo);
                    $('#faculty-selected-name').text(name);
                    $('#myschedules').empty();
                    $('#post-grade').parent().addClass('hide');
                    subj_title.text('');

                    $('#subjects').select2('val', '').attr('disabled', true);

                    $('#sections').select2('destroy').empty().html(result.list).select2({
                        placeholder: 'Please select section.'
                    }).on('change', function(e) {
                        _title = $(this).select2('data').element[0].attributes['data-subj-title'].value;
                        subj_title.text(_title);
                        _this.get_class_records($(this).val());
                    });

                    $('#faculty-class-record').removeClass('hide');
                }
            }
        );
        progressBar('off');
    },

    get_class_records: function(sched_id) {
        var period = $('#period').val();

        setAjaxRequest(
            base_url + 'class-record/eclass-records/thisRequest', {
                'event': 'students',
                'sched_id': sched_id,
                'period': period,
                'shs': 1,
            },
            function(result2) {
                if (result2.error) {
                    msgbox('error', result2.message);

                } else {
                    $('#post-grade').parent().removeClass('hide');
                    $('#myschedules').empty().html(result2.content);
                }
            }
        );
        progressBar('off');
    },

    post: function(e){

      var _checked = $('#tblgrade').find('.chk-child:checked'), _this = this;

      var validatepost = function($r){

            var flg = true;

            $r.find('td.eventScore').each(function(){
                var item = $(this).attr('data-items');
                var score = 0;

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if ( item > 0 ){
                    if(score == '' ){
                        flg = false ;
                    }
                }
            });

            $r.find('td.conductScore').each(function(){

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                }
            });

            return flg;
      };


      var execution = function(){
         var ids = [];
                _checked.each(function(e){
                    var _tr = $(this).closest('tr') ;
                    var _d = _tr.data();
                    var id = _tr.attr('data-idno');

                    if(id !== 0 || id !== undefined){
                        if (validatepost(_tr)) {
                            var x = {
                                idno : id,
                                reg : _d.reg,
                                yr : _d.yr,
                                subj : _d.subject,
                                grade : _tr.find('td.InitialGrade').find("div").html().trim(),
                                fgrade : _tr.find('td.TransmutedGrade').find("div").html().trim(),
                                remarks : _tr.find('td.Description').find("div").html().trim(),
                                cona : _tr.find('td.conductScore[data-conduct="1"]').find("div").html(),
                                conb : _tr.find('td.conductScore[data-conduct="2"]').find("div").html(),
                                conc : _tr.find('td.conductScore[data-conduct="3"]').find("div").html(),
                                cond : _tr.find('td.conductScore[data-conduct="4"]').find("div").html(),
                            };

                            ids.push(x);
                        }
                    }
                });

                if(ids.length >= 1){

                    var term_id = $('#academic-year').val(),
                        section_id = $('#sections').val(),
                        subject_id = $('#subjects').val(),
                        period = $('#period').val();

                    var data = {
                        event : 'post',
                        term : term_id,
                        sched : _this.sched_id,
                        subj : subject_id,
                        period : period,
                        stud : ids,
                    }

                    setAjaxRequest(
                        base_url + 'class-record/eclass-records/thisRequest', data,
                        function(r) {
                            if (r.error) {
                                msgbox('error','Sorry, unable to post grades');
                            } else {
                                //showSuccess(result.message);
                                // ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                                Metronic.alert({
                                    container: '#show-message'  , // alerts parent container(by default placed after the page breadcrumbs)
                                    place: 'prepend', // append or prepent in container
                                    type: 'success',  // alert's type
                                    message: r.message,  // alert's message
                                    close: true, // make alert closable
                                    reset: true, // close all previouse alerts first
                                    focus: true, // auto scroll to the alert after shown
                                    closeInSeconds: 10, // auto close after defined seconds
                                    icon: 'check' // put icon before the message
                                });
                                _this.get_class_records();
                            }
                        }
                    );
                    progressBar('off');
                } else{
                    msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
                }

        } ;


        if (_checked.length > 0 ) {

          $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
    			content : "Posting student record will activate the class record lock features.. <br/> You are no longer able to create, edit and delete an event. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     $.SmartMessageBox({
            			title : "<span class='text-warning'><i class='fa fa-lock'></i> Do you wish to POST the selected student(s)?</span>",
            			content : "Do you certify that all the grades per student in this class record are true and correct?",
            			buttons : '[No][Yes]'
            		}, function(ButtonPressed) {
            			if (ButtonPressed === "Yes") {
            			     execution(true);
            			}
            		});
    			}else{
    			     $(this).remove();
    			}
    		});
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },
}

function scrollme(e){
    var x = e.scrollLeft;
    document.getElementById('mytblheader').scrollLeft =x;
    var y = e.scrollTop;
    if(document.getElementById('studentcols')){
        document.getElementById('studentcols').scrollTop =y;
    }

}

$(document).ready(function() {

    $('body').on('click', '#show-sy-only', function(e) {
        var show_sy_only = false,
            at = $('#academic-year');

        if ($(this).is(':checked')) {
            at.children().each(function() {
                var _find = $(this).text().indexOf('School Year');

                if (_find === -1) {
                    $(this).addClass('hide');
                }
            });
        } else {
            at.children().removeClass('hide');
        }
    });

    $('#academic-year').select2({
        placeholder: 'Please select a Term.'

    }).on('change', function(e) {
        ClassReview.find_faculties();
        $('#faculty-find, #faculty-list').closest('div.row').removeClass('hide');
    });

    $('body').on('click', '#faculty-find-btn', function(e) {
        ClassReview.find_faculties();
    });

    $('body').on('click', '#faculty-list-get', function() {
        ClassReview.load_more_faculties();
    });

    $('body').on('click', '.item-details .item-name.primary-link', function(e) {
        var emp = $(this).attr('data-id');
        ClassReview.get_sections_with_subject(emp);
    });

    $('#period').select2().on('change', function(e) {
        var sched_id = $('#sections').val();

        if (Boolean(sched_id.trim())) {
            ClassReview.get_class_records(sched_id);
        }
    });

    $('body').on('click', '.eventScore', function(e){
        var note_div = $('#div-note-msg'), pos_t = $(this).offset().top, pos_l = $(this).offset().left;

        note_div.removeClass('hide').css({'top' : (pos_t - (note_div.outerHeight() + (note_div.outerHeight() / 2) - 20 )), 'left': pos_l - (note_div.outerHeight() / 2) });

    });

    $('body').on('click', '#note-btn-close', function(e){
        $('#div-note-msg').addClass('hide');
    });

    $('body').on('click', '#post-grade', function(e){
        ClassReview.post(e)
    });

    $('body').on('click', '.chk-header', function(e){
        var flg = $(e.target).prop('checked');
        $('#tblgrade tbody').find('.chk-child').prop('checked', flg);
        $('#studentlist tbody').find('.chk-child').prop('checked',flg);
    });

});
