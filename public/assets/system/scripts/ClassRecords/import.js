var Import ={ 
    
    layout: function(e){        
        var term = $('#term').find('option:selected').text()
        var x = term.trim().indexOf('School');
        var id = $('#schedule').val();
        var shs = (x > 0 ? 0 : 1 );
        window.open(ME.mylink+'layout?sched='+id+'&shs='+shs ,'_blank');
    },
    
    init : function(){                
    	var filefound = file.files[0];
        var y = 0;
        var $tbody = $('#tblimport tbody');
        $tbody.html('');
        if(filefound){
			var navigator = new FileNavigator(filefound);
			var indexToStartWith = 0;
			var limitIndex = 100;
			var stop = 0;
			navigator.findAll(new RegExp(''), indexToStartWith, limitIndex, function load_imports(err, index, limitHit, results) {
				if (err) {
					finished = new Date();
					stop = 1;
				}
				if (results.length == 0) {
					finished = new Date();
                    stop = 1;
				}
				var resultsAsLine = '';
				var rowindex = [];
				for (var i = 0; i < results.length; i++) {
					resultsAsLine +=  "<tr><td class='autofit font-xs'>"+ y +".</td><td class='autofit'>"+results[i].line.replace(/&/g,"").replace(/\r\n|\n/g, "").replace(/\|/g, "").replace(/\\,/g,"^").replace(/,/g,"</td><td class='autofit'>") + "</td></tr>" ;
					y += 1;
				}
				if(resultsAsLine != ''){
					//console.log(resultsAsLine);
				    $tbody.append(resultsAsLine);
                    return true;
				}
			});
        }
    },
    
    validate_form: function(){
        var i = $('#upload_idno').val();
        var r = $('#upload_rawscore').val();
        var e = $('#upload_event').val();
         
        if ( i == '' || i ==0 ){
            alert('Please enter column for Student Number');
            return false;
        }
        
        if ( r == '' || r ==0 ){
            alert('Please enter column for Raw score');
            return false;
        }
        
        if ( e == '' || e ==0 ){
            alert('Please enter column for Event ');
            return false;
        }
        
        return true;
    },
    
    process : function(e){
       var $btn = $(e.target);
       //var mapid = this.$mapping.attr('id');
       
       if( this.validate_form() ){
            
            ME.initAlerts('#mdlimport .modal-body','info',"Please wait while processing...",true,true,true,'fa-spin fa-refresh',100)                        
            var parm = $('#form-mapping').serialize();
            
            console.log(parm);
                var i = 0;
                var j = 0;
                
                var rowstart = $('#rowstart').val();                
                
                var idno = '';
                var raw = '';
                var event = '';
                var data =  [];
                
                $('#tblimport tbody tr').each(function(){
                    
                    if( i == 0 && i != rowstart ){
                        j = 0;
                        $(this).find('td').each(function(){                    
                            if ( $(this).html() == $('#upload_idno').val() ) { idno = j; }                            
                            if ( $(this).html() == $('#upload_rawscore').val() ) { raw = j; }                                                                                                                                            
                             ++j;
                        });
                        
                        if(idno == 0){
                            alert('Sorry, Student Number column not found!');
                            progressBar('off');
                            return false;
                        }
                        
                        if(raw == 0){
                            alert('Sorry, Rawscore column not found!');
                            progressBar('off');
                            return false;
                        }    
                        
                                                                                                                                                                            
                    }else{
                        
                        var d = {
                            id : $(this).find('td').eq(idno).html(),
                            rw : $(this).find('td').eq(raw).html(),                        
                        }
                                                            
                        data.push(d);                                                            
                    
                    }               
                    i++;                                             
                });
                
                var post =  {
                    event : 'import-score',
                    actvty : $('#upload_event').val(),
                    term  : $('#term').val(),
                    period  : $('#period').val(),
                    data  : data
                };
                
                console.log(post);
                
                var success = function(r){
                    if (r.error) {
                        ME.initAlerts('#mdlimport .modal-body','danger',r.message,true,true,true,'danger',10)
                    } else {
                        //showSuccess(result.message);
                        ME.initAlerts('#mdlimport .modal-body','success',r.message,true,true,true,'check',10)                                
                        setTimeout(function(){
                            ME.refresh();
                            $('#mdlimport').modal('hide');
                        },2000);                                                        
                    }
                };
                
                var my_error = function(r){
                    ME.initAlerts('#mdlimport .modal-body','danger',"Sorry, Unable to import raw score at this time...",true,true,true,'danger',10)
                };
                
                setAjaxRequest( base_url + page_url + 'thisRequest', post, success,my_error);                                                       
         }else{
            msgbox('warning','Please input the required field(s)!');
         }                
    },
    
}
