function scrollme(e){
    var x = e.scrollLeft;
    document.getElementById('mytblheader').scrollLeft =x;
    var y = e.scrollTop;
    if(document.getElementById('studentcols')){
        document.getElementById('studentcols').scrollTop =y+1;
    }
    ME.hideactiveText();
}

var ME = {
    init : function() {
        this.mylink = base_url+page_url;
        this.$term = $('#term');
        this.$schedule = $('#schedule');
        this.$period = $('#period');
        this.$tempholder = undefined;
        this.$editor = $('#tbleditor');
        /*this.$term.val(-1);*/
        this.$schedule.val(-1);
        this.$editor = $('#tbleditor');
        this.$editorText = this.$editor.find('.input-sm');
        this.Isbusy = false;
        this.Invalid = false;
        this.IsChanged = false;
		this.IsReload  = false;
        this.initplugins();
        this.addlistener();
        this.addlistener2();
        var t = getParameterByName('t');
        var s = getParameterByName('s');
        var p = localStorage.getItem('period');

        if(t != '' ){ this.$term.val(t); }
        if(s != ''){ this.$schedule.val(s); }
        if(t != '' || s != '' ){
            if(p == undefined || p == ''){
            this.$period.val(1);
            }else{
                this.$period.val(p);
            }
            $('.btn[data-menu="refresh"]').click();
        }

        if(p == undefined || p == ''){
            this.$period.val(1);
        }else{
            this.$period.val(p);
        }
		Metronic.initUniform();

    },
    initplugins: function(){
       var header_height = 0;
       $('.vertical-text').each(function() { if ($(this).outerWidth() > header_height) header_height = $(this).outerWidth(); });
       $('.vertical-tex').parent().height(header_height);
	   Metronic.initUniform();
    },
    addlistener: function() {
        this.$editorText.on('blur',$.proxy(this.hideactiveText,this));
        this.$editorText.on('focus',$.proxy(this.editorFocusUp,this));
        this.$editorText.on('mouseup',$.proxy(this.editorMouseUp,this));
        this.$editorText.on('change',$.proxy(this.editorChange,this));
        this.$editorText.on('paste',$.proxy(this.mypaste,this));
        this.$editorText.on('keypress',function(e){

            if( e.keyCode === 27) {
                var c = $(e.target).attr('data-col'), r =  $(e.target).attr('data-row');
                var $nr = $('#tblgrade tbody tr').eq(r).find('td').eq(c);
                ME.compute(r,c);
                ME.hideactiveText();
                ME.cell_focus($nr);
            }else if (e.keyCode == 13 ){
                if(!ME.IsChanged){
                    ME.editorChange(e);
                }
                ME.IsChanged=false;
            }

        });
        $('body').on('dblclick','#tblgrade tbody td', $.proxy(this.activeText, this));
        $('body').on('keydown','#tblgrade tbody td', $.proxy(this.activeText, this));
        $('body').on('click','.btn, .clink,  span, i', $.proxy(this.menu,this));
        $('body').on('click','#tblgrade tbody td', function(){ ME.cell_focus($(this)); });
        $('body').on('click','.chk-header', $.proxy(this.selectall, this));
        $('body').on('change','#file', $.proxy(Import.init, this));
        $('body').on('click','#studentlist .chk-child', $.proxy(this.syncselect, this));
        $('body').on('click','.views', $.proxy(this.help,this));
    },

    addlistener2: function(){
        $('body').find(this.$term).on('change',$.proxy(this.term_changed, this));
        $('body').find(this.$schedule).on('change',$.proxy(this.schedule_changed, this));
        $('body').find(this.$period).on('change',$.proxy(this.period_changed, this));
    },

    help: function(e){

        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');

        $('#summary-container').hide();
        $('#help-container').hide();
        $('#class-record-container').hide();

        var data = $(e.target).data();
        switch (data.menu) {
            case 'class-record': $('#class-record-container').show(); break;
            case 'summary': $('#summary-container').show(); break;
            case 'help': $('#help-container').show(); break;
        }
    },

    mypaste: function(e){

         var r = $(e.target).attr('data-row');
         var c = $(e.target).attr('data-col');
         var pasteData = e.originalEvent.clipboardData.getData('text')
         var rows = pasteData.split("\n");

         $('#tbleditor').find('input').val('');
         $('#tbleditor').hide();

         $(e.target).val('');

         var a = 0, b = 0;

         for(var y = 0 ; y <= rows.length ; ++y){
            if(rows[y] != undefined){
                var cells = rows[y].split("\t");
                for(var x = 0 ; x <= cells.length ; ++x){
                    if( cells[x] != undefined && cells[x].trim()!='') {
                        a = parseInt(r)+ parseInt(y);
                        b = parseInt(c)+ parseInt(x);

                        var $c = $('#tblgrade tbody tr').eq(a).find('td').eq(b);
                        if($c.hasClass('editable')){

                            if($c.hasClass('conductScore')){

                            }else{
                                var items = strCast( $c.attr('data-items') , 'float') ;
								var paste = strCast( cells[x] , 'float') ;
								    paste = ((paste==undefined || isNaN(paste))?'0.00':paste.toFixed(2));
                                if(items >=  cells[x] ){
									console.log('Paste:'+paste);
                                    $c.find('div').html(paste);
                                    ME.compute('total',a,b);
			                        ME.save_grade(a,b);
                                }
                            }
                        }
                    }
                }
            }
         }

         ME.save_bulkgrade(r,c);
    },

    syncselect : function(e){
        var flg = $(e.target).prop('checked');
        var id = $(e.target).attr('data-id');
        $('#tblgrade tbody').find('.chk-child[data-id="'+id+'"]').prop('checked', flg);
    },

    selectall: function(e){
        var flg = $(e.target).prop('checked');
        $('#tblgrade tbody').find('.chk-child').prop('checked', flg);
        $('#studentlist tbody').find('.chk-child').prop('checked',flg);
    },

    initAlerts : function(container, alert, message, closable, reset, focus,icon, time ) {

        Metronic.alert({
            container: container  , // alerts parent container(by default placed after the page breadcrumbs)
            place: 'prepend', // append or prepent in container
            type: alert,  // alert's type
            message: message,  // alert's message
            close: closable, // make alert closable
            reset: reset, // close all previouse alerts first
            focus: focus, // auto scroll to the alert after shown
            closeInSeconds: time, // auto close after defined seconds
            icon: icon // put icon before the message
        });
    },

    cell_focus : function(e){
        $('#tblgrade tbody td').removeClass('cell-focus');
        $(e).addClass('cell-focus');
        $(e).focus();
        var $r = $(e).closest('tr');
        var grade = $r.find('td.TransmutedGrade > div').html();

        $('.profile-usertitle-name').html($r.attr('data-name'));
        $('.profile-usertitle-job')
            .html($r.attr('data-student'))
            .attr('data-id', $r.attr('data-idno'))
            .attr('data-reg', $r.attr('data-reg'));

        if (grade != undefined){
            $('.profile-final-grade').html('Final Grade : '+  grade );
        }

        $('.img-responsive').attr('src', $r.find('td.img').find('img').attr('src'));
    },

    menu: function(e){

        if( $(e.target).is('span') || $(e.target).is('i') ){
            var data = $(e.target).parent().data();
        }else{
            var data = $(e.target).data();
        }

        //console.log(e);
        switch (data.menu) {
            case 'event':
                this.event(e,data.mode);
                e.stopPropagation();
            break;
            case 'refresh': this.refresh(); break;
            case 'post-grade':
                this.post(e);
                e.stopPropagation();
            break;
            case 'conduct': this.conduct(e, data.mode); break;
            case 'table': this.system_table(e, data.mode); break;
            case 'upload': this.upload(e); break;
            case 'layout': Import.layout(e); break;
            case 'import': Import.process(e); break;
            case 'print': ME.print(); break;
            case 'recompute': ME.recompute(); break;
            case 'varsity': ME.varsity('init'); break;
            case 'save-varsity-grade': ME.varsity('save'); break;
            default: break;
        }

    },


    varsity : function(mode){

        var $r = $('.cell-focus').closest('tr');

        if( mode =='init' ) {

            if($r.attr('data-posted') ==1 ){
                msgbox('error','Sorry, student was already posted no more modification allowed!');
                return false;
            }

            $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
    			content : "Do you wish to identify this student as varsity player? <br/> Being a varsity player enable the feature to encode only the final grade. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {

    			if (ButtonPressed === "Yes") {
    			     $('#varsity_modal').modal('show');
   			      }else{
   			          $(this).remove();
   			      }
    		});

        } else if(mode == 'save'){

            var grade = $('#varsitygrade').val();
            var idno = $('.profile-usertitle-job').attr('data-id');
            var reg = $('.profile-usertitle-job').attr('data-reg');

            var data = { event : 'save-grade',
                activity : 0,
                term : this.$term.val(),
                period : this.$period.val(),
                idno : idno,
                score : 0,
                regid : reg,
                // sched : $("#schedule").val(),
                sched : this.$schedule.val(),
				cgrde : 0,
                igrde : 0,
                finl : grade,
                hmrm : 0,
                varsity : 1,
            };

            var mysuccess = function(e){
                if(!e.error){ msgbox('success','final grade successfully saved!'); }
                $('#varsity_modal').modal('hide');
                $r.attr('data-varsity','1');
            };

            if (grade != ''){ setAjaxRequest(this.mylink+"thisRequest",data,mysuccess,ME.error,'json',true,false); }
        }
    },


    print : function(){
        var term = $('#term').find('option:selected').text()
        var x = term.trim().indexOf('School');
        var id = $('#schedule').val();
        var shs = (x > 0 ? 0 : 1 );
        window.open(ME.mylink+'print?sched='+id+'&period='+ $('#period').val() +'&shs='+shs ,'_blank');
    },

    recompute : function(){
        var r =0, c= 0;
        $('#tblgrade tbody tr').each(function(){
            r = $(this).index();
            c = $(this).find('td.eventScore').eq(0).index();
            ME.compute('total',r,c);
        });
        ME.save_bulkgrade(r,c);
        if(this.IsReload==false){
		alert('Class record successfully recomputed!');
		}
		this.IsReload  = false;
    },

    upload: function(){

        var schedule = this.$schedule.val();
        var period  = this.$period.val();
        if(schedule == '-1' || period == '-1'){ msgbox('warning','Please select schedule/period','No Schedule/Period found!'); return false; }

        if(ME.verify_posted() ){

            var x = '<option value="-1">- Select -</option>';
            var sub = '<option value="-1">- Select -</option>';
            var e = '<option value="-1">- Select -</option>';

            $('.event-label').each(function(){
                x += '<option>' + $(this).html() + '</option>';
            });

            $('#upload_component').html(x);

            $('.myheader').find('.csubcomp').each(function(){
                sub += '<option value="'+ $(this).attr('data-id') +'" data-parent="'+ $(this).attr('data-parent') +'" >' + $(this).html() + '</option>';
            });

            $('#upload_subcomponent').html(sub);

            $('.event-name').each(function(){
                if($(this).attr('data-id')!='0'){
                    e += '<option value="'+ $(this).attr('data-id') +'" data-comp="'+ $(this).attr('data-component') +'">' + $(this).html().trim() + '</option>';
                }
            });

            $('#upload_event').html(e);
            $('#mdlimport').modal('show');
        }
    },

    system_table: function(e, mode){

        switch(mode){
            case 'conduct': this.conduct(e,'table'); return false;  break;
            default :

                $('#gradetbl_modal').find('.modal-title').html('Grading Table : ' + mode );
                var data = { event  : 'grade-table', table : mode };
                setAjaxRequest(ME.mylink+"thisRequest",data,
                    function(e){
                        $('#gradetbl_modal').find('.modal-body').html(e);
                        $('#gradetbl_modal').modal('show');
                },ME.error,'html',true,false);


            break;
        }
        e.preventDefault();
    },

    post : function(e){

      var _checked = $('#tblgrade').find('.chk-child:checked');

      var validatepost = function($r){

            var flg = true;

            $r.find('td.eventScore').each(function(){
                var item = $(this).attr('data-items');
                var score = 0;

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if ( item > 0 ){
                    if( score == '' ){
                        flg = false ;
                    }
                }
            });

            $r.find('td.conductScore').each(function(){

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                }
            });
			
			
            $r.find('td.xconductScore').each(function(){

                if( $(this).has("div") ){
                    score = $(this).find("div").html().trim();
                }else{
                    score = $(this).html().trim();
                }

                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                }
            });

            if($r.attr('data-varsity') == 1){
                flg = true ;
            }

            return flg;
      };


      var execution = function(){
         var ids = [];
            _checked.each(function(e){
                var _tr = $(this).closest('tr') ;
                var _d = _tr.data();
                var id = _tr.attr('data-idno');

                if(id !== 0 || id !== undefined){
                    if (validatepost(_tr)) {
                        var x = {
                            idno : id,
                            reg : _d.reg,
                            yr : _d.yr,
                            subj : _d.subject,
                            grade : _tr.find('td.InitialGrade').find("div").html().trim(),
                            fgrade : _tr.find('td.TransmutedGrade').find("div").html().trim(),
                            remarks : _tr.find('td.Description').find("div").html().trim(),
                            cona : _tr.find('td.conductScore[data-conduct="1"]').find("div").html(),
                            conb : _tr.find('td.xconductScore[data-conduct="2"]').find("div").html(),
                            conc : _tr.find('td.xconductScore[data-conduct="3"]').find("div").html(),
                            cond : _tr.find('td.xconductScore[data-conduct="4"]').find("div").html(),
                        };
                        ids.push(x);
                    }
                }
            });

            if(ids.length >= 1){

                var data = {
                    event : 'post',
                    term : ME.$term.val(),
                    sched : ME.$schedule.val(),
                    subj : ME.$schedule.find('option:selected').attr('data-subj'),
                    period : ME.$period.val(),
                    stud : ids,
                };

                setAjaxRequest(
                    base_url + 'class-record/eclass-records/thisRequest', data,
                    function(r) {
                        if (r.error) {
                            msgbox('error','Sorry, unable to post grades');
                        } else {
                            //showSuccess(result.message);
                            ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                            ME.refresh();
                        }
                    }
                );
                progressBar('off');
            } else{
                msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
            }
        };

        if (_checked.length > 0 ) {

          $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
    			content : "Posting student record will activate the class record lock features.. <br/> You are no longer able to create, edit and delete an event. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     $.SmartMessageBox({
            			title : "<span class='text-warning'><i class='fa fa-lock'></i> Do you wish to POST the selected student(s)?</span>",
            			content : "Do you certify that all the grades per student in this class record are true and correct?",
            			buttons : '[No][Yes]'
            		}, function(ButtonPressed) {
            			if (ButtonPressed === "Yes") {
            			     execution(true);
            			}
            		});
    			}else{
    			     $(this).remove();
    			}
    		});
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },

    refresh : function(){
        var schedule = this.$schedule.val();
        var period  = this.$period.val();
        if(schedule != '-1' && period != '-1') this.load_students(schedule, period );
    },

    success : function(e){
        progressBar('off');
        ME.$tempholder.html(e);
        $('#event_modal').modal('show');
        FN.datePicker();
    },

    error : function(error,textStatus, xhr){
        if(error){
            progressBar('off');
            msgbox('error','Oops, there seem to be a problem... Try again later.');
            alert('Make sure all configurations and policy was set. Check your internet connection too.');
            //location.reload();
        }

        if(!navigator.onLine){
            alert('You are disconnected! System will reload to check your internet connection...');
            location.reload();
        }

    },

    print : function(){
        var term = $('#term').find('option:selected').text()
        var id = $('#schedule').val();
        var shs = 1;
        window.open(ME.mylink+'print?sched='+id+'&period='+ $('#period').val() +'&shs='+shs ,'_blank');
    },

    conduct : function(e, args){

        var conductGradeValidator = function(grade){
            var v = { valid : false, msg : 'invalid conduct grade', letter : '', desc : '', remarks : '', };
            $('#tblconduct tbody tr').each(function(){
                var min =  strCast( $(this).find('td.min').html().trim() , 'float') ;
                var max =  strCast( $(this).find('td.max').html().trim() , 'float') ;
                if( grade >= min && grade <= max ){
                    v = {
                        valid : true,
                        msg : 'conduct grade accepted',
                        letter : $(this).find('td.letter').html().trim(),
                        desc : $(this).find('td.desc').html().trim(),
                        remarks : $(this).find('td.remarks').html().trim(),
                    };
                    return v;
                }
            });
            return v;
        };

        var saveConduct = function(score){

                var row =  $(e.target).attr('data-row') ;
                var col =  $(e.target).attr('data-col') ;

                var $r = $('#tblgrade tbody tr').eq(row);
                var data = {
                    event : 'save-conduct',
                    sched : $('#schedule').val(),
                    conduct : $r.find('td').eq(col).attr('data-conduct'),
                    term : ME.$term.val(),
                    period : ME.$period.val(),
                    idno : $r.attr('data-idno'),
                    score : score,
                    idx : $r.find('td').eq(col).attr('data-id'),
                };

                var mysuccess = function(e){
                    if(!e.error){
						$r.find('td').eq(col).attr('data-id',e.xid);
                        progressBar('off');
                    }
                };
                setAjaxRequest(ME.mylink+"thisRequest",data,mysuccess,ME.error,'json',true,false);
                progressBar('off');
         };

         switch(args){

        	case 'table': $('#conduct_modal').modal('show'); break;
            case 'validate':
                var grade = strCast(e.target.value, 'float');
                var v = conductGradeValidator(grade);
                if(v.valid){ saveConduct(grade); }
                return v;
            break;
            case 'erase':
                var idx = $(e.target).attr('data-id');
                var c = $(e.target).attr('data-conduct');
                var idno = $(e.target).closest('tr').attr('data-idno');

                setAjaxRequest( base_url + page_url + 'thisRequest', {'event': 'erase-conduct', 'index': idx, 'idno' : idno, 'conduct':c },
					function(result) {
						if (result.error) {
							showError(result.message);
						} else {
							showSuccess(result.message);
                            progressBar('off');
						}
					}
				);
            break;

            case 'compute-conduct':

                 var row =  $(e.target).attr('data-row') ;
                 var col =  $(e.target).attr('data-col') ;
                 var $r = $('#tblgrade tbody tr').eq(row);
                 var total = 0;
                 var counter = 0;
                 $r.find('td.conductScore').each(function(){
                    counter ++;
                    if( $(this).has("div") ) {
                        total += strCast($(this).find("div").html().trim(),'float');
                    }else{
                        total += strCast($(this).html().trim(),'float');
                    }
                 });

                  var grade = total / counter;
                  var v = conductGradeValidator(grade);
                  if(v.valid){
                        $r.find('td.conductTotal').find('div').html(parseFloat(grade).toFixed(2));
                       $r.find('td.conductGrade').find('div').html(v.letter);
                       $r.find('td.conductRemarks').find('div').html(v.remarks);
                  }

            break;
          }
    },


    verify_posted : function(){
      var ifound = 0;
      var flg = true;

      $('#tblgrade tbody tr').each(function(){
            if ( $(this).attr('data-posted') != 0 ) {
                ++ifound;
            }
      });
      if(ifound>0){
            var msg = "Some student was already posted... \n You cannot Add, Edit and Delete an event or Import raw score from CSV...  ";
            alert(msg);
            flg = false;
       }
       return flg ;
    },

    event : function(e,args){

        var schedule = this.$schedule.val();
        var period  = this.$period.val();
        if(schedule == '-1' || period == '-1'){ msgbox('warning','Please select schedule/period','No Schedule/Period found!'); return false; }
        var validation =  function(){
            var err = 0;

            $('#event_form').find('.form-group').removeClass('has-error');
            $('#event_form').find('.help-block').remove();

            var setRequired = function(e){
                $(e).closest('.form-group').addClass('has-error');
                $(e).parent().append('<span class="help-block">This field is required!</span>')
                err++;
            };

            $('#event_form').find('.form-control').each(function(){
                if(!$(this).hasClass('except')){
                    if( $(this).is('select') ){
                        if ( $(this).val() == '-1'){ setRequired(this); }
                    }else{
                        if ( strCast($(this).val()) == ''){ setRequired(this); }
                    }
                }
            });

            return (err > 0 ? false : true);
        };

		var btnstate = function(state){
			$('.modal-footer').find('.btn-event').hide();
			if( state == 'add') {
				$('.modal-footer').find('.grp-add').show();
			}else if(state == 'remove' ){
				$('.modal-footer').find('.grp-rem').show();
			}
		};

        switch(args){

			case 'reset':
				$('#event_form').find('#name').val('');
                $('#event_form').find('#items').val('');
                $('#event_form').find('#date').val('');
                $('#event_form').find('#desc').val('');
				$('#event_form').find('#idx').val('');

			break;

            case 'add':

                if(ME.verify_posted() ){

                    progressBar('on');
                    var sched = this.$schedule.val();
                    var period = this.$period.val();
                    var data = { event  : 'events', schedule : sched, period : period , component: '' , term : ME.$term.val() };
                    this.$tempholder = $('#event_modal').find('.modal-body');
    				btnstate('add');
                    this.$tempholder.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
                    setAjaxRequest(this.mylink+"thisRequest",data,ME.success,ME.error,'html',true,false);
                }

            break;

            case 'edit':

                if(ME.verify_posted() ){

                	btnstate('add');
                    var sched = this.$schedule.val();
                    var period = this.$period.val();
                    var comp = $(e.target).attr('data-component');
                    var data = { event  : 'edit-event', index : $(e.target).attr('data-id') , schedule : sched, period : period , component: comp , term : ME.$term.val(), caption : $.trim($(e.target).html()) };
                    this.$tempholder = $('#event_modal').find('.modal-body');

                    this.$tempholder.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
                    setAjaxRequest(this.mylink+"thisRequest",data,ME.success,ME.error,'html',true,false);
                }

            break;

            case 'save':
                //if (ME.Isbusy){ return false; }
                //ME.Isbusy = true;

                if (validation()){

                    var data = $('#event_form').serialize();
                    var mysuccess = function(e){
                        if(!e.error){
                            sysfnc.showAlert('#event_modal .modal-body','success','Event Successfully created',true,true,true,'check',0)
							ME.refresh();
                        }else{
                            sysfnc.showAlert('#event_modal .modal-body','warning',e.message,true,true,true,'warning',10)
                        }
                        ME.Isbusy = false;
                    };
                    sysfnc.showAlert('#event_modal .modal-body','info','Please wait while saving...',true,true,true,'spinner fa-spin',0)
                    setAjaxRequest(this.mylink+"thisRequest",data,mysuccess,ME.error,'json',true,false);
                }
            break;

            case 'remove':
                if(ME.verify_posted() ){
    				progressBar('on');
    				btnstate('remove');
                    var sched = this.$schedule.val();
                    var period = this.$period.val();
                    var data = { event  : 'remove-event', schedule : sched, period : period , component: '' , term : ME.$term.val() };
                    this.$tempholder = $('#event_modal').find('.modal-body');
                    this.$tempholder.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
                    setAjaxRequest(this.mylink+"thisRequest",data,ME.success,ME.error,'html',true,false);
                }

            break;

			case 'delete':

				var _marked = $('#tbldelEvent').find('.chk-child:checked');

				 bootbox.confirm({
					message: "Are you sure you want to Delete the selected event(s)?",
					callback: function(yes) {
						if (yes) {
							var ids = [];
							_marked.each(function(e){
								var _tr = $(this).closest('tr'), id = _tr.attr('data-id');

								if(id !== 0 || id !== undefined){
									ids.push(_tr.attr('data-id'));
								}
							});
							if(ids.length >= 1){
								setAjaxRequest( base_url + page_url + 'thisRequest', {'event': 'erase-event', 'index': ids },
									function(result) {
										if (result.error) {
											showError(result.message);
										} else {
											showSuccess(result.message);
											ME.refresh();
                                            $('#event_modal').modal('hide');
										}
									}
								);
							}
						}
						progressBar('off');
					}
				});

			break;
        }
    },

    load_students : function(sched,period){
        /* need to review */

        var txt = $('#term').find('option:selected').text()
        var x = txt.trim().indexOf('School');
        var shs = 0;
        if(x <= 0){ shs = 1; }

        var data = { event : 'students', sched_id : sched, period : period, shs : shs };
        //$('#myschedules').html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        Metronic.blockUI({target: '#myschedules', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Please wait while loading... '});
        
		this.IsReload  = true;
        $schedules = $('#myschedules'); 
        
        var display_students = function(res){
            Metronic.unblockUI('#myschedules');
			$('#grading-system-table').html(res.grading);
            $schedules.html(res.content);
			ME.recompute();
            msgbox('success',res.message,'Student List');
        };
        setAjaxRequest(this.mylink+"thisRequest",data,display_students,ME.error,'json',true,false);
    },

    period_changed: function(e){
        var selected = $(e.target).val() ;
        var sched = this.$schedule.val();
        if(selected != '-1' && sched != '-1'){
            this.load_students(sched, selected );
        }
        localStorage.setItem("period", selected);
    },

    term_changed : function(e){

        var selected = $(e.target).val();
        var txt = $(e.target).find('option:selected').text()
        var x = txt.trim().indexOf('School');                
        var IsSummer = txt.trim().indexOf('Summer');
        
        $('#period').find('option').remove();
        
        if(x <= 0){
            if(IsSummer >= 0){
                $('#period')
                    .empty()
                    .append('<option value="-1"> - Select Period - </option><option value="14"> SUMMER</option>')
                    .val(14);
                
            }else{
                
            $('#period')
                .empty()
                .append('<option value="-1"> - Select Period - </option><option value="11"> 1ST TERM</option><option value="12"> 2ND TERM</option>');
            }
        }else{                        
                                                    
                $('#period')
                    .empty()
                    .append('<option value="-1"> - Select Period - </option><option value="1"> 1ST QUARTER</option><option value="2"> 2ND QUARTER</option><option value="3"> 3RD QUARTER</option><option value="4"> 4TH QUARTER</option>');
            
        }

        this.$schedule.find('option[value!=-1]').hide();
        this.$schedule.find('option').each(function(){
            if ( $(this).attr("data-term") == selected ) { $(this).show(); }
        });
        this.$schedule.val('-1');
    },

    schedule_changed : function(e){
        var selected = $(e.target).val();
        var period  = this.$period.val();
        var d = $(e.target).find('option:selected').data();

        if(d != undefined){
            $('#SubjectName').html('Suject Code : ' + d.code + ' - ' + d.title);
            $('#PolicyName').html('Policy : ' + (d.policy == '' ? '[No Policy]' : d.policy ));
        }

        if(selected != '-1' && period != '-1'){
            this.load_students(selected, period );
        }else{
            msgbox('error','Please select Period!');
        }
    },

    erase_grade : function(row,col){

        var $r = $('#tblgrade tbody tr').eq(row);

        var data = {
            event : 'erase-grade',
            activity : $r.find('td').eq(col).attr('data-event'),
            idno : $r.attr('data-idno'),
            name : $r.attr('data-name'),
        };

        var mysuccess = function(e){
            if(!e.error){

            }
        };
        setAjaxRequest(this.mylink+"thisRequest",data,mysuccess,ME.error,'json',true,false);

    },
    save_final : function(row,col){

        var $r = $('#tblgrade tbody tr').eq(row);

        var success = function(r){

            var $tg =  $('#tblgrade tbody tr').eq(row).find('td.TransmutedGrade');
            var $desc =  $('#tblgrade tbody tr').eq(row).find('td.Description');

            $tg.find("div").html(r.transmuted);
            $desc.find("div").html(r.message);
        };

        var data = {
            'regid' : $r.attr('data-reg'),
            'sched' : $("#schedule").val(),
            'period' : $("#period").val(),
            'event' : 'transmutation',
            'grade' : $r.find('td.InitialGrade').find('div').html() ,
            'homeroom' : $r.find('td.InitialGrade').attr('data-homeroom')
        };
        setAjaxRequest( base_url + page_url + 'thisRequest', data ,success ,ME.error(),'json',true, false);

    },

    save_grade : function(row,col){

        var $r = $('#tblgrade tbody tr').eq(row);
        var grade = $r.find('td').eq(col).find("div").html();

        var data = { event : 'save-grade',
					 activity : $r.find('td').eq(col).attr('data-event'),
					 term : this.$term.val(),
					 period : this.$period.val(),
					 idno : $r.attr('data-idno'),
					 score : grade,
					 regid : $r.attr('data-reg'),
				     sched : this.$schedule.val(),
					 cgrde : $r.find('td.ClassStand').find('div').html(),
					 igrde : $r.find('td.InitialGrade').find('div').html(),
					 finl : $r.find('td.TransmutedGrade').find('div').html(),
					 hmrm : $r.find('td.InitialGrade').attr('data-homeroom'),
				   };

        var mysuccess = function(e){
            if(!e.error){

            }
        };
        console.log('save:'+$r.attr('data-idno'));
        if (grade != ''){ setAjaxRequest(this.mylink+"thisRequest",data,mysuccess,ME.error,'json',true,false); }
    },

    save_bulkgrade : function(row,col){

        var score = [];
        var $r = $('#tblgrade tbody tr').eq(row);

        $('#tblgrade tbody tr').each(function(){
            var g = $(this).find('td').eq(col).find('div').html();
            var eventid = $(this).find('td').eq(col).attr('data-event');

            if( (g != undefined || g != '') && eventid != 0  ) {
                if( g.trim() !=''){
                var d = {
                    regid : $(this).attr('data-reg'),
                    idno  : $(this).attr('data-idno'),
                    evnt  : eventid,
                    score : g.trim(),
					cgrde : $(this).find('td.ClassStand').find('div').html(),
                    grde  : $(this).find('td.InitialGrade').find('div').html(),
                    fnl   : $(this).find('td.TransmutedGrade').find('div').html(),
                    hmrm  : $(this).find('td.InitialGrade').attr('homeroom'),
                };
                score.push(d);
                }

            }
        });

        if( score.length == 0  ){
            return 0;
        }

        var data = { event : 'save-bulk-grade',
            term : this.$term.val(),
            period : this.$period.val(),
            sched : this.$schedule.val(),
            rawscore : score,
        };
        var mysuccess = function(e){
            if(!e.error){

            }
        };
        setAjaxRequest(this.mylink+"thisRequest",data,mysuccess,ME.error,'json',true,false);
    },

    compute : function(mode, row, col){

        var total      = 0;
		var totaltxn   = 0;
		var transcount = 0;
   
        var txt = $("#term").find('option:selected').text()
        var IsSHS = (  txt.trim().indexOf('School') > 0 ? false : true ) ;

        var comp =  $('#tblgrade tbody tr').eq(row).find('td').eq(col).attr('data-com');
        var parent =  $('#tblgrade tbody tr').eq(row).find('td').eq(col).attr('data-parent');

        switch(mode){

            case 'total':

                $('#tblgrade tbody tr').eq(row).find('td.eventScore[data-com='+ comp +']').each(function(){

                    var items = $(this).attr('data-items');
                    if(items>0){
						var xrw   = $(this).find("div").html();
                        var raw   = ((xrw!='')?strCast( $(this).find("div").html(), 'float' ):0);
						var ave   = ((raw>=0)?((raw/items) * 100):-1);
						var trans =  0;
						if(xrw == ''){
							$(this).attr('data-ave', '');
							$(this).attr('data-trans', '');
							return;
						}
						if($('#tbltransmutation').length){
							$('#tbltransmutation').find('[data-min]').each(function(){
								var dmin   = $(this).attr('data-min');
								var dmax   = $(this).attr('data-max');
								var dgrade = $(this).attr('data-grade'); 
                                               ave    = parseFloat(ave).toFixed(2);
							  //console.log(ave+'-'+dmin+':'+dmax);
								if(parseFloat(dmin) <= ave && parseFloat(dmax) >= ave){
								  trans     = parseFloat(dgrade);
						          totaltxn += parseFloat(dgrade);
                                  console.log(ave+'-'+dmin+':'+dmax+'='+dgrade);
								  transcount++;
                                  return false;
								}else{
                                  $(this).attr('data-notrans',1); 
                                }
							});
						}
                      //console.log(raw);
                        total    += raw;
                        /* TODO : This is a temporary condition  */
                        if( comp == 27 ){
                            $(this).attr('data-ave', Math.round(ave * 100) / 100 );
                            $(this).attr('data-trans', trans);
                        }else{
                            $(this).attr('data-ave', Math.round(ave));
							$(this).attr('data-trans', trans);
                        }
				        console.log('Event:'+trans);
                        /* END */
                    }
                });
                
				console.log('Parent:'+totaltxn+" TransCount:"+transcount);
                if(parent > 0){
                    $('#tblgrade tbody tr').eq(row).find('td.subTotalItems[data-com='+ comp +']').find("div").html(total);
                    $('#tblgrade tbody tr').eq(row).find('td.subTotalItems[data-com='+ comp +']').find("div").attr('data-trans',(totaltxn/transcount));
                }else{
                    $('#tblgrade tbody tr').eq(row).find('td.sTotalItems[data-com='+ comp +']').find("div").html(total);
                    $('#tblgrade tbody tr').eq(row).find('td.sTotalItems[data-com='+ comp +']').find("div").attr('data-trans',(totaltxn/transcount));
                }

                ME.compute('percent',row,col);

            break;

            case 'percent':

                var total    = 0;
				var totaltxn = 0;
                var no_items = 0;
                var percent    =  (total/no_items) ;
                var percenttxn =  (totaltxn/no_items);
                
				if (IsSHS) {
                    $('#tblgrade tbody tr').eq(row).find('td.eventScore[data-com='+ comp +']').each(function(){
                        var items = $(this).attr('data-items');
						var xave  = $(this).attr('data-ave');
						if(xave==''){
						  return;	
						}
                        if(items>0){
                            total    += strCast( $(this).attr('data-ave') , 'float') ;
                            totaltxn += strCast( $(this).attr('data-trans') , 'float') ;
                            no_items++;
                        }
                    });
				    percent    =  (total/no_items) ;
                    percenttxn =  (totaltxn/no_items);
                    console.log('Component:'+totaltxn);
					console.log('Percent:'+percenttxn);
				
                  //alert(percenttxn);
                }else{

                    var $row =  $('#tblgrade tbody tr').eq(row).find((parent>0? 'td.subTotalItems':'td.sTotalItems')+'[data-com="'+ comp +'"]');
                    var items =  strCast($row.attr('data-items'),'float');

                    total      = strCast($row.find("div").html(),'float');
                    totaltxn   = strCast($row.find("div").attr('data-trans'),'float');
                    totaltxn   = ((totaltxn==undefined || isNaN(totaltxn))?0:totaltxn);
                    percent    = total/items * 100 ;
                    percenttxn = totaltxn;
                    console.log('Component:'+total+':'+items+'-'+totaltxn);
					console.log('Percent:'+percenttxn);
                }
				
                var transevent = $('[data-transevent]').attr('data-transevent');
				    transevent = ((transevent==undefined ||  transevent=='')?'0':transevent);
				
				if(transevent=='1'){
					percenttxn = ((percenttxn==undefined || isNaN(percenttxn))?'0.00':percenttxn);
					$('#tblgrade tbody tr')
						.eq(row)
						.find((parent>0? 'td.subPercentage':'td.sPercentage')+'[data-com="'+ comp +'"]')
						.find("div")
						.html(format('#0.00',percenttxn));
                }else{
					percent = ((percent==undefined || isNaN(percent))?'0.00':percent);
					$('#tblgrade tbody tr')
						.eq(row)
						.find((parent>0? 'td.subPercentage':'td.sPercentage')+'[data-com="'+ comp +'"]')
						.find("div")
						.html(format('#0.00',percent));
                }
				
                if(parent > 0){
                    ME.compute('sub-weight',row,col);
                }else{
                    ME.compute('weight',row,col);
                }

            break;

            case 'sub-weight':

                var $perct = $('#tblgrade tbody tr').eq(row).find('td.subPercentage[data-com="'+ comp +'"]');
                var $td =  $('#tblgrade tbody tr').eq(row).find('td.subWeight[data-com="'+ comp +'"]');

                var w =  strCast($td.attr('data-weight'),'float') / 100 ;
                var p =  0

                p = $perct.find("div").html()
                w = (p * w);

                $td.find("div").html(w.toFixed(2));
                ME.compute('weight',row,col);

            break;

            case 'weight':

                if(parent > 0){
                    var $td =  $('#tblgrade tbody tr').eq(row).find('td.sWeight[data-com="'+ parent +'"]');
                    var ave = 0;
                    $('#tblgrade tbody tr').eq(row).find('td.subWeight').each(function(){
                        if($(this).attr('data-parent') == parent){
                            ave +=  strCast( $(this).find('div').html(),'float');
                        }
                    });

                    var w =  strCast($td.attr('data-weight'),'float') / 100 ;
                    ave = (ave * w)
                    $td.attr('data-value',format ('#0.00',ave));
                    $td.find("div").html(format ('#0.00',ave));
					
                } else{
                    var $perct = $('#tblgrade tbody tr').eq(row).find('td.sPercentage[data-com="'+ comp +'"]');
                    var $td =  $('#tblgrade tbody tr').eq(row).find('td.sWeight[data-com="'+ comp +'"]');
                    var w =  strCast($td.attr('data-weight'),'float') / 100 ;
                    var p =  0
                    p = $perct.find("div").html()
                    w = (p * w);
                    $td.attr('data-value',format ('#0.00',w));
                    $td.find("div").html(format ('#0.00',w));
                }



                ME.compute('grade',row,col);

            break;

            case 'grade':

                var $td       = $('#tblgrade tbody tr').eq(row).find('.InitialGrade');
                var $cs       = $('#tblgrade tbody tr').eq(row).find('.ClassStand');
				var grade     = 0;
				var csgrade   = 0;
				var csoverall = 0;
               
                $('#tblgrade tbody tr').eq(row).find('.sWeight').each(function(){
					    var cselem     = 0;
					    var comid      = $(this).attr('data-com');
					    var xweight    = $(this).attr('data-weight');
						var xvalue     = ((xweight!=='' && xweight!=undefined)?strCast(xweight,'float'):0)/100;
						var notexempt  = $('.sWeight[data-com="'+comid+'"][data-value!="0.00"]').length;
				  		if( $(this).has("div") ){
							grade   += strCast($(this).find("div").html(),'float');
							cselem  += strCast($(this).find("div").html(),'float');
						} else {
							grade   += strCast($(this).html(),'float');
							cselem  += strCast($(this).html(),'float');
						}
						
						if(notexempt>0){
						  csoverall += ((xweight!=='' && xweight!=undefined)?strCast(xweight,'float'):0);
						  csgrade   += cselem;
						}
                });
                /*
                if(IsSHS){
                    grade = Math.round(grade);
                }
                */
				csgrade = (csgrade/csoverall)*100;
				
                $td.find("div").html(format('#0.00',grade));
                $cs.find("div").html(format('#0.00',csgrade));
                ME.compute('transmutation',row,col);    
                                
            break;

            case 'transmutation':

                var $td =  $('#tblgrade tbody tr').eq(row).find('.InitialGrade');
                var fnal = $td.find('div').html();
                var tmt = "";
                var remarks = "";
                var zerobase = ME.$schedule.find('option:selected').attr('data-zerobased');
                
                if( zerobase == '0'){
                    $('#tbltransmutation tbody tr').each(function(){
                        var mn = strCast($(this).find('td.min').html(), 'float') ;
                        var mx = strCast($(this).find('td.max').html(), 'float') ;
    
                        if( fnal >= mn && fnal <= mx ){
                            tmt = $(this).find('td.grade').html();
                            return false;
                        }
                    });                
                }else{                    
                    tmt = strCast(fnal, 'float');
                }                                

                $('#grading-system-table').find('.table tbody tr').each(function(){
                    var mn = strCast($(this).find('td.min').html(), 'float');
                    var mx = strCast($(this).find('td.max').html(), 'float');
                    //console.log(tmt+' - '+mn+':'+mx);
                    if( tmt.toFixed(2) >= mn && tmt.toFixed(2) <= mx ){
                        remarks = $(this).find('td.letter').html();
                        return false;
                    }
                });

                $('#tblgrade tbody tr').eq(row).find('td.TransmutedGrade').find('div').html(tmt);
                $('#tblgrade tbody tr').eq(row).find('td.Description').find('div').html(remarks);

            break;

            default: break;
        }
    },

    editorChange: function(e){

        ME.Invalid = false;
        ME.IsChanged = true;

        var col = this.$editorText.attr('data-col');
        var row = this.$editorText.attr('data-row');
        var $r =  $('#tblgrade tbody tr').eq(row).find('td').eq(col);
        var id = $r.attr('data-id');
        var cls = $r.attr('data-class');
        var isConduct = $r.hasClass('conductScore');
        var ixConduct = $r.hasClass('xconductScore');

        if( !isConduct && !ixConduct){

          //var items = strCast( $r.attr('data-items') , 'int') ;
		    var entry = e.target.value;
            var items = strCast( $r.attr('data-items') , 'float') ; //Update for to allow decimal in items
            if( e.target.value > items || !$.isNumeric( e.target.value ) ){
                msgbox('warning','Ooops, Invalid raw score');
                ME.Invalid = true;
                return false;
            }
            
			entry  = ((entry=='')?'':parseFloat(entry).toFixed(2));
			if($r.has("div")){
                $r.find('div').html(entry);
            }else{
                $r.html(entry);
            }

            ME.compute('total',row,col);
            ME.save_grade(row,col);
            //ME.save_final(row,col);

            var ud = parseInt(row)+1;
            var $nextrow = $('#tblgrade tbody tr').eq(ud).find('td').eq(col);
            this.cell_focus($nextrow);

        }else{

            var result= ME.conduct(e,'validate');
            if ( !result.valid){
                msgbox('warning','Ooops, Invalid conduct grade');
                ME.Isbusy = false;
                return false;
            }

            if($r.has("div")){
                $r.find('div').html(e.target.value);
            }else{
                $r.html(e.target.value );
            }

            ME.conduct(e, 'compute-conduct');
            var ud = parseInt(row)+1;

            var $nextrow = $('#tblgrade tbody tr').eq(ud).find('td').eq(col);
            this.cell_focus($nextrow);

        }

        this.hideactiveText();
        //ME.Isbusy = false;
        e.preventDefault();
    },
    editorFocusUp: function(e){ e.target.select();},
    editorMouseUp: function(e){ return false; },
    hideactiveText:function(e){ this.$editor.hide(); },
    activeText: function(e){

        var presskey = "";
        var ro = $(e.target).parent().index();
        var co = $(e.target).index();
        var isConduct = $(e.target).hasClass('conductScore');
        var ixConduct = $(e.target).hasClass('xconductScore');

        if(e.type == "keydown"){

            var gotonextcell = function(row,col){
               var $nextrow =  $('#tblgrade tbody tr').eq(row).find('td').eq(col);
               ME.cell_focus($nextrow);
               return false;
            };

            if(e.keyCode === 37 || e.keyCode === 39 ){
                var lr = parseInt(co + (e.keyCode === 37 ? -1 : 1));
                return gotonextcell(ro,lr);
            }

            if(e.keyCode === 38 || e.keyCode === 40 ){
                var ud = parseInt(ro + (e.keyCode === 38 ? -1 : 1));
                return gotonextcell(ud,co);
            }

            if( e.keyCode === 46){
                if($(e.target).hasClass('editable')){
                    $(e.target).find("div").html('');

				if( isConduct || ixConduct){
                        ME.conduct(e,'erase');
                    }else{
                        ME.erase_grade(ro,co);
                        ME.compute('total',ro,co);
                    }
                }
                return false;
            }

            /* code from keypress */
            //if ( (e.shiftKey || (e.charCode < 45 || e.charCode > 57) ) && e.keyCode !== 13  ) return false;
            if(e.keyCode == 27) { return false; }
        }

        var top  = window.pageYOffset || document.documentElement.scrollTop;
        if($(e.target).hasClass('editable')){

            var items = $(e.target).attr('data-items');

            if( (items == 0 || items == undefined ) && !isConduct && !ixConduct)  {
                msgbox('warning','sorry, there are no current event setup.','No Event Setup');
                return false;
            }

            var add_width = -10;
            var data = $(this).data();
            var currval = "";

            currval = $(e.target).find('div').html().trim();

            if(presskey != '') currval = presskey;
            var pos =  $(e.target).offset();
            var h = parseInt( $(e.target).height()-1  ) ;
            var w = parseInt( $(e.target).width()+add_width ) ;
           this.$editor.css({'position': 'fixed' , 'top' : pos.top - top +"px",'left' : pos.left +"px",});
           this.$editorText.val('');
           this.$editorText.height(h);
           this.$editorText.width(w);
           this.$editor.show();
           this.$editorText.attr('data-col',co);
           this.$editorText.attr('data-row',ro);
           if( currval != '' && currval != undefined ) this.$editorText.val(currval.trim() );
           this.$editorText.focus();
           this.$editorText.select(false);
		   
        } else {
            this.hideactiveText();
        }

        if(e.keyCode == 13){
            e.preventDefault();
        }
    },
};
