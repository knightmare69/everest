function scrollme(e){
    var x = e.scrollLeft;
    document.getElementById('mytblheader').scrollLeft =x;
    
    var y = e.scrollTop;
    if(document.getElementById('studentcols')){
        document.getElementById('studentcols').scrollTop =y;
    }
    

    ME.hideactiveText();
}

var copy_ready = 0;
var copy_text = "";
var explanation = "";



var ME = {
    
    err : function(){ msgbox('warning','Ooops, problem found. Please try again later.','Invalid'); progressBar('off'); },
    initplugins : function(){ $('.bs-select').selectpicker({ iconBase: 'fa', tickIcon: 'fa-check', size : 8 }); },

    initAlerts : function(container, alert, message, closable, reset, focus,icon, time ) {

        Metronic.alert({
            container: container  , // alerts parent container(by default placed after the page breadcrumbs)
            place: 'prepend', // append or prepent in container
            type: alert,  // alert's type
            message: message,  // alert's message
            close: closable, // make alert closable
            reset: reset, // close all previouse alerts first
            focus: focus, // auto scroll to the alert after shown
            closeInSeconds: time, // auto close after defined seconds
            icon: icon // put icon before the message
        });
    },
    erase: function(e){

        var idno = $(e.target).closest('tr').attr('data-idno');
        var type = $(e.target).find('div').html();
        var date = $(e.target).attr('data-date');
        
        setAjaxRequest( ME.mylink+"event", {'event': 'erase-att', 'date': date, 'idno' : idno, 'type': type },
			function(result) {
				if (result.error) {
					showError(result.message);
				} else {
					showSuccess(result.message);
                    progressBar('off');
				}
			}
		);
        progressBar('off');
        
                
    },
    post : function(e){

      var _checked = $('#tblstudents').find('.chk-child:checked');

      var validatepost = function($r){

            var flg = true;

            $r.find('td.conductScore').each(function(){

                score = $(this).html();

                if(score == '' || score == 0 || score == '0' ){
                    flg = false ;
                    return false;
                }
            });

            return flg;
      };


      var execution = function(){

            var ids = [];

            _checked.each(function(e){
                var _tr = $(this).closest('tr') ;
                var _d = _tr.data();
                var id = _tr.attr('data-idno');

                if(id !== 0 || id !== undefined){
                    if (validatepost(_tr)) {
                        var x = { idno : id, };
                        ids.push(x);
                    }
                }
            });

                if(ids.length >= 1){

                    var data = {
                        event : 'post',
                        term : ME.$term.val(),
                        type : $('#type').val(),
                        section : ME.$section.val(),
                        period : ME.$period.val(),
                        stud : ids,
                    }

                    setAjaxRequest(
                        base_url + page_url + 'event', data,
                        function(r) {
                            if (r.error) {
                                msgbox('error','Sorry, unable to post conduct');
                            } else {
                                //showSuccess(result.message);
                                ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                                ME.initstudents();
                            }
                        }
                    );
                    progressBar('off');
                } else{
                    msgbox('error', 'Sorry, there are no qualified students to post at this moment. Please complete student class record.');
                }

        } ;


        if (_checked.length > 0 ) {

          $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
    			content : "Posting student conduct will enable the record lock features.. <br/> You are no longer able to create, edit and delete a conduct. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     $.SmartMessageBox({
            			title : "<span class='text-warning'><i class='fa fa-lock'></i> Do you wish to POST the selected student(s)?</span>",
            			content : "Do you certify that all the conduct grades per student in this class record are true and correct?",
            			buttons : '[No][Yes]'
            		}, function(ButtonPressed) {
            			if (ButtonPressed === "Yes") {
            			     execution(true);
            			}
            		});
    			}else{
    			     $(this).remove();
    			}
    		});
        }else{
            alert('Please select student to post');
        }
        e.preventDefault();

    },

    saveRecord : function(row,col){

        var $r = $('#tblstudents tbody tr').eq(row).find('td').eq(col);

        var data = {
            event : 'save-att',
            term : $('#term').val(),
            type : $('#type').val(),
            section : $('#section').val(),
            date : $r.attr('data-date'),
            period : $('#period').val(),
            idno : $r.parent().attr('data-idno'),
            score : $r.find('div').html(),
            idx : $r.find('td').eq(col).attr('data-id'),
            explain : explanation
        };

        var mysuccess = function(e){
            if(!e.error){
                //console.log('conduct grade successfully saved!');

            }
        };
        setAjaxRequest(ME.mylink+"event",data,mysuccess,ME.error,'json',true,false);

    },

    compute : function($row){
        var score = 0;
        var i = 1;
        $row.find('td.conductScore').each(function(){
            score += strCast( $(this).html(),'float' );
            i++;
        });

        var ave = (score / i);
        var v = ME.validGrade(ave);
        if(v.valid){
            $row.find('td.total').html(ave.toFixed(2));
            $row.find('td.conduct').html(v.letter);
            $row.find('td.remarks').html(v.remarks);
        }

    },

    validGrade : function(grade)
    {
        var v = { valid : false, msg : 'invalid conduct grade', letter : '', desc : '', remarks : '', };
         $('#tblgradesystem tbody tr').each(function(){
            var val = $(this).find('td.min').html();
            if(  grade.toLowerCase() == val.toLowerCase() ){
                v = {
                    valid : true,
                    msg : 'accepted',
                    letter : $(this).find('td.letter').html().trim(),
                    desc : $(this).find('td.desc').html().trim(),
                    remarks : $(this).find('td.remarks').html().trim(),
                };

                return v;
            }

        });

        return v;

    },

    initstudents: function(e){

        var s = $('#section').val(), p = $('#period').val(), t = $('#term').val(), y = $('#month').val();

        var data = { event : 'students', term : t,section : s, period : p, month: y };

        var success = function(e){
            $('#studentlist-container').html(e.content);
            $('#section').html(e.sections);
            progressBar('off');
        }

        if(s == '' || s == '-1' ){
            this.initloads();
            alert('Please select a section');
            return false;
        }

        if(p == '' || p == '-1' ){
            alert('Please select a period');
            return false;
        }

        if(t == '' || t == '-1'){
            alert('Please select a academic year and term');
            return false;
        }
        
        localStorage.setItem('period', p);

        $('#studentlist-container').html("<h4> <i class='fa fa-spin fa-spinner'></i> <i class='font-sm'>Please wait while loading...</i>  </h4>");
        setAjaxRequest(this.mylink+"event",data,success,ME.err,'json',true);
    },

    initservices: function() {

        var $editor = $('#tbleditor');
        var $editorText = $('#tbleditor').find('input');

        $editorText.on('blur',$.proxy(this.hideactiveText,this));
        $editorText.on('focus',$.proxy(this.editorFocusUp,this));
        $editorText.on('mouseup',$.proxy(this.editorMouseUp,this));
        $editorText.on('change',$.proxy(this.editorChange,this));

        $editorText.on('keypress',function(e){
            
            
            
            if( e.keyCode === 27) {
                var c = $(e.target).attr('data-col'), r =  $(e.target).attr('data-row');
                var $nr = $('#tblstudents tbody tr').eq(r).find('td').eq(c);
                //ME.compute(r,c);
                ME.hideactiveText();
                ME.cell_focus($nr);
            }else if (e.keyCode == 13 || e.keyCode == 9 ){
                if( $(e.target).attr('data-dirty') =='0' ){
                    ME.editorChange(e);
                }
                $(e.target).attr('data-dirty',0);
                console.log('keypress');
            }
                        
            $(e.target).attr('data-dirty',1);
        });

        $('body').on('click','.btn, .reload, .clink', $.proxy(this.menu,this));
        $('body').on('change','#section, #period, #month', $.proxy(this.initstudents,this));
        $('body').on('click','#tblstudents tbody td', $.proxy(this.focus,this));
        $('body').on('keydown','#tblstudents tbody td', $.proxy(this.captureKey,this));
        $('body').on('change','#type, #term', $.proxy(this.term_changed,this));
        $('body').on('click','.profile-usermenu a', $.proxy(this.help,this));
    },

    captureKey: function(e){

        var presskey = "";
        var r = $(e.target).parent().index(), c = $(e.target).index();

        console.log(e.keyCode);
        
        switch(e.keyCode){

            case 37: //left

                var nc = parseInt(c)-1;
                var $nextrow = $('#tblstudents tbody tr').eq(r).find('td').eq(nc);
                this.focus($nextrow);
                return false;
            break;

            case 38: //up
                var nr = parseInt(r)-1;
                var nc = parseInt(c)-1;
                var $nextrow = $('#tblstudents tbody tr').eq(nr).find('td').eq(c);
                this.focus($nextrow);
                return false;
            break;

            case 39: //right

                var nc = parseInt(c)+1;
                var $nextrow = $('#tblstudents tbody tr').eq(r).find('td').eq(nc);
                this.focus($nextrow);
                return false;
            break;

            case 40: /*down*/

                var nr = parseInt(r)+1;
                var nc = parseInt(c)+1;
                var $nextrow = $('#tblstudents tbody tr').eq(nr).find('td').eq(c);
                this.focus($nextrow);
                return false;

            break;

            case 46: /*del, bs*/
            case 8:

                if($(e.target).hasClass('editable')){
                    ME.erase(e);
                    $(e.target).find('div').html('');                    
                }
                return false;
            break;

            case 17: //ctrl
                copy_ready = 1;
                return false;
            break;

            case 13: //Enter
            
                
            break;
            
            case 67 : //c
                 if ( copy_ready == '1' ) {
                    copy_text = $(e.target).find('div').html();   
                    copy_ready =0 ;
                    return false;
                 } 
                 
            break;                        
            
            case 86 : //v
                 
                 if ( copy_ready == '1' ) {
                    $(e.target).find('div').html(copy_text);   
                    copy_ready =0 ;
                    
                    bootbox.prompt("Please enter your remarks.",function(o){            
                        explanation = o;           
                        $(e.target).attr('title',explanation );       
                        ME.saveRecord(r,c);            
                    });                                        
                     
                    return false;
                 }
                 
            break; 
            
            case 65: //a
            case 84: //t
            default :
                var charCode = (e.which) ? e.which : e.keyCode
                
                if( charCode == 65 || charCode == 84 ){
                    // let proceed
                }else{
                    if (charCode > 31 && (charCode < 48 || charCode > 57)){
                        return false;
                    }    
                }
                
                presskey = String.fromCharCode(e.keyCode);
                copy_text = "";
                copy_ready = 0;
                
            break;
        }

        var top  = window.pageYOffset || document.documentElement.scrollTop;
        if($(e.target).hasClass('editable')){

            var $editor = $('#tbleditor');
            var $editorText = $('#tbleditor').find('input');

            var add_width = -10;
            var data = $(this).data();
            var currval = "";

            currval = $(e.target).find('div').html();

           if(presskey != '') currval = presskey;
           var pos =  $(e.target).offset();
           var h = parseInt( $(e.target).height()-1  ) ;
           var w = parseInt( $(e.target).width()+add_width ) ;

           $editorText.attr('data-col',c);
           $editorText.attr('data-row',r);
           $editorText.attr('data-dirty',0);
           
           $editorText
                .height(h)
                .width(w)
                .val('');

           $editor
                .css({'position': 'fixed' , 'top' : pos.top - top +"px",'left' : pos.left +"px",})
                .show();

           

           $editorText
                .focus()
                .select(false)
                .val('');
                

            if( currval != '' && currval != undefined ) $editorText.val(currval.trim() );

        } else {
            this.hideactiveText();
        }
        e.preventDefault();

    },

    editorChange: function(e){

        var $editor = $('#tbleditor');
        var $editorText = $('#tbleditor').find('input');

        var col = $editorText.attr('data-col');
        var row = $editorText.attr('data-row');
        var $r =  $('#tblstudents tbody tr').eq(row).find('td').eq(col);
        var id = $r.attr('data-id');
        var cls = $r.attr('data-class');

        var grade = e.target.value;

        var result= ME.validGrade(grade);
        if ( !result.valid){
            msgbox('warning','Ooops, Invalid input ');
            return false;
        }

        $r.find('div').html(e.target.value );

        //ME.compute($r.parent());
         bootbox.prompt("Please enter your remarks.",function(o){
            
            explanation = o;
            
            ME.saveRecord(row,col);
            var ud = parseInt(row)+1;
            var $nextrow = $('#tblstudents tbody tr').eq(ud).find('td').eq(col);
            ME.focus($nextrow);
            ME.hideactiveText();
            $editorText.attr('data-dirty',0);
         });
       
        //e.preventDefault();
    },

    editorFocusUp: function(e){ e.target.select();},
    editorMouseUp: function(e){ return false; },
    hideactiveText:function(e){
        $('#tbleditor').hide();
        
        $('#tbleditor').find('input').attr('data-dirty',0);
        
        //e.preventDefault();
    },

    focus : function(e){
        var idx = 0;
        $('#tblstudents tbody td').removeClass('cell-focus');
        if(e.target){
            if($(e.target).hasClass('editable')){
            $(e.target).addClass('cell-focus');
            $(e.target).focus();
            }
            
            var $r = $(e.target).closest('tr');
            idx = $(e.target).closest('tr').index();
        }else{
            
            if($(e).hasClass('editable')){
             $(e).addClass('cell-focus');
             $(e).focus();
            }
           
            var $r = $(e).closest('tr');
            idx = $(e).closest('tr').index();
        }

        

        $('#tblstudents tbody tr').removeClass('active-row');
        $('#tblnames tbody tr').removeClass('active-row');
        
        $r.addClass('active-row');
        
        $('#tblnames tbody tr').eq(idx).addClass('active-row');
        
        
        //$('.profile-usertitle-name').html($r.attr('data-name'));
        //$('.profile-usertitle-job').html($r.attr('data-idno'));
        //$('.img-responsive').attr('src', $r.find('td.pic').find('img').attr('src'));
        //e.preventDefault();
        
    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'refresh':  this.initstudents();  break;
            case 'post': this.post(e); break;
        }
    },

    term_changed : function(e){

        var selected = 0;

        if(e ==undefined){
            selected = this.$term.val();
        }else{
            selected = $(e.target).val() ;
        }

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select an academic year & term','No AY Term');
            return false;
        }

        this.initloads();

    },

    initloads : function(){

        var term = $('#term').find('option:selected').text()
        var x = term.trim().indexOf('School');
        var t = $('#type').val();
        var tid = $('#term').val();

        var p = localStorage.getItem('period');
        console.log(p);
        if(p !='' && p != undefined){
            this.$period.val(p);
        }else{
            this.$period.val(1);
        }
        
       this.initstudents();

    },


    init : function() {
        this.mylink = base_url+page_url;
        this.$term = $('#term');
        this.$section = $('#section');
        this.$period = $('#period');
        this.initplugins();
        this.initservices();
        this.initloads();
    },
};