var ME = {
    init : function() {
        this.mylink = base_url+page_url;
        this.$term = $('#term');
        this.$formation = $('#formations');
        this.$container = $('#formations').find('.scroller');
        this.initplugins();
        this.addlistener();
        if( this.$term.val() != '-1' || this.$term.val() ){ this.term_changed();}
    },
    err : function(){  progressBar('off'); },
    initplugins: function(){
        $('.bs-select').selectpicker({ iconBase: 'fa', tickIcon: 'fa-check' , size : 8});
    },

    initpolicy: function(e){
      var i =0;
      var d = $(e.target).closest('tr').data();

      $('.components').html('');
      $('#form-title').html(d.code + ' ' + d.name );

      $('#idx').val(d.id);
      $('#subid').val(d.subid);
      $('#cbopolicy').val('-1').change();

       var data = { event  : 'policy-setup', term : $('#term').val() ,sched : d.id, shs : d.shs  };
       var success = function(e){
            $('#polcontainer').html(e.content);
            progressBar('off');
        };
        
        setAjaxRequest(this.mylink+"thisRequest",data,success,ME.err,'json',true,false);

        $('#form_modal1').modal('show');

    },

    initcbopolicy: function(e){
        var $r = $(e.target).closest('tr');
        var sel = $r.find('td.policy').attr('data-policy') ;

        $('#cbopolicy').val(sel).change();
    },

    addlistener: function() {
        $('body').find(this.$term).on('change',$.proxy(this.term_changed, this));
        $('body').on('click','.view-subject', $.proxy(this.initpolicy,this));
        $('body').on('click','.btn', $.proxy(this.menu,this));
        $('body').on('change','.periods', $.proxy(this.period_changed,this));
        $('body').on('click','#tblschedule > tbody > tr', $.proxy(this.display_policy,this));
        //$('body').on('click','#tblsummary > tbody > tr', $.proxy(this.initcbopolicy,this));
    },

    display_policy : function(e){

        var $r = $(e.currentTarget);
        var selected = $r.attr('data-id') ;
        var shs = $r.attr('data-shs') ;
        var $holder = $('#policyinfo');

        if(selected == undefined || selected == '') return false;

        var data = { event  : 'get-policy', sched : selected, shs : shs  };

        var success = function(e){
            $holder.html(e.content);
            progressBar('off');
        };
        $holder.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        setAjaxRequest(this.mylink+"thisRequest",data,success,ME.err,'json',true,false);

    },
    
    period_changed : function(e){

        var selected = $(e.target).val() ;
        var $holder = $(e.target).parent().find('.components');

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select a policy','Policy Required!');
            return false;
        }

        var data = { event  : 'get-components', policy : selected, };

        var success = function(e){
            $holder.html(e.content);
            progressBar('off');
        };

        $holder.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        setAjaxRequest(this.mylink+"thisRequest",data,success,ME.err,'json',true,false);

    },

    initAlerts : function(container, alert, message, closable, reset, focus,icon, time ) {

        Metronic.alert({
            container: container  , // alerts parent container(by default placed after the page breadcrumbs)
            place: 'prepend', // append or prepent in container
            type: alert,  // alert's type
            message: message,  // alert's message
            close: closable, // make alert closable
            reset: reset, // close all previouse alerts first
            focus: focus, // auto scroll to the alert after shown
            closeInSeconds: time, // auto close after defined seconds
            icon: icon // put icon before the message
        });
    },


    policysetup: function(e){
        var q = [];

        $('#tblsummary tbody tr').each(function(){
            var pol = $(this).find('td.policy').attr('data-policy');
            if( pol != undefined && pol != '-1' && pol != "" ){
                var d = {
                    sched : $('#idx').val(),
                    subject : $('#subid').val(),
                    quarter : $(this).attr('data-period'),
                    policy : pol
                };
                q.push(d);
            }
        });

        if(q.length > 0){

            var success = function(e){
                if(e.error){
                    ME.initAlerts('#policybody','warning',e.message,true,true,true,'warning',10)
                }else{
                    ME.initAlerts('#policybody','success',e.message,true,true,true,'check',5)
                }
            };

            var err = function(e){
                ME.initAlerts('#policybody','danger','Ooops, something went wrong... Please try again later',true,true,true,'warning',5)
            };

            ME.initAlerts('#policybody','info','Please wait while saving...',true,true,true,'spinner fa-spin',0)
            var data = {
                event : 'set-policy',
                setup : q,
            };
            setAjaxRequest(this.mylink+"thisRequest",data,success,err,'json',true,false);

        }
    },

    menu: function(e){
        var data = $(e.target).data();
        switch (data.menu) {
            case 'save-policy': this.policysetup(e); break;
            case 'use-policy': this.apply_policy(e); break;
            case 'apply-to-all': this.apply_policy(e); break;
            case 'refresh': ME.term_changed(); break;
            default: break;
        }
    },

    apply_policy : function(e){

        var $r = $('#tblsummary tr.active-row');
        var period = $r.attr('data-period');

        var text = $('#cbopolicy').find('option:selected').text();
        var policyID = $('#cbopolicy').val();

        var i = 1;

        if(policyID == undefined || policyID =='-1' ){
            ME.initAlerts('#policybody','danger','Ooops, Please select a policy',true,true,true,'warning',5)
            return false;
        }

        if($(e.target).attr('data-mode') =='one'){

            $('#tblsummary tr').each(function(){

                if( $(this).attr('data-period') == period  ){
                    $(this).find('.policy').attr('data-policy',policyID);
                    $(this).find('.policy').html(text);
                   return;
                }
            });
        }else{
            $('#tblsummary tr').each(function(){
               $(this).find('.policy').attr('data-policy',policyID);
               $(this).find('.policy').html(text);
            });
        }
    },
    policy_changed : function(e){

        var selected = $(e.target).val() ;
        var $holder = $(e.target).parent().find('.components');

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select a policy','Policy Required!');
            return false;
        }        
        var data = { event : 'component', idx : selected};

        var success = function(e){
            $holder.html(e.content);
            progressBar('off');
        };

        var err = function(){
            msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
            progressBar('off');
        };

        $holder.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        setAjaxRequest(base_url + "grading-system/policy-setup/thisRequest",data,success,err,'json',true);
    },

    term_changed : function(e){
        var selected = 0;
        if(e ==undefined){
            selected = this.$term.val();
        }else{
            selected = $(e.target).val() ;
        }

        if(selected == undefined || selected == '-1'){
            msgbox('warning','Please select an academic year & term','No AY Term');
            return false;
        }

        var data = {
          event : 'schedules',
          term : selected,
        };

        var success = function(e){
            ME.$container.html(e.content);
            progressBar('off');
        };

        var err = function(){
            msgbox('warning','Ooops, problem found. Please try again later.','Invalid');
            progressBar('off');
        };

        this.$container.html("<h4> <i class='fa fa-spin fa-spinner'></i> Please wait while loading... </h4>");
        setAjaxRequest(this.mylink+"thisRequest",data,success,err,'json',true);
    },
};