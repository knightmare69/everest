var Login = function() {

	
    function login() {

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        jQuery.ajax({
            url: base_url+page_url+'login',
            dataType: 'json',
            data: $('.form_crud').serialize(),
            type: 'POST',
            async: true,
            beforeSend: function() {
                $('.btn_login').html('Loading...');
            },
            success: function(data) {
               alert(data.message);
               $('.btn_login').html('Login');
               if (data.error == false) {
                    window.location.reload();
               }
            },
            error: function(error) {
                $('.btn_login').html('Login');
            }
        });
    }
	

	return {
		init: function() {
			$('.btn_login').click( function() {
                login();
            });
		}
	}

}();	