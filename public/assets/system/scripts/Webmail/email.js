var emailto;
var emailcc;
var emailbc;
var replyto    = [];
var replycc    = [];
var replybc    = [];
var msgtrail   = ''

var isreply    = 0;

var attachfile = [];
var attachlink = [];

var datalink  = '';
var datatable = '';
var page      = 1; 
var last      = 10;
var itotal    = 0;
var event     = 'content';

$(document).ready(function() {	
  $('body').on('click','.btn-compose,.btn-reply',function(){
	 isreply  = (($(this).is('.btn-reply'))?1:0);
	 msgtrail = $('.inbox-view').html();
	 tmpdata = {};
	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event=compose',tmpdata,
			function(r)
			{
			   if(r.success){	 
				 $('.xmain').html(r.content); 
				 if(isreply==0){
					msgtrail = "";
					getcontacts(); 
				 }else{
					msgtrail = '<br/><br/><br/><div class="col-sm-12">'+msgtrail+'</div>'; 
					for(i=0;i<replyto.length;i++){
					  var tmptext = $('[name="to"]').val();
					  if(tmptext.indexOf(replyto[i].address)>=0){continue;}
                          tmptext = ((tmptext.trim()!='')?tmptext+",":"")+replyto[i].address;		
                      $('[name="to"]').val(tmptext);						  
					}
                    for(i=0;i<replycc.length;i++){
					  var tmptext = $('[name="cc"]').val();
					  if(tmptext.indexOf(replycc[i].address)>=0){continue;}
                          tmptext = ((tmptext.trim()!='')?tmptext+",":"")+replycc[i].address;		
                      $('[name="cc"]').val(tmptext);						  
					}
                    for(i=0;i<replybc.length;i++){
					  var tmptext = $('[name="bcc"]').val();
					  if(tmptext.indexOf(replybc[i].address)>=0){continue;}
                          tmptext = ((tmptext.trim()!='')?tmptext+",":"")+replybc[i].address;		
                      $('[name="bcc"]').val(tmptext);						  
					}
				 }
				 $('.summernote').summernote({height: 240});
                 $('.summernote').summernote('code', msgtrail);
			   }else{
				 $('.xmain').html('');  
			   }	
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save email!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  $('body').on('click','.inbox-nav>[data-link]',function(){
	 $('.inbox-nav').find('[data-link]').removeClass('active');
	 $(this).addClass('active');
	 page      = 1;
	 datalink  = $(this).attr('data-link');
	 datatable = $(this).attr('data-table');
	 event     = ((datalink!='')?'content':'tbrow');
	 tmpdata   = {'key':((datalink=='')?datatable:datalink),'page':page};
	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event='+event,tmpdata,
			function(r)
			{
			   if(r.success){
				 $('.xmain').html(r.content); 
				 last   = $('[data-itemno]').last().attr('data-itemno');
				 itotal = $('[data-itemno]').last().attr('data-total');
				 $('.lastrec').html(last);
			   }else{
				 $('.xmain').html('');  
				 $('.pagination-info').addClass('hidden');
			   }	
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load email!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  $('body').on('click','.btn-prev,.btn-next',function(){
	 if($(this).is('.btn-prev')){
		page = ((page>1)?(page--):1); 
	 }else{
		page++; 
	 }
	 event     = ((datalink!='')?'content':'tbrow');
	 tmpdata   = {'key':((datalink=='')?datatable:datalink),'page':page};
	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event='+event,tmpdata,
			function(r)
			{
			   if(r.success){
				 $('.xmain').html(r.content);  
				 last   = $('[data-itemno]').last().attr('data-itemno');
				 itotal = $('[data-itemno]').last().attr('data-total');
				 $('.lastrec').html(last);
			   }else{
				 $('.xmain').html('');  
				 $('.pagination-info').addClass('hidden');
			   }	
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load email!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  $('body').on('click','tr[data-messageid]>.view-message',function(){
	 var msgid  = $(this).closest('tr').attr('data-messageid')
	 tmpdata   = {'key':((datalink=='')?datatable:datalink),'page':page,'mid':msgid};
	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event=view',tmpdata,
			function(r)
			{
			   if(r.success){
				 replyto  = r.header['reply'];  
				 replycc  = r.header['cc'];  
				 replybc  = r.header['bcc'];
                 msgtrail = r.content;			
				 $('.xmain').html(r.content); 
			   }else{
				 $('.xmain').html('');  
			   }	
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save email!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  $('body').on('click','.btn-unlink',function(){
     progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event=unlink',{},
			function(r)
			{
			   if(r.success){
				 window.location.reload();
			   }else{
				 confirmEvent('<i class="fa fa-warning text-danger"></i> '+r.content,function(){return;});	 
			   }	
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to unlink email!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  
  $('body').on('click','.btn-link',function(){
    $('#modal_linker').modal('show');
	$('#email').val('');
	$('#pass').val('');
	var form = $('#frmlinker');
	var error = $('.alert-danger', form);
	var success = $('.alert-success', form); 
	var handleTitle = function(tab, navigation, index) {
		var total = navigation.find('li').length;
		var current = index + 1;
		// set wizard title
		$('.step-title', $('#modal_linker')).text('Step ' + (index + 1) + ' of ' + total);
		// set done steps
		jQuery('li', $('#modal_linker')).removeClass("done");
		var li_list = navigation.find('li');
		for (var i = 0; i < index; i++) {
			jQuery(li_list[i]).addClass("done");
		}

		if (current == 1) {
			$('#modal_linker').find('.button-previous').hide();
		} else {
			$('#modal_linker').find('.button-previous').show();
		}
        
		if (current == 4) {
			$('#modal_linker').find('.button-next').hide();
			$('#modal_linker').find('.button-submit').hide();
		} else if (current == 3) {
			$('#modal_linker').find('.button-next').hide();
			$('#modal_linker').find('.button-submit').show();
		} else {
			$('#modal_linker').find('.button-next').show();
			$('#modal_linker').find('.button-submit').hide();
		}
		Metronic.scrollTo($('.page-title'));
	}
	
	$('#modal_linker').bootstrapWizard({
		'nextSelector': '.button-next',
		'previousSelector': '.button-previous',
		onTabClick: function (tab, navigation, index, clickedIndex) {
			return false;
		},
		onNext: function (tab, navigation, index) {
			success.hide();
			error.hide();

		  //if (xformvalidation(index) == false){return false; }
			handleTitle(tab, navigation, index);
		},
		onPrevious: function (tab, navigation, index) {
			success.hide();
			error.hide();

			handleTitle(tab, navigation, index);
		},
		onFirst: function (tab, navigation, index) {
			var total = navigation.find('li').length;
			var current = 1;
			var $percent = (current / total) * 100;
			$('#modal_linker').find('.progress-bar').css({width: $percent + '%'});
			handleTitle(tab, navigation, index);
		},
		onTabShow: function (tab, navigation, index) {
			var total = navigation.find('li').length;
			var current = index + 1;
			var $percent = (current / total) * 100;
			$('#modal_linker').find('.progress-bar').css({width: $percent + '%'});
			handleTitle(tab, navigation, index);
		}
	});
  });
  
  $('body').on('click','.btn-send',function(){
     save_email(true);
  });
  
  $('body').on('click','.btn-save',function(){
     save_email(false);
  });
  
  $('body').on('click','.btn-alink',function(){
     var tmpdata = $('#frmlinker').serialize();
 	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event=acctlink',tmpdata,
			function(r)
			{
			   $('.button-next').trigger('click');
			   $('.validation-msg').html(r.content);
			   setTimeout(function(){window.location.reload();},2000);
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to link to email!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  $('body').on('change','#grpid',function(){
	  var tmpval = $(this).val();
	  if(tmpval>=0){
		 $('#multi-append').select2('val', '');
		 $('#multi-append').addClass('hidden');
	  }else{
	     $('#multi-append').removeClass('hidden');
	  }	
  });
  
  $('body').on('change','#fileinput-file',function(){
	  for(var i=0;i<this.files.length;i++){
		  var formData = new FormData();
		  var filename = $(this).val();
		  var trow     = '';
		  var arrid    = attachfile.length;
		  if(filename!='' && filename!=undefined){
		    formData.append('file', this.files[0]);
		    attachfile.push({fname:filename,xdata:formData});
			trow = '<tr><td data-id="tmparr" data-arrid="'+arrid+'" data-filename="'+filename+'" width="5%"><button class="btn btn-xs red btn-aremove"><i class="fa fa-trash-o"></i> Remove</button></td><td>'+filename+'</td></tr>';
		    $('#tblattachment').find('tbody').append(trow);
		  }	
	  }
  });
  
  $('body').on('click','.btn-aremove',function(){
	 $(this).closest('tr').remove(); 
  });
  
  if($('.inbox-nav').find('.active').length>0){
	$('.inbox-nav').find('.active').trigger('click');  
  }
  
 /* 
  $('form').on('submit', function (e) {
	e.preventDefault();
	alert($('.summernote').summernote('code'));
	alert($('.summernote').val());
  }); 
 */
});

function getcontacts(){
	setAjaxRequest(
		base_url+'email/txn?event=contacts',{}
	   ,function(r){
	      emailto = $('#multi-append').select2({
						allowClear: true,
						tags: true,
						tokenSeparators: [',', ' '],
						data: r.results,
					});		
          progressBar('off');		  
	   }
    );   
}

function upload_attachment(xid,sending){
	var err_attach = false;
	    sending    = ((sending==undefined)?false:sending);
	if(xid==undefined){
	  if(sending){
		 send_email(); 
	  }else{
	     progressBar('off');  
	  }	
	  return false;
	}
	
	if(attachfile.length>0){
	  $.each(attachfile,function(i,data){
          var xdata = data['xdata'];		  
		  if($('[data-arrid="'+i+'"]').length>0 && xdata){
			  ajaxRequestFile(
				 base_url+'email/txn?event=attachment&xid='+xid
				,xdata
				,function(r){
				  attachfile[i]['xdata'] = false; 
				}
				,function(err){
				  sending    = false;
                  err_attach = true;				  
				  alert('Error Encountered While Uploading Attachment');
				}
			  );
          }		  
	  });	
	  if(sending){
		  attachfile = [];
		  send_email(); 
	  }else{
		  if(err_attach==false){
		    attachfile = [];
		  }
		  progressBar('off')
	  }
	}
}

function save_email(sending){
	 var tmpdata = $('#compose').serialize();
 	     sending = ((sending==undefined)?false:sending);
 	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event=save',tmpdata,
			function(r)
			{
			  $('#xid').attr('name','xid');
			  $('#xid').val(r.xid);
			  if(attachfile.length>0){
			   upload_attachment(r.xid,sending); 
		      }else{	 		   
				if(sending){
				   send_email(); 
				}else{
				   progressBar('off');  
				}	
			  }
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save email!',function(){return;});	
			  progressBar('off');
			}
	 );	
}

function send_email(){
	 var tmpdata = $('#compose').serialize();
 	 progressBar('on');
	 setAjaxRequest(
	        base_url+'email/txn?event=send',tmpdata,
			function(r)
			{
			   if(r.success){
				 alert('Success!!');	
				 if($('[data-title="Inbox"]').length>0){
					$('[data-title="Inbox"]').closest('li').trigger('click'); 
				 }else{
					$('[data-table="draft"]').closest('li').trigger('click');  
				 }
	 		   }else{
				 alert(r.content);	
	 		   }
			   progressBar('off');
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save email!',function(){return;});	
			  progressBar('off');
			}
	 );	
}
