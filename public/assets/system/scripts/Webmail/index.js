var Webmail = function() {

    var isRefreshed = false;

    var isSending = false;

	function getInbox() {
        
        if (isRefreshed) {
            return;
        }
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		jQuery.ajax({
	        url: base_url+page_url+'getInbox',
	        dataType: 'HTML',
	        data: '',
	        type: 'GET',
	        async: true,
	        beforeSend: function() {
                isRefreshed = true;
                $('.btn-refresh').html('Reloading...');
                if (!hasInbox()) {
                    $('.table_wrapper table tbody').html('<h4>Loading</h4>');
                }
	        },
	        success: function(data) {
                if (data != '') {
                    $('.table_wrapper').html(data);
                    dataTable();
                }
                isRefreshed = false;
                $('.btn-refresh').html('Refresh');
	        },
	        error: function(error) {
	           isRefreshed = false;
               $('.btn-refresh').html('Refresh');
	        }
	    });
	}

    function previewMessage(el, msgno) {

        if (isSending) {
            return alert('Please wait. Still sending an email.');
        }

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: base_url+page_url+'preview/'+msgno,
            dataType: 'HTML',
            data: '',
            type: 'GET',
            async: true,
            beforeSend: function() {
                $('.btn-replyto').attr('data-msgno',msgno);
                $('.btn-replyto').removeClass('hide');
                $('.view_wrapper').html('<h4>Loading</h4>');
            },
            success: function(data) {
                el.addClass('read');
                el.removeClass('unread');
                $('.view_wrapper').html(data);
            },
            error: function(error) {
             
            }
        });
    }

    function compose() {
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: base_url+page_url+'compose',
            dataType: 'HTML',
            data: '',
            type: 'GET',
            async: true,
            beforeSend: function() {
                $('.btn-replyto').addClass('hide');
                $('.view_wrapper').html('<h4>Loading</h4>');
            },
            success: function(data) {
                $('.view_wrapper').html(data);
            },
            error: function(error) {
             
            }
        });
    }

    function replyto() {
        $('.message_wrapper').addClass('hide');
        $('.reply_wrapper').removeClass('hide');
        return;
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: base_url+page_url+'reply/'+$('.btn-replyto').attr('data-msgno'),
            dataType: 'HTML',
            data: '',
            type: 'GET',
            async: true,
            beforeSend: function() {
                $('.view_wrapper').html('<h4>Loading</h4>');
            },
            success: function(data) {
                $('.view_wrapper').html(data);
            },
            error: function(error) {
             
            }
        });
    }

    function send() {
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var message = $('#message_text').val() != undefined ? ($('#mymessage').html($('#mymessage').val()).val()+'<br /><br />'+$('#message_text').html($('#message_text').val()).val()) : $('#mymessage').html($('#mymessage').val()).val()
        
        jQuery.ajax({
            url: base_url+page_url+'send',
            dataType: 'json',
            data: $('.form_crud').serialize()+'&message='+message,
            type: 'POST',
            async: true,
            beforeSend: function() {
                $('.btn-send').html('sending....');
                isSending = true;
            },
            success: function(data) {
                isSending = false;
                $('.btn-send').html('Send');
                $('.message_wrapper').removeClass('hide');
                $('.reply_wrapper').addClass('hide');
                $('.view_wrapper').html('<h4>Message sent!</h4>');
                alert('Successfully sent message!');
            },
            error: function(error) {
                isSending = false;
                $('.btn-send').html('Send');
            }
        });
    }

    function hasInbox() {
        if ($('#table-inbox tbody tr').length > 0) {
            return true;
        }
        return false;
    }

    function cancelSend() {
        $('.view_wrapper').html('');
        $('.btn-replyto').addClass('hide');
    }

	function dataTable (table){
        var table = table != undefined ? table : '.table';
        var oTable = jQuery(table).dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "columnDefs": [{
                "orderable": false,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });
        var tableWrapper = jQuery(table+'_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        var tableColumnToggler = jQuery(table+'_column_toggler');
    }

	return {
		init: function() {
			setTimeout( function() {
				getInbox();
			},2000);	

            setInterval( function() {
                getInbox();
            },60000);    


            $(document).on('click','.btn-preview', function() {
                previewMessage($(this),$(this).attr('data-msgno'));
            });

            $(document).on('click','.btn-refresh', function() {
                isRefreshed = false;
                getInbox()
            });

            $(document).on('click','.btn-compose', function() {
                compose()
            });

            $(document).on('click','.btn-replyto', function() {
                replyto()
            });

            $(document).on('click','.btn-send', function() {
                send();
            });

            $(document).on('click','.btn-cancel-send', function() {
                cancelSend();
            });
		}
	}

}();	