var ME = {
    init : function() {        
        this.is_busy = false;
        this.mylink = base_url+page_url;
        this.addlistener();
    },
    
    delete_policy: function(e){

        if(this.is_busy){
           return false;
        }

        this.is_busy=true;

        var ids = [];
        var cnt = 0;

        $('#tblpolicy tbody tr').each(function(e){
            var flg = $(this).find('.chk-child').prop('checked');
            if(flg){
                ids.push( $(this).attr('data-id')); cnt++;
            }
        });
        
        if(cnt<=0){
            msgbox('warning','No record selected!','Delete policy',10000);
            this.is_busy=false;
            return false;
        }
        var thisResult = function(yes){
            if (yes) {
                setAjaxRequest( base_url + page_url + 'thisRequest', {'event': 'delete', 'ids': ids}, function(r) {
                    if (r.error) {
                        msgbox('danger',r.message,'Delete policy');
                    } else {
                        msgbox('success',r.message,'Delete policy');
                    }
                });
                progressBar('off');
                ME.$policy.find('.scroller').html(r.table);
            }
        }
        bootbox.confirm({ size: 'small', message: "Are you sure you want to remove this policy?",callback: thisResult});

        this.is_busy=false;


    },

    load_grades: function(e){
                
        var d = $(e.target).closest('tr').data();
        //this.$policy.fadeOut();
        var data = { event : 'grades', idx :  d.id, name : d.name,};
        var res = ajaxRequest(this.mylink+"event",data);
        $('#gradecontainer').html(res.content);
    },
    
    refresh_wait: function(){
        this.$policy.find('.scroller').html('<h4><i class="fa fa-spin fa-spinner"></i> Please wait while loading</h4>');
    },

    initcomponent : function(){
        var p =this.$selpolicy.attr('data-id');
        if( p == undefined || p == ''){
            msgbox('warning','Please select a policy','No policy selected',5000);
            return false;
        }
        $('#component_modal').modal('show');
    },

    savecomponent: function(e){
        var ids = [];

         $('#tblnewcomponents tbody tr').each(function(){
            if( $(this).find('.chk-child').prop('checked') ){
                var percent = $(this).find('.component-percentage').val();
                if(percent > 0){
                    var data = {
                        policy : ME.$selpolicy.attr('data-id'),
                        component : $(this).attr('data-id'),
                        code : $(this).attr('data-code'),
                        name : $(this).attr('data-name'),
                        percent : percent,
                    }
                    ids.push(data);
                }
            }
        });

        if(ids.length <=0){
            msgbox('warning','Please input percentage','No percentage');
            return false;
        }

        var data = {
           event : 'insert-component',
           details :  ids,
        };
        var res = ajaxRequest(this.mylink+"thisRequest",data);
        if (res.error) {
            msgbox('danger',res.message,'Add component to policy');
        } else {
            msgbox('success',res.message,'Add component to policy');
        }
        $('#component_modal').modal('hide');
        
    },
    
    erasecomponent : function(e){
        if(this.is_busy){ return false; }
        this.is_busy=true;

        var ids = [];
        var cnt = 0;
                
        $('#tblpolicycomponent tbody tr').each(function(e){
            var flg = $(this).find('.chk-child').prop('checked');
            
            if(flg){
                var arr = {
                    id : $(this).attr('data-id'),
                    code: $(this).attr('data-code'),
                    name :$(this).attr('data-name'),
                }
                ids.push( arr );
                cnt++;
            }
        });
        console.log(ids);
        if(cnt<=0){
            msgbox('warning','No record selected!','Delete policy component',5000);
            this.is_busy=false;
            return false;
        }
        var thisResult = function(yes){
            if (yes) {
                setAjaxRequest( base_url + page_url + 'thisRequest', {'event': 'delete-component', 'ids': ids}, function(r) {
                    if (r.error) {
                        msgbox('danger',r.message,'Delete policy component');
                    } else {
                        msgbox('success',r.message,'Delete policy component');
                    }
                });
                progressBar('off');
                ME.view_components();
            }
        }
        bootbox.confirm({ size: 'small', message: "Are you sure you want to remove the selected component(s)?",callback: thisResult});
        
        this.is_busy=false;
        
    },
    
    action : function(e){
        var data = $(e.target).data();                
        switch (data.mode) {                        
            case 'template': this.gstemplate.action(e); break;
            case 'grades': this.grades.action(e); break;            
            default: break;
        }
    },
    
    grades : {
       action : function(e){
            var data = $(e.target).data();
            switch (data.menu) {
                case 'create': this.init(e); break;
                case 'edit': this.init(e); break;
                case 'delete': this.idelete(e); break; 
                case 'save': this.save(); break;           
                default: break;
            }          
        },
        init : function(e){
            
            var $modal  = $('#grade_modal');            
            clear_value('#form_grade');
            var d = $(e.target).closest('tr').data();
            var template = $('#tbltemplate tbody tr.active-row').attr('data-id');                        
            if(d != undefined){                                    
                                 
                $modal.find('#idx').val(d.id);
                $modal.find('#min').val(d.min);
                $modal.find('#max').val(d.max);
                $modal.find('#grade').val(d.grade);
                
            }   
            
            $modal.find('#template').val(template);
                             
            $modal.modal('show');
        },
        save : function(){
    
            var v = isFormValid('#form_grade');
            if( v ){                                           
                var data = $('#form_grade').serialize();                                        
                var res = ajaxRequest(ME.mylink+"event",'event=save-grade&'+data);
                if (res.error) {
                    msgbox('warning',res.message,'Save Grade');
                } else {
                   msgbox('success',res.message,'Save Grade');
                   //this.$policy.find('.scroller').html(res.table);
                   $('#grade_modal').modal('hide');
                   $('#gradecontainer').html(res.table);
                }
            }        
        },
        idelete : function(e){

            if(ME.is_busy){ return false; }
            ME.is_busy=true;
            var $r = $('#tblgrades tbody tr.active-row');
            var data = {
                event : 'delete-grade',
                ids : $r.attr('data-id'), 
                tem : $r.attr('data-template'),               
                min : $r.attr('data-min'),                    
            }             
            
            var thisResult = function(yes){
                if (yes) {
                    setAjaxRequest( base_url + page_url + 'event', data, function(r) {
                        if (r.error) {
                            msgbox('error',r.message,'Delete grade');
                        } else {
                            msgbox('success',r.message,'Delete grade');
                            $('#gradecontainer').html(r.table);
                        }
                        progressBar('off');
                    });                    
                    //ME.$policy.find('.scroller').html(r.table);
                }                
            }            
            bootbox.confirm({ className: "",  message: "<span class='color-red'><i class='fa fa-times'></i> Are you sure you want to remove this grade?</span>",callback: thisResult});
            ME.is_busy=false;                      
        },
        
        
           
    },
    
    gstemplate : {
        action : function(e){
            var data = $(e.target).data();
            switch (data.menu) {
                case 'create': this.init(); break;
                case 'edit': this.init(e); break;
                case 'delete': ME.gstemplate.idelete(e); break; 
                case 'save': this.save(); break;           
                default: break;
            }          
        },
        
        init : function(e){
            
            var $modal  = $('#template_modal');            
            clear_value('#form_template');                        
            if(e != undefined){                                    
                var d = $(e.target).closest('tr').data();                 
                $modal.find('#idx').val(d.id)
            }                    
            $modal.modal('show');
        },
    
        save : function(){
    
            var v = isFormValid('#form_template');
            if( v ){                                           
                var data = $('#form_template').serialize();                                        
                var res = ajaxRequest(ME.mylink+"event",'event=save-template&'+data);
                if (res.error) {
                    msgbox('warning',res.message,'Save Policy');
                } else {
                   msgbox('success',res.message,'Save Policy');
                   //this.$policy.find('.scroller').html(res.table);
                   $('#template_modal').modal('hide');
                   $('#templatecontainer').html(res.table);
                }
            }        
        },
    
        idelete : function(e){

            if(ME.is_busy){ return false; }
            ME.is_busy=true;
            var $r = $('#tbltemplate tbody tr.active-row');
            var data = {
                event : 'delete-template',
                ids : $r.attr('data-id'),
                name : $r.find('td.name').html(),                    
            }             
            
            var thisResult = function(yes){
                if (yes) {
                    setAjaxRequest( base_url + page_url + 'event', data, function(r) {
                        if (r.error) {
                            msgbox('error',r.message,'Delete grading system template');
                        } else {
                            msgbox('success',r.message,'Delete grading system template');
                            $('#templatecontainer').html(r.table);
                        }
                        progressBar('off');
                    });                    
                    //ME.$policy.find('.scroller').html(r.table);
                }                
            }            
            bootbox.confirm({ className: "",  message: "<span class='color-red'><i class='fa fa-times'></i> Are you sure you want to remove this grading system?</span>",callback: thisResult});
            ME.is_busy=false;                      
        },
    },

    addlistener: function() {
        $('body').on('click','.btn,.edit', $.proxy(this.action, this));
        $('body').on('click','#tbltemplate tbody tr', $.proxy(this.load_grades, this));
    },
};
