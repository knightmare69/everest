$(document).ready(function(e){
    /*
        revised by : Allan Robert B. Sanchez [wacko0316@hotmail.com]
        date : 20160813:2251H
    */
    var _table = $('#records-table').DataTable({
        "aaSorting" : [],
        'aoColumnDefs': [
            { 'aTargets': [0], 'bSortable': false}
        ],
        fnDrawCallback: function(){
            Metronic.initUniform();
        }
    });

    $('body').on('click', '#check-parent', function(e){
        if($(this).is(':checked')){
            $('.check-child', _table.row().nodes()).prop('checked', true).parent().addClass('checked');
        } else {
            $('.check-child', _table.row().nodes()).prop('checked', false).parent().removeClass('checked');
        }
    });

    $('body').on('click', '#comp-add-row', function(e){
        _table.row.add([
            '<label><input type="checkbox" class="check-child"></label>',
            '<input type="text" class="form-control code">',
            '<input type="text" class="form-control description">',
            '<input type="text" class="form-control projection numberonly">',
            '<a href="javascript:void(0);" class="btn btn-sm btn-default" > Save <a>'
        ]).draw(false);

        _table.order([2, 'asc']).draw();
        // _table.order([0, 'asc']).draw();
    });

    $('body').on('click', '#comp-remove', function(e){
        var _checked = $('.check-child:checked', _table.row().nodes());

        if(_checked.length == 0){
            msgbox('info', 'Please select components to remove.');
        } else {

            bootbox.confirm({ size: 'small', message: "Are you sure you want to remove this component(s)?",
                callback: function(yes) {
                    if (yes) {
                        var ids = [];

                        _checked.each(function(e){
                            var _tr = $(this).closest('tr'), id = _tr.attr('data-id');

                            if(_tr.is(':first-child')){
                                $('#records-table tr:first-child td input').val('');
                            } else {
                                _table.row(_tr).remove();
                            }

                            if(id !== 0 || id !== undefined){
                                ids.push(_tr.attr('data-id'));
                            }
                        });

                        _table.draw();

                        if(ids.length >= 1){
                            setAjaxRequest(
                                base_url + page_url + 'event', {'event': 'delete', 'comps': ids},
                                function(result) {
                                    if (result.error) {
                                        showError(result.message);
                                    } else {
                                        showSuccess(result.message);

                                    }
                                }
                            );
                            progressBar('off');
                        }
                    }
                }
            });

        }
    });

    $('body').on('click', '#comp-save', function(e){
        var collect = {}, _new = [], _update = [];
        var tr = $('#records-table tr td input[type="text"]:not(.hide)').closest('tr');

        tr.each(function(e){
            var id = $(this).attr('data-id'),
                _code = $(this).find('.code').val(),
                _description = $(this).find('.description').val(),
                _proj = $(this).find('.projection').val();

            if(_code != '' && _description != ''){
                if(id == 0 || id == undefined){
                    _new.push({'code': _code, 'description': _description, 'projection': _proj });
                } else {
                    _update.push({'comp': id, 'code': _code, 'description': _description , 'projection': _proj  });
                }

                collect['new'] = _new;
                collect['update'] = _update;
            }
        });

        if(collect.length !== 0){

            setAjaxRequest(
                base_url + page_url + 'event', {'event': 'save', collect},
                function(result) {
                    if (result.error) {
                        showError(result.message);
                    } else {
                        showSuccess(result.message);
                        $('#component-result-holder').html(result.table);

                        _table = $('#records-table').DataTable({
                            "aaSorting" : [],
                            'aoColumnDefs': [
                                { 'aTargets': [0], 'bSortable': false}
                            ],
                            fnDrawCallback: function(){
                                Metronic.initUniform();
                            }
                        });
                    }
                }
            );
            progressBar('off');
        } else {
            msgbox('info', "There's nothing to save.");
        }
    });

    $('body').on('click', '#comp-edit', function(e){
        var _checked = $('.check-child:checked', _table.row().nodes());

        if(_checked.length == 0){
            msgbox('info', 'Please select component(s) to edit.');
        } else {
            _checked.each(function(e){
                var _tr = $(this).closest('tr');

                _tr.find('p').addClass('hide');
                _tr.find('input').removeClass('hide')
            });
        }
    });

    $('body').on('click', '#comp-cancel', function(e){
        var _checked = $('.check-child:checked', _table.row().nodes());
        _checked.each(function(e){
            var _tr = $(this).closest('tr');

            if(_tr.is(':first-child')){
                $('#records-table tr:first-child td input').val('');
            } else {
                id = _tr.attr('data-id');
                //  _code = _tr.find('.code'), _descp = _tr.find('.description');

                if(id == 0 || id == undefined){
                     _table.row(_tr).remove().draw();
                } else {
                    _tr.find('p').removeClass('hide');
                    _tr.find('input[type=text]').addClass('hide');
                }
            }
        });
    });
});