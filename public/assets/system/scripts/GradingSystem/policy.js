var ME = {
    init : function() {
        this.is_busy = false;
        this.mylink = base_url+page_url;
        this.$policy = $('#policy');
        this.$component = $('#components');
        this.$selpolicy = $('#policycomponent');
        this.initpolicy();
        sysfnc.div_autoheight(ME.$policy.find('.scroller'));
        this.addlistener();
    },

    delete_policy: function(e){

        if(this.is_busy){ return false; }
        this.is_busy=true;

        var ids = [];
        var cnt = 0;

        $('#tblpolicy tbody tr').each(function(e){
            var flg = $(this).find('.chk-child').prop('checked');
            if(flg){
                ids.push( $(this).attr('data-id')); cnt++;
            }
        });

        if(cnt<=0){
            msgbox('warning','No record selected!','Delete policy',10000);
            this.is_busy=false;
            return false;
        }
        var thisResult = function(yes){
            if (yes) {
                setAjaxRequest( base_url + page_url + 'thisRequest', {'event': 'delete', 'ids': ids}, function(r) {
                    if (r.error) {
                        msgbox('danger',r.message,'Delete policy');
                    } else {
                        msgbox('success',r.message,'Delete policy');

                    }
                });
                progressBar('off');
                ME.$policy.find('.scroller').html(r.table);
            }
        }
        bootbox.confirm({ size: 'small', message: "Are you sure you want to remove this policy?",callback: thisResult});

        this.is_busy=false;


    },
    initpolicy: function(){
        //this.$policy.fadeIn();
       // this.$component.hide();
    },

    refresh_component: function(){
        var d = $('#tblpolicy tbody tr.danger').data();
        this.$selpolicy.attr('data-id',d.id);
        this.$selpolicy.html(d.name);
        var data = { event : 'component', idx :  d.id, name : d.name,};
        var res = ajaxRequest(this.mylink+"thisRequest",data);
        this.$component.find('.scroller').html(res.content);
    },

    view_components: function(e){

        $('#tblpolicy tbody tr').removeClass('danger');
        $(e.target).closest('tr').addClass('danger');
        ME.refresh_component();
    },

    refresh_policy: function(){
        this.$policy.find('.scroller').html('<h4><i class="fa fa-spin fa-spinner"></i> Please wait while loading</h4>');
    },

    validate_policy: function(){
        var res = isFormValid('#form_createpolicy')
        return res;
    },

    save_policy: function(){

        if( this.validate_policy() ){

            var data = $('#form_createpolicy').serialize();
            this.refresh_policy();

            var res = ajaxRequest(this.mylink+"thisRequest",'event=save-policy&'+data);
            if (res.error) {
                msgbox('warning',res.message,'Save Policy');
            } else {
               msgbox('success',res.message,'Save Policy');
               this.$policy.find('.scroller').html(res.table);
               $('#form_modal1').modal('hide');
            }
        }

    },

    initmodal: function(e){

        clear_value('#form_createpolicy');

        var d = $(e.target).closest('tr').data();
        if(d != undefined){
            console.log(d);
            $('#form_modal1').find('#idx').val(d.id)
            $('#form_modal1').find('#policy').val(d.name)
            $('#form_modal1').find('#desc').val(d.desc)
            $('#form_modal1').find('#program').val(d.program)
            
            if(d.zerobased == '1'){
                $('#form_modal1').find('#zerobased').prop('checked','true' );      
                $('#form_modal1').find('#zerobased').parent().addClass('checked');
            }else{
                $('#form_modal1').find('#zerobased').removeAttr('checked');
                $('#form_modal1').find('#zerobased').parent().removeClass('checked');
            }
            
            if(d.club == '1'){
                $('#form_modal1').find('#club').prop('checked','true' );      
                $('#form_modal1').find('#club').parent().addClass('checked');
            }else{
                $('#form_modal1').find('#club').removeAttr('checked');
                $('#form_modal1').find('#club').parent().removeClass('checked');
            }
            
            if(d.conduct == '1'){
                $('#form_modal1').find('#conduct').prop('checked','true' );      
                $('#form_modal1').find('#conduct').parent().addClass('checked');
            }else{
                $('#form_modal1').find('#conduct').removeAttr('checked');
                $('#form_modal1').find('#conduct').parent().removeClass('checked');
            }
            
            $('#form_modal1').find('#inactive').prop('checked',d.status);
        }

        $('#form_modal1').modal('show');
    },

    initcomponent : function(){
        var p =this.$selpolicy.attr('data-id');
        if( p == undefined || p == ''){
            msgbox('warning','Please select a policy','No policy selected',5000);
            return false;
        }
        $('#component_modal').modal('show');
    },

    savecomponent: function(e){
        var ids = [];

         $('#tblnewcomponents tbody tr').each(function(){
            if( $(this).find('.chk-child').prop('checked') ){
                var parent = $(this).find('.component-parent').val();
                var percent = $(this).find('.component-percentage').val();
                var seq = $(this).find('.component-seq').val();
                if(percent > 0){
                    var data = {
                        policy : ME.$selpolicy.attr('data-id'),
                        component : $(this).attr('data-id'),
                        code : $(this).attr('data-code'),
                        name : $(this).attr('data-name'),
                        percent : percent,
                        parent : parent,
                        seq : seq,

                    }
                    ids.push(data);
                }
            }
        });

        if(ids.length <=0){
            msgbox('warning','Please input percentage','No percentage');
            return false;
        }

        var data = {
           event : 'insert-component',
           details :  ids,
        };
        var res = ajaxRequest(this.mylink+"thisRequest",data);
        if (res.error) {
            msgbox('danger',res.message,'Add component to policy');
        } else {
            msgbox('success',res.message,'Add component to policy');
            ME.refresh_component();
        }
        $('#component_modal').modal('hide');

    },

    erasecomponent : function(e){
        if(this.is_busy){ return false; }
        this.is_busy=true;

        var ids = [];
        var cnt = 0;

        $('#tblpolicycomponent tbody tr').each(function(e){
            var flg = $(this).find('.chk-child').prop('checked');

            if(flg){
                var arr = {
                    id : $(this).attr('data-id'),
                    code: $(this).attr('data-code'),
                    name :$(this).attr('data-name'),
                }
                ids.push( arr );
                cnt++;
            }
        });
        console.log(ids);
        if(cnt<=0){
            msgbox('warning','No record selected!','Delete policy component',5000);
            this.is_busy=false;
            return false;
        }
        var thisResult = function(yes){
            if (yes) {
                setAjaxRequest( base_url + page_url + 'thisRequest', {'event': 'delete-component', 'ids': ids}, function(r) {
                    if (r.error) {
                        msgbox('danger',r.message,'Delete policy component');
                    } else {
                        msgbox('success',r.message,'Delete policy component');
                        ME.refresh_component
                    }
                });
                progressBar('off');
                ME.view_components();
            }
        }
        bootbox.confirm({ size: 'small', message: "Are you sure you want to remove the selected component(s)?",callback: thisResult});

        this.is_busy=false;

    },

    action :function(e){

        var data = $(e.target).data();
        switch (data.menu) {
            case 'cancel': this.initpolicy(); break;
            case 'save-policy': this.save_policy(e); break;
            case 'add-policy': this.initmodal(e); break;
            case 'delete-policy': this.delete_policy(e); break;
            case 'add-component': this.initcomponent(e); break;
            case 'save-component': this.savecomponent(e); break;
            case 'delete-component': this.erasecomponent(e); break;
            default: break;
        }
    },

    compute_percent : function(e){
        var p = 0;
        $('#tblnewcomponents tbody tr').each(function(){
            if( $(this).find('.chk-child').prop('checked') ){
                var t = $(this).find('.component-percentage').val();
                p += strCast(t,'int');
            }
        });
        $('#totalpercentage').html(p+'%');
    },

    filter_component : function(e){
       var flg  = $(e.target).prop('checked')
        if(flg){
            $('#tblnewcomponents tbody tr').each(function(){
                if( $(this).find('.chk-child').prop('checked') ){
                    $(this).removeClass('hide');
                }else{
                    $(this).addClass('hide');
                }
            });
        }else{
            $('#tblnewcomponents tbody tr').removeClass('hide');
        }
    },

    addlistener: function() {
        $('body').on('click','.a_select', $.proxy(this.initmodal, this));
        $('body').on('click','.view-component', $.proxy(this.view_components, this));
        $('body').on('click','.btn', $.proxy(this.action, this));
        $('body').on('click','.component-selectonly', $.proxy(this.filter_component, this));
        $('body').on('change','.component-percentage', $.proxy(this.compute_percent, this));
    },
};
$(window).resize(function() { sysfnc.div_autoheight(ME.$policy.find('.scroller')); });