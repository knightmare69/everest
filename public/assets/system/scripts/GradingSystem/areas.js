$(document).ready(function(e){
    var _table = $('#records-table').DataTable({
        "aaSorting" : [],
        'aoColumnDefs': [{ 'aTargets': [0], 'bSortable': false}],
        fnDrawCallback: function(){
            Metronic.initUniform();
        }
    });
    $('body').on('click', '#check-parent', function(e){
        if($(this).is(':checked')){
            $('.check-child', _table.row().nodes()).prop('checked', true).parent().addClass('checked');
        } else {
            $('.check-child', _table.row().nodes()).prop('checked', false).parent().removeClass('checked');
        }
    });
    $('body').on('click', '#sub-ar-add-row', function(e){
        _table.row.add([
            '<label><input type="checkbox" class="check-child"></label>',
            '<input type="text" class="form-control subj-id">',
            '<input type="text" class="form-control name">',
            '<input type="text" class="form-control short-name">',
            '<div class="input-group"><input type="text" class="form-control coordinator"><div class="input-group-btn"><button class="coord-search-btn btn btn-default"><i class="fa fa-search"></i></button><button type="button" class="coord-search-cancel-btn btn btn-default text-danger"><i class="fa fa-times"></i></button></div></div>'
        ]).draw(false);
        _table.order([1, 'asc']).draw();
    });
    var curr_cord_search_box;
    $('body').on('click', '.coord-search-btn', function (e) {
        curr_cord_search_box = $(this).parent().prev();
        var search = curr_cord_search_box.val();
        setAjaxRequest( base_url + page_url + 'event', {'event': 'search-coordinator', 'search-coord': search},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    progressBar('off');
                    showModal({
                        name: 'basic',
                        title: 'Coordinators',
                        content: result.html,
                        class: 'modal_coord_result',
                        hasModalButton: false
                    });
                    $('#table-coord-res').DataTable({ "aaSorting" : [] });
                }
            }
        );
        progressBar('off');
    });
    $('body').on('click', '.coord-search-cancel-btn', function(e){
        $(this).prev().prop('disabled', false);
        $(this).parent().prev().attr({'data-id': '', 'disabled': false}).val('');
    });
    $('body').on('click', '.btn-coord-select-btn', function(e){
        var d = $(this).data();
        curr_cord_search_box.attr({'data-id':d.id, 'disabled': true}).val(d.name);
        curr_cord_search_box.next().find('.coord-search-btn').prop('disabled', true);
        closeModal('basic');
        progressBar('off');
    });
    $('body').on('click', '#sub-ar-remove', function(e){
        var _checked = $('.check-child:checked', _table.row().nodes());
        if(_checked.length == 0){
            msgbox('info', 'Please select subject areas to remove.');
        } else {
            bootbox.confirm({
                size: 'small',
                message: "Are you sure you want to remove this subject areas?",
                callback: function(yes) {
                    if (yes) {
                        var ids = [];
                        _checked.each(function(e){
                            var _tr = $(this).closest('tr'), id = _tr.attr('data-id');
                            if(_tr.is(':first-child')){
                                $('#records-table tr:first-child td input').val('');
                            } else {
                                _table.row(_tr).remove();
                            }
                            if(id !== 0 || id !== undefined){
                                ids.push(_tr.attr('data-id'));
                            }
                        });
                        _table.draw();
                        if(ids.length >= 1){
                            setAjaxRequest(
                                base_url + page_url + 'event', {'event': 'delete', 'sub-areas': ids},
                                function(result) {
                                    if (result.error) {
                                        msgbox('error', result.message);
                                    } else {
                                        msgbox('success', result.message);
                                    }
                                }
                            );
                            progressBar('off');
                        }
                    }
                }
            });
        }
    });
    $('body').on('click', '#sub-ar-save', function(e){
        var collect = {}, _new = [], _update = [], _proceed = true;
        var tr = $('#records-table tr td > input[type="text"]:not(.hide)').closest('tr');
        tr.each(function(e){
            var id = $(this).attr('data-id'),
                subj_id = $(this).find('.subj-id').val(),
                name = $(this).find('.name').val(),
                sname = $(this).find('.short-name').val(),
                coord = $(this).find('.coordinator');
            if(name != '' && sname != ''){
                var coord_id = coord.attr('data-id');
                if(coord.val() != '') {
                    if(coord_id == undefined || coord_id == 0){
                        msgbox('error', 'Please select valid coordinator');
                        _proceed = false;
                        return false;
                    }
                }
                if(id == 0 || id == undefined){
                    _new.push({'subj_id': subj_id, 'name': name, 'short_name': sname, 'coordinator':  coord_id});
                } else {
                    _update.push({'area': id, 'subj_id': subj_id, 'name': name, 'short_name': sname, 'coordinator': coord_id });
                }
                collect['new'] = _new;
                collect['update'] = _update;
            }
        });
        if(_proceed){
            setAjaxRequest(
                base_url + page_url + 'event', {'event': 'save', collect},
                function(result) {
                    if (result.error) {
                        msgbox('error', result.message);
                    } else {
                        $('#sub-ar-result-holder').html(result.table);
                        _table = $('#records-table').DataTable({
                            "aaSorting" : [],
                            'aoColumnDefs': [
                                { 'aTargets': [0], 'bSortable': false}
                            ],
                            fnDrawCallback: function(){
                                Metronic.initUniform();
                            }
                        });
                    }
                }
            );
            progressBar('off');
        }
    });
    $('body').on('click', '#sub-ar-edit', function(e){
        var _checked = $('.check-child:checked', _table.row().nodes());
        if(_checked.length == 0){
            msgbox('info', 'Please select component(s) to edit.');
        } else {
            _checked.each(function(e){
                var _tr = $(this).closest('tr');
                _tr.find('p').addClass('hide');
                _tr.find('input, .input-group').removeClass('hide');
            });
        }
    });
    $('body').on('click', '#sub-ar-cancel', function(e){
        var _checked = $('.check-child:checked', _table.row().nodes());
        _checked.each(function(e){
            var _tr = $(this).closest('tr');
            if(_tr.is(':first-child')){
                $('#records-table tr:first-child td input').val('');
            } else {
                id = _tr.attr('data-id');
                if(id == 0 || id == undefined){
                     _table.row(_tr).remove().draw();
                } else {
                    _tr.find('p').removeClass('hide');
                    _tr.find('td > input[type=text], div.input-group').addClass('hide');
                }
            }
        });
    });
});