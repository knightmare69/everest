function isValidForm(form_name) {
	var isValid = true;
	$(form_name+' input:text').each(function() {
		$(this).removeAttr('style');
        if (!$(this).hasClass('not-required')){
            if ($(this).hasClass('form-control') && $(this).val() == '')  {
                $(this).attr('style','border: 1px solid red');
                isValid = false;
            }
        }

        if ($(this).hasClass('form-control') && ($(this).attr('data-minlength') != '' && $(this).attr('data-minlength') != undefined)) {
            if ($(this).val().length < $(this).attr('data-minlength')) {
                $(this).attr('style','border: 1px solid red');
                isValid = false;
            }
        }

        if ($(this).hasClass('form-control') && !$(this).hasClass('not-required') && ($(this).attr('data-minage') != '' && $(this).attr('data-minage') != undefined)) {
            if (isNaN(parseFloat(getAge($(this).val()))) || (parseFloat(getAge($(this).val())) < $(this).attr('data-minage'))) {
                $(this).attr('style','border: 1px solid red');
                isValid = false;
            }
        }
    });

	//Textarea
    $(form_name+' textarea').each(function() {
    	$(this).removeAttr('style');
        if (!$(this).hasClass('not-required') && ( $(this).hasClass('form-control') && $(this).val() == '') ) {
            $(this).attr('style','border: 1px solid red');
            isValid = false;
        }

        if ($(this).hasClass('form-control') && !$(this).hasClass('not-required') && ($(this).attr('data-minlength') != '' && $(this).attr('data-minlength') != undefined)) {
            if ($(this).val().length < $(this).attr('data-minlength')) {
                $(this).attr('style','border: 1px solid red');
                isValid = false;
            }
        }
    });

    //Select
    $(form_name+' select').each(function() {

    	$(this).removeAttr('style');

        $(form_name+ ' '+' #s2id_'+$(this).attr('name')).removeAttr('style');

        var value = $(this).find('option:selected').val() == undefined ?  '' : $(this).find('option:selected').val();

        if (!$(this).hasClass('not-required') && value.trim() == '') {
            if ($(this).hasClass('select2')) {
                $(form_name+' #s2id_'+$(this).attr('name')).attr('style','border: 1px solid red !important');                
            }
            $(this).attr('style','border: 1px solid red');  
            isValid = false;
        }
        $(form_name+ ' '+' #s2id_'+$(this).attr('name')).attr('style','width: 100%');
    });

    return isValid;
}