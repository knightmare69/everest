var isEdit = false, PointKey = '';
var Me = function() {

	//handle saving of subject
	var handleSaving = function() {

		//create selector
		jQuery('#BtnCreate').click(function() {
			var self = $(this);

			var res = ajaxRequest(base_url+page_url+'event','event=showModal');

			if (res.error == true) {
				msgbox('error',res.message ? res.message : 'Could not load request');
				return;
			}

			//show modal form
			showModal({
				name: 'basic',
		        title : 'Create New '+getActiveCaption() +' Grading Points',
		        content: res.content,
		        class: 'modal_points',
		        button: {
		        	caption: 'Save',
		        	class: 'btn_save'
		        }
		    });

			isEdit = false;
		    $(':checkbox').uniform();
		});

		//save selector
		jQuery('body').on('click','.btn_save', function() {


			if (!isValidForm('.form_crud')) {
				return msgbox('error','Please complete all required fields');
			}

			//create confirmation
			confirmEvent("Are you sure you want to save this?", function(yes) {
				if (!yes) {
					return;
				}
				
				onProcess('.btn_save','','Saving...');

				setAjaxRequest(
					base_url+page_url+'event',
					'event=save&isEdit='+isEdit+'&setting='+getActive()+'&PointKey='+PointKey+'&'+jQuery('.form_crud').serialize(),
					function(result) {
						if (result.error == false) {
							clear();
							reset();
							closeModal('.modal_points');
							msgbox('success',result.message);
						} else {
							msgbox('error',result.message ? result.message : 'Could not save.');
						}
						progressBar('off');
						onProcess('.btn_save','','done',false);
					},	
					function(error) {
						onProcess('.btn_save','','done',false);
						progressBar('off');
					}
				);
			});
		});
	}

	var handleDelete = function() {
		jQuery('#BtnDelete').click(function() {
			var self = $(this);
			
			if (jQuery('.table tbody tr.row-selected').length <= 0 || PointKey == '') {
				return msgbox('error','No selected subject');
			}

			confirmEvent("Are you sure you want to delete this?", function(yes) {
				if (!yes) return;
				onProcess('#BtnDelete','','Deleting...');
				setAjaxRequest(
					base_url+page_url+'event',
					'event=delete&PointKey='+PointKey,
					function(result) {
						if (result.error == false) {
							clear();
							reset();
							msgbox('success',result.message);
						} else {
							msgbox('error',result.message ? result.message : 'Could not delete.');
						}
						progressBar('off');
						onProcess('#BtnDelete','','done',false);
					},
					function(error) {
						progressBar('off');
						onProcess('#BtnDelete','','done',false);
					},'json',true,false
				);
			});
			
		});
	}

	//handle filter
	var handleFilter = function() {

		jQuery('body').on('click','.BtnSearch', function() {

			var filterClass =  jQuery('#filterClass').val();
			var filterCourse =  jQuery('#filterCourse').val();

			var data = 'filterClass='+filterClass+'&filterCourse='+filterCourse;
			
			filter('.BtnSearch',data);
		});

		jQuery('body').on('click','.BtnMoreSearch', function() {			
			filter('.BtnMoreSearch',jQuery('#FormMoreFilter').serialize());
		});

		jQuery('body').on('click','.btnFilterSettings', function() {
			var self = $(this);

			var res = ajaxRequest(base_url+page_url+'event','event=showFilter');

			if (res.error == true) {
				msgbox('error',res.message ? res.message : 'Could not load request');
				return;
			}

			//show modal form
			showModal({
				name: 'basic',
		        title : 'Filter',
		        content: res.content,
		        class: 'modal_filter',
		        button: {
		        	caption: 'Search',
		        	class: 'BtnMoreSearch'
		        }
		    });
		});
	}

	var handleSelection = function() {
		//handle row selection
		jQuery('body').on('dblclick','.rowSel', function() {

			var self = $(this);

			var res = ajaxRequest(base_url+page_url+'event','event=showModal&key='+self.attr('data-id'));

			if (res.error == true) {
				msgbox('error',res.message ? res.message : 'Could not load request');
				return;
			}

			//show modal form
			showModal({
				name: 'basic',
		        title : 'Update '+getActiveCaption() +' Grading Points',
		        content: res.content,
		        class: 'modal_points',
		        button: {
		        	caption: 'Update',
		        	class: 'btn_save'
		        }
		    });

		    isEdit = true;
		    PointKey = self.attr('data-id');
		    $(':checkbox').uniform();
		});

		//handle row selection
		jQuery('body').on('click','.rowSel', function() {
			var self = $(this);

			if (self.hasClass('row-selected')) {
				self.removeClass('row-selected');
			} else {
				jQuery('.rowSel').removeClass('row-selected');
				self.addClass('row-selected');
				PointKey = self.attr('data-id');
			}
		});

		//handle settings selection
		jQuery('body').on('click','.aSettings', function() {

			var self = $(this);

			jQuery('.aSettings')
				.parent()
				.removeClass('active');

			self
				.parent()
				.addClass('active');

			//load points
			filter(getActive());


		});
	}

	function isLock() {
		return jQuery('#IsLock').val() == '1' ? true : false;
	}

	function clear() {
		PointKey = '';
		isEdit = false;
		clear_value('.form_crud');
	}

	function reset() {
		//load points
		filter(getActive());
	}

	function getActive() {
		return jQuery('#GradingSettings li.active').attr('data-code');
	}

	function getActiveCaption() {
		return jQuery('#GradingSettings li.active a').text().trim();
	}

	function filter(key) {
		// onProcess(el,'','Searching..');
		setAjaxRequest(
			base_url+page_url+'event',
			'event=loadPoints&code='+key,
			function(result) {
				if(result.error == false) {
					jQuery('#TableWrapper').html(result.content);
					$(':checkbox').uniform();
				} else {
					msgbox('error',result.message ? result.message : 'Could not load request');
				}
				// onProcess(el,'','done..',false);
				progressBar('off');
			},
			function() {
				// onProcess(el,'','Done',false);
				progressBar('off');
			},'json',true,false
		);
	}


	return {
		//initialize fx
		init: function() {

			//load points
			filter(getActive());

			//handle object selection
			handleSelection();

			//handle delete
			handleDelete();

			//handle saving
			handleSaving();

			handleFilter();
		}
	}
}();