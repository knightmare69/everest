var Reset = function () {

	var handleReset = function () {

         $('.reset-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                password: {
	                    required: true,
	                    minlength: 6,
	                },
	                password_confirmation: {
	                    equalTo: "#password",
	                    minlength: 6,
	                },
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                // form.submit();
	                var label = $('.reset-alert');
                	setAjaxRequest(base_url+'/reset/password',$('.reset-form').serialize(),function(result) {
	                	if (result.error == true) {
	                		label.removeClass('display-hide');
	                		label.addClass('alert-danger');
	                		label.removeClass('alert-success');
	                		label.html(result.message)
	                	} else {
	                		label.removeClass('display-hide');
	                		label.removeClass('alert-danger');
	                		label.addClass('alert-success');
	                		label.find('ul').remove();
	                		clear_value('.forget-form');
	                		label.html(result.message)
	                		$('.reset-form').addClass('hide');
	                		setTimeout(function() {
	                			window.location.href = base_url;
	                		},3000);
	                	} 
	                	progressBar('off');
	                }, function(error) {
                		label.removeClass('display-hide');
                		label.addClass('alert-danger');
                		label.removeClass('alert-success');
                		label.html("There was an error occurd. Please try again!");
                		progressBar('off');
	                });
					
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {        	
            handleReset();    
        }

    };

}();