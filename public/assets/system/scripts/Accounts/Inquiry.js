var Login = function () {
	var ParentKey = 0;
	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


		$('#select2_sample4').change(function () {
            $('.inquiry-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        $('.inquiry-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {

	                mname: {
	                    required: false
	                },
	                fname : {
	                    required: true
	                },
	                lname : {
	                    required: true
	                },
	                Email: {
	                    required: true,
	                    email: true
	                },
	                contact_no: {
	                    required: true,
	                },
	                yearlevel : {
	                    required: false
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept Terms of service."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                onProcess('#register-next-btn','','Loading',true);
	                lazyLoad( function() {
	                	var label = $('.register-alert');
	                	setAjaxRequest(base_url+'/inquiry/register',$('.inquiry-form').serialize(),function(result) {
		                	if (result.error == true) {
		                		label.removeClass('display-hide');
		                		label.addClass('alert-danger');
		                		label.removeClass('alert-success');
		                		label.html(result.message);
		                	} else {
		                		label.addClass('display-hide');
		                		label.removeClass('alert-danger');
		                		label.addClass('alert-success');
		                		ParentKey = result.id;
	                            lazyLoad( function() {
		                			$('.inquiry-form').hide();
	                				$('.inquiry-app-form').show();
	                				childForm();
		                		});

		                	}
		                	progressBar('off');
		                	onProcess('#register-next-btn','','',false);
		                }, function(error) {
	                		label.removeClass('display-hide');
	                		label.addClass('alert-danger');
	                		label.removeClass('alert-success');
	                		label.html("There was an error occurred. Please try again!");
	                		onProcess('#register-next-btn','','',false);
	                		progressBar('off');
		                });
	                });

	                return false;
	            }
	        });

			$('.inquiry-app-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {

	                mname: {
	                    required: false
	                },
	                fname : {
	                    required: true
	                },
	                lname : {
	                    required: true
	                },
	                Email: {
	                    required: false,
	                    email: true
	                },
	                contact_no: {
	                    required: false,
	                },
	                yearlevel : {
	                    required: true
	                },
	                schoolyear: {
	                	required: true
	                },
	                schedule_type: {
	                	required: false
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept Terms of service."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                onProcess('#register-submit-btn','','Loading',true);
	                setProgClass();
	                lazyLoad( function() {
	                	var label = $('.register-alert');
	                	setAjaxRequest(base_url+'/inquiry/register-child/'+ParentKey,$('.inquiry-app-form').serialize(),function(result) {
		                	if (result.error == true) {
		                		label.removeClass('display-hide');
		                		label.addClass('alert-danger');
		                		label.removeClass('alert-success');
		                		label.html(result.message)
		                	} else {
		                		label.removeClass('display-hide');
		                		label.removeClass('alert-danger');
		                		label.addClass('alert-success');
		                		label.find('ul').remove();
		                		clear_value('.inquiry-app-form');
	                            // jQuery('.inquiry-form').hide();
	                            // jQuery('.inquiry-app-form').hide();
	                            // jQuery('#register-success').show();
	                            // jQuery('#register-success').find('messageresult').html(result.message);
	                            label.html(result.message)

		                	}
		                	progressBar('off');
		                	onProcess('#register-submit-btn','','',false);
		                }, function(error) {
	                		label.removeClass('display-hide');
	                		label.addClass('alert-danger');
	                		label.removeClass('alert-success');
	                		label.html("There was an error occurred. Please try again!");
	                		onProcess('#register-submit-btn','','',false);
	                		progressBar('off');
		                });
	                });

	                return false;
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();

                jQuery('.register-form').show();
	            jQuery('#register-success').hide();
                jQuery('.activation-form').hide();
                jQuery('.content').addClass('content_register');

	        });



            jQuery('#btnresend').click(function () {

                setAjaxRequest(base_url+'/sendVCode','key='+ $('#_token').val(),function(result) {
                	if (result.error == false) {
                        alert('resend verification code success!');
                	}
                	progressBar('off');
                }, function(error) {
                	alert("There was an error occurred. Please try again!");
                	progressBar('off');
                });

	        });

            jQuery('#btnresendcode').click(function () {

                setAjaxRequest(base_url+'/RequestAccessCode','key='+ $('#resendemail').val(),function(result) {
                	if (result.error == false) {
                        alert('resend verification code success!');
                        $('.back-btn').click();
                	}
                	progressBar('off');
                }, function(error) {
                	alert("There was an error occurred. Please try again!");
                	progressBar('off');
                });

	        });

            jQuery('#btnconfirm').click(function () {
                window.location.href = base_url+'/registration/confirm?code='+ $('#vcode').val();
	        });

            jQuery('#register-back-btn').click( function() {
            	onProcess('#register-back-btn','','Loading',true);
                lazyLoad( function() {
                	$('.inquiry-app-form').hide();
                	$('.inquiry-form').show();
                	parentForm();
                	onProcess('#register-back-btn','','',false);
                });
            });
	}

	var handleReg = function() {
		$(document).ready(function() {
			if(getParameterByName('reg') == 'true') {
				jQuery('#register-btn').trigger('click');
			}
		});

		jQuery('body').on('click','#ResendConfirmCode', function() {
			setAjaxRequest(base_url+'/sendVCode','key='+$(this).attr('data-c'),
				function(result) {
					if (result.error == false) {
						msgbox('success',result.message);
					} else {
						msgbox('error',result.message);
					}
					progressBar('off');
				}
			);
		});

        jQuery('body').on('click','#activate-btn', function() {

		});
	};

    var handleValidation = function() {
		if (jQuery('#invpswd').val() != undefined && jQuery('#invpswd').val() == '1') {
			setTimeout(function() {
				window.location.href = base_url+'/changePswd?p=a&token='+jQuery('#_auth_token').val();
			},5000);
		}
	};

	var childForm = function() {
		$('.content').addClass('child-form');
	}

	var parentForm = function() {
		$('.content').removeClass('child-form');
	}

	var setProgClass = function() {
		$('#progclass').val($('#yearlevel option:selected').attr('data-progclass'));
	}

    return {
        //main function to initiate the module
        init: function () {
        	handleReg();
            handleRegister();
            handleValidation();
        }

    };

}();
