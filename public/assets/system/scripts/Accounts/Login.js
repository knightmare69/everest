var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                username: {
	                    required: "Username is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	            	var label = $('.reset-alert');
	            	setAjaxRequest(base_url+'/reset/sendEmailReset',$('.forget-form').serialize(),function(result) {
		                	if (result.error == true) {
		                		label.removeClass('display-hide');
		                		label.addClass('alert-danger');
		                		label.removeClass('alert-success');
		                		label.html(result.message)
		                	} else {
		                		label.removeClass('display-hide');
		                		label.removeClass('alert-danger');
		                		label.addClass('alert-success');
		                		$('.reset-show').addClass('hide');
		                		clear_value('.forget-form');
		                		label.html(result.message)
		                	} 
		                	progressBar('off');
		                }, function(error) {
	                		label.removeClass('display-hide');
	                		label.addClass('alert-danger');
	                		label.removeClass('alert-success');
	                		label.html("There was an error occurred. Please try again!");
	                		progressBar('off');
		                });
	                return false;
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	            jQuery('.reset-show').removeClass('hide');
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


		$('#select2_sample4').change(function () {
            $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

         $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
	                    required: true
	                },
	                Email: {
	                    required: true,
	                    email: true
	                },
	                fname : {
	                    required: true
	                },
	                Username: {
	                    required: true
	                },
	                upassword: {
	                    required: true,
	                    minlength: 6,
	                },
	                rpassword: {
	                    equalTo: "#register_password",
	                    minlength: 6,
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept Terms of service."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                // form.submit();
	                var label = $('.register-alert');
	                // confirmEvent("Are you sure you want to register?",function(yes) {
	                	// if (!yes) return;
	                	setAjaxRequest(base_url+'/register',$('.register-form').serialize(),function(result) {
		                	if (result.error == true) {
		                		label.removeClass('display-hide');
		                		label.addClass('alert-danger');
		                		label.removeClass('alert-success');
		                		label.html(result.message)
		                	} else {
		                		label.removeClass('display-hide');
		                		label.removeClass('alert-danger');
		                		label.addClass('alert-success');
		                		label.find('ul').remove();
		                		clear_value('.register-form');
		                		//label.html("<b>Success!</b> Registered. <br> Please go to your email account for the confirmation. <br> Redirecting to login page.");                                
		                		//setTimeout(function() { window.location.href = base_url; },10000);
                                jQuery('.register-form').hide();
                                jQuery('#register-success').show();
                                
		                	} 
		                	progressBar('off');
		                }, function(error) {
	                		label.removeClass('display-hide');
	                		label.addClass('alert-danger');
	                		label.removeClass('alert-success');
	                		label.html("There was an error occurred. Please try again!");
	                		progressBar('off');
		                });
	                // });
	                return false;
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            
                jQuery('.register-form').show();
	            jQuery('#register-success').hide();
                jQuery('.content').addClass('content_register');
                
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
                jQuery('#register-success').hide();
	            jQuery('.register-form').hide();
	            jQuery('.content').removeClass('content_register');
	        });
            
            jQuery('#btnresend').click(function () {
	           
                setAjaxRequest(base_url+'/sendVCode','key='+ $('#_token').val(),function(result) {
                	if (result.error == false) {		                		
                        alert('resend verification code success!');                                
                	} 
                	progressBar('off');
                }, function(error) {
                	alert("There was an error occurred. Please try again!");
                	progressBar('off');
                });
                         
	        });       
            
            jQuery('#btnconfirm').click(function () {	            
                window.location.href = base_url+'/registration/confirm?code='+ $('#vcode').val();                                                         
	        });       
                                                     
	}

	var handleReg = function() {
		$(document).ready(function() {
			if(getParameterByName('reg') == 'true') {
				jQuery('#register-btn').trigger('click');
			}
		});

		jQuery('body').on('click','#ResendConfirmCode', function() {
			setAjaxRequest(base_url+'/sendVCode','key='+$(this).attr('data-c'),
				function(result) {
					if (result.error == false) {
						msgbox('success',result.message);
					} else {
						msgbox('error',result.message);
					}
					progressBar('off');
				}
			);
		});
	}
    

    var handleValidation = function() {
		if (jQuery('#invpswd').val() != undefined && jQuery('#invpswd').val() == '1') {
			setTimeout(function() {
				window.location.href = base_url+'/changePswd?p=a&token='+jQuery('#_auth_token').val();
			},5000);
		}
	};

    return {
        //main function to initiate the module
        init: function () {
        	handleReg();
            handleLogin();
            handleForgetPassword();
            handleRegister();  
            handleValidation();  
        }

    };

}();