var dTables  = false;
var dtableID= '#onlinepayment_rectable';
var OnlineTxn = {
	setTable: function(){
	    var data  = $('#frm_search').serialize();
	    var xlink = base_url + 'cashier/online-transactions/event?event=load-list&'+data;
		    xlink = xlink.replace('&start=','&dtstart=');
		    xlink = xlink.replace('&end=','&dtend=');
			
		if(dTables!=false && dTables!=undefined){
		  dTables.destroy();
		}		
		
		if($(dtableID).length>0){
		  $('.table-scrollable').removeClass('table-scrollable');
		  dTables = $(dtableID).DataTable({
			   "processing": true,
               serverSide: true,
			   ajax: xlink,
			   "autoWidth":true,
			   "lengthMenu": [
								[10, 15, 20, 25, 50, -1],
								[10, 15, 20, 25, 50, "All"] // change per page values here
							  ]
			   ,"columnDefs": [{
								"targets": [1,2,6],
								"class":"text-center",
								"orderable": false
							   },{
								"targets": [0,3,4,7],
								"class":"text-left",
								"orderable": false
							   },{
								"targets": [5],
								"class":"text-right",
								"orderable": false
							   },]
			   ,"initComplete":function(settings,json){
							      $('.dataTables_filter').addClass('pull-right');
				               }
               ,"drawCallback":function(settings){
			                      /*  
			                      $('.rinfo').each(function(){
									   $(this).closest('tr').attr('data-receipt',$(this).attr('data-receipt'));
									   $(this).closest('tr').attr('data-void',$(this).attr('data-void'));
									   $(this).closest('tr').attr('data-type',$(this).attr('data-type'));
									   $(this).closest('tr').attr('data-txn',$(this).attr('data-txn'));
								  });
								  */
							   }
		  });
		}	
	},

};

$('document').ready(function(){
   OnlineTxn.setTable();
   
   $('body').on('click','.btnfilter',function(){
     OnlineTxn.setTable();
   });
   
   $('body').on('click','.btninfo',function(){
		var payid = $(this).closest('.data-info').attr('data-payment');
		var trgtrow = $(this).closest('tr');
		if($('.trinfo').length>0){
		  $('.trinfo').remove();
		}
		
        $(trgtrow).after('<tr class="trinfo"><td colspan="8"><center><i class="fa fa-refresh fa-spin"></i> Loading..</center></td></tr>');
		setAjaxRequest( base_url + 'cashier/online-transactions/event?event=reverse-entry', {entryid: payid },
					function(req) {
					   if(req.error==false){
					    $content = '<div class="row"><div class="col-sm-12"><a class="btn btn-danger pull-right" href="javascript:void(0);" onclick="'+"$('.trinfo').remove();"+'"><i class="fa fa-times"></i></a></div></div>'+req.content;
						$('.trinfo').find('td').html($content);
					   }else{
					    $('.trinfo').remove();
						msgbox('error', 'Failed to load info');
					   }
					   progressBar('off');
					},  
					function(err, resp_text) {
						progressBar('off');
						msgbox('error', resp_text);
					}, null, true
			);		
   });
   
   $('body').on('click','.btnreverse',function(){
	  var payid = $(this).closest('.data-info').attr('data-payment');
	  progressBar('on');
	  setAjaxRequest( base_url + 'cashier/online-transactions/event?event=reverse-entry', {entryid: payid },
				function(req) {
				   if(req.error==false){
					showModal({ name: 'large', title: '<i class="fa fa-money"></i> Payment Reversal', button:{caption:'Proceed', menu:'pay-reverse'}, content: req.content});
				   }else{
				   
				   }
				   progressBar('off');
				},  
				function(err, resp_text) {
					progressBar('off');
					msgbox('error', resp_text);
				}, null, true
		); 
   });
   
   $('body').on('click','.btncancel',function(){
	  var payid = $(this).closest('.data-info').attr('data-payment');
	  confirmEvent('<i class="fa fa-warning text-warning"></i> Are you sure you want to cancel this transaction?',function(data){
	     if(!data){return false;}
	     progressBar('on');
         setAjaxRequest( base_url + 'cashier/online-transactions/event?event=cancel-entry', {entryid: payid },
                    function(req) {
				 	   if(req.error==false){
					    $('.btnfilter').trigger('click');
                        progressBar('off');
					   }else{
                        progressBar('off');
                        msgbox('error', req.message);
					   }
					},
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
			);
	  });
   });
   
   $('body').on('click','[data-menu="pay-reverse"]',function(){
      var payinfo = $('#paydetail').attr('data-id');
	  var payid   = $('#paydetail').attr('data-id');
	  var withor  = $('#paydetail').find('.danger').length;
	  if(withor){
		msgbox('error','Transaction already in used in OR');
		return false;
	  }
	  
	  progressBar('on');
	  setAjaxRequest( base_url + 'cashier/online-transactions/event?event=revert-entry', {entryid: payid },
				function(req) {
				   if(req.error==false){
					$('.btnfilter').trigger('click');
					progressBar('off');
				   }else{
					progressBar('off');
					msgbox('error', req.message);
				   }
				},
				function(err, resp_text) {
					progressBar('off');
					msgbox('error', resp_text);
				}, null, true
		);
	  
   });
   
});