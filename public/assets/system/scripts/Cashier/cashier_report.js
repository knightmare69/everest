var Report_receipts = {
    
    menu: function(e){
        var d = $(e.target).data();
        if(d == undefined) return false;
        switch(d.menu){
            case 'print':  this.print();  break;
        }
    },

    print: function(){
                                      
         
              
    },
    
     bind : function(){                                       
        $('body').on('click','.btn', $.proxy(this.menu,this));
        $('body').on('click','.fa', function(){
            if( $(this).parent().hasClass('btn') ) {
                $(this).parent().click();
            }
        });
        $('body').on('click','.list-group-item', function(){
            $('.list-group-item').removeClass('active');
            $(this).addClass('active');
        });
        
        
        
        $('body').on('keypress', '.search-text', function(e) {
            if (e.keyCode == 13) {
                Receipts.initsearch();
                e.preventDefault();
            }
        });
    },
    plugins: function(){
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });

         $('.timepicker-default').timepicker({
            autoclose: true,
            showSeconds: true,
            minuteStep: 1
        });

    },
    init: function(){
        this.bind();
        this.plugins();        
    }
};