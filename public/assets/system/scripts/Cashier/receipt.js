var dTables  = false;
var Receipts = {
    
    menu: function(e){
        var d = $(e.target).data();
        if(d == undefined) return false;
        switch(d.menu){
            case 'search':  this.initsearch();  break;
            case 'void':  this.or_void();  break;
            case 'delete': this.or_delete(); break;
        }
    },
    or_delete: function(){
        var ornum = $('.active-row').attr('data-receipt');
        if(ornum==undefined || ornum==null){
			msgbox('error','Nothing selected!');
			return false;
		}
		
        confirmEvent('<i class="fa fa-warning text-danger"></i> Do you want to delete receipt :'+ornum+'?',function(data){
            if(data){
              setAjaxRequest( base_url + 'cashier/official-receipts/event?event=delete',  {ornum: ornum },
                    function(req) {
                        if (req.error == false) {
                            msgbox('success', req.message);
                            $('[data-menu="search"]').trigger('click');
                        } else {
                            msgbox('error', req.message);
                        }
                        progressBar('off');
                    },
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
                );   
            }        
        });  
    },
    
    or_void : function(){
        
        var ornum = $('.active-row').attr('data-receipt');
        if(ornum==undefined || ornum==null){
			msgbox('error','Nothing selected!');
			return false;
		} 
		
        confirmEvent('<i class="fa fa-warning text-danger"></i> Do you want to void receipt :'+ornum+'?',function(data){
            if(data){
              setAjaxRequest( base_url + 'cashier/official-receipts/event?event=void',  {ornum: ornum },
                    function(req) {
                        if (req.error == false) {
                            msgbox('success', req.message);
                            $('[data-menu="search"]').trigger('click');
        
                        } else {
                            msgbox('error', req.message);
                        }
                        progressBar('off');
                    },
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
                );   
            }        
        });                            
    },

    initsearch: function(){
       var data = $('#frm_search').serialize();
	    if(dTables!=false){
		    Receipts.setTable();
		}
		
	    if($('[data-menu="search"]').is('[disabled]')){
			return false;
		}
		
        $('[data-menu="search"]').attr('disabled',true);                   
        $('[data-menu="search"]').html('<i class="fa fa-refresh fa-spin"></i> Loading..');                   
        setAjaxRequest( base_url + 'cashier/official-receipts/event?event=orsearch',  data,
            function(req) {
                if (req.error == false) {		
				
                    $('.tx_list').html(req.view);       
                    $('[data-menu="search"]').removeAttr('disabled');                   
                    $('[data-menu="search"]').html('<i class="fa fa-filter"></i> Filter');
					Receipts.setTable();
					
                } else {
                    msgbox('error', req.message);
                    $('[data-menu="search"]').removeAttr('disabled');                   
                    $('[data-menu="search"]').html('<i class="fa fa-check-circle"></i> Search');
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
                $('[data-menu="search"]').removeAttr('disabled');                   
                $('[data-menu="search"]').html('<i class="fa fa-check-circle"></i> Search');
            }, null, true
        );        
    },
    
    bind : function(){               
        $('body').on('change','.txt-search',function(){
            $('.dataTables_filter').find('input[type="search"]').val($(this).val());		
			$('.dataTables_filter').find('input[type="search"]').trigger('onblur');
		});
		
        $('body').on('click','.btn', $.proxy(this.menu,this));
        $('body').on('click','.fa', function(){
            if( $(this).parent().hasClass('btn') ) {
                $(this).parent().click();
            }
        });

        $('body').on('keypress', '.search-text', function(e) {
            if (e.keyCode == 13) {
                Receipts.initsearch();
                e.preventDefault();
            }
        });
    },
    plugins: function(){
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

         $('.timepicker-default').timepicker({
            autoclose: true,
            showSeconds: true,
            minuteStep: 1
        });

    },
    init: function(){
        this.bind();
        this.plugins();        
		this.setTable();
    },
	
	setTable: function(){
	    var data = $('#frm_search').serialize();
	    var xlink = base_url + 'cashier/official-receipts/event?event=dtsearch&'+data;
		    xlink = xlink.replace('&start=','&dtstart=');
		    xlink = xlink.replace('&end=','&dtend=');
			
		if(dTables!=false && dTables!=undefined){
		  dTables.destroy();
		}		
		
		if($('[data-receipt]').length>0){
		  $('.table-scrollable').removeClass('table-scrollable');
		  dTables = $('#records-table').DataTable({
			   "processing": true,
               serverSide: true,
			   ajax: xlink,
			   "autoWidth":true,
			   "lengthMenu": [
								[10, 15, 20, 25, 50, -1],
								[10, 15, 20, 25, 50, "All"] // change per page values here
							  ]
			   ,"columnDefs": [{
								"targets": [0,12,13,14,15],
								"orderable": false
							   },{
								"targets": [1,2],
								"class":"text-center",
								"orderable": false
							   },{
								"targets": [3,4,5],
								"class":"autofit",
								"orderable": false
							   },{
								"targets": [6],
								"class":"autofit text-right bold info",
								"orderable": false
							   },{
								"targets": [7,8,9,10,11],
								"class":"autofit text-right",
								"orderable": false
							   },]
			   ,"initComplete":function(settings,json){
							      $('.dataTables_filter').addClass('pull-right');
				               }
               ,"drawCallback":function(settings){
			                      $('.rinfo').each(function(){
									   $(this).closest('tr').attr('data-receipt',$(this).attr('data-receipt'));
									   $(this).closest('tr').attr('data-void',$(this).attr('data-void'));
									   $(this).closest('tr').attr('data-type',$(this).attr('data-type'));
									   $(this).closest('tr').attr('data-txn',$(this).attr('data-txn'));
								  });
							   }
		  });
		}	
	}
};

$('document').ready(function(){
   $('body').on('click','.box',function(){
      var ornum = $(this).closest('tr').attr('data-receipt');
	  var xurl  = base_url + 'cashier/official-receipts/event?event=o-post';
	  var xmsg  = '<i class="fa fa-warning text-danger"></i> Do you want to post receipt :'+ornum+'?'
	  if($(this).is(':checked')){
	  
	  }else{
	     xurl  = base_url + 'cashier/official-receipts/event?event=o-upost';
	     xmsg  = '<i class="fa fa-warning text-danger"></i> Do you want to unpost receipt :'+ornum+'?'
	  }
	  
	  
	  confirmEvent(xmsg,function(conf){
		if(conf){
		  setAjaxRequest(xurl,  {ornum: ornum },
				function(req) {
					if (req.error == false) {
						msgbox('success', req.message);
						$('[data-menu="search"]').trigger('click');
					} else {
						msgbox('error', req.message);
						$('[data-menu="search"]').trigger('click');
					}
					progressBar('off');
				},
				function(err, resp_text) {
					progressBar('off');
					msgbox('error', resp_text);
				}, null, true
			);   
		}else{
			$('[data-menu="search"]').trigger('click');
		}        
	 });  
	  
   });
   
});