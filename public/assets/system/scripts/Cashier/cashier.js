var xloop      = false;
var online_rid = '';
var online_amt = 0;

var Cashiering = {

    id_num : '',
    id_typ : '',
    t_ref : '',
    t_typ : '',
    t_all : 0,

    getDetails: function(){
        var data=[];

        $('#records-table tbody tr').each(function(){
            data.push({
                id  : $(this).attr('data-ac')
               ,amt : $(this).attr('data-due')
               ,disc: $(this).attr('data-discount')
               ,trn : $(this).attr('data-trn')
               ,curr: $(this).attr('data-curr')
               ,rmks: $(this).attr('data-remarks')
               ,nonl: $(this).attr('data-nonledger')
               ,deptid : $(this).attr('data-deptid')
               ,deptcode : $(this).attr('data-dept')
               ,yearid : $(this).attr('data-yr')
               ,level : $(this).attr('data-lvl')
            })
        });

        return data;
    },

    getChecks: function(){

        var data=[];
        var i = 0;

        $('#tblchecks tbody tr').each(function()
        {
            i++;
            if($(this).find('#chkno'+i).val() != '')
            {
                data.push({
                    chktype    : $(this).find('#chktype'+i).val()
                   ,checkno : $(this).find('#chkno'+i).val()
                   ,chkdate : $(this).find('#chkdate'+i).val()
                   ,chkamount : $(this).find('#chkamt'+i).val()
                   ,chkbank : $(this).find('#chkbank'+i).val()
                })
            }

        });

        return i =='0' ? '' : data;
    },

    gen_new_ornum: function(orno){
        if(orno != ''){
            var ornum = parseInt(orno) + 1 ;
            localStorage.setItem("ornumber", ornum);
            $('#or_num').html(ornum);
        }
    },

    assessment: function(){

        var refno = $('#refs').find('option:selected').text();

        var data = { refno: refno,  'idno':  this.id_num , 'type': this.id_typ, 'txn' : this.t_typ, 'all': this.t_all , };

        setAjaxRequest( base_url + page_url + '/event?event=assessment', data ,
            function(r) {
                if (r.error == false) {
                    $('#transaction_details').html(r.view);
                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );
    },

    compute: function(mode){

        var total = 0 ;

        switch(mode){

            case 'item':

                $('#records-table tbody tr').each(function(){
                    total = parseFloat(total) +  parseFloat($(this).attr('data-due'));
                });

                $('#total-due').html(format_number(total));

            break;
            case 'check':

                $('.txt-check-amount').each(function(){
                    if($(this).val() != ''){ total +=  parseFloat($(this).val()); }
                });

                $('#total-check').html(format_number(total));
                this.compute('change');

            break;
            case 'card':

                $('.txt-card-amount').each(function(){
                    if($(this).val() != ''){ total += parseFloat($(this).val()) ; }
                });

                $('#total-card').html(format_number(total));
                this.compute('change');

            break;

            case 'change':

               var cash          = decimal($('#total-cash').val()) , check = decimal($('#total-check').html()), card = decimal($('#total-card').html());
			   var online        = decimal($('#total-online-debit').val());
               var due           = decimal($('#total-amountdue').html());
               var total_payment = ( parseFloat(cash) + parseFloat(check) + parseFloat(card) + parseFloat(online) );
               var total = decimal(total_payment)  - decimal(due);
			   
			   console.log(online);
			   

               //alert(inWords(due));
               $('#amtinwrd').val( inWords(due) );
               $('#total-change').html(total);

            break;

            case 'total-payment':

                var cash   =  decimal($('#total-cash').val()) , check = decimal($('#total-check').html()), card = decimal($('#total-card').html());
                var online =  decimal($('#total-online-debit').val());
				
                total = ( parseFloat(cash) + parseFloat(online) + parseFloat(check) + parseFloat(card) ) ;

            break;
        }

        return total;
    },
    print_receipt: function(){
        var orno = $('#or_num').html().toString();
        window.open(base_url + page_url+'/print?or='+ orno );
    },
    save_payment: function(){
        var xrno = (($('[data-orno]').length>0)?$('[data-orno]').attr('data-orno'):'');
        var orno = $('#or_num').html().toString().trim();
        var data = $('#form_payment').serialize();
        var check = $('#total-check').html().toString().trim();
        var card = $('#total-card').html().toString().trim();
        var name = $('#txtname').val();
        var term = $('#refs').find('option:selected').attr('data-tid');


        if( this.compute('total-payment') == '0' ){
            msgbox('danger','Invalid Amount! Payment cannot proceed.');
            return false;
        }

        var details = this.getDetails();
        var checks = this.getChecks();

        setAjaxRequest( base_url + page_url + '/event?event=save-payment&'+
                    data + '&check='+ check + '&card=' + card +'&due='+ $('#total-amountdue').html() + '&orno='+ orno + '&xrno='+ xrno +
                    '&date='+ $('#or_date').html() + '&term='+ term ,
                    { 'name':name , 'particular':$('#particular').val() , 'details' : details , 'checks': checks ,'online':online_rid }  ,
            function(r) {
                if (r.error == false){
                    Cashiering.gen_new_ornum(orno);
                    $('#modal_large').modal('hide');
                    sysfnc.msg('Success!', r.message);
                    if ($('#print').prop('checked')){
                        window.open(base_url + page_url+'/print?or='+ orno );
                        //console.log('print_or');
                        //$('.for_printing').html(r.view);
                    }
					
					if($('[data-menu="edit"]').length>0){
                     window.setTimeout(function(){
						window.location.href = base_url + page_url;
						},2500);
					}else{
					  window.location.href = base_url + page_url;
					}
                } else {
                    sysfnc.showAlert('.modal-body','danger','Invalid OR number',true,true, 'warning',20);
                     Metronic.blockUI({
                             target: '.modal-body',
                             boxed: true,
                             message: '<i class="fa fa-exclamation"></i>' + r.message ,
                             textOnly: true
                        });
                    window.setTimeout(function() {
                            Metronic.unblockUI('.modal-body');
                    }, 2000);
                }
            }, null, 'json', true, false
        );


    },
    payment_sched: function(){

        var type = $('#ptype').attr('data-id'), txn = $('#txn').val(), idno = $('#txtname').attr('data-id');

        if( (idno == '' || idno == undefined) && type != 5  ){
          msgbox('warning','Please search payer');
          return false;
        }

        setAjaxRequest( base_url + page_url + '/event?event=get-schedule', { 'idno':  idno , 'type': type, 'txn' : txn, ref : $('#refs').val() },
            function(r) {
                if (r.error == false) {

                    showModal({ name: 'large', title: 'Payment Schedule', button:{caption: 'Proceed',menu:'sched-select'}, content: r.schedule });

                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );
    },

    paymentproceed: function(){

        var type = $('#ptype').attr('data-id'), idno = $('#txtname').attr('data-id'),
            txn = $('#txn').val(), ref = $('#refs').val(), due = $('#total-due').html().trim(),
            payor = $('#txtname').val();

        var validation = $('#refs').find('option:selected').attr('data-validation');

        if((idno == '' || idno == undefined) && (type !=5 && type!=2)){
          msgbox('warning','Please search payer');
          return false;
        }
        if( payor  == '' && type == 5  ){
          msgbox('warning','Please input payor Name');
          return false;
        }
        if( type == 5){
          ref =$('#or_num').text();
          idno = '';
        }

        setAjaxRequest( base_url + page_url + '/event?event=get-payment', { 'idno':  idno , 'type': type, 'txn' : txn, due: due, 'ref': ref },
            function(r) {
                if (r.error == false) {
                    showModal({ name: 'large', title: 'Payment Form', content: r.form });
                    var isedit = $('[data-orno]').length;
                    if(validation != '' && isedit<=0){
                        $('#validate').hide();
						$('<i class="fa fa-check-square-o"></i>').insertAfter('#validate');
                    }else{
                        $('#validate').show();
                    }
                    
                    $('.btn_modal').attr('data-menu','save-payment');
					
					//Put online payment - amount and details
					if(online_rid!='' && online_rid!=undefined){
					  $('#total-online-debit').attr('readonly',true);
					  $('#total-online-debit').val(online_amt);
					  $('#total-online-debit').focus();
					  Cashiering.compute('total-payment');
					  Cashiering.compute('change');
					}
                } else {
                    msgbox('error', r.message);
                }
            }, null, 'json', true, false
        );

    },

    auto_distribution : function(){
       var id = $('#txtname').attr('data-id');

       if( id == '' || id == undefined ){
          msgbox('warning','Please search payer');
         return false;
       }
       var callbaks = function(value){
            var total = value;
            var subtotal = 0;

            if(value ==0){
                return false;
            }
            $('#records-table tbody tr').each(function(){

                var bal = $(this).attr('data-due');
                 if ( parseFloat(total) > parseFloat(bal) && parseFloat(total) > 0 ){
                    total = total - bal;
                    subtotal =  subtotal + parseFloat(bal);
                 }else if (parseFloat(total) <= parseFloat(bal) && parseFloat(total) > 0 ){

                    $(this).attr('data-due', total);
                    $(this).find('td.due').html(format_number(total));
                    $(this).find('td.balance').html(format_number(total) );
                    $(this).find('td.discount').html('0.00');
                    subtotal =  subtotal + parseFloat(total);

                    total = 0;
                 }else{
                    $(this).remove();
                 }
            });

            $('#total-due').attr('data-value',subtotal);
            $('#total-due').html(format_number(subtotal));
       };

       sysfnc.inputBox('Auto Distribution', 'Please input value to distribute','text',callbaks);
    },

    verify : function(){
        var or_num = $('#setup_orno').val();
        $('.setup-or-group').removeClass('has-error');
        $('.setup-or-group').removeClass('has-success');
        $('.setup-or-group').find('.help-block').html('');
        console.log('Checking:'+or_num);
        if( or_num !=''){
            setAjaxRequest( base_url + page_url + '/event?event=verify', { 'or_no': or_num  },
                function(r) {
                    if (r.error == false) {
                        $('.setup-or-group').addClass('has-success');
                        $('.setup-or-group').find('.help-block').html('OR number is valid to use');
                        $('#setup_orno').attr('data-valid','1');
                    } else {
                        $('.setup-or-group').addClass('has-error');
                        $('.setup-or-group').find('.help-block').html('Invalid OR number. OR already used.');
                        $('#setup_orno').attr('data-valid','0');
                    }
                }, null, 'json', true, false
            );
        }else{
		   $('#setup_modal').modal('show');
		}
    },

    set_setup : function(){
        if ( $('#setup_orno').attr('data-valid') == '1' ){

            var ornum = $('#setup_orno').val();
            var date = $('#setup_datetime').val();

            var udate = $('#setup_date').val();
            var utime = $('#setup_time').val();

            localStorage.setItem("ornumber", ornum);
            localStorage.setItem("ordatetime", date);

            localStorage.setItem("or_userdate", udate);
            localStorage.setItem("or_usertime", utime);

            this.initsetup();

            $('#setup_modal').modal('hide');

        } else{
            sysfnc.msg('Invalid OR Number', 'Please re-configure official receipt number');
        }

    },

    initsetup : function(){

        var st = srvTime();
        var newdate = new Date(st);

        var ornum = localStorage.getItem("ornumber");
        var date  = 0;
        
		localStorage.setItem("ordatetime",0);
        if(ornum == undefined || ornum == ''){
            this.setup();
        }else{
            $('#or_num').html(ornum);
            $('#setup_orno').val(ornum);
			
			if($('#setup_datetime').val()==1){
			   $('#or_date').attr('data-date',($('#setup_date').val()+' '+$('#setup_time').val()));
			}

            if(date == '0'){
                $('#or_date').html($('#or_date').attr('data-date'));
            }else{
                $('#or_date').html(newdate);
            }
        }

        var v = localStorage.getItem("all_txn");
        if(v == true) {
            this.t_all = v ;
            $('#chk_all').attr('checked', v);
        }

    },
    setup : function(){

        var ornum = localStorage.getItem("ornumber");
        var date = localStorage.getItem("ordatetime");
        if($('[data-orno]').length<=0){
		 $('#setup_datetime').val(0);
		}
		
        $('#setup_orno').val(ornum);
        $('#setup_modal').modal('show');

    },

    set_ref: function(ref){

        var data = $('#refs').find('option:selected').data();

        $('.term').html(data.term);
        $('.date').html(data.date);
        $('.remarks').html(data.remarks);
        $('.validation').html(data.validation);

    },

    set_idtype: function(e){
	    var x = $('[data-orno]').attr('data-ptype');
		var t = 1;
        if(x==undefined || x==''){
			t = $(e.target).attr('data-type');
			$('#ptype').find('span').html($(e.target).html());
		}else{
			t = x;
			$('#ptype').find('span').html($('.idtype[data-type="'+x+'"]').html());
		}	
		
		$('#ptype').attr('data-id',t);
		$('.entryphoto').attr('src',base_url+'assets/system/media/images/no-image.png?4241');
        if(t == 4 ){
            $('#txn').val(5);
            $('#idlabel').text('App #:');
            $('#ledger').attr('href',base_url+'accounting/studentledger' );
            this.clearTable();
        }else if(t == 5  || t == 2){
            $('#txn').val(15);
            $('#idlabel').text('ID #:');
            $('#txtname').attr('placeholder','Please Input Payor Name');
            $('#refs').val(-1);
            $('#tidno').text('');
            $('#tyr').text('');
            $('#ledger').attr('href',base_url+'accounting/studentledger' );
            this.clearTable();
        }else{
          $('#txn').val(1);
          $('#idlabel').text('ID #:');
          $('#txtname').attr('placeholder','Search by IDNo, Name');
        }

        // clearfields
        $('#txtname').val('');
        $('.term').text('');
        $('.date').text('');
        $('.remarks').text('');
        $('.validation').text('');

    },

    loadRefs : function(ref){
        var idno = $('#txtname').attr('data-id');
        if (idno =='' || idno == undefined ){
            msgbox('warning','Please search payer');
            return false;
        }
        this.initRefs(idno);
    },

    initRefs : function(idno){
        $('#refs').html('');

        this.id_num = idno;
        this.id_typ = $('#ptype').attr('data-id');
        this.t_typ = $('#txn').val();

        setAjaxRequest( base_url + page_url + '/event?event=get-refs', { 'idno':  idno , 'type': this.id_typ , 'txn' : this.t_typ, 'all' : this.t_all  },
            function(r) {
                if (r.error == false) {
                    $('#refs').html(r.data);
                    $('#obalance').html(r.obalance);
                    $('#transaction_details').html(r.assessment);

                    if($('#ptype').attr('data-id') == 4 ){


                  }else {
                    Cashiering.set_ref( $('#refs').val() );
                  }
                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );
    },
	
    xnitRefs : function(idno){
        var orno = $('#or_num').html();
		$('#refs').html('');

        this.id_num = idno;
        this.id_typ = $('#ptype').attr('data-id');
        this.t_typ = $('#txn').val();
     
        setAjaxRequest( base_url + page_url + '/event?event=get-refs', { 'orno': orno , 'idno':  idno , 'type': this.id_typ , 'txn' : this.t_typ, 'all' : this.t_all  },
            function(r){
                if (r.error == false) {
                    var xref = $('[data-menu="edit"]').attr('data-ref');
                    var xtxn = $('[data-menu="edit"]').attr('data-txn');
					$('#refs').html(r.data);
                    $('#obalance').html(r.obalance);
                    $('#transaction_details').html(r.assessment);
                    
                    if(xref!='' || xtxn!=''){
					  $('#refs').val(xref);
					  $('#txn').val(xtxn);				  
					  
					  $('[data-menu]').each(function(){
							var tmpmenu = $(this).attr('data-menu');
							if(tmpmenu=='accept' || tmpmenu=='template' || tmpmenu=='setup' || 
							   tmpmenu=='clear-table' || tmpmenu=='add-row' || tmpmenu=='rem-row'|| 
							   tmpmenu=='full' || tmpmenu=='schedule' || tmpmenu=='auto'){
							   $(this).removeAttr('data-menu');
							   $(this).attr('data-disabled',tmpmenu);
							   $(this).attr('disabled',true);
							}			
					  });	
					}
					
                    if($('#ptype').attr('data-id') == 4 ){

                    }else{
					   if($('#refs').val()!==undefined && $('#refs').val()!==null){
					    Cashiering.set_ref($('#refs').val());
					   }
                    }
                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );
    },

    initsearch: function(){
	    var ptype = $('#ptype').attr('data-id')
        if(ptype==1){
		  Students.get({'student-search-text': $('#txtname').val(), 'search-campus': 1, 'ptype': $('#ptype').attr('data-id') });
		}else{
		  if(ptype==5){
		    msgbox('warning','Not applicable');
		  }else{
		    Employees.search_list({'filter-text': $('#txtname').val(), 'search-campus': 1, 'ptype': $('#ptype').attr('data-id') });
		  }
		}
    },

    menu: function(e){
        var d = $(e.target).attr('data-menu');
        //if(d == undefined) return false;
        switch(d){
            case 'search':  this.initsearch();  break;
            case 'ref-refresh': this.loadRefs(); break;
            case 'setup': 
			  $('.datetime-setup').removeClass('hidden'); 
			  this.setup(); 
			break;
            case 'save-setup': this.set_setup(); break;
            case 'verify': this.verify(); break;
            case 'auto': this.auto_distribution(); break;
            case 'accept': this.paymentproceed(); break;
            case 'schedule': this.payment_sched(); break;
            case 'full': this.loadRefs(); break;
            case 'save-payment': this.save_payment(); break;
            case 'orsearch': this.orsearch(); break;
            case 'void': this.voidOR(); break;
            case 'edit': this.editOR(); break;
            case 'template': this.template(); break;
            case 'soa': this.soa(); break;
            case 'fee-select': this.setAssessment(); break;
            case 'sched-select': this.populate_paymentsched(e); break;
            case 'print-receipt': this.print_receipt(); break;
            case 'add-row': this.addRow(); break;
            case 'add-row-proceed': this.appendRow(); break;
            case 'rem-row': this.removeRow(); break;
            case 'clear-table': this.clearTable();break;
        }
    },
    clearTable: function(){

        $('#records-table tbody tr').remove();
        this.compute('item');

    },
    removeRow: function(){

        $('#records-table tbody tr.active-row').remove();
        this.compute('item');

    },
    appendRow: function(){
      var seq  = $('#records-table tbody tr').length;
      var amt = $('#acct_amt').val();
      if (1 ==1 ){
        var $row ='<tr data-due="'+amt+'" data-ac="'+$('#acct').val()+'" data-code="'+$('#acct').find('option:selected').attr('data-code')+'"'
            $row+=  'data-discount="0" data-first="'+amt+'" data-curr="1" data-txn="15" data-ref="" data-trn="" data-remarks="'+$('#acct_desc').val()+'" data-nonledger="1" data-deptid="" data-dept="" ';
            $row+='  data-yr="" data-lvl="" class="active-row"> <td class="font-xs autofit seqno bold">'+sysfnc.sum(seq,1)+'</td> <td class="autofit code">'+$('#acct').find('option:selected').attr('data-code')+'</td> <td class="name">'+$('#acct').find('option:selected').attr('data-name')+'</td> <td class="autofit text-right balance bold "> <input type="text" class="numberonly text-right balance-amt input-no-border input-xsmall " value="'+format_number(amt)+'"> </td> <td class="autofit text-right discount bold "> <input type="text" class="numberonly text-right discount-amt input-no-border input-xsmall " value="0.00"> </td> <td class="autofit danger bold text-right due">'+format_number(amt)+'</td> <td class="remarks">'+$('#acct_desc').val()+'</td> <td class="autofit text-center"><input type="checkbox" class="non-ledger" checked> </td> <td class="autofit class-short"> </td> <td class="autofit text-center txn_id"> </td> <td class="autofit ref_no"> </td> </tr>'
      }else{
        var $row = $('#records-table tfoot tr:first').clone();



        $row.attr('data-ac', $('#acct').val());
        $row.attr('data-txn', $('#acct_txn').val());

        $row.attr('data-code', $('#acct').find('option:selected').attr('data-code'));

        $row.attr('data-due', amt );
        $row.attr('data-first', amt );
        $row.attr('data-nonledger', 1 );
        $row.attr('data-remarks', $('#acct_desc').val() );
        $row.find('td.seqno').html(sysfnc.sum(seq,1) );
        $row.find('td.code').html($('#acct').find('option:selected').attr('data-code') );
        $row.find('td.name').html($('#acct').find('option:selected').attr('data-name') );
        $row.find('.balance-amt').val(amt);
        $row.find('td.due').html( amt );
        $row.find('.non-ledger').prop('checked',true);
        $row.find('td.remarks').html( $('#acct_desc').val() );



      }


       $('#records-table tbody').append($row);
       $('#modal_account').modal('hide');
       this.compute('item');

    },

    addRow: function(){
      var idno = $('#txtname').attr('data-id');
      var type = $('#ptype').attr('data-id');
      var payor =  $('#txtname').val();
      console.log(type)
      if( (idno == '' || idno == undefined) && type==1){
        msgbox('warning','Please search student');
        return false;
      }else{

        if( payor  == '' && type != 1 ){
          msgbox('warning','Please input payor Name');
          return false;

        }else{
          $('#acct').val(-1);
          $('#acct_desc').val('');
          $('#acct_amt').val(0);
          $('#modal_account').find('.modal-title').html('Select an account/entry');
          $('#modal_account').modal('show');
        }


        }
    },

    setAssessment: function(){

        var num = $('#txtname').attr('data-id');
        var term = $('#refs').find('option:selected').attr('data-tid');
        var f = $('#fees-table tbody tr.active-row').attr('data-id');
        var ref = $('#refs').val();
        var txn = $('#txn').val();

        setAjaxRequest( base_url + 'cashier/cashiering/event?event=get-fees', {idno: num, term: term, fee: f, refno:ref, txn: txn },
            function(req) {
                if (req.error == false) {
                    Cashiering.initRefs(num);
                    $('#modal_large').modal('hide');
                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );

    },

    soa: function(){

        var studno = $('#tidno').html();
        var term = $('#refs').find('option:selected').attr('data-tid');
    	var refno = $('#refs').val();

    	var url = base_url+'enrollment/print_report?event=soa&term='+atob(term)+'&campus=1&regid='+atob(refno)+'&studno='+studno;
      //window.open(base_url+'enrollment/print_report?event=assessment&term='+term+'&campus='+campus+'&regid='+regid+'&studno='+studno+'&yrlvl='+yrlvl+'&major='+major+'&progid='+progid,'_blank');
        if(studno == '' || studno == undefined || refno == '' || refno == undefined ){
    	   msgbox('warning','Payor Account ID not found!');
    	   return false;
    	}

        window.open(url,'_blank');

    },

    template: function(){
        var num = $('#txtname').attr('data-id');
        var term = $('#refs').find('option:selected').attr('data-tid');
        var validate = $('#refs').find('option:selected').attr('data-validation');

        if(num == '' || num == undefined){
            msgbox('warning', 'Student not found!');
            return false;
        }

        if( validate == '' || validate == undefined ){

            setAjaxRequest( base_url + 'cashier/cashiering/event?event=fees', {idno: num, term: term },
                function(req) {
                    if (req.error == false) {

                        showModal({ name: 'large', title: 'Searched Fees Template ',button:{caption: 'Select',menu:'fee-select'}, content: req.list});

                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
            );
        }else{
            msgbox('warning', 'Unable to proceed student transaction already validated...');
        }

    },

    orsearch: function(){

        setAjaxRequest( base_url + 'cashier/cashiering/event?event=orsearch',  {search: $('#orsearch').val()},
            function(req) {
                if (req.error == false) {

                    showModal({ name: 'large', title: 'Searched Official Receipt(s)',button:{caption: 'Select',menu:'orsel'}, content: req.list});

                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );

    },

    editOR: function(){

    },

    voidOR: function(){

        var inw = inWords( $('#orsearch').val() );
        alert(inw);
    },

    recompute_item : function(e){
        var $r = $(e).closest('tr');
        var due = 0, dsc = 0, total = 0;

        due =  $r.find('.balance-amt').val();
        dsc =  $r.find('.discount-amt').val();
        total = decimal(due) - decimal(dsc);

        $r.attr('data-due', total);
        $r.attr('data-discount', dsc);
        $r.find('td.due').html(format_number(total));

        Cashiering.compute('item');
    },

    populate_paymentsched: function(e){

        var col = $(e.target).attr('data-scheme');
        var refno = $('#refs').find('option:selected').text();
        var data = { refno: refno,  'idno':  this.id_num , 'type': this.id_typ, 'txn' : this.t_typ, 'col': col };

        setAjaxRequest( base_url + page_url + '/event?event=get-payment-schedule', data ,
            function(r) {
                if (r.error == false) {
                    $('#records-table tbody').html(r.view);
                    $('#modal_large').modal('hide');
                    Cashiering.compute('item');
                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );

    },

    bind : function(){

        $('body').on('change, blur','.balance-amt, .discount-amt ', function(){
            Cashiering.recompute_item(this);
        });
        $('body').on('keypress', '.balance-amt, .discount-amt', function(e) {
            if (e.keyCode == 13) {
                Cashiering.recompute_item(this);
                e.preventDefault();
            }
        });

        $('body').on('click','.btn,.icon-btn ', $.proxy(this.menu,this));
        $('body').on('click','.fa', function(){
            if( $(this).parent().hasClass('btn') ) {
                $(this).parent().click();
            }
        });

        $('body').on('click', '.student-select', $.proxy(Students.info,this));
        $('body').on('click', '.employee-select', $.proxy(Employees.select_emp,this));
        $('body').on('click','.idtype', $.proxy(this.set_idtype,this));
        $('body').on('change','#refs', function(){
            Cashiering.set_ref( $('#refs').val() );
            Cashiering.assessment();
        });

        $('body').on('change, blur','.txt-check-amount', function(){
            Cashiering.compute('check');
        });

        $('body').on('change, blur','.txt-card-amount', function(){
            Cashiering.compute('card');
        });

        $('body').on('change','#chk_all', function(){
            var vl = $(this).prop('checked') ? 1 : 0 ;
            localStorage.setItem("all_txn", vl );
            Cashiering.t_all = vl;
            Cashiering.set_ref( $('#refs').val() );
        });

        $('body').on('keypress', '#txtname', function(e) {
            if (e.keyCode == 13) {
                Cashiering.initsearch();
                e.preventDefault();
            }
        });

        $('body').on('keypress', '.total-cash', function(e) {

            var k = e.keyCode ;

            if(k == 10) {
                $(this).val( $('#total-amountdue').html().trim()) ;
            }
            if(k == 13) {
               Cashiering.compute('change');
               e.preventDefault();
               return false;
            }

        });

        $('body').on('keydown, blur', '.total-cash', function(e) {
            Cashiering.compute('change');
        });

        $('body').on('click', '.fees-filter', function(e) {
            $('.fees-filter').removeClass('active');
            $(this).addClass('active');

            var f = $(this).attr('data-val');

            if(f == 0){

                 $('#fees-table tbody tr').removeClass('hide');

            }else{

                $('#fees-table tbody tr').each(function(){
                    if( $(this).attr('data-scheme') == f ){
                        $(this).removeClass('hide');
                    }else{
                        $(this).addClass('hide');
                    }
                });
            }


        });

        $('body').on('keypress', '#txt1', function(e) { if(e.keyCode == 13) { $('#bot2-Msg1').click(); }});
        $('body').on('click','.pay-option', function(){
            var s = $(this).attr('data-scheme');

            $('#schedule-table tbody td').removeClass('danger');
            $('#schedule-table tbody td').each(function(){
              if($(this).attr('data-scheme') == s){
                $(this).addClass('danger');
              }
            });

            $('.ctotal').removeClass('danger');
            $('.ctotal').each(function(){
               if($(this).attr('data-scheme') == s){
                $(this).addClass('danger');
              }
            });

            $('.btn[data-menu=sched-select]').attr('data-scheme', s);
        });


    },

    plugins: function(){
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });

         $('.timepicker-default').timepicker({
            autoclose: true,
            showSeconds: true,
            minuteStep: 1
        });
    },
    init: function(){
        this.bind();
        this.plugins();
        this.initsetup();
		console.log('testing');
		Cashiering.verify();
    }
};

var Students = {
    get: function(params) {
        setAjaxRequest( base_url + 'students/event?event=get',  params,
            function(req) {
                if (req.error == false) {

                    showModal({ name: 'basic', title: 'Searched Student(s)', content: req.list});

                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#students-table-res').DataTable({ 'bDestroy': true, 'aaSorting': []});

                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },

    info: function(e) {

        var stud_no = $(e.target).attr('data-id');
        $('.modal').modal('hide');
        setAjaxRequest( base_url + 'students/event', 'event=get_info&student_no=' + stud_no + '&form=profile&ptype='+$('#ptype').attr('data-id'),
            function(req) {
                if (req.error == false) {

                    $('#txtname').val(req.name.trim());
                    $('#txtname').attr('data-id',req.stud_num_e);

                    $('#tidno').html(req.stud_num);
                    $('#tyr').html(req.year_level);
                    $('#stud_photo').attr('src', req.photo);
                    $('#ledger').attr('href',base_url+'accounting/studentledger/ledger?type=1&idno='+req.stud_num_e );

                    Cashiering.initRefs(req.stud_num_e);

                    progressBar('off');
                    return true;
                } else {
                    msgbox('error', req.message);
                }
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },

    xnfo: function() {

        var stud_no = $('[data-menu="edit"]').attr('data-id');
        $('.modal').modal('hide');
        setAjaxRequest( base_url + 'students/event', 'event=get_info&student_no=' + stud_no + '&form=profile&ptype='+$('#ptype').attr('data-id'),
            function(req) {
                if (req.error == false) {

                    $('#txtname').val(req.name.trim());
                    $('#txtname').attr('data-id',req.stud_num_e);

                    $('#tidno').html(req.stud_num);
                    $('#tyr').html(req.year_level);
                    $('#stud_photo').attr('src', req.photo);
                    $('#ledger').attr('href',base_url+'accounting/studentledger/ledger?type=1&idno='+req.stud_num_e );

                    Cashiering.xnitRefs(req.stud_num_e);

                    progressBar('off');
                    return true;
                } else {
                    msgbox('error', req.message);
                }
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },
    search_list: function() {

        setAjaxRequest(
            base_url + page_url + 'event', 'event=search&data_search=' + vars.searched_string + '&filters=' + JSON.stringify(vars.filter_stacked),
            function(req) {
                if (req.error == false) {
                    $('.div-stud-list').addClass('hide');
                    $('.div-stud-search-res').removeClass('hide').find('.general-item-list').html(req.list);
                    Students.get_students_photo();
                } else {
                    msgbox('error', req.message);
                }
            }, '', '', true
        );

        progressBar('off');
    },
};

var Employees = {
	search_list:function(params){
	    
        setAjaxRequest( base_url + 'cashier/cashiering/event?event=get-employees',  params,
            function(req) {
                if (req.error == false) {

                    showModal({ name: 'basic', title: 'Searched Employee(s)', content: req.list});

                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#employee-table-res').DataTable({ 'bDestroy': true, 'aaSorting': []});
					setTimeout(function(){
					$('#employee-table-res_filter').find('input[type="search"]').val($('#txtname').val());
					$('#employee-table-res_filter').find('input[type="search"]').trigger('change');
					},2000);

                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
		
	},
	select_emp:function(e){
	    var trdata = $(e.target).data();
		
		$('#txtname').val(trdata.name.trim());
		$('#txtname').attr('data-id',btoa(trdata.id));

		$('#tidno').html(trdata.id);
		$('#tdept').html(trdata.dept);
		$('.entryphoto').attr('src',base_url+'assets/system/media/images/no-image.png?4241');
        $('.modal').modal('hide');
	},
};

function jsDates(){
  console.log('checking');
  var display_date ='invalid';
  var date    = localStorage.getItem("ordatetime");
  var dtsetup = $('#setup_datetime').val();
  if(php_yrs!=undefined && php_mon!=undefined && php_day!=undefined && date!=1){
    php_sec = php_sec+2;
	if(php_sec>=60){
	  php_sec = php_sec-60;
	  php_min = php_min+1;
	}
	if(php_min>59){
	  php_min = php_min-60;
	  php_hrs = php_hrs+1;
	}
	if(php_hrs>23){
	  php_hrs = php_hrs-24;
	  php_day = php_day+1;
	}
	
	var display_odate=((php_mon<10)?('0'+php_mon):php_mon)+'/'+((php_day<10)?('0'+php_day):php_day)+'/'+php_yrs;
	var display_otime=((php_hrs<10)?('0'+php_hrs):php_hrs)+':'+((php_min<10)?('0'+php_min):php_min)+':'+((php_sec<10)?('0'+php_sec):php_sec);
	    display_date =php_yrs+'-'+((php_mon<10)?('0'+php_mon):php_mon)+'-'+((php_day<10)?('0'+php_day):php_day)+' '+((php_hrs<10)?('0'+php_hrs):php_hrs)+':'+((php_min<10)?('0'+php_min):php_min)+':'+((php_sec<10)?('0'+php_sec):php_sec);	
	
	if(dtsetup==0){
	 $('#or_date').html(display_date);
	 $('.datetime-setup').find('.date-picker').val(display_odate);	
	 $('.datetime-setup').find('.timepicker').val(display_otime);
	}
  }else{
    console.log(php_yrs+' - '+php_mon+' - '+php_day+' - '+date);
	//checkDates();
  }
}

function checkDates( ){
    var st = srvTime(), newdate = new Date(st);
    var date = localStorage.getItem("ordatetime");

    if(date == '1'){
        $('#or_date').html($('#or_date').attr('data-date'));
    }else{
        $('#or_date').html(sysfnc.parseDate(newdate));
    }
}

function lockbuttons(){
	$('[data-menu]').each(function(){
		var tmpmenu = $(this).attr('data-menu');
		if(tmpmenu=='accept' || tmpmenu=='template' || tmpmenu=='setup' || tmpmenu=='search' || 
		   tmpmenu=='clear-table' || tmpmenu=='add-row' || tmpmenu=='rem-row'|| 
		   tmpmenu=='full' || tmpmenu=='schedule' || tmpmenu=='auto'){
		   $(this).removeAttr('data-menu');
		   $(this).attr('data-disabled',tmpmenu);
		   $(this).attr('disabled',true);
		}			
	});
	
	$('.alert-posted').removeClass('hidden');
	$('[data-menu="print-receipt"]').removeAttr('disabled');
}

function unlockbuttons(){
	$('[data-disabled]').each(function(){
		var tmpmenu = $(this).attr('data-disabled');
	    $(this).removeAttr('data-disabled');
	    $(this).attr('data-menu',tmpmenu);
	    $(this).removeAttr('disabled');	
	});
	
	$('.alert-posted').addClass('hidden');
}

$('document').ready(function(){
    $('body').on('change','#setup_datetime',function(){
		var tmpval = $(this).val();
		
		if(tmpval==1){
		   clearInterval();
		   $('#setup_date').removeAttr('readonly');
		   $('#setup_time').removeAttr('readonly');
		}else{
		   setInterval(jsDates, 2000);
		   $('#setup_date').attr('readonly',true);
		   $('#setup_time').attr('readonly',true);
		}
	});
	
    if ($('[data-menu="edit"]').length>0){
		var orno  = $('[data-orno]').attr('data-orno');
		var ordt  = $('[data-orno]').attr('data-date');
		var orxd  = $('[data-orno]').attr('data-xdate');
		var orxt  = $('[data-orno]').attr('data-xtime');
		var ptype = $('[data-orno]').attr('data-ptype');
		var txn   = $('[data-orno]').attr('data-txn');
		var xref  = $('[data-orno]').attr('data-ref');
		var idno  = $('[data-orno]').attr('data-id');
		var payor = $('[data-orno]').attr('data-payor');
		var prtcl = $('[data-orno]').attr('data-particular');
		
		clearInterval();
		
		localStorage.setItem("ornumber", orno);
		localStorage.setItem("ordatetime", 1);
        $('#or_date').attr('data-date',ordt);
        $('#or_date').html(ordt);
		$('#setup_orno').val(orno);
		$('#setup_orno').attr('data-valid',1);
		$('#setup_datetime').val(1);
		$('#setup_date').val(orxd);
		$('#setup_time').val(orxt);
		
		Cashiering.set_idtype($('.idtype[data-type="'+ptype+'"]'));
		$('.idtype[data-type="'+ptype+'"]').trigger('click');
		$('#ptype').attr('data-id',ptype);
		
		$('#txn').val(txn);
		$('#refs').val(xref);
		$('#particular').val(prtcl);
		
		if(ptype==1 && txn==1){
			Students.xnfo();
		}else{
		    $('#txtname').val(payor);
			if(idno!=''){
			  $('#txtname').attr('data-id',btoa(idno));
			  $('#tidno').html(idno);
			}else{
			  idno='0';
			}
			
			Cashiering.xnitRefs(btoa(idno));
		}
		lockbuttons();
    }else{
		setInterval(jsDates, 2000);
		$('#setup_date').attr('readonly','readonly');
		$('#setup_time').attr('readonly','readonly');
		setTimeout(function(){Cashiering.verify();},2500);
	}
	
	$('body').on('change','#acct',function(){
	  var tmpval = $(this).val();
	  
	  if(tmpval>0 && tmpval!=''){
	    var tmpamt = $(this).find('option[value="'+tmpval+'"]').attr('data-amt');
		if(tmpamt!='' && tmpamt!=undefined){
			$('#acct_amt').val(parseFloat(tmpamt));
		}else{
			$('#acct_amt').val('0.00');
		}
	  }
	});
	
	$('body').on('click','[data-disabled],[disabled]',function(){
	   msgbox('error','Transaction Already Posted!');
	});
	
	$('body').on('click','[data-menu="edit"]',function(){
	   unlockbuttons();
	   $(this).removeAttr('data-menu');
	   $(this).attr('data-disabled','edit');
	   $(this).attr('disabled',true);
	});
	
	$('body').on('click','[data-menu="void"]',function(){
	   var ornum = $('[data-orno]').attr('data-orno');
         
        confirmEvent('<i class="fa fa-warning text-danger"></i> Do you want to void receipt :'+ornum+'?',function(data){
            if(data){
              setAjaxRequest( base_url + 'cashier/official-receipts/event?event=void',  {ornum: ornum },
                    function(req) {
                        if (req.error == false) {
                            msgbox('success', req.message);
						    window.location.href = base_url + page_url; 
                        } else {
                            msgbox('error', req.message);
                        }
                        progressBar('off');
                    },
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
                );   
            }        
        });
	});
	
	$('body').on('click','[data-menu="online"]',function(){
	    var idno = $('#txtname').attr('data-id');
		
		if(idno=='' || idno==undefined){
			msgbox('error','Please select a payor');
			return false;
		}
		
        setAjaxRequest( base_url + 'cashier/cashiering/event?event=get-online-payment',  {idno: idno}
		   ,function(req) {
			   if(req.error==false){
			    $('#online-validation').modal('show');
				$('#online-validation').find('[name="tx_number"]').replaceWith(req.content);
			   }else{
				msgbox('error',req.message);
			   }
			   progressBar('off');
			}
		   ,function(err){
				msgbox('error','Failed to load data');
				progressBar('off');
			}
		);
	});
	
	$('body').on('click','#set-online',function(){
	    var tmpval = $('[name="tx_number"]').val();
		var tmpopt = $('[name="tx_number"]').find('[value="'+tmpval+'"]');
		var tmpamt = $(tmpopt).attr('data-amt');
		if(tmpval!=undefined && tmpval!='-1' && tmpval!='' && tmpamt!=undefined){
            online_rid = tmpval;
            online_amt = tmpamt;
		    if(online_amt ==0){
                //return false;
            }else{
				var total    = tmpamt;
				var subtotal = 0;

				$('#records-table tbody tr').each(function(){

					var bal = $(this).attr('data-due');
					 if ( parseFloat(total) > parseFloat(bal) && parseFloat(total) > 0 ){
						total = total - bal;
						subtotal =  subtotal + parseFloat(bal);
					 }else if (parseFloat(total) <= parseFloat(bal) && parseFloat(total) > 0 ){

						$(this).attr('data-due', total);
						$(this).find('td.due').html(format_number(total));
						$(this).find('td.balance').html(format_number(total) );
						$(this).find('td.discount').html('0.00');
						subtotal =  subtotal + parseFloat(total);

						total = 0;
					 }else{
						$(this).remove();
					 }
				});

				$('#total-due').attr('data-value',subtotal);
				$('#total-due').html(format_number(subtotal));
			}
			
		    $('#online-validation').modal('hide');
		}else{
		   msgbox('error','Complete the form first!');
		}
	});
	
	$('body').on('change','[name="tx_number"]',function(){
	    var tmpval = $(this).val();
		if(tmpval!=undefined && tmpval!='-1'){
		  var tmpopt = $(this).find('[value="'+tmpval+'"]');
		  $('#online-school-term').html($(tmpopt).attr('data-ayterm'));
	      $('#online-debit-amount').html($(tmpopt).attr('data-amt'));
	      $('#online-debit-payor').html($(tmpopt).attr('data-name'));
	      $('#online-payed-id').html($(tmpopt).attr('data-std'));
		}
	});
	
	$('body').on('click','[data-menu="delete"]',function(){
	   var ornum = $('[data-orno]').attr('data-orno');
         
        confirmEvent('<i class="fa fa-warning text-danger"></i> Do you want to remove receipt :'+ornum+'?',function(data){
            if(data){
              setAjaxRequest( base_url + 'cashier/official-receipts/event?event=delete',  {ornum: ornum },
                    function(req) {
                        if (req.error == false) {
                            msgbox('success', req.message);
						    window.location.href = base_url + page_url; 
                        } else {
                            msgbox('error', req.message);
                        }
                        progressBar('off');
                    },
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
                );   
            }        
        });
	});
	
});
