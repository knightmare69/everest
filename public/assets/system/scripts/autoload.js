var AUTOLOAD =
{
	init : function()
	{
		AUTOLOAD.actions();
		AUTOLOAD.globalForm();
        AUTOLOAD.getUserByDepartment();
        AUTOLOAD.clearModal();
	},
	actions : function()
	{
		jQuery('body').on('click','.chk-header',function(){
            var table = jQuery(this).closest('table');
            var value = jQuery(this).is(':checked') ? true : false;
            var checker = jQuery(this).is(':checked') ? table.find('tbody tr td .checker span').addClass('checked') :  table.find('tbody tr td .checker span').removeClass('checked');
            table.find('tbody tr').each(function() {
                var _child = jQuery(this).find('td .chk-child');
                if (!_child.is(':disabled')) {
                    _child.prop('checked',value);
                }
            });
            FN.checkGrids();
        });

        jQuery('body').on('click','.chk-child',function(){
            FN.checkGrids();
        });

		jQuery('body').on('click','.MenuLedger',function(){
            var result = ajaxRequest(base_url+'autoload/ledger','','POST','HTML');
            jQuery('#modal_ledger .ledger-wrapper').html(result);
            jQuery('#modal_ledger').modal('show');
        });

        jQuery('body').on('click','.ledger-filter',function(){
            var param = 'warehouse='+jQuery('#FilterLedgerWarehouse option:selected').val()+'&filter='+jQuery('#LedgerFilter').val();
            var result = ajaxRequest(base_url+'autoload/ledgerFilter',param,'POST','HTML');
            jQuery('#modal_ledger .ledger-wrapper').html(result);
        });

        jQuery('body').on('click','.a-saveas',function(){
            var html, saveas = 1;
            saveas = jQuery(this).attr('data-saveas');
            html = jQuery(this).html();
            html=html+'<i class="fa fa-angle-down"></i>';
            jQuery('.btn-saveas').html(html);
            jQuery('.btn-saveas').attr('data-saveas',saveas);
        });
	},
	globalForm : function()
	{

        jQuery('body').on('click','#global-form',function(){
            var result = ajaxRequest(base_url+'autoload/globalForm','','POST','HTML');
            basicModal2(result,'');
            jQuery('#modal_basic2').addClass('autoload_form');
        });

        jQuery('body').on('click','.a-global-type',function(){
            var html, type;
            type = jQuery(this).find('text').text();

            html = jQuery(this).html();
            html = '<i class="fa fa-cog"></i> ' + type ;
            jQuery('.btn-global-type').html(html);
            jQuery('#modal_basic2 .type-wrapper').addClass('hide');
            jQuery('.'+type.toLowerCase().replace(' ','-')+'-wrapper').removeClass('hide');
            jQuery('#modal_basic2 .btn-save').addClass('btn-global-save');
            jQuery('#modal_basic2 .btn-global-save').attr('data-type',type.toLowerCase().replace(' ','-'));
            jQuery('#modal_basic2 .btn-global-save').removeClass('btn-save');
            if ( type.toLowerCase() == 'item' ) {
                jQuery('.autoload_form .modal-dialog').addClass('modal-lg');
            } else {
                jQuery('.autoload_form .modal-dialog').removeClass('modal-lg');
            }
        });

        jQuery('body').on('click','#global-refresh',function(){
            var form   = '.refresh-form';
            var result = ajaxRequest(base_url+'autoload/refreshForm');

            if ( jQuery('div').hasClass('refresh_category') )
            {   
                jQuery('.refresh_category select').html(result.category);
            }

            if ( jQuery('div').hasClass('refresh_category_type') )
            {   
                jQuery('.refresh_category_type select').html(result.category_type);
            }

            if ( jQuery('div').hasClass('refresh_unit') )
            {   
                jQuery('.refresh_unit select').html(result.unit);
            }

            if ( jQuery('div').hasClass('refresh_brand') )
            {   
                jQuery('.refresh_brand select').html(result.brand);
            }

            if ( jQuery('div').hasClass('refresh_supplier') )
            {   
                jQuery('.refresh_supplier select').html(result.supplier);
            }
        });

		jQuery('body').on('click','.btn-global-save',function(){
			var type = jQuery(this).attr('data-type');
			var source = jQuery(this).parent().parent().parent().parent();
            var method = type.replace('-','_');
            var data = jQuery(this).parent().parent().serialize()+'&method='+method;
            var form_class = '.'+type+'-wrapper';
            form_class = jQuery(form_class+' form').hasClass('form') ? form_class+' .form' : form_class+' .form-'+type.toLowerCase();
			
            if (check_form(form_class,source)){
                bootbox.confirm({
                    size: 'small',
                    message: "Are you sure you want to save this record?",
                    callback: function(yes) {
                        if (yes) {
                            var result = ajaxRequest(base_url+'autoload/globalFormSave',data);
                            globalMessage(type,source,result,result.error);
                        }
                    }
                });
            }
            
		});

        function globalMessage(type,source,result,isError) {
            var isError = isError == undefined ? false : isError;
            var caption = "<span class='badge "+(isError ? 'badge-danger' : 'badge-success')+"'>Form "+result.message+"</span>";
            switch(type.toLowerCase())
            {
                case 'item' :
                    jQuery('.autoload_form .modal-body .caption ').append(caption);
                    var timer = setTimeout(function() {
                         jQuery('.autoload_form .modal-body .caption span').remove();
                    },3000);
                    jQuery('.modal_search_item #filter').val(result.content.message[1]);
                    jQuery('.btn-type').attr('data-type','ITEM');
                    jQuery('.btn-type text').text('Stock #');
                    jQuery('.modal_search_item .btn-search').trigger('click');
                    break;
                default:
                      source.find('.caption').html(caption);
                    break;
            }
        }
	},

    getUserByDepartment: function()
    {
        jQuery('body').on('change','.userby-department',function(){
            var trigger = jQuery(this).attr('data-trigger') == undefined ? true : jQuery(this).attr('data-trigger').toLowerCase();
            if ( trigger == 'false') return;
            var value = jQuery(this).find('option:selected').val();
            if ( value != '' && value != 'select' && value != undefined){
                var users = ajaxRequest(base_url+'autoload/getUsersByDepartment','id='+value,'POST','HTML');
                jQuery('.user-select').removeAttr('option');
                jQuery('.user-select').html(users);
                if (jQuery('.user-select').hasClass('select2')) {
                    FN.multipleSelect('#RequestingParty');
                }
            }
        });
    },

    getGlobalCategoryType : function() 
    {
        jQuery('body').on('change', '.global_category_type', function() {
            var value = jQuery(this).find('option:selected').val().toLowerCase();
            if (value != '') {
                jQuery('.category_wrapper').html(ajaxRequest(base_url + 'items/getCategories','id=' + value).content);
            }
        });
    },

    clearModal : function() {
        jQuery('body').keydown(function(e) {
            if ( e.keyCode == 46 ) {
                jQuery('body').append(jQuery('.modal-backdrop').html());
                jQuery('.modal-backdrop').remove();
            }
        });
    }


};
jQuery(document).ready(AUTOLOAD.init());