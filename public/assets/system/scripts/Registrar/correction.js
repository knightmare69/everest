var Correction = {

    changeGrade : function(){
        Correction.initperiod();
        var html = $('#correction-container').html();
        showModal({ name: 'basic', title: 'Correction of Grades', content: html, class: 'modal_correction', hasModalButton: false });
    },

    saveChanges: function(subj, old, grade, gpa, reason, period ){

        setAjaxRequest( base_url + page_url + 'event', { 'event': 'save-changes', 'term': $('#term').val() 
		                                               , 'stud' : $('#student-num').html(), 'subj' : subj
													   , 'old' : old  , 'grade' : grade, 'gpa':gpa
													   , 'reason' : reason, 'period': period },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    msgbox('success', result.message);
                    Correction.getGrades();
                    $('#modal_basic').modal('hide');
                }
            }
        );
        progressBar('off');
    },

     history: function($r){

        var code = $r.find('td.code').html(), title = $r.find('td.title').html();

        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-history', 'subj': $r.attr('data-idx') },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {

                    showModal({
                        name: 'basic',
                        title: 'Correction of Grades History : ' + title + ' ['+ code +']' ,
                        content: result.html,
                        class: 'modal_history',
                        hasModalButton: false
                    });
                    $('.btn.blue.btn_modal').addClass('hide');
                    //$('#table-students-res').DataTable({ "aaSorting" : [] });
                    $('.modal_history').find('.modal-dialog').css('width','50%');
                }
            }
        );
        progressBar('off');
    },

    getStudents: function(_find){

        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-students', 'term': $('#term').val()},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {

                    showModal({
                        name: 'basic',
                        title: 'List of Students',
                        content: result.html,
                        class: 'modal_faculty_result',
                        hasModalButton: false
                    });
                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#table-students-res').DataTable({ "aaSorting" : [] });

                }
            }
        );
        progressBar('off');
    },

    getGrades : function(){

        var t = $('#term').val();
        var s = $('#student-num').html();

        var txt = $('#term').find('option:selected').text()
        var x = txt.trim().indexOf('School');

        var shs = (x > 1 ? 0 : 1);

        if(t != -1){
            var data = { event : 'grades', term :  t, 'stud': s , 'shs' : shs };
            Metronic.blockUI({target: '#grades', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Please wait while loading... '});
            var res = ajaxRequest(base_url + page_url+"event",data,'json',undefined,false);
            $('#grades').html(res.list);
            $('#history').html(res.history);
            Metronic.unblockUI('#grades');
        }
    },

    initperiod : function(){

        var $term = $('#term');

        var txt = $term.find('option:selected').text()
        var x = txt.trim().indexOf('School');

        if(x <= 0){
            $('#period').find('option').remove();
            $('#period')
                .empty()
                .append('<option value="-1"> - Select Period - </option><option value="11"> 1ST TERM</option><option value="12"> 2ND TERM</option><option value="13"> REMEDIAL</option>');
        }else{
            $('#period').find('option').remove();
            $('#period')
                .empty()
                .append('<option value="-1"> - Select Period - </option><option value="1"> 1ST QUARTER</option><option value="2"> 2ND QUARTER</option><option value="3"> 3RD QUARTER</option><option value="4"> 4TH QUARTER</option>');
        }

    },

}

$(document).ready(function(e){


    $('.select2').select2();

    $('body').on('click', '.btn-stud-select-btn', function(e){
        var stud_name = $(this).parent().prevUntil('td:first').find('p').text();
        $('#student-name')
            .html(stud_name)
            .attr('data-snum', $(this).attr('data-id'));
        $('#student-num').html($(this).attr('data-id'));
        $('.img-responsive').attr('src', base_url+'/general/getPupilPhoto?Idno='+ $(this).closest('tr').attr('data-id'));
            Correction.getGrades();
    });

    $('body').on('change', '#academic-term', function(e){
        Correction.getGrades();
    });

    $('body').on('change', '#period', function(e){
        var $r = $('#grades tbody tr.active-row');
        var v = $(this).val();

        var g = '';
        switch(v){
            case '1':
            case '11':
                g = $r.find('td.avea').html();
            break;
            case '2':
            case '12': g = $r.find('td.aveb').html();
            break;            
            case '3': g = $r.find('td.avec').html(); break;
            case '4': g = $r.find('td.aved').html(); break;
            case '13': g = $r.find('td.avec').html(); break;
        }

        $('#modal_basic').find('#grade').val(g);

    });

    $('body').on('click', '#reload', function(e){
        Correction.getGrades();
    });

    $('body').on('click', '.change-grade', function(e){

        var $r = $(this).closest('tr');

        Correction.changeGrade();
        $con = $('#modal_basic');
        $con.find('#code').val($r.attr('data-code'));
        $con.find('#title').val($r.attr('data-title'));
        $con.find('#idx').val($r.attr('data-idx'));

    });

    $('body').on('click', '.history', function(e){
        var $r = $(this).closest('tr');
        Correction.history($r);
    });

    $('body').on('click','.nav a', function(){
        var d = $(this).attr('data-menu');
        $('#grades').hide();
        $('#history').hide();
        switch(d){
            case 'grades': $('#grades').show(); break;
            case 'history': $('#history').show(); break;
            default : $('#grades').show(); break;
        }

        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');

    });


    $('body').on('click', '.btn_modal', function(e){

        $con = $('#modal_basic');

        var old = $con.find('#grade').val();
        var cor = $con.find('#correction').val();
        var gpa = $con.find('#gpa').val();
        var reason = $con.find('#reason').val();
        var period = $con.find('#period').val();

        if(cor == ''){
            alert('Please enter a valid correction grade.');
            return false;
        }

        if(reason == ''){
            alert('Please enter a valid reason.');
            return false;
        }

        var $r = $('#grades tbody tr.active-row');

        Correction.saveChanges( $r.attr('data-idx'),old, cor, gpa, reason, period );
    });

    $('body').on('click', '#student', function(e){ Correction.getStudents(''); });
});