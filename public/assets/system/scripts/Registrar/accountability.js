var MOD = {           
    
    load : function(){
      
      $('body').on('change', '#academic-term', function(e){               
            location.replace(base_url+page_url+'?t='+ $('#academic-term').val());            
      });
      
      $('body').on('change', '.track', function(e){               
            MOD.update(this,'track');                  
      });
                        
      $('body').on('click', '.btn', $.proxy(this.menu, this) );
      $('body').on('keypress','#searchkey', function(e){
            if(e.keyCode ==  13){
                MOD.searchOnEnter();
            }
      });      
    },        
    
    searchOnEnter: function(){
       
       var find = $('#searchkey').val();
       
       setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students', 'term': $('#academic-term').val(), 'find' : find }, function(r) {
            if (r.error) {
                msgbox('error', r.message);                
            }else{
                $('#event_modal .scrollbox').html(r.data);  
            }                    
        },undefined,'json',true,false);
        progressBar('off');
        
    },
    
    update : function(e,mode){
        var data;
        switch(mode){
            case 'track': data = {'id': $(e).closest('tr').attr('data-id') ,'name': 'track', 'val': $(e).val() };  break;
        }
        
        if(data){                            
            setAjaxRequest( base_url + page_url + '/event?event=set' ,data, function(r) {
                if (r.error) {
                    msgbox('error', r.message);                
                }else{
                    msgbox('success', r.message);
                }                    
            },undefined,'json',true,false);
            progressBar('off');        
        }
                        
    },
    
    add : function(){      
        this.loadStudents();                    
    },
    
    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'add': this.add(e); break;
            case 'save': this.saveThis(e); break;
            case 'remove': this.deleteThis(e); break;
        }
    },
    getSelected: function(){
        var ids = [];
        
        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {                
                ids.push($(this).attr('data-id'));                
           }                       
        });
        
        return ids;
        
    },
    
    saveThis: function(){
       
        var id =  $('#table-students-res').find('tr.active-row').attr('data-id');
		var rs =  $('#reason').val();
        setAjaxRequest( base_url + page_url + '/event?event=save&term='+ $('#academic-term').val() + '&idno='+id + '&reason='+rs  , undefined ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    showSuccess( '<i class="fa fa-check"></i> ' +  r.message);
                    $('.scroller').html(r.data);                                        
                }
                progressBar('off');
            }
        );
        
    },
    
    deleteThis : function(e){
        
        var _this = this;
        var selecteds = _this.getSelected();

        if (selecteds.length > 0 ) {

          $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove selected student/s? </h2>",
    			content : "Do you want to remove the selected student/s? Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
    			     setAjaxRequest( base_url + page_url + '/event?event=remove' , { idno : selecteds } ,
                        function(r) {
                            if (r.error) {
                                showError( '<i class="fa fa-times"></i> ' +  r.message);
                            } else {
                                showSuccess( '<i class="fa fa-check"></i> ' +  r.message);
                                location.reload();                                        
                            }
                            progressBar('off');
                        }
                    );
                     
    			}   			
    		});
        }else{
            alert('Please select student to delete');
        }
        //e.preventDefault();        
    },
    
 
    loadStudents : function(){
      var s = getParameterByName('s');
      var p = $('#period').val();
      
        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students', 'term': $('#academic-term').val() }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
                //sysfnc.showAlert('#grdSchedules','danger', r.message,true,true,true,'warning');
            }else{
                $('#event_modal .scrollbox').html(r.data);  
                //$('#table-students-res').DataTable({ "aaSorting" : [] });
                $('#event_modal').modal('show');                 
            }                    
        },undefined,'json');
        progressBar('off');  
    },
};