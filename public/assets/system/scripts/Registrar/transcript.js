var MOD = {
    init: function(){ this.load(); },
    add : function(){ this.loadStudents(); },
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    load : function(){

      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      $('body').on('click','.views', $.proxy(this.help,this));
      $('body').on('change', '#term', function(e){
        localStorage.TermID =  $('#term').val();
        location.replace(base_url+page_url+'?t='+ $('#term').val());
      });
      
      $('body').on('keypress','#searchkey', function(e){if(e.keyCode ==  13){ MOD.searchOnEnter(); }});
      $('body').on('keypress','#idno',function(e){  if(e.keyCode ==  13){ MOD.add(e); }});
      
      $('body').on('mousedown','#tblgrades tbody td', function(e){
         MOD.closemenu();
         if(e.button == 2){
              MOD.initrclick(e);
         }
      });
    },

    help: function(e){

        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');

        $('.tabpane').hide();        

        var data = $(e.target).data();
        switch (data.menu) {
            case 'transcript': $('#transcript').show(); break;
            case 'evaluation': this.initEval(); break;
            case 'setup': $('#setup').show(); break;
        }
    },

    reload : function(e){
        var m = $('.profile-usermenu').find('li.active').find('a').attr('data-menu');
        console.log(m);
        switch(m){
            case 'transcript': $('#transcript').show(); break;
            case 'evaluation': this.initEval(); break;
        }

    },

    initEval : function(){

     $('#evaluation').show();
      $('#tblgrades tbody').html('<tr><td colspan="8"><i class="fa fa-refresh fa-fw fa-spin"></i> Please wait while loading... </td></tr>')
      setAjaxRequest( base_url + page_url + '/event', {'event': 'get-grades', 'idno': getParameterByName('idno')}, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#tblgrades tbody').html(r.data);
            }
        },undefined,'json',true,false);

    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'search': this.add(e); break;
            case 'select': this.selectThis(e); break;
            case 'remove': this.deleteThis(e); break;
            case 'sched': this.schedule(e); break;
            case 'selsched': this.setSched(e); break;
            case 'register': this.saveThis(e); break;
            case 'print': this.print(e); break;
            case 'reload': this.reload(e); break;
            case 'cancel-menu': this.closemenu(); break;
            case 'credit': this.creditThisSubj(); break;
            case 'remcrd': this.creditThisSubj(false); break;
            case 'print-eval': this.printEval(e); break;

        }
    },

    setSched: function(){
       var $r = $('#tbloffered').find('tr.active-row');
       var $advised = $('#tbladvised').find('tr.active-row');

       var test = this.checkSchedConflict($r.attr('data-id'), $advised.attr('data-sched'));

       if(test.error == false){
              $advised.attr('data-sched', $r.attr('data-id'));
              $advised.attr('data-section', $r.attr('data-secId'));
              $advised.find('.section').html($r.attr('data-section'));
              $advised.find('.sched').html($r.attr('data-sched'));
              $advised.find('.room').html($r.attr('data-room'));

              if ($('#isblock').prop('checked') ){
                  var subj =  this.schedbysection($r.attr('data-secId'))
                //   console.log(subj);
                  for(i=0; i< subj.length; ++i){
                //    console.log(subj[i]['ScheduleID']);

                       $('#tbladvised tbody tr').each(function(){
                          if ( $(this).attr('data-sched') == '0' && $(this).attr('data-id') == subj[i]['SubjectID'] ){
                               $(this).attr('data-sched', subj[i]['ScheduleID'] );
                               $(this).attr('data-section', subj[i]['SectionID'] );
                               $(this).find('.section').html(subj[i]['SectionName']);
                               $(this).find('.sched').html(subj[i]['Sched1']);
                               $(this).find('.room').html(subj[i]['Room1']);
                          }
                       });

                  }
              }
       } else {
           msgbox('error', test.message);
       }


    },

    
    selectThis : function(){

       var id =  $('#table-students-res').find('tr.active-row').attr('data-id');
       if( id == undefined){
            msgbox('warning','No Selected student');
            return false;
       }
       location.replace(base_url+page_url+'?idno='+ id );
    },

    searchOnEnter: function(){

       var find = $('#searchkey').val();
      
       setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students',  'find' : find }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#event_modal .scrollbox').html(r.data);
            }
        },undefined,'json',true,false);
        progressBar('off');

    },

    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                var d = {
                    ref : $(this).attr('data-id'),
                    idno : $(this).attr('data-idno'),
                    prog : $(this).find('.level').find('option:selected').attr('data-prog'),
                    major : $(this).find('.track').find('option:selected').val(),
                    level : $(this).find('.level').val(),
                };
                ids.push(d);
           }
        });
        return ids;
    },

    saveThis: function(){

       var subj = [];
       var section = '';

       $('#tbladvised tbody tr').each(function(){
            var id = $(this).attr('data-sched');
            if(id != 0 && id != undefined){
               if(section == '') section = $(this).attr('data-section');
               subj.push(id);
            }
       });

        $.SmartMessageBox({
        	title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Register this transcript? </h2>",
        	content : "Do you want to register the advised/selected subject/s? Click [YES] to proceed...",
        	buttons : '[No][Yes]'
        }, function(ButtonPressed) {
        	if (ButtonPressed === "Yes"){
        	     var data = { term: $('#term').val(), idno : getParameterByName('idno'), section : section , option : $('#optionbk').val() ,  subj : subj };
                setAjaxRequest( base_url + page_url + '/event?event=register', data ,
                    function(r) {
                        if (r.error) {
                            showError( '<i class="fa fa-times"></i> ' +  r.message);
                        } else {
                            $('#btnprint').attr('data-reg', r.reg);
                            $.SmartMessageBox({
                        			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Successfully Registered! </h2>",
                        			content : "Do you want to print registration form? Click [YES] to proceed...",
                        			buttons : '[No][Print]'
                        		}, function(ButtonPressed) {
                        			if (ButtonPressed === "Print"){
                                        MOD.print();
                        			}
                        		});

                        }

                        progressBar('off');
                    }
                );
        	}
        });
    },

    loadStudents : function(){
        $('#searchkey').val($('#idno').val() );
        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students','find': $('#idno').val() }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
                //sysfnc.showAlert('#grdSchedules','danger', r.message,true,true,true,'warning');
            }else{
                $('#event_modal .scrollbox').html(r.data);
                $('#event_modal').modal('show');
            }
        },undefined,'json');
        progressBar('off');
    },

    deleteThis : function(e){

        var _this = this;
        var selecteds = _this.getSelected();

        if (selecteds.length > 0 ) {
          $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove selected student/s? </h2>",
    			content : "Do you want to remove the selected student/s? Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    setAjaxRequest( base_url + page_url + '/event?event=remove' , { idno : selecteds } , function(r) {
                    if(r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    }
                    progressBar('off');
                    });
    			}
    		});
        }else{
            alert('Please select student to delete');
        }
    },

   	schedule: function(e){

		var termid = $('#term').val();
        var subjid = $(e.target).closest('tr').attr('data-id');

        setAjaxRequest( base_url + page_url + '/event' ,{event:'schedule',termid:termid,progid:29,subjid:subjid}
			   ,function(r)
				{
				  if(r.error)
				  {
			        msgbox('danger',r.error);
			      }else{
					 $('#selsubj').val(subjid);
					 $('#schedulebox').html(r.content);
		             $('#sched_modal').modal('show');
					 Metronic.initUniform();
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
				});
        progressBar('off');

	},

    schedbysection : function(secname){

        var termid = $('#term').val();
        var ret;
        setAjaxRequest( base_url + page_url + '/event' ,{event:'schedule-by-section',termid:termid,progid:29,section:secname}
			   ,function(r)
				{
				  if(r.error)
				  {
			        msgbox('danger',r.error);
			      }else{
                    ret = r.list;
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
				});

        progressBar('off');

        return ret ;

    },

    print : function(){
        var id = $('#btnprint').attr('data-reg');

        if(id == 0 || id == undefined){
            alert('no registration found!');
            return false;
        }
        window.open(base_url+'students/enrollment/print?reg='+$('#btnprint').attr('data-reg')+'&t='+ Math.random() ,'_blank');
	 },
    initrclick: function(e){
        var pos = $(e.target).position();
        var cs = {top: pos.top, left: pos.left}

        $( "#mouse_menu_right" ).css(cs);
        $('#mouse_menu_right').show('50');
    },

    creditThisSubj: function(rem){

        rem = (rem == undefined ? true : rem);

        var _this = MOD;
        var subj = $('#tblgrades tbody').find('tr.active-row');
        var title = (rem == undefined || rem == true ? 'Credit this subject' : 'Remove credit from subject');
        _this.closemenu();

        if(subj.attr('data-credited') == '1' && rem  ){
            sysfnc.msg('<i class="fa fa-warning"></i> Subject was already credited!','unable to proceed...');
            return false;
        }

        $.SmartMessageBox({ title : "<h2 class='text-warning'> "+title+" : "+subj.attr('data-code')+" ? </h2>", content : "Click [YES] to proceed...", buttons : '[No]['+ (rem ? 'Credit' : 'Remove')  +']'},
        function(ButtonPressed) {
			if (ButtonPressed === "Credit" || ButtonPressed === "Remove" ){
                setAjaxRequest( base_url + page_url + '/event' ,{event:'credit', idno: getParameterByName('idno'), subj :subj.attr('data-id'), 'credit': rem }
			     ,function(r) {
				  if(r.error){
				    msgbox('danger',r.error);
			      }else{
					subj.find('td.grade').html(r.grade);
                    subj.find('td.remarks').html(r.remarks);
			      }
				}
               ,function(err){confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});},'json',true, false);
			}
		});
    },
     printEval : function(){
        window.open(base_url+'enrollment/transcript/printEval?idno='+ getParameterByName('idno') +'&t='+ Math.random() ,'_blank');
	 },

};
