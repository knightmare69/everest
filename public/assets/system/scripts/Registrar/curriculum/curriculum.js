var defaulttb;
var prgcls     = 0;
var yrterm     = 0;
var indxid     = 0;
var newcount   = 0;
var arr_prereq = [];
var arr_coreq  = [];
var arr_equiv  = [];

$('document').ready(function(){
 $('body').on('change','#program',function(){
	 var prog = $(this).val();
	 if(prog==undefined || prog==-1 || prog==''){return false; }
	 clearform();
	 setAjaxRequest(
				base_url+'registrar/curriculum/txn?event=progdata',{progid:prog},
				function(rs)
				{
				 if(rs.success)
				 {
	              console.clear();
				  $('#program').attr('data-class',rs.pdata.ProgClass);
				  $('.dvcontent').html(rs.content);
				  $('#major').replaceWith(rs.major);	 
				  $('.btncurrcontent').html(rs.curr);
				  if(rs.curr=='')
					$('.btncurrcontent').addClass('hidden');
                  else				
					$('.btncurrcontent').removeClass('hidden'); 
				  enable_yearterm(); 
				 }
                 else
				  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
				 
				 progressBar('off');	 
				},
                function(err)
                {	
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				  progressBar('off');
				});	 			
 });
 
 $('body').on('click','.trsubj,.modsubj,.eqsubj',function(){
	 $(this).closest('tbody').find('.danger').removeClass('danger');
	 $(this).addClass('danger');
 });
 
 $('body').on('click','.curriculum',function(){
	 var prog   = $('#program').val();
	 var xid    = $(this).attr('data-id');
	 var xmajor = $(this).attr('data-major');
	 var xcode  = $(this).find('.notes').attr('data-code');
	 var xdesc  = $(this).find('.notes').attr('data-desc');
	 var xnote  = $(this).find('.notes').html();
	 var rmin   = $(this).attr('data-rmin');
	 var rmax   = $(this).attr('data-rmax');
	 var smin   = $(this).attr('data-smin');
	 var smax   = $(this).attr('data-smax');
	 
	 var zero   = $(this).find('.notes').attr('data-zero');
	 var inact  = $(this).find('.notes').attr('data-inactive');
	 var lock   = $(this).find('.notes').attr('data-lock');
	 
	 if(zero==1)
	   $('.btnzerobase').addClass('btn-success').removeClass('btn-default');
     else   
	   $('.btnzerobase').addClass('btn-default').removeClass('btn-success');
	 
	 if(inact==1)
	   $('.btninactive').addClass('btn-success').removeClass('btn-default');
     else   
	   $('.btninactive').addClass('btn-default').removeClass('btn-success');
	 
	 if(lock==1)
	 {
	  $('.btnlock').addClass('hidden');	 
	  $('.btnunlock').removeClass('hidden');	 
	 }
     else 	 
	 {
	  $('.btnunlock').addClass('hidden');	 
	  $('.btnlock').removeClass('hidden');	 
	 }
	 
	 if(xid!=undefined || xid!='' && xid!='-1')
	 {
	  if(xmajor>0)
	   $('#major').val(xmajor);
	  else
	   $('#major').val('-1');
   
	  $('#code').attr('data-id',xid);	 
	  $('#code').val(xcode);	 
	  $('#desc').val(xdesc);	 
	  $('#note').val(xnote);	 
	  $('#rmin').val(rmin);	 
	  $('#rmax').val(rmax);	 	 
	  $('#smin').val(smin);	 
	  $('#smax').val(smax);	 
	  $('.btnsave').removeAttr('disabled');
	  $('.btncancel').removeAttr('disabled');
	  $('.btndelete').removeAttr('disabled');
	  $('.dvcontent').html(defaulttb);	
      
	  setAjaxRequest(
				base_url+'registrar/curriculum/txn?event=currdata',{progid:prog,currid:xid},
				function(rs)
				{
				 if(rs.success)
				 {
	              console.clear();
				  $('.currcode').html(xcode+'<b class="hidden-xs hidden-sm"> - '+xdesc+'</b>');
				  $('.dvcontent').html(rs.content);
				  enable_yearterm();
                 }
				 else
				  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
				 
				 progressBar('off');	 
				},
                function(err)
                {	
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				  progressBar('off');
				});
	 }	 
 });
 
 $('body').on('click','.btndelete',function(){
	 var cid = $('#code').attr('data-id');
	 if(cid)
	 {
	   setAjaxRequest(
				base_url+'registrar/curriculum/txn?event=delcurr',{currid:cid},
				function(rs)
				{
				 if(rs.success)
				 {	 
	              console.clear();
				  $('.btncurrcontent').find('[data-id="'+cid+'"]').remove();
				  $('.btnsave').attr('disabled',true);
				  $('.btncancel').attr('disabled',true);
				  $('.btndelete').attr('disabled',true);
				  $('.curr_data').val('');
				  $('.currcode').html('');
				  $('#code').attr('data-id','new');
				  $('.dvcontent').html(defaulttb);	
				  $('#rmin').val('15.0');
				  $('#rmax').val('AUTO');
				  $('#smin').val('3.0');
				  $('#smax').val('6.0');
				 }
				 
				 confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
				 progressBar('off');	 
				},
                function(err)
                {	
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to remove Data!',function(){return;});	
				  progressBar('off');
				}); 
	 }	 
 });
 
 $('body').on('click','.btnadd,.btnedit',function(){
   var filter  = '';	   
   var ismain  = $(this).closest('tr').is('.trmain');
   var cls     = $(this).closest('tr').attr('data-class');
       cls     = ((cls==undefined || cls=='')?$('#program').attr('data-class'):cls);
       prgcls  = cls;
   if(ismain)
   {	   
       yrterm  = $(this).closest('tr').attr('data-id');   
	   indxid  = 0;
   }
   else
   {	   
       yrterm  = $(this).closest('tr').attr('data-main'); 
	   indxid  = $(this).closest('tr').attr('data-indx');
   }
   
   load_subjects(cls,filter);
   $('.btnsubmit').attr('data-target','dvcontent');
 });
 
 
 $('body').on('dblclick','.vwsubj .danger',function(){
   $('.btnsubmit').click();
 });	 
 
 $('body').on('click','.btnsubmit',function(){
	var xcount = $('.vwsubj').find('.danger').length;
	var sdata  = $('.vwsubj').find('.danger').clone();
	var opt    = $(this).attr('data-target');
    if(opt=='dvcontent')
    {		
	 var xclone = $('[data-id="tmpsubj"]').clone();
	 if(xcount>0)
	 {
      var curr  = $('#code').attr('data-id');
	  var xid   = sdata.attr('data-id');	
	  var code  = sdata.find('.subjcode').html();	
	  var title = sdata.find('.subjtitle').html();	
	  var lecu  = sdata.find('.lecu').html();	
	  var lech  = sdata.find('.lecu').attr('data-hrs');	
	  var labu  = sdata.find('.labu').html();	
	  var labh  = sdata.find('.labu').attr('data-hrs');	
	  var crdu  = sdata.find('.crdu').html();	
	  var yrlvl = $('.trmain[data-id="'+yrterm+'"]').attr('data-yrlvl');
	  var yrstd = $('.trmain[data-id="'+yrterm+'"]').attr('data-yrstd');
	  var sort  = 0;
	  var elem;
	  var indx  = 'new';
	 
	  xclone.attr('data-main',yrterm);
	  xclone.attr('data-id',xid);
	  xclone.find('.subjid').html(xid);
	  xclone.find('.subjcode').html(code);
	  xclone.find('.subjtitle').html(title);
	  xclone.find('.slecu').html(lecu);
	  xclone.find('.slech').html(lech);
	  xclone.find('.slabu').html(labu);
	  xclone.find('.slabh').html(labh);
	  xclone.find('.scrdu').html(crdu);
	  xclone.find('.yrstand').html(yrstd);
	 
	  if(indxid==0)
	  {	 
       newcount++;
       xclone.attr('data-indx','new'+newcount);
	   $('.trfoot[data-main="'+yrterm+'"]').before(xclone);
	   sort = $('.trsubj[data-main="'+yrterm+'"]').length;
	   elem = $('[data-indx="new'+newcount+'"]');
	  }
	  else
	  {	 
       indx = indxid;
       xclone.attr('data-indx',indx);
	   $('.trsubj[data-indx="'+indxid+'"]').replaceWith(xclone);	 
	   sort = $('.trsubj').index($('[data-indx="'+indx+'"]'));
	   elem = $('[data-indx="'+indx+'"]');
	  }
	 
	  $('#modal_subject').modal('hide');
	  save_cdetail(elem,indx,curr,xid,yrterm,yrlvl,sort);
	  gen_sumfooter(yrterm);
	 }
   }
   else if(opt=='equivcontent')
   {
	var opt   = 1;
	var indx  = $('.itemsubj').attr('data-indx');
	var subj  = $('.itemsubj').attr('data-subj');
	var xsubj = $('.vwsubj').find('.danger').attr('data-id');
	process_equiv(opt,indx,subj,xsubj);
   }	   
 });
 
 $('body').on('click','[data-dismiss="modal"]',function(){
   $('.modal.hidden').first().delay(500).removeClass('hidden'); 
 });
 
 $('body').on('dblclick','.prereq,.coreq,.equiv',function(){
	var trrow  = $(this).closest('tr');
	var prog   = $('#program').val();
	var curr   = $('#code').attr('data-id');
	var indx   = trrow.attr('data-indx');
	var subj   = trrow.attr('data-id');
	var cls = trrow.attr('data-class');
	var code   = trrow.find('.subjcode').html();
	var desc   = trrow.find('.subjtitle').html();
	
	$('.itemsubj').attr('data-indx',indx);
	$('.itemsubj').attr('data-class',cls);
	$('.itemsubj').attr('data-subj',subj);
	$('.itemsubj').attr('data-code',code);
	$('.itemsubj').attr('data-desc',desc);
	$('.itemsubj').html(code+' - '+desc);
	load_precoequiv(prog,curr,indx);
 });
 
 $('body').on('click','.equivadd',function(){
	var cls    = $('.itemsubj').attr('data-class');
	var filter = '';
	load_subjects(cls,filter);
	setTimeout(function(){    
	  $('#modal_prereq').addClass('hidden');
	  $('.btnsubmit').attr('data-target','equivcontent');
	},1000);  
 });
 
 $('body').on('click','.equivdel',function(){
	var opt   = 2;
	var indx  = $('.itemsubj').attr('data-indx');
	var subj  = $('.itemsubj').attr('data-subj');
	var xsubj = $('.equivcontent').find('.danger').attr('data-id');
	process_equiv(opt,indx,subj,xsubj);
 });
 
 $('body').on('click','.btnremove',function(){
	var xid = $(this).closest('tr').attr('data-indx');
	delete_cdetail(xid);
 });
 
 $('body').on('click','.btnzerobase,.btninactive',function(){
   var currid = $('#code').attr('data-id');
   var istick = $(this).is('.btn-success');
   if(currid!=undefined && currid!='' && currid!='new')
   {	 
	if(istick)
	{	
	  $(this).removeClass('btn-success');
	  $(this).addClass('btn-default');
    }
	else
	{
	  $(this).addClass('btn-success');
	  $(this).removeClass('btn-default');
    }
	
	if($(this).is('.btninactive') && (currid!=undefined && currid!='' && currid!='new'))
	{
	 $('.btnsave').click();	
	}
	
   }	
 });
 $('body').on('click','.btnlock,.btnunlock',function(){
	var currid = $('#code').attr('data-id');
	var islock = $(this).is('.btnlock');
	
	if(currid==undefined || currid=='' || currid=='new')
	  return false;	
	
	if(islock)
	{	
	  $(this).addClass('hidden');
	  $('.btnunlock').removeClass('hidden');
    }
	else
	{
	  $(this).addClass('hidden');
	  $('.btnlock').removeClass('hidden');
    }
 });
 
 $('body').on('click','.btnnew',function(){
   if($('#program').val()=='-1' || $('#program').val()==''){return false;}
   clearform();
   $('.btnsave').removeAttr('disabled');
   $('.btncancel').removeAttr('disabled');
   $('.btndelete').attr('disabled',true);
   enable_yearterm();
 });
 
 $('body').on('click','.btncancel',function(){
	clearform();
	$('.btnsave').attr('disabled',true);
	$('.btncancel').attr('disabled',true);
	$('.btndelete').attr('disabled',true);
 });
 
 $('body').on('click','.btnsave',function(){
	var campus = $('#campus').val();
	var prog   = $('#program').val();
	var major  = (($('#major').val()>0)?$('#major').val():0);
	var cid    = $('#code').attr('data-id');
	var code   = $('#code').val();
	var desc   = $('#desc').val();
	var note   = $('#note').val();
	var zero   = $('.btnzerobase').is('.btn-success');
	var lock   = $('.btnlock').is('.hidden');
	var rmin   = $('#rmin').val();
	var rmax   = ($('#rmax').val()=='AUTO'? 26 : $('#rmax').val());
	var smin   = $('#smin').val();
	var smax   = $('#smax').val();
	var inact  = $('.btninactive').is('.btn-success');
	setAjaxRequest(
				base_url+'registrar/curriculum/txn?event=savecurr'
			   ,{cid:cid
			    ,campus:campus
				,progid:prog
				,majorid:major
				,code:code
				,desc:desc
				,notes:note
				,zero:zero
				,lock:lock
				,rmin:rmin
				,rmax:rmax
				,smin:smin
				,smax:smax
				,inactive:inact}
			   ,function(rs)
				{
				 if(rs.success)	
				 {	 
	              console.clear();
				  $('#code').attr('data-id',rs.cid);
				  $('.currcode').html(code+'<b class="hidden-xs hidden-sm"> - '+desc+'</b>');
				  $('.dvcontent').html(rs.content);
				  var listitem = '<li class="curriculum" data-id="'+rs.cid+'" data-major="'+major+'" data-rmin="'+rmin+'" data-rmax="'+rmax+'" data-smin="'+smin+'" data-smax="'+smax+'">'+
				                 '<a href="javascript:void(0);">'+code+' - '+desc+'</a><p class="notes hidden" data-code="'+code+'" data-desc="'+desc+'" data-zero="'+((zero)?1:0)+'" data-inactive="'+((inact)?1:0)+'" data-lock="'+((lock)?1:0)+'">'+note+'</p></li>';
				  if(cid=='new')
				   $('.btncurrcontent').append(listitem);
				  else
				   $('.btncurrcontent').find('[data-id="'+rs.cid+'"]').replaceWith(listitem);  
                  
				  enable_yearterm();
			     }
				 else
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save data!',function(){return;});	
					 
				 progressBar('off');	 
				},
                function(err)
                {	
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save data!',function(){return;});	
				  progressBar('off');
				});
 });
 
 $('body').on('click','.minmax',function(){
	var tid   = $(this).closest('tr').attr('data-id');
	var ismin = $(this).is('.inmin');
	if(ismin)
	{	
     $(this).removeClass('inmin');
     $(this).find('.fa').removeClass('fa-plus');		
     $(this).find('.fa').addClass('fa-minus');		
     $('[data-main="'+tid+'"]').removeClass('hidden');	
     $('[data-main="'+tid+'"]').removeClass('danger');
	 $(this).closest('tr').find('.btn').removeAttr('disabled');	 		
	}
    else
	{	
     $(this).addClass('inmin');
     $(this).find('.fa').addClass('fa-plus');		
     $(this).find('.fa').removeClass('fa-minus');		
     $('[data-main="'+tid+'"]').addClass('hidden');	
     $('[data-main="'+tid+'"]').removeClass('danger');
     $(this).closest('tr').find('.btn').attr('disabled',true);	 
	}		
 });
 
 $('body').on('keydown','.numberonly',function(e){
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		 // Allow: Ctrl+A, Command+A
		(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
		 // Allow: home, end, left, right, down, up
		(e.keyCode >= 35 && e.keyCode <= 40)) {
			 // let it happen, don't do anything
			 return;
	}
	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
 });
 
 $('body').on('click','.yrterm',function(){
	 var xid = $(this).attr('data-id');
	 if(xid>0 && xid!=undefined)
	 {
	  $('.dvcontent').find('[data-id="'+xid+'"]').removeClass('hidden');	 
	  $('[data-main="'+xid+'"]').removeClass('hidden');	 
	  $(this).addClass('hidden');	 
	 }	 
 });
 
 $('body').on('click','.btnremyt',function(){
	 var xid = $(this).closest('tr').attr('data-id');
	 if(xid>0 && xid!=undefined)
	 {
	  $('.dvcontent').find('[data-id="'+xid+'"]').addClass('hidden');	 
	  $('.dvcontent').find('.trsubj[data-main="'+xid+'"]').remove();	 
	  $('[data-main="'+xid+'"]').addClass('hidden');	 
	  $('.yrtermcontent').find('[data-id="'+xid+'"]').removeClass('hidden');	 
	 }	 
 }); 
 
 $('body').on('keypress','#txtfilter',function(e){
	if(e.keyCode==13)
	 $('.btnsearch').click();	 
 });
 
 $('body').on('click','.btnsearch',function(){
 var cls    = progcls;
 var filter = $('#txtfilter').val();  
 $('.btnsubmit').attr('disabled',true);
 setAjaxRequest(
	base_url+'registrar/curriculum/txn?event=getsubj',{progcls:cls,filter:filter},
	function(rs)
	{
	 if(rs.success)
	 {	 
	  $('.vwsubj').html(rs.content);
	  $('.vwsubj').find('tr').each(function(indx){
		var subjid  = $(this).attr('data-id');
		if(subjid!=undefined)
		{
		 var isexist = $('.dvcontent').find('.trsubj[data-id="'+subjid+'"]').length;	
		 var already = $('.equivcontent').find('.eqsubj[data-id="'+subjid+'"]').length;	
		 if(isexist>0 || already>0){$(this).addClass('hidden'); }		 
		}						
	  });
	  $('.btnsubmit').removeAttr('disabled');
	 }
	 else
	  $('.vwsubj').html('<i class="fa fa-warning text-danger"></i> '+rs.message);
	 
	 progressBar('off');	 
	},
	function(err)
	{	
	  $('.vwsubj').html('<i class="fa fa-warning text-danger"></i> Failed to load data!');	
	  progressBar('off');
	});  
 });
 
 $('body').on('click','.checker',function(){
	var istick = $(this).find('span').is('.checked'); 
	if(istick)
	{
	 $(this).find('span').removeClass('checked');	
	 $(this).find('[type="checkbox"]').removeAttr('checked');	
	}	
	else
	{
	 $(this).find('span').addClass('checked');	
	 $(this).find('[type="checkbox"]').prop('checked',true);	
	}	
	
	gen_sumreq();
 });
 
 $('body').on('click','.btnreqsave',function(){
	var indx   = $('.itemsubj').attr('data-indx'); 
	var csubj  = $('.itemsubj').attr('data-subj'); 
	var prereq = arr_prereq;
	var coreq  = arr_coreq;
	var equiv  = arr_equiv;
	
	setAjaxRequest(
		base_url+'registrar/curriculum/txn?event=saveprereq'
	   ,{indx:indx,subj:csubj,prereq:prereq,coreq:coreq,equiv:equiv}
	   ,function(rs)
		{
		 if(rs.success)
		 {	 
		  var indx      = $('.itemsubj').attr('data-indx');
		  var txtprereq = $('.sumpre').html();
		  var txtcoreq  = $('.sumco').html();
		  var txtequiv  = $('.sumequi').html();
		  
		  $('[data-indx="'+indx+'"]').find('.prereq').html(txtprereq);
		  $('[data-indx="'+indx+'"]').find('.coreq').html(txtcoreq);
		  $('[data-indx="'+indx+'"]').find('.equiv').html(txtequiv);
		  $('#modal_prereq').modal('hide');
		  //console.clear();
		 }
		 else
		  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});	
		 
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		 confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
		 progressBar('off');
		});     
	
 });
 
 $('body').on('click','.btncurrlvl',function(){
	 var curr = $('#code').attr('data-id');
	 var islock = $('.btnlock').is('.hidden'); 
	 
	 if(islock)
	  $('.btncursave').addClass('hidden'); 
     else	
	  $('.btncursave').removeClass('hidden'); 
	 
	 if(curr!=undefined && curr!='' && curr!='new')
	 {
	   setAjaxRequest(
		base_url+'registrar/curriculum/txn?event=currlvl'
	   ,{opt:'get',curr:curr}
	   ,function(rs)
		{
		 if(rs.success)
		 {	 
		  console.clear();
		  $('.vwlvl').html(rs.content);
		 }
		 else
		  $('.vwlvl').html('<i class="fa fa-warning text-danger"></i> '+rs.message);
		 $('.btncursave').attr('disabled',true);
		 $('#modal_currlvl').modal('show');	
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		  $('.vwlvl').html('<i class="fa fa-warning text-danger"></i> Failed to load data');
		  $('.btncursave').attr('disabled',true);
		  $('#modal_currlvl').modal('show');	
		  progressBar('off');
		});     		 
	 } 
 });
 
 $('body').on('click','.btnreset',function(){
	 var curr = $('#code').attr('data-id');
	 if(curr!=undefined && curr!='' && curr!='new')
	 {
	   setAjaxRequest(
		base_url+'registrar/curriculum/txn?event=currlvl'
	   ,{opt:'reset',curr:curr}
	   ,function(rs)
		{
		 if(rs.success)
		 {	 
		  console.clear();
		  $('.vwlvl').html(rs.content);
		 }
		 else
		  $('.vwlvl').html('<i class="fa fa-warning text-danger"></i> '+rs.message);
		 
		 $('.btncursave').attr('disabled',true);
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		  $('.vwlvl').html('<i class="fa fa-warning text-danger"></i> Failed to execute');
		  $('.btncursave').attr('disabled',true);
		  progressBar('off');
		});     		 
	 } 
 });
 
 $('body').on('change','.txtmin,.txtmax',function(){
	 $('.btncursave').removeAttr('disabled');
 });
 
 $('body').on('click','.btncursave',function(){
	 var curr   = $('#code').attr('data-id');
	 var islock = $('.btnlock').is('.hidden'); 
	 var yrlvl  = {}; 
	 if(curr!=undefined && curr!='' && curr!='new' && islock==false)
	 {
	   $('.vwlvl').find('.currlvl').each(function(indx){
		 var yrid = $(this).attr('data-id');
		 var min  = $(this).find('.txtmin').val();
		 var max  = $(this).find('.txtmax').val();
         
		 if(yrid!=undefined && yrid!='')
		 {
		   yrlvl[yrid] = {yrlvl:yrid,min:min,max:max};
		 }    
	   }); 
	   
	   if(yrlvl.length<=0){return false;}
	   
	   setAjaxRequest(
		base_url+'registrar/curriculum/txn?event=savecurrlvl'
	   ,{opt:'manual',curr:curr,yrlvl:yrlvl}
	   ,function(rs)
		{
		 if(rs.success)
		 { 
	      console.clear();
		  $('.btncursave').attr('disabled',true);
		 }
		 else
		  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});	
		 
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
		  progressBar('off');
		});     		 
	 }  
 });
 
 defaulttb = $('.dvcontent').find('table').clone();
 enable_yearterm();
});

function managebtn(main)
{
  $('.btnedit').attr('disabled',true);
  $('.btnremove').attr('disabled',true);
  
  if(main=='' || main==undefined)
	return false;
  else
  {
	$('[data-id="'+main+'"]').find('.btnedit').removeAttr('disabled');  
	$('[data-id="'+main+'"]').find('.btnremove').removeAttr('disabled');  
  }	  
}

function enable_yearterm()
{
  var xcontent  = '';
  var xcode     = $('#code').val();
  $('.trmain').each(function(indx){
	 var ishidden = $(this).is('.hidden');
	 var trid     = $(this).attr('data-id');
	 var lbl      = $(this).find('.yrterm-main').find('b').html();
	 if(ishidden)
	  xcontent  = xcontent+'<li class="yrterm" data-id="'+trid+'" data-yrterm="'+lbl+'"><a href="javascript:void(0);">'+lbl+'</a></li>';	 
	 else
      xcontent  = xcontent+'<li class="yrterm hidden" data-id="'+trid+'" data-yrterm="'+lbl+'"><a href="javascript:void(0);">'+lbl+'</a></li>';		 
  });
  
  if(xcontent!='')
   $('.yrtermcontent').html(xcontent);
  else
   $('.yrtermcontent').html('<li class="yrterm" data-id="-1" data-yrterm="Nothing to display"><a href="javascript:void(0);">Nothing to display</a></li>');	  

  if($('.btnadd').length>0 && xcontent!='' && xcode!='')
  {	  
   $('.btnaddterm').removeClass('hidden');
   $('.dvcontent').find('tbody').sortable({
					items: '.trsubj',
					axis:  'y',
					stop:  function (event, ui)
					       {
							var isclick    = ui.item.is('.danger');        
					        var origin     = ui.item.attr('data-main');
					        var place      = ui.item.next('tr').attr('data-main');
							
							if(!isclick)
							{	
							 alert('Click or Highlight first a row before moving.');
							 $(this).sortable('cancel');
							}
							else
							{	
							 if(origin!=place)
					          $(this).sortable('cancel');
					         else
							  reorder(origin);
						    }
					       }    
   });
  }
  else
  {	  
   progressBar('off');
   $('.btnaddterm').addClass('hidden');
   $('.dvcontent').find('tbody').sortable({items:'.wala'});
  } 
}	

function load_subjects(cls,filter,modal)
{
 progcls = cls;	
 $('#txtfilter').val(filter);
 $('.btnsubmit').attr('disabled',true);
 setAjaxRequest(
				base_url+'registrar/curriculum/txn?event=getsubj',{progcls:cls,filter:filter},
				function(rs)
				{
				 if(rs.success)
				 {	 
	              console.clear();
				  $('.vwsubj').html(rs.content);
				  $('.btnsubmit').removeAttr('disabled');
				  $('.vwsubj').find('tr').each(function(indx){
					var subjid  = $(this).attr('data-id');
                    if(subjid!=undefined)
                    {
					 var isexist = $('.dvcontent').find('.trsubj[data-id="'+subjid+'"]').length;	
					 var already = $('.equivcontent').find('.eqsubj[data-id="'+subjid+'"]').length;	
		             if(isexist>0 || already>0){$(this).addClass('hidden'); }		 
					}						
				  });
				 }
				 else
				  $('.vwsubj').html('<i class="fa fa-warning text-danger"></i> '+rs.message);
                 
				 $('#modal_subject').modal('show');	
				 progressBar('off');	 
				},
                function(err)
                {	
				  $('.vwsubj').html('<i class="fa fa-warning text-danger"></i> Failed to load data!');	
				  progressBar('off');
                  $('#modal_subject').modal('show');	
				});     	
}

function save_cdetail(elem,indx,curr,subj,yrterm,yrlvl,sort)
{
 if(elem==undefined){return false; }
 setAjaxRequest(
			base_url+'registrar/curriculum/txn?event=savecdetail'
		   ,{xid:indx,curr:curr,subjid:subj,yrterm:yrterm,yrlvl:yrlvl,sort:sort}
		   ,function(rs)
			{
			 if(rs.success)
			 {	 
	          console.clear();
			  elem.attr('data-indx',rs.indxid);
			 }
			 else
			  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
			 
			 progressBar('off');	 
			},
			function(err)
			{	
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
			  progressBar('off');
			}
	       ,'JSON',true);	 	
}

function delete_cdetail(indx)
{
  if(indx==undefined){return false; }
  setAjaxRequest(
			base_url+'registrar/curriculum/txn?event=delcdetail'
		   ,{xid:indx}
		   ,function(rs)
			{
			 if(rs.success)
		     {		 
		      var yrterm = $('[data-indx="'+indx+'"]').attr('data-main');
	          console.clear();
			  $('[data-indx="'+indx+'"]').remove();
			  gen_sumfooter(yrterm);
			 }
			 else
			  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
			 
			 progressBar('off');	 
			},
			function(err)
			{	
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to delete Data!',function(){return;});	
			  progressBar('off');
			});	 		
}

function reorder(yrterm)
{
 if(yrterm==undefined){return false; }
 var arr = {};
 $('.trsubj[data-main="'+yrterm+'"]').each(function(indx){
  var xid  = $(this).attr('data-indx');
  arr[xid] = (indx+1);  
 });
 
 setAjaxRequest(
		base_url+'registrar/curriculum/txn?event=reorder'
	   ,{xid:arr}
	   ,function(rs)
		{
		 if(!rs.success)
		  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
		 else
		  console.clear();	 
	  
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to execute!',function(){return;});	
		  progressBar('off');
		}
	   ,'JSON',true);	 	
}

function gen_sumfooter(yrterm)
{
 var count = 0;
 var lecu  = 0;
 var labu  = 0;
 var crdu  = 0;
 $('.trsubj[data-main="'+yrterm+'"]').each(function(indx){
   count++;
   lecu = lecu + parseFloat($(this).find('.slecu').html());   
   labu = labu + parseFloat($(this).find('.slabu').html());   
   crdu = crdu + parseFloat($(this).find('.scrdu').html());   
 });
 
 $('.trfoot[data-main="'+yrterm+'"]').find('.count').html(count);
 $('.trfoot[data-main="'+yrterm+'"]').find('.lecu').html(lecu.toFixed(1));
 $('.trfoot[data-main="'+yrterm+'"]').find('.labu').html(labu.toFixed(1));
 $('.trfoot[data-main="'+yrterm+'"]').find('.crdu').html(crdu.toFixed(1));
 
}

function clearform()
{	
 $('.btnsave').attr('disabled',true);
 $('.btncancel').attr('disabled',true);
 $('.btndelete').attr('disabled',true);
 
 $('.btnzerobase').addClass('btn-default').removeClass('btn-success');
 $('.btninactive').addClass('btn-default').removeClass('btn-success');
 $('.btnlock').removeClass('hidden');
 $('.btnunlock').addClass('hidden');
 $('.curr_data').val('');
 $('#code').attr('data-id','new');
 $('.dvcontent').html(defaulttb);	
 $('#rmin').val('15.0');
 $('#rmax').val('AUTO');
 $('#smin').val('3.0');
 $('#smax').val('6.0');	
}

function load_precoequiv(prog,curr,indx)
{
  $('#modal_prereq .nav-tabs').find('li').removeClass('active');
  $('#modal_prereq .tab-pane').removeClass('active');	
  setAjaxRequest(
		base_url+'registrar/curriculum/txn?event=pre_co_equiv'
	   ,{progid:prog,currid:curr,indx:indx}
	   ,function(rs)
		{
		 if(rs.success)
		 {
		  $('.reqcontent').html(rs.content);
		  $('.equivcontent').html(rs.equiv);
		 }
		 else
		 {	 
		  $('.reqcontent').html('<i class="fa fa-warning"></i> Nothing to display.');
		  $('.equivcontent').html('<i class="fa fa-warning"></i> Nothing to display.');
		  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});
		 }
		 
		 $('#modal_prereq .nav-tabs').find('a').first().click();
		 $('#modal_prereq').modal('show');
		 gen_sumreq();
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		});
}

function process_equiv(opt,indx,subj,xsubj)
{
 if(opt==undefined || indx==undefined || subj==undefined || xsubj==undefined){return false;}
 if(opt==1)
 {	  
   var xclone = $('.vwsubj').find('.danger').clone();
	   xclone.removeClass('modsubj');
	   xclone.removeClass('danger');
	   xclone.addClass('eqsubj');
   $('.equivcontent').find('tbody').append(xclone);
   $('#modal_prereq').removeClass('hidden');
   $('#modal_subject').find('.vwsubj').html('<i class="fa fa-warning"></i> Nothing to display.');
   $('#modal_subject').modal('hide');
 }
 else if(opt==2)
 {
   $('.equivcontent').find('.danger').remove();	  
 }
 
 gen_sumreq();
}

function gen_sumreq()
{
 var prereq     = '';
 var coreq      = '';
 var equiv      = '';
     arr_prereq = [];
     arr_coreq  = [];
     arr_equiv  = [];	
 
 $('.chkprereq').each(function(indx){
	var isfont = $(this).is('.fa');
	var istick = ((isfont) ? $(this).is('.fa-check-square-o') : $(this).find('span').is('.checked'));
	if(istick)
	{
	 var subjid = $(this).closest('tr').attr('data-id'); 	
	 var scode  = $(this).closest('tr').find('.subjcode').html(); 	
	     prereq = ((prereq=='')?'':(prereq+','))+scode;
		 arr_prereq.push(subjid);
	}	 
 }); 
 
 $('.chkcoreq').each(function(indx){
	var isfont = $(this).is('.fa');
	var istick = ((isfont) ? $(this).is('.fa-check-square-o') : $(this).find('span').is('.checked'));
	if(istick)
	{
	 var subjid = $(this).closest('tr').attr('data-id'); 	
	 var scode  = $(this).closest('tr').find('.subjcode').html(); 	
	     coreq  = ((coreq=='')?'':(coreq+','))+scode;
		 arr_coreq.push(subjid);
	}	
 }); 
 
 $('.equivcontent').find('.eqsubj').each(function(indx){
	var subjid = $(this).attr('data-id'); 	
	var scode  = $(this).find('.subjcode').html(); 	
	    equiv  = ((equiv=='')?'':(equiv+','))+scode;
		arr_equiv.push(subjid);
		 
 }); 
 
 $('.sumpre').html(prereq);
 $('.sumco').html(coreq);
 $('.sumequi').html(equiv);
}