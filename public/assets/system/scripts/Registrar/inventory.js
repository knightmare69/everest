var INV = {
    init: function(){
       this.load();       
    },
    
    load : function(){

      $('body').on('change', '#academic-term', function(e){
            location.replace(base_url+page_url+'?t='+ $('#academic-term').val());
      });
      $('body').on('change', '#period', function(e){ INV.loadStudents(); });
      $('body').on('click', '.btn', $.proxy(this.menu, this) );

    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'unpost': this.confirmUnpost(e); break;
        }
    },
    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {

                var param = {
                    idno :  $(this).attr('data-id'),
                    type : $(this).attr('data-type'),
                };

                ids.push(param);
           }
        });

        return ids;

    },

    unpostThis: function(){

        var ids = INV.getSelected();

        var data = {
            event : 'unpost',
            term : $('#term').val(),
            sched : $('#sched').val(),
            period : $('#period').val(),
            stud : ids,
        };

        setAjaxRequest(
            base_url + page_url + '/event', data,
            function(r) {
                if (r.error) {
                    msgbox('error','Sorry, unable to post grades');
                } else {
                    msgbox('success',r.message);
                    //ME.initAlerts('.portlet-body','success',r.message,true,true,true,'check',10)
                    location.reload();
                }
            }
        );
        progressBar('off');
    },
    confirmUnpost : function(e){

        var _this = this;
        var selecteds = _this.getSelected();

        if (selecteds.length > 0 ) {

          $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Unpost selected student/s? </h2>",
    			content : "Posting student record will activate the class record lock features.. <br/> You are no longer able to create, edit and delete an event. Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") _this.unpostThis();
    		});
        }else{
            alert('Please select student to post');
        }
        //e.preventDefault();
    },

    loadSections: function(){
        var t = getParameterByName('t');
        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-sections', 'term': t}, function(r) {
            if (r.error) {
                msgbox('error', r.message);
                sysfnc.showAlert('#grdSections','danger', r.message,true,true,true,'warning');
            }else{
                $('.scroller').html(r.data);
            }
        });
        progressBar('off');
    },
    loadStudents : function(){
      var s = getParameterByName('s');
      var p = $('#period').val();

        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-schedules', 'sched': s , 'period' : p }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
                sysfnc.showAlert('#grdSchedules','danger', r.message,true,true,true,'warning');
            }else{
                $('#grdSchedules').html(r.data);
            }
        });
        progressBar('off');
    },
};

$(document).ready(function(e){ INV.init(); });