var isEdit = false, subjectKey = '';
var Me = function() {
	var handleSaving = function() {
		
		jQuery('#BtnCreate').click(function() {
			var self = $(this);

			var res = ajaxRequest(base_url+page_url+'event','event=showModal');

			if (res.error == true) {
				msgbox('error',res.message ? res.message : 'Could not load request');
				return;
			}
			
			showModal({
				name: 'large',
		        title : 'Create New Subject',
		        content: res.content,
		        class: 'modal_subject',
		        button: { caption: 'Save', class: 'btn_save' }
		    });

			isEdit = false;
		    $(':checkbox').uniform();
		});

		//save selector
		jQuery('body').on('click','.btn_save', function() {

			if (isLock()) { return; }
			if (!isValidForm('.form_crud')) {
				return msgbox('error','Please complete all required fields');
			}

			//create confirmation
			confirmEvent("Are you sure you want to save this?", function(yes) {
				if (!yes) {
					return;
				}

				onProcess('.btn_save','','Saving...');

				setAjaxRequest(
					base_url+page_url+'event',
					'event=save&isEdit='+isEdit+'&subjectKey='+subjectKey+'&'+jQuery('.form_crud').serialize(),
					function(result) {
						if (result.error == false) {
							clear();
							reset();
							closeModal('.modal_subject');
							msgbox('success',result.message);
						} else {
							msgbox('error',result.message ? result.message : 'Could not save.');
						}
						progressBar('off');
						onProcess('.btn_save','','done',false);
					},
					function(error) {
						onProcess('.btn_save','','done',false);
						progressBar('off');
					}
				);
			});
		});
	}

	var handleDelete = function() {
		jQuery('#BtnDelete').click(function() {
			var self = $(this);

			if (jQuery('.table tbody tr.row-selected').length <= 0 || subjectKey == '') {
				return msgbox('error','No selected subject');
			}

			confirmEvent("Are you sure you want to delete this?", function(yes) {
				if (!yes) return;
				onProcess('#BtnDelete','','Deleting...');
				setAjaxRequest(
					base_url+page_url+'event',
					'event=delete&subjectKey='+subjectKey,
					function(result) {
						if (result.error == false) {
							clear();
							reset();
							msgbox('success',result.message);
						} else {
							msgbox('error',result.message ? result.message : 'Could not delete.');
						}
						progressBar('off');
						onProcess('#BtnDelete','','done',false);
					},
					function(error) {
						progressBar('off');
						onProcess('#BtnDelete','','done',false);
					},'json',true,false
				);
			});

		});
	}

	//handle filter
	var handleFilter = function() {

		jQuery('body').on('click','.BtnSearch', function() {

			var filterClass =  jQuery('#filterClass').val();
			var filterCourse =  jQuery('#filterCourse').val();

			var data = 'filterClass='+filterClass+'&filterCourse='+filterCourse;

			filter('.BtnSearch',data);
		});

		jQuery('body').on('click','.BtnMoreSearch', function() {
			filter('.BtnMoreSearch',jQuery('#FormMoreFilter').serialize());
		});

		jQuery('body').on('click','.btnFilterSettings', function() {
			var self = $(this);

			var res = ajaxRequest(base_url+page_url+'event','event=showFilter');

			if (res.error == true) {
				msgbox('error',res.message ? res.message : 'Could not load request');
				return;
			}

			//show modal form
			showModal({
				name: 'basic',
		        title : 'Filter',
		        content: res.content,
		        class: 'modal_filter',
		        button: {
		        	caption: 'Search',
		        	class: 'BtnMoreSearch'
		        }
		    });
		});

		function filter(el,data) {
			onProcess(el,'','Searching..');
			setAjaxRequest(
				base_url+page_url+'event',
				'event=search&'+data,
				function(result) {
					if(result.error == false) {
						jQuery('#TableWrapper').html(result.content);
						$(':checkbox').uniform();
					} else {
						msgbox('error',result.message ? result.message : 'Could not load request');
					}
					onProcess(el,'','done..',false);
					progressBar('off');
				},
				function() {
					onProcess(el,'','Done',false);
					progressBar('off');
				},'json',true,false
			);
		}

	}

	var handleSelection = function() {
		//handle row selection
		jQuery('body').on('dblclick','.rowSel', function() {

			var self = $(this);

			var res = ajaxRequest(base_url+page_url+'event','event=showModal&key='+self.attr('data-id'));

			if (res.error == true) {
				msgbox('error',res.message ? res.message : 'Could not load request');
				return;
			}

			//show modal form
			showModal({
				name: 'large',
		        title : 'Edit Subject',
		        content: res.content,
		        class: 'modal_subject',
		        button: {
		        	caption: 'Update',
		        	class: 'btn_save'
		        }
		    });

		    isEdit = true;
		    subjectKey = self.attr('data-id');
		    $(':checkbox').uniform();
		});

		//handle row selection
		jQuery('body').on('click','.rowSel', function() {
			var self = $(this);

			if (self.hasClass('row-selected')) {
				self.removeClass('row-selected');
			} else {
				jQuery('.rowSel').removeClass('row-selected');
				self.addClass('row-selected');
				subjectKey = self.attr('data-id');
			}
		});
	}

	function isLock() {
		return jQuery('#IsLock').val() == '1' ? true : false;
	}

	function clear() {
		subjectKey = '';
		isEdit = false;
		clear_value('.form_crud');
	}

	function reset() {
		jQuery('.BtnSearch').trigger('click');
	}


	return {
		//initialize fx
		init: function() {

			handleSelection();

			handleDelete();

			handleSaving();

			handleFilter();
		}
	}
}();