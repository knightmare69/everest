var SR = {
    request_id: 0,
    current_row_childs: '',
    student_no: 0,
    get_students: function() {
        var data = $("#search-form").serialize();

        setAjaxRequest(
            base_url + page_url + 'event', data + '&event=get-students&type=1',
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    var dt = $('#request-table');

                    dt.DataTable().destroy();
                    dt.find('tbody').html(result.rows);
                    dt.DataTable();

                    $('#req-type-header').text($('#type option:selected').text());

                }
            }
        );
        progressBar('off');
    },

    issue_report_card: function() {
        var rows = this.current_row_childs,
            search = $('#search-form').serialize(),
            stud_num = this.student_no;

        setAjaxRequest(
            base_url + page_url + 'event', 'event=issue-report-card&' + search + '&request=' + this.request_id + '&snum=' + this.student_no,
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    var term_id = $('#academic-year').val();

                    rows.eq(5).text('Done');
                    rows.eq(6).text(result.date);
                    rows.eq(7).text(result.name);
                    $('#btn-issue').attr('disabled', true);

                    window.open(base_url + 'registrar/report-card/print?report-type=18&academic-term=' + term_id + '&year-level='+ result.yl + '&section=' + result.sec + '&snum=' + stud_num + '&programs=' + result.prog, 'Report Preview');
                }
            }
        );8
        progressBar('off');
    },

    student_details: function(img_src, selected_row) {
        var row_children = selected_row.children(),
            btn_issue = $('#btn-issue');
        this.current_row_childs = row_children;
        this.student_no = row_children.eq(1).text();

        $('#student-photo').attr('src', img_src);

        $('.profile-usertitle-name').text(row_children.eq(2).text());
        $('.profile-usertitle-job').text(row_children.eq(1).text());

        if (row_children.eq(6).text().trim() == '') {
            btn_issue.attr('disabled', false);
        } else {
            btn_issue.attr('disabled', true);
        }
    }
};

$(document).ready(function() {
    $('#request-table').DataTable();

    $('.select2').select2({
        dropdownAutoWidth: true,
        width: 'auto'
    }).on('change', function(e) {
        SR.get_students();
    });

    $('body').on('click', '.student-select', function(e) {
        e.preventDefault();
        var img = $(this).attr('data-img-src'),
            row = $(this).closest('tr');

        SR.request_id = $(this).attr('data-id');
        SR.student_details(img, row);
    });

    $('body').on('click', '#btn-issue', function(e) {
        bootbox.confirm({
            size: 'small',
            message: "Issue report card to this student?",
            callback: function(yes) {
                if (yes) {
                    $(this).attr('disabled', true);
                    SR.issue_report_card();
                }
            }
        });

    });

});
