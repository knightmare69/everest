var Config = {
    loadConfig: function(term){
        setAjaxRequest( base_url + page_url + 'event', {'event': 'get-config', 'term': term}, function(r) {
		    var sterm  = $('#academic-term').find('[value="'+term+'"]').attr('data-term');
			var labela = 'First Quarter';
			var hidea = false;
			var labelb = 'Second Quarter';
			var hideb = false;
			var labelc = 'Third Quarter';
			var hidec = false;
			var labeld = 'Fourth Quarter';
			var hided = false;
			
			if(sterm!='School Year'){
			  labela = 'Prelim';
			  labelb = 'Midterm';
			  labelc = 'Final';
			  hided = true;
			}
			
			$('.lbfirst').text(labela);
            $('.lbsecond').text(labelb);		
			$('.lbthird').text(labelc);	
			if(hidec){
			  $('#third').closest('label').addClass('hidden');
			}else{
			  $('#third').closest('label').removeClass('hidden');
			}	
			$('.lbfourth').text(labeld);
			if(hided){
			  $('#fourth').closest('label').addClass('hidden');
			}else{
			  $('#fourth').closest('label').removeClass('hidden');
			}
			
			if (r.error) {
                msgbox('error', r.message);
                sysfnc.showAlert('#shs-publishform','danger', r.message,true,true,true,'warning');
            }
            
			
			if(r.first){
                $('#first').parent().addClass('checked');
                $('#first').prop('checked', true );
            }else{
                $('#first').parent().removeClass('checked');
                $('#first').prop('checked', false );
            }

            if(r.second){
                $('#second').parent().addClass('checked');
                $('#second').prop('checked', true );
            }else{
                $('#second').parent().removeClass('checked');
                $('#second').prop('checked', false );
            }

            if(r.third){
                $('#third').parent().addClass('checked');
                $('#third').prop('checked', true );
            }else{
                $('#third').parent().removeClass('checked');
                $('#third').prop('checked', false );
            }
			
            if(r.fourth){
                $('#fourth').parent().addClass('checked');
                $('#fourth').prop('checked', true );
            }else{
                $('#fourth').parent().removeClass('checked');
                $('#fourth').prop('checked', false );
            }
        });
        progressBar('off');
    },

    saveConfig: function(term, start, end){
		var checkc = $('#third').prop('checked');
		var checkd = $('#fourth').prop('checked');
        setAjaxRequest( base_url + page_url + 'event', {'event': 'save-config', 'first': start, 'second': end, 'third':checkc, 'fourth':checkd, 't': term}, function(result) {
            if (result.error) {
                msgbox('error', result.message);
            } else {
                msgbox('success', result.message);
                sysfnc.showAlert('#shs-publishform','success', result.message,true,true,true,'save',5);
            }
        });
        progressBar('off');
    },
};

$(document).ready(function(e){
    $('body').on('click', '#save-config', function(e){
        var t = $('#academic-term').val(), s = $('#first').prop('checked'), e =  $('#second').prop('checked');
        Config.saveConfig(t,s,e);
    });
    $('body').on('change', '#academic-term', function(e){
        var t = $('#academic-term').val()
        Config.loadConfig(t);
    });
});