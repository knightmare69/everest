var ADMISSION_HELPER = {

	isFormValid: function(step,hasBoarderColor) {
		var form = stepForm(step);
		var splitForm = stepForm(step).split(',');
		var hasBoarderColor = hasBoarderColor == undefined ? true : hasBoarderColor;

		if (!form) return true;

		if (splitForm.length > 1) {
			var valid = true;
			for(var i in splitForm) {
				// if (splitForm[i] == '#formGuardian') {
				// 	if (isGuardianEmailExist()) {
				// 		return false;
				// 	}
				// }
				if (!isFormValid(splitForm[i],hasBoarderColor)) {
					//familyCustomAppendError(splitForm[i]);
					valid = false;
				} 
				// else {
				// 	familyCustomRemoveError(splitForm[i]);
				// }
			}
			
			return valid;
		} else {

			if (form == '#formLoginAccount') {
				if (!validateAccount()) {
					return false;
				}
			}

			if (form == 'documents') {
				if (!ADMISSION_HELPER.validateDocuments()) {
					$('#tableDocumentsMessage').html("Please add attachement on the required documents.");
					return false;
				} else {
					$('#tableDocumentsMessage').html('');
				}
			}
			else if (!isFormValid(form,hasBoarderColor)) {

				if(form == '#formScholastic') {

					ADMISSION_HELPER.validateQuestions('#tableScholastic');
					ADMISSION_HELPER.validateQuestions('#tableQuestions');
				}
				return false;
			}
			
			if (form == '#formStudent') {
				 if(!validateStudentPhoto()) {
				 	return false;
				 }
				if(!isVerifiedCode()) {
				 	$('.order-error').removeClass('hide');
				 	$('.or-field').attr('style','border: 1px solid red !important');
				 	return false;
				} else {
					$('.or-field').removeAttr('style');
				 	$('.order-error').addClass('hide');
				}
				
				// if (!validateSchoolAttended()) {
				// 	return false;
				// }

				// if (!validateQuestions()) {
				// 	return false;
				// }

			}

			if (form == '#formLoginAccount') {
				if (!validateAccount()) {
					return false;
				}
			}
			
			return true;
		}


		function familyCustomAppendError(form) {
			if (form == '#formFather') {
				$('#family .nav li').eq(0).find('a').html('Father <small class="font-red">Required fields!.</small>')
			}
			else if (form == '#formMother') {
				$('#family .nav li').eq(1).find('a').html('Mother <small class="font-red">Required fields!.</small>')
			}
			else if (form == '#formGuardian') {
				$('#family .nav li').eq(2).find('a').html('Other Information <small class="font-red">Required fields!.</small>')
			}
		}

		function familyCustomRemoveError(i) {
			$('#family .nav > li > a  > small').remove();
		}

		function validateStudentPhoto() {
			$('.photo-message').addClass('hide');
			if (!$('#formStudent #StudentPhoto').hasClass('not-required')) {
				if ($('#formStudent #StudentPhoto')[0].files[0] == undefined) {
					$('.photo-message').removeClass('hide');
					return false;
				}
			}
			return true;
		}

		function isVerifiedCode() {
			if ($('#ORCode').val() != '' && $('#ORSecurityCode').val() != '') {
				return ADMISSION_DATA.isVerifiedCode() == '1' ? true : false;
			}
			return true;
		}

		function isGuardianEmailExist() {

			if (getInputValByName('#family #formGuardian','email') == '') return false;
			if ($('#family #formGuardian input[name="email"]').attr('isValidated') == '1') {
				return false;
			}
			var result = ajaxRequest(base_url+page_url+'event','event=validateGuardianEmail&FamilyID='+getInputVal('#FamilyID')+'&email='+getInputValByName('#formGuardian','email'));
			var object = $('#family #formGuardian input[name="email"]').closest('div');
			if (result == false) {
				$('#family #formGuardian input[name="email"]').attr('style','border: 1px solid red !important');
				if (object.find('span').hasClass('help-block')) {
					object.find('span.help-block').html('Guardian Email is already taken.');
				} else {
					object.append('<span class="help-block font-red">Guardian Email is already taken. </span>');
				}
				object.find('span.help-block').removeClass('hide');
				return true;
			}
			$('#family #formGuardian input[name="email"]').removeAttr('style');
			$('#family #formGuardian input[name="email"]').attr('isValidated',1);
			object.find('span.help-block').addClass('hide');
			return false;
		}

		function validateAccount() {
			
			if (!$('#user-account #IsManageAccount').is(':checked')) return true;

			if (!$('#user-account #username').val()) {
				$('#user-account msg').html("<span class='font-red'><small><i>Username must not be empty!</i></small></span>");
				return false;
			}
			
			if ($('#user-account #username').val().length < 6) {
				$('#user-account msg').html("<span class='font-red'><small><i>Username must have atleast 6 characters!</i></small></span>");
				return false;
			}

			if (isPasswordEmpty()) {
				$('#user-account msg').html("<span class='font-red'><small><i>Password must not be empty!</i></small></span>");
				return false;
			}

			if (!isPasswordMatch()) {
				$('#user-account msg').html("<span class='font-red'><small><i>Password do not match!</i></small></span>");
				return false;
			}

			var res = ajaxRequest(base_url+page_url+'event','event=validateUsername&username='+getInputValByName('#user-account #formLoginAccount','username'));
			if (res.error == true) {
				$('#user-account msg').html("<span class='font-red'><small><i>"+res.message+"</i></small></span>");
				return false
			}

			$('#user-account msg').html('');
			return true;
		}

		function validateSchoolAttended() {
			var status = true;
			$('#tableSchoolsAttended > tbody > tr').each( function() {
				$(this).removeAttr('style');
				if ($(this).find('input').val() == '') {
					$(this).attr('style','border: 1px solid red !important;');
					status = false;
				}
				if ($(this).find('select').val() == '') {
					$(this).attr('style','border: 1px solid red !important;');
					status = false;
				}
			});
			return status;
		}

	},

	validateQuestions: function(table) {
		var status  = true; 
		var xstr    = "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		var caption = 'This field is required.';
		$(table+' > tbody > tr').each( function() {
			var error = (($(this).attr('data-parentid') != 0) ? (xstr+caption) : caption);

			if ($(this).attr('data-parentid') != 0) {
				$(this).find('td select').attr('style','width:96% !important');
			}

			if ($(this).find('td span').hasClass('help-block')) {
		       $(this).find('td span.help-block').addClass('hide');
		    }

		    if (!$(this).find('td input:text').hasClass('not-required')){
	            if ($(this).find('td input:text').hasClass('form-control') && $(this).find('td input:text').val() == '')  {
				    if ($(this).find('td span').hasClass('help-block')) {
				        $(this).find('td span.help-block').removeClass('hide');
				        $(this).find('td span.help-block').html(error);
				    } else {
				        $(this).find('td').append('<span class="help-block font-red">'+error+'</span>');
				    }
	                status = false;
	            }
	        }
		});
		return status;
	},

	isNewStudentFirstYear: function() {
		if ($('#applicationType option:selected').val() == ADMISSION_DATA.getAppTypeDefault()) {
			if( $('#gradeLevel option:selected').val() == ADMISSION_DATA.getGradeLevelDefault()) {		
				return true;	
			}
		}
		return false;
	},
	triggerGradeLevel: function() {
		$('#gradeLevel').trigger('change');
	},

	isBeforeStepCompleted: function(step) {
		if (ADMISSION_HELPER.isFormValid(step)) {
			return true;
		}
		return false;
	},

	validateDocuments: function() {
		var isValid = true;
		$('#documents #tableDocuments tbody tr').each(function() {
			var self = $(this);
			self.find('td .fileUpload').addClass('btn-success');
			self.find('td .fileUpload').removeClass('btn-danger');
			if (self.attr('data-isrequired') == 1) {
				if (self.find('td .upload')[0].files[0] == undefined) {
					self.find('td .fileUpload').removeClass('btn-success');
					self.find('td .fileUpload').addClass('btn-danger');
					isValid = false;
				} 
			}
		});
		return isValid;
	},	
}