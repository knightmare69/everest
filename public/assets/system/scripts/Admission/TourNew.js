var TourKey = '';
var ParentKey = '';
var SchedType = $('#pagescheduletype').val();
var Tour = function() {

	function autoload() {
		dateTime();

		get();
	}

	function get() {
		setAjaxRequest(
			base_url+page_url+'get',
			'',
			function(res) {
				$('#table-wrapper').html(res.result);
			},
			function(error) {
			},
			'json',true, false
		);
	}

	function saveParent() {
		if (!isFormValid('.inquiry-form')) {
			return msgbox('error','Please complete all fields!');
		}
		confirmEvent(
			'Are you sure you want to '+(TourKey ? 'update' : 'save')+' this?',
			function() {
				 onProcess('#tour-btn','','Loading',true);
                lazyLoad( function() {
					setAjaxRequest(
						base_url+page_url+'parent-'+(TourKey ? 'update' : 'save')+'?id='+TourKey,
						$('.inquiry-form').serialize(),
						function(res) {
							if (res.error == false) {
								ParentKey = res.id;
								msgbox('success',res.message);
								$('.inquiry-app-form').show();
								$('.inquiry-form').hide();
								// clear();
							} else {
								msgbox('error',res.message);
							}
							progressBar('off');
							onProcess('#tour-btn','','Loading',false);
						},
						function(error) {
							onProcess('#tour-btn','','Loading',false);
							msgbox('error','There was an error occured. Please try again!');
							progressBar('off');
						}
					);
				});
			}
		);
	}

	function saveChild() {
		if (!isFormValid('.inquiry-app-form')) {
			return msgbox('error','Please complete all fields!');
		}
		confirmEvent(
			'Are you sure you want to '+(TourKey ? 'update' : 'save')+' this?',
			function() {
				onProcess('#tour-btn','','Loading',true);
                lazyLoad( function() {
					setAjaxRequest(
						base_url+page_url+'child-'+(TourKey ? 'update' : 'save')+'/'+ParentKey,
						$('.inquiry-app-form').serialize(),
						function(res) {
							if (res.error == false) {
								msgbox('success',res.message);
								clear_value('.inquiry-app-form');
							} else {
								msgbox('error',res.message);
							}
							progressBar('off');
							onProcess('#tour-btn','','Loading',false);
						},
						function(error) {
							onProcess('#tour-btn','','Loading',false);
							msgbox('error','There was an error occured. Please try again!');
							progressBar('off');
						}
					);
				});
			}
		);
	}

	function edit(id) {
		setAjaxRequest(
					base_url+page_url+'event?event=edit',
					'id='+id,
					function(res) {
						var res = res.data;
						$('#schedule_time').val(res.time);
						$('#schedule_date').val(res.date);
						$('#remarks').val(res.remarks);
						$('#schedule_status').val(res.status);
						$('#schedule_datetime').val(res.date+' '+res.time);
						TourKey = id;
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
	}

	function deletePanel(id) {
		confirmEvent(
			'Are you sure you want to delete this?',
			function() {
				setAjaxRequest(
					base_url+page_url+'event?event=delete&id='+id,
					$('#FormTour').serialize(),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							clear();
							get();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			}
		);
	}

	function clear() {
		$("#panel-user").val('').trigger('change');
		$('#FormTour input[type="text"],#FormTour select, #FormTour textarea').val('');
		clear_value('#batch-form');
		TourKey = '';
		batchKey = '';
	}

	function back() {
		$('.inquiry-app-form').hide();
		$('.inquiry-form').show();
	}

	function dateTime() {
		$('#schedule_time').timepicker();
	}

	return {
		init: function() {
			autoload();
		},

		saveParent: function() {
			saveParent();
		},

		saveChild: function() {
			saveChild();
		},

		edit: function(id) {
			edit(id);
		},

		delete: function(id) {
			deletePanel(id);
		},

		cancel: function() {
			clear();
		},

		back: function() {
			back();
		}
	}
}();

var batchKey = '';
var Batch = function() {

	function batchNew() {
		showModal({
            name: 'basic',
            title: 'Create New Group',
            content: ajaxRequest(base_url + page_url + 'batch-form', 'key=' + batchKey, 'html'),
            class: 'modal_batch'
        });
        var title = $('#yearlevel option:selected').val() != '' ? $('#yearlevel option:selected').text() : '';
        $('#groupTitle').val(title);
        dateTime();
	}

	function dateTime() {
		$('#schedule_time').timepicker();
		$('#schedule_date').datepicker();
	}

	function gotoWithSchedTab() {
		$('a[href="#with-schedule"]').trigger('click');
	}

	function gotoNonSchedTab() {
		$('a[href="#non-schedule"]').trigger('click');
	}

	function getBatch() {
		lazyLoad( function() {
			setAjaxRequest(
				base_url+page_url+'get-batch',
				'schedtype='+SchedType,
				function(res) {
					$('#table-batch').html(res);
				},
				function(error) {
					
				},
				'html',true,false
			);
		});
	}

	function getNonSched(yearlevel, term) {
		lazyLoad( function() {
			setAjaxRequest(
				base_url+page_url+'get-nonsched',
				'yearlevel='+(yearlevel == undefined ? '' : yearlevel)+'&schedtype='+SchedType+'&term='+(term == undefined ? '' : term),
				function(res) {
					$('#no-schedule').html(res);
				},
				function(error) {
					
				},
				'html',true,false
			);
		});
	}

	function getWithSched(BatchID) {
		lazyLoad( function() {
			setAjaxRequest(
				base_url+page_url+'get-withsched/'+BatchID,
				'schedtype='+SchedType,
				function(res) {
					$('#with-schedule').html(res);
					progressBar('off');
				},
				function(error) {
					progressBar('off');
				},
				'html',true,true
			);
		});
	}

	function emailBlast() {
		showModal({
            name: 'basic',
            title: 'Create Email & Send',
            content: ajaxRequest(base_url + page_url + 'email-content/'+batchKey, '', 'html'),
            class: 'modal_batch',
            button: {
            	caption: 'Send'
            }
        });
        $('#email_content').summernote({height: 300});
	}

	function mainSearch() {
		getNonSched($('#yearlevel option:selected').val(),$('#schoolyear option:selected').val());
	}

	function addApplicant() {
		if (!batchKey) {
			return msgbox('error','Please select Tour Group.');
		}

		var ids = new Array();
		$('#table-nonsched > tbody > tr').each( function() {
			if ($(this).find('.chk-nonsched').is(':checked')) {
				ids.push({
					id: $(this).attr('data-id')
				});
			}
		});

		confirmEvent(
			'Are you sure you want to add this?',
			function() {
				lazyLoad( function() {
				setAjaxRequest(
					base_url+page_url+'add-applicant/'+batchKey,
					'ids='+JSON.stringify(ids),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							mainSearch();
							clear();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
						onProcess('.btn_modal','','',false);
					},
					function(error) {
						onProcess('.btn_modal','','',false);
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			});
			}
		);
	}
    
	function removeApplicant(xid) {
		if (!batchKey) {
			return msgbox('error','Please select Tour Group.');
		}

		var ids = new Array();
		    ids.push({id: xid});

		confirmEvent(
			'Are you sure you want to remove this?',
			function() {
				lazyLoad( function() {
				setAjaxRequest(
					base_url+page_url+'rem-applicant/'+batchKey,
					'ids='+JSON.stringify(ids),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							mainSearch();
							clear();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
						onProcess('.btn_modal','','',false);
					},
					function(error) {
						onProcess('.btn_modal','','',false);
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			});
			}
		);
	}
	
	function confirmApplicant(xid) {
		if (!batchKey) {
			return msgbox('error','Please select Tour Group.');
		}

		var ids = new Array();
		    ids.push({id: xid});

		lazyLoad( function() {
			setAjaxRequest(
				base_url+page_url+'con-applicant/'+batchKey,
				'ids='+JSON.stringify(ids),
				function(res) {
					if (res.error == false) {
						msgbox('success','Success,Email Sent');
						mainSearch();
						clear();
					} else {
						msgbox('error',res.message);
					}
					progressBar('off');
					onProcess('.btn_modal','','',false);
				},
				function(error) {
					onProcess('.btn_modal','','',false);
					msgbox('error','There was an error occured. Please try again!');
					progressBar('off');
				}
			);
		});
	}
	
	function save() {
		if (!isFormValid('#form-batch')) {
			return msgbox('error','Please complete all fields!');
		}
		confirmEvent(
			'Are you sure you want to '+(TourKey ? 'update' : 'save')+' this?',
			function() {
				onProcess('.btn_modal','','Loading',true);
                lazyLoad( function() {
					setAjaxRequest(
						base_url+page_url+(TourKey ? 'update-batch' : 'save-batch')+'?id='+batchKey+'&schedtype='+SchedType,
						$('#form-batch').serialize(),
						function(res) {
							if (res.error == false) {
								msgbox('success',res.message);
								getBatch();
								clear();
							} else {
								msgbox('error',res.message);
							}
							progressBar('off');
							onProcess('.btn_modal','','',false);
						},
						function(error) {
							onProcess('.btn_modal','','',false);
							msgbox('error','There was an error occured. Please try again!');
							progressBar('off');
						}
					);
				});
			}
		);
	}

	function clear() {
		clear_value('#form-batch');
		dateTime();
		batchKey = '';
	}


	return {
		init: function () {
			getBatch();
			getNonSched($('#yearlevel option:selected').val());
			$(document).on('click','.modal_batch > .modal-dialog > .modal-content > .modal-footer > .btn_modal', function() {
				save();
			});
			
			$('body').on('click','.confirm,.remove',function(){
				var xid = $(this).closest('tr').attr('data-id');
				if($(this).is('.remove')){
					removeApplicant(xid);
				}else if($(this).is('.confirm')){
					confirmApplicant(xid);
				}else{
					alert(xid);
				}
			});
		},
		new: function() {
			batchNew();
		},
		getWithSched(BatchID) {
			getNonSched();
			getWithSched(batchKey = BatchID);
			gotoWithSchedTab();
		},
		addApplicant() {
			addApplicant();
		},
		emailBlast() {
			emailBlast();
		},
		mainSearch() {
			mainSearch();
		}
	}
}();
