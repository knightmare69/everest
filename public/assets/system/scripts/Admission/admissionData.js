var ADMISSION_DATA = {
	getGradeLevelDefault: function() {
		return $('#gradeLevel option:first-child').next().val();
	},

	getAppTypeDefault: function() {
		return $('#applicationType option:first-child').next().val();
	},

	setStudentPhoto: function() {
		$('#student #formStudent #StudentPhotoThumbnail').attr('src',base_url+'general/getStudentPhoto?AppNo='+getParameterByName('AppNo'));
	},

	setThumnbailPhoto: function() {
		var form_data = new FormData();
		form_data.append('photo',$('#student #formStudent #StudentPhoto')[0].files[0]);
		$.ajax({
            url: base_url+'general/getThumbnailPhoto',
            dataType: "html",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            async: false,
            success: (
             	function(result) {
    
              	}
             ),
       });
	},

	isVerifiedCode: function() {
		return true;//ajaxRequest(base_url+page_url+'event','event=verifyCode&ORSecurityCode='+$('#ORSecurityCode').val()+'&ORCode='+$('#ORCode').val());
	},
	// admitTemp: function(el,isTemp) {
	// 	$('.label-agree').addClass('hide');
	// 	return ajaxRequest(
	// 			base_url+page_url+'event',
	// 			'event=save&isTemp='+isTemp+
	// 			'&AppNo='+getParameterByName('AppNo')+
	// 			'&'+$('#formYear').serialize()+
	// 			'&'+$('#formStudent').serialize()+
	// 			'&'+$('#guardianInfo').serialize()+
	// 			'&'+$('#formFather').serialize()+
	// 			'&'+$('#formMother').serialize()+
	// 			'&'+$('#formGuardian').serialize()+
	// 			'&'+$('#formSibling').serialize()+
	// 			'&'+$('#formSignatures').serialize()+
	// 			'&'+$('#formLoginAccount').serialize()+
	// 			'&AdmissionStatus='+getSelectedVal('#AdmissionStatus')+
	// 			'&siblingsData='+ADMISSION_DATA.getSiblingsData()+
	// 			'&referencesData='+ADMISSION_DATA.getReferencesData()+
	// 			'&GuardianCityID='+$('#GuardianCity option:selected').attr('data-id')+
	// 			'&BillingCityID='+$('#BillingCity option:selected').attr('data-id')+
	// 			'&CourseChoicesData='+JSON.stringify(College.getCourseChoices())+
	// 			'&scholasticData='+ADMISSION_DATA.getScholastic()+
	// 			'&questionData='+ADMISSION_DATA.getQuestions()
	// 		);
	// },
	admitData: function(el,isTemp) {
		$('.label-agree').addClass('hide');
		return 'event=save&isTemp='+isTemp+
			'&AppNo='+getParameterByName('AppNo')+
			'&'+$('#formYear').serialize()+
			'&'+$('#formStudent').serialize()+
			'&'+$('#formScholastic').serialize()+
			'&'+$('#guardianInfo').serialize()+
			'&'+$('#formFather').serialize()+
			'&'+$('#formMother').serialize()+
			'&'+$('#formGuardian').serialize()+
			'&'+$('#formSibling').serialize()+
			'&'+$('#formSignatures').serialize()+
			'&'+$('#formLoginAccount').serialize()+
			'&AdmissionStatus='+$('#AdmissionStatus').val()+
			'&siblingsData='+ADMISSION_DATA.getSiblingsData()+
			'&referencesData='+ADMISSION_DATA.getReferencesData()+
			'&GuardianCityID='+$('#GuardianCity option:selected').attr('data-id')+
			'&BillingCityID='+$('#BillingCity option:selected').attr('data-id')+
			'&CourseChoicesData='+JSON.stringify(College.getCourseChoices())+
			'&scholasticData='+ADMISSION_DATA.getScholastic()+
			'&questionData='+ADMISSION_DATA.getQuestions()+
			'&schoolsAttendedData='+ADMISSION_DATA.getSchoolsAttended();
	},
	getSiblingsData: function() {
		var data = new Array;
		$('#siblings #tableSiblings tbody tr').each(function() {
			data.push(JSON.stringify({
				key: $(this).attr('data-id'),
				name: $(this).find('td').eq(1).find('input').val(),
				dob: $(this).find('td').eq(2).find('input').val(),
				age: $(this).find('td').eq(3).find('input').val(),
				gender: $(this).find('td').eq(4).find('select option:selected').val(),
				school: $(this).find('td').eq(5).find('input').val(),
			}));
		});
		return data;
	},
	getReferencesData: function() {
		var data = new Array;
		$('#references #tableReferences tbody tr').each(function() {
			data.push(JSON.stringify({
				key: $(this).attr('data-id'),
				name: $(this).find('td').eq(1).find('input').val(),
				rel: $(this).find('td').eq(2).find('input').val(),
				school: $(this).find('td').eq(3).find('input').val(),
				phone: $(this).find('td').eq(4).find('input').val(),
			}));
		});
		return data;
	},
	getSchoolsAttended: function() {
		var data = new Array;
		$('#scholastic #tableSchoolsAttended tbody tr').each(function() {
			data.push(JSON.stringify({
				id: $(this).attr('data-id'),
				school: $(this).find('td').eq(1).find('.school').val(),
				address:$(this).find('td').eq(2).find('.address').val(),
				fromMonth:$(this).find('td').eq(3).find('.FromMonthAttended option:selected').val(),
				fromYear:$(this).find('td').eq(3).find('.FromYearAttended option:selected').val(),
				toMonth:$(this).find('td').eq(3).find('.ToMonthAttended option:selected').val(),
				toYear:$(this).find('td').eq(3).find('.ToYearAttended option:selected').val(),
				yearLevel:$(this).find('td').eq(4).find('.GradeLevel').val(),
			}));
		});
		return data;
	},
	getQuestions: function() {
		var data = new Array;
		$('#tableQuestions tbody tr').each(function() {
			data.push(JSON.stringify({
				id: $(this).attr('data-id'),
				// question: $(this).attr('data-question'),
				answer: $(this).find('.answer').val(),
			}));
		});
		return data;
	},
	getScholastic: function() {
		var data = new Array;
		$('#tableScholastic tbody tr').each(function() {
			data.push(JSON.stringify({
				id: $(this).attr('data-id'),
				answer: $(this).find('.answer').val(),
			}));
		});
		return data;
	},
	showProgram: function(key,ProgClass) {
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
		setAjaxRequest(base_url+page_url+'event','event=showGradeLevelProgram&key='+key+'&class='+ProgClass
			, function(result) {
				var options = '<option value="">Select...</option>';
				for(var i in result) {
					options += "<option value='"+result[i].MID+"'>"+result[i].MName+"</option>";
				}

				var PName = (result[0] != undefined && result[0].PName != undefined)  ? result[0].PName : '';
				var PID = (result[0] != undefined &&  result[0].PID != undefined) ? result[0].PID : '';
				var PClass = (result[0] != undefined &&  result[0].PClass != undefined) ? result[0].PClass : '';


				if (result.length > 1 ) {
					$('#ProgramMajor').removeClass('not-required');
					$('.majorWrapper').removeClass('hide');
				}
				else {
					$('#ProgramMajor').addClass('not-required');
					$('.majorWrapper').addClass('hide');
					$('#gradeProgramMajorID').val('');
					$('#ProgramMajor').val('');
				}

				$('#ProgramMajor').html(options);
				$('#gradeProgramName').val(PName);
				$('#gradeProgramID').val(PID);
				$('#gradeProgramClass').val(PClass);

				if ($('#ProgramMajor option:selected').val() == '') {
					$('#ProgramMajor').val($('#gradeProgramMajorID').val());
				}

				if (College.isHigherLevel()) {
					$('#ProgramMajor').addClass('not-required');
					$('.majorWrapper').addClass('hide');
				} 
				isFormValid('#student #formStudent');
				progressBar('off');
			},
			'','JSON',true,false
		);
	},

	saveDocuments: function(AppNo,FamilyID) {
		var data = new Array,i = 0, form_data = new FormData();
		$('#documents #tableDocuments tbody tr').each(function() {
			var self = $(this);
			if (self.find('td .upload')[0].files[0] != undefined) {
				form_data.append('file['+i+']', self.find('td .upload')[0].files[0]);
			}
			data = {
				EntryID: self.attr('data-entryid'),
				TempID: self.attr('data-tempID'),
				Doc: self.attr('data-doc'),
				TempDetailsID: self.attr('data-id'),
				IsReviewed: self.find('.IsReviewed').is(':checked'),
				IsExempted: self.find('.IsExempted').is(':checked')
			};				
			form_data.append('details['+i+']',JSON.stringify(data));
			i++;
		});
		
		$.ajax({
            url: base_url +page_url+'event?event=saveDocuments&AppNo='+AppNo+'&FamilyID='+FamilyID,
            dataType: "html",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            async: false,
            success: (
             	function(result) {
             		
              	}
             ),
       });
	},

	savePhoto: function(AppNo,FamilyID) {
           // console.log($('#StudentPhoto')[0]);
           // return;
		if ($('#StudentPhoto')[0] == undefined ||  $('#parents #GuardianPhoto')[0] == undefined) return;
		var form_data = new FormData();
		form_data.append('photo', $('#formStudent #StudentPhoto')[0].files[0]);
		form_data.append('guardianPhoto', $('#parents #GuardianPhoto')[0].files[0]);
		$.ajax({
            url: base_url +page_url+'event?event=savePhoto&AppNo='+AppNo+'&FamilyID='+FamilyID,
            dataType: "html",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            async: false,
            success: (
             	function(result) {
             		
              	}
             ),
       });
	},

	countryIcons: function(el) {
		var el = el == undefined ? '#GuardianCountry' : el;
		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='" +base_url + "assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $(el).select2({
            placeholder: "Select a Country",
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });
	},

	getCity: function(code,CityEl) {
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
		var CityEl = CityEl != undefined ? CityEl : '#GuardianCity'
		var optionSel = '', options = '';
		setAjaxRequest(
			base_url+page_url+'event','event=getCity&code='+code,
			function(city) {
				options = '<option value=""></option>';
				optionSel = $(CityEl).parent().find('.CitySel').val();
				
				for(var i in city) {
					option = document.createElement('option');
					option.setAttribute('data-id',city[i].CityID);
					option.setAttribute('value',city[i].City);
					if (optionSel == city[i].CityID) {
						option.setAttribute('selected','selected');
					}
					option.appendChild(document.createTextNode(city[i].City));
					options += option.outerHTML;
				}
				$(CityEl).html(options);
				$(CityEl).select2();
				$(CityEl).trigger('change');
			},'','json',true,false
		);
	},

	toggleOtherCity: function(isCheck) {
		if (isCheck) {
			$('#otherCity').removeClass('hide');
			$('#otherCity').removeClass('not-required');
			$('#GuardianCity').addClass('hide');
			$('#GuardianCity').addClass('not-required');
		} else {
			$('#otherCity').addClass('hide');
			$('#otherCity').addClass('not-required');
			$('#GuardianCity').removeClass('hide');
			$('#GuardianCity').removeClass('not-required');
		}
	},

	toggleBillingOtherCity: function(isCheck) {
		if (isCheck) {
			$('#BillingOtherCity').removeClass('hide');
			$('#BillingOtherCity').removeClass('not-required');
			$('#BillingCity').addClass('hide');
			$('#BillingCity').addClass('not-required');

		} else {
			$('#BillingOtherCity').addClass('hide');
			$('#BillingOtherCity').addClass('not-required');
			$('#BillingCity').removeClass('hide');
			$('#BillingCity').removeClass('not-required');
		}
	},

	getYearLevel: function(type) {
		var level;
		if (isHigherLevel()) {
			level = type == undefined ? ADMISSION_DATA.getSelEducLevel() : type;
		} else {
			level = 'basic';
		}	
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
		setAjaxRequest(
				base_url+page_url+'event', 'event=getYearLevel&type='+level,
				function(data) {
					var SetGradeLevel = $('#SetGradeLevel').val();
					var SetProgClass = $('#SetProgClass').val();
					var options = '<option value="">- SELECT -</option>';
					var inquiry = $('#gradeLevel').data('inquiry');
					var prgcls  = $('#SetProgClass').val();
					for(var i in data) {
						option = document.createElement('option');
						option.setAttribute('value',data[i].id);
						option.setAttribute('data-progclass',data[i].class);
                        option.setAttribute('data-prog',data[i].prog);
						option.setAttribute('data-isGradeLevel',data[i].class <= '11' ? 1 : 0);
						option.appendChild(document.createTextNode(data[i].name));
						if (SetGradeLevel == data[i].id && SetProgClass == data[i].class) {
							option.setAttribute('selected','selected');

							if (data[i].class <= 11) {
								$('#ProgramMajor').addClass('not-required');
								$('.majorWrapper').addClass('hide');
							}
						}
						
						if(data[i].id==inquiry && data[i].class==prgcls){
						  option.setAttribute('selected','selected');	

						}
						
						options += option.outerHTML;
					}
					$('#gradeLevel').html(options);
					if(type != undefined){
						$('#gradeLevel').trigger('change');
					}
					// enableSchoolAttended();
					// examSchedule();
					isFormValid('#student #formStudent');
				},'','json',true,false
		);
	},

	getCourses: function(type) {
	    type =  type ==undefined ? 'higher': 'basic';
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
		setAjaxRequest(
				base_url+page_url+'event', 'event=getCourses&class='+type,
				function(data) {
					var Course1 = new Array,Course2 = new Array,Course3 = new Array;
					var options = '<option value=""></option>';

					Course1 = [$('#Course1Choice').val()];
					Course2 = [$('#Course2Choice').val()];
					Course3 = [$('#Course3Choice').val()];
					Course4 = [$('#Course4Choice').val()];

					for(var i in data) {
						option = document.createElement('option');
						option.setAttribute('value',data[i].id);
						option.setAttribute('data-mid',data[i].mid);
						option.appendChild(document.createTextNode(data[i].course));
						options += option.outerHTML;
					}

					
					$('#Course1').html(options);
					$('#Course1').select2('val',Course1);

					$('#Course2').html(options);
					$('#Course2').select2();
					$('#Course2').select2('val',[Course2]);

					$('#Course3').html(options);
					$('#Course3').select2();
					$('#Course3').select2('val',[Course3]);

					$('#Course4').html(options);
					$('#Course4').select2();
					$('#Course4').select2('val',[Course4]);

					isFormValid('#formYear');

				},'','json',true,false
		);
	},

	getSelEducLevel: function() {
		/*
		if ($('.EducLevel').iCheck('update')[1].checked) {
			return 'higher';
		}
		*/
		return 'basic';
		
	}
}
