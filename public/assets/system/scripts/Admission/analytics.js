var termid = 0;

function load_monitoring(target){
 var ayterm = $('#ayterm').val();
 var status = $('#monStatus').val();
 var filter = $('#monFilter').val();
 var limit  = $('#monLength').val();
 
 if(ayterm=='-1' || status=='' || status==''){return false;}
 
 setAjaxRequest(base_url+page_url+'event',
            {event:'monitoring',termid:ayterm,status:status,filter:filter,limit:limit},
			function(result) {
				progressBar('off');
				if(result.success){
				$(target).html(result.content);
				}
			},
			'',
			'JSON',true
		);
}

function load_total(targeta,targetb,source){
 var ayterm  = $('#ayterm').val();
 if(source==undefined || source==''){
   source = 0;
 }else{
   ayterm = source;
 }
 
 targeta = ((targeta==undefined)?'':targeta);
 targetb = ((targetb==undefined)?'':targetb);
 
 if(ayterm=='-1'){return false;}
 
 setAjaxRequest(base_url+page_url+'event',
            {event:'total',termid:ayterm},
			function(result) {
				progressBar('off');
				if(result.success){
				 if(targeta!=''){
				  $(targeta).html(result.applicant);
				 }
				 
				 if(targetb!=''){
				  $(targetb).html(result.student);
				 }
				}
			},
			'',
			'JSON',true
		);
}

$('body').ready(function(){
  $('body').on('change','#ayterm',function(){
    var tmpA  = $(this).val();
	var label = $(this).find('option[value="'+tmpA+'"]').text();
	$('.termLabel').html(' - '+label);
    load_monitoring('#monTable');
	load_total('#noApplicant','#noStudent');
	load_total('#mainApplicant','#mainStudent');
	$('#conList').trigger('change');
  });
  
  $('body').on('change','#xterm',function(){
    var tmpA  = $(this).val();
	var label = $(this).find('option[value="'+tmpA+'"]').text();
	$('.xLabel').html(' - '+label);
    load_total('#mainApplicant','#mainStudent');
	load_total('#xApplicant','#xStudent',tmpA);
  });
  
  $('body').on('change','#monStatus,#monLength',function(){
    load_monitoring('#monTable');
  });
  
  $('body').on('keydown','#monFilter',function(e){
    if(e.keyCode==13){
	load_monitoring('#monTable');
	}
  });
  
  $('body').on('click','.condRefresh',function(){
    $('#conList').trigger('change');
  });
  
  $('body').on('click','.noRefresh',function(){
    load_total('#noApplicant','#noStudent')
  });
  
  $('body').on('click','.mainRefresh',function(){
    load_total('#mainApplicant','#mainStudent')
  });
  
  $('body').on('click','.monRefresh',function(){
    load_monitoring('#monTable');
  });
  
  $('body').on('change','#conList',function(){
      switch($(this).val()){
	    case 'siblings':
	    case 'nation':
	    case 'religion':
		  $('#conFilter').closest('.portlet-input').addClass('hidden');
		break;
		default:
		  $('#conFilter').closest('.portlet-input').removeClass('hidden');
		break;
	  }
	  
      var ayterm = $('#ayterm').val();
      $('#conFilter').val('');
	  setAjaxRequest(base_url+page_url+'event',{event:'consolidated',param:$(this).val(),termid:ayterm},
			function(result) {
				progressBar('off');
				$('#conTable').html(result.content);
			},
			'',
			'JSON',true
		);
  });
  
  $('body').on('click','.maintab [data-toggle="tab"]',function(){
     $('.monitoring').addClass('hidden');
     $('.total_no').addClass('hidden');
     $('.cond_info').addClass('hidden');
     $('.compare').addClass('hidden');
     $($(this).attr('href')).removeClass('hidden');
  });
  
  $('body').on('click','.print',function(){
     var ayterm = $('#ayterm').val();
     var xterm  = $('#xterm').val();
     var param  = $('#conList').val();
     var status = $('#monStatus').val();
     var event  = $(this).attr('data-event');
	 if(event==undefined || event==''){return false;}
	 window.open(base_url+page_url+'print?event='+event+'&termid='+ayterm+'&xtermid='+xterm+'&status='+status+'&param='+param,'_blank');
  });
  
  $('body').on('change','#conFilter',function(){
    var input, filter, table, tr, td, i, txtValue;
	  input = $(this);
	  filter = $(this).val().toUpperCase();
	  table = document.getElementById("conTable");
	  tr = table.getElementsByTagName("tr");

	  // Loop through all table rows, and hide those who don't match the search query
	  for (i = 0; i < tr.length; i++) {
	    var x = tr[i].getElementsByTagName("td").length;
		var y = '';
		for(j=0;j<x;j++){
		td = tr[i].getElementsByTagName("td")[j];
		if (td) {
		  txtValue = td.textContent || td.innerText;
		  if (txtValue.toUpperCase().indexOf(filter) > -1) {
			y = "";
			console.log(i);
			break;
		  } else {
			y = "none";
		  }
		}
       }
       tr[i].style.display = y;	   
	  }
  
  });
  
  $('#ayterm').trigger('change');
  $('#xterm').trigger('change');
});