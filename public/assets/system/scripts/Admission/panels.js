var PanelKey = '';
var Panels = function() {

	function autoload() {
		$("#panel-user").select2({
		    minimumInputLength: 2,
		    tags: [],
		    ajax: {
		        url: base_url+page_url+'search-user',
		        dataType: 'json',
		        type: "GET",
		        quietMillis: 50,
		        data: function (term) {
		            return {
		                term: term
		            };
		        },
		        results: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                    return {
		                        text: item.FullName,
		                        slug: item.UserIDX,
		                        id: item.UserIDX
		                    }
		                })
		            };
		        }
		    }
		});

		get();
	}

	function get() {
		setAjaxRequest(
			base_url+page_url+'get',
			'',
			function(res) {
				$('#table-wrapper').html(res.result);
			},
			function(error) {
			},
			'json',true, false
		);
	}

	function save() {
		if (!isFormValid('#FormPanels')) {
			return msgbox('error','Please complete all fields!');
		}
		confirmEvent(
			'Are you sure you want to '+(PanelKey ? 'update' : 'save')+' this?',
			function() {
				setAjaxRequest(
					base_url+page_url+'event?event='+(PanelKey ? 'update' : 'save')+'&id='+PanelKey,
					$('#FormPanels').serialize(),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							clear();
							get();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			}
		);
	}

	function edit(id) {
		setAjaxRequest(
					base_url+page_url+'event?event=edit',
					'id='+id,
					function(res) {
						var res = res.data;
						$("#panel-user").select2('data',{id:res.UserID,text: res.FullName}).trigger('change');
						$('#level').val(res.Level);
						$('#schoolyear').val(res.TermID);
						$('#position-title').val(res.Title);
						PanelKey = id;
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
	}

	function deletePanel(id) {
		confirmEvent(
			'Are you sure you want to delete this?',
			function() {
				setAjaxRequest(
					base_url+page_url+'event?event=delete&id='+id,
					$('#FormPanels').serialize(),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							clear();
							get();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			}
		);
	}

	function clear() {
		$("#panel-user").val('').trigger('change');
		$('#FormPanels input[type="text"],#FormPanels select').val('');
		PanelKey = '';
	}

	return {
		init: function() {
			autoload();
		},

		save: function() {
			save();
		},

		edit: function(id) {
			edit(id);
		},

		delete: function(id) {
			deletePanel(id);
		},

		cancel: function() {
			clear();
		}
	}
}();
