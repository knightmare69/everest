var EMAIL = {
	send : function() {
		var url = 'admission/listing/';
		EMAIL.setMessage('');
		$('#s2id_RecepientsEmail').removeAttr('style');
		if ( $('.note-editable').attr('data-style') != undefined ) {
			$('.note-editable').attr('style',$('.note-editable').attr('data-style'));
		}
		$('#sendSubject').removeAttr('style');
		if (EMAIL.getRecepientsEmail().length <= 0) { 
			$('#s2id_RecepientsEmail').attr('style','border: 1px solid red !important');
			EMAIL.setMessage('Please add recipients!',true);
			return;
		} 
		if ($('#sendSubject').val().trim()  == '') {
			$('#sendSubject').attr('style','border: 1px solid red !important');
			EMAIL.setMessage('Please add email subject!',true);
			return;
		} 
		if ($('.note-editable').text().trim()  == '') {
			$('.note-editable').attr('data-style',$('.note-editable').attr('style'));
			$('.note-editable').attr('style',$('.note-editable').attr('style')+';border: 1px solid red !important;');
			EMAIL.setMessage('Please add email content!',true);
			return;
		}
        $('.btn_send_email').removeClass('blue');
        $('.btn_send_email').addClass('default');
        $('.btn_send_email').html('<img src="'+base_url+'assets/global/img/loading-spinner-grey.gif" style="width:12%;"> sending email. . .');
        $('.btn_send_email').prop('disabled',true);
        var message = 'Could not send email!';
        $.ajax({url:base_url+url+'sendManualEmail',dataType:'json',data:'title='+$('#sendSubject').val()+'&message='+$('.note-editable').html().trim()+'&recipient='+EMAIL.getRecepientsEmail()+'&cc='+EMAIL.getCc(),type:'POST',async:true,
            success: (
            function(data) {
                if(data){
                    message = 'Successfully sent!';
                    EMAIL.setMessage(message);
                }
            }),
            error: function(xhr,status,error){
            	EMAIL.setMessage(message);
                showError(error);
            }
        });
	},
	setMessage : function(message,error) {
		var error = error == undefined ? false : error;
		$('.email_message').html(message);
		if ( error ) {
			$('.email_message').addClass('badge-danger');
		} else {
			$('.email_message').addClass('badge-success');
		}

        $('.btn_send_email').removeClass('default');
        !$('.btn_send_email').hasClass('blue') ? $('.btn_send_email').addClass('blue') : '';
        $('.btn_send_email').html('<i class="fa fa-envelope"></i> Send');
	},
 	getRecepientsEmail : function() {
		var data = [], recipient = [],  i = 0;
		$('#s2id_RecepientsEmail .select2-choices li.select2-search-choice').each(function() {
			data.push({name:$(this).find('div').text().trim().toLowerCase()})
		});
		for(i in data) {
			$('#RecepientsEmail option').each(function() {
				if (data[i].name == $(this).text().trim().toLowerCase()){
					if (!isExist(recipient,$(this).val())){
						recipient.push($(this).val());
					}
				}
			});
		}
		function isExist(recipient,name){
			var  x = 0;
			for(x in recipient) {
				if (recipient[x] == name){
					return true;
				}
			}
			return false;
		}
		return recipient;
	},
	getCc : function() {
		var data = [], cc = [],  i = 0;
		$('#s2id_CcEmail .select2-choices li.select2-search-choice').each(function() {
			data.push({name:$(this).find('div').text().trim().toLowerCase()})
		});
		for(i in data) {
			$('#CcEmail option').each(function() {
				if (data[i].name == $(this).text().trim().toLowerCase()){
					if (!isExist(cc,$(this).val())){
						cc.push($(this).val());
					}
				}
			});
		}
		function isExist(cc,name){
			var  x = 0;
			for(x in cc) {
				if (cc[x] == name){
					return true;
				}
			}
			return false;
		}
		return cc;
	},
	createEmailOptions : function(email){
        var options = '';
        if(email != ''){
           var option = document.createElement('option');
            option.setAttribute('value',email);
            option.appendChild(document.createTextNode(email));
            options += option.outerHTML;
        }
        return options;
    }
};