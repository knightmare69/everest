var ListingData = function() {

    var dataTableLogs = function() {
        $('#tableApplicants').dataTable().empty();
        $('#tableApplicants').dataTable().fnDestroy();
        var grid = new Datatable();
        grid.init({
            src: $("#tableApplicants"),
            onSuccess: function (grid) {
                progressBar('off');
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here 
                ],
                "autoWidth": false,
                "pageLength": 10, // default record count per page
                "columns": [
                    { "class":"text-center","data": 0 },
                    { "data": 1,width: 160 },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 2 
                    },
                    { "data": 3, "class": 'notParentFields' },
                    { "data": 4 },
                    { "data": 5 },
                    { "data": 6 },
                    { "data": 7 },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 8 
                    },
                    { "data": 9 },
                    { "data": 10 },
                    { "class": "hide","data": 11 },
                    { "class": "hide","data": 12 },
                    { "class": "hide","data": 13 },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 14, "class": 'notParentFields' 
                    },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 15, "class": 'notParentFields' 
                    }
                ],
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": base_url+page_url+'search', // ajax source
                },
                "drawCallback": function (settings){ 
                    var response = settings.json;
                    if(response.isParent){
                        $('.notParentFields').addClass('hide');
                    }
                    Metronic.initUniform();
                },
                "order": [
                    [2, "asc"]
                ] // set first column as a default sort by asc
            }
        });

         // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            // grid.setAjaxParam("IsHigherLevel", $('#IsHigherLevel').is(':checked') ? 1 : 0);
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });
    };

    
    return {
        init: function() {
            dataTableLogs();
        }
    }
}();