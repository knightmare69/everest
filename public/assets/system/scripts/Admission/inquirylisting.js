var ListingData = function() {

    var dataTableLogs = function() {
        $('#table-inquiries').dataTable().empty();
        $('#table-inquiries').dataTable().fnDestroy();
        var grid = new Datatable();
        grid.init({
            src: $("#table-inquiries"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "serverSide": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here 
                ],
                "autoWidth": true,
                "pageLength": 10, // default record count per page
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": base_url+page_url+'listings', // ajax source
                    "data": {
                        "schoolYear": $('#schoolYear option:selected').val(),
                        "gradeLevel": $('#gradeLevel option:selected').val(),
                        "quarter": $('#quarter option:selected').val()
                    },
                     beforeSend: function(){
                        $('#table-inquiries > tbody').html(
                        '<div class="loading-message"><img src="/everest/public/assets/global/img/loader.gif" align=""><span>&nbsp;&nbsp;Loading...</span></div>'
                      );
                    },
                },
                "order": [
                    [1, "asc"]
                ] // set first column as a default sort by asc
            }
        });

         // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            // grid.setAjaxParam("IsHigherLevel", $('#IsHigherLevel').is(':checked') ? 1 : 0);
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });
    };

    var updateType = function(el, id, type) {
        if (type == '') {
            return msgbox('error','Please choose type!');
        }

        onProcess(el,'','Updating',true);
        lazyLoad( function() {
            setAjaxRequest(
                base_url+page_url+'update-type/'+id,
                'type='+type,
                function(res) {
                    if (res.error == false) {
                        msgbox('success',res.message);
                        clear_value('.inquiry-app-form');

                        if(type == 'denied') {
                            $('.modal_email #RecepientsEmail').html(EMAIL.createEmailOptions(el.closest('tr').find('td:nth-child(9)').html()));
                            $('.modal_email #RecepientsEmail').select2();
                            $('#sendSubject').val('Everest Academy Manila Admissions Inquiry');
                            $('.modal_email').modal('show');
                        }
                        $('.filter').find('#btnFilter').trigger('click');
                    } else {
                        msgbox('error',res.message);
                    }
                    progressBar('off');
                    onProcess(el,'','Loading',false);
                },
                function(error) {
                    onProcess(el,'','Loading',false);
                    msgbox('error','There was an error occured. Please try again!');
                    progressBar('off');
                }
            );
        });

    }

    var jqueryInit = function() {
        $(document).on('click','.btn-action', function() {

            updateType($(this), $(this).attr('data-id'),getType($(this)));

        });
    }

    function getType(el) {
        return el.closest('tr').find('.list-type option:selected').val();
    }

    
    return {
        init: function() {
            dataTableLogs();
            jqueryInit();

            $('body').on('click','#btnFilter',function(){
                dataTableLogs();
            });

            $('body').on('change','#schoolYear, #gradeLevel, #quarter',function(){
                $('.filter').find('#btnFilter').trigger('click');
            });

            $('body').on('click','.btn_send_email',function(){
                EMAIL.send();   
            });

             $('body').on('change','.list-type',function(){
                $(this).closest('tr').find('.btn-action').trigger('click');
            });
        }
    }
}();