var TourKey = '';
var Tour = function() {

	function autoload() {
		dateTime();

		get();
	}

	function get() {
		setAjaxRequest(
			base_url+page_url+'get',
			'',
			function(res) {
				$('#table-wrapper').html(res.result);
			},
			function(error) {
			},
			'json',true, false
		);
	}

	function save() {
		if (!isFormValid('#FormTour')) {
			return msgbox('error','Please complete all fields!');
		}
		confirmEvent(
			'Are you sure you want to '+(TourKey ? 'update' : 'save')+' this?',
			function() {
				setAjaxRequest(
					base_url+page_url+'event?event='+(TourKey ? 'update' : 'save')+'&id='+TourKey,
					$('#FormTour').serialize(),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							clear();
							get();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			}
		);
	}

	function edit(id) {
		setAjaxRequest(
					base_url+page_url+'event?event=edit',
					'id='+id,
					function(res) {
						var res = res.data;
						$('#schedule_time').val(res.time);
						$('#schedule_date').val(res.date);
						$('#remarks').val(res.remarks);
						$('#schedule_status').val(res.status);
						$('#schedule_datetime').val(res.date+' '+res.time);
						TourKey = id;
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
	}

	function deletePanel(id) {
		confirmEvent(
			'Are you sure you want to delete this?',
			function() {
				setAjaxRequest(
					base_url+page_url+'event?event=delete&id='+id,
					$('#FormTour').serialize(),
					function(res) {
						if (res.error == false) {
							msgbox('success',res.message);
							clear();
							get();
						} else {
							msgbox('error',res.message);
						}
						progressBar('off');
					},
					function(error) {
						msgbox('error','There was an error occured. Please try again!');
						progressBar('off');
					}
				);
			}
		);
	}

	function clear() {
		$("#panel-user").val('').trigger('change');
		$('#FormTour input[type="text"],#FormTour select, #FormTour textarea').val('');
		TourKey = '';
	}

	function dateTime() {
		$('#schedule_time').timepicker();
	}

	return {
		init: function() {
			autoload();
		},

		save: function() {
			save();
		},

		edit: function(id) {
			edit(id);
		},

		delete: function(id) {
			deletePanel(id);
		},

		cancel: function() {
			clear();
		}
	}
}();
