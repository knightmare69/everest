var current_step = 1;
var current_term = 0;
var department   = 'gs'; 

function init(){
  department = $('#department').val();
  if(department=='gs'){
    $('.dvapptype').addClass('hidden');
    $('.dvlevel').removeClass('hidden');
	$('#GradeLevelID').html(opt_gs);
	$('.firstchoice').addClass('hidden');
	$('.secondchoice').addClass('hidden');
  }else if(department=='jhs'){
    $('.dvapptype').addClass('hidden');
    $('.dvlevel').removeClass('hidden');
	$('#GradeLevelID').html(opt_jhs);
	$('.firstchoice').addClass('hidden');
	$('.secondchoice').addClass('hidden');
  }else if(department=='shs'){
    $('.dvapptype').addClass('hidden');
    $('.dvlevel').removeClass('hidden');
	$('#GradeLevelID').html(opt_shs);
	$('.firstchoice').removeClass('hidden');
	$('.secondchoice').addClass('hidden');
  }else if(department=='college'){
    $('.dvapptype').removeClass('hidden');
    $('.dvlevel').addClass('hidden');
	$('#GradeLevelID').html(opt_col);
	$('.firstchoice').removeClass('hidden');
	$('.secondchoice').removeClass('hidden');
  }else if(department=='graduate'){
    $('.dvapptype').removeClass('hidden');
    $('.dvlevel').addClass('hidden');
	$('#GradeLevelID').html(opt_grd);
	$('.firstchoice').removeClass('hidden');
	$('.secondchoice').addClass('hidden');
  }
}

function fn_next(){
   var next_step = current_step+1;
   if(current_step==1){
   
   }
   
   $('.div'+current_step).addClass('hidden');
   $('.div'+next_step).removeClass('hidden');
   $('.btn-prev').removeClass('disabled');
   current_step++;
}

function fn_prev(){
   var next_step = current_step-1;
   if(current_step==1){
   
   }
   
   if(next_step==1){
   $('.btn-prev').addClass('disabled');
   }
   
   $('.div'+current_step).addClass('hidden');
   $('.div'+next_step).removeClass('hidden');
   
   current_step--;
}

$('document').ready(function(){
   $('body').on('click','.btn-next',function(){
	  fn_next();
   });
   
   $('body').on('click','.btn-prev',function(){
	  fn_prev();
   });
   
   $('body').on('change',"#department",function(){
      init();
   });
   
   init();
});