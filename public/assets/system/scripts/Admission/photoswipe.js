var openPhotoSwipe = function(el,index) {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // build items array
    var items = new Array();
    var data = ajaxRequest(base_url+page_url+'event','event=getDocsPath&AppNo='+getParameterByName('AppNo'));

    for(var i in data) {
        items.push({
            src: ajaxRequest(base_url+page_url+'event','event=viewDocs&FamilyID='+data[i].FamilyID+'&key='+data[i].key,'HTML').trim(),
            w: data[i].w,
            h: data[i].h,
            key: data[i].key,
            title: data[i].desc,
            remarks: data[i].remarks
        });
    }

    var options = {
        // index: index,
        history: false,
        focus: false,
        closeOnScroll : false,
        mouseUsed: false,
        tapToClose: false,
        isMouseClick: false,
        isClickableElement : function() {
            console.log('Yes');
        },
        addCaptionHTMLFn: function(item, captionEl, isFake) {
            // item      - slide object
            // captionEl - caption DOM element
            // isFake    - true when content is added to fake caption container
            //             (used to get size of next or previous caption)

            if(!item.title) {
                captionEl.children[0].innerHTML = '';
                return false;
            }            
            
            captionEl.children[0].innerHTML = item.title + '<br />'+
                '<textarea style="margin-top: 3% !important;z-index: 99999 !important;" class="form-control TextRemarks" placeholder="Right Click to add remarks."></textarea>'+
                '<button type="button" data-id="'+item.key+'" style="margin-top: 3% !important;z-index: 99999 !important;text-align: right !important;" class="BtnSaveDocRemarks btn btn-primary"><i class="fa fa-save"></i></button>'+
                '<button type="button" data-id="'+item.key+'" style="margin-top: 3% !important;margin-left: 2%;z-index: 99999 !important;text-align: right !important;" class="BtnShowDocRemarks btn btn-success"><i class="glyphicon glyphicon-leaf"></i></button>'+
                '<button type="button" data-id="'+item.key+'" style="margin-top: 3% !important;margin-left: 2% !important;z-index: 99999 !important;text-align: right !important;" class="btnDownloadDoc btn btn-primary"><i class="fa fa-download"></i></button>';
            return true;
        },

        showAnimationDuration: 0,
        hideAnimationDuration: 0
        
    };
    
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};
