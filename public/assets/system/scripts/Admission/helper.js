function setSessionStep() {
	setAjaxRequest(base_url+page_url+'event','event=getCurrentStep',
		function(step) {
			setWizzardSteps(step);
		},'','JSON',true,false
	);
}

function toggleStepButton(isAjax) {
	var isAjax = isAjax == undefined ? true : isAjax;
	if (isAjax) {
		setAjaxRequest(base_url+page_url+'event','event=getCurrentStep',
			function(step) {
				setProgressButtons(step);
			},'','JSON',true,false
		);
	} else {
		setProgressButtons(getCurrentStep());
	}
}

function setPreviousStep() {
	var step = getCurrentStep() - 1;
	addHrefIfFormComplete();
	setWizzardSteps(step);	
	setProgressButtons(step);
	setSessionCurrStep(step);
	checkFormValidation();

}

function setProgressStep(step) {
	var step = step == undefined ? getCurrentStep() : step;
	var nextStep = step + 1;

	if (isLastStep()) return;

	// if ((nextStep) == getStepIndex('review')) {
	// 	review();
	// }

	// console.log(((nextStep) +'=='+ getStepIndex('documents')));
	// if ((nextStep) == getStepIndex('documents')) {
	// 	if (!isDocumentHasSameAttr(
	// 			getSelectedVal('#applicationType'),
	// 			getSelectedVal('#Nationality'),
	// 			getSelectedVal('#gradeLevel'),
	// 			getInputVal('#gradeProgramClass')
	// 		)
	// 	) {
	// 		documents(true);
	// 	}
	// }

	if(ADMISSION_HELPER.isFormValid(step,true)) {
		setWizzardSteps(nextStep);
		setSessionCurrStep(nextStep);
	} else {
		nextStep = step;
		msgbox('error','Please complete the required information.');
	}

	//addHrefIfFormComplete();
	setProgressButtons(nextStep);
	checkFormValidation();
}

function isDocumentHasSameAttr(AppType,Nationality,YearLevel,ProgClass) {
	var table = $('#documents #tableDocuments');
	if (
		table.attr('data-AppType') == AppType &&
		table.attr('data-nationality') == Nationality && 
		table.attr('data-YearLevel') == YearLevel && 
		table.attr('data-ProgClass') == ProgClass &&
		table.find('tbody tr').length > 0
	) {
		return true;
	}
	return false;
}

function setDateFormat(name) {
	var name = name == undefined ? ".date-picker" : name;
	$(name).inputmask("m/d/y", {
        "placeholder": "mm/dd/yyyy"
    }); 
}

function getMiddleInitial(value) {
	var value = value.split(' ');
	var str = '';
	for(var i = 0; i<value.length; i++) {
		str +=  value[i].substr(0,1).toUpperCase();
		
	}
	return str+'.';
}

function setGradeLevelSel(value,ProgClass) {
	$('#SetGradeLevel').val(value);
	$('#SetProgClass').val(ProgClass);   
}


function isHigherLevel() {
	return $('#IsHigherLevel').val();
}

function enableSchoolAttended(showError) {	
	if( $('#gradeLevel option:selected').attr('data-isGradeLevel') == 1) {
		setInputRequired('#formStudent','.ES');
		removeInputRequired('#formStudent','.HS');
	} else {
		setInputRequired('#formStudent','.HS');
		setInputRequired('#formStudent','.ES');
	}
}

function validateFormLoginAccount(HasAlreadyAccount) {

	if (HasAlreadyAccount == undefined) {
		if (!getInputValByName('#user-account #formLoginAccount','IsAccountEdit') && (isPasswordMatch() && !isPasswordEmpty())) {
 			HasAlreadyAccount = 0;
		} else {
			HasAlreadyAccount = 1;
		}
	}
	
	if (HasAlreadyAccount == 1 && $('#IsValidateLoginAccount').val() != '1') {
		$('#formLoginAccount #password').attr('readonly','readonly');
		$('#formLoginAccount #rpassword').attr('readonly','readonly');
		$('#formLoginAccount #username').attr('readonly','readonly');
		$('#formLoginAccount #username').addClass('not-required');
		$('#formLoginAccount #password').addClass('not-required');
		$('#formLoginAccount #rpassword').addClass('not-required');
		$('#formLoginAccount #IsLoginValidated').val(1);
	} else {
		$('#formLoginAccount #password').removeAttr('readonly');
		$('#formLoginAccount #rpassword').removeAttr('readonly');
		$('#formLoginAccount #username').removeAttr('readonly');
		$('#formLoginAccount #password').removeClass('not-required');
		$('#formLoginAccount #rpassword').removeClass('not-required');
		$('#formLoginAccount #username').removeClass('not-required');
		$('#formLoginAccount #IsLoginValidated').val(0);
	}

	$('#user-account msg').html('');
	isFormValid('#user-account #formLoginAccount');
}

function clearFamilyBG() {
	clear_value('#parents #formGuardian');
	clear_value('#parents #formFather');
	clear_value('#parents #formMother');
	clear_value('#user-account #formLoginAccount');
	$('#parents #GuardianPhotoThumbnail').attr('src',base_url+'general/getGuardianPhoto?FamilyID=');
	validateFormLoginAccount();
}

function isPasswordMatch() {
	if (
		getInputValByName('#user-account #formLoginAccount','password') != getInputValByName('#user-account #formLoginAccount','rpassword')
	)
		return false;

	return true;
}

function isPasswordEmpty() {
	if(
		!getInputValByName('#user-account #formLoginAccount','password') || !getInputValByName('#user-account #formLoginAccount','rpassword')
	)
		return true;

	return false;
}

function checkFormValidation() {
	setTimeout(function() {
		ADMISSION_HELPER.isFormValid(getCurrentStep());
	},2000);
}

function SetManageAccount() {
	if ($('#IsManageAccount').is(':checked')) {
		$('#username,#password,#rpassword').prop('disabled',false);
		$('#username,#password,#rpassword').removeAttr('readonly');
		$('#IsValidateLoginAccount').val(1);
	} else {
		$('#username,#password,#rpassword').prop('disabled',true);
		$('#username,#password,#rpassword').attr('readonly','readonly');
		$('#IsValidateLoginAccount').val(0);
	}
}

function showPleaseWait() {
    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-header" style="border-bottom:unset;padding: 10px;">\
                    <button type="button" class="close hide" data-dismiss="modal" aria-hidden="true">×</button>\
                    <h5 class="modal-title" style="font-weight: bold;">&nbsp;&nbsp;Saving your data, please wait...</h5>\
                </div>\
                <div class="modal-body" style="padding-top: unset;">\
                    <div class="progress" style="height: 10px !important;margin-bottom: auto;">\
                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                      </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $("#pleaseWaitDialog").modal("show");
}

function hidePleaseWait() {
    $('#pleaseWaitDialog .close').trigger('click');
}

function ajaxRequestLoader(url,data,successFunc,errorFunc){
    $('.bootbox-confirm').modal('hide');
    showPleaseWait();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: url,
        dataType: 'json',
        data:data,
        type: 'POST',
        async: true,
        success: (
            function(data) {
                if (successFunc != undefined && typeof(successFunc) == 'function'){
                    successFunc(data);
                }
            }),
        error: function(xhr,status,error){
            if (errorFunc != undefined){
                errorFunc(data);
            }
        }
    });
}
