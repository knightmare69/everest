var global_step = 1, examSchedule = '', documents;
var ADMISSION = function() {

	var addNewSibling = function() {
		var defaultRow = $('#tableDefaultSibling tbody').html();
		$('#tableSiblings tbody').append(defaultRow);
		FN.datePicker('.siblingDOB');
		setDateFormat('.siblingDOB');
	}

	var removeSibling = function(el) {
		el.closest('tr').remove();
	}

	// var setThumbnail = function() {
	// 	// console.log($('#formStudent #StudentPhoto')[0].files[0]);
	// 	// $('#formStudent #StudentPhotoThumbnail').attr('src',$('#formStudent #StudentPhoto').val());
	// }

	// var getSelectedVal = function(objName) {
	// 	return $(objName).find('option:selected').val();
	// }

	//var gradeLevel = function(el) {
		// var isProcess = isProcess == undefined ? true : isProcess;
		// removeInputRequired('#formStudent','.HS');
		// removeInputRequired('#formStudent','.ES');
		//ADMISSION_DATA.showProgram(el.find('option:selected').val());
	//};

	var loadScholasticByYrLvl = function() {
		var apptype = $('#applicationType').val(), grdlvl = $('#gradeLevel').find('option:selected').val(),prgid = $('#gradeLevel').find('option:selected').attr('data-prog');
		if(apptype != 0 && grdlvl != '') {
			if(window.atob(apptype) == 1) {
				if(window.atob(grdlvl) <= 2 && window.atob(prgid)==1){
					jQuery('.sch_wrapper').html(ajaxRequest(base_url+page_url+'event','event=byYrLvlScholastic&yrlvl=kinder-grade1','HTML'));
					Metronic.initUniform();
				}else{
					jQuery('.sch_wrapper').html(ajaxRequest(base_url+page_url+'event','event=byYrLvlScholastic&yrlvl=grade2-grade11','HTML'));
				}
				// removeInputRequired('#formStudent','.returnFields');
				$('#formStudent .notreturnFields').each(function() {
			        if (!$(this).hasClass('not-required-default')) {
			        	if($(this).hasClass('religionFields')) {
			        		if($('#isBaptized').is(":checked")) {
			        			$(this).removeClass('not-required');
			        		}
			        	} else {
			        		$(this).removeClass('not-required');
			        	}
			        } 
			    });
				$('.rtn').addClass('hidden');
				$('.not-rtn').removeClass('hidden');
			} else {
				jQuery('.sch_wrapper').html(ajaxRequest(base_url+page_url+'event','event=byYrLvlScholastic&yrlvl=returning','HTML'));
				// setInputRequired('#formStudent','.returnFields');
				removeInputRequired('#formStudent','.notreturnFields');
				$('.rtn').removeClass('hidden');
				$('.not-rtn').addClass('hidden');
			}

			isFormValid('#formStudent',true);
		}
	};

	var setForeignFields = function() {
		if ($('#Nationality option:selected').attr('data-isforeign') == 1) {
			$('.foreignFields').removeClass('not-required');
			$('#isForeign').val(1);
			$('.foreign').removeClass('hidden');
		} else {
			$('.foreignFields').addClass('not-required');
			$('#isForeign').val(0);
			$('.foreign').addClass('hidden');
		}
		// isFormValid('#formStudent');
	};

	var editStep = function(el) {
		var step = parseInt(el.attr('data-step'));

		$('#adminssionSteps li').removeClass('active');
		$('#adminssionSteps > li.step-'+step).addClass('active');

		$('.mainTabContent > .tab-pane').removeClass('active');
		$('.mainTabContent > .step-'+step).addClass('active');

		setSessionCurrStep(step);
	};

	var onLoad = function() {	
		ADMISSION_DATA.getYearLevel();
		setTimeout(function(){
			$('#gradeLevel').trigger('change');
		},0);
		// gradeLevel($('#gradeLevel'));
		// loadScholasticByYrLvl();
		// enableSchoolAttended(false);
		generateUlSteps();
		//set ajax current session step
		//setSessionStep();
		//toggle steps button
		//toggleStepButton();
		// setThumbnail();
		setDateFormat();
		review();
		// documents(true);
		ADMISSION_DATA.countryIcons();
		ADMISSION_DATA.countryIcons('#BillingCountry');
		
		ADMISSION_DATA.getCity($('#GuardianCountry option:selected').val());
		ADMISSION_DATA.getCity($('#BillingCountry option:selected').val(),'#BillingCity');
		ADMISSION_DATA.toggleOtherCity($('#isOtherCity').is(':checked'));
		ADMISSION_DATA.toggleBillingOtherCity($('#BillingIsOtherCity').is(':checked'));

		// College.init();
		// SetManageAccount();
		checkFormValidation();
		$(window).scrollTop(0);
	};


	var showFamilyModalSearch = function(el) {
		var result = ajaxRequest(base_url+page_url+'event','event=showModalFamilSearch');
		if (result.error == true) {
			return msgbox('error',result.message);
		}
		showModal({
			name: 'basic',
            title : 'Search Family Background',
            content: result.content,
            class: 'modal_family_background',
            button: { 
                icon: 'fa fa-search',
                class: 'btn_sel_family',
                caption: 'Select'
            },
            hasFooter: false,
        });
        FN.datePicker('body #tableFamilySearc #FamilyDOB');
	};

	// var enableSchoolAttended = function(showError) {
	// 	if( $('#gradeLevel option:selected').attr('data-isGradeLevel') == 1) {
	// 		setInputRequired('#formStudent','.ES');
	// 		removeInputRequired('#formStudent','.HS');
	// 	} else {
	// 		setInputRequired('#formStudent','.HS');
	// 		setInputRequired('#formStudent','.ES');
	// 	}
	// 	isFormValid('#formStudent',showError);
	// };

	var searchFamily = function(el) {
		if (!getInputVal('#FamilyFilter'))  {
			return msgbox('error','Please input Filter.');
		}
		setAjaxRequest(base_url+page_url+'event','event=searchFamily&filter='+
			getInputVal('#FamilyFilter')
			,function(result) {
				if (result.error == true) {
					return msgbox('error',result.message);
				} else {
					$('body .modal_family_background .modal-body').html(result.content);
				}
				progressBar('off');
			}
		);
	};

	documents = function(async) {
		var async = async == undefined ? false : async;
		var bar = async ? false : true;
		
		console.log(
			$('#Nationality option:selected').val()+','+
			$('#gradeLevel option:selected').val()+','+
			$('#gradeProgramClass').val()
			);
		if ($('#Nationality option:selected').val() == ''
			|| $('#gradeLevel option:selected').val() == ''
			|| $('#gradeProgramClass').val() == ''
			) {
			return false;
		}
		setAjaxRequest(base_url+page_url+'event','event=getRequiredDocs&AppID='
			+$('#applicationType option:selected').val()
			+'&IsForeign='+$('#Nationality option:selected').attr('data-isforeign') 
			+'&ProgClass='+$('#gradeProgramClass').val()
			+'&YearLevelID='+$('#gradeLevel option:selected').val(),
			function(result) {
				var table = $('#tableDocuments'), hasData = false;
				table.find('tbody tr').remove();
				for(var i in result) {
					table.find('tbody').append(
						"<tr data-tempID='"+result[i].TemplateID+"' data-id='"+result[i].DetailID+"' data-isRequired='"+(result[i].IsRequired)+"'  data-doc='"+result[i].RequirementID+"'>"
						+"<td>"+result[i].DocDesc+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsReviewed'"+(result[i].IsReviewed == '1' ? 'checked' : '')+">" : (result[i].IsReviewed ? 'Yes' : 'No') )+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsExempted'"+(result[i].IsExempted == '1' ? 'checked' : '')+">" : (result[i].IsExempted ? 'Yes' : 'No') )+"</td>"
						+"<td class='center'>"+(result[i].IsRequired == '1' ? 'Yes' : 'No')+"</td>"
						+'<td class="center">'
							+'<div class="fileUpload btn '+(result[i].EntryID == '1' ? 'btn-success' : (result[i].IsRequired == '1' ? 'btn-danger' : 'bg-grey-cascade'))+'">'
				                +'<span><i class="fa fa-file"></i></span>'
				                +'<input type="file" class="upload" />'
				            +'</div>'
				         +'</td>'
						+"</tr>"
					);
					hasData = true;
				}
				if (hasData) {
					table.attr('data-nationality',$('#Nationality option:selected').val());
					table.attr('data-YearLevel',$('#gradeLevel option:selected').val());
					table.attr('data-AppType',$('#applicationType option:selected').val());
					table.attr('data-ProgClass',$('#gradeProgramClass').val());
				}
				progressBar('off');
			},'','JSON',async,bar
		);
	};

	examSchedule = function() {
		setAjaxRequest(base_url+page_url+'event','event=ShowExamScheduleDates'
			+'&campus='+$('#schoolCampus option:selected').val()
			+'&term='+$('#schoolYear option:selected').val()
			+'&gradeLevel='+$('#gradeLevel option:selected').val(),
			function(result) {
				if (result.error == false) {
					$('#ScheduleDate').remove('option');
					var options = '<option value="">Select...</option>';
					$('#ScheduleDate').removeClass('not-required');
					if (result.data.length <= 0) {
						options = '<option value="">None</option>';	
						$('#ScheduleDate').addClass('not-required');
					}
					for(var i in result.data) {
						options += '<option value="'+result.data[i].SchedID+'">'+result.data[i].SchedDate+'</option>';
					}
					$('#ScheduleDate').html(options);
					if($('#ScheduleDate option:selected').val() == '') {
						$('#ScheduleDate').val($('#ScheduleDateID').val());
					}
				}
			},
			function(error) {
					$('#ScheduleDate').addClass('not-required');
					$('#ORCode').addClass('not-required');
					$('#ORSecurityCode').addClass('not-required');
			},
			'JSON',
			true,
			false
		);
	};

	var selFamily = function(el) {
		$('#FamilyID').val(el.closest('tr').attr('data-id'));
		setAjaxRequest(base_url+page_url+'event','event=getFamilyBG&key='+el.closest('tr').attr('data-id')
			, function(result) {
				if (result.error == true) {
					return msgbox('error',result.message);
				} else {
					var data = result.data
					setInputValByName('#formGuardian','name',data.GuardianName);
					// setInputValByName('#formGuardian','marital',data.Guardian_Name);
					setInputValByName('#formGuardian','livingWith',data.Guardian_LivingWith);
					setSelectedValByName('#formGuardian','marital',data.ParentMaritalID);
					setInputValByName('#formGuardian','livingWithOther',data.RelationshipOthers);
					setInputValByName('#formGuardian','GuardianDateOfBirth',setDateFormatTo(data.GDOB,'yyyy-mm-dd','mm/dd/yyyy'));
					// setInputValByName('#resident','name',data.Guardian_Name);
					setInputValByName('#formGuardian','street',data.Street);
					setInputValByName('#formGuardian','resident',data.Address);
					setInputValByName('#formGuardian','barangay',data.Barangay);
					setInputValByName('#formGuardian','GuardianCitySel',data.CityID);
					setInputValByName('#formGuardian','telephone',data.TelNo);
					setInputValByName('#formGuardian','mobile',data.Mobile);
					setInputValByName('#formGuardian','email',data.Email);
					setSelect2ValByName('#formGuardian','GuardianCountry',data.GuardianCountryCode);
					setSelect2ValByName('#formGuardian','GuardianCity',data.TownCity);
					//setSelect2ValByName('#formGuardian','city',data.TownCity);

					setInputValByName('#formGuardian','BillingStreet',data.BillingStreet);
					setInputValByName('#formGuardian','BillingResident',data.BillingAddress);
					setInputValByName('#formGuardian','BillingBrangay',data.BillingBarangay);
					setInputValByName('#formGuardian','BillingCitySel',data.BillingCityID);
					setSelect2ValByName('#formGuardian','BillingCountry',data.GuardianBillingCode);
					setSelect2ValByName('#formGuardian','BillingCity',data.BillingTownCity);

					setInputValByName('#formFather','FatherName',data.FName);
					setInputValByName('#formFather','FatherDateOfBirth',setDateFormatTo(data.FDOB,'yyyy-mm-dd','mm/dd/yyyy'));
					setInputValByName('#formFather','FatherCompany',data.FCompany);
					setInputValByName('#formFather','FatherOccupation',data.FOccupation);
					setSelectedValByName('#formFather','FatherMarital',data.FMaritalID);
					setInputValByName('#formFather','FatherBusinessType',data.FBusinessType);
					setInputValByName('#formFather','FatherAddress',data.FAddress);
					setInputValByName('#formFather','FatherCompanyAddress',data.FCompanyAddress);
					setInputValByName('#formFather','FatherCompanyPhone',data.FCompanyPhone);
					setInputValByName('#formFather','FatherTelNo',data.FTelNo);
					setInputValByName('#formFather','FatherEmailAddress',data.FEmail);
					setInputValByName('#formFather','FatherMobile',data.FMobile);
					setInputValByName('#formFather','FatherEducation',data.FEducAttainment);
					$('#formFather #s2id_FatherReligion').val(data.FReligion).trigger('change');
					$('#formFather #s2id_FatherNationality').val(data.FNationality).trigger('change');
					$('#formFather #s2id_FatherCitizenship').val(data.FCitizenship).trigger('change');
					setInputValByName('#formFather','FatherSchool',data.FSchoolAttended);

					setInputValByName('#formMother','MotherName',data.MName);
					setInputValByName('#formMother','MotherDateOfBirth',setDateFormatTo(data.MDOB,'yyyy-mm-dd','mm/dd/yyyy'));
					setInputValByName('#formMother','MotherCompany',data.MCompany);
					setInputValByName('#formMother','MotherOccupation',data.MOccupation);
					setSelectedValByName('#formMother','MotherMarital',data.MMaritalID);
					setInputValByName('#formMother','MotherBusinessType',data.MBusinessType);
					setInputValByName('#formMother','MotherAddress',data.MAddress);
					setInputValByName('#formMother','MotherCompanyAddress',data.MCompanyAddress);
					setInputValByName('#formMother','MotherCompanyPhone',data.MCompanyPhone);
					setInputValByName('#formMother','MotherTelNo',data.MTelNo);
					setInputValByName('#formMother','MotherEmailAddress',data.MEmail);
					setInputValByName('#formMother','MotherMobile',data.MMobile);
					setInputValByName('#formMother','MotherEducation',data.MEducAttainment);
					$('#formMother #s2id_MotherReligion').val(data.MReligion).trigger('change');
					$('#formMother #s2id_MotherNationality').val(data.MNationality).trigger('change');
					$('#formMother #s2id_MotherCitizenship').val(data.MCitizenship).trigger('change');
					setInputValByName('#formMother','MotherSchool',data.MSchoolAttended);

					setInputValByName('#formMother','DateOfMarriage',setDateFormatTo(data.MarriageDate,'yyyy-mm-dd','mm/dd/yyyy'));
					setInputValByName('#formMother','EmergencyContactName',data.ECName);
					setInputValByName('#formMother','EmergencyContactRelationship',data.ECRelationship);
					setInputValByName('#formMother','EmergencyContactAddress',data.ECAddress);
					setInputValByName('#formMother','EmergencyContactTelNo',data.ECTelNo);
					setInputValByName('#formMother','EmergencyContactEmail',data.ECEmail);
					setInputValByName('#formMother','EmergencyContactMobile',data.ECMobile);
					setInputValByName('#formMother','EmergencyContactCompanyPhone',data.ECCompanyPhone);

					setInputValByName('#formLoginAccount','username',data.LoginAccount.UserName);
					setInputValByName('#formLoginAccount','LoginID',data.LoginAccount.UserID);
					setInputValByName('#formLoginAccount','IsLoginValidated',data.HasAlreadyAccount ? 1 : 0);
					setInputValByName('#formLoginAccount','IsAccountEdit',data.HasAlreadyAccount ? 1 : 0);
					
					$('#user-account #IsManageAccount').prop('checked',data.HasAlreadyAccount ? false : true);
					$('#user-account #IsEmailSend').prop('checked',data.HasAlreadyAccount ? false : true);

					SetManageAccount();
					
					isFormValid('#parents #formFather');
					isFormValid('#parents #formMother');
					isFormValid('#parents #formGuardian');
					

					getProperty('#GuardianPhotoThumbnail').attr('src',base_url+'general/getGuardianPhoto?FamilyID='+el.closest('tr').attr('data-id'));
				}
				progressBar('off');
			}
		);
		$('.btnFamilySel').removeClass('btn-success');
		$('.btnFamilySel').addClass('default');
		el.removeClass('default');
		el.addClass('btn-success');
	};


	var listenEvent = function() {
		var myEvent = window.attachEvent || window.addEventListener;
	    var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compatable

	    myEvent(chkevent, function(e) { // For >=IE7, Chrome, Firefox
	        var confirmationMessage = ' ';  // a space
	        (e || window.event).returnValue = confirmationMessage;
	        return confirmationMessage;
	    });
	};

	var copyAdddress = function() {
		var form = '#formGuardian';

		var resident = $(form).find('input[name="resident"]').val();
		var street = $(form).find('input[name="street"]').val();
		var barangay = $(form).find('input[name="barangay"]').val();
		var GuardianCountry = $(form +' #GuardianCountry option:selected').val();
		var isOtherCity = $(form).find('input[name="isOtherCity"]').is(':checked') ? 1 : 0;
		var otherCity = $(form).find('input[name="otherCity"]').val();
		var GuardianCity = $(form).find('#GuardianCity option:selected').val();

		$(form).find('input[name="BillingResident"]').val(resident);
		$(form).find('input[name="BillingStreet"]').val(street);
		$(form).find('input[name="BillingBrangay"]').val(barangay);
		$(form).find('input[name="BillingGuardianCountry"]').val(GuardianCountry);
		$(form).find('input[name="BillingOtherCity"]').val(otherCity);

		$('#BillingIsOtherCity').prop('checked',isOtherCity);
		ADMISSION_DATA.toggleBillingOtherCity(isOtherCity);

		$('#BillingCountry').select2('val',[GuardianCountry]);
		$('#BillingCountry').trigger('change');

		$('#BillingCity').select2('val',[GuardianCity]);
		$('#BillingCity').trigger('change');

		$(':checkbox').uniform();
	};

	var AddSchoolAttended = function() {
		var row = $('#tableSchoolsAttended > tbody > tr:first-child');
		$('#tableSchoolsAttended').append('<tr>'+row.html()+'</tr>');
		$('#tableSchoolsAttended > tbody > tr:last-child')
			.find('input').val('');
		$('#tableSchoolsAttended > tbody > tr:last-child')
			.find('select').val('');
	}

	var removeSchoolAttended = function(el) {
		confirmEvent("Are you sure you want to remove this?",
			function(yes) {
				if (!yes) return;
				if (el.closest('tbody').find('tr').length  == 1) {
					el.closest('tr')	
						.find('input').val('');
					el.closest('tr')	
						.find('select').val('');
				} else {
					el.closest('tr')
						.remove();	
				}
			}
		);	
	}

	var addNewReference = function() {
		var defaultRow = $('#tableDefaultReferences tbody').html();
		$('#tableReferences tbody').append(defaultRow);
	}

	var removeReference = function(el) {
		el.closest('tr').remove();
	}

	return {
		init: function() {
			listenEvent();	
			onLoad();

			$('body').on('click','#btnNext',function() {
				review();
				setProgressStep(getCurrentStep());
				$(window).scrollTop(0);
			});

			$('body').on('click','#btnPrev',function() {
				review();
				setPreviousStep();
				$(window).scrollTop(0);
			});

			$('body').on('click','#btnAddSibling',function() {
				addNewSibling($(this));
			});

			$('body').on('click','.btnSiblingRemove',function() {
				removeSibling($(this));
			});

			$('body').on('change','#gradeLevel',function() {
				
                var data  = $(this).find('option:selected');
                //gradeLevel($(this));
                loadScholasticByYrLvl();
				ADMISSION_DATA.showProgram(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
				// documents(true);
				// enableSchoolAttended();
				// examSchedule();
				
				setGradeLevelSel(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);                        	
                                
                $('.strands').hide();
                if( data.attr('data-progclass') == '21'){ $('.strands').show();}                			
                $('#gradeProgramID').val(data.attr('data-prog'));
            
			});

			$('body').on('change','#applicationType',function() {
				loadScholasticByYrLvl();
			});
            
            $('body').on('change','#gradeMajorID',function() {
                var data  = $('#gradeLevel').find('option:selected');
                $('#gradeProgramID').val(data.attr('data-prog'));
                $('#SetProgClass').val(data.attr('data-progclass'))
            }); 

			$('body').on('click','.a-adminssionSteps',function() {
				if (is_parent == '1') return false;
				review();
				if (getStepIndex('complete') == $(this).closest('li').attr('data-step')) {
					return false;
				} else {
					var step = $(this).closest('li').attr('data-step');
					if ((step) == getStepIndex('review')) {
						progressBar('on');
						review();
						progressBar('off');
					}
					setSessionCurrStep(step,true);
					showCompleteButton($(this));
				}
			});	

			$('body').on('click','.btn-edit',function() {
				progressBar('on');
				editStep($(this));
				showCompleteButton($(this));
				progressBar('off');
			});

			$('body').on('click','#btnSubmitTemp',function() {
				if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
				confirmEvent("Are you sure you want to save this?",function(yes) {
					if (!yes) return
					// var result = ADMISSION_DATA.admitTemp($(this),1);
					//     is_parent = (($('#complete').is('.step-6'))?1:is_parent);
					// if (result.error != undefined && result.error == false) {
					// 	ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
					// 	ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
					// 	setWizzardSteps(is_parent ? 6 : 7);
					// 	$('.step-'+(is_parent ? 6 : 7)).addClass('active');
					// 	$('.complete-form').html(result.message);
					// 	hideButtons();
					// 	setSessionCurrStep(1);
					// 	clearSteps();
					// 	hideError();
					// 	hideSuccess();
					// 	$('.complete-form').html(result.message);
					// } else {
					// 	var msg = result.message != !undefined ? result.message : 'There was an error while saving';
					// 	msgbox('error',msg);
					// }
					
					ajaxRequestLoader(base_url+page_url+'event',ADMISSION_DATA.admitData($(this),1), function(success_data)
                    {
                        if ( success_data.error != undefined && success_data.error == false ) 
                        {
                            ADMISSION_DATA.savePhoto(success_data.AppNo,success_data.FamilyID);
							ADMISSION_DATA.saveDocuments(success_data.AppNo,success_data.FamilyID);
							setWizzardSteps(7);
							$('.step-7').addClass('active');
							$('.complete-form').html(success_data.message);
							hideButtons();
							setSessionCurrStep(1);
							clearSteps();
							hideError();
							hideSuccess();
                        }
                        hidePleaseWait();

                    },function(error_data){
                        hidePleaseWait();
                        msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
                    });

				});
			});

			$('body').on('click','#btnSubmit',function() {
				if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
				confirmEvent("Are you sure you want to submit this?",function(yes) {
					if (!yes) return
					// var result    = ADMISSION_DATA.admitTemp($(this),2);
					//     is_parent = (($('#complete').is('.step-6'))?1:is_parent);
					// if (result.error != undefined && result.error == false) {
					// 	ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
					// 	ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
					// 	hideButtons();
					// 	hideError();
					// 	setWizzardSteps(is_parent ? 6 : 7);
					// 	$('.step-'+(is_parent ? 6 : 7)).addClass('active');
					// 	$('.complete-form').html(result.message);
					// } else {
					// 	var msg = result.message != !undefined ? result.message : 'There was an error while saving';
					// 	msgbox('error',msg);
					// }

					ajaxRequestLoader(base_url+page_url+'event',ADMISSION_DATA.admitData($(this),2), function(success_data)
                    {
                        if ( success_data.error != undefined &&  success_data.error == false ) 
                        {
                            ADMISSION_DATA.savePhoto(success_data.AppNo,success_data.FamilyID);
							ADMISSION_DATA.saveDocuments(success_data.AppNo,success_data.FamilyID);
							hideButtons();
							hideError();
							setWizzardSteps(7);
							$('.step-7').addClass('active');
							$('.complete-form').html(success_data.message);
                        }
                        hidePleaseWait();

                    },function(error_data){
                        hidePleaseWait();
                        msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
                    });

				});
			});

			$('body').on('click','#btnNewApp',function() {
				setWizzardSteps(1);
				setSessionCurrStep(1);
				location.reload();
			});

			$('body').on('change','#Nationality',function() {
				setForeignFields();
				// documents(true);
			});

			$('body').on('bind keyup change keypress','#DateOfBirth',function() {
				$('#StudentAge').text(getAge($(this).val()));
			});

			$('body').on('bind keyup change keypress','.siblingDOB',function() {
				$(this).closest('tr').find('.siblingAge').val(getAge($(this).val()));
			});	

			$('body').on('bind keyup','input:text',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));

				if ($(this).closest('form').attr('id') == 'formScholastic') {
					ADMISSION_HELPER.validateQuestions('#tableScholastic');
					ADMISSION_HELPER.validateQuestions('#tableQuestions');
				}
			});

			$('body').on('change','form select',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
				if ($(this).closest('form').attr('id') == 'formScholastic') {
					ADMISSION_HELPER.validateQuestions('#tableScholastic');
					ADMISSION_HELPER.validateQuestions('#tableQuestions');
				}
			});

			$('body').on('change','#tableDocuments tbody tr td .upload',function() {
				$(this).closest('.fileUpload').removeClass('bg-grey-cascade');
				$(this).closest('.fileUpload').removeClass('btn-danger');
				$(this).closest('.fileUpload').addClass('btn-success');
			});

			$('body').on('change','#ProgramMajor',function() {
				if ($(this).find('option:selected').val()) {
					$('#gradeProgramMajorID').val($(this).find('option:selected').val());
					documents(true);
				}
			});
			
			$('body').on('change','#ScheduleDate',function() {
				$('#ScheduleDateID').val($(this).find('option:selected').val());
			});		

			$('body').on('change','#schoolYear',function() {
				documents(true);
			});	

			$('#MiddleName').bind('input',function() {
				$('#MiddleInitial').val(getMiddleInitial($(this).val()));
			});	

			$('#FamilySearch').click(function() {
				showFamilyModalSearch($(this));
			});

			$('#FamilyClearBG').click(function() {
				confirmEvent("Are you sure you want to clear this?", function(yes) {
					if (!yes) return;
					clearFamilyBG($(this));
					isFormValid('#parents #formFather');
					isFormValid('#parents #formMother');
					isFormValid('#parents #formGuardian');
				});
			});

			$('body').on('click','.btn_search_family',function() {
				searchFamily($(this));
			});	

			$('body').on('click','.btnFamilySel',function() {
				selFamily($(this));
			});	
			
			$('body').on('change','#GuardianCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val());
			});	

			$('body').on('click','#isOtherCity',function() {
				ADMISSION_DATA.toggleOtherCity($(this).is(':checked'));
			});	

			$('body').on('change','#BillingCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val(),'#BillingCity');
			});	

			$('body').on('click','#BillingIsOtherCity',function() {
				ADMISSION_DATA.toggleBillingOtherCity($(this).is(':checked'));
			});

			$('.EducLevel').on('ifChecked',function(event) {
				ADMISSION_DATA.getYearLevel($(this).val());
				College.init();
			});
	
			$('body').on('change','#BillingCity',function() {
				$('#BillingCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('.CourseSel').change(function(event) {
				if ($(this).val()) {
					College.CourseSel($(this));
				}
			});

			$('body').on('click','#CopyAddress', function() {
				if ($(this).is(':checked')) {
					copyAdddress();
				}
			});

			$('.CitySelOption').change(function(event) {
				if ($(this).val()) {
					$(this).parent().find('.CitySel').val($(this).find('option:selected').attr('data-id'));
				}
			});

			$('body').on('click','#IsManageAccount', function() {
				SetManageAccount();
			});    

			$('#btnAddSchoolAttended').click( function() {
				AddSchoolAttended();
			});
            
			$('[name="Religion"]').change(function(){
				var xid = $(this).val();
				
			});
			
			$('body').on('change','[name="Nationality"],[name="Citizenship"],[name="Religion"],[name="FatherNationality"],[name="FatherCitizenship"],[name="MotherNationality"],[name="MotherCitizenship"],[name="FatherReligion"],[name="MotherReligion"]',function() {
               var xid   = $(this).val();
			   var xtxt  = $(this).find('option[value="'+xid+'"]').text();
               var xname = $(this).attr('name');
			   console.log(xtxt);
			   $('[name="add'+xname+'"]').val('');
			   $('[name="add'+xname+'"]').attr('type','hidden');
			   $('[name="add'+xname+'"]').addClass('not-required');
			   $('[name="add'+xname+'"]').removeAttr('required');
               if(xid=='x'){
				   $('[name="add'+xname+'"]').attr('type','text');
				   $('[name="add'+xname+'"]').attr('required','true');
				   $('[name="add'+xname+'"]').removeClass('not-required');
				   $('[name="add'+xname+'"]').attr('placeholder','Please specify');
				   $('[name="add'+xname+'"]').closest('div').find('.help-block').removeClass('hide');
			   }
			   if((xname=='Religion' || xname=='FatherReligion' || xname=='MotherReligion') && xtxt=='Other Christian Denomination'){
				   $('[name="add'+xname+'"]').attr('type','text');
				   $('[name="add'+xname+'"]').attr('required','true');
				   $('[name="add'+xname+'"]').removeClass('not-required');
				   $('[name="add'+xname+'"]').attr('placeholder','Please specify');
				   $('[name="add'+xname+'"]').closest('div').find('.help-block').removeClass('hide');
			   }
			   
			});
			
			$('body').on('change','.answer[name="select-24"]',function(){
			   var tmpval = $(this).val();
			   $('[name="answer-25"]').attr('placeholder','Please specify');
			   $('[name="answer-25"]').addClass('not-required');
			   $('[name="answer-25"]').removeAttr('required');
			   
			   if(tmpval=='Yes'){
			     $('[name="answer-25"]').removeClass('not-required');
			     $('[name="answer-25"]').attr('required',true);
			   }
			
			});
			
			$('body').on('change','.answer[name="select-27"]',function(){
			   var tmpval = $(this).val();
			   $('[name="answer-28"]').attr('placeholder','Please specify');
			   $('[name="answer-28"]').addClass('not-required');
			   $('[name="answer-28"]').removeAttr('required');
			   
			   if(tmpval=='Yes'){
			     $('[name="answer-28"]').removeClass('not-required');
			     $('[name="answer-28"]').attr('required',true);
			   }
			
			});
			
			$('body').on('change','.answer[name="select-31"]',function(){
			   var tmpval = $(this).val();
			   $('[name="answer-32"]').attr('placeholder','Please specify');
			   $('[name="answer-32"]').addClass('not-required');
			   $('[name="answer-32"]').removeAttr('required');
			   $('[data-id="32"]').find('.help-block').addClass('hide');
			   
			   $('[name="answer-33"]').attr('placeholder','Please specify');
			   $('[name="answer-33"]').addClass('not-required');
			   $('[name="answer-33"]').removeAttr('required');
			   $('[data-id="33"]').find('.help-block').addClass('hide');
			   
			   $('[name="answer-34"]').attr('placeholder','Please specify');
			   $('[name="answer-34"]').addClass('not-required');
			   $('[name="answer-34"]').removeAttr('required');
			   $('[data-id="34"]').find('.help-block').addClass('hide');
			   
			   $('[name="answer-35"]').attr('placeholder','Please specify');
			   $('[name="answer-35"]').addClass('not-required');
			   $('[name="answer-35"]').removeAttr('required');
			   $('[data-id="35"]').find('.help-block').addClass('hide');
			   
			   $('[name="answer-36"]').attr('placeholder','Please specify');
			   $('[name="answer-36"]').addClass('not-required');
			   $('[name="answer-36"]').removeAttr('required');
			   $('[data-id="36"]').find('.help-block').addClass('hide');
			   
			   if(tmpval=='Yes'){
			     $('[name="answer-32"]').removeClass('not-required');
			     $('[name="answer-32"]').attr('required',true);
				 
			     $('[name="answer-33"]').removeClass('not-required');
			     $('[name="answer-33"]').attr('required',true);
				 
			     $('[name="answer-33"]').removeClass('not-required');
			     $('[name="answer-33"]').attr('required',true);
				 
			     $('[name="answer-34"]').removeClass('not-required');
			     $('[name="answer-34"]').attr('required',true);
				 
			     $('[name="answer-35"]').removeClass('not-required');
			     $('[name="answer-35"]').attr('required',true);
				 
			     $('[name="answer-36"]').removeClass('not-required');
			     $('[name="answer-36"]').attr('required',true);
			   }
			
			});
			
			$('body').on('change','[name="PassAllSubjects"]',function(){
			    var tmpval = $(this).val();
				$('[name="SubjectsFailing"]').attr('placeholder','');
				$('[name="SubjectsFailing"]').addClass('not-required');
				$('[name="SubjectsFailing"]').removeAttr('required');
				$('[name="SubjectsFailing"]').closest('div').find('.help-block').addClass('hide');
				if(tmpval=="No" || tmpval=="no"){
				  $('[name="SubjectsFailing"]').attr('placeholder','Please specify');
				  $('[name="SubjectsFailing"]').removeClass('not-required');
				  $('[name="SubjectsFailing"]').attr('required',true);
				  $('[name="SubjectsFailing"]').closest('div').find('.help-block').removeClass('hide');
				}
			});
			
			$('body').on('change','.answer[name="select-6"]',function(){
			   var tmpval = $(this).val();
			   $('[name="answer-7"]').attr('placeholder','Please specify');
			   $('[name="answer-7"]').addClass('not-required');
			   $('[name="answer-7"]').removeAttr('required');
			   $('[data-id="7"]').find('.help-block').addClass('hide');
			   $('[name="answer-8"]').attr('placeholder','Please specify');
			   $('[name="answer-8"]').addClass('not-required');
			   $('[name="answer-8"]').removeAttr('required');
			   $('[data-id="8"]').find('.help-block').addClass('hide');
			   if(tmpval=="Everest family/employee"){
			    $('[name="answer-7"]').attr('placeholder','Please specify');
			    $('[name="answer-7"]').removeClass('not-required');
			    $('[name="answer-7"]').attr('required',true);
			    $('[name="answer-7"]').val('');
			    $('[name="answer-7"]').trigger('change');
			   }
			   
			   if(tmpval=="Other"){
			    $('[name="answer-8"]').attr('placeholder','Please specify');
			    $('[name="answer-8"]').removeClass('not-required');
			    $('[name="answer-8"]').attr('required',true);
			    $('[name="answer-7"]').val('');
			    $('[name="answer-8"]').trigger('change');
			   }
			});
			
			$(document).on('click','.btnSchoolsAttendedRemove', function() {
				removeSchoolAttended($(this));
			});

			$('body').on('click','#btnAddReferences',function() {
				addNewReference($(this));
			});

			$('body').on('click','.btnReferencesRemove',function() {
				removeReference($(this));
			});

			$('body').on('click', '#isBaptized', function() {
		        if ($(this).is(":checked")) {
		        	$('.religionFields').removeClass('not-required');
		        } else{
		            $('.religionFields').addClass('not-required');
		        }
		        isFormValid('#formStudent',true);
		    });

		    $('body').on('change','#Citizenship',function() {
				if ($('#Citizenship option:selected').attr('data-isforeign') == 1) {
					$('#isForeignCitizen').val(1);
				} else {
					$('#isForeignCitizen').val(0);
				}
			});

			$('body').on('change','#PresentSchoolCurriculum',function() {
				if ($(this).val() == 'Other') {
					$('#PresentSchoolCurriculumOthers').removeClass('not-required');
				} else {
					$('#PresentSchoolCurriculumOthers').addClass('not-required');
				}
			});
			
			$('.StudentPhoto').change(function(){
				$('.photo-message').addClass('hidden');
			});
            
			var chkdate = $('#DateOfBirth').val();
			if(chkdate!=''){
			   $('#DateOfBirth').trigger('change');
			}
		}
	}
}();