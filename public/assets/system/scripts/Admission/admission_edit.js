var global_step = 1, global_this = '', documents = '',examSchedule = '', completeStep = 9, firstStep = 1;
var ADMISSION = function() {

	var admissionDirectClick = function(el) {
	};

	var setThumbnail = function() {
		// console.log($('#formStudent #StudentPhoto')[0].files[0]);
		// $('#formStudent #StudentPhotoThumbnail').attr('src',$('#formStudent #StudentPhoto').val());
	}

	var parseAgeSiblings = function() {
		$('#tableDefaultSibling tbody tr').each(function() {
			$(this).find('td .siblingAge').val(getAge($(this).find('td .siblingDOB').val()));
		});
	}

	var setStudentAge = function() {
		$('#StudentAge').text(getAge($('#DateOfBirth').val()));
	}


	var addNewSibling = function() {
		var defaultRow = $('#tableDefaultSibling tbody').html();
		$('#tableSiblings tbody').append(defaultRow);
		FN.datePicker('.siblingDOB');
		setDateFormat('.siblingDOB');
	}

	var gradeLevel = function(el,isProcess) {
		var isProcess = isProcess == undefined ? true : isProcess;

		removeInputRequired('#formStudent','.HS');
		removeInputRequired('#formStudent','.ES');
	};

	var isSuccessRemarksStatus = false;
	var saveAdmissionStatus = function() {
        var $rem = $('#StatusRemarks'); 
		var result = ajaxRequest(base_url+page_url+'event','event=saveRemarksStatus&AppNo='+getParameterByName('AppNo')+'&remarks='+$rem.val()+'&status='+ $rem.attr('data-status') )
		if (result.error == false) {
			msgbox('success',result.message);
			//getProperty('#AdmissionStatusSel').val(getSelectedVal('#AdmissionStatus'));
            location.reload();
			closeModal('.modal_status_remarks');
			isSuccessRemarksStatus = true;
		} else {
			msgbox('success',result.message);
			//getProperty('#AdmissionStatus').val(getInputVal('#AdmissionStatusSel'));
            location.reload();
		}
	};

	var showModalAdmissionStatus = function($e) {
		showModal({
			name: 'basic',
			title: 'Remarks',
			content: ajaxRequest(base_url+page_url+'event','event=modalRemarks&AppNo='+getParameterByName('AppNo')+'&status='+ $e.attr('data-val') ,'HTML'),
			class: 'modal_status_remarks'
		});
	};

	var setForeignFields = function(showError) {
		
		// if ($('#Nationality option:selected').attr('data-isforeign') == 1) {
		// 	$('.foreignFields').removeClass('not-required');
		// 	$('#isForeign').val(1);
		// } else {
		// 	$('.foreignFields').addClass('not-required');
		// 	$('#isForeign').val(0);
		// }
	}

	var editStep = function(el) {
		var step = parseInt(el.attr('data-step'));

		$('#adminssionSteps li').removeClass('active');
		$('#adminssionSteps > li.step-'+step).addClass('active');

		$('.mainTabContent > .tab-pane').removeClass('active');
		$('.mainTabContent > .step-'+step).addClass('active');

		setSessionCurrStep(step);
	};

	var removeSibling = function(el) {
		if (el.closest('tr').attr('data-id') != undefined && el.closest('tr').attr('data-id') != '' ) {
			confirmEvent("Are you sure you want to remove this?", function(yes) {
				if (!yes) return;
				setAjaxRequest(base_url+page_url+'event','event=removeSibling&key='+el.closest('tr').attr('data-id')
					, function(result) {
						if (result.error == false) {
							msgbox('success',result.message);
							el.closest('tr').remove();
						} else {
							msgbox('error',result.message);
						}
						progressBar('off');
					}
				);
			});
		} else {
			el.closest('tr').remove();
		}
	}

	var onLoad = function() {	
		var step;
		
		//gradeLevel($('#gradeLevel'),false);
		// ADMISSION_DATA.showProgram(getSelectedVal('#gradeLevel'));
		setForeignFields(false);

		generateUlSteps();

		//set ajax current session step
		// setSessionStep();
		// //toggle steps button
		// toggleStepButton();

		// setProgressButtons();
		// setThumbnail();
		//review();
		parseAgeSiblings();
		setStudentAge();
		isFormValid('#formStudent');
		setDateFormat();

		ADMISSION_DATA.setStudentPhoto();
		ADMISSION_DATA.countryIcons();
		ADMISSION_DATA.countryIcons('#BillingCountry');

		ADMISSION_DATA.toggleOtherCity($('#isOtherCity').is(':checked'));
		ADMISSION_DATA.toggleBillingOtherCity($('#BillingIsOtherCity').is(':checked'));
		calculateSiblingDOB();

		ADMISSION_DATA.getYearLevel(ADMISSION_DATA.getSelEducLevel());
		loadScholasticByYrLvl();
		ADMISSION_DATA.getCity($('#GuardianCountry').find('option:selected').val());
		ADMISSION_DATA.getCity($('#BillingCountry').find('option:selected').val(),'#BillingCity');
		College.init();	
		
		SetManageAccount();

		checkFormValidation();
	};


	examSchedule = function() {
		setAjaxRequest(base_url+page_url+'event','event=ShowExamScheduleDates'
			+'&campus='+$('#schoolCampus option:selected').val()
			+'&term='+$('#schoolYear option:selected').val()
			+'&gradeLevel='+$('#gradeLevel option:selected').val(),
			function(result) {

				if (result.error == false) {
					$('#ScheduleDate').remove('option');
					var options = '<option value="">Select...</option>';
					$('#ScheduleDate').removeClass('not-required');
						
					if (result.data.length <= 0) {
						options += '<option selected value="">None</option>';	
						$('#ScheduleDate').addClass('not-required');
					}
					for(var i in result.data) {
						options += '<option  value="'+result.data[i].SchedID+'">'+result.data[i].SchedDate+'</option>';
					}

					if($('#ScheduleDate option:selected').val() == '') {
						if (getInputVal('#ScheduleDateID')) {
							$('#ScheduleDate').val($('#ScheduleDateID').val());
						}
						$('#ScheduleDate').html(options);
					}
				}
			},
			function(error) {

			},
			'JSON',
			true,
			false
		);
	};

	documents = function(async) {
		var async = async == undefined ? false : async;
		var bar = async ? false : true;
		if ($('#Nationality option:selected').val() == ''
			|| $('#gradeLevel option:selected').val() == ''
			|| $('#gradeProgramClass').val() == ''
			) {
			return false;
		}
		setAjaxRequest(base_url+page_url+'event','event=getRequiredDocs&AppID='
			+$('#applicationType option:selected').val()
			+'&IsForeign='+$('#Nationality option:selected').attr('data-isforeign') 
			+'&ProgClass='+$('#gradeProgramClass').val()
			+'&YearLevelID='+$('#gradeLevel option:selected').val()
			+'&AppNo='+getParameterByName('AppNo'),
			function(result) {
				var table = $('#tableDocuments');
				table.find('tbody tr').remove();
				var hasData = false, swipe_index = 0;
				for(var i in result) {
					table.find('tbody').append(
						"<tr data-has-attachment='"+ result[i].HasAttachment+"' data-swipe-index='"+(result[i].HasAttachment == '1' ? swipe_index++ : 0) +"'  data-tempID='"+result[i].TemplateID+"' data-id='"+result[i].DetailID+"' data-isRequired='"+(result[i].IsRequired)+"' data-entryid='"+result[i].EntryID+"' data-doc='"+result[i].RequirementID+"'>"
						+"<td>"+(result[i].HasAttachment == '1' ? '<a href="javascript:void(0)" class=" aViewDocs fancybox-button" data-rel="fancybox-button">'+result[i].DocDesc+'</a>' : result[i].DocDesc)+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsReviewed'"+(result[i].IsReviewed == '1' ? 'checked' : '')+">" : (result[i].IsReviewed ? 'Yes' : 'No') )+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsExempted'"+(result[i].IsExempted == '1' ? 'checked' : '')+">" : (result[i].IsExempted ? 'Yes' : 'No') )+"</td>"
						+"<td class='center'>"+(result[i].IsRequired == '1' ? 'Yes' : 'No')+"</td>"
						+'<td class="center">'
							+'<div class="fileUpload btn '+(result[i].HasAttachment == '1' ? 'btn-success' : (result[i].IsRequired == '1' ? 'btn-danger' : 'bg-grey-cascade'))+'">'
				                +'<span><i class="fa fa-file"></i></span>'
				                +'<input type="file" class="upload" />'
				            +'</div>'
				         +'</td>'
						+"</tr>"
					);
					hasData = true;
				}
				if (hasData) {
					table.attr('data-nationality',$('#Nationality option:selected').val());
					table.attr('data-YearLevel',$('#gradeLevel option:selected').val());
					table.attr('data-AppType',$('#applicationType option:selected').val());
					table.attr('data-ProgClass',$('#gradeProgramClass').val());
				}
				progressBar('off');
			},'','JSON',async,bar
		);
	};

	var calculateSiblingDOB = function() {
		$('#siblings #tableSiblings tbody tr').each(function() {
			$(this).find('.siblingAge').val(getAge($(this).find('.siblingDOB').val()));
		});
	};

	var viewDocuments = function(el) {
        $('#modal_doc .modal-body').html(ajaxRequest(base_url+page_url+'event','event=viewDocs&FamilyID='+el.closest('tr').attr('data-family')+'&key='+el.closest('tr').attr('data-entryid'),'HTML'));
        $('#modal_doc').modal('show');
	};

	var downloadDoc = function(el) {
		window.open(base_url+'general/downloadAdmissionDoc?key='+el.attr('data-id'));
	};

	var saveDocRemarks = function(el) {
		var remarks = el.parent().find('.TextRemarks').val().trim();
		if (remarks) {
			confirmEvent("Are you sure you want to save this?", function(yes) {
				if (!yes) return;
				var result = ajaxRequest(base_url+page_url+'event','event=saveDocRemarks&key='+el.attr('data-id')+'&remarks='+remarks);
				if (result.error == false) {
					msgbox('success',result.message);
				} else {
					msgbox('error',"There was a problem while saving.");
				}
			});
		}
	};

	var showDocRemarks = function(el) {
		showModal({
			name: 'basic',
			title: 'Document Remarks',
			content: ajaxRequest(base_url+page_url+'event','event=getDocsRemarks&key='+el.attr('data-id'),'HTML'),
			hasFooter: true,
			hasModalButton: false,
		})
	};

	var copyAdddress = function() {
		var form = '#formGuardian';

		var resident = $(form).find('input[name="resident"]').val();
		var street = $(form).find('input[name="street"]').val();
		var barangay = $(form).find('input[name="barangay"]').val();
		var GuardianCountry = $(form +' #GuardianCountry option:selected').val();
		var isOtherCity = $(form).find('input[name="isOtherCity"]').is(':checked') ? 1 : 0;
		var otherCity = $(form).find('input[name="otherCity"]').val();
		var GuardianCity = $(form).find('#GuardianCity option:selected').val();

		$(form).find('input[name="BillingResident"]').val(resident);
		$(form).find('input[name="BillingStreet"]').val(street);
		$(form).find('input[name="BillingBrangay"]').val(barangay);
		$(form).find('input[name="BillingGuardianCountry"]').val(GuardianCountry);
		$(form).find('input[name="BillingOtherCity"]').val(otherCity);

		$('#BillingIsOtherCity').prop('checked',isOtherCity);
		ADMISSION_DATA.toggleBillingOtherCity(isOtherCity);

		$('#BillingCountry').select2('val',[GuardianCountry]);
		$('#BillingCountry').trigger('change');

		$('#BillingCity').select2('val',[GuardianCity]);
		$('#BillingCity').trigger('change');

		$(':checkbox').uniform();
	};

	var saveCommittee = function() {
		var form = '#formCommittee';
		var admitted = $(form).find('input[class="chk-admitted"]').is(':checked'),
		condition = $(form).find('input[class="chk-condition"]').is(':checked'),
		waitpool = $(form).find('input[class="chk-waitpool"]').is(':checked'),
		denied = $(form).find('input[class="chk-denied"]').is(':checked');

		if (isFormValid(form)) {
			confirmEvent("Are you sure you want to save this remarks?", function(yes) {
				if (!yes) return;
				var result = ajaxRequest(base_url+page_url+'event','event=saveCommittee&AppNo='+getParameterByName('AppNo')+'&'+$(form).serialize()+'&admitted='+admitted+'&condition='+condition+'&waitpool='+waitpool+'&denied='+denied+'&id='+$(form).attr('data-id'));
				if (result.error == false) {
                	$(form).attr('data-id',result.id);
                	$('.btnaction-committee').html("<i class='fa fa-check'></i> Update Remarks");
					msgbox('success',result.message);
					$('#tblcommittee').DataTable().search($('.dtcomm_search').val()).draw();
				} else {
					msgbox('error',"There was a problem while saving.");
				}
			});
		}
	};

	var loadCommittee = function() {
        $('#tblcommittee').dataTable().empty();
        $('#tblcommittee').dataTable().fnDestroy();
        var grid = new Datatable();

        grid.init({
            src: $("#tblcommittee"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "serverSide": true,
                "lengthMenu": [
                                [10, 15, 20, 25, 50, -1],
                                [10, 15, 20, 25, 50, "All"] // change per page values here
                              ],
                "pageLength": 20, // default record count per page   
                "autoWidth": false,
                "columnDefs": [
                    { 
                        'targets': [0],
                        'orderable': false
                    },  
                    { 
                        'targets': '_all',
                        'searchable': true,
                        'orderable': true
                    }     
                ],
                "columns": [
                    { "data": 0 }, { "data": 1 },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 2 
                    },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 3 
                    },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 4 
                    },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 5
                    },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 6
                    },
                    {
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            $(td).css('text-align', 'center'); 
                        },
                        "data": 7
                    }
                ],
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": base_url+page_url+'event', // ajax source
                    "data": {
                        "event": 'dataTableCommittee',
                        'AppNo': getParameterByName('AppNo')
                    },
                },
                "order": [
                    [0, 'asc']
                ]
                
            }
        });

    };
	
    var saveAppevaluate = function(){
	    var xform = $('#formAppEval').serialize();
		//saveAppEvaluate
		ajaxRequestLoader(base_url+page_url+'event',
		    'event=saveAppEvaluation&AppNo='+getParameterByName('AppNo')+'&'+xform, 
			function(success_data){
				hidePleaseWait();
				$('#formAppEval').find('#eEntryID').remove();
				$('#formAppEval').find('input[type="text"]').val('');
				$('#formAppEval').find('textarea').val('');
				$('#formAppEval').find('select').val('');
				$('#formAppEval').find('input[type="radio"]').prop('checked',false);
				loadAppEvaluate(0);
			},
			function(error_data){
				hidePleaseWait();
				msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
		    });
		//alert(xform);
	};
	
	var removeAppEvaluate = function(){
	    $('#tblappeval').find('.chk-child').each(function(){
		   var eid   = $(this).closest('tr').attr('data-id');
		   var ischk = $(this).prop('checked');
           if(ischk){
		      setAjaxRequest(base_url+page_url+'event',
					   'event=removeAppEvaluation&EntryID='+eid,
					   function(result){
						   if(result.error==false){
						    $('#tblappeval').find('tr[data-id="'+result.entryid+'"]').remove();
						   }
					   },
					   function(error){
						   msgbox('error',error.message != !undefined ? error.message : 'There was an error while saving');
					});
		   }
		});
		
		progressBar('off');
	};
	
	var loadAppEvaluate = function(xid){
	    var eid = ((xid!=undefined && xid!='')?xid:0);
		$('#formAppEval').find('#eEntryID').remove();
		$('#formAppEval').find('input[type="text"]').val('');
		$('#formAppEval').find('textarea').val('');
		$('#formAppEval').find('select').val('');
		$('#formAppEval').find('input[type="radio"]').prop('checked',false);

		var form_data = new FormData(), dataAttach = new Array;	
		$('.table-evalattach tbody tr.include').each(function() {
			var self = $(this);
			if (self.find('td input[type="file"]').hasClass('file')) {
				if (self.find('td .file')[0].files[0] != undefined) {
					form_data.append('file[]', self.find('td .file')[0].files[0]);
					dataAttach.push(JSON.stringify({
						AppNo: getParameterByName('AppNo'),
						id: (self.attr('data-id') == '' || self.attr('data-id') == undefined) ? '' : self.attr('data-id'),
						type: self.find('td .filetype').val(),
						size: self.find('td .filesize').val()
					}));
				}
			}
		});
		
		if(dataAttach.length>0){
			$.ajax({
				url: base_url+page_url+'saveAttachment?id=0&attachmentData='+dataAttach,
				dataType: "json",
				data: form_data,
				type: 'POST',
				contentType: false,
				processData: false,
				async: false,
				success: (
					function(success) {
						if ( !success ) {
							return msgbox('error',"There was an error while saving");
						}else{
							loadEAttachment();
						}
					}
				),
			});
		}
		
		setAjaxRequest(base_url+page_url+'event',
					   'event=loadAppEvaluation&appno='+getParameterByName('AppNo')+'&entryid='+eid,
					   function(result){
						   if(result.error==false){
						    if(result.entryid==0){
				              $('#formAppEval').find('#eEntryID').remove();
						      $('#tblappeval').find('tbody').html(result.content);
							}else{
							  msgbox('error',result.message != !undefined ? result.message : 'There was an error while loading');
							}
							progressBar('off');
						   }
					   },
					   function(error){
						   msgbox('error',error.message != !undefined ? error.message : 'There was an error while saving'); 
		                   progressBar('off');
					});
	};
	
	var loadEAttachment = function(){
	    $.ajax({
			url: base_url+page_url+'loadAttachment?AppNo='+getParameterByName('AppNo'),
			dataType: "html",
			data: {},
			type: 'GET',
			success: (
				function(data) {
					if (data!='') {
					 $('#tab-evalattach').html(data);
					}
				}
			),
		});
	};
	
	var saveEAttachment = function(){
		var form = '#formEvaluation';
		
		confirmEvent("Are you sure you want to save this evaluation?", function(yes) {
				if (!yes) return;

				var form_data = new FormData(), dataAttach = new Array;	
		        $('.table-evalattach tbody tr.include').each(function() {
		        	var self = $(this);
	        		if (self.find('td input[type="file"]').hasClass('file')) {
	        			if (self.find('td .file')[0].files[0] != undefined) {
	        				form_data.append('file[]', self.find('td .file')[0].files[0]);
	        		 		dataAttach.push(JSON.stringify({
	        		 			AppNo: getParameterByName('AppNo'),
	        		 			id: (self.attr('data-id') == '' || self.attr('data-id') == undefined) ? '' : self.attr('data-id'),
	        		 			type: self.find('td .filetype').val(),
	        		 			size: self.find('td .filesize').val()
	        		 		}));
	        			}
	        		}
		        });
				
				$.ajax({
					url: base_url+page_url+'saveAttachment?id=0&attachmentData='+dataAttach,
					dataType: "json",
					data: form_data,
					type: 'POST',
					contentType: false,
					processData: false,
					async: false,
					success: (
						function(success) {
							if ( !success ) {
							    return msgbox('error',"There was an error while saving");
							}else{
								loadEAttachment();
							}
						}
					),
				});
                hidePleaseWait();
		});
	};
	
    var saveEvaluation = function() {
		var form = '#formEvaluation';
		if (isFormValid(form)) {
			confirmEvent("Are you sure you want to save this evaluation?", function(yes) {
				if (!yes) return;

				var form_data = new FormData(), dataAttach = new Array;	
		        $('.table-evalattach tbody tr.include').each(function() {
		        	var self = $(this);
	        		if (self.find('td input[type="file"]').hasClass('file')) {
	        			if (self.find('td .file')[0].files[0] != undefined) {
	        				form_data.append('file[]', self.find('td .file')[0].files[0]);
	        		 		dataAttach.push(JSON.stringify({
	        		 			AppNo: getParameterByName('AppNo'),
	        		 			id: (self.attr('data-id') == '' || self.attr('data-id') == undefined) ? '' : self.attr('data-id'),
	        		 			type: self.find('td .filetype').val(),
	        		 			size: self.find('td .filesize').val()
	        		 		}));
	        			}
	        		}
		        });


				ajaxRequestLoader(base_url+page_url+'event','event=saveEvaluation&AppNo='+getParameterByName('AppNo')+'&'+$(form).serialize()+'&id='+$(form).attr('data-id')+'&InterviewType='+$('#InterviewType').val(), function(success_data)
                {
                    if ( success_data.error != undefined && success_data.error == false ) 
                    {
						$(form).attr('data-id',success_data.id);
	                	$('.btnaction-evaluation').html("<i class='fa fa-check'></i> Update Evaluation");
						$('#tblevaluation').DataTable().search($('.dteval_search').val()).draw();

						$.ajax({
			            	url: base_url+page_url+'saveAttachment?id='+success_data.id+'&attachmentData='+dataAttach,
			            	dataType: "json",
			            	data: form_data,
			            	type: 'POST',
			            	contentType: false,
			            	processData: false,
			            	async: false,
			             	success: (
			            	 	function(success) {
				              		if ( !success ) {
										return msgbox('error',"There was an error while saving");
					            	}else{
										loadEAttachment();
									}
			              		}
			             	),
		       			});

						msgbox('success',success_data.message);
                    }
                    hidePleaseWait();

                },function(error_data){
                    hidePleaseWait();
                    msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
                });
			});
		}
	};
	
	var loadEvaluation = function() {
        $('#tblevaluation').dataTable().empty();
        $('#tblevaluation').dataTable().fnDestroy();
        var grid = new Datatable();

        grid.init({
            src: $("#tblevaluation"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "serverSide": true,
                "lengthMenu": [
                                [10, 15, 20, 25, 50, -1],
                                [10, 15, 20, 25, 50, "All"] // change per page values here
                              ],
                "pageLength": 20, // default record count per page   
                "autoWidth": true,
                "columnDefs": [
                    { 
                        'targets': [0],
                        'orderable': false
                    },  
                    { 
                        'targets': '_all',
                        'searchable': true,
                        'orderable': true
                    }     
                ],
                "ajax": {
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    "url": base_url+page_url+'event', // ajax source
                    "data": {
                        "event": 'dataTableEvaluation',
                        'AppNo': getParameterByName('AppNo')
                    },
                },
                "order": [
                    [1, 'asc']
                ]
            }
        });

    };

    var removeEvalComm = function(form) {
    	var prompt = '', dt = ''; 
    	if(form == 'committee'){
    		prompt = form+' remarks';
    		dt = '.dtcomm_search';
    	} else {
    		prompt = 'admission '+form;
    		dt = '.dteval_search';
    	}

        if ($('#tbl'+form+' tbody tr td .chk-child:checked').length > 0) 
        {
            bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to delete this record/s?",
            callback: function(yes) {
            if (yes) 
                { 
                    var result = ajaxRequest(base_url+page_url+'event','event=delete'+form[0].toUpperCase()+form.substr(1)+'&ids='+getTableID('#tbl'+form));
                    if ( result.error == false ) 
                    {
                        $('#tbl'+form+' tbody tr td .chk-child:checked').each(function(){
                            $(this).closest('tr').remove();
                        });
                        
                        $('#tbl'+form).DataTable().search($(dt).val()).draw();
                    }

                    msgbox(result.error == false ? 'success' : 'error' ,result.message); 
                }
            }
            });
                
        } else { msgbox('error', 'Oops! Please select '+prompt+' first!'); }
    };

    var getTableID = function(table) {

        var _this, id, table = table == undefined ? '.table' : table;
        var data = new Array(), obj = new Object(), field = new Array();

        jQuery(table+' tbody tr').each(function(){
            _this = jQuery(this);
            id = _this.closest('tr').find('.chk-child').val();
            if ( _this.find('td .chk-child').is(':checked') ){
                obj.id=id;
                field[0]='id';
                data.push(JSON.stringify(obj,field,'\t'));
            }
        });
        return data;
    };

	var loadScholasticByYrLvl = function() {
		var apptype = $('#applicationType').val(), grdlvl = $('#gradeLevel').find('option:selected').val();
		if(apptype != 0 && grdlvl != '') {
			if(window.atob(apptype) == 1) {
				if(window.atob(grdlvl) <= 2){
					jQuery('.sch_wrapper').html(ajaxRequest(base_url+page_url+'event','event=byYrLvlScholastic&yrlvl=kinder-grade1&AppNo='+getParameterByName('AppNo'),'HTML'));
					Metronic.initUniform();
				}
				if(window.atob(grdlvl) >= 3){
					jQuery('.sch_wrapper').html(ajaxRequest(base_url+page_url+'event','event=byYrLvlScholastic&yrlvl=grade2-grade11&AppNo='+getParameterByName('AppNo'),'HTML'));
				}
				$('#formStudent .notreturnFields').each(function() {
			        if (!$(this).hasClass('not-required-default')) {
			        	if($(this).hasClass('religionFields')) {
			        		if($('#isBaptized').is(":checked")) {
			        			$(this).removeClass('not-required');
			        		}
			        	} else {
			        		$(this).removeClass('not-required');
			        	}
			        } 
			    });
				$('.rtn').addClass('hidden');
				$('.not-rtn').removeClass('hidden');
			} else {
				jQuery('.sch_wrapper').html(ajaxRequest(base_url+page_url+'event','event=byYrLvlScholastic&yrlvl=returning&AppNo='+getParameterByName('AppNo'),'HTML'));
				removeInputRequired('#formStudent','.notreturnFields');
				$('.rtn').removeClass('hidden');
				$('.not-rtn').addClass('hidden');
			}
		}
	};

	var validateAttachment = function() {
		var error_count = 0;
		if ( $('.table-evalattach tbody tr.include').length == 0 ) {
			msgbox('error','Oops! Please add attachment first!');
			return false;
		} else {
			$('.table-evalattach tbody tr.include').each(function() {
        		var self = $(this);
        		if (self.attr('data-id') == '' || self.attr('data-id') == undefined) {
        			self.find('td .span_validate').removeAttr('style');
        			if ( self.find('td .file')[0].files[0] == undefined || self.find('td .file')[0].files[0].size / 1024 / 1024 > 25 ){
        				error_count++;
        				self.find('td .span_validate').attr('style','color: #ff0000');
        			}
        		}
        	});
        	if (error_count > 0) { msgbox('error',"Error in file size limit (Max of 25MB). Please Try again!"); }
        	return error_count > 0 ? false : true;
		}
	}


	var isValidFile = function(el) {
		var up_path = el.parent().find("#fileUpload").val().toLowerCase(),
		up_file = el.parent().find("#fileUpload")[0].files[0],
		regex_xlsfile = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/, 
		regex_txtfile = /^([a-zA-Z0-9\s_\\.\-:])+(.txt)$/, 
		regex_imgfile = /^([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.bmp)$/, 
		regex_docxpdf = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$");
		if(regex_xlsfile.test(up_path)){
			file_type = 'application/vnd.ms-excel';
	        return checkFileSize(up_file);
		}
		if(regex_txtfile.test(up_path)){
			file_type = 'text/plain';
			return checkFileSize(up_file);
		}
		if(regex_imgfile.test(up_path)){
			file_type = 'image/jpeg';
			return checkFileSize(up_file);
		}
		if(regex_docxpdf.test(up_path)){
			file_type = 'application/pdf';
			return checkFileSize(up_file);
		}
		msgbox('error', 'Please select a valid excel/text/image/document/pdf files');
		setTimeout(function(){
			el.parent().find('.fileinput-exists').trigger('click');
		},10);
		return false;

		function checkFileSize(_file) {
			if(_file == undefined || _file.size / 1024 / 1024 > 25){
	        	el.parent().find('.fileinput-exists').trigger('click');
	        	file_type = '';
	        	msgbox('error',"Error in file size limit (Max of 25MB). Please Try again!");
	        	return false;
	        } else {
	        	return true;
	        }
		}
	}

	return {
		init: function() {

			onLoad();
			loadEvaluation();
			loadAppEvaluate(0);
			loadCommittee();
			$('body').on('click','#btnNext',function() {
				setProgressStep(getCurrentStep());
			});

			$('body').on('click','#btnPrev',function() {
				setPreviousStep();
			});

			$('body').on('click','#btnAddSibling',function() {
				addNewSibling($(this));
			});

			$('body').on('click','.btnSiblingRemove',function() {
				removeSibling($(this));
			});

			$('body').on('change','#gradeLevel',function() {
				if ($(this).val() == '') return;
				//gradeLevel($(this));
				loadScholasticByYrLvl();
				ADMISSION_DATA.showProgram(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
				enableSchoolAttended();
				examSchedule();
				documents(true);
				setGradeLevelSel(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
				isFormValid('#formStudent',showError);
			});

			$('body').on('click','.a-adminssionSteps',function() {
				var step = parseInt($(this).closest('li').attr('data-step'));
				if (getStepIndex('complete') == step) {
					return false;
				} else {
					var step = $(this).closest('li').attr('data-step');
					if ((step) 
						== 
						getStepIndex('review')
					) {
						progressBar('on');
						review();
						progressBar('off');
					}
					setSessionCurrStep(step,true);
					showCompleteButton($(this));
				}
			});	

			$('body').on('click','.btn-edit',function() {
				progressBar('on');
				editStep($(this));				
				showCompleteButton($(this));
				progressBar('off');
			});

			$('body').on('click','#btnSubmitTemp,.btnsave',function() {
			     console.log('hey');
				/*
                if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
                */
                
				confirmEvent("Are you sure you want to save this?",function(yes) {
					if (!yes) return
					// var result = ADMISSION_DATA.admitTemp($(this),1);
					// if (result.error != undefined && result.error == false) {
					// 	ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
					// 	ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
					// 	hideButtons();
					// 	$('#btnNewApp').addClass('hide');
					// 	setWizzardSteps(is_parent ? 8 : 9);
					// 	setSessionCurrStep(1);
					// 	hideError();
					// 	hideSuccess();
					// 	$('.complete-form').html(result.message);
					// } else {
					// 	var msg = result.message != !undefined ? result.message : 'There was an error while saving';
					// 	msgbox('error',msg);
					// }

					ajaxRequestLoader(base_url+page_url+'event',ADMISSION_DATA.admitData($(this),1), function(success_data)
	                {
	                    if ( success_data.error == false ) 
	                    {
	                        ADMISSION_DATA.savePhoto(success_data.AppNo,success_data.FamilyID);
							ADMISSION_DATA.saveDocuments(success_data.AppNo,success_data.FamilyID);
							hideButtons();
							$('#btnNewApp').addClass('hide');
							setWizzardSteps(is_parent ? 8 : 9);
							setSessionCurrStep(1);
							hideError();
							hideSuccess();
							$('.complete-form').html(success_data.message);
	                    }
	                    hidePleaseWait();

	                },function(error_data){
	                    hidePleaseWait();
	                    msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
	                });

				});
			});

			$('body').on('click','#btnSubmit',function() {
				if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
				confirmEvent("Are you sure you want to submit this?",function(yes) {
					if (!yes) return
					var result = ADMISSION_DATA.admitTemp($(this),2);
					if (result.error != undefined && result.error == false) {
						ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
						ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
						hideButtons();
						$('#btnNewApp').addClass('hide');
						setWizzardSteps(is_parent ? 8 : 9);
						setSessionCurrStep(1);
						hideError();
						hideSuccess();
						clearSteps();
						$('.complete-form').html(result.message);
					} else {
						var msg = result.message != !undefined ? result.message : 'There was an error while saving';
						msgbox('error',msg);
					}
				});
			});

			$('body').on('click','#btnNewApp',function() {
				setWizzardSteps(1);
				setSessionCurrStep(1);
				location.reload();
			});

			$('body').on('change','#Nationality',function() {
				setForeignFields();
				documents(true);
				isFormValid('#formStudent');
			});

			$('body').on('bind keyup change keypress','#DateOfBirth',function() {
				$('#StudentAge').text(getAge($(this).val()));
			});

			$('body').on('bind keyup change keypress','.siblingDOB',function() {
				$(this).closest('tr').find('.siblingAge').val(getAge($(this).val()));
			});	

			$('body').on('bind keyup','input:text',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
			});

			$('body').on('change','form select',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
			});

			$('body').on('change','#documents #tableDocuments tbody tr td .upload',function() {
				$(this).closest('.fileUpload').removeClass('btn-danger');
				$(this).closest('.fileUpload').removeClass('bg-grey-cascade');
				$(this).closest('.fileUpload').addClass('btn-success');
			});

			$('body').on('change','#ProgramMajor',function() {
				if ($(this).find('option:selected').val()) {
					$('#gradeProgramMajorID').val($(this).find('option:selected').val());
					documents(true);
				}
			});
			
			$('body').on('change','#ScheduleDate',function() {
				$('#ScheduleDateID').val($(this).find('option:selected').val());
			});		

			$('body').on('change','#schoolYear',function() {
				documents(true);
			});	

			$('#MiddleName').bind('input',function() {
				$('#MiddleInitial').val(getMiddleInitial($(this).val()));
			});	

			$('body').on('click','.aViewDocs',function() {
				openPhotoSwipe(global_this = $(this),$(this).closest('tr').attr('data-swipe-index'));
			});	

			$('body').on('click','.btnDownloadDoc',function() {
				downloadDoc($(this));
			});

			$('body').on('click','.BtnSaveDocRemarks',function() {
				saveDocRemarks($(this));
			});		

			$('body').on('click','.BtnShowDocRemarks',function() {
				showDocRemarks($(this));
			});	

			$(document).on("contextmenu", ".TextRemarks", function(e){
			   return false;
			});

			$('body').on('click','.pswp__scroll-wrap',function() {
				return false;
			});

			$('body').on('click','.admstatus',function() {
				showModalAdmissionStatus($(this));
				isSuccessRemarksStatus = false;
			});

			$('body').on('click','.modal_status_remarks .btn_modal',function() {
				confirmEvent("Are you sure you want to save this?", function(yes) {
					if (!yes) return;
					saveAdmissionStatus($(this));
				});
			});

			$('body').on('click','.modal_status_remarks .modal-footer .default',function() {
				if (!isSuccessRemarksStatus) {
					getProperty('#AdmissionStatus').val(getInputVal('#AdmissionStatusSel'));
				}
			});

			$('body').on('click','.modal_status_remarks .modal-content .close',function() {
				if (!isSuccessRemarksStatus) {
					getProperty('#AdmissionStatus').val(getInputVal('#AdmissionStatusSel'));
				}
			});

			$('body').on('change','#GuardianCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val());
			});	

			$('body').on('click','#isOtherCity',function() {
				ADMISSION_DATA.toggleOtherCity($(this).is(':checked'));
			});
			
			$('body').on('change','#BillingCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val(),'#BillingCity');
			});	

			$('body').on('click','#BillingIsOtherCity',function() {
				ADMISSION_DATA.toggleBillingOtherCity($(this).is(':checked'));
			});

			$('body').on('change','#BillingCity',function() {
				$('#BillingCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('.EducLevel').on('ifChecked',function(event) {
				ADMISSION_DATA.getYearLevel($(this).val());
				College.init();
			});

			$('.CourseSel').change(function(event) {
				if ($(this).val()) {
					College.CourseSel($(this));
				}
			});

			$('body').on('click','#CopyAddress', function() {
				if ($(this).is(':checked')) {
					copyAdddress();
				}
			});

			$('.CitySelOption').change(function(event) {
				if ($(this).val()) {
					$(this).parent().find('.CitySel').val($(this).find('option:selected').attr('data-id'));
				}
			});

			$('body').on('click','#IsManageAccount', function() {
				SetManageAccount();
			}); 

			//Evaluation
           	$('body').on('keyup','.dteval_search',function(e){
                if(e.keyCode==13)
                {
                    $('#tblevaluation').DataTable().search($(this).val()).draw();
                }
            });

           	$('body').on('bind keyup change keypress','#DateInterviewed',function() {
				isFormValid('#formEvaluation');
			});

			$('body').on('bind keyup','#formEvaluation textarea',function() {
				isFormValid('#formEvaluation');
			});

			$('body').on('click','.editAppEval',function() {
				var xid = $(this).closest('tr').attr('data-id');
				var tr  = $(this).closest('tr');
				var rid = $(tr).find('[data-rrecommend]').attr('data-rrecommend');
				$('#formAppEval').append("<input type='hidden' id='eEntryID' name='EntryID' value='"+xid+"'/>");
				$('#formAppEval').find('#DateEvaluate').val($(tr).find('[data-edate]').attr('data-edate'));
				$('#formAppEval').find('#RemarkType').val($(tr).find('[data-rtype]').attr('data-rtype'));
				$('#formAppEval').find('[name="EvalRemarks"]').val($(tr).find('.eremarks').html());
				$('#formAppEval').find('[name="optrecommend"][value="'+rid+'"]').trigger('click');
			});
			
			$('body').on('click','.btnsave-evaluation',function() {
				if($('#tab-evaluate').is('.active')){
				   saveAppevaluate();
				}else if($('#tab-evalform').is('.active')){
				   saveEvaluation();
				}else if($('#tab-evalattach').is('.active')){
				   saveEAttachment();
				}
			});

            $('body').on('click','.a-select-evaluation',function() {
                var sel = $(this).closest('tr').find('.chk-child').val();
                $('.evalform').html(ajaxRequest(base_url+page_url+'event','event=editEvaluation&id='+sel,'HTML'));
                var button = $('.btnaction-evaluation');
                button.html("<i class='fa fa-check'></i> Update Evaluation");
                FN.datePicker('#DateInterviewed');
				setDateFormat('#DateInterviewed');
            });

            $('body').on('click','.btnreset-evaluation',function() {
                if($('#tab-evaluate').is('.active')){
				  loadAppEvaluate(0);
				}else{
				  var button = jQuery('.btnaction-evaluation'), form = '#formEvaluation';
                  clear_value(form); 
                  $(form).attr('data-id','');
                  $(form+' select[name="InterviewType"]').val($(form).attr('data-interview-type'));
                  $(form).find('span.help-block').addClass('hide');
                  button.html("<i class='fa fa-check'></i> Save Evaluation");
				}
            });

			//Committee
			$('body').on('keyup','.dtcomm_search',function(e){
                if(e.keyCode==13)
                {
                    $('#tblcommittee').DataTable().search($(this).val()).draw();
                }
            });

			$('body').on('click','.btnsave-committee',function() {
				saveCommittee();
			});	

			$('body').on('click','.a-select-committee',function() {
                var sel = $(this).closest('tr').find('.chk-child').val();
                $('.form-comm').html(ajaxRequest(base_url+page_url+'event','event=editCommittee&id='+sel,'HTML'));
                var button = $('.btnaction-committee');
                button.html("<i class='fa fa-check'></i> Update Remarks");
                Metronic.initUniform();
            });

            $('body').on('click','.btnreset-committee',function() {
                var button = jQuery('.btnaction-committee'), form = '#formCommittee';
                clear_value(form); 
                $(form).attr('data-id','');
                $(form+' .checker span').removeClass('checked');
                $(form+' input[type=checkbox]').removeAttr('checked');
                Metronic.initUniform();
                button.html("<i class='fa fa-check'></i> Save Remarks");
            });

            $('body').on('click','.btnremove',function(){
                if($('#tab-evaluate').is('.active')){
				  removeAppEvaluate();
				}else{
				  removeEvalComm($(this).attr('data-form'));
				}
            });

            $('body').on('click','.btnprint',function() {
    //         	var templ = 1, grdlvl = $('#gradeLevel option:selected').text().trim();
    //         	if ($('#applicationType option:selected').val() == window.btoa(1)) {
    //         		if(grdlvl != 'Kinder' && grdlvl != 'Grade 1') {
    //         			templ = 2;
    //         		}
    //         	} else {
    //         		templ = 3;
    //         	}
				// print_reports(base_url+page_url+'printApplication?AppNo='+getParameterByName('AppNo')+'&templ='+templ+'&age='+getAge($('#DateOfBirth').val()));
				print_reports(base_url+page_url+'printApplication?AppNo='+getParameterByName('AppNo'));
			});

			$('body').on('click','.btnattach-add',function() {
				$('.table-evalattach tbody tr.default').remove();
				$('.table-evalattach tbody').append($('#table-defaultevalattach tbody').html());
			});

			$('body').on('click','.btnattach-remove',function(){
				var _this = $(this), key = _this.closest('tr').attr('data-id');
				if (key == '' || key == undefined) {
					$(this).closest('tr').remove();
					msgbox('success',"Successfully Remove!");
				} else {
					bootbox.confirm({
			        size : 'small',
			        message : "Are you sure you want to remove this record?",
			            callback : function(yes) {
			               if ( yes ) {
			               		var response = ajaxRequest(base_url+page_url+'event','event=deleteAttachment&id='+key);
			               		if (response) {
			               			_this.closest('tr').remove();
			               			msgbox('success',"Successfully Remove!");
			               		}
			               }
			            }
			        }); 
			       
				}
				if ( $('.table-evalattach tbody tr.include').length == 0 ) {
                    $('.table-evalattach tbody').html("<tr class='default'><td class='center' colspan='6'>No Attachment Found!</td></tr>");
                }
               
			});

			$('body').on('change.bs.fileinput','.fileinput', function(){
	        	if (isValidFile($(this))) {
	            	if (typeof (FileReader) != "undefined") {
	        			var _file = $(this).parent().find("#fileUpload")[0].files[0];
        				// $(this).parent().parent().find('td .filetype').val(_file.type);
        				$(this).parent().parent().find('td .filetype').val(file_type);
        				$(this).parent().parent().find('td .filesize').val(_file.size);
	        		
	            	} else {
	            		$(this).parent().find('.fileinput-exists').trigger('click');
	                	msgbox('error', 'This features only support on latest browser.');
	            	}
	        	}
			});
	
			$('body').on('clear.bs.fileinput','.fileinput', function(){
				$(this).parent().parent().find('td .filetype').val('');
	        	$(this).parent().parent().find('td .filesize').val('');
			});

			$('body').on('click','.btnpreview',function() {
            	
				print_reports(base_url+page_url+'filePreview?attach_id='+$(this).closest('tr').attr('data-id'));
			});

		}	
	}
}();