var Listing = function () {
	var searchRec = function(el) {
		el.find('i').removeClass('fa-search');
		el.find('i').html('<img src="'+(base_url+'assets/system/media/images/loading-spinner-grey.gif')+'" height="20px" width="20px">');
		setAjaxRequest(base_url+page_url+'search',$('#FormTableApplicants').serialize(),
			function(result) {
				progressBar('off');
                applicationDataTableDestroy();
				$('#tableApplicationsWrapper').html(result);
                applicationDataTable();
			},
			'',
			'HTML',true
		);
	};
    
	var updateStatus = function(statusId) {
        
        if (getIds().length  <= 0) {
            return msgbox('error','There is no selected record.');
        }
        
        confirmEvent( 
            "Are you sure you want to update the status?",
            function(yes) {
                if (!yes) return;
                update(statusId);
            }
        );

        function update(statusId) {
            var result = ajaxRequest(base_url+page_url+'update-status','data='+JSON.stringify(getIds())+'&statusid='+statusId)
            if (result.error == false) {
                msgbox('success',result.message);
				$('#btnFilter').trigger('click');
            } else {
                msgbox('success',result.message);
            }
        }
        
        
        function getIds(){
            var data = new Array();
            $('#tableApplicants tbody > tr').each( function() {
                if ($(this).is('.active-row')) {
                    data.push(
                        $(this).find('td:nth-child(5)').html()
                    );
                }
            });
            $('#tableApplicants .chkspan').each( function() {
                if ($(this).is('.checked')) {
                    data.push(
                        $(this).attr('data-appno')
                    );
                }
            });
            return data;
        }  
    };

    var loadYearLevel = function(IsHigherLevel) {
        setAjaxRequest(base_url+page_url+'getYearLevel','IsHigherLevel='+IsHigherLevel,
            function(data) {
                var options = '<option value="0-'+IsHigherLevel+'">All Grade Levels</option>';
                for(var i in data) {
                    var option = document.createElement('option');
                    option.setAttribute('value',data[i].YearLevelID+'-'+IsHigherLevel);
                    option.appendChild(document.createTextNode(data[i].YearLevel));
                    options += option.outerHTML;
                }
                $('#gradeLevel').html(options);
                progressBar('off');
            }
        );
    }

    var showModalAdmissionStatus = function($e) {
        showModal({
            name: 'basic',
            title: 'Remarks',
            content: ajaxRequest(base_url+page_url+'event','event=modalRemarks' ,'HTML'),
            class: 'modal_status_remarks'
        });
    };

    var updatePaymentStatus = function(_this) {
        var ischeck = _this.is(":checked");
        // if (getAppNo().length  <= 0) {
        //     return msgbox('error','There is no selected record.');
        // }
        confirmEvent( 
            "Are you sure you want to update the payment status?",
            function(yes) {
                if (!yes) {
                    ischeck ? _this.parent().removeClass('checked') : _this.parent().addClass('checked');
                    _this.prop("checked", ischeck ? false : true);
                    return;
                }
                ajaxRequestLoader(base_url+page_url+'paymentStatus','data='+JSON.stringify(getAppNo())+'&ispaid='+ischeck, function(success_data)
                {
                    if ( success_data.error != undefined && success_data.error == false ) 
                    {
                        if (ischeck) {
                            var options = '';
                            options += EMAIL.createEmailOptions(atob(success_data.femail));
                            options += EMAIL.createEmailOptions(atob(success_data.memail));
                            options += EMAIL.createEmailOptions(atob(success_data.gemail));

                            $('.modal_email #RecepientsEmail').html(options);
                            $('.modal_email #RecepientsEmail').select2();
                            $('#sendSubject').val('Entrance Test Schedule');
                            $('.modal_email').modal('show');
                        }

                        msgbox('success',success_data.message);
                        $('#btnFilter').trigger('click');
                    }
                    hidePleaseWait();

                },function(error_data){
                    hidePleaseWait();
                    msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
                });
            }
        );
    };

    var getAppNo =  function(){
        var data = new Array();
        $('#tableApplicants tbody > tr').each( function() {
            if ($(this).is('.active-row')) {
                data.push(
                    $(this).find('td:nth-child(5)').html()
                );
            }
        });
        return data;
    };  

    var updateHasInterviewed = function(_this) {
        var ischeck = _this.is(":checked");
        // if (getAppNo().length  <= 0) {
        //     return msgbox('error','There is no selected record.');
        // }
        confirmEvent( 
            "Are you sure you want to update the interview inquiry status?",
            function(yes) {
                if (!yes) {
                    ischeck ? _this.parent().removeClass('checked') : _this.parent().addClass('checked');
                    _this.prop("checked", ischeck ? false : true);
                    return;
                }
                ajaxRequestLoader(base_url+page_url+'hasInterviewed','data='+JSON.stringify(getAppNo())+'&hasinterviewed='+ischeck, function(success_data)
                {
                    if ( success_data.error != undefined && success_data.error == false ) 
                    {
                        msgbox('success',success_data.message);
                        $('#btnFilter').trigger('click');
                    }
                    hidePleaseWait();

                },function(error_data){
                    hidePleaseWait();
                    msgbox('error',error_data.message != !undefined ? error_data.message : 'There was an error while saving'); 
                });
            }
        );
    };


    return {
        updateStatus: function(statusId) {
            updateStatus(statusId);
        },
        //main function to initiate the module
        init: function () {
            // loadYearLevel($('#IsHigherLevel').is(':checked') ? 1 : 0);
            ListingData.init();
                                                     
            $('#btnFilter').trigger('click');

            // searchRec($('.btnSearch'));
        	$('body').on('click','.btnSearch',function() {
        		searchRec($(this));
        	});

        	$('body').keydown(function(e) {
        		if (e.keyCode == 13) {
        			$('.btnSearch').trigger('click');
                    return false;
        		}
        	});

            $('#FormTableApplicants').submit(function() {
                return false;
            });

            $('body').on('click','.AppRowSel', function() {
                showRemarks($(this));
            });

            // $('#IsHigherLevel').click(function() {
            //     loadYearLevel($(this).is(':checked') ? 1 : 0);
            // });

            $('body').on('click', '.chk-ispaid', function() {
                var _this = $(this), ischeck = _this.is(':checked');
                setTimeout(function() {
                    updatePaymentStatus(_this);
                },0);
            });

            $('body').on('click', '.chk-hasinterviewed', function() {
                var _this = $(this), ischeck = _this.is(':checked');
                setTimeout(function() {
                    updateHasInterviewed(_this);
                },0);
            });

            $('body').on('click','.btn_send_email',function(){
                EMAIL.send();  
            });

        }
    };

}();

$(document).ready(function(){
    $('body').on('click','.chkapp',function(){
	   $(this).closest('tr').addClass('active-row');
	   alert('testing');
	});
	
	$('body').on('click','#tableFilter #btnFilter',function(){
		$('#tableApplicants').find('#btnFilter').trigger('click');
	});
	$('body').on('change','#tableFilter #schoolCampus',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#schoolCampus').val(tmpval);
	});
	$('body').on('change','#tableFilter #schoolYear',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#schoolYear').val(tmpval);
        $('#tableApplicants').find('#btnFilter').trigger('click');
	});
    $('body').on('change','#tableFilter #admissionStatus',function(){
        var tmpval = $(this).val();
        $('#tableApplicants').find('#admissionStatus').val(tmpval);
        $('#tableApplicants').find('#btnFilter').trigger('click');
    });
	$('body').on('change','#tableFilter #gradeLevel',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#gradeLevel').val(tmpval);
        $('#tableApplicants').find('#btnFilter').trigger('click');
	});
	
	$('body').on('change','#tableFilter #filter',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#filter').val(tmpval);
	});
	
	$('body').on('click','.btn-remove',function(){
		var appno = $(this).attr('data-id');
        confirmEvent( 
            "Are you sure you want to remove this entry?",
            function(yes) {
                if (!yes) return;
                ajaxRequestLoader(base_url+page_url+'removeApp','appno='+appno, function(rs){
				  $('#btnFilter').trigger('click');
                  hidePleaseWait();
                },function(error_data){
                    hidePleaseWait();
					alert('Error on deleting the item.');
                });				
            }
        );
	});
	
	$('body').on('click','.chkspan',function(){
	   var cnt   = 0;
	   var tmpuc = $(this).attr('data-uncheck');
	   if(!$(this).is('.checked')){
	     $(this).addClass('checked');
		 $(this).html('<i class="fa fa-check text-success"></i>');
	   }else{
	     $(this).removeClass('checked');
		 $(this).html(tmpuc);
	   }
	   
	   $('.chkspan').each(function(){
		 if($(this).is('.checked')){
		   cnt++;
		 }
	   });
	   
	   if(cnt>1){
		 $('.bulktools').removeClass('hidden');
	   }else{
	     $('.bulktools').addClass('hidden');
	   }
	});
});