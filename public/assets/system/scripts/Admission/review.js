 function review() {
	$('.accordionReview .panel').each(function() {
		
		var panel = $(this).find('.accordion-toggle').attr('href').split('_')[1];

		$(this).find('#collapse_'+panel +' .wrapper_body').html('<div class="col-md-12" style="padding: 3% !important;">'+$('.mainTabContent #'+panel+' .portlet-body').html()+'</div>');
		
		 if (panel == 'siblings') {
			$('#collapse_siblings').find('#tableSiblings').attr('id','tableSiblings_collapse');
			$('#collapse_siblings').find('#tableDefaultSibling').attr('id','tableDefaultSibling_collapse');
			var tableSiblings = $('#siblings #tableSiblings tbody');
			$('#collapse_siblings #tableSiblings_collapse thead tr th #btnAddSibling').remove();
			$('#collapse_siblings #tableSiblings_collapse tbody tr').each(function() {
				var trIndex = $(this).index();
				$(this).find('td').each(function() {
					var tdIndex = $(this).index();
					if (tdIndex > 0) {
						if ($(this).find('.form-control').is('input')) {
							$(this).html(tableSiblings.find('tr').eq(trIndex).find('td').eq(tdIndex).find('input').val());								
						} else {
							$(this).html(tableSiblings.find('tr').eq(trIndex).find('td').eq(tdIndex).find('select option:selected').text());
						}
					} else {
						$(this).html('');
					}
				});
			});

			$('.mainTabContent #'+panel+' .portlet-body form input:text').each(function() {
 				if (!$(this).hasClass('do-not-review')) {
					$('#collapse_siblings form input[name="'+$(this).attr('name')+'"]').attr('disabled', true);
					$('#collapse_siblings form input[name="'+$(this).attr('name')+'"]').val($(this).val());
				}
			});
		}
		else if (panel == 'references') {
			$('#collapse_references').find('#tableReferences').attr('id','tableReferences_collapse');
			$('#collapse_references').find('#tableDefaultReferences').attr('id','tableDefaultReferences_collapse');
			var tableReferences = $('#references #tableReferences tbody');
			$('#collapse_references #tableReferences_collapse thead tr th #btnAddReferences').remove();
			$('#collapse_references #tableReferences_collapse tbody tr').each(function() {
				var trIndex = $(this).index();
				$(this).find('td').each(function() {
					var tdIndex = $(this).index();
					if (tdIndex > 0) {
						if ($(this).find('.form-control').is('input')) {
							$(this).html(tableReferences.find('tr').eq(trIndex).find('td').eq(tdIndex).find('input').val());								
						} else {
							$(this).html(tableReferences.find('tr').eq(trIndex).find('td').eq(tdIndex).find('select option:selected').text());
						}
					} else {
						$(this).html('');
					}
				});
			});
		}
		// else if (panel == 'documents') {
		// 	$('#collapse_documents #tableDocuments tbody tr > td > .fileUpload > .upload').remove();
		// 	// $('#collapse_documents #tableDocuments tbody tr > td > .fileUpload').removeClass('btn-danger');
		// 	var i = 0;
		// 	$('#collapse_documents #tableDocuments tbody tr').each(function() {
		// 		var self = $(this);
		// 		var IsReviewed = $('#documents #tableDocuments tbody tr').eq(i).find('td .IsReviewed').is(':checked');
		// 		var IsExempted = $('#documents #tableDocuments tbody tr').eq(i).find('td .IsExempted').is(':checked');
		// 		self.find('td .IsReviewed').prop('checked',IsReviewed);
		// 		self.find('td .IsExempted').prop('checked',IsExempted);
		// 		self.find('td .IsReviewed').prop('disabled',true);
		// 		self.find('td .IsExempted').prop('disabled',true);
		// 		i++;
		// 	});

		// }
		else {

			var panelForm = $(this).find('#collapse_'+panel+ ' form');
			var collapseForm = panelForm.attr('id')+'_collapse';
			
			panelForm.attr('id',collapseForm);

 			$('.mainTabContent #'+panel+' .portlet-body form input:text').each(function() {
 				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('input[name="'+$(this).attr('name')+'"]').attr('disabled', true);
					panelForm.find('input[name="'+$(this).attr('name')+'"]').val($(this).val());
				}
			});

			$('.mainTabContent #'+panel+' .portlet-body form input[type="email"]').each(function() {
				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('input[name="'+$(this).attr('name')+'"]').attr('disabled', true);
					panelForm.find('input[name="'+$(this).attr('name')+'"]').val($(this).val());
				}
			});

			$('.mainTabContent #'+panel+' .portlet-body form select').each(function() {
				panelForm.find('select[name="'+$(this).attr('name')+'"]').attr('disabled', true);
				panelForm.find('select[name="'+$(this).attr('name')+'"]').val($(this).find('option:selected').val());
			});

			$('.mainTabContent #'+panel+' .portlet-body form input[type="number"]').each(function() {
 				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('input[name="'+$(this).attr('name')+'"]').attr('disabled', true);
					panelForm.find('input[name="'+$(this).attr('name')+'"]').val($(this).val());
				}
			});

			$('.mainTabContent #'+panel+' .portlet-body form textarea').each(function() {
 				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('textarea[name="'+$(this).attr('name')+'"]').attr('disabled', true);
					panelForm.find('textarea[name="'+$(this).attr('name')+'"]').val($(this).val());
				}
			});

			$('.mainTabContent #'+panel+' .portlet-body form input[type="checkbox"]').each(function() {
 				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('input[name="'+$(this).attr('name')+'"]').attr('disabled', true);
					panelForm.find('input[name="'+$(this).attr('name')+'"]').val($(this).val());
				}
			});

			if (panel == 'student') {
				$('#collapse_student .btn-file').remove();
			}

		}
	});
}