var GUARDIAN = function () {
	var handleSave = function () {

		var rules =  {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true
            },
          //city: {
          //    required: false
          //},
            resident: {
                required: true
            },
          //street: {
          //    required: true
          //},
            livingWith: {
                required: true
            },
            mobile: {
                required: true
            },
            // GuardianCity: {
            // 	required: true
            // },
            // BillingCity: {
            // 	required: true
            // },
            marital: {
            	required: true
            },
			IncludeGroup:{
			    required: true
			},
			datapriv:{
			    required: true
			}
            
        };
	             
	           
		$('#livingWith').change(function () {
			if ($(this).val() == 4) {
				$('#formGuardian').validate().element($('#livingWithOther')); //revalidate the chosen dropdown value and show error or success message for the input
			}
        });

         $('#formGuardian').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: rules,

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                },
					IncludeGroup:{
	                    required: "Please select an option."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
					    if($('#'+element.attr("name")+'-error').length>0){
						  $('#'+element.attr("name")+'-error').replaceWith(error);
						}else if(element.attr('name')=='datapriv' && element.prop('checked')==false){
						  msgbox('error','You need to agree to the data privacy policy');
						}else{
	                	  error.insertAfter(element);
						}
	                }
					$(element).closest('.form-group').addClass('has-error');
	            },

	            submitHandler: function (form) {
	                var label = $('.register-alert');
	                if (!validateForm()) return;
					
	                $('.has-error').removeClass('has-error');
	               //confirmEvent("Are you sure you want to save this?",function(yes) {
	               //	if (!yes) return;
	                	setAjaxRequest(base_url+page_url+'event','event=updateGuardian&'+$('#formGuardian').serialize()+'&GuardianCityID='+$('#GuardianCity option:selected').attr('data-id'),function(result) {
		                	if (result.error == true) {
		                		msgbox('error',result.message)
		                	} else {
		                		savePhoto(getInputVal('#FamilyID'));
		                	  //msgbox('success',result.message)
								$('[name="name"]').focus();
					            $('[href="#father-tab"]').trigger('click');
								$('[name="father-full-name"]').focus();
		                	} 
		                	progressBar('off');
		                }, function(error) {
	                		progressBar('off');
		                	msgbox('error','Failed to cache! Check your inputs!');
		                });
	               //});
	                return false;
	            }
	        });
	}

	var savePhoto = function(FamilyID) {
		if ($('#formGuardian #GuardianPhoto')[0] == undefined) return;
		var form_data = new FormData();
		form_data.append('guardianPhoto', $('#formGuardian #GuardianPhoto')[0].files[0]);
		$.ajax({
            url: base_url +page_url+'event?event=savePhoto&FamilyID='+FamilyID,
            dataType: "html",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            async: false,
            success: (
             	function(result) {
             		
              	}
             ),
       });
	};

	var toggleOtherCity = function(isCheck) {
		if (isCheck) {
			$('#otherCity').removeClass('hide');
			$('#otherCity').removeClass('not-required');
			$('#GuardianCity').addClass('hide');
			$('#GuardianCity').addClass('not-required');
			$('#GuardianCity').removeAttr('required');
		} else {
			$('#otherCity').addClass('hide');
			$('#otherCity').addClass('not-required');
			$('#GuardianCity').removeClass('hide');
			$('#GuardianCity').removeClass('not-required');
			$('#GuardianCity').attr('required',true);
		}
	}

	var toggleBillingOtherCity = function(isCheck) {
		if (isCheck) {
			$('#BillingOtherCity').removeClass('hide');
			$('#BillingOtherCity').removeClass('not-required');
			$('#BillingCity').addClass('hide');
			$('#BillingCity').addClass('not-required');
			$('#BillingCity').removeAttr('required');

		} else {
			$('#BillingOtherCity').addClass('hide');
			$('#BillingOtherCity').addClass('not-required');
			$('#BillingCity').removeClass('hide');
			$('#BillingCity').removeClass('not-required');
			$('#BillingCity').attr('required',true);
		}
	}

	var countryIcons = function(el) {
		var el = el == undefined ? '#GuardianCountry' : el;
		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='" +base_url + "assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $(el).select2({
            placeholder: "Select a Country",
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });
	}

	var getCity = function(code,CityEl) {
		var CityEl = CityEl != undefined ? CityEl : '#GuardianCity'
		var city = ajaxRequest(base_url+page_url+'event','event=getCity&code='+code);
		var optionSel = '', options = '';
		for(var i in city) {
			option = document.createElement('option');
			option.setAttribute('data-id',city[i].CityID);
			option.appendChild(document.createTextNode(city[i].City));
			options += option.outerHTML;
		}

		$(CityEl).html(options);
		$(CityEl).select2();
	}

	var copyAdddress = function() {
		var form = '#formGuardian';

		var resident = $(form).find('input[name="resident"]').val();
		var street = $(form).find('input[name="street"]').val();
		var barangay = $(form).find('input[name="barangay"]').val();
		var GuardianCountry = $(form +' #GuardianCountry option:selected').val();
		var isOtherCity = $(form).find('input[name="isOtherCity"]').is(':checked') ? 1 : 0;
		var otherCity = $(form).find('input[name="otherCity"]').val();
		var GuardianCity = $(form).find('#GuardianCity option:selected').val();
		var province = $(form).find('input[name="province"]').val();
		var zipcode = $(form).find('input[name="zipcode"]').val();

		$(form).find('input[name="BillingResident"]').val(resident);
		$(form).find('input[name="BillingStreet"]').val(street);
		$(form).find('input[name="BillingBrangay"]').val(barangay);
		$(form).find('input[name="BillingGuardianCountry"]').val(GuardianCountry);
		$(form).find('input[name="BillingOtherCity"]').val(otherCity);

		$(form).find('input[name="BillingProvince"]').val(province);
		$(form).find('input[name="BillingZipCode"]').val(zipcode);

		$('#BillingIsOtherCity').prop('checked',isOtherCity);
		toggleBillingOtherCity(isOtherCity);

		$('#BillingCountry').select2('val',[GuardianCountry]);
		$('#BillingCountry').trigger('change');

		$('#BillingCity').select2('val',[GuardianCity]);
		$('#BillingCity').trigger('change');

		$(':checkbox').uniform();
	}

	var validateForm = function() {
		var isvalid = true;
		$('#otherCity,#BillingOtherCity').removeAttr('style');
		if ($('#isOtherCity').is(':checked')) {
			if (!$('#otherCity').val()) {
				$('#otherCity').attr('style','border: 1px solid red');
				isvalid = false;
			}
		}
		if ($('#BillingIsOtherCity').is(':checked')) {
			if (!$('#BillingOtherCity').val()) {
				$('#BillingOtherCity').attr('style','border: 1px solid red');
				isvalid = false;
			}
		}
		if (!isvalid) {
			msgbox("error","Please complete all required fields");
		}
		return isvalid;
	}

   
    return {
        //main function to initiate the module
        init: function () {

        	countryIcons('#BillingCountry');
        	countryIcons();

        	toggleOtherCity($('#isOtherCity').is(':checked'));
        	toggleBillingOtherCity($('#BillingIsOtherCity').is(':checked'));

        	handleSave();
			
			if($('[name="IncludeGroup"]').val()==undefined){
				$('#IncludeGroup-error').removeClass('hidden');
			}

        	$(window).on('load',function(){
        	$('#privacymodal').modal('show');
    		});

        	$('body').on('click','#isOtherCity',function() {
				toggleOtherCity($(this).is(':checked'));
			});

			$('body').on('change','#GuardianCountry',function() {
				getCity($(this).val());
			});
			
			$('body').on('change','#BillingCountry',function() {
				getCity($(this).find('option:selected').val(),'#BillingCity');
			});	

			$('body').on('change','#BillingCity',function() {
				$('#BillingCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('body').on('change','#GuardianCity',function() {
				$('#GuardianCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('body').on('click','#BillingIsOtherCity',function() {
				toggleBillingOtherCity($(this).is(':checked'));
			});
			
			

			$('body').on('click','#CopyAddress', function() {
				if ($(this).is(':checked')) {
					copyAdddress();
				}
			});
        }

    };

}();

function xformvalidation(form){
 var result = true;
 var pos    = false;
 $(form).find('[required]').each(function(){
   var elemval='';
   if($(this).is('input[type="text"]') || $(this).is('input[type="date"]') || $(this).is('select'))
	elemval = $(this).val();
   else
	elemval = $(this).html();
	
   if($(this).is('input[type="checkbox"]') || $(this).is('input[type="radio"]')){
    elemval = $('[name="'+$(this).attr('name')+'"]:checked').length;  
   }

   $(this).closest('.form-group').removeClass('has-success');
   $(this).closest('.form-group').removeClass('has-error');
   $(this).closest('.form-group').find('.help-block').html('');
   //alert(elemval);
   if(elemval=='' || elemval==undefined || elemval==null || elemval=='-1' || elemval==-1){
	$(this).closest('.form-group').addClass('has-error');
	$(this).closest('.form-group').find('.help-block').html('This field is required');
	if(pos==false){
	  msgbox('error','Please complete all required fields');
	  pos = true;
	}
	result = false;
   }else
	$(this).closest('.form-group').addClass('has-success');
 });

 return result;
}

$('document').ready(function(){
   $('body').on('click','#submitFather',function(){
		var label = $('.register-alert');
		if (!xformvalidation('#formFather')) return;
		//confirmEvent("Are you sure you want to save this?",function(yes) {
		//	if (!yes) return;
			setAjaxRequest(base_url+page_url+'event','event=updateFather&'+$('#formFather').serialize(),
			function(result) {
				if (result.error == true) {
					msgbox('error',result.message)
				} else {
				  //msgbox('success',result.message)
					$('[name="father-full-name"]').focus();
					window.scrollTo(0,0);
					$('[href="#mother-tab"]').trigger('click');
					$('[name="mother-full-name"]').focus();
				} 
				progressBar('off');
			}, function(error) {
				progressBar('off');
		        msgbox('error','Failed to cache! Check your inputs!');
			});
		//});
		return false;
   });
	
   $('body').on('click','#submitMother',function(){
		var label = $('.register-alert');
		if (!xformvalidation('#formMother')) return;
		//confirmEvent("Are you sure you want to save this?",function(yes) {
		//	if (!yes) return;
			setAjaxRequest(base_url+page_url+'event','event=updateMother&'+$('#formMother').serialize(),
			function(result) {
				if (result.error == true) {
					msgbox('error',result.message)
				} else {
				  //msgbox('success',result.message)
					$('[name="mother-full-name"]').focus();
					window.scrollTo(0,0);
					$('[href="#emergency-tab"]').trigger('click');
				} 
				progressBar('off');
			}, function(error) {
				progressBar('off');
		        msgbox('error','Failed to cache! Check your inputs!');
			});
		//});
		return false;
	});
	
	
   $('body').on('click','#submitEmergency',function(){
		var label = $('.register-alert');
		if (!xformvalidation('#formEmergency')) return;
		confirmEvent("Are you sure you want to save this?",function(yes) {
			if (!yes) return;
			setAjaxRequest(base_url+page_url+'event','event=updateEmergency&'+$('#formEmergency').serialize(),
			function(result) {
				if (result.error == true) {
					msgbox('error',result.message)
				} else {
					msgbox('success',result.message)
					window.scrollTo(0,0);
					$('[href="#guardian-tab"]').trigger('click');
				} 
				progressBar('off');
			}, function(error) {
				progressBar('off');
		        msgbox('error','Failed to cache! Check your inputs!');
			});
		});
		return false;
	});
   
   $('body').on('click','.btnprev',function(){
		var tmpval = $(this).attr("data-previous");
		$('[href="'+tmpval+'"]').trigger('click');
		$(tmpval).find('input[type="text"]').first().focus();
   });
					
   
   $('body').on('click','.dataprivacy',function(){
      $('#modal_dprivacy').modal('show');
   });   
   
   if($('.menu-toggler').is(':visible')==false){
     $('.datepicker').datepicker();
   }else{
     console.log('mobile');
   }
   
   $('#modal_dprivacy').modal('show');
});