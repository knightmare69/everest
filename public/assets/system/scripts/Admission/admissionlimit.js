function loadlist(){
	var term  = $('#ayterm').val();
	var campus= $('#campus').val();
	
	if(term>0 && campus>0){
	  $('#tblimit').find('tbody').html('');
	  $('#tblimit').find('tfoot').removeClass('hidden');
      progressBar('on');
      setAjaxRequest(
		base_url+'admission-limit/txn?event=list',{termid:term,campusid:campus},
		function(r){
		  if(r.success){
	        $('#tblimit').find('tfoot').addClass('hidden');
			$('#tblimit tbody').html(r.content);
		  }else{
			confirmEvent(('<i class="fa fa-warning text-danger"></i> '+r.message+'!'),function(){return;});	
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	  );
	}
}

$('document').ready(function(){
	loadlist();
	
	$('body').on('change','#ayterm,#campus',function(){
		loadlist();
	});
	
	$('body').on('click','.btncancel',function(){
	    loadlist();
	});
	
	$('body').on('click','.btnsave',function(){
	    var term  = $('#ayterm').val();
	    var campus= $('#campus').val();
		var tmpstr='';
		var i=1;
		$('.txtquota').each(function(){
		    var tmprow  = $(this).closest('tr');
			var college = $(tmprow).attr('data-college');
			var program = $(tmprow).attr('data-prog');
			var major   = $(tmprow).attr('data-major');
			var limit   = parseInt($(this).val());
			tmpstr += i+'<c>'+term+'<c>'+campus+'<c>'+college+'<c>'+program+'<c>'+major+'<c>'+limit+'<n>'; 
			i++;
		});
		
		if(term>0 && campus>0){
		  progressBar('on');
		  setAjaxRequest(
			base_url+'admission-limit/txn?event=save',{xdata:tmpstr},
			function(r){
			  if(r.success){
				msgbox('success','Success');
			  }else{
				msgbox('error',r.message);
			  }
			  
			  progressBar('off');
			},
			function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
			  progressBar('off');
			}
		  );
		}
	});
	
	
});