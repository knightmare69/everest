var Connect = function(){
	var page_url = 'subscription/';
	var loadFilter = function(data) {
		var filter = $('#TableFilterResult tbody');
		
		var data = data[0];

		$('#ChildPhoto').attr('src',base_url+page_url+'getPhoto?isOld='+(data.isOld)+'&key='+data.ChilNo+'&ids='+Math.random());

		if (data.isOld == '1') {
			filter.find('#ChildNo').html('Student No.');
		} else {
			filter.find('#ChildNo').html('App No.');
		}

		$('#isOld').val(data.isOld);

		filter.find('#AppNo').html(data.ChilNo);
		filter.find('#name').html(data.name);
		filter.find('#YearLevel').html(data.YearLevel == null ? 'None' : data.YearLevel);
		filter.find('#Gender').html(data.Gender);
		filter.find('#BirthDate').html(data.DateOfBirth);

		if (data.isOld == '1') {
			if (data.isConnected == '1') {
				return isConnected();
			} 
		} else {
			if (data.parentConnected == '1' && data.isConnected == '1') {
				return isConnected();
			}
		}
		return isNowConnecting();
	}

	var connectChild = function(el) {
		var filter = $('#TableFilterResult tbody');
		el.html('<i class="fa fa-spinner fa-spin"></i> loading... ');
        
		setAjaxRequest(
			base_url+page_url+'event', 'event=connectChild&ChildNo='+ $('#idno').val(),
			function(result) {
				if (result.error == false) {
					msgbox('success',result.message);
					isConnected();
				} else {
					connectAgain();
					msgbox('error',result.message ? result.message : 'There was an error occured.');
				}
				progressBar('off');
                el.html('<i class="fa fa-search"></i> Search ');
			},
			function() {
				connectAgain();
				progressBar('off');
			}
		)
	}


	var showModalConnect = function(el) {
		showModal({
			name: 'basic',
	        title : 'Connect to your child',
	        content: ajaxRequest(base_url+page_url+'event','event=showModalPhoto&key='+el.closest('tr').attr('data-id'),'html'),
	        class: 'modal_childconnect',
	       	hasFooter: false
	    });

	    $('#GuardianConnectDOB').inputmask("d/m/y", {
	        "placeholder": "dd/mm/yyyy"
	    });
	}

	var toggleForm = function(isShowFilter) {
		if (isShowFilter) {
			$('.filter')
				.animate({'opacity': 'show'},'slow')
				.removeClass('hide');
			$('.result').addClass('hide');
		} else {
			$('.result')
				.animate({'opacity': 'show'},'slow')
				.removeClass('hide');
			$('.filter').addClass('hide');
		}
	}

	return {
		init: function() {
			FN.datePicker();
            
			$('body').on('click','#btnsearch', function() {
				var self = $(this);
                var id = $('#idno').val();                
				
                if( id == '' ){				    
				    alert('Please input idno to search');
                    return false;
				}
                
				self.html('<i class="fa fa-spinner fa-spin"></i> loading... ');
				
				setAjaxRequest(base_url+page_url+'event','event=search&idno='+id,
					function(e) {
						if(e.error ) {
						   $('#studentname').html(e.message);
						} else {
							$('#studentname').html(e.fullname);
                            $('#yrlvl').html(e.yearlevel);
						}
						progressBar('off');
                        self.html('<i class="fa fa-search"></i> Search ');
					}
				);
			});

			$('body').on('click','#btnrequest', function() {
				var self = $(this);
				confirmEvent("Are you sure you want to connect to this child?", function(yes) {
					if (!yes) return;
					connectChild(self);
				});
			});

			$('body').on('click','.showFilter', function() {
				toggleForm(1);
			});

			$('body').on('click','#GuardianConnect', function() {
				showModalConnect($(this));
			});			
		}
	}
}();

function validForm() {
	var isValid = true;
	$('#ConnectChildForm input:text').each(function() {
		$(this).removeAttr('style');
		if (!$(this).hasClass('not-required')) {
			if ($(this).hasClass('form-control') && $(this).val() == '')  {
	            $(this).attr('style','border: 1px solid red');
	            isValid = false;
	        }
		}
   });
	return isValid;
}

function isConnected() {
	$('#BtnConnectChild').html('<i class="fa fa-check"></i> Connected');
	$('#BtnConnectChild').prop('disabled',true);
}

function connectAgain() {
	$('#BtnConnectChild').html('<i class="fa fa-arrow-right"></i> Connect again.');
	$('#BtnConnectChild').prop('disabled',false);
}

function isNowConnecting() {
	$('#BtnConnectChild').html('<i class="fa fa-arrow-right"></i> Connect');
	$('#BtnConnectChild').prop('disabled',false);
}

function showNoResultSearchBtn() {
	$('#BtnSearchConnectChild').html('<i class="fa fa-search"></i> There\'s no result found. Please try again.');
}

function showHasResultSearchBtn() {
	$('#BtnSearchConnectChild').html('<i class="fa fa-search"></i> Search');
}