

$(document).ready(function() {

    $('body').on('click','#update_selected_or', function(e){
        var entryid_arr = [];
        entryid_arr= [1,2,3,4];
        var termid_or;
        var checkedUpdateOR = jQuery('#checkUpdateOR').is(":checked");
        var checkedDistributeTerms = jQuery('#checkDistributeTerms').is(":checked");
        var checkedpost_payment = jQuery('#post_payment').is(":checked");
        var checked_nonledger = jQuery('#checknonledger').is(":checked");


        var postpayment = $('#post_payment:checkbox:checked').length > 0;
        var  postpayment_ =(checkedpost_payment ?  0:1);
        var checkUpdateOR = (checkedUpdateOR ? 1:0);
        var checkDistributeTerms =(checkedDistributeTerms? 1:0);
        var checknonledger = (checked_nonledger? 1 :0)
        if(checknonledger == 1){
            $('#or_tbl  tbody  tr td input.checkboxes').each(function(i, chk) {
                entryid_arr.push(eval($(this).parent().parent().attr('data-id')));
            });
        }

         termid_or = $('#reg_no').val();
         console.log('trerm'+termid_or);
        //Mapping Begin
        if(checknonledger == 0){
        $('#or_tbl  tbody  tr td input.checkboxes').each(function(i, chk) {
            if (chk.checked) {
                var new_type =  $(this).parent().parent().attr('data-newtype');
                var new_refno =  $(this).parent().parent().attr('data-newrefno');
                var new_entryid =  $(this).parent().parent().attr('data-newentryid');
                var entryid = $(this).parent().parent().attr('data-id');
                var trnenrtyid =  $(this).parent().parent().attr('data-trnentryid');
                var trnrefno =  $(this).parent().parent().attr('data-refno');
                var new_trnidtext =  $(this).parent().siblings('.newentryid').text();
                console.log('jay'+new_trnidtext);



                $.ajax({
                    url: base_url + page_url + 'event?event=referencing_or',
                    type: 'POST',
                    data: {
                        new_type:new_type,
                        new_refno:new_refno,
                        new_entryid:new_entryid,
                        entryid:entryid,
                        trnenrtyid:trnenrtyid,
                        trnrefno:trnrefno,
                        new_trnidtext: new_trnidtext,
                        checkDistributeTerms:checkDistributeTerms,
                        termid: termid_or,
                    },
                    dataType: 'json',
                    cache: false,

                    success: function(data) {



                    }
                });
    }
        });
    }
        //MAPPING END
        // var checkUpdateOR = (checkedUpdateOR ? 1:0);
        // var checkDistributeTerms =(checkedDistributeTerms? 1:0);
        // var checknonledger = (checked_nonledger? 1 :0)
        $.ajax({
            url: base_url + page_url + 'event?event=update_or',
            type: 'POST',
            data: {
                post_payment: postpayment_,
                or_no: transid,
                checknonledger:checknonledger,
                entryid_arr: entryid_arr,
                selected_termid:selected_termid,
                checkUpdateOR: checkUpdateOR,
                termid:termid_or,
            },
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                progressBar('on');
            },
            success: function(req) {
                var data =  req.ledger;
                //console.log(req);

                if(req.error == false){
				    msgbox('success', req.message);
				    location.reload();

                }else {
                    msgbox('error', req.message);
                }

            }
        });



    });

    $('body').on('change','#post_payment', function(e){
       if( $(this).parent().hasClass('checked')){
           $('#post_payment').attr("checked", true);
       }else{
           $('#post_payment').attr("checked", false);
       }

    });

    $('body').on('click', '#assess_tbl tr', function(e){
        $('#assess_tbl tr').removeClass('danger');
        $('#assess_tbl tr').removeClass('success');
        $(this).addClass('danger');


    });
    $('body').on('click', '#or_tbl tr', function(e){
        var account = $(this).attr('data-account');
        var or_newentryid = $(this).attr('data-newentryid');
        var or_entryid = $(this).attr('data-entry');
        var index = $("#assess_tbl tr.active-row").index();


        $('#assess_tbl tr').removeClass('active-row');

        $('#assess_tbl tr').removeAttr('id');
        //assess_tbl
        $('#assess_tbl tr[data-entryid="'+or_newentryid+'"]').addClass('active-row');
    //    $('#assess_tbl tr.active-row').focus();


        var as_entryid =$("#assess_tbl tr.active-row").attr('data-entryid');
        console.log(or_newentryid+'='+as_entryid);
        if( or_newentryid === as_entryid || or_entryid === as_entryid){
            document.getElementById("map").disabled = true;
            document.getElementById("unmap").disabled = false;
            $('#or_tbl tr').removeClass('active-row');
            $('#assess_tbl tr').removeClass('danger');
            $("#assess_tbl tr.active-row").attr('id','selected');
            var el = document.getElementById('selected');
            el.scrollIntoView(true);
            $('#assess_tbl tr').removeClass('success');
            $("#assess_tbl tr.active-row").addClass('success');
            $('#assess_tbl tr').removeClass('active-row');
            $('#or_tbl tr').removeClass('success')
            $(this).addClass('success')
            console.log('found haha');

        }else{
            document.getElementById("unmap").disabled = true;
            document.getElementById("map").disabled = false;
            $('#or_tbl tr').removeClass('success')
            $('#assess_tbl tr').removeClass('success');
            $('#assess_tbl tr[data-account="'+account+'"]').addClass('active-row');
            $("#assess_tbl tr.active-row").attr('id','selected');
            var el = document.getElementById('selected');
            el.scrollIntoView(true);
            $('#assess_tbl tr').removeClass('danger');
            $("#assess_tbl tr.active-row").addClass('danger');
            $('#assess_tbl tr').removeClass('active-row');

        }

    });

    $('body').on('click','#map', function(e){
     var index = $("#or_tbl tr.active-row").data('id');
     var nt = $('#assess_tbl tr.danger').attr('data-type');
     var ne = $('#assess_tbl tr.danger').attr('data-entryid');
     var rn = $('#assess_tbl tr.danger').attr('data-refno');
     $('#or_tbl tr.active-row td.newtype').text(nt);
     $('#or_tbl tr.active-row td.newrefno').text(rn);
     $('#or_tbl tr.active-row td.newentryid').text(ne);
     $('#or_tbl tbody tr.active-row td input.checkboxes').prop('checked', true);

     $('#or_tbl tr.active-row').attr('data-newtype',nt);
     $('#or_tbl tr.active-row').attr('data-newentryid',ne);
     $('#or_tbl tr.active-row').attr('data-newrefno',rn);
     $('#or_tbl tr.active-row').addClass('success');
     $('#assess_tbl tr.danger').addClass('success');
     $('#assess_tbl tr.success').removeClass('danger');


    console.log(index);
    document.getElementById("map").disabled = true;
    document.getElementById("unmap").disabled = false;




    });
    $('body').on('click','#unmap', function(e){
        console.log('unmap')
        $('#or_tbl tr.success').attr('data-newtype','#Non-Ledger#');
        $('#or_tbl tr.success').attr('data-newentryid','#Non-Ledger#');
        $('#or_tbl tr.success').attr('data-newentry','#Non-Ledger#');
        $('#or_tbl tr.success td.newtype').text(0);
        $('#or_tbl tr.success td.newrefno').text('#Non-Ledger#');
        $('#or_tbl tr.success td.newentryid').text('#Non-Ledger#');
        $('#or_tbl tbody tr.success td input.checkboxes').prop('checked', true);
        $('#or_tbl tr.success').addClass('active-row');
        $('#or_tbl tr.success').removeClass('success');

        $('#assess_tbl tr.success').addClass('danger');
        $('#assess_tbl tr.success').removeClass('success');
        document.getElementById("unmap").disabled = true;
        document.getElementById("map").disabled = false;




    });


    $('body').on('change', '#checknonledger', function () {
        console.log('haha change');
        //$(this).parents('tr').toggleClass("active");
        var checked = jQuery(this).is(":checked");
        if (checked) {
        //$('#reg_no').html('');
    //    $('#assess_type').html('');
        document.getElementById("reg_no").disabled = true;
        document.getElementById("assess_type").disabled = true;
            //$(this).parents('tr').addClass("active");
        } else {
    $('#reg_no').html(zz);
    $('#assess_type').html(zzz);
    document.getElementById("reg_no").disabled =false;
    document.getElementById("assess_type").disabled = false;
        //    $(this).parents('tr').removeClass("active");
        }

    });

    $('body').on('change', 'table thead tr th input.group-checkable', function () {
        console.log('find grtop');
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).attr("checked", true);
                //$(this).parents('tr').addClass("active");
            } else {
                $(this).attr("checked", false);
            //    $(this).parents('tr').removeClass("active");
            }
        });
        jQuery.uniform.update(set);
    });
    $('body').find('#or_tbl thead tr th input.group-checkable').change(function () {

    });





$('#or_tbl > tbody  > tr').each(function() {
    console.log('haha');
});

});



function orChange(){
    console.log('haha changed')
 var refno_or =    $("#reg_no").find(':selected').data('refnum');

 console.log(refno_or);
 $.ajax({
     url: base_url + page_url + 'event?event=on_or_change',
     type: 'POST',
     data: {
         or_refno: refno_or
     },
     dataType: 'json',
     cache: false,
     beforeSend: function() {
         progressBar('on');
     },
     success: function(data) {
         var assess_data2 = data.assess;
         assess_table(assess_data2);
         progressBar('off');


     }
 });
}

function assess_table(assess_data){



                            var yy = '';
                            yy += '';
                            yy += '     <table id="assess_tbl" class="table table-striped table-bordered table-hover">';
                            yy += '     <thead class="flip-content">';
                            yy += '     <tr>';
                            yy += '         <th width="20%"> TRNID </th>';
                            yy += '         <th> Account </th>';
                            yy += '         <th > Balance </th>';
                            yy += '         <th> Assessed </th>';
                            yy += '         <th> TotalPayment </th>';
                            yy += '         <th> TransID </th>';
                            yy += '         <th width="20%"> ReferenceNo </th>';
                            yy += '         <th> AccountID </th>';
                            yy += '     </tr>';
                            yy += '     </thead>';
                            yy += '     <tbody>';
                            var balance_total =0;
                            var assess_total=0;
                            var payment_total= 0;
                            $.each(assess_data, function(key, val) {
                                //balance_total =

                                //  var assessfee =  assess_data[key].ActualPayment;
                                var payment = (assess_data[key].first > 0 ? assess_data[key].first : 0) + (assess_data[key].second > 0 ? assess_data[key].second : 0) + (assess_data[key].third > 0 ? assess_data[key].third : 0) + (assess_data[key].fourth > 0 ? assess_data[key].fourth : 0) + (assess_data[key].fifth > 0 ? assess_data[key].fifth : 0);
                                    payment = parseFloat(payment);
								var assessfee = assess_data[key].Debit - 0.0000;
                                var str = assess_data[key].TransRefNo;
                                var arr = str.split(';'); //split into array
                                // console.log('array'+arr);
                                yy += '        <tr data-account="'+assess_data[key].AcctName+'" data-refno="' + assess_data[key].ReferenceNo + '" data-entryid="' + assess_data[key].EntryID + '"  data-type="' + assess_data[key].TransID + '">';
                                yy += '                  <td>' + assess_data[key].EntryID + '</td> ';
                                yy += '                  <td>' + assess_data[key].AcctName + '</td> ';
                                yy += '                  <td data-pay="'+payment+'"  data-ass="'+assessfee+'">' + ReplaceNumberWithCommas(payment - assessfee) + '</td> ';
                                yy += '                  <td>' + ReplaceNumberWithCommas(assessfee) + '</td> ';
                                yy += '                  <td>' + ReplaceNumberWithCommas(payment) + '</td> ';
                                yy += '                  <td> ' + assess_data[key].TransID + ' </td> ';
                                yy += '                  <td>' + assess_data[key].ReferenceNo + '</td> ';
                                yy += '                  <td>' + assess_data[key].AccountID + ' </td> ';
                                yy += '         </tr>';
                                assess_total = assess_total + assessfee;
                            });
                            yy += '     </tbody>';
                            yy += '     </table>';
                            $('#assessment_div_tbl').html(yy);
                            $('#assess_total').text('Balance: '+ ReplaceNumberWithCommas(balance_total) +' / Assessed: '+ReplaceNumberWithCommas(assess_total) +' / Payment: '+ReplaceNumberWithCommas(payment_total));
                            console.log('ass'+assess_total);
                            yy = '';

                            $('#assess_tbl').DataTable({
                                "scrollY": 300,
                                "searching": false,
                                "paging": false,
                                "info": false,
                                //    "ordering":false,
                                "scrollX": true
                            });
}
