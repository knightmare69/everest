"use strict";
var transid = 0;
var posted = null;
var stud_number ='';
var full_name= '';
var transcode= null;
var or_refno = 0;
var selected_termid= null;
var prevt=0;
    
    var zzz= '';


 zzz+=  '  <option>Enrollment</option>';
 zzz+=  '    <option>Add/Drop/Change Subject</option>';
 zzz+=  '    <option>Transcript of Record</option>';
 zzz+=  '    <option>Thesis</option>';
 zzz+=  '    <option>Admission/Testing</option>';
 zzz+=  '    <option>Other Assessment</option>';
 zzz+=  '    <option>Beginning Balance</option>';



var modify_or_modal = $('#modify_or_modal');
var Students = {
    get: function(params) {
        //var term = $('#search-ac-year').select2('data').text,
        //     yl = $('#search-level').select2('data').text;

        setAjaxRequest(
            base_url + page_url + 'event?event=get', params,
            function(req) {
                if (req.error == false) {

                    showModal({
                        name: 'basic',
                        title: 'Searched Student(s)',
                        content: req.list,
                    });

                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#students-table-res').DataTable({
                        'bDestroy': true,
                        'aaSorting': []
                    });

                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },

    info: function(stud_no, info_type) {
        stud_no = (stud_no != '' || stud_no != 'undefined') ? stud_no : 0;

        setAjaxRequest(
            base_url + page_url + 'event', 'event=get_info&student_no=' + stud_no + '&form=' + info_type,
            function(req) {
                if (req.error == false) {

                    progressBar('off');
                    animate_form(req.form);

                    if (info_type == 'profile') {
                        $('.div-blocker').addClass('hide');
                        $('.profile-usertitle-name #full-name').text(req.name);
                        $('.profile-usertitle-job').text(req.stud_num);
                        $('.profile-yl').html(req.year_level);
                        $('#profile-photo').attr('src', req.photo);
                        $('#profile-upload-photo').attr('data-id', req.stud_num_e);

                        if (req.chinese_name != '') {
                            $('#ch-name').text(req.chinese_name);
                        } else {
                            $('#ch-name').text('');
                        }
                    }

                    return true;
                    // $('#profile-container').html(req.profile);
                    // $('.select2-profile').select2({
                    //     'width': '100%'
                    // })
                    // reload_elements();
                } else {
                    msgbox('error', req.message);
                }
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },



    search_list: function() {

        setAjaxRequest(
            base_url + page_url + 'event', 'event=search&data_search=' + vars.searched_string + '&filters=' + JSON.stringify(vars.filter_stacked),
            function(req) {
                if (req.error == false) {
                    $('.div-stud-list').addClass('hide');
                    $('.div-stud-search-res').removeClass('hide').find('.general-item-list').html(req.list);
                    Students.get_students_photo();
                } else {
                    msgbox('error', req.message);
                }
            }, '', '', true
        );

        progressBar('off');
    },

    get_students_photo: function() {
        $('.general-item-list li a').each(function(e) {
            var stud_no = $(this).attr('data-id');
            var stud_img = $(this).prev();
            var photo = setAjaxRequest(base_url + page_url + 'event', 'event=get_photo&student_no=' + stud_no, function(ret) {
                if (ret.photo != false) {
                    stud_img.attr('src', ret.photo);
                }
            }, '', '', true);
        });
        progressBar('off');
    },


}

var vars = {
    filter_stacked: {},
    save_filter_vars: function(form_id) {

        var data = $('#' + form_id).serializeArray();
        for (var a = 0; a < data.length; a++) {
            var _this = data[a];
            this.filter_stacked[_this.name] = _this.value;
        }

        if(this.filter_stacked['search-prog']){
            this.filter_stacked['search-prog'] = $('#search-level').select2('data').element[0].attributes[1].nodeValue;
        }

    },
    student_no: 0,
    family_id: 0,
    searched_string: '',
}
var getStudData = function(e) {
    var _id = 'search-form',
        data = $('#' + _id).serialize();

    // if ($('#search-ac-year').val() && $('#search-level').val()) {
    vars.save_filter_vars(_id);
    Students.get(vars.filter_stacked);
    // } else {z
    // msgbox('warning', 'Please provide a school year/semester with level.');
    // }
};

function ReplaceNumberWithCommas(yourNumber) {
    //Seperates the components of the number
    var n = Number(parseFloat(yourNumber).toFixed(2)).toLocaleString('en', {
    minimumFractionDigits: 2
});
    //Comma-fies the first part

    //Combines the two sections
    return n;
}

function studentledger(data,sname,student_number){
    var length = data.length;
        zz = '';
    //console.log(length);


    var color =['success','info', 'warning','success','info', 'warning','success','info', 'warning'];

    var xy = '';
    xy += '<table id="student_ledger_tbl" class="table table-striped table-bordered table-advance table-hover" >';
    xy += '<thead>';
    xy += '<tr>';
    // xy += '<th style="text-align:center;">Academic Year and Term </th>';
    xy += '<th >TransDate</th>';
    xy += '<th>TransCode</th>';
    xy += '<th>ReferenceNo</th>';
    xy += '<th>Debit</th>';
    xy += '<th>Credit</th>';
    xy += '<th>Balance</th>';
    xy += '<th>Remarks</th>';
    xy += '<th>Posted</th>';
    xy += '<th>DatePosted</th>';
    xy += '</tr>';
    xy += '</thead>';
    xy += '<tbody>';
    var groups = Object.create(null);

    for (var i = 0; i < data.length; i++) {
        var item = data[i];
        if (!groups[item.AcademicYearTerm]) {
            groups[item.AcademicYearTerm] = [];
        }
        var xs = item.AcademicYearTerm;
        //console.log(xs);
        groups[item.AcademicYearTerm].push({
            TransDate: item.TransDate,
            TransCode: item.TransCode,
            ReferenceNo: item.ReferenceNo,
            Debit: item.Debit,
            Credit: item.Credit,
            Remarks: item.Remarks,
            Balance: item.Balance,
            Posted: item.Posted,
            PostedDate: item.PostedDate,
            FinancialAid: item.FinancialAid,
            AssessFee: item.AssessFee,
            NonLedger: item.NonLedger,
            DMCMRefNo: item.DMCMRefNo,
            ORRefNo: item.ORRefNo,
            TermID: item.TermID
        });
    }
    var totalb =0;
    var result = [];
    var led_r = 0;
    var termb = 0;
    var termc= 0;
        for (var x in groups) {
            var obj = {};
            obj[x] = groups[x];
            var ledger_array = $.map(obj, function(value, index) {
                return [value];
            });

            xy += '<tr >';
            xy += '<td   style:"text-align:left;" colspan="9" >';
            xy += '<span class="caption-subject font-red-sunglo bold uppercase" >' + (x === 'null' ? '' : x) + '</span>';
            xy += '</td>';
            xy += '</tr>';

            var totald= 0;
            $.each(ledger_array[0], function(key, val) {
                var credit = (ledger_array[0][key].FinancialAid > 0 ? ledger_array[0][key].FinancialAid :ledger_array[0][key].Credit );
                var non_ledger_row = 0;
                var non_ledger = 0;
                var debit_ = 0;
                var cbalance = (ledger_array[0][key].NonLedger > 0 ? 0  :(ledger_array[0][key].AssessFee - credit )) ;
                var dbalance = (ledger_array[0][key].Debit -ledger_array[0][key].Credit )
                if (ledger_array[0][key].NonLedger > 0){
                    non_ledger_row  = ledger_array[0][key].Credit - ledger_array[0][key].NonLedger;
                    cbalance = cbalance - non_ledger_row;
                    non_ledger = ledger_array[0][key].NonLedger - 0.00;
                    dbalance = dbalance  + non_ledger;
                }
                if(ledger_array[0][key].TransCode=='DMCM' && ledger_array[0][key].Debit > 0 ){
                        debit_ = ledger_array[0][key].Debit - 0.00;
                        cbalance = cbalance + debit_;
                }

                if(ledger_array[0][key].PostedDate == null && ledger_array[0][key].Posted == 0 ){
                    var f = ledger_array[0][key].Credit - 0.00;
                    cbalance = cbalance + f;
                    dbalance = dbalance  + f;
                }

                if(ledger_array[0][key].TransCode == 'REG'){
                    if(prevt==ledger_array[0][key].TermID){
					  return false;
					  console.log(prevt+':'+ledger_array[0][key].TermID);
					}else{
					  prevt=ledger_array[0][key].TermID;
					  zz +=' <option data-refnum="'+ledger_array[0][key].ReferenceNo+'" value="'+ledger_array[0][key].TermID+'">'+ledger_array[0][key].ReferenceNo+'</option>';
					  console.log(prevt+':'+ledger_array[0][key].TermID);
					}
                }
                totalb += cbalance;
                totald += dbalance;


                var transdate = new Date(ledger_array[0][key].TransDate);
                transdate = transdate.toLocaleString('en-US');
                var posteddate = (ledger_array[0][key].PostedDate == null ? null : new Date(ledger_array[0][key].PostedDate));
                posteddate = (posteddate == null ? null :posteddate.toLocaleString('en-US')) ;
                var repeat = 1;
                var repeat2 = 1 ;
                var colorcode;
                if(ledger_array[0][key].TransCode=='DMCM'){
                    colorcode = 'yellow-saffron';

                }else if(ledger_array[0][key].TransCode == 'REG'){
                    colorcode = 'blue-madison';

                // }else if(ledger_array[0][key].TransCode == 'OR'  ){
                //     colorcode = 'green-seagreen';
                }else if(ledger_array[0][key].TransCode == 'OR'){
                    colorcode = 'green-jungle';

                }


                if (ledger_array[0][key].FinancialAid > 0 || non_ledger_row > 0) {
                    repeat = 2;
                }
                    for (var j = 0; j < repeat; j++) {
                        xy += '<tr data-termid="'+ledger_array[0][key].TermID+'" data-posted="'+ledger_array[0][key].Posted+'" data-or_refno="'+ledger_array[0][key].ORRefNo+'" data-dmcmrefno="'+ledger_array[0][key].DMCMRefNo+'" data-transdate="'+ledger_array[0][key].TransDate+'" data-sname="'+sname+'" data-studno="'+student_number+'" data-code="'+ledger_array[0][key].TransCode+'" data-ledger="'+ledger_array[0][key].NonLedger+'" data-ref="'+ledger_array[0][key].ReferenceNo+'" class="print_assessment ' + (ledger_array[0][key].NonLedger > 0  ? (j == 0 ? 'danger': '')  : "") + ' "  >';
                        if (ledger_array[0][key].NonLedger > 0){
                            if (j == 0 ){
                                led_r++;
                            }
                        }
                        // if (key === 0 && j === 0  ) {
                        //
                        //     xy += '<td  style="background-color:#f9f9fa;" rowspan="' + (ledger_array[0].length + repeat ) + '"    > <span class="caption-subject font-red-sunglo bold uppercase">' + (x === 'null' ? '' : x) + '</span></td>';
                        // }
                        xy += '<td style="width:160px;" class="highlight"><span class="todo-tasklist-date"><i class="fa fa-calendar"> &nbsp;</i>  ' + transdate + '</span></td>';
                        xy += '<td ><button style="font-size: 1em;width:56px;" type="button" class="btn btn-circle '+(j==0 && ledger_array[0][key].NonLedger > 0 ?'green-seagreen':colorcode)+' btn-xs">' + ledger_array[0][key].TransCode + '</button></td>';
                        xy += '<td><a >' + ledger_array[0][key].ReferenceNo + '</a></td>';
                        xy += '<td style="text-align:right;">' + ReplaceNumberWithCommas(j==0 ? (ledger_array[0][key].TransCode=='DMCM' && ledger_array[0][key].Debit > 0 ?ledger_array[0][key].Debit :(ledger_array[0][key].AssessFee)):0 ) + '</td>';
                        xy += '<td style="text-align:right;">' + ReplaceNumberWithCommas(j==0 && ledger_array[0][key].NonLedger == 0 ? ledger_array[0][key].Credit :(ledger_array[0][key].NonLedger > 0  ? (j == 0 ? ledger_array[0][key].NonLedger : non_ledger_row ) :ledger_array[0][key].FinancialAid))  + '</td>';
                        xy += '<td style="text-align:right;">' + ReplaceNumberWithCommas(j==0 && ledger_array[0][key].FinancialAid > 0 ?ledger_array[0][key].AssessFee :totalb )  + '</td>';
                        xy += '<td>' + ledger_array[0][key].Remarks + '</td>';
                        xy += '<td><div class="icheckbox_flat-green '+(ledger_array[0][key].NonLedger > 0  ? (j == 1 &&  ledger_array[0][key].Posted == 1? 'checked':'' ) : 'checked')+'"><input type="checkbox"  style="position: absolute; opacity: 0;" class="icheck" ' + (ledger_array[0][key].Posted === 1 ? 'checked' : '') + ' ></div></td>';
                        xy += '<td style="width:160px;"><span class="todo-tasklist-date"><i class="fa fa-calendar">&nbsp;</i>  ' + (ledger_array[0][key].NonLedger > 0  ? ( j == 0 ?'*Non-Ledger*' : (posteddate == null ? '':posteddate) ) : posteddate) + '</span></td>';
                        xy += '</tr>';
                    }
                termb = totalb;
                termc = totald;
            });
            xy += '<tr >';
            xy += '<td colspan="9" >';
            xy += '<div style="font-size:0.9em;" class="col-lg-12 btn bold-letter ">';
            xy += '<div class="col-lg-5">';
            xy += '<span >*** Ending Balance for ' + (x === 'null' ? '' : x) + '  ***</span>';
            xy += '</div>';
            xy += '<div class="col-lg-4">';
            xy += '<span>PHP '+ReplaceNumberWithCommas(termb)+'</span>';
            xy += '</div>';
            xy += '<div class="col-lg-3">';
            xy += '<span>'+(termc < 0 ? '*Back Account':'Term Balance: ' + ReplaceNumberWithCommas(termc))+ '</span>';
            xy += '</div>';
            xy += '</div>';
            xy += '</td>';
            xy += '</tr>';
        }
    xy += '</tbody>';
    xy += '</table>';
    $("#ledger_div").html(xy);
}





$(document).ready(function() {
    $('body').on('click', '.print_assessment', function(e) {
        $('#modify_or').attr('data-modify_or_modal', $(this).data("ref"));
        transid =$(this).data("ref") ;
        transcode = $(this).data("code");
        or_refno = $(this).data("or_refno");
        posted = $(this).data("posted");
        selected_termid= $(this).data("termid");
    });
    $('body').on('dblclick', '.print_assessment', function(e) {
         var ref = $(this).data("ref");
         var ledger = $(this).data("ledger");
         var code = $(this).data("code");
         var stud_name = $(this).data("sname");
         var stud_no = $(this).data("studno");
          var transdate = $(this).data("transdate");
           var dmcmrefno = $(this).data("dmcmrefno");
          //console.log('ledger'+code);
          window.open(base_url + page_url + 'print?' + '&dmcmrefno=' + dmcmrefno  + '&transdate=' + transdate  + '&studno=' + stud_no  + '&studname=' + stud_name   + '&code=' + code  + '&ref=' + ref + '&ledger=' + ledger + '&t='+ Math.random() );
    });

    $('body').on('click', '.student-select-cert', function(e) {
        //e.preventDefault();
var stud_id = $(this).attr('data-id');
         location.replace(base_url + page_url +'ledger?type=1&idno='+ stud_id );

        // $.ajax({
        //     url: base_url + page_url + 'event?event=get_student_ledger',
        //     type: 'POST',
        //     data: {
        //         student_no: stud_id
        //     },
        //     dataType: 'json',
        //     cache: false,
        //     beforeSend: function() {
        //         progressBar('on');
        //     },
        //     success: function(ledgerdata) {
        //         var data = ledgerdata.ledger;
        //         $('.prog').text(ledgerdata.info.Program);
        //         $('.student_no').text(ledgerdata.info.StudentNo);
        //         $('.yrlvl').text(ledgerdata.info.YearLevelName);
        //         $('#student-search-text').val(ledgerdata.info.LastName + ', ' + ledgerdata.info.FirstName + ' ' + ledgerdata.info.Middlename);
        //         var sname = (ledgerdata.info.LastName + ', ' + ledgerdata.info.FirstName + ' ' + ledgerdata.info.Middlename);
        //         stud_number = ledgerdata.info.StudentNo;
        //         full_name = (ledgerdata.info.LastName + ', ' + ledgerdata.info.FirstName + ' ' + ledgerdata.info.Middlename);
        //         var ab ='';
        //         //ab += '<div class="actions">';
        //         ab += '                       <a  class="btn red btn-sm" id="modify_or" data-info="">';
        //         ab += '                       <i class="fa fa-plus"></i> Modify </a>';
        //         ab += '                       <a  class="btn green btn-sm" id="refresh_ledger" data-info="">';
        //         ab += '                       <i class="fa fa-refresh"></i> Refresh </a>';
        //         ab += '                <a href="#" class="btn default btn-sm" id="print_ledger_btn" data-studnumber="'+ledgerdata.info.StudentNo+'">';
        //         ab += '                <i class="fa fa-print"></i> Print </a>';
        //         //ab += '            </div>';
        //         $("#print_btn").html(ab);
        //         //console.log(data.legder.length);
        //         studentledger(data,full_name,stud_number);
        //
        //         closeModal('basic');
        //         progressBar('off');
        //     }
        // });
    });

    $('body').on('keypress', '#student-search-text', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            getStudData();
        }
    });
    $('#btn_search').on('click', function() {
        getStudData();


    });
	
	$('body').on('click','.btnmodify',function(){
	  $(this).closest('tr').trigger('click');
	  setTimeout(function(){$('#modify_or').trigger('click');},1000);
	});
});
