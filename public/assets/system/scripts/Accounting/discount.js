var Discount = function() {
	var discount_base_url = 'setup/discount/';
	return {
		init: function() {
			//select2 search
			$("#reg").select2({
		        minimumInputLength: 2,
		        tags: [],
		        delay: 250,
		        ajax: {
		            url: base_url+discount_base_url+'search-student',
		            dataType: 'json',
		            type: "GET",
		            quietMillis: 50,
		            data: function (term) {
		                return {
		                    term: term
		                };
		            },
		            results: function (data) {
		                return {
		                    results: $.map(data, function (item) {
		                    	console.log(item);
		                        return {
		                            text: item.name,
		                            id: item.id
		                        }
		                    })
		                };
		            },
		            cache: true
		        }
		    });

		    //datetime picker
		    $(".form_datetime").datetimepicker({
	            autoclose: true,
	            isRTL: Metronic.isRTL(),
	            format: "m/d/yyyy hh:ii",
	            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
	        });

	        $(document).on('click','#searchProvider', function() {
	        	showModal({
					name: 'basic',
					title: 'Providers',
					content: ajaxRequest(base_url+discount_base_url+'provider-modal','','HTML'),
					class: 'modal_provider',
					hasActionButton: false
				});

				FN.dataTable('#table-discount-provider');
	        });

	        $(document).on('click','.a-provider-sel', function() {
	        	var self = $(this);
	        	if (self.attr('data-id') != '') {
	        		$('#provider_name').val(self.closest('tr').find('td').eq(1).text().trim());
	        		$('#provider').val(self.attr('data-id'));
	        		closeModal('.modal_provider');
	        	}
	        });

	        $(document).on('click','.a-provider-sel', function() {
	        	var self = $(this);
	        	if (self.attr('data-id') != '') {
	        		$('#provider_name').val(self.closest('tr').find('td').eq(1).text().trim());
	        		$('#provider').val(self.closest('tr').attr('data-id'));
	        		closeModal('.modal_provider');	        		
	        	}
	        });

	        $(document).on('click','#showDiscounts', function() {
	        	var reg = $('#reg').val();
	        	if (reg == '') return;
	        	showModal({
					name: 'basic',
					title: 'Discounts list',
					content: ajaxRequest(base_url+discount_base_url+'show-discounts','regid='+reg,'HTML'),
					class: 'modal_provider',
					hasActionButton: false
				});
	        });

	        $(document).on('click','.btn-discount-remove', function() {
	        	var self = $(this).closest('tr');
	        	if (self.attr('data-id') == '') return;

	        	confirmEvent("Are you sure you want to remove this?", function(yes) {
	        		if (!yes) return;
	        		setAjaxRequest(
		        		base_url+discount_base_url+'remove-discount/'+self.attr('data-id'),
		        		'',
		        		function(result) {
		        			if (result.error == false) {
		        				self.remove();
		        				msgbox('success',result.message);
		        			} else {
		        				msgbox('error','There was an that error occured. Please try again!');
		        			}
		        			progressBar('off');
		        			onProcess(self,'','',false);
		        		},
		        		function(error) {
		        			progressBar('off');
		        			onProcess(self,'','',false);
		        		}
		        	);
	        	});
	        });

	        $(document).on('click', '.btn-save-discount', function() {
	        	var self = $(this);
	        	if (!isFormValid('#form-discount',false)) {
	        		return msgbox('error','Please complete all required fields!');
	        	}
	        	onProcess(self,'','Saving...')
	        	setAjaxRequest(
	        		base_url+discount_base_url+'save-discount',
	        		$('#form-discount').serialize()
	        		+'&student_name='+$('#student').text(),
	        		function(result) {
	        			if (result.error == false) {
	        				clear_value('#form-discount');
	        				$('#reg').val('').trigger('change');
	        				$('#reg').val('');
	        				msgbox('success',result.message);
	        			} else {
	        				msgbox('error','There was an that error occured. Please try again!');
	        			}
	        			progressBar('off');
	        			onProcess(self,'','',false);
	        		},
	        		function(error) {
	        			progressBar('off');
	        			onProcess(self,'','',false);
	        		}
	        	);
	        });
		}
	}
}();