var term   = 0;
var campus = 1;
var studno = '';
var regid  = 0;
var yrlvl  = 0;
var progid = 0;
var major  = 0;

function load_assess(stdno,termid,templid){
    setAjaxRequest(
		base_url+'accounting/soaccounts/txn?event=assessment',{termid:termid,studno:stdno,templid:templid},
		function(r){
		  if(r.success){
			$('.tbl-assess').html(r.content);
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load assessment!',function(){return;});	
		  progressBar('off');
		}
	);
}

function load_payment(regid){
    setAjaxRequest(
		base_url+'accounting/soaccounts/txn?event=duedate',{regid:regid},
		function(r){
		  if(r.success){
			$('.tbl-duedate').html(r.content);
			if($('#studname').find('[value!="-1"]').length==1){
			  $('.btnacart').addClass('hidden');
			}
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load assessment!',function(){return;});	
		  progressBar('off');
		}
	);
}

function load_reginfo(stdno){
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/soaccounts/txn?event=reginfo',{studno:stdno},
		function(r){
		  if(r.success){
		    var reginfo = r.content;
			$('.xstdno').html(reginfo.StudentNo);
			$('.regid').html(reginfo.RegID);
			$('.yrlvl').html(reginfo.YearLevelCode);
			$('.sstats').html(((reginfo.StatusID==0)?'New':'Returning'));
			$('.term').html(reginfo.AcademicYear);
			term   = reginfo.TermID;
			campus = 1;
			studno = reginfo.StudentNo;
			regid  = reginfo.RegID;
			yrlvl  = reginfo.YearLevelID;
			progid = reginfo.ProgID;
			major  = reginfo.MajorID;
			$('.btn-assess').addClass('hidden');
			$('.btn-soa').addClass('hidden');
			//alert(reginfo.DateValidation);
			if(reginfo.ValidationDate!=''){
			  $('.btn-soa').removeClass('hidden');
			}else{
			  $('.btn-assess').removeClass('hidden');
			}
			
			load_assess(stdno,reginfo.TermID,reginfo.TableofFeeID);
			load_payment(reginfo.RegID);
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
}
$(document).ready(function(){
   $('body').on('change','#studname',function(){
      var stdno = $(this).val();
	  if(stdno!=undefined && stdno!=''){
	    load_reginfo(stdno);
	  }
   });
   
   $('body').on('click','.btn-refresh',function(){
      $('#studname').trigger('change');
   });
   
   $('body').on('click','.btn-assess',function(){
	if(studno!='' && studno!=undefined && regid!='' && regid!=undefined && yrlvl!='' && yrlvl!=undefined){
	  window.open(base_url+'enrollment/print_report?event=assessment&term='+term+'&campus='+campus+'&regid='+regid+'&studno='+studno+'&yrlvl='+yrlvl+'&major='+major+'&progid='+progid,'_blank');
	}
   });
   
   $('body').on('click','.btnacart',function(){
    var regid   = $(this).attr('data-regid');
    var payid   = $(this).attr('data-payment');
    var balance = $(this).attr('data-balance');
	
	setAjaxRequest(
		base_url+'accounting/soaccounts/txn?event=addcart',{regid:regid,payid:payid,balance:balance},
		function(r){
		  if(r.success){
		    if(r.content!='' && r.content!=undefined){
			  $('#epayment').find('tbody').html(r.content);
			  $('.dvcart').removeClass('hidden');
			  if($('.dvncart').is(':visible')){
			     $('.dvncart').addClass('hidden');
			     $('.dvwcart').removeClass('hidden');
			  }
			  
			}else{
			  $('#epayment').find('tbody').html('');
			  $('.dvcart').addClass('hidden');	
			}
		  }else{
		   confirmEvent('<i class="fa fa-warning text-danger"></i>'+r.message,function(){return;});	
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to process!',function(){return;});	
		  progressBar('off');
		}
	 );
   });
   
   $('body').on('click','.btnrcart',function(){
    var detid   = $(this).closest('tr').attr('data-id');
    var payid   = $('[data-epayment]').attr('data-epayment');
	
	setAjaxRequest(
		base_url+'accounting/soaccounts/txn?event=delcart',{payid:payid,detailid:detid},
		function(r){
		  if(r.success){
		    if(r.content!='' && r.content!=undefined){
			  $('#epayment').find('tbody').html(r.content);
			  $('.dvcart').removeClass('hidden');
			}else{
			  $('#epayment').find('tbody').html('');
			  $('.dvcart').addClass('hidden');
			  if($('.dvwcart').is(':visible')){
			     $('.dvwcart').addClass('hidden');
			     $('.dvncart').removeClass('hidden');
			  }	
			}
		  }else{
		   confirmEvent('<i class="fa fa-warning text-danger"></i>'+r.message,function(){return;});	
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to process!',function(){return;});	
		  progressBar('off');
		}
	 );
   });
   
   $('body').on('click','.btnepayment',function(){
    var payid   = $('[data-epayment]').attr('data-epayment');
	window.location.href = base_url+'epayment?pid='+btoa(payid+':1');	
   });
   
   $('body').on('click','.btn-soa',function(){
	if(studno!='' && studno!=undefined && regid!='' && regid!=undefined && yrlvl!='' && yrlvl!=undefined){
	  window.open(base_url+'enrollment/print_report?event=soa&term='+term+'&campus='+campus+'&regid='+regid+'&studno='+studno+'&yrlvl='+yrlvl+'&major='+major+'&progid='+progid,'_blank');
	}
   });
   
   
   $('body').on('click','.btnrevert',function(){
	  var payid   = $(this).attr('data-payment');
	  confirmEvent('<i class="fa fa-warning text-warning"></i> Are you sure you want to cancel this transaction?',function(data){
	     if(!data){return;}
	     progressBar('on');
         setAjaxRequest( base_url + 'cashier/online-transactions/event?event=cancel-entry', {entryid: payid },
                    function(req) {
				 	   if(req.error==false){
					    $('.btn-refresh').trigger('click');
					   }else{
                        progressBar('off');
					   }
                       progressBar('off');
					},
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
			);
	  });
   
   });
   
   if($('#studname').val()!=undefined && $('#studname').val()!=''){
      $('#studname').trigger('change');
   }else{
      if($('#studname').find('[value!="-1"]').length==1){
		var tmpval = $('#studname').find('[value!="-1"]').attr('value');
		$('#studname').val(tmpval);
        $('#studname').trigger('change');
	  }
   }
});