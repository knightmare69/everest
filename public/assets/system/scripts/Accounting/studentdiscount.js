var soffset=0;
var slimit =0; 

function optbtn(x){
  x = ((x==undefined)?0:x);
  if(x==0){
	$('.btn-tblfee').attr('disabled',true); 
	$('.btn-other-save').attr('disabled',true);  
	$('.btn-other-clear').attr('disabled',true);  
  }else{
	$('.btn-tblfee').removeAttr('disabled');
	$('.btn-other-save').removeAttr('disabled');
	$('.btn-other-clear').removeAttr('disabled');  
  }	
}

function getstudentinfo(stdno){
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=reginfo',{type:1,args:stdno},
		function(r){
		  var reginfo = r.record[0];
		  $('.regid').html(reginfo.RegID);
		  $('.regdt').html(reginfo.RegDate);
		  $('.btnrefresh').trigger('click');
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
}

function getstudentdiscount(refno,studno){
    var termid = 0;
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/student-discount/txn?event=discount',{termid:termid,refno:refno,studno:studno},
		function(r)
		{
		  $('#tbldiscount').find('tbody').html(r.content);
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);	
}

function getschoprovider(provid){
	$('#template').find('[value!="-1"]').remove();
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=schoprovider',{provid:provid},
		function(r)
		{
		  $('#template').append(r.template);
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);		
}

function load_students(){

}

$('document').ready(function(){
  $('body').on('click','.btnfilter',function(){
	 var src_type  = $(this).data('source');
	 var src_param = $(this).data('parameter');
	 var dest_xid  = $(this).data('id');
	 var param     = $(src_param).val();
	 $('#modal_filter').modal('show');
	 $('#modal_filter').attr('data-type',src_type);
	 switch(src_type){
		case 'student': $('#txt-modal-search').attr('placeholder','StudentNo,LastName,Firstname'); 
		case 'scholarship': $('#txt-modal-search').attr('placeholder','Code,Provider'); 
	 }
	 $('#txt-modal-search').val(param);
	 $('.btn-modal-search').trigger('click');
  });
  
  $('body').on('keydown','[data-button]',function(e){
	 var target = $(this).data('button');
	 var parent;
	 if(e.keyCode==13){
		if($(this).is('input[type="text"]')){
			parent = $(this).closest('.input-group');
			$(parent).find(target).trigger('click');
		}
	 } 
  });
  
  $('body').on('click','.btn-modal-search',function(){
	var src_type  = $('#modal_filter').attr('data-type');
	var src_param = $(this).data('source');
	var param     = $(src_param).val();
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=filter',{type:src_type,args:param},
		function(r)
		{
		  if(r.success){
			$('.modal_list').html(r.content);
		  }
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
  });
  
  $('body').on('click','[data-list]',function(){
	 var tbl = $(this).closest('table');
     $(tbl).find('[data-list]').removeClass('info');	 
     $(this).addClass('info');	 
  });
  
  $('body').on('click','.btn-templ-select',function(){
	  var templelem = $('.modal_template_list').find('.info');
	  var templid   = $(templelem).data('list');
	  var templname = $(templelem).data('code');
	  var refno     = $('#refno').val();
	  $('.templname').data('id',templid);
	  $('.templname').html(templname);
	  $('#refno').find('[value="'+refno+'"]').attr('data-templateid',templid);
	  $('#refno').find('[value="'+refno+'"]').attr('data-template',templname);
	  gettemplatedetails(templid);
	  $('#modal_template').modal('hide');
  });	  
  
  $('body').on('click','.btn-modal-select',function(){
	 var type = $('#modal_filter').attr('data-type');
	 var data = $('#modal_filter').find('.info');
	 var xid  = $(data).data('list');
	 switch(type){
		case 'student':
		   $('.stdno').html($(data).data('list'));
		   $('.yrlvl').html($(data).data('yrlvl'));
		   $('.yrlvl').data('yrlvlid',$(data).data('yrlvlid'));
		   $('.program').html($(data).data('progname'));
		   $('.program').data('progid',$(data).data('progid'));
		   $('.stdname').html($(data).data('name'));
		   $('.sibling').html($(data).find('td:nth-child(7)').html());
		   getstudentinfo(xid);
        break;
        case 'scholarship':
		   var name = $(data).attr('data-name');
		   $('#scholprovider').val(name);
		   $('#scholprovider').attr('data-id',xid);
		   getschoprovider(xid);
        break;		
	 }
	 $('#modal_filter').modal('hide');
	 $('.modal_list').html('');	 
  });
  
   $('.table-responsive').scroll(function(){
      var tmpval = $(this).attr('data-target');
	  console.log(tmpval);
	  if(tmpval!=undefined){
		var a = $(this).height();
		var b = $(this).prop('scrollHeight');
		var c = $(this).scrollTop();
		console.log(a+':'+b+':'+c);
		if (c >= ((b-a)* 0.9)){
		   //alert('down');
		}
	  }
   });
   
   $('body').on('click','.table [data-id]',function(){
      $(this).closest('table').find('.active-row').removeClass('active-row');
	  $(this).addClass('active-row');
   });
  
   $('body').on('change','#schoOption',function(){
	 var optval = $(this).val();
	 switch(optval){
		case '1':
		  $('#application').removeClass('hidden');
		  $('#template').addClass('hidden');
		  $('#scholamount').closest('.input-group').removeClass('hidden');
		  $('.optgrant').addClass('hidden');
        break;		
		case '2':
		  $('#application').addClass('hidden');
		  $('#template').removeClass('hidden');
		  $('#scholamount').closest('.input-group').addClass('hidden');
		  $('.optgrant').removeClass('hidden');
        break;		
	 }
   });
   
   $('body').on('click','.btnadd',function(){
		var campus   = 1;
		var idtype   = 1;
		var studno   = $('.stdno').html(); 
		var refno    = $('.regid').html();
		var termid   = 0;
		var feeid    = 0;
		var provid   = $('#scholprovider').attr('data-id');
		var provtype = $('#provider').val();
		var option   = $('#schoOption').val();
		var templid  = 0;
		var grant    = $('#template').val();
		var schoacct = $('#application').val();
		var custom   = $('#customdisc').val();
		var fund     = 0;
		var disc     = 0;
		var perc     = 0;
		var onnet    = (($('#applyNet').is(':checked'))?1:0);
		var data     = {campus:campus
					   ,termid:termid
					   ,idtype:idtype
					   ,studno:studno
					   ,regid:refno
					   ,feeid:templid
					   ,provid:provid
					   ,provtype:provtype
					   ,grant:grant
					   ,schoacct:schoacct
					   ,option:option
					   ,fund:fund
					   ,discount:disc
					   ,perc:perc
					   ,onnet:onnet};
		switch(option){
		   case '1':
			  data['fund'] = $('#scholamttype').attr('data-value');
			  if(data['fund']=='0')
				data['perc'] = $('#scholamount').val();
			  else
				data['discount'] = $('#scholamount').val();
		   break;	   
		   case '2':
			  data['fund'] = $('[name="optgranttempl"]:checked').val();
			  if(data['fund']==0)
				data['perc']     = parseFloat(custom);
			  else
				data['discount'] = parseFloat(custom);  
		   break;	   
		}			   
		
		progressBar('on');
		setAjaxRequest(
			base_url+'accounting/txn?event=savediscount'
		   ,data
		   ,function(r){
			  $('.btnrefresh').trigger('click');
			  progressBar('off');
			}
		   ,function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
			  progressBar('off');
			}
		);		
   });
   
   $('body').on('click','.btndelete',function(){
	var xid = $(this).closest('tr').attr('data-refno');
	if(xid==undefined || xid==''){return false;}
	  
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=rem_discount'
	   ,{refno:xid}
	   ,function(r){
		  $('.btnrefresh').trigger('click');
		  confirmEvent('<i class="fa fa-check text-success"></i> '+r.content,function(){return;});	 
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to delete Data!',function(){return;});	
		  progressBar('off');
		}
	);
   });
   
   $('body').on('click','.btnrefresh',function(){
      var studno = $('.stdno').html();
      var refno  = $('.regid').html();
      getstudentdiscount(refno,studno);
   });

});