var MOD = {

    init: function(){        
        var t = sysfnc.get('term');
        var term = getParameterByName('t');

        if(t != undefined && term == undefined){
           location.replace(base_url+page_url+'?t='+ t);
        }
        this.load();
    },

    cell_focus : function(e){
        $('#customtable tbody td').removeClass('cell-focus');
        $(e).addClass('cell-focus');
        $(e).focus();
    },
    tabselect: function(e){
        
        var m= $(e.target).attr('data-menu');
        if(m='custom'){
            setAjaxRequest( base_url + page_url + '/event?event=get-custom', {ref : getParameterByName('ref') } ,
                function(r) {
    
                    if (r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    } else {
                        $('#custom_table').html(r.table);
                        $('.date-picker').datepicker();
						
						$('#custom_box').scroll(function(){
							var tmpscrl = $(this).scrollLeft()
							$('#custom_header').scrollLeft(tmpscrl);
							console.log(tmpscrl);
						});
                    }
    
                    progressBar('off');
                } , undefined, 'json',false,false
    
            );
        }
        e.preventDefault();
    },
    compute_custom: function(mode,r){
        switch(mode){
            case 'balance' :

                var bal = 0;
                var assess  = decimal($('#customtable tbody tr').eq(r).find('td.assess').html());

                $('#customtable tbody tr').eq(r).find('td.editable').each(function(){
                    if( $(this).html().toString().replace(',','') != ''){
                        bal += parseFloat($(this).html().toString().replace(',','')) ;
                    }
                });

                bal = parseFloat(assess) - parseFloat(bal);                

                var $cbal = $('#customtable tbody tr').eq(r).find('td.cbalance');

                $cbal.html(format_number(bal));

                if(bal != 0){
                    $cbal.addClass('danger');
                }else{
                    $cbal.removeClass('danger');
                }

            break;
            
            case 'total':
            
            break;
            
            
        }
    },
    
    setCustomSched: function(e){
        
        var p = $(e.target).prop('checked');

         setAjaxRequest( base_url + page_url + '/event?event=set-custom', {value : p } ,
            function(r) {

                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    msgbox('success', r.message);
                }

                progressBar('off');
            } , undefined, 'json',false,false

        );
    },
    set_duedate: function(e){
        
        var c = $(e.target).attr('data-id');
        var v = $(e.target).val();
        var idx = $(e.target).closest('tr').attr('data-due');
        
        var data = {col:c, amt:v, ref: getParameterByName('ref'), idx: idx };
        setAjaxRequest( base_url + page_url + '/event?event=save-due', data ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    msgbox('success', r.message);
                }
                progressBar('off');
            }
        );
    
    },
    
    load : function(){
        
      $('body').on('change','.date-picker', $.proxy(this.set_duedate,this));
      
      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      
      $('body').on('click', 'span,  i', function(e){
          if ( $(this).parent().hasClass('btn') ) { $(this).parent().click(); }
          e.preventDefault();
      });
      
      $('body').on('click','#customtable tbody td', $.proxy(this.cell_focus,this.target));      
        
      $('body').on('click','.tabselect', $.proxy(this.tabselect,this));
      
      $('body').on('click','#chkcustom', $.proxy(this.setCustomSched,this));

      $('body').on('change', '#term', function(e){
        var t =  $('#term').val();
        sysfnc.set('term', t);
        location.replace(base_url+page_url+'?t='+ $('#term').val());
      });

      $('body').on('change', '#prog', $.proxy(this.prog_changed,this) );

      $('body').on('mousedown','#records-table tbody td', function(e){
         MOD.closemenu(); if(e.button == 2){ MOD.initrclick(e); }
      });

      $('body').on('dblclick','#customtable tbody td', $.proxy(this.initEdit, this));
      $('body').on('keydown','#customtable tbody td', $.proxy(this.initEdit, this));

      $('body').on('blur','#txteditor', $.proxy(this.hideactiveText, this));
      $('body').on('focus','#txteditor', $.proxy(this.editorFocusUp, this));
      $('body').on('mouseup','#txteditor', $.proxy(this.editorMouseUp, this));
      $('body').on('change','#txteditor', $.proxy(this.editorChange, this));
      
	  
      $('#txteditor').on('keypress',function(e){

            if( e.keyCode === 27) {
                var c = $(e.target).attr('data-col'), r =  $(e.target).attr('data-row');
                var $nr = $('#customtable tbody tr').eq(r).find('td').eq(c);                
                MOD.hideactiveText();
                MOD.cell_focus($nr);
            }else if (e.keyCode == 13 ){
                if(!MOD.IsChanged){ MOD.editorChange(e); } MOD.IsChanged=false;
            }

        });        
    },
    
     editorChange: function(e){

        MOD.Invalid = false;
        MOD.IsChanged = true;

        var table = 'customtable';
        var col = $('#txteditor').attr('data-col');
        var row = $('#txteditor').attr('data-row');

        var $r =  $('#'+table+' tbody tr').eq(row).find('td').eq(col);
        var id = $r.closest('tr').attr('data-id');
        var cls = $r.attr('data-class');

        var amt = decimal($('#txteditor').val());

        if( !$.isNumeric( amt ) ){
            msgbox('warning','Ooops, Invalid input');
            MOD.Invalid = true;
            return false;
        }

        $r.html( format_number(amt) ) ;        
        MOD.compute_custom('balance',row, amt);
        MOD.saveCustom(id, col, amt);        
        
        var $nextrow = $('#customtable tbody tr').eq(row).find('td').eq( parseInt(col)+1);
        this.cell_focus($nextrow);
        this.hideactiveText();

        e.preventDefault();
    },

    editorFocusUp: function(e){ e.target.select();},
    editorMouseUp: function(e){ return false; },
    hideactiveText:function(e){ $('#tbleditor').hide(); },
    initEdit: function(e){
        var table = 'customtable';
        var presskey = "";
        var ro = $(e.target).parent().index();
        var co = $(e.target).index();

        if(e.type == "keydown"){

            var gotonextcell = function(row,col){
               var $nextrow =  $('#'+table+' tbody tr').eq(row).find('td').eq(col);
               MOD.cell_focus($nextrow);
               return false;
            };

            if(e.keyCode === 37 || e.keyCode === 39 ){
                var lr = parseInt(co + (e.keyCode === 37 ? -1 : 1));
                return gotonextcell(ro,lr);
            }

            if(e.keyCode === 38 || e.keyCode === 40 ){
                var ud = parseInt(ro + (e.keyCode === 38 ? -1 : 1));
                return gotonextcell(ud,co);
            }

            if( e.keyCode === 46){
                if($(e.target).hasClass('editable')){
                    $(e.target).html('');
                    MOD.saveCustom($(e.target).attr('data-id'), co, 0);
                }
                return false;
            }

            /* code from keypress */
            //if ( (e.shiftKey || (e.charCode < 45 || e.charCode > 57) ) && e.keyCode !== 13  ) return false;
            if(e.keyCode == 27) { return false; }
        }

        var top  = window.pageYOffset || document.documentElement.scrollTop;
        if($(e.target).hasClass('editable'))
        {
            var add_width = -10;
            var data = $(this).data();
            var currval = "";

            currval = $(e.target).html().trim();

            if(presskey != '') currval = presskey;
            var pos =  $(e.target).offset();
            var h = parseInt( $(e.target).height()-1  ) ;
            var w = parseInt( $(e.target).width()+add_width ) ;

           $('#tbleditor').css({'position': 'fixed' , 'top' : pos.top - top +"px",'left' : pos.left +"px",});

           $("#txteditor").val('');
           $("#txteditor").height(h);
           $("#txteditor").width(w);

           $('#tbleditor').show();

           $("#txteditor").attr('data-col',co);
           $("#txteditor").attr('data-row',ro);
           if( currval != '' && currval != undefined ) $("#txteditor").val(currval.trim() );
           $("#txteditor").focus();
           $("#txteditor").select(false);

        } else {
            this.hideactiveText();
        }

        if(e.keyCode == 13){ e.preventDefault(); }
    },
    prog_changed: function(e){

         var p = $(e.target).val();

         setAjaxRequest( base_url + page_url + '/event?event=get-parameters', {prog : p } ,
            function(r) {

                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    $('#major').html(r.major);
                    $('#level').html(r.level);
                    $('.select2').select();
                }

                progressBar('off');
            } , undefined, 'json',false,false

        );
    },

    add : function(){ this.clearform(); },
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    clearform: function(){

        var rows = $('#records-table tbody tr').length;
        clear_value('#formFee');

        $('#template').val( getParameterByName('ref') );
        $('#acct').val('-1').trigger('change');
        $('#status').val('0').trigger('change');
        $('#level').val('0').trigger('change');
        $('#session').val('0').trigger('change');
        $('#seqno').val(rows+1);
        $('#idx').val('0');

    },
    initcurrency: function(){
        var cur = 1; $('#curr').val(cur);
    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'add':  d.mode =='fees' ? this.add(e) :  template.add() ;  break;
            case 'edit':  (d.mode =='fees' ? this.edit(e) : template.edit()) ; break;            
            case 'remove': ( d.mode =='fees' ? this.deleteThis(e) : template.remove() ); break;            
            case 'save': ( d.mode =='fees' ? this.saveFees(e) : template.save() ); e.preventDefault(); break;
            case 'delete': this.deleteThis(e); break;
            case 'view': this.view(e); break;
            case 'print': this.print(e); break;
            case 'cancel-menu': this.closemenu(); break;
            case 'cancel-txn': this.cancelThis(e); break;
            case 'setup' : this.setup(e); break;
            case 'add-row': this.add_row(); break;
            case 'rem-row': this.rem_row(); break;
            case 'save-programs': this.save_row(); break;
            case 'saveAs': this.saveAs(); break;
            case 'saveas_save': this.saveAs('save'); break;
            default: break;
        }
        
    },
    saveAs: function(mode){
        switch(mode){
            case 'save': 
                if (isFormValid('#form_saveas')){
                    
                    setAjaxRequest( base_url + page_url + '/event?event=save-as-template', $('#form_saveas').serialize() ,
                        function(r) {
                            if (r.error) {
                                showError( '<i class="fa fa-times"></i> ' +  r.message);
                            } else {
                               location.reload();
                            }
                            progressBar('off');
                        }
                    );
                }
                
            break;
            default: 
            
                var id = $('#records-table tbody tr.active-row').attr('data-id');
                var code = $('#records-table tbody tr.active-row').find('.tcode').html();
                console.log(id);
                if(id != '' && id != undefined ){
                    clear_value('#form_saveas');
                    $('#saveas_idx').val(id);
                    $('#from').html(code);
                    $('#saveas_modal').modal('show');    
                }else{
                    msgbox('warning', 'Please select a template to save as.');
                }
                    
            break;
        }                
    },
    rem_row: function(){
         $('#tblprograms tbody tr').each(function(){
            if ( $(this).find('input:checkbox').prop('checked') ) {
                $(this).remove();
            }
        });
    },

    save_row: function(){
        var r = [];

        var t = $('#tblprograms').attr('data-template');

        $('#tblprograms tbody tr').each(function(){
            var data = $(this).data();
            r.push({ prog : data.prog, maj : data.major, level : data.level });
        });

        setAjaxRequest( base_url + page_url + '/event?event=save-programs', { template : t ,  data:r } ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                   location.reload();
                }
                progressBar('off');
            }
        );
    },

    add_row: function(){
        var prog = $('#prog').find('option:selected');

        var major_ids = "";
        var major_name = "";

        $('#major').find('option:selected').each(function(){
            major_ids += (major_ids != '' ? ', ' :  '' ) + $(this).val();
            major_name += (major_name != '' ? ', ' :  '' ) + $(this).html();
        });

        var lvid = "";
        var level = "";

        $('#level').find('option:selected').each(function(){
            lvid +=  (lvid != '' ? ', ' :  '' ) + $(this).val();
            level += (level != '' ? ', ' :  '' ) + $(this).html();
        });

        $('#tblprograms tbody').append("<tr data-prog='"+ prog.val() +"' data-major='"+ major_ids +"' data-level='"+lvid+"'><td><input type='checkbox'></td><td>"+ prog.html() +"</td><td>"+ major_name +"</td><td>"+ level +"</td></tr>");

    },

    setup: function(e){

        var t = $(e.target).closest('tr').attr('data-id')

        $('#tblprograms').attr('data-template', t);

        setAjaxRequest( base_url + page_url + '/event?event=get-programs', {template : t } ,
            function(r) {

                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                   $('#tblprograms tbody').html('');
                   $('#tblprograms tbody').html(r.table);
                   $('#setup_modal').modal('show');
                }

                progressBar('off');
            } , undefined, 'json',false,false

        );
    },
    edit: function(e){
        var $r = $(e.target).closest('tr');

        $('.form-control').each(function(){
            var nm = $(this).attr('data-col');
            if (nm!=''){
                $(this).val($r.attr('data-'+nm)).trigger('change');
            }

        });

        $('#amt').val(get_number($('#amt').val()));
        $('#idx').val($r.attr('data-id'));
    },

    view : function(e){
        var $r = $(e.target).closest('tr');
       if( $r.attr('data-id') == undefined){
            msgbox('warning','No Selected fees template');
            return false;
       }
       location.replace(base_url+page_url+'/view?t='+ $('#term').val()+'&ref='+ $r.attr('data-id') );
    },


    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                var d = {
                    ref : $(this).attr('data-id'),
                    idno : $(this).attr('data-idno'),
                    prog : $(this).find('.level').find('option:selected').attr('data-prog'),
                    major : $(this).find('.track').find('option:selected').val(),
                    level : $(this).find('.level').val(),
                };
                ids.push(d);
           }
        });
        return ids;
    },

    saveFees: function(){

        var is_valid = isFormValid('#formFee');
        if( is_valid ) {
            var data = $('#formFee').serialize();
            setAjaxRequest( base_url + page_url + '/event?event=save-fee', data ,
                function(r) {
                    if (r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    } else {
                         Metronic.blockUI({
                             target: '.portlet-body',
                             boxed: true,
                             message: '<i class="fa fa-save"></i> Successfully saved!',
                             textOnly: true
                        });

                        window.setTimeout(function() {
                            Metronic.unblockUI('.portlet-body');
                            $('#fee-section').html(r.fees);
                            MOD.clearform();
                        }, 2000);
                    }
                    progressBar('off');
                }
            );
        }
    },

    deleteThis : function(e){

        var _this = this;
        var selecteds = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                selecteds.push($(this).attr('data-id'));
           }
        });


        if (selecteds.length > 0 ) {
            $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove selected Fee? </h2>",
    			content : "Click [YES] to confirm...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    setAjaxRequest( base_url + page_url + '/event?event=delete-fee' , { ids : selecteds } , function(r) {
                    if(r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    }else{
                        sysfnc.msg('Successfully cancelled!','add/drop/change transaction successfully cancelled');
                        location.reload();
                    }
                    progressBar('off');
                    });
    			}
    		});
        }else{
            alert('Please select student to delete');
        }
    },

    print : function(){
        var id = $('#btnprint').attr('data-ref');

        if(id == 0 || id == undefined){
            alert('no adc transaction found!');
            return false;
        }
        window.open(base_url+'add-drop-change/print?ref='+id+'&t='+ Math.random() ,'_blank');
	 },

    initrclick: function(e){
        var pos = $(e.target).position();
        var cs = {top: pos.top, left: pos.left}

        $( "#mouse_menu_right" ).css(cs);
        $('#mouse_menu_right').show('50');
    },

    saveCustom: function(r, c, v){
        if (c == undefined){
            return false;
        }
        var data = {r:r, col:c, amt:v, ref: getParameterByName('ref') };
        setAjaxRequest( base_url + page_url + '/event?event=save-custom', data ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    msgbox('success', r.message);
                    MOD.computeTotal();
                }
                progressBar('off');
            }
        );
    },
    
    computeTotal: function(){
        
        $('#customtable tfoot tr.total').find('td').each(function(){
            
            if ( $(this).attr('data-col') != undefined ){
                var total =0;
                var id = $(this).attr('data-col');
                console.log(id);
                $('#customtable tbody .editable').each(function(){
                    if ( $(this).attr('data-col') == id){
                        total += parseFloat( decimal($(this).html())) ;
                    }                    
                });
                $(this).html(format_number(total));
            }
        });           
    }

};

 var template =  {
   edit: function(){
        var $r= $('#records-table tbody tr.active-row');
        var d = $r.data();
        clear_value('#form_template');
        $('#mterm').val( $('#term').val());
        $('#type').val(d.txn);
        $('#code').val($r.find('.tcode').html());
        $('#desc').val($r.find('.options').html());
        
        if( d.foreign == 1){
            $('#foreign').prop('checkbox', true);    
        }
        
        $('#gender').val(d.gender);
        $('#status').val(d.status);
        $('#curr').val(d.curr);
        $('#scheme').val(d.scheme);
        $('#option').val(d.payoption);
        $('#remarks').val(d.remarks);
        if( d.inactive == 1){
            $('#inactive').prop('checkbox', true);    
        }
        $('#idx').val(d.id);           
        $('#mcampus').val(d.campus);
        
        $('#template_modal').modal('show');
   },
   remove: function(){
        var $r = $('#records-table tbody tr.active-row');
        $.SmartMessageBox({
			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove Template : "+ $r.find('.tcode').html() +"? </h2>",
			content : "Click [YES] to confirm...",
			buttons : '[No][Yes]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "Yes"){
                setAjaxRequest( base_url + page_url + '/event?event=delete-template' , { template : $r.attr('data-id') } , function(r) {
                if(r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                }else{
                    sysfnc.msg('Successfully deleted!','Table of Fee deleted');
                    location.reload();
                }
                progressBar('off');
                });
			}
		});
   },

   add : function(){
        clear_value('#form_template');
        $('#mterm').val( $('#term').val() );
        $('#template_modal').modal('show');
   },
   save : function(){

        var valid_ba = isFormValid('#form_template');

        if(valid_ba == false){
            msgbox('warning','Please complete the form');
            return false;
        }

        $.SmartMessageBox({
        	title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Save Template? </h2>",
        	content : "Do you want to save template? Click [YES] to proceed...",
        	buttons : '[No][Yes]'
        }, function(ButtonPressed) {
        	if (ButtonPressed === "Yes"){
        	     var data = $('#form_template').serialize();
                setAjaxRequest( base_url + page_url + '/event?event=save-template', data ,
                    function(r) {
                        if (r.error) {
                            showError( '<i class="fa fa-times"></i> ' +  r.message);
                        } else {
                            location.reload();
                        }
                        progressBar('off');
                    }
                );
        	}
        });
   },
};