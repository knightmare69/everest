$('body').on('change', '#term', function(e){ Memo.load_term($(this)); });
var Memo = {
    id_num : '',
    id_typ : '',
    t_ref : '',
    t_typ : '',
    t_all : 0,
    getDetails: function(){
        var data=[];
        $('#tbltxn tbody tr').each(function(){
            if( $(this).attr('data-id') ){
                data.push({
                    id  : $(this).attr('data-id')
                   ,debit : $(this).find('td.debit').find('input').val()
                   ,credit : $(this).find('td.credit').find('input').val()
                   ,rmks: $(this).attr('data-remarks')
                   ,trn : $(this).attr('data-trn')
                   ,dmcode : $(this).attr('data-dmcode')
                });
            }
        });
        return data;
    },
    load_term: function($t){
      progressBar('on');
        var d = $t.val();
        console.log(d);
        setAjaxRequest( base_url + page_url + '/event?event=list', {term: d} ,
            function(r) {
                if (r.error == false) {
                    $('#grdList').html(r.data);
                    progressBar('off');
                } else {
                    msgbox('error', req.message);
                    progressBar('off');
                }
            }, null, 'json', true, false
        );
    },
    assessment: function()
    {
        var refno = $('#refs').find('option:selected').text();
        var term = $('#refs').find('option:selected').attr('data-tid');
        var data = { refno: refno, term:  term  ,  'idno':  this.id_num , 'type': this.id_typ, 'txn' : this.t_typ, 'all': this.t_all , };
        setAjaxRequest( base_url + page_url + '/event?event=assessment', data ,
            function(r) {
                if (r.error == false) {
                    $('#current_balance').html(r.view);
                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );
    },
    compute: function(mode){
        var total = 0 ;
        switch(mode){
            case 'current':
                $('#tblcurrent tbody tr').each(function(){
                    total = parseFloat(total) +  parseFloat($(this).attr('data-bal'));
                });
                $('.current_total').html(format_number(total));
            break;
            case 'txn':
                $('#tblcurrent tbody tr').each(function(){
                    total = parseFloat(total) +  parseFloat($(this).attr('data-bal'));
                });
                $('.current_total').html(format_number(total));
            break;
        }
        return total;
    },
    save_txn: function(){
      progressBar('on');
        var exp = $('#explanation').val();
        var date = $('#txdate').val();
        var date = $('#txtime').val();
        var idx = $('#dmcm_no').attr('data-value');
        var term = $('#refs').find('option:selected').attr('data-tid');
        if( exp == '' || exp == undefined ){
            msgbox('danger','Invalid Expanation! Saving cannot proceed.');
            progressBar('off');
            return false;
        }
        var details = this.getDetails();
        setAjaxRequest( base_url + page_url + '/event?event=save-record', { 'idno':  this.id_num, 'name': $('#txtname').val() , 'type': this.id_typ , 'txn' : this.t_typ, 'explanation':  exp , 'term':term, 'reg': $('#refs').val(), 'details': details  } ,
            function(r) {
                if (r.error == false)
                {
                  progressBar('off');
                    sysfnc.msg('Success!', r.message);
                    Metronic.blockUI({
                     target: '#main_module',
                     boxed: true,
                     message: '<i class="fa fa-exclamation"></i>' + r.message ,
                     textOnly: true
                    });
                    window.setTimeout(function() {
                            Metronic.unblockUI('#main_module');
                            location.reload();
                    }, 3000);
                } else {
                  progressBar('off');
                    sysfnc.showAlert('.modal-body','danger','Invalid OR number',true,true, 'warning',20);
                     Metronic.blockUI({
                             target: '.modal-body',
                             boxed: true,
                             message: '<i class="fa fa-exclamation"></i>' + r.message ,
                             textOnly: true
                        });
                    window.setTimeout(function() { Metronic.unblockUI('.modal-body'); }, 2000);
                }
            }, null, 'json', true, false
        );
    },
    set_ref: function(ref=0){
        var r = $('#refs').find('option:selected');
        $('.term').html(r.attr('data-term'));
        $('.date').html(r.attr('data-date'));
        $('.remarks').html(r.attr('data-remarks'));
        $('.validation').html(r.attr('data-validation'));
    },
    set_idtype: function(e){
        var t = $(e.target).attr('data-type');
        $('#ptype').find('span').html($(e.target).html());
        $('#ptype').attr('data-id',t);
    },
    loadRefs : function(ref){
        var idno = $('#txtname').attr('data-id');
        if (idno =='' || idno == undefined ){
            msgbox('warning','Please search payer');
            return false;
        }
        this.initRefs(idno);
    },
    initRefs : function(idno){
        $('#refs').html('');
        this.id_num = idno;
        this.id_typ = $('#ptype').attr('data-id');
        this.t_typ = $('#txn').val();
        setAjaxRequest( base_url + page_url + '/event?event=get-refs', { 'idno':  idno , 'type': this.id_typ , 'txn' : this.t_typ, 'all' : this.t_all  },
            function(r) {
                if (r.error == false) {
                    $('#refs').html(r.data);
                    $('#obalance').html(r.obalance);
                    $('#current_balance').html(r.assessment);
                    Memo.set_ref( r.idx );
                } else {
                    msgbox('error', r.message);
                }
            }, null, 'json', true, false
        );
        $('#dmcm_no').attr('data-value',0);
        $('#dmcm_no').html('[AUTO NUMBER]');
        $('#explanation').val('');
        this.clear_txn();
    },
    clear_txn: function(){
         $('#tbltxn tbody tr').each(function(){
            $(this).attr('data-id','')
            $(this).find('td.debit').find('input').val('')
            $(this).find('td.credit').find('input').val('')
            $(this).attr('data-remarks','')
            $(this).attr('data-trn','')
            $(this).attr('data-dmcode','')
         });
    },
    initsearch: function(){ Students.get({'student-search-text': $('#txtname').val(), 'search-campus': 1 }); },
    menu: function(e){
        var d = $(e.target).attr('data-menu');
        switch(d){
            case 'search':  this.initsearch();  break;
            case 'ref-refresh': this.loadRefs(); break;
            case 'soa': this.soa(); break;
            case 'add-row': this.addRow(); break;
            case 'add-row-proceed': this.appendRow(); break;
            case 'rem-row': this.removeRow(); break;
            case 'save' : this.save_txn(); break;
            case 'void': this.voidTrans(); break;
        }
    },
    voidTrans: function(){
        var ref = $('#records-table tbody tr.active-row').attr('data-id');
        sysfnc.msgcb("Void DMCM", "Do you want to void ref # "+ ref +"?", function(){
                setAjaxRequest( base_url + page_url + '/event?event=void', {'ref': ref},
                function(req) {
                    if (req.error == false) {
                        msgbox('success', req.message);
                        location.reload();
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
                );
            });
    },
    removeRow: function(){
        $('#tbltxn tbody tr.active-row').remove();
        this.compute('txn');
    },
    appendRow: function(e){
       var $row = $('#tblcurrent tbody tr.active-row');
	   var $mod = $('#modal_account').is(':visible');
	   
	   if($mod){
           $('#tbltxn tbody tr').each(function(){
                if ( $(this).attr('data-id') == ''){
				    var acct   = $('#acct').val();
                    var acode  = '';
					var aname  = '';
					var d      = 0;
                    var c      = 0;
                    var dmcode = '';
                    var amt  = $('#acct_cdt').val();
                    var dbt  = $('#acct_dbt').val();
					var xref = $('#refs').val();
					var xtrn = $('#refs').val();
                    
					if(acct!='-1' && acct!=undefined){
					  acode = $('#acct').find('[value="'+acct+'"]').attr('data-code');
					  aname = $('#acct').find('[value="'+acct+'"]').attr('data-name');
					}
					
					if($('#tblcurrent tr[data-ref]').length>0){
					   xref = $('#tblcurrent tr[data-ref]').first().attr('data-ref');
					   xtrn = $('#tblcurrent tr[data-ref]').first().attr('data-trn');
					}else{
					   xref = atob(xref)+';';
					   xtrn = xtrn;
					}
					
					if( parseFloat(dbt) > 0 ){
                        dmcode = 'Refund';
                        d = parseFloat(dbt) * -1;
                    }
					
					d = ( parseFloat(dbt) > 0 ? dbt : 0 );
                    c = ( parseFloat(amt) > 0 ? amt : 0 );
                    
					$(this).attr('data-id', acct);
                    $(this).attr('data-ref', xref);
                    $(this).attr('data-trn', xtrn);
                    $(this).attr('data-dmcode', dmcode );
                    $(this).find('td.code').html(acode);
                    $(this).find('td.name').html(aname);
                    $(this).find('td.dr').html('');
                    $(this).find('td.debit').find('input').val(format('###0.00',d));
                    $(this).find('td.credit').find('input').val(format('###0.00',c));
                    $('#modal_account').modal('hide');
					return false;
                }
			});
			this.compute('txn');
		
       }else{
        var f =  $('#tbltxn tbody tr[data-ref="'+ $row.attr('data-ref') +'"]');
		if(f.length<=0){
           $('#tbltxn tbody tr').each(function(){
                if ( $(this).attr('data-id') == ''){
                    var d = 0;
                    var c = 0;
                    var dmcode = '';
                    var amt = $row.attr('data-bal');
                    if( parseFloat(amt) < 0 )
                    {
                        dmcode = 'Refund';
                        d = parseFloat(amt) * -1;
                    }
                    c = ( parseFloat(amt) > 0 ? amt : 0 );
                    $(this).attr('data-id', $row.attr('data-id') );
                    $(this).attr('data-ref', $row.attr('data-ref') );
                    $(this).attr('data-trn', $row.attr('data-trn') );
                    $(this).attr('data-dmcode', dmcode );
                    $(this).find('td.code').html($row.attr('data-code'));
                    $(this).find('td.name').html($row.attr('data-name'));
                    $(this).find('td.dr').html($row.attr('data-memoref'));
                    $(this).find('td.debit').find('input').val(format('###0.00',d));
                    $(this).find('td.credit').find('input').val(format('###0.00',c));
                    return false;
                }
			});
			this.compute('txn');
		}else{
			alert('selected already exists!');
		}
	  }   
    },
    addRow: function(){
       $('#acct').val(-1);
       $('#acct_desc').val('');
       $('#acct_cdt').val(0);
       $('#acct_dbt').val(0);
       $('#modal_account').modal('show');
    },
    setAssessment: function(){
        var num = $('#txtname').attr('data-id');
        var term = $('#refs').find('option:selected').attr('data-tid');
        var f = $('#fees-table tbody tr.active-row').attr('data-id');
        var ref = $('#refs').val();
        var txn = $('#txn').val();
        setAjaxRequest( base_url + 'cashier/cashiering/event?event=get-fees', {idno: num, term: term, fee: f, refno:ref, txn: txn },
            function(req) {
                if (req.error == false) {
                    Cashiering.initRefs(num);
                    $('#modal_large').modal('hide');
                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },
    soa: function(){
        var studno = $('#tidno').html();
        var term = $('#refs').find('option:selected').attr('data-tid');
    	var refno = $('#refs').val();
    	var url = base_url+'accounting/soa?term='+term+'&campus=1&regid='+refno+'&studno='+studno;
        if(studno == '' || studno == undefined || refno == '' || refno == undefined ){
    	   msgbox('warning','Payor Account ID not found!');
    	   return false;
    	}
        window.open(url,'_blank');
    },
    recompute_item : function(e){
        var $r = $(e).closest('tr');
        var due = 0, dsc = 0, total = 0;
        due =  $r.find('.balance-amt').val();
        dsc =  $r.find('.discount-amt').val();
        total = decimal(due) - decimal(dsc);
        $r.attr('data-due', total);
        $r.attr('data-discount', dsc);
        $r.find('td.due').html(format_number(total));
        Memo.compute('txn');
    },
    bind : function()
    {
        $('body').on('click','.btn-add', function(){
			$('#modal_account').modal('show');
		});
		
        $('body').on('click','.btn,.icon-btn ', $.proxy(this.menu,this));
        $('body').on('click','.fa', function(){
            if( $(this).parent().hasClass('btn') ) { $(this).parent().click(); }
        });
        $('body').on('click', '.student-select', $.proxy(Students.info,this));
        $('body').on('click','.idtype', $.proxy(this.set_idtype,this));
        $('body').on('change','#refs', function(){
            Memo.set_ref( $('#refs').val() );
            Memo.assessment();
        });
        $('body').on('dblclick','#tblcurrent tbody tr', function(){
            Memo.appendRow(this);
        });
        $('body').on('keypress', '#txtname', function(e) {
            if (e.keyCode == 13) {
                Memo.initsearch();
                e.preventDefault();
            }
        });
        $('body').on('keypress', '#txt1', function(e) { if(e.keyCode == 13) { $('#bot2-Msg1').click(); }});
		
		$('body').on('click','.btn-remove',function(){
		   var xbody = $('#tbltxn').find('tbody');
		   
		   if($(xbody).find('.active-row').length<=0){
			return false;
		   }
		   
		   $('#tbltxn').find('.active-row').attr('data-id','');
		   $('#tbltxn').find('.active-row').attr('data-ref','');
		   $('#tbltxn').find('.active-row').attr('data-txn','');
		   $('#tbltxn').find('.active-row').attr('data-trn','');
		   $('#tbltxn').find('.active-row').attr('data-remarks','');
		   $('#tbltxn').find('.active-row').attr('data-dmode','');
		   $('#tbltxn').find('.active-row').find('.code').html('');
		   $('#tbltxn').find('.active-row').find('.name').html('');
		   $('#tbltxn').find('.active-row').find('.dr').html('');
		   $('#tbltxn').find('.active-row').find('.rmks').html('');
		   $('#tbltxn').find('.active-row').find('input[type="text"]').val('');
		   $('#tbltxn').find('.active-row').removeClass('active-row');
		   
		   for(var i=1;i<29;i++){
		     var j    = i+1;
		     var curr = $(xbody).find('tr:nth-child('+i+')');
		     var next = $(xbody).find('tr:nth-child('+j+')');
			 
			 if($(curr).attr('data-id')=='' && $(next).attr('data-id')!=''){
			    $(curr).attr('data-id',$(next).attr('data-id'));
				$(curr).attr('data-ref',$(next).attr('data-ref'));
				$(curr).attr('data-trn',$(next).attr('data-trn'));
				$(curr).attr('data-txn',$(next).attr('data-txn'));
				$(curr).attr('data-remarks',$(next).attr('data-remarks'));
				$(curr).attr('data-dmode',$(next).attr('data-dmode'));
				$(curr).find('.code').html($(next).find('.code').html());
				$(curr).find('.name').html($(next).find('.name').html());
				$(curr).find('.dr').html($(next).find('.dr').html());
				$(curr).find('.rmks').html($(next).find('.rmks').html());
				$(curr).find('.credit').html($(next).find('.credit').find('input[type="text"]').clone());
				$(curr).find('.debit').html($(next).find('.debit').find('input[type="text"]').clone());
				
			    $(next).attr('data-id','');
				$(next).attr('data-ref','');
				$(next).attr('data-trn','');
				$(next).attr('data-txn','');
				$(next).attr('data-remarks','');
				$(next).attr('data-dmode','');
				$(next).find('.code').html('');
				$(next).find('.name').html('');
				$(next).find('.dr').html('');
				$(next).find('.rmks').html('');
				$(next).find('input[type="text"]').val('');
			 }
		   }
		});
    },
    plugins: function(){
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
         $('.timepicker-default').timepicker({
            autoclose: true,
            showSeconds: true,
            minuteStep: 1
        });
    },
    init: function(){
        this.bind();
        this.plugins();
        if($('#term').val() != undefined){ 
		  this.load_term($('#term')); 
		}
    }
};
var Students = {
    get: function(params) {
        setAjaxRequest( base_url + 'students/event?event=get',  params,
            function(req) {
                if (req.error == false) {
                    showModal({ name: 'basic', title: 'Searched Student(s)', content: req.list});
                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#students-table-res').DataTable({ 'bDestroy': true, 'aaSorting': []});
                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },
    info: function(e) {
        var stud_no = $(e.target).attr('data-id');
        var pic = $(e.target).parent().parent().find('.img').attr('src');
        $('#stud_photo').attr('src', pic);
        $('.modal').modal('hide');
        setAjaxRequest( base_url + 'students/event', 'event=get_info&student_no=' + stud_no + '&form=profile',
            function(req) {
                if (req.error == false) {
                    $('#txtname').val(req.name.trim());
                    $('#txtname').attr('data-id',req.stud_num_e);
                    $('#tidno').html(req.stud_num);
                    $('#tyr').html(req.year_level);
                    $('#ledger').attr('href',base_url+'accounting/studentledger/ledger?type=1&idno='+req.stud_num_e );
                    Memo.initRefs(req.stud_num_e);
                    progressBar('off');
                    return true;
                } else {
                    msgbox('error', req.message);
                }
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },
    search_list: function()
    {
        setAjaxRequest(
            base_url + page_url + 'event', 'event=search&data_search=' + vars.searched_string + '&filters=' + JSON.stringify(vars.filter_stacked),
            function(req) {
                if (req.error == false) {
                    $('.div-stud-list').addClass('hide');
                    $('.div-stud-search-res').removeClass('hide').find('.general-item-list').html(req.list);
                } else {
                    msgbox('error', req.message);
                }
            }, '', '', true
        );
        progressBar('off');
    },
};
setInterval(checkDates, 50000);
function checkDates( )
{
    var st = srvTime(), newdate = new Date(st);
    var date = localStorage.getItem("ordatetime");
}