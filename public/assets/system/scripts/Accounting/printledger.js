var Report = {
    printReport: function() {
        var details = $('#report-form').serialize(),
            pclass = $('#programs option:selected').attr('data-pclass');

        var stud = $('#student-name'),
            stud_num = 0;
        if (stud.val() != '' || Boolean(stud.val().trim())) {
            stud_num = stud.attr('data-snum');
        }

        window.open(base_url + page_url + 'print?' + details + '&snum=' + stud_num + '&pclass=' + pclass + '&t=' + Math.random());
    },

    exportReport: function() {
        var details = $('#report-form').serialize(),
            pclass = $('#programs option:selected').attr('data-pclass');

        var stud = $('#student-name'),
            stud_num = 0;
        if (stud.val() != '' || Boolean(stud.val().trim())) {
            stud_num = stud.attr('data-snum');
        }
        window.open(base_url + page_url + 'export?' + details + '&snum=' + stud_num + '&pclass=' + pclass + '&t=' + Math.random());
        //window.open(base_url+page_url+'export?r='+ $('#report-type').val() + '&key=')  ;
    }
}


$(document).ready(function() {

    $('body').on('click', '#print_ledger_btn', function(e) {
        var studnumber = $(this).data('studnumber');
        console.log(studnumber);
        window.open(base_url + page_url + 'print?' + '&studnumber=' + studnumber + '&t=' + Math.random());
    });

    $('body').on('click', '#refresh_ledger', function(e) {
        location.reload();

        // setAjaxRequest(
        //     base_url + page_url + 'event?event=refresh_ledger', {
        //         student_no : stud_number,
        //
        //     },
        //     function(sl) {
        //         if (sl.error == false) {
        //
        //             var data = sl.ledger;
        //             msgbox('success', 'Successfully refreshed');
        //
        //              studentledger(data,full_name,stud_number);
        //              progressBar('off');
        //
        //         } else {
        //             msgbox('error', sl.message);
        //         }
        //         progressBar('off');
        //     },
        //     function(err, resp_text) {
        //         progressBar('off');
        //         msgbox('error', resp_text);
        //     }, null, true
        // );
        // $.ajax({
        //     url: base_url + page_url + 'event?event=refresh_ledger',
        //     type: 'POST',
        //     data: {
        //         student_no : stud_number,
        //
        //     },
        //     dataType: 'json',
        //     cache: false,
        //     beforeSend: function() {
        //         progressBar('on');
        //     },
        //     success:function(sl){
        //         var data = sl.ledger;
        //         msgbox('success', 'Successfully refreshed');
        //
        //          studentledger(data,full_name,stud_number);
        //          progressBar('off');
        //     }
        // });

    });

    $('body').on('click', '#modify_or', function(e) {

        //transid=   $(this).data("transid");
        if (transid == 0) {
            // alert();
            msgbox('warning', 'Please select OR to Modify');
        } else {
            if (transcode == 'OR') {

                //console.log(transid);
                $('#checknonledger').removeAttr('checked');
                $('#checknonledger').parent().removeClass('checked');
                $('#checkUpdateOR').removeAttr('checked');
                $('#checkUpdateOR').parent().removeClass('checked');
                $('#checkDistributeTerms').removeAttr('checked');
                $('#checkDistributeTerms').parent().removeClass('checked');

                $('#or_no').text(transid);

                if(posted == 1){
                $('#post_payment').attr("checked", true);
                $('#post_payment').parent().addClass('checked');
                }

                //window.onunload = function() { debugger; }

                setAjaxRequest(
                    base_url + page_url + 'event?event=get_or_details',  {
                        or_no: transid,
                        or_refno: or_refno
                    },
                    function(data) {
                        if (data.error == false) {



                                    var or_data = data.or;
                                    var assess_data = data.assess;
                                    var xy = '';
                                    xy += '';
                                    xy += '     <table id="or_tbl" class="table table-striped table-bordered table-hover">';
                                    xy += '     <thead class="flip-content">';
                                    xy += '     <tr>';
                                    xy +='      <th class="table-checkbox">';
                                    xy +='          <input type="checkbox" class="group-checkable" data-set="#or_tbl .checkboxes"/>';
                                    xy +='      </th>';
                                    xy += '         <th width="20%"> TRNID </th>';
                                    xy += '         <th> Account </th>';
                                    xy += '         <th > Amount </th>';
                                    xy += '         <th> Type </th>';
                                    xy += '         <th> RefNo </th>';
                                    xy += '         <th> NewType </th>';
                                    xy += '         <th width="20%"> NewRefNo </th>';
                                    xy += '         <th > NewTRNID </th>';
                                    xy += '         <th> AccountID </th>';
                                    xy += '         <th> EntryID </th>';
                                    xy += '     </tr>';
                                    xy += '     </thead>';
                                    xy += '     <tbody>';
                                    var or_total =0;
                                    $.each(or_data, function(key, val) {
                                        var amount_cr =  or_data[key].Credit - 0.0000 ;
                                        or_total = or_total + amount_cr ;
                                        var str = or_data[key].TransRefNo;
                                        var arr = str.split(';'); //split into array0-ref1-entry-2type
                                        console.log('array' + arr);
                                        xy += '        <tr data-trnentryid="'+(arr[1]=== undefined ? '': arr[1]) +'" data-refno="'+(arr[0]=== undefined ? '': arr[0])+'" data-newtype="'+ (arr[2]=== undefined ? '#Non-Ledger#': arr[2]) +'" data-newrefno="'+(arr[0]=== undefined ? '#Non-Ledger#': arr[0]) +'" data-newentryid="'+ (arr[1]=== undefined ? '#Non-Ledger#': arr[1]) +'"    data-entry="'+or_data[key].EntryID+'"   data-account="'+or_data[key].AcctName+'" data-id="'+or_data[key].EntryID+'">';
                                        xy += '         <td> <input type="checkbox" class="checkboxes" value="1"/> </td>';
                                        xy += '                  <td>' + (arr[1]=== undefined ? '': arr[1]) + '</td> ';
                                        xy += '                  <td>' + or_data[key].AcctName + '</td> ';
                                        xy += '                  <td>' + ReplaceNumberWithCommas(or_data[key].Credit) + '</td> ';
                                        xy += '                  <td>' + or_data[key].TransType + '</td> ';
                                        xy += '                  <td>' + arr[0] + '</td> ';
                                        xy += '                  <td class="newtype"> </td> ';
                                        xy += '                  <td class="newrefno" >  </td> ';
                                        xy += '                  <td class="newentryid"> </td> ';
                                        xy += '                  <td>' + or_data[key].AccountID + '</td> ';
                                        xy += '                  <td>' + or_data[key].EntryID + '</td> ';
                                        xy += '         </tr>';
                                    });
                                    xy += '     </tbody>';
                                    xy += '     </table>';
                                    $('#or_div_tbl').html(xy);
                                    $('#or_total').text('Total: '+ReplaceNumberWithCommas(or_total));
                                    //console.log('asdf'+or_total);

                                    xy = '';

                                    $('#or_tbl').DataTable({
                                        "scrollY": 300,
                                        "searching": false,
                                        "paging": false,
                                        "info": false,
                                        //    "ordering":false,
                                        "scrollX": true,
                                        "columns": [{
                                            "orderable": false
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        },{
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }, {
                                            "orderable": true
                                        }]
                                    });
                                    assess_table(assess_data);

                                    $('#reg_no').html(zz);
                                    console.log(zz);
									selected_termid = $('#reg_no').find('[data-refnum="'+or_refno+'"]').attr('value');
                                    $('#reg_no').val(selected_termid);
                                    modify_or_modal.modal('show');
                                    console.log(data);
                                    progressBar('off');


                        } else {
                            msgbox('error', data.message);
                        }
                        progressBar('off');
                    },
                    function(err, resp_text) {
                        progressBar('off');
                        msgbox('error', resp_text);
                    }, null, true
                );
                // $.ajax({
                //     url: base_url + page_url + 'event?event=get_or_details',
                //     type: 'POST',
                //     data: {
                //         or_no: transid,
                //         or_refno: or_refno
                //     },
                //     dataType: 'json',
                //     cache: false,
                //     beforeSend: function() {
                //         progressBar('on');
                //     },
                //     success: function(data) {
                //
                //
                //
                //         var or_data = data.or;
                //         var assess_data = data.assess;
                //         var xy = '';
                //         xy += '';
                //         xy += '     <table id="or_tbl" class="table table-striped table-bordered table-hover">';
                //         xy += '     <thead class="flip-content">';
                //         xy += '     <tr>';
                //         xy +='      <th class="table-checkbox">';
                //         xy +='          <input type="checkbox" class="group-checkable" data-set="#or_tbl .checkboxes"/>';
                //         xy +='      </th>';
                //         xy += '         <th width="20%"> TRNID </th>';
                //         xy += '         <th> Account </th>';
                //         xy += '         <th > Amount </th>';
                //         xy += '         <th> Type </th>';
                //         xy += '         <th> RefNo </th>';
                //         xy += '         <th> NewType </th>';
                //         xy += '         <th width="20%"> NewRefNo </th>';
                //         xy += '         <th > NewTRNID </th>';
                //         xy += '         <th> AccountID </th>';
                //         xy += '         <th> EntryID </th>';
                //         xy += '     </tr>';
                //         xy += '     </thead>';
                //         xy += '     <tbody>';
                //         var or_total =0;
                //         $.each(or_data, function(key, val) {
                //             var amount_cr =  or_data[key].Credit - 0.0000 ;
                //             or_total = or_total + amount_cr ;
                //             var str = or_data[key].TransRefNo;
                //             var arr = str.split(';'); //split into array0-ref1-entry-2type
                //             console.log('array' + arr);
                //             xy += '        <tr data-trnentryid="'+(arr[1]=== undefined ? '': arr[1]) +'" data-refno="'+(arr[0]=== undefined ? '': arr[0])+'" data-newtype="'+ (arr[2]=== undefined ? '#Non-Ledger#': arr[2]) +'" data-newrefno="'+(arr[0]=== undefined ? '#Non-Ledger#': arr[0]) +'" data-newentryid="'+ (arr[1]=== undefined ? '#Non-Ledger#': arr[1]) +'"    data-entry="'+or_data[key].EntryID+'"   data-account="'+or_data[key].AcctName+'" data-id="'+or_data[key].EntryID+'">';
                //             xy += '         <td> <input type="checkbox" class="checkboxes" value="1"/> </td>';
                //             xy += '                  <td>' + (arr[1]=== undefined ? '': arr[1]) + '</td> ';
                //             xy += '                  <td>' + or_data[key].AcctName + '</td> ';
                //             xy += '                  <td>' + ReplaceNumberWithCommas(or_data[key].Credit) + '</td> ';
                //             xy += '                  <td>' + or_data[key].TransType + '</td> ';
                //             xy += '                  <td>' + arr[0] + '</td> ';
                //             xy += '                  <td class="newtype"> </td> ';
                //             xy += '                  <td class="newrefno" >  </td> ';
                //             xy += '                  <td class="newentryid"> </td> ';
                //             xy += '                  <td>' + or_data[key].AccountID + '</td> ';
                //             xy += '                  <td>' + or_data[key].EntryID + '</td> ';
                //             xy += '         </tr>';
                //         });
                //         xy += '     </tbody>';
                //         xy += '     </table>';
                //         $('#or_div_tbl').html(xy);
                //         $('#or_total').text('Total: '+ReplaceNumberWithCommas(or_total));
                //         //console.log('asdf'+or_total);
                //
                //         xy = '';
                //
                //         $('#or_tbl').DataTable({
                //             "scrollY": 300,
                //             "searching": false,
                //             "paging": false,
                //             "info": false,
                //             //    "ordering":false,
                //             "scrollX": true,
                //             "columns": [{
                //                 "orderable": false
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             },{
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }, {
                //                 "orderable": true
                //             }]
                //         });
                //         assess_table(assess_data);
                //
                //         $('#reg_no').html(zz);
                //         console.log(zz);
                //         $('#reg_no').val(selected_termid);
                //         modify_or_modal.modal('show');
                //         console.log(data);
                //         progressBar('off');
                //
                //     }
                // });



            } else {
                msgbox('warning', 'Please select only OR to Modify');
            }

        }


    });
});
