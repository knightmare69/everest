$('document').ready(function(){
  //alert(base_url);
  $('body').on('click','.btn_save',function(){
	exec_print();
  });
  
  $('body').on('click','.pexport',function(){
	var typeid = $(this).attr('data-typeid');  
	exec_export(typeid);
  });
  $('.datepicker').datepicker();
});

function exec_print(){
	  if(validate_form()==true){
		 var xurl  = base_url+'accounting/print'; 
		 var xform = $('#report-form').serialize();
		 window.open(xurl+'?'+xform,'_blank');
	  } 
}

function exec_export(type){
	  if(validate_form()==true){
		 var xurl  = base_url+'accounting/export'; 
		 var xform = $('#report-form').serialize();
		 window.open(xurl+'?'+xform+'&typeid='+type,'_blank');
	  } 
}

function validate_form(){
	var result = true;
	if($('#campus').val()==0 || $('#campus').val()==''){
		result = false;
		msgbox('error','Please select a campus');
	} 
	if($('#ayterm').val()==0 || $('#ayterm').val()==''){
		result = false;
		msgbox('error','Please select a academic year');
	} 
	if($('#report').val()==0 || $('#report').val()=='' || $('#report').val()== undefined || $('#report').val()== null){
		result = false;
		msgbox('error','Please select a report to print');
	} 
	return result;
}