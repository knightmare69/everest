var payengine = '';
var studentno = '';

function getpaymentform(){
    var event = 'get_paynamics';
    progressBar('on');
    setAjaxRequest(
		base_url+'epayment/txn?event='+payengine,{studno:studentno},
		function(r){
		  if(r.success){
			$('.submit_form').html(r.content);
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
}

$('document').ready(function(){
  $('body').on('click','[name="optpayment"]',function(){
      payengine = $(this).attr('data-event');
	  studentno = '2013349';
	  getpaymentform();
  });
});