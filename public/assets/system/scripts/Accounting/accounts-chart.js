"use strict";

var AcctCharts = {
    _dataByAttrib: function (inp_class, key_attrb, value_attrb) {
        var d = [];

        $('.' + inp_class).each(function(i, v) {
            var vl = ($(this).attr(value_attrb)) ? $(this).attr(value_attrb) : '';
            var t = $(this).attr(key_attrb) + '=' + vl;
            d.push(t);
        });

        return d.join('&');
    },

    getForm: function (form, uid) {
        uid = (uid) ? uid : 0;

        setAjaxRequest(
            base_url + page_url + 'event', {'e': 'form', 'form': form, 'id': uid},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {

                    if(form == 'account') {
                        var rec_id = '#acc-tbl-rec-holder',
                            formh_id = '#acc-form-holder';

                        $(rec_id).animate({
                            'opacity': '0',
                            'margin-top': '40px',
                        }, 500, function() {
                            $('#form-holder').empty().append(result.content);
                            $('.select2me').select2();

                            $(formh_id).removeClass('hide').animate({
                                'opacity': '1',
                                'margin-top': '0'
                            }, 500);

                            $(rec_id).addClass('hide');
                            Metronic.initUniform();
                        });

                    } else {
                        showModal({
                            name: 'basic',
                            title: result.title,
                            content: result.content,
                            class: 'modal_students_result',
                            hasModalButton: false
                        });
                        $('.btn.blue.btn_modal').addClass('hide');
                        $('.modal-body table').DataTable({ "aaSorting" : [] });

                    }

                }
            }
        );
        progressBar('off');
    },

    submitForm: function(_data, form) {
        var r = false;

        $('div.form-group').removeClass('has-error');

        setAjaxRequest(
            base_url + page_url + 'event', {'e': 'save-form', 'form': form, 'data': _data},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                    $.each(result.err_list.keys, function (i, v) {
                        $('#' + v).closest('div.form-group').addClass('has-error');
                    });

                } else {
                    msgbox('success', result.message);
                    r = true;
                }
            }
        );
        progressBar('off');
        return r;
    }

}

$(document).ready(function (e){

    var active_inp_el = '';

    // Custom DT Search Ui
    var dt = $('#cot-res-table').DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: {
            url: base_url + page_url +'event',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: function(d) {
                d.e = 'records';
                d.search_opt = $("#search-form :input[value!='']").serialize();
            },
            complete: function () {
                $('.dataTables_filter .btn-tblr-refresh i').removeClass('fa-spin');
            }
        },
    });

    $('div#cot-res-table_filter').html($('#rep-inpt-search').html());

    $('body').on('click', '#find-search-opt', function(e) {
        dt.search(null).draw();
    });

    $('body').on('click', '.dataTables_filter .btn-tblr-refresh', function(e) {
        $('.dataTables_filter .btn-tblr-refresh i').addClass('fa-spin');
        dt.search(null).draw();
    });
    // End

    $('body').on('click', '.dataTables_filter .btn-acc-new, .res-a-acct', function(e) {
        var id = $(this).attr('data-id');
        AcctCharts.getForm('account', id);
    });

    $('body').on('click', '#cancel-acct-form', function(e) {
        var rec_id = '#acc-tbl-rec-holder',
            formh_id = '#acc-form-holder';

        $(formh_id).animate({
            'opacity': '0',
            'margin-top': '40px',
        }, 500, function() {
            $(rec_id).removeClass('hide').animate({
                'opacity': '1',
                'margin-top': '0'
            }, 500);

            $(formh_id).addClass('hide');
        });
    });

    $('body').on('click', '.search-show-modal', function (e) {
        var form = $(this).attr('data-search');

        AcctCharts.getForm(form);
        active_inp_el = $(this).parent().prev();
    });

    $('body').on('click', '.a-acct', function(e) {
        var v = $(this).attr('data-id'),
            t = $(this).text();

        active_inp_el.attr('data-id', v).val(t);
        closeModal('basic');
    });

    $('body').on('click', '#modal-submit-btn', function (e) {
        var d = $('.acct-form-modal').serialize(),
            f = $('.acct-form-modal').attr('data-form-type');

        var r = AcctCharts.submitForm(d, f);
        if (r) {
            $('.acct-form-modal input[type=text]').val('');
        }
    });

    $('body').on('submit', '#charts-acc-form', function(e) {
        e.preventDefault();
        var _id = $('#charts-acc-form').attr('data-id');
        var d = 'id=' + _id + '&' + $('#charts-acc-form').serialize() + '&' + AcctCharts._dataByAttrib('inp-with-attr', 'name', 'data-id');
        AcctCharts.submitForm(d, 'account');
    });

    $('body').on('click', 'input.form-control', function(e) {
        $(this).closest('div.form-group').removeClass('has-error');
    });

});
