var MOD = {

    init: function(){        
        var t = sysfnc.get('term');
        console.log(t);
        var term = getParameterByName('t');

        if(t != undefined && term == undefined){
           location.replace(base_url+page_url+'?t='+ t);
        }
        this.load();
    },
    
    load : function(){

      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      $('body').on('click', 'span, i', function(e){
          if ( $(this).parent().hasClass('btn') ) { $(this).parent().click(); }
          e.preventDefault();
      });


      $('body').on('change', '#term', function(e){
        var t =  $('#term').val();
        sysfnc.set('term', t);
        location.replace(base_url+page_url+'?t='+ $('#term').val());
      });
      
      $('.date-picker').datepicker();
    },
         
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    clearform: function(){

        var rows = $('#records-table tbody tr').length;
        clear_value('#formFee');

        $('#template').val( getParameterByName('ref') );
        $('#acct').val('-1').trigger('change');
        $('#status').val('0').trigger('change');
        $('#level').val('0').trigger('change');
        $('#session').val('0').trigger('change');
        $('#seqno').val(rows+1);
        $('#idx').val('0');

    },
   
    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'clear':  this.clearform(e);  break;
            case 'save': this.save(); break;

            default: break;
        }
        
    },
    save: function(){
        
        var data;
        
        setAjaxRequest( base_url + page_url + '/event?event=save', data ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                   location.reload();
                }
                progressBar('off');
            }
        );
            
    },      
   
};