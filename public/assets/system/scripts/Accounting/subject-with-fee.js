var initTable5 = function () {

        var table = $('#tblfund');

        /* Fixed header extension: http://datatables.net/extensions/scroller/ */

        var oTable = table.dataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // datatable layout without  horizobtal scroll
            "scrollY": "300",
            "deferRender": true,
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            "pageLength": 10 // set the initial value            
        });


        var tableWrapper = $('#tblfund_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown        
    };
    
var MOD = {
     
    init: function(){
        var t = localStorage.TermID;
        var term = getParameterByName('t');

        if(t != undefined && term == undefined){
           location.replace(base_url+page_url+'?t='+ t);
        }
        this.load();
        initTable5();
    },
        
    load : function(){

      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      $('body').on('click', '.btn span, .btn i', function(e){
          if ( $(this).parent().hasClass('btn') ) {
            $(this).parent().click();
          }
          console.log('heu');
          e.preventDefault();
      });
      
      $('body').on('click','.views', $.proxy(this.help,this));
      
      $('body').on('change', '#term', function(e){
        localStorage.TermID =  $('#term').val();
        location.replace(base_url+page_url+'?t='+ $('#term').val());
      });
      
      $('body').on('change', '#prog', $.proxy(this.prog_changed,this) );
      
      $('body').on('mousedown','#records-table tbody td', function(e){
         MOD.closemenu();
         if(e.button == 2){
              MOD.initrclick(e);
         }
      });
    },

    prog_changed: function(e){
         
         var p = $(e.target).val();
         
         setAjaxRequest( base_url + page_url + '/event?event=get-parameters', {prog : p } ,
            function(r) {
                
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    $('#major').html(r.major);
                    
                    $('#level').html(r.level);
                    $('.select2').select();                                    
                }
                
                progressBar('off');
            } , undefined, 'json',false,false   
            
        );                                    
    },

    add : function(){ this.clearform(); },
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    clearform: function(){ 
        
        var rows = $('#records-table tbody tr').length;
        
        clear_value('#formFee');
        
        $('#template').val( getParameterByName('ref') );
        $('#acct').val('-1').trigger('change');                   
        $('#status').val('0').trigger('change');
        $('#level').val('0').trigger('change');
        $('#session').val('0').trigger('change');
        $('#seqno').val(rows+1);         
        $('#idx').val('0');
        
    },
    initcurrency: function(){
        
        var cur = 1;                        
        $('#curr').val(cur);
        
    },

    menu: function(e){
        var d = $(e.target).data();
    
        switch(d.menu){
            case 'add': 
                if( d.mode =='fees'){
                    this.add(e);    
                }else{
                    template.add();
                }
                 
            break;
            case 'edit': this.edit(e); break;
            case 'view': this.view(e); break;
            case 'remove':
                
                if( d.mode =='fees'){
                    this.deleteThis(e); 
                } else{
                    template.remove();    
                }
                 
                 
            break;
            case 'delete': this.deleteThis(e); break;
            case 'save':
                if( d.mode =='fees'){
                    this.saveFees(e);
                } else{
                    template.save();    
                }
                 
            break;
            case 'print': this.print(e); break;            
            case 'cancel-menu': this.closemenu(); break;
            case 'cancel-txn': this.cancelThis(e); break;
            case 'setup' : this.setup(e); break;        
            case 'add-row': this.add_row(); break;
            case 'rem-row': this.rem_row(); break;
            case 'save-programs': this.save_row(); break;
        }
        
        console.log('heu');
        e.preventDefault();
    }, 
    
    rem_row: function(){
         $('#tblprograms tbody tr').each(function(){
            if ( $(this).find('input:checkbox').prop('checked') ) {
                $(this).remove();
            }
        });
    },
    
    save_row: function(){
        var r = [];
        
        var t = $('#tblprograms').attr('data-template');
         
        $('#tblprograms tbody tr').each(function(){
            var data = $(this).data();
            r.push({ prog : data.prog, maj : data.major, level : data.level });
        });
                        
        setAjaxRequest( base_url + page_url + '/event?event=save-programs', { template : t ,  data:r } ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                  
                }
                progressBar('off');
            }
        );
                    
        
        
    },
    add_row: function(){
        var prog = $('#prog').find('option:selected');
        
        var major_ids = "";
        var major_name = "";
        
        $('#major').find('option:selected').each(function(){
            major_ids += (major_ids != '' ? ', ' :  '' ) + $(this).val();
            major_name += (major_name != '' ? ', ' :  '' ) + $(this).html();
        });
        
        var lvid = "";
        var level = "";
        
        $('#level').find('option:selected').each(function(){
            lvid +=  (lvid != '' ? ', ' :  '' ) + $(this).val();
            level += (level != '' ? ', ' :  '' ) + $(this).html();
        });
        
        $('#tblprograms tbody').append("<tr data-prog='"+ prog.val() +"' data-major='"+ major_ids +"' data-level='"+lvid+"'><td><input type='checkbox'></td><td>"+ prog.html() +"</td><td>"+ major_name +"</td><td>"+ level +"</td></tr>");
        
    },
    
    setup: function(e){
        
        var t = $(e.target).closest('tr').attr('data-id')               
        
        $('#tblprograms').attr('data-template', t);
        
        setAjaxRequest( base_url + page_url + '/event?event=get-programs', {template : t } ,
            function(r) {
                
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                   $('#tblprograms tbody').html('');
                   $('#tblprograms tbody').html(r.table);
                   $('#setup_modal').modal('show');                               
                }
                
                progressBar('off');
            } , undefined, 'json',false,false   
            
        );        
        
        
    },
    edit: function(e){
        var $r = $(e.target).closest('tr');
        
        $('.form-control').each(function(){
            var nm = $(this).attr('data-col');
                //console.log(nm);
            if (nm!=''){
                $(this).val($r.attr('data-'+nm)).trigger('change');    
            }
             
        });
                
        $('#amt').val(get_number($('#amt').val()));        
        $('#idx').val($r.attr('data-id'));
    },
    
    view : function(e){
        var $r = $(e.target).closest('tr');
       if( $r.attr('data-id') == undefined){
            msgbox('warning','No Selected fees template');
            return false;
       }
       location.replace(base_url+page_url+'/view?t='+ $('#term').val()+'&ref='+ $r.attr('data-id') );       
    },


    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                var d = {
                    ref : $(this).attr('data-id'),
                    idno : $(this).attr('data-idno'),
                    prog : $(this).find('.level').find('option:selected').attr('data-prog'),
                    major : $(this).find('.track').find('option:selected').val(),
                    level : $(this).find('.level').val(),
                };
                ids.push(d);
           }
        });
        return ids;
    },

    saveFees: function(){

        var is_valid = isFormValid('#formFee');
        if( is_valid ) {
            var data = $('#formFee').serialize();
            setAjaxRequest( base_url + page_url + '/event?event=save-fee', data ,
                function(r) {
                    if (r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    } else {
                         Metronic.blockUI({
                             target: '.portlet-body',
                             boxed: true,
                             message: '<i class="fa fa-save"></i> Successfully saved!',
                             textOnly: true
                        });
            
                        window.setTimeout(function() {
                            Metronic.unblockUI('.portlet-body');
                            $('#fee-section').html(r.fees);
                            MOD.clearform();
                        }, 2000);  
                    }
                    progressBar('off');
                }
            );        
        }        
    },

    deleteThis : function(e){

        var _this = this;
        var selecteds = [];
        
        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                selecteds.push($(this).attr('data-id'));
           }
        });
        

        if (selecteds.length > 0 ) {
            $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove selected Fee? </h2>",
    			content : "Click [YES] to confirm...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    setAjaxRequest( base_url + page_url + '/event?event=delete-fee' , { ids : selecteds } , function(r) {
                    if(r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    }else{
                        sysfnc.msg('Successfully cancelled!','add/drop/change transaction successfully cancelled');
                        location.reload();
                    }
                    progressBar('off');
                    });
    			}
    		});
        }else{
            alert('Please select student to delete');
        }
    },

   

    print : function(){
        var id = $('#btnprint').attr('data-ref');

        if(id == 0 || id == undefined){
            alert('no adc transaction found!');
            return false;
        }
        window.open(base_url+'add-drop-change/print?ref='+id+'&t='+ Math.random() ,'_blank');
	 },

    initrclick: function(e){
        var pos = $(e.target).position();
        var cs = {top: pos.top, left: pos.left}

        $( "#mouse_menu_right" ).css(cs);
        $('#mouse_menu_right').show('50');
    },
   
};

 var template =  {
    
       remove: function(){
                                    
            var $r = $('#records-table tbody tr.active-row');                        
            $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove Template : "+ $r.find('.tcode').html() +"? </h2>",
    			content : "Click [YES] to confirm...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    setAjaxRequest( base_url + page_url + '/event?event=delete-template' , { template : $r.attr('data-id') } , function(r) {
                    if(r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);
                    }else{
                        sysfnc.msg('Successfully deleted!','Table of Fee deleted');
                        location.reload();
                    }
                    progressBar('off');
                    });
    			}
    		});                            
       },
    
       add : function(){
            clear_value('#form_template');
            $('#mterm').val( $('#term').val() );            
            $('#template_modal').modal('show');
       },
       save : function(){
        
            var valid_ba = isFormValid('#form_template');
        
            if(valid_ba == false){
                msgbox('warning','Please complete the form');
                return false;
            }
        
            $.SmartMessageBox({
            	title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Save Template? </h2>",
            	content : "Do you want to save template? Click [YES] to proceed...",
            	buttons : '[No][Yes]'
            }, function(ButtonPressed) {
            	if (ButtonPressed === "Yes"){
            	     var data = $('#form_template').serialize();
                    setAjaxRequest( base_url + page_url + '/event?event=save-template', data ,
                        function(r) {
                            if (r.error) {
                                showError( '<i class="fa fa-times"></i> ' +  r.message);
                            } else {
                              
                            }
                            progressBar('off');
                        }
                    );
            	}
            });
       },  
    };