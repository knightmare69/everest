var Report = {
    getYearLevel: function(prog_class){
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-year-level', 'pclass': prog_class},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    var res = '<option value="" disabled selected></option>'+result.html;
                    $('#year-level').html(res).removeAttr('disabled').select2("destroy").select2();

                }
            }
        );
        progressBar('off');
    },

    getSections: function(term, prog, yl_id){
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-sections', 'yl': yl_id, 'p': prog, 't': term},
            function(result) {
                if (result.error) {
                    $('#section').attr('disabled', true);
                    msgbox('error', result.message);

                } else {
                    var res = '<option value="0" selected>*All*</option>'+result.html;
                    $('#section').html(res).removeAttr('disabled').select2("destroy").select2();
                    $('#student-name, #student-find').attr('disabled', false);
                }
            }
        );
        progressBar('off');
    },

    getStudents: function(_find){
        var details = $('#report-form').serialize();

        setAjaxRequest(
            base_url + page_url + 'event?'+details, {'event': 'get-students'},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {

                    showModal({
                        name: 'basic',
                        title: 'Students',
                        content: result.html,
                        class: 'modal_students_result',
                        hasModalButton: false
                    });
                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#table-students-res').DataTable({ "aaSorting" : [] });

                }
            }
        );
        progressBar('off');
    },

    printReport: function(){
        var details = $('#report-form').serialize(), pclass = $('#programs option:selected').attr('data-pclass');

        var stud = $('#student-name'), stud_num = 0;
        if(stud.val() != '' || Boolean(stud.val().trim()) ){
            stud_num = stud.attr('data-snum');
        }

        window.open(base_url + page_url + 'print?' + details + '&snum=' + stud_num + '&pclass=' + pclass + '&t='+ Math.random() );
    },

     exportReport: function(){
        var details = $('#report-form').serialize(), pclass = $('#programs option:selected').attr('data-pclass');

        var stud = $('#student-name'), stud_num = 0;
        if(stud.val() != '' || Boolean(stud.val().trim()) ){
            stud_num = stud.attr('data-snum');
        }
             window.open(base_url+page_url+'export?'  + details + '&snum=' + stud_num + '&pclass=' + pclass + '&t='+ Math.random());
       //window.open(base_url+page_url+'export?r='+ $('#report-type').val() + '&key=')  ;
    }
}

$(document).ready(function(e){

    $('.select2').select2();

    $('body').on('click', '#student-find', function(e){
        var _find = $('#student-name').val();
        Report.getStudents(_find);
    });

    $('body').on('click', '.btn-stud-select-btn', function(e){
        var stud_name = $(this).parent().prevUntil('td:first').find('p').text();
        $('#student-name').val(stud_name).attr('data-snum', $(this).attr('data-id'));
    });

    $('body').on('change', '#programs', function(e){
        var pclass = $(this).find('option:selected').attr('data-pclass');
        $('#student-find, #student-name, #section').attr('disabled', true);
        $('#section').select2('destroy').empty();
        $('#student-name').attr('data-snum', '').val('');
        Report.getYearLevel(pclass);

        localStorage.setItem("ProgID", $(this).find('option:selected').val() );
    });

    $('body').on('change', '#year-level', function(e){
        var prog = $('#programs').val(), yl = $(this).val(), t = $('#academic-term').val();
        Report.getSections(t, prog, yl);
        //$('#student-find, #student-name').attr('disabled', true);
        $('#student-name').attr('data-snum', '').val('');
    });

     $('body').on('change', '#report-type', function(e){
        var prog = $('#programs').val(), yl = $('#year-level').val(), t = $(this).val();
        $('#student-name').attr('data-snum', '').val('');
        if($('#report-type option:selected').text().indexOf('List of Payors') >= 0){
            $('#payor').text('Payor')
        } else {
            $('#payor').text('Student(s)');
           
        }

        if( Boolean(prog) && Boolean(yl) ){
            Report.getSections(t, prog, yl);
        }
    });

    $('body').on('change', '#academic-term', function(e){
        var prog = $('#programs').val(), yl = $('#year-level').val(), t = $(this).val(), quaters_opt = $('#period option[data-group="1"]'), term_opt = $('#period option[data-group="21"]');
        $('#student-name').attr('data-snum', '').val('');

        if($('#academic-term option:selected').text().indexOf('School Year') === -1){
            quaters_opt.addClass('hide');
            term_opt.removeClass('hide');
            $('#period').select2('val', term_opt.eq(0).val());
        } else {
            quaters_opt.removeClass('hide');
            term_opt.addClass('hide');
            $('#period').select2('val', quaters_opt.eq(0).val());
        }

        if( Boolean(prog) && Boolean(yl) ){
            Report.getSections(t, prog, yl);
        }
    });

    $('body').on('change', '#section', function(e){
        $('.stud-find').attr('disabled', false);
        $('#student-name').attr('data-snum', '').val('');
    });

    $('body').on('click', '#print-report', function(e){
        var stud_num = $('#student-name').attr('data-snum');
        var prog = $('#programs').val(), yl = $('#year-level').val(), t = $('#academic-term').val(), r = $('#report-type').val();

        if(r <= 2){
            if(Boolean(prog.trim()) && Boolean(yl.trim()) && Boolean(t.trim())){
                Report.printReport();
            } else {
                msgbox('error', 'Please fill required fields to proceed.');
            }
        } else {

            if(prog != null){
                Report.printReport();
            } else {
                msgbox('error', 'Please select a program to proceed.');
            }
        }

    });

    $('body').on('click', '#btnsetup', function(e){
        var t = $('#academic-term').val(), p = $('#programs').val() ;
        setAjaxRequest( base_url + page_url + 'event', {'event': 'profile', term : t , prog : p }, function(r) {
            $('#tblprojection tbody').html(r.html)
            $('#setup_modal').modal('show');
        },undefined,'json',true,false);

    });

    $('body').on('click', '.btnsaveproj', function(e){

        var t = $('#academic-term').val(), p = $('#programs').val() ;
        var data = [];

        $('#tblprojection tbody tr').each(function(){
             var r = {
                 prog : p,
                 major : $(this).attr('data-major'),
                 level : $(this).attr('data-level'),
                 sec : $(this).find('.sect').find('input').val(),
                 stu : $(this).find('.stud').find('input').val(),
             };

             data.push(r);

        });

         setAjaxRequest( base_url + page_url + 'event', {'event': 'save-profile', term : t , data : data }, function(r) {
            if(!r.error){
                msgbox('success',r.message);
            }
        },undefined,'json',true,false);
    });

    $('body').on('click', '#student-remove', function(e){
        $('#student-name').attr('data-snum', '').val('');
    });

    $('body').on('click', '#export', function(e){
         var stud_num = $('#student-name').attr('data-snum');
        var prog = $('#programs').val(), yl = $('#year-level').val(), t = $('#academic-term').val(), r = $('#report-type').val();

        if(r <= 2){
            if(Boolean(prog.trim()) && Boolean(yl.trim()) && Boolean(t.trim())){
                      Report.exportReport();                    
            } else {
                msgbox('error', 'Please fill required fields to proceed.');
            }
        } else {

            if(prog != null){
                Report.exportReport();  
            } else {
                msgbox('error', 'Please select a program to proceed.');
            }
        }



       
    });

    $('#report-type').select2().on('change', function(e){
        var id = $(this).val(), _for = {1: 'report-card', 2: 'subject-grade'};
        var _find = _for[id.trim()];
        // console.log(Boolean(_find));

        switch( id){
            case '11': $('#btnsetup').show(); break;
            default : $('#btnsetup').hide(); break;
        }

        if(Boolean(_find)){
            //hide_divs(_find);
        } else {
            return false;
        }

    });

    function hide_divs(_for){
        var ids = ['period', 'student-name'];

        switch (_for) {
            case 'report-card':
                $('#' + ids[0]).closest('.row').addClass('hide');
                $('#' + ids[1]).closest('.row').removeClass('hide');
                break;

            case 'subject-grade':
                $('#' + ids[0]).closest('.row').removeClass('hide');
                $('#' + ids[1]).closest('.row').addClass('hide');
                break;

            default:
                return false;
        }
    }

});
