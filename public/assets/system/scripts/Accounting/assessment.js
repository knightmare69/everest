var defaultimg = '';
$('document').ready(function(){
  defaultimg = $('.entryphoto').attr('src');
  $('body').on('keydown','#studname',function(e){
    if(e.keyCode == 13) {
        $('.btnfilter[data-source="student"]').trigger('click');
    }
  });
  
  $('body').on('click','.tdedit',function(){
     var point = $(this).attr('data-pointer');
     var amtval = $(this).attr(point);
	 var tmptxt = '<input type="text" class="form-control text-right txedit numberonly" style="padding:1px !important;font-size:9px !important;" value="'+amtval+'"/>';
	 if($(this).find('input[type="text"]').length<=0){
	   $(this).html(tmptxt);
	   $(this).find('input').focus();
	 }
  });
  
  $('body').on('blur','.txedit',function(){
     var amtval  = $(this).val();
	 var point   = $(this).closest('td').attr('data-pointer');
	 var acctid  = $(this).closest('tr').attr('data-acctid');
	 $(this).closest('td').attr(point,amtval);
	 $(this).closest('td').html(amtval);
	 save_tdedit(acctid,point,amtval)
	 generate_summary();
  });
  
  
  $('body').on('click','.btnfilter',function(){
	 var src_type  = $(this).data('source');
	 var src_param = $(this).data('parameter');
	 var dest_xid  = $(this).data('id');
	 var param     = $(src_param).val();
     $('#modal_filter').modal('show');
	 $('#modal_filter').attr('data-type',src_type);
	 switch(src_type){
		case 'student': $('#txt-modal-search').attr('placeholder','StudentNo,LastName,Firstname'); 
		case 'scholarship': $('#txt-modal-search').attr('placeholder','Code,Provider'); 
	 }
	 $('#txt-modal-search').val(param);
	 $('.btn-modal-search').trigger('click');
  });
  
  $('body').on('keydown','[data-button]',function(e){
	 var target = $(this).data('button');
	 var parent;
	 if(e.keyCode==13){
		if($(this).is('input[type="text"]')){
			parent = $(this).closest('.input-group');
			$(parent).find(target).trigger('click');
		}
	 } 
  });
  
  $('body').on('click','.btn-tblfee,[name="optscheme"]',function(){
	var term   = $('.termid').data('termid');
	var yrlvl  = $('.yrlvl').data('yrlvlid');
	var progid = $('.prog').data('progid');
	var scheme = -1;
	if($(this).is('[name="optscheme"]')){
	    scheme = $(this).val();	
	}
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=templatelist',{term:term,progid:progid,yrlvl:yrlvl,scheme:scheme},
		function(r)
		{
		  if(r.success){
			$('.modal_template_list').find('tbody').html(r.content);
	        $('#modal_template').modal('show'); 
		  }
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	); 
  });
  
  $('body').on('click','.btn-modal-search',function(){
	var src_type  = $('#modal_filter').attr('data-type');
	var src_param = $(this).data('source');
	var param     = $(src_param).val();
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=filter',{type:src_type,args:param},
		function(r)
		{
		  if(r.success){
			$('.modal_list').html(r.content);
		  }
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
  });
  
  $('body').on('click','[data-list]',function(){
	 var tbl = $(this).closest('table');
     $(tbl).find('[data-list]').removeClass('info');	 
     $(this).addClass('info');	 
  });
  
  $('body').on('click','.btn-templ-select',function(){
	  var templelem = $('.modal_template_list').find('.info');
	  var templid   = $(templelem).data('list');
	  var templname = $(templelem).data('code');
	  var refno     = $('#refno').val();
	  $('.templname').data('id',templid);
	  $('.templname').html(templname);
	  $('#refno').find('[value="'+refno+'"]').attr('data-templateid',templid);
	  $('#refno').find('[value="'+refno+'"]').attr('data-template',templname);
	  gettemplatedetails(templid);
	  $('#modal_template').modal('hide');
  });	  
  
  $('body').on('click','.btn-modal-select',function(){
	 var type = $('#modal_filter').attr('data-type');
	 var data = $('#modal_filter').find('.info');
	 var xid  = $(data).data('list');
	 switch(type){
		case 'student':
		   $('.studno').html($(data).data('list'));
		   $('.yrlvl').html($(data).data('yrlvl'));
		   $('.yrlvl').data('yrlvlid',$(data).data('yrlvlid'));
		   $('.prog').html($(data).data('progname'));
		   $('.prog').data('progid',$(data).data('progid'));
		   $('#studname').val($(data).data('name'));
		   getstudentinfo(xid);
        break;
        case 'scholarship':
		   var name = $(data).attr('data-name');
		   $('#scholprovider').val(name);
		   $('#scholprovider').attr('data-id',xid);
		   getschoprovider(xid);
        break;		
	 }
	 $('#modal_filter').modal('hide');
	 $('.modal_list').html('');	 
  });
  
  $('body').on('click','.btn-refno-refresh',function(){
	$('#refno').trigger('change');
	$('.btn-discount-refresh').trigger('click');
  });
  
  $('body').on('click','.validate',function(){
	var regid   = $('#refno').val();
	var tmplid  = $('.templname').attr('data-id');
	var isvalid = $(this).attr('data-valid');
	
	if(regid==undefined || regid<=0){
	  msgbox('error','Nothing to validate');
	  return false;
	}
	
	if((tmplid<=0 || tmplid==undefined) && isvalid!=1){
	  msgbox('error','Not yet assessed!');
	  return false;
	}
	
	if(regid>0 && (tmplid>0 || isvalid==1)){
	var opt = 1;
	var msg = '<i class="fa fa-warning text-danger"></i> Are you sure you want to validate this transaction?';
	if(isvalid==1){
	  msg = '<i class="fa fa-warning text-danger"></i> Are you sure you want to remove validation?'; 
	  opt = 0;
	}
	
	confirmEvent(msg,function(sel){
	if(sel==false){
	 //return false;
	}else{
	progressBar('on');
		setAjaxRequest(
			base_url+'accounting/txn?event=validation',{opt:opt,regid:regid},
			function(r){
			  $('#refno').find('[value="'+r.regid+'"]').attr('data-validator',r.regid);
			  $('#refno').find('[value="'+r.regid+'"]').attr('data-validate',r.vdate);
			  $('#refno').find('[value="'+r.regid+'"]').data('validate',r.vdate);
			  if(r.vdate==''){
				 $('.validate').attr('data-valid',0);
				 $('.validate').html('&nbsp;&nbsp;&nbsp;<i class="text-danger pull-right">[Validation]</i>');
			  }else{
				 $('.validate').attr('data-valid',1);
				 $('.validate').html(r.vdate+'&nbsp;&nbsp;<i class="text-danger pull-right">[Validation]</i>');
			  }
			  
			  setTimeout(function(){
			  $('.btn-refno-refresh').trigger('click');
			  progressBar('off');
			  },200);
			},
			function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to execute!',function(){return;});	
			  progressBar('off');
			}
		);
	}	
	  });
	}
  });
  
  $('body').on('change','#refno',function(){
	 var studno    = $('.studno').html(); 
	 var refno     = $(this).val(); 
	 var termid    = $('#refno').find('[value="'+refno+'"]').data('termid');
	 var ayterm    = $('#refno').find('[value="'+refno+'"]').data('ayterm');
	 var regdate   = $('#refno').find('[value="'+refno+'"]').data('regdate');
	 var yrlvl     = $('#refno').find('[value="'+refno+'"]').data('yrlvlid');
	 var xyrlvl    = $('#refno').find('[value="'+refno+'"]').data('xyrlvlid');
	 var valdate   = $('#refno').find('[value="'+refno+'"]').data('validate');
	 var templid   = $('#refno').find('[value="'+refno+'"]').attr('data-templateid');
	 var templname = $('#refno').find('[value="'+refno+'"]').attr('data-template');
	 var studstats = $('#refno').find('[value="'+refno+'"]').data('studstats');
	 var studphoto = $('#refno').find('[value="'+refno+'"]').data('studpic');
	 
	 optbtn(1);
	 $('.termid').data('termid',termid);
	 $('.termid').html(ayterm);
	 $('.regdate').html(regdate);
	 if(valdate==''){
	 $('.validate').attr('data-valid',0);
	 $('.validate').html('&nbsp;&nbsp;&nbsp;<i class="text-danger pull-right">[Validation]</i>');
	 }else{
	 $('.validate').attr('data-valid',1);
	 $('.validate').html(valdate+'&nbsp;&nbsp;<i class="text-danger pull-right">[Validation]</i>');
	 }
	 
	 $('.templname').data('id',templid);
	 $('.templname').html(templname);
	 $('.studno').data('stats',studstats);
	 if(studphoto)
	   $('.entryphoto').attr('src',studphoto);
     else
	   $('.entryphoto').attr('src',defaultimg);
   
	 if(valdate!=''){optbtn(0); }
	 getstudentjournal(termid,refno,studno,templid);
	 grdlimitview(xyrlvl);
  });
  
  $('body').on('click','.btn-discount-new',function(){
	 $('#provider').val(1); 
	 $('#scholprovider').val(''); 
	 $('#scholprovid').val(''); 
	 $('#schoOption').val(1);
	 $('#scholamount').val('0.00');
	 $('#schoOption').trigger('change');
  });
  
  $('body').on('click','.btn-discount-refresh',function(){
	 var studno    = $('.studno').html(); 
	 var refno     = $('#refno').val(); 
	 var termid    = $('#refno').find('[value="'+refno+'"]').data('termid');
	 getstudentdiscount(termid,refno,studno);
  });
  
  $('body').on('change','#schoOption',function(){
	 var optval = $(this).val();
	 switch(optval){
		case '1':
		  $('#application').removeClass('hidden');
		  $('#template').addClass('hidden');
		  $('#scholamount').closest('.input-group').removeClass('hidden');
		  $('.optgrant').addClass('hidden');
        break;		
		case '2':
		  $('#application').addClass('hidden');
		  $('#template').removeClass('hidden');
		  $('#scholamount').closest('.input-group').addClass('hidden');
		  $('.optgrant').removeClass('hidden');
        break;		
	 }
  });
  
  $('body').on('click','[data-btntarget]',function(){
	 var trgt = $(this).attr('data-btntarget');
	 var lbl  = $(this).attr('data-label'); 
	 var val  = $(this).attr('data-value');
     $(trgt).html(lbl);	 
     $(trgt).attr('data-value',val);	 
  });
  
  $('body').on('click','.btn-discount-delete',function(){
	var xid = $('#tbldiscount').find('.active-row').attr('data-refno');
	if(xid==undefined || xid==''){return false;}
	  
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=rem_discount'
	   ,{refno:xid}
	   ,function(r){
		  $('.btn-discount-refresh').trigger('click');
		  $('.btn-refno-refresh').trigger('click');
		  confirmEvent('<i class="fa fa-check text-success"></i> '+r.content,function(){return;});	 
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to delete Data!',function(){return;});	
		  progressBar('off');
		}
	);	
  });
  
  $('body').on('click','.btn-print-assess',function(){
	var studno           = $('.studno').html(); 
	var refno            = $('#refno').val(); 
	var termid           = $('#refno').find('[value="'+refno+'"]').data('termid');
	var url              = base_url+'enrollment/print_report?event=assessment&term='+termid+'&campus=1&group=0&regid='+refno+'&studno='+studno+'&yrlvl=1&major=0&progid=5';
	//window.location.href = url;
    window.open(url,'_blank');
  });
  $('body').on('click','.btn-print-otherx',function(){
	var studno           = $('.studno').html(); 
	var refno            = $('#refno').val(); 
	var termid           = $('#refno').find('[value="'+refno+'"]').data('termid');
	var url              = base_url+'enrollment/print_report?event=otherassess&term='+termid+'&campus=1&group=0&regid='+refno+'&studno='+studno+'&yrlvl=1&major=0&progid=5';
	//window.location.href = url;
    window.open(url,'_blank');
  });
  $('body').on('click','.btn-print-soa',function(){
	var studno           = $('.studno').html(); 
	var refno            = $('#refno').val(); 
	var termid           = $('#refno').find('[value="'+refno+'"]').data('termid');
	var url              = base_url+'enrollment/print_report?event=soa&term='+termid+'&campus=1&group=0&regid='+refno+'&studno='+studno+'&yrlvl=1&major=0&progid=5';
	//window.location.href = url;
    window.open(url,'_blank');
  });
  
  $('body').on('click','.btn-other-clear',function(){
	var studno   = $('.studno').html(); 
	var refno    = $('#refno').val(); 
	var termid   = $('#refno').find('[value="'+refno+'"]').data('termid');
	
    progressBar('on');
	setAjaxRequest(
		base_url+'accounting/txn?event=other_clear'
	   ,{termid:termid,studno:studno,regid:refno}
	   ,function(r){  
		  $('[data-otxn-acctid]').each(function(){
			var acctid = $(this).attr('data-otxn-acctid');
			var price  = $(this).find('[data-price]');		
			var qty    = $(this).find('[data-qty]');
			var amt    = $(this).find('[data-amt]');
			var tmparr = {};
				
			if($(qty).attr('data-qty')=='html')
				$(qty).html('0');
			else if($(qty).attr('data-qty')=='value')
				$(qty).val('0');
				
			if($(amt).attr('data-amt')=='html')
				$(amt).html('0.00');
			else if($(amt).attr('data-amt')=='value')
				$(amt).val('0.00');
		  });
          $('[data-source]').html('0.00');		  
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
		  progressBar('off');
		}
	);
  });
  $('body').on('click','.btn-other-save',function(){
	 var studno   = $('.studno').html(); 
	 var refno    = $('#refno').val(); 
	 var termid   = $('#refno').find('[value="'+refno+'"]').data('termid');
	 var odata = [];
     $('[data-otxn-acctid]').each(function(){
		var acctid = $(this).attr('data-otxn-acctid');
        var price  = $(this).find('[data-price]');		
        var qty    = $(this).find('[data-qty]');
        var amt    = $(this).find('[data-amt]');
		var tmparr = {};
		
		if($(price).attr('data-price')=='html')
		    price = $(price).html();
		else if($(price).attr('data-price')=='value')
		    price = $(price).val();
			
		if($(qty).attr('data-qty')=='html')
		    qty = $(qty).html();
		else if($(qty).attr('data-qty')=='value')
		    qty = $(qty).val();
			
		if($(amt).attr('data-amt')=='html')
		    amt = $(amt).html();
		else if($(amt).attr('data-amt')=='value')
		    amt = $(amt).val();
			
		tmparr['acctid'] = acctid;
		tmparr['price']  = parseFloat(price);
		tmparr['qty']    = parseFloat(qty);
		tmparr['amt']    = parseFloat(amt);
		
		if(parseFloat(amt)>0 && acctid!=0){
		  odata.push(tmparr);
		}
	 });
    
	var xacct  = $('.otherx').val();
	var xdesc  = $('.remarkx').val();
    var xprice = $('.pricex').val();
    if(parseFloat(xprice)>0){
	  var tmparr = {};
	  tmparr['acctid'] = xacct;
	  tmparr['desc']   = xdesc;
	  tmparr['price']  = parseFloat(xprice);
	  tmparr['qty']    = 1;
	  tmparr['amt']    = parseFloat(xprice);
	  odata.push(tmparr);
	} 	 
	
    if(odata.length>0){
	 progressBar('on');
     setAjaxRequest(
		base_url+'accounting/txn?event=other_acct'
	   ,{termid:termid,studno:studno,regid:refno,other:odata}
	   ,function(r){
		  $('.btn-refno-refresh').trigger('click');
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
		  progressBar('off');
		}
	 );
    }	 
  });
  
  $('body').on('blur','.component',function(){
	 var tpane  = $(this).closest('.tab-pane');
	 var trrow  = $(this).closest('.formula-row');
	 var tmpval = $(this).val();
    
	 if(tmpval=='' || tmpval=='-' || tmpval==undefined){
		tmpval = '0.00';
	 }
     $(this).val(tmpval);	 
	 exec_formula(trrow);
	 summarizetab(tpane);
	 summarizetable($('#tblassess'));
  });	
     
  $('body').on('click','.btn-discount-save',function(){
	var campus   = 1;
	var idtype   = 1;
	var studno   = $('.studno').html(); 
	var refno    = $('#refno').val(); 
	var termid   = $('#refno').find('[value="'+refno+'"]').data('termid');
	var feeid    = $('.templname').attr('data-id');
	var provid   = $('#scholprovider').attr('data-id');
	var provtype = $('#provider').val();
	var option   = $('#schoOption').val();
	var templid  = 0;
	var grant    = $('#template').val();
	var schoacct = $('#application').val();
	var custom   = $('#customdisc').val();
	var fund     = 0;
	var disc     = 0;
	var perc     = 0;
	var onnet    = (($('#applyNet').is(':checked'))?1:0);
	var data     = {campus:campus
	               ,termid:termid
				   ,idtype:idtype
				   ,studno:studno
				   ,regid:refno
				   ,feeid:templid
				   ,provid:provid
				   ,provtype:provtype
				   ,grant:grant
				   ,schoacct:schoacct
				   ,option:option
				   ,fund:fund
				   ,discount:disc
				   ,perc:perc
				   ,onnet:onnet};
	switch(option){
	   case '1':
		  data['fund'] = $('#scholamttype').attr('data-value');
		  if(data['fund']=='0')
			data['perc'] = $('#scholamount').val();
		  else
			data['discount'] = $('#scholamount').val();
       break;	   
	   case '2':
		  data['fund'] = $('[name="optgranttempl"]:checked').val();
		  if(data['fund']==0)
		    data['perc']     = parseFloat(custom);
	      else
			data['discount'] = parseFloat(custom);  
       break;	   
	}			   
	
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=savediscount'
	   ,data
	   ,function(r){
		  $('.btn-discount-refresh').trigger('click');
		  $('.btn-refno-refresh').trigger('click');
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save Data!',function(){return;});	
		  progressBar('off');
		}
	);		
  });  
   
  $('body').on('click','.btn-remove-other',function(){
    var entryid = $(this).attr('data-id');
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=other_rem',{entryid:entryid},
		function(r){
		  $('.btn-refno-refresh').trigger('click');
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to remove Data!',function(){return;});	
		  progressBar('off');
		}
	);
  });
  
  $('body').on('click','.btn-add-other',function(){
	$('[data-otxn-acctid]').each(function(){
	var acctid = $(this).attr('data-otxn-acctid');
	var price  = $(this).find('[data-price]');		
	var qty    = $(this).find('[data-qty]');
	var amt    = $(this).find('[data-amt]');
	var tmparr = {};
		
	if($(qty).attr('data-qty')=='html')
		$(qty).html('0');
	else if($(qty).attr('data-qty')=='value')
		$(qty).val('0');
		
	if($(amt).attr('data-amt')=='html')
		$(amt).html('0.00');
	else if($(amt).attr('data-amt')=='value')
		$(amt).val('0.00');
	});
	$('.dvothertxn').find('[data-source]').html('0.00');	
    $('#modal_otheracct').modal('show');
  }); 
  
  $('body').on('click','[data-menu]',function(){
      var tmpmenu = $(this).attr('data-menu');
	  $('.menu-'+tmpmenu).trigger('click');
  });
});


function optbtn(x){
  x = ((x==undefined)?0:x);
  if(x==0){
	$('.btn-tblfee').attr('disabled',true); 
	//$('.btn-other-save').attr('disabled',true);  
	//$('.btn-other-clear').attr('disabled',true);  
  }else{
	$('.btn-tblfee').removeAttr('disabled');
	//$('.btn-other-save').removeAttr('disabled');
	//$('.btn-other-clear').removeAttr('disabled');  
  }	
}

function getschoprovider(provid){
	$('#template').find('[value!="-1"]').remove();
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=schoprovider',{provid:provid},
		function(r)
		{
		  $('#template').append(r.template);
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);		
}

function gettemplatedetails(templid){
	var studno    = $('.studno').html(); 
	var refno     = $('#refno').val(); 
	var termid    = $('#refno').find('[value="'+refno+'"]').data('termid');
	 
  	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=template',{termid:termid,studno:studno,refno:refno,templid:templid},
		function(r)
		{
		  $('#tblassess').find('tbody').html(r.content);
		  generate_summary();
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);		
}

function getstudentjournal(termid,refno,studno,templid){
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=journal',{termid:termid,refno:refno,studno:studno,templid:templid},
		function(r){
		  $('#tblassess').find('tbody').html(r.content);
		  $('#tbldiscount').find('tbody').html(r.discount);
		  $('.dvothertxn').find('input[data-qty]').val('0');
		  $('.dvothertxn').find('.formula-total').html('0.00');
		  $('.dvothertxn').find('[data-source]').html('0.00');
		  console.log('clear');
		  setothertxn(r.othertxn);
		  generate_summary();
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);	
}

function setothertxn(arrdata){
  if(arrdata){
    $.each(arrdata,function(i,d){
	  if($('[data-otxn-acctid="'+d['AccountID']+'"]').find('[data-price]').attr('data-price')=='value'){
	   $('[data-otxn-acctid="'+d['AccountID']+'"]').find('[data-price]').val(d['Price']);
	  }else{
	   $('[data-otxn-acctid="'+d['AccountID']+'"]').find('[data-price]').html(d['Price']);	  
	  }
	  $('[data-otxn-acctid="'+d['AccountID']+'"]').find('[data-qty]').val(d['Count']);
	  $('[data-otxn-acctid="'+d['AccountID']+'"]').find('[data-qty]').trigger('blur');
    });		
  }	
}

function generate_summary(){
  var thd = $('#tblassess').find('thead');
  var tbl = $('#tblassess').find('tbody');
  var tft = $('#tblassess').find('tfoot');
  var totalassess     = 0;
  var totaldiscount   = 0;
  var totalfinancial  = 0;
  var totalpayment    = 0;
  var totalcreditmemo = 0;
  var totaldebitmemo  = 0;
  var totalbalance    = 0;
  var totalcolumn     = {3:0,4:0,5:0,6:0,7:0,8:0,9:0,10:0,11:0,12:0,13:0,14:0,15:0,16:0,17:0,18:0,19:0,20:0,21:0};
  var columnattrb     = {3:'data-assess',4:'data-discount',5:'data-financial',6:'data-netassess'
                        ,7:'data-paymenta',8:'data-paymentb',9:'data-paymentc',10:'data-paymentd'
                        ,11:'data-paymente',12:'data-paymentf',13:'data-paymentg',14:'data-paymenth',15:'data-paymenti',16:'data-paymentj'
						,17:'data-paydiscount',18:'data-payment',19:'data-creditmemo',20:'data-refund',21:'data-balance'};
  $(tbl).find('[data-entryid]').each(function(){
     var tmprowtotal = 0;
	 for(var i=3;i<=21;i++){
		var columnval  = $(this).find('td:nth-child('+i+')').attr(columnattrb[i]);
        var thide      = $(this).find('td:nth-child('+i+')').is('.thide');		
		totalcolumn[i] = totalcolumn[i]+ parseFloat(columnval); 
		if(i>=7 && i<=16){
		 tmprowtotal    = tmprowtotal + parseFloat(columnval); 
		}
		
		$('.totalrow').find('td:nth-child('+(i-1)+')').html(moneyFormat(totalcolumn[i],2,true));
		$(thd).find('th:nth-child('+i+')').removeClass('hidden');
		$(tbl).find('td:nth-child('+i+')').removeClass('hidden');
		$(tft).find('td:nth-child('+i+')').removeClass('hidden');
		if(thide==true){
		   $(thd).find('th:nth-child('+i+')').addClass('hidden');
		   $(tbl).find('td:nth-child('+i+')').addClass('hidden');
		   $(tft).find('td:nth-child('+i+')').addClass('hidden');
		}
	 }
	 
	 if(tmprowtotal>0){
	   $(this).find('[data-netassess]').attr('data-netassess',moneyFormat(tmprowtotal,2,false));
	   $(this).find('[data-netassess]').html(moneyFormat(tmprowtotal,2,true));
	 }
	 
	 var assess     = $(this).find('[data-netassess]').attr('data-netassess'); 
	 var discount   = $(this).find('[data-discount]').attr('data-discount'); 
	 var financial  = $(this).find('[data-financial]').attr('data-financial'); 
	 var payment    = $(this).find('[data-payment]').attr('data-payment'); 
	 var creditmemo = $(this).find('[data-creditmemo]').attr('data-creditmemo'); 
	 var debitmemo  = $(this).find('[data-refund]').attr('data-refund'); 
	 var balance    = $(this).find('[data-balance]').attr('data-balance');
         totalassess     += parseFloat(assess);
         totaldiscount   += parseFloat(discount);
         totalfinancial  += parseFloat(financial);
         totalpayment    += parseFloat(payment);
         totalcreditmemo += parseFloat(creditmemo);
		 totaldebitmemo  += parseFloat(debitmemo);
         totalbalance     = (totalassess+totaldebitmemo)-totalpayment-totalcreditmemo;
  });
  
  $('.netassess').html(moneyFormat(totalassess,2,true));
  $('.totalpayment').html(moneyFormat(totalpayment,2,true));	
  $('.totalbalance').html(moneyFormat(totalbalance,2,true));	
  $('.totaloutbalance').html(moneyFormat(totalbalance,2,true));	
}

function moneyFormat(amount,decimal,comma){
      amount      = ((amount==undefined || amount=='0')?'0.00':amount);
      decimal     = ((decimal==undefined)?2:decimal);
      comma       = ((comma==undefined)?false:comma);
  if(amount!=0){
    var tmptotal    = amount.toFixed(decimal);
    var tmpsplit    = tmptotal.toString().split(".");
    if(comma==true){
      tmpsplit[0] = parseInt(tmpsplit[0]).toLocaleString();
    }	  
    return tmpsplit.join(".");		
  }else{
	return '0.00';  
  }
}

function getstudentdiscount(termid,refno,studno){
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=discount',{termid:termid,refno:refno,studno:studno},
		function(r)
		{
		  $('#tbldiscount').find('tbody').html(r.content);
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);	
}

function getstudentinfo(stdno){
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=reginfo',{type:1,args:stdno},
		function(r)
		{
		  $('#refno').html(r.content);
		  if($('#refno').find('option').length>0){
		    $('#refno').trigger('change');
			$('.btn-discount-refresh').trigger('click');
		  }
		  progressBar('off');
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
}

function exec_formula(trrow){
	var operate   = $(trrow).data('operate');
	var lbltotal  = $(trrow).find('.formula-total');
	var tmpresult = 0;
	var tmpval    = 0;
    switch(operate){
		case 'multiply':
		   tmpresult = 1;
		   $(trrow).find('.component').each(function(){
			   tmpval = 0;
			   if($(this).is('input[type="text"]') || $(this).is('select'))
				   tmpval = $(this).val();
			   else
				   tmpval = $(this).html();
			 //alert(tmpval);
			   tmpval = parseFloat(tmpval);
			   tmpresult = tmpresult*tmpval;
		   });
		break;
	}
	if(tmpresult>0)
	   tmpresult = moneyFormat(tmpresult,2,false);
    else if(tmpresult<0)
	   tmpresult = moneyFormat(tmpresult,2,false);
    else
	   tmpresult = '0.00';
	$(lbltotal).html(tmpresult);
}
  
function grdlimitview(yrlvl){
	 yrlvl  = ((yrlvl==undefined)?1:yrlvl);
	 $('[data-grdmin]').each(function(){
		var minx = $(this).attr('data-grdmin');
		var maxx = $(this).attr('data-grdmax');
	  //alert(minx);
	    if(parseInt(minx)<=parseInt(yrlvl) && parseInt(maxx)>=parseInt(yrlvl))
		  $(this).removeClass('hidden');			
		else{
		  $(this).addClass('hidden');			
		  $(this).find('[data-qty]').val('0');
		  $(this).find('[data-qty]').trigger('blur');
		}  
	 });
}	  


function summarizetab(tpane){
   var target    = $(tpane).data('target');
   var tmpresult = 0;
   $(tpane).find('.formula-total').each(function(){
	   var tmpval = 0;
	   if($(this).is('input[type="text"]') || $(this).is('select'))
		   tmpval = $(this).val();
	   else
		   tmpval = $(this).html();
	 //alert(tmpval);
	   tmpval = parseFloat(tmpval);
	   tmpresult = tmpresult+tmpval;
   });
   
	if(tmpresult>0)
	   tmpresult = tmpresult.toFixed(4);
    else if(tmpresult<0)
	   tmpresult = tmpresult.toFixed(4);
    else
	   tmpresult = '0.00';
   $('[data-source="'+target+'"]').html(tmpresult);
   if($('[data-source="'+target+'"]').closest('[data-assess]').length>0){
	 $('[data-source="'+target+'"]').closest('[data-assess]').attr('data-assess',tmpresult);
	 $('[data-source="'+target+'"]').closest('[data-assess]').attr('data-netamt',tmpresult);
   }
}

function summarizetable(tbl){
   var tmpresult = 0;
   $(tbl).find('.tblcomponent').each(function(){
	   var tmpval = 0;
	   if($(this).is('input[type="text"]') || $(this).is('select'))
		   tmpval = $(this).val();
	   else
		   tmpval = $(this).html();
	 //alert(tmpval);
	   tmpval = parseFloat(tmpval);
	   tmpresult = tmpresult+tmpval;
   });
   
	if(tmpresult>0)
	   tmpresult = tmpresult.toFixed(4);
    else if(tmpresult<0)
	   tmpresult = tmpresult.toFixed(4);
    else
	   tmpresult = '0.00';
   $(tbl).find('.tbltotal').html(tmpresult);
}

function save_tdedit(acctid,point,amt){
    var stdno = $('#student-name').html();
	var regid = $('#refno').val();
	var term  = $('#refno').find('option[value="'+regid+'"]').attr('data-termid');
	if(stdno==undefined || regid==undefined){
	  return false;
	}
	progressBar('on');
    setAjaxRequest(
		base_url+'accounting/txn?event=save_payment',{aid:acctid,point:point,amt:amt,term:term,refno:regid,student:stdno},
		function(r){
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save data!',function(){return;});	
		  progressBar('off');
		}
	);
}