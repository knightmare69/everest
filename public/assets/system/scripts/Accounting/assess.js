var MOD = {
    init: function(){
        this.load();
        this.load_term($('#term'));
    },
    add : function(){ this.loadStudents(); },
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    load : function(){
      $('body').on('click', '.soa', $.proxy(this.soa, this) );
      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      $('body').on('click','.views', $.proxy(this.help,this));
      $('body').on('click','.grades', $.proxy(this.init_grades,this));
      $('body').on('change', '#term', function(e){ MOD.load_term($(this)); });
      $('body').on('change', '#schoOption', function(e){ MOD.change_setting(e); });
      $('body').on('keypress','#searchkey', function(e){ if(e.keyCode ==  13){ MOD.searchOnEnter(); }});
      $('body').on('keypress','#idno',function(e){  if(e.keyCode ==  13){
        Students.get({'student-search-text': $('#idno').val(), 'search-campus': 1 });
      }});
      $('body').on('click', '.student-select', $.proxy(Students.info,this));
      $('body').on('mousedown','#tblgrades tbody td', function(e){
         MOD.closemenu();
         if(e.button == 2){
              MOD.initrclick(e);
         }
      });

      $('.remove_validation_btn').click(function(){
        bootbox.confirm({
            message: "Are you sure?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result){
                    setAjaxRequest( base_url + page_url + '/event',
                        {
                            'event': 'remove-validation',
                            'reg': $('#term').find("option:selected").attr('data-reg')
                        },
                        function(r) {
                            location.reload();
                        },
                        undefined,
                        'json',
                        true,
                        false
                    );
                }
            }
        });
      });

      $('body').on('click', '.fees-filter', function(e) {
            $('.fees-filter').removeClass('active');
            $(this).addClass('active');
            var f = $(this).attr('data-val');
            if(f == 0){
                 $('#fees-table tbody tr').removeClass('hide');
            }else{
                $('#fees-table tbody tr').each(function(){
                    if( $(this).attr('data-scheme') == f ){
                        $(this).removeClass('hide');
                    }else{
                        $(this).addClass('hide');
                    }
                });
            }
        });

    },
    load_term: function($t){
        var d = $t.find('option:selected').data();
        //localStorage.TermID = t;
        //location.replace(base_url+page_url+'?t='+ $('#term').val());
        if(d != undefined)
        {
            $('#vdate').html(d.odate);
            $('#tcode').html(d.code);
            $('#reg_id').html(d.reg);
            MOD.load_fees(d.term, d.reg);
        }
    },
    init_grades: function(e){
        var v = $(e.target).attr('data-view');
        $('.gradeview').hide();
        $('#'+v).show();
    },
    help: function(e){
        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');
        $('.tabpane').hide();
        var data = $(e.target).data();
        switch (data.menu) {
            case 'assessment': $('#assessment').show(); break;
            case 'subjects': this.initSubjects(); break;
            case 'discount': this.init_discount(); break;d
        }
    },
    reload : function(e){
        var m = $('.profile-usermenu').find('li.active').find('a').attr('data-menu');
        console.log(m);
        switch(m){
            case 'assessment': $('#assessment').show(); break;
            case 'subjects': this.initSubjects(); break;
            case 'discount': this.init_discount(); break;
        }
    },
    load_fees: function(term, reg){
        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-assessment', 'idno': getParameterByName('idno'), 'term': term, 'ref': reg }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#feetable').html(r.view)
            }
        },undefined,'json',true,false);
    },
    summary: function(){
        setAjaxRequest( base_url + page_url + '/event', {'event': 'summary', 'idno': getParameterByName('idno')}, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                showModal({ name: 'large', title: 'Summary of Grades', button:{caption: 'Recompute',menu:'recalc'}, content: r.view });
            }
        },undefined,'json',true,false);
    },
    initSubjects : function(){
        $('#subjects').show();
        $('#subjects').html('<h4><i class="fa fa-cog fa-spin"></i> Loading...</h4>')
        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-subjects', 'reg': $('#term').find("option:selected").attr('data-reg') }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#subjects').html(r.data);
            }
        },undefined,'json',true,false);
    },
    init_discount : function(){
        $('#discount').show();
        $('#discount').html('<h4><i class="fa fa-cog fa-spin"></i> Loading...</h4>')
        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-discounts', 'reg': $('#term').find("option:selected").attr('data-reg') }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#discount').html(r.data);
            }
        },undefined,'json',true,false);
    },

    soa: function ()
    {
      console.log('haha sample');
    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'search': this.add(e); break;
            case 'select': this.selectThis(e); break;
            case 'print': this.print(e); break;
            case 'reload': this.reload(e); break;
            case 'cancel-menu': this.closemenu(); break;
            case 'print-eval': this.printEval(e); break;
            case 'summary': this.summary(); break;
            case 'recalc': this.recompute(); break;
            case 'template': this.template(); break;
            case 'fee-select': this.setAssessment(); break;
            case 'add-discount': this.discount('create'); break;
            case 'rem-discount': this.discount('remove'); break;
            case 'recompute-discount': this.discount('recompute'); break;
            case 'apply': this.discount(d.menu); break;
            case 'reset': this.discount(d.menu); break;
            case 'save-new-discount': this.discount('save'); break;
            case 'rem': this.remove_item(e); break;
            case 'add': this.add_item(e); break;
            case 'add-row-proceed': this.add_account(e); break;
            case 'schedule': this.schedule(e); break;
            case 'soa': window.open(base_url + page_url + "/soa?termid="+ $('#term').val(),'_blank');
            case 'assessment': window.open(base_url + page_url + "/assessment?termid="+ $('#term option:selected').attr('data-term')+'&ref='+$('#reg_id').text()+'&idno='+ $('#idno').val(), '_blank');
        }
    },
    schedule: function(){
        var t = $('#term').find('option:selected');
         setAjaxRequest( base_url + page_url + '/event?event=schedule', {reg: t.attr('data-reg') },
                function(req) {
                    if (req.error == false) {
                        showModal({ name: 'large', title: 'Schedule of Fees',button:{caption: 'Select',menu:'fee-select'}, content: req.view});
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
            );
    },

    add_account: function()
    {
        var data = $('#add_form').serialize();
        var t = $('#term').find('option:selected');
        var x = $('#acct_txn').val();
        if( x == '1'){

          sysfnc.msgcb("Add an Account", "Do you want to add the account?", function(){

                setAjaxRequest( base_url + page_url + '/event?event=add-account&reg='+t.attr('data-reg'), data,
                function(req) {
                    if (req.error == false) {
                        msgbox('success', req.message);
                        location.reload();
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
                );
            });
        } else{
            alert('Sorry, Other Transactions not yet activated.');
        }

    },

    add_item: function()
    {
        $('#modal_account').modal('show');
    },

    remove_item: function(){

        var row = $('#tbl_assessment tbody tr.active-row').attr('data-entryid');
        var code = $('#tbl_assessment tbody tr.active-row').attr('data-acctcode');
        if (row == undefined || row == '0'){
            return;
        }
        sysfnc.msgcb("Remove Account", "Do you want to remove the account '"+code+"'?", function(){

                setAjaxRequest( base_url + page_url + '/event', {event: 'remove', ref: row },
                function(req) {
                    if (req.error == false) {
                        msgbox('success', req.message);
                        location.reload();
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
                );
            });
    },
    setAssessment: function(){

        var num = getParameterByName('idno')
        var term = $('#term').find('option:selected').attr('data-term');
        var f = $('#fees-table tbody tr.active-row').attr('data-id');
        var ref = $('#term').val();
        var txn = 1;

        setAjaxRequest( base_url + 'cashier/cashiering/event?event=get-fees', {idno: num, term: term, fee: f, refno:ref, txn: txn },
            function(req) {
                if (req.error == false) {
                    location.reload();
                    $('#modal_large').modal('hide');
                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );

    },


    recompute: function(){
        var idno = getParameterByName('idno');
        var t=$('#term').find('option:selected');

        if (t){

            setAjaxRequest( base_url + page_url + '/event', {'event': 'recompute', 'idno': idno, 'reg': t.attr('data-reg')}, function(r) {
                if (r.error) {
                    msgbox('error', r.message);
                }else{
                    msgbox('success', r.message);
                    location.reload();
                }
            },undefined,'json',true,false);

        }else{
            msgbox('error', 'Please select term to recompute...');
        }

    },

    print : function(){
        var id = getParameterByName('idno');
        var mode = $('.grades.active').attr('data-view');
        if(id == 0 || id == undefined || id == '' ){
            alert('no student found!');
            return false;
        }
        window.open(base_url+ page_url + '/print?idno='+id+'&m='+ mode +'&t='+ Math.random() ,'_blank');
    },

    initrclick: function(e){
        var pos = $(e.target).position();
        var cs = {top: pos.top, left: pos.left}

        $( "#mouse_menu_right" ).css(cs);
        $('#mouse_menu_right').show('50');
    },

     printEval : function(){
        window.open(base_url+'enrollment/transcript/printEval?idno='+ getParameterByName('idno') +'&t='+ Math.random() ,'_blank');
	 },

     template: function(){
        var num = getParameterByName('idno');
        var term = $('#term').find('option:selected').attr('data-term');
        var validate = $('#vdate').html();
        if(num == '' || num == undefined){
            msgbox('warning', 'Student not found!');
            return false;
        }

        if( validate == '' || validate == undefined ){
            setAjaxRequest( base_url + 'cashier/cashiering/event?event=fees', {idno: num, term: term },
                function(req) {
                    if (req.error == false) {
                        showModal({ name: 'large', title: 'Searched Fees Template ',button:{caption: 'Select',menu:'fee-select'}, content: req.list});
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
            );
        }else{
            msgbox('warning', 'Unable to proceed student transaction already validated...');
        }
    },
    change_setting: function(e){
        var s = $(e.target).val();
        if(s == '4'){
            /*Alloted Amt*/
            $('.label-amount').html('Enter Fixed Amount');
            $('.compute-from-option').hide();
            $('#scholamttype').attr('IsFixed',1);
        }else if(s == '2'){
            /*Template*/
        }else{
            $('.label-amount').html('Enter Percent');
            $('.compute-from-option').show();
            $('#scholamttype').attr('IsFixed',0);
        }
    },
    discount: function(mode){
        if(mode == 'create'){
            var num = getParameterByName('idno');
            var term = $('#term').find('option:selected').attr('data-term');
            var reg = $('#term').val();
            if(num == '' || num == undefined){
                msgbox('warning', 'Student not found!');
                return false;
            }
            setAjaxRequest( base_url + page_url + '/event', {event: 'discount',mode:'create' , idno: num, reg:  reg, term: term },
                function(req) {
                    if (req.error == false) {
                        showModal({ name: 'large', title: 'Create Discount',button:{caption: 'Save',menu:'save-new-discount'}, content: req.html});
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
            );
        }else if (mode == 'apply'){
            var amt = $('#scholamount').val();
            var typ = $('#scholamount').attr('IsFixed');
            if( parseFloat(amt) == 0){
                $('#scholamount').addClass('required');
                msgbox('error', 'Please input percent/amount to proceed.');
                return false;
            }
            this.compute_discount_percent(amt);
        } else if (mode =='reset'){
            $('#tbl_discount tbody tr').each(function(){
                $(this).find('td.discount').html('0.00');
                $(this).find('td.discount').attr('data-amount',0)
            });
            $('#tbl_discount tfoot tr').find('td.total_aid').html('0.00');
        }
        else if (mode =='save')
        {
            var data = [];
            var d;
            var term = $('#term').find('option:selected').attr('data-term');
            $('#tbl_discount tbody tr').each(function(){
               var n = $(this).find('td.discount').attr('data-amount');
               if( parseFloat(n) > 0)
               {
                  d = {id : $(this).attr('data-acctid') ,  amt :  n };
                  data.push(d);
               }
            });
            setAjaxRequest( base_url + page_url + '/event?' + $('#discount_form').serialize() , {event: 'discount',mode:'save' ,'SchoAccounts': $('#scholamount').attr('IsFixed') , regid : $('#reg_id').html() , term:term,  'total': $('.total_aid').html() ,  data: data },
                function(req) {
                    if (req.error == false) {
                        msgbox('success', req.message);
                        MOD.init_discount();
                        $('#modal_large').modal('hide');
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
            );
        }
        else if (mode =='remove')
        {
            sysfnc.msgcb("Remove Discount", "Do you want to remove discount?", function(){
                var $row = $('#tbl_aid tbody tr.active-row');
                setAjaxRequest( base_url + page_url + '/event', {event: 'discount',mode:'remove' , ref: $row.attr('data-refno') },
                function(req) {
                    if (req.error == false) {
                        msgbox('success', req.message);
                        MOD.init_discount();
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
                );
            });
        }
        else if (mode =='recompute')
        {
            sysfnc.msgcb("Recompute Discount", "Do you want to recompute discount?", function(){
                setAjaxRequest( base_url + page_url + '/event', {event: 'discount',mode:'recompute' , reg: $('#reg_id').html() },
                function(req) {
                    if (req.error == false) {
                        msgbox('success', req.message);
                        MOD.init_discount();
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                },
                function(err, resp_text) {
                    progressBar('off');
                    msgbox('error', resp_text);
                }, null, true
                );
            });
        }
    },
    compute_discount_percent: function(percent)
    {
        var opt = $('#schoOption').val();
        var apy = $('#application').val();
        var type = $('#compute_from').val();
        var disc = 0;
        var amt = 0;
        var computed =0 ;
        var total = 0;
        var debit = 0;
        $('#tbl_discount tbody tr').each(function(){
            var bal = $(this).find('td.bal').attr('data-amount');
            if( type ==  '0')
            {
              amt = $(this).find('td.gross').attr('data-amount');
            }
            else
            {
              amt = $(this).find('td.net').attr('data-amount');
            }
            debit += parseFloat(amt);
            computed = (parseFloat(percent)/100) * amt;
            //console.log(computed);
            if ( $(this).find('.box-child').prop('checked') ){
                total += computed;
            }
            if(opt == '1')
            {
                 /*per item*/
                if ( $(this).find('.box-child').prop('checked') ){
                    /*if(apy == '1'){
                        $(this).find('td.discount').attr('data-amount', computed);
                        $(this).find('td.discount').html(computed);
                    }else if (apy =='2' && parseFloat(bal) > 0 ){
                        $(this).find('td.discount').attr('data-amount', computed);
                        $(this).find('td.discount').html(computed);
                    }
                    */
                        $(this).find('td.discount').attr('data-amount', computed);
                        $(this).find('td.discount').html(format('#,##0.00',computed));
                }else{
                    $(this).find('td.discount').html('0.00');
                     $(this).find('td.discount').attr('data-amount',0)
                }
            }
        });
            if(opt == '3'){
                total = (parseFloat(percent)/100) * debit;
                $('#tbl_discount tbody tr').each(function(){
                    if ( $(this).find('.box-child').prop('checked')  && parseFloat(total) > 0 ){
                        var bal = 0;
                        var net = $(this).find('td.net').attr('data-amount');
                        if( parseFloat(total) > parseFloat(net)  ){
                            $(this).find('td.discount').attr('data-amount', net);
                            $(this).find('td.discount').html(format('#,##0.00',net) );
                            bal = 0;
                        }else{
                            $(this).find('td.discount').attr('data-amount', total);
                            $(this).find('td.discount').html(format('#,##0.00',total) );
                            bal = parseFloat(net) - parseFloat(total);
                        }
                        total = parseFloat(total) - parseFloat(net);
                        $(this).find('td.bal').html( format('#,##0.00',bal) );
                    }
                });
            }
            if(opt == '4'){
                var total = percent;
                $('#tbl_discount tbody tr').each(function(){
                    if ( $(this).find('.box-child').prop('checked') && parseFloat(total) > 0 ){
                        var bal = 0;
                        var net = $(this).find('td.net').attr('data-amount');
                        if( parseFloat(total) > parseFloat(net)  ){
                            $(this).find('td.discount').attr('data-amount', net);
                            $(this).find('td.discount').html(format('#,##0.00',net) );
                            bal = 0;
                        }else{
                            $(this).find('td.discount').attr('data-amount', total);
                            $(this).find('td.discount').html(format('#,##0.00',total) );
                            bal = parseFloat(net) - parseFloat(total);
                        }
                        total = parseFloat(total) - parseFloat(net);
                        $(this).find('td.bal').html( format('#,##0.00',bal) );
                    }
                });
            }
            var total = 0;
            $('#tbl_discount tbody tr').each(function(){
                if ( $(this).find('.box-child').prop('checked') ){
                    total += parseFloat( $(this).find('td.discount').attr('data-amount')) ;
                }
            });
            $('#tbl_discount tfoot tr').find('td.total_aid').html( format('#,##0.00',total) );
    },
};
var Students = {
    get: function(params) {
        setAjaxRequest( base_url + 'students/event?event=get',  params,
            function(req) {
                if (req.error == false) {
                    showModal({ name: 'large', title: 'Searched Student(s)', content: req.list});
                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#students-table-res').DataTable({ 'bDestroy': true, 'aaSorting': []});
                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },
    info: function(e) {
        var id = $(e.target).closest('tr').attr('data-id');
        $('.modal').modal('hide');
        if( id == undefined){
            msgbox('warning','No Selected student');
            return false;
        }
        location.replace(base_url+page_url+'?idno='+ id );
    },
    search_list: function() {
        setAjaxRequest(
            base_url + page_url + 'event', 'event=search&data_search=' + vars.searched_string + '&filters=' + JSON.stringify(vars.filter_stacked),
            function(req) {
                if (req.error == false) {
                    $('.div-stud-list').addClass('hide');
                    $('.div-stud-search-res').removeClass('hide').find('.general-item-list').html(req.list);
                    Students.get_students_photo();
                } else {
                    msgbox('error', req.message);
                }
            }, '', '', true
        );
        progressBar('off');
    },
};