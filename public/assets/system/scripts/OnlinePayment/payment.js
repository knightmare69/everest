var TOTAL_CASH = 0;
var TOTAL_CASH_I = 0;
var TRANSACTION_TEMPLATE;
var USER_TRANSACTION_DATA;
var TX_ID;
var SELECTED_STUDENT = new Object();
var ONLINE_PAYMENT = 0;
var TRXN_DESC;
var ONLINE_PAYMENT = {

    init: function () {
        this.mylink = base_url + "online-payment/" + page_url;
        this.addlistener();
    },

    load_billing: function (acadYearmTerm) {
        var academic_term = acadYearmTerm;
        var student_id = $('#students').length ? $('#students').val() : 0;
        var data = {
            event : 'billing',
            term : academic_term,
            studnum : student_id
        };

        var res = ajaxRequest(this.mylink + 'event', data, 'json', undefined, false);

        if (!res.error)
        {
            TRANSACTION_TEMPLATE = res.transaction;
            USER_TRANSACTION_DATA = res.student_details;
            CHARGE_SETUP = res.student_details.active_charge_setup;

            $('#billing').html(res.list);

            ONLINE_PAYMENT.bindData();
            ONLINE_PAYMENT.stepper(1)

            msgbox('warning', 'Please Select Payment Plan');
            
            Metronic.unblockUI('#billing');
            Metronic.init();
        }
        else
        {
            msgbox('danger', 'Please contact administrator');
        }
    },

    student_term: function (stud_no) {
        if (stud_no) {
            setAjaxRequest(
                base_url + 'online-payment/dragonpay/event', 
                { 'event': 'enrolled-term', 'studnum': stud_no },
                function (res) {
                    ONLINE_PAYMENT.load_billing(res.data);
                    progressBar('off');
                }
            );
        } else {
            msgbox('warning', 'Cant proceed.');
        }
    },

    addlistener: function () {
        $('body').on('keyup', '#email', function () {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            console.log(regex.test($(this).val()), "  value email => ", $(this).val());
            var valid_email_format = regex.test($(this).val());
            if (!valid_email_format) {
                $(this).css("border", "2px solid red");
                $('#email-error').css("display", "");
                $('#email-valid').css("display", "none");
            }
            else {
                $(this).css("border", "2px solid #28a745");
                $('#email-error').css("display", "none");
                $('#email-valid').css("display", "");
            }
        })
        $('body').on('click', '#submit-payment', function () {
            $.SmartMessageBox({
                title: "<h2 class='text-warning'></i> Payment Confirmation </h2>",
                content: "Click YES to proceed...",
                buttons: '[No][Yes]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Yes") {
                    var data = { 'event': 'pay-request', 'txid': TX_ID, 'txnid': $('#txnid').val(), 'amount': TOTAL_CASH_I, 'description': $('#description').val(), 'email': $('#email').val(), 'u_data': USER_TRANSACTION_DATA, 'charges_info': CHARGE_SETUP }
                    progressBar('on');
                    setAjaxRequest(
                        base_url + 'online-payment/dragonpay/event', data,
                        function (req) {
                            progressBar('off');
                            window.open(req.html);
                            window.location.href = base_url + 'online-payment';
                        }
                    );
                }
            });
        });

        $('body').on('change', '.payment_list', function () {
            var total_checked = 0;
            var check = $('.payment_list:checked');
            var fullpayment = $(this).prop('className').split(' ')[1] == "full-payment";
            var dividedpayment = $(this).prop('className').split(' ')[1] == "divided-payment";

            if (fullpayment && check && check.length > 0) {
                total_checked = parseFloat($(this).val());
                TRXN_DESC = "Payment for " + SELECTED_STUDENT.name + "\nPay selected FullPayment";
                $('#submit-online-payment').prop('disabled', false);
                $(this).prop('checked') ? $(this).parents().eq(2).css({ "background-color": "#911", "color": "#fff" }) : $(this).parents().eq(2).css({ "background-color": "#EFEFEF", "color": "#000" });
                $('.divided-payment').each(function () {
                    $(this).prop("checked", false);
                    $(this).parents().removeClass('checked');
                    $(this).parents().eq(2).css({ "background-color": "#EFEFEF", "color": "#000" })
                })
            }
            else if (dividedpayment && check && check.length > 0) {
                $('.full-payment').prop("checked", false);
                $('.full-payment').parents().eq(2).css({ "background-color": "#EFEFEF", "color": "#000" })
                $(this).prop('checked') ? $(this).parents().eq(2).css({ "background-color": "#911", "color": "#fff" }) : $(this).parents().eq(2).css({ "background-color": "#EFEFEF", "color": "#000" });
                TRXN_DESC = "Payment for " + SELECTED_STUDENT.name + "\nPay selected";
                $('.divided-payment:checked').each(function () {
                    total_checked += parseFloat($(this).val());
                    $('#submit-online-payment').prop('disabled', false);
                    TRXN_DESC += " " + $(this).prop('id').replace(/[ ]/g, "");
                });
            }
            else if (((fullpayment && check) || (dividedpayment && check)) && check.length <= 0) {
                total_checked = 0;
                $('.full-payment').parents().removeClass('checked');
                $('.full-payment').parents().eq(1).removeClass('disabled');
                $('.full-payment').prop("disabled", false);
                $('.full-payment').prop("checked", false);
                $('.full-payment').parents().eq(2).css({ "background-color": "#EFEFEF", "color": "#000" })
                $('.divided-payment').each(function () {
                    $(this).prop("disabled", false);
                    $(this).prop("checked", false);
                    $(this).parents().removeClass('checked');
                    $(this).parents().eq(1).removeClass('disabled');
                    $(this).parents().eq(2).css({ "background-color": "#EFEFEF", "color": "#000" })
                });
            }
            var added_charge = CHARGE_SETUP.code == 'F' ? (parseInt(CHARGE_SETUP.ptc_amount) + parseInt(CHARGE_SETUP.school_amount)) : (total_checked * (CHARGE_SETUP.amount / 100));
            var total_with_charge = total_checked + (total_checked > 0 ? parseInt(added_charge) : 0);
            $('#convenience-fee').text(parseFloat(added_charge, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString())
            $('#total_payment').text(parseFloat(total_with_charge, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString())
            $('#totalpayment').val(total_with_charge.toFixed(2));
            TOTAL_CASH_I = total_with_charge.toFixed(2);
            TOTAL_CASH = parseFloat(total_with_charge, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
        });

        $('body').on('click', '#submit-online-payment', function () {
            $('#transaction').html(TRANSACTION_TEMPLATE);
            $('#description').html(TRXN_DESC);
            $('#amount').val(TOTAL_CASH_I);
            $('#total-payment-receipt').text(TOTAL_CASH);
            $('#submit-payment').removeAttr("disabled");
            $(this).parents().eq(2).removeClass('active');

            ONLINE_PAYMENT.stepper(2)

            ONLINE_PAYMENT.checkAllSteps();
            $('#txnid').val(ONLINE_PAYMENT.generateTransactionID());
            Metronic.unblockUI('#transaction');
            Metronic.init();
        });

        $('body').on('click', '.btn-back', function() {
            var btn = $('.tab-active').prev('.step-content-tab');
            var step = $('.step-active').prev('.step');
            console.log(step);

            if(!btn.length || !step.length) {
                btn = $('.step-content-tab').last();
                step = $('.step').last();
            }

            $('.tab-active').removeClass('tab-active');
            btn.addClass('tab-active');

            $('.step-active').removeClass('step-active');
            step.addClass('step-active');
            step.removeClass('step-done');
        })

        $('body').on('click', '#step-1-btn', function () {
            ONLINE_PAYMENT.student_term(SELECTED_STUDENT.idnumber);
        });

        $('body').on('click', '#step-1-btn-student', function () {
            ONLINE_PAYMENT.load_billing(SELECTED_STUDENT.ayterm);
        });

        $('body').on('change','#acad-year-term', function () {
            SELECTED_STUDENT.name = $('#current-user').val();
            SELECTED_STUDENT.ayterm = $(this).val();
            $("#step-1-btn-student").prop('disabled', false);
        });

        $('body').on('change', '#students', function (e) {
            SELECTED_STUDENT.idnumber = $(this).val();
            SELECTED_STUDENT.name = $("#students option:selected").text();
            $("#step-1-btn").prop('disabled', false);
        });
    },
    generateTransactionID: function () {
        var td_data = { event: 'get-transaction', code: 'OPR-' };
        var ut_data = { event: 'unused-transaction', code: 'OPR-' };
        var unused_transaction = ajaxRequest(this.mylink + "event", ut_data, 'json', undefined, false);
        var tx_code = "";
        
        if (unused_transaction.unused) {
            TX_ID = unused_transaction.tx_id;
            tx_code = "OPR-P" + ONLINE_PAYMENT.pad(unused_transaction.tx_id, 5);
        }
        else {
            var transaction_details = ajaxRequest(this.mylink + "event", td_data, 'json', undefined, false);
            TX_ID = transaction_details.tx_id;
            tx_code = "OPR-P" + ONLINE_PAYMENT.pad(transaction_details.tx_id, 5);
        }
        
        return tx_code;
    },

    bindData: function () {
        $('#student-name').val(SELECTED_STUDENT.name);
        $('#student-number').val(USER_TRANSACTION_DATA.student_no);
        $('#school-term').val(USER_TRANSACTION_DATA.school_term);
        $('#school-year').val(USER_TRANSACTION_DATA.academic_year);
    },

    stepper: function (step) {
        if(step == 1)
        {
            $('.step').removeClass('step-active');
            $('.step:nth-child(1)').addClass('step-done');
            $('.step:nth-child(2)').addClass("step-active");
        
            $('.step-content-tab').removeClass('tab-active');
            $('.step-content-tab:nth-child(2)').addClass('tab-active');
        
            $('#payment-plan').toggleClass("red green");

            $('#payment-plan').attr('disabled', false);
        }
        else
        {
            $('.step').removeClass('step-active');
            $('.step:nth-child(2)').addClass('step-done')
            $('.step:nth-child(3)').addClass('step-active');

            $('.step-content-tab').removeClass('tab-active');
            $('.step-content-tab:nth-child(3)').addClass('tab-active');
        }
    },
    pad: function (str, max) {
        str = str.toString();
        return str.length < max ? ONLINE_PAYMENT.pad("0" + str, max) : str;
    },

    checkAllSteps: function () {
        $('.step-title').click(function () {
            $('.step').removeClass('active');
            $(this).parent().addClass('active');
        })
    },
}