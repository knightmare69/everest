var payment_recordtbl = $('#onlinepayment_rectable');
var generate_payment = $('#create-online-payment');

$('#onlinepayment_rectable thead tr:eq(1) th').each( function (i) {
    var title = $(this).text();
    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

    $( 'input', this ).on( 'keyup change', function () {
        if ( table.column(i).search() !== this.value ) {
            table
                .column(i)
                .search( this.value )
                .draw();
        }
    } );
});

var dttbl = payment_recordtbl.DataTable();

generate_payment.on("click", function () {
    window.open(base_url+page_url+"dragonpay", "_self");
});