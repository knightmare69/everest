var ONLINE_DEBIT = 0;
var StatusCodes = [
    {
        'codes' : 'S',
        'description' : 'Success, Payment already received',
        'proceed' : true
    },
    {
        'codes' : 'F',
        'description' : 'Failure, Payment failure. Please check your transaction or contact the administrator',
        'proceed' : false
    },
    {
        'codes' : 'P',
        'description' : 'Pending, Payment still pending',
        'proceed' : false
    },
    {
        'codes' : 'U',
        'description' : 'Unknown, Transaction code is unknown. Please make transaction on student/parent portal',
        'proceed' : false
    },
    {
        'codes' : 'R',
        'description' : 'Refund, Please return cash paid by the client',
        'proceed' : false
    },
    {
        'codes' : 'K',
        'description' : 'Chargeback',
        'proceed' : false
    },
    {
        'codes' : 'V',
        'description' : 'Void, Transaction has void. Please contact dragonpay for inquiries',
        'proceed' : false
    },
    {
        'codes' : 'A',
        'description' : 'Authorized',
        'proceed' : false
    },
]
var Clicks_Counter = 0;
var Portals = [];
var PreventDoubleLoad = true;
var OnlineCashier = {
    menu: function(){
        var onlinepayment_table = $('#onlinepayment_rectable').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            lengthChange: false,
            dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            ajax: {
                url: base_url + page_url + 'event',
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (d) {
                    d.event = 'sort-transaction-list'
                    d.dateFrom = $('#date-from').val()
                    d.dateTo = $('#date-to').val()
                    d.payment_status = $('#payment-status').val()
                    d.payment_portal = $('#portal-lists').find('option:selected').attr('data-info')
                    d.transaction_status = $('#transaction-status').val()
                }
            }
        });

        var onlinepayment_reporttable = $('#onlinepayment_reporttable').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            lengthChange: false,
            dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            ajax: {
                url: base_url + page_url + 'event',
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (d) {
                    d.event = 'report-collected'
                    d.dateFrom = $('#report-date-from').val()
                    d.dateTo = $('#report-date-to').val()
                    d.payment_portal = $('#portal-lists').find('option:selected').attr('data-info')
                }
            }
        });

        $('body').on('click','#transaction-btn',function() {
            var val = $(this).attr('data-btn');
            var portal = $(this).attr('data-portal');
            var student_id = $(this).attr('data-id');
            switch(val)
            {
                case 'pay-and-verify':
                    var tx_number = $(this).closest('tr').find('td').eq(4).text()
                    setAjaxRequest(
                        base_url + 'online-payment/event',
                        {
                            event: 'cashier-verify-transaction',
                            portal: portal,
                        },
                        function (res) {
                            var payment_url = res.data.is_production > 0 ? res.data.merchant_production_url : res.data.merchant_test_url;
                            var password = res.data.is_production > 0 ? res.data.merchant_production_password : res.data.merchant_test_password;
                            if(res.count == 1)
                            {
                                $.ajax({
                                    url: base_url + 'online-payment/event?event=verify-transaction',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {
                                        url: payment_url + 'MerchantRequest.aspx?op=GETSTATUS&merchantid=' + res.data.merchant_name + '&merchantpwd=' + password + '&txnid=' + tx_number,
                                        id_number : student_id,
                                        transaction_number: tx_number
                                    },
                                    success: function (data) {
                                        msgbox('success', 'Verified Successfully!');
                                        jQuery('#online-validation').modal('show');
                                        progressBar('off');
                                        var status_codes = StatusCodes.find(code => code.codes == data.status);
                                        status_codes.codes == 'S' ? $('#save-payment').css("display","initial") : '';
                                        if(!data.error)
                                        {
                                            ONLINE_DEBIT = parseInt(data.records[0].amount);
                                            $('#notice-message').text(status_codes.description);
                                            $('#student-name').text(data.records[0].student_name);
                                            $('#school-id').text(data.records[0].user_id);
                                            $('#school-term').text(data.records[0].student_term);
                                            $('#amount-payed').text((ONLINE_DEBIT).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                                            $('#amount-payor').text(data.records[0].user_fullname);
                                            onlinepayment_table.draw();
                                        }
                                    },
                                    error: (error) => { console.log(error); PreventDoubleLoad = true; }
                                });
                            }
                        },
                        function (err, resp_text) {
                            PreventDoubleLoad = true;
                            progressBar('off');
                        },
                            null,
                            true
                    );
                break;

                case 'encash-online-debit':
                    if($('#txtname').attr('data-id')){
                        jQuery('#online-validation').modal('show');
                    }
                    else{
                        msgbox('error', 'Please select payer!');
                    }
                break;
            }
        });

        $('#save-payment').click(function() {
            $('#online-validation').modal('hide');
            Cashiering.auto_distribution();
        });

        $('body').on('click', '#collected-btn-list', function() {
            var charge_id = JSON.parse($(this).attr('data-collections'));
            var bill = $(this).attr('data-bill');
            setAjaxRequest(
                base_url + page_url + 'print',
                {
                    'bill': 'view-report',
                    'charge_id' : charge_id,
                    'collection_for' : bill
                },
                function(res) {
                    progressBar('off');
                    // var win = window.open('','_blank')
                    // win.document.body.innerHTML = res.html
                }
            );
        });

        $('#tx_number').select2({
            allowClear: true,
            placeholder: 'Section',
            ajax: {
                url: base_url + 'online-payment/event?event=transaction-id',
                dataType: 'json',
                type: 'post',
                data: function (params) {
                    var query = {
                        search: params,
                        page: params.page || 1,
                        portal : $('#portal-lists').find('option:selected').attr('data-info'),
                        id_number : $('#txtname').attr('data-id') == '' || $('#txtname').attr('data-id') == undefined ? 0 : $('#txtname').attr('data-id')
                    }
                    return query;
                },
                results: function (data) {
                    return data;
                },
            }
        });

        $('#tx_number').change(function() {
            setAjaxRequest(
                base_url + 'online-payment/event', 
                {
                    'event': 'transaction-information',
                    'transaction_number' : $('#tx_number').val(),
                    'id_number' : $('#txtname').attr('data-id'),
                    'decode' : true
                },
                function (res) {
                    if(res.results)
                    {
                        var status_codes = StatusCodes.find(code => code.codes == res.results[0].request_status);
                        status_codes.codes == 'S' ? $('#save-payment').css("display","initial") : '';
                        ONLINE_DEBIT = parseInt(res.results[0].tx_amount);
                        $('#status-message').text("Note : " + status_codes.description);
                        $('#online-debit-amount').text((ONLINE_DEBIT).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString())
                        $('#online-debit-payor').text(res.results[0].user_fullname);
                        $('#online-payed-to').text(res.results[0].student_name);
                        $('#online-payed-id').text(res.results[0].user_id);
                        $('#online-school-term').text(res.results[0].student_term);
                        progressBar('off');
                    }
                }
            );
        });

        $('#date-to').change(function() {
            onlinepayment_table.draw();
        });

        $('#report-date-to').change(function() {
            onlinepayment_reporttable.draw()
        });

        $('#payment-status').change(function() {
            onlinepayment_table.draw();
        });

        $('#transaction-status').change(function() {
            onlinepayment_table.draw();
        });

        $('#portal-lists').change(function() {
            $("select option:selected").each(function() {
                var info = $(this).attr('data-info');
                info == '' ? OnlineCashier.clearInput() : '';
                onlinepayment_table.draw();
            });
        });

        $('#search-tx-code').keyup(OnlineCashier.delay(function(){
            onlinepayment_table.search(this.value).draw();
        },500));

        OnlineCashier.transaction_paramaters();
        OnlineCashier.report_portals();
        OnlineCashier.bill_portals();
    },
    delay : function (callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    },
    transaction_paramaters: function() {
        if(Portals.length < 1){
            progressBar('on')
            setAjaxRequest(
                base_url + 'online-payment/event', { 'event': 'portal-list'},
                function (res) {
                    if(res.data)
                    {
                        var response_data = res.data
        
                        for(var i=0; i < response_data.length; i++)
                        {
                            $('#portal-lists').append("<option data-info='"+response_data[i].name+"'value='"+ response_data[i].id +"'>"+response_data[i].name+"</option>")
                            Portals.push(response_data[i]);
                        }
                        msgbox('success', 'Select Active Payment Portals');
                        progressBar('off');
                    }
                }
            );
        }
    },
    report_portals: function() {
        progressBar('on')
        setAjaxRequest(
            base_url + 'online-payment/event', { 'event': 'portal-list'},
            function (res) {
                if(res.data)
                {
                    var response_data = res.data
        
                    for(var i=0; i < response_data.length; i++)
                    {
                        $('#report-portal-lists').append("<option data-info='"+response_data[i].name+"'value='"+ response_data[i].id +"'>"+response_data[i].name+"</option>")
                        Portals.push(response_data[i]);
                    }
                    msgbox('success', 'Select Report Payment Portals');
                    progressBar('off');
                }
            }
        );
    },
    bill_portals: function() {
        progressBar('on')
        setAjaxRequest(
            base_url + 'online-payment/event', { 'event': 'portal-list'},
            function (res) {
                if(res.data)
                {
                    var response_data = res.data
        
                    for(var i=0; i < response_data.length; i++)
                    {
                        $('#bill-portal-lists').append("<option data-info='"+response_data[i].name+"'value='"+ response_data[i].id +"'>"+response_data[i].name+"</option>")
                        Portals.push(response_data[i]);
                    }
                    progressBar('off');
                }
            }
        );

        $('body').on('click', '#preview-bill', function() {
            var printable = $('#bill-persona').find('option:selected').val();
            var datefrom = $('#bill-date-from').val()
            var dateto = $('#bill-date-to').val()
            var portal = $('#bill-portal-lists').find('option:selected').attr('data-info')
            console.log("Portal => " + portal + " Date From => " + datefrom + " Date To => " + dateto + " Printable => " + printable)
            if(portal && datefrom && dateto && printable)
            {
                OnlineCashier.errorForm(portal,datefrom,dateto,printable);
                bootbox.dialog({
                    title: 'Report Name',
                    message: '<div class="form-group"><label for="email">Enter report name/Or leave blank for today`s date-time report name</label><input type="text" class="form-control" id="report_name" name="report_name"></input></div>',
                    backdrop: true,
                    inputType: 'text',
                    buttons: {
                        cancel: {
                        label: 'Cancel',
                        className: 'btn-danger',
                    },
                        confirm: {
                        label: 'Confirm',
                        className: 'btn-primary',
                        callback: function()
                            {
                                bootbox.dialog({
                                    message: '<h5 class="text-center">Once confirm the payment will be set as collected. Confirmation is required, click/tap <b>Continue</b> to confirm</h5>',
                                    backdrop: true,
                                    closeButton: false,
                                    buttons: {
                                        cancel: {
                                        label: 'Cancel',
                                        className: 'btn-danger',
                                        },
                                        confirm: {
                                        label: 'Continue',
                                        className: 'btn-primary',
                                        callback: function()
                                            {
                                                var reportname = $('#report_name').val();
                                                switch(printable){
                                                    case 'ptc-bill':
                                                        setAjaxRequest(
                                                            base_url + page_url + 'print',
                                                            {
                                                                'bill': printable,
                                                                'portal' : portal,
                                                                'dateFrom' : datefrom,
                                                                'dateTo' : dateto,
                                                                'report-name': reportname
                                                            },
                                                            function(res) {
                                                                progressBar('off');
                                                                if(!res.error)
                                                                {
                                                                    $('#print-btn-bill').css("display","block")
                                                                    // var win = window.open('','_blank')
                                                                    // win.document.body.innerHTML = res.html
                                                                    $('#print-preview').html(res.html);
                                                                }
                                                                else
                                                                {
                                                                    msgbox('error', res.message);
                                                                }
                                                            }
                                                        );
                                                    break;

                                                    case 'school-bill':
                                                        setAjaxRequest(
                                                            base_url + page_url + 'print',
                                                            {
                                                                'bill': printable,
                                                                'portal' : portal,
                                                                'dateFrom' : datefrom,
                                                                'dateTo' : dateto,
                                                                'report-name': reportname
                                                            },
                                                            function(res) {
                                                                progressBar('off');
                                                                if(!res.error)
                                                                {
                                                                    $('#print-btn-bill').css("display","block")
                                                                    // var win = window.open('','_blank')
                                                                    // win.document.body.innerHTML = res.html
                                                                    $('#print-preview').html(res.html);
                                                                }
                                                                else
                                                                {
                                                                    msgbox('error', res.message);
                                                                }
                                                            }
                                                        );
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                })
            }
            else
            {
                OnlineCashier.errorForm(portal,datefrom,dateto,printable);
            }
        });

        $('#print-btn-bill').click(function () {
            var win = window.open('','_blank')
            win.document.body.innerHTML = $('#print-preview').html()
        })
    },
    clearInput: function() {
        $('#payment-status option').prop('selected', '');
        $('#date-to').val('')
        $('#date-from').val('')
    },
    clearModal: function() {
        $('#status-message').text('')
        $('#online-debit-amount').text('')
        $('#online-debit-payor').text('')
        $('#online-payed-to').text('')
        $('#online-payed-id').text('')
        $('#online-school-term').text('')
        $('#portal-lists option[value="-1"]').prop('selected',true);
        $('#tx_number').select2('data', null);
    },
    errorForm: function(portal,datefrom,dateto,printable){
        if (portal == '')
        {
            $('#bill-portal-lists-error').removeClass('hidden');
            $('#bill-portal-lists').parent().addClass('has-error');
        }
        else
        {
            $('#bill-portal-lists-error').addClass('hidden');
            $('#bill-portal-lists').parent().removeClass('has-error');
        }

        if (datefrom == '')
        {
            $('#bill-date-from-error').removeClass('hidden');
            $('#bill-date-from').parent().addClass('has-error');
        }
        else
        {
            $('#bill-date-from-error').addClass('hidden');
            $('#bill-date-from').parent().removeClass('has-error');
        }

        if (dateto == '')
        {
            $('#bill-date-to-error').removeClass('hidden');
            $('#bill-date-to').parent().addClass('has-error');
        }
        else
        {
            $('#bill-date-to-error').addClass('hidden');
            $('#bill-date-to').parent().removeClass('has-error');
        }
        if (printable == '')
        {
            $('#bill-persona-error').removeClass('hidden');
            $('#bill-persona').parent().addClass('has-error');
        }
        else
        {
            $('#bill-persona-error').addClass('hidden');
            $('#bill-persona').parent().removeClass('has-error');
        }
    },
    init: function(){
        this.menu();
    }
}