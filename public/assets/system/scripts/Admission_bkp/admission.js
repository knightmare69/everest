var global_step = 1, examSchedule = '', documents;
var ADMISSION = function() {

	var addNewSibling = function() {
		var defaultRow = $('#tableDefaultSibling tbody').html();
		$('#tableSiblings tbody').append(defaultRow);
		FN.datePicker('.siblingDOB');
		setDateFormat('.siblingDOB');
	}

	var removeSibling = function(el) {
		el.closest('tr').remove();
	}

	var setThumbnail = function() {
		// console.log($('#formStudent #StudentPhoto')[0].files[0]);
		// $('#formStudent #StudentPhotoThumbnail').attr('src',$('#formStudent #StudentPhoto').val());
	}

	var getSelectedVal = function(objName) {
		return $(objName).find('option:selected').val();
	}

	var gradeLevel = function(el,isProcess) {
		var isProcess = isProcess == undefined ? true : isProcess;
		removeInputRequired('#formStudent','.HS');
		removeInputRequired('#formStudent','.ES');
		// ADMISSION_DATA.showProgram(el.find('option:selected').val());
	};

	var setForeignFields = function(showError) {
		
		if ($('#Nationality option:selected').attr('data-isforeign') == 1) {
			// $('.foreignFields').removeClass('not-required');
			$('#isForeign').val(1);
		} else {
			// $('.foreignFields').addClass('not-required');
			$('#isForeign').val(0);
		}
		isFormValid('#formStudent');
	};

	var editStep = function(el) {
		var step = parseInt(el.attr('data-step'));

		$('#adminssionSteps li').removeClass('active');
		$('#adminssionSteps > li.step-'+step).addClass('active');

		$('.mainTabContent > .tab-pane').removeClass('active');
		$('.mainTabContent > .step-'+step).addClass('active');

		setSessionCurrStep(step);
	};

	var onLoad = function() {	
		gradeLevel($('#gradeLevel'),false);
		
		setForeignFields(false);
		enableSchoolAttended(false);
		generateUlSteps();

		//set ajax current session step
		setSessionStep();
		//toggle steps button
		toggleStepButton();

		setThumbnail();
		review();

		documents(true);

		setDateFormat();
		
		ADMISSION_DATA.countryIcons();
		ADMISSION_DATA.countryIcons('#BillingCountry');
		
		ADMISSION_DATA.getCity($('#GuardianCountry option:selected').val());
		ADMISSION_DATA.getCity($('#BillingCountry option:selected').val(),'#BillingCity');
		ADMISSION_DATA.toggleOtherCity($('#isOtherCity').is(':checked'));
		ADMISSION_DATA.toggleBillingOtherCity($('#BillingIsOtherCity').is(':checked'));

		ADMISSION_DATA.getYearLevel();
		College.init();

		SetManageAccount();
		checkFormValidation();
	};


	var showFamilyModalSearch = function(el) {
		var result = ajaxRequest(base_url+page_url+'event','event=showModalFamilSearch');
		if (result.error == true) {
			return msgbox('error',result.message);
		}
		showModal({
			name: 'basic',
            title : 'Search Family Background',
            content: result.content,
            class: 'modal_family_background',
            button: { 
                icon: 'fa fa-search',
                class: 'btn_search_family',
                caption: 'Search'
            },
            hasFooter: false,
        });

        FN.datePicker('body #tableFamilySearc #FamilyDOB');
	};

	var enableSchoolAttended = function(showError) {
		if( $('#gradeLevel option:selected').attr('data-isGradeLevel') == 1) {
			setInputRequired('#formStudent','.ES');
			removeInputRequired('#formStudent','.HS');
		} else {
			setInputRequired('#formStudent','.HS');
			setInputRequired('#formStudent','.ES');
		}
		isFormValid('#formStudent',showError);
	};

	var searchFamily = function(el) {
		if (!getInputVal('#tableFamilySearch #FamilyFilter'))  {
			return msgbox('error','Please input Filter.');
		}
		setAjaxRequest(base_url+page_url+'event','event=searchFamily&filter='+
			getInputVal('#tableFamilySearch #FamilyFilter')
			, function(result) {
				if (result.error == true) {
					return msgbox('error',result.message);
				} else {
					$('body .modal_family_background .modal-body').html(result.content);
				}
				progressBar('off');
			}
		);
	};

	documents = function(async) {
		var async = async == undefined ? false : async;
		var bar = async ? false : true;
		
		console.log(
			$('#Nationality option:selected').val()+','+
			$('#gradeLevel option:selected').val()+','+
			$('#gradeProgramClass').val()
			);
		if ($('#Nationality option:selected').val() == ''
			|| $('#gradeLevel option:selected').val() == ''
			|| $('#gradeProgramClass').val() == ''
			) {
			return false;
		}
		setAjaxRequest(base_url+page_url+'event','event=getRequiredDocs&AppID='
			+$('#applicationType option:selected').val()
			+'&IsForeign='+$('#Nationality option:selected').attr('data-isforeign') 
			+'&ProgClass='+$('#gradeProgramClass').val()
			+'&YearLevelID='+$('#gradeLevel option:selected').val(),
			function(result) {
				var table = $('#tableDocuments'), hasData = false;
				table.find('tbody tr').remove();
				for(var i in result) {
					table.find('tbody').append(
						"<tr data-tempID='"+result[i].TemplateID+"' data-id='"+result[i].DetailID+"' data-isRequired='"+(result[i].IsRequired)+"'  data-doc='"+result[i].RequirementID+"'>"
						+"<td>"+result[i].DocDesc+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsReviewed'"+(result[i].IsReviewed == '1' ? 'checked' : '')+">" : (result[i].IsReviewed ? 'Yes' : 'No') )+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsExempted'"+(result[i].IsExempted == '1' ? 'checked' : '')+">" : (result[i].IsExempted ? 'Yes' : 'No') )+"</td>"
						+"<td class='center'>"+(result[i].IsRequired == '1' ? 'Yes' : 'No')+"</td>"
						+'<td class="center">'
							+'<div class="fileUpload btn '+(result[i].EntryID == '1' ? 'btn-success' : (result[i].IsRequired == '1' ? 'btn-danger' : 'bg-grey-cascade'))+'">'
				                +'<span><i class="fa fa-file"></i></span>'
				                +'<input type="file" class="upload" />'
				            +'</div>'
				         +'</td>'
						+"</tr>"
					);
					hasData = true;
				}
				if (hasData) {
					table.attr('data-nationality',$('#Nationality option:selected').val());
					table.attr('data-YearLevel',$('#gradeLevel option:selected').val());
					table.attr('data-AppType',$('#applicationType option:selected').val());
					table.attr('data-ProgClass',$('#gradeProgramClass').val());
				}
				progressBar('off');
			},'','JSON',async,bar
		);
	};

	examSchedule = function() {
		setAjaxRequest(base_url+page_url+'event','event=ShowExamScheduleDates'
			+'&campus='+$('#schoolCampus option:selected').val()
			+'&term='+$('#schoolYear option:selected').val()
			+'&gradeLevel='+$('#gradeLevel option:selected').val(),
			function(result) {
				if (result.error == false) {
					$('#ScheduleDate').remove('option');
					var options = '<option value="">Select...</option>';
					$('#ScheduleDate').removeClass('not-required');
					if (result.data.length <= 0) {
						options = '<option value="">None</option>';	
						$('#ScheduleDate').addClass('not-required');
					}
					for(var i in result.data) {
						options += '<option value="'+result.data[i].SchedID+'">'+result.data[i].SchedDate+'</option>';
					}
					$('#ScheduleDate').html(options);
					if($('#ScheduleDate option:selected').val() == '') {
						$('#ScheduleDate').val($('#ScheduleDateID').val());
					}
				}
			},
			function(error) {
					$('#ScheduleDate').addClass('not-required');
					$('#ORCode').addClass('not-required');
					$('#ORSecurityCode').addClass('not-required');
			},
			'JSON',
			true,
			false
		);
	};

	var selFamily = function(el) {
		$('#FamilyID').val(el.closest('tr').attr('data-id'));
		setAjaxRequest(base_url+page_url+'event','event=getFamilyBG&key='+el.closest('tr').attr('data-id')
			, function(result) {
				if (result.error == true) {
					return msgbox('error',result.message);
				} else {
					var data = result.data
					setInputValByName('#formGuardian','name',data.GuardianName);
					// setInputValByName('#formGuardian','marital',data.Guardian_Name);
					setInputValByName('#formGuardian','livingWith',data.Guardian_LivingWith);
					setSelectedValByName('#formGuardian','marital',data.ParentMaritalID);
					setInputValByName('#formGuardian','livingWithOther',data.RelationshipOthers);
					// setInputValByName('#resident','name',data.Guardian_Name);
					setInputValByName('#formGuardian','street',data.Street);
					setInputValByName('#formGuardian','resident',data.Address);
					setInputValByName('#formGuardian','barangay',data.Barangay);
					setInputValByName('#formGuardian','GuardianCitySel',data.CityID);
					setInputValByName('#formGuardian','telephone',data.TelNo);
					setInputValByName('#formGuardian','mobile',data.Mobile);
					setInputValByName('#formGuardian','email',data.Email);
					setSelect2ValByName('#formGuardian','GuardianCountry',data.GuardianCountryCode);
					setSelect2ValByName('#formGuardian','city',data.TownCity);

					setInputValByName('#formGuardian','BillingStreet',data.BillingStreet);
					setInputValByName('#formGuardian','BillingResident',data.BillingAddress);
					setInputValByName('#formGuardian','BillingBrangay',data.BillingBarangay);
					setInputValByName('#formGuardian','BillingCitySel',data.BillingCityID);
					setSelect2ValByName('#formGuardian','BillingCountry',data.GuardianBillingCode);
					setSelect2ValByName('#formGuardian','BillingCity',data.Guardian_Billing_TownCity);

					setInputValByName('#formFather','FatherName',data.FName);
					setInputValByName('#formFather','FatherDateOfBirth',setDateFormatTo(data.FDOB,'yyyy-mm-dd','mm/dd/yyyy'));
					setInputValByName('#formFather','FatherCompany',data.FCompany);
					setInputValByName('#formFather','FatherOccupation',data.FOccupation);
					setInputValByName('#formFather','FatherBusinessAddress',data.FCompanyAddress);
					setInputValByName('#formFather','FatherBusinessPhone',data.FCompanyPhone);
					setInputValByName('#formFather','FatherEmailAddress',data.FEmail);
					setInputValByName('#formFather','FatherMobile',data.FMobile);
					setInputValByName('#formFather','FatherEducation',data.FEducAttainment);
					setInputValByName('#formFather','FatherSchool',data.FSchoolAttended);

					setInputValByName('#formMother','MotherName',data.MName);
					setInputValByName('#formMother','MotherDateOfBirth',setDateFormatTo(data.MDOB,'yyyy-mm-dd','mm/dd/yyyy'));
					setInputValByName('#formMother','MotherCompany',data.MCompany);
					setInputValByName('#formMother','MotherOccupation',data.MOccupation);
					setInputValByName('#formMother','MotherBusinessAddress',data.MCompanyAddress);
					setInputValByName('#formMother','MotherBusinessPhone',data.MCompanyPhone);
					setInputValByName('#formMother','MotherEmailAddress',data.MEmail);
					setInputValByName('#formMother','MotherMobile',data.FMobile);
					setInputValByName('#formMother','MotherEducation',data.MEducAttainment);
					setInputValByName('#formMother','MotherSchool',data.MSchoolAttended);

					setInputValByName('#formLoginAccount','username',data.LoginAccount.UserName);
					setInputValByName('#formLoginAccount','LoginID',data.LoginAccount.UserID);
					setInputValByName('#formLoginAccount','IsLoginValidated',data.HasAlreadyAccount ? 1 : 0);
					setInputValByName('#formLoginAccount','IsAccountEdit',data.HasAlreadyAccount ? 1 : 0);
					
					$('#user-account #IsManageAccount').prop('checked',data.HasAlreadyAccount ? false : true);
					$('#user-account #IsEmailSend').prop('checked',data.HasAlreadyAccount ? false : true);

					SetManageAccount();
					
					isFormValid('#family #formGuardian');
					isFormValid('#family #formFather');
					isFormValid('#family #formMother');
					

					getProperty('#GuardianPhotoThumbnail').attr('src',base_url+'general/getGuardianPhoto?FamilyID='+el.closest('tr').attr('data-id'));
				}
				progressBar('off');
			}
		);
		$('.btnFamilySel').removeClass('btn-success');
		$('.btnFamilySel').addClass('default');
		el.removeClass('default');
		el.addClass('btn-success');
	};


	var listenEvent = function() {
		var myEvent = window.attachEvent || window.addEventListener;
	    var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compatable

	    myEvent(chkevent, function(e) { // For >=IE7, Chrome, Firefox
	        var confirmationMessage = ' ';  // a space
	        (e || window.event).returnValue = confirmationMessage;
	        return confirmationMessage;
	    });
	};

	var copyAdddress = function() {
		var form = '#formGuardian';

		var resident = $(form).find('input[name="resident"]').val();
		var street = $(form).find('input[name="street"]').val();
		var barangay = $(form).find('input[name="barangay"]').val();
		var GuardianCountry = $(form +' #GuardianCountry option:selected').val();
		var isOtherCity = $(form).find('input[name="isOtherCity"]').is(':checked') ? 1 : 0;
		var otherCity = $(form).find('input[name="otherCity"]').val();
		var GuardianCity = $(form).find('#GuardianCity option:selected').val();

		$(form).find('input[name="BillingResident"]').val(resident);
		$(form).find('input[name="BillingStreet"]').val(street);
		$(form).find('input[name="BillingBrangay"]').val(barangay);
		$(form).find('input[name="BillingGuardianCountry"]').val(GuardianCountry);
		$(form).find('input[name="BillingOtherCity"]').val(otherCity);

		$('#BillingIsOtherCity').prop('checked',isOtherCity);
		ADMISSION_DATA.toggleBillingOtherCity(isOtherCity);

		$('#BillingCountry').select2('val',[GuardianCountry]);
		$('#BillingCountry').trigger('change');

		$('#BillingCity').select2('val',[GuardianCity]);
		$('#BillingCity').trigger('change');

		$(':checkbox').uniform();
	};

	var AddSchoolAttended = function() {
		var row = $('#tableSchoolsAttended > tbody > tr:first-child');
		$('#tableSchoolsAttended').append('<tr>'+row.html()+'</tr>');
		$('#tableSchoolsAttended > tbody > tr:last-child')
			.find('input').val('');
		$('#tableSchoolsAttended > tbody > tr:last-child')
			.find('select').val('');
	}

	var removeSchoolAttended = function(el) {
		confirmEvent("Are you sure you want to remote this?",
			function(yes) {
				if (!yes) return;
				if (el.closest('tbody').find('tr').length  == 1) {
					el.closest('tr')	
						.find('input').val('');
					el.closest('tr')	
						.find('select').val('');
				} else {
					el.closest('tr')
						.remove();	
				}
			}
		);	
	}

	return {
		init: function() {
			listenEvent();	
			onLoad();

			$('body').on('click','#btnNext',function() {
				review();
				setProgressStep(getCurrentStep());
			});

			$('body').on('click','#btnPrev',function() {
				review();
				setPreviousStep();
			});

			$('body').on('click','#btnAddSibling',function() {
				addNewSibling($(this));
			});

			$('body').on('click','.btnSiblingRemove',function() {
				removeSibling($(this));
			});

			$('body').on('change','#gradeLevel',function() {
				
                var data  = $(this).find('option:selected');
                
                gradeLevel($(this));
				ADMISSION_DATA.showProgram(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
				documents(true);
				enableSchoolAttended();
				examSchedule();
				
				setGradeLevelSel(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);                        	
                                
                $('.strands').hide();
                if( data.attr('data-progclass') == '21'){ $('.strands').show();}                			
                $('#gradeProgramID').val(data.attr('data-prog'));
            
			});
            
            $('body').on('change','#gradeMajorID',function() {
                var data  = $('#gradeLevel').find('option:selected');
                $('#gradeProgramID').val(data.attr('data-prog'));
                $('#SetProgClass').val(data.attr('data-progclass'))
            }); 

			$('body').on('click','.a-adminssionSteps',function() {
				if (is_parent == '1') return false;
				review();
				if (getStepIndex('complete') == $(this).closest('li').attr('data-step')) {
					return false;
				} else {
					var step = $(this).closest('li').attr('data-step');
					if ((step) == getStepIndex('review')) {
						progressBar('on');
						review();
						progressBar('off');
					}
					setSessionCurrStep(step,true);
					showCompleteButton($(this));
				}
			});	

			$('body').on('click','.btn-edit',function() {
				progressBar('on');
				editStep($(this));
				showCompleteButton($(this));
				progressBar('off');
			});

			$('body').on('click','#btnSubmitTemp',function() {
				if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
				confirmEvent("Are you sure you want to save this?",function(yes) {
					if (!yes) return
					var result = ADMISSION_DATA.admitTemp($(this),1);
					    is_parent = (($('#complete').is('.step-6'))?1:is_parent);
					if (result.error != undefined && result.error == false) {
						ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
						ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
						setWizzardSteps(is_parent ? 6 : 7);
						$('.step-'+(is_parent ? 6 : 7)).addClass('active');
						$('.complete-form').html(result.message);
						hideButtons();
						setSessionCurrStep(1);
						clearSteps();
						hideError();
						hideSuccess();
						$('.complete-form').html(result.message);
					} else {
						var msg = result.message != !undefined ? result.message : 'There was an error while saving';
						msgbox('error',msg);
					}
				});
			});

			$('body').on('click','#btnSubmit',function() {
				if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
				confirmEvent("Are you sure you want to submit this?",function(yes) {
					if (!yes) return
					var result    = ADMISSION_DATA.admitTemp($(this),2);
					    is_parent = (($('#complete').is('.step-6'))?1:is_parent);
					if (result.error != undefined && result.error == false) {
						ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
						ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
						hideButtons();
						hideError();
						setWizzardSteps(is_parent ? 6 : 7);
						$('.step-'+(is_parent ? 6 : 7)).addClass('active');
						$('.complete-form').html(result.message);
					} else {
						var msg = result.message != !undefined ? result.message : 'There was an error while saving';
						msgbox('error',msg);
					}
				});
			});

			$('body').on('click','#btnNewApp',function() {
				setWizzardSteps(1);
				setSessionCurrStep(1);
				location.reload();
			});

			$('body').on('change','#Nationality',function() {
				setForeignFields();
				documents(true);
			});

			$('body').on('bind keyup change keypress','#DateOfBirth',function() {
				$('#StudentAge').text(getAge($(this).val()));
			});

			$('body').on('bind keyup change keypress','.siblingDOB',function() {
				$(this).closest('tr').find('.siblingAge').val(getAge($(this).val()));
			});	

			$('body').on('bind keyup','input:text',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
			});

			$('body').on('change','form select',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
			});

			$('body').on('change','#tableDocuments tbody tr td .upload',function() {
				$(this).closest('.fileUpload').removeClass('bg-grey-cascade');
				$(this).closest('.fileUpload').removeClass('btn-danger');
				$(this).closest('.fileUpload').addClass('btn-success');
			});

			$('body').on('change','#ProgramMajor',function() {
				if ($(this).find('option:selected').val()) {
					$('#gradeProgramMajorID').val($(this).find('option:selected').val());
					documents(true);
				}
			});
			
			$('body').on('change','#ScheduleDate',function() {
				$('#ScheduleDateID').val($(this).find('option:selected').val());
			});		

			$('body').on('change','#schoolYear',function() {
				documents(true);
			});	

			$('#MiddleName').bind('input',function() {
				$('#MiddleInitial').val(getMiddleInitial($(this).val()));
			});	

			$('#FamilySearch').click(function() {
				showFamilyModalSearch($(this));
			});

			$('#FamilyClearBG').click(function() {
				confirmEvent("Are you sure you want to clear this?", function(yes) {
					if (!yes) return;
					clearFamilyBG($(this));
				});
			});

			$('body').on('click','.btn_search_family',function() {
				searchFamily($(this));
			});	

			$('body').on('click','.btnFamilySel',function() {
				selFamily($(this));
			});	
			
			$('body').on('change','#GuardianCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val());
			});	

			$('body').on('click','#isOtherCity',function() {
				ADMISSION_DATA.toggleOtherCity($(this).is(':checked'));
			});	

			$('body').on('change','#BillingCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val(),'#BillingCity');
			});	

			$('body').on('click','#BillingIsOtherCity',function() {
				ADMISSION_DATA.toggleBillingOtherCity($(this).is(':checked'));
			});

			$('.EducLevel').on('ifChecked',function(event) {
				ADMISSION_DATA.getYearLevel($(this).val());
				College.init();
			});
	
			$('body').on('change','#BillingCity',function() {
				$('#BillingCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('.CourseSel').change(function(event) {
				if ($(this).val()) {
					College.CourseSel($(this));
				}
			});

			$('body').on('click','#CopyAddress', function() {
				if ($(this).is(':checked')) {
					copyAdddress();
				}
			});

			$('.CitySelOption').change(function(event) {
				if ($(this).val()) {
					$(this).parent().find('.CitySel').val($(this).find('option:selected').attr('data-id'));
				}
			});

			$('body').on('click','#IsManageAccount', function() {
				SetManageAccount();
			});    

			$('#btnAddSchoolAttended').click( function() {
				AddSchoolAttended();
			});
            
			$('[name="Religion"]').change(function(){
				var xid = $(this).val();
				
			});
			
			$('body').on('change','[name="Nationality"],[name="Religion"]',function() {
               var xid   = $(this).val();
               var xname = $(this).attr('name');
			   $('[name="add'+xname+'"]').val('');
			   $('[name="add'+xname+'"]').attr('type','hidden');
			   $('[name="add'+xname+'"]').addClass('not-required');
               if(xid=='x'){
				   $('[name="add'+xname+'"]').attr('type','text');
				   $('[name="add'+xname+'"]').removeClass('not-required');
			   }			   
			});
			
			$(document).on('click','.btnSchoolsAttendedRemove', function() {
				removeSchoolAttended($(this));
			});
		}
	}
}();