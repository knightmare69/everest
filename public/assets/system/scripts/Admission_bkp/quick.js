var examSchedule;
var Quick = function() {

	var toggleMiddleName = function() {
		if ($('#IsElegitimateChild').is(':checked')) {
			$('#MiddleName').prop('disabled',true);
		} else {
			$('#MiddleName').prop('disabled',false);
		}
	};

	var clearForm = function() {
		clear_value('#FormQuick');
		$('#IsElegitimateChild').prop('disabled',false);
		$('#IsElegitimateChild').prop('checked',false);
		$(':checkbox').uniform();
		toggleMiddleName();
	};

	var save = function(isPrint) {
		setAjaxRequest(
			base_url+page_url+'event',
			'event=saveQuick&isPrint='+isPrint+'&'+$('#FormQuick').serialize(),
			function(result) {
				if (result.error == false) {
					msgbox('success','Successfully Save');

					if (isPrint) {
						print_reports(base_url+page_url+'print?AppNo='+result.AppNo);
					}

					clearForm();
				} else {
					var msg = result.message != !undefined ? result.message : 'There was an error while saving';
				    msgbox('error',msg);
				}
				progressBar('off');
			}
		);
	}

	examSchedule = function() {
		setAjaxRequest(base_url+page_url+'event','event=ShowExamScheduleDates'
			+'&campus='+$('#schoolCampus option:selected').val()
			+'&term='+$('#schoolYear option:selected').val()
			+'&gradeLevel='+$('#gradeLevel option:selected').val(),
			function(result) {
				if (result.error == false) {
					$('#ScheduleDate').remove('option');
					var options = '<option value="">Select...</option>';
					$('#ScheduleDate').removeClass('not-required');
					if (result.data.length <= 0) {
						options = '<option value="">None</option>';	
						$('#ScheduleDate').addClass('not-required');
					}
					for(var i in result.data) {
						options += '<option value="'+result.data[i].SchedID+'">'+result.data[i].SchedDate+'</option>';
					}
					$('#ScheduleDate').html(options);
					if($('#ScheduleDate option:selected').val() == '') {
						$('#ScheduleDate').val($('#ScheduleDateID').val());
					}
				}
			},
			function(error) {

			},
			'JSON',
			true,
			false
		);
	};

	var showFamilyModalSearch = function(el) {
		var result = ajaxRequest(base_url+page_url+'event','event=showModalFamilSearch');
		if (result.error == true) {
			return msgbox('error',result.message);
		}
		showModal({
			name: 'basic',
            title : 'Search Family Background',
            content: result.content,
            class: 'modal_family_background',
            button: {
                icon: 'fa fa-search',
                class: 'btn_search_family',
                caption: 'Search'
            },
            hasFooter: false,
        });

        FN.datePicker('body #tableFamilySearc #FamilyDOB');
	};

	var searchFamily = function(el) {
		if (!getInputVal('#tableFamilySearch #FamilyFilter'))  {
			return msgbox('error','Please input Filter.');
		}
		setAjaxRequest(base_url+page_url+'event','event=searchFamily&filter='+
			getInputVal('#tableFamilySearch #FamilyFilter')
			, function(result) {
				if (result.error == true) {
					return msgbox('error',result.message);
				} else {
					$('body .modal_family_background .modal-body').html(result.content);
				}
				progressBar('off');
			}
		);
	};

	var selFamily = function(el) {
		$('#family').val(el.closest('tr').find('td').eq(0).text());
		$('#familyid').val(el.closest('tr').attr('data-id'));
		closeModal('basic');
	}

    initStrands = function(){

    };

	return {
		init: function() {

			toggleMiddleName();

			setDateFormat();
			ADMISSION_DATA.getYearLevel();


            $('.majorWrapper').hide();

			$('body').on('click','#ClearForm',function() {
				clearForm();
			});

			$('body').on('click','#IsElegitimateChild',function() {
				toggleMiddleName();
			});

			$('body').on('bind keyup change keypress','#DateOfBirth',function() {
				$('#StudentAge').text(getAge($(this).val()));
			});

			$('.EducLevel').on('ifChecked',function(event) {
			  if ($(this).val() == 'senior'){
			     $('.majorWrapper').show();
			  }else{
			     $('.majorWrapper').hide();
			  }
			});

			$('body').on('change','#gradeLevel',function() {

                var p = $(this).find('option:selected');
                $('#gradeProgramID').val(p.attr('data-prog'));

                if( p.attr('data-progclass') == '21'){
                    $('.majorWrapper').show();
                    initStrands();
                }else{
                    $('#ProgramMajor').val(0);
                    $('.majorWrapper').hide();
                }

				examSchedule();

				setGradeLevelSel(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
			});

			$('body').on('click','#BtnSave',function() {

				if (!isFormValid('#FormQuick')) {
					return msgbox('error','Please complete all required fields');
				}

				confirmEvent("Are you sure you want to save this?",function(yes) {
					if (!yes) return
					save();
				});
			});

			$('body').on('click','#BtnSavePrint',function() {

				if (!isFormValid('#FormQuick')) {
					return msgbox('error','Please complete all required fields');
				}

				confirmEvent("Are you sure you want to save this?",function(yes) {
					if (!yes) return
					save(1);
				});
			});

			$('#FamilySearch').click(function() {
				showFamilyModalSearch($(this));
			});

			$('body').on('click','.btn_search_family',function() {
				searchFamily($(this));
			});

			$('body').on('click','.btnFamilySel',function() {
				selFamily($(this));
			});

		}
	}
}();