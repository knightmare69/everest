function getCurrentStep() {
	var step = parseInt($('#adminssionSteps li.active').attr('data-step'));
	return (!step || isNaN(step)) ? 1 : step;
}

function setSessionCurrStep(step,async) {
	var async = async == undefined ? false : async;
	var bar = async ? false : true;
	if (!step) return;
	setAjaxRequest(base_url+page_url+'event','event=setCurrentStep&step='+step,
		function(result) {
			progressBar('off');
		},'','json',true,false
	);	
}

function setMainSteps(step) {
	$('#adminssionSteps > li').each(function() {
		$(this).find('a').attr('href','javascript:void(0)');
	});
}

function addHrefIfFormComplete() {
	var forms = stepForm();
	for(var i in forms) {
		$('#adminssionSteps > li.step-'+i+' a').attr('href','#'+stepHref(i));
		if (!ADMISSION_HELPER.isFormValid(i,false)) {
			return;
		}
	}
}

function generateUlSteps() {
	var step = 1;
	$('#adminssionSteps > li').each(function() {
		$(this).addClass('step-'+step);
		$(this).attr('data-step',step);
		step++;
	});
	var step = 1;
	$('#admissionMainTab  .mainTabContent > .tab-pane').each(function() {
		$(this).addClass('step-'+(step));
		$(this).attr('step',step);
		step += 1;
	});
}

function getCurrentStepByClass(_class) {
	var step = _class;
	step = step.split(' ');
	if (step[0] == 'active') {
		return parseInt(step[1].split('-')[1]);
	}
	else if (step[0] != 'active') {
		return parseInt(step[0].split('-')[1]);
	}
	else if (step[1] == 'active') {
		return parseInt(step[0].split('-')[1]);
	}
	
	return 1;
}

function stepHref(step) {
	var data = {
		1: 'year',
		2: 'student',
		3: 'family',
		4: 'siblings',
		5: 'documents',
		6: 'user-account',
		7: 'signatures',
		8: 'review',
		9: 'complete',
	}
	if (is_parent) {
		var data = {
			1: 'year',
			2: 'student',
			3: 'family',
			4: 'siblings',
			5: 'documents',
			// 6: 'user-account',
			6: 'signatures',
			7: 'review',
			8: 'complete',
		}
	}

	return step != undefined ? data[step] : data;
}

function stepForm(step) {
	var data = {
		1: '#formYear',
		2: '#formStudent',
		3: '#formGuardian,#formFather,#formMother',
		4: '',
		5: 'documents',
		6: '#formLoginAccount',
		7: '#formSignatures',
		8: '',
		9: ''
	};

	if (is_parent) {
		var data = {
			1: '#formYear',
			2: '#formStudent',
			3: '#formGuardian,#formFather,#formMother',
			4: '',
			5: 'documents',
			// 6: '#formLoginAccount',
			6: '#formSignatures',
			7: '',
			8: ''
		};
	}

	return step != undefined ? data[step] : data;
}

function getStepIndex(form) {
	var steps =  stepHref();
	for(var i in steps) {
		if (form.toLowerCase() == steps[i].toLowerCase()) {
			return i;
		}
	}
	return 1;
}

function getStepName(key) {
	var steps =  stepHref();
	for(var i in steps) {
		if (key == i) {
			return steps[i].toLowerCase();
		}
	}
	return '';
}

function generateUlSteps() {
	var step = 1;
	$('#adminssionSteps > li').each(function() {
		$(this).addClass('step-'+step);
		$(this).attr('data-step',step);
		step++;
	});
	var step = 1;
	$('#admissionMainTab  .mainTabContent > .tab-pane').each(function() {
		$(this).addClass('step-'+(step));
		$(this).attr('step',step);
		step += 1;
	});
}

function setProgressButtons(step) {
	var nextLabel = 'Proceed to next step';
	if (step <= 1) {
		$('body #btnPrev').addClass('hide');
		$('body #btnNext').removeClass('hide');
		$('body .final-button').addClass('hide');
		$('body #btnNewApp').addClass('hide');
	}
	else if (step > 1) {
		$('body #btnPrev').removeClass('hide');
		$('body #btnNext').removeClass('hide');
		$('body .final-button').addClass('hide');	
		$('body #btnNewApp').addClass('hide');		
	}
	
	if ($('#adminssionSteps li.step-'+step+' a').hasClass('review')) {
		$('body #btnPrev').removeClass('hide');
		$('body #btnNext').addClass('hide');
		$('body .final-button').removeClass('hide');
		$('body #btnNewApp').addClass('hide');
	}
	if ($('#adminssionSteps li.step-'+step+' a').hasClass('last-step')) {
		$('body #btnPrev').addClass('hide');
		$('body #btnNext').addClass('hide');
		$('body .final-button').addClass('hide');
		$('body #btnNewApp').removeClass('hide');
	}
}

function showCompleteButton(el) {
	var nextLabel = 'Proceed to next step';
	if (el.hasClass('review')) {
		$('body #btnPrev').removeClass('hide');
		$('body #btnNext').addClass('hide');
		$('body .final-button').removeClass('hide');
		$('body #btnNewApp').addClass('hide');
	} 
	else if (el.hasClass('last-step')) {
		$('body #btnPrev').addClass('hide');
		$('body #btnNext').addClass('hide');
		$('body .final-button').addClass('hide');
		$('body #btnNewApp').removeClass('hide');
	} 
	else if (el.closest('li').is(':first-child')) {
		$('body #btnPrev').addClass('hide');
		$('body #btnNext').removeClass('hide');
		$('body .final-button').addClass('hide');
		$('body #btnNewApp').addClass('hide');
	}
	else {
		$('body #btnPrev').removeClass('hide');
		$('body #btnNext').removeClass('hide');
		$('body .final-button').addClass('hide');
		$('body #btnNewApp').addClass('hide');
	}
}

function hideButtons() {
	$('body #btnPrev').addClass('hide');
	$('body #btnNext').addClass('hide');
	$('body .final-button').addClass('hide');
	$('body #btnNewApp').removeClass('hide');
}

function showDefaultButtons() {
	$('body #btnPrev').addClass('hide');
	$('body #btnNext').removeClass('hide');
	$('body .final-button').addClass('hide');
	$('body #btnNewApp').addClass('hide');
}

function clearSteps() {
	clear_value('form');
	$('select').val('');
	$('.select2').each(function() {
		$('select[name="'+$(this).attr('name')+'"]').select2().val('val',['']).trigger('change');
	});
	$('#tableSiblings tbody tr').remove();
	$('#chckConfirm').prop('checked',false);
	$('#StudentPhoto').val('');
}

function isStepsCompleted() {
	for(var i in stepForm()) {
		panelMsgErrorRemove(i);
		if (!ADMISSION_HELPER.isFormValid(i,false)) {
			panelMsgError(i);
			return false;
		}
	}

	function panelMsgError(i) {			
		var title = $('#collapse_'+getStepName(i)).closest('.panel').find('.panel-title > a');
		title.html(title.html() + ' <small class="font-red error-title">Missing Required Information.</small>');
	}

	function panelMsgErrorRemove(i) {
		$('#collapse_'+getStepName(i)).closest('.panel').find('.panel-title > a > small.error-title ').remove();
	}

	return true;
}

function setWizzardSteps(step) {
	$('#adminssionSteps li').removeClass('active');
	$('#adminssionSteps li.step-'+step).addClass('active');
	$('#adminssionSteps li').removeClass('step-'+step);
	$('#adminssionSteps li.active').addClass('step-'+step);

	$('#admissionMainTab .mainTabContent > .tab-pane').removeClass('active');
	$('#admissionMainTab .mainTabContent > .step-'+step).addClass('active');
}

function isLastStep() {
	if ($('#adminssionSteps li.step-'+getCurrentStep()+' a').hasClass('last-step')) {
		return true;
	}
	return false;
}