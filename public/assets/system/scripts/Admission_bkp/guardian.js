var GUARDIAN = function () {
	var handleSave = function () {

		var rules =  {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true
            },
            city: {
                required: false
            },
            resident: {
                required: true
            },
            street: {
                required: true
            },
            livingWith: {
                required: true
            },
            mobile: {
                required: true
            },
            // GuardianCity: {
            // 	required: true
            // },
            // BillingCity: {
            // 	required: true
            // },
            marital: {
            	required: true
            }
            
        };
	             
	           
		$('#livingWith').change(function () {
			if ($(this).val() == 4) {
				$('#formGuardian').validate().element($('#livingWithOther')); //revalidate the chosen dropdown value and show error or success message for the input
			}
        });

         $('#formGuardian').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: rules,

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                var label = $('.register-alert');
	                if (!validateForm()) return;
	                confirmEvent("Are you sure you want to save this?",function(yes) {
	                	if (!yes) return;
	                	setAjaxRequest(base_url+page_url+'event','event=updateGuardian&'+$('#formGuardian').serialize()+'&GuardianCityID='+$('#GuardianCity option:selected').attr('data-id'),function(result) {
		                	if (result.error == true) {
		                		msgbox('error',result.message)
		                	} else {
		                		savePhoto(getInputVal('#FamilyID'));
		                		msgbox('success',result.message)
		                	} 
		                	progressBar('off');
		                }, function(error) {
	                		progressBar('off');
		                });
	                });
	                return false;
	            }
	        });
	}

	var savePhoto = function(FamilyID) {
		if ($('#formGuardian #GuardianPhoto')[0] == undefined) return;
		var form_data = new FormData();
		form_data.append('guardianPhoto', $('#formGuardian #GuardianPhoto')[0].files[0]);
		$.ajax({
            url: base_url +page_url+'event?event=savePhoto&FamilyID='+FamilyID,
            dataType: "html",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            async: false,
            success: (
             	function(result) {
             		
              	}
             ),
       });
	};

	var toggleOtherCity = function(isCheck) {
		if (isCheck) {
			$('#otherCity').removeClass('hide');
			$('#otherCity').removeClass('not-required');
			$('#GuardianCity').addClass('hide');
			$('#GuardianCity').addClass('not-required');
		} else {
			$('#otherCity').addClass('hide');
			$('#otherCity').addClass('not-required');
			$('#GuardianCity').removeClass('hide');
			$('#GuardianCity').removeClass('not-required');
		}
	}

	var toggleBillingOtherCity = function(isCheck) {
		if (isCheck) {
			$('#BillingOtherCity').removeClass('hide');
			$('#BillingOtherCity').removeClass('not-required');
			$('#BillingCity').addClass('hide');
			$('#BillingCity').addClass('not-required');

		} else {
			$('#BillingOtherCity').addClass('hide');
			$('#BillingOtherCity').addClass('not-required');
			$('#BillingCity').removeClass('hide');
			$('#BillingCity').removeClass('not-required');
		}
	}

	var countryIcons = function(el) {
		var el = el == undefined ? '#GuardianCountry' : el;
		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='" +base_url + "assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $(el).select2({
            placeholder: "Select a Country",
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });
	}

	var getCity = function(code,CityEl) {
		var CityEl = CityEl != undefined ? CityEl : '#GuardianCity'
		var city = ajaxRequest(base_url+page_url+'event','event=getCity&code='+code);
		var optionSel = '', options = '';
		for(var i in city) {
			option = document.createElement('option');
			option.setAttribute('data-id',city[i].CityID);
			option.appendChild(document.createTextNode(city[i].City));
			options += option.outerHTML;
		}

		$(CityEl).html(options);
		$(CityEl).select2();
	}

	var copyAdddress = function() {
		var form = '#formGuardian';

		var resident = $(form).find('input[name="resident"]').val();
		var street = $(form).find('input[name="street"]').val();
		var barangay = $(form).find('input[name="barangay"]').val();
		var GuardianCountry = $(form +' #GuardianCountry option:selected').val();
		var isOtherCity = $(form).find('input[name="isOtherCity"]').is(':checked') ? 1 : 0;
		var otherCity = $(form).find('input[name="otherCity"]').val();
		var GuardianCity = $(form).find('#GuardianCity option:selected').val();

		$(form).find('input[name="BillingResident"]').val(resident);
		$(form).find('input[name="BillingStreet"]').val(street);
		$(form).find('input[name="BillingBrangay"]').val(barangay);
		$(form).find('input[name="BillingGuardianCountry"]').val(GuardianCountry);
		$(form).find('input[name="BillingOtherCity"]').val(otherCity);

		$('#BillingIsOtherCity').prop('checked',isOtherCity);
		toggleBillingOtherCity(isOtherCity);

		$('#BillingCountry').select2('val',[GuardianCountry]);
		$('#BillingCountry').trigger('change');

		$('#BillingCity').select2('val',[GuardianCity]);
		$('#BillingCity').trigger('change');

		$(':checkbox').uniform();
	}

	var validateForm = function() {
		var isvalid = true;
		$('#otherCity,#BillingOtherCity').removeAttr('style');
		if ($('#isOtherCity').is(':checked')) {
			if (!$('#otherCity').val()) {
				$('#otherCity').attr('style','border: 1px solid red');
				isvalid = false;
			}
		}
		if ($('#BillingIsOtherCity').is(':checked')) {
			if (!$('#BillingOtherCity').val()) {
				$('#BillingOtherCity').attr('style','border: 1px solid red');
				isvalid = false;
			}
		}
		if (!isvalid) {
			msgbox("error","Please complete all required fields");
		}
		return isvalid;
	}

   
    return {
        //main function to initiate the module
        init: function () {

        	countryIcons('#BillingCountry');
        	countryIcons();

        	toggleOtherCity($('#isOtherCity').is(':checked'));
        	toggleBillingOtherCity($('#BillingIsOtherCity').is(':checked'));

        	handleSave();

        	$(window).on('load',function(){
        	$('#privacymodal').modal('show');
    		});

        	$('body').on('click','#isOtherCity',function() {
				toggleOtherCity($(this).is(':checked'));
			});

			$('body').on('change','#GuardianCountry',function() {
				getCity($(this).val());
			});
			
			$('body').on('change','#BillingCountry',function() {
				getCity($(this).find('option:selected').val(),'#BillingCity');
			});	

			$('body').on('change','#BillingCity',function() {
				$('#BillingCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('body').on('change','#GuardianCity',function() {
				$('#GuardianCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('body').on('click','#BillingIsOtherCity',function() {
				toggleBillingOtherCity($(this).is(':checked'));
			});
			
			

			$('body').on('click','#CopyAddress', function() {
				if ($(this).is(':checked')) {
					copyAdddress();
				}
			});
        }

    };

}();

$('document').ready(function(){
   $('.form-actions').addClass('hidden');
   $('body').on('click','#submitForm',function(){
       var xtarget = $('.nav-tabs').find('.active').attr('data-target');
	   if(xtarget!=undefined){
	    $('#submit'+xtarget).trigger('click');
	   }
   });
   $('body').on('click','#submitFather',function(){
		var label = $('.register-alert');
		confirmEvent("Are you sure you want to save this?",function(yes) {
			if (!yes) return;
			setAjaxRequest(base_url+page_url+'event','event=updateFather&'+$('#formFather').serialize(),
			function(result) {
				if (result.error == true) {
					msgbox('error',result.message)
				} else {
					msgbox('success',result.message)
				} 
				progressBar('off');
			}, function(error) {
				progressBar('off');
			});
		});
		return false;
   });
	
   $('body').on('click','#submitMother',function(){
		var label = $('.register-alert');
		confirmEvent("Are you sure you want to save this?",function(yes) {
			if (!yes) return;
			setAjaxRequest(base_url+page_url+'event','event=updateMother&'+$('#formMother').serialize(),
			function(result) {
				if (result.error == true) {
					msgbox('error',result.message)
				} else {
					msgbox('success',result.message)
				} 
				progressBar('off');
			}, function(error) {
				progressBar('off');
			});
		});
		return false;
	});

   $('.datepicker').datepicker();
});