function applicationDataTable() {
    var isReload = isReload ? isReload : false;
    var table = '#tableApplicants';
    var oTable = jQuery(table).dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columnDefs": [{
            "orderable": false,
            "targets": [0]
        }],
        "order": [
            [4, 'asc']
        ],
        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,
    });
    var tableWrapper = jQuery(table+'_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
    var tableColumnToggler = jQuery(table+'_column_toggler');
}

function applicationDataTableDestroy() {
    var table = '#tableApplicants';
    jQuery(table).dataTable().empty();
    jQuery(table).dataTable().fnDestroy();
}