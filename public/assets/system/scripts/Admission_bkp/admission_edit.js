var global_step = 1, global_this = '', documents = '',examSchedule = '', completeStep = 9, firstStep = 1;
var ADMISSION = function() {

	var admissionDirectClick = function(el) {
	};

	var setThumbnail = function() {
		// console.log($('#formStudent #StudentPhoto')[0].files[0]);
		// $('#formStudent #StudentPhotoThumbnail').attr('src',$('#formStudent #StudentPhoto').val());
	}

	var parseAgeSiblings = function() {
		$('#tableDefaultSibling tbody tr').each(function() {
			$(this).find('td .siblingAge').val(getAge($(this).find('td .siblingDOB').val()));
		});
	}

	var setStudentAge = function() {
		$('#StudentAge').text(getAge($('#DateOfBirth').val()));
	}


	var addNewSibling = function() {
		var defaultRow = $('#tableDefaultSibling tbody').html();
		$('#tableSiblings tbody').append(defaultRow);
		FN.datePicker('.siblingDOB');
		setDateFormat('.siblingDOB');
	}

	var gradeLevel = function(el,isProcess) {
		var isProcess = isProcess == undefined ? true : isProcess;

		removeInputRequired('#formStudent','.HS');
		removeInputRequired('#formStudent','.ES');
	};

	var isSuccessRemarksStatus = false;
	var saveAdmissionStatus = function() {
        var $rem = $('#StatusRemarks'); 
		var result = ajaxRequest(base_url+page_url+'event','event=saveRemarksStatus&AppNo='+getParameterByName('AppNo')+'&remarks='+$rem.val()+'&status='+ $rem.attr('data-status') )
		if (result.error == false) {
			msgbox('success',result.message);
			//getProperty('#AdmissionStatusSel').val(getSelectedVal('#AdmissionStatus'));
            location.reload();
			closeModal('.modal_status_remarks');
			isSuccessRemarksStatus = true;
		} else {
			msgbox('success',result.message);
			//getProperty('#AdmissionStatus').val(getInputVal('#AdmissionStatusSel'));
            location.reload();
		}
	};

	var showModalAdmissionStatus = function($e) {
		showModal({
			name: 'basic',
			title: 'Remarks',
			content: ajaxRequest(base_url+page_url+'event','event=modalRemarks&AppNo='+getParameterByName('AppNo')+'&status='+ $e.attr('data-val') ,'HTML'),
			class: 'modal_status_remarks'
		});
	};

	var setForeignFields = function(showError) {
		
		// if ($('#Nationality option:selected').attr('data-isforeign') == 1) {
		// 	$('.foreignFields').removeClass('not-required');
		// 	$('#isForeign').val(1);
		// } else {
		// 	$('.foreignFields').addClass('not-required');
		// 	$('#isForeign').val(0);
		// }
	}

	var editStep = function(el) {
		var step = parseInt(el.attr('data-step'));

		$('#adminssionSteps li').removeClass('active');
		$('#adminssionSteps > li.step-'+step).addClass('active');

		$('.mainTabContent > .tab-pane').removeClass('active');
		$('.mainTabContent > .step-'+step).addClass('active');

		setSessionCurrStep(step);
	};

	var removeSibling = function(el) {
		if (el.closest('tr').attr('data-id') != undefined && el.closest('tr').attr('data-id') != '' ) {
			confirmEvent("Are you sure you want to remove this?", function(yes) {
				if (!yes) return;
				setAjaxRequest(base_url+page_url+'event','event=removeSibling&key='+el.closest('tr').attr('data-id')
					, function(result) {
						if (result.error == false) {
							msgbox('success',result.message);
							el.closest('tr').remove();
						} else {
							msgbox('error',result.message);
						}
						progressBar('off');
					}
				);
			});
		} else {
			el.closest('tr').remove();
		}
	}

	var onLoad = function() {	
		var step;
		gradeLevel($('#gradeLevel'),false);
		// ADMISSION_DATA.showProgram(getSelectedVal('#gradeLevel'));
		setForeignFields(false);

		generateUlSteps();

		//set ajax current session step
		setSessionStep();
		//toggle steps button
		toggleStepButton();

		setProgressButtons();
		setThumbnail();
		review();
		parseAgeSiblings();
		setStudentAge();
		isFormValid('#formStudent');
		setDateFormat();

		ADMISSION_DATA.setStudentPhoto();
		ADMISSION_DATA.countryIcons();
		ADMISSION_DATA.countryIcons('#BillingCountry');

		ADMISSION_DATA.toggleOtherCity($('#isOtherCity').is(':checked'));
		ADMISSION_DATA.toggleBillingOtherCity($('#BillingIsOtherCity').is(':checked'));
		calculateSiblingDOB();

		ADMISSION_DATA.getYearLevel(ADMISSION_DATA.getSelEducLevel());

		ADMISSION_DATA.getCity($('#GuardianCountry').find('option:selected').val());
		ADMISSION_DATA.getCity($('#BillingCountry').find('option:selected').val(),'#BillingCity');

		College.init();	

		SetManageAccount();

		checkFormValidation();
	};


	examSchedule = function() {
		setAjaxRequest(base_url+page_url+'event','event=ShowExamScheduleDates'
			+'&campus='+$('#schoolCampus option:selected').val()
			+'&term='+$('#schoolYear option:selected').val()
			+'&gradeLevel='+$('#gradeLevel option:selected').val(),
			function(result) {

				if (result.error == false) {
					$('#ScheduleDate').remove('option');
					var options = '<option value="">Select...</option>';
					$('#ScheduleDate').removeClass('not-required');
						
					if (result.data.length <= 0) {
						options += '<option selected value="">None</option>';	
						$('#ScheduleDate').addClass('not-required');
					}
					for(var i in result.data) {
						options += '<option  value="'+result.data[i].SchedID+'">'+result.data[i].SchedDate+'</option>';
					}

					if($('#ScheduleDate option:selected').val() == '') {
						if (getInputVal('#ScheduleDateID')) {
							$('#ScheduleDate').val($('#ScheduleDateID').val());
						}
						$('#ScheduleDate').html(options);
					}
				}
			},
			function(error) {

			},
			'JSON',
			true,
			false
		);
	};

	documents = function(async) {
		var async = async == undefined ? false : async;
		var bar = async ? false : true;
		if ($('#Nationality option:selected').val() == ''
			|| $('#gradeLevel option:selected').val() == ''
			|| $('#gradeProgramClass').val() == ''
			) {
			return false;
		}
		setAjaxRequest(base_url+page_url+'event','event=getRequiredDocs&AppID='
			+$('#applicationType option:selected').val()
			+'&IsForeign='+$('#Nationality option:selected').attr('data-isforeign') 
			+'&ProgClass='+$('#gradeProgramClass').val()
			+'&YearLevelID='+$('#gradeLevel option:selected').val()
			+'&AppNo='+getParameterByName('AppNo'),
			function(result) {
				var table = $('#tableDocuments');
				table.find('tbody tr').remove();
				var hasData = false, swipe_index = 0;
				for(var i in result) {
					table.find('tbody').append(
						"<tr data-has-attachment='"+ result[i].HasAttachment+"' data-swipe-index='"+(result[i].HasAttachment == '1' ? swipe_index++ : 0) +"'  data-tempID='"+result[i].TemplateID+"' data-id='"+result[i].DetailID+"' data-isRequired='"+(result[i].IsRequired)+"' data-entryid='"+result[i].EntryID+"' data-doc='"+result[i].RequirementID+"'>"
						+"<td>"+(result[i].HasAttachment == '1' ? '<a href="javascript:void(0)" class=" aViewDocs fancybox-button" data-rel="fancybox-button">'+result[i].DocDesc+'</a>' : result[i].DocDesc)+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsReviewed'"+(result[i].IsReviewed == '1' ? 'checked' : '')+">" : (result[i].IsReviewed ? 'Yes' : 'No') )+"</td>"
						+"<td>"+(is_parent != '1' ? "<input type='checkbox' class='form-control IsExempted'"+(result[i].IsExempted == '1' ? 'checked' : '')+">" : (result[i].IsExempted ? 'Yes' : 'No') )+"</td>"
						+"<td class='center'>"+(result[i].IsRequired == '1' ? 'Yes' : 'No')+"</td>"
						+'<td class="center">'
							+'<div class="fileUpload btn '+(result[i].HasAttachment == '1' ? 'btn-success' : (result[i].IsRequired == '1' ? 'btn-danger' : 'bg-grey-cascade'))+'">'
				                +'<span><i class="fa fa-file"></i></span>'
				                +'<input type="file" class="upload" />'
				            +'</div>'
				         +'</td>'
						+"</tr>"
					);
					hasData = true;
				}
				if (hasData) {
					table.attr('data-nationality',$('#Nationality option:selected').val());
					table.attr('data-YearLevel',$('#gradeLevel option:selected').val());
					table.attr('data-AppType',$('#applicationType option:selected').val());
					table.attr('data-ProgClass',$('#gradeProgramClass').val());
				}
				progressBar('off');
			},'','JSON',async,bar
		);
	};

	var calculateSiblingDOB = function() {
		$('#siblings #tableSiblings tbody tr').each(function() {
			$(this).find('.siblingAge').val(getAge($(this).find('.siblingDOB').val()));
		});
	};

	var viewDocuments = function(el) {
        $('#modal_doc .modal-body').html(ajaxRequest(base_url+page_url+'event','event=viewDocs&FamilyID='+el.closest('tr').attr('data-family')+'&key='+el.closest('tr').attr('data-entryid'),'HTML'));
        $('#modal_doc').modal('show');
	};

	var downloadDoc = function(el) {
		window.open(base_url+'general/downloadAdmissionDoc?key='+el.attr('data-id'));
	};

	var saveDocRemarks = function(el) {
		var remarks = el.parent().find('.TextRemarks').val().trim();
		if (remarks) {
			confirmEvent("Are you sure you want to save this?", function(yes) {
				if (!yes) return;
				var result = ajaxRequest(base_url+page_url+'event','event=saveDocRemarks&key='+el.attr('data-id')+'&remarks='+remarks);
				if (result.error == false) {
					msgbox('success',result.message);
				} else {
					msgbox('error',"There was a problem while saving.");
				}
			});
		}
	};

	var showDocRemarks = function(el) {
		showModal({
			name: 'basic',
			title: 'Document Remarks',
			content: ajaxRequest(base_url+page_url+'event','event=getDocsRemarks&key='+el.attr('data-id'),'HTML'),
			hasFooter: true,
			hasModalButton: false,
		})
	};

	var copyAdddress = function() {
		var form = '#formGuardian';

		var resident = $(form).find('input[name="resident"]').val();
		var street = $(form).find('input[name="street"]').val();
		var barangay = $(form).find('input[name="barangay"]').val();
		var GuardianCountry = $(form +' #GuardianCountry option:selected').val();
		var isOtherCity = $(form).find('input[name="isOtherCity"]').is(':checked') ? 1 : 0;
		var otherCity = $(form).find('input[name="otherCity"]').val();
		var GuardianCity = $(form).find('#GuardianCity option:selected').val();

		$(form).find('input[name="BillingResident"]').val(resident);
		$(form).find('input[name="BillingStreet"]').val(street);
		$(form).find('input[name="BillingBrangay"]').val(barangay);
		$(form).find('input[name="BillingGuardianCountry"]').val(GuardianCountry);
		$(form).find('input[name="BillingOtherCity"]').val(otherCity);

		$('#BillingIsOtherCity').prop('checked',isOtherCity);
		ADMISSION_DATA.toggleBillingOtherCity(isOtherCity);

		$('#BillingCountry').select2('val',[GuardianCountry]);
		$('#BillingCountry').trigger('change');

		$('#BillingCity').select2('val',[GuardianCity]);
		$('#BillingCity').trigger('change');

		$(':checkbox').uniform();
	};

	return {
		init: function() {

			onLoad();

			$('body').on('click','#btnNext',function() {
				setProgressStep(getCurrentStep());
			});

			$('body').on('click','#btnPrev',function() {
				setPreviousStep();
			});

			$('body').on('click','#btnAddSibling',function() {
				addNewSibling($(this));
			});

			$('body').on('click','.btnSiblingRemove',function() {
				removeSibling($(this));
			});

			$('body').on('change','#gradeLevel',function() {
				if ($(this).val() == '') return;
				gradeLevel($(this));
				ADMISSION_DATA.showProgram(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
				enableSchoolAttended();
				examSchedule();
				documents(true);
				setGradeLevelSel(
					$(this).find('option:selected').val(),
					$(this).find('option:selected').attr('data-progclass')
				);
				isFormValid('#formStudent',showError);
			});

			$('body').on('click','.a-adminssionSteps',function() {
				var step = parseInt($(this).closest('li').attr('data-step'));
				if (getStepIndex('complete') == step) {
					return false;
				} else {
					var step = $(this).closest('li').attr('data-step');
					if ((step) 
						== 
						getStepIndex('review')
					) {
						progressBar('on');
						review();
						progressBar('off');
					}
					setSessionCurrStep(step,true);
					showCompleteButton($(this));
				}
			});	

			$('body').on('click','.btn-edit',function() {
				progressBar('on');
				editStep($(this));				
				showCompleteButton($(this));
				progressBar('off');
			});

			$('body').on('click','#btnSubmitTemp,.btnsave',function() {
			     console.log('hey');
				/*
                if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
                */
                
				confirmEvent("Are you sure you want to save this?",function(yes) {
					if (!yes) return
					var result = ADMISSION_DATA.admitTemp($(this),1);
					if (result.error != undefined && result.error == false) {
						ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
						ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
						hideButtons();
						$('#btnNewApp').addClass('hide');
						setWizzardSteps(is_parent ? 8 : 9);
						setSessionCurrStep(1);
						hideError();
						hideSuccess();
						$('.complete-form').html(result.message);
					} else {
						var msg = result.message != !undefined ? result.message : 'There was an error while saving';
						msgbox('error',msg);
					}
				});
			});

			$('body').on('click','#btnSubmit',function() {
				if (!isStepsCompleted()) {
					return showError("Please Complete All required fields.");
				}
				confirmEvent("Are you sure you want to submit this?",function(yes) {
					if (!yes) return
					var result = ADMISSION_DATA.admitTemp($(this),2);
					if (result.error != undefined && result.error == false) {
						ADMISSION_DATA.savePhoto(result.AppNo,result.FamilyID);
						ADMISSION_DATA.saveDocuments(result.AppNo,result.FamilyID);
						hideButtons();
						$('#btnNewApp').addClass('hide');
						setWizzardSteps(is_parent ? 8 : 9);
						setSessionCurrStep(1);
						hideError();
						hideSuccess();
						clearSteps();
						$('.complete-form').html(result.message);
					} else {
						var msg = result.message != !undefined ? result.message : 'There was an error while saving';
						msgbox('error',msg);
					}
				});
			});

			$('body').on('click','#btnNewApp',function() {
				setWizzardSteps(1);
				setSessionCurrStep(1);
				location.reload();
			});

			$('body').on('change','#Nationality',function() {
				setForeignFields();
				documents(true);
				isFormValid('#formStudent');
			});

			$('body').on('bind keyup change keypress','#DateOfBirth',function() {
				$('#StudentAge').text(getAge($(this).val()));
			});

			$('body').on('bind keyup change keypress','.siblingDOB',function() {
				$(this).closest('tr').find('.siblingAge').val(getAge($(this).val()));
			});	

			$('body').on('bind keyup','input:text',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
			});

			$('body').on('change','form select',function() {
				isFormValid('#'+$(this).closest('form').attr('id'));
			});

			$('body').on('change','#documents #tableDocuments tbody tr td .upload',function() {
				$(this).closest('.fileUpload').removeClass('btn-danger');
				$(this).closest('.fileUpload').removeClass('bg-grey-cascade');
				$(this).closest('.fileUpload').addClass('btn-success');
			});

			$('body').on('change','#ProgramMajor',function() {
				if ($(this).find('option:selected').val()) {
					$('#gradeProgramMajorID').val($(this).find('option:selected').val());
					documents(true);
				}
			});
			
			$('body').on('change','#ScheduleDate',function() {
				$('#ScheduleDateID').val($(this).find('option:selected').val());
			});		

			$('body').on('change','#schoolYear',function() {
				documents(true);
			});	

			$('#MiddleName').bind('input',function() {
				$('#MiddleInitial').val(getMiddleInitial($(this).val()));
			});	

			$('body').on('click','.aViewDocs',function() {
				openPhotoSwipe(global_this = $(this),$(this).closest('tr').attr('data-swipe-index'));
			});	

			$('body').on('click','.btnDownloadDoc',function() {
				downloadDoc($(this));
			});

			$('body').on('click','.BtnSaveDocRemarks',function() {
				saveDocRemarks($(this));
			});		

			$('body').on('click','.BtnShowDocRemarks',function() {
				showDocRemarks($(this));
			});	

			$(document).on("contextmenu", ".TextRemarks", function(e){
			   return false;
			});

			$('body').on('click','.pswp__scroll-wrap',function() {
				return false;
			});

			$('body').on('click','.admstatus',function() {
				showModalAdmissionStatus($(this));
				isSuccessRemarksStatus = false;
			});

			$('body').on('click','.modal_status_remarks .btn_modal',function() {
				confirmEvent("Are you sure you want to save this?", function(yes) {
					if (!yes) return;
					saveAdmissionStatus($(this));
				});
			});

			$('body').on('click','.modal_status_remarks .modal-footer .default',function() {
				if (!isSuccessRemarksStatus) {
					getProperty('#AdmissionStatus').val(getInputVal('#AdmissionStatusSel'));
				}
			});

			$('body').on('click','.modal_status_remarks .modal-content .close',function() {
				if (!isSuccessRemarksStatus) {
					getProperty('#AdmissionStatus').val(getInputVal('#AdmissionStatusSel'));
				}
			});

			$('body').on('change','#GuardianCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val());
			});	

			$('body').on('click','#isOtherCity',function() {
				ADMISSION_DATA.toggleOtherCity($(this).is(':checked'));
			});
			
			$('body').on('change','#BillingCountry',function() {
				ADMISSION_DATA.getCity($(this).find('option:selected').val(),'#BillingCity');
			});	

			$('body').on('click','#BillingIsOtherCity',function() {
				ADMISSION_DATA.toggleBillingOtherCity($(this).is(':checked'));
			});

			$('body').on('change','#BillingCity',function() {
				$('#BillingCitySel').val($(this).find('option:selected').attr('data-id'));
			});

			$('.EducLevel').on('ifChecked',function(event) {
				ADMISSION_DATA.getYearLevel($(this).val());
				College.init();
			});

			$('.CourseSel').change(function(event) {
				if ($(this).val()) {
					College.CourseSel($(this));
				}
			});

			$('body').on('click','#CopyAddress', function() {
				if ($(this).is(':checked')) {
					copyAdddress();
				}
			});

			$('.CitySelOption').change(function(event) {
				if ($(this).val()) {
					$(this).parent().find('.CitySel').val($(this).find('option:selected').attr('data-id'));
				}
			});

			$('body').on('click','#IsManageAccount', function() {
				SetManageAccount();
			});
            
           
		}
	}
}();