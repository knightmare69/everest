 function review() {
	$('.accordionReview .panel').each(function() {
		
		var panel = $(this).find('.accordion-toggle').attr('href').split('_')[1];

		$(this).find('#collapse_'+panel +' .wrapper_body').html('<div class="col-md-12" style="padding: 3% !important;">'+$('.mainTabContent #'+panel+' .portlet-body').html()+'</div>');
		
		//family custom
		if (panel == 'family') {

			$('#collapse_family ul.nav-tabs li ').each(function() {
				var tabHref = $(this).find('a').attr('href')+'_collapse';
				$(this).find('a').attr('href',tabHref);
			});

			$('#collapse_family .tab-content .tab-pane').each(function() {
				var tabPane = $(this).attr('id')+'_collapse';
				$(this).attr('id',tabPane);
				$(this).find('form').attr('id',$(this).find('form').attr('id')+'_collapse');
			});

			$('.mainTabContent #'+panel+' .portlet-body form input:text').each(function() {
				$('#collapse_family .tab-content .tab-pane form input[name="'+$(this).attr('name')+'"]').parent().html('<p class="form-control-static">'+$(this).val()+'</p>');
			});

			$('.mainTabContent #'+panel+' .portlet-body form input[type="email"').each(function() {
				$('#collapse_family .tab-content .tab-pane form input[name="'+$(this).attr('name')+'"]').parent().html('<p class="form-control-static">'+$(this).val()+'</p>');
			});

			$('.mainTabContent #'+panel+' .portlet-body form select').each(function() {
				$('#collapse_family .tab-content .tab-pane form select[name="'+$(this).attr('name')+'"]').parent().html('<p class="form-control-static">'+$(this).find('option:selected').text()+'</p>');
			});

		}
		else if (panel == 'siblings') {
			$('#collapse_siblings').find('#tableSiblings').attr('id','tableSiblings_collapse');
			$('#collapse_siblings').find('#tableDefaultSibling').attr('id','tableDefaultSibling_collapse');
			var tableSiblings = $('#siblings #tableSiblings tbody');
			$('#collapse_siblings #tableSiblings_collapse thead tr th #btnAddSibling').remove();
			$('#collapse_siblings #tableSiblings_collapse tbody tr').each(function() {
				var trIndex = $(this).index();
				$(this).find('td').each(function() {
					var tdIndex = $(this).index();
					if (tdIndex > 0) {
						if ($(this).find('.form-control').is('input')) {
							$(this).html(tableSiblings.find('tr').eq(trIndex).find('td').eq(tdIndex).find('input').val());								
						} else {
							$(this).html(tableSiblings.find('tr').eq(trIndex).find('td').eq(tdIndex).find('select option:selected').text());
						}
					} else {
						$(this).html('');
					}
				});
			});
		}
		else if (panel == 'documents') {
			$('#collapse_documents #tableDocuments tbody tr > td > .fileUpload > .upload').remove();
			// $('#collapse_documents #tableDocuments tbody tr > td > .fileUpload').removeClass('btn-danger');
			var i = 0;
			$('#collapse_documents #tableDocuments tbody tr').each(function() {
				var self = $(this);
				var IsReviewed = $('#documents #tableDocuments tbody tr').eq(i).find('td .IsReviewed').is(':checked');
				var IsExempted = $('#documents #tableDocuments tbody tr').eq(i).find('td .IsExempted').is(':checked');
				self.find('td .IsReviewed').prop('checked',IsReviewed);
				self.find('td .IsExempted').prop('checked',IsExempted);
				self.find('td .IsReviewed').prop('disabled',true);
				self.find('td .IsExempted').prop('disabled',true);
				i++;
			});

		}
		else {

			var panelForm = $(this).find('#collapse_'+panel+ ' form');
			var collapseForm = panelForm.attr('id')+'_collapse';
			
			panelForm.attr('id',collapseForm);

			$('.mainTabContent #'+panel+' .portlet-body form input:text').each(function() {
				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('input[name="'+$(this).attr('name')+'"]').closest('div').html('<p class="form-control-static">'+$(this).val()+'</p>');
				}
			});

			$('.mainTabContent #'+panel+' .portlet-body form input[type="email"').each(function() {
				if (!$(this).hasClass('do-not-review')) {
					panelForm.find('input[name="'+$(this).attr('name')+'"]').closest('div').html('<p class="form-control-static">'+$(this).val()+'</p>');
				}
			});

			$('.mainTabContent #'+panel+' .portlet-body form select').each(function() {
				if (!$(this).hasClass('do-not-review')) {
					if (!$(this).hasClass('select2')) {
						panelForm.find('select[name="'+$(this).attr('name')+'"]').closest('div').html('<p class="form-control-static">'+$(this).find('option:selected').text()+'</p>');
					} else {
						panelForm.find('select[name="'+$(this).attr('name')+'"]').closest('div').html('<p class="form-control-static">'+$(this).find('option:selected').text()+'</p>');
					}
				}	
			});

			if (panel == 'student') {
				$('#collapse_student .btn-file').remove();
			}
		}
	});
}