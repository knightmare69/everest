var College =  {

	init: function() {
		if (isHigherLevel()) {
			College.toggleCourseControl();
			College.setCollege();	
		}
	},

	toggleCourseControl: function() {
		if (College.isHigherLevel()) {
			$('.BasicControl').addClass('hide');
			$('.BasicControl').find('input').addClass('not-required');
			$('.BasicControl').find('select').addClass('not-required');

			$('.HigherControl').removeClass('hide');
			$('.HigherControl').find('input').removeClass('not-required');
			$('.HigherControl').find('select').removeClass('not-required');

			$('.majorWrapper').addClass('hide');
			$('#ProgramMajor').addClass('not-required');

			/* School Campus 1 */
			if (!$('#year #schoolCampus1').attr('data-isRequired')) {
				$('#year #schoolCampus1').addClass('not-required');
			}

			/* Course 2 */
			if (!$('#year #Course1').attr('data-isRequired')) {
				$('#year #Course1').addClass('not-required');
			}

			/* School Campus 2 */
			if (!$('#year #schoolCampus2').attr('data-isRequired')) {
				$('#year #schoolCampus2').addClass('not-required');
			}

			/* Course 2 */
			if (!$('#year #Course2').attr('data-isRequired')) {
				$('#year #Course2').addClass('not-required');
			}

			/* School Campus 3 */
			if (!$('#year #schoolCampus3').attr('data-isRequired')) {
				$('#year #schoolCampus3').addClass('not-required');
			}

			/* Course 3 */
			if (!$('#year #Course3').attr('data-isRequired')) {
				$('#year #Course3').addClass('not-required');
			}

			/* School Campus 4 */
			if (!$('#year #schoolCampus4').attr('data-isRequired')) {
				$('#year #schoolCampus4').addClass('not-required');
			}

			/* Course 4 */
			if (!$('#year #Course4').attr('data-isRequired')) {
				$('#year #Course4').addClass('not-required');
			}

		} else {

			$('.BasicControl').removeClass('hide');
			$('.BasicControl').find('input').removeClass('not-required');
			$('.BasicControl').find('select').removeClass('not-required');

			if(!$('.majorWrapper').hasClass('hide')) {
				$('.majorWrapper').removeClass('hide');
				$('#ProgramMajor').removeClass('not-required');
			}

			$('.HigherControl').addClass('hide');
			$('.HigherControl').find('input').addClass('not-required');
			$('.HigherControl').find('select').addClass('not-required');
		}
	},

	setCollege: function() {
		if (College.isHigherLevel()) {
			ADMISSION_DATA.getCourses();
		}
	},
    
    setBasicEd : function(){
  		ADMISSION_DATA.getCourses('basic');        
    },

	getCourseChoices: function() {
		var data = new Array();
		data = {
			campus1: $('#schoolCampus1').val(),
			campus2: $('#schoolCampus2').val(),
			campus3: $('#schoolCampus3').val(),
			campus4: $('#schoolCampus4').val(),
			course1: $('#Course1').val(),
			course1Major: $('#Course1MajorChoice').val(),
			campus2: $('#schoolCampus2').val(),
			course2: $('#Course2').val(),
			course2Major: $('#Course2MajorChoice').val(),
			campus3: $('#schoolCampus3').val(),
			course3: $('#Course3').val(),
			course3Major: $('#Course3MajorChoice').val(),
			campus4: $('#schoolCampus4').val(),
			course4: $('#Course4').val(),
			course4Major: $('#Course4MajorChoice').val(),
		};
		return data;
	},

	CourseSel: function(el) {
		var elem = el.attr('name');
		$('#'+elem+'Choice').val(el.val());
		$('#'+elem+'MajorChoice').val(
			el.find('option:selected').attr('data-mid')
		);
	},

	isHigherLevel: function() {
		//return $('.EducLevel').iCheck('update')[1].checked;
		return false;
	}
};

