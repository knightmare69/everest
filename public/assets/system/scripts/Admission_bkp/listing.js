var Listing = function () {
	var searchRec = function(el) {
		el.find('i').removeClass('fa-search');
		el.find('i').html('<img src="'+(base_url+'assets/system/media/images/loading-spinner-grey.gif')+'" height="20px" width="20px">');
		setAjaxRequest(base_url+page_url+'search',$('#FormTableApplicants').serialize(),
			function(result) {
				progressBar('off');
                applicationDataTableDestroy();
				$('#tableApplicationsWrapper').html(result);
                applicationDataTable();
			},
			'',
			'HTML',true
		);
	};

    var updateStatus = function(statusId) {
        
        if (getIds().length  <= 0) {
            return msgbox('error','There is no selected record.');
        }
        
        confirmEvent( 
            "Are you sure you want to update the status?",
            function(yes) {
                if (!yes) return;
                update(statusId);
            }
        );

        function update(statusId) {
            var result = ajaxRequest(base_url+page_url+'update-status','data='+JSON.stringify(getIds())+'&statusid='+statusId)
            if (result.error == false) {
                msgbox('success',result.message);
				$('#btnFilter').trigger('click');
            } else {
                msgbox('success',result.message);
            }
        }
        
        
        function getIds(){
            var data = new Array();
            $('#tableApplicants tbody > tr').each( function() {
                if ($(this).is('.active-row')) {
                    data.push(
                        $(this).find('td:nth-child(5)').html()
                    );
                }
            });
            return data;
        }  
    };

    var loadYearLevel = function(IsHigherLevel) {
        setAjaxRequest(base_url+page_url+'getYearLevel','IsHigherLevel='+IsHigherLevel,
            function(data) {
                var options = '<option value="0-'+IsHigherLevel+'">All Grade Levels</option>';
                for(var i in data) {
                    var option = document.createElement('option');
                    option.setAttribute('value',data[i].YearLevelID+'-'+IsHigherLevel);
                    option.appendChild(document.createTextNode(data[i].YearLevel));
                    options += option.outerHTML;
                }
                $('#gradeLevel').html(options);
                progressBar('off');
            }
        );
    }

    var showModalAdmissionStatus = function($e) {
        showModal({
            name: 'basic',
            title: 'Remarks',
            content: ajaxRequest(base_url+page_url+'event','event=modalRemarks' ,'HTML'),
            class: 'modal_status_remarks'
        });
    };

    return {
        updateStatus: function(statusId) {
            updateStatus(statusId);
        },
        //main function to initiate the module
        init: function () {
            // loadYearLevel($('#IsHigherLevel').is(':checked') ? 1 : 0);
            ListingData.init();

            $('#btnFilter').trigger('click');

            // searchRec($('.btnSearch'));
        	$('body').on('click','.btnSearch',function() {
        		searchRec($(this));
        	});

        	$('body').keydown(function(e) {
        		if (e.keyCode == 13) {
        			$('.btnSearch').trigger('click');
                    return false;
        		}
        	});

            $('#FormTableApplicants').submit(function() {
                return false;
            });

            $('body').on('click','.AppRowSel', function() {
                showRemarks($(this));
            });

            // $('#IsHigherLevel').click(function() {
            //     loadYearLevel($(this).is(':checked') ? 1 : 0);
            // });
        }

    };

}();

$(document).ready(function(){
	$('body').on('click','#tableFilter #btnFilter',function(){
		$('#tableApplicants').find('#btnFilter').trigger('click');
	});
	$('body').on('change','#tableFilter #schoolCampus',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#schoolCampus').val(tmpval);
	});
	$('body').on('change','#tableFilter #schoolYear',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#schoolYear').val(tmpval);
	});
	$('body').on('change','#tableFilter #gradeLevel',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#gradeLevel').val(tmpval);
	});
	$('body').on('change','#tableFilter #filter',function(){
		var tmpval = $(this).val();
		$('#tableApplicants').find('#filter').val(tmpval);
	});
});