var Examinee = {
    update: function(id){
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to update this record?",
            callback : function(yes) {
               if ( yes )
               {
                    setAjaxRequest(
                        base_url+page_url+'event','event=update&id='+id+'&'+ $(form).serialize(),
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                showSuccess(result.message);
                                var stat_td = $('a[data-id='+id+']').closest('tr');
                                var _clone = $('.result:checked').closest('div').next().clone();
                                var last_child = stat_td.find('td:last-child');
                                last_child.html(_clone);
                                last_child.prev().text($('#act-sched-date').val());
                            }
                        }
                    );
                    progressBar('off');
               }
            }
        })
    },
}

$(document).ready(function(){
    var id = 0;
    $('select').select2();

    $('#records-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+page_url+'event?event=search',
            "type": 'post',
            "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        },
        limit: 10,
    });

    $('body').on('click', '.a-select', function(e){
        id = $(this).attr('data-id');
        showModal({
            name: 'basic2',
            title : 'Examinee',
            content: ajaxRequest(base_url+page_url+'event','event=edit&id='+id,'HTML'),
            class: 'modal_examinee',
            button: {
                icon: 'fa fa-check',
                class: 'btn_save btn green',
                caption: 'Save'
            }
        });
        Metronic.init();
        $('.date-picker').datepicker();
    });

    $('body').on('click', '.btn_save', function(e){
        if(isFormValid(form)){
            Examinee.update(id);
        }
    });

    $('body').on('click', '.result', function(){
        var val = $(this).attr('data-name');

        if(val == 'Reconsidered' || val == 'Failed' || val == 'Conditional'){
            $('#remarks').attr('disabled', false).removeClass('not-required');
        } else {
            $('#remarks').attr('disabled', true).addClass('not-required');
        }
    });

    $('body').on('change', '#search-ac-year, #search-campus', function(e) {
        var ac_year = $('#search-ac-year').val(), campus = $('#search-campus').val();
        var get = ajaxRequest(base_url + page_url + 'event', 'event=table_get&search=' + JSON.stringify({
            'ac_year': ac_year,
            'campus': campus
        }), 'JSON');

        if (get.table != undefined) {
            $('.table_wrapper').html(get.table);
            $('#records-table').dataTable();
            $('select').select2();
        }

        progressBar('off');

    });

})
