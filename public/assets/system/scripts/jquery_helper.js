function getInputVal(objName) {
	return $(objName).val();
}

function getInputValByName(form,name) {
	return $(form+' input[name="'+name+'"]').val();
}

function setInputValByName(form,name,value) {
	return $(form+' input[name="'+name+'"]').val(value);
}

function setSelectedValByName(form,name,value) {
	return $(form+' select[name="'+name+'"]').val(value);
}

function setSelect2ValByName(form,name,value) {
	return $(form+' select[name="'+name+'"]').val(value).trigger('change');
}

function getSelectedVal(objName) {
	return $('body '+objName+' option:selected').val();
}

function getProperty(object) {
	return $('body '+object);
}

function getInputByName(name) {
	return $("form input[name='"+name+"']").val();
}

function callInputByName(name) {
	return $("form input[name='"+name+"']");
}

function setInputRequired(formRead,formAttr) {
	$(formRead+' '+formAttr).each(function() {
        if (!$(this).hasClass('not-required-default')) {
            $(this).removeClass('not-required');
        } else {
        	if (!$(this).hasClass('not-required')) {
	            $(this).addClass('not-required');
	        }
        }
    });
}

function removeInputRequired(formRead,formAttr) {
	$(formRead+' '+formAttr).each(function() {
        if (!$(this).hasClass('not-required-default') && !$(this).hasClass('not-required')) {
            $(this).addClass('not-required');
        }
    });
}