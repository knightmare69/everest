function load_acct_widget(xevent,xtarget){
	setAjaxRequest(
			base_url+'home/widget'
		   ,{event:xevent}
		   ,function(r){
			  if(r.success){
			     if(r.content!=''){
				   $(xtarget).replaceWith(r.content);
				 }
			  }
		      progressBar('off');
			}
		   ,function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Load Data!',function(){return;});	
			  progressBar('off');
			}
	    );	  
}

$('document').ready(function(){
   if($('.dvattrition').length>0){
     setTimeout(function(){load_acct_widget('attrition','.dvattrition');},800);
   }
   
   if($('.dvrpayment').length>0){
     setTimeout(function(){load_acct_widget('reservations_payment','.dvrpayment');},800);
   }
   
   if($('.dvrpayment').length>0){
     setTimeout(function(){load_acct_widget('student_discount','.dvstuddiscount');},800);
   }
   
   if($('.dvdiscrevenue').length>0){
     setTimeout(function(){load_acct_widget('phpdiscount','.dvdiscrevenue');},800);
   }
});