var PrintID = function() {

	var pageSetup = function(el) {
		if (el.is(':checked')) {
			return $('.setup').removeClass('hide');
		}
		return $('.setup').addClass('hide');
	};
    

	var searchFilter = function(el) {
		if (el.is(':checked')) {
			return $('.more_filter').removeClass('hide');
		}
		return $('.more_filter').addClass('hide');
	};

	var beginPrint = function(el) {	
		window.print();	
		if (isBulk()) {
			return printBulk();
		}
		return individual();

		function printBulk() {
			$('#tableEmployee tbody tr').each(function() {
				var self = $(this);
					self.addClass('employee-print-process');
				setTimeout(function() {
					self.removeClass('employee-print-process');
					self.addClass('employee-print-success');
				},1000);
			});
		}

		function individual() {

		}

		function printNow() {

		}

		function isBulk() {
			if ($('#printByBulk').is(':checked')) {
				return true;
			}
			return false;
		}
        
        
        
	};
        
    var activateStudent =  function(e){
        
        if( isFormValid('#form_activation') ){
                                    
            setAjaxRequest( base_url + 'activate/students', $('#form_activation').serialize(),
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);   
                    sysfnc.showAlert('.form-body','danger',result.message,true,true,true,'warning');     
                } else {
                    sysfnc.showAlert('.form-body','success',result.message,true,true,true,'check');
                    msgbox('success', result.message);        
                }
                progressBar('off');
            });
        
        } 
                       
    };
    
	return {
		init: function() {
			$('body').on('click','#moreFilters',function() {
				searchFilter($(this));
			});

			$('body').on('click','#showSetup',function() {
				pageSetup($(this));
			});

			$('body').on('click','#btnPrint',function() {
				beginPrint($(this));
			});
            
            $('body').on('click','#btnactivate',function() {
                console.log('hey');
				activateStudent($(this));
			});
            
            $('body').on('click','#btnrequest',function() {
				var data = $('#form_survey').serialize();
                
                setAjaxRequest( base_url + 'students/survey', data, function(result) {
                if (result.error) {
                    msgbox('error', result.message);   
                    sysfnc.showAlert('.form-body','danger',result.message,true,true,true,'warning');     
                } else {
                    sysfnc.showAlert('.form-body','success',result.message,true,true,true,'check');
                    msgbox('success', result.message);        
                }
                progressBar('off');
            });
                    
			});                                    
            
            $('.datepicker').datepicker();
            
                                    
		}
	}
}();
$(document).ready(function(){
	$('.datepicker').datepicker();
	$('body').on('click','.btn-refresh',function(){
		Calendar.refresh();
	});
	
	$('.item-content').scroll(function(){
	   var dvparent  = $(this).closest('.item');
	   var sposition = $(this).scrollTop();
	   if(sposition>=180){
		 $(dvparent).find('.carousel-caption').addClass('hidden');
	   }else{
	     $(dvparent).find('.carousel-caption').removeClass('hidden');
	   }
	});
	
	$('body').on('click','[data-filename]',function(){
	  if($(this).is('td'))
		window.open($(this).closest('tr').attr('data-filename'), '_blank');
      else		  
		window.open($(this).attr('data-filename'), '_blank');
	});
	
	$('body').on('change','#type',function(){
		var typeid   = $(this).val();
		var wperiod  = $(this).find('option[value="'+typeid+'"]').attr('data-period');
		var wroom    = $(this).find('option[value="'+typeid+'"]').attr('data-room');
		var wreserve = $(this).find('option[value="'+typeid+'"]').attr('data-reserve');
		var wattach  = $(this).find('option[value="'+typeid+'"]').attr('data-attach');
		
		$('.dvtype').removeClass('hidden');
		$('.dvnoperiod').removeClass('hidden');
		$('#note').closest('.form-group').addClass('hidden');	
		$('.dvperiod').addClass('hidden');
		$('.dvattach').addClass('hidden');
		$('.dvreserve').addClass('hidden');
		$('.dvattachform').addClass('hidden');
		$('.dvattendee').addClass('hidden');
		$('.dvreserveform').addClass('hidden');
		$('.btn-reserve').addClass('hidden');
		$('.dvattachform').find('tbody').html('');
		$('.dvattendee').find('tbody').html('');
		$('.dvroom').addClass('hidden');
		
		if(typeid==0 || typeid==1){
		   $('.dvnotify').addClass('hidden');
		   $('.dvnotify').find('input[type="checkbox"]').removeAttr('checked');
		   $('.dvnotify').find('input[type="checkbox"]').closest('span').removeClass('checked');
		   $('.dvoption').addClass('hidden');
		   $('.dvoption').find('input[type="checkbox"]').removeAttr('checked');
		   $('.dvoption').find('input[type="checkbox"]').closest('span').removeClass('checked');
		   if(typeid==0){
		     $('#ispublic').attr('checked',true);
		     $('#ispublic').closest('span').addClass('checked');
			 $('.dvattachform').removeClass('hidden');  
		   }else if(typeid==1){
		     $('#note').closest('.form-group').removeClass('hidden');	
			 $('.dvattendee').removeClass('hidden');  
		   }else{
			 var currdate = $('[data-current-date]').attr('data-current-date');  
			 var currtime = $('[data-current-date]').attr('data-current-time');  
			 $('#startd').val(currdate);
			 $('#startd').attr('value',currdate);
			 $('#startt').val(currtime);
			 $('#startt').attr('value',currtime);
			 $('#endd').val(currdate);
			 $('#endd').attr('value',currdate);
			 $('#endt').val(currtime);  
			 $('#endt').attr('value',currtime);  
		   }
		}else{
		   $('.dvnotify').find('input[type="checkbox"]').prop('checked',true);
		   $('.dvnotify').find('input[type="checkbox"]').closest('span').addClass('checked');
		   $('#ispublic').attr('checked',true);
		   $('#ispublic').closest('span').addClass('checked');
		   if(typeid!=2){
		     $('.dvnotify').removeClass('hidden');
		     $('.dvoption').removeClass('hidden');
		   }	
		}
		
		if(wperiod == '1'){$('.dvperiod').removeClass('hidden'); }
		if(wattach == '1'){$('.dvattach').removeClass('hidden'); }
		if(wreserve == '1'){$('.dvreserve').removeClass('hidden'); }
		if(wroom == '1'){$('.dvroom').removeClass('hidden'); }
	});
	
	$('body').on('change','#withreserve',function(){
	  if($(this).is(':checked')){	
		$('.dvreserveform').removeClass('hidden');
	  }else{
		$('.dvreserveform').addClass('hidden');  
	  }	
	});
	
	$('body').on('change','#withattach',function(){
	  if($(this).is(':checked')){	
		$('.dvattachform').removeClass('hidden');
	  }else{
		$('.dvattachform').addClass('hidden');  
	  }	
	});
	
	$('body').on('change','#room',function(){
	  var selected = $(this).val();
	  var bldgid   = $(this).find('[value="'+selected+'"]').attr('data-bldg');
	  $('#bldg').val(bldgid);	
	});
	
	$('body').on('click','.btn-cancel',function(){
		$('#xid').val('new');
	    $('.btn-delete').addClass('hidden');
		$('#eventform').find('input[type="text"]').val('');
		$('#eventform').find('input[type="checkbox"]').removeAttr('checked');
		$('#eventform').find('select').val('-1');
	})
	
	$('body').on('click','[data-eventid]',function(){
	  var xid = $(this).attr('data-eventid');
	  $('#eventform').find('input[type="text"]').val('');
	  $('#eventform').find('input[type="checkbox"]').removeAttr('checked');
	  $('#eventform').find('select').val('-1');
	  progressBar('on');
      setAjaxRequest(
		base_url+'calendar/txn?event=info'
	   ,{xid:xid}
	   ,function(r){
		  if(r.success){
		    $('#xid').val(xid);
			if($('.btn-save').is('.hidden') || $('.btn-save').length==0)
	          $('.action').html("View");
			else{
			  $('.action').html("Modify");
			  $('.btn-delete').removeClass('hidden');
	     	}
			
			$('#type').val(r.content.TypeID);
            $('#type').trigger('change');		  
	        $('#title').val(r.content.Title);
	        $('#desc').val(r.content.Description);
	        $('#note').val(r.content.Notes);
			$('#bldg').val(r.content.BuildingID);
			$('#startd').val(r.content.StartDate);
			$('#endd').val(r.content.EndDate);
			$('#tblattachment').find('tbody').html(r.attach);
		  //$('.form-control').addClass('readonly');
			$('.form-control').attr('disabled','true');
			$('.btn-attach').addClass('hidden');	
			$('.btn-aremove').replaceWith('<button class="btn btn-xs blue btn-view"><i class="fa fa-download"></i> View</button>');	
			$('.btn-aview').remove();
			
			$('#modal_event').modal('show');
		  }
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Save Data!',function(){return;});	
		  progressBar('off');
		}
	  );
	});
});
