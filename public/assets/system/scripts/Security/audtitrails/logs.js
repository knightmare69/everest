var Logs = function() {

    var dataTableLogs = function() {


        $('#DataTableLogs').dataTable().empty();
        $('#DataTableLogs').dataTable().fnDestroy();
        var grid = new Datatable();
        grid.init({
            src: $("#DataTableLogs"),
            onSuccess: function (grid) {
                progressBar('off');
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            loadingMessage: 'Loading...',

            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": base_url+page_url+'search',
                  // ajax source
                },
                "order": [
                    [1, "desc"]
                ], // set first column as a default sort by asc

            }
        });
         var table = $('#DataTableLogs').DataTable();
        $('body').on('click', '.export', function(){
            console.log('hi');
            table.page.len( -1 ).draw();
        window.open('data:application/vnd.ms-excel,' +
            encodeURIComponent($('#DataTableLogs').parent().html()));
      setTimeout(function(){
        table.page.len(10).draw();
      }, 1000)
        });

         // handle group actionsubmit button click
        $('.filter-submit').on('click', function (e) {

          /*  Modified by Jays: update v1.1
            Added Filter Function - 02-28-19
          */
            e.preventDefault();
            let parameter= $("#parameter").val(),
                log_by = $('#log_by').val(),
                action = $('#action').val(),
                browser = $('browser').val(),
                device = $('device').val(),
                ip_address = $('ip_address').val(),
                log_date_to = $('#log_date_to').val(),
                log_date_from = $('#log_date_from').val()

            ;


            //grid.setAjaxParam("customActionType", "group_action");
                //grid.setAjaxParam("customActionName", 'log_by');

                grid.setAjaxParam("log_by", log_by);
                grid.setAjaxParam("action", action);
                grid.setAjaxParam("ip_address", ip_address);
                grid.setAjaxParam("parameter", parameter);
                grid.setAjaxParam("device", device);
                grid.setAjaxParam("browser", browser);
                grid.setAjaxParam("log_date_from", log_date_from);
                grid.setAjaxParam("log_date_to", log_date_to);
                grid.getDataTable().ajax.reload();


            /*
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.id);
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                Metronic.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
            */
        });
    };


    return {
        init: function() {
            dataTableLogs();
        }
    }
}();
