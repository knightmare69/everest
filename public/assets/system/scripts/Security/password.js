var Password = function() {
	
	return {
		init: function() {
			jQuery('.table tbody > tr > td').on('bind keyup','.value',function() {
				var self = $(this);
				setAjaxRequest(base_url+page_url+'event',
					'event=save&key='+self.closest('tr').attr('data-id')+
					'&value='+self.val(),
					function(result) {

					},'','json',true,false
				);
			});

			jQuery('.table tbody > tr > td .make-switch').on('switchChange.bootstrapSwitch',function(event, state) {
				var self = $(this);
				setAjaxRequest(base_url+page_url+'event',
					'event=save&key='+self.closest('tr').attr('data-id')+
					'&value='+ (self.is(':checked') ? 1 : 0),
					function(result) {

					},'','json',true,false
				);
			});
		}
	}
}();