var USER = function() {
    jQuery('body').on('bind keyup','#username',function() {
        var self = jQuery(this);
        var span_username_checker = $('.span_username_checker');
        if (self.val() == '') {
            span_username_checker.addClass('bg-red-sunglo');
            span_username_checker.removeClass('bg-green-jungle');
            return;
        }
        setAjaxRequest(
            base_url+page_url+'event', 'event=checkUsername&id='+self.val(),
            function(result) {
                if (result.isExist > 0) {
                    span_username_checker.addClass('bg-red-sunglo');
                    span_username_checker.removeClass('bg-green-jungle');
                } else {
                    span_username_checker.addClass('bg-green-jungle');
                    span_username_checker.removeClass('bg-red-sunglo');
                }
            },
            function(error){
                console.log(error.error);
            },
            'JSON',true,false
        );
    });

    // added by lloyd
    jQuery('body').on('click', '#reset-passw', function(){
        var user = $(this).attr('data-id');

        bootbox.confirm({
            size : 'small',
            message : "An email will be sent to this user for resetting password. Proceed?",
            callback : function(yes) {
               if ( yes )
               {
                    setAjaxRequest(
                        base_url+page_url+'event','event=getEmail&user='+ user,
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                setAjaxRequest(
                                    base_url+'/reset/sendEmailReset','email='+ result.email,
                                    function(result) {
                                        if (result.error) {
                                            showError(result.message);
                                        } else {
                                            showSuccess(result.message);
                                        }
                                    }
                                );

                            }
                        }
                    );
                    progressBar('off');
               }
            }
        })
    });


        // added by ARS
    jQuery('body').on('click', '.reset-password', function(){
        var user = $(this).attr('data-id');

        bootbox.confirm({
            size : 'small',
            message : "An email will be sent to this user for resetting password. Proceed?",
            callback : function(yes) {
               if ( yes )
               {
                    $('.chk-child').each(function(){
                        var $r = $(this).closest('tr');
                        if( $(this).prop('checked') ) {
                            var email = $r.find('td.email').html();
                            setAjaxRequest(
                                    base_url+'/reset/sendEmailReset','email='+ email,
                                    function(result) {
                                        if (result.error) {
                                            msgbox('error',result.message);
                                        } else {
                                            msgbox('success',result.message);
                                        }
                                    }
                                );

                        }
                    });
                    progressBar('off');
               }
            }
        })
    });


}();

window.onload = function(){
    UserDT.get();
}