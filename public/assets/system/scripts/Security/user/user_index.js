var USER = function() {
    var id;
    jQuery('body').on('click','.a_addnew',function(){
        showModal({
            title : 'Create Account',
            content: ajaxRequest(base_url+page_url+'event','event=getForm','HTML'),
            class: 'modal_user',
            button: {
                icon: 'fa fa-save',
                class: 'btn_save',
                caption: 'Save'
            }
        });
    });

    jQuery('body').on('click','.btn_save',function(){
        if (isFormValid(form)) {
            bootbox.confirm({
                size : 'small',
                message : "Are you sure you want to save this record?",
                callback : function(yes) {
                   if ( yes )
                   {
                        setAjaxRequest(
                            base_url+page_url+'event','event=save&'+ $(form).serialize(),
                            function(result) {
                                if (result.error) {
                                    showError(result.message);
                                } else {
                                    showSuccess(result.message);
                                    clear_value(form);
                                }
                            }
                        );
                        progressBar('off');
                   }
                }
            })
        }
    });

    jQuery('body').on('click','.btn_update',function(){
        if (isFormValid(form)) {
            bootbox.confirm({
                size : 'small',
                message : "Are you sure you want to save this record?",
                callback : function(yes) {
                   if ( yes )
                   {
                        setAjaxRequest(
                            base_url+page_url+'event','event=update&id='+id+'&'+ $(form).serialize(),
                            function(result) {
                                if (result.error) {
                                    showError(result.message);
                                } else {
                                    showSuccess(result.message);
                                }
                            }
                        );
                        progressBar('off');
                   }
                }
            })
        }
    });

    jQuery('.a_remove').click(function(){
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to remove this record/s?",
            callback : function(yes) {
                if ( yes )
                {
                    setAjaxRequest(
                        base_url+page_url+'event','event=delete&ids='+FN.getTableID(),
                        function(result) {
                           msgbox('success',result.message);
                           window.location.reload();
                        }
                    );
                    progressBar('off');
                }
            }
        })
    });

    jQuery('body').on('click', '.a_select', function(){
        id = jQuery(this).closest('tr').attr('data-id');
        showModal({
            title : 'Update Account',
            content: ajaxRequest(base_url+page_url+'event','event=edit&id='+id,'HTML'),
            class: 'modal_user',
            button: {
                icon: 'fa fa-save',
                class: 'btn_update',
                caption: 'Update'
            }
        });
    });

    return {
        init: function() {

        }
    }
}();

var UserDT = {
    get: function(tble_id){
        tble_id = (tble_id) ? tble_id : '#tbl_users';

        $(tble_id).DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: base_url + 'security/users/event?event=get',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                // beforeSend: function() {
                //     Metronic.blockUI({target: '#tbl_users', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Loading record(s), please wait... '});
                // }
            },
            columnDefs: [
                {targets: [0, 1], orderable: false},
            ],
            order: [[2, 'asc']],
            createdRow:function(row, data, index){
                $(row).attr('data-id', data.DT_RowData.id);
            },
            drawCallback: function(sets){
                // Metronic.unblockUI('#tbl_users');
                Metronic.initUniform();
            }
        });
    }
}