var account = '';
var account_type = '';
var SECURITY = function()
{	
	jQuery('#tree_access').on("changed.jstree", function (e, data) {
	  
	});

	jQuery('#tree_accounts').on('click','.jstree-anchor',function() {
		var self = $(this);
		if (!self.hasClass('jstree-clicked')) {
			if (self.find('id').hasClass('account')) {
				account = self.find('.account').text();
				account_type = 'account';
				ACCESS.getUserAccess(account);
				//$('.BtnProgramAccess').removeClass('hide');
			}
			else if (self.find('id').hasClass('group')) {
				account = self.find('.group').text();
				account_type = 'group';
				ACCESS.getGroupAccess(account);
				//$('.BtnProgramAccess').addClass('hide');
			}
			$('.middle_wrapper  accountselected small i').html('<span class="label bg-red-flamingo">'+self.find('sysaccount').text()+'</span>');
		}
	});

	jQuery('.btn_access_save').click(function() {
		if ((account == '' || account == 0) && (account_type == '' || account_type == undefined)) {
			msgbox("error","Please select account first!");
			return;
		}

	 	bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to save this user access of "+$('.middle_wrapper  accountselected').html()+" ?",
            callback: function(yes) {
                if (yes) {
                	var result = ACCESS.save(ACCESS.getAccess(),account,account_type);
                	if(result.error == false) {
                		msgbox('success',result.message);
                	} else {
                		msgbox('error',result.message);
                	}
                }
            }
        });
	});

	jQuery('#tree_access').on('click','.action_add_new',function() {
		var self = $(this);
		var page = self.closest('ul').parent().find('.jstree-anchor > .page').text();
		var add_new_html = self.closest('li').html();
		var add_new_classes = self.closest('li').attr('class');
		var parent = self.closest('ul');

		bootbox.prompt("Please input page action.", function(action) {
            if (action === null) {
            } else {
                if (action.trim() == '') {
                	self.closest('li').find('.action_add_new').trigger('click');
                } else {
                	var result = ACCESS.saveAction(page,action);
                	if(result.error == false) {
                		self.closest('li').remove();
                		parent.append(ACCESS.addNewAction(action,result.action_id));
                		parent.append('<li class="'+add_new_classes+'">'+add_new_html+'</li>');
                		parent.find('li:last-child').find('.jstree-wholerow').removeClass('jstree-wholerow-hovered');
                		msgbox('success',result.message);     
                	} else {
                		msgbox('error',result.message); 
                	}           	
                }
            }
        });
	});

	jQuery('#tree_access').on('dblclick','.jstree-anchor',function() {
		var self = $(this);
		if (self.closest('li').hasClass('jstree-leaf')) {
			var action_id = self.find('.action').text();
			var action_name = self.find('sysaction').text();
			
			bootbox.prompt({
				size: 'small',
				title: "Update action",
				value: action_name,
				callback: function(action) {
		            if (action === null) {
		            } else {
		                if (action.trim() == '') {
		                	self.closest('li').find('.jstree-anchor').trigger('dblclick');
		                } else {
		                	var result = ACCESS.updateAction(action_id,action);
		                	if (result.error == false) {
		                		self.find('sysaction').text(action);                                
	                			msgbox('success',result.message);
		                	} else {
	                			msgbox('error',result.message);
		                	}
		                }
		            }
		        }
       	 	});
		}
	});

	jQuery('#tree_access').on('click','.jstree-children .jstree-leaf .jstree-anchor',function() {
		var self = $(this), page, data, action;

		page = self.closest('.jstree-children').parent().find('a .page').text().trim();
		action = self.find('id').text().trim();

		data = 'account='+account+'&account_type='+account_type+'&page='+page+'&action='+action;
		
		if (self.hasClass('jstree-disabled')) {
			return;
		}

		if ((account == '' || account == 0) && (account_type == '' || account_type == undefined)) {
			return;
		}

		if (!self.hasClass('jstree-clicked')) {
			ACCESS.autoSaveAction(data);
		} else {
			ACCESS.autoUncheckAction(data);
		}
	});

	jQuery('#tree_access').on('click','.jstree-node .jstree-anchor',function() {
		var self = $(this), page, data, action;

		if ((account == '' || account == 0) && (account_type == '' || account_type == undefined)) {
			return;
		}
		if (!self.closest('li').hasClass('jstree-leaf')) {
			self.closest('li').find('.jstree-children > .jstree-leaf > a').each(function() {
				page = $(this).closest('.jstree-children').parent().find('a .page').text().trim();
				action = $(this).find('id').text().trim();
				data = 'account='+account+'&account_type='+account_type+'&page='+page+'&action='+action;
				if (!$(this).hasClass('jstree-disabled')) {				
					if (!self.hasClass('jstree-clicked')) {
						ACCESS.autoSaveAction(data);
					} else {
						ACCESS.autoUncheckAction(data);
					}
				}
			});
		}
	});

	
	return {
		init: function() {
			TREE.accounts();
			TREE.access(ajaxRequest(base_url+page_url+'accounts','event=loadAccess','JSON',false,false));
		}
	}
}();

