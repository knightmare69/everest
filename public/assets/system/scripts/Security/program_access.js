var account;
var IsGroup =0;
jQuery('.BtnProgramAccess').click(function() {
	account = getAccountIDSel();
	//var key = getUserProgramTemplate();
    
    if(account == undefined){
        bootbox.alert("<i class='fa fa-warning'></i> There was no selected user account(s)...");
        return false;        
    }    
	closeModal('.modal_program_access');
	//refreshProgramTemplates();
    refreshTable(getProgramAccess(account));
    //setTemplateID(key);
    $('.SearchTemplateWrapper').removeClass('hide');
	$('.CreateTemplateWrapper').addClass('hide');
	$('#modal_program_access').modal('show');        
});

jQuery('body').on('change','#ProgramTemplate',function() {
	if (getSelectedVal('#ProgramTemplate')) {
		refreshTable(getProgramAccess(getTemplateID()));
	}
});

jQuery('body').on('click','#TablePrograms tbody tr > td > .chk-child',function() {
	var $r = $(this).closest('tr');
    var d = {
        event : 'setDataAccess',
        prog : $r.attr('data-id'),
        user : account,
        val : $(this).prop('checked'),
        isgroup : IsGroup,
    };
	var result = ajaxRequest(base_url+page_url+'event',d);    
});


jQuery('body').on('click','.CreateTemplate',function() {
	$('.SearchTemplateWrapper').addClass('hide');
	$('.CreateTemplateWrapper').removeClass('hide');
	refreshTable(getProgramAccess());
});

jQuery('body').on('click','.BtnCancelTemplateName',function() {
	$('.SearchTemplateWrapper').removeClass('hide');
	$('.CreateTemplateWrapper').addClass('hide');
});

jQuery('body').on('click','.btnSaveProgramAccess',function() {
	confirmEvent("Are you sure you want to save this user access?", function(yes) {
		if (!yes) return;
		var result = ajaxRequest(base_url+page_url+'event','event=saveUserProgramAccess&TemplateID='+getTemplateID()+'&account='+account);
		if(result.error == false) {
			if (!isTemplate()) {
				refreshProgramTemplates();
				refreshTable(getProgramAccess());
			}
			$('.SearchTemplateWrapper').removeClass('hide');
			$('.CreateTemplateWrapper').addClass('hide');
			$('#TemplateName').val('');
			msgbox('success',result.message);
		} else {
			msgbox('error',result.message);
		}
	});
});



jQuery('body').on('click','.BtnSaveTemplateName',function() {
	if (isTemplate() && getTemplateID() == '') {
		return;
	}
	if (!isTemplate() && getInputVal('#TemplateName') == '') {
		return;
	}
	
	confirmEvent("Are you sure you want to save this?", function(yes) {
		if (!yes) return;
		var result = ajaxRequest(base_url+page_url+'event','event=saveTemplate&isTemplate='+isTemplate()+'&TemplateID='+getTemplateID()+'&TemplateName='+getInputVal('#TemplateName')+'&data='+getData());
		if(result.error == false) {
			if (!isTemplate()) {
				refreshProgramTemplates();
				refreshTable(getProgramAccess());
			}
			$('.SearchTemplateWrapper').removeClass('hide');
			$('.CreateTemplateWrapper').addClass('hide');
			$('#TemplateName').val('');
			msgbox('success',result.message);
		} else {
			msgbox('error',result.message);
		}

		function getData() {
			var data = new Array;
			$('body #modal_program_access #TablePrograms tbody tr').each(function() {
				var self = $(this);
				if (self.find('.chk-child').is(':checked')) {
					data.push({
						id: self.attr('data-id')
					});
				}
			});
			return JSON.stringify(data);
		}
	});
});


function getProgramAccess(Program) {
	var Program = ( Program == undefined ? '' : Program ) ;
    var data = {
        event : 'showModalProgramAccess',
        ProgramTemplate : Program,
        isgroup : IsGroup,
    };
	return ajaxRequest(base_url+page_url+'event',data).content;
}

function refreshTable(data) {
	$('#modal_program_access .modal-body #TableProgramWrapper').html(data);
}

function refreshProgramTemplates() {
	var result = ajaxRequest(base_url+page_url+'event','event=getTemplates');
	var options = '<option value="" selected="">Select</option>';
	var option;
	for(var i in result) {
		var option = document.createElement('option');
		option.setAttribute('value',result[i].id);
		option.appendChild(document.createTextNode(result[i].name));
		options += option.outerHTML;
	}
	// $('#ProgramTemplate').select2('destroy');
	$('#ProgramTemplate').remove('option');
	$('#ProgramTemplate').html(options);
	// $('#ProgramTemplate').select2();
}

function isTemplate() {
	if(!$('.SearchTemplateWrapper').hasClass('hide')) {
		return 1;
	}
	return 0;
}

function getTemplateID() {
	return getSelectedVal('#ProgramTemplate');
}

function getUserProgramTemplate() {
    var res = '';
    if(account != undefined){
        res = ajaxRequest(base_url+page_url+'event','event=getUserProgramTemplate&account='+account,'HTML'); 
    }    
	return res.trim();
}

function setTemplateID(key) {
	$('#ProgramTemplate').val(key);
}

function getAccountIDSel() {
    
    var output = jQuery('#TableUsers tbody tr td .chk-child:checked').closest('tr').attr('data-id');    
    if(output == undefined){
        output = jQuery('.jstree-anchor.jstree-clicked').find('id').html();       
        IsGroup = 1; 
    }    
	return output;
}