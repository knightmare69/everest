 window.onscroll = changePos;
        
function changePos() {
    var header = document.getElementById("wgtreeview");
    var wdth = $("#tvaccounts").width();
    
    if (window.pageYOffset > 50) {
        header.style.position = "fixed";
        header.style.top = "60px";
        header.style.wdth = wdth;
        $("#wgtreeview").width('24%');
    } else {
        header.style.position = "";
        header.style.top = "";
        header.style.width = "";
    }
}

var ACCESS = {
	getAccess : function() {
		var data = new Array;
		$('#tree_access .jstree-container-ul li').find('ul.jstree-children li').each(function() {
			var child = $(this);
			if (child.hasClass('jstree-leaf')) {
				if (child.find('.jstree-anchor') && (child.find('.jstree-anchor').hasClass('jstree-clicked')))
				{	
					var action_id = child.find('.action').text();
					var menu = child.closest('ul').parent().find('a.jstree-anchor > .page');
					var page = menu.text();
					if (action_id != '') {
						data.push(JSON.stringify({
							'page': page,
							'action': action_id
						}));
					}
				}
			}
		});
		return data;
	},

	addNewAction: function(action,action_id) {
		return '<li role="treeitem" id="j2_21321" class="jstree-node  jstree-leaf" aria-selected="false">'
					+'<div unselectable="on" class="jstree-wholerow jstree-wholerow-clicked">&nbsp;</div>'
					+'<i class="jstree-icon jstree-ocl"></i>'
					+'<a class="jstree-anchor jstree-clicked" href="#">'
						+'<i class="jstree-icon jstree-checkbox"></i>'
						+'<i class="jstree-icon jstree-themeicon fa fa-gear jstree-themeicon-custom"></i>'
						+action+'<id class="action hide">'+action_id+'</id>'
					+'</a>'
				+'</li>';

	},

	saveAction: function(page,action) {
		return ajaxRequest(base_url+page_url+"actions",'event=save'+'&page='+page+'&action='+action);
	},

	updateAction: function(action_id,action) {
		return ajaxRequest(base_url+page_url+"actions",'event=update'+'&action_id='+action_id+'&action='+action);
	},

	autoSaveAction: function(data) {
		return ajaxRequest(base_url+page_url+"actions",'event=autoSaveAction&'+data,'JSON',false,true);
	},

	autoUncheckAction: function(data) {
		return ajaxRequest(base_url+page_url+"actions",'event=autoUncheckAction&'+data,'JSON',false,true);
	},

	save: function(data,account,account_type) {
		return ajaxRequest(base_url+page_url+'access','event=save&account='+account+'&account_type='+account_type+'&access='+data);
	},

	getGroupAccess: function(key) {
		return TREE.access(ajaxRequest(base_url+page_url+"access",'event=getGroupAccess&id='+key));
	},

	getUserAccess: function(key) {
		return TREE.access(ajaxRequest(base_url+page_url+"access",'event=getUserAccess&id='+key));
	}
};