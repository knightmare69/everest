var USER = function() {
    var id;
    jQuery('body').on('click','.a_addnew',function(){
        showModal({
            title : 'Create Account',
            content: ajaxRequest(base_url+page_url+'event','event=getForm','HTML'),
            class: 'modal_user',
            button: { 
                icon: 'fa fa-save',
                class: 'btn_save',
                caption: 'Save'
            }
        });
    });

    jQuery('body').on('click','.btn_save',function() {
        if (isFormValid(form)) {

            if (isEmailExist($('#email').val())) {
                msgbox('error','Email already taken.')
                return;
            }
          
            confirmEvent("Are you sure you want to save this record?", function(yes) {
                if (!yes) return;
                setAjaxRequest(
                    base_url+page_url+'event','event=save&'+ $(form).serialize(),
                    function(result) {
                        if (result.error) {
                            showError(result.message);
                        } else {
                            showSuccess(result.message);
                            clear_value(form);
                        }
                    }
                );
                progressBar('off');
            });
        }
    });

    jQuery('body').on('click','.btn_update',function() {
        if (isFormValid(form)) {
            if (isEmailExist(id)) {
                msgbox('error','Email already taken.');
                return;
            }

            if (!isPswdConfirmed()) {
                msgbox('error','Password did not matched!');
                return;
            }

            confirmEvent("Are you sure you want to save this record?", function(yes) {
                if (!yes) return;
                setAjaxRequest(
                    base_url+page_url+'event','event=update&FamilyID='+id+'&'+ $(form).serialize(),
                    function(result) {
                        if (result.error) {
                            showError(result.message);
                        } else {
                            showSuccess(result.message);
                        }
                    }
                );
                progressBar('off');
            });
        }
    });

    jQuery('.a_remove').click(function(){
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to remove this record/s?",
            callback : function(yes) {
                if ( yes )
                {
                    setAjaxRequest(
                        base_url+page_url+'event','event=delete&ids='+FN.getTableID(),
                        function(result) {
                           msgbox('success',result.message);
                        }
                    );
                    progressBar('off');
                }
            }
        })
    });

    jQuery('.a_modal_select').click(function(){
        id = jQuery(this).closest('tr').attr('data-id');
        showModal({
            title : 'Update Account',
            content: ajaxRequest(base_url+page_url+'event','event=edit&FamilyID='+id,'HTML'),
            class: 'modal_user',
            button: { 
                icon: 'fa fa-save',
                class: 'btn_update',
                caption: 'Update'
            }
        });

       $('body .modal_user .EmailToUser').uniform({checkboxClass: 'EmailToUser'});
    });

	return {
		init: function() {

		}
	}
	
}();