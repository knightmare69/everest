var USER = function() {
    jQuery('body').on('bind keyup','#username',function() {
        var self = jQuery(this);
        var span_username_checker = $('.span_username_checker');
        if (self.val() == '') {
            span_username_checker.addClass('bg-red-sunglo');
            span_username_checker.removeClass('bg-green-jungle');
            return;
        }
        setAjaxRequest(
            base_url+page_url+'event',
            'event=checkUsername&id='+self.val(),
            function(result) {
                if (result.isExist > 0) {
                    span_username_checker.addClass('bg-red-sunglo');
                    span_username_checker.removeClass('bg-green-jungle');
                } else {
                    span_username_checker.addClass('bg-green-jungle');
                    span_username_checker.removeClass('bg-red-sunglo');
                }
            },
            function(error){
                console.log(error.error);
            },
            'JSON',true,false
        );
    });
}();

function isEmailExist(key) {
    var result = ajaxRequest(base_url+page_url+'event','event=checkEmail&FamilyID='+key+'&email='+$('#email').val());

    if (result.isExist == true) {
        return true;
    }
    return false;
}

function isPswdConfirmed() {
    $('#upassword,#cpassword').removeAttr('style');
    if($('#upassword').val() == '' || $('#cpassword').val() == '') {
         $('#upassword,#cpassword').attr('style','border: 1px solid red');
        return false;
    }
    if ( $('#upassword').val() != $('#cpassword').val() ) {
        $('#upassword,#cpassword').attr('style','border: 1px solid red');
        return false;
    } 
    return true;
}