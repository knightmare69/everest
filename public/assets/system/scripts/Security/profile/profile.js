var Profile = function() {

    return {
        //main function
        init: function() {
            
            var min = 5
            
            $('#btnUpdatePassword').click(function() {
                if (!isValid()) return;
                confirmEvent("Are you sure you want to update this?", function(yes) {
                    if (!yes) return;
                    var result = ajaxRequest(base_url+page_url+'event','event=updatePassword&'+$('#formPassword').serialize());
                    if (result.error == false) {
                        msgbox('success',result.message);
                    } else {
                        msgbox('error',result.message);
                    }
                });

                function isMatch() {
                    return (getInputValByName('#formPassword','NewPassword') == getInputValByName('#formPassword','ConfirmPassword'));
                }

                function isMin() {
                    return (getInputValByName('#formPassword','NewPassword').length > min);
                }

                function isValid() {
                    var valid = true;
                    if (!isFormValid('#formPassword')) {
                        return valid = false;
                    }
                    if (!isMatch()) {
                        valid = false;
                        return msgbox('error','Password do not match!');
                    }
                    if (!isMin()) {
                        valid = false;
                        return msgbox('error','Password minimum length is 6 characters.');
                    }
                    return valid;
                }
            });

            $('#btnUpdateProfile').click(function() {
                if (!isFormValid('#formProfile')) return;
                confirmEvent("Are you sure you want to update this?", function(yes) {
                    if (!yes) return;
                    var result = ajaxRequest(base_url+page_url+'event','event=updateProfile&'+$('#formProfile').serialize());
                    if (result.error == false) {
                        msgbox('success',result.message);
                    } else {
                        msgbox('error',result.message);
                    }
                });
            });

            $('#btnUpdateAvatar').click(function() {
                if (!hasPhoto()) return;
                confirmEvent("Are you sure you want to update this?", function(yes) {
                    if (!yes) return;
                    ajaxRequestFile(base_url+page_url+'event?event=updateAvatar',getPhoto(),function(result) {
                        if (result.error == false) {
                            msgbox('success',result.message);
                        } else {
                            msgbox('error',result.message);
                        }
                    });
                });

                function getPhoto() {
                    var form_data = new FormData();
                    form_data.append('photo',$('#file')[0].files[0]);

                    return form_data;
                }

                function hasPhoto() {
                    if ($('#file')[0] != undefined) {
                        if ($('#file')[0].files[0] != undefined) {
                            return true;
                        }
                    }
                    return false;
                }
            });

            
        },

    };

}();