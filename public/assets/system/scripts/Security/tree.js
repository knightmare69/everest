var TREE = {
    accounts: function()
    {
        jQuery('#tree_accounts').jstree("destroy");
        jQuery('#tree_accounts').jstree({
            'plugins': ["wholerow"],
            'core': {
                "themes" : {
                    "responsive": true
                },    
                'data': ajaxRequest(base_url+page_url+'accounts','event=loadAccounts','JSON',false,false)
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    },
    access: function(data)
    {
        jQuery('#tree_access').jstree("destroy");
        jQuery('#tree_access').jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "themes" : {
                    "responsive": true
                },    
                'data': data
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    }
};