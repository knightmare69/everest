var i = 1;
var FN={
    ini : function() {
        FN.actions();
        FN.checkGrids();
        FN.dropDownSubTable();
    },
    actions: function(){
        // jQuery('body').keydown(function(e) {
        //     console.log(e.keyCode);
        // });
    },
    dropDownSubTable : function() {
        jQuery('body').on('click','.table_sub',function(){
            var self = jQuery(this).closest('tr');
            if (self.next().hasClass('sub_data')){
                if(self.next().hasClass('hide')){
                    self.next().animate({'opacity':'show'},'slow');
                    self.next().removeClass('hide');
                    jQuery(this).find('i').removeClass('fa-plus');
                    jQuery(this).find('i').addClass('fa-minus');
                } else {
                    self.next().addClass('hide');
                    jQuery(this).find('i').addClass('fa-plus');
                    jQuery(this).find('i').removeClass('fa-minus');
                    self.next().animate({'opacity':'hide'},'slow');
                }
            }
        });
    },
    checkGrids : function() {
         // jQuery('body .table tbody tr td .chk-child').each(function(){
         //    var _this = jQuery(this);

         //    var parent1 = _this.parent().parent();
         //    var parent2 = _this.parent().parent().parent().parent();
         //    var value = jQuery(this).is(':checked') ? true : false;

         //    if ( !parent1.hasClass('without-bg') && !parent2.hasClass('without-bg') && !jQuery(this).is(':disabled') )
         //    {
         //        if ( value )
         //        {
         //            parent1.removeClass('bg color-bg-light-green');
         //            //parent2.removeClass('bg bg-blue-hoki');

         //            parent1.addClass('bg bg-blue-steel');
         //           // parent2.addClass('bg bg-blue-steel');
         //        }
         //        else
         //        {
         //            parent1.removeClass('bg bg-blue-steel');
         //           // parent2.removeClass('bg bg-blue-steel');

         //            parent1.addClass('bg color-bg-light-green');
         //           // parent2.addClass('bg bg-blue-hoki');
         //        }
         //    }
         // });
    },

    getTableID : function(table) {

        var table = table == undefined ? '.table' : table;
        var _this,id;
        var arrItems=new Array();
        var objItems=new Object();
        var items=new Array();

        jQuery(table+' tbody tr').each(function(){
            _this = jQuery(this);
            id=_this.attr('data-id');
            if ( _this.find('td .chk-child').is(':checked') ){
                objItems.id=id;
                arrItems[0]='id';
                items.push(JSON.stringify(objItems,arrItems,'\t'));
            }
        });
        return items;
    },

    getTableStatus : function(table) {

        var table = table == undefined ? '.table' : table;
        var data = new Array();

        jQuery(table+' tbody tr').each(function(){
            var self = jQuery(this);
            if ( self.find('td .chk-child').is(':checked')){
                if (self.attr('data-status') == 1) {
                    data.push(JSON.stringify({id: self.attr('data-id'),status: 0}));
                } else {
                    data.push(JSON.stringify({id: self.attr('data-id'),status: 1}));
                }
            }
        });
        return data;
    },

    datePicker : function(name){
        var name = name != undefined ? name : '.date-picker';
        jQuery(name).datepicker();
    },

    datePickerTimeNoSeconds : function(name){
        var name = name != undefined ? name : '.timepicker-no-seconds';
        $(name).timepicker({
            autoclose: true,
            minuteStep: 1
        });
    },

    destroyDataTable : function() {
        var table = table != undefined ? table : '.table';
        jQuery(table).dataTable().empty();
        jQuery(table).dataTable().fnDestroy();
    }, 

    dataTable : function(table){
        var table = table != undefined ? table : '.table';
        var oTable = jQuery(table).dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "columnDefs": [{
                "orderable": false,
                "targets": [0]
            }],
            "order": [
                [1, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });
        var tableWrapper = jQuery(table+'_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        var tableColumnToggler = jQuery(table+'_column_toggler');
    },

    multipleSelect : function(name){
        var name = name != undefined ? name : '.select2';
        jQuery(name).select2();
        jQuery(name).trigger('change');
    }

};
jQuery(document).ready(FN.ini);