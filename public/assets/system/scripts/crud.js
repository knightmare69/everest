var CRUD = function() {
    var id;
    jQuery('body').on('click','.a_addnew',function(){
        $('.btn_modal').removeClass('btn_update');
        showModal({
            title : 'Create Account',
            content: ajaxRequest(base_url+page_url+'event','event=getForm','HTML'),
            class: 'modal_user',
            button: { 
                icon: 'fa fa-save',
                class: 'btn_save',
                caption: 'Save'
            }
        });
    });
    
   jQuery('body').on('click','.paginate_button',function(){
		Metronic.initUniform();
   });
   
    jQuery('body').on('click','.btn_save',function(){
	    var tmpform = $(this).closest('form').attr('id');
		    form    = ((tmpform!=undefined)?('#'+tmpform):'.form_crud');
	    if (isFormValid(form)) {
            bootbox.confirm({
                size : 'small',
                message : "Are you sure you want to save this record?",
                callback : function(yes) {
                   if ( yes )
                   {
                        setAjaxRequest(
                            base_url+page_url+'event','event=save&'+ $(form).serialize(),
                            function(result) {
                                if (result.error) {
                                    showError(result.message);
                                } else {
								    showSuccess(result.message);
                                    clear_value(form);
                                    if (result.table != undefined) {
                                        FN.destroyDataTable();
                                        $('.table_wrapper').html(result.table);
										FN.dataTable();
                                    }
                                }
                            }
                        );
                        progressBar('off');
						Metronic.initUniform();
                   }
                }
            })
        }else{
		  alert("Please complete the form before submit/save!");
		}
    });

    jQuery('body').on('click','.btn_update',function(){
	    var tmpform = $(this).closest('form').attr('id');
		    form    = ((tmpform!=undefined)?('#'+tmpform):'.form_crud');
        if (isFormValid(form)){
            var button = $(this);
            bootbox.confirm({
                size : 'small',
                message : "Are you sure you want to update this record?",
                callback : function(yes) {
                   if ( yes )
                   {
                        setAjaxRequest(
                            base_url+page_url+'event','event=update&id='+id+'&'+ $(form).serialize(),
                            function(result) {
                                if (result.error) {
                                    showError(result.message);
                                } else {
                                    showSuccess(result.message);
                                    if (result.table != undefined) {
                                        FN.destroyDataTable();
                                        $('.table_wrapper').html(result.table);
                                        FN.dataTable();
                                        clear_value(form);
                                    }

                                }
                            }
                        );
                        progressBar('off');
						Metronic.initUniform();
                   }
                }
            })
        }
    });

    jQuery('.a_remove').click(function(){
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to remove this record/s?",
            callback : function(yes) {
                if ( yes )
                {
                    setAjaxRequest(
                        base_url+page_url+'event','event=delete&ids='+FN.getTableID(),
                        function(result) {
                           msgbox(result.error == false ? 'success' : 'error',result.message);
                           if (result.table != undefined) {
                                FN.destroyDataTable();
                                $('.table_wrapper').html(result.table);
                                FN.dataTable();
                            }
                        }
                    );
                    progressBar('off');
					Metronic.initUniform();
                }
            }
        })
    });

    jQuery('.a_block').click(function(){
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to block this record/s?",
            callback : function(yes) {
                if ( yes )
                {
                    setAjaxRequest(
                        base_url+page_url+'event','event=block&ids='+FN.getTableID(),
                        function(result) {
                           msgbox(result.error == false ? 'success' : 'error',result.message);
                        }
                    );
                    progressBar('off');
					Metronic.initUniform();
                }
            }
        })
    });

   jQuery('.a_status').click(function(){
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to update this record/s status?",
            callback : function(yes) {
                if ( yes )
                {
                    setAjaxRequest(
                        base_url+page_url+'event','event=acOrdc&ids='+FN.getTableStatus(),
                        function(result) {
                           msgbox(result.error == false ? 'success' : 'error',result.message);
                            if (result.table != undefined) {
                                FN.destroyDataTable();
                                $('.table_wrapper').html(result.table);
                                FN.dataTable();
                            }
                        }
                    );
                    progressBar('off');
					Metronic.initUniform();
                }
            }
        })
    });

    jQuery('body').on('click','.a_modal_select',function(){
        id = jQuery(this).closest('tr').attr('data-id');
        showModal({
            title : 'Update Account',
            content: ajaxRequest(base_url+page_url+'event','event=edit&id='+id,'HTML'),
            class: 'modal_user',
            button: { 
                icon: 'fa fa-save',
                class: 'btn_update',
                caption: 'Update'
            }
        });
    });

    jQuery('body').on('click','.a_select',function(){
        id = jQuery(this).closest('tr').attr('data-id');
        jQuery('.form_wrapper').html(ajaxRequest(base_url+page_url+'event','event=edit&id='+id,'HTML'));
        var button = jQuery('.btn_action');
        button.addClass('btn_update');
        button.removeClass('btn_save');
        button.html("<i class='fa fa-save'></i> Update");
    });


    jQuery('body').on('click','.btn_reset',function(){
        var button = jQuery('.btn_action');
        clear_value(form);
        button.removeClass('btn_update');
        button.addClass('btn_save');
        button.html("<i class='fa fa-save'></i> Save");
        jQuery('.alert-dismissable').addClass('hide');
    });

	return {
		init: function() {
           Metronic.initUniform();
		}
	}
	
}();
