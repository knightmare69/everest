var MOD = {
    init: function(){
       this.load();
       $('.datepicker').datepicker();
    },

    load : function(){

      $('body').on('click', '.btn', $.proxy(this.menu, this) );
      $('body').on('change', '#academic-term,#level,#filter', function(e){
            var p = $('#level').find('option:selected').attr('data-prog');
            var f = $('#filter').val();
            location.replace(base_url+page_url+'?t='+ $('#academic-term').val()+'&y='+ $('#level').val()+'&p='+p+'&f='+f+'&r='+ Math.random() );
      });
    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'add': this.promote(e); break;
            case 'save': this.saveThis(e); break;
        }
    },

    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
            
           if( $(this).find('.chk-child').prop('checked') ) {
                
                var maj = $(this).find('.track').find('option:selected').val();
                
                if(maj == '' || maj == undefined){
                    maj = $(this).attr('data-major');
                }
            
                var d = {
                    ref : $(this).attr('data-id'),
                    idno : $(this).attr('data-idno'),
                    prog : $(this).find('.level').find('option:selected').attr('data-prog'),
                    major : maj,
                    level : $(this).find('.level').val(),
                };
                ids.push(d);
           }
        });
        return ids;
    },

    saveThis: function(){

        var selected = MOD.getSelected();
        var p = $('#level').find('option:selected').attr('data-prog');

        var data = { event : 'save' , term : $('#academic-term').val(),y : $('#level').val(), p: p ,f : $('#filter').val() , 'nos': selected }
        setAjaxRequest( base_url + page_url +'/event' , data ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    showSuccess( '<i class="fa fa-check"></i> ' +  r.message);
                    $('.scroller').html(r.data);
                }
                progressBar('off');
        },null,'json',true,true);
    },

    promote : function(){
        var s = MOD.getSelected();    
        if(s.length > 0){
            $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning! Do you wish to promote the selected student/s?</span>",
    			content : "Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes") {
    			     MOD.saveThis();
   			    }
    		});
        }
    },
};