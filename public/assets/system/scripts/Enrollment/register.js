var use_old_id = true;
var limit=10;
var yrlvl=0;
var xyrlvl=0;
var progid=0;
var progclass=0;
var classwmjr=21;
var type=1;
var termid=0;
var campus=1;
var default_tbl;
var dataTB;
var table = '#tblapplicant';
var toregister = [];
var selected   = {};
var registered = {};
var sel_count  = 0;
var wizard =false;
var active_modal='';
var xselect = false;

$(document).ready(function(){
  $('body').on('click','#agreed,.vwagreed',function(){
    //$('#modal_agreement').modal('show');
  });
  
  $('body').on('click','.checker span',function(){
	  var ischecked = $(this).is('.checked');
	  if(ischecked)
	  {
		$(this).removeClass('checked');
        $(this).find('input').prop('checked',false);
	  }
	  else
	  {

        $(this).addClass('checked');
        $(this).find('input').prop('checked',true);
	  }
  });

  $('body').on('click','.chk-header, .chk-child',function(){
	 get_allselected();
  });

  $('body').on('click','.chk-registered',function(){
	 var ischk = $(this).is(':checked');
	 var regid   = $(this).closest('span').attr('data-id');
     var isvalid = $(this).closest('span').attr('data-isvalid');
     var studno  = $(this).closest('span').attr('data-studno');
     var yrlvl   = $(this).closest('span').attr('data-yrlvl');
     if(ischk)
	 {
      registered[regid]={regid:regid,studno:studno,yrlvl:yrlvl,isvalid:isvalid};
	  sel_count++;
	 }
	 else
	 {
      delete registered[regid];
	  sel_count--;
	 }

	 sel_count=((sel_count<0)?0:sel_count);
	 if(sel_count>0)
	  $('.trselected').html('('+sel_count+' selected)');
	 else
	 {
      registered={};
	  $('.trselected').html('');
	  $('.chk-registered').removeClass('checked');
	  $('.chk-registered').find('input[type="checkbox"]').prop('checked',false);
	 }
  });

  $('body').on('change','#nxtlvl',function(){
	var trow   = $(this).closest('tr');
	var value  = $(this).val();
	var prgcls = $(this).find('[value="'+value+'"]').attr('data-class');
	if(prgcls<21)
	{
	 $(trow).find('#major').attr('disabled','disabled');
	 $(trow).find('#major').addClass('hidden');
     $(trow).find('#major').val(0);
	}
    else
    {
	 $(trow).find('#major').removeAttr('disabled');
	 $(trow).find('#major').removeClass('hidden');
	}
	get_allselected();
  });

  $('body').on('change','#major',function(){
	get_allselected();
  });

  $('body').on('change','#yrlvl,#type,#ayterm,#campus',function(){
	 if($(this).is('#yrlvl'))
     {
	  var optval = $(this).find('option:selected').attr('data-old-id');
	  progid     = $(this).find('[value="'+optval+'"]').attr('data-prog');
      progclass  = $(this).find('[value="'+optval+'"]').attr('data-class');
	 }

     localStorage.setItem('Campus', $('#campus').val() );
     localStorage.setItem('Term', $('#ayterm').val() );

	 get_list();
  });

  $('body').on('click','.btn-register',function(){
     toregister=[];
	 haserror=false;
	 $.each(selected,function(key,arr){
	    var studno = arr['studno'];
		var appno  = arr['appno'];
		var code   = arr['code'];
		var major  = arr['major'];
		/*
        if((code=='G11' || code=='G12') && major<=0)
	    {
		 haserror=true;
		 confirmEvent('<i class="fa fa-warning text-danger"></i> Select Strand/Track First for '+key,function(data){return; });
		 delete selected[key];
		 return;
	    }
        else
        */
		 toregister.push(arr);
	 });

	 if(haserror)
	 {
	  toregister=[];
	  return false;
	 }
	 else
	  reg_selected();
  });

  $('body').on('click','.btn-validate',function(){

    var regid   = ""; studno = '', validate =0;
    var cnt = 0;
    var validate_enrollment= function(r,s){
	  setAjaxRequest(
		base_url+'enrollment/actions?event=validate',{reg:r,idno: s },
		function(r)
		{
		  if(r.success){
			msgbox('success',r.message);
		  }
		},
		function(err)
		{
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
		});
      progressBar('off');
    }

    if(sel_count > 0 ){
        bootbox.confirm({ size : 'small',
    	message : "Do you wish to validate the selected registration(s)?",
    	callback : function(yes) {
    	   if (yes)
    	    $('.chk-registered:checked').each(function(){
               regid = $(this).closest('span').attr('data-id');
               studno = $(this).closest('span').attr('data-stdno');
               validate = $(this).closest('span').attr('data-isvalid');

               if(validate == 0 ){
                validate_enrollment(regid, studno);
                cnt++;
               }
            });
            if(cnt > 0){
                //location.reload();
            }
    	   else
    		return;
        }
       });

    }
  });

   $('body').on('click','.btn-cor',function(){
	 var term    = $('#ayterm').val();
	 var campus  = $('#campus').val();
	 var studno  = $('.chk-registered:checked').closest('span').attr('data-stdno');
	 var regid   = $('.chk-registered:checked').closest('span').attr('data-id');
	 var yrlvl   = $('.chk-registered:checked').closest('span').attr('data-yrlvl');
	 var major   = $('.chk-registered:checked').closest('span').attr('data-track');
	 var progid  = $('.chk-registered:checked').closest('span').attr('data-prog');
	 var feeid   = '';//$('.chk-registered:checked').closest('span').attr('data-feeid');
     var valid   = $('.chk-registered:checked').closest('span').attr('data-isvalid');

	 var arrdata = '';

     if(valid == '0'){
            $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning! Registered student is not yet validated...</span>",
    			content : "printing of <b>Certificate of Registration</b> was cancelled",
    		});

	   return false;
     }

	 if(sel_count>1)
	 {
	  alert('select one registration to print');
	  return false;
	 }

	 if(sel_count>0)
	 {
	  window.open(base_url+'enrollment/print_report?event=cor&term='+term+'&campus='+campus+
	                                                                ((regid!=undefined && regid!='')?'&regid='+regid:'')+
																	((studno!=undefined && studno!='')?'&studno='+studno:'')+
																	((yrlvl!=undefined && yrlvl!='')?'&yrlvl='+yrlvl:'')+
																	((major!=undefined && major!='')?'&major='+major:'')+
																	((progid!=undefined && progid!='')?'&progid='+progid:'')+
																	((arrdata!='')?'&'+arrdata:'')+
                                                                    '&t='+Math.random() ,'_blank');
	 }
  });

  $('body').on('click','.btn-sal',function(){
	 var term    = $('#ayterm').val();
	 var campus  = $('#campus').val();
	 var studno  = $('.chk-registered:checked').closest('span').attr('data-stdno');
	 var regid   = $('.chk-registered:checked').closest('span').attr('data-id');
	 var yrlvl   = $('.chk-registered:checked').closest('span').attr('data-yrlvl');
	 var major   = $('.chk-registered:checked').closest('span').attr('data-track');
	 var progid  = $('.chk-registered:checked').closest('span').attr('data-prog');
	 var feeid   = '';//$('.chk-registered:checked').closest('span').attr('data-feeid');
     var valid   = $('.chk-registered:checked').closest('span').attr('data-isvalid');

	 var arrdata = '';

     if(valid == '0'){
            $.SmartMessageBox({
    			title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning! Registered student is not yet validated...</span>",
    			content : "printing of <b>Certificate of Registration</b> was cancelled",
    		});

	   return false;
     }

	 if(sel_count>1)
	 {
	  alert('select one registration to print');
	  return false;
	 }

	 if(sel_count>0)
	 {
	  window.open(base_url+'enrollment/print_report?event=sal&term='+term+'&campus='+campus+
	                                                                ((regid!=undefined && regid!='')?'&regid='+regid:'')+
																	((studno!=undefined && studno!='')?'&studno='+studno:'')+
																	((yrlvl!=undefined && yrlvl!='')?'&yrlvl='+yrlvl:'')+
																	((major!=undefined && major!='')?'&major='+major:'')+
																	((progid!=undefined && progid!='')?'&progid='+progid:'')+
																	((arrdata!='')?'&'+arrdata:'')+
                                                                    '&t='+Math.random() ,'_blank');
	 }
  });


  $('body').on('click','.btn-print',function(){
	 var term    = $('#ayterm').val();
	 var campus  = $('#campus').val();
	 var studno  = $('.chk-registered:checked').closest('span').attr('data-stdno');
	 var regid   = $('.chk-registered:checked').closest('span').attr('data-id');
	 var yrlvl   = $('.chk-registered:checked').closest('span').attr('data-yrlvl');
	 var major   = $('.chk-registered:checked').closest('span').attr('data-track');
	 var progid  = $('.chk-registered:checked').closest('span').attr('data-prog');
	 var feeid   = '';//$('.chk-registered:checked').closest('span').attr('data-feeid');
	 var event   = ((feeid=='')?'assessment':'assessmentx');
	 var arrdata = '';
	 var group = 0;
     var counter = $('.chk-registered:checked').length;

     /* Allowed Bulk Printing */
     if(counter>1) {

         if(counter > 30){
            alert('select one registration to print');
            return false;
         }

         var markd = [];
         group = 1;

         //$('.chk-registered:checked').closest('span').attr('data-id');
         $('.chk-registered:checked').each(function(){
            markd.push($(this).closest('span').attr('data-id'));
         });

         Metronic.blockUI({target: '#ctable', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Please wait while generating report... '});

         var data = ajaxRequest( base_url+'enrollment/actions?event=assessment-reg',{regid:markd},'json',false, false);

         Metronic.unblockUI('#ctable');

         //console.log(markd);
         regid = data.content;
	    //return false;
	 }

     var url = base_url+'enrollment/print_report?event='+event+'&term='+term+'&campus='+campus+'&group='+group+
                ((regid!=undefined && regid!='')?'&regid='+regid:'')+
				((studno!=undefined && studno!='')?'&studno='+studno:'')+
				((yrlvl!=undefined && yrlvl!='')?'&yrlvl='+yrlvl:'')+
				((major!=undefined && major!='')?'&major='+major:'')+
				((progid!=undefined && progid!='')?'&progid='+progid:'')+
				((arrdata!='')?'&'+arrdata:'') + '&t='+ Math.random()
                ;

	 // if(sel_count>0) {
        var newWin = window.open(url,'_blank');
        if(!newWin || newWin.closed || typeof newWin.closed=='undefined')
        {
            alert("Pop-up Blocker is enabled! Please add this site to your exception list.");
        }

	 //}
  });

  $('body').on('click','.btn-child-assessment',function(){
	var term   = $('[data-term]').attr('data-term');
	var campus = $(this).closest('tr').attr('data-campus');
	var studno = $(this).closest('tr').attr('data-id');
	var regid  = $(this).closest('tr').attr('data-regid');
    var yrlvl  = ((use_old_id)?$(this).closest('tr').attr('data-oldid'):$(this).closest('tr').attr('data-target'));
	var progid = $(this).closest('tr').attr('data-prog');
	var major  = $(this).closest('tr').attr('data-track');

	if(studno!='' && studno!=undefined && regid!='' && regid!=undefined && yrlvl!='' && yrlvl!=undefined){
	//window.open(base_url+'enrollment/print_report?event=assessment&term='+term+'&campus='+campus+'&regid='+regid+'&studno='+studno+'&yrlvl='+yrlvl+'&major='+major+'&progid='+progid,'_blank');
	  window.location.href=base_url+'accounting/soaccounts?studno='+studno;
	}
  });

  $('body').on('click','.btn-child-soa',function(){
	var term   = $('[data-term]').attr('data-term');
	var campus = $(this).closest('tr').attr('data-campus');
	var studno = $(this).closest('tr').attr('data-id');
	var regid  = $(this).closest('tr').attr('data-regid');
    var yrlvl  = ((use_old_id)?$(this).closest('tr').attr('data-oldid'):$(this).closest('tr').attr('data-target'));
	var progid = $(this).closest('tr').attr('data-prog');
	var major  = $(this).closest('tr').attr('data-track');

	if(studno!='' && studno!=undefined && regid!='' && regid!=undefined && yrlvl!='' && yrlvl!=undefined){
	//window.open(base_url+'enrollment/print_report?event=soa&term='+term+'&campus='+campus+'&regid='+regid+'&studno='+studno+'&yrlvl='+yrlvl+'&major='+major+'&progid='+progid,'_blank');
	  window.location.href=base_url+'accounting/soaccounts?studno='+studno;
	}
  });

  $('body').on('click','.btn-stats',function(){
	 get_stats();
  });

  $('body').on('click','.btn-list',function(){
	  window.location = base_url+'enrollment/registered';
  });

  $('body').on('click','.btn-child-payment',function(){
	  var xregid = $(this).closest('tr').attr('data-regid');
		  active_modal = 'modal_payment';
	  progressBar('on');
	  setAjaxRequest(
				base_url+'enrollment/actions?event=reginfo',{regid:xregid},
				function(data)
				{
				  if(data.success && data.content!='')
				  {
			        $('#payment_form').replaceWith(data.content);
                    $('#modal_payment').modal('show');
                    $('#'+active_modal).find('#studtemplate').change(function(){
						var id = $(this).val();
						get_templateinfo(id);
						//console.clear();
					});
			      }
				  progressBar('off');
				},
                function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
				});

  });

  $('body').on('click','.button-update',function(){
	  var regid   = $('#'+active_modal).find('#regid').val();
	  var studno  = $('#'+active_modal).find('#studno').val();
	  var yrlvl   = $('#'+active_modal).find('#studlvl').val();
	  var tempid  = $('#'+active_modal).find('#studtemplate').val();
	  var method  = $('#'+active_modal).find('#paymethod').val();
	  var xamt    = $('#'+active_modal).find('.studtotalamt').attr('data-value');

	  regid  = ((regid==undefined || regid==null)?'':regid);
	  studno = ((studno==undefined || studno==null)?'':studno);
	  yrlvl  = ((yrlvl==undefined || yrlvl==null || yrlvl=='-1')?0:yrlvl);
	  tempid = ((tempid==undefined || tempid==null || tempid=='-1')?0:tempid);
	  method = ((method==undefined || method==null || method=='-1')?0:method);
	  xamt   = ((xamt==undefined || xamt==null)?0:xamt);

	  if(regid!='' && studno!='' && yrlvl!=0 && tempid!=0)
	  {
		toregister=[];
        toregister.push({regid:regid
						,studno:studno
		                ,yrlvl:yrlvl
						,tempid:tempid
						,term:termid
						,campus:campus
						,method:method
						,amt:xamt});
        reg_child();
	  }
  });
  
  $('body').on('click','.button-cancel, .btn-child-xregister',function(){
    $('.dvchild').removeClass('hidden');
	$('.dvenroll').addClass('hidden');
  });
  
  $('body').on('click','.btn-child-register, .btn-child-xregister',function(){
	var id          = $(this).closest('tr').attr('data-id');
	var regid       = $(this).closest('tr').attr('data-regid');
	var family      = $(this).closest('tr').attr('data-family');
    var name        = $(this).closest('tr').find('td:nth-child(3)').html();
	var xyrlvl      = $(this).closest('tr').attr('data-target');
	var prgcls      = $(this).closest('tr').attr('data-trgtcls');
	var major       = $(this).closest('tr').attr('data-track');
	var majorname   = $(this).closest('tr').find('td:nth-child(6)').html();
	var xyrlvlname  = $(this).closest('tr').find('td:nth-child(5)').html();
	    termid      = $('[data-term]').attr('data-term');
	var	syterm      = $('[data-term]').attr('data-sy');
	    campus      = $(this).closest('tr').attr('data-campus');
	    active_modal= 'modal_register';
	
	xinit_Wizard();
	load_familyinfo();
	$(".help-block").html('');
	$('#term').html(syterm);
	$('#term').attr('data-id',termid);
	$('#regid').val(regid);
	$('#famid').val(family);
	$('#studno').html(id);
	$('#studname').html(name);
	$('.lbstdname').html(name);
	$('#studlvl').attr('data-id',xyrlvl);
	$('#studlvl').html(xyrlvlname);
	$('.lbstdlvl').html(xyrlvlname);
	$('.major-group').addClass('hidden');
	$('#studmajor').removeAttr('required');
	$('#studmajor').attr('data-major',0);
	$('#studmajor').html('');
	$('.xinstruction').closest('.form-group').addClass('hidden');
	$('.xinstruction').html('');
	//$('#studmajor').val('-1');
	if(prgcls==21){
	 $('#studmajor').attr('required',true);
	 $('.major-group').removeClass('hidden');
	 $('#studmajor').attr('data-major',major);
	 $('#studmajor').html(majorname);
	 //$('#studmajor').val(major);
	}

	$('.dvchild').addClass('hidden');
	$('.dvenroll').removeClass('hidden');
	$('.datepicker').datepicker();
  });

  $('body').on('click','.td-current',function(){
	var stdno = $(this).closest('tr').attr('data-id');
	var term  = $('[data-term]').attr('data-term');
	get_ledger(term,stdno)
  });

  $('body').on('click','.button-submit',function(){
	 var isagree = $('#agreed').prop('checked');
	 if(!isagree){
	   confirmEvent('<i class="fa fa-warning text-danger"></i> Agree first before Submitting!',function(btn){return;});
       return false;
	 }

	 if($('.has-error').length<=0 && isagree){
	  var ispay     = $(this).attr('data-payment');
	  var regid     = $('#regid').val();
	  var studno    = $('#studno').html();
	  var yrlvl     = $('#studlvl').attr('data-id');
	  var major     = $('#studmajor').attr('data-major');
	//var major     = $('#studmajor').val();
	  var tempid    = $('#studtemplate').val();
	  var method    = $('#paymethod').val();
	  var famid     = $('#famid').val();
	  var xoption   = $('#xoption').val();
	  var guarddata = $('.guarddata').serialize()+'&option='+((xoption!='' && xoption!==undefined)?xoption:'new');
      var xamt      = $('.studtotalamt').attr('data-value');
	  
	  regid  = ((regid==undefined || regid==null)?'':regid);
	  studno = ((studno==undefined || studno==null)?'':studno);
	  yrlvl  = ((yrlvl==undefined || yrlvl==null || yrlvl=='-1')?0:yrlvl);
	  major  = ((major==undefined || major==null || major=='-1')?0:major);
	  tempid = ((tempid==undefined || tempid==null || tempid=='-1')?0:tempid);
	  method = ((method==undefined || method==null || method=='-1')?0:method);
	  famid  = ((famid==undefined || famid==null)?'':famid);
	  xamt   = ((xamt==undefined || xamt==null)?0:xamt);

	  //return false;
	  if(studno!='' && yrlvl!=0 && tempid!=0)
	  {
		toregister=[];
        toregister.push({regid:regid
						,studno:studno
		                ,yrlvl:yrlvl
						,major:major
						,tempid:tempid
						,term:termid
						,campus:campus
						,method:method
						,famid:famid
						,guard:guarddata
						,amt:xamt});

	  }
	  if(toregister.length<=0){return false; }
         bootbox.confirm({
	     size : 'small',
	     message : "Are you sure you want to register selected students?",
	     callback : function(yes) {
	                 if (yes){
		               reg_child(ispay);
		             }
		           }
		 });
	 }
  });

  $('body').on('change','.studpaymode',function(){
	var scheme    = $(this).val();
	var id        = 0;
    var xyrlvl    = 0;
	var xmajor    = 0;
	if(active_modal=='modal_register'){
	 id     = $('#'+active_modal).find('#studno').html();
     xyrlvl = $('#'+active_modal).find('#studlvl').attr('data-id');
	 xmajor = $('#'+active_modal).find('#studmajor').attr('data-major');
	}else if(active_modal=='modal_payment'){
	 termid = $('#'+active_modal).find('#termid').val();
	 campus = $('#'+active_modal).find('#campus').val();
	 id     = $('#'+active_modal).find('#studno').val();
     xyrlvl = $('#'+active_modal).find('#studlvl').val();
	 xmajor = $('#'+active_modal).find('#studmajor').val();
	}

	$('#'+active_modal).find('.breakdown').html('');
	setAjaxRequest(
				base_url+'enrollment/actions?event=seltemplate',
				{studno:id,yrlvl:xyrlvl,major:xmajor,scheme:scheme,term:termid,campus:campus,tempid:0},
				function(data){
				  if(data.success && data.content!=''){
			       $('#'+active_modal).find('#paymethod').val('-1');
				   $('#'+active_modal).find('#studtemplate').replaceWith(data.content);
				   get_templateinfo($('#'+active_modal).find('#studtemplate').find('option[value!="-1"]').attr('value'));
				   /*
				   if($('#'+active_modal).find('#studtemplate').length>0){
					 $('#'+active_modal).find('#studtemplate').attr('required',true);
					 $('#'+active_modal).find('#studtemplate').change(function(){
						var id = $(this).val();
						get_templateinfo(id);
						//console.clear();
					 });
				   }
				   */
				  }
				  progressBar('off');
				}
	 );

  });

  $('body').on('change','.paymethod',function(){
	  var opt_val  = $(this).val();
	  var instruct = $(this).find('[value="'+opt_val+'"]').attr('data-instruct');

	  $('#'+active_modal).find('.xinstruction').closest('form-group').addClass('hidden');
	  $('#'+active_modal).find('.xinstruction').html();
	  if(instruct!='' && instruct!=undefined && instruct!=null)
      {
		$('#'+active_modal).find('.xinstruction').closest('.form-group').removeClass('hidden');
	    $('#'+active_modal).find('.xinstruction').html(instruct);
	  }
      else
      {
		$('#'+active_modal).find('.xinstruction').closest('.form-group').addClass('hidden');
	    $('#'+active_modal).find('.xinstruction').html('');
	  }
  });

  $('.tmp_length').change(function(){
	  var value = $(this).val();
	  limit = value;
	  $(table+'_length').find('select').val(value);
	  $(table+'_length').find('select').trigger('change');
  });

  $('body').on('keydown','.tmp_search',function(e){
	var value = $(this).val();
	$(table+'_filter').find('input').val(value);
	if(e.keyCode==13)
	{
	  $(table+'_filter').find('input').trigger('input');
	}
  });


  $('body').on('click','.btn-purge',function(){
   bootbox.confirm({
	size : 'small',
	message : "Are you sure you want to delete selected data?",
	callback : function(yes) {
	   if (yes)
	    purge_selected();
	   else
		return;
    }
   });

  });

  if($('#tblapplicant').length>0)
  {
   table = '#tblapplicant';
   get_list();
  }

  if($('#tblregistered').length>0)
  {
   table = '#tblregistered';
   get_list();
  }

  if($('#modal_register').length>0)
  {
	init_Wizard();
	//alert('henshin');
  }

  if($('[data-trgtno]').attr('data-trgtno')!='')
  {
	var appno  = $('[data-trgtno]').attr('data-trgtno');
	var trrow  = $('[data-appno="'+appno+'"]');

	if($(trrow).find('.btn-child-register').length>0)
	 setTimeout(function(){$(trrow).find('.btn-child-register').click(); },800);
    else if($(trrow).find('.btn-child-xregister').length>0)
	 setTimeout(function(){$(trrow).find('.btn-child-xregister').click(); },800);
  }

 //console.clear();
});

function get_ledger(term,stdno)
{
 active_modal='';
 if(stdno!='' && stdno!=undefined)
 {
  progressBar('on');
  setAjaxRequest(
				base_url+'enrollment/actions?event=ledger',
				{studno:stdno,term:term},
				function(data){
				  if(data.success && data.content!='')
				  {
			       $('.dv_ledger').html(data.content);
				   $('#modal_ledger').modal('show');
				   //console.clear();
				  }
				  progressBar('off');
				}
	);
 }
 return false;
}

function get_templateinfo(tid)
{
 $('.breakdown').html('');
 var id        = $('#studno').html();
 var xyrlvl    = $('#studlvl').attr('data-id');
 var scheme    = $('#studpaymode').val();
 if(id==undefined || tid==undefined){return; }
 setAjaxRequest(
			  //base_url+'enrollment/actions?event=tempinfo',
				base_url+'accounting/txn?event=child_template',
				{studno:id,yrlvl:xyrlvl,scheme:scheme,termid:termid,campus:campus,templid:tid},
				function(data){
				  if(data.success && data.content!=''){
			       $('.breakdown').html(data.content);
				   //console.clear();
				  }
				  progressBar('off');
				}
	);
}

function get_stats()
{
  //alert(termid);
  setAjaxRequest(
				base_url+'enrollment/actions?event=stats&term='+termid,{},
				function(data){
				  if(data.success)
				  {
				   bootbox.dialog({
					message: data.content,
					title: "Statistics",
					buttons: {
					  OK: {
							label: "OK",
							className: "blue",
							callback: function(){ }
				     },
					}
				   });
				   //console.clear();
			      }
				  progressBar('off');
				},
				function(err){
				  confirmEvent('<i class="fa fa-warning text-warning"></i> Failed to Load Stats!',function(data){return;});
				  progressBar('off');
				}
			   );
}

function purge_selected()
{
 $.each(registered,function(regid,arr){
  var isvalid = arr.isvalid;
  if(regid==undefined){return; }
  if(isvalid==1)
  {
    confirmEvent('<i class="fa fa-warning text-danger"></i> '+regid+' Is Already Validated!',function(data){return;});
    delete registered[regid];
	sel_count--;
	if(sel_count<=0){get_list(); }
  }
  else
  {
    setAjaxRequest( base_url+'enrollment/actions?event=delete&regid='+regid,{},
        function(data){
            if(data.success)
            {
                delete registered[regid];
                sel_count--;
                if(sel_count<=0){get_list(); }
			}
            sysfnc.showAlert('#tblregistered_wrapper','success',data.message,true,true,true);
            console.clear();
            progressBar('off');
	    },
        function(err){
            confirmEvent('<i class="fa fa-warning text-warning"></i> Failed to Execute!',function(data){return;});
            progressBar('off');
        }
    );
  }
 });
}

function get_allselected()
{
 $('.chk-student').each(function(){
	var id    = $(this).closest('span').attr('data-id');
	var appno = $(this).closest('span').attr('data-appno');
    var status = $(this).closest('span').attr('data-status');
    var foreign = $(this).closest('span').attr('data-foreign');
    var prog = $(this).closest('span').attr('data-prog');
    var ischk = $(this).is(':checked');
	var trow  = $(this).closest('tr');

	if(ischk && (id!==undefined || appno!==undefined))
	{
	  var xyrlvl = $(trow).find('#nxtlvl').val();
	  var code   = $(trow).find('#nxtlvl').find('[value="'+xyrlvl+'"]').attr('data-code');
	  var xmajor = $(trow).find('#major').val();;
	  xmajor= ((xmajor==undefined || xmajor==undefined)?0:xmajor);
	  appno = ((appno==undefined)?'':appno);

	  if((id==undefined||id=='') && appno!=undefined && appno!='')
	   selected[appno]={termid:termid,studno:'',appno:appno,code:code,yrlvl:xyrlvl,major:xmajor, status:status, foreign:foreign,prog:prog};
      else
	   selected[id]={termid:termid,studno:id,appno:appno,code:code,yrlvl:xyrlvl,major:xmajor,status:status, foreign:foreign,prog:prog};

	}
	else if(ischk==false && (id!==undefined || appno!==undefined))
	{
	  if((id==undefined||id=='') && appno!=undefined && appno!='')
	   delete selected[appno];
      else
	   delete selected[id];
	}
 });

 count_selected();
}

function count_selected()
{
 xcount = 0;
 $.each(selected,function(key,arr){
  if(key!==undefined)
  {
	xcount++;
  }
 });

 if(xcount>0)
  $('.tr_selected').html('('+xcount+' selected)');
 else
  $('.tr_selected').html('');
}

function assess_child(){
 var termid  = $('[data-term]').attr('data-term');
 var refno   = $('#regid').val();
 var studno  = $('#studno').html();
 var yrlvl   = $('#'+active_modal).find('#studlvl').val();
 var tempid  = $('#'+active_modal).find('#studtemplate').val();
 var method  = $('#'+active_modal).find('#paymethod').val();
 var xamt    = $('#'+active_modal).find('.studtotalamt').attr('data-value');
 setAjaxRequest(
				base_url+'accounting/txn?event=child_assess',{termid:termid,studno:studno,refno:refno,templid:tempid},
				function(data){
				  if (data.success){ 
				    console.log('assess success');
				  }
				  $('.button-cancel').trigger('click');
				},
				function(err)
				{
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Execute',function(data){return;});
			      progressBar('off');
				}
 	);		  
}

function reg_child(ispay){
 var xlink = base_url+'enrollment/actions?event=register';
     ispay = ((ispay===undefined)?0:ispay);
 setAjaxRequest(
				xlink,
				{register:toregister},
				function(data){
				  var icon = '<i class="fa fa-warning text-warning"></i>';
				  var xoption = $('#xoption').val();
				  if (data.success){
					if($('#modal_register').length>0){
					  assess_child();
					  if(ispay==1){
					    var termid = toregister[0]['term'];
					    var stdno  = toregister[0]['studno'];
					    window.location.href = base_url+'epayment?term='+termid+'&stdno='+stdno+'&schedid=1';
					  }
					}
					
					$('#xoption').val('old');
					icon = '<i class="fa fa-check text-success"></i>';
				    if(active_modal=='modal_register' && $('.guarddata').length>0){update_familyinfo(); }
					get_child();
					if($('#modal_payment').length>0){$('#modal_payment').modal('hide'); }
					//console.clear();
				  }else{
				   confirmEvent(icon+' '+data.message,function(data){return;});
			       progressBar('off');
				  }
				},
				function(err)
				{
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Execute',function(data){return;});
			      progressBar('off');
				}
			);
}

function get_child(){
 var xlink = base_url+'enrollment/actions?event=listchild';
 setAjaxRequest(xlink,'',function(data){
   if(data.success)
   {
    $('#tbchildren').replaceWith(data.content);
    //console.clear();
   }
   else
    confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Load',function(data){return;});
 });
 progressBar('off');
}

function reg_selected()
{
 var xlink = base_url+'enrollment/actions?event=register';
 if(toregister.length<=0){
	alert('Error');
	return false;
 }
 bootbox.confirm({
	size : 'small',
	message : "Are you sure you want to register selected students?",
	callback : function(yes) {
	   if (yes)
	   {
		    //function setAjaxRequest(url,data,success,error,dataType,async,progress)
			setAjaxRequest(
				xlink,
				{register:toregister},
				function(data)
				{
				  var icon = '<i class="fa fa-warning text-warning"></i>';
				  if (data.success)
				  {
					icon = '<i class="fa fa-check text-success"></i>';
					get_list();
					progressBar('off');
				  }
				  confirmEvent(icon+' '+data.message,function(data){return;});
				  ////console.clear();
				},
				function(err)
				{
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Execute',function(data){return;});
				}
			);
			progressBar('off');
	   }
	}
 });
}

function get_list()
{
  $('.chk-header').closest('span').removeClass('checked');
  $('.chk-header').prop('checked',false);
  toregister = [];
  selected   = {};
  registered = {};
  sel_count  = 0;
  if(default_tbl==undefined)
   default_tbl = $(table).clone();
  else if(dataTB!==undefined)
   dataTB.destroy();

 if(table=='#tblapplicant')
 {
  yrlvl  = $('#yrlvl').val();
  xyrlvl = $('#yrlvl').find('[value="'+yrlvl+'"]').attr('data-old-id');
  progid = $('#yrlvl').find('[value="'+yrlvl+'"]').attr('data-prog');
  type   = $('#type').val();
  termid = $('#ayterm').val();
  campus = $('#campus').val();
  init_Table(table);
 }
 else if(table=='#tblregistered')
 {
  yrlvl  = $('#yrlvl').find('option:selected').attr('data-old-id');
  termid = $('#ayterm').val();
  campus = $('#campus').val();
  if (campus == '0'){
    campus = localStorage.getItem("Campus");
    $('#campus').val(campus);
  }

  sy     = $('#ayterm').find('[value="'+termid+'"]').html();
  $('.SY').html(sy);
  init_List(table);
 }
}

function init_Table(id)
{
   if(yrlvl==undefined || type==undefined){return false;}  
   var xlink  = base_url+'enrollment/actions?event=list&level='+yrlvl+'&progid='+progid+'&type='+type+'&term='+termid+'&campus='+campus;
   dataTB = $(id).DataTable({
	            processing: true,
				serverSide: true,
				ajax: xlink,
                "order": [[ 2, "asc" ]],
				"lengthMenu": [
								[10, 15, 20, 25, 50, -1],
								[10, 15, 20, 25, 50, "All"] // change per page values here
							  ],
				"pageLength": limit,
				"language": {
							 "lengthMenu": "_MENU_",
							 "paging": {
										"previous": "Prev",
										"next": "Next"
									   },
							 "emptyTable": "<i class='fa fa-warning text-warning'></i> No data available",
							 "info": "Showing _START_ to _END_ of <strong class='tbltotal'> _TOTAL_ </strong> entries <i class='tr_selected'></i>",
							 "infoEmpty": "",
							 "infoFiltered": "(filtered1 from _MAX_ total entries <span class='tr_selected'></span>)",
							 "search": "Search:",
							 "zeroRecords": "<i class='fa fa-warning text-warning'></i> No matching records found"
							},
				"dom": '<"hidden"lf>r<"#tbstyle"t><"row"<"col-sm-6"i><"col-sm-6 sm right"p>>',
				"autoWidth": false,
				"columnDefs": [{
								"targets": [0],
								"orderable": false
							   },
							   {
								"targets": [4],
								"visible": false,
								"searchable": false
								},],
				"initComplete":function(settings,json)
				               {
								 var sel_yrlvl = $('#yrlvl').val();
								 var sel_yrcode = $('#yrlvl').find('[value="'+sel_yrlvl+'"]').attr('data-code');
								 var withmajor = $('#yrlvl').find('[value="'+sel_yrlvl+'"]').attr('data-withmajor');
								 var sel_opt   = $('#type').val();

								 if((sel_opt<2 && withmajor==1) || (sel_opt==2 && withmajor==1 && sel_yrcode!='G10'))
								 {
								  col_trgt = dataTB.column(4);
                                  col_trgt.visible(true);
								 }
								 else if((sel_opt==3 && withmajor==1))
								 {
								  col_trgt = dataTB.column(4);
                                  col_trgt.visible(true);
								 }
								 else
								 {
								  $('#major').addClass('hidden');
								 }

								 var totalcount = xparseFloat($('.tbltotal').html());
								 if(totalcount>1000)
								  $('[name="tblapplicant_length"]').find('[value="-1"]').addClass('hidden');
								 else
                                  $('[name="tblapplicant_length"]').find('[value="-1"]').removeClass('hidden');

								 $('#tbstyle').attr('style','overflow:auto;max-height:400px;');
								 //console.clear();
							   },
                "drawCallback":function(settings)
				               {
								 is_checked();
								 count_selected();
								 //console.clear();
				               },
			  });
   //console.clear();
}

function is_checked()
{
 $('.chk-student').each(function(){
	var id = $(this).closest('[data-id]').attr('data-id');
	var appno = $(this).closest('[data-id]').attr('data-appno');
	var trdata = [];
	if(selected[id]!==undefined)
	{
	  trdata = selected[id];
	  $(this).prop('checked',true);
	  $(this).closest('span').addClass('checked');
	}
	else if(selected[appno]!==undefined)
	{
	  trdata = selected[appno];
	  $(this).prop('checked',true);
	  $(this).closest('span').addClass('checked');
	}

	if(selected[id]!==undefined || selected[appno]!==undefined)
	{
	  var yrlvl = trdata['yrlvl'];
	  var major = trdata['major'];
	  var trrow = $(this).closest('tr');
	  $(this).closest('tr').find('#nxtlvl').val(yrlvl);
	  $(this).closest('tr').find('#major').val(major);
	}
 });
}

function init_List(id)
{
   if(yrlvl==undefined){return false;}
       progid = $('#yrlvl').find('option:selected').attr('data-prog');
   var xlink = base_url+'enrollment/actions?event=registered&level='+yrlvl+'&progid='+progid+'&term='+termid+'&campus='+campus;
   dataTB = $(id).DataTable({
	            processing: true,
				serverSide: true,
				ajax: xlink,
                "order": [[ 2, "desc" ]],
				"lengthMenu": [
								[10, 15, 20, 25, 50, -1],
								[10, 15, 20, 25, 50, "All"] // change per page values here
							  ],
				"pageLength": limit,
				"language": {
							 "lengthMenu": "_MENU_",
							 "paging": {
										"previous": "Prev",
										"next": "Next"
									   },
							 "emptyTable": "<i class='fa fa-warning text-warning'></i> No data available",
							 "info": "Showing _START_ to _END_ of <strong class='tbltotal'>_TOTAL_</strong> entries <i class='trselected'></i>",
							 "infoEmpty": "",
							 "infoFiltered": "(filtered1 from _MAX_ total entries)",
							 "search": "Search:",
							 "zeroRecords": "<i class='fa fa-warning text-warning'></i> No matching records found"
							},
				"dom": '<"hidden"lf>r<"#tbstyle"t><"row"<"col-sm-6"i><"col-sm-6 sm right"p>>',
				"autoWidth": false,
				"columnDefs": [{
								"targets": [0],
								"orderable": false
							   }],
				"initComplete":function(settings,json)
				               {
								 var totalcount = xparseFloat($('.tbltotal').html());
								 if(totalcount>1000)
								  $('[name="tblregisterd_length"]').find('[value="-1"]').addClass('hidden');
								 else
                                  $('[name="tblregisterd_length"]').find('[value="-1"]').removeClass('hidden');

								 $('#tbstyle').attr('style','overflow:auto;max-height:400px;');

								 //console.clear();
								},
                "drawCallback":function(settings)
				               {
								 //console.clear();
								 is_selected();
				               },
			  });
  //console.clear();
}

function is_selected()
{
 $.each(registered,function(regid,arr){
    $('[data-id="'+regid+'"]').find('input[type="checkbox"]').prop('checked',true);
    $('[data-id="'+regid+'"]').addClass('checked');
 });

 if(sel_count>0)
  $('.trselected').html('('+sel_count+' selected)');
 else
  $('.trselected').html('');

}

function xinit_Wizard()
{
 $('.done').removeClass('done');
 $('.has-error').removeClass('has-error');
 $('.has-success').removeClass('has-success');
 $(form).find('.form-control').each(function(){
	if($(this).is('.guarddata')){
		return;
	}
	
	if($(this).is('input[type="checkbox"]')){
	  $(this).prop('checked',false);
	  $(this).removeAttr('checked');
	}
	
	if($(this).is('input[type!="checkbox"]'))
	 $(this).val('');
	else if($(this).is('select'))
	 $(this).val('-1');
	else
	 $(this).html('');
 });
 
 $(form).find('input[type="checkbox"]').prop('checked',false);
 $(form).find('input[type="checkbox"]').removeAttr('checked');
 $(form).find('.checked').removeClass('checked');
 
 $('#tab2 .nav-tabs').find('.active').removeClass('active');
 $('#tab2 .tab-content').find('.active').removeClass('active');
 $('#tab2 .tab-content').find('.in').removeClass('in');
 
 $('#tab2 .nav-tabs').find('li:nth-child(1)').addClass('active');
 $('#tab2 #tab_guardian').addClass('active');
 $('#tab2 #tab_guardian').addClass('in');
 
 $('.breakdown').html('');
 $('.major-group').addClass('hidden');
 $('.button-previous').hide();
 $('.button-submit').hide();
 $('.button-next').show();
 if(wizard==false)
 {
   init_Wizard();
   wizard=true;
 }
 else
  $('#modal_register').bootstrapWizard('show',0);

}

function init_Wizard()
{
 if (!jQuery().bootstrapWizard){return; }

            function format(state){
              if (!state.id) return state.text; // optgroup
              return "State:" + state.id.toLowerCase() + "'" + state.text;
            }

            var form = $('#registration_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

			function xformvalidation(indx){
			 var result = true;
			 var pos    = false;
			     indx   = ((indx>=2)?(indx+1):indx);
			 $('#tab'+indx).find('[required]').each(function(){
			   var elemval='';
			   if($(this).is('input[type="text"]') || $(this).is('input[type="date"]') || $(this).is('select'))
				elemval = $(this).val();
			   else
				elemval = $(this).html();

			   $(this).closest('.form-group').removeClass('has-success');
			   $(this).closest('.form-group').removeClass('has-error');
			   $(this).closest('.form-group').find('.help-block').html('');
			   //alert(elemval);
			   if(elemval=='' || elemval==undefined || elemval==null || elemval=='-1' || elemval==-1){
				$(this).closest('.form-group').addClass('has-error');
				$(this).closest('.form-group').find('.help-block').html('This field is required');
				
				if($(this).closest('.tab-pane').is('#tab_father')){
				   if(pos==false){
				    $('#tab2 .nav-tabs').find('.active').removeClass('active');
				    $('#tab2 .tab-content').find('.active').removeClass('active');
				    $('#tab2 .tab-content').find('.in').removeClass('in');
				    $('[href="#tab_father"]').closest('li').addClass('active');
				    $('#tab_father').addClass('active in');
				    $(this).focus();
				    pos=true;
				   }
				   msgbox('error','Father information is incomplete');
				}else if($(this).closest('.tab-pane').is('#tab_mother')){
				   if(pos==false){
				    $('#tab2 .nav-tabs').find('.active').removeClass('active');
				    $('#tab2 .tab-content').find('.active').removeClass('active');
				    $('#tab2 .tab-content').find('.in').removeClass('in');
				    $('[href="#tab_mother"]').closest('li').addClass('active');
				    $('#tab_mother').addClass('active in');
				    $(this).focus();
				    pos=true;
				   }
				   msgbox('error','Mother information is incomplete');
				}else if($(this).closest('.tab-pane').is('#tab_emergency')){
				   if(pos==false){
				    $('#tab2 .nav-tabs').find('.active').removeClass('active');
				    $('#tab2 .tab-content').find('.active').removeClass('active');
				    $('#tab2 .tab-content').find('.in').removeClass('in');
				    $('[href="#tab_emergency"]').closest('li').addClass('active');
				    $('#tab_emergency').addClass('active in');
				    $(this).focus();
				    pos=true;
				   }
				   msgbox('error','Emergency information is incomplete');
				}
				
				result = false;
			   }else
				$(this).closest('.form-group').addClass('has-success');
			 });
			 switch(indx)
			 {
			   case 1:
			    if($('#studlvl').attr('data-id')=='' || $('#studlvl').attr('data-id')==undefined)
				{
				  $('#studlvl').closest('.form-group').addClass('has-error');
				  $('#studlvl').closest('.form-group').find('.help-block').html('This field is required');
				  result = false;
				}
				else
                  $('#studlvl').closest('.form-group').addClass('has-success');

			   break;
			  /* 
			  case 2:
			    if($('#datapriv').prop('checked')==false){
				  msgbox('error','You need to agree to the data privacy policy');
				  result = false;
				}
               break;
			  */ 
			 }

             return result;
			}

            var displayConfirm = function() {
                $('.form-control-static', form).each(function(){
				   var elem  = $(this).attr('data-display');
				   var value = '';
				   if(elem == 'xinstruction')
				   {
					 var pmethod   = $('#paymethod').val();
					 var instruct  = $('#paymethod').find('[value="'+pmethod+'"]').attr('data-instruct');
					 if(instruct!='' && instruct!=undefined && instruct!=null)
					 {
					   value = '<label class="control-label col-md-5">Instruction:</label><label class="col-md-7"><p class="form-control-static font-sm">'+instruct+'</p></label>';
					 }
				   }
				   else if($(elem).is('input'))
					 value = $(elem).val();
				   else if($(elem).is('select'))
				   {
				    tmp_val = $(elem).val();
					value = $(elem).find('[value="'+tmp_val+'"]').html();
				   }
				   else if(elem!==undefined)
					 value = $(elem).html();
				   else
					 value = $(this).html();

                   $(this).html(value);
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#modal_register')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#modal_register')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#modal_register').find('.button-previous').hide();
                } else {
	                if(current==2 && xselect==false){
					  xselect = $('.select2').select2({dropdownParent:$('#modal_register')});
					}
                    $('#modal_register').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#modal_register').find('.button-next').hide();
                    $('#modal_register').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#modal_register').find('.button-next').show();
                    $('#modal_register').find('.button-submit').hide();
                }
                Metronic.scrollTo($('.page-title'));
            }


			// default form wizard
            $('#modal_register').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;

                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (xformvalidation(index) == false){return false; }
                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
				onFirst: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = 1;
                    var $percent = (current / total) * 100;
                    $('#modal_register').find('.progress-bar').css({width: $percent + '%'});
					handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#modal_register').find('.progress-bar').css({width: $percent + '%'});
					handleTitle(tab, navigation, index);
                }
            });

			$('#modal_register').find('.button-previous').hide();
			$('#modal_register').find('.button-submit').hide();
}

function load_familyinfo()
{
 $('.guardinfo').each(function(){
	var id     = $(this).attr('id');
	var target = id.replace('x','#');
    var value  = $(this).val();
    $(target).val(value)
 });
}
function update_familyinfo()
{
 $('.guarddata').each(function(){
	var id     = $(this).attr('id');
	var target = '#x'+id;
    var value  = $(this).val();
    $(target).val(value)
 });
}


function xparseFloat(int_str)
{
 if(int_str==undefined){return 0;}
 int_str = int_str.replace(/,/g,'');
 int_str = int_str.replace(/ /g,'');
 var output = parseFloat(int_str);
 return ((output!=undefined && output!=NaN)?output:0);
}
