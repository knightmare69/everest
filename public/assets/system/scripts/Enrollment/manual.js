function savedetails(regdt){
    progressBar('on');
    setAjaxRequest(
		base_url+'manual-class/txn',{event:'save',regdata:regdt},
		function(r){
		  if(r.success){
			$('.btnrefresh').trigger('click');
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save data!',function(){return;});	
		  progressBar('off');
		}
	);
}

function regdetails(regid){
    progressBar('on');
    setAjaxRequest(
		base_url+'manual-class/txn',{event:'regdetail',regid:regid},
		function(r){
		  if(r.success){
			$('#regdetail').find('tbody').html(r.content);
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
}

$(document).ready(function(){
  $('body').on('keypress','#studname,#txt-modal-search',function(e){
    //console.log('testing:'+e.keyCode);
	  if(e.keyCode==13){
		var tmpdv = $(this).closest('.input-group');
		$(tmpdv).find('[data-source]').trigger('click');
	  }
  });
  
  $('body').on('click','.btnfilter',function(){
	 var src_type  = $(this).data('source');
	 var src_param = $(this).data('parameter');
	 var dest_xid  = $(this).data('id');
	 var param     = $(src_param).val();
     $('#modal_filter').modal('show');
	 $('#modal_filter').attr('data-type',src_type);
	 switch(src_type){
		case 'student': $('#txt-modal-search').attr('placeholder','StudentNo,LastName,Firstname'); 
		case 'scholarship': $('#txt-modal-search').attr('placeholder','Code,Provider'); 
	 }
	 $('#txt-modal-search').val(param);
	 $('.btn-modal-search').trigger('click');
  });
  
  $('body').on('click','.btn-modal-search',function(){
	var src_type  = $('#modal_filter').attr('data-type');
	var src_param = $(this).data('source');
	var param     = $(src_param).val();
	var term      = $('#term').val();
	    term      = ((term==undefined || term==-1)?1:term);
	progressBar('on');
    setAjaxRequest(
		base_url+'manual-class/txn',{event:'filter',type:src_type,args:param,term:term},
		function(r){
		  if(r.success){
			$('.modal_list').html(r.content);
			if(src_type=='subjects' || src_type=='changesubj'){
			 $('#regdetail').find('tr[data-sched]').each(function(){
			    var tmpid = $(this).attr('data-sched');
				$('.modal_list').find('[data-list="'+tmpid+'"]').addClass('hidden');
			 });
			 
			 $('#regdetail').find('tr[data-targetsubj]').each(function(){
			    var tmpid = $(this).attr('data-targetsubj');
				$('.modal_list').find('[data-list="'+tmpid+'"]').addClass('hidden');
			 });
			}
		  }
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		}
	);
  });
  
  $('body').on('dblclick','.modal_list tr[data-id]',function(){
	$('.btn-modal-select').trigger('click');
  });
  
  $('body').on('click','.btn-modal-select',function(){
	var selrow = $('.modal_list').find('.info');
	var ftype  = $('#modal_filter').attr('data-type');
	
	if($(selrow).length<=0){
		return false;
	}
	
	switch(ftype){
	  case 'changesubj':
	    var trgt   = $('#modal_filter').attr('data-target');
		var trrow  = $('#regdetail').find('tbody').find('[data-sched="'+trgt+'"]');
	    var tmplt  = $('#regdetail').find('[data-regid="tmp"]').clone();
		var regid  = $('#regid').val();
	    var schid  = $(selrow).data('list');
	    var subid  = $(selrow).data('subject');
	    var acadu  = $(selrow).data('acadunit');
	    var labu   = $(selrow).data('labunit');
		$(trrow).attr('data-targetsubj',schid);
		$(trrow).find('td:nth-child(7)').html($(selrow).find('td:nth-child(1)').html());
		$(trrow).find('td:nth-child(8)').html($(selrow).find('td:nth-child(2)').html());
		$(trrow).find('td:nth-child(9)').html($(selrow).find('td:nth-child(3)').html());
		$(trrow).addClass('warning');
	  break;	
	  case 'subjects':
	    var tmplt  = $('#regdetail').find('[data-regid="tmp"]').clone();
		var regid  = $('#regid').val();
	    var schid  = $(selrow).data('list');
	    var subid  = $(selrow).data('subject');
	    var acadu  = $(selrow).data('acadunit');
	    var labu   = $(selrow).data('labunit');
			tmplt.find('td:nth-child(2)').html($(selrow).find('td:nth-child(1)').html());
			tmplt.find('td:nth-child(3)').html($(selrow).find('td:nth-child(2)').html());
			tmplt.find('td:nth-child(4)').html($(selrow).find('td:nth-child(3)').html());
			tmplt.find('td:nth-child(5)').html(acadu);
			tmplt.find('td:nth-child(6)').html(labu);
			tmplt.attr('data-sched',schid);
			tmplt.attr('data-targetsubj',schid);
			tmplt.attr('data-subj',subid);
			tmplt.attr('data-regid',regid);
			tmplt.addClass('warning');
		$('#regdetail').find('tbody').append(tmplt);
		$('#regdetail').find('tbody').find('[data-regid="tmp"]').attr('data-regid',regid);
	  break;
	  case 'student':
	    var regid  = $(selrow).data('id');
	    var regdt  = $(selrow).data('regdate');
	    var studno = $(selrow).data('list');
	    var lname  = $(selrow).find('td:nth-child(2)').html();
	    var fname  = $(selrow).find('td:nth-child(3)').html();
	    var mname  = $(selrow).find('td:nth-child(4)').html();
	    var progn  = $(selrow).find('td:nth-child(6)').html();
	    var level  = $(selrow).find('td:nth-child(7)').html();
		$('#regid').val(regid);
		$('#regdate').val(regdt);
		$('#studname').val(lname+', '+fname);
		$('.studno').html(studno);
		$('.yrlvl').html(level);
		$('.program').html(progn);
		regdetails(regid);
	  break;
	}
    $('#modal_filter').modal('hide');
  });
  
  $('body').on('click','.btnadd',function(){
	 $('#modal_filter').attr('data-type','subjects');
	 $('#txt-modal-search').val('');
	 $('.btn-modal-search').trigger('click');
     $('#modal_filter').modal('show');
  });
  
  $('body').on('click','.btnsubjdel',function(){
	if($(this).closest('tr').is('.warning') && $(this).closest('tr').attr('data-sched')==$(this).closest('tr').attr('data-targetsubj')){
	  $(this).closest('tr').remove();
	}else{
      $(this).closest('tr').addClass('danger');
	}
  });
  
  $('body').on('click','.btnsubjedt',function(){
     var tmpid = $(this).closest('tr').attr('data-sched');
	 $('#modal_filter').attr('data-target',tmpid);
	 $('#modal_filter').attr('data-type','changesubj');
	 $('#txt-modal-search').val('');
	 $('.btn-modal-search').trigger('click');
     $('#modal_filter').modal('show');
  });
  
  $('body').on('click','.btnrefresh',function(){
    var regid = $('#regid').val();
	if(regid!=undefined && regid!=''){
	  regdetails(regid);
	}
  });
  
  $('body').on('click','.btnsave',function(){
    var tmpdata = '';
	var tbldata = $('#regdetail').find('tbody');
	var count   = $(tbldata).find('[data-regid!="tmp"]').length;
	$(tbldata).find('tr').each(function(){
	  var regid  = $(this).attr('data-regid');
	  var sched  = $(this).attr('data-sched');
	  var trgtid = $(this).attr('data-targetsubj');
	  if($(this).is('.danger')){
		tmpdata += ((tmpdata!='')?',':'')+'d:'+regid+':'+sched+':'+sched;
	  }else if($(this).is('.warning') && $(this).attr('data-sched')==$(this).attr('data-targetsubj')){
		tmpdata += ((tmpdata!='')?',':'')+'a:'+regid+':'+sched+':'+sched;
	  }else if($(this).is('.warning') && $(this).attr('data-sched')!=$(this).attr('data-targetsubj')){
		tmpdata += ((tmpdata!='')?',':'')+'e:'+regid+':'+sched+':'+trgtid;
	  }
	  console.log('run');
	});
	savedetails(tmpdata);
  });
  
  $('body').on('click','tr[data-id]',function(){
	$('.modal_list').find('tr[data-id]').removeClass('info');
	$(this).addClass('info');
  });
  
});