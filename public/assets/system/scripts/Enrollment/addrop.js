var MOD = {
    init: function(){

        var t = localStorage.TermID;
        var term = getParameterByName('t');

        if(t != undefined && term == undefined){
           location.replace(base_url+page_url+'?t='+ t);
        }
        this.load();
    },
    add : function(){ this.loadStudents(); },
    closemenu: function(){ $('#mouse_menu_right').hide(); },
    initEval : function(){ $('#evaluation').show(); },
    cancelThis: function(e){
        
        var $r = $(e.target).closest('tr');
        
        switch( $r.attr('data-tag') ){
            case '1': $r.remove(); break;
            case '2': // Change Subject
                $r.attr('data-dirty',0);
                $r.find('td.action').html('<a href="javascript:void(0);" class="options" data-menu="change" ><i class="fa fa-retweet"></i> Change</a>');
                $r.find('td.status').html("Regular Subject");
                $r.removeClass('danger');                                
            break;
            case '0': 
                
                $r.attr('data-dirty',0);
                $r.find('td.action').html('<a href="javascript:void(0);" class="options" data-menu="change" ><i class="fa fa-retweet"></i> Change</a>');
                $r.find('td.status').html("Regular Subject");
                $r.removeClass('danger'); 
            break;
        }
         
    },
    load : function(){

      $('body').on('click', '.btn, .options', $.proxy(this.menu, this) );
      $('body').on('click','.views', $.proxy(this.help,this));
      $('body').on('change', '#term', function(e){
        localStorage.TermID =  $('#term').val();
        location.replace(base_url+page_url+'?t='+ $('#term').val());
      });
      $('body').on('keypress','#searchkey', function(e){
        if(e.keyCode ==  13){ MOD.searchOnEnter(); }
      });

      $('body').on('mousedown','#tblgrades tbody td', function(e){
         MOD.closemenu();
         if(e.button == 2){
              MOD.initrclick(e);                  
         }
      });
    },

    help: function(e){

        $('.profile-usermenu').find('li').removeClass('active');
        $(e.target).parent().addClass('active');

        $('#advising').hide();
        $('#evaluation').hide();

        var data = $(e.target).data();
        switch (data.menu) {
            case 'addrop': $('#advising').show(); break;
            case 'history': this.initEval(); break;
        }
    },

    reload : function(e){
        var m = $('.profile-usermenu').find('li.active').find('a').attr('data-menu');
        console.log(m);
        switch(m){
            case 'addrop': break;
            case 'evaluation': this.initEval(); break;
        }

    },

    menu: function(e){
        var d = $(e.target).data();
        switch(d.menu){
            case 'add': this.add(e); break;
            case 'edit': this.edit(e); break;
            case 'advised': this.advisedThis(e); break;
            case 'remove': this.deleteThis(e); break;
            case 'dropsubj': this.dropThis(e); break;
            case 'addsubj': this.schedule(e); break;
            case 'addselect': this.addSubject(e); break;
            case 'change': this.changeSubj(e); break;
            case 'save': this.saveThis(e); break;
            case 'print': this.print(e); break;
            case 'print2': this.print2(e); break;
            case 'reload': this.reload(e); break;
            case 'cancel-menu': this.closemenu(); break;
            case 'cancel-txn': this.cancelThis(e); break;
        }
    },
    dropThis: function(){
       var _this = this;
        var selected = $('#tblregistered tbody').find('tr.active-row');

        if (selected != undefined ) {
          $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Drop selected subject/s? </h2>",
    			content : "Do you want to drop the selected subject/s? Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    selected.attr('data-dirty',1);
                    selected.attr('data-tag',3);
                    selected.find('td.action').html('<a href="javascript:void(0);" class="options" data-menu="cancel-txn" ><i class="fa fa-times"></i> Cancel</a>');
                    selected.find('td.status').html("Drop Subject");
                    selected.addClass('danger');
    			}
    		});
        }else{
            alert('Please select subject to drop');
        }
        
    },
    addSubject: function(){
       var $r = $('#tbloffered').find('tr.active-row');
       var $reg = $('#tblregistered tfoot tr').clone();
       var exist = false;
       $('#tblregistered tbody tr').each(function(){
            if($(this).attr('data-sched') == $r.attr('data-id') ){
                msgbox('warning','Subject already exist in the registered subject/s');
                exist = true;
            }
       });
       if(!exist){              
           $reg.attr('data-sched', $r.attr('data-id'));
           $reg.attr('data-tag',1);
           $reg.attr('data-dirty',1);
           $reg.attr('data-unit',$r.attr('data-unit'));
           
           $reg.find('.code').html($r.attr('data-code'));
           $reg.find('.name').html($r.attr('data-name'));
           $reg.find('.unit').html($r.attr('data-unit'));
           $reg.find('.sect').html($r.attr('data-section'));
           $reg.find('.sched').html($r.attr('data-sched'));
           $reg.find('.room').html($r.attr('data-room'));
           $reg.find('.faculty').html($r.attr('data-faculty'));
           $reg.find('.stat').html('Add Subject');
           $('#tblregistered tbody').append($reg);
           $('#sched_modal').modal('hide');
       }
       
    },

    edit : function(e){
        var $r = $(e.target).closest('tr'); 
       if( $r.attr('data-idno') == undefined){
            msgbox('warning','No Selected student');
            return false;
       }
       location.replace(base_url+page_url+'/edit?t='+ $('#term').val()+'&idno='+ $r.attr('data-idno') + '&ref='+ $r.attr('data-id') );
    },

    advisedThis : function(){

       var id =  $('#table-students-res').find('tr.active-row').attr('data-id');
       if( id == undefined){
            msgbox('warning','No Selected student');
            return false;
       }
       location.replace(base_url+page_url+'/new?t='+ $('#term').val()+'&idno='+ id );
    },

    searchOnEnter: function(){

       var find = $('#searchkey').val();

       setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students', 'term': $('#term').val(), 'find' : find }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#event_modal .scrollbox').html(r.data);
            }
        },undefined,'json',true,false);
        progressBar('off');

    },

    getSelected: function(){
        var ids = [];

        $('#records-table tbody tr').each(function(){
           if( $(this).find('.chk-child').prop('checked') ) {
                var d = {
                    ref : $(this).attr('data-id'),
                    idno : $(this).attr('data-idno'),
                    prog : $(this).find('.level').find('option:selected').attr('data-prog'),
                    major : $(this).find('.track').find('option:selected').val(),
                    level : $(this).find('.level').val(),
                };
                ids.push(d);
           }
        });
        return ids;
    },

    saveThis: function(){

       var subj = [];

       $('#tblregistered tbody tr').each(function(){
            if($(this).attr('data-dirty') == '1' ){
                var id = {
                    id : $(this).attr('data-sched')
                    ,tag : $(this).attr('data-tag')
                    ,change : 0
                    ,unit : $(this).attr('data-unit')
                };
                    
                if(id != 0 && id != undefined){
                   subj.push(id);
                }
            }
       });

        $.SmartMessageBox({
        	title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Save Add/Drop/Change Transaction? </h2>",
        	content : "Do you want to save the add/drop/change subject/s? Click [YES] to proceed...",
        	buttons : '[No][Yes]'
        }, function(ButtonPressed) {
        	if (ButtonPressed === "Yes"){
        	     var data = { term: $('#term').val(), idno : getParameterByName('idno'), ref: getParameterByName('ref') , subj : subj };
                setAjaxRequest( base_url + page_url + '/event?event=save', data ,
                    function(r) {
                        if (r.error) {
                            showError( '<i class="fa fa-times"></i> ' +  r.message);
                        } else {                            
                            $('#btnprint').attr('data-reg', r.reg);                            
                            $.SmartMessageBox({ title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Successfully Registered! </h2>",
                        			content : "Do you want to print registration form? Click [YES] to proceed...",
                        			buttons : '[No][Print]'
                    		}, function(ButtonPressed) {
                    			if (ButtonPressed === "Print"){
                                    MOD.print();
                    			}
                    		});                                
                        }

                        progressBar('off');
                    }
                );
        	}
        });
    },

    loadStudents : function(){
      var s = getParameterByName('s');
      var p = $('#period').val();

        setAjaxRequest( base_url + page_url + '/event', {'event': 'get-students', 'term': $('#term').val() }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
                //sysfnc.showAlert('#grdSchedules','danger', r.message,true,true,true,'warning');
            }else{
                $('#event_modal .scrollbox').html(r.data);
                $('#event_modal').modal('show');
            }
        },undefined,'json');
        progressBar('off');
    },

    deleteThis : function(e){
        
        var _this = this;
        var selecteds = _this.getSelected();

        if (selecteds.length > 0 ) {
          $.SmartMessageBox({
    			title : "<h2 class='text-warning'><i class='fa fa-warning fa-lg fa-fw '></i> Remove selected ADC tranction? </h2>",
    			content : "Do you want to cancel the selected add/drop/change transaction? Click [YES] to proceed...",
    			buttons : '[No][Yes]'
    		}, function(ButtonPressed) {
    			if (ButtonPressed === "Yes"){
                    setAjaxRequest( base_url + page_url + '/event?event=remove' , { idno : selecteds } , function(r) {
                    if(r.error) {
                        showError( '<i class="fa fa-times"></i> ' +  r.message);                                                        
                    }else{
                        sysfnc.msg('Successfully cancelled!','add/drop/change transaction successfully cancelled');
                        location.reload();
                    }
                    progressBar('off');
                    });
    			}
    		});
        }else{
            alert('Please select student to delete');
        }
    },

   	schedule: function(e){

		var termid = $('#term').val();
        var rid = $(e.target).closest('tr').attr('data-id');
        var pid = $('#student-name').attr('data-prog');
        
        setAjaxRequest( base_url + page_url + '/event' ,{event:'schedule',termid:termid,progid:pid,regid:rid}
			   ,function(r)
				{
				  if(r.error)
				  {
			        msgbox('danger',r.error);
			      }
                  else
                  {
					 $('#schedulebox').html(r.content);
		             $('#sched_modal').modal('show');
					 Metronic.initUniform();
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
				});
        progressBar('off');

	},

    print : function(){
        var id = $('#btnprint').attr('data-reg');

        if(id == 0 || id == undefined){
            alert('no registration found!');
            return false;
        }
        window.open(base_url+'add-drop-change/print?ref='+getParameterByName('ref')+'&t='+ Math.random() ,'_blank');
	 },
    print2 : function(e){
       var id = $('#records-table tbody').find('tr.active-row').attr('data-id');
        window.open(base_url+'add-drop-change/print?ref='+id+'&t='+ Math.random() ,'_blank');
	 },
      
    initrclick: function(e){
        var pos = $(e.target).position();
        var cs = {top: pos.top, left: pos.left}

        $( "#mouse_menu_right" ).css(cs);
        $('#mouse_menu_right').show('50');
    },
    
    changeSubj: function(e){
       var termid = $('#term').val();
        var rid = $(e.target).closest('tr').attr('data-id');
        var pid = $('#student-name').attr('data-prog');
        
        setAjaxRequest( base_url + page_url + '/event' ,{event:'schedule',termid:termid,progid:pid,regid:rid}
			   ,function(r)
				{
				  if(r.error)
				  {
			        msgbox('danger',r.error);
			      }
                  else
                  {
					 $('#schedulebox').html(r.content);
		             $('#sched_modal').modal('show');
					 Metronic.initUniform();
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});
				});
        progressBar('off');
       
    },
};