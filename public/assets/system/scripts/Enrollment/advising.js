function get_total(){
	$('.totali').html($('#adv_details tbody').find('tr[data-subj!="0"]').length);
	$('.totalu').html(0);
}

function get_advising(){
  var termid = $('#ayterm').val();
  var studno = $('#studno').val();
  var yrlvl  = $('#yrlvlid').val();
  var strand = $('#strand').val();
  var curr   = $('#curriculum').val();
  $('#adv_details').find('tbody').html('<tr><td colspan="5"></td></tr>');
  $('.btnregister').addClass('hidden');	 
  $('.btnprintg').addClass('hidden');	 
  setAjaxRequest(
				base_url+'enrollment/actions?event=studadv'
			   ,{termid:termid,studno:studno,strand:strand,curr:curr}
			   ,function(r)
				{
				  if(r.error)
				  { 
			        msgbox('danger',r.error);					
			      }else{
					 var xclone  = $('#tmpcurr').find("option[value!='-1']").clone();
		             $('#regid').val(r.content.RegID);
					 $('#lname').html(r.content.LastName);
                     $('#fname').html(r.content.FirstName);
                     $('#mname').html(r.content.MiddleName);
                     $('#yrlvlid').val(r.content.YearLevelID);
                     $('#strand').val(r.content.MajorDiscID);
					 $('#strand').trigger('change');
					 $('#curriculum').val(r.content.CurriculumID);
					 if($('#curriculum').find('option[value="'+r.content.CurriculumID+'"]').length>0){
					    $('#min').html($('#curriculum').find('option[value="'+r.content.CurriculumID+'"]').data('min'));
                        $('#max').html($('#curriculum').find('option[value="'+r.content.CurriculumID+'"]').data('max'));
                     }else{	
                        $('#min').html(r.content.Min);
                        $('#max').html(r.content.Max);
                     }
					
					
					$('#adv_details').find('tbody').html(r.advise);
					if($('#regid').val()!='' && $('#regid').val()!='0'){
					  $('.btnregister').addClass('hidden');	 
				      $('#adv_details').find('.btnremove').addClass('disabled');
				      $('#adv_details').find('.btnsched').addClass('disabled');
					  $('.btnprintg').removeClass('hidden');	 
					}else{
					  if($('#adv_details tbody').find('[data-subj]').length>0){
					    $('.btnregister').removeClass('hidden');	
				        $('#adv_details').find('.btnremove').removeClass('disabled');	
				        $('#adv_details').find('.btnsched').removeClass('disabled');	
					  }else{
					    $('.btnregister').addClass('hidden');		
				        $('#adv_details').find('.btnremove').addClass('disabled');	 
				        $('#adv_details').find('.btnsched').addClass('disabled');	 
					  }
					  $('.btnprintg').removeClass('hidden');	 
					}
					get_total();
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				});	
        progressBar('off');
}

$(document).ready(function(){
	$('.btnprintg').addClass('hidden');
	$('.btnregister').addClass('hidden');
	$('.tmpcurr').find('#curriculum').attr('id','tmpcurr');
	
	$('body').on('click','.btnselect',function(){
		var tmpval = $('#tblsearch').find('.info').attr('data-value');
		if(tmpval == 'undefined' || tmpval==''){return false;}
		$('#studno').val(tmpval);
		$('.btnsearch').click();
		$('#modal_search').modal('hide');
	});
		
	$('body').on('keypress','#search',function(e){
	   if(e.keyCode==13){	
		$('.btnfind').click();
	   }	
	});
	
	$('body').on('click','.btnfind',function(){
	  var param = $('#search').val();
	  if(param==undefined || param==''){return false;}
      progressBar('on');
	  setAjaxRequest(
		base_url+'enrollment/actions?event=search'
	   ,{param:param}
	   ,function(r)
		{
		 var tmpdata = $('#tblsearch').find('tfoot').html();	
		 if(r.success)
	       $('#tblsearch').find('tbody').html(r.content);
		 else
	       $('#tblsearch').find('tbody').html(tmpdata);
		 
		 progressBar('off');
		}
	   ,function(err)
		{
		  var tmpdata = $('#tblsearch').find('tfoot').html();	
	      $('#tblsearch').find('tbody').html(tmpdata);
          progressBar('off');
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		});	
	});
	
	$('body').on('click','#tblsearch .tddata',function(){
	    $('#tblsearch').find('.tddata').removeClass('info');	
	    $(this).addClass('info');	
	});
	
	$('body').on('focus','#studno',function(){
		var trdefault = $('#tblsearch').find('tfoot').html();
		$('#search').val('');
		$('#tblsearch').find('tbody').html(trdefault);
		$('#modal_search').modal('show');
	});
	$('body').on('validate','#studno',function(){
		$('.btnsearch').click();
	});
	$('body').on('keypress','#studno',function(e){
	   if(e.keyCode==13){	
		$('.btnsearch').click();
	   }	
	});
	$('body').on('click','.btnsearch',function(){
		var studno = $('#studno').val();
		if(studno=='' || studno==undefined){return false;}
	    $('#lname').html('');
	    $('#fname').html('');
	    $('#mname').html('');
	    $('#yrlvlid').val('5');
	    $('#strand').val('-1');
	    $('#curriculum').val('-2');
		get_advising();
	});
	
	$('body').on('change','#strand',function(){
		var strand = $(this).val();
		var xclone  = $('#tmpcurr').find('[data-majorid="'+strand+'"]').clone();
		$('#curriculum').find("option[value!='-1']").remove();
		$('#curriculum').append(xclone);
	});
	
	$('body').on('click','.btn-refresh',function(){
		get_advising();
	});
	
	$('body').on('click','#sched_details [type="checkbox"]',function(){
	   $('#sched_details').find('[type="checkbox"]').closest('span').removeClass('checked');	
	   $('#sched_details').find('[type="checkbox"]').removeAttr('checked');	
	   $(this).closest('span').addClass('checked');	
	   $(this).prop('checked',true);
	});
	
	$('body').on('click','.btnsched',function(){
		var termid = $('#ayterm').val();
        var subjid = $(this).closest('tr').data('subj');
        setAjaxRequest(
				base_url+'enrollment/actions?event=schedule'
			   ,{termid:termid,progid:29,subjid:subjid}
			   ,function(r)
				{
				  if(r.error)
				  { 
			        msgbox('danger',r.error);					
			      }else{
					 $('#selsubj').val(subjid);
					 $('#sched_details').find('tbody').html(r.content);
		             $('#modal_schedule').modal('show');
					 Metronic.initUniform();
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				});	
        progressBar('off');
	});
	$('body').on('click','.btnremove',function(){
		var isblock = 0;
		var termid  = $('#ayterm').val();
		var studno  = $('#studno').val();
        var yrlvl   = $('#yrlvlid').val();
        var strand  = $('#strand').val();
        var curr    = $('#curriculum').val();
		var subjid  = $(this).closest('tr').attr('data-subj');
		var sched   = 0;
		var sect    = 0;
		if(sched==undefined || sect==undefined || subjid==undefined){return false;}
		setAjaxRequest(
				base_url+'enrollment/actions?event=setschedule'
			   ,{termid:termid,studno:studno,progid:29,strand:strand,curr:curr,yrlvl:yrlvl,subjid:subjid,sched:sched,sect:sect,block:isblock}
			   ,function(r)
				{
				  if(r.error)
				   msgbox('danger',r.error);					
			      else{
					 $('#adv_details').find('[data-subj="'+subjid+'"]').find('.tdsection').html('');
					 $('#adv_details').find('[data-subj="'+subjid+'"]').find('.tdschedule').html('');
					 $('#adv_details').find('[data-subj="'+subjid+'"]').attr('data-sched',sched);
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				});	
        progressBar('off');
	});
	
	$('body').on('click','.btnsetsched',function(){
		var isblock = (($('#block').is(':checked'))?1:0);
		var termid  = $('#ayterm').val();
		var studno  = $('#studno').val();
        var yrlvl   = $('#yrlvlid').val();
        var strand  = $('#strand').val();
        var curr    = $('#curriculum').val();
		var subjid  = $('#selsubj').val();
		var sched   = $('#sched_details').find('.checked').closest('tr').data('id');
		var sect    = $('#sched_details').find('.checked').closest('tr').data('section');
		if(sched==undefined || sect==undefined || subjid==undefined){return false;}
		setAjaxRequest(
				base_url+'enrollment/actions?event=setschedule'
			   ,{termid:termid,studno:studno,progid:29,strand:strand,curr:curr,yrlvl:yrlvl,subjid:subjid,sched:sched,sect:sect,block:isblock}
			   ,function(r)
				{
				  if(r.error)
				   msgbox('danger',r.error);					
			      else{
					 for(var i = 0;i<r.content.length;i++){
						 var rowid  = r.content[i].SubjectID;
						 var detail = r.content[i];
						 $('#'+rowid).attr('data-sched',detail.ScheduleID);
						 $('#'+rowid).find('.tdsection').html(detail.SectionName);
						 $('#'+rowid).find('.tdschedule').html(detail.Sched1);
					 } 
					 $('#modal_schedule').modal('hide');
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				});	
        progressBar('off');
	});
	
	$('body').on('click','.btnregister',function(){
		var isblock = (($('#block').is(':checked'))?1:0);
		var termid  = $('#ayterm').val();
		var studno  = $('#studno').val();
        var yrlvl   = $('#yrlvlid').val();
        var strand  = $('#strand').val();
        var curr    = $('#curriculum').val();
		if(termid==undefined || studno==undefined || curr==undefined){return false;}
		if($('#adv_details').find('[data-subj]').length == $('#adv_details').find('[data-sched="0"]').length){
		  msgbox('danger','No schedule to enroll');
		  return false;
		}else if($('#adv_details').find('[data-sched="0"]').length > 0 && $('#adv_details').find('[data-subj]').length < $('#adv_details').find('[data-sched="0"]').length){
		  msgbox('danger','Too many subjects without schedule');		
		  return false;
		}else if($('#adv_details').find('[data-sched="0"]').length > 0 && $('#adv_details').find('[data-subj]').length > $('#adv_details').find('[data-sched="0"]').length){
		  msgbox('danger','Subjects without schedule will not be saved');		
		}
		
		setAjaxRequest(
				base_url+'enrollment/actions?event=advregister'
			   ,{termid:termid,studno:studno,progid:29,strand:strand,curr:curr,yrlvl:yrlvl}
			   ,function(r)
				{
				  if(r.error)
				   msgbox('danger',r.error);					
			      else{
				   msgbox('success','Successfully Registerd');					
			      }
				}
               ,function(err)
                {
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to register!',function(){return;});	
				});	
        progressBar('off');
	});
	
	 $('body').on('click','.btn-print',function(){
	   var term    = $('#ayterm').val();
	   var campus  = $('#campus').val();
	   var studno  = $('#studno').val();
	   var regid   = $('#regid').val();
	   var yrlvl   = $('#yrlvlid').val();
	   var progid  = 29; 
	   var major   = $('#strand').val();
	   var feeid   = '';//$('.chk-registered:checked').closest('span').attr('data-feeid'); 
	   var event   = ((feeid=='')?'assessment':'assessmentx');
	   var arrdata = '';
	   if(regid!=0 && regid!='' && regid!=undefined){
	     window.open(base_url+'enrollment/print_report?event='+event+'&term='+term+'&campus='+campus+
	                                                 ((regid!=undefined && regid!='')?'&regid='+regid:'')+
			   									     ((studno!=undefined && studno!='')?'&studno='+studno:'')+
												     ((yrlvl!=undefined && yrlvl!='')?'&yrlvl='+yrlvl:'')+
												     ((major!=undefined && major!='')?'&major='+major:'')+
												     ((progid!=undefined && progid!='')?'&progid='+progid:'')+
												     ((arrdata!='')?'&'+arrdata:'')+'&t='+ Math.random() ,'_blank');		 
	   }else{
		  alert('Nothing to print');
	   }				
	 });	 		
});