$('body').ready(function(){
   $('body').on('click','.btn-refresh',function(){
	  var check = $('#student').val();
	  if(check!=-1){
		 $('#student').trigger('change'); 
	  } 
   });	
   
   $('body').on('click','.btn-submit,.btn-unlock',function(){
   	 var _this = $(this);
	 var schedule = $('#schedule').html();
	 var studno   = $('#student').val();
	 var option   = (($(this).is('.btn-submit'))?1:0);
	 var pdata    = 'sched='+schedule+'&studno='+studno+'&option='+option;
	 /*
	 if($('[data-start=""]').length>0){
		alert('Please complete the form first before submitting!');
        return false;		
	 }
	 */
	 onProcess(_this,'','Submitting');
	 Metronic.blockUI({target: '#schedule-list', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Please wait while submitting... '});
	 lazyLoad( function() {
 		 ajaxRequestFile(
				 base_url+'scheduling/txn?event=locksched&'+pdata
				,''
				,function(r){
				   if(r.success){	
				       //$('#student').trigger('change'); 
					   alert('You have successfully blocked your schedule, Kindly check your email(Inbox,Spam) for the confirmation. Thank You');
				   }else{
					   alert('Failed to lock reservation!');  
				   }
				   progressBar('off'); 
				   onProcess(_this,'','Done!',false); 
				   Metronic.unblockUI('#schedule-list');
				}
		 );  
 	}); 
   });	
   
   $('body').on('change','#student',function(){
	   var schedule = $('#schedule').html();
	   var regid = $(this).val();
	   var opt   = $(this).find('option[value="'+regid+'"]');
	   var prog  = $(opt).attr('data-program');
	   var yrlvl = $(opt).attr('data-yrlvl');
	   
	   $('#program').html(prog);
	   $('#yrlvl').html(yrlvl);
	   $('.btn-submit').removeClass('hidden');
       $('.btn-unlock').addClass('hidden');
	   progressBar('on');  
	   setAjaxRequest(
			 base_url+'scheduling/txn?event=faculty&sched='+schedule+'&regid='+regid
			,''
			,function(r){
			   if(r.success){	
			     $('#tblschedule').find('tbody').html(r.content);  
				 if($('.btn-schedule').length==0){
					$('.btn-submit').addClass('hidden'); 
					if($('.btn-unlock').length>0){
					  $('.btn-unlock').removeClass('hidden'); 	
					}
				 }
			   }else{
				 $('#tblschedule').find('tbody').html(r.content);  
			   }
			   progressBar('off');  
			}
			,function(err){
				alert('Failed to load schedules');
				progressBar('off');
			}
	   );
   });
   
   $('body').on('click','.slot-data',function(){
	 var schedule = $('#schedule').html();
	 var studno   = $('#student').val();
	 var empid    = $(this).closest('tr').attr('data-id');
	 var subj     = $(this).closest('tr').attr('data-subject');
	 var rdate    = $(this).closest('tr').find('.sched_date').html();
     var txttime  = $(this).find('a').html();
	 var tstart   = $(this).attr('data-start');
	 var tend     = $(this).attr('data-end');
	 var btnctrl  = $(this).closest('.btn-group');
	 var pdata    = 'sched='+schedule+'&studno='+studno+'&faculty='+empid+'&subject='+subj+'&rdate='+rdate+'&start='+tstart+'&end='+tend;
	 $(btnctrl).attr('data-start',tstart);
	 $(btnctrl).attr('data-end',tend);
	 progressBar('on');  
	 setAjaxRequest(
			 base_url+'scheduling/txn?event=savesched&'+pdata
			,''
			,function(r){
			   if(!r.success){	
			       alert('Failed to save reservation! Either your schedule is taken or conflict with other user!');  
	               $(btnctrl).find('.btn-label').html('--:-- -- - --:-- --');
			   }else{
				   $(btnctrl).find('.btn-label').html(txttime);
				   msgbox('success','Successfully Saved');
			   }
			   progressBar('off');  
			}
			,function(err){
			   alert('Failed to save reservation! Either your schedule is taken or conflict with other user!');  
			   progressBar('off');  
			}
	 );  
	 progressBar('off');  
   });
   
   $('body').on('click','.btn-schedule',function(){
	   var sched = $('#schedule').html();
	   var facid = $(this).closest('tr').attr('data-id');
	   var bfacu = $(this).closest('tr');
	   progressBar('on');  
	   $(bfacu).find('.slot-loading').removeClass('hidden');
	   $(bfacu).find('.slot-data').remove();
	   $(bfacu).addClass('initialize');
	   ajaxRequestFile(
			 base_url+'scheduling/txn?event=optsched&sched='+sched+'&faculty='+facid
			,''
			,function(r){
			   if(r.success){	
			     $('.initialize').find('.dropdown-menu').append(r.content);  
				 $('.initialize').find('.slot-loading').addClass('hidden');
				 $('.initialize').removeClass('initialize');
			   }else{
				 alert('Failed to load available schedule');  
				 $(bfacu).find('.slot-loading').removeClass('hidden');
				 $('.initialize').removeClass('initialize');
			   }
			   progressBar('off');  
			}
	   );
   });
   
   $('body').on('change','#event',function(){
	   var sched = $(this).val();
	   if(sched<=0){
		  $('#tbschedule').find('tbody').html('');
		  return false;
	   }
	   progressBar('on');  
	   ajaxRequestFile(
			 base_url+'scheduling/txn?event=fac_schedule&schedule='+sched
			,''
			,function(r){
			   if(r.success){	
			      $('#tbschedule').find('tbody').html(r.content);
			   }else{
				  $('#tbschedule').find('tbody').html('');
				  alert('Failed to load data');
			   }
			   progressBar('off');  
			}
	   );
   });
   
   $('body').on('click','.btn-reload',function(){
	   $('#event').trigger('change');   
   });
   
   $('body').on('click','.btn-fprint',function(){
	   var sched = $('#event').val();
	   window.location.href = base_url+'scheduling/print?schedule='+sched;
   });
});

function alertmsg(container, alert, message, closable, reset, focus,icon, time ) {
	Metronic.alert({
		container: container  , // alerts parent container(by default placed after the page breadcrumbs)
		place: 'prepend', // append or prepent in container
		type: alert,  // alert's type
		message: message,  // alert's message
		close: closable, // make alert closable
		reset: reset, // close all previouse alerts first
		focus: focus, // auto scroll to the alert after shown
		closeInSeconds: time, // auto close after defined seconds
		icon: icon // put icon before the message
	});
}		