var attachfile = []; 
var Calendar   = function() {


    return {
        //main function to initiate the module
        init: function() {
            Calendar.initCalendar();
			//alert('bwisit');
        },
		
        initCalendar: function() {

            if (!jQuery().fullCalendar) {return;}

            var date = new Date();
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var condition = $('[data-condition]').attr('data-condition');
            
            var h = {};
			var f = {};

            if (Metronic.isRTL()) {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
					f = {month:'MMM YYYY',week:'MMDD',day:'MM-DD-YYYY',};
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,month,agendaWeek,agendaDay',
                    };
					f = {month:'MMM YYYY',week:'MMDD',day:'MM-DD-YYYY',};
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        left: 'title',
                        center: 'prev,next,today,',
                        right: 'month,agendaWeek,agendaDay'
                    };
                }
            }

            var initDrag = function(el) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim(el.text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };

            var addEvent = function(title) {
                title = title.length === 0 ? "Untitled Event" : title;
                var html = $('<div class="external-event label label-default">' + title + '</div>');
                jQuery('#event_box').append(html);
                initDrag(html);
            };
             
			var generate_url  = function(){
				   var condition = $('[data-condition]').attr('data-condition');
				   var url       = base_url+'calendar/txn?event=event&condition='+condition;
				   return url;
			};
			
            $('#external-events div.external-event').each(function() {
                initDrag($(this));
            });

            $('#event_add').unbind('click').click(function() {
                var title = $('#event_title').val();
                addEvent(title);
            });

            //predefined events
            $('#event_box').html("");
 
            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                header: h,//{left:'title',center:'prev,next today',right:'month,agendaWeek,agendaDay'},
                titleFormat:f,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 20,
                events: generate_url(),
		        eventAfterRender: function(){
									$('.fc-event[href]').each(function(){
										var xid = $(this).attr('href');
										if(xid!='javascript:void(0);'){
										    $(this).attr('data-id',xid);
										    $(this).attr('href','javascript:void(0);');
										}	
									});	
									$('.fc-time-grid-container').scrollTop(0);
								  },
				dayClick: function(date, jsEvent, view) {
                             var clickdate = date.format();
							     clickdate = clickdate.replace('T','-');
							     partdate  = clickdate.split('-');
							 $('.btn-add').click();
							 $('#startd').val(partdate[1]+'/'+partdate[2]+'/'+partdate[0]);
							 $('#endd').val(partdate[1]+'/'+partdate[2]+'/'+partdate[0]);
				          },
            });
        },
		destroy:function(){
			$('#calendar').fullCalendar('destroy');
		},
		refresh:function(){
			$('#calendar').fullCalendar('refetchEvents');
		},
		reinit:function(){
			$('#calendar').fullCalendar('destroy');
            Calendar.init();
		}

    };

}();

function upload_attachment(xid){
	if(xid==undefined){
	  $('.btn-cancel').trigger('click');
	  Calendar.refresh();
	  progressBar('off');  
	  return false;
	}
	if(attachfile.length>0){
	  $.each(attachfile,function(i,data){
          var xdata = data['xdata'];		  
		  if($('[data-arrid="'+i+'"]').length>0){
			  ajaxRequestFile(
				 base_url+'calendar/txn?event=attachment&xid='+xid
				,xdata
				,function(r){
				  $('.btn-cancel').trigger('click');
				  Calendar.refresh();
				  progressBar('off');  
				}
			  );
          }		  
	  });	
	  $('#tblattachment').find('tbody').html('');
	  attachfile = [];
	}
}

function save_attendee(xid){
  var acount = $('#tblattendee').find('[data-id]').length;
  var uid    = '';
  if(acount>0){
	  $('#tblattendee').find('[data-id]').each(function(){
		   uid = ((uid=='')?'':(uid+'|'))+$(this).attr('data-id');  
	  });
	  ajaxRequestFile(
		 base_url+'calendar/txn?event=attendee&xid='+xid+'&uid='+uid,{}
		,function(r){
		   $('#tblattendee').find('[data-id]').remove();  
		}
	  );
  }
}

$('document').ready(function(){
	var base_condition =  $('[data-condition]').attr('data-condition');
	$('.datepicker').datepicker({
       onSelect: function () {
         $(this).text(this.value);
        }
    });
	
	$('.timepicker').timepicker({
       onSelect: function () {
         $(this).text(this.value);
        }
    });
	
	$('body').on('click','.btn-refresh',function(){
		Calendar.refresh();
	});
	
	$('body').on('click','.schoolview',function(){
		var condition = base_condition;
		var filter    = $(this).attr('data-text');
		if(filter=='Internal Event'){
			condition = condition+'AND NotifyParents=0 ';
		}else if(filter=='School Event'){
			condition = condition+'AND NotifyParents=1 ';
		}
		$('.btnselected').html(filter);
		$('[data-condition]').attr('data-condition',condition);
		Calendar.reinit();
	});
	
	$('body').on('click','[data-view]',function(){
	  if($(this).is('td') || $(this).is('.btn-aview'))
		window.open($(this).closest('tr').attr('data-filename'), '_blank');
      else		  
		window.open($(this).attr('data-filename'), '_blank');
	});
	
	$('body').on('change','#type',function(){
		var typeid   = $(this).val();
		var wperiod  = $(this).find('option[value="'+typeid+'"]').attr('data-period');
		var wroom    = $(this).find('option[value="'+typeid+'"]').attr('data-room');
		var wreserve = $(this).find('option[value="'+typeid+'"]').attr('data-reserve');
		var wattach  = $(this).find('option[value="'+typeid+'"]').attr('data-attach');
		
		$('.dvtype').removeClass('hidden');
		$('.dvnoperiod').removeClass('hidden');
		$('#note').closest('.form-group').addClass('hidden');	
		$('.dvperiod').addClass('hidden');
		$('.dvattach').addClass('hidden');
		$('.dvreserve').addClass('hidden');
		$('.dvattachform').addClass('hidden');
		$('.dvattendee').addClass('hidden');
		$('.dvreserveform').addClass('hidden');
		$('.btn-reserve').addClass('hidden');
		$('.dvattachform').find('tbody').html('');
		$('.dvattendee').find('tbody').html('');
		$('.dvroom').addClass('hidden');
		
		if(typeid==0 || typeid==1){
		   $('.dvnotify').addClass('hidden');
		   $('.dvnotify').find('input[type="checkbox"]').removeAttr('checked');
		   $('.dvnotify').find('input[type="checkbox"]').closest('span').removeClass('checked');
		   $('.dvoption').addClass('hidden');
		   $('.dvoption').find('input[type="checkbox"]').removeAttr('checked');
		   $('.dvoption').find('input[type="checkbox"]').closest('span').removeClass('checked');
		   if(typeid==0){
		     $('#ispublic').attr('checked',true);
		     $('#ispublic').closest('span').addClass('checked');
			 $('.dvattachform').removeClass('hidden');  
		   }else if(typeid==1){
		     $('#note').closest('.form-group').removeClass('hidden');	
			 $('.dvattendee').removeClass('hidden');  
		   }else{
			 var currdate = $('[data-current-date]').attr('data-current-date');  
			 var currtime = $('[data-current-date]').attr('data-current-time');  
			 $('#startd').val(currdate);
			 $('#startd').attr('value',currdate);
			 $('#startt').val(currtime);
			 $('#startt').attr('value',currtime);
			 $('#endd').val(currdate);
			 $('#endd').attr('value',currdate);
			 $('#endt').val(currtime);  
			 $('#endt').attr('value',currtime);  
		   }
		}else{
		   $('.dvnotify').find('input[type="checkbox"]').prop('checked',true);
		   $('.dvnotify').find('input[type="checkbox"]').closest('span').addClass('checked');
		   $('#ispublic').attr('checked',true);
		   $('#ispublic').closest('span').addClass('checked');
		   if(typeid!=2){
		     $('.dvnotify').removeClass('hidden');
		     $('.dvoption').removeClass('hidden');
		   }	
		}
		
		if(wperiod == '1'){$('.dvperiod').removeClass('hidden'); }
		if(wattach == '1'){$('.dvattach').removeClass('hidden'); }
		if(wreserve == '1'){$('.dvreserve').removeClass('hidden'); }
		if(wroom == '1'){$('.dvroom').removeClass('hidden'); }
	});
	
	$('body').on('change','#withreserve',function(){
	  if($(this).is(':checked')){	
		$('.dvreserveform').removeClass('hidden');
	  }else{
		$('.dvreserveform').addClass('hidden');  
	  }	
	});
	
	$('body').on('change','#withattach',function(){
	  if($(this).is(':checked')){	
		$('.dvattachform').removeClass('hidden');
	  }else{
		$('.dvattachform').addClass('hidden');  
	  }	
	});
	
	$('body').on('click','.btn-add,.btn-bulletin,.btn-room,.btn-ptc',function(){
		if($('.btn-add').is('hidden')){return false;}
		attachfile = [];
		$('#xid').val('new');
	    $('.btn-delete').addClass('hidden');
		$('.btn-save').removeClass('hidden');
		$('.action').html("Create");
		if($(this).is('.btn-bulletin')){
		  $('#type').val('0');
        }else{
		  $('#type').val('1');	
		}
        $('#type').trigger('change');		  
		$('#modal_event').modal('show');
	});
	
	$('body').on('change','#room',function(){
	  var selected = $(this).val();
	  var bldgid   = $(this).find('[value="'+selected+'"]').attr('data-bldg');
	  $('#bldg').val(bldgid);	
	});
	
	$('body').on('click','.btn-cancel',function(){
		$('#xid').val('new');
	    $('.btn-delete').addClass('hidden');
		$('#eventform').find('input[type="text"]').val('');
		$('#eventform').find('input[type="checkbox"]').removeAttr('checked');
		$('#eventform').find('select').val('-1');
	})
	
	$('body').on('click','.fc-event[data-id]',function(){
	  var xid = $(this).attr('data-id');
	  $('#eventform').find('input[type="text"]').val('');
	  $('#eventform').find('input[type="checkbox"]').removeAttr('checked');
	  $('#eventform').find('select').val('-1');
	  progressBar('on');
      setAjaxRequest(
		base_url+'calendar/txn?event=info'
	   ,{xid:xid}
	   ,function(r){
		  if(r.success){
		    $('#xid').val(xid);
			$('#type').val(r.content.TypeID);
            $('#type').trigger('change');	
			$('.creator').html('');	  
	        
			if($('.btn-save').length<=0){
	          $('.action').html("View");
		      $('.dvnotify').addClass('hidden');
		      $('.dvoption').addClass('hidden');
			  $('#eventform').find('input').addClass('disabled');
			  $('#eventform').find('select').addClass('disabled');
			}else{
			  $('.action').html("Modify");
			  $('.btn-delete').removeClass('hidden');
			  if(r.content.IsLock==1){
			  	 $('.creator').html('created by:'+r.content.Fullname);
			     $('.btn-save').addClass('hidden');	
			     $('.btn-delete').addClass('hidden');	
			     $('#eventform').find('input').addClass('disabled');
			     $('#eventform').find('select').addClass('disabled');
			     $('#eventform').find('textarea').addClass('disabled');
			     $('#eventform').find('button').addClass('disabled');
			  }else{
			  	 $('.creator').html('created by:'+r.content.Fullname);
				 $('.btn-save').removeClass('hidden');	
				 $('.btn-delete').removeClass('hidden');	
			     $('#eventform').find('input').removeClass('disabled');
			     $('#eventform').find('select').removeClass('disabled');
			     $('#eventform').find('textarea').removeClass('disabled');
			     $('#eventform').find('button').removeClass('disabled');
			  }
	     	}
			
			$('#title').val(r.content.Title);
	        $('#desc').val(r.content.Description);
	        $('#note').val(r.content.Notes);
			$('#bldg').val(r.content.BuildingID);
			$('#room').val(r.content.RoomID);
			$('#startd').val(r.content.StartDate);
			$('#startd').attr('value',r.content.StartDate);
			$('#endd').val(r.content.EndDate);
			$('#endd').attr('value',r.content.EndDate);
			
			$('#startt').val(r.content.StartTime);
			$('#startt').attr('value',r.content.StartTime);
			$('#endt').val(r.content.EndTime);
			$('#endt').attr('value',r.content.EndTime);
			
			if(r.content.NotifyParents==1){
				$('#notifyparent').prop('checked',true);
				$('#notifyparent').closest('span').addClass('checked');
			}else{
				$('#notifyparent').prop('checked',false);
				$('#notifyparent').closest('span').removeClass('checked');
			}
			
			if(r.content.NotifyFaculty==1){
				$('#notifyfaculty').prop('checked',true);
				$('#notifyfaculty').closest('span').addClass('checked');
			}else{
				$('#notifyfaculty').prop('checked',false);
				$('#notifyfaculty').closest('span').removeClass('checked');
			}
			
			if(r.content.NotifyEmployees==1){
				$('#notifyemployee').prop('checked',true);
				$('#notifyemployee').closest('span').addClass('checked');
			}else{
				$('#notifyemployee').prop('checked',false);
				$('#notifyemployee').closest('span').removeClass('checked');
			}
			
		    $('.dvreservebtn').addClass('hidden');
			$('.dvreserveform').addClass('hidden');
			if(r.content.Reservation==1){
				$('#withreserve').prop('checked',true);
				$('#withreserve').closest('span').addClass('checked');
				if($('.btn-save').length>0){
				  $('.dvreserveform').removeClass('hidden');
				  $('#rstartd').val(r.content.RStartDate);
				  $('#rstartt').val(r.content.RStartTime);
				  $('#rstartt').text(r.content.RStartTime);
				  $('#rendt').val(r.content.REndTime);
				  
				  $('#rstartt,#rendt').timepicker({
				   onSelect: function () {
					 $(this).text(this.value);
					}
				  });
				  
				  $('#rendt').text(r.content.REndTime);
				  $('#rduration').val(r.content.RDuration);
				}else{
				  if($('.btn-schedule').length>0){
				    window.location.href = r.xid;
                    return false;				  
				  }else{
				    $('.dvreservebtn').removeClass('hidden');
				    $('.btn-rview').attr('href',r.xid);
				  }	
				}
			}
			
			$('#tblattachment').find('tbody').html(r.attach);
			$('#tblattendee').find('tbody').html(r.attend);
			if(r.content.IsLock==1){
			   $('#eventform').find('button').addClass('disabled');
			}	
			$('#modal_event').modal('show');
		  }
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Save Data!',function(){return;});	
		  progressBar('off');
		}
	  );
	});
	
	$('body').on('click','.btn-reserve',function(){
	   $('#modal_reserve').modal('show');
	});
	
	$('body').on('click','.btn-save',function(){
	  var xdata = $('#eventform').serialize();
	  progressBar('on');
      setAjaxRequest(
		base_url+'calendar/txn?event=save'
	   ,xdata
	   ,function(r){
		  save_attendee(r.xid); 
		  if(attachfile.length>0){
			upload_attachment(r.xid); 
		  }else{
			$('.btn-cancel').trigger('click');
		    Calendar.refresh();
		    progressBar('off');  
		  }
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Save Data!',function(){return;});	
		  progressBar('off');
		}
	  );	
	});
	
	$('body').on('click','.btn-delete',function(){
	  var xid = $('#xid').val();
	  progressBar('on');
      setAjaxRequest(	
		base_url+'calendar/txn?event=remove'
	   ,{xid:xid}
	   ,function(r){
		  $('.btn-cancel').trigger('click');
		  Calendar.refresh();
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Delete Data!',function(){return;});	
		  progressBar('off');
		}
	  );	
	});
	
	$('body').on('click','.btn-attach',function(){
	   $('#attach').click();	
	});
	
	$('body').on('click','.btn-aremove',function(){
	   var aid = $(this).closest('tr').attr('data-id');
	   if(aid!='tmparr'){
         setAjaxRequest(
		   base_url+'calendar/txn?event=delattach&aid='+aid,{},
		   function(r){}
		 );
	   }
	   
	   $(this).closest('tr').remove();	 
	});
	
	$('body').on('change','#attach',function(){
		var formData = new FormData();
		var filename = $(this).val();
		var trow     = '';
		var arrid    = attachfile.length;
		if(filename!='' && filename!=undefined){
		  formData.append('file', $(this)[0].files[0]);
		  attachfile.push({fname:filename,xdata:formData});
		  trow = '<tr><td data-id="tmparr" data-arrid="'+arrid+'" data-filename="'+filename+'"><button class="btn btn-xs red btn-aremove"><i class="fa fa-trash-o"></i> Remove</button></td><td>'+filename+'</td></tr>';
		  $('#tblattachment').find('tbody').append(trow);
		  console.log(attachfile.length);
		}
	});
	
	
	$('body').on('click','.btn-add-attend',function(){
		$('#modal_filter').modal('show');
	});
		
	$('body').on('click','.btn-rem-attendee',function(){
		$(this).closest('tr').remove();
	});

	$('body').on('keyup','#empfilter',function(e){
	    if(e.keyCode==13){
		  	var tmpval = $('#empfilter').val();
			if(tmpval.trim()!=''){
			   $('.btn-emp-filter').trigger('click');
			}
		}
	});
	
	$('body').on('click','.btn-emp-filter',function(){
	  var xid = $('#empfilter').val();
	  progressBar('on');
      setAjaxRequest(
		base_url+'calendar/txn?event=filter'
	   ,{type:'employee',filter:xid}
	   ,function(r){
		  $('#tblempfilter').find('tbody').html(r.content); 
		  progressBar('off');
		}
	   ,function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Load Data!',function(){return;});	
		  progressBar('off');
		}
	  );	
	
	});
	
	$('body').on('change','#startd,#startt,#endd,#endt',function(){
		var typeid = $('#type').val();
		var startd = $('#startd').val();
		var startt = $('#startt').val();
		var endd   = $('#endd').val();
		var endt   = $('#endt').val();
		if(typeid==1){
		  setAjaxRequest(
			base_url+'calendar/txn?event=room'
		   ,{startd:startd,startt:startt,endd:endd,endt:endt}
		   ,function(r){
			  $('#room').find('[value!="-1"]').remove(); 
			  $('#room').append(r.content); 
			  progressBar('off');
			}
		   ,function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to Load Data!',function(){return;});	
			  progressBar('off');
			}
		  );
		}  
	});
	
	$('body').on('click','[data-id]',function(){
	   var tmptable = $(this).closest('table');
	   if($(tmptable).is('.table-select')){
	      $(tmptable).find('.info').removeClass('info');
	      $(this).closest('tr').addClass('info');
	   }
	});	
	
	$('body').on('click','.btn-filter-select',function(){
		var xid     = $('#tblempfilter').find('.info').attr('data-uid');
		var eid     = $('#tblempfilter').find('.info').attr('data-id');
		var tmpname = $('#tblempfilter').find('.info').find('.data-name').html();
		var tmprow  = '<tr data-id="'+((xid=='')?eid:xid)+'"><td><button class="btn btn-xs btn-danger btn-rem-attendee"><i class="fa fa-times"></i> Remove</button></td><td>'+tmpname+'</td></tr>';
		var trcount = $('#tblattendee').find('[data-id]').length;
		var isexist = $('#tblattendee').find('[data-id="'+((xid=='')?eid:xid)+'"]').length;
		if(isexist<=0){
			if(trcount>0)
			   $('#tblattendee').find('tbody').append(tmprow);
			else
			   $('#tblattendee').find('tbody').html(tmprow);
	    }
		$('#modal_filter').modal('hide');
	});
	
	$('body').on('click','.btn-rexempt',function(){
	   var schedule = $('#eventform').find('#xid').val();
	   var pdata    = 'schedule='+schedule;
	   setAjaxRequest(
			 base_url+'calendar/txn?event=exemption&'+pdata
			,''
			,function(r){
			   if(r.success){	
			       $('#tblrexemption').find('tbody').html(r.content)
				   $('#tblrexemption').find('[data-standard="0"]').remove();
				   $('#tblrexemption').find('#entityid').append(r.employee);
				   $('#modal_reexempt').modal('show');
			   }
			   progressBar('off');  
			}
			,function(err){
			   alert('Failed to load exemption!');  
			   progressBar('off');  
			}
	   );  
	   progressBar('off');  
	});
	
	$('body').on('click','.btn-vwbook',function(){
	   var xurl = base_url+'calendar/booking?event='+$('#eventform').find('#xid').val();
	   window.open(xurl,'_blank');
	});
	$('body').on('click','.btn-vwsched',function(){
	   var xurl = base_url+'calendar/booking?event='+$('#eventform').find('#xid').val()+'&mode=f';
	   window.open(xurl,'_blank');
	});
		   	   
    $('body').on('click','.btn-rremove',function(){
       var entryid = $(this).attr('data-id');
	   var trrow   = $(this).closest('tr');
	   progressBar('on');  
	   setAjaxRequest(
			 base_url+'calendar/txn?event=remexempt&entryid='+entryid
			,''
			,function(r){
			   if(r.success){
				   $(trrow).remove();
			   }else{	    
			       alert('Failed to remove entry!');  
			   }
			   progressBar('off');  
			}
			,function(err){
			   alert('Unable to remove the entry!');  
			   progressBar('off');  
			}
	   );  
	   progressBar('off');  
	});	
	
    $('body').on('click','.btn-rsave',function(){
	   var schedule = $('#eventform').find('#xid').val();
	   var studno   = '0';
	   var empid    = $('.exemptform').find('#entityid').val();
	   var subj     = '0';
	   var rdate    = $('.exemptform').find('#xdate').val();
       var tstart   = $('.exemptform').find('#xstart').val();
	   var tend     = $('.exemptform').find('#xend').val();
	   var pdata    = 'sched='+schedule+'&studno='+studno+'&faculty='+empid+'&subject='+subj+'&rdate='+rdate+'&start='+tstart+'&end='+tend;
	   progressBar('on');  
	   setAjaxRequest(
			 base_url+'scheduling/txn?event=savesched&'+pdata
			,''
			,function(r){
			   if(r.success){	
				   $('#tblrexemption').find('tbody').html(r.content)
			   }else{
			       alert('Failed to save reservation! Either your schedule is taken or conflict with other user!');  
			   }
			   progressBar('off');  
			}
			,function(err){
			   alert('Failed to save reservation! Either your schedule is taken or conflict with other user!');  
			   progressBar('off');  
			}
	   );  
	   progressBar('off');  
    });
	
});