var imgctrl;
var xfiles;

$('document').ready(function(){

	$('.date-picker').datepicker({
       onSelect: function () {
         $(this).text(this.value);
        }
    });
  $('body').on('click','.checker',function(){
	var isdisabled = $(this).is('.disabled'); 
	var istick     = $(this).find('span').is('.checked'); 
	if(isdisabled){return false; }
	if(istick)
	{
	 $(this).find('span').removeClass('checked');	
	 $(this).find('[type="checkbox"]').removeAttr('checked');		 
	}	
	else
	{
	 $(this).find('span').addClass('checked');	
	 $(this).find('[type="checkbox"]').prop('checked',true);	
	 if($(this).is('.chkisfac')){$('.navdetail').click(); }	 
	}	
  });
  
  $('body').on('click','.emprow',function(){
	var tbmain = $(this).closest('tbody');	
	tbmain.find('tr').removeClass('danger');
	$(this).addClass('danger');	
  //load_data('xemp',$(this));
  });
  
  $('body').on('click','.btnrefresh',function(){
	clear_form(0);    
    load_data('list');
  });
  
  $('body').on('click','.btncancel',function(){
	clear_form(0);    
    $('#facultyinfo').hide();
    $('#masterlist').show();

  });
  
  $('body').on('click','.btnnew,.btnedit',function(){
	clear_form(1);
	$('#empid').attr('data-eid','new');
	$('#empid').attr('data-fid','new');
     $('#facultyinfo').show();
     $('#masterlist').hide();
        
	if($(this).is('.btnedit'))
	{
       
	 load_data('emp',$(this).closest('tr'));
	}	
  });
  $('body').on('click','.btnsave',function(){
     var xlink   = base_url+'academics/faculty-management/txn';
	 var empid   = $('#empid').val();
	 var xid     = $('#empid').attr('data-eid');
	 var fid   = $('#empid').attr('data-fid');
     if(xid!='' && xid!=undefined)
     {
	  if(!validateform()){return false;}	 
	  setAjaxRequest(
		xlink+'?event=saveemp'
	   ,generate_data()
	   ,function(rs)
		{
		 if(rs.success)
		 {	 
	      if(xid=='new'){$('.btnempphoto').removeClass('hidden'); }
		  load_data('list');
		  setTimeout(function(){
		   var xid = $('.emprow[data-id="'+empid+'"]').attr('data-id');
		   var fid = $('.emprow[data-id="'+empid+'"]').attr('data-fid');
		   if(xid!=undefined)
		   {
			$('#empid').attr('data-eid',xid); 
			$('.emprow[data-id="'+empid+'"]').addClass('danger');
		   }
		   if(fid!=undefined){$('#empid').attr('data-fid',fid); }
		   $('.btnedit').attr('disabled',true);
		   $('.btnremove').attr('disabled',true);
		   confirmEvent('<i class="fa fa-check text-success"></i> Successfully Save!',function(){return;});	
		   progressBar('off');
          },800);		 
		 }
         else
         {
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save data!',function(){return;});	
		  progressBar('off');
		 }			 
		}
	   ,function(err)
		{	
		 confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save data!',function(){return;});	
		 progressBar('off');
		});  
	 }		 
  });
  
  $('body').on('click','.btnremove',function(){
     var xlink = base_url+ page_url + 'txn';
	 
     var empid = $('#tblfaculty tbody tr.danger').attr('data-id');
     
     if(empid!='' && empid!=undefined)
     {
	  confirmEvent('<i class="fa fa-question-circle text-warning"></i> Are you sure you want to delete this employee?',function(e){
		if(e)
        {
		 setAjaxRequest(
			xlink+'?event=delemp'
		   ,{empid:empid}
		   ,function(rs)
			{
			 if(rs.success)
			 {	 
			  if(rs.isdelete)
			  {	  
			   $('emprow[data-id="'+empid+'"]').remove();
			   confirmEvent('<i class="fa fa-warning text-danger"></i> Employee is successfully deleted',function(){return;});	
			  }
			  else
			  {	  
			   $('emprow[data-id="'+empid+'"]').find('.inactive').attr('data-value',1);
			   $('emprow[data-id="'+empid+'"]').find('.inactive').html('<i class="fa fa-check-square-o"></i>');
			   confirmEvent('<i class="fa fa-warning text-danger"></i> Employee is not deletable, It was set to <b>INACTIVE</b>',function(){return;});	
			  }
			  
			  console.clear();
			 }
			 else
			  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});	
			 
			 progressBar('off');	 
			}
		   ,function(err)
			{	
			 confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to delete item!',function(){return;});	
			 progressBar('off');
			});  	
		}	
        else
         return false;	  	
	  });	
	  
	 }		 
  });
  
  $('body').on('keypress keydown keyup','.nonedit',function(e){
	 e.preventDefault();
	 return false;
  });
  
  $('body').on('click','.btnempphoto',function(){
	  var empid  = $('#empid').attr('data-eid');
	  var imgsrc = $('.ephoto').attr('src');
	  if(empid!='' && empid!='new' && empid!=undefined)
	  {	
        if(imgctrl==undefined){imgctrl = $('.inpimg').clone(true); }			
        $('.previmg').attr('src',imgsrc);
	    $('.inpimg').replaceWith(imgctrl);
	    $('.inpimg').val('');
	    $('.inpimg').attr('onchange','previewimg(this);');
		$('#modal_upload').modal('show');
	  } 
  });
  
  $('.inpimg').change(function(){
	var input      = this;
	previewimg(input);
  });
  
  $('body').on('click','.btnupload',function(){
     var xlink = base_url+'academics/faculty/txn?event=upload_img';
	 var isvalid = $('.inpimg').attr('data-valid');
     var empid   = $('#empid').attr('data-eid');
	 
	 if(!isvalid && xfiles==undefined)
       alert('Input an image to upload');
     else
     {
	   var data = new FormData();
	   if(xfiles!=undefined)
	   {   
        $.each(xfiles, function(key, value)
        {
         data.append(key, value);
        });
	   }
	   else
		return false;
	 
	   data.append('empid',empid);
	   
	   $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
       });
	   
	   $.ajax({
        url: xlink,
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, 
        contentType: false, 
        success: function(rs)
        {
          if(rs.success)
		  {
			$('.ephoto').attr('src',$('.previmg').attr('src'));
			$('#modal_upload').modal('hide');
		  }	
          else
          {
			confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to upload image!',function(){return;});	
		    progressBar('off');  
		  }			  
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to upload image!',function(){return;});	
		  progressBar('off');  
        }
	   });
     }		 
  });
  
  $('.btncancel').click();
  setTimeout(function(){load_data('list'); },800);
});

function clear_form(opt)
{
 if(opt==undefined){opt=0; } 
 $('.btnempphoto').addClass('hidden');
 $('#ephoto').attr('src',$('#ephoto').attr('data-default'));
 $('#empid').attr('data-eid','new');
 $('#empid').attr('data-fid','new');
 $('.emp_form').addClass('nonedit');
 $('.emp_form').find('input[type="text"]').val('');
 $('.emp_form').find('select').val('-1');
 $('.emp_form').find('.checker').find('span').removeClass('checked');
 $('.emp_form').find('.checker').find('input').removeAttr('checked');
 $('.emp_data').addClass('nonedit');
 $('.emp_data').find('input[type="text"]').val('');
 $('.emp_data').find('select').val('-1');
 $('.emp_data').find('.checker').find('span').removeClass('checked');
 $('.emp_data').find('.checker').find('input').removeAttr('checked');
 $('.emp_data').find('.radio').find('span').removeClass('checked');
 $('.emp_data').find('.radio').find('input').removeAttr('checked');
 $('.subjtaught').html('');
 $('.teachload').html('');
 if(opt==0)
 {	 
  $('.emp_data').find('select').attr('disabled',true);
  $('.emp_form').find('select').attr('disabled',true);
  $('.emp_form').find('.checker').find('span').addClass('disabled');
  $('.emp_form').find('.radio').find('span').addClass('disabled');
  $('.emp_data').find('.checker').find('span').addClass('disabled');
  $('.emp_data').find('.radio').find('span').addClass('disabled');
  $('.btncancel').attr('disabled',true);
  $('.btnsave').attr('disabled',true);
  $('.btnnew').removeAttr('disabled');
  $('.btnedit').removeAttr('disabled');
  $('.btnremove').removeAttr('disabled');
 }
 else
 {
  $('.emp_form').removeClass('nonedit');
  $('.emp_data').removeClass('nonedit');
  $('.emp_data').find('select').removeAttr('disabled');
  $('.emp_form').find('select').removeAttr('disabled');
  $('.emp_form').find('.checker').find('span').removeClass('disabled');
  $('.emp_form').find('.radio').find('span').removeClass('disabled');
  $('.emp_data').find('.checker').find('span').removeClass('disabled');
  $('.emp_data').find('.radio').find('span').removeClass('disabled');
  $('.btncancel').removeAttr('disabled');
  $('.btnsave').removeAttr('disabled');
  $('.btnnew').attr('disabled',true);
  $('.btnedit').attr('disabled',true);
  $('.btnremove').attr('disabled',true);
 }	
}

function load_data(opt,param)
{
  if(opt==undefined){return false;}
  if(opt==undefined){param = false;}
  var xlink = base_url+ page_url + 'txn';
  switch(opt)
  {
	case 'list':
	  setAjaxRequest( xlink+'?event=emplist',{},
        function(rs) {
		 if(rs.success)
		 {	 
		  $('.emplist').html(rs.content);
		  console.clear();
		 }
		 else
		  confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});	
		 
		 progressBar('off');	 
		}
	   ,function(err)
		{	
		 confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		 progressBar('off');
		}); 
    break;
	case 'xemp':
	case 'emp':
	 var trrow = param;
	 var empid = trrow.attr('data-id');
	 $('.btnempphoto').removeClass('hidden');
	 $('.ephoto').attr('src',base_url+page_url+'photo?empid='+empid);
	 $('#empid').val(trrow.attr('data-id'));	
	 $('#empid').attr('data-eid',trrow.attr('data-id'));
	 $('#empid').attr('data-fid',trrow.attr('data-fid'));
	 $('#title').val(trrow.find('.empname').attr('data-title'));	
	 $('#lname').val(trrow.find('.empname').attr('data-lname'));	
	 $('#fname').val(trrow.find('.empname').attr('data-fname'));	
	 $('#mname').val(trrow.find('.empname').attr('data-mname'));	
	 $('#miname').val(trrow.find('.empname').attr('data-miname'));	
	 $('#suffix').val(trrow.find('.empname').attr('data-suffix'));	
	 $('#bday').val(trrow.find('.bday').html());	
	 $('#sex').val(trrow.find('.sex').html());	
	 $('#civilstats').val(trrow.find('.civilstat').attr('data-id'));	
	 $('#pos').val(trrow.find('.position').attr('data-id'));	
	 $('#campus').val(trrow.attr('data-campus'));
	 $('#college').val(trrow.attr('data-college'));
	 $('#dept').val(trrow.find('.dept').attr('data-id'));	
	 
	 if(trrow.attr('data-isfac')==1)
	 {
	   $('#isfaculty').prop('checked',true);
	   $('#isfaculty').closest('span').addClass('checked');
	   
	   $('#empid').attr('data-fid',trrow.attr('data-fid'));	
	   $('#facrank').val(trrow.attr('data-rank'));
       $('#emstatus').val(trrow.attr('data-isfull'));	   
	   $('#educlvl').val(trrow.attr('data-lvl'));
	   $('#discipline').val(trrow.attr('data-degree'));
	   $('#prc').val(trrow.attr('data-prc'));
	   $('#load').val(trrow.attr('data-load'));
	   
	   if(empid!=undefined && empid!='')
	   {	   
	    setAjaxRequest(
		  xlink+'?event=emphist'
	    ,{empid:empid}
	    ,function(rs)
		 {
		  if(rs.success)
		  {	 
		   $('.subjtaught').html(rs.subjs);
		   $('.teachload').html(rs.teach);
		   //console.clear();
		  }
		  else{
		   confirmEvent('<i class="fa fa-warning text-danger"></i> '+rs.message,function(){return;});   
		  }
		   	
		 
		  progressBar('off');	 
		 }
	    ,function(err)
		 {	
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
		  progressBar('off');
		 }); 
	   }
	   
	   if(opt=='emp')
	   {	   
        $('.navdetail').click();	   
	   }	
	 }
	 
	 if(trrow.find('.inactive').attr('data-value')==1)
	 {
	   $('#inactive').prop('checked',true);
	   $('#inactive').closest('span').addClass('checked');
	 }
	break;
    default:
	 return false;
    break;	
  }  
}

function validateform()
{
 var xid    = $('#empid').attr('data-eid');	
 var fid    = $('#empid').attr('data-fid');	
 var empid  = $('#empid').val();
 var isfac  = $('#isfaculty').prop('checked');
 var result = true;
 var xmsg   = '';
 
 $('.has-error').removeClass('has-error');
 if(xid=='new' && $('.emprow[data-id="'+empid+'"]').length>0)
 {
   $('#empid').closest('.form-group').addClass('has-error');
   alert('EmployeeID is already in use.');
   return false;   
 }
 
 $('.emp_form').find('input[type="text"]').each(function(indx){
	var isrequired = $(this).is('.required');
	var xval       = $(this).val(); 
	
	if(isrequired && (xval==undefined || xval=='' || xval==' '))
	{	
	 $(this).closest('div').addClass('has-error');
	 xmsg = 'Complete all required fields.';
	 result = false;
	} 
 });
 
 $('.emp_form').find('select').each(function(indx){
	var isrequired = $(this).is('.required');
	var xval       = $(this).val(); 
	
	if(isrequired && (xval==undefined || xval=='' || xval==' ' || xval=='-1'))
	{	
	 $(this).closest('div').addClass('has-error');
	 xmsg = 'Complete all required fields.';
	 result = false;
	}
 });
 
 if(isfac)
 {
  $('.emp_data').find('input[type="text"]').each(function(indx){
	var isrequired = $(this).is('.required');
	var xval       = $(this).val(); 
	
	if(isrequired && (xval==undefined || xval=='' || xval==' '))
	{	
	 $(this).closest('div').addClass('has-error');
	 xmsg = 'Complete all required fields.';
	 result = false;
	} 
  });
 
  $('.emp_data').find('select').each(function(indx){
	var isrequired = $(this).is('.required');
	var xval       = $(this).val(); 
	
	if(isrequired && (xval==undefined || xval=='' || xval==' ' || xval=='-1'))
	{	
	 $(this).closest('div').addClass('has-error');
	 xmsg = 'Complete all required fields.';
	 result = false;
	}
  });
 }	 
 
 if(xmsg!=''){alert(xmsg);}
 return result;
}

function focusitem(target,main)
{
 var rowpos = target.position();
 main.scrollTop(rowpos.top - 50);	
}

function generate_data()
{
 var xdata = {};
 xdata['xid']        = $('#empid').attr('data-eid');
 xdata['fid']        = $('#empid').attr('data-fid');
 xdata['empid']      = $('#empid').val();
 xdata['lname']      = $('#lname').val();
 xdata['fname']      = $('#fname').val();
 xdata['mname']      = $('#mname').val();
 xdata['miname']     = $('#miname').val();
 xdata['title']      = (($('#title').val()!='-1') ? $('#title').val() : '');
 xdata['extname']    = (($('#suffix').val()!='-1') ? $('#suffix').val() : '');
 xdata['bday']       = $('#bday').val();
 xdata['sex']        = $('#sex').val();
 xdata['civil']      = $('#civilstats').val();
 xdata['pos']        = $('#pos').val();
 xdata['dept']       = $('#dept').val();
 xdata['college']    = $('#college').val();
 xdata['camp']       = $('#campus').val();
 xdata['isfac']      = (($('#isfaculty').prop('checked'))?1:0);
 xdata['inactive']   = (($('#inactive').prop('checked'))?1:0);
 
 if(xdata['isfac']==1)
 {
  xdata['rank']    = $('#facrank').val(); 
  xdata['isfull']  = $('#emstatus').val(); 
  xdata['educlvl'] = $('#educlvl').val(); 
  xdata['degree']  = $('#discipline').val(); 
  xdata['prc']     = $('#prc').val(); 
  xdata['load']    = $('#load').val(); 
 }	 
 
 return xdata;
}

function previewimg(input)
{
 var defaultimg = $('.tmpimg').attr('data-default');
 if (input.files && input.files[0]) 
 {
   var reader = new FileReader();
       xfiles = input.files;
	   
   reader.onload = function(e) 
   {
	 var filetype = input.files[0].type;
	 var filesize = input.files[0].size; 		
	 if(filetype.indexOf('image/') == 0)
	 {
       $('.tmpimg').attr('src',e.target.result);
	   var fileheight = $('.tmpimg').height();
	   var filewidth  = $('.tmpimg').width();
	   if(fileheight<=500 && filewidth<=500)
	   {	   
		$('.previmg').attr('src', e.target.result);	  
	    $('.inpimg').attr('data-valid',true);	
	   }
	   else
	   {
		alert('Image is too big!');
		$('.inpimg').replaceWith(imgctrl);
	    $('.inpimg').attr('onchange','previewimg(this);');		  
		xfiles=undefined;
	   }	
	 }
	 else
	 {
	  alert('Invalid file to upload');
      $('.inpimg').replaceWith(imgctrl);
	  $('.inpimg').attr('onchange','previewimg(this);');	
	  xfiles=undefined;	  
	 }	  
	 $('.tmpimg').attr('src',defaultimg);	
   }
   reader.readAsDataURL(input.files[0]);
 }	
}
