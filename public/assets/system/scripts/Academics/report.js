var Report = {
    
    init : function(){
        this.initperiod();
        this.initYearlevel();
        this.change_report();
    },
    
    change_report: function(){
        
        var report = $("#report").val();
        
        $('.row[data-mode="students"]').show();
        $('.row[data-mode="schedules"]').show();
        $('.row[data-mode="club"]').hide();
        
        if (report=='13') {
            $('.faculty').show();
            $('.student').hide();
        }
        else {
            $('.faculty').hide();
            $('.student').show();
        }
        
        if (report=='17') {
            Report.getClubs();
            $('.row[data-mode="club"]').show();
            $('.row[data-mode="students"]').hide();
            $('.row[data-mode="schedules"]').hide();
        }
        if (report=='25') {
            $('.row[data-mode="subject"]').hide();
            $('.row[data-mode="students"]').hide();
            $('.faculty').hide();
        }        
    },
    
    getYearLevel: function(prog_class){
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-year-level', 'pclass': prog_class},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    var res = '<option value="" disabled selected></option>'+result.html;
                    $('#year-level').html(res).removeAttr('disabled').select2("destroy").select2();

                }
            }
        );
        progressBar('off');
    },        
    
    getSections: function(term, prog, yl_id){
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-sections', 'yl': yl_id, 'p': prog, 't': term},
            function(result) {
                if (result.error) {
                    $('#section').attr('disabled', true);
                    msgbox('error', result.message);

                } else {
                    var res = '<option value="0" selected>*All*</option>'+result.html;
                    var subj = '<option value="0" selected>*All*</option>'+result.subjects;
                    $('#section').html(res).removeAttr('disabled').select2("destroy").select2();
                    $('#subject').html(subj).removeAttr('disabled').select2("destroy").select2();
                }
            }
        );
        progressBar('off');
    },
    
    getClubs: function(){
        var term = $('#academic-term').val();
        var prog = $('#programs').val();
        
        setAjaxRequest( base_url + page_url + 'event', {'event': 'get-clubs', 't': term,'p': prog },
            function(result) {
                if (result.error) {
                    $('#clubs').attr('disabled', true);
                    msgbox('error', result.message);

                } else {
                    var res = '<option value="0" selected>*All*</option>'+result.html;
                    $('#clubs').html(res).removeAttr('disabled').select2("destroy").select2();
                }
            }
        );
        progressBar('off');
    },
    
    getStudents: function(_find){
        var details = $('#report-form').serialize();

        setAjaxRequest(
            base_url + page_url + 'event?'+details, {'event': 'get-students'},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);

                } else {

                    showModal({
                        name: 'basic',
                        title: 'Student List',
                        content: result.html,
                        class: 'modal_student_result',
                        hasModalButton: false
                    });
                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#table-students-res').DataTable({ "aaSorting" : [] });

                }
            }
        );
        progressBar('off');
    },

    printReport: function(){
        var details = $('#report-form').serialize(), stud_num = $('#student-name').attr('data-snum'), pclass = $('#programs option:selected').attr('data-pclass');
        window.open(base_url + page_url + 'print?' + details + '&snum=' + stud_num+ '&pclass=' + pclass,'Report Preview');
    },
    
    initperiod : function(){
        
        var $term = $('#academic-term');

        var txt = $term.find('option:selected').text()
        var x = txt.trim().indexOf('School');

        if(x <= 0){
            $('#period').find('option').remove();
            $('#period')
                .empty()
                .append('<option value="-1"> - Select Period - </option><option value="11"> 1ST TERM</option><option value="12"> 2ND TERM</option>');
        }else{
            $('#period').find('option').remove();
            $('#period')
                .empty()
                .append('<option value="-1"> - Select Period - </option><option value="1"> 1ST QUARTER</option><option value="2"> 2ND QUARTER</option><option value="3"> 3RD QUARTER</option><option value="4"> 4TH QUARTER</option><option value="14"> Final </option>');
        }                                  
    },
    
    initYearlevel : function(){
       var pclass = $('#programs')
                        .find('option:selected')
                        .attr('data-pclass');
       Report.getYearLevel(pclass);        
    },        
};

$(document).ready(function(e){

    $('.select2').select2();
    $('.faculty').hide();
    Report.change_report();
    
    $('body').on('click', '#student-find', function(e){
        var _find = $('#student-name').val();
        Report.getStudents(_find);
    });

    $('body').on('click', '.btn-stud-select-btn', function(e){
        var stud_name = $(this).parent().prevUntil('td:first').find('p').text();
        $('#student-name').val(stud_name).attr('data-snum', $(this).attr('data-id'));
    });

    $('body').on('change', '#programs', function(e){
        var pclass = $(this).find('option:selected').attr('data-pclass');
        $('#student-find, #student-name, #section').attr('disabled', true);
        $('#section').select2('destroy').empty();
        $('#student-name').attr('data-snum', '').val('');
        Report.getYearLevel(pclass);
        Report.getClubs();
    });

    $('body').on('change', '#year-level', function(e){
        var prog = $('#programs').val(), yl = $(this).val(), t = $('#academic-term').val();
        Report.getSections(t, prog, yl);
        $('#student-find, #student-name').attr('disabled', true);
        $('#student-name').attr('data-snum', '').val('');
    });

    $('body').on('change', '#academic-term', function(e){        
        var prog = $('#programs').val(), yl = $('#year-level').val(), t = $(this).val();
        $('#student-name').attr('data-snum', '').val('');
                
        var r_id = $("#report").val();
        
        if(r_id == 17){
            Report.getClubs(t);     
        }else{
            Report.getSections(t, prog, yl);    
        }     
        Report.initperiod();                
    });

    $('body').on('change', '#section', function(e){
        $('.stud-find').attr('disabled', false);
        $('#student-name').attr('data-snum', '').val('');
    });

    $('body').on('click', '#print-report', function(e){
        
        var valid = isFormValid('report-form');
        
        var stud_num = $('#student-name').attr('data-snum');
        var prog = $('#programs').val(), yl = $('#year-level').val(), t = $('#academic-term').val(), p = $('#period').val();
        var report = $("#report").val();
        if (report=='17') {
            if( $('#clubs').val() != '' || $('#clubs').val() != undefined ){
                Report.printReport();
            } else {
                alert('Please fill required fields to proceed.');
            }
        
        }
        if(report=='13') {
            if(Boolean(t.trim())){
                Report.printReport();
            } else {
                msgbox('error', 'Please fill required fields to proceed.');
            }
        }
        
        if (report=='14') {
            if(Boolean(prog.trim())){
                Report.printReport();
            } else {
                msgbox('error', 'Please fill required fields to proceed.');
            }
        }
        else {
            if(p <=0 ){
                msgbox('invalid parameter', 'Please select period!');
                return false;
            }
        
            if(Boolean(prog.trim()) && Boolean(yl.trim()) && Boolean(t.trim())){
                Report.printReport();
            } else {
                msgbox('error', 'Please fill required fields to proceed.');
            }
        }
    });

    $('body').on('click', '#student-remove', function(e){
        $('#student-name').attr('data-snum', '').val('');
    });

    $('body').on('change', '#report', function(e){
        Report.change_report();    
    }); 
});