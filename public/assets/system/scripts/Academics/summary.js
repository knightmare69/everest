var ME = {
    init : function() {
        this.is_busy = false;
        this.bind();        
    },    
           
    load : function(){        
        var r = $('#table-students-res tr.active-row').attr('data-id');                
        location.replace(base_url+page_url+'form?t='+ $('#term').val()+'&idno='+ r  );        
    },
    
    reload : function(e){        
        location.replace(base_url+page_url+'?t='+ $('#term').val() + '&c='+ $('#campus').val()+'&l=' + $('#level').val() +'&sec='+ $('#section').val()  );        
    },
    
    section : function(e){
        var s = $(e.target).val();
        
        setAjaxRequest( base_url + page_url + '/event', {'event': 'sections','term': $('#term').val(), 'level': s}, function(r) {
            if (r.error) {
                msgbox('danger',r.message,'Summary of Grades');
            } else {
                $('#section').html(r.sections)
                msgbox('success',r.message,'Summary of Grades');                
            }
        }); 
        progressBar('off');      
    },
    compute: function(e){
        var data = [];
        
        $('.chk-child').each(function(){
            if ( $(this).prop('checked') ){
                data.push($(this).closest('tr').attr('data-id'));
            }
        });
        console.log(data);
        $.SmartMessageBox({
        	title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
        	content : "Do you wish to recompute selected student/s quarters and final grade? <br/> Click [YES] to proceed...",
        	buttons : '[No][Yes]'
        }, function(ButtonPressed) {        
        	if (ButtonPressed === "Yes") {
                
                setAjaxRequest( base_url + page_url + '/event?event=compute',{'list': data} , function(r) {
                    if (r.error) {
                        msgbox('danger',r.message,'Summary of Grades');
                    } else {
                        $('#section').html(r.sections)
                        msgbox('success',r.message,'Summary of Grades');                
                    }
                }); 
                progressBar('off');  
        
            }
        });
        
    },
    action :function(e){

        var data = $(e.target).data();
        switch (data.menu) {            
            case 'refresh': this.reload(e); break;            
            case 'cog': this.compute(e); break; 
			case 'view': this.view(e); break;
            default: break;
        }
    },
	
	view: function(e){
		var idx = $(e.target).attr('data-val');
		var name = $(e.target).attr('data-name');
		setAjaxRequest( base_url + page_url + '/event?event=get-details', { 'ref':  idx },
            function(r) {
                if (r.error == false) {
                    
                    showModal({ name: 'large', title: 'Grade Details : '+name, content: r.view });

                } else {
                    msgbox('error', req.message);
                }
            }, null, 'json', true, false
        );
		
	},

    bind: function() {
        $('body').on('click','.btn, .options', $.proxy(this.action, this));        
        $('body').on('change','#level', $.proxy(this.section, this));
    },        
};