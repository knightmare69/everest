var Config = {
    loadConfig: function(term){
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get-config', 'term': term},
            function(r) {
                if (r.error) {
                    //msgbox('error', r.message);
                    sysfnc.showAlert('.form-body','danger','<i class="fa fa-warning"></i> ' + r.message);
                    $('#startdate').val(r.start);
                    $('#finishdate').val(r.end);
                } else {
                    $('#startdate').val(r.start);
                    $('#finishdate').val(r.end);
                    
                    var track = r.allow.split(",");
                                                        
                    $('.chk-child').each(function(){
                        var _this = $(this);
                        var id = $(this).attr('data-id');
                        track.forEach(function(item, index){
                            console.log(item +' | ' + id);
                            if( item == id ){
                               _this.prop('checked',true);
                               _this.parent().addClass('checked');
                            }
                        });                                                                     
                                                                                                 
                    });
                }
            }
        );
        progressBar('off');
    },
    saveConfig: function(term, start, end){
        var rcd = $('#clsrcd').prop('checked');
        var active = $('#cActive').prop('checked');
        
        var seltrack = []; 
        $('.chk-child').each(function(){
            if( $(this).prop('checked') ){
                seltrack.push($(this).attr('data-id'));
            } 
        });
        
        console.log(seltrack);
        
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'save-config', 'start': start, 'end': end, 't': term, 'clsrcd' : (rcd ? 1 : 0 ) , 'active' : active, track : seltrack },
            function(r) {
                if (r.error) {
                    msgbox('error', r.message);                    
                } else {
                    msgbox('success', r.message);
                }
            }
        );
        progressBar('off');
    },
}
$(document).ready(function(e){
    $('.select2').select2();
    $('.datepicker').datepicker();
    $('body').on('click', '#save-config', function(e){
        var t = $('#academic-term').val(), s = $('#startdate').val(), e =  $('#finishdate').val();
        Config.saveConfig(t,s,e);
    });
    $('body').on('change', '#academic-term', function(e){
        var t = $('#academic-term').val()
        Config.loadConfig(t);
    });
});