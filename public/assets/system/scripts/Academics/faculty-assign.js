"use strict";
$(document).ready(function(){
    $('.select2').select2();
    $('#records-table').DataTable({
        "aaSorting": [],
        "bDestroy": true,
    });
    $('body').on('click', '#class-sec-load', function(e){
        e.preventDefault();
        var tbl = $('#records-table'), trs = tbl.DataTable().rows().nodes(), sections = [], c = $('#campus').val(), ac = $('#academic-year').val();
        for(var a = 0; a < trs.length - 1; a++){
            var _this = trs[a];
            sections.push($(_this).find('td a.sec-name').attr('data-id'));
        }
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'get', 'sections': sections, 'campus': c, 'ac-year': ac},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    var cur_page = $('#class-section li.paginate_button.active a').text();
                    tbl.dataTable().fnDestroy();
                    $(result.recs).insertBefore('tr.load-more-row');
                    tbl.DataTable({
                        "aaSorting": [],
                        initComplete: function(){
                            this.fnPageChange(parseInt(cur_page) - 1);
                        }
                    });
                }
            }
        );
        progressBar('off');
    });
    $('body').on('click', '.sec-name', function(e){
        e.preventDefault();
        var section = $(this).attr('data-id'), sec_name = $(this).text();
        setAjaxRequest( base_url + page_url + 'event', {'event': 'get-sched', 'section': section},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    var tab1_id = '#class-section', tab2_id = '#class-section-sched',  subj_tab = $('a[href="'+ tab2_id +'"]');

                    $(tab2_id).attr('data-id',section);
                    $(tab2_id).html(result.subjs);

                    $('#subj-records-table').DataTable({"aaSorting": [], 'autoWidth': false});
                    subj_tab.parent().removeClass('hide').addClass('active').end().text(sec_name + ' Schedule');
                    $(tab2_id).addClass('active in');
                    $('a[href="' + tab1_id + '"]').parent().removeClass('active');
                    $(tab1_id).removeClass('active in');
                }
            }
        );
        progressBar('off');
    });
    $('body').on('click', '.faculty-search-cancel-btn', function(e){
        var $r = $(this).closest('tr');
        var id = $r.attr('data-id'), type_for = $(this).attr('data-type'), check_id = $(this).attr('data-id');
        var self = $(this);

        if(check_id == '' || check_id == 0 || check_id == undefined){
            msgbox('error', "There's no "+ type_for +" to be replaced.");
        } else {
            bootbox.confirm({
                size: 'small',
                message: "Remove "+ type_for +"?",
                callback: function(yes) {
                    if (yes) {
                        setAjaxRequest(
                            base_url + page_url + 'event', {'event': 'replace-faculty', 'f': id, 'type': type_for},
                            function(result) {
                                if (result.error) {
                                    msgbox('error', result.message);
                                } else {
                                    msgbox('success', result.message);
                                    if(type_for =='adviser'){
                                        self.parent().find('.faculty-search-btn').text('[Adviser]');
                                    }else{
                                        self.parent().find('.faculty-search-btn').text('[Teacher]');
                                    }
                                }
                            }
                        );
                        progressBar('off');
                    }
                }
            });
        }
    });
    var faculty_search_box = '';
    $('body').on('click', '.faculty-search-btn', function (e) {
        e.preventDefault();
        faculty_search_box = $(this);
        var search = '',
            id = $(this).closest('tr').attr('data-id'),
            type_for = $(this).attr('data-type'),
            c = $('#campus').val();
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'search-faculty', 'faculty': search, 'id': id, 'type': type_for, 'campus' : c},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    showModal({
                        name: 'basic',
                        title: 'Faculty',
                        content: result.html,
                        class: 'modal_faculty_result',
                        hasModalButton: false
                    });
                    $('#table-faculty-res').DataTable({ "aaSorting" : [] });
                }
            }
        );
        progressBar('off');
    });
    $('body').on('click', '.check-gender', function(e){
        var v = $(this).prop('checked');
        var tr = $(this).closest('tr').attr('data-id');
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'update-bygender', 'v': v, 'sched': tr },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    msgbox('success', result.message);
                }
            }
        );
        progressBar('off');
    });


    $('body').on('click', '.btn-faculty-select-btn', function(e){
        var f = $(this).attr('data-id'), id = $('#table-faculty-res').attr('data-key'), type_for = faculty_search_box.attr('data-for');
        var tr = $(this).closest('tr'), f_name = tr.children().eq(0).find('p').text(), f_rank = tr.children().eq(1).find('p').text();
        setAjaxRequest(
            base_url + page_url + 'event', {'event': 'update-faculty', 'f': f, 'sched': id, 'type': type_for , 'mode': faculty_search_box.attr('data-type') },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    msgbox('success', result.message);
                    faculty_search_box.html(f_name).attr({'data-id':f}).end();
                    faculty_search_box.closest('td').next().text(f_rank).end();
                    $('#modal_basic').modal('hide');
                }
            }
        );
        progressBar('off');
    });
    $('body').on('change', '#campus, #academic-year', function(e){
        reload_list();
    });

    function reload_list(){

        var c = $('#campus').val(), ac = $('#academic-year').val();

        setAjaxRequest( base_url + page_url + 'event', {'event': 'get', 'campus': c, 'ac-year': ac},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    var tbl = $('#records-table');
                    tbl.dataTable().fnDestroy();
                    $('#class-section').html(result.recs);
                    $('#records-table').DataTable({
                        "aaSorting": [],
                    });
                }
            }
        );
        progressBar('off');
    }

    $('body').on('click', '.btn-refresh', function(e){
        reload_list();
    });

    $('body').on('click', '.new-section', function (e) {

        setAjaxRequest(base_url + page_url + 'event', {'event': 'create-section','term': $('#academic-year').val() },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    showModal({
                        name: 'large',
                        title: 'Create Class Section',
                        content: result.html,
                        class: 'modal_section',
                        hasModalButton: false,

                    });

                    $('.modal_section').find('.btn_modal').addClass('save-section');

                    progressBar('off');
                }
            }
        );
        progressBar('off');
    });

    $('body').on('change', '#curriculum', function(e){
        setAjaxRequest(base_url + page_url + 'event', {'event': 'get-yearterm','curriculum': $(this).val() },
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    $('#yearterm').html(result.html);
                }
            }
        );
        progressBar('off');
    });


    $('body').on('change', '#yearterm', function(e){
        var c = $('#curriculum').val(), y = $(this).val();
        var l = $("#yearterm option:selected").attr('data-level');
        $('#yearlevel').val(l);
        load_subjects(c,y);
    });

    function load_subjects(curr_id, yearterm){
        setAjaxRequest(base_url + page_url + 'event', {'event': 'get-subjects','curriculum': curr_id, 'yearterm' : yearterm },function(result) {
            if (result.error) {
                msgbox('error', result.message);
            } else {
                $('#subjectlist').html(result.html);
            }
        });
        progressBar('off');
    }

    $('body').on('click', '.save-section', function(e){
        var data = $('#form_section').serialize();
        var subjs = [];

        $('#tblsubjects tbody tr').each(function(){
           if($(this).find('.chk-child').prop('checked')){
                subjs.push($(this).attr('data-subj'));
           }
        });

        if( isFormValid('#from_section') ){
            setAjaxRequest(base_url + page_url + 'event', 'event=save-section&subjects='+subjs+'&'+data ,function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    msgbox('success', result.message);
                    $('#modal_large').modal('hide');
                }
                progressBar('off');
            });
        }
    });

    $('body').on('click', '.scheduler', function(e){

        var section = $('#records-table tbody tr.active-row').find('.sec-name').html();
        var subj = $(this).closest('tr').find('td.scode').html();

        var data = {
            event : 'scheduler',
            term : $('#academic-year').val(),
            campus : $('#campus').val(),
            sched : $(this).closest('tr').attr('data-id'),
        };
        setAjaxRequest(base_url + page_url + 'event', data,
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    showModal({
                        name: 'large',
                        title: section + ' : ' + subj,
                        content: result.html,
                        class: 'modal_scheduler',
                        hasModalButton: false,
                    });

                    $('.modal_scheduler').find('.btn_modal').addClass('hide');
                    Metronic.init();
                    progressBar('off');
                    $('#teacher').select2({
                        placeholder: "Select an option",
                        allowClear: true
                    });

                    $('#room').select2({ placeholder: "Select an option", allowClear: true });
                    $('.timepicker-no-seconds').timepicker({ autoclose: true, minuteStep: 5});

                }
            }
        );
        progressBar('off');
        e.preventDefault();
    });

     $('body').on('click', '.delete-section', function(e){

         bootbox.confirm({
            size: 'small',
            message: "Delete this Section?",
            callback: function(yes) {
                if (yes) {
                    var data = {
                        event : 'delete-section',
                        section :  $('#records-table tbody tr.active-row').attr('data-id'),
                    };
                    setAjaxRequest(base_url + page_url + 'event', data , function(result) {
                        if (result.error) {
                            msgbox('error', result.message);
                        } else {
                            msgbox('success', result.message);
                            $('#records-table tbody tr.active-row').remove();
                            $('#modal_large').modal('hide');
                        }
                    });
                    progressBar('off');
                }
            }
        });

    });

    function clearDays(){
        $('.schedays').each(function(){
            $(this).prop('checked',false);
            $(this).parent().removeClass('checked');
        });
    }

    $('body').on('click', '#schedlist tbody tr', function(e){
        clearDays();
        //$('#tblconflict').html('');

        var stime = militaryToStandardTime($(this).attr('data-stime'));
        var etime = militaryToStandardTime($(this).attr('data-etime'));
        var days = $(this).attr('data-days');

        $('#timestart').val(stime);
        $('#timend').val(etime);

        $('#room').select2('val', $(this).attr('data-room'));
        $('#teacher').select2('val',$(this).attr('data-faculty'));

        for (var i = 0, len = days.length; i < len; i++) {
            $('#day'+days[i]).prop('checked',true);
            $('#day'+days[i]).parent().addClass('checked');
        }

    });

    $('body').on('click', '.schedays', function(e){
        display_schedule();
    });

    $('body').on('change', '#timestart,#timend', function(e){
        display_schedule();
    });

    $('body').on('change', '#room', function(e){
        $('#schedlist tbody tr.active-row').attr('data-room', $(this).val() );
        $('#schedlist tbody tr.active-row').find('td.room').html( $("#room option:selected").text());
    });

    $('body').on('change', '#teacher', function(e){
        $('#schedlist tbody tr.active-row').attr('data-faculty', $(this).val() );
        $('#schedlist tbody tr.active-row').find('td.teacher').html( $("#teacher option:selected").text());

    });

    function display_schedule(){
        var $r = $('#schedlist tbody tr.active-row');

        if($r.length == 0){
            $('#tblconflict').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> Please select a schedule mode!</div>')
            return false;
        }

         var sched='';
         var days="";
         $('.schedays').each(function(){
            if($(this).prop('checked')){
                sched += $(this).attr('data-code');
                days += $(this).val();
            }
        });

        if(days==''){
            $('#tblconflict').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> Please select a day(s) !</div>')
            return false;
        }

        sched += ' ' + $('#timestart').val() + ' - ' + $('#timend').val();
        $('#schedlist tbody tr.active-row').find('td.schedule').html('');
        $('#schedlist tbody tr.active-row').find('td.schedule').html(sched);
        $r.attr('data-sched',sched);
        $r.attr('data-days',days);
        $r.attr('data-stime', StandardTomilitaryTime($('#timestart').val()));
        $r.attr('data-etime', StandardTomilitaryTime($('#timend').val()));
    }

    $('body').on('click', '.update-sched', function(e){
        $('#tblconflict').html(' ');
        var $r = $(e.target).parent().parents();

        if(!valid_schedule()){ return false;}

        var data = {
            event : 'save-schedule',
            term : $('#academic-year').val(),
            section : $r.attr('data-section'),
            sched_id : $('#schedlist').attr('data-id'),

            mode : $r.attr('data-id'),
            days : $r.attr('data-days'),
            start : $r.attr('data-stime'),
            end : $r.attr('data-etime'),
            room : $r.attr('data-room'),
            teacher : $r.attr('data-faculty'),
            sched : $r.attr('data-sched'),

        };

        setAjaxRequest(base_url + page_url + 'event', data ,function(result) {
            if (result.error) {
                msgbox('error', result.message);
                $('#tblconflict').html(result.html);
                //console.log(result.html);
                //sysfnc.showAlert('#form_schedule','danger',result.html,true,true,true,'warning',10);
            } else {
                msgbox('success', result.message);
                $('#subj-records-table tbody tr.active-row').find('.scheduler').html($r.attr('data-sched'));
            }
            progressBar('off');
        });

    });

    function valid_schedule(){
        var $res = true;

        if( $('#timestart').val() == '' ){
            $('#tblconflict').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> Please select time start !</div>')
            $res = false;
        }

        return $res;
    }

    $('body').on('click','.adjust-limit',function(){
        var val = $(this).html();
       showModal({
            name: 'basic',
            title: "Adjust Class Limit",
            content: '<input type="text" maxlength="3" class="form-control numberonly" name="climit" id="climit" value="'+val+'">',
            class: 'modal_limit',
            hasModalButton: false,
        });
        $('.modal_rename').find('.btn_modal').removeClass('save-new-name');
        $('.modal_limit').find('.btn_modal').addClass('save-class-limit');
        $('.modal_limit').find('.btn_modal').attr('data-id', $(this).closest('tr').attr('data-id'));
    });

    $('body').on('click','.save-class-limit',function(){
        var sec  = $(this).attr('data-id');

        setAjaxRequest(base_url + page_url + 'event', 'event=adjust-class-limit&section='+sec +'&newsize=' + $('#climit').val() ,function(result) {
            if (result.error) {
                msgbox('error', result.message);
            } else {
                msgbox('success', result.message);
                $('.modal_limit').modal('hide');
                $('#records-table tbody tr.active-row').find('a.adjust-limit').html( $('#climit').val() );
            }
            progressBar('off');
        });

    });


    $('body').on('click','.rename',function(){

        var oldname = $('#records-table tbody tr.active-row').find('a.sec-name').html();

       showModal({
            name: 'basic',
            title: "Class Section Rename",
            content: '<label class="control-label">Old Name </label> <input type="text" maxlength="50" class="form-control input-medium" onkeydown="return false;" name="oldname" id="oldname" value="'+oldname+'"> <br/> <label class="control-label">New Class Name </label> <input type="text" maxlength="50" class="form-control input-medium" name="classnewname" id="classnewname" value="">',
            class: 'modal_rename',
            hasModalButton: false,
        });
        $('.modal_rename').find('.btn_modal').removeClass('save-class-limit');
        $('.modal_rename').find('.btn_modal').addClass('save-new-name');
        $('.modal_rename').find('.btn_modal').attr('data-id', $(this).closest('tr').attr('data-id'));
    });

    $('body').on('click','.save-new-name',function(){
        var sec = $('#records-table tbody tr.active-row').attr('data-id');

        setAjaxRequest(base_url + page_url + 'event', 'event=save-new-name&section='+sec +'&old='+ $('#oldname').val() +'&new=' + $('#classnewname').val() ,function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    msgbox('success', result.message);
                $('.modal_rename').modal('hide');
                $('#records-table tbody tr.active-row').find('a.sec-name').html( $('#classnewname').val() );

                }
                progressBar('off');
            });

    });

    $('body').on('click', '#replace-subj, #add-subj', function(e) {
        e.preventDefault();

        var subj_tbl = $('#subj-records-table'),
            act = $(this).attr('id').split('-'),
            suff = '',
            sec_name = $('a[href=#class-section-sched]').text(),
            cur_tr = subj_tbl.find('tr.active-row'),
            subj_code = cur_tr.find('td:first').text();

        if (!subj_code && act[0] == 'replace') {
            msgbox('warning', 'Please select a subject to replace.');
            return false;
        } else if(sec_name == 'None' && act[0] == 'add'){
            msgbox('warning', 'Please select a section before adding a subject.');
            return false;
        }

        setAjaxRequest(base_url + page_url + 'event', {
                'event': 'get-subject-replace',
                'action': act[0]
            },
            function(result) {
                if(act[0] == 'replace'){
                    suff = ' (' + subj_code + ')';
                }

                showModal({
                    name: 'basic',
                    title: act[0].charAt(0).toUpperCase() + act[0].substr(1) + ' Subject | ' + sec_name + suff,
                    content: result.html,
                    class: 'modal_repl_subj',
                    hasModalButton: false,
                });

                $('.modal_repl_subj').find('.btn_modal').addClass('hide');
                $('#tbl-subjects-replace').DataTable();

            }
        );

        progressBar('off');

    });

    $('body').on('click', '.subj-replace-btn', function(e){
        var _this = $(this),
            cur_tr = $('#subj-records-table tr.active-row'),
            cur_tds = cur_tr.children(),
            sched_id = cur_tr.data('id'),
            subj_id = _this.data('subj'),
            term = $('#academic-year').val();

        setAjaxRequest(base_url + page_url + 'event', {
                'event': 'save-subject-replace',
                'term': term,
                'sched': sched_id,
                'subj': subj_id
            },
            function(result) {
                var tds = _this.closest('tr').children(),
                    subj_code = tds.eq(0).text(),
                    subj_title = tds.eq(1).text();

                cur_tds.eq(0).text(subj_code);
                cur_tds.eq(1).text(subj_title);

                closeModal('basic');
                msgbox('success', result.message);
            }
        );
        progressBar('off');
    });

    $('body').on('click', '.subj-add-btn', function(e){
        var _this = $(this),
            cur_sec_tab = $('#class-section-sched'),
            sec_id = cur_sec_tab.data('id'),
            subj_id = _this.data('subj'),
            term = $('#academic-year').val();

        setAjaxRequest(base_url + page_url + 'event', {
                'event': 'save-added-subject',
                'term': term,
                'sec': sec_id,
                'subj': subj_id
            },
            function(result) {
                if(result.error){
                    msgbox('error', result.message);

                } else {
                    $('#class-section-sched').html(result.html);

                    $('#subj-records-table').DataTable({"aaSorting": [], 'autoWidth': false});

                    closeModal('basic');
                    msgbox('success', result.message);
                }

            }
        );
        progressBar('off');
    });

    $('body').on('click', '#remove-subj', function(e) {
        e.preventDefault();
        var cur_tr = $('#subj-records-table').find('tr.active-row'),
            subj_code = cur_tr.find('td:first').text();

        if(typeof cur_tr.attr('data-id') !== 'undefined' ){
            bootbox.confirm({
                size: 'small',
                message: "Delete the subject " + subj_code + "?",
                callback: function(yes) {
                    if (yes) {
                        var data = {
                            event : 'delete-subject',
                            sched :  cur_tr.attr('data-id'),
                        };

                        setAjaxRequest(base_url + page_url + 'event', data , function(result) {
                            msgbox('success', result.message);
                            cur_tr.remove();
                        });

                        progressBar('off');
                    }
                }
            });
        } else {
            msgbox('warning', 'Please select a subject to delete.');
        }

        progressBar('off');

    });

    $('body').on('change', '#program', function(e){
        var val = $(this).val();

        $('#curriculum').removeAttr('disabled').children().addClass('hide');
        $('#curriculum option[data-prog="'+ val +'"]').removeClass('hide');
    });

    $('body').on('click', '#print-sec-sched', function(e){
        e.preventDefault();
        var _id = $('#records-table tbody tr.active-row').attr('data-id');

        if(_id){
            window.open(window.location + '/section_schedule?type=section&id=' + _id, '_blank');
        } else {
            msgbox('warning', 'Please select a section with schedules to print.');
        }

    });

    $('body').on('click', '.btn-faculty-print-btn', function(e){
        e.preventDefault();
        var acad_term = $('#academic-year').val(),
            _id = $(this).attr('data-id');

        window.open(window.location + '/section_schedule?type=faculty&term=' + acad_term + '&id=' + _id, '_blank');
    });

    reload_list();

});
