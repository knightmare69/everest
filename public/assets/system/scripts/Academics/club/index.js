var 
    isEdit = false,
    StudentNo = '',
    Campus = '',
    CampusID = '',
    TermID = '',
    CampusID = '',
    ClubID = '',
    Term = '',
    Section = '',
    ProgID = '';

var  Me = function() {

	var handleActionButtons = function() {

         //handle attaendance saving
        jQuery('body').on('click','.btnSaveClub', function() {
            var self = jQuery(this);
            if (!isFormValid('#FormClub')) {
                return msgbox('error','Please complete all required fields.');
            }

            confirmEvent("Are you sure you want to save this?", function(yes) {
                if (!yes) return;
                onProcess('',self,'Saving...');
                setAjaxRequest(
                    base_url+page_url+'event',
                    'event=saveClub&isEdit='+isEdit+'&ClubID='+ClubID+'&'+jQuery('#FormClub').serialize(),
                    function(result){
                        if (result.error == false) {
                            if (!isEdit) {
                                clear_value('#FormClub');
                            }
                            showClubs();
                            closeModal('.modal');
                            msgbox('success',result.message);
                        } else {
                             msgbox('error',result.message);
                        }
                       
                        onProcess('',self,'Save',false);
                        progressBar('off');
                    },
                    function() {
                        progressBar('off');
                        onProcess('',self,'Save',false);
                        msgbox("error","There was an error occurd");
                    }
                );
            });
        });
    
        //handle  delete attaendance
        jQuery('#BtnDelete').click(function() {
            var self = jQuery(this);
            if (!ClubID) {
                return msgbox('error','Please select Club.');
            }

            confirmEvent("Are you sure you want to delete this?", function(yes) {
                if (!yes) return;
                onProcess('',self,'Deleting...');
                setAjaxRequest(
                    base_url+page_url+'event',
                    'event=delete&ClubID='+ClubID,
                    function(result){
                        if (result.error == false) {
                            ClubID = '';
                            showClubs();
                            showBothStudents();
                            msgbox('success',result.message);
                        } else {
                            msgbox('error',result.message);
                        }
                        onProcess('',self,'Delete',false);
                        progressBar('off');
                    },
                    function() {
                        progressBar('off');
                        onProcess('',self,'Delete',false);
                        msgbox("error","There was an error occurd");
                    }
                );
            });
        });

        jQuery('#BtnReset').click(function() {
            if (!StudentNo) {
               return resetForm();
            }
            confirmEvent("Are you sure you want to reset this?", function(yes) {
                if (!yes) return;
               resetForm();
            });
        });

		jQuery('#BtnFilter').click(function() {
			var result = ajaxRequest(base_url+page_url+'event','event=showFilter');

			if (result.error == true) {
				msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
			}

			showModal({
                name: 'basic',
                title : 'Filter',
                content: result.content,
                class: 'modal_filter',
                button: {
                	class: 'btnSearch',
                	caption: 'Search'
                }
            });
            jQuery('.filter').trigger('change');
            modifyModalBtnClass('btnSearch');
		});

        jQuery('#BtnCreate').click(function() {
            var self = jQuery(this);
            var result = ajaxRequest(base_url+page_url+'event','event=showClubModal&ClubID='+ClubID);

            if (result.error == true) {
                msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
            }

            showModal({
                name: 'basic',
                title : (isEdit? 'Update' : 'Create')+' Club',
                content: result.content,
                button: {
                    class: 'btnSaveClub',
                    caption: 'Save'
                }
            });

            FN.multipleSelect();
            modifyModalBtnClass('btnSaveClub');
            showPrograms('#FormClub',jQuery('#FormClub #Program'));
        });

        jQuery('#btnMoveToClub').click(function() {
            if (!ClubID);

            if (getStudentWithoutClubs().length <= 0) {
                return msgbox('error','Please select student/s');
            }

            confirmEvent("Are you sure you want to move this to club?", function(yes) {

                if (!yes) return;

                var result = ajaxRequest(base_url+page_url+'event','event=moveToClub&ClubID='+ClubID+'&data='+JSON.stringify(getStudentWithoutClubs()));

                if (result.error == true) {
                    msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
                } else {
                    msgbox('success',result.message);
                    clearProfile();
                    showBothStudents();
                }
            });
        });

         jQuery('#btnRmoveFromClub').click(function() {
            if (!ClubID);

            if (getStudentWithClubs().length <= 0) {
                return msgbox('error','Please select student/s');
            }

            confirmEvent("Are you sure you want to remove this from club?", function(yes) {

                if (!yes) return;

                var result = ajaxRequest(base_url+page_url+'event','event=removeFromClub&ClubID='+ClubID+'&data='+JSON.stringify(getStudentWithClubs()));

                if (result.error == true) {
                    msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
                } else {
                    msgbox('success',result.message);
                    clearProfile();
                    showBothStudents();
                }
            });
        });
	}

	var handleFilterEvents = function() {

		//handle search
		jQuery('body').on('click','.btnSearch',function() {

			var self = jQuery(this);

			if (!isFormValid('#FormFilter')) {
				return msgbox('error','Please complete all fields');
			}

			onProcess('',self,'Searching...');
			setAjaxRequest(
				base_url+page_url+'event',
				'event=loadClubs&'+jQuery('body #FormFilter').serialize()+'&ClubID='+ClubID,
				function(result) {
					if (result.error == false) {
						jQuery('#TableClubWrapper').html(result.content);
						jQuery(':checkbox').uniform();
                        setActiveFilter();
                        showActiveFilter();
                        closeModal('.modal_filter');

					} else {
						msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
					}
					onProcess('',self,'...',false);
					progressBar('off');
				},
				function(error) {
					onProcess('',self,'...',false);
					progressBar('off');
				}
			);

            hideFilter();
            filterToggle();
			
            modifyModalBtnClass('btnSearch');
		});

		//handle year level load sections
		jQuery('body').on('change','.filter',function() {
            showPrograms();
		});

        //handle year level load sections
        jQuery('body').on('change','.Clubfilter',function() {
            showPrograms('#FormClub',jQuery('#FormClub #Program'));
        });

        jQuery('.showFilter').click(function() {
            filterToggle();
        });

        jQuery('body').on('change','#Program',function() {
            jQuery('#ProgramSel').val(jQuery(this).val());
        });
	}

	var handleSelections = function() {

        //handle row selection double click
        jQuery('body').on('click','.clubRowSel', function() {
            var self = $(this), isChecked = false;

            jQuery('table tbody tr td .chk-child').prop('checked',false);

            if (self.hasClass('row-selected')) {
                self.removeClass('row-selected');
                clearProfile();
                changeCreateClubStateToCreate();
                isEdit = false;
                ClubID = '';
                showBothStudents();
            } else {
                jQuery('.clubRowSel').removeClass('row-selected');
                self.addClass('row-selected');
                ClubID = self.attr('data-clubid');
                isChecked = true;
                isEdit = true;
                changeCreateClubStateToEdit();
                showBothStudents();
            }
            self.find('.chk-child').prop('checked',isChecked);
            jQuery(':checkbox').uniform();
        });

        //handle row selection double click
        jQuery('body').on('click','#TableWithoutClubStudents tbody tr', function() {
            var self = $(this), isChecked = false;

            if (self.hasClass('row-selected')) {
                self.removeClass('row-selected');
                clearProfile();
            } else {
                jQuery('#TableWithoutClubStudents tbody tr').removeClass('row-selected');
                self.addClass('row-selected');
                showProfile(self);
                isChecked = true;
            }

            self.find('.chk-child').prop('checked',isChecked);
            jQuery(':checkbox').uniform();
        });

        //handle row selection double click
        jQuery('body').on('click','#TableWithClubStudents tbody tr', function() {
            var self = $(this), isChecked = false;

            if (self.hasClass('row-selected')) {
                self.removeClass('row-selected');
                clearProfile();
            } else {
                jQuery('#TableWithClubStudents tbody tr').removeClass('row-selected');
                self.addClass('row-selected');
                showProfile(self);
                 isChecked = true;
            }

            self.find('.chk-child').prop('checked',isChecked);
            jQuery(':checkbox').uniform();
        });
        

		//handle attendance  double selection
		jQuery('body').on('dblclick','.attendanceSel', function() {
			var self = $(this);

            AttendanceKey = self.closest('tr').attr('data-attendancekey');

			var result = ajaxRequest(base_url+page_url+'event','event=showAttendanceModal&AttendanceKey='+AttendanceKey);

			if (result.error == true) {
				msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
			}

			showModal({
                name: 'basic',
                title : 'Attendance',
                content: result.content,
                class: 'modal_filter',
                button: {
                	class: 'btnSaveAttendance',
                	caption: 'Save'
                }
            });

            isEdit = true;
            FN.datePicker();
            FN.datePickerTimeNoSeconds();
            setActiveAttendanceTab(self);
            disableNotActiveTab();
            self.addClass('row-selected');
            modifyModalBtnClass('btnSaveAttendance');
            self.closest('table').find('.rowSel').removeClass('row-selected');
		});

        //handle attendance  double selection
        jQuery('body').on('click','.attendanceSel', function() {
            var self = $(this);
            AttendanceKey = self.closest('tr').attr('data-attendancekey');
            if (self.hasClass('row-selected')) {
                self.removeClass('row-selected');
            } else {
                self.closest('table').find('tr').removeClass('row-selected');
                self.addClass('row-selected');
            }
        });		
	}

    var handleAutoLoad = function() {
        showPrograms();
        filterToggle();
        setActiveFilter();
    }

	return {
		init: function() {

            handleAutoLoad();

            clearProfile();

			handleActionButtons();

			handleFilterEvents();

			handleSelections();
		}
	}
}();