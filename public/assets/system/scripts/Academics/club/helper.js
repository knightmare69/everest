function showBothStudents() {
    showStudentsWithoutClubs();
    showStudentsWithClubs();
}

function showStudentsWithoutClubs() {
    dataTableLogs(
        '#TableWithoutClubStudents',
        base_url+page_url+'event?event=showStudentsWithoutClubs&Program='+ProgramID+'&SchoolCampus='+CampusID+'&SchoolYear='+TermID+'&ClubID='+ClubID
    );
    jQuery('#TableWithoutClubStudents_wrapper .row .col-sm-12').removeClass('col-md-8');
    jQuery('#TableWithoutClubStudents_wrapper .row .col-sm-12').addClass('col-md-12');
}

function showStudentsWithClubs() {
    dataTableLogs(
        '#TableWithClubStudents',
        base_url+page_url+'event?event=showStudentsWitClubs&Program='+ProgramID+'&SchoolCampus='+CampusID+'&SchoolYear='+TermID+'&ClubID='+ClubID
    );
    jQuery('#TableWithClubStudents_wrapper .row .col-sm-12').removeClass('col-md-8');
    jQuery('#TableWithClubStudents_wrapper .row .col-sm-12').addClass('col-md-12');
}

function showPrograms(form,ProgramEl) {
    if (!isFormValid('#FormFilter',false)) return;

    var form = form == undefined ? '#FormFilter' : form;

    setAjaxRequest(
        base_url+page_url+'event',
        'event=loadPrograms&'+jQuery('body '+form).serialize(),
        function(result) {
            if (result.error == false) {
                setPrograms(result.data,ProgramEl);
            } else {
                msgbox('error',result.message ? result.message : 'There was an error occurd. Try again.');
            }
            progressBar('off');
        },
        function(error) {
            progressBar('off');
        },
        'json',true,false
    );
}

function setPrograms(data,el) {
    var option = document.createElement('option');
    var options = "<option value=''>- SELECT - </option>";
    var el = el == undefined ? jQuery('#Program') : el;
    for(var i in data) {
        option = document.createElement('option');
        option.setAttribute('value',data[i].ProgID)
        option.appendChild(document.createTextNode(data[i].Program));

        if (jQuery('#ProgramSel').val() == data[i].ProgID) {
            option.setAttribute('selected','selected');
        }

        options += option.outerHTML;
    }

    el.html(options);
}

function modifyModalBtnClass(myClass) {
    var classes = ['btnSearch','btnSaveAttendance'];
    for(var i = 0;i<=classes.length;i++) {
        jQuery('.modal .btn_modal').removeClass(classes[i]);
    }
    jQuery('.modal .btn_modal').addClass(myClass);
}

function setActiveAttendanceTab(self) {
    jQuery('#AttendanceTabs ul li').each(function() {
        var aSelf = jQuery(this);
        if (aSelf.find('a').attr('data-type') == getAttendanceType(self)) {
            aSelf.find('a').trigger('click');
        }
    });
}

function disableNotActiveTab() {
    jQuery('#AttendanceTabs ul li').each(function() {
        var self = jQuery(this);
        if (!self.hasClass('active')) {
            self.find('a').attr('data-toggle','');
        }
    });
}

function ifNull(data) {
	if (data == null || !data) {
		return '';
	}
	return data;
}

function filterToggle() {
    if (jQuery('#isShowFilter').is(':checked')) {
        jQuery('.filter_wrapper').animate({'opacity': 'show'},'fast');
    } else {
        jQuery('.filter_wrapper').animate({'opacity': 'hide'},'fast');
    }
}

function hideFilter() {
   jQuery('#isShowFilter').prop('checked',false);
   jQuery(':checkbox').uniform();
}

function showActiveFilter() {
    jQuery('#SchoolYearTitle u').html(Term);
    jQuery('#ProgramTitle u').html(Program);
}

function clearActiveFilter() {
    jQuery('#SchoolYearTitle u').html('School Year');
    jQuery('#SectionTitle u').html('Section');
}

function resetForm() {
    clearProfile();
    clearStudentsList();
    clearActiveFilter();
    clearSelectedProfile();
    clearSelectedAttendance();
    changeCreateClubStateToCreate();
    isEdit = false;
    ClubID = '';
}

function setActiveFilter() {
    Campus = jQuery('#SchoolCampus option:selected').text();
    CampusID = jQuery('#SchoolCampus option:selected').val();

    Term = jQuery('#SchoolYear option:selected').text();
    TermID = jQuery('#SchoolYear option:selected').val();

    Program = jQuery('#Program option:selected').text();
    ProgramID = jQuery('#Program option:selected').val();
}

function getAttendanceType(self) {
    return self.closest('table').attr('data-type')
}

function getType() {
	return jQuery('#AttendanceTabs ul li.active a').attr('data-type');
}

function getCurrentActiveForm() {
    return '#'+jQuery('#AttendanceTabs .tab-content .active form').attr('id');
}

function clearSelectedProfile() {
    jQuery('#TableMasterList tbody tr').removeClass('row-selected');
    jQuery('#TableMasterList tbody tr td .chk-child').prop('checked',false);
    jQuery(':checkbox').uniform();
}

function clearSelectedAttendance() {
    jQuery('#TableAttendanceWrapper tbody tr').removeClass('row-selected');
}

function clearStudentsList() {
    jQuery('#TableMasterList tfoot').html('<tr><td colspan="6" class="center bold">No Record found.</td></tr>');
}

function clearProfile() {

    //clear info
    jQuery('#Name').text('');
    jQuery('#StudentNo').text('');
    jQuery('#Gender').text('');
    jQuery('#BirthDate').text('');

    isEdit = false;
    
    jQuery('#StudentPhoto').attr('src',base_url+'general/getStudentPhotoByStudentNo?StudentNo=');
}

function showProfile(el) {
    var el = el.closest('tr');
	//set up student info
    jQuery('#Name').text(el.find('td').eq(2).text());
    jQuery('#StudentNo').text(el.find('td').eq(1).text());
    jQuery('#Gender').text(el.find('td').eq(3).text());
    // jQuery('#BirthDate').text(Profile.DateofBirth);

    //show student photo
    jQuery('#StudentPhoto').attr('src',base_url+'general/getStudentPhotoByStudentNo?StudentNo='+el.find('td').eq(1).text());
}

function getStudentWithoutClubs() {
    var data = [];
    jQuery('#TableWithoutClubStudents tbody tr').each(function() {
        var self = jQuery(this);
        if (self.find('.chk-child').is(':checked')) {
             data.push({
                StudentNo: self.find('td').eq(1).text().trim(),
                RegID: self.find('.chk-child').attr('data-regid'),
            });
        }
    });
    return data;
}

function getStudentWithClubs() {
    var data = [];
    jQuery('#TableWithClubStudents tbody tr').each(function() {
        var self = jQuery(this);
        if (self.find('.chk-child').is(':checked')) {
             data.push({
                StudentNo: self.find('td').eq(1).text().trim(),
                RegID: self.find('.chk-child').attr('data-regid'),
            });
        }
    });
    return data;
}

function changeCreateClubStateToEdit() {
    jQuery('#BtnCreate').html('<i class="fa fa-file"></i> Edit Club');
}

function changeCreateClubStateToCreate() {
    jQuery('#BtnCreate').html('<i class="fa fa-file"></i> Create Club');
}

function showClubs() {
    jQuery('.btnSearch').trigger('click');
}