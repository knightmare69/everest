var ClassSectioning = {
    getSearch: function(section_id, with_section) {
        var data = $('.search-form').serialize();
        var with_section = (with_section) ? 1 : 0;
        var yl_selected = $('#srch-yl option:selected'),
            yl_text = yl_selected.text();

        data += '&srch-yl=' + yl_selected.attr('data-id') + '&prog=' + yl_selected.attr('data-prog') + '&college=' + yl_selected.attr('data-col');

        setAjaxRequest(
            base_url + page_url + '/event', 'event=search&' + data + '&with_section=' + with_section + '&section=' + section_id,
            function(req) {
                if (req.error) {
                    showError(req.message);
                } else {

                    var tbl;

                    if (with_section == 1) {
                        tbl = 'with-section-tbl';
                        var sec_caption = ($('.jstree-clicked').text() != '') ? $('.jstree-clicked').text() : $('#transfer-section-options option:selected').text();
                        $('.nav-tabs li a[href="#student-result-holder2"]').html(sec_caption + ' (<span id="stud-total-count">' + req.total + '</span>)').click();
                        $('#student-result-holder2').html(req.students);

                    } else {
                        tbl = 'no-section-tbl';
                        _a = $('a[href="#student-result-holder1"]');

                        $('#with-section-tbl').DataTable().clear().draw();
                        _a.click().parent().next().find('a').text('With Section');

                        $('#section-level').text(yl_text);

                        $('#section-result-holder').html(req.sections);
                        $('#student-result-holder1').html(req.students);

                        $('#div-sec-action').removeClass('hide');

                        $('#sections-jstree').jstree({
                            "core": {
                                // so that create works
                                "check_callback": true,
                            }
                        });
                    }

                    Metronic.init();
                    $('#' + tbl).dataTable({
                        "pageLength": 50,
                        "columnDefs": [{
                            "orderable": false,
                            "targets": 0,
                            'width': '20px'
                        }, {
                            "targets": 2,
                            'width': '80px'
                        }, {
                            'targets': 3,
                            'width': '200px'
                        }, {
                            'targets': 6,
                            'width': '150px'
                        }],
                        'aaSorting': [
                            [3, 'asc']
                        ]
                    });

                    ClassSectioning.vars.selected_section_label = yl_text;
                    ClassSectioning.vars.jstree_selectd_section = 0;

                    if(yl_text == 'Grade 11' || yl_text == 'Grade 12'){
                        ClassSectioning.vars.is_senior_high = true;
                    } else {
                        ClassSectioning.vars.is_senior_high = false;
                    }
                }
            }
        );
        progressBar('off');
    },
    save: function() {
        var gl = $('#srch-yl option:selected');
        var data = $('.section-form').serialize() + '&section-grade-level=' + gl.attr('data-id');
        data += '&prog=' + gl.attr('data-prog') + '&college=' + gl.attr('data-col') + '&term=' + $('#srch-ay').val() + '&campus=' + $('#srch-campus').val();

        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to save this section?",
            callback: function(yes) {
                if (yes) {
                    setAjaxRequest(
                        base_url + page_url + '/event', 'event=save&' + data,
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                showSuccess(result.message);

                                if (gl.text() == $('#section-level').text()) {
                                    $('#sections-jstree').jstree().create_node('#', {
                                        "text": $('#section-name').val(),
                                        'li_attr': {
                                            'data-id': result.return_id
                                        }
                                    }, "first", false);
                                }

                                if ($('#check-addnew').is(':checked')) {
                                    ClassSectioning.resetForm();
                                } else {
                                    closeModal('basic');
                                    $('.alert').addClass('hide');
                                }
                            }
                        }
                    );
                    progressBar('off');
                }
            }
        });
    },
    update: function(id) {
        var gl = $('#srch-yl option:selected');
        var data = $('.section-form').serialize() + '&section-grade-level=' + gl.attr('data-id');
        data += '&prog=' + gl.attr('data-prog') + '&college=' + gl.attr('data-col') + '&term=' + $('#srch-ay').val() + '&campus=' + $('#srch-campus').val();

        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to update this section?",
            callback: function(yes) {
                if (yes) {
                    setAjaxRequest(
                        base_url + page_url + '/event', 'event=update&' + data + '&id=' + id,
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                showSuccess(result.message);

                                if ($('#check-addnew').is(':checked')) {
                                    ClassSectioning.resetForm();
                                } else {
                                    closeModal('basic');
                                    $('.alert').addClass('hide');
                                }

                                $('li[data-id="' + id + '"]').find('a').html('<i class="jstree-icon jstree-themeicon"></i>' + $('#section-name').val());
                            }
                        }
                    );
                    progressBar('off');
                }
            }
        });
    },
    delete: function(id) {
        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to remove this section?",
            callback: function(yes) {
                if (yes) {
                    setAjaxRequest(
                        base_url + page_url + '/event', 'event=delete&id=' + id,
                        function(result) {
                            if (result.error) {
                                msgbox('error', result.message);
                            } else {
                                msgbox('success', result.message);
                                $('li[data-id="' + id + '"]').remove();
                            }
                        }
                    );
                    progressBar('off');
                }
            }
        });
    },
    transfer: function(section_id, checked_holder_id) {
        var exempt        = $(".optsubj").attr('data-exempt');
        var students      = $(checked_holder_id).find('input').serialize(),
            total_checked = $(checked_holder_id).find('.check-child:checked').length;
            exempt        = ((exempt==undefined || exempt=='')?"":exempt);
        students += '&section=' + section_id + '&is_second_sec=' + ($('#is_second_class').is(':checked') ? 1 : 0) + '&term=' + $('#srch-ay').val()+'&exempt='+exempt;
        
        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to transfer this students?",
            callback: function(yes) {
                if (yes) {

                    setAjaxRequest(
                        base_url + page_url + '/event', 'event=transfer&' + students,
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                showSuccess(result.message);
                                var stud_total;

                                if ($('#transfer-section-options').val() == '') {
                                    ClassSectioning.getSearch(ClassSectioning.vars.jstree_selectd_section, 0);
                                } else if (ClassSectioning.vars.jstree_selectd_section == section_id) {
                                    ClassSectioning.getSearch(ClassSectioning.vars.jstree_selectd_section, 1);
                                    stud_total = ($('#stud-total-count').text() - total_checked);
                                }

                                $('#stud-total-count').text(stud_total);

                                $(checked_holder_id).find('.check-child:checked').closest('tr').remove();
                                $('.alert').addClass('hide');
                                closeModal('basic2');
                            }
                        }
                    );
                    progressBar('off');
                }
            }
        });
    },
    showForm: function(id) {
        var id = (id == undefined ? '' : id);
        var campus = $('#srch-campus option:selected').text();
        var acad_year = $('#srch-ay option:selected').text();

        showModal({
            name: 'basic',
            title: 'Section Form | ' + campus + ' (' + acad_year + ')',
            content: ajaxRequest(base_url + page_url + '/event', 'event=form&id=' + id, 'HTML'),
            class: 'modal_examinee',
            button: {
                icon: 'fa fa-check',
                class: 'btn green btn-section-form-save',
                caption: 'Save',
            }
        });

        if ($('#is_second_class').is(':checked')) {
            $('#section-name2').prop('disabled', false);
        } else {
            $('#section-name2').prop('disabled', true);
        }
        var yl = $('#srch-yl option:selected'), yl_text = yl.text();
        $('#section-gl-text').text(yl_text);
        $('#modal_basic').find('.default').html('<i class="fa fa-times"></i> Close');
        $('.section-form select').select2();
        Metronic.initUniform();
    },
    showFormTransfer: function(checked_holder_id) {

        var _table = $(checked_holder_id + ' table').dataTable(),
            get_checked = $('input[type="checkbox"]:checked', _table.fnGetNodes()),
            elem = '',
            get_sections = $('.jstree-leaf'),
            section_elem = '';

        if (get_checked.length == 0) {
            msgbox('info', 'Please select student to transfer.');
        } else {

            get_checked.each(function() {
                var _this = $(this).closest('tr').html();
                elem += '<tr class="students-row-added">' + _this + '</tr>';
            });

            get_sections.each(function(e) {
                var this_text = $(this).find('.jstree-anchor').text();
                if ($('a[href="#student-result-holder1"]').parent().hasClass('active')) {
                    section_elem += '<option value="' + $(this).attr('data-id') + '">' + this_text + '</option>';
                } else if ($('.jstree-clicked').text() != this_text) {
                    section_elem += '<option value="' + $(this).attr('data-id') + '">' + this_text + '</option>';
                }
            });

            if ($('a[href="#student-result-holder1"]').parent().hasClass('active') == false) {
                section_elem = '<option value="">None</option>' + section_elem;
            }

            $("#transfer-table").dataTable().fnDestroy();
            var sh_th = $('#transfer-table thead th.th-full-name');
            if(ClassSectioning.vars.is_senior_high){
                if(sh_th.next().hasClass('th-sh') == false){
                    // sh_th.after('<th class="th-sh">Strand/Track</th>');
                }
            } else {
                sh_th.next('.th-sh').remove();
            }
            $('#transfer-table tbody').empty().append(elem).find('.students-row-added td:first-child').addClass('hide');
            $('.student-transfer-section select').html(section_elem);

            Metronic.init();
            $('#transfer-table').dataTable({
                "pageLength": 50,
                "columnDefs": [{
                    "targets": 2,
                    'width': '80px'
                }, {
                    'targets': 3,
                    'width': '200px'
                }, {
                    'targets': 4,
                    'width': '55px'
                }, {
                    'targets': 5,
                    'width': '25px'
                }, {
                    'targets': 6,
                    'width': '150px'
                }],
                'aaSorting': [
                    [3, 'asc']
                ]
            });

            // $('#transfer-section-options').select2("destroy").select2();
            $('.alert').addClass('hide');
            showModal({
                name: 'basic2',
                title: 'Student Section',
                content: $('.student-transfer-section').html(),
                button: {
                    icon: 'icon-action-redo',
                    class: 'btn green btn-transfer-save',
                    caption: 'Transfer',
                }
            });
        }
    },
    resetForm: function() {
        $('#section-name2, #section-name, #section-limit').val('');
        $('#section-name').focus();
        ClassSectioning.vars.jstree_selectd_section = 0;
    },
    vars: {
        selected_section_label: '',
        jstree_selectd_section: 0,
        is_senior_high: false,
    }
}

$(document).ready(function() {

    $(".bootstrap-dialog").removeAttr("tabindex");

    $('.search-selec2').select2({
        // width: 'resolve'
        dropdownAutoWidth : true,
        width: 'auto'
    });

    $('#sections-jstree').jstree();
    $('table').dataTable();

    $('body').on('click', '.btn-section-form-save', function() {
        if (isFormValid('.section-form')) {
            if (ClassSectioning.vars.jstree_selectd_section == 0) {
                ClassSectioning.save();
            } else {
                ClassSectioning.update(ClassSectioning.vars.jstree_selectd_section);
            }
        }
    });

    $('body').on('click', '#btn-search', function() {
        ClassSectioning.getSearch('', false);
    });

    // add-edit-delete
    $('body').on('click', '.jstree-anchor', function(e) {
        ClassSectioning.vars.jstree_selectd_section = $(this).parent().attr('data-id');
        ClassSectioning.getSearch(ClassSectioning.vars.jstree_selectd_section, true);
    });

    $('body').on('click', '#section-new', function() {
        ClassSectioning.vars.jstree_selectd_section = 0;
        ClassSectioning.showForm();
    });

    $('body').on('click', '#section-modify', function() {
        var get_selected = $('#sections-jstree').jstree(true).get_selected('full', true);

        if (get_selected.length == 0){
            msgbox('info', "There's no list of section.");
        } else if (get_selected[0]['li_attr']['data-id'] == undefined || get_selected[0]['li_attr']['data-id'] == 0) {
            msgbox('info', 'Please select a section.');
        } else {
            ClassSectioning.vars.jstree_selectd_section = get_selected[0]['li_attr']['data-id'];
            ClassSectioning.showForm(ClassSectioning.vars.jstree_selectd_section);
        }
    });

    $('body').on('click', '#section-remove', function() {
        var get_selected = $('#sections-jstree').jstree(true).get_selected('full', true);

        if (get_selected.length == 0){
            msgbox('info', "There's no list of section.");
        } else if (get_selected[0]['li_attr']['data-id'] == undefined || get_selected[0]['li_attr']['data-id'] == 0) {
            msgbox('info', 'Please select a section.');
        } else {
            ClassSectioning.vars.jstree_selectd_section = get_selected[0]['li_attr']['data-id'];
            ClassSectioning.delete(ClassSectioning.vars.jstree_selectd_section);
        }
    });
    // end

    $('body').on('click', '.paginate_button a', function(e) { Metronic.init();});

    $('body').on('click', '.nav-tabs li a', function() {
        $('#section-student-transfer').attr('data-tab-open', $(this).attr('href'))
    });

    $('body').on('click', '#section-student-transfer', function(e) {
        var check_holder_id = $(this).attr('data-tab-open');
        ClassSectioning.showFormTransfer(check_holder_id);
        $('#transfer-section-options').trigger('change');
    });

    $('body').on('click', '.btn-transfer-save', function(e) {
        var check_holder_id = $('#section-student-transfer').attr('data-tab-open');
        var sec_select = '';
        $('#transfer-section-options option').each(function(e) {
            if ($(this).is(':selected')) {
                sec_select = $(this).attr('value');
            }
        });
        ClassSectioning.transfer(sec_select, check_holder_id);
    })

    $('body').on('click', '#check-parent', function(e) {
        var check_child = $(this).closest('table').find('.check-child');
        if ($(this).is(':checked')) {
            check_child.each(function() {
                $(this).attr('checked', true).parent().addClass('checked')
            });

        } else {
            check_child.attr('checked', false).parent().removeClass('checked');
        }
    });

    // $('body').on('change', '#srch-ay', function() {
    //     location.replace(base_url+page_url+'?t='+ $(this).val());
    // });

    $('body').on('change', '#srch-ay', function(){
        var txt = $(this).find('option:selected').text(),
            opts = $('#srch-yl');

        if(txt.indexOf('School Year') !== -1){
            opts.find('option[data-by-sem=0]').removeClass('hide');
            opts.find('option[data-by-sem=1]').addClass('hide');
        } else {
            opts.find('option[data-by-sem=0]').addClass('hide');
            opts.find('option[data-by-sem=1]').removeClass('hide');
        }
    });

    $('body').on('change', '#section-building', function(e){
        var _id = $(this).val();

        $('#section-room').children().addClass('hide').end().find('option[data-bldg="' + _id + '"]').removeClass('hide');
    });

    
    $('body').on('change','#transfer-section-options',function(){
        var termid  = $('#srch-ay').val();
        var yrlvl   = $('#srch-yl').find('option:selected').attr('data-id');
        var progid  = $('#srch-yl').find('option:selected').attr('data-prog');
        var section = $(this).val();        
        $('.optsubj').closest('.row').addClass('hidden');
        progressBar('on');
        setAjaxRequest(
            base_url+'enrollment/sectioning/event?event=optional',
            {termid:termid,yrlvl:yrlvl,progid:progid,section:section},
            function(r)
            {
              $('.optsubj').closest('.row').removeClass('hidden');
              $('.optsubj').html(r.content);
              progressBar('off');
            },
            function(err)
            {
              confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});   
              progressBar('off');
            }
        ); 
    });
    
    $('body').on('click','.optionaldata',function(){
        var name  = $(this).attr('name');
        $('.optsubj').find('[name="'+name+'"]').removeClass('selected');
        $(this).addClass('selected');
        
        var data  = [];
        $('.optionaldata').each(function(){
          var target = $(this).attr('value');
          var x = data.indexOf(target);
          if($(this).is('.selected')){
            if(x > -1){data.splice(x,1); }
          }else{
            if(x == -1){data.push(target); } 
          }
        });
        $(".optsubj").attr('data-exempt',data.join("|"));

    });

});
