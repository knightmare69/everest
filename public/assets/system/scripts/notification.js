var Noti = function() {
   var  InboxID = '', ToID = '';
    var handleCompose = function() {
        jQuery('body').on('click','.BtnMessageReply', function() {
            var self = jQuery(this);
            showModal({
                name: 'large',
                title : 'Message',
                content: ajaxRequest(base_url+'notification/event','event=MessageCompose&InboxID='+InboxID+'&ToID='+ToID,'html'),
                class: 'modal_compose_message',
                button: {
                    hasActionButton: false
                }
            });
           
            initFileUpload();        
            initWysihtml5();
            closeModal('.modal_show_message');
        });

        jQuery('body').on('click','.SystemSendMessage', function() {
            var self = jQuery(this);
            setAjaxRequest(
                base_url+'notification/event',
                'event=reply&'+jQuery('.SystemInboxComposeForm').serialize()+'&InboxID='+InboxID+'&ToID='+ToID,
                function() {

                }
            );
        });
        
        function initWysihtml5() {
            jQuery('.inbox-wysihtml5').wysihtml5({
                "stylesheets": ["assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        function initFileUpload() {
            jQuery('#fileupload').fileupload({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: 'assets/global/plugins/jquery-file-upload/server/php/',
                autoUpload: true
            });

            // Upload server status check for browsers with CORS support:
            if (jQuery.support.cors) {
                jQuery.ajax({
                    url: 'assets/global/plugins/jquery-file-upload/server/php/',
                    type: 'HEAD'
                }).fail(function () {
                    jQuery('<span class="alert alert-error"/>')
                        .text('Upload server currently unavailable - ' +
                        new Date())
                        .appendTo('#fileupload');
                });
            }
        }

    };

    var handleNotiEvents = function() {
        jQuery('body').on('click','.aSelUnreadMessage', function() {
            var self = jQuery(this);

            ToID = self.attr('data-toid');
            InboxID = self.attr('data-inboxid');

            showModal({
                name: 'basic',
                title : 'Message',
                content: ajaxRequest(base_url+'notification/event','event=showMessageForm&InboxID='+InboxID+'&ToID='+ToID,'html'),
                class: 'modal_show_message',
                button: {
                    caption: 'Reply',
                    class: 'BtnMessageReply',
                    hasActionButton: false
                }
            });

            loadUnreadMessageCounts();
        });

        jQuery('body').on('click','.aSelPendingNoti', function() {
            var self = jQuery(this);

            ToID = self.attr('data-toid');
            InboxID = self.attr('data-inboxid');

            showModal({
                name: 'basic',
                title : 'Message',
                content: ajaxRequest(base_url+'notification/event','event=showMessageForm&InboxID='+InboxID+'&ToID='+ToID,'html'),
                class: 'modal_show_message',
                button: {
                    hasActionButton: false
                }
            });

            loadPendingNotificationCounts();
        });
        
    };

    function loadPendingNotificationCounts() {
        setAjaxRequest(
            base_url+'notification/getNofificationsCount','',
            function(result) {
                jQuery('#HeaderPendingNotificationTotal').html(result.total);
                jQuery('#PendingNotificationTotal').html(result.total);
                loadPendingNotifications();
            },
            function() {
            },'json',true,false
        );
    };

    function loadUnreadMessageCounts() {
        setAjaxRequest(
            base_url+'notification/getUnreadMessagesCount','',
            function(result) {
                jQuery('#HeaderUnreadMessagesTotal').html(result.total);
                jQuery('#UnreadMessagesTotal').html(result.total);
                loadUnreadMessages();
            },
            function() {
            },'json',true,false
        );
    };

    function loadPendingNotifications() {
        setAjaxRequest( base_url+'notification/getNofifications','',
            function(result) {
                jQuery('#HeaderPendingNotification').html(result);
            },
            function() {
            },'html',true,false
        );
    };

    function loadUnreadMessages() {
        setAjaxRequest( base_url+'notification/getUnreadMessages','',
            function(result) {
                jQuery('#HeaderUnreadMessages').html(result);
            },
            function() {
            },'html',true,false
        );
    };

	return {
		init: function() {

            //handle compose message
            handleCompose();

            //initialize notification events
            handleNotiEvents();

            setTimeout(function() {
                //initialize pending notifications
                //loadPendingNotificationCounts();

                //initialize unread messages
                //loadUnreadMessageCounts();
            },2000);

            //set interval to refresh the notifications
            setInterval(function() {

                //initialize pending notifications
                //loadPendingNotificationCounts();

                //initialize unread messages
                //loadUnreadMessageCounts();
            },50000);
		}
	}
}();
jQuery(document).ready(Noti.init());