var FormWizard = function () {
    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) { return; }
            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    yearterm: { required: true },
                    campus: {   required: true },
                    department: { required: true, },
                    //profile
                    track: { required: true },
                    curriculum: { required: true, },
                    yearlevel: { required: true },
                    retention: { required: true },
                    sections: { required: true },
                    book: { required: true },                    
                },

                messages: { // custom messages for radio buttons and checkboxes
                   
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element); // for other inputs, just perform default behavior                    
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                
                success: function (label) {                    
                    label.addClass('valid') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group                    
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();                    
                }

            });
            
            var saveRegistion = function() {
                
                var data = "event=enrolment&" + $('#submit_form').serialize() ;
                
                ME.refresh_wait();                
                var res = ajaxRequest(ME.mylink+"event",data,'json',false,false);
                if(!res.error){
                    
                    $('#regid').val(res.reg.id);
                    $('#regid').attr('data-key',res.reg.key);
                    $('#regdate').val(res.reg.date);
                    $('#regstatus').val(res.reg.status);
                    
                    sysfnc.msg("Enrollment",'Your enrollment was successfully saved!')
                    
                        
                }else{
                    alert(res.message);
                }
                Metronic.unblockUI('.tab-content');            
                
                return !res.error;
                
            };

            var displayConfirm = function() {
                $('#tab3 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));                    
                    }
                });
                
                var x = $('#schedules').html();
                $('#enrolledsubj').html(x);
                
            };

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current == 3) {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                    
                    displayConfirm();
                } else if( current >= 4){                    
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                       
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                console.log(current);
                Metronic.scrollTo($('.page-title'));
            };

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) { return false;   },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) { return false; }                                                                                
                    if(index == 1){ ME.load_grades(); } 
                    
                    if (index == 3){ if ( saveRegistion() == false ) { return false; }}                    
                    handleTitle(tab, navigation, index);                                            
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({ width: $percent + '%' });                    
                    var status = $('#retention').val();
                    if(status == 'Registered' || status == 'Validated' ){                       
                        var current = 4;
                        var $percent = (current / total) * 100;
                        $('#form_wizard_1').find('.progress-bar').css({
                            width: $percent + '%'
                        });                                                                                
                        handleTitle(tab, navigation, 3);                    
                        $('.tab-pane').removeClass('active');
                        $('#tab4').addClass('active');                        
                        $('#btnsubmit').removeClass('button-submit');
                        $('#btnsubmit').addClass('btn-print');                        
                    }                                                    
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();            
            $('#form_wizard_1 .button-submit').click(function () {
                ME.print();
            }).hide();                                                                        
        }

    };

}();

var ME = {
    init : function() {        
        this.is_busy = false;
        this.mylink = base_url+page_url;
        this.addlistener();
        FormWizard.init();        
    },
 
    load_grades: function(e){
                
        var t = $('#sections').val();
        if(t != -1){                    
            var data = { event : 'schedules', section :  t };
            ME.refresh_wait();
            var res = ajaxRequest(this.mylink+"event",data,'json',undefined,false);
            $('#schedules').html(res.list);
            Metronic.unblockUI('.tab-content');
        }        
    },
    
    refresh_wait: function(){ Metronic.blockUI({target: '.tab-content', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Please wait while loading... '}); },
    print : function(){ window.open(ME.mylink+'print?reg='+$('#regid').attr('data-key')+'&t='+ Math.random() ,'_blank');},
    action : function(e){
        var data = $(e.target).data();                
        switch (data.mode) {                        
            case 'template': this.gstemplate.action(e); break;
            case 'grades': this.grades.action(e); break;            
            default: break;
        }
    },
    
    addlistener: function() {
        $('body').on('click','.btn,.edit', $.proxy(this.action, this));  
        $('body').on('change','#sections', $.proxy(this.load_grades, this));
        $('body').on('click','.btn-print',function () { ME.print();});               
    },
};