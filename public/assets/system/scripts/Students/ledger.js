$('document').ready(function(){
  $('body').on('change','#studno',function(){
     var xlink = base_url+'students/event?event=get_ledger';	
	 var stdno = $(this).val();
     if(stdno==undefined || stdno=='')
		  return false;
	 else
		 progressBar('on');
	 
	 setAjaxRequest(
				xlink,
				{stdno:stdno},
				function(data){
				  var icon = '<i class="fa fa-warning text-warning"></i>';
				  if (data.success){
					$('.stdledger').html(data.content);
					progressBar('off');
				  }else{
				    confirmEvent(icon+' '+data.message,function(data){return;});
				    progressBar('off');
				  }
				},
				function(err)
				{
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed To Execute',function(data){return;});
				}
			);
			progressBar('off');	 
  });
});