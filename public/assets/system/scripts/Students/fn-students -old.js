"use strict";

var vars = {
    filter_stacked: {},
    save_filter_vars: function(form_id) {
        var data = $('#' + form_id).serializeArray();
        for (var a = 0; a < data.length; a++) {
            var _this = data[a];
            this.filter_stacked[_this.name] = _this.value;
        }
        this.filter_stacked['search-prog'] = $('#search-level').select2('data').element[0].attributes[1].nodeValue;
    },
    student_no: 0,
    family_id: 0,
    searched_string: '',
}

var calculate_age = function(dob, admit_date) {
    dob = new Date(dob);
    admit_date = new Date(admit_date);
    var age = admit_date.getFullYear() - dob.getFullYear();
    var age2;
    if (dob.getMonth() >= admit_date.getMonth()) {
        age = age - 1;
        age2 = 11 + (admit_date.getMonth() - dob.getMonth());
    }
    return age + '.' + age2;
}

var reload_elements = function() {
    $('#prof-bdate').datepicker().on('select', function(e) {
        var dob = $(this).val();
        var admit_date = $('#prof-admit-date').val();
        var age = calculate_age(dob, admit_date);
        $('#prof-age').val(age);
    });

    $('#acad-admit-date').datepicker();

    $('.select2-profile').select2();
    Metronic.init();
}

var animate_form = function(content) {
    var body_id = '#form-stack-new';

    $(body_id).animate({
        'opacity': '0',
        'margin-top': '10px'
    }, 500, function() {
        $(body_id).empty().append(content).animate({
            'opacity': '1',
            'margin-top': '0'
        }, 500);
        reload_elements();
    });

    return true;
}

var remove_disabled = function() {

}

var Students = {
    get: function(params) {
        var term = $('#search-ac-year').select2('data').text,
            yl = $('#search-level').select2('data').text;

        setAjaxRequest(
            base_url + page_url + 'event?event=get', params,
            function(req) {
                if (req.error == false) {

                    showModal({
                        name: 'basic',
                        title: yl + ' Students (' + term + ')',
                        content: req.list,
                    });

                    $('.btn.blue.btn_modal').addClass('hide');
                    $('#students-table-res').DataTable({
                        'bDestroy': true,
                        'aaSorting': []
                    });

                } else {
                    msgbox('error', req.message);
                }
                progressBar('off');
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },

    info: function(stud_no, info_type) {
        stud_no = (stud_no != '' || stud_no != 'undefined') ? stud_no : 0;

        setAjaxRequest(
            base_url + page_url + 'event', 'event=get_info&student_no=' + stud_no + '&form=' + info_type,
            function(req) {
                if (req.error == false) {

                    progressBar('off');
                    animate_form(req.form);

                    if (info_type == 'profile') {
                        $('.div-blocker').addClass('hide');
                        $('.profile-usertitle-name #full-name').text(req.name);
                        $('.profile-usertitle-job').text(req.stud_num);
                        $('.profile-yl').html(req.year_level);
                        $('#profile-photo').attr('src', req.photo);

                        if (req.chinese_name != '') {
                            $('#ch-name').text(req.chinese_name);
                        } else {
                            $('#ch-name').text('');
                        }
                    }

                    return true;
                    // $('#profile-container').html(req.profile);
                    // $('.select2-profile').select2({
                    //     'width': '100%'
                    // })
                    // reload_elements();
                } else {
                    msgbox('error', req.message);
                }
            },
            function(err, resp_text) {
                progressBar('off');
                msgbox('error', resp_text);
            }, null, true
        );
    },

    save: function(form_id, stud_no) {
        var _data = $('#' + form_id).serialize();

        setAjaxRequest(
            base_url + page_url + 'event', 'event=save&student=' + stud_no + '&' + _data,
            function(req) {
                if (req.error == false) {
                    msgbox('success', req.message);
                } else {
                    msgbox('error', req.message);
                }
            }
        );

        progressBar('off');
    },

    search_list: function() {

        setAjaxRequest(
            base_url + page_url + 'event', 'event=search&data_search=' + vars.searched_string + '&filters=' + JSON.stringify(vars.filter_stacked),
            function(req) {
                if (req.error == false) {
                    $('.div-stud-list').addClass('hide');
                    $('.div-stud-search-res').removeClass('hide').find('.general-item-list').html(req.list);
                    Students.get_students_photo();
                } else {
                    msgbox('error', req.message);
                }
            }, '', '', true
        );

        progressBar('off');
    },

    // get_students: function(parent_class) {
    //
    //     var total_shown_count = $('.' + parent_class +' .item-details a').length;
    //
    //     setAjaxRequest(
    //         base_url + page_url + 'event', 'event=get_list&shown_count=' + total_shown_count + '&filters=' + JSON.stringify(vars.filter_stacked) + '&search_val=' + vars.searched_string,
    //         function(req) {
    //             if (req.error == false) {
    //                 $('.' + parent_class + ' .general-item-list').html(req.list);
    //                 Students.get_students_photo();
    //             } else {
    //                 msgbox('error', req.message);
    //             }
    //         }, '', '', true
    //     );
    //     progressBar('off');
    // },

    get_students_photo: function() {
        $('.general-item-list li a').each(function(e) {
            var stud_no = $(this).attr('data-id');
            var stud_img = $(this).prev();
            var photo = setAjaxRequest(base_url + page_url + 'event', 'event=get_photo&student_no=' + stud_no, function(ret) {
                if (ret.photo != false) {
                    stud_img.attr('src', ret.photo);
                }
            }, '', '', true);
        });
        progressBar('off');
    },

    upload_picture: function(file_input_id) {
        var file_input = $('#' + file_input_id)[0];
        var stud_id = $('#' + file_input_id).attr('data-id');

        var _data = new FormData();
        _data.append('image', file_input.files[0]);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: base_url + page_url + 'event?event=upload_picture&stud_id=' + stud_id,
            type: 'POST',
            data: _data,
            contentType: false,
            processData: false,
            async: false,
            beforeSend: function() {
                progressBar('on');
            },
            success: function(ret) {
                progressBar('off');

                if (ret.error == false) {

                    msgbox('success', ret.message);

                    var _read_photo = new FileReader();

                    _read_photo.onload = function(e) {
                        $('#profile-photo').attr('src', e.target.result);
                    }

                    _read_photo.readAsDataURL(file_input.files[0]);

                } else {
                    msgbox('error', ret.message);
                }

            },
            error: function(ret) {
                progressBar('off');
                msgbox('error', "There's something wrong.");
            }
        })

    }
}

var Admission = {
    save: function(form_id, stud_no) {
        var _data = $('#' + form_id).serialize();
        var _this = this;
        setAjaxRequest(
            base_url + page_url + 'admission/event', 'event=save&student=' + stud_no + '&' + _data,
            function(req) {
                if (req.error == false) {
                    msgbox('success', req.message);
                    _this.attachments(stud_no);
                } else {
                    msgbox('error', req.message);
                }
            }
        );
    },

    remove_attach: function(template_detail_id, student_no, action) {
        var _ret = false
        setAjaxRequest(
            base_url + page_url + 'admission/event', 'event=remove_attach&temp_detail=' + template_detail_id + '&stud_id=' + student_no + '&action=' + action,
            function(req) {

                if (req.error == false) {
                    _ret = true;
                } else {
                    _ret = false;
                }
            }
        );
        progressBar('off');
        return _ret;
    },

    attachments: function(stud_no) {
        var adm_docs = this.collect_docs('adm-documents-table');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: base_url + page_url + 'admission/event?event=save_attach&stud_id=' + stud_no,
            type: 'POST',
            data: adm_docs,
            contentType: false,
            processData: false,
            async: false,
            beforeSend: function() {
                progressBar('on');
            },
            success: function(ret) {
                progressBar('off');

                if (ret.error == false) {
                    $('.adm-upload-file').val('');
                    msgbox('success', ret.message);

                } else {
                    var _data = ret.err_data,
                        err = '';
                    if (_data) {
                        for (var a = 0; a < _data.length; a++) {
                            var _this = _data[a];
                            var id = _this.id.replace('file-', '');

                            var err_label = $('tr[data-id=' + id + '] .adm-doc-descp').text();
                            err += '<li>' + err_label + ' ' + _this.message + '</li>';
                        }
                        msgbox('error', '<ul>' + err + '</ul>');
                    }
                }

            },
            error: function(ret) {
                progressBar('off');
                msgbox('error', "There's something wrong.");
            }
        });

    },

    collect_docs: function(table_id) {
        var attach_docs = new FormData(),
            _data = [];

        $('#' + table_id + ' tbody tr').each(function(e) {
            var _this = $(this);
            var reviewed = _this.find('.adm-doc-reviewed').is(':checked'),
                exempted = _this.find('.adm-doc-exempted').is(':checked');
            var rev_val = (reviewed) ? 1 : 0,
                exemp_val = (exempted) ? 1 : 0;
            var _file = _this.find('.adm-upload-file')[0].files;

            if ((rev_val != 0 && _file.length != 0) || (exemp_val != 0 && _file.length != 0)) {
                attach_docs.append('file-' + _this.attr('data-id'), _file[0]);
                _data.push({
                    'detail': _this.attr('data-id'),
                    'template': _this.attr('data-temp'),
                    'reviewed': rev_val,
                    'exempted': exemp_val,
                    'has_file': true
                });
            } else if ((rev_val == 0 && _file.length != 0) || (exemp_val == 0 && _file.length != 0)) {
                attach_docs.append('file-' + _this.attr('data-id'), _file[0]);
                _data.push({
                    'detail': _this.attr('data-id'),
                    'template': _this.attr('data-temp'),
                    'has_file': true
                });
            } else if ((rev_val != 0 && _file.length == 0) || (exemp_val != 0 && _file.length == 0)) {
                _data.push({
                    'detail': _this.attr('data-id'),
                    'template': _this.attr('data-temp'),
                    'reviewed': rev_val,
                    'exempted': exemp_val,
                    'has_file': false
                });
            }

        });

        attach_docs.append('attach_data_stat', JSON.stringify(_data));

        if (_data.length != 0) {
            return attach_docs;
        } else {
            return false;
        }
    }
}

var Family = {
    save: function(guardian_form_id, father_form_id, mother_form_id, stud_no) {
        var _data = $('#' + guardian_form_id).serialize() + '&' + $('#' + father_form_id).serialize() + '&' + $('#' + mother_form_id).serialize();

        setAjaxRequest(
            base_url + page_url + 'family/event', 'event=save&student=' + stud_no + '&family=' + vars.family_id + '&' + _data,
            function(req) {
                if (req.error == false) {
                    msgbox('success', req.message);
                } else {
                    msgbox('error', req.message);
                }
            }
        );

        progressBar('off');
    }
}

var Medical = {
    clinic_form: function(stud_id, clinic_id) {
        stud_id = (stud_id != '' || stud_id != undefined) ? stud_id : 0;

        showModal({
            name: 'basic',
            title: 'Clinic Form | New',
            content: ajaxRequest(base_url + page_url + 'medical/clinic/event', 'event=form&stud_id=' + stud_id, 'HTML'),
            class: 'modal_clinic_form',
            button: {
                icon: 'fa fa-check',
                class: 'btn green btn-clinic-form-save',
                caption: 'Save',
            }
        });

        $('#cf-date-entry').datepicker();
        $('#cf-ac-year').select2();
    }
}

var Counseling = {
    form: function(stud_id, couns_id) {
        stud_id = (stud_id != '' || stud_id != undefined) ? stud_id : 0;

        showModal({
            name: 'basic',
            title: 'Counseling Form | New',
            content: ajaxRequest(base_url + page_url + 'counseling/event', 'event=form&stud_id=' + stud_id, 'HTML'),
            class: 'modal_counseling_form',
            button: {
                icon: 'fa fa-check',
                class: 'btn green btn-counseling-form-save',
                caption: 'Save',
            }
        });

        $('#couns-date').datepicker();
        $('#couns-ac-year').select2();
    },

    save: function(form_id, stud_no) {
        var _data = $('#' + form_id).serialize();

        setAjaxRequest(
            base_url + page_url + 'counseling/event', 'event=save&student=' + stud_no + '&' + _data,
            function(req) {
                if (req.error == false) {
                    msgbox('success', req.message);
                } else {
                    msgbox('error', req.message);
                }
            }
        );

        progressBar('off');
    }
}

var Documents = {
    form: function(stud_id, couns_id) {
        stud_id = (stud_id != '' || stud_id != undefined) ? stud_id : 0;

        showModal({
            name: 'basic',
            title: 'Document Form | New',
            content: ajaxRequest(base_url + page_url + 'documents/event', 'event=form&stud_id=' + stud_id, 'HTML'),
            class: 'modal_document_form',
            button: {
                icon: 'fa fa-check',
                class: 'btn green btn-documents-save',
                caption: 'Save',
            }
        });

        $('#docu-date').datepicker();
    }
}
