var ME = {
    init : function() {
        this.is_busy = false;
        this.mylink = base_url+page_url;
        this.addlistener();
        this.load_grades();
    },

    load_grades: function(e){
        var t = $('#acad-year-term').val(),
            stud_select = $('#students'),
            stud_no = 0;

        if(stud_select.length){
            stud_no = stud_select.val();
        }

        if(t != -1){
            var data = { event : 'grades', term :  t, 'studnum' : stud_no};
            this.refresh_wait();
            var res = ajaxRequest(this.mylink+"event",data,'json',undefined,false);
            $('#grades').html(res.list);
            Metronic.unblockUI('#grades');
            Metronic.init();
        }
    },

    refresh_wait: function(){
        Metronic.blockUI({target: '#grades', boxed: true , textOnly: true, message: '<i class="fa fa-refresh fa-spin"></i> Please wait while loading... '});
    },

    initcomponent : function(){
        var p =this.$selpolicy.attr('data-id');
        if( p == undefined || p == ''){
            msgbox('warning','Please select a policy','No policy selected',5000);
            return false;
        }
        $('#component_modal').modal('show');
    },

    action : function(e){
        var data = $(e.target).data();
        switch (data.mode) {
            //case 'template': this.gstemplate.action(e); break;
            //case 'grades': this.grades.action(e); break;
            case 'cog-college': this.print_cog(); break;
            default: break;
        }
    },
    
    print_cog: function(){
        window.open(base_url + page_url + 'print?idno=' + $('#students').val() + '&term=' + $('#acad-year-term').val()+'&t='+ Math.random());
    },

    student_term: function (stud_no) {
        if(stud_no){
            setAjaxRequest(
                base_url + 'students/event', {'event': 'enrolled-term', 'studnum':stud_no},
                function(req) {
                    if (req.error == false) {
                        var opt = '<option value="-1"> - Select Academic Year - </option>' + req.html;
                        $('#acad-year-term').html(opt);
                        // msgbox('success', req.message);
                    } else {
                        msgbox('error', req.message);
                    }
                    progressBar('off');
                }
            );

        } else {
            msgbox('warning', 'Cant proceed.');
        }
    },

    addlistener: function() {
        $('body').on('click','.btn,.edit', $.proxy(this.action, this));
        $('body').on('change','#acad-year-term', $.proxy(this.load_grades, this));
        $('body').on('click','#btnrequest',function() {
                var data = $('#form_survey').serialize();

                setAjaxRequest( base_url + 'students/survey', data, function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                    sysfnc.showAlert('.form-body','danger',result.message,true,true,true,'warning');
                } else {
                    sysfnc.showAlert('.form-body','success',result.message,true,true,true,'check');
                    msgbox('success', result.message);
                }
                progressBar('off');
            });
        });

        $('body').on('change', '#students', function(e){
            var v = $(this).val();
            ME.student_term(v);
        });
        
        $('body').on('click','#btnref',function(){
            ME.load_grades();
        });

    },
};
