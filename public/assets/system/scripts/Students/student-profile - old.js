"use strict";

var getQueryString = function(field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

var getStudData = function(e) {
    // console.log('asdasdasd');
    if ($('#search-ac-year').val() && $('#search-level').val()) {
        var _id = 'search-form',
            data = $('#' + _id).serialize();

        vars.save_filter_vars(_id);
        Students.get(vars.filter_stacked);
    } else {
        msgbox('warning', 'Please provide a school year/semester with level.');
    }
};

window.onload = function() {
    $('body').on('click', '#student-prof-navs li a', function(e) {
        if (vars.student_no != 0) {
            $('#student-prof-navs li.active').removeClass('active');
            $(this).parent().addClass('active');
            Students.info(vars.student_no, $(this).attr('data-form'));
        } else {
            msgbox('warning', 'Unable to get form. Please select a student.');
        }
    });

    var is_get_url = getQueryString('g'),
        form = window.location.hash;
    if (is_get_url != null) {
        vars.student_no = is_get_url;
        $('#student-prof-navs li:first a').click();
    }

}

$(document).ready(function(e) {

    $('.select2').select2({
        width: '100%'
    });

    $('.datepicker').datepicker();

    $('.datatables').dataTable();

    $('.datatables-no-search').dataTable({
        bFilter: false,
        bLengthChange: false,
        pageLength: 5
    });

    $('body').on('change', '#search-ac-year', function() {
        var txt = $(this).find('option:selected').text(),
            opts = $('#search-level');

        if (txt.indexOf('School Year') !== -1) {
            opts.find('option[data-by-sem=0]').removeClass('hide');
            opts.find('option[data-by-sem=1]').addClass('hide');
        } else {
            opts.find('option[data-by-sem=0]').addClass('hide');
            opts.find('option[data-by-sem=1]').removeClass('hide');
        }
        opts.val(null).trigger('change');
    });

    $('body').on('click', '#btn-search', function() {
        getStudData()
    });



    // Students
    // $('body').on('click', '#student-list-get, #student-search-list-get', function(e) {
    //     // if(Object.keys(vars.filter_stacked).length == 0){
    //     //     vars.save_filter_vars('search-form');
    //     // }
    //     var this_id = $(this).attr('id'), parent_class = '';
    //     switch (this_id) {
    //         case 'student-list-get':
    //             parent_class = 'div-stud-list';
    //             break;
    //         case 'student-search-list-get':
    //             parent_class = 'div-stud-search-res';
    //             break;
    //     }
    //
    //     Students.get_students(parent_class);
    // });

    $('body').on('click', '.student-select', function(e) {
        e.preventDefault();

        var stud_id = $(this).attr('data-id');
        vars.student_no = stud_id;
        $('#student-prof-navs > li > a:first').click();
        // Students.info(stud_id, 'profile');
        closeModal('basic');
    });

    var tab_open1, tab_open2;
    $('body').on('click', '.nav-tabs-main li a', function(e) {
        tab_open1 = $(this).attr('href');
        location.hash = tab_open1.replace('#', '');
    });

    $('body').on('click', '.nav-tabs-sub li a', function(e) {
        tab_open2 = $(this).attr('href');
        location.hash = tab_open2.replace('#', '');
    });

    $('body').on('click', '#profile-upload-photo-btn', function(e) {
        Students.upload_picture('profile-upload-photo');
    });

    $('body').on('click', '#prof-save-btn', function() {
        Students.save('profile-form', vars.student_no);
    });

    function search_student(elem) {
        var search_val = elem.val();
        if (search_val != '') {
            if (Object.keys(vars.filter_stacked).length == 0) {
                vars.save_filter_vars('search-form');
            }
            vars.searched_string = search_val;
            Students.search_list();
        } else {
            $('.div-stud-list').removeClass('hide');
            $('.div-stud-search-res').addClass('hide');
        }
    }

    $('body').on('keypress', '#student-search-text', function(e) {
        if (e.keyCode == 13) {
            search_student($(this));
        }
    });

    $('body').on('click', '#student-search-btn', function(e) {
        search_student($('#student-search-text'));
    });
    // End

    // Family
    $('body').on('click', '#family-save-btn', function() {
        Family.save('guardian-form', 'father-form', 'mother-form', vars.student_no);
    });
    // End

    // Admission
    $('body').on('click', '#admission-save-btn', function() {
        Admission.save('school-attended-form', vars.student_no);
    });

    $('body').on('click', '.adm-btn-file-new', function(e) {
        e.preventDefault();
        $(this).siblings('.adm-upload-file').click();
    });

    $('body').on('change', '.adm-upload-file', function() {
        var filename = $(this)[0].files[0].name;
        var filename_elem = $(this).closest('tr').find('td:first-child .adm-upl-filename');

        if (filename == '') {
            filename_elem.text('');
        } else {
            filename_elem.text('(' + filename + ')');
        }
    });

    $('body').on('click', '.adm-btn-file-remove', function(e) {
        e.preventDefault();

        var input_file = $(this).next(),
            attach_id = input_file.attr('data-id'),
            showed_filename = $(this).closest('tr').find('td:first-child .adm-upl-filename'),
            showed_filename_text = showed_filename.text().trim(),
            dl_btn = $(this).prev();

        if ((input_file.val() == '') && (showed_filename_text != '')) {
            var file_check = Admission.remove_attach(attach_id, vars.student_no, 'check');

            if (file_check == true) {
                var filename;

                if (showed_filename_text != '') {
                    filename = showed_filename_text;
                } else {
                    '(' + input_file[0].files[0].name + ')';
                }

                bootbox.confirm({
                    size: 'small',
                    message: "Remove this" + filename + " attachment?",
                    callback: function(yes) {
                        if (yes) {
                            rem_file = Admission.remove_attach(attach_id, vars.student_no, 'remove');
                            if (rem_file) {
                                input_file.val('');
                                showed_filename.text('');
                                dl_btn.remove();
                            }
                        }
                    }
                });

            } else {
                input_file.val('');
                showed_filename.text('');
            }
        }

    });

    $('body').on('click', '.adm-btn-file-dl', function(e) {
        var doc_id = $(this).attr('data-id');
        if (doc_id != 0 || doc_id != '') {
            window.location.href = base_url + page_url + 'admission/event/download?stud_id=' + vars.student_no + '&file=' + doc_id;
        }
    });
    // End

    // Medical & Clinic
    $('body').on('click', '#med-clinic-showform', function(e) {
        Medical.clinic_form();
    });
    // End

    // Counseling
    $('body').on('click', '#counseling-showform', function(e) {
        Counseling.form();
    });

    $('body').on('click', '.btn-counseling-form-save', function(e) {
        Counseling.save('counseling-form', vars.student_no);
    });
    // End

    // Documents
    $('body').on('click', '#docu-showform', function(e) {
        Documents.form();
    });
    // End

    $('body').on('change', '#academic-track', function(e) {
        var academictrack = $('#academic-track option:selected').val();
        $('.academic-curriculum').val('');
        $('.available-sections').val('');
        $('.curriculum').addClass('hide');
        $('.curriculum[data-parent-id="' + academictrack + '"]').removeClass('hide');
    });

    $('body').on('change', '#academic-curriculum', function(e) {
        var academiccurriculum = $('#academic-curriculum option:selected').val();
        $('.available-sections').val('');
        $('.sections').addClass('hide');
        $('.sections[data-curr-id="' + academiccurriculum + '"]').removeClass('hide');
    });

    $('body').on('click', '.shift-academic-track', function(e) {
        bootbox.confirm({
            message: "Are you sure you want to shift Academic Track?",
            callback: function(yes) {
                if (yes) {
                    var studno = vars.student_no;
                    var academictrackto = $('#academic-track').val();
                    var academiccurriculumto = $('#academic-curriculum').val();
                    var newsection = $('#available-sections').val();
                    var termid = $('#school-term').val();
                    // var progid = $('#school-term option:selected').attr('data-prog-id');
                    var progid = $('#stud-prog-id').val();
                    var academictrackold = $('#current-academic-track').attr('data-id');
                    var academiccurriculumold = $('#current-academic-curriculum').attr('data-id');
                    var oldsection = $('#shift-current-section').val();

                    setAjaxRequest(
                        base_url + page_url + 'event', 'event=shift-academic-track&studno=' + studno + '&academictrackto=' + academictrackto + '&academiccurriculumto=' + academiccurriculumto +
                        '&academictrackold=' + academictrackold + '&academiccurriculumold=' + academiccurriculumold + '&newsection=' + newsection + '&oldsection=' + oldsection + '&termid=' + termid + '&progid=' + progid,
                        function(req) {
                            if (req.error == false) {
                                msgbox('success', req.message);
                                // $('#academic-shifting').modal('hide');
                                // location.reload()
                                $('#student-prof-navs li:nth(1) a').click();
                            } else {
                                msgbox('error', req.message);
                            }
                        }
                    );
                }
            }
        });

        progressBar('off');
    });
});

// $(window).load(function(e){
//
//
//     // Students.get_students_photo();
// });
