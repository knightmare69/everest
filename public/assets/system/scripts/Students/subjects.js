var MOD = {
    init: function(){
        this.load();
        this.initSubjects()
    },
    load : function(){
        $('body').on('change', '#term', function(e){
            MOD.initSubjects(); 
        });
        $('body').on('click', '#btnref', function(e){
            MOD.initSubjects(); 
        });
    },
    initSubjects : function(){
        $('#subjects').show();
        $('#subjects').html('<h4><i class="fa fa-cog fa-spin"></i> Loading...</h4>')
        setAjaxRequest( base_url + page_url + 'event', {'event': 'get-subjects', 'reg': $('#term').find("option:selected").attr('data-reg') }, function(r) {
            if (r.error) {
                msgbox('error', r.message);
            }else{
                $('#subjects').html(r.data);
            }
        },undefined,'json',true,false);
    },
}