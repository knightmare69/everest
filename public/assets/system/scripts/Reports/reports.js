var REPORTS = function() {
    
    var treeReport = function () {

        $('#tree_report').jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }            
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "plugins": ["types"]
        });

        // handle link clicks in tree nodes(support target="_blank" as well)
        $('#tree_report').on('select_node.jstree', function(e,data) { 
            var link = $('#' + data.selected).find('a');
            if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
                if (link.attr("target") == "_blank") {
                    link.attr("href").target = "_blank";
                }
                //document.location.href = link.attr("href");
                
                return false;
            }
        });
    }
    
	return {
		init: function() {
		  
            treeReport();
          
            FN.datePicker('.date_from');
            FN.datePicker('.date_to');

            FN.datePicker('.exp_date_from');
            FN.datePicker('.exp_date_to');

            FN.datePicker('.inv_date_from');
            FN.datePicker('.inv_date_to');

            $('body').on('click','.btn_sales_print',function() {
            	$('#iframe_sales').attr('src',base_url+page_url+'sales?date_from='+$('.date_from').val()+'&date_to='+$('.date_to').val());
            });

             $('body').on('click','.btn_sales_materials_print',function() {
                  $('#iframe_sales').attr('src',base_url+page_url+'salesWithMaterials?date_from='+$('.date_from').val()+'&date_to='+$('.date_to').val());
            });

            $('body').on('click','.btn_expenses_print',function() {
            	$('#iframe_expenses').attr('src',base_url+page_url+'expenses?date_from='+$('.exp_date_from').val()+'&date_to='+$('.exp_date_to').val());
            });

             $('body').on('click','.btn_inv_print',function() {
            	$('#iframe_inv').attr('src',base_url+page_url+'inventory?date_from='+$('.inv_date_from').val()+'&date_to='+$('.inv_date_to').val());
            });

		}
	}
	
}();