"use strict";
var module_form ='';
var report_name ='';
var Report2 = {

    load: function(m,s){
      s = ((s==undefined)?m:s);
      if(module_form == 'cashier-report'){
        setAjaxRequest( base_url + page_url + 'event?event=get-report', {rep_id: report_name  } ,
            function(r) {
                if (r.error) {
                    showError( '<i class="fa fa-times"></i> ' +  r.message);
                } else {
                    var report = 'Collection Report by Account'
                    if(r.rep.toUpperCase() === report.toUpperCase()){
                      $('#acctdiv').removeClass('hide');
                    }else{
                        $('#acctdiv').addClass('hide');
                    }
                }
                progressBar('off');
            } , undefined, 'json',false,false

        );

      }



      if(module_form != m || report_name!=s){
			module_form = m;
			report_name = s;
			setAjaxRequest( base_url + page_url + 'event?event=get-form', {mod : m, slug : s} ,
				function(r) {
					if (r.error) {
						showError( '<i class="fa fa-times"></i> ' +  r.message);
					} else {
						$('#report-form').html(r.view);
					}
					progressBar('off');
				} , undefined, 'json',false,false

			);
      }
    },
    rep_elems: {
        'StudentNo': 'student-div',
        'TermID': 'term-div',
        'YearLevelID': 'yl-div',
        'ProgramID': 'prog-div',
        'PeriodID': 'period-div',
        'SectionID': 'sec-div',
        'ExcludeEmptySubj': 'exclude-chk',
        'WithLogo': 'w-logo',
        'CampusID': 'campus-div'
    },

    printReport: function(){

        var details = $('#report-form').serialize();
        var stud    = $('#student-name'), stud_num = 0;
        var isclear = true;
		$('#report-form').find('[required]').each(function(){
			var tmpval = $(this).val();
			$(this).closest('.form-group').removeClass('alert-danger');
			if(tmpval=='' || tmpval==null){
				isclear = false;
				$(this).closest('.form-group').addClass('alert-danger');
			}
		});
		
		if (isclear==false){
			alert('Complete all required fields first!');
			return false;
		}
		
		if (stud.val() != '' || Boolean(stud.val().trim()) ){
            stud_num = stud.attr('data-snum');
        }
        
        if ($('.report-card-logo').is(":checked")){
            var logo = 1;
        } else {
            var logo = 0;
        }

        window.open(base_url + 'reports/print?' + details + '&report='+ report_name + '&snum=' + stud_num + '&t='+ Math.random() + '&logo=' + logo);
    },

    exportReport: function(){

        var details = $('#report-form').serialize();
        var stud = $('#student-name'), stud_num = 0;
        if(stud.val() != '' || Boolean(stud.val().trim()) ){
            stud_num = stud.attr('data-snum');
        }

        if ($('.report-card-logo').is(":checked"))
        {
            var logo = 1;
        }
        else {
            var logo = 0;
        }

        window.open(base_url + 'reports/export?' + details + '&report='+ report_name + '&snum=' + stud_num + '&t='+ Math.random() + '&logo=' + logo);
    },

    run: function() {
        $('#faculty-list').jstree({
            "plugins" : [ "themes", "html_data", "search", "adv_search", "checkbox"],
            "search": {
                "show_only_matches" : true
            }
        });

        $('#btn-fac-search').click(function(e) {
            var s = $('#rep-faculty-search').val();
            $('#faculty-list').jstree('search', s);
        });

        $('#rep-faculty-search').on('input', function(e) {
            if($(this).val().trim() == '') {
                $("#faculty-list").jstree("clear_search");
            }
        });

        $('body').on('click', '#btn_print', function(e){
            Report2.printReport();
        });
		
        $('body').on('click', '#btn_export', function(e){
            Report2.exportReport();
        });

        $('body').on('click', '#tree_report li', function(e){
            var p = $(e.target).closest('li').attr('data-parent');
			var s = $(e.target).closest('li').attr('data-slug');
            report_name = $(e.target).closest('li').attr('data-value');
            Report2.load(p,s);
        });


        $('body').on('click','.optbox', function(e){
            $('.optbox').prop('checked',false);
            $(e.target).prop('checked', true);
            console.log('hey click mo .optbox eh');
        });


        $('body').on('click', '#print-report2', function(e){
            var stud_num = $('#student-name').attr('data-snum');
            var prog = $('#programs').val(), yl = $('#year-level').val(), t = $('#academic-term').val(), r = $('#report-type').val();

            if(r <= 2){
                if(Boolean(prog.trim()) && Boolean(yl.trim()) && Boolean(t.trim())){
                    Report2.printReport();
                } else {
                    msgbox('error', 'Please fill required fields to proceed.');
                }
            } else {

                if(prog != null){
                    Report2.printReport();
                } else {
                    msgbox('error', 'Please select a program to proceed.');
                }
            }

        });

        $('#report-type').select2({
            placeholder: 'Select Report',
            allowClear: false
        }).on('change', function(e) {
            var f = $(this).find(':selected').attr('data-find'),
                keys = f.split(',');

            $('.rep-inp-hold').addClass('hide');

            for (var a = 0; a < keys.length; a++) {
                $('#' + Report2.rep_elems[keys[a]]).removeClass('hide');
            }

            if ($('#year-level').hasClass('hide') == false) {
                $('#prog-div').removeClass('hide');
            }

        });

        $('#tree_report').jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "plugins": ["types"]
        });

        // handle link clicks in tree nodes(support target="_blank" as well)
        $('#tree_report').on('select_node.jstree', function(e,data) {
            var link = $('#' + data.selected).find('a');
            if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
                if (link.attr("target") == "_blank") {
                    link.attr("href").target = "_blank";
                }
                //document.location.href = link.attr("href");
                console.log(e);
                return false;
            }
        });

    }
}
