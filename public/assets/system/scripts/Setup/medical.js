var Medical = {
    save: function() {
        var ac_year = $('#search-ac-year').val(),
            campus = $('#search-campus').val();
        var prog_class = $('#app option:selected').parent().attr('data-id');
        var yl = $('#app option:selected').attr('data-id');

        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to save this record?",
            callback: function(yes) {
                if (yes) {
                    setAjaxRequest(
                        base_url + page_url + 'event', 'event=save&' + $(form).serialize() + '&ac-year=' + ac_year + '&campus=' + campus + '&prog=' + prog_class + '&yl=' + yl,
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                showSuccess(result.message);
                                clear_value(form);
                                if (result.table != undefined) {
                                    Medical.result_to_table(result.table, '.table_wrapper');
                                    Medical.load_functions();
                                }
                            }
                        }
                    );
                    progressBar('off');
                }
            }
        })
    },
    update: function(id) {
        var ac_year = $('#search-ac-year').val(),
            campus = $('#search-campus').val();
        var prog_class = $('#app option:selected').parent().attr('data-id');
        var yl = $('#app option:selected').attr('data-id');

        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to update this record?",
            callback: function(yes) {
                if (yes) {
                    setAjaxRequest(
                        base_url + page_url + 'event', 'event=update&id=' + id + '&' + $(form).serialize() + '&ac-year=' + ac_year + '&campus=' + campus + '&prog=' + prog_class + '&yl=' + yl,
                        function(result) {
                            if (result.error) {
                                showError(result.message);
                            } else {
                                showSuccess(result.message);
                                if (result.table != undefined) {
                                    Medical.result_to_table(result.table, '.table_wrapper');
                                    Medical.load_functions();
                                }

                            }
                        }
                    );
                    progressBar('off');
                }
            }
        })
    },
    remove: function(id) {
        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to remove this record?",
            callback: function(yes) {
                if (yes) {
                    setAjaxRequest(
                        base_url + page_url + 'event', 'event=delete&id=' + id,
                        function(result) {
                            msgbox(result.error == false ? 'success' : 'error', result.message);
                            if (result.table != undefined) {
                                Medical.result_to_table(result.table, '.table_wrapper');
                                Medical.load_functions();
                            }
                        }
                    );
                    progressBar('off');
                }
            }
        })
    },
    result_to_table: function(data, table_wrap_class) {
        // FN.destroyDataTable();
        $('.table_wrapper').html(data);
        $('table').dataTable();
    },
    load_functions: function() {
        $('.date-picker').datepicker();
        $('.timepicker').timepicker({
            'defaultTime': 'value'
        });
        $('select').select2('destroy').select2();
        $('table').dataTable();
    },
    init: function() {
        var id = 0;
        Medical.load_functions();
        $('body').on('click', '#is-days', function(e) {
            if ($(this).is(':checked')) {
                $(this).val('1');
                $('#test-days').addClass('hide').find('input[type=checkbox]').addClass('not-required');
                $('#test-date').parent().removeClass('hide');
            } else {
                $(this).val('0');
                $('#test-days').removeClass('hide').find('input[type=checkbox]').removeClass('not-required');
                $('#test-date').parent().addClass('hide');
            }
        });

        $('body').on('click', '.btn_save', function(e) {
            if (isFormValid(form)) {
                Medical.save();
            }
        });

        $('body').on('click', '.btn_update', function(e) {
            if (isFormValid(form)) {
                Medical.update(id);
            }
        });

        $('body').on('click', '.remove', function() {
            var data_id = $(this).closest('tr').attr('data-id');
            Medical.remove(data_id);
        });

        // Added from crud.js
        jQuery('body').on('click', '.a_select', function() {
            id = jQuery(this).closest('tr').attr('data-id');
            jQuery('.form_wrapper').html(ajaxRequest(base_url + page_url + 'event', 'event=edit&id=' + id, 'HTML'));
            var button = jQuery('.btn_action');
            button.addClass('btn_update').removeClass('btn_save').html("<i class='fa fa-save'></i> Update");
            Medical.load_functions();
            Metronic.init();
        });

        jQuery('body').on('click', '.btn_reset', function() {
            var button = jQuery('.btn_action');
            clear_value(form);
            button.removeClass('btn_update').addClass('btn_save').html("<i class='fa fa-save'></i> Save");
            jQuery('.alert-dismissable').addClass('hide');
        });
        // End

        $('body').on('change', '#search-ac-year, #search-campus', function(e) {
            var ac_year = $('#search-ac-year').val(),
                campus = $('#search-campus').val();
            var get = ajaxRequest(base_url + page_url + 'event', 'event=table_get&search=' + JSON.stringify({
                'ac_year': ac_year,
                'campus': campus
            }), 'JSON');

            if (get.table != undefined) {
                Medical.result_to_table(get.table, '.table_wrapper');
            }

            progressBar('off');

        });
    }
}
