var Content = function() {

	return {
		init: function() {
			$('#Content').summernote({height: 300});

			$('.btn_save').click( function() {
				var self = $(this);
				confirmEvent("Are you sure you want to save this?", function(yes) {
					if (!yes) return;
					var data = new Object();

					var content = $('textarea[name="Content"]').html($('#Content').code());
					 
					setAjaxRequest(
						base_url+page_url+'event',
						'event=save&'+$('form').serialize()+'&ContentHTML='+content,
						function(result) {
							if (result.error == false) {
								setTimeout( function() {
									window.location.href = base_url+page_url+'edit/'+result.key;
								},1500);
								msgbox('success',result.message);
							} else {
								msgbox('error',result.message);
							}
							progressBar('off');
						},
						function() {
							progressBar('off');
						}
					);
				});
			});

		}
	}
}();