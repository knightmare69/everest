$(document).ready(function(){
   $('body').on('change','#selgroup',function(){
     var tmpval = $(this).val();
	 var target = $(this).attr('data-xtarget');
	 var xvalue = $(this).attr('data-xvalue');
	 var xlabel = $(this).attr('data-xlabel');
	 $(target).attr('type','hidden');
	 if(tmpval=='-1'){
	   $(xlabel).attr('type','text');
	   $(target).val('');
	   $(xvalue).val('-1');
	 }else{
	   $(xvalue).val(tmpval);
	   $(xlabel).val($(this).find('option[value="'+tmpval+'"]').text());
	 }
   });
   
   $('body').on('click','.a_delete',function(){
        var xid = $(this).closest('tr').attr('data-id');
		    xid = ((xid==undefined)?FN.getTableID():('{"id":"'+xid+'"}'));
			
        bootbox.confirm({
            size : 'small',
            message : "Are you sure you want to remove this record/s?",
            callback : function(yes) {
                if ( yes )
                {
                    setAjaxRequest(
                        base_url+page_url+'event','event=delete&ids='+xid,
                        function(result) {
                           msgbox(result.error == false ? 'success' : 'error',result.message);
                           if (result.table != undefined) {
                                FN.destroyDataTable();
                                $('.table_wrapper').html(result.table);
                                FN.dataTable();
                            }
                        }
                    );
                    progressBar('off');
					Metronic.initUniform();
                }
            }
        })
   
   });
   
   $('.progtabs').find('li').on('click',function(){
     var href  = $(this).find('a').attr('href');
	 var table = '';
     $('.xformwrap').removeClass('form_wrapper');
     $('.xtablewrap').removeClass('table_wrapper');
     $('.xtablewrap').find('table').removeClass('table');
	 $(href).find('.xformwrap').addClass('form_wrapper');
	 $(href).find('.xtablewrap').addClass('table_wrapper');
     $(href).find('.xtablewrap').find('table').addClass('table');
	 $('.success_message').addClass('hide');
   });
   
   setTimeout(function(){
   $('.form_wrapper').addClass('xformwrap');
   $('.table_wrapper').addClass('xtablewrap');
   $('.progtabs').find('.active').trigger('click');
   },1500);
});