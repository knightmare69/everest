function modifyvalue(e)
{
 var $key = e.getAttribute('data-pointer');
 var $val = e.value;
 $(e).attr('name',$key);
}

$(document).ready(function(){
    $('body').on('click','.btnsave',function(){
	    setAjaxRequest(
			base_url+page_url+'txn','event=save&'+$(this).closest('form').serialize(),
			function(result) {
			   msgbox(result.error == false ? 'success' : 'error',result.message);
			}
		);
		progressBar('off');
	});
});