var Me = function () {
    var table_name = null;

    function initTable(){
        table_name = $('#scho_provider');
        table_name.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": base_url + page_url+'data',
            "deferRender": true,
            "columns": [
                { "data": 'ProvCode', "class": 'autofit' },
                { "data": 'ProvName' },
                { "data": 'ProvAcronym' },
                { "data": 'ProvShort' },
                { "data": 'Inactive', "class": 'autofit text-center' },
                { "data": 'SchoProviderID', 
                  "render": actionLinks, "orderable": false },
            ],
			"initComplete":function(settings,json){
			   $('.dataTables_filter').addClass('pull-right');
			},
           
        });

        
        table_name.delegate('.scho_provider_update_btn','click',function(){
            var data_id = $(this).attr('data-id');
            showModal({
                name: 'basic',
                title: 'Add new scho provider',
                content: ajaxRequest(base_url + page_url + 'event', 'event=updateProvider&id=' + data_id, 'HTML'),
                class: 'modal_scho_provider',
                hasFooter: false
            });
            $('#modal_basic').removeClass('modal_template');
        });

        table_name.delegate('.scho_provider_remove_btn','click',function(){
            var data_id = $(this).attr('data-id');

            bootbox.confirm({
                size : 'small',
                message : "Are you sure you want to remove this?",
                callback : function(yes) {
                    if (yes) 
                    {
                        setAjaxRequest(
                            base_url + page_url + 'event', 
                            'event=removeProvider&id=' + data_id, 
                            function(result) 
                            {
                                if (result.error == false) {
                                    msgbox('success', result.message);
                                    progressBar('off');
                                    table_name.fnDraw();
                                } else {
                                    msgbox('error', result.message);
                                }
                            }
                        );
                        progressBar('off');
                    }
                }
            })
           
        });

    }

    function initBTN(){
        $('#scho_provider_add_btn').click(function () {
            showModal({
                name: 'basic',
                title: 'Add new scho provider',
                content: ajaxRequest(base_url + page_url + 'event', 'event=addProvider', 'HTML'),
                class: 'modal_scho_provider',
                hasFooter: false
            });
            $('#modal_basic').removeClass('modal_template');
            // $(':checkbox').uniform();
            FN.multipleSelect();
        });

        $('body').on('click', '.modal_scho_provider .btn_save', function () {
            if (isFormValid('.modal_scho_provider .form_crud')) {
                confirmEvent("Are you sure you want to save this?", function (yes) {
                    if (!yes) return;

                    var form_data = new FormData();

                    ajaxRequestFile(
                        base_url + page_url + 'event?event=saveProvider&' + $('.form_crud').serialize() + '&FileType=' + $('#FileType').val(),
                        form_data,
                        function (result) {
                            if (result.error == false) {
                                msgbox('success', result.message);
                                clear_value('.form_crud');
                                progressBar('off');
                                closeModal('basic');
                                table_name.fnDraw();
                            } else {
                                msgbox('error', result.message);
                            }
                        }
                    );
                });
            }
        });

        $('body').on('click','.modal_scho_provider .btn_reset', function () {
            $('.form_crud').trigger('reset');
        });
    }

    function actionLinks(data, type, full) {
        var actionValue = $('#action_str').html();
        actionValue = actionValue.replace(/##ID##/g, data);
        return actionValue;
    }

    return {
        init: function () {
            
            initTable();
            initBTN();
        }
    }
}();