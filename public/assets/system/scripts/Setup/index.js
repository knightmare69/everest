'use strict';

var SetupManager = {
    save: function(form, id) {
        var id = (id) ? id : 0,
            data = $('.setup-form#' + form).serialize(),
            form_el = $('.setup-form#' + form);

        form_el.find('.form-group').removeClass('has-error');

        setAjaxRequest(
            base_url + page_url + '/event', {'e': 'save', 'form': form, 'id': id, 'data': data},
            function(result) {

                if (result.error) {
                    $.each(result.err['keys'], function(i, v){
                        form_el.find('#'+v).closest('.form-group').addClass('has-error');
                    });

                    msgbox('error', result.message);

                } else {
                    var tbl = $('table#'+form+'-res-tbl'),
                        dt = tbl.DataTable();

                    if (id != 0) {
                        var row_pos = tbl.find('a.setup-tbl-dt[data-id="'+ id +'"]').parent().siblings('.setup-row-count').text(),
                            d = [row_pos].concat(result.row);

                        dt.row(row_pos - 1).data(d).draw(false);

                    } else {
                        var c = dt.rows()[0].length,
                            d = [c + 1].concat(result.row);

                        dt.row.add(d).draw(false);
                    }

                    $('a[href="#form-' + form + '"]').text('New');
                    form_el.find('.setup-btn-cancel').click();

                    msgbox('success', result.message);
                }
            }
        );
        progressBar('off');

    },

    remove: function(form, id) {
        var id = (id) ? id : 0,
            rem_elem = this.removeElemDetails(form);

        $.SmartMessageBox({
            title : "<span class='text-warning'><i class='fa fa-warning'></i> Warning!</span>",
            content : "Do you wish to remove this record? Click [YES] to proceed..." + '<br><br>' + rem_elem,
            buttons : '[No][Yes]'
        }, function(ButtonPressed) {

            if (ButtonPressed === "Yes") {
                setAjaxRequest(
                    base_url + page_url + '/event', {'e': 'remove', 'form': form, 'id': id},
                    function(result) {

                        if (result.error) {
                            msgbox('error', result.message);
                        } else {
                            var tbl = $('#' + form + '-res-tbl'),
                                row_pos = tbl.find('tr.active-row td:first').text();

                            tbl.DataTable().row(row_pos - 1).remove().draw(false);

                        }
                    }
                );
                progressBar('off');
            }

            return false;
        });

    },

    removeElemDetails: function (elem_id) {
        var tbl = $('#' + elem_id + '-res-tbl'),
            tbl_arow = tbl.find('tr.active-row td'),
            det = '';

            tbl.find('thead tr th').each(function(i, v) {
                if (i != 0) {
                    det += $(this).text() + ': ' + tbl_arow.eq(i).text() + '<br>';
                }
            });

        return det;
    },

    get: function(form, id) {
        var id = (id) ? id : 0,
            form_el = $('.setup-form#' + form);

        setAjaxRequest(
            base_url + page_url + '/event', {'e': 'get', 'form': form, 'id': id},
            function(result) {

                if (result.error) {
                    msgbox('error', result.message);

                } else {
                    $('.tab-pane#form-' + form).html(result.form).find('select.setup-select2').select2();

                    Metronic.initUniform();

                    $('a[href="#form-' + form + '"]').text('Edit');

                    if (form == 'ayterm') {
                        AY_Term.runDP('.input-daterange');
                        AY_Term.academicYear();
                    }

                    msgbox('success', result.message);
                }
            }
        );
        progressBar('off');

    },

    getDT_tblId: function(parent_elem) {
        var elem = parent_elem.closest('div.dataTables_filter').attr('id').split('-');

        return elem[0];
    },

    run: function() {
        $('body').on('click', '.setup-tbl-dt',function(e) {
            var form = $(this).attr('data-for'),
                id = $(this).attr('data-id');

            SetupManager.get(form, id);
        });

        // Custom DT
        $('#bldg-res-tbl, #rooms-res-tbl, #ayterm-res-tbl').DataTable({
            'aaSorting': [],
        });

        $('.dataTables_filter').html($('.setup-tbl-action').html());

        $('body').on('keyup', '.setup-search-dt-txt', function(e) {
            var dt_id = $(this).closest('.dataTables_filter').attr('id').replace('_filter', ''),
                s_val = $(this).val();

            $('#' + dt_id).DataTable().search(s_val).draw();
        });
        // End

        $('body').on('click', '.setup-btn-save', function(e) {
            e.preventDefault();
            var act_form = $(this).closest('form.setup-form');
            SetupManager.save(act_form.attr('id'), act_form.attr('data-id'));
        });

        $('body').on('click', '.setup-btn-cancel', function(e) {
            var act_form = $(this).closest('form.setup-form'),
                tab = act_form.attr('id');

            act_form.find('input').val('');
            // act_form.find('select').select2('val', act_form.find('select option:first').val());
            act_form.find('input[type=checkbox]').attr('checked', false);
            act_form.attr('data-id', 0);
            $('a[href="#form-' + tab + '"]').text('New');
            Metronic.updateUniform();
        });

        $('body').on('click', '.setup-btn-new', function(e) {
            var tab = SetupManager.getDT_tblId($(this)),
                form_tab = $('a[href="#form-' + tab + '"]');

            $('form#' + tab).find('button.setup-btn-cancel').click();
            form_tab.text('New').click();
        });

        $('body').on('click', '.setup-btn-remove', function(e){
            var tbl_id = SetupManager.getDT_tblId($(this)),
                active_row = $('#'+ tbl_id +'-res-tbl tbody tr.active-row').find('a.setup-tbl-dt'),
                form = active_row.attr('data-for'),
                id = active_row.attr('data-id');

            SetupManager.remove(form, id);
        });

    }
}
