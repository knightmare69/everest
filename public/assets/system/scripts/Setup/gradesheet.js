$(document).ready(function(){
  $('body').on('change','[data-id]',function(){
	 $(this).addClass('henshin'); 
  });
  
  $('body').on('change','#yrlvl',function(){
	 var tmpval   = $(this).val();
     var progid   = $(this).find(':selected').attr('data-progid');
	 $('#progid').val(progid);	 
  });
  
  $('body').on('change','#progid',function(){
	 $('#major').val(-1);	 
  });
  
  $('body').on('click','.btn-save',function(){
	 var campus = $('#campus').val();
	 var term = $('#ayterm').val();
	 var data = $('#frmGradeSetup').serialize()+'&campus='+campus+'&term='+term;  
	 progressBar('on');
	 setAjaxRequest(
	        base_url+'setup/grade-setup/event?event=xsave',data,
			function(r)
			{
			  alert('success');
			  $('.btn-refresh').click();
	 		},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  
  $('body').on('click','[data-id]',function(){
	  $('#tblyrlvl').find('.info').removeClass('info');
	  $(this).closest('tr').addClass('info');
	  $('#yrlvl').find('[value="'+$(this).attr('data-id')+'"][data-progid="'+$(this).attr('data-prog')+'"]').attr('selected',true);
	  $('#progid').val($(this).attr('data-prog'));
	  $('#transmute').val($(this).attr('data-transid'));
	  $('#transopt').val($(this).attr('data-option'));
	  $('#gradsys').val($(this).attr('data-letter'));
  }); 	  
  
  $('body').on('click','.btn-refresh',function(){
	 var campus = $('#campus').val();
	 var term   = $('#ayterm').val();
	 var data   = 'term='+term+'&campus='+campus;  
	 progressBar('on');
	 setAjaxRequest(
	        base_url+'setup/grade-setup/event?event=xload',data,
			function(r)
			{
			  $('#tblyrlvl').find('tbody').html(r.content);
			  progressBar('off');
	 		},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load!',function(){return;});	
			  progressBar('off');
			}
	 );	
  });
  $('.btn-refresh').click();
});