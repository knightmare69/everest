'use strict';

var AY_Term = {
    runDP: function(elem) {
        $(elem).datepicker({
            // dateFormat: 'mm/dd/yyyy',
            // startDate: '-0m',
            // useCurrent: true,
            // setDate: new Date(),
        }).on('changeDate', function(e) {
            var num_w = AY_Term.computeDP_Weeks(),
                ay = AY_Term.academicYear();
            $('#weeks').val(num_w);
            $('#academic_year').val(ay);
        });

    },

    computeDP_Weeks: function(sd, ed) {
        var sd = (sd) ? sd : $('#period_from').val(),
            ed = (ed) ? ed : $('#period_to').val(),

            week = 1000*60*60*24*7,
            nsd = new Date(sd),
            ned = new Date(ed),
            diff = Math.floor(Math.abs((nsd - ned)/(week)));

        return (diff) ? diff : 0;
    },

    academicYear: function(sy, ey, term) {
        var sy = (sy) ? sy : $('#period_from').datepicker('getDate').getFullYear(),
            ey = (ey) ? ey : $('#period_to').datepicker('getDate').getFullYear(),
            term = (term) ? term : $('#school_term').val(),
            ret = '';

        if(sy && ey) {
            if(term == 'Summer') {
                ret = (sy - 1) + '-' + sy;
            } else if(term == '1st Semester') {
                ret = sy + '-' + (ey + 1);
            } else {
                ret = sy + '-' + ey;
            }
        }

        return ret;
    },

    run: function() {
        this.runDP('.input-daterange');

        $('body').on('change', '#school_term', function(e) {
            $('#academic_year').val(AY_Term.academicYear());
        });
    }
};

$('document').ready(function(){

	$('body').on('click', '.setup-btn-save-days', function(e){
		e.preventDefault();
		var data=[];
		var seq=[];
		var i =1;
		$('.days').each(function(){
			var d = { code: $(this).attr('data-code'), val : $(this).val() };
			data.push(d);
		});

		$('.seqno').each(function(){
			seq[i] = $(this).val();
			++i;
		});
		setAjaxRequest(
			base_url + page_url + '/event', {'e': 'save','form': 'school-days' , 'id': $('#att-table').attr('data-id') , 'update': data,'seq':seq},
			function(result) {
				if (result.error) {
					msgbox('error', result.message);
				} else {
					msgbox('success', result.message);
				}
			}
		);
		progressBar('off');
	});
});
