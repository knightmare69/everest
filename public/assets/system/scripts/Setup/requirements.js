var Me = function() {
	var refreshReq = function(key) {
		var key = key == undefined ? '' : key;
		setAjaxRequest(base_url+page_url+'event','event=showRequirements&key='+key,
			function(data) {
				$('#table_wrapper').html(data);
				$(':checkbox').uniform();
			},'','html',true,false
		);
	}

	var refreshTemplate = function() {
		setAjaxRequest(base_url+page_url+'event','event=showTemplates',
			function(data) {
				var options = "<option value=''>Select</option>";
				for(var i in data) {
					var option = document.createElement('option');
					option.setAttribute('value',data[i].id);
					option.appendChild(document.createTextNode(data[i].prog+' - '+data[i].name+' - '+data[i].type));
					options += option.outerHTML;
				}
				$('#Template').html(options);
			},'','json',true,false
		);
	}

	var updateIsRequired = function(self) {
		setAjaxRequest(
			base_url+page_url+'event',
			'event=updateIsRequired&key='+$('#Template option:selected').val()+'&RequirementID='+self.closest('tr').attr('data-id')+'&IsRequired='+(self.is(':checked') ? 1 : 0),
			function(result) {
				console.log(result.message ? result.message : 'Could not update');
				progressBar('off');
			},
			function() {
				progressBar('off');
			},'json',true,false
		);
	}

	var updateAllowUpload = function(self) {
		setAjaxRequest(
			base_url+page_url+'event',
			'event=updateAllowUpload&key='+$('#Template option:selected').val()+'&RequirementID='+self.closest('tr').attr('data-id')+'&AllowUpload='+(self.is(':checked') ? 1 : 0),
			function(result) {
				console.log(result.message ? result.message : 'Could not update');
				progressBar('off');
			},
			function() {
				progressBar('off');
			},'json',true,false
		);
	}

	var updateSlot = function(self) {
		setAjaxRequest(
			base_url+page_url+'event',
			'event=updateSlot&key='+$('#Template option:selected').val()+'&RequirementID='+self.closest('tr').attr('data-id')+'&Slot='+self.val(),
			function(result) {
				console.log(result.message ? result.message : 'Could not update');
				progressBar('off');
			},
			function() {
				progressBar('off');
			},'json',true,false
		);
	}

	var updateCode = function(self) {
		setAjaxRequest(
			base_url+page_url+'event',
			'event=updateCode&key='+$('#Template option:selected').val()+'&RequirementID='+self.closest('tr').attr('data-id')+'&Code='+self.val(),
			function(result) {
				console.log(result.message ? result.message : 'Could not update');
				progressBar('off');
			},
			function() {
				progressBar('off');
			},'json',true,false
		);
	}

	return {
		init: function() {

			$(window).load(function() {
				refreshReq($('#Template').find('option:selected').val());
			});

			$('#aAddNewRequirement').click(function() {
				 showModal({
	                name: 'basic',
	                title : 'Add new requirements',
	                content: ajaxRequest(base_url+page_url+'event','event=modalRequirements','HTML'),
	                class: 'modal_requirements',
	                hasFooter: false
	            });
				$('#modal_basic').removeClass('modal_template');
				$(':checkbox').uniform();
				FN.multipleSelect();
			});

			$('#aAddNewTemplate').click(function() {
				 showModal({
	                name: 'basic',
	                title : 'Add new Template',
	                content: ajaxRequest(base_url+page_url+'event','event=modalTemplate','HTML'),
	                class: 'modal_template',
	                hasFooter: false
	            });
				$('#modal_basic').removeClass('modal_requirements');
				FN.multipleSelect();
			});

			$('body').on('change','#Template',function() {
				 refreshReq($(this).find('option:selected').val());
			});

			$('body').on('click','.IsRequired',function() {
				 updateIsRequired($(this));
			});

			$('body').on('click','.AllowUpload',function() {
				 updateAllowUpload($(this));
			});

			$('body').on('bind change keyup','.Slot',function() {
				 updateSlot($(this));
			});

			$('body').on('bind change keyup','.Code',function() {
				 updateCode($(this));
			});

			$('body').on('click','.modal_requirements .btn_save', function() {
				if (isFormValid('.modal_requirements .form_crud')) {
					confirmEvent("Are you sure you want to save this?", function(yes) {
						if (!yes) return;

						var form_data = new FormData();

						if ($('#file')[0] != undefined && $('#file')[0].files[0] != undefined) {
							form_data.append('file',$('#file')[0].files[0]);
						}

						ajaxRequestFile(
							base_url+page_url+'event?event=saveRequirements&'+$('.form_crud').serialize()+'&FileType='+$('#FileType').val(),
							form_data,
							function(result) {
								if (result.error == false) {
									msgbox('success',result.message);
									clear_value('.form_crud');
									refreshReq();
									progressBar('off');
								} else {
									msgbox('error',result.message);
								}
							}
						);
					});
				}
			});

			$('body').on('click','.modal_template .btn_save', function() {
				if (isFormValid('.modal_template .form_crud')) {
					confirmEvent("Are you sure you want to save this?", function(yes) {
						if (!yes) return;
						setAjaxRequest(base_url+page_url+'event','event=saveTemplate&'+$('.modal_template .form_crud').serialize(),
							function(result) {
								if (result.error == false) {
									msgbox('success',result.message);
									clear_value('.form_crud');
									refreshTemplate();
									progressBar('off');
								} else {
									msgbox('error',result.message);
								}
							}
						);
					});
				}
			});

			$('body').on('click','#BtnSaveTemplate', function() {
				if (!$('#Template').val() || !isValid()) return;
				confirmEvent("Are you sure you want to save this?", function(yes) {
					if (!yes) return;

					setAjaxRequest(base_url+page_url+'event','event=saveTemplateItems&data='+getData()+'&key='+$('#Template option:selected').val(),
						function(result) {
							if (result.error == false) {
								msgbox('success',result.message);
								refreshTemplate();
								progressBar('off');
							} else {
								msgbox('error',result.message);
							}
						}
					);
				});

				function getData() {
					var data = new Array();
					$('#TableRequirements tbody tr').each(function() {
						var self = $(this);
						if (self.find('.chk-child').is(':checked')) {
							data.push({
								key: self.attr('data-key'),
								id: self.attr('data-id'),
								Code: self.find('.Code').val(),
								Slot: self.find('.Slot').val(),
								IsRequired: self.find('.IsRequired').is(':checked') ? 1 : 0,
								AllowUpload: self.find('.AllowUpload').is(':checked') ? 1 : 0,
								HasFormat: self.find('.HasFormat').is(':checked') ? 1 : 0,
							});
						}
					});
					console.log(data);
					return JSON.stringify(data);
				}

				function isValid() {
					if ($('#TableRequirements tbody tr td .chk-child:checked').length > 0) {
						return true;
					}
					return false;
				}
			});
		}
	}
}();