var tmpid = 0;
function load_list(){
	progressBar('on');
	setAjaxRequest(
		base_url+'setup/scho-granttemplate/event',{event:'list'},
		function(r){
		  if(r.success){
		    $('#grantlist').find('tbody').html(r.content);
		  }else
			msgbox('danger',r.message);
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load data!',function(){return;});	
		  progressBar('off');
		}
	);
}		  
		  
function load_info(schoid){
	progressBar('on');
	setAjaxRequest(
		base_url+'setup/scho-granttemplate/event',{event:'info',schoid:schoid},
		function(r){
		  if(r.success){
			 $('#frmdata').find('input[type="text"]').val('');
			 $('#frmdata').find('select').val('-1');
	         $('#frmdata').prepend('<input type="hidden" id="xid" name="xid" value="'+r.content.GrantTemplateID+'"/>');
	         $('#frmdata').find('#term').val(r.content.TermID);
	         $('#frmdata').find('#term').val(r.content.SchoProviderID);
	         $('#frmdata').find('#code').val(r.content.TemplateCode);
	         $('#frmdata').find('#name').val(r.content.ShortName);
	         $('#frmdata').find('#desc').val(r.content.Description);
	         $('#frmdata').find('#provider').val(r.content.SchoProviderID);
	         $('#frmdata').find('#type').val(r.content.SchoType);
	         $('#tbldetails').find('tbody').html('');
		     
			 for(var i=0;i<r.detail.length;i++){
		       var tmprow = $('#tbldetails').find('[data-id="tmp"]').clone();
	           var accttn = $('#accttype').find('option[value="'+r.detail[i].TypeUsed+'"]').html();
			   tmprow.attr('data-id',r.detail[i].AcctID);
			   tmprow.attr('data-acct',r.detail[i].AcctID);
			   tmprow.attr('data-type',r.detail[i].TypeUsed);
			   tmprow.find('td:nth-child(2)').html(r.detail[i].AcctCode);
			   tmprow.find('td:nth-child(3)').html(r.detail[i].AcctName);
			   tmprow.find('td:nth-child(4)').html(accttn);
			   tmprow.find('td:nth-child(5)').html(parseFloat(r.detail[i].Amount).toFixed(2));
			   tmprow.find('td:nth-child(6)').html(r.detail[i].Remarks);
			   $('#tbldetails').find('tbody').append(tmprow);
			 }
		  }else
			msgbox('danger',r.message);
		  progressBar('off');
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load data!',function(){return;});	
		  progressBar('off');
		}
	);
}

$('document').ready(function(){
	$('body').on('blur','.numberonly',function(){
	  var tmpval = $(this).val();
	      format = parseFloat(tmpval);
	  
	  if(format!=undefined && isNaN(format)==false){
	    $(this).val(format.toFixed(2));
	  }else{
	    alert('Invalid Input');
		$(this).val('0.00');
	  }  	  
	});
	
	$('body').on('click','.btnrefresh',function(){
	  $('.dvtable').find('.caption').html('<i class="fa fa-table"></i> List of Template');
	  $('.dvtable').find('.dvdetails').addClass('hidden');
	  $('.dvtable').find('.dvlist').removeClass('hidden');
	  $('#frmdata').find('#xid').remove();
	  $('#frmdata').find('input[type="text"]').val('');
	  $('#frmdata').find('select').val('-1');
	  $('#tbldetails').find('tbody').html('');
	  load_list();
	});
	
	$('body').on('click','.btndelete',function(){
	    var tmpid = $(this).closest('tr').data('id');
		if(confirm('Are you sure you want to remove this item?')){
		 progressBar('on');
		 setAjaxRequest(
			base_url+'setup/scho-granttemplate/event',{event:'remove','schoid':tmpid},
			function(r){
			  if(r.success){
				$('.btnrefresh').trigger('click');
			  }else
				msgbox('danger',r.message);
				
			  progressBar('off');
			},
			function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to remove data!',function(){return;});	
			  progressBar('off');
			}
		 );
		}else
		   console.log('Cancel');
	});
	
	$('body').on('click','.btn-remove-list',function(){
	  $(this).closest('tr').remove();
	});
	
	$('body').on('click','.btnadd',function(){
		// tmpid = $(this).closest('tr').data('id');
		console.log(tmpid);
	    var acctid = $('#acctname').attr('data-id');
	    var acc = $('#acct').val();
	    var acctcd = $('#acct').find(':selected').attr('acctcode'); // get account code
	    var acctnm = $('#acct').find('option[value="'+acc+'"]').html(); // get account name
	    var acctty = $('#accttype').val(); // get account type 
	    var accttn = $('#accttype').find('option[value="'+acctty+'"]').html(); // get adjustment	
	    var rate   = $('#rate').val(); // get rate value
	    var remark = $('#remarks').val(); // get remarks
		var tmprow = $('#tbldetails').find('[data-id="tmp"]');

		if(tmpid != 0){

			if(acc!=0 && rate>0){
			    tmprow.attr('data-id',acctid);
			    tmprow.attr('data-acct',acctid);
			    tmprow.attr('data-type',acctty);
			    tmprow.find('td:nth-child(2)').html(acctcd);
			    tmprow.find('td:nth-child(3)').html(acctnm);
			    tmprow.find('td:nth-child(4)').html(accttn);
			    tmprow.find('td:nth-child(5)').html(rate);
			    tmprow.find('td:nth-child(6)').html(remark);
			    $('#tbldetails').find('tbody').append(tmprow);
			}

		progressBar('on');
		
			setAjaxRequest(
				base_url+'setup/scho-granttemplate/event',{event:'save-details', schoid: tmpid, accountid : acc, type: acctty, amount: rate, r: remark},'&schoid='+tmpid,
				// xurl+'?event=save',xdata,
				function(r){
				  if(r.success){
					msgbox('success','Successfully Saved');
					$('.acctcode').html('');
					$('#acctname').data('id',0);
					$('.acctform').find('input[type="text"]').val('');
					$('.acctform').find('select').val('0');
				  }else{
				    msgbox('danger',((r.message=='' || r.message==undefined)?'failed':r.message));
				  }
				  progressBar('off');
				},
				function(err){
				  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
				  progressBar('off');
				}
			);
		}
		else{ progressBar('off'); alert('select template first!');}

		
	});
	
	$('body').on('click','.btn-modal-select',function(){
		var selrow = $('.modal_list').find('.info');
		var ftype  = $('#modal_filter').attr('data-type');
		
		var acctid = $(selrow).attr('data-list');
		var acctcd = $(selrow).find('td:nth-child(1)').html();
		var acctnm = $(selrow).find('td:nth-child(2)').html();
		
		$('.acctcode').html(acctcd);
		$('#acctname').attr('data-id',acctid);
		$('#acctname').val(acctnm);
		
		$('#modal_filter').modal('hide');
	});
	
	$('body').on('click','.btnsearch',function(){
		progressBar('on');
		setAjaxRequest(
			base_url+'setup/scho-granttemplate/event',{event:'filter',src:'account'},
			function(r){
			  if(r.success){
				$('#modal_filter').find('.modal_list').html(r.content);
		        $('#modal_filter').modal('show');
			  }
			  progressBar('off');
			},
			function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load data!',function(){return;});	
			  progressBar('off');
			}
		);
	});
	
	$('body').on('click','.modal_list [data-list]',function(){
		$('.modal_list [data-list]').removeClass('info');
		$(this).addClass('info');
	});
	
	$('body').on('click','.btnedit',function(){
      tmpid = $(this).closest('tr').data('id');
	  $('.dvtable').find('.caption').html('<i class="fa fa-cog"></i> Details');
	  $('.dvtable').find('.dvlist').addClass('hidden');
	  $('.dvtable').find('.dvdetails').removeClass('hidden');
	  load_info(tmpid);
	});
	
	$('body').on('click','.btnnew',function(){
	  $('.dvtable').find('.caption').html('<i class="fa fa-cog"></i> Details');
	  $('.dvtable').find('.dvlist').addClass('hidden');
	  $('.dvtable').find('.dvdetails').removeClass('hidden');
	  $('#frmdata').find('#xid').remove();
	  $('#frmdata').find('input[type="text"]').val('');
	  $('#frmdata').find('select').val('-1');
	  tmpid = 0;
	});
	
	$('body').on('click','.btnsave',function(){
		
		var xdata = $('#frmdata').serialize();
		var xurl  = base_url+'setup/scho-granttemplate/event';
		var t =  $('#term').val();
		var c  = $('#frmdata').find('#code').val();
		var n  = $('#frmdata').find('#name').val();
		var d  = $('#frmdata').find('#desc').val();
		var p = $('#frmdata').find('#provider').val();
		var ty = $('#frmdata').find('#type').val();
		var tdtl  = $('#tbldetails tbody').find('tr[data-id]').length;

		// var xdtl  = '';	
		// if(code==undefined || code=='' || name==undefined || name=='' || tdtl<=0){
		//   return false;
		// }
		
		// $('#tbldetails tbody').find('tr[data-id]').each(function(){
		//    var xid   = $(this).attr('data-id');  
		//    var type  = $(this).attr('data-type');  
		//    var amt   = $(this).find('td:nth-child(5)').html();
		//    var rmks  = $(this).find('td:nth-child(6)').html();  
		// 	   xdtl += ((xdtl=='')?'':',')+xid+'|'+type+'|'+amt+'|'+rmks;
		// });
		
		// xdata = (xdata+'&detail='+xdtl);
		
		progressBar('on');
		setAjaxRequest(
			base_url+'setup/scho-granttemplate/event',{event:'save', term:t, code:c, name:n, desc:d,provider:p, type:ty},'&detail='+xdata,
			// xurl+'?event=save',xdata,
			function(r){
			  if(r.success){
				msgbox('success','Successfully Saved');
			  }else{
			    msgbox('danger',((r.message=='' || r.message==undefined)?'failed':r.message));
			  }
			  progressBar('off');
			},
			function(err){
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load Data!',function(){return;});	
			  progressBar('off');
			}
		);
		load_list();

	});
});