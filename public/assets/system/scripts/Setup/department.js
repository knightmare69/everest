$(document).ready(function(e){

    var cur_showed_modal = '';

    $('body').on('click', '.employee-search-btn', function (e) {
        cur_showed_modal = $(this).attr('data-for');

        var search = $(this).parent().prev('.employee').val(),
            dept = $(this).closest('.input-group').prev().text();

        setAjaxRequest( base_url + 'grading-system/areas/event', {'event': 'search-coordinator', 'search-coord': search},
            function(result) {
                if (result.error) {
                    msgbox('error', result.message);
                } else {
                    progressBar('off');
                    showModal({
                        name: 'basic',
                        title: 'Department ' + dept ,
                        content: result.html,
                        class: 'modal_coord_result',
                        hasModalButton: false
                    });
                    $('#table-coord-res').DataTable({ "aaSorting" : [], "bDestroy" : true });
                }
            }
        );
        progressBar('off');
    });

    $('body').on('click', '.employee-remove-btn', function(e){
        // $(this).prev().prop('disabled', false);
        $(this).parent().prev().attr({'data-id': '', 'disabled': false}).val('');
    });

    $('body').on('click', '.btn-coord-select-btn', function(e){
        var d = $(this).data(), id = '';

        switch (cur_showed_modal) {
            case 'head':
                id = 'dept-head';
                break;
            case 'vice':
                id="dept-vice";
                break;
            default:
                return false;
        }

        $('#' + id).val(d.id);
        $('button[data-for=' + cur_showed_modal + ']').parent().prev().val(d.name);
        closeModal('basic');
        $('#table-coord-res').remove();
    });

    $('body').on('click', '.employee-remove-btn', function(e){
        var target_input = $(this).attr('data-empty-val');
        $(target_input).val('');
    });

});
