function lazyLoad(_calback, time) {
    var time = time == undefined ? 500 : time;
    setTimeout(function(){
        _calback();
    }, time);
}

function onProcess(el,src,msg,process) {
    var process = process == undefined ? true : false;
    var el = (!src ||  src == undefined) ? $(el) : src;
    var msg = (!msg || msg == undefined) ? 'loading...' : msg;
    if (process) {
        var ht = el.html();
        el.attr('data-place',ht);
        el.html(msg);
    } else {
        el.html(el.attr('data-place'));
    }
}

function mergeObject(obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
}
function setDateFormatTo(date,givenDate,formatTo) {
    var date = date.split(' ')[0];
    switch(givenDate.toLowerCase()) {
        case 'yyyy-mm-dd':
            date = date.split('-');
            switch(formatTo.toLowerCase()) {
                case 'mm/dd/yyyy':
                    date = date[1]+'/'+date[2]+'/'+date[0];
                break;
            }
        break;
    }
    return date;
}

function confirmEvent(message,callback) {
    bootbox.confirm({
        size: 'small',
        message: message,
        callback: callback
    });
}

function showError(message) {
    $('.error_message').removeClass('hide');
    $('.error_message message').html(message);
    $('.success_message').addClass('hide');
}

function showSuccess(message) {
    $('.success_message').removeClass('hide');
    $('.success_message message').html(message);
    $('.error_message').addClass('hide');
}

function hideError(message) {
    $('.error_message').addClass('hide');
}

function hideSuccess(message) {
    $('.success_message').addClass('hide');
}


function decimal(value) {
    var deci = value.toString().split(','), decimal_value = '';
    for(var i=0;i<deci.length;i++) {
        decimal_value += deci[i].replace(',','');
    }
    return decimal_value;
}
function changeColumnWidth(table,width,column) {
    $('body '+table+' thead tr').each(function(){
        $(this).find('th').eq(column).css({'width':width},'fast');
    });
    $('body '+table+' tbody tr').each(function(){
        $(this).find('td').eq(column).css({'width':width},'fast');
    });
}
function confirmTemplate(){
    $('#modal_large .modal-footer .blue').text('Confirm');
    $('#modal_large .modal-header h4').attr("style",'color: #fff !important');
    $('#modal_large .modal-body,#modal_large .modal-body .portlet-body').attr('style','background-color: #e9edef  !important');
    $('#modal_large .modal-header,#modal_large .modal-footer').attr('style','background-color: #7eb839  !important');
}

function closeModal(name){
    var name = name ? name.toLowerCase() : 'large';
    if (name == 'large') {
        modal = '#modal_large';
    }
    else if (name == 'large2') {
        modal = '#modal_large2';
    }
    else if (name == 'basic') {
        modal = '#modal_basic';
    }
    else if (name == 'basic2') {
        modal = '#modal_basic2';
    } else {
        modal = name;
    }
    jQuery(modal).modal('hide');
}
function unloadProgressBarModal(){
     // $('#progress_modal').modal('hide');
   window.setTimeout(function() {
        Metronic.stopPageLoading();
    }, 300);
}
function showModal(data){
    var modal;
    var name = data.name != undefined ? data.name.toLowerCase() : 'large';
    var content = data.content == undefined ? '' : data.content;
    var title = data.title == undefined ? '' : data.title;
    var button =  data.button != undefined ?  data.button : [];
    var icon = button.icon != undefined ? "<i class='"+button.icon+"'></i> " : '';
    var caption = button.caption != undefined ? button.caption : 'Save';
    var datamenu = button.menu != undefined ? button.menu : '';
    var hasFooter = data.hasFooter == undefined ? true : data.hasFooter;
    var hasModalButton = data.hasModalButton == undefined ? true : data.hasModalButton;
    var hasActionButton = data.hasActionButton == undefined ? true : data.hasActionButton;

    if (name == 'large') {
        modal = '#modal_large';
    }
    else if (name == 'large2') {
        modal = '#modal_large2';
    }
    else if (name == 'basic') {
        modal = '#modal_basic';
    }
    else if (name == 'basic2') {
        modal = '#modal_basic2';
    } else {
        modal = name;
    }
    modal = jQuery(modal);
    modal.modal('show');
    modal.find('.modal-header h4').html(title);
    if (content) {
        modal.find('.modal-body').html(content);
    }
    modal.find('.modal-footer .btn_modal').html(icon+caption);
    if (hasFooter )  {
        modal.find('.modal-footer').removeClass('hide');
    } else {
        modal.find('.modal-footer').addClass('hide');
    }
    if (!hasModalButton) {
         modal.find('.modal-footer .btn_modal').addClass('hide');
    }

     if (hasActionButton )  {
        modal.find('.modal-footer .btn_modal').removeClass('hide');
    } else {
        modal.find('.modal-footer .btn_modal').addClass('hide');
    }
    
    modal.find('.modal-footer .btn_modal').attr('data-menu',datamenu);
        
    if(button.class != undefined) {
        modal.find('.modal-footer .btn_modal').addClass(button.class);
    }
    
    if (data.class != undefined) {
        modal.addClass(data.class);
    }
}
function progressBarModal(){
    // $('#progress_modal').modal('show');
    Metronic.startPageLoading({animate: true});
}
function print_reports(url){
    window.open(url,'_blank','toolbar=no, location=no');
}
function changeAddressUrl(urlPath){
    document.title = 'Administrative System';
    window.history.pushState(
        "",
        "",
        urlPath
   );
}
function browser(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1]||'');
        }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return 'Opera '+tem[1];}
        }
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return  M[0].toLowerCase();
}
function browserVersion() {
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1]||'');
        }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return 'Opera '+tem[1];}
        }
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return M[1];
}
function clearconsole() {
  // if ( browserVersion() >= 4 ) {
  //   console.log(window.console);
  //   if(window.console || window.console.firebug) {
  //    console.clear();
  //   }
  // }
}
function message(message){
    $('.dialog-message').text('');
    $('.dialog-message').html('<p>'+message+'</p>');
    $('.dialog-message').dialog('open');
}
function load_modal(message,title){
    var title=title != undefined ? title : '';
    $('#dialog-message .modal-title').html(title);
    $('#dialog-message .modal-body').html(message);
    $('#dialog-message').modal('show');
}
function isFormValid(form_name,hasErrorBoarder){
    var isValid = true;
    var hasErrorBoarder = hasErrorBoarder == undefined ? true : hasErrorBoarder;
    //text
    $(form_name+' input:text').each(function() {
        if (hasErrorBoarder) {
            // $(this).removeAttr('style');
            hideHelpBlock($(this));
        }
        if (!$(this).hasClass('not-required')){
            if ($(this).hasClass('form-control') && $(this).val() == '')  {
                if (hasErrorBoarder) {
                    // $(this).attr('style','border: 1px solid red');
                    addHelpBlockError($(this));
                }
                isValid = false;
            }
        }
        if ($(this).hasClass('form-control') && ($(this).attr('data-minlength') != '' && $(this).attr('data-minlength') != undefined)) {
            if ($(this).val().length < $(this).attr('data-minlength')) {
                 if (hasErrorBoarder) {
                    addHelpBlockError($(this),'','Minimum at least 3 character.');
                    // $(this).attr('style','border: 1px solid red');
                }
                isValid = false;
            } else {
                hideHelpBlock($(this));
            }
        }
        if ($(this).hasClass('form-control') && ($(this).attr('data-minage') != '' && $(this).attr('data-minage') != undefined)) {
            if (isNaN(parseFloat(getAge($(this).val()))) || (parseFloat(getAge($(this).val())) < $(this).attr('data-minage'))) {
                 if (hasErrorBoarder) {
                    addHelpBlockError($(this),'','Minimum of age atleast '+$(this).attr('data-minage')+' years old.');
                }
                isValid = false;
            } else {
                hideHelpBlock($(this));
            }
        }
    });
     $(form_name+' input[type="password"]').each(function() {
        if (hasErrorBoarder) {
            // $(this).removeAttr('style');
            hideHelpBlock($(this));
        }
        if (!$(this).hasClass('not-required')) {
            if ($(this).val() == '') {
                if (hasErrorBoarder) {
                    // $(this).attr('style','border: 1px solid red');
                    addHelpBlockError($(this));
                }
                isValid = false;
            }
        }
    });
    $(form_name+' input[type="email"]').each(function() {
        if (hasErrorBoarder) {
            // $(this).removeAttr('style');
            hideHelpBlock($(this));
        }
        if (!$(this).hasClass('not-required')) {
            if (($(this).val() == '' || $(this).val().split('@').length <= 1)) {
                if (hasErrorBoarder) {
                    // $(this).attr('style','border: 1px solid red');
                    addHelpBlockError($(this),'','Invalid Email address.');
                }
                isValid = false;
            }
        }
    });
    //Textarea
    $(form_name+' textarea').each(function() {
        if (hasErrorBoarder) {
            // $(this).removeAttr('style');
            hideHelpBlock($(this));
        }
        if (!$(this).hasClass('not-required') && ( $(this).hasClass('form-control') && $(this).val() == '') ) {
            if (hasErrorBoarder) {
                // $(this).attr('style','border: 1px solid red');
                addHelpBlockError($(this));
            }
            isValid = false;
        }
    });
    //Select
    $(form_name+' select').each(function() {
        if (hasErrorBoarder) {
            $(this).removeAttr('style');
            hideHelpBlock($(this));
        }
        $(form_name+ ' '+' #s2id_'+$(this).attr('name')).removeAttr('style');
        var value = $(this).find('option:selected').val() == undefined ?  '' : $(this).find('option:selected').val();
        if (!$(this).hasClass('not-required') && value.trim() == '') {
            if ($(this).hasClass('select2')) {
                if (hasErrorBoarder) {
                    addHelpBlockError($(this));
                    //$(form_name+' #s2id_'+$(this).attr('name')).attr('style','border: 1px solid red !important');
                }
            }
            if (hasErrorBoarder) {
                // $(this).attr('style','border: 1px solid red');
                addHelpBlockError($(this));
            }
            isValid = false;
        }
    });
    return isValid;
}
function addHelpBlockError(self,caption,caption2) {
    var caption = caption == undefined || caption == '' ? 'This field is required. ' : caption;
    var caption2 = caption2 == undefined ? '' : caption2;
    if (self.closest('div').find('span').hasClass('help-block')) {
        self.closest('div').find('span.help-block').removeClass('hide');
        self.closest('div').find('span.help-block').html(caption+caption2);
    } else {
        self.closest('div').append('<span class="help-block font-red">'+caption+caption2+'</span>');
    }
}
function hideHelpBlock(self) {
    if (self.closest('div').find('span').hasClass('help-block')) {
       self.closest('div').find('span.help-block').addClass('hide');
    }
}
function getAge(birthday) {
    var birthday = replaceByChar(birthday,'-','/');
    var birthdate = new Date(birthday);
    var cur = new Date();
    var diff = cur-birthdate;
    var age =(diff/31536000000);
    age = (age <= 0 || isNaN(age) || !age) ? 0 : age.toFixed(1);
    if (age <= 0) {
        return 0;
    }
    age = age.split('.');
    return age[0]+'.'+((age[1].length <= 1) ? '0'+age[1] : age[1]);
}
function inWords(s)
{
	if ( parseFloat(s) == 0) {
		return 'zero';
	}
    var th = ['','thousand','million', 'billion','trillion'];
    var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
    var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
    var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
    s = s.toString();
    s = s.replace(/[\, ]/g,'');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i=0; i < x; i++)
    {
        if ((x-i)%3==2)
        {
            if (n[i] == '1')
            {
                str += tn[Number(n[i+1])] + ' ';
                i++;
                sk=1;
            }
            else if (n[i]!=0)
            {
                str += tw[n[i]-2] + ' ';
                sk=1;
            }
        }
        else if (n[i]!=0)
        {
            str += dg[n[i]] +' ';
            if ((x-i)%3==0) str += 'hundred ';
            sk=1;
        }
        if ((x-i)%3==1)
        {
            if (sk) str += th[(x-i-1)/3] + ' ';
            sk=0;
        }
    }
    if (x != s.length)
    {
        //var y = s.length;               
		// for (var i=x+1; i<y; i++){ str += dg[n[i]] +' '; }				
		var y = s.split('.');
		if( parseFloat(y[1]) > 0){
			str += ' and ' + inWords(y[1]) + 'cent(s)';	
		}		
		
    }
    return str.replace(/\s+/g,' ');
}
function format_number(num){
     num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
    {
        num = "0";
    }
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
    {
        cents = "0" + cents;
    }
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
    {
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    }
    return (((sign) ? '' : '-')  + num + '.' + cents);
}
function replaceByChar(value,delimiter,setChar) {
    var setChar = (setChar == undefined || setChar == '') ? '&<comma>&' : setChar;
    var str_splited = value.toString().split(delimiter), str = '';
    for(var i=0;i<str_splited.length;i++) {
        str += str_splited[i] + setChar;
    }
    return str.substr(0,(str.length)-(setChar.length));
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function check_number(value){
    //if (value.match(/[^\d]/)){
    //    return false;
    //}
    //else{
    //    return true;
    //}
    if (isNaN(value)){
        return false;
    }
    return true;
}
function check_email(value){
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(value);
}
function curreny_converter(num){
    var p = parseFloat(num).toFixed(2).split(".");
    return "P " + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}
function clear_value(form_name){
    $(form_name+ " input[type=text],"+form_name+ " textarea").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    $(form_name+ " input[type=password]").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    $(form_name+ " input[type=hidden]").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    $(form_name+ " input[type=file]").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    $(form_name+ " input[type=hidden]").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    $(form_name+ " input[type=email]").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    $(form_name+ " select").each(function(){
        if ( !$(this).hasClass('do-not-clear') ) $(this).val("");
    });
    return false;
}

function get_number(num){
    num = num.replace(/\$|\,/g, '');
    num = num.replace('%','');
    return num;
}

function addDialogTitle(message){
     $('.ui-dialog .ui-dialog-title .widget-header h4').text('');
    $('.ui-dialog .ui-dialog-title .widget-header h4').append(message);
}

function addProgressMessage(message){
    $('.MessageBoxContainer .MsgTitle center p').remove();
    $('.MessageBoxContainer .MsgTitle center').append('<p>'+message+'</p>');
}
function ajaxRequest(url,data,dataType,async,progress){
    var contentType = contentType == undefined ? 'POST' : contentType;
    var dataType = dataType == undefined ? 'json' : dataType;
    var progress = progress == undefined ? true : progress;
    var async = async == undefined ? false : async;
    $('.bootbox-confirm').modal('hide');
    $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var return_data;
    jQuery.ajax({
        url: url,
        dataType: dataType,
        data: data,
        type: contentType,
        async: async,
        beforeSend: function() {
            if ( progress ) progressBar('on');
        },
        success: function(data) {
            return_data = data;
            progressBar('off');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            if ( progress ) progressBar('off');
            msgbox('error','There was an error occurred!');
            if(xhr.status  == 401 ){
                location.reload();
            }
            //alert(thrownError);
            msgbox('error',thrownError);

        }
    });
    return return_data;
}
function setAjaxRequest(url,data,success,error,dataType,async,progress){
    var dataType= dataType == undefined ? 'json' : dataType;
    var progress = progress == undefined ? true : progress;
    var async = async == undefined ? false : async;
    $('.bootbox-confirm').modal('hide');

    if (error == undefined) {
        error = function(err, textStatus, xhr) {
            if ( progress ) progressBar('off');
            msgbox('error',textStatus);
            if(!navigator.onLine){
                alert('You are disconnected!');
                location.reload();
            }
        }
    }

    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: url,
        dataType: dataType,
        data: data,
        type: 'POST',
        async: async,
        beforeSend: function() {
            if ( progress ) progressBar('on');
        },
        success: success,
        error: error
    });
    return;
}
function ajaxRequestFile(url,data,_callback,dataType,async,progress){
    var contentType = contentType == undefined ? 'POST' : contentType;
    var dataType = dataType == undefined ? 'json' : dataType;
    var progress = progress == undefined ? true : progress;
    var async = async == undefined ? false : async;
    $('.bootbox-confirm').modal('hide');
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: url,
        dataType: dataType,
        data: data,
        type: 'POST',
        contentType: false,
        processData: false,
        async: async,
        success: _callback
   });
}
function filteringErrors(message) {
    var str_message = '';
    for(i in message) {
        str_message+=message[i]+'\n';
    }
    msgbox('error',str_message);
}
// check if a valid number
function isValidNum(field_id){
    var val = $('#'+field_id).val();
    if (isNaN(val)){
        return false;
    }
    return true;
}
function isValidField(name,parentIndex){
    var tag = $('#'+name);
    var val = tag.val();
    var parent = tag;
    for(var i=0;i<parentIndex;i++){
        parent = parent.parent();
    }
    parent.removeClass('error');
    if (val == ""){
        parent.addClass('error');
        return false;
    }
    return true;
}
// change current Date to a Given Format
function dateFormat(input_string,format){
    var current = new Date(input_string);
    return current.format(format);
}
// check if a input is in a data of array
function inArray(input,data){
    var status = false;
    for(var i=0;i<data.length;i++){
        if (input == data[i]){
            status = true;
            break;
        }
    }
    return status;
}

function msgbox(shortCutFunction,msg,title,time,position) {
    var timeout=2000;
    var title=title == undefined ? '' : title;
    timeout=time == undefined ? timeout : time;
    toastr.options.timeOut = timeout;
    toastr.options.hideDuration = timeout;
    toastr.options.timeOut =timeout;
    toastr.options.extendedTimeOut = timeout;
    position = (position == undefined ? 'toast-top-right' : position );
    shortCutFunction = (shortCutFunction == 'danger' ?  'error' : shortCutFunction);
    var shortCutFunction = shortCutFunction;
    toastr.options = {
        closeButton: true,
        positionClass: position,
        onclick: null
    };
    var $toast = toastr[shortCutFunction](msg, title);
    return 0;
}

function progressBar(action){
    if ( action == 'on' ){
        progressBarModal();
   } else {
        var delay=800;
        setTimeout(function(){
            unloadProgressBarModal();
        },delay);
    }
}

function militaryToStandardTime(time){
    if(time =='' || time == undefined ){
        return '';
    }
   var x = time.substring(0,1) ;
   if(x > 2){ time = '0'+ time; }

   var xtime = time.substring(0,4) ;

   var hours24 = parseInt(xtime.substring(0, 2),10);
    var hours = ((hours24 + 11) % 12) + 1;
    var amPm = hours24 > 11 ? ' PM' : ' AM';
    var minutes = xtime.substring(2);

    return hours + ':' + minutes + amPm;

}

function StandardTomilitaryTime(time){
    if(time =='' || time == undefined ){
        return '';
    }

   var output = time.replace(":", "");
   var ispm = time.indexOf("PM");

   output = output.replace("AM", "");
   output = output.replace("PM", "");
   
   if(ispm > 0 && parseInt(output) < 1200){
      output = parseInt(output)+1200;
   }

   return $.trim(output);

}

window['strCast']=function(v,t){
    var result ;
    switch(t){
        case 'int':
            result = 0;
            if (v) {
                v =  ((v.trim() == '' || v == undefined) ? 0 : v.replace(',','')) ;
                result = parseInt(v);
            }
        break;
        case 'float' :
            result = 0;
            if (v) {
                v =  ((v == '' || v == undefined) ? 0 : v.replace(',','')) ;
                result = parseFloat(v);
            }
        break;
        case 'string':
            v =  ((v == '' || v == undefined) ? '' : v  ) ;
            result = v.trim();
        break;
        default :
            v =  ((v == '' || v == undefined) ? '' : v  ) ;
            result = v.trim();
        break;
    }
    return result;
}



//===============================================================================================================
//Add from CICS general.js
//Added by   : Jeremiah James G. Samson
//Date Added : 2016-01-13
// format('#,##0.00',1000)
window['format'] = function(m, v) {
	if (!m || isNaN(+v)) {
		return v;
	}
	var v = m.charAt(0) == '-' ? -v : +v;
	var isNegative = v < 0 ? v = -v : 0;
	var result = m.match(/[^\d\-\+#]/g);
	var Decimal = (result && result[result.length - 1]) || '.';
	var Group = (result && result[1] && result[0]) || ',';
	var m = m.split(Decimal);
	v = v.toFixed(m[1] && m[1].length);
	v = +(v) + '';
	var pos_trail_zero = m[1] && m[1].lastIndexOf('0');
	var part = v.split('.');
	if (!part[1] || part[1] && part[1].length <= pos_trail_zero) {
		v = (+v).toFixed(pos_trail_zero + 1);
	}
	var szSep = m[0].split(Group);
	m[0] = szSep.join('');
	var pos_lead_zero = m[0] && m[0].indexOf('0');
	if (pos_lead_zero > -1) {
		while (part[0].length < (m[0].length - pos_lead_zero)) {
			part[0] = '0' + part[0];
		}
	} else if (+part[0] == 0) {
		part[0] = '';
	}
	v = v.split('.');
	v[0] = part[0];
	var pos_separator = (szSep[1] && szSep[szSep.length - 1].length);
	if (pos_separator) {
		var integer = v[0];
		var str = '';
		var offset = integer.length % pos_separator;
		for (var i = 0, l = integer.length; i < l; i++) {
			str += integer.charAt(i);
			if (!((i - offset + 1) % pos_separator) && i < l - pos_separator) {
				str += Group;
			}
		}
		v[0] = str;
	}
	v[1] = (m[1] && v[1]) ? Decimal + v[1] : "";
	return (isNegative ? '-' : '') + v[0] + v[1];
};
//============================================================================================
var sysfnc = {
    set: function(c,v){
        localStorage.setItem(c, v);
    },
    get: function(c){
        localStorage.getItem(c);
    },
    serializeArrtoJSON : function($form){
         var data = $form.serializeArray();
         var fields = {};
         for (var i in data) {
            fields[data[i].name] = data[i].value;
         }
         return fields;
    },
    table_resequence : function($tbl){
        var i = 1;
        $tbl.find('tbody tr').each(function(){
            $(this).find('.seq').html(i);
            i++;
        });
    },
    div_autoheight: function($tbl) {
        var bh = 70;
        var bc = 10;
        var bf = 92;
        var $mydiv = $($tbl);
        var divheight = (window.innerHeight)-bf-bh-bc-$mydiv.position().top;
        $mydiv.css({'height' : divheight ,});

    },

    showAlert : function(container, alert, message, closable, reset, focus,icon, time ) {

        Metronic.alert({
            container: container  , // alerts parent container(by default placed after the page breadcrumbs)
            place: 'prepend', // append or prepent in container
            type: alert,  // alert's type
            message: message,  // alert's message
            close: closable, // make alert closable
            reset: reset, // close all previouse alerts first
            focus: focus, // auto scroll to the alert after shown
            closeInSeconds: time, // auto close after defined seconds
            icon: icon // put icon before the message
        });
    },
    
    msg :  function(title, message){
        $.SmartMessageBox({title : "<h2 class='text-warning'> "+title+" </h2>",content : message, buttons : '[Close]'});
    },
    
    msgcb: function(title, message, callback){
        $.SmartMessageBox({title : "<h2 class='text-warning'> "+title+" </h2>",content : message, buttons : '[Cancel][Yes]'},function(ButtonPressed){
            if (ButtonPressed === "Yes") {
                callback();
            }
        });
    },
    
    inputBox: function(title, message, input , callback){
        
        $.SmartMessageBox({title : "<h2 class='text-warning'> "+title+" </h2>",content : message, buttons : '[Cancel][Ok]', input : input, inputValue : '' },function(ButtonPressed, Value){
            if (ButtonPressed === "Ok") {
                callback(Value);
            }
        });
    },
    
    parseDate : function(xdate){
         var d = new Date(xdate),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear(),
            hour = d.getHours(),
            min = d.getMinutes(),
            sec = d.getSeconds();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
    
        return [year, month, day].join('-') + ' ' + [hour, min, sec].join(':') ;
    
    }, 
    sum : function(arg){
        result = 0;
        var i = 0 ;
        
           for ( i = 0; i < arg.length; i++) {
            result = parseInt(result) + parseInt(arg[i]);
          }
  
        return result;

    },
    
};

if(!String.prototype.trim){
  String.prototype.trim = function(){
    return this.replace(/^\s+|\s+$/g,'');
  };
}    

var isMobile = function(){
  try{
     document.createEvent("TouchEvent");
     return true;
  }catch(e){
    return false;
  }
    
};

var xmlHttp;
var IsConnected=1;
var retry_connect = 0;
function srvTime(){    
    try {
        //FF, Opera, Safari, Chrome
        xmlHttp = new XMLHttpRequest();
    }
    catch (err1) {
        //IE
        try {
            xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch (err2) {
            try {
                xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (eerr3) {
                //AJAX not supported, use CPU time.
                alert("AJAX not supported");
            }
        }
    }
    
    try {  
        xmlHttp.open('HEAD',window.location.href.toString(),false);
        xmlHttp.setRequestHeader("Content-Type", "text/html");
        xmlHttp.send('');
        return xmlHttp.getResponseHeader("Date");
    } catch (err3){
        if(retry_connect == 3){
            alert('Your device is disconnected. Please check your connection.');
            location.reload();
        }            
        IsConnected = 0;         
        retry_connect++;   
        return (new Date()).toLocaleDateString()+toLocaleTimeString();
    }
    
}