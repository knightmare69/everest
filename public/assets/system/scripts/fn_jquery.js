$(window).load(function(){
   clearconsole();
});
$(document).ready(function(){

    var time_idle = 7200*1000; // Set time idle in seconds

	$(window).bind("mousemove mouseclick keydown mousewheel",function (){ //user interaction on the page
		if ( window.idleTimeout )
		{
			clearTimeout( window.idleTimeout );
		}
		window.idleTimeout = setTimeout( triggerIdle, time_idle );
	});

	function triggerIdle()
	{
		alert("Session has expired, Please Login again!");
		window.location = base_url+'logout';
	}

// JQUERY PLUGINS

    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title : function(title) {
            if (!this.options.title) {
                    title.html("&#160;");
            } else {
                    title.html(this.options.title);
            }
        }
    }));
    $('.dialog-message').dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            title : "<div class='widget-header'><h4><i class='fa fa-info'></i>Message</h4></div>",
            buttons : [ {
                    html : "<i class='fa fa-times'></i>&nbsp; CLOSE",
                    "class" : "btn btn-default",
                    click : function() {
                            $(this).dialog("close");
                    }
            }]
    });
    $('.ajax-dialogbox').dialog({
            autoOpen : false,
            width : 300,
            resizable : false,
            modal : true,
            title : "&nbsp;"
    });
    //ALERTS POP UP

//FORMATING INPUTS TO NUMBER ONLY
    $("body").on('keydown','.numberonly',function (e) {
        var allowed = $(this).attr('data-allow') == undefined ? '' : $(this).attr('data-allow').trim();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39) ||
            //Allowed code in attribute
            (e.keyCode == allowed && allowed != '')) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (allowed == ''  || e.keyCode != allowed)) {
            e.preventDefault();
        }
    });

    $(".filter-table").on('change',function () {
        var tblname = $(this).attr('data-table');
        var data = this.value.split(" ");
        var r = $("#"+tblname+" tbody").find("tr");
        if (this.value == "") {
            r.show();
            return;
        }
        r.hide();
        r.filter(function (i, v) {
            var $t = $(this);
            for (var d = 0; d < data.length; ++d) {
                if ($t.text().toLowerCase().indexOf(data[d].toLowerCase()) > -1) {
                    return true;
                }
            }
            return false;
        })
        .show();
    }).focus(function () {
        this.value = "";
        $(this).css({ "color": "black"});
        $(this).unbind('focus');
    }).css({"color": "#C0C0C0"});


    $('body').on('click','.table.table-selection tbody tr,.table.table-hover tbody tr',function(e){
        $(this).closest('.table').find('tbody > tr').removeClass('active-row');
        $(this).addClass('active-row');
    });

    var sidebarmenu = function(){
        var search = window.location.search;
        var url = window.location.href.replace(search,'').replace('#','');

        $('.page-sidebar-menu li').each(function(){
            var href = $(this).find('a').attr('href');

           if( href  == url ){
                $(this).addClass('active');
                if( $(this).parent().hasClass('sub-menu') ){
                    $(this).parent().parent().addClass('active');
                }
                return true;
           }
        });

        $('.page-sidebar-menu').find('.sub-menu').each(function(){
            var count = $(this).find('li').length;
            if( count  === 0 ){
                // console.log($(this).parent().find('.title').html() + ' no sub-menu');
                $(this).parent().remove();
            }
        });
    }
    sidebarmenu();
});
