$(document).ready(function(){
  $('body').on('change','[data-id]',function(){
	 $(this).addClass('henshin'); 
  });
  $('body').on('click','.btn-save',function(){
	 if($('.henshin').length>0){
		var data = [];
	    $('[data-id]').each(function(){
			var xid    = $(this).data('id');
			var desc   = $(this).data('desc');
			var val    = $(this).val();
			    val    = ((val==undefined || val=='' || val==' ')?'-1':val);
			var tmparr = {xid:xid,desc:desc,data:val};
			data.push(tmparr);	
		});
		if(data.length<=0){return false;}
		progressBar('on');
		setAjaxRequest(
	        base_url+'setup/grade-setup/event?event=save',{setup:data},
			function(r)
			{
			  window.location = base_url+'setup/grade-setup';
			},
			function(err)
			{
			  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to save!',function(){return;});	
			  progressBar('off');
			}
		);		 
	 }else 
	  window.location = base_url+'setup/grade-setup';
  });
});