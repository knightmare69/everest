$('body').ready(function(){
	$('body').on('click','.employee-remove-btn',function(){
	   $('.employee-search-btn').closest('.input-group').find('input[type="hidden"]').val(0);
	   $('.employee-search-btn').closest('.input-group').find('input[type="text"]').val('');
	});
	
	$('body').on('click','.btn_save',function(){
	
	});
	
	$('body').on('click','.employee-search-btn',function(){
	  setAjaxRequest(
		base_url+'setup/college/event',{event:'empl'},
		function(r){
		  if(r.error==false){
		    $('#frm_modal').find('.modal-body').html(r.content);
			$('#frm_modal').modal('show');
		  }else
		    confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load data!',function(){return;});
		},
		function(err){
		  confirmEvent('<i class="fa fa-warning text-danger"></i> Failed to load data!',function(){return;});
		});
      progressBar('off');
	});
	
	$('body').on('click','.btnselect',function(){
	   var tmpxid =$(this).closest('tr').attr('data-id');
	   var tmpdata=$(this).closest('tr').attr('data-name');
	   $('.employee-search-btn').closest('.input-group').find('input[type="hidden"]').val(tmpxid);
	   $('.employee-search-btn').closest('.input-group').find('input[type="text"]').val(tmpdata);
	   $('#frm_modal').modal('hide');
	});
});