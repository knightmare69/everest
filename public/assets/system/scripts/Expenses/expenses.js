var global_this;
var EXPENSES = function() {
	//get default component input
	var defaultComponent = function() {
		return $('#component_default tbody').html();
	}

	//remove default if there's no item on the row
	var removeDefault = function() {
		if ( $('#table_sales tbody tr').length > 1) {
			 $('#table_sales tbody tr.default_row').addClass('hide');
		} else {
			$('#table_sales tbody tr.default_row').removeClass('hide');
		}
	}

	//save component if key exist!
	var saveComponent = function(hasConfirm,showMsg) {
		var hasConfirm = hasConfirm == undefined ? true : hasConfirm;
		var showMsg = showMsg == undefined ? true : showMsg;

		if (validatesItem()) {
			if (hasConfirm == true) {
				bootbox.confirm({
		            size : 'small',
		            message : "Are you sure you want to save this?",
		            callback : function(yes) {
		               if ( yes ) {
		               		save();
		               }
		            }
		        }); 
			} else {
				save();
			}
			
		} else {
			return msgbox('error',"Please see erros highligted with red.");
		}

		function save() {
        	var data = new Array, i = 0;
			$('#table_sales tbody tr.include').each(function() {
				var self = $(this);
				data.push(JSON.stringify({
					key: self.attr('data-id') == undefined ? '' : self.attr('data-id'),
					code: self.find('.component_no').val(),
					name: self.find('.component_part_desc').attr('data-name'),
					desc: self.find('.component_part_desc').attr('data-desc'),
					category: self.find('.category option:selected').val(),
					unit: self.find('.unit option:selected').val(),
					quantity: self.find('.quantity').val(),
					cost: self.find('.cost').val(),
					sort: i++
				}));
			});
		
			var result = ajaxRequest(base_url+page_url+'event','event=save'+'&items='+data);
			if (result.error == false) {
				//parsing of assigned item number
				// assigneItemNumber(result.content.item);
       			return showMsg == true ? msgbox('success',result.message) : '';
       		}
       		return msgbox('error',result.message);
        }
	}

	//assigned item number to newly created item
	var assigneItemNumber = function(data) {
		for (i in data) {
			var row = $('#table_component tbody tr.include').eq(data[i].sort);
			if (row != undefined) {
				row.find('.component_no').val(data[i].item_no);
				row.find('.component_no').removeClass('font-red');
				row.find('.component_no').addClass('font-green');
			}
		}
	}

	//validates item
	var validatesItem = function() {
		var ret = true;
		$('#table_sales tbody tr.include').each(function() {
			var self = $(this);

			self.find('.component_part_desc').removeClass('label-danger');
			self.find('.component_part_desc').addClass('label-primary');

			if (
				(self.find('.component_part_desc').attr('data-desc') == '' ||  self.find('.component_part_desc').attr('data-desc') == undefined)
			) {
				self.find('.component_part_desc').removeClass('label-primary');
				self.find('.component_part_desc').addClass('label-danger');
				ret = false;
			}
			self.find('.quantity').removeAttr('style');
			if (self.find('.quantity').val().trim() == '' || self.find('.produce').val() <= 0) {
				self.find('.quantity').attr('style','border: 1px solid red !important');
				ret = false;
			}
		});

		if ($('#table_sales tbody tr.include').length <= 0) {
			ret = false;
		}
		return ret;
	}

	//remove component
	var removeComponent = function(key,self) {
		if (key == undefined || key == '') {
			return self.closest('tr').remove();
		}

		var result = ajaxRequest(base_url+'property/component/event','event=remove&key='+key);
		if (result.error == false) {
			self.closest('tr').remove();
   			return  msgbox('success',result.message);
   		}
   		return msgbox('error',result.message);
	}

	//load components via ajax async
	var loadComponents = function(AssetKey) {
		setAjaxRequest(
			base_url+'property/component/event',
			'event=loadComponents&assetKey='+AssetKey,
			function(result) {
				$('#tab_components_wrapper').html(result);
			},
			'','html',true,false
		);
	}

	//save inputed component desc
	var saveComponentDesc = function(el) {
		var desc = el.closest('.modal-content').find('.modal-body .component_desc').val();
		var name = el.closest('.modal-content').find('.modal-body .component_name').val();
		var serial = el.closest('.modal-content').find('.modal-body .component_serial_no').val();
		var cat = el.closest('.modal-content').find('.modal-body #component_category option:selected').val();
		var type = el.closest('.modal-content').find('.modal-body #categoryType option:selected').val();
		
		global_this.attr('data-desc',desc);
		global_this.attr('data-name',name);
		global_this.attr('data-cat',cat);
		global_this.attr('data-serial',serial);
		global_this.attr('data-type',type);

		$('.span_component_save_result').removeClass('hide');
		setTimeout(function() {
			$('.span_component_save_result').addClass('hide');
		},2000);
	}

	//asynchronose search ajax request
	var partNumberSearch = function(self,value) {
		setAjaxRequest(
			base_url+page_url+'event',
			'event=searchStock&code='+value,
			function(result) {
				var tr = self.closest('tr');
				if (result.error == false) {
					tr.find('.component_no').removeClass('font-red');
					tr.find('.component_no').addClass('font-green');
				} else {
					tr.find('.component_no').removeClass('font-green');
					tr.find('.component_no').addClass('font-red');
				}
				
			var desc = getResultField(result.content[0].description);
				var name = getResultField(result.content[0].name);

				tr.find('.component_part_desc').attr('data-desc',desc);
				tr.find('.component_part_desc').attr('data-name',name);
				
				function getResultField(field) {
					if (field != undefined) {
						return field;
					}
					return ;
				}

			},'','JSON',true,false
		);
	}

	//show modal cost history
	var showCostHistory = function(self) {
		setAjaxRequest(
			base_url+'property/component/event',
			'event=showCostHistory&part_no='+self.closest('tr').find('.component_no').val(),
			function(result) {
				if(result.error == false) {
					basicModal(result.content,'Cost History');
					$('#modal_basic .modal-header').addClass('modal-header-bg');
				}
				progressBar('off');
			}
		);
	}

	//append cost in a component table column cost
	var chooseCost = function(self) {	
		//append selected cost to component price
		global_this.closest('tr').find('.component_price').val(self.find('td').eq(1).attr('data-cost'));

		//remove all class font-green

		self.parent().find('tr td .fa-check').removeClass('font-green');
	
		//add class font-green to the current selected cost
		self.find('td').eq(2).find('i').addClass('font-green');
	}
	//show modal and add/edit description
	var addComponentDesc = function(el)  {		
		var desc = el.attr('data-desc') == undefined ? '' : el.attr('data-desc');
		var name = el.attr('data-name') == undefined ? '' : el.attr('data-name');
		var serial = el.attr('data-serial') == undefined ? '' : el.attr('data-serial');
		var cat = el.attr('data-cat') == undefined ? '' : el.attr('data-cat');
		var type = el.attr('data-type') == undefined ? '' : el.attr('data-type');

		global_this = el;

		showModal({
			name: 'basic',
            title : 'Stock',
            content: ajaxRequest(base_url+page_url+'event','event=modalStockDesc&cat='+cat,'HTML'),
            class: 'modal_stockmaster',
            button: { 
                icon: 'fa fa-save',
                class: 'btn_component_save_desc',
                caption: 'Save'
            }
        });

		var modal = $('#modal_basic ');

		modal.find('.modal-content').find('.modal-body .component_desc').val(desc);
		modal.find('.modal-content').find('.modal-body .component_name').val(name);
		modal.find('.modal-content').find('.modal-body .component_serial_no').val(serial);

		$('#modal_basic .modal-footer .btn_basic').addClass('btn_component_save_desc');
		$('#modal_basic .modal-header').addClass('modal-header-bg');
	}

	//load all categories by category type
	var loadCategories = function(el) {
		setAjaxRequest(
			base_url+'property/component/event',
			'event=loadCategory&key='+el.find('option:selected').val(),
			function(result) {
				progressBar('off');
				loadCategory(result);
			}
		);
	}

	//parse category to html elemenent #category
	var loadCategory = function(data) {
		var options = '';
		for(i in data) {
			var option = document.createElement('option');
			option.setAttribute('value',data[i].id);
			option.appendChild(document.createTextNode(data[i].name));
			options += option.outerHTML;
		}
		$('#component_category').html(options);
	}

	return {
		loadComponents: function(key) {
			return loadComponents(key);
		},

		saveComponent: function(hasConfirm,showMsg) {
			return saveComponent(hasConfirm,showMsg);
		},

		init: function() {
			$(window).load(function() {
				// $('#table_component tbody').append(defaultComponent());
			});

			//handle adding new table part row
			$('body').on('click','#btn_add_component',function() {
				$('#table_sales tbody').append(defaultComponent());
				removeDefault();
			});

			//handle saving of components
			$('body').on('click','#btn_save_component',function() {
		        return saveComponent();	
			});

			//handle asynchronouse search for part no# on keypress
			$('body').on('bind keypress','.component_no',function() {
				isDebugging = false;
				// partNumberSearch($(this),$(this).val());
				isDebugging = true;
			});

			//handle asynchronouse search for part no# on click
			$('body').on('click','.component_no_search',function() {
				isDebugging = false;
				partNumberSearch($(this),$(this).closest('td').find('.component_no').val());
				isDebugging = true;
			});

			//handle removal of component/s
			$('body').on('click','.component_remove',function() {
				var self = $(this);
				bootbox.confirm(
		        {
		            size : 'small',
		            message : "Are you sure you want to remove this?",
		            callback : function(yes) {
		               if ( yes )
		               {
		               		if (self.attr('data-id') != undefined || self.attr('data-id') != '') {
		               			removeComponent(self.closest('tr').attr('data-id'),self);
		               		}
							removeDefault();
		               }
		            }
		        }); 
			});
			
			//handle show of cost history
			$('body').on('click','.component_price_search',function() {
				global_this = $(this);
				showCostHistory($(this));
			});

			//handle selecting of cost in a modal table
			$('body').on('click','#table_cost_history tbody tr',function() {
				chooseCost($(this));
			});

			//handle show modal for adding of description
			$('body').on('click','.component_part_desc',function() {
				addComponentDesc($(this));
			});

			//handle appending desc via bind input
			$('body').on('bind keyup','.component_desc',function() {
				//global_this.attr('data-desc',$(this).val());
			});

			//handle appending name via bind input
			$('body').on('bind keyup','.component_name',function() {
				//global_this.attr('data-name',$(this).val());
			});


			//handle save component desc input
			$('body').on('click','.btn_component_save_desc',function() {
				saveComponentDesc($(this));
				validatesItem();
			});

			$('body').on('change','#categoryType',function() {
				loadCategories($(this));
			});
		}
	}
}();