﻿function cc_onkeyup() {
    var scream1 = document.getElementById('ccform_cardnumber_TB').value;
    var scream = scream1.substring(0, 1);

    var scream2 = scream1.substring(scream1.length, scream1.length - 1);

    var ctypes = document.getElementById("ccform_cardtype_TB");

    if (isNaN(scream2)) {
        document.getElementById("ccform_cardnumber_TB").value = "";
        document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
    }
    else {
        //visa
        if (scream == "4") {

            //start check available card type
            var i;
            var ctype = "";
            var check = false;
            for (i = 0; i < ctypes.length; i++) {
                if (ctypes.options[i].value == "visa") {
                    check = true;
                }
            }

            if (check) {
                document.getElementById("ccform_cardtype_TB").value = "visa";
                document.getElementById("imgCard").style.display = "block";
                document.getElementById("imgCard").src = "images/cc-icons/visa.png";

                if (scream1.length == 16) {
                    $("#ccform_cvv_TB").attr('maxlength', '3');
                    document.getElementById("ccform_expmonth_DD2").focus();
                }
                else if (scream1.length >= 17) {
                    document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                }
            }
            else {
                document.getElementById("ccform_cardnumber_TB").value = "";
            }
        }

        //master
        else if (scream == "5") {

            //start check available card type
            var i;
            var ctype = "";
            var check = false;
            for (i = 0; i < ctypes.length; i++) {
                if (ctypes.options[i].value == "mastercard") {
                    check = true;
                }
            }

            if (check) {
                document.getElementById("ccform_cardtype_TB").value = "mastercard";
                document.getElementById("imgCard").style.display = "block";
                document.getElementById("imgCard").src = "images/cc-icons/master.png";

                if (scream1.length == 16) {
                    $("#ccform_cvv_TB").attr('maxlength', '3');
                    document.getElementById("ccform_expmonth_DD2").focus();
                    document.getElementById("ccform_expmonth_DD2").click();
                }
                else if (scream1.length >= 17) {
                    document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                }
            }
            else {
                document.getElementById("ccform_cardnumber_TB").value = "";
            }
        }

        //amex
        else if (scream == "3") {
            var first2 = scream1.substring(0, 2);
            if (first2.length == 2 && (first2 == '34' || first2 == '37')) {

                //start check available card type
                var i;
                var ctype = "";
                var check = false;
                for (i = 0; i < ctypes.length; i++) {
                    if (ctypes.options[i].value == "amex") {
                        check = true;
                    }
                }

                if (check) {
                    document.getElementById("ccform_cardtype_TB").value = "amex";
                    document.getElementById("imgCard").style.display = "block";
                    document.getElementById("imgCard").src = "images/cc-icons/amex.png";

                    if (scream1.length == 15) {
                        $("#ccform_cvv_TB").attr('maxlength', '4');
                        document.getElementById("ccform_expmonth_DD2").focus();
                        document.getElementById("ccform_expmonth_DD2").click();
                    }
                    else if (scream1.length >= 16) {
                        document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                    }
                }
                else {
                    document.getElementById("ccform_cardnumber_TB").value = "";
                }
            }
            else if (first2.length == 2 && first2 == '35') {

                //start check available card type
                var i;
                var ctype = "";
                var check = false;
                for (i = 0; i < ctypes.length; i++) {
                    if (ctypes.options[i].value == "jcb") {
                        check = true;
                    }
                }

                if (check) {
                    document.getElementById("ccform_cardtype_TB").value = "jcb";
                    document.getElementById("imgCard").style.display = "block";
                    document.getElementById("imgCard").src = "images/cc-icons/jcb.png";

                    if (scream1.length == 16) {
                        $("#ccform_cvv_TB").attr('maxlength', '3');
                        document.getElementById("ccform_expmonth_DD2").focus();
                        document.getElementById("ccform_expmonth_DD2").click();
                    }
                    else if (scream1.length >= 17) {
                        document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                    }
                }
                else {
                    document.getElementById("ccform_cardnumber_TB").value = "";
                }
            }
            else if (first2.length == 1) {
                //start check available card type
                var i;
                var ctype = "";
                var check_jcb = false;
                var check_amex = false;
                for (i = 0; i < ctypes.length; i++) {
                    if (ctypes.options[i].value == "jcb") {
                        check_jcb = true;
                    }

                    if (ctypes.options[i].value == "amex") {
                        check_amex = true;
                    }
                }

                if (!check_jcb && !check_amex) {
                    document.getElementById("ccform_cardtype_TB").value = "";
                    document.getElementById("ccform_cardnumber_TB").value = "";
                }
            }
            else {
                document.getElementById("ccform_cardtype_TB").value = "";
                document.getElementById("ccform_cardnumber_TB").value = "";
            }
        }

        //jcb
        else if (scream == "1") {

            //start check available card type
            var i;
            var ctype = "";
            var check = false;
            for (i = 0; i < ctypes.length; i++) {
                if (ctypes.options[i].value == "jcb") {
                    check = true;
                }
            }

            if (check) {
                document.getElementById("ccform_cardtype_TB").value = "jcb";
                document.getElementById("imgCard").style.display = "block";
                document.getElementById("imgCard").src = "images/cc-icons/jcb.png";

                if (scream1.length == 16) {
                    $("#ccform_cvv_TB").attr('maxlength', '3');
                    document.getElementById("ccform_expmonth_DD2").focus();
                    document.getElementById("ccform_expmonth_DD2").click();
                }
                else if (scream1.length >= 17) {
                    document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                }
            }
            else {
                document.getElementById("ccform_cardnumber_TB").value = "";
            }
        }

        //master or jcb
        else if (scream == "2") {
            first6 = scream1.substring(0, 6);

            if (first6.length == 6 && (first6 >= 222100 && first6 <= 272099)) {
                //mastercard nga

                //start check available card type
                var i;
                var ctype = "";
                var check = false;
                for (i = 0; i < ctypes.length; i++) {
                    if (ctypes.options[i].value == "mastercard") {
                        check = true;
                    }
                }

                if (check) {
                    document.getElementById("ccform_cardtype_TB").value = "mastercard";
                    document.getElementById("imgCard").style.display = "block";
                    document.getElementById("imgCard").src = "images/cc-icons/master.png";

                    if (scream1.length == 16) {
                        $("#ccform_cvv_TB").attr('maxlength', '3');
                        document.getElementById("ccform_expmonth_DD2").focus();
                    }
                    else if (scream1.length >= 17) {
                        document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                    }
                }
                else {
                    document.getElementById("ccform_cardnumber_TB").value = "";
                }
            }
            else if (first6.length == 6 && (first6 < 222100 || first6 > 272099)) {
                //jcb pala

                //start check available card type
                var i;
                var ctype = "";
                var check = false;
                for (i = 0; i < ctypes.length; i++) {
                    if (ctypes.options[i].value == "jcb") {
                        check = true;
                    }
                }

                if (check) {
                    document.getElementById("ccform_cardtype_TB").value = "jcb";
                    document.getElementById("imgCard").style.display = "block";
                    document.getElementById("imgCard").src = "images/cc-icons/jcb.png";

                    if (scream1.length == 16) {
                        $("#ccform_cvv_TB").attr('maxlength', '3');
                        document.getElementById("ccform_expmonth_DD2").focus();
                        document.getElementById("ccform_expmonth_DD2").click();
                    }
                    else if (scream1.length >= 17) {
                        document.getElementById('ccform_cardnumber_TB').value = scream1.substring(0, scream1.length - 1);
                    }
                }
                else {
                    document.getElementById("ccform_cardnumber_TB").value = "";
                }
            }
            else {
                //start check available card type
                var i;
                var ctype = "";
                var check_jcb = false;
                var check_master = false;
                for (i = 0; i < ctypes.length; i++) {
                    if (ctypes.options[i].value == "jcb") {
                        check_jcb = true;
                    }
                    if (ctypes.options[i].value == "mastercard") {
                        check_master = true;
                    }
                }

                document.getElementById("imgCard").style.display = "none";
            }
        }
        else {
            document.getElementById("ccform_cardtype_TB").value = "";
            document.getElementById("ccform_cardnumber_TB").value = "";
        }

        if (scream1 == "") {
            document.getElementById("imgCard").style.display = "none";
        }
    }
}

function cc_exp_month_onblur() {
    var month = document.getElementById("ccform_expmonth_DD2").value;
    if (month.length == 1) {
        document.getElementById("ccform_expmonth_DD2").value = pad(month, 2);
    }
}

function cc_exp_year_onblur() {
    var year = document.getElementById("ccform_expyear_DD2").value;
    if (year.length < 4 && year.length != 0) {
        document.getElementById("ccform_expyear_DD2").value = "";
    }
    else { }
}

function cc_exp_year_focus() {
    var month = document.getElementById("ccform_expmonth_DD2").value;

    if (month < 1 || month > 12) {
        document.getElementById("ccform_expmonth_DD2").value = "";
        document.getElementById("ccform_expmonth_DD2").focus();
        document.getElementById("ccform_expmonth_DD2").click();
    }
}

function cc_exp_month_onkeyup() {

    var scream1 = document.getElementById('ccform_expmonth_DD2').value;
    var scream2 = scream1.substring(scream1.Length, scream1.Length);

    if (isNaN(scream2)) {
        document.getElementById("ccform_expmonth_DD2").value = "";
    }
    else {
        if (scream1.length == 2) {
            if (scream1 < 1) {
                document.getElementById("ccform_expmonth_DD2").value = "";
            }
            else if (scream1 > 12) {
                document.getElementById("ccform_expmonth_DD2").value = "";
            }
            else {
                document.getElementById("ccform_expyear_DD2").focus();
                document.getElementById("ccform_expyear_DD2").click();
            }
        }
        else if (scream1.length == 0) { }
        else
        {
        }
    }
}

function cc_exp_yr_onkeyup() {

    var scream1 = document.getElementById('ccform_expyear_DD2').value;
    var scream2 = scream1.substring(scream1.Length, scream1.Length);

    if (isNaN(scream2)) {
        document.getElementById("ccform_expyear_DD2").value = "";
    }
    else {
        if (scream1.length == 4) {
            if (scream1 < new Date().getFullYear()) {
                document.getElementById("ccform_expyear_DD2").value = "";
            }
            else if (scream1 == new Date().getFullYear()) {
                var input_month = document.getElementById('ccform_expmonth_DD2').value;
                var cur_month = new Date().getMonth();
                if (input_month <= cur_month) {
                    document.getElementById("ccform_expyear_DD2").value = "";
                }
                else {
                    document.getElementById("ccform_cvv_TB").focus();
                    document.getElementById("ccform_cvv_TB").click();
                }
            }
            else {
                document.getElementById("ccform_cvv_TB").focus();
                document.getElementById("ccform_cvv_TB").click();
            }
        }
        else if (scream1.length == 0) { }
        else { }
    }
}

function cc_cvv_focus() {
    var input_month = document.getElementById('ccform_expmonth_DD2').value;
    var current_month = new Date().getMonth();

    var year = document.getElementById("ccform_expyear_DD2").value;

    if (year < new Date().getFullYear()) {
        document.getElementById("ccform_expyear_DD2").value = "";
        document.getElementById("ccform_expyear_DD2").focus();
        document.getElementById("ccform_expyear_DD2").click();
    }
    else {
        if (year == new Date().getFullYear()) {
            if (input_month <= current_month) {
                document.getElementById("ccform_expyear_DD2").value = "";
                document.getElementById("ccform_expyear_DD2").focus();
                document.getElementById("ccform_expyear_DD2").click();
            }
        }
    }
}

function cc_cvv_onkeyup() {
    var type = document.getElementById('ccform_cardtype_TB').value;
    var data = document.getElementById('ccform_cvv_TB').value;

    var scream2 = data.substring(data.Length, data.Length);

    if (isNaN(scream2)) {
        document.getElementById("ccform_cvv_TB").value = "";

    }

    if (type == "visa" || type == "mastercard" || type == "jcb") {
        document.getElementById("ccform_cvv_TB").maxLength = 3;
        if (data.length == 3) {
            document.getElementById("ccform_cardholdername_TB").focus();
            document.getElementById("ccform_cardholdername_TB").click();
        }
        else if (data.length >= 4) {
            document.getElementById('ccform_cvv_TB').value = data.substring(0, data.length - 1);
        }
    }
    else if (type == "amex") {
        document.getElementById("ccform_cvv_TB").maxLength = 4;
        if (data.length == 4) {
            document.getElementById("ccform_cardholdername_TB").focus();
            document.getElementById("ccform_cardholdername_TB").click();
        }
        else if (data.length >= 5) {
            document.getElementById('ccform_cvv_TB').value = data.substring(0, data.length - 1);
        }
    }
}

function loadTerms(mid) {
    var td = document.getElementById("terms_display").innerText;

    if (td == "1") {
        document.getElementById("ccTerms").style.display = "block";
        document.getElementById("nonccTerms").style.display = "none";
        $("#mtac").load("terms-conditions/" + mid + ".html");
    }
}